.class public LX/6pZ;
.super LX/6pY;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile e:LX/6pZ;


# instance fields
.field public final b:LX/6p6;

.field public final c:LX/6o9;

.field public final d:LX/6oC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1149510
    const-class v0, LX/6pZ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6pZ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/6p6;LX/6o9;LX/6oC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1149479
    invoke-direct {p0}, LX/6pY;-><init>()V

    .line 1149480
    iput-object p1, p0, LX/6pZ;->b:LX/6p6;

    .line 1149481
    iput-object p2, p0, LX/6pZ;->c:LX/6o9;

    .line 1149482
    iput-object p3, p0, LX/6pZ;->d:LX/6oC;

    .line 1149483
    return-void
.end method

.method public static a(LX/0QB;)LX/6pZ;
    .locals 6

    .prologue
    .line 1149497
    sget-object v0, LX/6pZ;->e:LX/6pZ;

    if-nez v0, :cond_1

    .line 1149498
    const-class v1, LX/6pZ;

    monitor-enter v1

    .line 1149499
    :try_start_0
    sget-object v0, LX/6pZ;->e:LX/6pZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1149500
    if-eqz v2, :cond_0

    .line 1149501
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1149502
    new-instance p0, LX/6pZ;

    invoke-static {v0}, LX/6p6;->a(LX/0QB;)LX/6p6;

    move-result-object v3

    check-cast v3, LX/6p6;

    invoke-static {v0}, LX/6o9;->b(LX/0QB;)LX/6o9;

    move-result-object v4

    check-cast v4, LX/6o9;

    invoke-static {v0}, LX/6oC;->b(LX/0QB;)LX/6oC;

    move-result-object v5

    check-cast v5, LX/6oC;

    invoke-direct {p0, v3, v4, v5}, LX/6pZ;-><init>(LX/6p6;LX/6o9;LX/6oC;)V

    .line 1149503
    move-object v0, p0

    .line 1149504
    sput-object v0, LX/6pZ;->e:LX/6pZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1149505
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1149506
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1149507
    :cond_1
    sget-object v0, LX/6pZ;->e:LX/6pZ;

    return-object v0

    .line 1149508
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1149509
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static f(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1149494
    const v0, 0x7f081ddd

    invoke-virtual {p0, v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(I)V

    .line 1149495
    invoke-virtual {p0, p1, p2}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V

    .line 1149496
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/6pM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1149493
    sget-object v0, LX/6pM;->CHANGE_ENTER_OLD:LX/6pM;

    sget-object v1, LX/6pM;->CHANGE_CREATE_NEW:LX/6pM;

    sget-object v2, LX/6pM;->CHANGE_CREATE_NEW_CONFIRMATION:LX/6pM;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)LX/6oc;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1149492
    new-instance v0, LX/6pS;

    invoke-direct {v0, p0, p1}, LX/6pS;-><init>(LX/6pZ;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;LX/6pM;)LX/6od;
    .locals 3

    .prologue
    .line 1149486
    sget-object v0, LX/6pX;->a:[I

    invoke-virtual {p3}, LX/6pM;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1149487
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected page: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1149488
    :pswitch_0
    new-instance v0, LX/6pO;

    invoke-direct {v0, p0, p1, p1, p2}, LX/6pO;-><init>(LX/6pZ;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;)V

    .line 1149489
    :goto_0
    return-object v0

    .line 1149490
    :pswitch_1
    new-instance v0, LX/6pQ;

    invoke-direct {v0, p0, p1}, LX/6pQ;-><init>(LX/6pZ;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    goto :goto_0

    .line 1149491
    :pswitch_2
    new-instance v0, LX/6pR;

    invoke-direct {v0, p0, p1, p2}, LX/6pR;-><init>(LX/6pZ;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1149484
    iget-object v0, p0, LX/6pZ;->b:LX/6p6;

    invoke-virtual {v0}, LX/6p6;->a()V

    .line 1149485
    return-void
.end method
