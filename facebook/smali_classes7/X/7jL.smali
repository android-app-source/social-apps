.class public final LX/7jL;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1229287
    const-class v1, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;

    const v0, -0x19708ffa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "CommerceProductCreateMutation"

    const-string v6, "0f89c15cc7de4ced170744296b8a6b73"

    const-string v7, "commerce_product_item_create"

    const-string v8, "2"

    const-string v9, "10155069963191729"

    const/4 v10, 0x0

    .line 1229288
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1229289
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1229290
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1229291
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1229292
    sparse-switch v0, :sswitch_data_0

    .line 1229293
    :goto_0
    return-object p1

    .line 1229294
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1229295
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1229296
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1229297
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6ef528d5 -> :sswitch_0
        0x5fb57ca -> :sswitch_2
        0x683094a -> :sswitch_3
        0x1d1017c1 -> :sswitch_1
    .end sparse-switch
.end method
