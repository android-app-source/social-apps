.class public final enum LX/7Rq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Rq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Rq;

.field public static final enum APP_IN_BACKGROUND:LX/7Rq;

.field public static final enum PLAYER_DOCK_POSITION:LX/7Rq;

.field public static final enum SESSION_ELAPSED_TIME:LX/7Rq;

.field public static final enum SESSION_ID:LX/7Rq;

.field public static final enum VIDEO_ID:LX/7Rq;

.field public static final enum VIDEO_TIME_POSITION:LX/7Rq;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1207914
    new-instance v0, LX/7Rq;

    const-string v1, "SESSION_ID"

    const-string v2, "session_id"

    invoke-direct {v0, v1, v4, v2}, LX/7Rq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rq;->SESSION_ID:LX/7Rq;

    .line 1207915
    new-instance v0, LX/7Rq;

    const-string v1, "SESSION_ELAPSED_TIME"

    const-string v2, "session_elapsed_time"

    invoke-direct {v0, v1, v5, v2}, LX/7Rq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rq;->SESSION_ELAPSED_TIME:LX/7Rq;

    .line 1207916
    new-instance v0, LX/7Rq;

    const-string v1, "VIDEO_ID"

    const-string v2, "video_id"

    invoke-direct {v0, v1, v6, v2}, LX/7Rq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rq;->VIDEO_ID:LX/7Rq;

    .line 1207917
    new-instance v0, LX/7Rq;

    const-string v1, "VIDEO_TIME_POSITION"

    const-string v2, "video_time_position"

    invoke-direct {v0, v1, v7, v2}, LX/7Rq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rq;->VIDEO_TIME_POSITION:LX/7Rq;

    .line 1207918
    new-instance v0, LX/7Rq;

    const-string v1, "APP_IN_BACKGROUND"

    const-string v2, "app_in_background"

    invoke-direct {v0, v1, v8, v2}, LX/7Rq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rq;->APP_IN_BACKGROUND:LX/7Rq;

    .line 1207919
    new-instance v0, LX/7Rq;

    const-string v1, "PLAYER_DOCK_POSITION"

    const/4 v2, 0x5

    const-string v3, "player_dock_position"

    invoke-direct {v0, v1, v2, v3}, LX/7Rq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rq;->PLAYER_DOCK_POSITION:LX/7Rq;

    .line 1207920
    const/4 v0, 0x6

    new-array v0, v0, [LX/7Rq;

    sget-object v1, LX/7Rq;->SESSION_ID:LX/7Rq;

    aput-object v1, v0, v4

    sget-object v1, LX/7Rq;->SESSION_ELAPSED_TIME:LX/7Rq;

    aput-object v1, v0, v5

    sget-object v1, LX/7Rq;->VIDEO_ID:LX/7Rq;

    aput-object v1, v0, v6

    sget-object v1, LX/7Rq;->VIDEO_TIME_POSITION:LX/7Rq;

    aput-object v1, v0, v7

    sget-object v1, LX/7Rq;->APP_IN_BACKGROUND:LX/7Rq;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7Rq;->PLAYER_DOCK_POSITION:LX/7Rq;

    aput-object v2, v0, v1

    sput-object v0, LX/7Rq;->$VALUES:[LX/7Rq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1207903
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1207904
    iput-object p3, p0, LX/7Rq;->value:Ljava/lang/String;

    .line 1207905
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/7Rq;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1207908
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1207909
    :cond_0
    :goto_0
    return-object v0

    .line 1207910
    :cond_1
    invoke-static {}, LX/7Rq;->values()[LX/7Rq;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 1207911
    iget-object v5, v1, LX/7Rq;->value:Ljava/lang/String;

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 1207912
    goto :goto_0

    .line 1207913
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Rq;
    .locals 1

    .prologue
    .line 1207907
    const-class v0, LX/7Rq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Rq;

    return-object v0
.end method

.method public static values()[LX/7Rq;
    .locals 1

    .prologue
    .line 1207906
    sget-object v0, LX/7Rq;->$VALUES:[LX/7Rq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Rq;

    return-object v0
.end method
