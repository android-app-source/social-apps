.class public final enum LX/7HY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7HY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7HY;

.field public static final enum LOCAL:LX/7HY;

.field public static final enum MEMORY_CACHE:LX/7HY;

.field public static final enum NULL_STATE:LX/7HY;

.field public static final enum REMOTE:LX/7HY;

.field public static final enum REMOTE_ENTITY:LX/7HY;

.field public static final enum REMOTE_KEYWORD:LX/7HY;

.field public static final enum UNSET:LX/7HY;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1190824
    new-instance v0, LX/7HY;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v3}, LX/7HY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7HY;->UNSET:LX/7HY;

    .line 1190825
    new-instance v0, LX/7HY;

    const-string v1, "MEMORY_CACHE"

    invoke-direct {v0, v1, v4}, LX/7HY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7HY;->MEMORY_CACHE:LX/7HY;

    .line 1190826
    new-instance v0, LX/7HY;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v5}, LX/7HY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7HY;->LOCAL:LX/7HY;

    .line 1190827
    new-instance v0, LX/7HY;

    const-string v1, "REMOTE"

    invoke-direct {v0, v1, v6}, LX/7HY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7HY;->REMOTE:LX/7HY;

    .line 1190828
    new-instance v0, LX/7HY;

    const-string v1, "REMOTE_ENTITY"

    invoke-direct {v0, v1, v7}, LX/7HY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7HY;->REMOTE_ENTITY:LX/7HY;

    .line 1190829
    new-instance v0, LX/7HY;

    const-string v1, "REMOTE_KEYWORD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7HY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7HY;->REMOTE_KEYWORD:LX/7HY;

    .line 1190830
    new-instance v0, LX/7HY;

    const-string v1, "NULL_STATE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/7HY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7HY;->NULL_STATE:LX/7HY;

    .line 1190831
    const/4 v0, 0x7

    new-array v0, v0, [LX/7HY;

    sget-object v1, LX/7HY;->UNSET:LX/7HY;

    aput-object v1, v0, v3

    sget-object v1, LX/7HY;->MEMORY_CACHE:LX/7HY;

    aput-object v1, v0, v4

    sget-object v1, LX/7HY;->LOCAL:LX/7HY;

    aput-object v1, v0, v5

    sget-object v1, LX/7HY;->REMOTE:LX/7HY;

    aput-object v1, v0, v6

    sget-object v1, LX/7HY;->REMOTE_ENTITY:LX/7HY;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7HY;->REMOTE_KEYWORD:LX/7HY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7HY;->NULL_STATE:LX/7HY;

    aput-object v2, v0, v1

    sput-object v0, LX/7HY;->$VALUES:[LX/7HY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1190834
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7HY;
    .locals 1

    .prologue
    .line 1190835
    const-class v0, LX/7HY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7HY;

    return-object v0
.end method

.method public static values()[LX/7HY;
    .locals 1

    .prologue
    .line 1190833
    sget-object v0, LX/7HY;->$VALUES:[LX/7HY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7HY;

    return-object v0
.end method


# virtual methods
.method public final isRemote()Z
    .locals 1

    .prologue
    .line 1190832
    sget-object v0, LX/7HY;->REMOTE:LX/7HY;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/7HY;->REMOTE_ENTITY:LX/7HY;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/7HY;->REMOTE_KEYWORD:LX/7HY;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
