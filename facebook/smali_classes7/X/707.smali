.class public LX/707;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Lorg/json/JSONObject;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/common/locale/Country;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1161828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161829
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;)LX/707;
    .locals 2

    .prologue
    .line 1161830
    iget-boolean v0, p1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;->a:Z

    .line 1161831
    iput-boolean v0, p0, LX/707;->a:Z

    .line 1161832
    move-object v0, p0

    .line 1161833
    iget-object v1, p1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;->b:Lorg/json/JSONObject;

    .line 1161834
    iput-object v1, v0, LX/707;->b:Lorg/json/JSONObject;

    .line 1161835
    move-object v0, v0

    .line 1161836
    iget-object v1, p1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;->d:Lcom/facebook/common/locale/Country;

    .line 1161837
    iput-object v1, v0, LX/707;->d:Lcom/facebook/common/locale/Country;

    .line 1161838
    move-object v0, v0

    .line 1161839
    return-object v0
.end method

.method public final a(Z)LX/707;
    .locals 0

    .prologue
    .line 1161840
    iput-boolean p1, p0, LX/707;->a:Z

    .line 1161841
    return-object p0
.end method

.method public final e()Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;
    .locals 1

    .prologue
    .line 1161842
    new-instance v0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;

    invoke-direct {v0, p0}, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;-><init>(LX/707;)V

    return-object v0
.end method
