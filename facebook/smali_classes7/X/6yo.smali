.class public LX/6yo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public a:C

.field private b:Z

.field private c:Z

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1160422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160423
    const/16 v0, 0x2d

    iput-char v0, p0, LX/6yo;->a:C

    .line 1160424
    return-void
.end method

.method public static a(LX/0QB;)LX/6yo;
    .locals 1

    .prologue
    .line 1160425
    new-instance v0, LX/6yo;

    invoke-direct {v0}, LX/6yo;-><init>()V

    .line 1160426
    move-object v0, v0

    .line 1160427
    return-object v0
.end method


# virtual methods
.method public final declared-synchronized afterTextChanged(Landroid/text/Editable;)V
    .locals 7

    .prologue
    .line 1160428
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/6yo;->b:Z

    if-nez v0, :cond_2

    .line 1160429
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6yo;->b:Z

    .line 1160430
    iget-boolean v0, p0, LX/6yo;->c:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/6yo;->d:I

    if-lez v0, :cond_0

    .line 1160431
    iget-boolean v0, p0, LX/6yo;->e:Z

    if-eqz v0, :cond_3

    iget v0, p0, LX/6yo;->d:I

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 1160432
    iget v0, p0, LX/6yo;->d:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, LX/6yo;->d:I

    invoke-interface {p1, v0, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 1160433
    :cond_0
    :goto_0
    iget-char v0, p0, LX/6yo;->a:C

    .line 1160434
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/6yU;->a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v1

    const/4 v6, 0x4

    .line 1160435
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v3

    .line 1160436
    sget-object v2, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->AMEX:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    if-eq v1, v2, :cond_4

    const/16 v2, 0x13

    if-le v3, v2, :cond_4

    .line 1160437
    :cond_1
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6yo;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1160438
    :cond_2
    monitor-exit p0

    return-void

    .line 1160439
    :cond_3
    :try_start_1
    iget v0, p0, LX/6yo;->d:I

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1160440
    iget v0, p0, LX/6yo;->d:I

    iget v1, p0, LX/6yo;->d:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {p1, v0, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1160441
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1160442
    :cond_4
    sget-object v2, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->AMEX:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    if-ne v1, v2, :cond_5

    const/16 v2, 0x11

    if-gt v3, v2, :cond_1

    .line 1160443
    :cond_5
    const/4 v2, 0x5

    if-lt v3, v2, :cond_1

    .line 1160444
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/6yU;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1160445
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    .line 1160446
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1160447
    sget-object v4, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->AMEX:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    if-eq v1, v4, :cond_7

    .line 1160448
    if-le v5, v6, :cond_6

    .line 1160449
    invoke-static {v2, v6, v0}, LX/6yU;->a(Ljava/lang/StringBuilder;IC)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1160450
    const/16 v4, 0x8

    if-le v5, v4, :cond_6

    .line 1160451
    const/16 v4, 0x9

    invoke-static {v2, v4, v0}, LX/6yU;->a(Ljava/lang/StringBuilder;IC)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1160452
    const/16 v4, 0xc

    if-le v5, v4, :cond_6

    .line 1160453
    const/16 v4, 0xe

    invoke-static {v2, v4, v0}, LX/6yU;->a(Ljava/lang/StringBuilder;IC)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1160454
    :cond_6
    :goto_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1160455
    const/4 v4, 0x0

    invoke-interface {p1, v4, v3, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_1

    .line 1160456
    :cond_7
    if-le v5, v6, :cond_6

    .line 1160457
    invoke-static {v2, v6, v0}, LX/6yU;->a(Ljava/lang/StringBuilder;IC)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1160458
    const/16 v4, 0xa

    if-le v5, v4, :cond_6

    .line 1160459
    const/16 v4, 0xb

    invoke-static {v2, v4, v0}, LX/6yU;->a(Ljava/lang/StringBuilder;IC)Ljava/lang/StringBuilder;

    move-result-object v2

    goto :goto_2
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1160460
    iget-boolean v0, p0, LX/6yo;->b:Z

    if-nez v0, :cond_0

    .line 1160461
    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    .line 1160462
    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    .line 1160463
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-le v2, v4, :cond_2

    if-ne p3, v4, :cond_2

    if-nez p4, :cond_2

    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    iget-char v3, p0, LX/6yo;->a:C

    if-ne v2, v3, :cond_2

    if-ne v0, v1, :cond_2

    .line 1160464
    iput-boolean v4, p0, LX/6yo;->c:Z

    .line 1160465
    iput p2, p0, LX/6yo;->d:I

    .line 1160466
    add-int/lit8 v1, p2, 0x1

    if-ne v0, v1, :cond_1

    .line 1160467
    iput-boolean v4, p0, LX/6yo;->e:Z

    .line 1160468
    :cond_0
    :goto_0
    return-void

    .line 1160469
    :cond_1
    iput-boolean v5, p0, LX/6yo;->e:Z

    goto :goto_0

    .line 1160470
    :cond_2
    iput-boolean v5, p0, LX/6yo;->c:Z

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1160471
    return-void
.end method
