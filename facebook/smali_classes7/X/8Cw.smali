.class public final LX/8Cw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field public final synthetic a:LX/0nx;

.field private final b:Landroid/support/v4/app/Fragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TF;"
        }
    .end annotation
.end field

.field public final c:I


# direct methods
.method public constructor <init>(LX/0nx;Landroid/support/v4/app/Fragment;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;)V"
        }
    .end annotation

    .prologue
    .line 1312045
    iput-object p1, p0, LX/8Cw;->a:LX/0nx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1312046
    iput-object p2, p0, LX/8Cw;->b:Landroid/support/v4/app/Fragment;

    .line 1312047
    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/8Cw;->c:I

    .line 1312048
    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 1312049
    :try_start_0
    iget-object v0, p0, LX/8Cw;->b:Landroid/support/v4/app/Fragment;

    .line 1312050
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 1312051
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1312052
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/8Cw;->a:LX/0nx;

    iget-object v1, v1, LX/0nx;->c:LX/0gp;

    .line 1312053
    iget-object v2, v1, LX/0gp;->d:LX/0iW;

    move-object v1, v2

    .line 1312054
    iget v2, v1, LX/0iW;->g:I

    move v1, v2

    .line 1312055
    if-gtz v1, :cond_1

    .line 1312056
    :cond_0
    :goto_0
    return v3

    .line 1312057
    :cond_1
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1312058
    iget-object v0, p0, LX/8Cw;->a:LX/0nx;

    iget-object v1, p0, LX/8Cw;->b:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, LX/0nx;->a(Landroid/support/v4/app/Fragment;)V

    .line 1312059
    const/4 v2, 0x1

    .line 1312060
    iget-object v0, p0, LX/8Cw;->a:LX/0nx;

    iget-object v0, v0, LX/0nx;->c:LX/0gp;

    invoke-virtual {v0}, LX/0gp;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1312061
    :cond_2
    iget-object v0, p0, LX/8Cw;->a:LX/0nx;

    iget-object v0, v0, LX/0nx;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    .line 1312062
    if-nez v0, :cond_9
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1312063
    :cond_3
    goto :goto_0

    .line 1312064
    :catch_0
    move-exception v0

    .line 1312065
    sget-object v1, LX/0nx;->d:Ljava/lang/String;

    const-string v2, "onPreDraw"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1312066
    :cond_4
    :try_start_1
    iget-object v0, p0, LX/8Cw;->a:LX/0nx;

    iget-object v0, v0, LX/0nx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1312067
    iget-object v0, p0, LX/8Cw;->a:LX/0nx;

    iget-object v0, v0, LX/0nx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1312068
    iget-object v0, p0, LX/8Cw;->a:LX/0nx;

    iget-object v0, v0, LX/0nx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v0, 0x0

    move v4, v0

    :goto_1
    if-ge v4, v5, :cond_2

    iget-object v0, p0, LX/8Cw;->a:LX/0nx;

    iget-object v0, v0, LX/0nx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Cx;

    .line 1312069
    iget v1, v0, LX/8Cx;->a:I

    and-int/lit8 v1, v1, 0x3

    if-nez v1, :cond_5

    .line 1312070
    invoke-virtual {v0}, LX/8Cx;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget v6, p0, LX/8Cw;->c:I

    if-ge v1, v6, :cond_7

    move v1, v2

    :goto_2
    iput v1, v0, LX/8Cx;->a:I

    .line 1312071
    :cond_5
    iget v1, v0, LX/8Cx;->a:I

    if-ne v1, v2, :cond_8

    .line 1312072
    iget-object v1, p0, LX/8Cw;->a:LX/0nx;

    iget-object v1, v1, LX/0nx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, LX/8Cx;->a()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1312073
    iget-object v1, p0, LX/8Cw;->a:LX/0nx;

    iget-object v1, v1, LX/0nx;->c:LX/0gp;

    .line 1312074
    iget-object v6, v1, LX/0gp;->d:LX/0iW;

    move-object v1, v6

    .line 1312075
    if-eqz v1, :cond_6

    .line 1312076
    invoke-virtual {v0}, LX/8Cx;->a()Landroid/view/View;

    move-result-object v0

    .line 1312077
    iget v6, v1, LX/0iW;->g:I

    move v1, v6

    .line 1312078
    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 1312079
    :cond_6
    :goto_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 1312080
    :cond_7
    const/4 v1, 0x2

    goto :goto_2

    .line 1312081
    :cond_8
    iget-object v1, p0, LX/8Cw;->a:LX/0nx;

    iget-object v1, v1, LX/0nx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, LX/8Cx;->a()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1312082
    iget-object v1, p0, LX/8Cw;->a:LX/0nx;

    iget-object v1, v1, LX/0nx;->c:LX/0gp;

    .line 1312083
    iget-object v6, v1, LX/0gp;->e:LX/0iW;

    move-object v1, v6

    .line 1312084
    if-eqz v1, :cond_6

    .line 1312085
    invoke-virtual {v0}, LX/8Cx;->a()Landroid/view/View;

    move-result-object v0

    .line 1312086
    iget v6, v1, LX/0iW;->g:I

    move v1, v6

    .line 1312087
    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1312088
    :cond_9
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getClipChildren()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1312089
    :cond_a
    :goto_4
    if-eqz v0, :cond_3

    .line 1312090
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 1312091
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1312092
    if-eqz v0, :cond_b

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_b

    check-cast v0, Landroid/view/ViewGroup;

    .line 1312093
    :goto_5
    if-eqz v0, :cond_a

    .line 1312094
    invoke-virtual {v0}, Landroid/view/ViewGroup;->postInvalidate()V

    goto :goto_4

    .line 1312095
    :cond_b
    const/4 v0, 0x0

    goto :goto_5
.end method
