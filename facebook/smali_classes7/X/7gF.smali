.class public LX/7gF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/content/DialogInterface$OnDismissListener;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/7gF;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/2ms;

.field private final d:LX/7gG;

.field private final e:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1224004
    const-class v0, LX/7gF;

    sput-object v0, LX/7gF;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2ms;LX/7gG;LX/03V;)V
    .locals 0
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/2ms;",
            "LX/7gG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1223998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1223999
    iput-object p1, p0, LX/7gF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1224000
    iput-object p2, p0, LX/7gF;->c:LX/2ms;

    .line 1224001
    iput-object p3, p0, LX/7gF;->d:LX/7gG;

    .line 1224002
    iput-object p4, p0, LX/7gF;->e:LX/03V;

    .line 1224003
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x778e80ec

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1224005
    iget-object v0, p0, LX/7gF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1224006
    if-nez v0, :cond_0

    .line 1224007
    iget-object v0, p0, LX/7gF;->e:LX/03V;

    sget-object v2, LX/7gF;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Parent Story is null"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224008
    const v0, 0x47fa766f

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1224009
    :goto_0
    return-void

    .line 1224010
    :cond_0
    iget-object v2, p0, LX/7gF;->c:LX/2ms;

    iget-object v3, p0, LX/7gF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x0

    .line 1224011
    iget-object v5, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1224012
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1224013
    const v6, -0x1e53800c

    invoke-static {v5, v6}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v5

    .line 1224014
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1224015
    invoke-static {v5}, LX/2ms;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v6

    .line 1224016
    invoke-static {v5}, LX/2ms;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v7

    .line 1224017
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1224018
    iget-object v5, v2, LX/2ms;->k:LX/03V;

    const-string v6, "MessengerAdsActionHandler"

    const-string v7, "send message failed: page id is null"

    invoke-virtual {v5, v6, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224019
    :cond_1
    :goto_1
    iget-object v2, p0, LX/7gF;->d:LX/7gG;

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    const-string v4, "cta_message_sent"

    .line 1224020
    iget-object v5, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v5

    .line 1224021
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v0

    .line 1224022
    if-eqz v3, :cond_2

    invoke-virtual {v3}, LX/0lF;->e()I

    move-result v5

    if-nez v5, :cond_5

    .line 1224023
    :cond_2
    const/4 v5, 0x0

    .line 1224024
    :goto_2
    move-object v5, v5

    .line 1224025
    if-nez v5, :cond_4

    .line 1224026
    iget-object v5, v2, LX/7gG;->c:LX/03V;

    sget-object v6, LX/7gG;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Honey Client Event is null as there are no Tracking codes in source: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224027
    :goto_3
    const v0, 0x101caa4d

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1224028
    :cond_3
    sget-object v5, LX/0ax;->aE:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, p1, v5}, LX/2ms;->a(LX/2ms;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;)V

    .line 1224029
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 1224030
    invoke-static {v6}, LX/2ms;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1224031
    iget-object v8, v2, LX/2ms;->i:Lcom/facebook/content/SecureContextHelper;

    iget-object v9, v2, LX/2ms;->j:Landroid/content/Context;

    invoke-interface {v8, v5, v9}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1224032
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1224033
    invoke-static {v6}, LX/2ms;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object v5, v2

    move-object v8, v3

    move-object v10, v4

    invoke-static/range {v5 .. v10}, LX/2ms;->a(LX/2ms;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/7gH;)V

    goto :goto_1

    .line 1224034
    :cond_4
    iget-object v6, v2, LX/7gG;->b:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_3

    .line 1224035
    :cond_5
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "cta_message_send"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "tracking"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "native_newsfeed"

    .line 1224036
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1224037
    move-object v5, v5

    .line 1224038
    if-eqz v4, :cond_6

    .line 1224039
    const-string v6, "1"

    invoke-virtual {v5, v4, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_2

    .line 1224040
    :cond_6
    const-string v6, "cta_click"

    const-string v7, "1"

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_2
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 1223997
    return-void
.end method
