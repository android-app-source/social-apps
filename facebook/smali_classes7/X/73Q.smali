.class public final LX/73Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/payments/ui/MediaGridTextLayoutParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1165804
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/payments/ui/MediaGridTextLayoutParams;
    .locals 1

    .prologue
    .line 1165805
    new-instance v0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    invoke-direct {v0, p0}, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1165806
    invoke-static {p1}, LX/73Q;->a(Landroid/os/Parcel;)Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1165807
    new-array v0, p1, [Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    move-object v0, v0

    .line 1165808
    return-object v0
.end method
