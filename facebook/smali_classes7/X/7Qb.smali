.class public LX/7Qb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/Runnable;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field public final b:LX/7Qe;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2nv;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Zc;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/7Qa;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/7QZ;

.field private final i:LX/16I;

.field public final j:Landroid/os/Handler;

.field public k:Z

.field public l:LX/0Yb;


# direct methods
.method public constructor <init>(LX/0Xl;LX/0Ot;LX/7Qe;LX/16I;LX/0Zc;Landroid/os/Handler;LX/0Ot;LX/0Ot;)V
    .locals 3
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p6    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xl;",
            "LX/0Ot",
            "<",
            "LX/2nv;",
            ">;",
            "LX/7Qe;",
            "LX/16I;",
            "LX/0Zc;",
            "Landroid/os/Handler;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1204651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204652
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7Qb;->g:Ljava/util/Map;

    .line 1204653
    new-instance v0, LX/7QZ;

    invoke-direct {v0, p0}, LX/7QZ;-><init>(LX/7Qb;)V

    iput-object v0, p0, LX/7Qb;->h:LX/7QZ;

    .line 1204654
    iput-object p2, p0, LX/7Qb;->c:LX/0Ot;

    .line 1204655
    iput-object p3, p0, LX/7Qb;->b:LX/7Qe;

    .line 1204656
    iget-object v0, p0, LX/7Qb;->b:LX/7Qe;

    new-instance v1, LX/7QY;

    invoke-direct {v1, p0}, LX/7QY;-><init>(LX/7Qb;)V

    invoke-virtual {v0, v1}, LX/7Qe;->a(LX/7QY;)V

    .line 1204657
    iput-object p4, p0, LX/7Qb;->i:LX/16I;

    .line 1204658
    iput-object p5, p0, LX/7Qb;->d:LX/0Zc;

    .line 1204659
    iput-object p6, p0, LX/7Qb;->j:Landroid/os/Handler;

    .line 1204660
    iput-object p7, p0, LX/7Qb;->e:LX/0Ot;

    .line 1204661
    iput-object p8, p0, LX/7Qb;->f:LX/0Ot;

    .line 1204662
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7Qb;->k:Z

    .line 1204663
    invoke-static {p0}, LX/7Qb;->f(LX/7Qb;)V

    .line 1204664
    new-instance v0, LX/7QW;

    invoke-direct {v0, p0}, LX/7QW;-><init>(LX/7Qb;)V

    .line 1204665
    invoke-interface {p1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 1204666
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z
    .locals 1

    .prologue
    .line 1204650
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_PREVIEW:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_CANCELED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SEAL_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/7Qb;Ljava/lang/String;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1204635
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel;->a()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1204636
    :cond_0
    invoke-static {p0, p1}, LX/7Qb;->b$redex0(LX/7Qb;Ljava/lang/String;)V

    .line 1204637
    :goto_0
    return-void

    .line 1204638
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel;->a()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;

    move-result-object v0

    .line 1204639
    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;->d()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    .line 1204640
    iget-object v1, p0, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Qa;

    .line 1204641
    if-eqz v1, :cond_2

    iget-object v2, v1, LX/7Qa;->f:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v2, v0, :cond_2

    .line 1204642
    iget-object v2, p0, LX/7Qb;->d:LX/0Zc;

    sget-object v3, LX/7IM;->LIVE_SUBSCRIPTION_UPDATE:LX/7IM;

    const-string v4, "Updated broadcast status to %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v2, p1, v3, v4, v5}, LX/0Zc;->a(Ljava/lang/String;LX/7IM;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1204643
    invoke-static {v0}, LX/7Qb;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1204644
    invoke-virtual {v1}, LX/7Qa;->a()V

    .line 1204645
    iget-object v2, p0, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1204646
    :goto_1
    invoke-virtual {v1, v0, p2}, LX/7Qa;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V

    .line 1204647
    :cond_2
    goto :goto_0

    .line 1204648
    :cond_3
    iput-object v0, v1, LX/7Qa;->f:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1204649
    goto :goto_1
.end method

.method public static b$redex0(LX/7Qb;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1204623
    iget-object v0, p0, LX/7Qb;->d:LX/0Zc;

    sget-object v1, LX/7IM;->LIVE_SUBSCRIPTION_UPDATE:LX/7IM;

    const-string v2, "Live video was deleted"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1, v2, v3}, LX/0Zc;->a(Ljava/lang/String;LX/7IM;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1204624
    iget-object v0, p0, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Qa;

    .line 1204625
    if-eqz v0, :cond_0

    .line 1204626
    invoke-virtual {v0}, LX/7Qa;->a()V

    .line 1204627
    invoke-virtual {v0, v4, v4}, LX/7Qa;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V

    .line 1204628
    :cond_0
    return-void
.end method

.method public static f(LX/7Qb;)V
    .locals 3

    .prologue
    .line 1204630
    iget-object v0, p0, LX/7Qb;->l:LX/0Yb;

    if-nez v0, :cond_1

    .line 1204631
    iget-object v0, p0, LX/7Qb;->a:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 1204632
    new-instance v0, Lcom/facebook/video/videohome/liveupdates/BroadcastStatusUpdateManager$3;

    invoke-direct {v0, p0}, Lcom/facebook/video/videohome/liveupdates/BroadcastStatusUpdateManager$3;-><init>(LX/7Qb;)V

    iput-object v0, p0, LX/7Qb;->a:Ljava/lang/Runnable;

    .line 1204633
    :cond_0
    iget-object v0, p0, LX/7Qb;->i:LX/16I;

    sget-object v1, LX/1Ed;->CONNECTED:LX/1Ed;

    iget-object v2, p0, LX/7Qb;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/7Qb;->l:LX/0Yb;

    .line 1204634
    :cond_1
    return-void
.end method

.method public static h(LX/7Qb;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1204629
    iget-boolean v0, p0, LX/7Qb;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/7Qb;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v2, 0x3ba

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
