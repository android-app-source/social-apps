.class public final enum LX/74p;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/74p;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/74p;

.field public static final enum DESELECT:LX/74p;

.field public static final enum PRESELECTED:LX/74p;

.field public static final enum SELECT:LX/74p;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1168564
    new-instance v0, LX/74p;

    const-string v1, "PRESELECTED"

    invoke-direct {v0, v1, v2}, LX/74p;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74p;->PRESELECTED:LX/74p;

    .line 1168565
    new-instance v0, LX/74p;

    const-string v1, "SELECT"

    invoke-direct {v0, v1, v3}, LX/74p;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74p;->SELECT:LX/74p;

    .line 1168566
    new-instance v0, LX/74p;

    const-string v1, "DESELECT"

    invoke-direct {v0, v1, v4}, LX/74p;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74p;->DESELECT:LX/74p;

    .line 1168567
    const/4 v0, 0x3

    new-array v0, v0, [LX/74p;

    sget-object v1, LX/74p;->PRESELECTED:LX/74p;

    aput-object v1, v0, v2

    sget-object v1, LX/74p;->SELECT:LX/74p;

    aput-object v1, v0, v3

    sget-object v1, LX/74p;->DESELECT:LX/74p;

    aput-object v1, v0, v4

    sput-object v0, LX/74p;->$VALUES:[LX/74p;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1168561
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/74p;
    .locals 1

    .prologue
    .line 1168562
    const-class v0, LX/74p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/74p;

    return-object v0
.end method

.method public static values()[LX/74p;
    .locals 1

    .prologue
    .line 1168563
    sget-object v0, LX/74p;->$VALUES:[LX/74p;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/74p;

    return-object v0
.end method
