.class public final LX/76K;
.super LX/76J;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/76J",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final b:LX/0lC;

.field private final c:LX/03V;

.field private final d:LX/FGX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/push/mqtt/service/response/JsonMqttResponseProcessor$Callback",
            "<TT;>;"
        }
    .end annotation
.end field

.field private e:LX/0lF;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Xl;LX/0So;LX/1fU;LX/0lC;LX/03V;LX/FGX;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Xl;",
            "LX/0So;",
            "LX/1fU;",
            "LX/0lC;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/push/mqtt/service/response/JsonMqttResponseProcessor$Callback",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1170951
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, LX/76J;-><init>(Ljava/lang/String;LX/0Xl;LX/0So;LX/1fU;LX/2gV;LX/03V;)V

    .line 1170952
    iput-object p5, p0, LX/76K;->b:LX/0lC;

    .line 1170953
    iput-object p6, p0, LX/76K;->c:LX/03V;

    .line 1170954
    iput-object p7, p0, LX/76K;->d:LX/FGX;

    .line 1170955
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;[B)V
    .locals 3

    .prologue
    .line 1170959
    :try_start_0
    iget-object v0, p0, LX/76K;->b:LX/0lC;

    invoke-virtual {v0, p2}, LX/0lC;->a([B)LX/0lF;

    move-result-object v0

    iput-object v0, p0, LX/76K;->e:LX/0lF;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1170960
    :goto_0
    return-void

    .line 1170961
    :catch_0
    move-exception v0

    .line 1170962
    iget-object v1, p0, LX/76K;->c:LX/03V;

    const-string v2, "json_parse_error"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1170963
    :catch_1
    move-exception v0

    .line 1170964
    iget-object v1, p0, LX/76K;->c:LX/03V;

    const-string v2, "json_parse_error"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1170965
    iget-object v1, p0, LX/76K;->e:LX/0lF;

    .line 1170966
    const-string p0, "fbid"

    invoke-virtual {v1, p0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result p0

    move v0, p0

    .line 1170967
    return v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 1170956
    iget-object v1, p0, LX/76K;->e:LX/0lF;

    .line 1170957
    const-string v2, "fbid"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->c(LX/0lF;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v0, v2

    .line 1170958
    return-object v0
.end method
