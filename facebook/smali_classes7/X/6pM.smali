.class public final enum LX/6pM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6pM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6pM;

.field public static final enum CHANGE_CREATE_NEW:LX/6pM;

.field public static final enum CHANGE_CREATE_NEW_CONFIRMATION:LX/6pM;

.field public static final enum CHANGE_ENTER_OLD:LX/6pM;

.field public static final enum CREATE:LX/6pM;

.field public static final enum CREATE_CONFIRMATION:LX/6pM;

.field public static final enum DELETE:LX/6pM;

.field public static final enum DELETE_WITH_PASSWORD:LX/6pM;

.field public static final enum RESET:LX/6pM;

.field public static final enum UPDATE:LX/6pM;

.field public static final enum VERIFY:LX/6pM;


# instance fields
.field private final mActionBarTitleResId:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private final mAnalyticsEvent:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mFragmentProvider:LX/6pJ;

.field private final mHeaderTextResId:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field public final mShowForgotLink:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1149270
    new-instance v0, LX/6pM;

    const-string v1, "CREATE"

    new-instance v2, LX/6pI;

    invoke-direct {v2}, LX/6pI;-><init>()V

    const v3, 0x7f081dd1

    .line 1149271
    iput v3, v2, LX/6pI;->a:I

    .line 1149272
    move-object v2, v2

    .line 1149273
    const v3, 0x7f081dd2

    .line 1149274
    iput v3, v2, LX/6pI;->b:I

    .line 1149275
    move-object v2, v2

    .line 1149276
    sget-object v3, LX/6pJ;->ENTER_PIN:LX/6pJ;

    .line 1149277
    iput-object v3, v2, LX/6pI;->c:LX/6pJ;

    .line 1149278
    move-object v2, v2

    .line 1149279
    const-string v3, "p2p_set_pin"

    .line 1149280
    iput-object v3, v2, LX/6pI;->d:Ljava/lang/String;

    .line 1149281
    move-object v2, v2

    .line 1149282
    invoke-direct {v0, v1, v5, v2}, LX/6pM;-><init>(Ljava/lang/String;ILX/6pI;)V

    sput-object v0, LX/6pM;->CREATE:LX/6pM;

    .line 1149283
    new-instance v0, LX/6pM;

    const-string v1, "CREATE_CONFIRMATION"

    new-instance v2, LX/6pI;

    invoke-direct {v2}, LX/6pI;-><init>()V

    const v3, 0x7f081dd1

    .line 1149284
    iput v3, v2, LX/6pI;->a:I

    .line 1149285
    move-object v2, v2

    .line 1149286
    const v3, 0x7f081dd3

    .line 1149287
    iput v3, v2, LX/6pI;->b:I

    .line 1149288
    move-object v2, v2

    .line 1149289
    sget-object v3, LX/6pJ;->ENTER_PIN:LX/6pJ;

    .line 1149290
    iput-object v3, v2, LX/6pI;->c:LX/6pJ;

    .line 1149291
    move-object v2, v2

    .line 1149292
    invoke-direct {v0, v1, v6, v2}, LX/6pM;-><init>(Ljava/lang/String;ILX/6pI;)V

    sput-object v0, LX/6pM;->CREATE_CONFIRMATION:LX/6pM;

    .line 1149293
    new-instance v0, LX/6pM;

    const-string v1, "CHANGE_ENTER_OLD"

    new-instance v2, LX/6pI;

    invoke-direct {v2}, LX/6pI;-><init>()V

    const v3, 0x7f081dd5

    .line 1149294
    iput v3, v2, LX/6pI;->a:I

    .line 1149295
    move-object v2, v2

    .line 1149296
    const v3, 0x7f081dd7

    .line 1149297
    iput v3, v2, LX/6pI;->b:I

    .line 1149298
    move-object v2, v2

    .line 1149299
    sget-object v3, LX/6pJ;->ENTER_PIN:LX/6pJ;

    .line 1149300
    iput-object v3, v2, LX/6pI;->c:LX/6pJ;

    .line 1149301
    move-object v2, v2

    .line 1149302
    const-string v3, "p2p_confirm_pin"

    .line 1149303
    iput-object v3, v2, LX/6pI;->d:Ljava/lang/String;

    .line 1149304
    move-object v2, v2

    .line 1149305
    invoke-static {v2}, LX/6pI;->a$redex0(LX/6pI;)LX/6pI;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LX/6pM;-><init>(Ljava/lang/String;ILX/6pI;)V

    sput-object v0, LX/6pM;->CHANGE_ENTER_OLD:LX/6pM;

    .line 1149306
    new-instance v0, LX/6pM;

    const-string v1, "CHANGE_CREATE_NEW"

    new-instance v2, LX/6pI;

    invoke-direct {v2}, LX/6pI;-><init>()V

    const v3, 0x7f081dd6

    .line 1149307
    iput v3, v2, LX/6pI;->a:I

    .line 1149308
    move-object v2, v2

    .line 1149309
    const v3, 0x7f081dd8

    .line 1149310
    iput v3, v2, LX/6pI;->b:I

    .line 1149311
    move-object v2, v2

    .line 1149312
    sget-object v3, LX/6pJ;->ENTER_PIN:LX/6pJ;

    .line 1149313
    iput-object v3, v2, LX/6pI;->c:LX/6pJ;

    .line 1149314
    move-object v2, v2

    .line 1149315
    invoke-direct {v0, v1, v8, v2}, LX/6pM;-><init>(Ljava/lang/String;ILX/6pI;)V

    sput-object v0, LX/6pM;->CHANGE_CREATE_NEW:LX/6pM;

    .line 1149316
    new-instance v0, LX/6pM;

    const-string v1, "CHANGE_CREATE_NEW_CONFIRMATION"

    new-instance v2, LX/6pI;

    invoke-direct {v2}, LX/6pI;-><init>()V

    const v3, 0x7f081dd6

    .line 1149317
    iput v3, v2, LX/6pI;->a:I

    .line 1149318
    move-object v2, v2

    .line 1149319
    const v3, 0x7f081ddc

    .line 1149320
    iput v3, v2, LX/6pI;->b:I

    .line 1149321
    move-object v2, v2

    .line 1149322
    sget-object v3, LX/6pJ;->ENTER_PIN:LX/6pJ;

    .line 1149323
    iput-object v3, v2, LX/6pI;->c:LX/6pJ;

    .line 1149324
    move-object v2, v2

    .line 1149325
    invoke-direct {v0, v1, v9, v2}, LX/6pM;-><init>(Ljava/lang/String;ILX/6pI;)V

    sput-object v0, LX/6pM;->CHANGE_CREATE_NEW_CONFIRMATION:LX/6pM;

    .line 1149326
    new-instance v0, LX/6pM;

    const-string v1, "UPDATE"

    const/4 v2, 0x5

    new-instance v3, LX/6pI;

    invoke-direct {v3}, LX/6pI;-><init>()V

    const v4, 0x7f081dd5

    .line 1149327
    iput v4, v3, LX/6pI;->a:I

    .line 1149328
    move-object v3, v3

    .line 1149329
    const v4, 0x7f081dd7

    .line 1149330
    iput v4, v3, LX/6pI;->b:I

    .line 1149331
    move-object v3, v3

    .line 1149332
    sget-object v4, LX/6pJ;->ENTER_PIN:LX/6pJ;

    .line 1149333
    iput-object v4, v3, LX/6pI;->c:LX/6pJ;

    .line 1149334
    move-object v3, v3

    .line 1149335
    const-string v4, "p2p_pin_status_update"

    .line 1149336
    iput-object v4, v3, LX/6pI;->d:Ljava/lang/String;

    .line 1149337
    move-object v3, v3

    .line 1149338
    invoke-static {v3}, LX/6pI;->a$redex0(LX/6pI;)LX/6pI;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/6pM;-><init>(Ljava/lang/String;ILX/6pI;)V

    sput-object v0, LX/6pM;->UPDATE:LX/6pM;

    .line 1149339
    new-instance v0, LX/6pM;

    const-string v1, "DELETE"

    const/4 v2, 0x6

    new-instance v3, LX/6pI;

    invoke-direct {v3}, LX/6pI;-><init>()V

    const v4, 0x7f081de9

    .line 1149340
    iput v4, v3, LX/6pI;->a:I

    .line 1149341
    move-object v3, v3

    .line 1149342
    const v4, 0x7f081dd7

    .line 1149343
    iput v4, v3, LX/6pI;->b:I

    .line 1149344
    move-object v3, v3

    .line 1149345
    sget-object v4, LX/6pJ;->ENTER_PIN:LX/6pJ;

    .line 1149346
    iput-object v4, v3, LX/6pI;->c:LX/6pJ;

    .line 1149347
    move-object v3, v3

    .line 1149348
    const-string v4, "p2p_initiate_delete_pin"

    .line 1149349
    iput-object v4, v3, LX/6pI;->d:Ljava/lang/String;

    .line 1149350
    move-object v3, v3

    .line 1149351
    invoke-static {v3}, LX/6pI;->a$redex0(LX/6pI;)LX/6pI;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/6pM;-><init>(Ljava/lang/String;ILX/6pI;)V

    sput-object v0, LX/6pM;->DELETE:LX/6pM;

    .line 1149352
    new-instance v0, LX/6pM;

    const-string v1, "DELETE_WITH_PASSWORD"

    const/4 v2, 0x7

    new-instance v3, LX/6pI;

    invoke-direct {v3}, LX/6pI;-><init>()V

    const v4, 0x7f081dea

    .line 1149353
    iput v4, v3, LX/6pI;->a:I

    .line 1149354
    move-object v3, v3

    .line 1149355
    const v4, 0x7f081deb

    .line 1149356
    iput v4, v3, LX/6pI;->b:I

    .line 1149357
    move-object v3, v3

    .line 1149358
    sget-object v4, LX/6pJ;->ENTER_PASSWORD:LX/6pJ;

    .line 1149359
    iput-object v4, v3, LX/6pI;->c:LX/6pJ;

    .line 1149360
    move-object v3, v3

    .line 1149361
    invoke-direct {v0, v1, v2, v3}, LX/6pM;-><init>(Ljava/lang/String;ILX/6pI;)V

    sput-object v0, LX/6pM;->DELETE_WITH_PASSWORD:LX/6pM;

    .line 1149362
    new-instance v0, LX/6pM;

    const-string v1, "VERIFY"

    const/16 v2, 0x8

    new-instance v3, LX/6pI;

    invoke-direct {v3}, LX/6pI;-><init>()V

    const v4, 0x7f081dd5

    .line 1149363
    iput v4, v3, LX/6pI;->a:I

    .line 1149364
    move-object v3, v3

    .line 1149365
    const v4, 0x7f081dd7

    .line 1149366
    iput v4, v3, LX/6pI;->b:I

    .line 1149367
    move-object v3, v3

    .line 1149368
    sget-object v4, LX/6pJ;->ENTER_PIN:LX/6pJ;

    .line 1149369
    iput-object v4, v3, LX/6pI;->c:LX/6pJ;

    .line 1149370
    move-object v3, v3

    .line 1149371
    const-string v4, "p2p_enter_pin"

    .line 1149372
    iput-object v4, v3, LX/6pI;->d:Ljava/lang/String;

    .line 1149373
    move-object v3, v3

    .line 1149374
    invoke-static {v3}, LX/6pI;->a$redex0(LX/6pI;)LX/6pI;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/6pM;-><init>(Ljava/lang/String;ILX/6pI;)V

    sput-object v0, LX/6pM;->VERIFY:LX/6pM;

    .line 1149375
    new-instance v0, LX/6pM;

    const-string v1, "RESET"

    const/16 v2, 0x9

    new-instance v3, LX/6pI;

    invoke-direct {v3}, LX/6pI;-><init>()V

    const v4, 0x7f081ddf

    .line 1149376
    iput v4, v3, LX/6pI;->a:I

    .line 1149377
    move-object v3, v3

    .line 1149378
    const v4, 0x7f081de0

    .line 1149379
    iput v4, v3, LX/6pI;->b:I

    .line 1149380
    move-object v3, v3

    .line 1149381
    sget-object v4, LX/6pJ;->ENTER_PASSWORD:LX/6pJ;

    .line 1149382
    iput-object v4, v3, LX/6pI;->c:LX/6pJ;

    .line 1149383
    move-object v3, v3

    .line 1149384
    const-string v4, "p2p_reset_pin"

    .line 1149385
    iput-object v4, v3, LX/6pI;->d:Ljava/lang/String;

    .line 1149386
    move-object v3, v3

    .line 1149387
    invoke-direct {v0, v1, v2, v3}, LX/6pM;-><init>(Ljava/lang/String;ILX/6pI;)V

    sput-object v0, LX/6pM;->RESET:LX/6pM;

    .line 1149388
    const/16 v0, 0xa

    new-array v0, v0, [LX/6pM;

    sget-object v1, LX/6pM;->CREATE:LX/6pM;

    aput-object v1, v0, v5

    sget-object v1, LX/6pM;->CREATE_CONFIRMATION:LX/6pM;

    aput-object v1, v0, v6

    sget-object v1, LX/6pM;->CHANGE_ENTER_OLD:LX/6pM;

    aput-object v1, v0, v7

    sget-object v1, LX/6pM;->CHANGE_CREATE_NEW:LX/6pM;

    aput-object v1, v0, v8

    sget-object v1, LX/6pM;->CHANGE_CREATE_NEW_CONFIRMATION:LX/6pM;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/6pM;->UPDATE:LX/6pM;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6pM;->DELETE:LX/6pM;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6pM;->DELETE_WITH_PASSWORD:LX/6pM;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6pM;->VERIFY:LX/6pM;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6pM;->RESET:LX/6pM;

    aput-object v2, v0, v1

    sput-object v0, LX/6pM;->$VALUES:[LX/6pM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/6pI;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6pI;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1149389
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1149390
    iget v0, p3, LX/6pI;->a:I

    if-lez v0, :cond_0

    iget v0, p3, LX/6pI;->b:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1149391
    iget v0, p3, LX/6pI;->a:I

    iput v0, p0, LX/6pM;->mActionBarTitleResId:I

    .line 1149392
    iget v0, p3, LX/6pI;->b:I

    iput v0, p0, LX/6pM;->mHeaderTextResId:I

    .line 1149393
    iget-object v0, p3, LX/6pI;->c:LX/6pJ;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6pJ;

    iput-object v0, p0, LX/6pM;->mFragmentProvider:LX/6pJ;

    .line 1149394
    iget-object v0, p3, LX/6pI;->d:Ljava/lang/String;

    iput-object v0, p0, LX/6pM;->mAnalyticsEvent:Ljava/lang/String;

    .line 1149395
    iget-boolean v0, p3, LX/6pI;->e:Z

    iput-boolean v0, p0, LX/6pM;->mShowForgotLink:Z

    .line 1149396
    return-void

    .line 1149397
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6pM;
    .locals 1

    .prologue
    .line 1149398
    const-class v0, LX/6pM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6pM;

    return-object v0
.end method

.method public static values()[LX/6pM;
    .locals 1

    .prologue
    .line 1149399
    sget-object v0, LX/6pM;->$VALUES:[LX/6pM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6pM;

    return-object v0
.end method


# virtual methods
.method public final getActionBarTitleResId()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 1149400
    iget v0, p0, LX/6pM;->mActionBarTitleResId:I

    return v0
.end method

.method public final getAnalyticsEvent()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1149401
    iget-object v0, p0, LX/6pM;->mAnalyticsEvent:Ljava/lang/String;

    return-object v0
.end method

.method public final getFragment(Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;Landroid/content/res/Resources;I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 1149402
    iget-object v0, p0, LX/6pM;->mFragmentProvider:LX/6pJ;

    invoke-virtual {v0, p0, p1, p2, p3}, LX/6pJ;->getFragment(LX/6pM;Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;Landroid/content/res/Resources;I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public final getHeaderTextResId()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 1149403
    iget v0, p0, LX/6pM;->mHeaderTextResId:I

    return v0
.end method

.method public final shouldShowForgotLink()Z
    .locals 1

    .prologue
    .line 1149404
    iget-boolean v0, p0, LX/6pM;->mShowForgotLink:Z

    return v0
.end method
