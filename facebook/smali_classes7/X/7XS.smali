.class public final LX/7XS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1218262
    const/4 v9, 0x0

    .line 1218263
    const/4 v8, 0x0

    .line 1218264
    const/4 v7, 0x0

    .line 1218265
    const/4 v6, 0x0

    .line 1218266
    const/4 v5, 0x0

    .line 1218267
    const/4 v4, 0x0

    .line 1218268
    const/4 v3, 0x0

    .line 1218269
    const/4 v2, 0x0

    .line 1218270
    const/4 v1, 0x0

    .line 1218271
    const/4 v0, 0x0

    .line 1218272
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 1218273
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1218274
    const/4 v0, 0x0

    .line 1218275
    :goto_0
    return v0

    .line 1218276
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1218277
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_a

    .line 1218278
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1218279
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1218280
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1218281
    const-string v11, "campaign_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1218282
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1218283
    :cond_2
    const-string v11, "enabled_ui_features"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1218284
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1218285
    :cond_3
    const-string v11, "fast_hash"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1218286
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1218287
    :cond_4
    const-string v11, "features"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1218288
    invoke-static {p0, p1}, LX/7XR;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1218289
    :cond_5
    const-string v11, "mode"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1218290
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLZeroTokenMode;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLZeroTokenMode;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_1

    .line 1218291
    :cond_6
    const-string v11, "reg_status"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1218292
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 1218293
    :cond_7
    const-string v11, "token_hash"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1218294
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1218295
    :cond_8
    const-string v11, "ttl"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1218296
    const/4 v0, 0x1

    .line 1218297
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    goto/16 :goto_1

    .line 1218298
    :cond_9
    const-string v11, "unregistered_reason"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1218299
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    goto/16 :goto_1

    .line 1218300
    :cond_a
    const/16 v10, 0x9

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1218301
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1218302
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1218303
    const/4 v8, 0x2

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1218304
    const/4 v7, 0x3

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 1218305
    const/4 v6, 0x4

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 1218306
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 1218307
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 1218308
    if-eqz v0, :cond_b

    .line 1218309
    const/4 v0, 0x7

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v2, v3}, LX/186;->a(III)V

    .line 1218310
    :cond_b
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1218311
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 9

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1218312
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1218313
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1218314
    if-eqz v0, :cond_0

    .line 1218315
    const-string v1, "campaign_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218316
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1218317
    :cond_0
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1218318
    if-eqz v0, :cond_1

    .line 1218319
    const-string v0, "enabled_ui_features"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218320
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1218321
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1218322
    if-eqz v0, :cond_2

    .line 1218323
    const-string v1, "fast_hash"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218324
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1218325
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1218326
    if-eqz v0, :cond_8

    .line 1218327
    const-string v1, "features"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218328
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1218329
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_7

    .line 1218330
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    const/4 v8, 0x0

    .line 1218331
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1218332
    invoke-virtual {p0, v3, v8}, LX/15i;->g(II)I

    move-result v7

    .line 1218333
    if-eqz v7, :cond_3

    .line 1218334
    const-string v7, "__type__"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218335
    invoke-static {p0, v3, v8, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1218336
    :cond_3
    const/4 v7, 0x1

    invoke-virtual {p0, v3, v7}, LX/15i;->g(II)I

    move-result v7

    .line 1218337
    if-eqz v7, :cond_4

    .line 1218338
    const-string v8, "graphql"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218339
    invoke-static {p0, v7, p2, p3}, LX/7XT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1218340
    :cond_4
    const/4 v7, 0x2

    invoke-virtual {p0, v3, v7}, LX/15i;->g(II)I

    move-result v7

    .line 1218341
    if-eqz v7, :cond_5

    .line 1218342
    const-string v8, "mqtt"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218343
    invoke-static {p0, v7, p2, p3}, LX/7XU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1218344
    :cond_5
    const/4 v7, 0x3

    invoke-virtual {p0, v3, v7}, LX/15i;->g(II)I

    move-result v7

    .line 1218345
    if-eqz v7, :cond_6

    .line 1218346
    const-string v8, "url_rules"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218347
    invoke-static {p0, v7, p2, p3}, LX/7XU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1218348
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1218349
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1218350
    :cond_7
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1218351
    :cond_8
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1218352
    if-eqz v0, :cond_9

    .line 1218353
    const-string v0, "mode"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218354
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1218355
    :cond_9
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 1218356
    if-eqz v0, :cond_a

    .line 1218357
    const-string v0, "reg_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218358
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1218359
    :cond_a
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1218360
    if-eqz v0, :cond_b

    .line 1218361
    const-string v1, "token_hash"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218362
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1218363
    :cond_b
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1218364
    if-eqz v0, :cond_c

    .line 1218365
    const-string v1, "ttl"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218366
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1218367
    :cond_c
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1218368
    if-eqz v0, :cond_d

    .line 1218369
    const-string v0, "unregistered_reason"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218370
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1218371
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1218372
    return-void
.end method
