.class public LX/6sm;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/6E8;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6r9;

.field public b:Lcom/facebook/payments/checkout/model/CheckoutData;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/6qh;


# direct methods
.method public constructor <init>(LX/6r9;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1153762
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1153763
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1153764
    iput-object v0, p0, LX/6sm;->c:LX/0Px;

    .line 1153765
    iput-object p1, p0, LX/6sm;->a:LX/6r9;

    .line 1153766
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1153756
    iget-object v0, p0, LX/6sm;->a:LX/6r9;

    iget-object v1, p0, LX/6sm;->b:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    .line 1153757
    iget-object v2, v0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1153758
    iget-object v2, v0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Dy;

    iget-object v2, v2, LX/6Dy;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Dp;

    .line 1153759
    :goto_0
    move-object v0, v2

    .line 1153760
    invoke-static {}, LX/6so;->values()[LX/6so;

    move-result-object v1

    aget-object v1, v1, p2

    .line 1153761
    invoke-interface {v0, p1, v1}, LX/6Dp;->a(Landroid/view/ViewGroup;LX/6so;)LX/6E8;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v2, v0, LX/6r9;->a:LX/0P1;

    sget-object p0, LX/6qw;->SIMPLE:LX/6qw;

    invoke-virtual {v2, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Dy;

    iget-object v2, v2, LX/6Dy;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Dp;

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 1153767
    check-cast p1, LX/6E8;

    .line 1153768
    iget-object v0, p0, LX/6sm;->d:LX/6qh;

    invoke-virtual {p1, v0}, LX/6E8;->a(LX/6qh;)V

    .line 1153769
    iget-object v0, p0, LX/6sm;->c:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6E2;

    invoke-virtual {p1, v0}, LX/6E8;->a(LX/6E2;)V

    .line 1153770
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1153755
    iget-object v0, p0, LX/6sm;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6E3;

    invoke-interface {v0}, LX/6E3;->b()LX/6so;

    move-result-object v0

    invoke-virtual {v0}, LX/6so;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1153754
    iget-object v0, p0, LX/6sm;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
