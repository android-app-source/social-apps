.class public final LX/8At;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1308082
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1308083
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1308084
    :goto_0
    return v1

    .line 1308085
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1308086
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1308087
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1308088
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1308089
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1308090
    const-string v4, "menu_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1308091
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1308092
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_e

    .line 1308093
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1308094
    :goto_2
    move v2, v3

    .line 1308095
    goto :goto_1

    .line 1308096
    :cond_2
    const-string v4, "page_link_menus"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1308097
    invoke-static {p0, p1}, LX/8Ar;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1308098
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1308099
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1308100
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1308101
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1308102
    :cond_5
    const-string v12, "has_link_menus"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1308103
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    move v9, v6

    move v6, v4

    .line 1308104
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 1308105
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1308106
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1308107
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_6

    if-eqz v11, :cond_6

    .line 1308108
    const-string v12, "available_menu_info"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1308109
    invoke-static {p0, p1}, LX/8Aq;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_3

    .line 1308110
    :cond_7
    const-string v12, "has_photo_menus"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1308111
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    move v8, v5

    move v5, v4

    goto :goto_3

    .line 1308112
    :cond_8
    const-string v12, "has_structured_menu"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1308113
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v2

    move v7, v2

    move v2, v4

    goto :goto_3

    .line 1308114
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1308115
    :cond_a
    const/4 v11, 0x4

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1308116
    invoke-virtual {p1, v3, v10}, LX/186;->b(II)V

    .line 1308117
    if-eqz v6, :cond_b

    .line 1308118
    invoke-virtual {p1, v4, v9}, LX/186;->a(IZ)V

    .line 1308119
    :cond_b
    if-eqz v5, :cond_c

    .line 1308120
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v8}, LX/186;->a(IZ)V

    .line 1308121
    :cond_c
    if-eqz v2, :cond_d

    .line 1308122
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v7}, LX/186;->a(IZ)V

    .line 1308123
    :cond_d
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_e
    move v2, v3

    move v5, v3

    move v6, v3

    move v7, v3

    move v8, v3

    move v9, v3

    move v10, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1308124
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1308125
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1308126
    if-eqz v0, :cond_0

    .line 1308127
    const-string v1, "menu_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1308128
    invoke-static {p0, v0, p2, p3}, LX/8As;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1308129
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1308130
    if-eqz v0, :cond_1

    .line 1308131
    const-string v1, "page_link_menus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1308132
    invoke-static {p0, v0, p2, p3}, LX/8Ar;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1308133
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1308134
    return-void
.end method
