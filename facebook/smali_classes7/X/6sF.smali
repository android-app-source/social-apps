.class public LX/6sF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/payments/form/model/FormRowDefinition;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/6rs;


# direct methods
.method public constructor <init>(LX/6rs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1153025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153026
    iput-object p1, p0, LX/6sF;->a:LX/6rs;

    .line 1153027
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1153028
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1153029
    iget-object v0, p0, LX/6sF;->a:LX/6rs;

    .line 1153030
    iget-object v2, v0, LX/6rs;->r:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6rr;

    move-object v2, v2

    .line 1153031
    invoke-virtual {p2}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1153032
    invoke-interface {v2, p1, v0}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1153033
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
