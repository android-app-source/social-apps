.class public LX/6lC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;


# instance fields
.field public final durationMs:Ljava/lang/Integer;

.field public final height:Ljava/lang/Integer;

.field public final rotation:Ljava/lang/Integer;

.field public final source:Ljava/lang/Integer;

.field public final thumbnailUri:Ljava/lang/String;

.field public final videoUri:Ljava/lang/String;

.field public final width:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xb

    const/4 v4, 0x1

    const/16 v3, 0x8

    .line 1142420
    new-instance v0, LX/1sv;

    const-string v1, "VideoMetadata"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6lC;->b:LX/1sv;

    .line 1142421
    new-instance v0, LX/1sw;

    const-string v1, "width"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6lC;->c:LX/1sw;

    .line 1142422
    new-instance v0, LX/1sw;

    const-string v1, "height"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6lC;->d:LX/1sw;

    .line 1142423
    new-instance v0, LX/1sw;

    const-string v1, "durationMs"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6lC;->e:LX/1sw;

    .line 1142424
    new-instance v0, LX/1sw;

    const-string v1, "thumbnailUri"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6lC;->f:LX/1sw;

    .line 1142425
    new-instance v0, LX/1sw;

    const-string v1, "videoUri"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6lC;->g:LX/1sw;

    .line 1142426
    new-instance v0, LX/1sw;

    const-string v1, "source"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6lC;->h:LX/1sw;

    .line 1142427
    new-instance v0, LX/1sw;

    const-string v1, "rotation"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6lC;->i:LX/1sw;

    .line 1142428
    sput-boolean v4, LX/6lC;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1142432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142433
    iput-object p1, p0, LX/6lC;->width:Ljava/lang/Integer;

    .line 1142434
    iput-object p2, p0, LX/6lC;->height:Ljava/lang/Integer;

    .line 1142435
    iput-object p3, p0, LX/6lC;->durationMs:Ljava/lang/Integer;

    .line 1142436
    iput-object p4, p0, LX/6lC;->thumbnailUri:Ljava/lang/String;

    .line 1142437
    iput-object p5, p0, LX/6lC;->videoUri:Ljava/lang/String;

    .line 1142438
    iput-object p6, p0, LX/6lC;->source:Ljava/lang/Integer;

    .line 1142439
    iput-object p7, p0, LX/6lC;->rotation:Ljava/lang/Integer;

    .line 1142440
    return-void
.end method

.method private static a(LX/6lC;)V
    .locals 3

    .prologue
    .line 1142429
    iget-object v0, p0, LX/6lC;->source:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, LX/6lE;->a:LX/1sn;

    iget-object v1, p0, LX/6lC;->source:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1142430
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'source\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6lC;->source:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1142431
    :cond_0
    return-void
.end method

.method public static b(LX/1su;)LX/6lC;
    .locals 11

    .prologue
    const/16 v10, 0xb

    const/16 v9, 0x8

    const/4 v7, 0x0

    .line 1142390
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v6, v7

    move-object v5, v7

    move-object v4, v7

    move-object v3, v7

    move-object v2, v7

    move-object v1, v7

    .line 1142391
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1142392
    iget-byte v8, v0, LX/1sw;->b:B

    if-eqz v8, :cond_7

    .line 1142393
    iget-short v8, v0, LX/1sw;->c:S

    packed-switch v8, :pswitch_data_0

    .line 1142394
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1142395
    :pswitch_0
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v9, :cond_0

    .line 1142396
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 1142397
    :cond_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1142398
    :pswitch_1
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v9, :cond_1

    .line 1142399
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    .line 1142400
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1142401
    :pswitch_2
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v9, :cond_2

    .line 1142402
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    .line 1142403
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1142404
    :pswitch_3
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_3

    .line 1142405
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 1142406
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1142407
    :pswitch_4
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_4

    .line 1142408
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 1142409
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1142410
    :pswitch_5
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v9, :cond_5

    .line 1142411
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_0

    .line 1142412
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1142413
    :pswitch_6
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v9, :cond_6

    .line 1142414
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto/16 :goto_0

    .line 1142415
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1142416
    :cond_7
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1142417
    new-instance v0, LX/6lC;

    invoke-direct/range {v0 .. v7}, LX/6lC;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1142418
    invoke-static {v0}, LX/6lC;->a(LX/6lC;)V

    .line 1142419
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1142441
    if-eqz p2, :cond_d

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1142442
    :goto_0
    if-eqz p2, :cond_e

    const-string v0, "\n"

    move-object v3, v0

    .line 1142443
    :goto_1
    if-eqz p2, :cond_f

    const-string v0, " "

    move-object v1, v0

    .line 1142444
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "VideoMetadata"

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1142445
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142446
    const-string v0, "("

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142447
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142448
    const/4 v0, 0x1

    .line 1142449
    iget-object v6, p0, LX/6lC;->width:Ljava/lang/Integer;

    if-eqz v6, :cond_0

    .line 1142450
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142451
    const-string v0, "width"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142452
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142453
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142454
    iget-object v0, p0, LX/6lC;->width:Ljava/lang/Integer;

    if-nez v0, :cond_10

    .line 1142455
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v0, v2

    .line 1142456
    :cond_0
    iget-object v6, p0, LX/6lC;->height:Ljava/lang/Integer;

    if-eqz v6, :cond_2

    .line 1142457
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142458
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142459
    const-string v0, "height"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142460
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142461
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142462
    iget-object v0, p0, LX/6lC;->height:Ljava/lang/Integer;

    if-nez v0, :cond_11

    .line 1142463
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v0, v2

    .line 1142464
    :cond_2
    iget-object v6, p0, LX/6lC;->durationMs:Ljava/lang/Integer;

    if-eqz v6, :cond_4

    .line 1142465
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142466
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142467
    const-string v0, "durationMs"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142468
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142469
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142470
    iget-object v0, p0, LX/6lC;->durationMs:Ljava/lang/Integer;

    if-nez v0, :cond_12

    .line 1142471
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v0, v2

    .line 1142472
    :cond_4
    iget-object v6, p0, LX/6lC;->thumbnailUri:Ljava/lang/String;

    if-eqz v6, :cond_6

    .line 1142473
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142474
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142475
    const-string v0, "thumbnailUri"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142476
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142477
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142478
    iget-object v0, p0, LX/6lC;->thumbnailUri:Ljava/lang/String;

    if-nez v0, :cond_13

    .line 1142479
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    move v0, v2

    .line 1142480
    :cond_6
    iget-object v6, p0, LX/6lC;->videoUri:Ljava/lang/String;

    if-eqz v6, :cond_8

    .line 1142481
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142482
    :cond_7
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142483
    const-string v0, "videoUri"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142484
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142485
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142486
    iget-object v0, p0, LX/6lC;->videoUri:Ljava/lang/String;

    if-nez v0, :cond_14

    .line 1142487
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_7
    move v0, v2

    .line 1142488
    :cond_8
    iget-object v6, p0, LX/6lC;->source:Ljava/lang/Integer;

    if-eqz v6, :cond_18

    .line 1142489
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142490
    :cond_9
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142491
    const-string v0, "source"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142492
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142493
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142494
    iget-object v0, p0, LX/6lC;->source:Ljava/lang/Integer;

    if-nez v0, :cond_15

    .line 1142495
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142496
    :cond_a
    :goto_8
    iget-object v0, p0, LX/6lC;->rotation:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 1142497
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142498
    :cond_b
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142499
    const-string v0, "rotation"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142500
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142501
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142502
    iget-object v0, p0, LX/6lC;->rotation:Ljava/lang/Integer;

    if-nez v0, :cond_17

    .line 1142503
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142504
    :cond_c
    :goto_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142505
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142506
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1142507
    :cond_d
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1142508
    :cond_e
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1142509
    :cond_f
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 1142510
    :cond_10
    iget-object v0, p0, LX/6lC;->width:Ljava/lang/Integer;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1142511
    :cond_11
    iget-object v0, p0, LX/6lC;->height:Ljava/lang/Integer;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1142512
    :cond_12
    iget-object v0, p0, LX/6lC;->durationMs:Ljava/lang/Integer;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1142513
    :cond_13
    iget-object v0, p0, LX/6lC;->thumbnailUri:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1142514
    :cond_14
    iget-object v0, p0, LX/6lC;->videoUri:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1142515
    :cond_15
    sget-object v0, LX/6lE;->b:Ljava/util/Map;

    iget-object v6, p0, LX/6lC;->source:Ljava/lang/Integer;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1142516
    if-eqz v0, :cond_16

    .line 1142517
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142518
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142519
    :cond_16
    iget-object v6, p0, LX/6lC;->source:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1142520
    if-eqz v0, :cond_a

    .line 1142521
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1142522
    :cond_17
    iget-object v0, p0, LX/6lC;->rotation:Ljava/lang/Integer;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    :cond_18
    move v2, v0

    goto/16 :goto_8
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1142357
    invoke-static {p0}, LX/6lC;->a(LX/6lC;)V

    .line 1142358
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1142359
    iget-object v0, p0, LX/6lC;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1142360
    iget-object v0, p0, LX/6lC;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1142361
    sget-object v0, LX/6lC;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142362
    iget-object v0, p0, LX/6lC;->width:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1142363
    :cond_0
    iget-object v0, p0, LX/6lC;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1142364
    iget-object v0, p0, LX/6lC;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1142365
    sget-object v0, LX/6lC;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142366
    iget-object v0, p0, LX/6lC;->height:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1142367
    :cond_1
    iget-object v0, p0, LX/6lC;->durationMs:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1142368
    iget-object v0, p0, LX/6lC;->durationMs:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1142369
    sget-object v0, LX/6lC;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142370
    iget-object v0, p0, LX/6lC;->durationMs:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1142371
    :cond_2
    iget-object v0, p0, LX/6lC;->thumbnailUri:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1142372
    iget-object v0, p0, LX/6lC;->thumbnailUri:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1142373
    sget-object v0, LX/6lC;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142374
    iget-object v0, p0, LX/6lC;->thumbnailUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1142375
    :cond_3
    iget-object v0, p0, LX/6lC;->videoUri:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1142376
    iget-object v0, p0, LX/6lC;->videoUri:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1142377
    sget-object v0, LX/6lC;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142378
    iget-object v0, p0, LX/6lC;->videoUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1142379
    :cond_4
    iget-object v0, p0, LX/6lC;->source:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 1142380
    iget-object v0, p0, LX/6lC;->source:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 1142381
    sget-object v0, LX/6lC;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142382
    iget-object v0, p0, LX/6lC;->source:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1142383
    :cond_5
    iget-object v0, p0, LX/6lC;->rotation:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 1142384
    iget-object v0, p0, LX/6lC;->rotation:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 1142385
    sget-object v0, LX/6lC;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142386
    iget-object v0, p0, LX/6lC;->rotation:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1142387
    :cond_6
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1142388
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1142389
    return-void
.end method

.method public final a(LX/6lC;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1142305
    if-nez p1, :cond_1

    .line 1142306
    :cond_0
    :goto_0
    return v2

    .line 1142307
    :cond_1
    iget-object v0, p0, LX/6lC;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1142308
    :goto_1
    iget-object v3, p1, LX/6lC;->width:Ljava/lang/Integer;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1142309
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1142310
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1142311
    iget-object v0, p0, LX/6lC;->width:Ljava/lang/Integer;

    iget-object v3, p1, LX/6lC;->width:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1142312
    :cond_3
    iget-object v0, p0, LX/6lC;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1142313
    :goto_3
    iget-object v3, p1, LX/6lC;->height:Ljava/lang/Integer;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1142314
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1142315
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1142316
    iget-object v0, p0, LX/6lC;->height:Ljava/lang/Integer;

    iget-object v3, p1, LX/6lC;->height:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1142317
    :cond_5
    iget-object v0, p0, LX/6lC;->durationMs:Ljava/lang/Integer;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1142318
    :goto_5
    iget-object v3, p1, LX/6lC;->durationMs:Ljava/lang/Integer;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1142319
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1142320
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1142321
    iget-object v0, p0, LX/6lC;->durationMs:Ljava/lang/Integer;

    iget-object v3, p1, LX/6lC;->durationMs:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1142322
    :cond_7
    iget-object v0, p0, LX/6lC;->thumbnailUri:Ljava/lang/String;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1142323
    :goto_7
    iget-object v3, p1, LX/6lC;->thumbnailUri:Ljava/lang/String;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1142324
    :goto_8
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1142325
    :cond_8
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1142326
    iget-object v0, p0, LX/6lC;->thumbnailUri:Ljava/lang/String;

    iget-object v3, p1, LX/6lC;->thumbnailUri:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1142327
    :cond_9
    iget-object v0, p0, LX/6lC;->videoUri:Ljava/lang/String;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1142328
    :goto_9
    iget-object v3, p1, LX/6lC;->videoUri:Ljava/lang/String;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1142329
    :goto_a
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1142330
    :cond_a
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1142331
    iget-object v0, p0, LX/6lC;->videoUri:Ljava/lang/String;

    iget-object v3, p1, LX/6lC;->videoUri:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1142332
    :cond_b
    iget-object v0, p0, LX/6lC;->source:Ljava/lang/Integer;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1142333
    :goto_b
    iget-object v3, p1, LX/6lC;->source:Ljava/lang/Integer;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1142334
    :goto_c
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1142335
    :cond_c
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1142336
    iget-object v0, p0, LX/6lC;->source:Ljava/lang/Integer;

    iget-object v3, p1, LX/6lC;->source:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1142337
    :cond_d
    iget-object v0, p0, LX/6lC;->rotation:Ljava/lang/Integer;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1142338
    :goto_d
    iget-object v3, p1, LX/6lC;->rotation:Ljava/lang/Integer;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1142339
    :goto_e
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1142340
    :cond_e
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1142341
    iget-object v0, p0, LX/6lC;->rotation:Ljava/lang/Integer;

    iget-object v3, p1, LX/6lC;->rotation:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_f
    move v2, v1

    .line 1142342
    goto/16 :goto_0

    :cond_10
    move v0, v2

    .line 1142343
    goto/16 :goto_1

    :cond_11
    move v3, v2

    .line 1142344
    goto/16 :goto_2

    :cond_12
    move v0, v2

    .line 1142345
    goto/16 :goto_3

    :cond_13
    move v3, v2

    .line 1142346
    goto/16 :goto_4

    :cond_14
    move v0, v2

    .line 1142347
    goto/16 :goto_5

    :cond_15
    move v3, v2

    .line 1142348
    goto/16 :goto_6

    :cond_16
    move v0, v2

    .line 1142349
    goto/16 :goto_7

    :cond_17
    move v3, v2

    .line 1142350
    goto :goto_8

    :cond_18
    move v0, v2

    .line 1142351
    goto :goto_9

    :cond_19
    move v3, v2

    .line 1142352
    goto :goto_a

    :cond_1a
    move v0, v2

    .line 1142353
    goto :goto_b

    :cond_1b
    move v3, v2

    .line 1142354
    goto :goto_c

    :cond_1c
    move v0, v2

    .line 1142355
    goto :goto_d

    :cond_1d
    move v3, v2

    .line 1142356
    goto :goto_e
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1142297
    if-nez p1, :cond_1

    .line 1142298
    :cond_0
    :goto_0
    return v0

    .line 1142299
    :cond_1
    instance-of v1, p1, LX/6lC;

    if-eqz v1, :cond_0

    .line 1142300
    check-cast p1, LX/6lC;

    invoke-virtual {p0, p1}, LX/6lC;->a(LX/6lC;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1142304
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1142301
    sget-boolean v0, LX/6lC;->a:Z

    .line 1142302
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6lC;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1142303
    return-object v0
.end method
