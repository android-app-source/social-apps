.class public abstract LX/7Jo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Di;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/7Q0;

.field private final c:LX/3FK;

.field private final d:Ljava/lang/String;

.field private final e:LX/36s;

.field public f:Ljava/io/File;

.field private g:Ljava/io/RandomAccessFile;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1194158
    const-class v0, LX/7Jo;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Jo;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/7Q0;LX/3FK;Ljava/lang/String;LX/36s;)V
    .locals 0

    .prologue
    .line 1194152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194153
    iput-object p1, p0, LX/7Jo;->b:LX/7Q0;

    .line 1194154
    iput-object p2, p0, LX/7Jo;->c:LX/3FK;

    .line 1194155
    iput-object p3, p0, LX/7Jo;->d:Ljava/lang/String;

    .line 1194156
    iput-object p4, p0, LX/7Jo;->e:LX/36s;

    .line 1194157
    return-void
.end method


# virtual methods
.method public final a(LX/3Dd;)Ljava/io/OutputStream;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1194135
    :try_start_0
    iget-object v0, p0, LX/7Jo;->b:LX/7Q0;

    iget-object v1, p0, LX/7Jo;->d:Ljava/lang/String;

    .line 1194136
    iget-object v2, v0, LX/7Q0;->a:Ljava/io/File;

    invoke-static {v2}, LX/04M;->a(Ljava/io/File;)V

    .line 1194137
    new-instance v2, Ljava/io/File;

    iget-object v3, v0, LX/7Q0;->a:Ljava/io/File;

    invoke-direct {v2, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1194138
    move-object v0, v2

    .line 1194139
    iput-object v0, p0, LX/7Jo;->f:Ljava/io/File;

    .line 1194140
    iget-object v0, p0, LX/7Jo;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 1194141
    new-instance v0, Ljava/io/RandomAccessFile;

    iget-object v1, p0, LX/7Jo;->f:Ljava/io/File;

    const-string v2, "rw"

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, LX/7Jo;->g:Ljava/io/RandomAccessFile;

    .line 1194142
    iget-object v0, p0, LX/7Jo;->g:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    iget-wide v2, p1, LX/3Dd;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1194143
    iget-object v0, p0, LX/7Jo;->g:Ljava/io/RandomAccessFile;

    iget-wide v2, p1, LX/3Dd;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 1194144
    :cond_0
    iget-object v0, p0, LX/7Jo;->g:Ljava/io/RandomAccessFile;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1194145
    iget-object v0, p0, LX/7Jo;->e:LX/36s;

    iget-object v1, p0, LX/7Jo;->c:LX/3FK;

    iget v1, v1, LX/3FK;->c:I

    int-to-long v2, v1

    iget-object v1, p0, LX/7Jo;->c:LX/3FK;

    iget v1, v1, LX/3FK;->f:I

    invoke-virtual {v0, v2, v3, v4, v1}, LX/36s;->a(JII)I

    move-result v0

    .line 1194146
    iget-object v1, p0, LX/7Jo;->c:LX/3FK;

    iget-boolean v1, v1, LX/3FK;->k:Z

    if-eqz v1, :cond_1

    int-to-long v2, v0

    iget-wide v4, p1, LX/3Dd;->a:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 1194147
    iget-wide v0, p1, LX/3Dd;->a:J

    long-to-int v0, v0

    .line 1194148
    :cond_1
    new-instance v1, LX/7Jn;

    iget-object v2, p0, LX/7Jo;->g:Ljava/io/RandomAccessFile;

    invoke-direct {v1, p0, v2, v0}, LX/7Jn;-><init>(LX/7Jo;Ljava/io/RandomAccessFile;I)V

    move-object v0, v1

    :goto_0
    return-object v0

    .line 1194149
    :catch_0
    move-exception v0

    .line 1194150
    sget-object v1, LX/7Jo;->a:Ljava/lang/String;

    const-string v2, "Error creating direct play file"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194151
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 3

    .prologue
    .line 1194129
    :try_start_0
    invoke-virtual {p0}, LX/7Jo;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1194130
    :goto_0
    return-void

    .line 1194131
    :catch_0
    move-exception v0

    .line 1194132
    sget-object v1, LX/7Jo;->a:Ljava/lang/String;

    const-string v2, "Exception throw"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1194133
    :catch_1
    move-exception v0

    .line 1194134
    sget-object v1, LX/7Jo;->a:Ljava/lang/String;

    const-string v2, "Exception throw"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/io/OutputStream;Ljava/io/IOException;)V
    .locals 4

    .prologue
    .line 1194108
    instance-of v0, p2, LX/7Og;

    if-eqz v0, :cond_0

    .line 1194109
    const/4 p2, 0x0

    .line 1194110
    :cond_0
    if-eqz p2, :cond_1

    .line 1194111
    sget-object v0, LX/7Jo;->a:Ljava/lang/String;

    const-string v1, "Exception is thrown"

    invoke-static {v0, v1, p2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1194112
    :cond_1
    :try_start_0
    check-cast p1, LX/7Jm;

    .line 1194113
    iget-boolean v0, p1, LX/7Jm;->d:Z

    move v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1194114
    if-nez v0, :cond_2

    .line 1194115
    :try_start_1
    invoke-virtual {p0}, LX/7Jo;->b()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1194116
    :cond_2
    :goto_0
    :try_start_2
    iget-object v0, p0, LX/7Jo;->g:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1194117
    :goto_1
    return-void

    .line 1194118
    :catch_0
    move-exception v0

    .line 1194119
    :try_start_3
    sget-object v1, LX/7Jo;->a:Ljava/lang/String;

    const-string v2, "Exception throw"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1194120
    :catchall_0
    move-exception v0

    .line 1194121
    :try_start_4
    iget-object v1, p0, LX/7Jo;->g:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1194122
    :goto_2
    throw v0

    .line 1194123
    :catch_1
    move-exception v0

    .line 1194124
    :try_start_5
    sget-object v1, LX/7Jo;->a:Ljava/lang/String;

    const-string v2, "Exception throw"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 1194125
    :catch_2
    move-exception v0

    .line 1194126
    sget-object v1, LX/7Jo;->a:Ljava/lang/String;

    const-string v2, "Exception is thrown"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1194127
    :catch_3
    move-exception v1

    .line 1194128
    sget-object v2, LX/7Jo;->a:Ljava/lang/String;

    const-string v3, "Exception is thrown"

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a()Z
.end method

.method public abstract b()V
.end method
