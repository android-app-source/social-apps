.class public final LX/7c9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:D

.field private b:D

.field public c:D

.field public d:D


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    iput-wide v0, p0, LX/7c9;->a:D

    const-wide/high16 v0, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    iput-wide v0, p0, LX/7c9;->b:D

    iput-wide v2, p0, LX/7c9;->c:D

    iput-wide v2, p0, LX/7c9;->d:D

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/LatLng;)LX/7c9;
    .locals 13

    iget-wide v0, p0, LX/7c9;->a:D

    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iput-wide v0, p0, LX/7c9;->a:D

    iget-wide v0, p0, LX/7c9;->b:D

    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iput-wide v0, p0, LX/7c9;->b:D

    iget-wide v0, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-wide v2, p0, LX/7c9;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-eqz v2, :cond_2

    iput-wide v0, p0, LX/7c9;->c:D

    :cond_0
    iput-wide v0, p0, LX/7c9;->d:D

    :cond_1
    :goto_0
    return-object p0

    :cond_2
    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-wide v8, p0, LX/7c9;->c:D

    iget-wide v10, p0, LX/7c9;->d:D

    cmpg-double v8, v8, v10

    if-gtz v8, :cond_5

    iget-wide v8, p0, LX/7c9;->c:D

    cmpg-double v8, v8, v0

    if-gtz v8, :cond_4

    iget-wide v8, p0, LX/7c9;->d:D

    cmpg-double v8, v0, v8

    if-gtz v8, :cond_4

    :cond_3
    :goto_1
    move v2, v6

    if-nez v2, :cond_1

    iget-wide v2, p0, LX/7c9;->c:D

    const-wide v8, 0x4076800000000000L    # 360.0

    sub-double v6, v2, v0

    add-double/2addr v6, v8

    rem-double/2addr v6, v8

    move-wide v2, v6

    iget-wide v4, p0, LX/7c9;->d:D

    const-wide v8, 0x4076800000000000L    # 360.0

    sub-double v6, v0, v4

    add-double/2addr v6, v8

    rem-double/2addr v6, v8

    move-wide v4, v6

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    iput-wide v0, p0, LX/7c9;->c:D

    goto :goto_0

    :cond_4
    move v6, v7

    goto :goto_1

    :cond_5
    iget-wide v8, p0, LX/7c9;->c:D

    cmpg-double v8, v8, v0

    if-lez v8, :cond_3

    iget-wide v8, p0, LX/7c9;->d:D

    cmpg-double v8, v0, v8

    if-lez v8, :cond_3

    move v6, v7

    goto :goto_1
.end method

.method public final a()Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 8

    iget-wide v0, p0, LX/7c9;->c:D

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "no included points"

    invoke-static {v0, v1}, LX/1ol;->a(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/maps/model/LatLngBounds;

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p0, LX/7c9;->a:D

    iget-wide v4, p0, LX/7c9;->c:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, p0, LX/7c9;->b:D

    iget-wide v6, p0, LX/7c9;->d:D

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
