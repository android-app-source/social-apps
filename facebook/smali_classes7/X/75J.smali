.class public LX/75J;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/75J;


# instance fields
.field public final a:LX/6Z0;

.field public final b:LX/0So;


# direct methods
.method public constructor <init>(LX/6Z0;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1169202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169203
    iput-object p1, p0, LX/75J;->a:LX/6Z0;

    .line 1169204
    iput-object p2, p0, LX/75J;->b:LX/0So;

    .line 1169205
    return-void
.end method

.method public static a(LX/0QB;)LX/75J;
    .locals 5

    .prologue
    .line 1169206
    sget-object v0, LX/75J;->c:LX/75J;

    if-nez v0, :cond_1

    .line 1169207
    const-class v1, LX/75J;

    monitor-enter v1

    .line 1169208
    :try_start_0
    sget-object v0, LX/75J;->c:LX/75J;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1169209
    if-eqz v2, :cond_0

    .line 1169210
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1169211
    new-instance p0, LX/75J;

    invoke-static {v0}, LX/6Z0;->a(LX/0QB;)LX/6Z0;

    move-result-object v3

    check-cast v3, LX/6Z0;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/75J;-><init>(LX/6Z0;LX/0So;)V

    .line 1169212
    move-object v0, p0

    .line 1169213
    sput-object v0, LX/75J;->c:LX/75J;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1169214
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1169215
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1169216
    :cond_1
    sget-object v0, LX/75J;->c:LX/75J;

    return-object v0

    .line 1169217
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1169218
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
