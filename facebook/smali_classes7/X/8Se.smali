.class public LX/8Se;
.super LX/6LI;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1347210
    const-string v0, "QuicksilverBannerNotification"

    invoke-direct {p0, v0}, LX/6LI;-><init>(Ljava/lang/String;)V

    .line 1347211
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1347212
    iput-object v0, p0, LX/8Se;->a:LX/0Ot;

    .line 1347213
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1347214
    iput-object v0, p0, LX/8Se;->b:LX/0Ot;

    .line 1347215
    const/4 v0, 0x0

    iput v0, p0, LX/8Se;->c:I

    .line 1347216
    const v0, 0x7f0a00d2

    iput v0, p0, LX/8Se;->d:I

    .line 1347217
    return-void
.end method

.method public static b(LX/0QB;)LX/8Se;
    .locals 3

    .prologue
    .line 1347218
    new-instance v0, LX/8Se;

    invoke-direct {v0}, LX/8Se;-><init>()V

    .line 1347219
    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x3c

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    .line 1347220
    iput-object v1, v0, LX/8Se;->a:LX/0Ot;

    iput-object v2, v0, LX/8Se;->b:LX/0Ot;

    .line 1347221
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 1347222
    iget-object v0, p0, LX/8Se;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f03018f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/banner/BasicBannerNotificationView;

    .line 1347223
    new-instance v2, LX/2M6;

    invoke-direct {v2}, LX/2M6;-><init>()V

    iget v1, p0, LX/8Se;->c:I

    if-nez v1, :cond_0

    const-string v1, ""

    .line 1347224
    :goto_0
    iput-object v1, v2, LX/2M6;->a:Ljava/lang/CharSequence;

    .line 1347225
    move-object v2, v2

    .line 1347226
    iget-object v1, p0, LX/8Se;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const v3, 0x7f0a00d5

    invoke-static {v1, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    .line 1347227
    iput v1, v2, LX/2M6;->i:I

    .line 1347228
    move-object v2, v2

    .line 1347229
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, LX/8Se;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget v4, p0, LX/8Se;->d:I

    invoke-static {v1, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v3, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1347230
    iput-object v3, v2, LX/2M6;->c:Landroid/graphics/drawable/Drawable;

    .line 1347231
    move-object v1, v2

    .line 1347232
    invoke-virtual {v1}, LX/2M6;->a()LX/6LR;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setParams(LX/6LR;)V

    .line 1347233
    return-object v0

    .line 1347234
    :cond_0
    iget-object v1, p0, LX/8Se;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v3, p0, LX/8Se;->c:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
