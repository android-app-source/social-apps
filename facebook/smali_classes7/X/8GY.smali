.class public LX/8GY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1319704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1319705
    iput-object p1, p0, LX/8GY;->a:Landroid/content/Context;

    .line 1319706
    iget-object v0, p0, LX/8GY;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0c07

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/8GY;->b:I

    .line 1319707
    return-void
.end method

.method public static a(Landroid/graphics/Rect;II)Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    .line 1319712
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319713
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1319714
    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1319715
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v3

    int-to-float v1, p1

    div-float/2addr v1, v3

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    .line 1319716
    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    int-to-float v2, p2

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    .line 1319717
    new-instance v2, Landroid/graphics/Rect;

    add-int v3, v0, p1

    add-int v4, v1, p2

    invoke-direct {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v2

    :cond_0
    move v0, v2

    .line 1319718
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1319719
    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/8GY;
    .locals 2

    .prologue
    .line 1319720
    new-instance v1, LX/8GY;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/8GY;-><init>(Landroid/content/Context;)V

    .line 1319721
    return-object v1
.end method

.method public static b(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 7

    .prologue
    .line 1319709
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1319710
    div-int/lit8 v0, v0, 0x2

    int-to-double v0, v0

    const-wide v2, 0x3ff6a09aaa3ad18dL    # 1.41421

    mul-double/2addr v0, v2

    .line 1319711
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    double-to-int v4, v0

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    double-to-int v5, v0

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Landroid/graphics/Rect;->centerX()I

    move-result v5

    double-to-int v6, v0

    add-int/2addr v5, v6

    invoke-virtual {p0}, Landroid/graphics/Rect;->centerY()I

    move-result v6

    double-to-int v0, v0

    add-int/2addr v0, v6

    invoke-direct {v2, v3, v4, v5, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v2
.end method

.method public static c(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 1319708
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, -0x5

    iget v2, p0, Landroid/graphics/Rect;->top:I

    iget v3, p0, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, 0x5

    iget v4, p0, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method
