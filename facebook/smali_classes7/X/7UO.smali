.class public LX/7UO;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/7UR;


# direct methods
.method public constructor <init>(LX/7UR;)V
    .locals 0

    .prologue
    .line 1212593
    iput-object p1, p0, LX/7UO;->a:LX/7UR;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    .line 1212584
    iget-object v0, p0, LX/7UO;->a:LX/7UR;

    iget-boolean v0, v0, LX/7UR;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1212585
    iget-object v0, p0, LX/7UO;->a:LX/7UR;

    iget-boolean v0, v0, LX/7UR;->i:Z

    if-eqz v0, :cond_0

    .line 1212586
    iget-object v0, p0, LX/7UO;->a:LX/7UR;

    invoke-virtual {v0}, LX/7UQ;->getScale()F

    move-result v0

    .line 1212587
    iget-object v1, p0, LX/7UO;->a:LX/7UR;

    iget-object v2, p0, LX/7UO;->a:LX/7UR;

    invoke-virtual {v2}, LX/7UQ;->getMaxZoom()F

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/7UR;->a(FF)F

    move-result v0

    .line 1212588
    iget-object v1, p0, LX/7UO;->a:LX/7UR;

    invoke-virtual {v1}, LX/7UQ;->getMaxZoom()F

    move-result v1

    iget-object v2, p0, LX/7UO;->a:LX/7UR;

    invoke-virtual {v2}, LX/7UQ;->getMinZoom()F

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1212589
    iget-object v1, p0, LX/7UO;->a:LX/7UR;

    iput v0, v1, LX/7UR;->d:F

    .line 1212590
    iget-object v1, p0, LX/7UO;->a:LX/7UR;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    const/high16 v4, 0x43c80000    # 400.0f

    invoke-virtual {v1, v0, v2, v3, v4}, LX/7UQ;->a(FFFF)V

    .line 1212591
    iget-object v0, p0, LX/7UO;->a:LX/7UR;

    invoke-virtual {v0}, LX/7UR;->invalidate()V

    .line 1212592
    :cond_0
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8

    .prologue
    .line 1212594
    iget-object v0, p0, LX/7UO;->a:LX/7UR;

    const/high16 v7, 0x44480000    # 800.0f

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1212595
    iget-boolean v3, v0, LX/7UR;->k:Z

    if-nez v3, :cond_1

    .line 1212596
    :cond_0
    :goto_0
    move v0, v1

    .line 1212597
    return v0

    .line 1212598
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-gt v3, v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-gt v3, v2, :cond_0

    .line 1212599
    iget-object v3, v0, LX/7UR;->a:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v3}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1212600
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v3, v4

    .line 1212601
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float/2addr v4, v5

    .line 1212602
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v5, v5, v7

    if-gtz v5, :cond_2

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v5, v5, v7

    if-lez v5, :cond_0

    .line 1212603
    :cond_2
    div-float v1, v3, v6

    div-float v3, v4, v6

    const-wide v5, 0x4072c00000000000L    # 300.0

    invoke-virtual {v0, v1, v3, v5, v6}, LX/7UQ;->a(FFD)V

    .line 1212604
    invoke-virtual {v0}, LX/7UR;->invalidate()V

    move v1, v2

    .line 1212605
    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 1212568
    iget-object v0, p0, LX/7UO;->a:LX/7UR;

    invoke-virtual {v0}, LX/7UR;->isLongClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212569
    iget-object v0, p0, LX/7UO;->a:LX/7UR;

    iget-object v0, v0, LX/7UR;->a:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1212570
    iget-object v0, p0, LX/7UO;->a:LX/7UR;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/7UR;->setPressed(Z)V

    .line 1212571
    iget-object v0, p0, LX/7UO;->a:LX/7UR;

    invoke-virtual {v0}, LX/7UR;->performLongClick()Z

    .line 1212572
    :cond_0
    return-void
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    .prologue
    .line 1212573
    iget-object v0, p0, LX/7UO;->a:LX/7UR;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1212574
    iget-boolean v3, v0, LX/7UR;->k:Z

    if-nez v3, :cond_1

    .line 1212575
    :cond_0
    :goto_0
    move v0, v1

    .line 1212576
    return v0

    .line 1212577
    :cond_1
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 1212578
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-gt v3, v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-gt v3, v2, :cond_0

    .line 1212579
    iget-object v3, v0, LX/7UR;->a:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v3}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1212580
    invoke-virtual {v0}, LX/7UQ;->getScale()F

    move-result v3

    const/high16 p0, 0x3f800000    # 1.0f

    cmpl-float v3, v3, p0

    if-eqz v3, :cond_0

    .line 1212581
    neg-float v1, p3

    neg-float v3, p4

    invoke-virtual {v0, v1, v3}, LX/7UQ;->c(FF)V

    .line 1212582
    invoke-virtual {v0}, LX/7UR;->invalidate()V

    move v1, v2

    .line 1212583
    goto :goto_0
.end method
