.class public LX/7YR;
.super LX/6Ya;
.source ""

# interfaces
.implements LX/12K;


# instance fields
.field public final c:LX/0yW;

.field public final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/0yW;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1219890
    invoke-direct {p0}, LX/6Ya;-><init>()V

    .line 1219891
    iput-object p1, p0, LX/7YR;->c:LX/0yW;

    .line 1219892
    iput-object p2, p0, LX/7YR;->d:LX/0ad;

    .line 1219893
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1219894
    new-instance v0, LX/6YP;

    invoke-direct {v0, p1}, LX/6YP;-><init>(Landroid/content/Context;)V

    .line 1219895
    invoke-virtual {v0}, LX/6YP;->a()V

    .line 1219896
    iget-object v1, p0, LX/7YR;->c:LX/0yW;

    invoke-virtual {v1, p0}, LX/0yW;->a(LX/12K;)V

    .line 1219897
    iget-object v1, p0, LX/7YR;->c:LX/0yW;

    sget-object v2, LX/1lF;->UPSELL_FLOW_STARTING:LX/1lF;

    invoke-virtual {v1, v2}, LX/0yW;->a(LX/1lF;)V

    .line 1219898
    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1219899
    invoke-super {p0}, LX/6Ya;->a()V

    .line 1219900
    iget-object v0, p0, LX/7YR;->c:LX/0yW;

    .line 1219901
    if-eqz p0, :cond_0

    .line 1219902
    iget-object v1, v0, LX/0yW;->r:Ljava/util/Set;

    invoke-interface {v1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1219903
    :cond_0
    return-void
.end method

.method public final a(LX/2XZ;)V
    .locals 0

    .prologue
    .line 1219904
    return-void
.end method

.method public final a(ZLX/1lF;)V
    .locals 2

    .prologue
    .line 1219905
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1219906
    sget-object v0, LX/1lF;->UPSELL_FLOW_STARTING:LX/1lF;

    if-eq p2, v0, :cond_1

    sget-object v0, LX/1lF;->UPSELL_FLOW_FINISHING:LX/1lF;

    if-eq p2, v0, :cond_1

    .line 1219907
    :cond_0
    :goto_0
    return-void

    .line 1219908
    :cond_1
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1219909
    if-eqz v0, :cond_0

    .line 1219910
    new-instance v1, Lcom/facebook/zero/upsell/ui/screencontroller/FbZeroBalanceSpinnerController$1;

    invoke-direct {v1, p0, p1, v0, p2}, Lcom/facebook/zero/upsell/ui/screencontroller/FbZeroBalanceSpinnerController$1;-><init>(LX/7YR;ZLandroid/support/v4/app/FragmentActivity;LX/1lF;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
