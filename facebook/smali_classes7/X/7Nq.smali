.class public final LX/7Nq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7J2;

.field public final synthetic b:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;LX/7J2;)V
    .locals 0

    .prologue
    .line 1200745
    iput-object p1, p0, LX/7Nq;->b:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    iput-object p2, p0, LX/7Nq;->a:LX/7J2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1200732
    iget-object v0, p0, LX/7Nq;->a:LX/7J2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/7J2;->a(Z)V

    .line 1200733
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1200734
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1200735
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1200736
    check-cast v0, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;

    .line 1200737
    if-nez v0, :cond_0

    move v1, v2

    :goto_0
    if-eqz v1, :cond_2

    .line 1200738
    iget-object v0, p0, LX/7Nq;->a:LX/7J2;

    invoke-virtual {v0, v3}, LX/7J2;->a(Z)V

    .line 1200739
    :goto_1
    return-void

    .line 1200740
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_1

    move v1, v2

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_0

    .line 1200741
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1200742
    iget-object v1, p0, LX/7Nq;->b:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v3, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1200743
    iget-object v1, p0, LX/7Nq;->b:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v3, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1200744
    iget-object v0, p0, LX/7Nq;->a:LX/7J2;

    invoke-virtual {v0, v2}, LX/7J2;->a(Z)V

    goto :goto_1
.end method
