.class public LX/87a;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Landroid/content/res/Resources;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1300375
    const-class v0, LX/87Z;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/87a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/res/Resources;)V
    .locals 11

    .prologue
    .line 1300376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300377
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1300378
    iput-object p1, p0, LX/87a;->b:Ljava/lang/String;

    .line 1300379
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, LX/87a;->c:Landroid/content/res/Resources;

    .line 1300380
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p0, LX/87a;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1300381
    const-string v1, "styles"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "styles"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    move-object v1, v0

    .line 1300382
    :goto_1
    if-nez v1, :cond_2

    .line 1300383
    sget-object v0, LX/87a;->a:Ljava/lang/String;

    const-string v1, "json object does not contain \"styles\" key."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1300384
    :goto_2
    return-void

    .line 1300385
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1300386
    :cond_1
    :try_start_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_1

    .line 1300387
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1300388
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_7

    .line 1300389
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/87a;->c:Landroid/content/res/Resources;

    const/4 v5, 0x0

    .line 1300390
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    move-result-object v6
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1300391
    :try_start_2
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1300392
    const-string v8, "name"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1300393
    const-string v8, "name"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1300394
    :cond_3
    const-string v8, "can_default"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1300395
    const-string v8, "can_default"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setCanDefault(Ljava/lang/Boolean;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1300396
    :cond_4
    const-string v8, "text_color"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1300397
    const-string v8, "text_color"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1300398
    invoke-static {v8}, LX/87Z;->a(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_8
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1300399
    :cond_5
    :goto_4
    :try_start_3
    move-object v3, v5

    .line 1300400
    if-eqz v3, :cond_6

    .line 1300401
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1300402
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1300403
    :cond_7
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/87a;->d:LX/0Px;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 1300404
    :catch_0
    move-exception v0

    .line 1300405
    sget-object v1, LX/87a;->a:Ljava/lang/String;

    const-string v2, "string is not a valid json"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1300406
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 1300407
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1300408
    iput-object v0, p0, LX/87a;->d:LX/0Px;

    goto :goto_2

    .line 1300409
    :cond_8
    :try_start_4
    invoke-virtual {v6, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setColor(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1300410
    :cond_9
    const-string v8, "bg_color"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1300411
    const-string v8, "bg_color"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1300412
    invoke-static {v8}, LX/87Z;->a(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1300413
    invoke-virtual {v6, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setBackgroundColor(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1300414
    :cond_a
    const-string v8, "align"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1300415
    const-string v8, "align"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/5RS;->getValue(Ljava/lang/String;)LX/5RS;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setTextAlign(LX/5RS;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1300416
    :cond_b
    const-string v8, "font_weight"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 1300417
    const-string v8, "font_weight"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/5RY;->getValue(Ljava/lang/String;)LX/5RY;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setFontWeight(LX/5RY;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1300418
    :cond_c
    const-string v8, "bg_image_name"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_e

    const-string v8, "bg_image_1x"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 1300419
    const-string v8, "bg_image_name"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setBackgroundImageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1300420
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1300421
    const-string v8, "bg_image_1x"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_11

    .line 1300422
    const/4 v8, 0x0

    .line 1300423
    :cond_d
    :goto_5
    move-object v8, v8

    .line 1300424
    invoke-virtual {v6, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setBackgroundImageUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1300425
    :cond_e
    const-string v8, "gradient_end"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_f

    const-string v8, "gradient_direction"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 1300426
    const-string v8, "gradient_end"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1300427
    invoke-static {v8}, LX/87Z;->a(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1300428
    invoke-virtual {v6, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setGradientEndColor(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1300429
    const-string v8, "gradient_direction"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setGradientDirection(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1300430
    :cond_f
    const-string v8, "preset_id"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 1300431
    const-string v8, "preset_id"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setPresetId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    .line 1300432
    :cond_10
    :try_start_5
    invoke-virtual {v6}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v5

    goto/16 :goto_4

    .line 1300433
    :catch_1
    move-exception v6

    .line 1300434
    sget-object v7, LX/87a;->a:Ljava/lang/String;

    const-string v8, "string is not a valid json"

    invoke-static {v7, v8, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1300435
    invoke-virtual {v6}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_4
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0

    .line 1300436
    :cond_11
    const-string v8, "bg_image_1x"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1300437
    const-string v8, "bg_image_1_5x"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_13

    const-string v8, "bg_image_1_5x"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1300438
    :goto_6
    const-string v10, "bg_image_2x"

    invoke-virtual {v7, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_14

    const-string v10, "bg_image_2x"

    invoke-virtual {v7, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1300439
    :goto_7
    const-string p1, "bg_image_3x"

    invoke-virtual {v7, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_15

    const-string p1, "bg_image_3x"

    invoke-virtual {v7, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1300440
    :goto_8
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    iget p2, p2, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 1300441
    const/16 v3, 0x78

    if-eq p2, v3, :cond_12

    const/16 v3, 0xa0

    if-ne p2, v3, :cond_16

    :cond_12
    move-object v8, v9

    .line 1300442
    goto/16 :goto_5

    :cond_13
    move-object v8, v9

    .line 1300443
    goto :goto_6

    :cond_14
    move-object v10, v8

    .line 1300444
    goto :goto_7

    :cond_15
    move-object p1, v10

    .line 1300445
    goto :goto_8

    .line 1300446
    :cond_16
    const/16 v9, 0xf0

    if-eq p2, v9, :cond_d

    .line 1300447
    const/16 v8, 0x140

    if-ne p2, v8, :cond_17

    move-object v8, v10

    .line 1300448
    goto/16 :goto_5

    :cond_17
    move-object v8, p1

    .line 1300449
    goto/16 :goto_5
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1300450
    iget-object v0, p0, LX/87a;->d:LX/0Px;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
