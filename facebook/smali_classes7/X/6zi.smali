.class public LX/6zi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6w1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6w1",
        "<",
        "Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/03V;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/70D;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1Ck;

.field private e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/70k;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1161380
    const-class v0, LX/6zi;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6zi;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0Or;LX/1Ck;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "LX/70D;",
            ">;",
            "LX/1Ck;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1161375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161376
    iput-object p1, p0, LX/6zi;->b:LX/03V;

    .line 1161377
    iput-object p2, p0, LX/6zi;->c:LX/0Or;

    .line 1161378
    iput-object p3, p0, LX/6zi;->d:LX/1Ck;

    .line 1161379
    return-void
.end method

.method public static b(LX/0QB;)LX/6zi;
    .locals 4

    .prologue
    .line 1161367
    new-instance v2, LX/6zi;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    const/16 v1, 0x2d96

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-direct {v2, v0, v3, v1}, LX/6zi;-><init>(LX/03V;LX/0Or;LX/1Ck;)V

    .line 1161368
    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1161373
    iget-object v0, p0, LX/6zi;->d:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1161374
    return-void
.end method

.method public final a(LX/6zj;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;)V
    .locals 4

    .prologue
    .line 1161381
    iget-object v0, p0, LX/6zi;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6zi;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1161382
    :goto_0
    return-void

    .line 1161383
    :cond_0
    iget-object v0, p0, LX/6zi;->f:LX/70k;

    invoke-virtual {v0}, LX/70k;->a()V

    .line 1161384
    invoke-virtual {p2}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    .line 1161385
    iget-object v1, p2, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    move-object v1, v1

    .line 1161386
    check-cast v1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;

    .line 1161387
    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    invoke-static {v2}, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a(LX/6xg;)LX/70C;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->d:Ljava/lang/String;

    .line 1161388
    iput-object v3, v2, LX/70C;->b:Ljava/lang/String;

    .line 1161389
    move-object v2, v2

    .line 1161390
    iget-object v3, v1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;->b:Lorg/json/JSONObject;

    .line 1161391
    iput-object v3, v2, LX/70C;->d:Lorg/json/JSONObject;

    .line 1161392
    move-object v2, v2

    .line 1161393
    iget-object v3, v1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;->c:Ljava/lang/String;

    .line 1161394
    iput-object v3, v2, LX/70C;->c:Ljava/lang/String;

    .line 1161395
    move-object v2, v2

    .line 1161396
    iget-object v3, v1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;->d:Lcom/facebook/common/locale/Country;

    .line 1161397
    iput-object v3, v2, LX/70C;->e:Lcom/facebook/common/locale/Country;

    .line 1161398
    move-object v2, v2

    .line 1161399
    invoke-virtual {v2}, LX/70C;->a()Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;

    move-result-object v3

    .line 1161400
    iget-boolean v2, v1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;->a:Z

    if-eqz v2, :cond_1

    .line 1161401
    iget-object v2, p0, LX/6zi;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/70D;

    invoke-virtual {v2, v3}, LX/6u3;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1161402
    :goto_1
    move-object v0, v2

    .line 1161403
    iput-object v0, p0, LX/6zi;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1161404
    new-instance v0, LX/6zh;

    invoke-direct {v0, p0, p1, p2}, LX/6zh;-><init>(LX/6zi;LX/6zj;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;)V

    .line 1161405
    iget-object v1, p0, LX/6zi;->d:LX/1Ck;

    const-string v2, "fetch_payment_methods"

    iget-object v3, p0, LX/6zi;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, LX/6zi;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/70D;

    invoke-virtual {v2, v3}, LX/6u3;->c(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    goto :goto_1
.end method

.method public final bridge synthetic a(LX/6zj;Lcom/facebook/payments/picker/model/PickerRunTimeData;)V
    .locals 0

    .prologue
    .line 1161372
    check-cast p2, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;

    invoke-virtual {p0, p1, p2}, LX/6zi;->a(LX/6zj;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;)V

    return-void
.end method

.method public final a(LX/70k;)V
    .locals 0

    .prologue
    .line 1161370
    iput-object p1, p0, LX/6zi;->f:LX/70k;

    .line 1161371
    return-void
.end method

.method public final bridge synthetic b(LX/6zj;Lcom/facebook/payments/picker/model/PickerRunTimeData;)V
    .locals 0

    .prologue
    .line 1161369
    return-void
.end method
