.class public final LX/8Kz;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "LX/0bP;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8L4;


# direct methods
.method public constructor <init>(LX/8L4;)V
    .locals 0

    .prologue
    .line 1331019
    iput-object p1, p0, LX/8Kz;->a:LX/8L4;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/0bP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1331045
    const-class v0, LX/0bP;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 1331020
    check-cast p1, LX/0bP;

    .line 1331021
    iget-object v0, p1, LX/0b5;->b:LX/8KZ;

    move-object v0, v0

    .line 1331022
    sget-object v1, LX/8KZ;->UPLOADING:LX/8KZ;

    if-eq v0, v1, :cond_0

    .line 1331023
    iget-object v0, p1, LX/0b5;->b:LX/8KZ;

    move-object v0, v0

    .line 1331024
    sget-object v1, LX/8KZ;->PUBLISHING:LX/8KZ;

    if-ne v0, v1, :cond_1

    .line 1331025
    :cond_0
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v2, v0

    .line 1331026
    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1331027
    iget-object v0, v2, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v0, v0

    .line 1331028
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1331029
    :goto_0
    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1331030
    iget v1, p1, LX/0bP;->a:I

    move v1, v1

    .line 1331031
    mul-int/lit8 v1, v1, 0x64

    div-int/2addr v1, v0

    int-to-float v1, v1

    .line 1331032
    :goto_1
    iget-object v3, p0, LX/8Kz;->a:LX/8L4;

    iget-object v3, v3, LX/8L4;->f:LX/8Ko;

    iget-object v4, p0, LX/8Kz;->a:LX/8L4;

    iget-object v4, v4, LX/8L4;->b:Landroid/content/Context;

    .line 1331033
    iget v5, p1, LX/0bP;->a:I

    move v5, v5

    .line 1331034
    invoke-virtual {v3, v4, v5, v0}, LX/8Ko;->a(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    .line 1331035
    iget-object v3, p0, LX/8Kz;->a:LX/8L4;

    iget-object v3, v3, LX/8L4;->q:LX/8L0;

    float-to-int v4, v1

    iget-object v5, p0, LX/8Kz;->a:LX/8L4;

    invoke-static {v5, v0, v2}, LX/8L4;->a$redex0(LX/8L4;Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, LX/8Kz;->a:LX/8L4;

    invoke-static {v5, v2}, LX/8L4;->g(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v0, v5}, LX/8L0;->a(LX/0b5;ILjava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v0

    .line 1331036
    iget-object v3, p0, LX/8Kz;->a:LX/8L4;

    invoke-static {v3, v2, v0}, LX/8L4;->a$redex0(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/app/Notification;)V

    .line 1331037
    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1331038
    const/high16 v0, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 1331039
    iget-object v1, p0, LX/8Kz;->a:LX/8L4;

    invoke-static {v1, v2, v0}, LX/8L4;->a$redex0(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;I)V

    .line 1331040
    :cond_1
    return-void

    .line 1331041
    :cond_2
    iget v0, p1, LX/0bP;->b:I

    move v0, v0

    .line 1331042
    goto :goto_0

    .line 1331043
    :cond_3
    iget v1, p1, LX/0b5;->c:F

    move v1, v1

    .line 1331044
    goto :goto_1
.end method
