.class public final LX/76q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:[Z

.field public final synthetic c:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public final synthetic d:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;I[ZLcom/facebook/quickpromotion/model/QuickPromotionDefinition;)V
    .locals 0

    .prologue
    .line 1171279
    iput-object p1, p0, LX/76q;->d:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    iput p2, p0, LX/76q;->a:I

    iput-object p3, p0, LX/76q;->b:[Z

    iput-object p4, p0, LX/76q;->c:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 1171280
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, LX/76q;->a:I

    if-ge v0, v1, :cond_1

    .line 1171281
    iget-object v1, p0, LX/76q;->b:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    .line 1171282
    iget-object v1, p0, LX/76q;->d:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    iget-object v1, v1, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->f:LX/13N;

    iget-object v2, p0, LX/76q;->c:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-static {}, LX/2fy;->values()[LX/2fy;

    move-result-object v3

    aget-object v3, v3, v0

    .line 1171283
    iget-object v4, v1, LX/13N;->a:LX/13O;

    invoke-static {v3}, LX/13N;->a(LX/2fy;)Ljava/lang/String;

    move-result-object p1

    iget-object p2, v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-virtual {v4, p1, p2}, LX/13O;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1171284
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1171285
    :cond_1
    return-void
.end method
