.class public final LX/781;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)V
    .locals 0

    .prologue
    .line 1172342
    iput-object p1, p0, LX/781;->a:Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x12b59ec6

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1172343
    iget-object v1, p0, LX/781;->a:Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    .line 1172344
    iget v2, v1, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->t:I

    iget p1, v1, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->u:I

    add-int/lit8 p1, p1, -0x1

    if-ne v2, p1, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 1172345
    if-eqz v1, :cond_1

    .line 1172346
    iget-object v1, p0, LX/781;->a:Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    invoke-virtual {v1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->l()V

    .line 1172347
    :cond_0
    :goto_1
    const v1, 0xa441e94

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1172348
    :cond_1
    const/16 v1, 0x0

    if-nez v1, :cond_0

    .line 1172349
    iget-object v1, p0, LX/781;->a:Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    .line 1172350
    iget-object v2, v1, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->e:LX/78A;

    .line 1172351
    iget-object p0, v2, LX/78A;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object p0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-static {v2, p0}, LX/78A;->a(LX/78A;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)V

    .line 1172352
    sget-object v2, LX/77y;->PRIMARY:LX/77y;

    iget-object p0, v1, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->e:LX/78A;

    invoke-virtual {p0}, LX/78A;->e()Z

    move-result p0

    invoke-virtual {v1, v2, p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(LX/77y;Z)V

    .line 1172353
    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
