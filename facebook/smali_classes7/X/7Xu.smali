.class public final LX/7Xu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0yJ;


# instance fields
.field public final synthetic a:LX/7Xv;


# direct methods
.method public constructor <init>(LX/7Xv;)V
    .locals 0

    .prologue
    .line 1218817
    iput-object p1, p0, LX/7Xu;->a:LX/7Xv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yi;)V
    .locals 2

    .prologue
    .line 1218821
    iget-object v0, p0, LX/7Xu;->a:LX/7Xv;

    iget-object v0, v0, LX/7Xv;->c:LX/7Xw;

    iget-object v0, v0, LX/7Xw;->b:LX/0yP;

    invoke-virtual {v0, p0}, LX/0yP;->b(LX/0yJ;)V

    .line 1218822
    iget-object v0, p0, LX/7Xu;->a:LX/7Xv;

    iget-object v0, v0, LX/7Xv;->c:LX/7Xw;

    iget-object v0, v0, LX/7Xw;->c:LX/0yI;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yi;)V

    .line 1218823
    iget-object v0, p0, LX/7Xu;->a:LX/7Xv;

    iget-boolean v0, v0, LX/7Xv;->a:Z

    if-eqz v0, :cond_1

    .line 1218824
    iget-object v0, p0, LX/7Xu;->a:LX/7Xv;

    iget-object v0, v0, LX/7Xv;->c:LX/7Xw;

    iget-object v1, p0, LX/7Xu;->a:LX/7Xv;

    iget-object v1, v1, LX/7Xv;->b:Landroid/app/Activity;

    .line 1218825
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 1218826
    const-string p0, "ref"

    const-string p2, "flex_dsm_optin_dialog"

    invoke-virtual {p1, p0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1218827
    const-string p2, "dialtone://switch_to_dialtone"

    .line 1218828
    iget-object p0, v0, LX/7Xw;->d:LX/17Y;

    invoke-interface {p0, v1, p2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    .line 1218829
    if-nez p0, :cond_0

    .line 1218830
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    .line 1218831
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p0, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1218832
    :cond_0
    invoke-virtual {p0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1218833
    iget-object p1, v0, LX/7Xw;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p1, p0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1218834
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/Throwable;LX/0yi;)V
    .locals 1

    .prologue
    .line 1218818
    iget-object v0, p0, LX/7Xu;->a:LX/7Xv;

    iget-object v0, v0, LX/7Xv;->c:LX/7Xw;

    iget-object v0, v0, LX/7Xw;->b:LX/0yP;

    invoke-virtual {v0, p0}, LX/0yP;->b(LX/0yJ;)V

    .line 1218819
    iget-object v0, p0, LX/7Xu;->a:LX/7Xv;

    iget-object v0, v0, LX/7Xv;->c:LX/7Xw;

    iget-object v0, v0, LX/7Xw;->c:LX/0yI;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Ljava/lang/Throwable;LX/0yi;)V

    .line 1218820
    return-void
.end method
