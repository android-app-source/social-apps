.class public LX/7CP;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field private static final j:LX/0Tn;

.field private static final k:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1180512
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "search_awareness/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1180513
    sput-object v0, LX/7CP;->a:LX/0Tn;

    const-string v1, "learning_nux/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7CP;->b:LX/0Tn;

    .line 1180514
    sget-object v0, LX/7CP;->a:LX/0Tn;

    const-string v1, "tutorial_nux/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7CP;->c:LX/0Tn;

    .line 1180515
    sget-object v0, LX/7CP;->a:LX/0Tn;

    const-string v1, "spotlight/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7CP;->d:LX/0Tn;

    .line 1180516
    sget-object v0, LX/7CP;->b:LX/0Tn;

    const-string v1, "next_eligible_fetch_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7CP;->e:LX/0Tn;

    .line 1180517
    sget-object v0, LX/7CP;->b:LX/0Tn;

    const-string v1, "should_force_new_fetch"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7CP;->f:LX/0Tn;

    .line 1180518
    sget-object v0, LX/7CP;->c:LX/0Tn;

    const-string v1, "primary_action_clicked/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7CP;->j:LX/0Tn;

    .line 1180519
    sget-object v0, LX/7CP;->c:LX/0Tn;

    const-string v1, "impression_count/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7CP;->k:LX/0Tn;

    .line 1180520
    sget-object v0, LX/7CP;->c:LX/0Tn;

    const-string v1, "debug_mode_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7CP;->g:LX/0Tn;

    .line 1180521
    sget-object v0, LX/7CP;->d:LX/0Tn;

    const-string v1, "next_eligible_fetch_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7CP;->h:LX/0Tn;

    .line 1180522
    sget-object v0, LX/7CP;->a:LX/0Tn;

    const-string v1, "opt_out/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7CP;->i:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1180523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0Tn;
    .locals 2

    .prologue
    .line 1180511
    sget-object v0, LX/7CP;->k:LX/0Tn;

    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)LX/0Tn;
    .locals 2

    .prologue
    .line 1180510
    sget-object v0, LX/7CP;->j:LX/0Tn;

    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static c(Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 1180509
    sget-object v0, LX/7CP;->i:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method
