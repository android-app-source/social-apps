.class public LX/7l0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/75Q;


# direct methods
.method public constructor <init>(LX/75Q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1233158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1233159
    iput-object p1, p0, LX/7l0;->a:LX/75Q;

    .line 1233160
    return-void
.end method

.method public static a(LX/0QB;)LX/7l0;
    .locals 1

    .prologue
    .line 1233161
    invoke-static {p0}, LX/7l0;->b(LX/0QB;)LX/7l0;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/7l0;
    .locals 2

    .prologue
    .line 1233162
    new-instance v1, LX/7l0;

    invoke-static {p0}, LX/75Q;->a(LX/0QB;)LX/75Q;

    move-result-object v0

    check-cast v0, LX/75Q;

    invoke-direct {v1, v0}, LX/7l0;-><init>(LX/75Q;)V

    .line 1233163
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/util/List;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1233164
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1233165
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1233166
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v3

    sget-object v4, LX/4gF;->PHOTO:LX/4gF;

    if-ne v3, v4, :cond_0

    .line 1233167
    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1233168
    iget-object v3, p0, LX/7l0;->a:LX/75Q;

    invoke-virtual {v3, v0}, LX/75Q;->b(Lcom/facebook/photos/base/media/PhotoItem;)Ljava/util/List;

    move-result-object v0

    .line 1233169
    if-eqz v0, :cond_0

    .line 1233170
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1233171
    iget-wide v8, v0, Lcom/facebook/photos/base/tagging/Tag;->c:J

    move-wide v4, v8

    .line 1233172
    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    .line 1233173
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1233174
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
