.class public final LX/7tE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1267324
    const/4 v9, 0x0

    .line 1267325
    const/4 v8, 0x0

    .line 1267326
    const/4 v7, 0x0

    .line 1267327
    const/4 v6, 0x0

    .line 1267328
    const/4 v5, 0x0

    .line 1267329
    const/4 v4, 0x0

    .line 1267330
    const/4 v3, 0x0

    .line 1267331
    const/4 v2, 0x0

    .line 1267332
    const/4 v1, 0x0

    .line 1267333
    const/4 v0, 0x0

    .line 1267334
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 1267335
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1267336
    const/4 v0, 0x0

    .line 1267337
    :goto_0
    return v0

    .line 1267338
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1267339
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 1267340
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1267341
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1267342
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1267343
    const-string v11, "birthdate"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1267344
    invoke-static {p0, p1}, LX/7tD;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1267345
    :cond_2
    const-string v11, "can_viewer_post"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1267346
    const/4 v1, 0x1

    .line 1267347
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 1267348
    :cond_3
    const-string v11, "first_name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1267349
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1267350
    :cond_4
    const-string v11, "has_viewer_posted_for_birthday"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1267351
    const/4 v0, 0x1

    .line 1267352
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    goto :goto_1

    .line 1267353
    :cond_5
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1267354
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1267355
    :cond_6
    const-string v11, "name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1267356
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1267357
    :cond_7
    const-string v11, "posted_item_privacy_scope"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1267358
    invoke-static {p0, p1}, LX/5R2;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1267359
    :cond_8
    const-string v11, "profile_picture"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1267360
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1267361
    :cond_9
    const/16 v10, 0x8

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1267362
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1267363
    if-eqz v1, :cond_a

    .line 1267364
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v8}, LX/186;->a(IZ)V

    .line 1267365
    :cond_a
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1267366
    if-eqz v0, :cond_b

    .line 1267367
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 1267368
    :cond_b
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1267369
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1267370
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1267371
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1267372
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1267275
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1267276
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1267277
    if-eqz v0, :cond_3

    .line 1267278
    const-string v1, "birthdate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267279
    const/4 v3, 0x0

    .line 1267280
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1267281
    invoke-virtual {p0, v0, v3, v3}, LX/15i;->a(III)I

    move-result v1

    .line 1267282
    if-eqz v1, :cond_0

    .line 1267283
    const-string v2, "day"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267284
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1267285
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, v3}, LX/15i;->a(III)I

    move-result v1

    .line 1267286
    if-eqz v1, :cond_1

    .line 1267287
    const-string v2, "month"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267288
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1267289
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, v3}, LX/15i;->a(III)I

    move-result v1

    .line 1267290
    if-eqz v1, :cond_2

    .line 1267291
    const-string v2, "year"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267292
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1267293
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1267294
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267295
    if-eqz v0, :cond_4

    .line 1267296
    const-string v1, "can_viewer_post"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267297
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267298
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1267299
    if-eqz v0, :cond_5

    .line 1267300
    const-string v1, "first_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267301
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1267302
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267303
    if-eqz v0, :cond_6

    .line 1267304
    const-string v1, "has_viewer_posted_for_birthday"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267305
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267306
    :cond_6
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1267307
    if-eqz v0, :cond_7

    .line 1267308
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267309
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1267310
    :cond_7
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1267311
    if-eqz v0, :cond_8

    .line 1267312
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267313
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1267314
    :cond_8
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1267315
    if-eqz v0, :cond_9

    .line 1267316
    const-string v1, "posted_item_privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267317
    invoke-static {p0, v0, p2, p3}, LX/5R2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1267318
    :cond_9
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1267319
    if-eqz v0, :cond_a

    .line 1267320
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267321
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1267322
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1267323
    return-void
.end method
