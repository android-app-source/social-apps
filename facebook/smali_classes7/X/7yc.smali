.class public LX/7yc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Sh;

.field public final b:Lcom/facebook/performancelogger/PerformanceLogger;

.field public final c:Lcom/facebook/facedetection/detector/MacerFaceDetector;

.field public final d:Landroid/os/Handler;

.field private final e:LX/03V;

.field private final f:LX/18V;

.field public final g:LX/7yd;

.field public final h:LX/1o9;

.field private final i:LX/0TD;

.field public final j:Landroid/os/Looper;

.field public final k:Landroid/os/Handler;

.field public final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/facerec/job/TagSuggestFetchJob;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;LX/0Sh;LX/03V;LX/18V;LX/0TD;Lcom/facebook/facedetection/detector/MacerFaceDetector;Lcom/facebook/performancelogger/PerformanceLogger;LX/7yd;LX/0Zr;LX/0hB;)V
    .locals 3
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1279788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279789
    iput-object p8, p0, LX/7yc;->g:LX/7yd;

    .line 1279790
    iput-object p3, p0, LX/7yc;->e:LX/03V;

    .line 1279791
    iput-object p2, p0, LX/7yc;->a:LX/0Sh;

    .line 1279792
    iput-object p4, p0, LX/7yc;->f:LX/18V;

    .line 1279793
    iput-object p6, p0, LX/7yc;->c:Lcom/facebook/facedetection/detector/MacerFaceDetector;

    .line 1279794
    iput-object p7, p0, LX/7yc;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 1279795
    const-string v0, "FaceRecManager_FD"

    sget-object v1, LX/0TP;->BACKGROUND:LX/0TP;

    invoke-virtual {p9, v0, v1}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 1279796
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 1279797
    const-string v0, "TagSuggestInterrupter"

    sget-object v1, LX/0TP;->BACKGROUND:LX/0TP;

    invoke-virtual {p9, v0, v1}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 1279798
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 1279799
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, LX/7yc;->j:Landroid/os/Looper;

    .line 1279800
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, LX/7yc;->j:Landroid/os/Looper;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/7yc;->k:Landroid/os/Handler;

    .line 1279801
    iput-object p5, p0, LX/7yc;->i:LX/0TD;

    .line 1279802
    iget-object v0, p0, LX/7yc;->i:LX/0TD;

    .line 1279803
    new-instance v1, Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;

    invoke-direct {v1, p8}, Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;-><init>(LX/7yd;)V

    move-object v1, v1

    .line 1279804
    const v2, 0x60e974dd

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1279805
    iput-object p1, p0, LX/7yc;->d:Landroid/os/Handler;

    .line 1279806
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/7yc;->l:Ljava/util/Map;

    .line 1279807
    invoke-virtual {p10}, LX/0hB;->c()I

    move-result v0

    invoke-virtual {p10}, LX/0hB;->d()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1279808
    new-instance v1, LX/1o9;

    invoke-direct {v1, v0, v0}, LX/1o9;-><init>(II)V

    iput-object v1, p0, LX/7yc;->h:LX/1o9;

    .line 1279809
    return-void
.end method

.method public static b(LX/0QB;)LX/7yc;
    .locals 11

    .prologue
    .line 1279784
    new-instance v0, LX/7yc;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v4

    check-cast v4, LX/18V;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    .line 1279785
    const/16 v6, 0x1c2a

    invoke-static {p0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v6}, LX/7yT;->a(LX/0Or;)Lcom/facebook/facedetection/detector/MacerFaceDetector;

    move-result-object v6

    move-object v6, v6

    .line 1279786
    check-cast v6, Lcom/facebook/facedetection/detector/MacerFaceDetector;

    invoke-static {p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v7

    check-cast v7, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {p0}, LX/7yd;->b(LX/0QB;)LX/7yd;

    move-result-object v8

    check-cast v8, LX/7yd;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v9

    check-cast v9, LX/0Zr;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v10

    check-cast v10, LX/0hB;

    invoke-direct/range {v0 .. v10}, LX/7yc;-><init>(Landroid/os/Handler;LX/0Sh;LX/03V;LX/18V;LX/0TD;Lcom/facebook/facedetection/detector/MacerFaceDetector;Lcom/facebook/performancelogger/PerformanceLogger;LX/7yd;LX/0Zr;LX/0hB;)V

    .line 1279787
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;JLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;J",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 1279810
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 1279811
    iget-object v1, p0, LX/7yc;->k:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 1279812
    iget-object v1, p0, LX/7yc;->e:LX/03V;

    const-string v2, "FaceRecManager"

    const-string v3, "InterrupterHandler is not initialized."

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1279813
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const v2, -0x27349818

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1279814
    :goto_0
    return-object v0

    .line 1279815
    :cond_0
    new-instance v1, Lcom/facebook/facerec/job/TagSuggestFetchJob;

    new-instance v2, LX/7ya;

    invoke-direct {v2, p0, v0, p2, p3}, LX/7ya;-><init>(LX/7yc;Lcom/google/common/util/concurrent/SettableFuture;J)V

    iget-object v3, p0, LX/7yc;->a:LX/0Sh;

    iget-object v4, p0, LX/7yc;->e:LX/03V;

    iget-object v5, p0, LX/7yc;->f:LX/18V;

    iget-object v7, p0, LX/7yc;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v8, p0, LX/7yc;->g:LX/7yd;

    move-object v6, p1

    move-object/from16 v9, p4

    move-wide v10, p2

    invoke-direct/range {v1 .. v11}, Lcom/facebook/facerec/job/TagSuggestFetchJob;-><init>(LX/7ya;LX/0Sh;LX/03V;LX/18V;Ljava/util/List;Lcom/facebook/performancelogger/PerformanceLogger;LX/7yd;Ljava/lang/String;J)V

    .line 1279816
    iget-object v2, p0, LX/7yc;->l:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1279817
    iget-object v2, p0, LX/7yc;->k:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1279818
    iget-object v2, p0, LX/7yc;->k:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/facerec/manager/FaceRecManager$1;

    invoke-direct {v3, p0, v1}, Lcom/facebook/facerec/manager/FaceRecManager$1;-><init>(LX/7yc;Lcom/facebook/facerec/job/TagSuggestFetchJob;)V

    const-wide/16 v4, 0x2710

    const v6, 0x4961c8fe    # 924815.9f

    invoke-static {v2, v3, v4, v5, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1279819
    iget-object v2, p0, LX/7yc;->i:LX/0TD;

    const v3, -0x24993a07

    invoke-static {v2, v1, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Bitmap;IZ)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "IZ)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1279765
    iget-object v0, p0, LX/7yc;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1279766
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1279767
    iget-object v0, p0, LX/7yc;->c:Lcom/facebook/facedetection/detector/MacerFaceDetector;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/facedetection/detector/MacerFaceDetector;->a(Landroid/graphics/Bitmap;IZ)Ljava/util/List;

    move-result-object v0

    .line 1279768
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1279769
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yO;

    .line 1279770
    iget-object v3, v0, LX/7yO;->c:Landroid/graphics/RectF;

    move-object v3, v3

    .line 1279771
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v4

    const/4 v5, 0x0

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    .line 1279772
    iget v4, v3, Landroid/graphics/RectF;->top:F

    .line 1279773
    iget v5, v3, Landroid/graphics/RectF;->bottom:F

    iput v5, v3, Landroid/graphics/RectF;->top:F

    .line 1279774
    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 1279775
    :cond_0
    new-instance v4, Lcom/facebook/photos/base/tagging/FaceBox;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v4, v3, v5, v6}, Lcom/facebook/photos/base/tagging/FaceBox;-><init>(Landroid/graphics/RectF;Ljava/util/List;Z)V

    .line 1279776
    iget v3, v0, LX/7yO;->h:I

    move v3, v3

    .line 1279777
    iget v5, v0, LX/7yO;->i:I

    move v5, v5

    .line 1279778
    invoke-virtual {v4, v3, v5}, Lcom/facebook/photos/base/tagging/FaceBox;->a(II)V

    .line 1279779
    iget-object v3, v0, LX/7yO;->g:[B

    move-object v0, v3

    .line 1279780
    iput-object v0, v4, Lcom/facebook/photos/base/tagging/FaceBox;->i:[B

    .line 1279781
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1279782
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 1279783
    return-object v1
.end method

.method public final finalize()V
    .locals 4

    .prologue
    .line 1279757
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 1279758
    iget-object v0, p0, LX/7yc;->l:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 1279759
    :cond_0
    iget-object v0, p0, LX/7yc;->j:Landroid/os/Looper;

    if-eqz v0, :cond_1

    .line 1279760
    iget-object v0, p0, LX/7yc;->j:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 1279761
    :cond_1
    return-void

    .line 1279762
    :cond_2
    iget-object v0, p0, LX/7yc;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facerec/job/TagSuggestFetchJob;

    .line 1279763
    iget-object v2, p0, LX/7yc;->k:Landroid/os/Handler;

    if-eqz v2, :cond_3

    .line 1279764
    iget-object v2, p0, LX/7yc;->k:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/facerec/manager/FaceRecManager$2;

    invoke-direct {v3, p0, v0}, Lcom/facebook/facerec/manager/FaceRecManager$2;-><init>(LX/7yc;Lcom/facebook/facerec/job/TagSuggestFetchJob;)V

    const v0, 0x3e217bdb

    invoke-static {v2, v3, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
