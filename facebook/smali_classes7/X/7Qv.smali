.class public LX/7Qv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile d:LX/7Qv;


# instance fields
.field public final b:LX/0Sh;

.field public final c:LX/0tX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1204858
    const-class v0, LX/7Qv;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Qv;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1204859
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204860
    iput-object p1, p0, LX/7Qv;->b:LX/0Sh;

    .line 1204861
    iput-object p2, p0, LX/7Qv;->c:LX/0tX;

    .line 1204862
    return-void
.end method

.method public static a(LX/0Px;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)LX/0jT;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLStorySeenState;",
            ")",
            "LX/0jT;"
        }
    .end annotation

    .prologue
    .line 1204863
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1204864
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1204865
    new-instance v4, LX/7Qx;

    invoke-direct {v4}, LX/7Qx;-><init>()V

    .line 1204866
    iput-object v0, v4, LX/7Qx;->a:Ljava/lang/String;

    .line 1204867
    move-object v0, v4

    .line 1204868
    iput-object p1, v0, LX/7Qx;->b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1204869
    move-object v0, v0

    .line 1204870
    const/4 v9, 0x1

    const/4 v11, 0x0

    const/4 v7, 0x0

    .line 1204871
    new-instance v5, LX/186;

    const/16 v6, 0x80

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    .line 1204872
    iget-object v6, v0, LX/7Qx;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1204873
    iget-object v8, v0, LX/7Qx;->b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v5, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1204874
    const/4 v10, 0x2

    invoke-virtual {v5, v10}, LX/186;->c(I)V

    .line 1204875
    invoke-virtual {v5, v11, v6}, LX/186;->b(II)V

    .line 1204876
    invoke-virtual {v5, v9, v8}, LX/186;->b(II)V

    .line 1204877
    invoke-virtual {v5}, LX/186;->d()I

    move-result v6

    .line 1204878
    invoke-virtual {v5, v6}, LX/186;->d(I)V

    .line 1204879
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 1204880
    invoke-virtual {v6, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1204881
    new-instance v5, LX/15i;

    move-object v8, v7

    move-object v10, v7

    invoke-direct/range {v5 .. v10}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1204882
    new-instance v6, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;

    invoke-direct {v6, v5}, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;-><init>(LX/15i;)V

    .line 1204883
    move-object v0, v6

    .line 1204884
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1204885
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1204886
    :cond_0
    new-instance v0, LX/7Qw;

    invoke-direct {v0}, LX/7Qw;-><init>()V

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1204887
    iput-object v1, v0, LX/7Qw;->a:LX/0Px;

    .line 1204888
    move-object v0, v0

    .line 1204889
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 1204890
    new-instance v5, LX/186;

    const/16 v6, 0x80

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    .line 1204891
    iget-object v6, v0, LX/7Qw;->a:LX/0Px;

    invoke-static {v5, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 1204892
    invoke-virtual {v5, v9}, LX/186;->c(I)V

    .line 1204893
    invoke-virtual {v5, v8, v6}, LX/186;->b(II)V

    .line 1204894
    invoke-virtual {v5}, LX/186;->d()I

    move-result v6

    .line 1204895
    invoke-virtual {v5, v6}, LX/186;->d(I)V

    .line 1204896
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 1204897
    invoke-virtual {v6, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1204898
    new-instance v5, LX/15i;

    move-object v8, v7

    move-object v10, v7

    invoke-direct/range {v5 .. v10}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1204899
    new-instance v6, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$MarkVideoHomeNotificationSeenMutationModel;

    invoke-direct {v6, v5}, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$MarkVideoHomeNotificationSeenMutationModel;-><init>(LX/15i;)V

    .line 1204900
    move-object v0, v6

    .line 1204901
    return-object v0
.end method

.method public static a(LX/0QB;)LX/7Qv;
    .locals 5

    .prologue
    .line 1204902
    sget-object v0, LX/7Qv;->d:LX/7Qv;

    if-nez v0, :cond_1

    .line 1204903
    const-class v1, LX/7Qv;

    monitor-enter v1

    .line 1204904
    :try_start_0
    sget-object v0, LX/7Qv;->d:LX/7Qv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1204905
    if-eqz v2, :cond_0

    .line 1204906
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1204907
    new-instance p0, LX/7Qv;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-direct {p0, v3, v4}, LX/7Qv;-><init>(LX/0Sh;LX/0tX;)V

    .line 1204908
    move-object v0, p0

    .line 1204909
    sput-object v0, LX/7Qv;->d:LX/7Qv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1204910
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1204911
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1204912
    :cond_1
    sget-object v0, LX/7Qv;->d:LX/7Qv;

    return-object v0

    .line 1204913
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1204914
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoHomeVisitEvents;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1204915
    new-instance v0, LX/7Qu;

    invoke-direct {v0, p0}, LX/7Qu;-><init>(LX/7Qv;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, LX/7Qu;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1204916
    return-void
.end method
