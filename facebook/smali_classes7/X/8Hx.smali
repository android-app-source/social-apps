.class public LX/8Hx;
.super LX/4fO;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final b:Ljava/lang/String;

.field private static d:LX/0TU;


# instance fields
.field private final c:Ljava/lang/String;

.field private final e:LX/03V;

.field public f:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/8Ht;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:[Landroid/graphics/RectF;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public h:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public i:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public k:Lcom/facebook/photos/imageprocessing/FiltersEngine;

.field public l:LX/8Hw;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1321823
    const-class v0, LX/8Hx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/8Hx;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/imageprocessing/FiltersEngine;LX/0UR;LX/03V;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1321810
    invoke-direct {p0}, LX/4fO;-><init>()V

    .line 1321811
    const-string v0, "EDITABLE_PHOTO_POSTPROCESSOR"

    iput-object v0, p0, LX/8Hx;->c:Ljava/lang/String;

    .line 1321812
    iput-object v2, p0, LX/8Hx;->f:LX/1FJ;

    .line 1321813
    iput-object v2, p0, LX/8Hx;->g:[Landroid/graphics/RectF;

    .line 1321814
    iput-boolean v1, p0, LX/8Hx;->h:Z

    .line 1321815
    iput-boolean v1, p0, LX/8Hx;->i:Z

    .line 1321816
    sget-object v0, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v0}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8Hx;->j:Ljava/lang/String;

    .line 1321817
    sget-object v0, LX/8Hw;->NONE:LX/8Hw;

    iput-object v0, p0, LX/8Hx;->l:LX/8Hw;

    .line 1321818
    iput-object p1, p0, LX/8Hx;->k:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    .line 1321819
    iput-object p3, p0, LX/8Hx;->e:LX/03V;

    .line 1321820
    sget-object v0, LX/8Hx;->d:LX/0TU;

    if-nez v0, :cond_0

    .line 1321821
    const-string v0, "autoenhance"

    const/16 v1, 0x8

    const/16 v2, 0x40

    invoke-virtual {p2, v0, v1, v2}, LX/0UR;->a(Ljava/lang/String;II)LX/0TU;

    move-result-object v0

    sput-object v0, LX/8Hx;->d:LX/0TU;

    .line 1321822
    :cond_0
    return-void
.end method

.method private declared-synchronized e()Z
    .locals 1

    .prologue
    .line 1321809
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/8Hx;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    .line 1321824
    monitor-enter p0

    .line 1321825
    :try_start_0
    sget-object v0, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v0}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/8Hx;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1321826
    monitor-exit p0

    .line 1321827
    :goto_0
    return-void

    .line 1321828
    :cond_0
    iget-object v0, p0, LX/8Hx;->l:LX/8Hw;

    sget-object v1, LX/8Hw;->NONE:LX/8Hw;

    if-ne v0, v1, :cond_1

    .line 1321829
    sget-object v0, LX/8Hw;->PREPROCESSING:LX/8Hw;

    iput-object v0, p0, LX/8Hx;->l:LX/8Hw;

    .line 1321830
    invoke-virtual {p0}, LX/4fO;->c()V

    .line 1321831
    monitor-exit p0

    goto :goto_0

    .line 1321832
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1321833
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/8Hx;->l:LX/8Hw;

    sget-object v1, LX/8Hw;->CLOSING:LX/8Hw;

    if-ne v0, v1, :cond_2

    .line 1321834
    invoke-virtual {p0}, LX/8Hx;->d()V

    .line 1321835
    monitor-exit p0

    goto :goto_0

    .line 1321836
    :cond_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1321837
    monitor-enter p0

    .line 1321838
    :try_start_2
    iget-object v0, p0, LX/8Hx;->f:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v6

    .line 1321839
    iget-object v4, p0, LX/8Hx;->g:[Landroid/graphics/RectF;

    .line 1321840
    iget-object v7, p0, LX/8Hx;->l:LX/8Hw;

    .line 1321841
    iget-object v5, p0, LX/8Hx;->j:Ljava/lang/String;

    .line 1321842
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1321843
    if-nez v6, :cond_5

    if-eqz v4, :cond_5

    .line 1321844
    sget-object v0, LX/8Hw;->PREPROCESSING:LX/8Hw;

    if-eq v7, v0, :cond_3

    sget-object v0, LX/8Hw;->APPLYING:LX/8Hw;

    if-ne v7, v0, :cond_4

    .line 1321845
    :cond_3
    new-instance v2, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v2, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 1321846
    :try_start_3
    sget-object v8, LX/8Hx;->d:LX/0TU;

    new-instance v0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;-><init>(LX/8Hx;Ljava/util/concurrent/CountDownLatch;Landroid/graphics/Bitmap;[Landroid/graphics/RectF;Ljava/lang/String;)V

    const v1, 0x756259d2

    invoke-static {v8, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1321847
    const-wide/16 v0, 0x3a98

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1321848
    :cond_4
    :goto_1
    invoke-static {v6}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 1321849
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 1321850
    :catch_0
    move-exception v0

    .line 1321851
    sget-object v1, LX/8Hx;->b:Ljava/lang/String;

    const-string v2, "sessionLatch has been interrupted."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1321852
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected failure: Autoenhance repeatedpostprocessor latch interrupted. \nCurrent state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, LX/8Hw;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nExecutor queue size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/8Hx;->d:LX/0TU;

    invoke-interface {v2}, LX/0TU;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nFiltered applied: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, LX/8Hx;->e()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nFilter name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 1321853
    iget-object v1, p0, LX/8Hx;->e:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_1

    .line 1321854
    :cond_5
    if-eqz v6, :cond_4

    sget-object v0, LX/8Hw;->CLOSING:LX/8Hw;

    if-eq v7, v0, :cond_4

    .line 1321855
    invoke-virtual {v6}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Ht;

    invoke-virtual {v0, p1, v5}, LX/8Ht;->a(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    .line 1321856
    monitor-enter p0

    .line 1321857
    const/4 v0, 0x1

    :try_start_5
    iput-boolean v0, p0, LX/8Hx;->h:Z

    .line 1321858
    iget-boolean v0, p0, LX/8Hx;->i:Z

    if-nez v0, :cond_6

    .line 1321859
    invoke-virtual {p0}, LX/8Hx;->d()V

    .line 1321860
    :cond_6
    monitor-exit p0

    goto :goto_1

    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1321800
    monitor-enter p0

    .line 1321801
    :try_start_0
    iput-object p1, p0, LX/8Hx;->j:Ljava/lang/String;

    .line 1321802
    iget-object v0, p0, LX/8Hx;->f:LX/1FJ;

    if-nez v0, :cond_0

    .line 1321803
    sget-object v0, LX/8Hw;->PREPROCESSING:LX/8Hw;

    iput-object v0, p0, LX/8Hx;->l:LX/8Hw;

    .line 1321804
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1321805
    invoke-virtual {p0}, LX/4fO;->c()V

    .line 1321806
    return-void

    .line 1321807
    :cond_0
    :try_start_1
    sget-object v0, LX/8Hw;->APPLYING:LX/8Hw;

    iput-object v0, p0, LX/8Hx;->l:LX/8Hw;

    goto :goto_0

    .line 1321808
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a([Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 1321794
    monitor-enter p0

    .line 1321795
    :try_start_0
    iput-object p1, p0, LX/8Hx;->g:[Landroid/graphics/RectF;

    .line 1321796
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1321797
    invoke-virtual {p0}, LX/4fO;->c()V

    .line 1321798
    return-void

    .line 1321799
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1321793
    const-string v0, "EDITABLE_PHOTO_POSTPROCESSOR"

    return-object v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1321784
    monitor-enter p0

    .line 1321785
    :try_start_0
    iget-object v0, p0, LX/8Hx;->f:LX/1FJ;

    .line 1321786
    const/4 v1, 0x0

    iput-object v1, p0, LX/8Hx;->f:LX/1FJ;

    .line 1321787
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/8Hx;->i:Z

    .line 1321788
    sget-object v1, LX/8Hw;->NONE:LX/8Hw;

    iput-object v1, p0, LX/8Hx;->l:LX/8Hw;

    .line 1321789
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1321790
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1321791
    return-void

    .line 1321792
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
