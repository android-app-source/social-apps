.class public final LX/6gs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 1126491
    const-wide/16 v8, 0x0

    .line 1126492
    const-wide/16 v6, 0x0

    .line 1126493
    const-wide/16 v4, 0x0

    .line 1126494
    const/4 v2, 0x0

    .line 1126495
    const/4 v1, 0x0

    .line 1126496
    const/4 v0, 0x0

    .line 1126497
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v10, :cond_8

    .line 1126498
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1126499
    const/4 v0, 0x0

    .line 1126500
    :goto_0
    return v0

    .line 1126501
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_4

    .line 1126502
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 1126503
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1126504
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 1126505
    const-string v4, "x"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1126506
    const/4 v0, 0x1

    .line 1126507
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 1126508
    :cond_1
    const-string v4, "y"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1126509
    const/4 v0, 0x1

    .line 1126510
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v7, v0

    move-wide v10, v4

    goto :goto_1

    .line 1126511
    :cond_2
    const-string v4, "z"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1126512
    const/4 v0, 0x1

    .line 1126513
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v6, v0

    move-wide v8, v4

    goto :goto_1

    .line 1126514
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1126515
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1126516
    if-eqz v1, :cond_5

    .line 1126517
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1126518
    :cond_5
    if-eqz v7, :cond_6

    .line 1126519
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v10

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1126520
    :cond_6
    if-eqz v6, :cond_7

    .line 1126521
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1126522
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_8
    move-wide v10, v6

    move v7, v1

    move v6, v0

    move v1, v2

    move-wide v2, v8

    move-wide v8, v4

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1126476
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1126477
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126478
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 1126479
    const-string v2, "x"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126480
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126481
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126482
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 1126483
    const-string v2, "y"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126484
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126485
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126486
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 1126487
    const-string v2, "z"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126488
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126489
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1126490
    return-void
.end method
