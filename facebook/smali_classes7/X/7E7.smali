.class public final LX/7E7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

.field private b:F

.field private c:F

.field private d:F


# direct methods
.method public constructor <init>(Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;FF)V
    .locals 1

    .prologue
    .line 1184286
    iput-object p1, p0, LX/7E7;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184287
    iput p2, p0, LX/7E7;->b:F

    .line 1184288
    iput p3, p0, LX/7E7;->c:F

    .line 1184289
    invoke-static {p2, p3}, LX/7Cq;->a(FF)F

    move-result v0

    iput v0, p0, LX/7E7;->d:F

    .line 1184290
    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 1184291
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v0

    .line 1184292
    iget v1, p0, LX/7E7;->d:F

    mul-float/2addr v1, v0

    iget v2, p0, LX/7E7;->b:F

    add-float/2addr v1, v2

    .line 1184293
    iget-object v2, p0, LX/7E7;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iget-object v2, v2, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->d:Lcom/facebook/spherical/ui/HeadingFovView;

    invoke-virtual {v2, v1}, Lcom/facebook/spherical/ui/HeadingFovView;->setCompassYaw(F)V

    .line 1184294
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    .line 1184295
    iget-object v1, p0, LX/7E7;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iget-object v1, v1, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->e:Lcom/facebook/spherical/ui/HeadingPoiView;

    iget v2, p0, LX/7E7;->c:F

    invoke-virtual {v1, v2}, Lcom/facebook/spherical/ui/HeadingPoiView;->setRotation(F)V

    .line 1184296
    iget-object v1, p0, LX/7E7;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iget-object v1, v1, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->e:Lcom/facebook/spherical/ui/HeadingPoiView;

    invoke-virtual {v1, v6}, Lcom/facebook/spherical/ui/HeadingPoiView;->setVisibility(I)V

    .line 1184297
    :cond_0
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v2

    long-to-float v1, v2

    mul-float/2addr v1, v0

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v2

    const-wide/16 v4, 0x578

    sub-long/2addr v2, v4

    long-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 1184298
    iget-object v1, p0, LX/7E7;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-virtual {v1}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a()V

    .line 1184299
    :cond_1
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v2

    long-to-float v1, v2

    mul-float/2addr v1, v0

    const/high16 v2, 0x43480000    # 200.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 1184300
    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_3

    .line 1184301
    iget-object v1, p0, LX/7E7;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iget-object v1, v1, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->e:Lcom/facebook/spherical/ui/HeadingPoiView;

    invoke-virtual {v1, v6}, Lcom/facebook/spherical/ui/HeadingPoiView;->setVisibility(I)V

    .line 1184302
    :goto_0
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 1184303
    iget-object v0, p0, LX/7E7;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iget-object v0, v0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->e:Lcom/facebook/spherical/ui/HeadingPoiView;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/HeadingPoiView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 1184304
    iget-object v0, p0, LX/7E7;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iget-object v0, v0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->e:Lcom/facebook/spherical/ui/HeadingPoiView;

    invoke-virtual {v0, v7}, Lcom/facebook/spherical/ui/HeadingPoiView;->setVisibility(I)V

    .line 1184305
    :cond_2
    return-void

    .line 1184306
    :cond_3
    iget-object v1, p0, LX/7E7;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iget-object v1, v1, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->e:Lcom/facebook/spherical/ui/HeadingPoiView;

    invoke-virtual {v1, v7}, Lcom/facebook/spherical/ui/HeadingPoiView;->setVisibility(I)V

    goto :goto_0
.end method
