.class public LX/721;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6w3;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6w3",
        "<",
        "Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/71t;


# direct methods
.method public constructor <init>(LX/71t;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1163881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163882
    iput-object p1, p0, LX/721;->a:LX/71t;

    .line 1163883
    return-void
.end method


# virtual methods
.method public final a(LX/6qh;LX/70k;)V
    .locals 0

    .prologue
    .line 1163884
    return-void
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1163885
    check-cast p1, Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;

    const/4 v3, 0x1

    const/4 v0, -0x1

    .line 1163886
    packed-switch p2, :pswitch_data_0

    .line 1163887
    :cond_0
    :goto_0
    return-void

    .line 1163888
    :pswitch_0
    if-ne p3, v0, :cond_0

    if-eqz p4, :cond_0

    .line 1163889
    :pswitch_1
    if-ne p3, v0, :cond_0

    .line 1163890
    if-eqz p4, :cond_1

    .line 1163891
    iget-object v1, p0, LX/721;->a:LX/71t;

    new-instance v2, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;

    invoke-direct {v2, v3}, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;-><init>(Z)V

    sget-object v3, LX/729;->SHIPPING_ADDRESSES:LX/729;

    const-string v0, "shipping_address"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v2, v3, v0}, LX/6vv;->a(Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;LX/6vZ;Ljava/lang/String;)V

    goto :goto_0

    .line 1163892
    :cond_1
    iget-object v0, p0, LX/721;->a:LX/71t;

    new-instance v1, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;

    invoke-direct {v1, v3}, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;-><init>(Z)V

    invoke-virtual {v0, p1, v1}, LX/6vv;->a(Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
