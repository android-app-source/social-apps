.class public LX/8Gm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0TD;

.field private final b:LX/3LP;

.field private final c:LX/3iT;

.field private final d:LX/0WJ;

.field private final e:LX/2RQ;


# direct methods
.method public constructor <init>(LX/3iT;LX/3LP;LX/0WJ;LX/2RQ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1319934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1319935
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, LX/0TA;->a(Ljava/util/concurrent/ExecutorService;)LX/0TD;

    move-result-object v0

    iput-object v0, p0, LX/8Gm;->a:LX/0TD;

    .line 1319936
    iput-object p1, p0, LX/8Gm;->c:LX/3iT;

    .line 1319937
    iput-object p2, p0, LX/8Gm;->b:LX/3LP;

    .line 1319938
    iput-object p3, p0, LX/8Gm;->d:LX/0WJ;

    .line 1319939
    iput-object p4, p0, LX/8Gm;->e:LX/2RQ;

    .line 1319940
    return-void
.end method

.method private static a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/Name;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1319941
    invoke-virtual {p0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/user/model/User;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1319942
    :cond_0
    new-instance v0, Lcom/facebook/user/model/Name;

    .line 1319943
    iget-object v1, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v1, v1

    .line 1319944
    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v2, v1}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1319945
    :goto_0
    return-object v0

    .line 1319946
    :cond_1
    new-instance v0, Lcom/facebook/user/model/Name;

    .line 1319947
    iget-object v1, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v1, v1

    .line 1319948
    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v1

    .line 1319949
    iget-object v2, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v2, v2

    .line 1319950
    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->c()Ljava/lang/String;

    move-result-object v2

    .line 1319951
    iget-object v3, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v3, v3

    .line 1319952
    invoke-virtual {v3}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/8Gm;
    .locals 5

    .prologue
    .line 1319953
    new-instance v4, LX/8Gm;

    invoke-static {p0}, LX/3iT;->b(LX/0QB;)LX/3iT;

    move-result-object v0

    check-cast v0, LX/3iT;

    invoke-static {p0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v1

    check-cast v1, LX/3LP;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v2

    check-cast v2, LX/0WJ;

    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v3

    check-cast v3, LX/2RQ;

    invoke-direct {v4, v0, v1, v2, v3}, LX/8Gm;-><init>(LX/3iT;LX/3LP;LX/0WJ;LX/2RQ;)V

    .line 1319954
    return-object v4
.end method

.method public static b(LX/8Gm;Ljava/util/Collection;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1319955
    iget-object v0, p0, LX/8Gm;->b:LX/3LP;

    iget-object v1, p0, LX/8Gm;->e:LX/2RQ;

    invoke-virtual {v1, p1}, LX/2RQ;->b(Ljava/util/Collection;)LX/2RR;

    move-result-object v1

    sget-object v2, LX/3Oq;->FRIENDS:LX/0Px;

    .line 1319956
    iput-object v2, v1, LX/2RR;->c:Ljava/util/Collection;

    .line 1319957
    move-object v1, v1

    .line 1319958
    invoke-virtual {v0, v1}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v6

    .line 1319959
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v7

    .line 1319960
    :cond_0
    :goto_0
    invoke-interface {v6}, LX/3On;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1319961
    invoke-interface {v6}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/facebook/user/model/User;

    .line 1319962
    if-eqz v2, :cond_0

    .line 1319963
    invoke-virtual {v2}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v4

    .line 1319964
    iget-object v0, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v8, v0

    .line 1319965
    iget-object v0, p0, LX/8Gm;->c:LX/3iT;

    invoke-static {v2}, LX/8Gm;->a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/Name;

    move-result-object v1

    .line 1319966
    iget-object v3, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1319967
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v5, LX/7Gr;->USER:LX/7Gr;

    invoke-virtual/range {v0 .. v5}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1319968
    :cond_1
    invoke-interface {v6}, LX/3On;->close()V

    .line 1319969
    iget-object v0, p0, LX/8Gm;->d:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v4

    .line 1319970
    iget-object v0, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v6, v0

    .line 1319971
    iget-object v0, p0, LX/8Gm;->c:LX/3iT;

    invoke-static {v4}, LX/8Gm;->a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/Name;

    move-result-object v1

    .line 1319972
    iget-object v2, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1319973
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v4}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/7Gr;->SELF:LX/7Gr;

    invoke-virtual/range {v0 .. v5}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    invoke-interface {v7, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319974
    return-object v7
.end method
