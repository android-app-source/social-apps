.class public final LX/7qN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/enums/GraphQLEventType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:I

.field public S:I

.field public T:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/enums/GraphQLEventVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aB:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Z

.field public ac:Z

.field public ad:Z

.field public ae:Z

.field public af:Z

.field public ag:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:I

.field public am:Z

.field public an:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:J

.field public aq:J

.field public ar:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:Z

.field public au:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public av:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aw:I

.field public ax:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ay:Z

.field public az:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:J

.field public o:J

.field public p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:I

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1255577
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
    .locals 61

    .prologue
    .line 1255578
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1255579
    move-object/from16 v0, p0

    iget-object v3, v0, LX/7qN;->a:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    invoke-virtual {v2, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1255580
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7qN;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1255581
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7qN;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1255582
    move-object/from16 v0, p0

    iget-object v6, v0, LX/7qN;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    invoke-static {v2, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1255583
    move-object/from16 v0, p0

    iget-object v7, v0, LX/7qN;->k:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-virtual {v2, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1255584
    move-object/from16 v0, p0

    iget-object v8, v0, LX/7qN;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1255585
    move-object/from16 v0, p0

    iget-object v9, v0, LX/7qN;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1255586
    move-object/from16 v0, p0

    iget-object v10, v0, LX/7qN;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    invoke-static {v2, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1255587
    move-object/from16 v0, p0

    iget-object v11, v0, LX/7qN;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1255588
    move-object/from16 v0, p0

    iget-object v12, v0, LX/7qN;->r:Ljava/lang/String;

    invoke-virtual {v2, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1255589
    move-object/from16 v0, p0

    iget-object v13, v0, LX/7qN;->t:Ljava/lang/String;

    invoke-virtual {v2, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1255590
    move-object/from16 v0, p0

    iget-object v14, v0, LX/7qN;->u:Ljava/lang/String;

    invoke-virtual {v2, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1255591
    move-object/from16 v0, p0

    iget-object v15, v0, LX/7qN;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    invoke-static {v2, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1255592
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->w:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1255593
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->x:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1255594
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1255595
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1255596
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->A:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1255597
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->B:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1255598
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->C:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1255599
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 1255600
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->E:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 1255601
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->F:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v25

    .line 1255602
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->G:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1255603
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->H:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 1255604
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->I:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 1255605
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->J:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v29

    .line 1255606
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->K:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v30

    .line 1255607
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->L:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 1255608
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->M:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 1255609
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->N:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 1255610
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->O:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 1255611
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->P:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    .line 1255612
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->Q:Lcom/facebook/graphql/enums/GraphQLEventType;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v36

    .line 1255613
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->T:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 1255614
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->U:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v38

    .line 1255615
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->V:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 1255616
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->W:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 1255617
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->X:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 1255618
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->Y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 1255619
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->Z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 1255620
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->aa:Ljava/lang/String;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v44

    .line 1255621
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->ag:Ljava/lang/String;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v45

    .line 1255622
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->ah:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 1255623
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->ai:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    move-object/from16 v47, v0

    move-object/from16 v0, v47

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 1255624
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->aj:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v48

    .line 1255625
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->ak:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-object/from16 v49, v0

    move-object/from16 v0, v49

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v49

    .line 1255626
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->an:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-object/from16 v50, v0

    move-object/from16 v0, v50

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 1255627
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->ao:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-object/from16 v51, v0

    move-object/from16 v0, v51

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 1255628
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->ar:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-object/from16 v52, v0

    move-object/from16 v0, v52

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 1255629
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->as:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v53

    .line 1255630
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->au:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-object/from16 v54, v0

    move-object/from16 v0, v54

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v54

    .line 1255631
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->av:Ljava/lang/String;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v55

    .line 1255632
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->ax:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-object/from16 v56, v0

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v56

    .line 1255633
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->az:LX/0Px;

    move-object/from16 v57, v0

    move-object/from16 v0, v57

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v57

    .line 1255634
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->aA:Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-object/from16 v58, v0

    move-object/from16 v0, v58

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v58

    .line 1255635
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7qN;->aB:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object/from16 v59, v0

    move-object/from16 v0, v59

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v59

    .line 1255636
    const/16 v60, 0x50

    move/from16 v0, v60

    invoke-virtual {v2, v0}, LX/186;->c(I)V

    .line 1255637
    const/16 v60, 0x0

    move/from16 v0, v60

    invoke-virtual {v2, v0, v3}, LX/186;->b(II)V

    .line 1255638
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v4}, LX/186;->b(II)V

    .line 1255639
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1255640
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v6}, LX/186;->b(II)V

    .line 1255641
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7qN;->e:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1255642
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7qN;->f:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1255643
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7qN;->g:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1255644
    const/4 v3, 0x7

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7qN;->h:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1255645
    const/16 v3, 0x8

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7qN;->i:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1255646
    const/16 v3, 0x9

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7qN;->j:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1255647
    const/16 v3, 0xa

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1255648
    const/16 v3, 0xb

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1255649
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1255650
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7qN;->n:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1255651
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7qN;->o:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1255652
    const/16 v3, 0xf

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1255653
    const/16 v3, 0x10

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1255654
    const/16 v3, 0x11

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 1255655
    const/16 v3, 0x12

    move-object/from16 v0, p0

    iget v4, v0, LX/7qN;->s:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1255656
    const/16 v3, 0x13

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 1255657
    const/16 v3, 0x14

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 1255658
    const/16 v3, 0x15

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 1255659
    const/16 v3, 0x16

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255660
    const/16 v3, 0x17

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255661
    const/16 v3, 0x18

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255662
    const/16 v3, 0x19

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255663
    const/16 v3, 0x1a

    move/from16 v0, v20

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255664
    const/16 v3, 0x1b

    move/from16 v0, v21

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255665
    const/16 v3, 0x1c

    move/from16 v0, v22

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255666
    const/16 v3, 0x1d

    move/from16 v0, v23

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255667
    const/16 v3, 0x1e

    move/from16 v0, v24

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255668
    const/16 v3, 0x1f

    move/from16 v0, v25

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255669
    const/16 v3, 0x20

    move/from16 v0, v26

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255670
    const/16 v3, 0x21

    move/from16 v0, v27

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255671
    const/16 v3, 0x22

    move/from16 v0, v28

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255672
    const/16 v3, 0x23

    move/from16 v0, v29

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255673
    const/16 v3, 0x24

    move/from16 v0, v30

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255674
    const/16 v3, 0x25

    move/from16 v0, v31

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255675
    const/16 v3, 0x26

    move/from16 v0, v32

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255676
    const/16 v3, 0x27

    move/from16 v0, v33

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255677
    const/16 v3, 0x28

    move/from16 v0, v34

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255678
    const/16 v3, 0x29

    move/from16 v0, v35

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255679
    const/16 v3, 0x2a

    move/from16 v0, v36

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255680
    const/16 v3, 0x2b

    move-object/from16 v0, p0

    iget v4, v0, LX/7qN;->R:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1255681
    const/16 v3, 0x2c

    move-object/from16 v0, p0

    iget v4, v0, LX/7qN;->S:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1255682
    const/16 v3, 0x2d

    move/from16 v0, v37

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255683
    const/16 v3, 0x2e

    move/from16 v0, v38

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255684
    const/16 v3, 0x2f

    move/from16 v0, v39

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255685
    const/16 v3, 0x30

    move/from16 v0, v40

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255686
    const/16 v3, 0x31

    move/from16 v0, v41

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255687
    const/16 v3, 0x32

    move/from16 v0, v42

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255688
    const/16 v3, 0x33

    move/from16 v0, v43

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255689
    const/16 v3, 0x34

    move/from16 v0, v44

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255690
    const/16 v3, 0x35

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7qN;->ab:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1255691
    const/16 v3, 0x36

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7qN;->ac:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1255692
    const/16 v3, 0x37

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7qN;->ad:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1255693
    const/16 v3, 0x38

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7qN;->ae:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1255694
    const/16 v3, 0x39

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7qN;->af:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1255695
    const/16 v3, 0x3a

    move/from16 v0, v45

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255696
    const/16 v3, 0x3b

    move/from16 v0, v46

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255697
    const/16 v3, 0x3c

    move/from16 v0, v47

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255698
    const/16 v3, 0x3d

    move/from16 v0, v48

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255699
    const/16 v3, 0x3e

    move/from16 v0, v49

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255700
    const/16 v3, 0x3f

    move-object/from16 v0, p0

    iget v4, v0, LX/7qN;->al:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1255701
    const/16 v3, 0x40

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7qN;->am:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1255702
    const/16 v3, 0x41

    move/from16 v0, v50

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255703
    const/16 v3, 0x42

    move/from16 v0, v51

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255704
    const/16 v3, 0x43

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7qN;->ap:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1255705
    const/16 v3, 0x44

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7qN;->aq:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1255706
    const/16 v3, 0x45

    move/from16 v0, v52

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255707
    const/16 v3, 0x46

    move/from16 v0, v53

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255708
    const/16 v3, 0x47

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7qN;->at:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1255709
    const/16 v3, 0x48

    move/from16 v0, v54

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255710
    const/16 v3, 0x49

    move/from16 v0, v55

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255711
    const/16 v3, 0x4a

    move-object/from16 v0, p0

    iget v4, v0, LX/7qN;->aw:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1255712
    const/16 v3, 0x4b

    move/from16 v0, v56

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255713
    const/16 v3, 0x4c

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7qN;->ay:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1255714
    const/16 v3, 0x4d

    move/from16 v0, v57

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255715
    const/16 v3, 0x4e

    move/from16 v0, v58

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255716
    const/16 v3, 0x4f

    move/from16 v0, v59

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1255717
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1255718
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1255719
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1255720
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1255721
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1255722
    new-instance v3, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-direct {v3, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;-><init>(LX/15i;)V

    .line 1255723
    return-object v3
.end method
