.class public LX/6ny;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0Tn;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V
    .locals 0
    .param p2    # LX/0Tn;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1147739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1147740
    iput-object p1, p0, LX/6ny;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1147741
    iput-object p2, p0, LX/6ny;->b:LX/0Tn;

    .line 1147742
    return-void
.end method

.method public static c(LX/6ny;Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 1147743
    iget-object v0, p0, LX/6ny;->b:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1147744
    iget-object v0, p0, LX/6ny;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0, p1}, LX/6ny;->c(LX/6ny;Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1147745
    iget-object v0, p0, LX/6ny;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    invoke-static {p0, p1}, LX/6ny;->c(LX/6ny;Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1147746
    return-void
.end method
