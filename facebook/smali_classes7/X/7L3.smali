.class public final LX/7L3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/FullScreenVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V
    .locals 0

    .prologue
    .line 1196810
    iput-object p1, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1196777
    iget-object v0, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->L:LX/7Ks;

    .line 1196778
    iget-boolean v1, v0, LX/7Ks;->b:Z

    move v0, v1

    .line 1196779
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1196780
    :goto_0
    iget-object v1, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->L:LX/7Ks;

    .line 1196781
    iput-boolean v0, v1, LX/7Ks;->b:Z

    .line 1196782
    iget-object v1, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a(Z)V

    .line 1196783
    iget-object v1, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/VideoController;->a(Z)V

    .line 1196784
    return-void

    .line 1196785
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(I)V
    .locals 11

    .prologue
    .line 1196786
    iget-object v0, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    iget-object v1, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v1}, LX/7Kd;->getVideoViewCurrentPosition()I

    move-result v1

    .line 1196787
    iget-object v2, v0, LX/7Kr;->e:LX/0Aq;

    .line 1196788
    iput v1, v2, LX/0Aq;->c:I

    .line 1196789
    iput p1, v2, LX/0Aq;->d:I

    .line 1196790
    iget-object v2, v0, LX/7Kr;->d:LX/1C2;

    iget-object v3, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1196791
    iget-object v4, v3, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a:LX/162;

    move-object v3, v4

    .line 1196792
    iget-object v4, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    .line 1196793
    iget-object v5, v4, Lcom/facebook/video/analytics/VideoPlayerInfo;->a:LX/04G;

    move-object v4, v5

    .line 1196794
    sget-object v5, LX/04g;->BY_USER:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, v0, LX/7Kr;->e:LX/0Aq;

    .line 1196795
    iget v7, v6, LX/0Aq;->c:I

    move v7, v7

    .line 1196796
    iget-object v6, v0, LX/7Kr;->j:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    .line 1196797
    iget-object v8, v6, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a:Ljava/lang/String;

    move-object v8, v8

    .line 1196798
    iget-object v6, v0, LX/7Kr;->k:Lcom/facebook/video/analytics/VideoPlayerInfo;

    .line 1196799
    iget-object v9, v6, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    move-object v9, v9

    .line 1196800
    iget-object v6, v0, LX/7Kr;->f:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1196801
    iget-boolean v10, v6, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->d:Z

    move v10, v10

    .line 1196802
    move v6, p1

    invoke-virtual/range {v2 .. v10}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;IILjava/lang/String;LX/04D;Z)LX/1C2;

    .line 1196803
    iget-object v2, v0, LX/7Kr;->e:LX/0Aq;

    .line 1196804
    iget v3, v2, LX/0Aq;->c:I

    iput v3, v2, LX/0Aq;->b:I

    .line 1196805
    iget-object v2, v0, LX/7Kr;->h:LX/0J7;

    iget-object v3, v0, LX/7Kr;->e:LX/0Aq;

    .line 1196806
    iget v4, v3, LX/0Aq;->b:I

    move v3, v4

    .line 1196807
    iput v3, v2, LX/0J7;->c:I

    .line 1196808
    iget-object v0, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->o()V

    .line 1196809
    return-void
.end method

.method public final a(LX/04g;)V
    .locals 1

    .prologue
    .line 1196774
    iget-object v0, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    .line 1196775
    iput-object p1, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->au:LX/04g;

    .line 1196776
    return-void
.end method

.method public final b(LX/04g;)V
    .locals 6

    .prologue
    .line 1196760
    iget-object v0, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v0}, LX/7Kd;->getVideoViewCurrentPosition()I

    move-result v0

    .line 1196761
    iget-object v1, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->aI:LX/7Kb;

    .line 1196762
    const/4 v4, 0x0

    iput-object v4, v1, LX/7Kb;->d:Ljava/lang/String;

    .line 1196763
    const-wide/16 v4, 0x0

    iput-wide v4, v1, LX/7Kb;->f:J

    .line 1196764
    const-wide/16 v4, -0x64

    iput-wide v4, v1, LX/7Kb;->e:J

    .line 1196765
    iget-object v1, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v1}, LX/7Kd;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1196766
    iget-object v1, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    invoke-virtual {v1, p1, v0}, LX/7Kr;->c(LX/04g;I)V

    .line 1196767
    :cond_0
    sget-object v1, LX/04g;->BY_USER:LX/04g;

    if-ne p1, v1, :cond_1

    .line 1196768
    iget-object v1, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->x:Z

    .line 1196769
    :cond_1
    iget-object v1, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->k()V

    .line 1196770
    iget-object v1, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->av:I

    if-ltz v1, :cond_2

    .line 1196771
    iget-object v0, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->av:I

    .line 1196772
    :cond_2
    iget-object v1, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->ax:LX/16V;

    new-instance v2, LX/2qL;

    sget-object v3, LX/7IB;->b:LX/7IB;

    invoke-direct {v2, v0, v3}, LX/2qL;-><init>(ILX/7IB;)V

    invoke-virtual {v1, v2}, LX/16V;->a(LX/1AD;)V

    .line 1196773
    return-void
.end method

.method public final c(LX/04g;)V
    .locals 4

    .prologue
    .line 1196751
    iget-object v0, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aY:LX/7LE;

    .line 1196752
    iget-boolean v1, v0, LX/7LE;->b:Z

    move v0, v1

    .line 1196753
    if-nez v0, :cond_0

    .line 1196754
    iget-object v0, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v0}, LX/7Kd;->getVideoViewCurrentPosition()I

    move-result v0

    .line 1196755
    iget-object v1, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->ax:LX/16V;

    new-instance v2, LX/2qT;

    sget-object v3, LX/7IB;->b:LX/7IB;

    invoke-direct {v2, v0, v3}, LX/2qT;-><init>(ILX/7IA;)V

    invoke-virtual {v1, v2}, LX/16V;->a(LX/1AD;)V

    .line 1196756
    iget-object v1, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    iget-object v2, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-boolean v2, v2, Lcom/facebook/video/player/FullScreenVideoPlayer;->x:Z

    invoke-virtual {v1, p1, v0, v2}, LX/7Kr;->a(LX/04g;IZ)V

    .line 1196757
    :cond_0
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    if-ne p1, v0, :cond_1

    .line 1196758
    iget-object v0, p0, LX/7L3;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->x:Z

    .line 1196759
    :cond_1
    return-void
.end method
