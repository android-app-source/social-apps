.class public final LX/8Rz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Qe;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/CustomPrivacyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/CustomPrivacyFragment;)V
    .locals 0

    .prologue
    .line 1345832
    iput-object p1, p0, LX/8Rz;->a:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1345833
    iget-object v0, p0, LX/8Rz;->a:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->w:LX/8Qe;

    invoke-interface {v0, p1}, LX/8Qe;->a(Ljava/util/List;)V

    .line 1345834
    iget-object v0, p0, LX/8Rz;->a:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->u:LX/0RV;

    invoke-virtual {v0}, LX/0RV;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v1, p0, LX/8Rz;->a:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->s:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v2, p0, LX/8Rz;->a:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    iget-object v2, v2, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->f:Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    iget-object v3, p0, LX/8Rz;->a:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    iget-object v3, v3, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->t:LX/8Qe;

    invoke-static {p1, v0, v1, v2, v3}, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->b(Ljava/util/List;Ljava/util/List;Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;Lcom/facebook/privacy/selector/CustomPrivacyAdapter;LX/8Qe;)V

    .line 1345835
    return-void
.end method
