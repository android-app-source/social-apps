.class public LX/8Gt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;",
        "Lcom/facebook/graphql/model/GraphQLAlbum;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320178
    return-void
.end method

.method public static a(LX/0QB;)LX/8Gt;
    .locals 1

    .prologue
    .line 1320174
    new-instance v0, LX/8Gt;

    invoke-direct {v0}, LX/8Gt;-><init>()V

    .line 1320175
    move-object v0, v0

    .line 1320176
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1320123
    check-cast p1, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;

    .line 1320124
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1320125
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1320126
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320127
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "name"

    iget-object v3, p1, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320128
    iget-object v1, p1, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1320129
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "place"

    iget-object v3, p1, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320130
    :cond_0
    :goto_0
    iget-object v1, p1, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1320131
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "description"

    iget-object v3, p1, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320132
    :cond_1
    iget-object v1, p1, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1320133
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "privacy"

    iget-object v2, p1, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;->f:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320134
    :cond_2
    :goto_1
    iget-object v0, p1, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1320135
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "id"

    iget-object v2, p1, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;->g:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320136
    :cond_3
    new-instance v0, LX/14N;

    const-string v1, "createPhotoAlbum"

    const-string v2, "POST"

    const-string v3, "method/photos.createAlbum"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 1320137
    :cond_4
    iget-object v1, p1, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1320138
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "location"

    iget-object v3, p1, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1320139
    :cond_5
    iget-object v1, p1, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1320140
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "privacy"

    const-string v3, "value"

    iget-object v5, p1, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;->e:Ljava/lang/String;

    invoke-virtual {v0, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1320141
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1320142
    new-instance v1, LX/173;

    invoke-direct {v1}, LX/173;-><init>()V

    const-string v2, "name"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 1320143
    iput-object v2, v1, LX/173;->f:Ljava/lang/String;

    .line 1320144
    move-object v1, v1

    .line 1320145
    invoke-virtual {v1}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1320146
    const-string v2, "visible"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 1320147
    new-instance v3, LX/4YL;

    invoke-direct {v3}, LX/4YL;-><init>()V

    .line 1320148
    iput-object v2, v3, LX/4YL;->h:Ljava/lang/String;

    .line 1320149
    move-object v3, v3

    .line 1320150
    new-instance v4, LX/2dc;

    invoke-direct {v4}, LX/2dc;-><init>()V

    .line 1320151
    iput-object v2, v4, LX/2dc;->f:Ljava/lang/String;

    .line 1320152
    move-object v2, v4

    .line 1320153
    invoke-virtual {v2}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 1320154
    iput-object v2, v3, LX/4YL;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1320155
    move-object v2, v3

    .line 1320156
    invoke-virtual {v2}, LX/4YL;->a()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    .line 1320157
    const-string v3, "media_owner_object_id"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 1320158
    new-instance v4, LX/25F;

    invoke-direct {v4}, LX/25F;-><init>()V

    .line 1320159
    iput-object v3, v4, LX/25F;->C:Ljava/lang/String;

    .line 1320160
    move-object v3, v4

    .line 1320161
    invoke-virtual {v3}, LX/25F;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    .line 1320162
    new-instance v4, LX/4Vp;

    invoke-direct {v4}, LX/4Vp;-><init>()V

    .line 1320163
    iput-object v2, v4, LX/4Vp;->v:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 1320164
    move-object v2, v4

    .line 1320165
    iput-object v1, v2, LX/4Vp;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1320166
    move-object v1, v2

    .line 1320167
    iput-object v3, v1, LX/4Vp;->p:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 1320168
    move-object v1, v1

    .line 1320169
    const-string v2, "object_id"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1320170
    iput-object v0, v1, LX/4Vp;->m:Ljava/lang/String;

    .line 1320171
    move-object v0, v1

    .line 1320172
    invoke-virtual {v0}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    .line 1320173
    return-object v0
.end method
