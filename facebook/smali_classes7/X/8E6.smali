.class public LX/8E6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/8E5;

.field public final b:Ljava/lang/String;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final d:J

.field public e:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<TT;>;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8E4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/8E3;)V
    .locals 2

    .prologue
    .line 1313432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1313433
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8E6;->f:Z

    .line 1313434
    iget-object v0, p1, LX/8E3;->a:LX/8E5;

    iput-object v0, p0, LX/8E6;->a:LX/8E5;

    .line 1313435
    iget-object v0, p1, LX/8E3;->b:Ljava/lang/String;

    iput-object v0, p0, LX/8E6;->b:Ljava/lang/String;

    .line 1313436
    iget-object v0, p1, LX/8E3;->c:Ljava/util/List;

    iput-object v0, p0, LX/8E6;->c:Ljava/util/List;

    .line 1313437
    iget-wide v0, p1, LX/8E3;->d:J

    iput-wide v0, p0, LX/8E6;->d:J

    .line 1313438
    iget-object v0, p1, LX/8E3;->e:Ljava/util/concurrent/Callable;

    iput-object v0, p0, LX/8E6;->e:Ljava/util/concurrent/Callable;

    .line 1313439
    iget-boolean v0, p1, LX/8E3;->f:Z

    iput-boolean v0, p0, LX/8E6;->f:Z

    .line 1313440
    iget-object v0, p1, LX/8E3;->g:Ljava/util/List;

    iput-object v0, p0, LX/8E6;->g:Ljava/util/List;

    .line 1313441
    return-void
.end method

.method public static a(LX/0Rf;)J
    .locals 5
    .param p0    # LX/0Rf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "LX/8E7;",
            ">;)J"
        }
    .end annotation

    .prologue
    const-wide/16 v0, 0x0

    .line 1313448
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/0Rf;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move-wide v2, v0

    .line 1313449
    :cond_1
    return-wide v2

    .line 1313450
    :cond_2
    invoke-virtual {p0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8E7;

    .line 1313451
    const/4 v1, 0x1

    invoke-virtual {v0}, LX/8E7;->ordinal()I

    move-result v0

    shl-int v0, v1, v0

    int-to-long v0, v0

    or-long/2addr v0, v2

    move-wide v2, v0

    .line 1313452
    goto :goto_0
.end method


# virtual methods
.method public final a(J)Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1313442
    iget-object v0, p0, LX/8E6;->e:Ljava/util/concurrent/Callable;

    if-nez v0, :cond_0

    move v0, v1

    .line 1313443
    :goto_0
    return v0

    .line 1313444
    :cond_0
    iget-object v0, p0, LX/8E6;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1313445
    and-long v6, v4, p1

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    .line 1313446
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1313447
    goto :goto_0
.end method
