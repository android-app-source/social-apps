.class public final enum LX/6en;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6en;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6en;

.field public static final enum FB:LX/6en;

.field public static final enum SMS:LX/6en;

.field public static final enum TINCAN:LX/6en;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1118173
    new-instance v0, LX/6en;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v2}, LX/6en;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6en;->SMS:LX/6en;

    .line 1118174
    new-instance v0, LX/6en;

    const-string v1, "FB"

    invoke-direct {v0, v1, v3}, LX/6en;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6en;->FB:LX/6en;

    .line 1118175
    new-instance v0, LX/6en;

    const-string v1, "TINCAN"

    invoke-direct {v0, v1, v4}, LX/6en;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6en;->TINCAN:LX/6en;

    .line 1118176
    const/4 v0, 0x3

    new-array v0, v0, [LX/6en;

    sget-object v1, LX/6en;->SMS:LX/6en;

    aput-object v1, v0, v2

    sget-object v1, LX/6en;->FB:LX/6en;

    aput-object v1, v0, v3

    sget-object v1, LX/6en;->TINCAN:LX/6en;

    aput-object v1, v0, v4

    sput-object v0, LX/6en;->$VALUES:[LX/6en;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1118178
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6en;
    .locals 1

    .prologue
    .line 1118179
    const-class v0, LX/6en;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6en;

    return-object v0
.end method

.method public static values()[LX/6en;
    .locals 1

    .prologue
    .line 1118177
    sget-object v0, LX/6en;->$VALUES:[LX/6en;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6en;

    return-object v0
.end method
