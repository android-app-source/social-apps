.class public final LX/8GA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/util/List",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
        ">;>;",
        "LX/0Px",
        "<",
        "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/36e;


# direct methods
.method public constructor <init>(LX/36e;)V
    .locals 0

    .prologue
    .line 1318713
    iput-object p1, p0, LX/8GA;->a:LX/36e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1318699
    check-cast p1, Ljava/util/List;

    .line 1318700
    if-nez p1, :cond_0

    .line 1318701
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1318702
    :goto_0
    return-object v0

    .line 1318703
    :cond_0
    new-instance v3, Ljava/util/PriorityQueue;

    const/16 v0, 0xa

    new-instance v1, LX/8G9;

    invoke-direct {v1, p0}, LX/8G9;-><init>(LX/8GA;)V

    invoke-direct {v3, v0, v1}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    .line 1318704
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1318705
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 1318706
    invoke-virtual {v3, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 1318707
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1318708
    :cond_2
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1318709
    :goto_2
    invoke-virtual {v3}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1318710
    invoke-virtual {v3}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 1318711
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1318712
    :cond_3
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
