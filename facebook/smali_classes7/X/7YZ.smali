.class public LX/7YZ;
.super LX/7YU;
.source ""


# instance fields
.field public b:Ljava/lang/String;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0

    .prologue
    .line 1220130
    invoke-direct {p0, p1}, LX/7YU;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1220131
    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V
    .locals 1

    .prologue
    .line 1220121
    invoke-direct {p0, p1, p2}, LX/7YU;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V

    .line 1220122
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YZ;->b:Ljava/lang/String;

    .line 1220123
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->I()Z

    move-result v0

    iput-boolean v0, p0, LX/7YZ;->c:Z

    .line 1220124
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YZ;->d:Ljava/lang/String;

    .line 1220125
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YZ;->e:Ljava/lang/String;

    .line 1220126
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YZ;->f:Ljava/lang/String;

    .line 1220127
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YZ;->g:Ljava/lang/String;

    .line 1220128
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YZ;->h:Ljava/lang/String;

    .line 1220129
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1220117
    iget-object v0, p0, LX/7YU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 1220118
    invoke-super {p0, v1}, LX/7YU;->a(LX/0hN;)V

    .line 1220119
    sget-object v0, LX/0df;->B:LX/0Tn;

    const-string v2, "image_url_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YZ;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v0, LX/0df;->B:LX/0Tn;

    const-string v2, "should_show_confirmation_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-boolean v2, p0, LX/7YZ;->c:Z

    invoke-interface {v1, v0, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    sget-object v0, LX/0df;->B:LX/0Tn;

    const-string v2, "confirmation_title_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YZ;->d:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v0, LX/0df;->B:LX/0Tn;

    const-string v2, "confirmation_description_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YZ;->e:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v0, LX/0df;->B:LX/0Tn;

    const-string v2, "confirmation_primary_button_text_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YZ;->f:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v0, LX/0df;->B:LX/0Tn;

    const-string v2, "confirmation_secondary_button_text_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YZ;->g:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v0, LX/0df;->B:LX/0Tn;

    const-string v2, "confirmation_back_button_behavior_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YZ;->h:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1220120
    return-void
.end method

.method public final b()LX/0Tn;
    .locals 1

    .prologue
    .line 1220116
    sget-object v0, LX/0df;->B:LX/0Tn;

    return-object v0
.end method
