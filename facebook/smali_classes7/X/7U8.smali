.class public LX/7U8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1211615
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1211616
    return-void
.end method

.method public static a(IIIIDD)Landroid/graphics/Matrix;
    .locals 8

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    .line 1211617
    int-to-float v0, p2

    int-to-float v2, p3

    div-float/2addr v0, v2

    .line 1211618
    int-to-float v2, p0

    int-to-float v3, p1

    div-float/2addr v2, v3

    .line 1211619
    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 1211620
    int-to-float v0, p1

    int-to-float v2, p3

    div-float/2addr v0, v2

    .line 1211621
    int-to-float v2, p2

    mul-float/2addr v2, v0

    float-to-double v2, v2

    mul-double/2addr v2, p4

    int-to-float v4, p0

    mul-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1211622
    int-to-float v3, p2

    mul-float/2addr v3, v0

    int-to-float v4, p0

    sub-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 1211623
    :goto_0
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 1211624
    invoke-virtual {v3}, Landroid/graphics/Matrix;->reset()V

    .line 1211625
    invoke-virtual {v3, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1211626
    neg-float v0, v2

    neg-float v1, v1

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1211627
    return-object v3

    .line 1211628
    :cond_0
    int-to-float v0, p0

    int-to-float v2, p2

    div-float/2addr v0, v2

    .line 1211629
    int-to-float v2, p3

    mul-float/2addr v2, v0

    float-to-double v2, v2

    mul-double/2addr v2, p6

    int-to-float v4, p1

    mul-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1211630
    int-to-float v3, p3

    mul-float/2addr v3, v0

    int-to-float v4, p1

    sub-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    move v6, v2

    move v2, v1

    move v1, v6

    goto :goto_0
.end method
