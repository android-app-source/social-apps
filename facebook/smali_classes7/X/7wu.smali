.class public final LX/7wu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

.field private d:J

.field private e:J

.field private f:I

.field private g:I

.field private h:Lcom/facebook/payments/currency/CurrencyAmount;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:I

.field private l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1276749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)LX/7wu;
    .locals 0

    .prologue
    .line 1276747
    iput p1, p0, LX/7wu;->f:I

    .line 1276748
    return-object p0
.end method

.method public final a(J)LX/7wu;
    .locals 1

    .prologue
    .line 1276745
    iput-wide p1, p0, LX/7wu;->d:J

    .line 1276746
    return-object p0
.end method

.method public final a(LX/0Px;)LX/7wu;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;",
            ">;)",
            "LX/7wu;"
        }
    .end annotation

    .prologue
    .line 1276743
    iput-object p1, p0, LX/7wu;->l:LX/0Px;

    .line 1276744
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;)LX/7wu;
    .locals 0

    .prologue
    .line 1276741
    iput-object p1, p0, LX/7wu;->c:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    .line 1276742
    return-object p0
.end method

.method public final a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/7wu;
    .locals 0

    .prologue
    .line 1276739
    iput-object p1, p0, LX/7wu;->h:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1276740
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/7wu;
    .locals 0

    .prologue
    .line 1276750
    iput-object p1, p0, LX/7wu;->a:Ljava/lang/String;

    .line 1276751
    return-object p0
.end method

.method public final a()Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;
    .locals 15

    .prologue
    .line 1276738
    new-instance v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    iget-object v1, p0, LX/7wu;->a:Ljava/lang/String;

    iget-object v2, p0, LX/7wu;->b:Ljava/lang/String;

    iget-object v3, p0, LX/7wu;->c:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    iget-wide v4, p0, LX/7wu;->d:J

    iget-wide v6, p0, LX/7wu;->e:J

    iget v8, p0, LX/7wu;->f:I

    iget v9, p0, LX/7wu;->g:I

    iget-object v10, p0, LX/7wu;->h:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v11, p0, LX/7wu;->i:Ljava/lang/String;

    iget-object v12, p0, LX/7wu;->j:Ljava/lang/String;

    iget v13, p0, LX/7wu;->k:I

    iget-object v14, p0, LX/7wu;->l:LX/0Px;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;JJIILcom/facebook/payments/currency/CurrencyAmount;Ljava/lang/String;Ljava/lang/String;ILX/0Px;)V

    return-object v0
.end method

.method public final b(I)LX/7wu;
    .locals 0

    .prologue
    .line 1276736
    iput p1, p0, LX/7wu;->g:I

    .line 1276737
    return-object p0
.end method

.method public final b(J)LX/7wu;
    .locals 1

    .prologue
    .line 1276734
    iput-wide p1, p0, LX/7wu;->e:J

    .line 1276735
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/7wu;
    .locals 0

    .prologue
    .line 1276732
    iput-object p1, p0, LX/7wu;->b:Ljava/lang/String;

    .line 1276733
    return-object p0
.end method

.method public final c(I)LX/7wu;
    .locals 0

    .prologue
    .line 1276726
    iput p1, p0, LX/7wu;->k:I

    .line 1276727
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/7wu;
    .locals 0

    .prologue
    .line 1276730
    iput-object p1, p0, LX/7wu;->i:Ljava/lang/String;

    .line 1276731
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/7wu;
    .locals 0

    .prologue
    .line 1276728
    iput-object p1, p0, LX/7wu;->j:Ljava/lang/String;

    .line 1276729
    return-object p0
.end method
