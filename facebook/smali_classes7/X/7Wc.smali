.class public LX/7Wc;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1216718
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1216719
    iput-object p1, p0, LX/7Wc;->a:Landroid/content/Context;

    .line 1216720
    iput-object p2, p0, LX/7Wc;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1216721
    new-instance v0, LX/7Wb;

    invoke-direct {v0, p0}, LX/7Wb;-><init>(LX/7Wc;)V

    invoke-virtual {p0, v0}, LX/7Wc;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1216722
    const v0, 0x7f080e9e

    invoke-virtual {p0, v0}, LX/7Wc;->setTitle(I)V

    .line 1216723
    return-void
.end method

.method public static b(LX/0QB;)LX/7Wc;
    .locals 3

    .prologue
    .line 1216724
    new-instance v2, LX/7Wc;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v2, v0, v1}, LX/7Wc;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 1216725
    return-object v2
.end method
