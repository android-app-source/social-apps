.class public abstract LX/7HQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rf;
.implements LX/2Sq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0rf;",
        "LX/2Sq",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;

.field public static final c:Ljava/lang/String;


# instance fields
.field public a:LX/7B6;

.field private final d:LX/7Hg;

.field private final e:LX/7Hl;

.field public final f:LX/7Hj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Hj",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final g:LX/7HW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7HW",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final h:LX/2Sd;

.field public final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TT;TT;>;"
        }
    .end annotation
.end field

.field public final j:LX/7Hn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Hn",
            "<TT;>;"
        }
    .end annotation
.end field

.field public k:LX/7HP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7HP",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1190652
    const-class v0, LX/7HQ;

    .line 1190653
    sput-object v0, LX/7HQ;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7HQ;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/7Hg;LX/7Hl;LX/7HW;LX/7Hk;LX/2Sd;)V
    .locals 1

    .prologue
    .line 1190682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1190683
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/7HQ;->i:Ljava/util/Map;

    .line 1190684
    new-instance v0, LX/7Hn;

    invoke-direct {v0}, LX/7Hn;-><init>()V

    iput-object v0, p0, LX/7HQ;->j:LX/7Hn;

    .line 1190685
    sget-object v0, LX/7B6;->a:LX/7B6;

    iput-object v0, p0, LX/7HQ;->a:LX/7B6;

    .line 1190686
    iput-object p1, p0, LX/7HQ;->d:LX/7Hg;

    .line 1190687
    iput-object p2, p0, LX/7HQ;->e:LX/7Hl;

    .line 1190688
    iput-object p3, p0, LX/7HQ;->g:LX/7HW;

    .line 1190689
    iget-object v0, p0, LX/7HQ;->g:LX/7HW;

    invoke-static {v0}, LX/7Hk;->a(LX/7HW;)LX/7Hj;

    move-result-object v0

    iput-object v0, p0, LX/7HQ;->f:LX/7Hj;

    .line 1190690
    iget-object v0, p0, LX/7HQ;->d:LX/7Hg;

    .line 1190691
    iput-object p0, v0, LX/7Hg;->j:LX/2Sq;

    .line 1190692
    iput-object p5, p0, LX/7HQ;->h:LX/2Sd;

    .line 1190693
    return-void
.end method

.method public static a(LX/7HQ;LX/7Hi;LX/7Hi;)LX/7Hc;
    .locals 8
    .param p1    # LX/7Hi;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<TT;>;",
            "LX/7Hi",
            "<TT;>;)",
            "LX/7Hc",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1190660
    iget-object v0, p0, LX/7HQ;->a:LX/7B6;

    iget-object v0, v0, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, LX/7HQ;->b(LX/7Hi;Ljava/lang/String;)LX/7Hi;

    move-result-object v1

    .line 1190661
    if-nez p2, :cond_2

    move-object v0, v1

    .line 1190662
    :goto_0
    invoke-direct {p0, v0}, LX/7HQ;->h(LX/7Hi;)V

    .line 1190663
    iget-object v0, v1, LX/7Hi;->b:LX/7Hc;

    move-object v2, v0

    .line 1190664
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1190665
    iget-object v0, v2, LX/7Hc;->b:LX/0Px;

    move-object v4, v0

    .line 1190666
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_1

    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    .line 1190667
    iget-object v7, p0, LX/7HQ;->i:Ljava/util/Map;

    invoke-interface {v7, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 1190668
    if-nez v7, :cond_0

    .line 1190669
    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1190670
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1190671
    :cond_0
    iget-object p1, p0, LX/7HQ;->g:LX/7HW;

    sget-object p2, LX/7HY;->MEMORY_CACHE:LX/7HY;

    invoke-interface {p1, v6, v7, p2}, LX/7HW;->a(Ljava/lang/Object;Ljava/lang/Object;LX/7HY;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1190672
    :cond_1
    new-instance v0, LX/7Hi;

    .line 1190673
    iget-object v4, v1, LX/7Hi;->a:LX/7B6;

    move-object v4, v4

    .line 1190674
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-static {v2, v3}, LX/7Hc;->a(LX/7Hc;LX/0Px;)LX/7Hc;

    move-result-object v2

    .line 1190675
    iget-object v3, v1, LX/7Hi;->c:LX/7HY;

    move-object v3, v3

    .line 1190676
    iget-object v5, v1, LX/7Hi;->d:LX/7Ha;

    move-object v5, v5

    .line 1190677
    invoke-direct {v0, v4, v2, v3, v5}, LX/7Hi;-><init>(LX/7B6;LX/7Hc;LX/7HY;LX/7Ha;)V

    move-object v0, v0

    .line 1190678
    invoke-virtual {p0, v0}, LX/7HQ;->d(LX/7Hi;)V

    .line 1190679
    iget-object v1, v0, LX/7Hi;->b:LX/7Hc;

    move-object v0, v1

    .line 1190680
    return-object v0

    .line 1190681
    :cond_2
    iget-object v0, p0, LX/7HQ;->a:LX/7B6;

    iget-object v0, v0, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {p0, p2, v0}, LX/7HQ;->b(LX/7Hi;Ljava/lang/String;)LX/7Hi;

    move-result-object v0

    goto :goto_0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 1190658
    iget-object v0, p0, LX/7HQ;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1190659
    return-void
.end method

.method private h(LX/7Hi;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1190654
    iget-object v0, p0, LX/7HQ;->j:LX/7Hn;

    .line 1190655
    iget-object v1, p1, LX/7Hi;->a:LX/7B6;

    move-object v1, v1

    .line 1190656
    iget-object v1, v1, LX/7B6;->c:Ljava/lang/String;

    iget-object v2, p0, LX/7HQ;->a:LX/7B6;

    iget-object v2, v2, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1}, LX/7Hn;->a(Ljava/lang/String;Ljava/lang/String;LX/7Hi;)V

    .line 1190657
    return-void
.end method


# virtual methods
.method public a(LX/7Hi;Ljava/lang/String;)LX/7HN;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "LX/7HN",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1190578
    sget-object v0, LX/7Hb;->a:LX/7Hb;

    move-object v0, v0

    .line 1190579
    return-object v0
.end method

.method public final a(LX/0P1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1190647
    iget-object v0, p0, LX/7HQ;->d:LX/7Hg;

    .line 1190648
    invoke-static {v0}, LX/7Hg;->g(LX/7Hg;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1190649
    iget-object p0, v0, LX/7Hg;->a:LX/7He;

    invoke-static {p0, p1}, LX/7He;->a$redex0(LX/7He;LX/0P1;)V

    .line 1190650
    :cond_0
    iget-object p0, v0, LX/7Hg;->b:LX/7He;

    invoke-static {p0, p1}, LX/7He;->a$redex0(LX/7He;LX/0P1;)V

    .line 1190651
    return-void
.end method

.method public final a(LX/2Sp;)V
    .locals 1

    .prologue
    .line 1190644
    iget-object v0, p0, LX/7HQ;->d:LX/7Hg;

    .line 1190645
    iput-object p1, v0, LX/7Hg;->k:LX/2Sp;

    .line 1190646
    return-void
.end method

.method public a(LX/32G;)V
    .locals 1

    .prologue
    .line 1190641
    iget-object v0, p0, LX/7HQ;->e:LX/7Hl;

    invoke-virtual {v0, p1}, LX/7Hl;->a(LX/32G;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1190642
    invoke-direct {p0}, LX/7HQ;->g()V

    .line 1190643
    :cond_0
    return-void
.end method

.method public a(LX/7B6;)V
    .locals 0

    .prologue
    .line 1190639
    invoke-virtual {p0, p1}, LX/7HQ;->b(LX/7B6;)Z

    .line 1190640
    return-void
.end method

.method public a(LX/7Hi;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1190580
    iget-object v0, p1, LX/7Hi;->c:LX/7HY;

    move-object v0, v0

    .line 1190581
    invoke-virtual {v0}, LX/7HY;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1190582
    iget-object v1, p1, LX/7Hi;->b:LX/7Hc;

    move-object v1, v1

    .line 1190583
    iget-object v2, v1, LX/7Hc;->b:LX/0Px;

    move-object v1, v2

    .line 1190584
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 1190585
    if-lez v1, :cond_0

    .line 1190586
    iget-object v2, p0, LX/7HQ;->h:LX/2Sd;

    sget-object v3, LX/7HQ;->c:Ljava/lang/String;

    sget-object v4, LX/7CQ;->TYPEAHEAD_UNITS_LOADED:LX/7CQ;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Source: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Num: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;Ljava/lang/String;)V

    .line 1190587
    :cond_0
    invoke-virtual {p0, p1}, LX/7HQ;->b(LX/7Hi;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1190588
    iget-object v0, p1, LX/7Hi;->a:LX/7B6;

    move-object v0, v0

    .line 1190589
    invoke-virtual {v0}, LX/7B6;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1190590
    invoke-virtual {p0, p1}, LX/7HQ;->c(LX/7Hi;)LX/7Hc;

    .line 1190591
    :cond_1
    :goto_0
    iget-object v0, p1, LX/7Hi;->c:LX/7HY;

    move-object v0, v0

    .line 1190592
    sget-object v1, LX/7HY;->REMOTE:LX/7HY;

    if-ne v0, v1, :cond_2

    .line 1190593
    iget-object v0, p1, LX/7Hi;->b:LX/7Hc;

    move-object v0, v0

    .line 1190594
    iget-object v1, v0, LX/7Hc;->b:LX/0Px;

    move-object v1, v1

    .line 1190595
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 1190596
    iget-object v4, p0, LX/7HQ;->i:Ljava/util/Map;

    invoke-interface {v4, v3, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1190597
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1190598
    :cond_2
    return-void

    .line 1190599
    :cond_3
    iget-object v0, p0, LX/7HQ;->j:LX/7Hn;

    .line 1190600
    iget-object v1, p1, LX/7Hi;->a:LX/7B6;

    move-object v1, v1

    .line 1190601
    iget-object v1, v1, LX/7B6;->c:Ljava/lang/String;

    iget-object v2, p0, LX/7HQ;->a:LX/7B6;

    iget-object v2, v2, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/7Hn;->a(Ljava/lang/String;Ljava/lang/String;)LX/7Hi;

    move-result-object v1

    .line 1190602
    sget-object v0, LX/7Ha;->EXACT:LX/7Ha;

    .line 1190603
    iget-object v2, p0, LX/7HQ;->a:LX/7B6;

    iget-object v2, v2, LX/7B6;->b:Ljava/lang/String;

    .line 1190604
    iget-object v3, p1, LX/7Hi;->a:LX/7B6;

    move-object v3, v3

    .line 1190605
    iget-object v3, v3, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1190606
    sget-object v0, LX/7Ha;->PREFIX:LX/7Ha;

    .line 1190607
    if-eqz v1, :cond_4

    .line 1190608
    iget-object v2, v1, LX/7Hi;->b:LX/7Hc;

    move-object v2, v2

    .line 1190609
    iget-object v3, v2, LX/7Hc;->b:LX/0Px;

    move-object v2, v3

    .line 1190610
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1190611
    iget-object v2, v1, LX/7Hi;->c:LX/7HY;

    move-object v2, v2

    .line 1190612
    sget-object v3, LX/7HY;->REMOTE:LX/7HY;

    if-ne v2, v3, :cond_4

    .line 1190613
    iget-object v2, v1, LX/7Hi;->d:LX/7Ha;

    move-object v2, v2

    .line 1190614
    sget-object v3, LX/7Ha;->EXACT:LX/7Ha;

    if-ne v2, v3, :cond_4

    .line 1190615
    goto :goto_0

    .line 1190616
    :cond_4
    iget-object v2, p0, LX/7HQ;->a:LX/7B6;

    iget-object v2, v2, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {p0, p1, v2}, LX/7HQ;->a(LX/7Hi;Ljava/lang/String;)LX/7HN;

    move-result-object v2

    .line 1190617
    iget-object v3, p0, LX/7HQ;->a:LX/7B6;

    iget-object v3, v3, LX/7B6;->b:Ljava/lang/String;

    invoke-interface {v2, p1, v3}, LX/7HN;->a(LX/7Hi;Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 1190618
    new-instance v3, LX/7Hi;

    .line 1190619
    iget-object v4, p1, LX/7Hi;->a:LX/7B6;

    move-object v4, v4

    .line 1190620
    iget-object v5, p1, LX/7Hi;->b:LX/7Hc;

    move-object v5, v5

    .line 1190621
    invoke-static {v5, v2}, LX/7Hc;->a(LX/7Hc;LX/0Px;)LX/7Hc;

    move-result-object v2

    .line 1190622
    iget-object v5, p1, LX/7Hi;->c:LX/7HY;

    move-object v5, v5

    .line 1190623
    sget-object v6, LX/7Ha;->EXACT:LX/7Ha;

    invoke-direct {v3, v4, v2, v5, v6}, LX/7Hi;-><init>(LX/7B6;LX/7Hc;LX/7HY;LX/7Ha;)V

    move-object v2, v3

    .line 1190624
    iget-object v3, v2, LX/7Hi;->b:LX/7Hc;

    move-object v2, v3

    .line 1190625
    if-eqz v1, :cond_5

    .line 1190626
    iget-object v3, v1, LX/7Hi;->b:LX/7Hc;

    move-object v1, v3

    .line 1190627
    :goto_2
    iget-object v3, p0, LX/7HQ;->f:LX/7Hj;

    sget-object v4, LX/7HY;->MEMORY_CACHE:LX/7HY;

    invoke-virtual {v3, v1, v2, v4}, LX/7Hj;->a(LX/7Hc;LX/7Hc;LX/7HY;)LX/7Hc;

    move-result-object v3

    .line 1190628
    iget-object v4, p0, LX/7HQ;->f:LX/7Hj;

    sget-object v5, LX/7HY;->MEMORY_CACHE:LX/7HY;

    invoke-virtual {v4, v1, v2, v5}, LX/7Hj;->a(LX/7Hc;LX/7Hc;LX/7HY;)LX/7Hc;

    move-result-object v4

    .line 1190629
    new-instance v5, LX/7Hi;

    .line 1190630
    iget-object v6, p1, LX/7Hi;->a:LX/7B6;

    move-object v6, v6

    .line 1190631
    iget-object v7, p1, LX/7Hi;->c:LX/7HY;

    move-object v7, v7

    .line 1190632
    invoke-direct {v5, v6, v4, v7, v0}, LX/7Hi;-><init>(LX/7B6;LX/7Hc;LX/7HY;LX/7Ha;)V

    move-object v1, v5

    .line 1190633
    new-instance v2, LX/7Hi;

    .line 1190634
    iget-object v4, p1, LX/7Hi;->a:LX/7B6;

    move-object v4, v4

    .line 1190635
    iget-object v5, p1, LX/7Hi;->c:LX/7HY;

    move-object v5, v5

    .line 1190636
    invoke-direct {v2, v4, v3, v5, v0}, LX/7Hi;-><init>(LX/7B6;LX/7Hc;LX/7HY;LX/7Ha;)V

    invoke-static {p0, v1, v2}, LX/7HQ;->a(LX/7HQ;LX/7Hi;LX/7Hi;)LX/7Hc;

    goto/16 :goto_0

    .line 1190637
    :cond_5
    sget-object v1, LX/7Hc;->a:LX/7Hc;

    move-object v1, v1

    .line 1190638
    goto :goto_2
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1190694
    return-void
.end method

.method public b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1190547
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1190548
    return-object v0
.end method

.method public b(LX/7Hi;Ljava/lang/String;)LX/7Hi;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "LX/7Hi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1190549
    return-object p1
.end method

.method public b(LX/7B6;)Z
    .locals 1

    .prologue
    .line 1190550
    iput-object p1, p0, LX/7HQ;->a:LX/7B6;

    .line 1190551
    iget-object v0, p1, LX/7B6;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1190552
    invoke-virtual {p0}, LX/7HQ;->d()V

    .line 1190553
    :cond_0
    iget-object v0, p0, LX/7HQ;->d:LX/7Hg;

    invoke-virtual {v0, p1}, LX/7Hg;->a(LX/7B6;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/7Hi;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 1190554
    iget-object v0, p1, LX/7Hi;->c:LX/7HY;

    move-object v0, v0

    .line 1190555
    sget-object v1, LX/7HY;->REMOTE:LX/7HY;

    if-eq v0, v1, :cond_0

    .line 1190556
    iget-object v0, p1, LX/7Hi;->c:LX/7HY;

    move-object v0, v0

    .line 1190557
    sget-object v1, LX/7HY;->MEMORY_CACHE:LX/7HY;

    if-eq v0, v1, :cond_0

    .line 1190558
    iget-object v0, p1, LX/7Hi;->a:LX/7B6;

    move-object v0, v0

    .line 1190559
    iget-object v0, v0, LX/7B6;->b:Ljava/lang/String;

    iget-object v1, p0, LX/7HQ;->a:LX/7B6;

    iget-object v1, v1, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1190560
    iget-object v0, p1, LX/7Hi;->c:LX/7HY;

    move-object v0, v0

    .line 1190561
    sget-object v1, LX/7HY;->LOCAL:LX/7HY;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(LX/7Hi;)LX/7Hc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<TT;>;)",
            "LX/7Hc",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1190562
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/7HQ;->a(LX/7HQ;LX/7Hi;LX/7Hi;)LX/7Hc;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1190563
    iget-object v0, p0, LX/7HQ;->a:LX/7B6;

    iget-object v0, v0, LX/7B6;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 1190564
    invoke-virtual {p0}, LX/7HQ;->f()LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/7HQ;->a(LX/0P1;)V

    .line 1190565
    invoke-virtual {p0}, LX/7HQ;->mI_()V

    .line 1190566
    iget-object v0, p0, LX/7HQ;->e:LX/7Hl;

    invoke-virtual {v0}, LX/7Hl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1190567
    invoke-direct {p0}, LX/7HQ;->g()V

    .line 1190568
    :cond_0
    return-void
.end method

.method public d(LX/7Hi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1190569
    iget-object v0, p0, LX/7HQ;->k:LX/7HP;

    if-eqz v0, :cond_0

    .line 1190570
    iget-object v0, p0, LX/7HQ;->k:LX/7HP;

    invoke-interface {v0, p1}, LX/7HP;->a(LX/7Hi;)V

    .line 1190571
    :cond_0
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 1190572
    return-void
.end method

.method public f()LX/0P2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P2",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1190573
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    return-object v0
.end method

.method public mI_()V
    .locals 5

    .prologue
    .line 1190574
    new-instance v0, LX/7Hi;

    sget-object v1, LX/7B6;->a:LX/7B6;

    new-instance v2, LX/7Hc;

    invoke-virtual {p0}, LX/7HQ;->b()LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3}, LX/7Hc;-><init>(LX/0Px;)V

    sget-object v3, LX/7HY;->NULL_STATE:LX/7HY;

    sget-object v4, LX/7Ha;->EXACT:LX/7Ha;

    invoke-direct {v0, v1, v2, v3, v4}, LX/7Hi;-><init>(LX/7B6;LX/7Hc;LX/7HY;LX/7Ha;)V

    .line 1190575
    invoke-direct {p0, v0}, LX/7HQ;->h(LX/7Hi;)V

    .line 1190576
    invoke-virtual {p0, v0}, LX/7HQ;->d(LX/7Hi;)V

    .line 1190577
    return-void
.end method
