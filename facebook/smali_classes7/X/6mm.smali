.class public LX/6mm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final unsubscribeGenericTopics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final unsubscribeTopics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xf

    const/4 v3, 0x1

    .line 1146578
    new-instance v0, LX/1sv;

    const-string v1, "UnsubscribeMessage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mm;->b:LX/1sv;

    .line 1146579
    new-instance v0, LX/1sw;

    const-string v1, "unsubscribeTopics"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mm;->c:LX/1sw;

    .line 1146580
    new-instance v0, LX/1sw;

    const-string v1, "unsubscribeGenericTopics"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mm;->d:LX/1sw;

    .line 1146581
    sput-boolean v3, LX/6mm;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1146614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146615
    iput-object p1, p0, LX/6mm;->unsubscribeTopics:Ljava/util/List;

    .line 1146616
    iput-object p2, p0, LX/6mm;->unsubscribeGenericTopics:Ljava/util/List;

    .line 1146617
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1146582
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1146583
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1146584
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1146585
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "UnsubscribeMessage"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1146586
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146587
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146588
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146589
    const/4 v1, 0x1

    .line 1146590
    iget-object v5, p0, LX/6mm;->unsubscribeTopics:Ljava/util/List;

    if-eqz v5, :cond_0

    .line 1146591
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146592
    const-string v1, "unsubscribeTopics"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146593
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146594
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146595
    iget-object v1, p0, LX/6mm;->unsubscribeTopics:Ljava/util/List;

    if-nez v1, :cond_6

    .line 1146596
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146597
    :goto_3
    const/4 v1, 0x0

    .line 1146598
    :cond_0
    iget-object v5, p0, LX/6mm;->unsubscribeGenericTopics:Ljava/util/List;

    if-eqz v5, :cond_2

    .line 1146599
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146600
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146601
    const-string v1, "unsubscribeGenericTopics"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146602
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146603
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146604
    iget-object v0, p0, LX/6mm;->unsubscribeGenericTopics:Ljava/util/List;

    if-nez v0, :cond_7

    .line 1146605
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146606
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146607
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146608
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1146609
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1146610
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1146611
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1146612
    :cond_6
    iget-object v1, p0, LX/6mm;->unsubscribeTopics:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1146613
    :cond_7
    iget-object v0, p0, LX/6mm;->unsubscribeGenericTopics:Ljava/util/List;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1146618
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1146619
    iget-object v0, p0, LX/6mm;->unsubscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1146620
    iget-object v0, p0, LX/6mm;->unsubscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1146621
    sget-object v0, LX/6mm;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146622
    new-instance v0, LX/1u3;

    const/16 v1, 0x8

    iget-object v2, p0, LX/6mm;->unsubscribeTopics:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1146623
    iget-object v0, p0, LX/6mm;->unsubscribeTopics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1146624
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    goto :goto_0

    .line 1146625
    :cond_0
    iget-object v0, p0, LX/6mm;->unsubscribeGenericTopics:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1146626
    iget-object v0, p0, LX/6mm;->unsubscribeGenericTopics:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1146627
    sget-object v0, LX/6mm;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146628
    new-instance v0, LX/1u3;

    const/16 v1, 0xb

    iget-object v2, p0, LX/6mm;->unsubscribeGenericTopics:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1146629
    iget-object v0, p0, LX/6mm;->unsubscribeGenericTopics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1146630
    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 1146631
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1146632
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1146633
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1146552
    if-nez p1, :cond_1

    .line 1146553
    :cond_0
    :goto_0
    return v0

    .line 1146554
    :cond_1
    instance-of v1, p1, LX/6mm;

    if-eqz v1, :cond_0

    .line 1146555
    check-cast p1, LX/6mm;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1146556
    if-nez p1, :cond_3

    .line 1146557
    :cond_2
    :goto_1
    move v0, v2

    .line 1146558
    goto :goto_0

    .line 1146559
    :cond_3
    iget-object v0, p0, LX/6mm;->unsubscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1146560
    :goto_2
    iget-object v3, p1, LX/6mm;->unsubscribeTopics:Ljava/util/List;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1146561
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1146562
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146563
    iget-object v0, p0, LX/6mm;->unsubscribeTopics:Ljava/util/List;

    iget-object v3, p1, LX/6mm;->unsubscribeTopics:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1146564
    :cond_5
    iget-object v0, p0, LX/6mm;->unsubscribeGenericTopics:Ljava/util/List;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1146565
    :goto_4
    iget-object v3, p1, LX/6mm;->unsubscribeGenericTopics:Ljava/util/List;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1146566
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1146567
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146568
    iget-object v0, p0, LX/6mm;->unsubscribeGenericTopics:Ljava/util/List;

    iget-object v3, p1, LX/6mm;->unsubscribeGenericTopics:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1146569
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1146570
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1146571
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1146572
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1146573
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1146574
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1146575
    sget-boolean v0, LX/6mm;->a:Z

    .line 1146576
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mm;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1146577
    return-object v0
.end method
