.class public final LX/6oy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QR",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

.field public final synthetic d:LX/6p6;


# direct methods
.method public constructor <init>(LX/6p6;JLjava/lang/String;Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;)V
    .locals 0

    .prologue
    .line 1148730
    iput-object p1, p0, LX/6oy;->d:LX/6p6;

    iput-wide p2, p0, LX/6oy;->a:J

    iput-object p4, p0, LX/6oy;->b:Ljava/lang/String;

    iput-object p5, p0, LX/6oy;->c:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1148731
    iget-object v0, p0, LX/6oy;->d:LX/6p6;

    iget-object v1, v0, LX/6p6;->i:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    iget-wide v2, p0, LX/6oy;->a:J

    iget-object v4, p0, LX/6oy;->b:Ljava/lang/String;

    iget-object v0, p0, LX/6oy;->c:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    .line 1148732
    iget-object v5, v0, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;->b:LX/03R;

    move-object v5, v5

    .line 1148733
    iget-object v0, p0, LX/6oy;->c:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;->b()LX/0P1;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(JLjava/lang/String;LX/03R;Ljava/util/Map;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
