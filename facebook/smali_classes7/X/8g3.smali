.class public final LX/8g3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "sentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "sentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1386431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1386432
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1386433
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1386434
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v3, p0, LX/8g3;->a:LX/15i;

    iget v5, p0, LX/8g3;->b:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v1, -0x5f19e7b1

    invoke-static {v3, v5, v1}, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$DraculaImplementation;

    move-result-object v1

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1386435
    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1386436
    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1386437
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1386438
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1386439
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1386440
    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1386441
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1386442
    new-instance v1, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    invoke-direct {v1, v0}, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;-><init>(LX/15i;)V

    .line 1386443
    return-object v1

    .line 1386444
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
