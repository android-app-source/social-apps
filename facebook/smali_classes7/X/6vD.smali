.class public final LX/6vD;
.super LX/6oO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;)V
    .locals 0

    .prologue
    .line 1156724
    iput-object p1, p0, LX/6vD;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    invoke-direct {p0}, LX/6oO;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1156725
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, LX/6vD;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    iget-object v1, v1, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->f:LX/6wm;

    invoke-interface {v1}, LX/6wm;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1156726
    iget-object v0, p0, LX/6vD;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->e:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    .line 1156727
    :cond_0
    iget-object v0, p0, LX/6vD;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->c:LX/6v4;

    iget-object v1, p0, LX/6vD;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    invoke-virtual {v1}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->c()Z

    move-result v1

    .line 1156728
    iget-object p0, v0, LX/6v4;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    iget-object p0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->o:LX/108;

    .line 1156729
    iput-boolean v1, p0, LX/108;->d:Z

    .line 1156730
    iget-object p0, v0, LX/6v4;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    iget-object p0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->h:LX/0h5;

    iget-object p1, v0, LX/6v4;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    iget-object p1, p1, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->o:LX/108;

    invoke-virtual {p1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object p1

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1156731
    return-void
.end method
