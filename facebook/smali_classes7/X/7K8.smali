.class public LX/7K8;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Landroid/graphics/Matrix;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1194527
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, LX/7K8;->a:Landroid/graphics/Matrix;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1194528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194529
    return-void
.end method

.method public static a(IILX/2oF;Landroid/graphics/RectF;)Landroid/graphics/Matrix;
    .locals 10

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1194530
    sget-object v1, LX/7K8;->a:Landroid/graphics/Matrix;

    .line 1194531
    sget-object v2, Lcom/facebook/video/engine/VideoDataSource;->a:Landroid/graphics/RectF;

    invoke-virtual {v2, p3}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, LX/2oF;->MIRROR_HORIZONTALLY:LX/2oF;

    if-ne p2, v2, :cond_3

    .line 1194532
    :cond_0
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 1194533
    div-int/lit8 v3, p0, 0x2

    .line 1194534
    div-int/lit8 v4, p1, 0x2

    .line 1194535
    sget-object v1, Lcom/facebook/video/engine/VideoDataSource;->a:Landroid/graphics/RectF;

    invoke-virtual {v1, p3}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1194536
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float v1, v0, v1

    .line 1194537
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v5

    div-float/2addr v0, v5

    .line 1194538
    :goto_0
    sget-object v5, LX/2oF;->MIRROR_HORIZONTALLY:LX/2oF;

    if-ne p2, v5, :cond_1

    .line 1194539
    const/high16 v5, -0x40800000    # -1.0f

    mul-float/2addr v1, v5

    .line 1194540
    :cond_1
    int-to-float v5, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    .line 1194541
    int-to-float v6, p0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v7

    mul-float/2addr v6, v7

    float-to-int v6, v6

    .line 1194542
    sub-int v7, v5, p1

    div-int/lit8 v7, v7, 0x2

    .line 1194543
    sub-int v8, v6, p0

    div-int/lit8 v8, v8, 0x2

    .line 1194544
    iget v9, p3, Landroid/graphics/RectF;->top:F

    int-to-float v5, v5

    mul-float/2addr v5, v9

    float-to-int v5, v5

    .line 1194545
    iget v9, p3, Landroid/graphics/RectF;->left:F

    int-to-float v6, v6

    mul-float/2addr v6, v9

    float-to-int v6, v6

    .line 1194546
    int-to-float v3, v3

    int-to-float v4, v4

    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/graphics/Matrix;->preScale(FFFF)Z

    .line 1194547
    sub-int v0, v8, v6

    int-to-float v0, v0

    sub-int v1, v7, v5

    int-to-float v1, v1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    move-object v0, v2

    .line 1194548
    :goto_1
    return-object v0

    :cond_2
    move v1, v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public static a(III)Landroid/graphics/RectF;
    .locals 7

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1194549
    rem-int/lit16 v0, p0, 0xb4

    if-eqz v0, :cond_1

    .line 1194550
    :goto_0
    int-to-float v0, p2

    .line 1194551
    int-to-float v2, p1

    .line 1194552
    int-to-float v3, p1

    cmpg-float v3, v0, v3

    if-gez v3, :cond_0

    .line 1194553
    int-to-float v2, p1

    sub-float v0, v2, v0

    div-float/2addr v0, v5

    .line 1194554
    int-to-float v2, p1

    div-float/2addr v0, v2

    .line 1194555
    :goto_1
    new-instance v2, Landroid/graphics/RectF;

    sub-float v3, v4, v1

    sub-float/2addr v4, v0

    invoke-direct {v2, v1, v0, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v2

    .line 1194556
    :cond_0
    int-to-float v0, p2

    sub-float/2addr v0, v2

    div-float/2addr v0, v5

    .line 1194557
    int-to-float v2, p2

    div-float/2addr v0, v2

    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_1

    :cond_1
    move v6, p2

    move p2, p1

    move p1, v6

    goto :goto_0
.end method

.method public static b(III)D
    .locals 4

    .prologue
    .line 1194558
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-double v0, v0

    .line 1194559
    int-to-double v2, p0

    div-double v0, v2, v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    return-wide v0
.end method
