.class public LX/8E2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/8E2;


# instance fields
.field private final a:LX/0TD;

.field private final b:LX/0TD;

.field private final c:[Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/Set",
            "<",
            "LX/8E6;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0TD;LX/0TD;)V
    .locals 3
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1313398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1313399
    iput-object p1, p0, LX/8E2;->a:LX/0TD;

    .line 1313400
    iput-object p2, p0, LX/8E2;->b:LX/0TD;

    .line 1313401
    invoke-static {}, LX/8E5;->values()[LX/8E5;

    move-result-object v0

    array-length v0, v0

    iput v0, p0, LX/8E2;->d:I

    .line 1313402
    iget v0, p0, LX/8E2;->d:I

    new-array v0, v0, [Ljava/util/Set;

    iput-object v0, p0, LX/8E2;->c:[Ljava/util/Set;

    .line 1313403
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, LX/8E2;->d:I

    if-ge v0, v1, :cond_0

    .line 1313404
    iget-object v1, p0, LX/8E2;->c:[Ljava/util/Set;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    aput-object v2, v1, v0

    .line 1313405
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1313406
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8E2;->e:Ljava/util/Map;

    .line 1313407
    return-void
.end method

.method public static a(LX/0QB;)LX/8E2;
    .locals 5

    .prologue
    .line 1313385
    sget-object v0, LX/8E2;->f:LX/8E2;

    if-nez v0, :cond_1

    .line 1313386
    const-class v1, LX/8E2;

    monitor-enter v1

    .line 1313387
    :try_start_0
    sget-object v0, LX/8E2;->f:LX/8E2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1313388
    if-eqz v2, :cond_0

    .line 1313389
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1313390
    new-instance p0, LX/8E2;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-direct {p0, v3, v4}, LX/8E2;-><init>(LX/0TD;LX/0TD;)V

    .line 1313391
    move-object v0, p0

    .line 1313392
    sput-object v0, LX/8E2;->f:LX/8E2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1313393
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1313394
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1313395
    :cond_1
    sget-object v0, LX/8E2;->f:LX/8E2;

    return-object v0

    .line 1313396
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1313397
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/8E2;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1313374
    monitor-enter p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget v0, p0, LX/8E2;->d:I

    if-ge v1, v0, :cond_2

    .line 1313375
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1313376
    iget-object v0, p0, LX/8E2;->c:[Ljava/util/Set;

    aget-object v0, v0, v1

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8E6;

    .line 1313377
    iget-object v4, v0, LX/8E6;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1313378
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1313379
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1313380
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1313381
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/8E2;->c:[Ljava/util/Set;

    aget-object v0, v0, v1

    invoke-interface {v0, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 1313382
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1313383
    :cond_2
    iget-object v0, p0, LX/8E2;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1313384
    monitor-exit p0

    return-void
.end method

.method public static declared-synchronized a$redex0(LX/8E2;Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 1313350
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8E2;->e:Ljava/util/Map;

    invoke-direct {p0, p1}, LX/8E2;->b(Ljava/lang/String;)J

    move-result-wide v2

    or-long/2addr v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1313351
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, LX/8E2;->d:I

    if-ge v1, v0, :cond_2

    .line 1313352
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1313353
    iget-object v0, p0, LX/8E2;->c:[Ljava/util/Set;

    aget-object v0, v0, v1

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8E6;

    .line 1313354
    iget-object v4, v0, LX/8E6;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1313355
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1313356
    iget-object v4, v0, LX/8E6;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1313357
    invoke-direct {p0, v4}, LX/8E2;->b(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, LX/8E6;->a(J)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1313358
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1313359
    invoke-direct {p0, v0}, LX/8E2;->b(LX/8E6;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1313360
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1313361
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/8E2;->c:[Ljava/util/Set;

    aget-object v0, v0, v1

    invoke-interface {v0, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1313362
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1313363
    :cond_2
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized b(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 1313408
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8E2;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/8E2;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(LX/8E6;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/8E6",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1313364
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8E2;->a:LX/0TD;

    iget-object v1, p0, LX/8E2;->b:LX/0TD;

    .line 1313365
    iget-boolean v2, p1, LX/8E6;->f:Z

    if-eqz v2, :cond_0

    iget-object v2, p1, LX/8E6;->e:Ljava/util/concurrent/Callable;

    invoke-interface {v0, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v3, v2

    .line 1313366
    :goto_0
    iget-object v2, p1, LX/8E6;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8E4;

    .line 1313367
    iget-object v5, v2, LX/8E4;->a:LX/0TF;

    iget-boolean v2, v2, LX/8E4;->b:Z

    if-eqz v2, :cond_1

    move-object v2, v0

    :goto_2
    invoke-static {v3, v5, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 1313368
    :cond_0
    iget-object v2, p1, LX/8E6;->e:Ljava/util/concurrent/Callable;

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    :cond_1
    move-object v2, v1

    .line 1313369
    goto :goto_2

    .line 1313370
    :cond_2
    move-object v0, v3

    .line 1313371
    new-instance v1, LX/8E1;

    invoke-direct {v1, p0, p1}, LX/8E1;-><init>(LX/8E2;LX/8E6;)V

    iget-object v2, p0, LX/8E2;->a:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1313372
    monitor-exit p0

    return-void

    .line 1313373
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/8E6;)V
    .locals 2

    .prologue
    .line 1313340
    monitor-enter p0

    if-nez p1, :cond_0

    .line 1313341
    :goto_0
    monitor-exit p0

    return-void

    .line 1313342
    :cond_0
    :try_start_0
    iget-object v0, p1, LX/8E6;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1313343
    invoke-direct {p0, v0}, LX/8E2;->b(Ljava/lang/String;)J

    move-result-wide v0

    .line 1313344
    invoke-virtual {p1, v0, v1}, LX/8E6;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1313345
    invoke-direct {p0, p1}, LX/8E2;->b(LX/8E6;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1313346
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1313347
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/8E2;->c:[Ljava/util/Set;

    .line 1313348
    iget-object v1, p1, LX/8E6;->a:LX/8E5;

    move-object v1, v1

    .line 1313349
    invoke-virtual {v1}, LX/8E5;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;LX/0Rf;)V
    .locals 2
    .param p2    # LX/0Rf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Rf",
            "<",
            "LX/8E7;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1313335
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/0Rf;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 1313336
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1313337
    :cond_1
    :try_start_1
    invoke-static {p2}, LX/8E6;->a(LX/0Rf;)J

    move-result-wide v0

    .line 1313338
    invoke-static {p0, p1, v0, v1}, LX/8E2;->a$redex0(LX/8E2;Ljava/lang/String;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1313339
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;LX/0Rf;)V
    .locals 3
    .param p2    # LX/0Rf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Rf",
            "<",
            "LX/8E7;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1313325
    monitor-enter p0

    if-eqz p2, :cond_0

    :try_start_0
    invoke-virtual {p2}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1313326
    :cond_0
    invoke-static {p0, p1}, LX/8E2;->a$redex0(LX/8E2;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1313327
    :goto_0
    monitor-exit p0

    return-void

    .line 1313328
    :cond_1
    :try_start_1
    new-instance v0, LX/8E3;

    invoke-direct {v0}, LX/8E3;-><init>()V

    new-instance v1, LX/8E0;

    invoke-direct {v1, p0, p1}, LX/8E0;-><init>(LX/8E2;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/8E3;->a(Ljava/util/concurrent/Callable;Z)LX/8E3;

    move-result-object v0

    sget-object v1, LX/8E5;->CAN_WAIT:LX/8E5;

    .line 1313329
    iput-object v1, v0, LX/8E3;->a:LX/8E5;

    .line 1313330
    move-object v0, v0

    .line 1313331
    iput-object p1, v0, LX/8E3;->b:Ljava/lang/String;

    .line 1313332
    move-object v0, v0

    .line 1313333
    invoke-virtual {v0, p2}, LX/8E3;->a(LX/0Rf;)LX/8E3;

    move-result-object v0

    invoke-virtual {v0}, LX/8E3;->a()LX/8E6;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/8E2;->a(LX/8E6;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1313334
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
