.class public LX/7Hj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/7HW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7HW",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7HW;)V
    .locals 0
    .param p1    # LX/7HW;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1191090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1191091
    iput-object p1, p0, LX/7Hj;->a:LX/7HW;

    .line 1191092
    return-void
.end method


# virtual methods
.method public final a(LX/7Hc;LX/7Hc;LX/7HY;)LX/7Hc;
    .locals 9
    .param p1    # LX/7Hc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/7Hc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hc",
            "<TT;>;",
            "LX/7Hc",
            "<TT;>;",
            "LX/7HY;",
            ")",
            "LX/7Hc",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 1191093
    if-eqz p1, :cond_0

    .line 1191094
    iget-object v0, p1, LX/7Hc;->b:LX/0Px;

    move-object v0, v0

    .line 1191095
    move-object v1, v0

    .line 1191096
    :goto_0
    if-eqz p2, :cond_1

    .line 1191097
    iget-object v0, p2, LX/7Hc;->b:LX/0Px;

    move-object v0, v0

    .line 1191098
    :goto_1
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    add-int/2addr v4, v5

    .line 1191099
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1191100
    invoke-interface {v5, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move v1, v2

    .line 1191101
    :goto_2
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 1191102
    iget-object v4, p2, LX/7Hc;->b:LX/0Px;

    move-object v4, v4

    .line 1191103
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 1191104
    invoke-interface {v5, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v6

    .line 1191105
    if-ne v6, v3, :cond_2

    .line 1191106
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1191107
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1191108
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1191109
    move-object v1, v0

    goto :goto_0

    .line 1191110
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1191111
    goto :goto_1

    .line 1191112
    :cond_2
    iget-object v7, p0, LX/7Hj;->a:LX/7HW;

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v7, v8, v4, p3}, LX/7HW;->a(Ljava/lang/Object;Ljava/lang/Object;LX/7HY;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v5, v6, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 1191113
    :cond_3
    new-instance v1, LX/7Hc;

    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    if-eqz p2, :cond_4

    .line 1191114
    iget v0, p2, LX/7Hc;->c:I

    move v2, v0

    .line 1191115
    :cond_4
    if-eqz p2, :cond_5

    .line 1191116
    iget v0, p2, LX/7Hc;->d:I

    move v0, v0

    .line 1191117
    :goto_4
    invoke-direct {v1, v4, v2, v0}, LX/7Hc;-><init>(LX/0Px;II)V

    return-object v1

    :cond_5
    move v0, v3

    goto :goto_4
.end method
