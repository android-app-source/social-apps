.class public LX/6jt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final messageLiveLocations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6kl;",
            ">;"
        }
    .end annotation
.end field

.field public final threadKey:LX/6l9;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1133487
    new-instance v0, LX/1sv;

    const-string v1, "DeltaLiveLocationData"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jt;->b:LX/1sv;

    .line 1133488
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jt;->c:LX/1sw;

    .line 1133489
    new-instance v0, LX/1sw;

    const-string v1, "messageLiveLocations"

    const/16 v2, 0xf

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jt;->d:LX/1sw;

    .line 1133490
    sput-boolean v4, LX/6jt;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6l9;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6l9;",
            "Ljava/util/List",
            "<",
            "LX/6kl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1133483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1133484
    iput-object p1, p0, LX/6jt;->threadKey:LX/6l9;

    .line 1133485
    iput-object p2, p0, LX/6jt;->messageLiveLocations:Ljava/util/List;

    .line 1133486
    return-void
.end method

.method public static a(LX/6jt;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1133478
    iget-object v0, p0, LX/6jt;->threadKey:LX/6l9;

    if-nez v0, :cond_0

    .line 1133479
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'threadKey\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jt;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1133480
    :cond_0
    iget-object v0, p0, LX/6jt;->messageLiveLocations:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1133481
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'messageLiveLocations\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jt;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1133482
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1133411
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1133412
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1133413
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1133414
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaLiveLocationData"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1133415
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133416
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133417
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133418
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133419
    const-string v4, "threadKey"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133420
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133421
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133422
    iget-object v4, p0, LX/6jt;->threadKey:LX/6l9;

    if-nez v4, :cond_3

    .line 1133423
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133424
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133425
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133426
    const-string v4, "messageLiveLocations"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133427
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133428
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133429
    iget-object v0, p0, LX/6jt;->messageLiveLocations:Ljava/util/List;

    if-nez v0, :cond_4

    .line 1133430
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133431
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133432
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133433
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1133434
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1133435
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1133436
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1133437
    :cond_3
    iget-object v4, p0, LX/6jt;->threadKey:LX/6l9;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1133438
    :cond_4
    iget-object v0, p0, LX/6jt;->messageLiveLocations:Ljava/util/List;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1133465
    invoke-static {p0}, LX/6jt;->a(LX/6jt;)V

    .line 1133466
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1133467
    iget-object v0, p0, LX/6jt;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1133468
    sget-object v0, LX/6jt;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133469
    iget-object v0, p0, LX/6jt;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1133470
    :cond_0
    iget-object v0, p0, LX/6jt;->messageLiveLocations:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1133471
    sget-object v0, LX/6jt;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133472
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/6jt;->messageLiveLocations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1133473
    iget-object v0, p0, LX/6jt;->messageLiveLocations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kl;

    .line 1133474
    invoke-virtual {v0, p1}, LX/6kl;->a(LX/1su;)V

    goto :goto_0

    .line 1133475
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1133476
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1133477
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1133443
    if-nez p1, :cond_1

    .line 1133444
    :cond_0
    :goto_0
    return v0

    .line 1133445
    :cond_1
    instance-of v1, p1, LX/6jt;

    if-eqz v1, :cond_0

    .line 1133446
    check-cast p1, LX/6jt;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1133447
    if-nez p1, :cond_3

    .line 1133448
    :cond_2
    :goto_1
    move v0, v2

    .line 1133449
    goto :goto_0

    .line 1133450
    :cond_3
    iget-object v0, p0, LX/6jt;->threadKey:LX/6l9;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1133451
    :goto_2
    iget-object v3, p1, LX/6jt;->threadKey:LX/6l9;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1133452
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1133453
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133454
    iget-object v0, p0, LX/6jt;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6jt;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133455
    :cond_5
    iget-object v0, p0, LX/6jt;->messageLiveLocations:Ljava/util/List;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1133456
    :goto_4
    iget-object v3, p1, LX/6jt;->messageLiveLocations:Ljava/util/List;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1133457
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1133458
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133459
    iget-object v0, p0, LX/6jt;->messageLiveLocations:Ljava/util/List;

    iget-object v3, p1, LX/6jt;->messageLiveLocations:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1133460
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1133461
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1133462
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1133463
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1133464
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1133442
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1133439
    sget-boolean v0, LX/6jt;->a:Z

    .line 1133440
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jt;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1133441
    return-object v0
.end method
