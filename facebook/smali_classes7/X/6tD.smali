.class public LX/6tD;
.super LX/6E8;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E8",
        "<",
        "Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;",
        "LX/6tC;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private l:LX/6qh;

.field private m:LX/6tC;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;)V
    .locals 0

    .prologue
    .line 1154095
    invoke-direct {p0, p1}, LX/6E8;-><init>(LX/6E6;)V

    .line 1154096
    return-void
.end method


# virtual methods
.method public final a(LX/6E2;)V
    .locals 2

    .prologue
    .line 1154097
    check-cast p1, LX/6tC;

    .line 1154098
    iget-object v0, p0, LX/6tD;->l:LX/6qh;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1154099
    iput-object p1, p0, LX/6tD;->m:LX/6tC;

    .line 1154100
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;

    .line 1154101
    iget-object v1, p0, LX/6tD;->l:LX/6qh;

    invoke-virtual {v0, v1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1154102
    iget-object v1, p1, LX/6tC;->b:LX/73O;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->setViewParams(LX/73O;)V

    .line 1154103
    invoke-virtual {v0, p0}, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1154104
    iget-object v1, v0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    move-object v0, v1

    .line 1154105
    invoke-virtual {v0, p0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1154106
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1154107
    iput-object p1, p0, LX/6tD;->l:LX/6qh;

    .line 1154108
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0xc237594

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1154109
    iget-object v1, p0, LX/6tD;->m:LX/6tC;

    iget-object v1, v1, LX/6tC;->c:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 1154110
    iget-object v1, p0, LX/6tD;->l:LX/6qh;

    iget-object v2, p0, LX/6tD;->m:LX/6tC;

    iget-object v2, v2, LX/6tC;->c:Landroid/content/Intent;

    iget-object v3, p0, LX/6tD;->m:LX/6tC;

    iget v3, v3, LX/6tC;->d:I

    invoke-virtual {v1, v2, v3}, LX/6qh;->a(Landroid/content/Intent;I)V

    .line 1154111
    :cond_0
    const v1, -0x6d903cc3

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
