.class public final LX/7Vk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V
    .locals 0

    .prologue
    .line 1215225
    iput-object p1, p0, LX/7Vk;->a:Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;)V
    .locals 1

    .prologue
    .line 1215221
    iget-object v0, p0, LX/7Vk;->a:Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;

    .line 1215222
    iput-object p1, v0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    .line 1215223
    iget-object v0, p0, LX/7Vk;->a:Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;

    invoke-static {v0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->m(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V

    .line 1215224
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1215226
    iget-object v0, p0, LX/7Vk;->a:Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;

    invoke-virtual {v0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->finish()V

    .line 1215227
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1215220
    check-cast p1, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-direct {p0, p1}, LX/7Vk;->a(Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;)V

    return-void
.end method
