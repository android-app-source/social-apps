.class public LX/6wY;
.super LX/6sV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sV",
        "<",
        "Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;",
        "Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1157787
    const-class v0, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;

    invoke-direct {p0, p1, v0}, LX/6sV;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 1157788
    return-void
.end method

.method public static b(LX/0QB;)LX/6wY;
    .locals 2

    .prologue
    .line 1157785
    new-instance v1, LX/6wY;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/6wY;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 1157786
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1157762
    check-cast p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;

    .line 1157763
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1157764
    iget-object v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    if-eqz v0, :cond_0

    .line 1157765
    iget-object v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    check-cast v0, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;

    .line 1157766
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "user_input_email"

    iget-object v0, v0, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1157767
    :cond_0
    iget-object v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    invoke-interface {v0}, Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;->a()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-boolean v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->c:Z

    if-eqz v0, :cond_3

    .line 1157768
    :cond_2
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "default"

    const-string v3, "1"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1157769
    :cond_3
    iget-boolean v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->d:Z

    if-eqz v0, :cond_4

    const-string v0, "DELETE"

    .line 1157770
    :goto_0
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "add_email_contact_info"

    .line 1157771
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1157772
    move-object v2, v2

    .line 1157773
    iput-object v0, v2, LX/14O;->c:Ljava/lang/String;

    .line 1157774
    move-object v0, v2

    .line 1157775
    const-string v2, "%d"

    iget-object v3, p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1157776
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 1157777
    move-object v0, v0

    .line 1157778
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1157779
    move-object v0, v0

    .line 1157780
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 1157781
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1157782
    move-object v0, v0

    .line 1157783
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1157784
    :cond_4
    const-string v0, "POST"

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157761
    const-string v0, "edit_email_contact_info"

    return-object v0
.end method
