.class public final LX/8M8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/7mo;

.field public final synthetic b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7mo;)V
    .locals 0

    .prologue
    .line 1334298
    iput-object p1, p0, LX/8M8;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iput-object p2, p0, LX/8M8;->a:LX/7mo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x2

    const v1, 0x6d8a8f17

    invoke-static {v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1334299
    iget-object v0, p0, LX/8M8;->a:LX/7mo;

    .line 1334300
    iget-object v2, v0, LX/7mo;->a:LX/7mm;

    sget-object v4, LX/7mm;->POST:LX/7mm;

    if-ne v2, v4, :cond_6

    invoke-virtual {v0}, LX/7mo;->c()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/7mo;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 1334301
    if-eqz v0, :cond_4

    .line 1334302
    iget-object v0, p0, LX/8M8;->a:LX/7mo;

    invoke-virtual {v0}, LX/7mo;->c()Ljava/lang/String;

    move-result-object v2

    .line 1334303
    iget-object v0, p0, LX/8M8;->a:LX/7mo;

    .line 1334304
    iget-object v3, v0, LX/7mo;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1334305
    if-eqz v2, :cond_2

    .line 1334306
    iget-object v0, p0, LX/8M8;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->r:LX/0hy;

    invoke-interface {v0, v2}, LX/0hy;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1334307
    iget-object v3, p0, LX/8M8;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v3, v3, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    .line 1334308
    iget-object v4, v3, LX/1RW;->a:LX/0Zb;

    const-string v5, "open_permalink"

    invoke-static {v3, v5}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "story_id"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1334309
    :goto_1
    iget-object v2, p0, LX/8M8;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v2, v2, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1334310
    :cond_1
    :goto_2
    const v0, 0x221f1ba2

    invoke-static {v0, v1}, LX/02F;->a(II)V

    :goto_3
    return-void

    .line 1334311
    :cond_2
    if-eqz v3, :cond_3

    .line 1334312
    iget-object v0, p0, LX/8M8;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->r:LX/0hy;

    new-instance v2, Lcom/facebook/ipc/feed/PermalinkStoryFbIdParams;

    invoke-direct {v2, v3}, Lcom/facebook/ipc/feed/PermalinkStoryFbIdParams;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v2}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryFbIdParams;)Landroid/content/Intent;

    move-result-object v0

    .line 1334313
    iget-object v2, p0, LX/8M8;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v2, v2, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    .line 1334314
    iget-object v4, v2, LX/1RW;->a:LX/0Zb;

    const-string v5, "open_permalink"

    invoke-static {v2, v5}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "story_fbid"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1334315
    goto :goto_1

    .line 1334316
    :cond_3
    iget-object v0, p0, LX/8M8;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->z:LX/03V;

    const-string v2, "compost_click_permalink_story"

    const-string v3, "both story ID and fbid were null"

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1334317
    const v0, 0x2458eb46

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_3

    .line 1334318
    :cond_4
    iget-object v0, p0, LX/8M8;->a:LX/7mo;

    .line 1334319
    iget-object v2, v0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v2

    .line 1334320
    invoke-static {v0}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1334321
    iget-object v0, p0, LX/8M8;->a:LX/7mo;

    .line 1334322
    iget-object v2, v0, LX/7mo;->a:LX/7mm;

    sget-object v4, LX/7mm;->COVER_PHOTO:LX/7mm;

    if-eq v2, v4, :cond_5

    iget-object v2, v0, LX/7mo;->a:LX/7mm;

    sget-object v4, LX/7mm;->PROFILE_PIC:LX/7mm;

    if-ne v2, v4, :cond_7

    :cond_5
    const/4 v2, 0x1

    :goto_4
    move v0, v2

    .line 1334323
    if-eqz v0, :cond_1

    .line 1334324
    iget-object v0, p0, LX/8M8;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1334325
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v2

    .line 1334326
    iget-object v2, p0, LX/8M8;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v2, v2, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    .line 1334327
    iget-object v4, v2, LX/1RW;->a:LX/0Zb;

    const-string v5, "open_profile"

    invoke-static {v2, v5}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "user_id"

    invoke-virtual {v5, v6, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1334328
    sget-object v2, LX/0ax;->bE:Ljava/lang/String;

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1334329
    iget-object v0, p0, LX/8M8;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v3, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1334330
    if-eqz v0, :cond_1

    .line 1334331
    iget-object v2, p0, LX/8M8;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v2, v2, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_2

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_7
    const/4 v2, 0x0

    goto :goto_4
.end method
