.class public final enum LX/7iN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7iN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7iN;

.field public static final enum COMMERCE_DETAILS_PAGE:LX/7iN;

.field public static final enum COMMERCE_NEWS_FEED:LX/7iN;

.field public static final enum COMMERCE_STOREFRONT:LX/7iN;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1227437
    new-instance v0, LX/7iN;

    const-string v1, "COMMERCE_DETAILS_PAGE"

    const-string v2, "commerce_details_page"

    invoke-direct {v0, v1, v3, v2}, LX/7iN;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iN;->COMMERCE_DETAILS_PAGE:LX/7iN;

    .line 1227438
    new-instance v0, LX/7iN;

    const-string v1, "COMMERCE_STOREFRONT"

    const-string v2, "commerce_store_front"

    invoke-direct {v0, v1, v4, v2}, LX/7iN;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iN;->COMMERCE_STOREFRONT:LX/7iN;

    .line 1227439
    new-instance v0, LX/7iN;

    const-string v1, "COMMERCE_NEWS_FEED"

    const-string v2, "commerce_news_feed"

    invoke-direct {v0, v1, v5, v2}, LX/7iN;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iN;->COMMERCE_NEWS_FEED:LX/7iN;

    .line 1227440
    const/4 v0, 0x3

    new-array v0, v0, [LX/7iN;

    sget-object v1, LX/7iN;->COMMERCE_DETAILS_PAGE:LX/7iN;

    aput-object v1, v0, v3

    sget-object v1, LX/7iN;->COMMERCE_STOREFRONT:LX/7iN;

    aput-object v1, v0, v4

    sget-object v1, LX/7iN;->COMMERCE_NEWS_FEED:LX/7iN;

    aput-object v1, v0, v5

    sput-object v0, LX/7iN;->$VALUES:[LX/7iN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1227434
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1227435
    iput-object p3, p0, LX/7iN;->value:Ljava/lang/String;

    .line 1227436
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7iN;
    .locals 1

    .prologue
    .line 1227432
    const-class v0, LX/7iN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7iN;

    return-object v0
.end method

.method public static values()[LX/7iN;
    .locals 1

    .prologue
    .line 1227433
    sget-object v0, LX/7iN;->$VALUES:[LX/7iN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7iN;

    return-object v0
.end method
