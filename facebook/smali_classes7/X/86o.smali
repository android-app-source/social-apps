.class public final enum LX/86o;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/86o;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/86o;

.field public static final enum CAMERA_ROLL:LX/86o;

.field public static final enum COLOR_PICKER:LX/86o;

.field public static final enum EFFECTS:LX/86o;

.field public static final enum NONE:LX/86o;


# instance fields
.field private final mShouldBeDismissedBySwipeDownAndTapOutside:Z

.field private final mShouldBlockOtherUIComponents:Z

.field private final mShouldCloseOnBackPressed:Z

.field private final mShouldUseTrayBackgroundGradient:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1297810
    new-instance v0, LX/86o;

    const-string v1, "EFFECTS"

    move v4, v3

    move v5, v3

    move v6, v3

    invoke-direct/range {v0 .. v6}, LX/86o;-><init>(Ljava/lang/String;IZZZZ)V

    sput-object v0, LX/86o;->EFFECTS:LX/86o;

    .line 1297811
    new-instance v4, LX/86o;

    const-string v5, "CAMERA_ROLL"

    move v6, v3

    move v7, v3

    move v8, v2

    move v9, v3

    move v10, v3

    invoke-direct/range {v4 .. v10}, LX/86o;-><init>(Ljava/lang/String;IZZZZ)V

    sput-object v4, LX/86o;->CAMERA_ROLL:LX/86o;

    .line 1297812
    new-instance v4, LX/86o;

    const-string v5, "COLOR_PICKER"

    move v6, v11

    move v7, v2

    move v8, v2

    move v9, v3

    move v10, v2

    invoke-direct/range {v4 .. v10}, LX/86o;-><init>(Ljava/lang/String;IZZZZ)V

    sput-object v4, LX/86o;->COLOR_PICKER:LX/86o;

    .line 1297813
    new-instance v4, LX/86o;

    const-string v5, "NONE"

    move v6, v12

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    invoke-direct/range {v4 .. v10}, LX/86o;-><init>(Ljava/lang/String;IZZZZ)V

    sput-object v4, LX/86o;->NONE:LX/86o;

    .line 1297814
    const/4 v0, 0x4

    new-array v0, v0, [LX/86o;

    sget-object v1, LX/86o;->EFFECTS:LX/86o;

    aput-object v1, v0, v2

    sget-object v1, LX/86o;->CAMERA_ROLL:LX/86o;

    aput-object v1, v0, v3

    sget-object v1, LX/86o;->COLOR_PICKER:LX/86o;

    aput-object v1, v0, v11

    sget-object v1, LX/86o;->NONE:LX/86o;

    aput-object v1, v0, v12

    sput-object v0, LX/86o;->$VALUES:[LX/86o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZZ)V"
        }
    .end annotation

    .prologue
    .line 1297804
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1297805
    iput-boolean p3, p0, LX/86o;->mShouldBeDismissedBySwipeDownAndTapOutside:Z

    .line 1297806
    iput-boolean p4, p0, LX/86o;->mShouldUseTrayBackgroundGradient:Z

    .line 1297807
    iput-boolean p5, p0, LX/86o;->mShouldBlockOtherUIComponents:Z

    .line 1297808
    iput-boolean p6, p0, LX/86o;->mShouldCloseOnBackPressed:Z

    .line 1297809
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/86o;
    .locals 1

    .prologue
    .line 1297803
    const-class v0, LX/86o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/86o;

    return-object v0
.end method

.method public static values()[LX/86o;
    .locals 1

    .prologue
    .line 1297815
    sget-object v0, LX/86o;->$VALUES:[LX/86o;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/86o;

    return-object v0
.end method


# virtual methods
.method public final shouldBeDismissedBySwipeDownAndTapOutside()Z
    .locals 1

    .prologue
    .line 1297799
    iget-boolean v0, p0, LX/86o;->mShouldBeDismissedBySwipeDownAndTapOutside:Z

    return v0
.end method

.method public final shouldBlockOtherUIComponents()Z
    .locals 1

    .prologue
    .line 1297800
    iget-boolean v0, p0, LX/86o;->mShouldBlockOtherUIComponents:Z

    return v0
.end method

.method public final shouldCloseOnBackPressed()Z
    .locals 1

    .prologue
    .line 1297801
    iget-boolean v0, p0, LX/86o;->mShouldCloseOnBackPressed:Z

    return v0
.end method

.method public final shouldUseTrayBackgroundGradient()Z
    .locals 1

    .prologue
    .line 1297802
    iget-boolean v0, p0, LX/86o;->mShouldUseTrayBackgroundGradient:Z

    return v0
.end method
