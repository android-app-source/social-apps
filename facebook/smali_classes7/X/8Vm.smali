.class public LX/8Vm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/8Uw;

.field private final b:LX/8TD;

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8T9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/8Uw;LX/8TD;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1353311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1353312
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1353313
    iput-object v0, p0, LX/8Vm;->c:LX/0Ot;

    .line 1353314
    iput-object p1, p0, LX/8Vm;->a:LX/8Uw;

    .line 1353315
    iput-object p2, p0, LX/8Vm;->b:LX/8TD;

    .line 1353316
    return-void
.end method

.method public static a$redex0(LX/8Vm;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)LX/8Tp;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "LX/8Tp;"
        }
    .end annotation

    .prologue
    .line 1353309
    iget-object v7, p0, LX/8Vm;->a:LX/8Uw;

    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    new-instance v0, LX/8Vi;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/8Vi;-><init>(LX/8Vm;Ljava/util/List;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v7

    move-object v2, p2

    move v3, v8

    move-object v4, p4

    move-object v5, p5

    move-object v6, v0

    move v7, p6

    invoke-virtual/range {v1 .. v7}, LX/8Uw;->a(Ljava/lang/String;ILjava/lang/String;Ljava/util/List;LX/8Uv;Z)V

    .line 1353310
    sget-object v0, LX/8Tp;->MN_SHARING_STARTED:LX/8Tp;

    return-object v0
.end method

.method public static a$redex0(LX/8Vm;Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Z)LX/8Tp;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "LX/8Tp;"
        }
    .end annotation

    .prologue
    .line 1353341
    iget-object v0, p0, LX/8Vm;->a:LX/8Uw;

    new-instance v1, LX/8Vk;

    invoke-direct {v1, p0, p3, p1, p2}, LX/8Vk;-><init>(LX/8Vm;Ljava/util/List;Landroid/content/Context;Ljava/lang/String;)V

    .line 1353342
    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1353343
    :cond_0
    :goto_0
    sget-object v0, LX/8Tp;->MN_SHARING_STARTED:LX/8Tp;

    return-object v0

    .line 1353344
    :cond_1
    new-instance v2, LX/4GX;

    invoke-direct {v2}, LX/4GX;-><init>()V

    .line 1353345
    const-string v3, "game_id"

    invoke-virtual {v2, v3, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1353346
    move-object v2, v2

    .line 1353347
    const-string v3, "thread_id"

    invoke-virtual {v2, v3, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1353348
    move-object v2, v2

    .line 1353349
    new-instance v3, LX/8Ud;

    invoke-direct {v3}, LX/8Ud;-><init>()V

    move-object v3, v3

    .line 1353350
    const-string p0, "input"

    invoke-virtual {v3, p0, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1353351
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 1353352
    iget-object v3, v0, LX/8Uw;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1353353
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1353354
    sget-object p0, LX/8TE;->GAME_ID:LX/8TE;

    iget-object p0, p0, LX/8TE;->value:Ljava/lang/String;

    invoke-interface {v3, p0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1353355
    sget-object p0, LX/8TE;->EXTRAS_THREAD_IDS:LX/8TE;

    iget-object p0, p0, LX/8TE;->value:Ljava/lang/String;

    invoke-static {p3}, LX/8Uw;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v3, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1353356
    sget-object p0, LX/8TE;->EXTRAS_IS_RETRY:LX/8TE;

    iget-object p0, p0, LX/8TE;->value:Ljava/lang/String;

    invoke-static {p4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v3, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1353357
    new-instance p0, LX/8Us;

    invoke-direct {p0, v0, v3, v1}, LX/8Us;-><init>(LX/8Uw;Ljava/util/Map;LX/8Uv;)V

    iget-object v3, v0, LX/8Uw;->d:Ljava/util/concurrent/Executor;

    invoke-static {v2, p0, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/8Vm;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1353335
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 1353336
    check-cast p1, Landroid/app/Activity;

    new-instance v0, Lcom/facebook/quicksilver/sharing/GameShareMutator$3;

    invoke-direct {v0, p0}, Lcom/facebook/quicksilver/sharing/GameShareMutator$3;-><init>(LX/8Vm;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1353337
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/8Vm;Ljava/util/List;LX/8TI;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/8TI;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1353338
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1353339
    iget-object v2, p0, LX/8Vm;->b:LX/8TD;

    sget-object v3, LX/8TE;->MESSENGER:LX/8TE;

    invoke-virtual {v2, p2, v3, v0}, LX/8TD;->a(LX/8TI;LX/8TE;Ljava/lang/String;)V

    goto :goto_0

    .line 1353340
    :cond_0
    return-void
.end method

.method public static b(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1353333
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f082363

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f08237a

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080043

    invoke-virtual {v0, v1, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080017

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0ju;->c(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1353334
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;Landroid/app/Activity;)LX/8Tp;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1353317
    if-nez p1, :cond_0

    .line 1353318
    sget-object v0, LX/8Tp;->ACTIVITY_RETURN_UNSUCCESSFUL:LX/8Tp;

    .line 1353319
    :goto_0
    return-object v0

    .line 1353320
    :cond_0
    const-string v0, "destination_thread_ids"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 1353321
    const-string v0, "mn_games_share_extras"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;

    .line 1353322
    invoke-static {v5}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_1

    if-nez v0, :cond_2

    .line 1353323
    :cond_1
    sget-object v0, LX/8Tp;->SOMETHING_WENT_WRONG:LX/8Tp;

    goto :goto_0

    .line 1353324
    :cond_2
    sget-object v1, LX/8Vl;->a:[I

    invoke-virtual {v0}, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->a()LX/8TI;

    move-result-object v2

    invoke-virtual {v2}, LX/8TI;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1353325
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled share type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->a()LX/8TI;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1353326
    :pswitch_0
    iget-object v1, v0, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1353327
    invoke-static {p0, p2, v0, v5, v6}, LX/8Vm;->a$redex0(LX/8Vm;Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Z)LX/8Tp;

    move-result-object v0

    goto :goto_0

    .line 1353328
    :pswitch_1
    check-cast v0, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;

    .line 1353329
    iget-object v1, v0, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->a:Ljava/lang/String;

    move-object v2, v1

    .line 1353330
    iget-object v1, v0, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;->a:Ljava/lang/String;

    move-object v3, v1

    .line 1353331
    iget-object v1, v0, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;->b:Ljava/lang/String;

    move-object v4, v1

    .line 1353332
    move-object v0, p0

    move-object v1, p2

    invoke-static/range {v0 .. v6}, LX/8Vm;->a$redex0(LX/8Vm;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)LX/8Tp;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
