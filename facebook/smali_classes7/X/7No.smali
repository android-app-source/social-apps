.class public final LX/7No;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/Video360Plugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/Video360Plugin;)V
    .locals 0

    .prologue
    .line 1200674
    iput-object p1, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1200675
    const/4 v2, 0x0

    .line 1200676
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1200677
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->z:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    invoke-virtual {v0}, LX/2qW;->getPitch()F

    move-result v0

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    invoke-virtual {v0}, LX/2qW;->getYaw()F

    move-result v0

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1

    .line 1200678
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v1}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v1

    invoke-virtual {v1}, LX/2qW;->getPitch()F

    move-result v1

    .line 1200679
    iput v1, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->w:F

    .line 1200680
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v1}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v1

    invoke-virtual {v1}, LX/2qW;->getYaw()F

    move-result v1

    .line 1200681
    iput v1, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->x:F

    .line 1200682
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v1}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v1

    invoke-virtual {v1}, LX/2qW;->getFov()F

    move-result v1

    .line 1200683
    iput v1, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->y:F

    .line 1200684
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    .line 1200685
    iput v2, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->z:F

    .line 1200686
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 1200687
    :cond_1
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    invoke-virtual {v0}, LX/2qW;->getPitch()F

    move-result v0

    iget-object v1, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->w:F

    sub-float/2addr v0, v1

    .line 1200688
    iget-object v1, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v1}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v1

    invoke-virtual {v1}, LX/2qW;->getYaw()F

    move-result v1

    iget-object v2, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v2, v2, Lcom/facebook/video/player/plugins/Video360Plugin;->x:F

    invoke-static {v1, v2}, LX/7Cq;->a(FF)F

    move-result v1

    .line 1200689
    iget-object v2, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1200690
    iput v0, v2, Lcom/facebook/video/player/plugins/Video360Plugin;->z:F

    .line 1200691
    goto :goto_0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1200692
    check-cast p1, Ljava/lang/Void;

    .line 1200693
    invoke-super {p0, p1}, LX/3nE;->onPostExecute(Ljava/lang/Object;)V

    .line 1200694
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_2

    .line 1200695
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->z:F

    const/high16 v1, 0x41200000    # 10.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1200696
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v1}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v1

    invoke-virtual {v1}, LX/2qW;->getPitch()F

    move-result v1

    .line 1200697
    iput v1, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->w:F

    .line 1200698
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v1}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v1

    invoke-virtual {v1}, LX/2qW;->getYaw()F

    move-result v1

    .line 1200699
    iput v1, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->x:F

    .line 1200700
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    invoke-virtual {v0}, LX/2qW;->getRoll()F

    move-result v3

    .line 1200701
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    invoke-virtual {v0}, LX/2qW;->getFov()F

    move-result v4

    .line 1200702
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    invoke-virtual {v0}, LX/2qW;->getRotationMatrix()[F

    move-result-object v5

    .line 1200703
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    .line 1200704
    iget-boolean v1, v0, LX/7E5;->b:Z

    move v0, v1

    .line 1200705
    if-eqz v0, :cond_0

    .line 1200706
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->w:F

    iget-object v2, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v2, v2, Lcom/facebook/video/player/plugins/Video360Plugin;->x:F

    invoke-static {v0, v1, v2}, Lcom/facebook/video/player/plugins/Video360Plugin;->a$redex0(Lcom/facebook/video/player/plugins/Video360Plugin;FF)V

    .line 1200707
    :cond_0
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v7, v0, LX/2oy;->i:LX/2oj;

    new-instance v0, LX/2qh;

    iget-object v1, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->w:F

    iget-object v2, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v2, v2, Lcom/facebook/video/player/plugins/Video360Plugin;->x:F

    iget-object v6, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v6, v6, Lcom/facebook/video/player/plugins/Video360Plugin;->Q:LX/3IP;

    invoke-virtual {v6}, LX/3IP;->b()Z

    move-result v6

    invoke-direct/range {v0 .. v6}, LX/2qh;-><init>(FFFF[FZ)V

    invoke-virtual {v7, v0}, LX/2oj;->a(LX/2ol;)V

    .line 1200708
    :cond_1
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-boolean v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->b:Z

    if-nez v0, :cond_2

    .line 1200709
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    invoke-virtual {v0}, LX/2qW;->getPitch()F

    move-result v0

    iget-object v1, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->v:F

    sub-float/2addr v0, v1

    .line 1200710
    iget-object v1, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v1}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v1

    invoke-virtual {v1}, LX/2qW;->getYaw()F

    move-result v1

    iget-object v2, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v2, v2, Lcom/facebook/video/player/plugins/Video360Plugin;->u:F

    invoke-static {v1, v2}, LX/7Cq;->a(FF)F

    move-result v1

    .line 1200711
    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 1200712
    const-wide/high16 v2, 0x403e000000000000L    # 30.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    .line 1200713
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->b:Z

    .line 1200714
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7M0;

    invoke-direct {v1}, LX/7M0;-><init>()V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1200715
    :cond_2
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-boolean v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->G:Z

    if-eqz v0, :cond_3

    .line 1200716
    iget-object v0, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->B:Landroid/os/Handler;

    iget-object v1, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->L:Ljava/lang/Runnable;

    iget-object v2, p0, LX/7No;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v2, v2, Lcom/facebook/video/player/plugins/Video360Plugin;->t:I

    int-to-long v2, v2

    const v4, -0x36dcd037

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1200717
    :cond_3
    return-void
.end method
