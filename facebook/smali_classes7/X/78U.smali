.class public final enum LX/78U;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/78U;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/78U;

.field public static final enum EXPANDED:LX/78U;

.field public static final enum INTRO_COLLAPSED:LX/78U;

.field public static final enum OUTRO_COLLAPSED:LX/78U;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1172702
    new-instance v0, LX/78U;

    const-string v1, "INTRO_COLLAPSED"

    invoke-direct {v0, v1, v2}, LX/78U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/78U;->INTRO_COLLAPSED:LX/78U;

    .line 1172703
    new-instance v0, LX/78U;

    const-string v1, "EXPANDED"

    invoke-direct {v0, v1, v3}, LX/78U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/78U;->EXPANDED:LX/78U;

    .line 1172704
    new-instance v0, LX/78U;

    const-string v1, "OUTRO_COLLAPSED"

    invoke-direct {v0, v1, v4}, LX/78U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/78U;->OUTRO_COLLAPSED:LX/78U;

    .line 1172705
    const/4 v0, 0x3

    new-array v0, v0, [LX/78U;

    sget-object v1, LX/78U;->INTRO_COLLAPSED:LX/78U;

    aput-object v1, v0, v2

    sget-object v1, LX/78U;->EXPANDED:LX/78U;

    aput-object v1, v0, v3

    sget-object v1, LX/78U;->OUTRO_COLLAPSED:LX/78U;

    aput-object v1, v0, v4

    sput-object v0, LX/78U;->$VALUES:[LX/78U;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1172706
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/78U;
    .locals 1

    .prologue
    .line 1172707
    const-class v0, LX/78U;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/78U;

    return-object v0
.end method

.method public static values()[LX/78U;
    .locals 1

    .prologue
    .line 1172708
    sget-object v0, LX/78U;->$VALUES:[LX/78U;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/78U;

    return-object v0
.end method
