.class public LX/7OL;
.super LX/7Mr;
.source ""


# instance fields
.field public a:LX/ATq;

.field private b:Landroid/view/View;

.field public o:Landroid/view/ViewGroup;

.field public p:Landroid/view/ViewGroup;

.field public q:Lcom/facebook/widget/FbImageView;

.field public r:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1201338
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7OL;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1201339
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1201340
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7OL;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1201341
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1201342
    invoke-direct {p0, p1, p2, p3}, LX/7Mr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1201343
    const v0, 0x7f0d132a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/7OL;->b:Landroid/view/View;

    .line 1201344
    const v0, 0x7f0d3100

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/7OL;->o:Landroid/view/ViewGroup;

    .line 1201345
    const v0, 0x7f0d3101

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/7OL;->p:Landroid/view/ViewGroup;

    .line 1201346
    const v0, 0x7f0d1333

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, LX/7OL;->q:Lcom/facebook/widget/FbImageView;

    .line 1201347
    iget-object v0, p0, LX/7OL;->q:Lcom/facebook/widget/FbImageView;

    new-instance v1, LX/7OI;

    invoke-direct {v1, p0}, LX/7OI;-><init>(LX/7OL;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1201348
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7OK;

    invoke-direct {v1, p0}, LX/7OK;-><init>(LX/7OL;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1201349
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/7OL;->r:Ljava/util/LinkedHashMap;

    .line 1201350
    return-void
.end method

.method public static e(LX/7OL;I)I
    .locals 1

    .prologue
    .line 1201351
    invoke-virtual {p0}, LX/7OL;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(J)V
    .locals 5

    .prologue
    .line 1201352
    iget v0, p0, LX/7Mr;->d:I

    if-eqz v0, :cond_0

    iget v0, p0, LX/7Mr;->d:I

    .line 1201353
    :goto_0
    long-to-int v1, p1

    iget-object v2, p0, LX/7OL;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    mul-int/2addr v1, v2

    div-int v0, v1, v0

    .line 1201354
    const v1, 0x7f0b06a2

    invoke-static {p0, v1}, LX/7OL;->e(LX/7OL;I)I

    move-result v1

    add-int/2addr v1, v0

    .line 1201355
    iget-object v2, p0, LX/7OL;->s:Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;

    if-eqz v2, :cond_1

    int-to-float v2, v1

    iget-object v3, p0, LX/7OL;->s:Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;

    invoke-virtual {v3}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->getX()F

    move-result v3

    iget-object v4, p0, LX/7OL;->s:Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;

    invoke-virtual {v4}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->getWidth()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 1201356
    :goto_1
    return-void

    .line 1201357
    :cond_0
    iget-object v0, p0, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    goto :goto_0

    .line 1201358
    :cond_1
    new-instance v2, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;

    invoke-virtual {p0}, LX/7OL;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;-><init>(Landroid/content/Context;)V

    .line 1201359
    int-to-float v1, v1

    invoke-virtual {v2, v1}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->setX(F)V

    .line 1201360
    new-instance v1, LX/7OJ;

    invoke-direct {v1, p0, p1, p2}, LX/7OJ;-><init>(LX/7OL;J)V

    invoke-virtual {v2, v1}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1201361
    iget-object v1, p0, LX/7OL;->o:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1201362
    iget-object v1, p0, LX/7OL;->r:Ljava/util/LinkedHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1201363
    const/high16 p1, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 1201364
    const-string v1, "scaleX"

    invoke-static {v2, v1, v4, p1}, LX/4mQ;->a(Ljava/lang/Object;Ljava/lang/String;FF)LX/4mQ;

    move-result-object v1

    .line 1201365
    const-string v3, "scaleY"

    invoke-static {v2, v3, v4, p1}, LX/4mQ;->a(Ljava/lang/Object;Ljava/lang/String;FF)LX/4mQ;

    move-result-object v3

    .line 1201366
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1201367
    const/4 p1, 0x2

    new-array p1, p1, [Landroid/animation/Animator;

    const/4 p2, 0x0

    aput-object v1, p1, p2

    const/4 v1, 0x1

    aput-object v3, p1, v1

    invoke-virtual {v4, p1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1201368
    invoke-virtual {v4}, Landroid/animation/AnimatorSet;->start()V

    .line 1201369
    iget-object v1, p0, LX/7OL;->p:Landroid/view/ViewGroup;

    const v3, 0x7f0b06a8

    invoke-static {p0, v3}, LX/7OL;->e(LX/7OL;I)I

    move-result v3

    add-int/2addr v0, v3

    .line 1201370
    new-instance v3, Landroid/view/View;

    invoke-virtual {p0}, LX/7OL;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1201371
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const p1, 0x7f0b06a6

    invoke-static {p0, p1}, LX/7OL;->e(LX/7OL;I)I

    move-result p1

    const/4 p2, -0x1

    invoke-direct {v4, p1, p2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1201372
    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1201373
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/7OL;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0a00d5

    invoke-static {p1, p2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result p1

    invoke-direct {v4, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1201374
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x10

    if-lt p1, p2, :cond_2

    .line 1201375
    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1201376
    :goto_2
    int-to-float v4, v0

    invoke-virtual {v3, v4}, Landroid/view/View;->setX(F)V

    .line 1201377
    move-object v0, v3

    .line 1201378
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1201379
    iput-object v2, p0, LX/7OL;->s:Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;

    goto/16 :goto_1

    .line 1201380
    :cond_2
    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method public getActiveThumbResource()I
    .locals 1

    .prologue
    .line 1201381
    const/4 v0, 0x0

    return v0
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1201382
    const v0, 0x7f0315b8

    return v0
.end method
