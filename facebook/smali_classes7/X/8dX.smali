.class public final LX/8dX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Z

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:D

.field public F:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Z

.field public J:I

.field public K:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:I

.field public P:I

.field public Q:I

.field public R:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Z

.field public T:Z

.field public U:Z

.field public V:Z

.field public W:Z

.field public X:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:I

.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aB:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aC:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aD:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aF:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:D

.field public aI:D

.field public aJ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:I

.field public aM:J

.field public aN:Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aO:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aR:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aS:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aV:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aX:Z

.field public aY:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aZ:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:D

.field public af:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aq:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ar:I

.field public as:I

.field public at:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:D

.field public av:D

.field public aw:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ax:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ay:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public az:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ba:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bb:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bc:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bd:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public be:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bf:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bg:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bh:I

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:J

.field public s:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:J

.field public u:Z

.field public v:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Z

.field public x:J

.field public y:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1376020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)LX/8dX;
    .locals 4

    .prologue
    .line 1376224
    new-instance v0, LX/8dX;

    invoke-direct {v0}, LX/8dX;-><init>()V

    .line 1376225
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1376226
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Q()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->b:Ljava/lang/String;

    .line 1376227
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->A()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->c:Ljava/lang/String;

    .line 1376228
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bp()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->d:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    .line 1376229
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bq()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->e:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    .line 1376230
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->c()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->f:LX/0Px;

    .line 1376231
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->br()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->g:Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    .line 1376232
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bs()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    .line 1376233
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bt()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->i:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    .line 1376234
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->U()Z

    move-result v1

    iput-boolean v1, v0, LX/8dX;->j:Z

    .line 1376235
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->X()Z

    move-result v1

    iput-boolean v1, v0, LX/8dX;->k:Z

    .line 1376236
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->u()Z

    move-result v1

    iput-boolean v1, v0, LX/8dX;->l:Z

    .line 1376237
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->v()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->m:LX/0Px;

    .line 1376238
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bu()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->n:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    .line 1376239
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->o:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1376240
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dV_()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->p:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1376241
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bv()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->q:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 1376242
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->V()J

    move-result-wide v2

    iput-wide v2, v0, LX/8dX;->r:J

    .line 1376243
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1376244
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->W()J

    move-result-wide v2

    iput-wide v2, v0, LX/8dX;->t:J

    .line 1376245
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->w()Z

    move-result v1

    iput-boolean v1, v0, LX/8dX;->u:Z

    .line 1376246
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bw()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->v:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    .line 1376247
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aa()Z

    move-result v1

    iput-boolean v1, v0, LX/8dX;->w:Z

    .line 1376248
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ab()J

    move-result-wide v2

    iput-wide v2, v0, LX/8dX;->x:J

    .line 1376249
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bx()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->y:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    .line 1376250
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->by()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->z:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    .line 1376251
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ad()Z

    move-result v1

    iput-boolean v1, v0, LX/8dX;->A:Z

    .line 1376252
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->O()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->B:Ljava/lang/String;

    .line 1376253
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bz()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->C:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    .line 1376254
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->af()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->D:Ljava/lang/String;

    .line 1376255
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ag()D

    move-result-wide v2

    iput-wide v2, v0, LX/8dX;->E:D

    .line 1376256
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->F:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1376257
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bA()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->G:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    .line 1376258
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bB()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->H:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 1376259
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ah()Z

    move-result v1

    iput-boolean v1, v0, LX/8dX;->I:Z

    .line 1376260
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aj()I

    move-result v1

    iput v1, v0, LX/8dX;->J:I

    .line 1376261
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ak()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->K:Ljava/lang/String;

    .line 1376262
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->L:Ljava/lang/String;

    .line 1376263
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bC()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->M:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    .line 1376264
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bD()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->N:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1376265
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->am()I

    move-result v1

    iput v1, v0, LX/8dX;->O:I

    .line 1376266
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->an()I

    move-result v1

    iput v1, v0, LX/8dX;->P:I

    .line 1376267
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ao()I

    move-result v1

    iput v1, v0, LX/8dX;->Q:I

    .line 1376268
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bE()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->R:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    .line 1376269
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->at()Z

    move-result v1

    iput-boolean v1, v0, LX/8dX;->S:Z

    .line 1376270
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ap()Z

    move-result v1

    iput-boolean v1, v0, LX/8dX;->T:Z

    .line 1376271
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aq()Z

    move-result v1

    iput-boolean v1, v0, LX/8dX;->U:Z

    .line 1376272
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aw()Z

    move-result v1

    iput-boolean v1, v0, LX/8dX;->V:Z

    .line 1376273
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->o()Z

    move-result v1

    iput-boolean v1, v0, LX/8dX;->W:Z

    .line 1376274
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bF()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->X:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    .line 1376275
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bG()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->Y:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    .line 1376276
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->au()I

    move-result v1

    iput v1, v0, LX/8dX;->Z:I

    .line 1376277
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bH()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aa:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    .line 1376278
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bI()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->ab:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1376279
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bJ()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->ac:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    .line 1376280
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->ad:Ljava/lang/String;

    .line 1376281
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aA()D

    move-result-wide v2

    iput-wide v2, v0, LX/8dX;->ae:D

    .line 1376282
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bK()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->af:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    .line 1376283
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bL()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->ag:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    .line 1376284
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bM()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->ah:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    .line 1376285
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bN()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->ai:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    .line 1376286
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bO()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aj:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    .line 1376287
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aI()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->ak:Ljava/lang/String;

    .line 1376288
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bQ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->al:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    .line 1376289
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aK()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->am:Ljava/lang/String;

    .line 1376290
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bR()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->an:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 1376291
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bT()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->ao:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    .line 1376292
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bU()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->ap:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    .line 1376293
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->H()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aq:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 1376294
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aE()I

    move-result v1

    iput v1, v0, LX/8dX;->ar:I

    .line 1376295
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aF()I

    move-result v1

    iput v1, v0, LX/8dX;->as:I

    .line 1376296
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->I()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->at:Ljava/lang/String;

    .line 1376297
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aG()D

    move-result-wide v2

    iput-wide v2, v0, LX/8dX;->au:D

    .line 1376298
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aH()D

    move-result-wide v2

    iput-wide v2, v0, LX/8dX;->av:D

    .line 1376299
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bV()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aw:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1376300
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aO()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->ax:Ljava/lang/String;

    .line 1376301
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bW()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->ay:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    .line 1376302
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bX()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->az:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    .line 1376303
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cc()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aA:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    .line 1376304
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cd()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aB:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    .line 1376305
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ce()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aC:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    .line 1376306
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->J()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aD:LX/0Px;

    .line 1376307
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aS()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aE:Ljava/lang/String;

    .line 1376308
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cf()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aF:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    .line 1376309
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cg()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aG:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    .line 1376310
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aU()D

    move-result-wide v2

    iput-wide v2, v0, LX/8dX;->aH:D

    .line 1376311
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aV()D

    move-result-wide v2

    iput-wide v2, v0, LX/8dX;->aI:D

    .line 1376312
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aW()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aJ:Ljava/lang/String;

    .line 1376313
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aY()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aK:Ljava/lang/String;

    .line 1376314
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aZ()I

    move-result v1

    iput v1, v0, LX/8dX;->aL:I

    .line 1376315
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ba()J

    move-result-wide v2

    iput-wide v2, v0, LX/8dX;->aM:J

    .line 1376316
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ch()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aN:Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    .line 1376317
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ci()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aO:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    .line 1376318
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aP:Ljava/lang/String;

    .line 1376319
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cj()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aQ:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    .line 1376320
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ck()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aR:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    .line 1376321
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cl()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aS:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1376322
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cm()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aT:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    .line 1376323
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bh()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aU:Ljava/lang/String;

    .line 1376324
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bi()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aV:Ljava/lang/String;

    .line 1376325
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->z()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aW:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1376326
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bj()Z

    move-result v1

    iput-boolean v1, v0, LX/8dX;->aX:Z

    .line 1376327
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cn()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aY:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    .line 1376328
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->aZ:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1376329
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->ba:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1376330
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->K()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->bb:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1376331
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->bc:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1376332
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->co()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->bd:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    .line 1376333
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cp()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->be:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    .line 1376334
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bm()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->bf:LX/0Px;

    .line 1376335
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bn()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dX;->bg:Ljava/lang/String;

    .line 1376336
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bo()I

    move-result v1

    iput v1, v0, LX/8dX;->bh:I

    .line 1376337
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;
    .locals 84

    .prologue
    .line 1376021
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1376022
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1376023
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8dX;->b:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1376024
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8dX;->c:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1376025
    move-object/from16 v0, p0

    iget-object v6, v0, LX/8dX;->d:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    invoke-static {v2, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1376026
    move-object/from16 v0, p0

    iget-object v7, v0, LX/8dX;->e:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1376027
    move-object/from16 v0, p0

    iget-object v8, v0, LX/8dX;->f:LX/0Px;

    invoke-virtual {v2, v8}, LX/186;->b(Ljava/util/List;)I

    move-result v8

    .line 1376028
    move-object/from16 v0, p0

    iget-object v9, v0, LX/8dX;->g:Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1376029
    move-object/from16 v0, p0

    iget-object v10, v0, LX/8dX;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    invoke-static {v2, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1376030
    move-object/from16 v0, p0

    iget-object v11, v0, LX/8dX;->i:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    invoke-static {v2, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1376031
    move-object/from16 v0, p0

    iget-object v12, v0, LX/8dX;->m:LX/0Px;

    invoke-virtual {v2, v12}, LX/186;->b(Ljava/util/List;)I

    move-result v12

    .line 1376032
    move-object/from16 v0, p0

    iget-object v13, v0, LX/8dX;->n:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    invoke-static {v2, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1376033
    move-object/from16 v0, p0

    iget-object v14, v0, LX/8dX;->o:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v2, v14}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v14

    .line 1376034
    move-object/from16 v0, p0

    iget-object v15, v0, LX/8dX;->p:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-virtual {v2, v15}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    .line 1376035
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->q:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1376036
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1376037
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->v:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1376038
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->y:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1376039
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->z:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1376040
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->B:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 1376041
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->C:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1376042
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->D:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 1376043
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->F:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v24

    .line 1376044
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->G:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 1376045
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->H:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1376046
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->K:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 1376047
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->L:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 1376048
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->M:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 1376049
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->N:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 1376050
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->R:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 1376051
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->X:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 1376052
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->Y:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 1376053
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aa:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 1376054
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->ab:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 1376055
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->ac:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 1376056
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->ad:Ljava/lang/String;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 1376057
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->af:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 1376058
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->ag:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 1376059
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->ah:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 1376060
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->ai:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 1376061
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aj:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 1376062
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->ak:Ljava/lang/String;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v43

    .line 1376063
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->al:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 1376064
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->am:Ljava/lang/String;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v45

    .line 1376065
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->an:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 1376066
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->ao:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    move-object/from16 v47, v0

    move-object/from16 v0, v47

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 1376067
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->ap:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v48

    .line 1376068
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aq:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-object/from16 v49, v0

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v49

    .line 1376069
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->at:Ljava/lang/String;

    move-object/from16 v50, v0

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v50

    .line 1376070
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aw:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-object/from16 v51, v0

    move-object/from16 v0, v51

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 1376071
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->ax:Ljava/lang/String;

    move-object/from16 v52, v0

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v52

    .line 1376072
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->ay:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v53

    .line 1376073
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->az:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    move-object/from16 v54, v0

    move-object/from16 v0, v54

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v54

    .line 1376074
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aA:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v55

    .line 1376075
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aB:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-object/from16 v56, v0

    move-object/from16 v0, v56

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v56

    .line 1376076
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aC:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-object/from16 v57, v0

    move-object/from16 v0, v57

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v57

    .line 1376077
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aD:LX/0Px;

    move-object/from16 v58, v0

    move-object/from16 v0, v58

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v58

    .line 1376078
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aE:Ljava/lang/String;

    move-object/from16 v59, v0

    move-object/from16 v0, v59

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v59

    .line 1376079
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aF:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-object/from16 v60, v0

    move-object/from16 v0, v60

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v60

    .line 1376080
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aG:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-object/from16 v61, v0

    move-object/from16 v0, v61

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v61

    .line 1376081
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aJ:Ljava/lang/String;

    move-object/from16 v62, v0

    move-object/from16 v0, v62

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v62

    .line 1376082
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aK:Ljava/lang/String;

    move-object/from16 v63, v0

    move-object/from16 v0, v63

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v63

    .line 1376083
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aN:Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    move-object/from16 v64, v0

    move-object/from16 v0, v64

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v64

    .line 1376084
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aO:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    move-object/from16 v65, v0

    move-object/from16 v0, v65

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v65

    .line 1376085
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aP:Ljava/lang/String;

    move-object/from16 v66, v0

    move-object/from16 v0, v66

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v66

    .line 1376086
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aQ:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-object/from16 v67, v0

    move-object/from16 v0, v67

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v67

    .line 1376087
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aR:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-object/from16 v68, v0

    move-object/from16 v0, v68

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v68

    .line 1376088
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aS:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v69, v0

    move-object/from16 v0, v69

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v69

    .line 1376089
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aT:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-object/from16 v70, v0

    move-object/from16 v0, v70

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v70

    .line 1376090
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aU:Ljava/lang/String;

    move-object/from16 v71, v0

    move-object/from16 v0, v71

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v71

    .line 1376091
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aV:Ljava/lang/String;

    move-object/from16 v72, v0

    move-object/from16 v0, v72

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v72

    .line 1376092
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aW:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object/from16 v73, v0

    move-object/from16 v0, v73

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v73

    .line 1376093
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aY:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    move-object/from16 v74, v0

    move-object/from16 v0, v74

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v74

    .line 1376094
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->aZ:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-object/from16 v75, v0

    move-object/from16 v0, v75

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v75

    .line 1376095
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->ba:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-object/from16 v76, v0

    move-object/from16 v0, v76

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v76

    .line 1376096
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->bb:Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-object/from16 v77, v0

    move-object/from16 v0, v77

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v77

    .line 1376097
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->bc:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object/from16 v78, v0

    move-object/from16 v0, v78

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v78

    .line 1376098
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->bd:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-object/from16 v79, v0

    move-object/from16 v0, v79

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v79

    .line 1376099
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->be:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-object/from16 v80, v0

    move-object/from16 v0, v80

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v80

    .line 1376100
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->bf:LX/0Px;

    move-object/from16 v81, v0

    move-object/from16 v0, v81

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v81

    .line 1376101
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dX;->bg:Ljava/lang/String;

    move-object/from16 v82, v0

    move-object/from16 v0, v82

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v82

    .line 1376102
    const/16 v83, 0x70

    move/from16 v0, v83

    invoke-virtual {v2, v0}, LX/186;->c(I)V

    .line 1376103
    const/16 v83, 0x0

    move/from16 v0, v83

    invoke-virtual {v2, v0, v3}, LX/186;->b(II)V

    .line 1376104
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v4}, LX/186;->b(II)V

    .line 1376105
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1376106
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v6}, LX/186;->b(II)V

    .line 1376107
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1376108
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1376109
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1376110
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1376111
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1376112
    const/16 v3, 0x9

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dX;->j:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1376113
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dX;->k:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1376114
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dX;->l:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1376115
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 1376116
    const/16 v3, 0xd

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 1376117
    const/16 v3, 0xe

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 1376118
    const/16 v3, 0xf

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 1376119
    const/16 v3, 0x10

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376120
    const/16 v3, 0x11

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dX;->r:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1376121
    const/16 v3, 0x12

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376122
    const/16 v3, 0x13

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dX;->t:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1376123
    const/16 v3, 0x14

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dX;->u:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1376124
    const/16 v3, 0x15

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376125
    const/16 v3, 0x16

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dX;->w:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1376126
    const/16 v3, 0x17

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dX;->x:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1376127
    const/16 v3, 0x18

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376128
    const/16 v3, 0x19

    move/from16 v0, v20

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376129
    const/16 v3, 0x1a

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dX;->A:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1376130
    const/16 v3, 0x1b

    move/from16 v0, v21

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376131
    const/16 v3, 0x1c

    move/from16 v0, v22

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376132
    const/16 v3, 0x1d

    move/from16 v0, v23

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376133
    const/16 v3, 0x1e

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dX;->E:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1376134
    const/16 v3, 0x1f

    move/from16 v0, v24

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376135
    const/16 v3, 0x20

    move/from16 v0, v25

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376136
    const/16 v3, 0x21

    move/from16 v0, v26

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376137
    const/16 v3, 0x22

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dX;->I:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1376138
    const/16 v3, 0x23

    move-object/from16 v0, p0

    iget v4, v0, LX/8dX;->J:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1376139
    const/16 v3, 0x24

    move/from16 v0, v27

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376140
    const/16 v3, 0x25

    move/from16 v0, v28

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376141
    const/16 v3, 0x26

    move/from16 v0, v29

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376142
    const/16 v3, 0x27

    move/from16 v0, v30

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376143
    const/16 v3, 0x28

    move-object/from16 v0, p0

    iget v4, v0, LX/8dX;->O:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1376144
    const/16 v3, 0x29

    move-object/from16 v0, p0

    iget v4, v0, LX/8dX;->P:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1376145
    const/16 v3, 0x2a

    move-object/from16 v0, p0

    iget v4, v0, LX/8dX;->Q:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1376146
    const/16 v3, 0x2b

    move/from16 v0, v31

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376147
    const/16 v3, 0x2c

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dX;->S:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1376148
    const/16 v3, 0x2d

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dX;->T:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1376149
    const/16 v3, 0x2e

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dX;->U:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1376150
    const/16 v3, 0x2f

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dX;->V:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1376151
    const/16 v3, 0x30

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dX;->W:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1376152
    const/16 v3, 0x31

    move/from16 v0, v32

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376153
    const/16 v3, 0x32

    move/from16 v0, v33

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376154
    const/16 v3, 0x33

    move-object/from16 v0, p0

    iget v4, v0, LX/8dX;->Z:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1376155
    const/16 v3, 0x34

    move/from16 v0, v34

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376156
    const/16 v3, 0x35

    move/from16 v0, v35

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376157
    const/16 v3, 0x36

    move/from16 v0, v36

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376158
    const/16 v3, 0x37

    move/from16 v0, v37

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376159
    const/16 v3, 0x38

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dX;->ae:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1376160
    const/16 v3, 0x39

    move/from16 v0, v38

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376161
    const/16 v3, 0x3a

    move/from16 v0, v39

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376162
    const/16 v3, 0x3b

    move/from16 v0, v40

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376163
    const/16 v3, 0x3c

    move/from16 v0, v41

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376164
    const/16 v3, 0x3d

    move/from16 v0, v42

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376165
    const/16 v3, 0x3e

    move/from16 v0, v43

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376166
    const/16 v3, 0x3f

    move/from16 v0, v44

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376167
    const/16 v3, 0x40

    move/from16 v0, v45

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376168
    const/16 v3, 0x41

    move/from16 v0, v46

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376169
    const/16 v3, 0x42

    move/from16 v0, v47

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376170
    const/16 v3, 0x43

    move/from16 v0, v48

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376171
    const/16 v3, 0x44

    move/from16 v0, v49

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376172
    const/16 v3, 0x45

    move-object/from16 v0, p0

    iget v4, v0, LX/8dX;->ar:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1376173
    const/16 v3, 0x46

    move-object/from16 v0, p0

    iget v4, v0, LX/8dX;->as:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1376174
    const/16 v3, 0x47

    move/from16 v0, v50

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376175
    const/16 v3, 0x48

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dX;->au:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1376176
    const/16 v3, 0x49

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dX;->av:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1376177
    const/16 v3, 0x4a

    move/from16 v0, v51

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376178
    const/16 v3, 0x4b

    move/from16 v0, v52

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376179
    const/16 v3, 0x4c

    move/from16 v0, v53

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376180
    const/16 v3, 0x4d

    move/from16 v0, v54

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376181
    const/16 v3, 0x4e

    move/from16 v0, v55

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376182
    const/16 v3, 0x4f

    move/from16 v0, v56

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376183
    const/16 v3, 0x50

    move/from16 v0, v57

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376184
    const/16 v3, 0x51

    move/from16 v0, v58

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376185
    const/16 v3, 0x52

    move/from16 v0, v59

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376186
    const/16 v3, 0x53

    move/from16 v0, v60

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376187
    const/16 v3, 0x54

    move/from16 v0, v61

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376188
    const/16 v3, 0x55

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dX;->aH:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1376189
    const/16 v3, 0x56

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dX;->aI:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1376190
    const/16 v3, 0x57

    move/from16 v0, v62

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376191
    const/16 v3, 0x58

    move/from16 v0, v63

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376192
    const/16 v3, 0x59

    move-object/from16 v0, p0

    iget v4, v0, LX/8dX;->aL:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1376193
    const/16 v3, 0x5a

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dX;->aM:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1376194
    const/16 v3, 0x5b

    move/from16 v0, v64

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376195
    const/16 v3, 0x5c

    move/from16 v0, v65

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376196
    const/16 v3, 0x5d

    move/from16 v0, v66

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376197
    const/16 v3, 0x5e

    move/from16 v0, v67

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376198
    const/16 v3, 0x5f

    move/from16 v0, v68

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376199
    const/16 v3, 0x60

    move/from16 v0, v69

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376200
    const/16 v3, 0x61

    move/from16 v0, v70

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376201
    const/16 v3, 0x62

    move/from16 v0, v71

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376202
    const/16 v3, 0x63

    move/from16 v0, v72

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376203
    const/16 v3, 0x64

    move/from16 v0, v73

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376204
    const/16 v3, 0x65

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dX;->aX:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1376205
    const/16 v3, 0x66

    move/from16 v0, v74

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376206
    const/16 v3, 0x67

    move/from16 v0, v75

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376207
    const/16 v3, 0x68

    move/from16 v0, v76

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376208
    const/16 v3, 0x69

    move/from16 v0, v77

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376209
    const/16 v3, 0x6a

    move/from16 v0, v78

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376210
    const/16 v3, 0x6b

    move/from16 v0, v79

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376211
    const/16 v3, 0x6c

    move/from16 v0, v80

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376212
    const/16 v3, 0x6d

    move/from16 v0, v81

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376213
    const/16 v3, 0x6e

    move/from16 v0, v82

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1376214
    const/16 v3, 0x6f

    move-object/from16 v0, p0

    iget v4, v0, LX/8dX;->bh:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1376215
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1376216
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1376217
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1376218
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1376219
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1376220
    new-instance v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;-><init>(LX/15i;)V

    .line 1376221
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8dX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    .line 1376222
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8dX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->L_()LX/0x2;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/graphql/model/GraphQLStory;->a(LX/0x2;)V

    .line 1376223
    :cond_0
    return-object v3
.end method
