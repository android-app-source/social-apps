.class public LX/6mM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final accuracy:Ljava/lang/String;

.field public final latitude:Ljava/lang/String;

.field public final longitude:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/16 v3, 0xb

    .line 1143881
    new-instance v0, LX/1sv;

    const-string v1, "Coordinates"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mM;->b:LX/1sv;

    .line 1143882
    new-instance v0, LX/1sw;

    const-string v1, "latitude"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mM;->c:LX/1sw;

    .line 1143883
    new-instance v0, LX/1sw;

    const-string v1, "longitude"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mM;->d:LX/1sw;

    .line 1143884
    new-instance v0, LX/1sw;

    const-string v1, "accuracy"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mM;->e:LX/1sw;

    .line 1143885
    sput-boolean v4, LX/6mM;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1143886
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1143887
    iput-object p1, p0, LX/6mM;->latitude:Ljava/lang/String;

    .line 1143888
    iput-object p2, p0, LX/6mM;->longitude:Ljava/lang/String;

    .line 1143889
    iput-object p3, p0, LX/6mM;->accuracy:Ljava/lang/String;

    .line 1143890
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1143891
    if-eqz p2, :cond_4

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1143892
    :goto_0
    if-eqz p2, :cond_5

    const-string v0, "\n"

    move-object v3, v0

    .line 1143893
    :goto_1
    if-eqz p2, :cond_6

    const-string v0, " "

    .line 1143894
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "Coordinates"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1143895
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143896
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143897
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143898
    const/4 v1, 0x1

    .line 1143899
    iget-object v6, p0, LX/6mM;->latitude:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 1143900
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143901
    const-string v1, "latitude"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143902
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143903
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143904
    iget-object v1, p0, LX/6mM;->latitude:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 1143905
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1143906
    :cond_0
    iget-object v6, p0, LX/6mM;->longitude:Ljava/lang/String;

    if-eqz v6, :cond_a

    .line 1143907
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143908
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143909
    const-string v1, "longitude"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143910
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143911
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143912
    iget-object v1, p0, LX/6mM;->longitude:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 1143913
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143914
    :goto_4
    iget-object v1, p0, LX/6mM;->accuracy:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1143915
    if-nez v2, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143916
    :cond_2
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143917
    const-string v1, "accuracy"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143918
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143919
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143920
    iget-object v0, p0, LX/6mM;->accuracy:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 1143921
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143922
    :cond_3
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143923
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143924
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1143925
    :cond_4
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1143926
    :cond_5
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1143927
    :cond_6
    const-string v0, ""

    goto/16 :goto_2

    .line 1143928
    :cond_7
    iget-object v1, p0, LX/6mM;->latitude:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1143929
    :cond_8
    iget-object v1, p0, LX/6mM;->longitude:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1143930
    :cond_9
    iget-object v0, p0, LX/6mM;->accuracy:Ljava/lang/String;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_a
    move v2, v1

    goto/16 :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1143931
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1143932
    iget-object v0, p0, LX/6mM;->latitude:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1143933
    iget-object v0, p0, LX/6mM;->latitude:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1143934
    sget-object v0, LX/6mM;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143935
    iget-object v0, p0, LX/6mM;->latitude:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1143936
    :cond_0
    iget-object v0, p0, LX/6mM;->longitude:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1143937
    iget-object v0, p0, LX/6mM;->longitude:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1143938
    sget-object v0, LX/6mM;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143939
    iget-object v0, p0, LX/6mM;->longitude:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1143940
    :cond_1
    iget-object v0, p0, LX/6mM;->accuracy:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1143941
    iget-object v0, p0, LX/6mM;->accuracy:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1143942
    sget-object v0, LX/6mM;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143943
    iget-object v0, p0, LX/6mM;->accuracy:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1143944
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1143945
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1143946
    return-void
.end method

.method public final a(LX/6mM;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1143947
    if-nez p1, :cond_1

    .line 1143948
    :cond_0
    :goto_0
    return v2

    .line 1143949
    :cond_1
    iget-object v0, p0, LX/6mM;->latitude:Ljava/lang/String;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1143950
    :goto_1
    iget-object v3, p1, LX/6mM;->latitude:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1143951
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1143952
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1143953
    iget-object v0, p0, LX/6mM;->latitude:Ljava/lang/String;

    iget-object v3, p1, LX/6mM;->latitude:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1143954
    :cond_3
    iget-object v0, p0, LX/6mM;->longitude:Ljava/lang/String;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1143955
    :goto_3
    iget-object v3, p1, LX/6mM;->longitude:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1143956
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1143957
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1143958
    iget-object v0, p0, LX/6mM;->longitude:Ljava/lang/String;

    iget-object v3, p1, LX/6mM;->longitude:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1143959
    :cond_5
    iget-object v0, p0, LX/6mM;->accuracy:Ljava/lang/String;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1143960
    :goto_5
    iget-object v3, p1, LX/6mM;->accuracy:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1143961
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1143962
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1143963
    iget-object v0, p0, LX/6mM;->accuracy:Ljava/lang/String;

    iget-object v3, p1, LX/6mM;->accuracy:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_7
    move v2, v1

    .line 1143964
    goto :goto_0

    :cond_8
    move v0, v2

    .line 1143965
    goto :goto_1

    :cond_9
    move v3, v2

    .line 1143966
    goto :goto_2

    :cond_a
    move v0, v2

    .line 1143967
    goto :goto_3

    :cond_b
    move v3, v2

    .line 1143968
    goto :goto_4

    :cond_c
    move v0, v2

    .line 1143969
    goto :goto_5

    :cond_d
    move v3, v2

    .line 1143970
    goto :goto_6
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1143971
    if-nez p1, :cond_1

    .line 1143972
    :cond_0
    :goto_0
    return v0

    .line 1143973
    :cond_1
    instance-of v1, p1, LX/6mM;

    if-eqz v1, :cond_0

    .line 1143974
    check-cast p1, LX/6mM;

    invoke-virtual {p0, p1}, LX/6mM;->a(LX/6mM;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1143975
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1143976
    sget-boolean v0, LX/6mM;->a:Z

    .line 1143977
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mM;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1143978
    return-object v0
.end method
