.class public final LX/6fC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Mk;

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Mk;)V
    .locals 1

    .prologue
    .line 1119434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119435
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/6fC;->b:Ljava/util/Map;

    .line 1119436
    iput-object p1, p0, LX/6fC;->a:LX/2Mk;

    .line 1119437
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 2

    .prologue
    .line 1119448
    iget-object v0, p0, LX/6fC;->b:Ljava/util/Map;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1119449
    invoke-static {p1}, LX/2Mk;->r(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1119450
    iget-object v0, p0, LX/6fC;->b:Ljava/util/Map;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1119451
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 1119447
    invoke-virtual {p0, p1}, LX/6fC;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;
    .locals 2

    .prologue
    .line 1119442
    iget-object v0, p0, LX/6fC;->b:Ljava/util/Map;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 1119443
    if-eqz v0, :cond_1

    .line 1119444
    :cond_0
    :goto_0
    return-object v0

    .line 1119445
    :cond_1
    invoke-static {p1}, LX/2Mk;->r(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1119446
    iget-object v0, p0, LX/6fC;->b:Ljava/util/Map;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    goto :goto_0
.end method

.method public final d(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 2

    .prologue
    .line 1119438
    iget-object v0, p0, LX/6fC;->b:Ljava/util/Map;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1119439
    invoke-static {p1}, LX/2Mk;->r(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1119440
    iget-object v0, p0, LX/6fC;->b:Ljava/util/Map;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1119441
    :cond_0
    return-void
.end method
