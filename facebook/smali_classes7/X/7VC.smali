.class public LX/7VC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/48R;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/1oy;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/63p;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0yH;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1oy;LX/0Or;Ljava/util/Set;LX/0yH;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1oy;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/63p;",
            ">;",
            "LX/0yH;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1214060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1214061
    iput-object p1, p0, LX/7VC;->a:Landroid/content/Context;

    .line 1214062
    iput-object p2, p0, LX/7VC;->b:LX/1oy;

    .line 1214063
    iput-object p3, p0, LX/7VC;->c:LX/0Or;

    .line 1214064
    iput-object p4, p0, LX/7VC;->d:Ljava/util/Set;

    .line 1214065
    iput-object p5, p0, LX/7VC;->e:LX/0yH;

    .line 1214066
    return-void
.end method

.method private a(Landroid/content/Intent;)LX/7VB;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1214067
    iget-object v0, p0, LX/7VC;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1214068
    sget-object v0, LX/7VB;->DONT_HANDLE:LX/7VB;

    .line 1214069
    :goto_0
    return-object v0

    .line 1214070
    :cond_0
    iget-object v0, p0, LX/7VC;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/63p;

    .line 1214071
    invoke-interface {v0, p1}, LX/63p;->a(Landroid/content/Intent;)LX/03R;

    move-result-object v0

    .line 1214072
    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v0, v2, :cond_2

    .line 1214073
    sget-object v0, LX/7VB;->DONT_HANDLE:LX/7VB;

    goto :goto_0

    .line 1214074
    :cond_2
    sget-object v2, LX/03R;->NO:LX/03R;

    if-ne v0, v2, :cond_1

    .line 1214075
    sget-object v0, LX/7VB;->HANDLE_BEHIND_DIALOG:LX/7VB;

    goto :goto_0

    .line 1214076
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, LX/007;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1214077
    sget-object v0, LX/7VB;->HANDLE_BEHIND_DIALOG:LX/7VB;

    goto :goto_0

    .line 1214078
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1H1;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1214079
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1H1;->b(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 1214080
    iget-object v1, p0, LX/7VC;->b:LX/1oy;

    invoke-interface {v1, v0}, LX/1oy;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1214081
    iget-object v1, p0, LX/7VC;->b:LX/1oy;

    invoke-interface {v1, v0}, LX/1oy;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 1214082
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 1214083
    invoke-static {v1}, LX/1H1;->a(Landroid/net/Uri;)Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1214084
    const-string v2, "u"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, v2, p0}, LX/1H1;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object v0, v2

    .line 1214085
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1214086
    sget-object v0, LX/7VB;->HANDLE_AFTER_REWRITE:LX/7VB;

    goto/16 :goto_0

    .line 1214087
    :cond_5
    sget-object v0, LX/7VB;->HANDLE_BEHIND_DIALOG:LX/7VB;

    goto/16 :goto_0

    .line 1214088
    :cond_6
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_7

    iget-object v0, p0, LX/7VC;->e:LX/0yH;

    sget-object v1, LX/0yY;->LEAVING_APP_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1214089
    sget-object v0, LX/7VB;->HANDLE_BEHIND_DIALOG:LX/7VB;

    goto/16 :goto_0

    .line 1214090
    :cond_7
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/7VC;->b:LX/1oy;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1oy;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1214091
    sget-object v0, LX/7VB;->HANDLE_AFTER_REWRITE:LX/7VB;

    goto/16 :goto_0

    .line 1214092
    :cond_8
    sget-object v0, LX/7VB;->HANDLE_BEHIND_DIALOG:LX/7VB;

    goto/16 :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/content/Intent;IZ)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1214093
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1214094
    const-class v1, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1214095
    const-string v1, "destination_intent"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1214096
    const-string v1, "request_code"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1214097
    const-string v1, "start_for_result"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1214098
    const-string v1, "zero_feature_key_string"

    sget-object v2, LX/0yY;->EXTERNAL_URLS_INTERSTITIAL:LX/0yY;

    iget-object v2, v2, LX/0yY;->prefString:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1214099
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1214100
    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1214101
    return-object v0
.end method

.method private b(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1214102
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1214103
    iget-object v0, p0, LX/7VC;->b:LX/1oy;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1oy;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1214104
    :cond_0
    return-object p1
.end method


# virtual methods
.method public final a(Landroid/content/Intent;ILandroid/app/Activity;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1214105
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 1214106
    :try_start_0
    sget-object v2, LX/7VA;->a:[I

    invoke-direct {p0, p1}, LX/7VC;->a(Landroid/content/Intent;)LX/7VB;

    move-result-object v3

    invoke-virtual {v3}, LX/7VB;->ordinal()I

    move-result v3

    aget v2, v2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v2, :pswitch_data_0

    .line 1214107
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1214108
    :pswitch_0
    :try_start_1
    invoke-direct {p0, p1}, LX/7VC;->b(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object p1

    .line 1214109
    invoke-virtual {p3, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1214110
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    goto :goto_0

    .line 1214111
    :pswitch_1
    const/4 v2, 0x1

    :try_start_2
    invoke-static {p3, p1, p2, v2}, LX/7VC;->a(Landroid/content/Context;Landroid/content/Intent;IZ)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p3, v2, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1214112
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1214113
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 1214114
    :try_start_0
    sget-object v2, LX/7VA;->a:[I

    invoke-direct {p0, p1}, LX/7VC;->a(Landroid/content/Intent;)LX/7VB;

    move-result-object v3

    invoke-virtual {v3}, LX/7VB;->ordinal()I

    move-result v3

    aget v2, v2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v2, :pswitch_data_0

    .line 1214115
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1214116
    :pswitch_0
    :try_start_1
    invoke-direct {p0, p1}, LX/7VC;->b(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object p1

    .line 1214117
    invoke-virtual {p3, p1, p2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1214118
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    goto :goto_0

    .line 1214119
    :pswitch_1
    :try_start_2
    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, p1, p2, v3}, LX/7VC;->a(Landroid/content/Context;Landroid/content/Intent;IZ)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p3, v2, p2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1214120
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1214121
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    .line 1214122
    :try_start_0
    sget-object v3, LX/7VA;->a:[I

    invoke-direct {p0, p1}, LX/7VC;->a(Landroid/content/Intent;)LX/7VB;

    move-result-object v4

    invoke-virtual {v4}, LX/7VB;->ordinal()I

    move-result v4

    aget v3, v3, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v3, :pswitch_data_0

    .line 1214123
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-static {v2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move v0, v1

    :goto_0
    return v0

    .line 1214124
    :pswitch_0
    :try_start_1
    invoke-direct {p0, p1}, LX/7VC;->b(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object p1

    .line 1214125
    invoke-virtual {p2, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1214126
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    goto :goto_0

    .line 1214127
    :pswitch_1
    const/4 v1, 0x0

    const/4 v3, 0x0

    :try_start_2
    invoke-static {p2, p1, v1, v3}, LX/7VC;->a(Landroid/content/Context;Landroid/content/Intent;IZ)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1214128
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
