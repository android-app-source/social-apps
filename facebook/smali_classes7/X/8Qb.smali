.class public final LX/8Qb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 1343446
    iput-object p1, p0, LX/8Qb;->a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 1343445
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 1343436
    packed-switch p2, :pswitch_data_0

    .line 1343437
    :goto_0
    return-void

    .line 1343438
    :pswitch_0
    iget-object v0, p0, LX/8Qb;->a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->f:LX/0Sy;

    iget-object v1, p0, LX/8Qb;->a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;

    .line 1343439
    iget-object p1, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, p1

    .line 1343440
    invoke-virtual {v0, v1}, LX/0Sy;->a(Landroid/view/View;)V

    .line 1343441
    iget-object v0, p0, LX/8Qb;->a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;

    invoke-virtual {v0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->l()V

    goto :goto_0

    .line 1343442
    :pswitch_1
    iget-object v0, p0, LX/8Qb;->a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->f:LX/0Sy;

    iget-object v1, p0, LX/8Qb;->a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;

    .line 1343443
    iget-object p0, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, p0

    .line 1343444
    invoke-virtual {v0, v1}, LX/0Sy;->b(Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
