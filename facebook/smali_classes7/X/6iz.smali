.class public LX/6iz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public a:Lcom/facebook/messaging/model/messages/Message;

.field private b:LX/03R;

.field public c:Lcom/facebook/fbtrace/FbTraceNode;

.field public d:I

.field public e:J

.field public f:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 1130164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130165
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/6iz;->b:LX/03R;

    .line 1130166
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    iput-object v0, p0, LX/6iz;->c:Lcom/facebook/fbtrace/FbTraceNode;

    .line 1130167
    const/4 v0, -0x1

    iput v0, p0, LX/6iz;->d:I

    .line 1130168
    iput-wide v2, p0, LX/6iz;->e:J

    .line 1130169
    iput-wide v2, p0, LX/6iz;->f:J

    .line 1130170
    return-void
.end method


# virtual methods
.method public final a(Z)LX/6iz;
    .locals 1

    .prologue
    .line 1130171
    invoke-static {p1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/6iz;->b:LX/03R;

    .line 1130172
    return-object p0
.end method

.method public final a()Lcom/facebook/messaging/service/model/SendMessageParams;
    .locals 10

    .prologue
    .line 1130173
    iget-object v0, p0, LX/6iz;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1130174
    iget-object v0, p0, LX/6iz;->b:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1130175
    new-instance v1, Lcom/facebook/messaging/service/model/SendMessageParams;

    iget-object v2, p0, LX/6iz;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, p0, LX/6iz;->b:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v3

    iget-object v4, p0, LX/6iz;->c:Lcom/facebook/fbtrace/FbTraceNode;

    iget v5, p0, LX/6iz;->d:I

    iget-wide v6, p0, LX/6iz;->e:J

    iget-wide v8, p0, LX/6iz;->f:J

    invoke-direct/range {v1 .. v9}, Lcom/facebook/messaging/service/model/SendMessageParams;-><init>(Lcom/facebook/messaging/model/messages/Message;ZLcom/facebook/fbtrace/FbTraceNode;IJJ)V

    return-object v1
.end method
