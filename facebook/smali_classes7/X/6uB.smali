.class public LX/6uB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

.field public b:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

.field private final c:LX/6u8;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1155635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1155636
    new-instance v0, LX/6u9;

    invoke-direct {v0, p0}, LX/6u9;-><init>(LX/6uB;)V

    iput-object v0, p0, LX/6uB;->c:LX/6u8;

    .line 1155637
    return-void
.end method

.method public static a(LX/0QB;)LX/6uB;
    .locals 1

    .prologue
    .line 1155634
    invoke-static {}, LX/6uB;->b()LX/6uB;

    move-result-object v0

    return-object v0
.end method

.method private static b()LX/6uB;
    .locals 1

    .prologue
    .line 1155632
    new-instance v0, LX/6uB;

    invoke-direct {v0}, LX/6uB;-><init>()V

    .line 1155633
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/common/locale/Country;
    .locals 1

    .prologue
    .line 1155629
    iget-object v0, p0, LX/6uB;->a:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    .line 1155630
    iget-object p0, v0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->d:Lcom/facebook/common/locale/Country;

    move-object v0, p0

    .line 1155631
    return-object v0
.end method

.method public final a(Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;)V
    .locals 2

    .prologue
    .line 1155626
    iput-object p1, p0, LX/6uB;->a:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    .line 1155627
    iget-object v0, p0, LX/6uB;->a:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    iget-object v1, p0, LX/6uB;->c:LX/6u8;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->a(LX/6u8;)V

    .line 1155628
    return-void
.end method

.method public final a(Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;)V
    .locals 2

    .prologue
    .line 1155623
    iput-object p1, p0, LX/6uB;->b:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    .line 1155624
    iget-object v0, p0, LX/6uB;->b:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    new-instance v1, LX/6uA;

    invoke-direct {v1, p0}, LX/6uA;-><init>(LX/6uB;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1155625
    return-void
.end method
