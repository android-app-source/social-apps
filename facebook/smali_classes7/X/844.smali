.class public final LX/844;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:LX/0QK;

.field public final synthetic c:LX/2dk;


# direct methods
.method public constructor <init>(LX/2dk;LX/0TF;LX/0QK;)V
    .locals 0

    .prologue
    .line 1290784
    iput-object p1, p0, LX/844;->c:LX/2dk;

    iput-object p2, p0, LX/844;->a:LX/0TF;

    iput-object p3, p0, LX/844;->b:LX/0QK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1290782
    iget-object v0, p0, LX/844;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1290783
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1290779
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1290780
    iget-object v0, p0, LX/844;->a:LX/0TF;

    iget-object v1, p0, LX/844;->b:LX/0QK;

    invoke-interface {v1, p1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1290781
    return-void
.end method
