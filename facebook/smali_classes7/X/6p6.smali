.class public LX/6p6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static n:LX/0Xm;


# instance fields
.field public a:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

.field private final j:LX/03V;

.field private final k:LX/0Zb;

.field private final l:Ljava/util/concurrent/Executor;

.field public final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;LX/03V;LX/0Zb;Ljava/util/concurrent/Executor;LX/0Or;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Zb;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1148857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148858
    iput-object p1, p0, LX/6p6;->i:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    .line 1148859
    iput-object p2, p0, LX/6p6;->j:LX/03V;

    .line 1148860
    iput-object p3, p0, LX/6p6;->k:LX/0Zb;

    .line 1148861
    iput-object p4, p0, LX/6p6;->l:Ljava/util/concurrent/Executor;

    .line 1148862
    iput-object p5, p0, LX/6p6;->m:LX/0Or;

    .line 1148863
    return-void
.end method

.method public static a(LX/0QB;)LX/6p6;
    .locals 9

    .prologue
    .line 1148846
    const-class v1, LX/6p6;

    monitor-enter v1

    .line 1148847
    :try_start_0
    sget-object v0, LX/6p6;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1148848
    sput-object v2, LX/6p6;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1148849
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1148850
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1148851
    new-instance v3, LX/6p6;

    invoke-static {v0}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    const/16 v8, 0x12cc

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/6p6;-><init>(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;LX/03V;LX/0Zb;Ljava/util/concurrent/Executor;LX/0Or;)V

    .line 1148852
    move-object v0, v3

    .line 1148853
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1148854
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6p6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1148855
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1148856
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/6p6;Lcom/google/common/util/concurrent/ListenableFuture;LX/0QR;LX/6p5;LX/6nn;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;",
            "LX/0QR",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;>;",
            "LX/6p5;",
            "LX/6nn",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1148840
    invoke-static {p1}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1148841
    :goto_0
    return-object p1

    .line 1148842
    :cond_0
    invoke-virtual {p4}, LX/6nn;->a()V

    .line 1148843
    invoke-interface {p2}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148844
    new-instance v1, LX/6p3;

    invoke-direct {v1, p0, p3, p4}, LX/6p3;-><init>(LX/6p6;LX/6p5;LX/6nn;)V

    iget-object v2, p0, LX/6p6;->l:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    move-object p1, v0

    .line 1148845
    goto :goto_0
.end method

.method private static a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0    # Lcom/google/common/util/concurrent/ListenableFuture;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1148769
    invoke-static {p0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1148770
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1148771
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1148831
    iget-object v0, p0, LX/6p6;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/6p6;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148832
    iget-object v0, p0, LX/6p6;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/6p6;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148833
    iget-object v0, p0, LX/6p6;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/6p6;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148834
    iget-object v0, p0, LX/6p6;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/6p6;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148835
    iget-object v0, p0, LX/6p6;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/6p6;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148836
    iget-object v0, p0, LX/6p6;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/6p6;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148837
    iget-object v0, p0, LX/6p6;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/6p6;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148838
    iget-object v0, p0, LX/6p6;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/6p6;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148839
    return-void
.end method

.method public final a(JLjava/lang/String;LX/6nn;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "LX/6nn",
            "<",
            "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1148821
    new-instance v0, LX/6p0;

    invoke-direct {v0, p0, p1, p2, p3}, LX/6p0;-><init>(LX/6p6;JLjava/lang/String;)V

    .line 1148822
    invoke-static {}, LX/6p5;->a()LX/6p4;

    move-result-object v1

    const-string v2, "p2p_pin_entered"

    .line 1148823
    iput-object v2, v1, LX/6p4;->a:Ljava/lang/String;

    .line 1148824
    move-object v1, v1

    .line 1148825
    const-string v2, "p2p_pin_enter_fail"

    .line 1148826
    iput-object v2, v1, LX/6p4;->b:Ljava/lang/String;

    .line 1148827
    move-object v1, v1

    .line 1148828
    invoke-virtual {v1}, LX/6p4;->a()LX/6p5;

    move-result-object v1

    .line 1148829
    iget-object v2, p0, LX/6p6;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {p0, v2, v0, v1, p4}, LX/6p6;->a(LX/6p6;Lcom/google/common/util/concurrent/ListenableFuture;LX/0QR;LX/6p5;LX/6nn;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148830
    return-void
.end method

.method public final a(JLjava/lang/String;Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;LX/6nn;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;",
            "LX/6nn",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1148811
    new-instance v0, LX/6oy;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/6oy;-><init>(LX/6p6;JLjava/lang/String;Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;)V

    .line 1148812
    invoke-static {}, LX/6p5;->a()LX/6p4;

    move-result-object v1

    const-string v2, "p2p_pin_status_updated"

    .line 1148813
    iput-object v2, v1, LX/6p4;->a:Ljava/lang/String;

    .line 1148814
    move-object v1, v1

    .line 1148815
    const-string v2, "p2p_pin_status_update_fail"

    .line 1148816
    iput-object v2, v1, LX/6p4;->b:Ljava/lang/String;

    .line 1148817
    move-object v1, v1

    .line 1148818
    invoke-virtual {v1}, LX/6p4;->a()LX/6p5;

    move-result-object v1

    .line 1148819
    iget-object v2, p0, LX/6p6;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {p0, v2, v0, v1, p5}, LX/6p6;->a(LX/6p6;Lcom/google/common/util/concurrent/ListenableFuture;LX/0QR;LX/6p5;LX/6nn;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148820
    return-void
.end method

.method public final a(JLjava/lang/String;Ljava/lang/String;LX/6nn;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/6nn",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1148801
    new-instance v0, LX/6ox;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/6ox;-><init>(LX/6p6;JLjava/lang/String;Ljava/lang/String;)V

    .line 1148802
    invoke-static {}, LX/6p5;->a()LX/6p4;

    move-result-object v1

    const-string v2, "p2p_pin_changed"

    .line 1148803
    iput-object v2, v1, LX/6p4;->a:Ljava/lang/String;

    .line 1148804
    move-object v1, v1

    .line 1148805
    const-string v2, "p2p_pin_change_fail"

    .line 1148806
    iput-object v2, v1, LX/6p4;->b:Ljava/lang/String;

    .line 1148807
    move-object v1, v1

    .line 1148808
    invoke-virtual {v1}, LX/6p4;->a()LX/6p5;

    move-result-object v1

    .line 1148809
    iget-object v2, p0, LX/6p6;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {p0, v2, v0, v1, p5}, LX/6p6;->a(LX/6p6;Lcom/google/common/util/concurrent/ListenableFuture;LX/0QR;LX/6p5;LX/6nn;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148810
    return-void
.end method

.method public final a(JLjava/lang/String;ZLX/6nn;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Z",
            "LX/6nn",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1148791
    new-instance v0, LX/6oz;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/6oz;-><init>(LX/6p6;JLjava/lang/String;Z)V

    .line 1148792
    invoke-static {}, LX/6p5;->a()LX/6p4;

    move-result-object v1

    const-string v2, "p2p_pin_deleted"

    .line 1148793
    iput-object v2, v1, LX/6p4;->a:Ljava/lang/String;

    .line 1148794
    move-object v1, v1

    .line 1148795
    const-string v2, "p2p_pin_delete_fail"

    .line 1148796
    iput-object v2, v1, LX/6p4;->b:Ljava/lang/String;

    .line 1148797
    move-object v1, v1

    .line 1148798
    invoke-virtual {v1}, LX/6p4;->a()LX/6p5;

    move-result-object v1

    .line 1148799
    iget-object v2, p0, LX/6p6;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {p0, v2, v0, v1, p5}, LX/6p6;->a(LX/6p6;Lcom/google/common/util/concurrent/ListenableFuture;LX/0QR;LX/6p5;LX/6nn;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148800
    return-void
.end method

.method public final a(LX/6nn;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6nn",
            "<",
            "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1148784
    new-instance v0, LX/6ow;

    invoke-direct {v0, p0}, LX/6ow;-><init>(LX/6p6;)V

    .line 1148785
    invoke-static {}, LX/6p5;->a()LX/6p4;

    move-result-object v1

    const-string v2, "P2P_PAYMENT_PIN_FETCH_FAILED"

    const-string v3, "Payment PIN failed to fetch"

    .line 1148786
    invoke-static {v2, v3}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v4

    iput-object v4, v1, LX/6p4;->c:LX/0VG;

    .line 1148787
    move-object v1, v1

    .line 1148788
    invoke-virtual {v1}, LX/6p4;->a()LX/6p5;

    move-result-object v1

    .line 1148789
    iget-object v2, p0, LX/6p6;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {p0, v2, v0, v1, p1}, LX/6p6;->a(LX/6p6;Lcom/google/common/util/concurrent/ListenableFuture;LX/0QR;LX/6p5;LX/6nn;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148790
    return-void
.end method

.method public final a(LX/6p5;Z)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1148777
    if-eqz p2, :cond_2

    iget-object v0, p1, LX/6p5;->b:Ljava/lang/String;

    .line 1148778
    :goto_0
    if-eqz v0, :cond_0

    .line 1148779
    iget-object v1, p0, LX/6p6;->k:LX/0Zb;

    const-string v2, "p2p_settings"

    invoke-static {v2, v0}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1148780
    :cond_0
    if-nez p2, :cond_1

    iget-object v0, p1, LX/6p5;->d:LX/0VG;

    if-eqz v0, :cond_1

    .line 1148781
    iget-object v0, p0, LX/6p6;->j:LX/03V;

    iget-object v1, p1, LX/6p5;->d:LX/0VG;

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1148782
    :cond_1
    return-void

    .line 1148783
    :cond_2
    iget-object v0, p1, LX/6p5;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/6nn;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/6nn",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1148772
    new-instance v0, LX/6p1;

    invoke-direct {v0, p0, p1}, LX/6p1;-><init>(LX/6p6;Ljava/lang/String;)V

    .line 1148773
    iget-object v1, p0, LX/6p6;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148774
    sget-object v2, LX/6p5;->a:LX/6p5;

    move-object v2, v2

    .line 1148775
    invoke-static {p0, v1, v0, v2, p2}, LX/6p6;->a(LX/6p6;Lcom/google/common/util/concurrent/ListenableFuture;LX/0QR;LX/6p5;LX/6nn;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148776
    return-void
.end method
