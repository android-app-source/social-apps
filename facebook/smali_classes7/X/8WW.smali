.class public final LX/8WW;
.super LX/1a1;
.source ""


# instance fields
.field public final synthetic l:LX/8WX;

.field private final m:Landroid/view/ViewGroup;

.field private final n:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(LX/8WX;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1354113
    iput-object p1, p0, LX/8WW;->l:LX/8WX;

    .line 1354114
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1354115
    const v0, 0x7f0d13fb

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/8WW;->m:Landroid/view/ViewGroup;

    .line 1354116
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b17ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1354117
    iget-object v1, p0, LX/8WW;->m:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 1354118
    const v0, 0x7f0d13fc

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8WW;->n:Landroid/widget/TextView;

    .line 1354119
    iget-object v0, p0, LX/8WW;->n:Landroid/widget/TextView;

    const v1, 0x7f08236f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1354120
    iget-object v0, p0, LX/8WW;->n:Landroid/widget/TextView;

    new-instance v1, LX/8WV;

    invoke-direct {v1, p0, p1}, LX/8WV;-><init>(LX/8WW;LX/8WX;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1354121
    return-void
.end method
