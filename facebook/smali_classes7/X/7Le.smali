.class public LX/7Le;
.super LX/7IG;
.source ""


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/7Kd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1198395
    const-class v0, LX/7Le;

    sput-object v0, LX/7Le;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/7Kd;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1198396
    invoke-direct {p0, p2}, LX/7IG;-><init>(Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1198397
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/7Le;->d:Ljava/lang/ref/WeakReference;

    .line 1198398
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1198399
    const/4 v1, -0x1

    .line 1198400
    iget-object v0, p0, LX/7Le;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Kd;

    .line 1198401
    if-eqz v0, :cond_0

    .line 1198402
    :try_start_0
    invoke-interface {v0}, LX/7Kd;->getVideoViewCurrentPosition()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1198403
    :goto_0
    return v0

    :catch_0
    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1198404
    const/4 v1, -0x1

    .line 1198405
    iget-object v0, p0, LX/7Le;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Kd;

    .line 1198406
    if-eqz v0, :cond_0

    .line 1198407
    :try_start_0
    invoke-interface {v0}, LX/7Kd;->getVideoViewDurationInMillis()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1198408
    :goto_0
    return v0

    :catch_0
    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1198409
    const/4 v0, 0x1

    return v0
.end method
