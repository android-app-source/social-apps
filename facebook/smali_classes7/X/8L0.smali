.class public final LX/8L0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/8L4;


# direct methods
.method public constructor <init>(LX/8L4;)V
    .locals 0

    .prologue
    .line 1331046
    iput-object p1, p0, LX/8L0;->a:LX/8L4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLcom/facebook/photos/upload/operation/UploadOperation;)Landroid/app/Notification;
    .locals 3

    .prologue
    .line 1331047
    iget-object v0, p0, LX/8L0;->a:LX/8L4;

    iget-object v1, v0, LX/8L4;->A:LX/2HB;

    monitor-enter v1

    .line 1331048
    :try_start_0
    iget-object v0, p0, LX/8L0;->a:LX/8L4;

    iget-object v0, v0, LX/8L4;->A:LX/2HB;

    const/16 v2, 0x64

    invoke-virtual {v0, v2, p5, p6}, LX/2HB;->a(IIZ)LX/2HB;

    .line 1331049
    iget-object v0, p0, LX/8L0;->a:LX/8L4;

    iget-object v0, v0, LX/8L4;->A:LX/2HB;

    .line 1331050
    iput-object p1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1331051
    move-object v0, v0

    .line 1331052
    invoke-virtual {v0, p2}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/2HB;->d(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    iget-object v2, p0, LX/8L0;->a:LX/8L4;

    iget-object v2, v2, LX/8L4;->f:LX/8Ko;

    invoke-virtual {v2}, LX/8Ko;->a()I

    move-result v2

    invoke-virtual {v0, v2}, LX/2HB;->a(I)LX/2HB;

    move-result-object v2

    iget-object v0, p0, LX/8L0;->a:LX/8L4;

    invoke-static {v0, p7}, LX/8L4;->n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v0

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    monitor-exit v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1331053
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(LX/0b5;ILjava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 8

    .prologue
    .line 1331054
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v7, v0

    .line 1331055
    iget-object v0, p0, LX/8L0;->a:LX/8L4;

    iget-object v1, v0, LX/8L4;->f:LX/8Ko;

    iget-object v0, p0, LX/8L0;->a:LX/8L4;

    iget-object v2, v0, LX/8L4;->b:Landroid/content/Context;

    .line 1331056
    iget-object v0, p1, LX/0b5;->b:LX/8KZ;

    move-object v0, v0

    .line 1331057
    sget-object v3, LX/8KZ;->PROCESSING:LX/8KZ;

    if-ne v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v7, v0}, LX/8Ko;->a(Landroid/content/Context;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v2

    .line 1331058
    iget-object v0, p0, LX/8L0;->a:LX/8L4;

    iget-object v0, v0, LX/8L4;->f:LX/8Ko;

    iget-object v1, p0, LX/8L0;->a:LX/8L4;

    iget-object v1, v1, LX/8L4;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/8Ko;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 1331059
    invoke-static {v7}, LX/8L4;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8L0;->a:LX/8L4;

    invoke-static {v0, v7}, LX/8L4;->n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1331060
    :cond_0
    const-string v4, ""

    .line 1331061
    :cond_1
    iget-object v0, p1, LX/0b5;->b:LX/8KZ;

    move-object v0, v0

    .line 1331062
    sget-object v1, LX/8KZ;->PUBLISHING:LX/8KZ;

    if-ne v0, v1, :cond_3

    .line 1331063
    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p4

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, LX/8L0;->a(Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLcom/facebook/photos/upload/operation/UploadOperation;)Landroid/app/Notification;

    move-result-object v0

    .line 1331064
    :goto_1
    return-object v0

    .line 1331065
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1331066
    :cond_3
    if-lez p2, :cond_4

    .line 1331067
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p4

    move-object v3, p3

    move v5, p2

    invoke-direct/range {v0 .. v7}, LX/8L0;->a(Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLcom/facebook/photos/upload/operation/UploadOperation;)Landroid/app/Notification;

    move-result-object v0

    goto :goto_1

    .line 1331068
    :cond_4
    iget-object v0, p0, LX/8L0;->a:LX/8L4;

    iget-object v1, v0, LX/8L4;->A:LX/2HB;

    monitor-enter v1

    .line 1331069
    :try_start_0
    iget-object v0, p0, LX/8L0;->a:LX/8L4;

    iget-object v0, v0, LX/8L4;->A:LX/2HB;

    .line 1331070
    iput-object p4, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1331071
    move-object v3, v0

    .line 1331072
    iget-object v0, p0, LX/8L0;->a:LX/8L4;

    iget-boolean v0, v0, LX/8L4;->z:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/8L0;->a:LX/8L4;

    invoke-static {v0, v7}, LX/8L4;->n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v3, v0}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v0

    iget-object v3, p0, LX/8L0;->a:LX/8L4;

    iget-object v3, v3, LX/8L4;->f:LX/8Ko;

    invoke-virtual {v3}, LX/8Ko;->a()I

    move-result v3

    invoke-virtual {v0, v3}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 1331073
    iget-object v0, p0, LX/8L0;->a:LX/8L4;

    iget-object v0, v0, LX/8L4;->A:LX/2HB;

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    .line 1331074
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1331075
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method
