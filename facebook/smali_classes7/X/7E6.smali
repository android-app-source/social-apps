.class public final LX/7E6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:F


# direct methods
.method public constructor <init>(Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;FFFF)V
    .locals 1

    .prologue
    .line 1184272
    iput-object p1, p0, LX/7E6;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184273
    iput p2, p0, LX/7E6;->b:F

    .line 1184274
    iput p3, p0, LX/7E6;->c:F

    .line 1184275
    iput p4, p0, LX/7E6;->d:F

    .line 1184276
    iput p5, p0, LX/7E6;->e:F

    .line 1184277
    invoke-static {p2, p3}, LX/7Cq;->a(FF)F

    move-result v0

    iput v0, p0, LX/7E6;->f:F

    .line 1184278
    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 1184279
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1184280
    iget v1, p0, LX/7E6;->f:F

    mul-float/2addr v1, v0

    iget v2, p0, LX/7E6;->b:F

    add-float/2addr v1, v2

    .line 1184281
    iget-object v2, p0, LX/7E6;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iget-object v2, v2, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->d:Lcom/facebook/spherical/ui/HeadingFovView;

    invoke-virtual {v2, v1}, Lcom/facebook/spherical/ui/HeadingFovView;->setFovYaw(F)V

    .line 1184282
    iget v1, p0, LX/7E6;->e:F

    iget v2, p0, LX/7E6;->d:F

    sub-float/2addr v1, v2

    .line 1184283
    mul-float/2addr v0, v1

    iget v1, p0, LX/7E6;->d:F

    add-float/2addr v0, v1

    .line 1184284
    iget-object v1, p0, LX/7E6;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iget-object v1, v1, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->d:Lcom/facebook/spherical/ui/HeadingFovView;

    invoke-virtual {v1, v0}, Lcom/facebook/spherical/ui/HeadingFovView;->setFov(F)V

    .line 1184285
    return-void
.end method
