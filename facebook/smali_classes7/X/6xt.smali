.class public final LX/6xt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1159226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159227
    new-instance v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    invoke-direct {v0, p2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    .line 1159228
    iget-object v0, p0, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    .line 1159229
    iput-object p1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1159230
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/6xt;
    .locals 2

    .prologue
    .line 1159224
    iget-object v0, p0, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    const-string v1, "card_issuer"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1159225
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/6xt;
    .locals 2

    .prologue
    .line 1159222
    iget-object v0, p0, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1159223
    return-object p0
.end method
