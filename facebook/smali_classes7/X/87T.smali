.class public LX/87T;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/87T;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/87P;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8GN;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/74n;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/86n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1300127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300128
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1300129
    iput-object v0, p0, LX/87T;->c:LX/0Ot;

    .line 1300130
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1300131
    iput-object v0, p0, LX/87T;->d:LX/0Ot;

    .line 1300132
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1300133
    iput-object v0, p0, LX/87T;->e:LX/0Ot;

    .line 1300134
    return-void
.end method

.method public static a(LX/0QB;)LX/87T;
    .locals 8

    .prologue
    .line 1300135
    sget-object v0, LX/87T;->f:LX/87T;

    if-nez v0, :cond_1

    .line 1300136
    const-class v1, LX/87T;

    monitor-enter v1

    .line 1300137
    :try_start_0
    sget-object v0, LX/87T;->f:LX/87T;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1300138
    if-eqz v2, :cond_0

    .line 1300139
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1300140
    new-instance v3, LX/87T;

    invoke-direct {v3}, LX/87T;-><init>()V

    .line 1300141
    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v4

    const/16 v5, 0x22a9

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x2e30

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2e13

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 p0, 0x2292

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1300142
    iput-object v4, v3, LX/87T;->a:LX/0Or;

    iput-object v5, v3, LX/87T;->b:LX/0Or;

    iput-object v6, v3, LX/87T;->c:LX/0Ot;

    iput-object v7, v3, LX/87T;->d:LX/0Ot;

    iput-object p0, v3, LX/87T;->e:LX/0Ot;

    .line 1300143
    move-object v0, v3

    .line 1300144
    sput-object v0, LX/87T;->f:LX/87T;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1300145
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1300146
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1300147
    :cond_1
    sget-object v0, LX/87T;->f:LX/87T;

    return-object v0

    .line 1300148
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1300149
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0io;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSpec$ProvidesInspirationDoodleParams;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
            ":",
            "LX/0is;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationTextParamsSpec$ProvidesInspirationTextParams;",
            ">(TModelData;)",
            "Lcom/facebook/composer/attachments/ComposerAttachment;"
        }
    .end annotation

    .prologue
    .line 1300150
    invoke-interface {p1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->f(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v7

    move-object v0, p1

    .line 1300151
    check-cast v0, LX/0is;

    invoke-static {v0}, LX/87Q;->a(LX/0is;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v4

    .line 1300152
    iget-object v0, p0, LX/87T;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/87P;

    .line 1300153
    invoke-virtual {v0, v7}, LX/87P;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1300154
    invoke-virtual {v7}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    if-nez v1, :cond_5

    invoke-static {}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->newBuilder()Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    .line 1300155
    :goto_0
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    invoke-virtual {v7}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v2

    :goto_1
    move-object v1, p1

    .line 1300156
    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v6

    move-object v0, p0

    .line 1300157
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1300158
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v8

    .line 1300159
    if-eqz v4, :cond_9

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getFrame()Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    move-result-object v9

    if-eqz v9, :cond_9

    .line 1300160
    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getFrame()Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->getFrame()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v9

    .line 1300161
    iget-object v8, v0, LX/87T;->c:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/8GN;

    const-string v12, ""

    invoke-virtual {v9}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v13

    move v10, v5

    move v11, v6

    invoke-virtual/range {v8 .. v13}, LX/8GN;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;IILjava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a()LX/0Px;

    move-result-object v8

    move-object v11, v8

    .line 1300162
    :goto_2
    iget-object v8, v0, LX/87T;->a:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    .line 1300163
    iget-object v9, v0, LX/87T;->e:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/86n;

    move-object v10, v1

    check-cast v10, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v10}, Lcom/facebook/composer/system/model/ComposerModelImpl;->p()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v10

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v9, v10, v12}, LX/86n;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;Landroid/content/res/Resources;)Lcom/facebook/photos/creativeediting/model/TextParams;

    move-result-object v9

    .line 1300164
    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->k()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    move-result-object v10

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 1300165
    if-eqz v10, :cond_0

    invoke-virtual {v10}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getUri()Landroid/net/Uri;

    move-result-object v8

    if-nez v8, :cond_a

    .line 1300166
    :cond_0
    const/4 v8, 0x0

    .line 1300167
    :goto_3
    move-object v10, v8

    .line 1300168
    invoke-static {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v8

    invoke-virtual {v8, v11}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setStickerParams(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v11

    if-eqz v9, :cond_7

    invoke-static {v9}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    :goto_4
    invoke-virtual {v11, v8}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setTextParams(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v9

    if-eqz v10, :cond_8

    invoke-static {v10}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    :goto_5
    invoke-virtual {v9, v8}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setDoodleParams(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v8

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setOriginalUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v8

    move-object v0, v8

    .line 1300169
    invoke-static {v7}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v1

    .line 1300170
    iput-object v0, v1, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1300171
    move-object v1, v1

    .line 1300172
    invoke-static {v7}, LX/7kq;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1300173
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1300174
    invoke-static {}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->newBuilder()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v2

    const/4 v4, 0x0

    .line 1300175
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1300176
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v7

    .line 1300177
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v4

    :goto_6
    if-ge v5, v8, :cond_1

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 1300178
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/StickerParams;->o()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1300179
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_6

    .line 1300180
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDoodleParams()LX/0Px;

    move-result-object v7

    .line 1300181
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v4

    :goto_7
    if-ge v5, v8, :cond_2

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    .line 1300182
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->n()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1300183
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_7

    .line 1300184
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v5

    .line 1300185
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    :goto_8
    if-ge v4, v7, :cond_3

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/creativeediting/model/TextParams;

    .line 1300186
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/TextParams;->p()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1300187
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_8

    .line 1300188
    :cond_3
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v3, v3

    .line 1300189
    invoke-virtual {v2, v3}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setGLRendererConfigs(LX/0Px;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMuted()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setIsVideoMuted(Z)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMirrorOn()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setShouldFlipHorizontally(Z)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v2

    move-object v0, v2

    .line 1300190
    iput-object v0, v1, LX/7kv;->f:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1300191
    const-string v0, "raw"

    .line 1300192
    iput-object v0, v1, LX/7kv;->i:Ljava/lang/String;

    .line 1300193
    :cond_4
    invoke-virtual {v1}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    return-object v0

    .line 1300194
    :cond_5
    invoke-virtual {v7}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    goto/16 :goto_0

    .line 1300195
    :cond_6
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 1300196
    :cond_7
    sget-object v8, LX/0Q7;->a:LX/0Px;

    move-object v8, v8

    .line 1300197
    goto/16 :goto_4

    .line 1300198
    :cond_8
    sget-object v8, LX/0Q7;->a:LX/0Px;

    move-object v8, v8

    .line 1300199
    goto/16 :goto_5

    :cond_9
    move-object v11, v8

    goto/16 :goto_2

    .line 1300200
    :cond_a
    invoke-virtual {v10}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getMediaRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v8

    .line 1300201
    new-instance v12, LX/5iF;

    invoke-virtual {v10}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getUri()Landroid/net/Uri;

    move-result-object v13

    invoke-direct {v12, v13}, LX/5iF;-><init>(Landroid/net/Uri;)V

    invoke-static {v8}, LX/63w;->a(Lcom/facebook/photos/creativeediting/model/PersistableRect;)F

    move-result v13

    .line 1300202
    iput v13, v12, LX/5iF;->d:F

    .line 1300203
    move-object v12, v12

    .line 1300204
    invoke-static {v8}, LX/63w;->b(Lcom/facebook/photos/creativeediting/model/PersistableRect;)F

    move-result v13

    .line 1300205
    iput v13, v12, LX/5iF;->e:F

    .line 1300206
    move-object v12, v12

    .line 1300207
    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getLeft()F

    move-result v13

    .line 1300208
    iput v13, v12, LX/5iF;->b:F

    .line 1300209
    move-object v12, v12

    .line 1300210
    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getTop()F

    move-result v8

    .line 1300211
    iput v8, v12, LX/5iF;->c:F

    .line 1300212
    move-object v8, v12

    .line 1300213
    const-string v12, "doodle"

    .line 1300214
    iput-object v12, v8, LX/5iF;->g:Ljava/lang/String;

    .line 1300215
    move-object v8, v8

    .line 1300216
    invoke-virtual {v8}, LX/5iF;->a()Lcom/facebook/photos/creativeediting/model/DoodleParams;

    move-result-object v8

    goto/16 :goto_3
.end method
