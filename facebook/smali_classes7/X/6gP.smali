.class public final LX/6gP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1125017
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 1125018
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125019
    :goto_0
    return v1

    .line 1125020
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125021
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_7

    .line 1125022
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1125023
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1125024
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1125025
    const-string v8, "image_assets"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1125026
    invoke-static {p0, p1}, LX/6gN;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1125027
    :cond_2
    const-string v8, "text_assets"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1125028
    invoke-static {p0, p1}, LX/6gV;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1125029
    :cond_3
    const-string v8, "thumbnail"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1125030
    invoke-static {p0, p1}, LX/6gO;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1125031
    :cond_4
    const-string v8, "thumbnail_image_assets"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1125032
    invoke-static {p0, p1}, LX/6gN;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1125033
    :cond_5
    const-string v8, "thumbnail_text_assets"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1125034
    invoke-static {p0, p1}, LX/6gV;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1125035
    :cond_6
    const-string v8, "unique_id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1125036
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1125037
    :cond_7
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1125038
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1125039
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1125040
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1125041
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1125042
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1125043
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1125044
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1125045
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1125046
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125047
    if-eqz v0, :cond_0

    .line 1125048
    const-string v1, "image_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125049
    invoke-static {p0, v0, p2, p3}, LX/6gN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1125050
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125051
    if-eqz v0, :cond_1

    .line 1125052
    const-string v1, "text_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125053
    invoke-static {p0, v0, p2, p3}, LX/6gV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1125054
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125055
    if-eqz v0, :cond_2

    .line 1125056
    const-string v1, "thumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125057
    invoke-static {p0, v0, p2}, LX/6gO;->a(LX/15i;ILX/0nX;)V

    .line 1125058
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125059
    if-eqz v0, :cond_3

    .line 1125060
    const-string v1, "thumbnail_image_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125061
    invoke-static {p0, v0, p2, p3}, LX/6gN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1125062
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125063
    if-eqz v0, :cond_4

    .line 1125064
    const-string v1, "thumbnail_text_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125065
    invoke-static {p0, v0, p2, p3}, LX/6gV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1125066
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1125067
    if-eqz v0, :cond_5

    .line 1125068
    const-string v1, "unique_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125069
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1125070
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1125071
    return-void
.end method
