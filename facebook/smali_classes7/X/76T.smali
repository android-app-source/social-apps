.class public LX/76T;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/76T;


# instance fields
.field public final a:LX/0W3;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7Cc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0W3;LX/0Ot;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "LX/0Or",
            "<",
            "LX/7Cc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171036
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1171037
    iput-object p1, p0, LX/76T;->a:LX/0W3;

    .line 1171038
    iput-object p2, p0, LX/76T;->b:LX/0Ot;

    .line 1171039
    iput-object p3, p0, LX/76T;->c:LX/0Or;

    .line 1171040
    return-void
.end method

.method public static a(LX/0QB;)LX/76T;
    .locals 6

    .prologue
    .line 1171041
    sget-object v0, LX/76T;->d:LX/76T;

    if-nez v0, :cond_1

    .line 1171042
    const-class v1, LX/76T;

    monitor-enter v1

    .line 1171043
    :try_start_0
    sget-object v0, LX/76T;->d:LX/76T;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1171044
    if-eqz v2, :cond_0

    .line 1171045
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1171046
    new-instance v4, LX/76T;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    const/16 v5, 0x3567

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x3564

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, LX/76T;-><init>(LX/0W3;LX/0Ot;LX/0Or;)V

    .line 1171047
    move-object v0, v4

    .line 1171048
    sput-object v0, LX/76T;->d:LX/76T;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1171049
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1171050
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1171051
    :cond_1
    sget-object v0, LX/76T;->d:LX/76T;

    return-object v0

    .line 1171052
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1171053
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
