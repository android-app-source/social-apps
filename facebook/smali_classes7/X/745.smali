.class public LX/745;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/73v;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Z

.field private static j:LX/0Xm;


# instance fields
.field private c:Ljava/lang/String;

.field private d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0XJ;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field private f:LX/74N;

.field private g:LX/74S;

.field public h:Ljava/lang/String;

.field private final i:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1167407
    const-class v0, LX/745;

    sput-object v0, LX/745;->a:Ljava/lang/Class;

    .line 1167408
    const-string v0, "MediaLogger"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, LX/745;->b:Z

    return-void
.end method

.method public constructor <init>(LX/0Zb;Ljava/lang/String;LX/0Or;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/user/gender/UserGender;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "Ljava/lang/String;",
            "LX/0Or",
            "<",
            "LX/0XJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1167409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1167410
    iput-object p1, p0, LX/745;->i:LX/0Zb;

    .line 1167411
    iput-object p2, p0, LX/745;->c:Ljava/lang/String;

    .line 1167412
    iput-object p3, p0, LX/745;->d:LX/0Or;

    .line 1167413
    return-void
.end method

.method public static a(LX/0QB;)LX/745;
    .locals 6

    .prologue
    .line 1167414
    const-class v1, LX/745;

    monitor-enter v1

    .line 1167415
    :try_start_0
    sget-object v0, LX/745;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1167416
    sput-object v2, LX/745;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1167417
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1167418
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1167419
    new-instance v5, LX/745;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/16 p0, 0x12c8

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/745;-><init>(LX/0Zb;Ljava/lang/String;LX/0Or;)V

    .line 1167420
    move-object v0, v5

    .line 1167421
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1167422
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/745;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1167423
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1167424
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/745;LX/74R;Ljava/util/Map;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74R;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1167425
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p1}, LX/74R;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1167426
    const-string v0, "composer"

    .line 1167427
    iput-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1167428
    if-nez p2, :cond_0

    .line 1167429
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object p2

    .line 1167430
    :cond_0
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1167431
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 1167432
    :cond_1
    iget-object v0, p0, LX/745;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1167433
    iget-object v0, p0, LX/745;->e:Ljava/lang/String;

    .line 1167434
    iput-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1167435
    :cond_2
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1167436
    invoke-virtual {v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->i(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1167437
    :cond_3
    sget-boolean v0, LX/745;->b:Z

    if-eqz v0, :cond_5

    .line 1167438
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1167439
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1167440
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1167441
    const-string v7, "%s%s:%s"

    const/4 p3, 0x3

    new-array p3, p3, [Ljava/lang/Object;

    if-eqz v1, :cond_4

    const-string v1, ""

    :goto_2
    aput-object v1, p3, v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    aput-object v1, p3, v3

    const/4 v1, 0x2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, p3, v1

    invoke-static {v7, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v4

    .line 1167442
    goto :goto_1

    .line 1167443
    :cond_4
    const-string v1, ", "

    goto :goto_2

    .line 1167444
    :cond_5
    iget-object v0, p0, LX/745;->i:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1167445
    return-void
.end method

.method public static d(LX/745;)Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1167446
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1167447
    iget-object v1, p0, LX/745;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167448
    iget-object v1, p0, LX/745;->e:Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167449
    iget-object v1, p0, LX/745;->f:LX/74N;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167450
    iget-object v1, p0, LX/745;->g:LX/74S;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167451
    const-string v1, "viewer_id"

    iget-object v2, p0, LX/745;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167452
    const-string v1, "viewing_session_id"

    iget-object v2, p0, LX/745;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167453
    const-string v1, "viewing_surface"

    iget-object v2, p0, LX/745;->f:LX/74N;

    iget-object v2, v2, LX/74N;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167454
    const-string v1, "referrer"

    iget-object v2, p0, LX/745;->g:LX/74S;

    iget-object v2, v2, LX/74S;->referrer:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167455
    iget-object v1, p0, LX/745;->h:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1167456
    const-string v1, "referrer_id"

    iget-object v2, p0, LX/745;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167457
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(LX/74N;)LX/745;
    .locals 0

    .prologue
    .line 1167458
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167459
    iput-object p1, p0, LX/745;->f:LX/74N;

    .line 1167460
    return-object p0
.end method

.method public final a(LX/74S;)LX/745;
    .locals 0

    .prologue
    .line 1167461
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167462
    iput-object p1, p0, LX/745;->g:LX/74S;

    .line 1167463
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/745;
    .locals 0

    .prologue
    .line 1167464
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167465
    iput-object p1, p0, LX/745;->e:Ljava/lang/String;

    .line 1167466
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1167467
    invoke-static {p0}, LX/745;->d(LX/745;)Ljava/util/HashMap;

    move-result-object v1

    .line 1167468
    const-string v0, "content_id"

    invoke-virtual {v1, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167469
    const-string v2, "actor_gender"

    iget-object v0, p0, LX/745;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0XJ;

    invoke-virtual {v0}, LX/0XJ;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167470
    if-eqz p3, :cond_0

    .line 1167471
    const-string v0, "photo_publish_time"

    invoke-virtual {v1, v0, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167472
    :cond_0
    if-eqz p4, :cond_1

    .line 1167473
    const-string v0, "num_faceboxes"

    invoke-virtual {v1, v0, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167474
    :cond_1
    if-eqz p10, :cond_2

    .line 1167475
    const-string v0, "photo_type"

    invoke-virtual {v1, v0, p10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167476
    :cond_2
    if-eqz p5, :cond_3

    .line 1167477
    const-string v0, "photo_tagged_ids"

    invoke-virtual {v1, v0, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167478
    :cond_3
    if-eqz p6, :cond_4

    .line 1167479
    const-string v0, "photo_privacy"

    invoke-virtual {v1, v0, p6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167480
    :cond_4
    if-eqz p7, :cond_5

    .line 1167481
    const-string v0, "photo_container_id"

    invoke-virtual {v1, v0, p7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167482
    :cond_5
    if-eqz p8, :cond_6

    .line 1167483
    const-string v0, "photo_container_privacy"

    invoke-virtual {v1, v0, p8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167484
    :cond_6
    if-eqz p2, :cond_7

    .line 1167485
    const-string v0, "owner_id"

    invoke-virtual {v1, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167486
    :cond_7
    if-eqz p9, :cond_8

    .line 1167487
    const-string v0, "user_relationship_to_photo_owner"

    invoke-virtual {v1, v0, p9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167488
    :cond_8
    sget-object v0, LX/74R;->PHOTO_SAVE_SUCCEEDED:LX/74R;

    invoke-static {p0, v0, v1, p1}, LX/745;->a(LX/745;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167489
    return-void
.end method
