.class public LX/8bf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/8bf;


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Qc;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Bf;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/2Sc;

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/7C0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/2Sc;LX/0Xl;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p5    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/3Qc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7Bf;",
            ">;",
            "LX/2Sc;",
            "LX/0Xl;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1372210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1372211
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/8bf;->e:Ljava/util/Set;

    .line 1372212
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/8bf;->f:Ljava/util/Set;

    .line 1372213
    iput-object p1, p0, LX/8bf;->a:Ljava/util/concurrent/ExecutorService;

    .line 1372214
    iput-object p2, p0, LX/8bf;->b:LX/0Ot;

    .line 1372215
    iput-object p3, p0, LX/8bf;->c:LX/0Ot;

    .line 1372216
    iput-object p4, p0, LX/8bf;->d:LX/2Sc;

    .line 1372217
    invoke-interface {p5}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string p1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance p2, LX/8be;

    invoke-direct {p2, p0}, LX/8be;-><init>(LX/8bf;)V

    invoke-interface {v0, p1, p2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 1372218
    return-void
.end method

.method public static a(LX/0QB;)LX/8bf;
    .locals 9

    .prologue
    .line 1372219
    sget-object v0, LX/8bf;->g:LX/8bf;

    if-nez v0, :cond_1

    .line 1372220
    const-class v1, LX/8bf;

    monitor-enter v1

    .line 1372221
    :try_start_0
    sget-object v0, LX/8bf;->g:LX/8bf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1372222
    if-eqz v2, :cond_0

    .line 1372223
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1372224
    new-instance v3, LX/8bf;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    const/16 v5, 0x113c

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x32bb

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v7

    check-cast v7, LX/2Sc;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v8

    check-cast v8, LX/0Xl;

    invoke-direct/range {v3 .. v8}, LX/8bf;-><init>(Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/2Sc;LX/0Xl;)V

    .line 1372225
    move-object v0, v3

    .line 1372226
    sput-object v0, LX/8bf;->g:LX/8bf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1372227
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1372228
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1372229
    :cond_1
    sget-object v0, LX/8bf;->g:LX/8bf;

    return-object v0

    .line 1372230
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1372231
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/8bf;)V
    .locals 3

    .prologue
    .line 1372232
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8bf;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1372233
    iget-object v0, p0, LX/8bf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Qc;

    iget-object v1, p0, LX/8bf;->e:Ljava/util/Set;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    sget-object v2, LX/8ca;->PROFILE_VIEW:LX/8ca;

    invoke-virtual {v0, v1, v2}, LX/3Qc;->a(LX/0Px;LX/8ca;)V

    .line 1372234
    iget-object v0, p0, LX/8bf;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1372235
    :cond_0
    iget-object v0, p0, LX/8bf;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1372236
    iget-object v0, p0, LX/8bf;->f:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1372237
    iget-object v1, p0, LX/8bf;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/search/bootstrap/PendingBootstrapEntitiesManager$2;

    invoke-direct {v2, p0, v0}, Lcom/facebook/search/bootstrap/PendingBootstrapEntitiesManager$2;-><init>(LX/8bf;LX/0Px;)V

    const v0, -0x1395b5af

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1372238
    iget-object v0, p0, LX/8bf;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1372239
    :cond_1
    monitor-exit p0

    return-void

    .line 1372240
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
