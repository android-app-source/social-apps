.class public LX/8Mf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/8Mf;


# instance fields
.field public final a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7md;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/7m8;

.field private final d:LX/0ad;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/0Ot;LX/7m8;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            "LX/0Ot",
            "<",
            "LX/7md;",
            ">;",
            "LX/7m8;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1334953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1334954
    iput-object p1, p0, LX/8Mf;->a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1334955
    iput-object p2, p0, LX/8Mf;->b:LX/0Ot;

    .line 1334956
    iput-object p3, p0, LX/8Mf;->c:LX/7m8;

    .line 1334957
    iput-object p4, p0, LX/8Mf;->d:LX/0ad;

    .line 1334958
    return-void
.end method

.method public static a(LX/0QB;)LX/8Mf;
    .locals 7

    .prologue
    .line 1334959
    sget-object v0, LX/8Mf;->e:LX/8Mf;

    if-nez v0, :cond_1

    .line 1334960
    const-class v1, LX/8Mf;

    monitor-enter v1

    .line 1334961
    :try_start_0
    sget-object v0, LX/8Mf;->e:LX/8Mf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1334962
    if-eqz v2, :cond_0

    .line 1334963
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1334964
    new-instance v6, LX/8Mf;

    invoke-static {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    const/16 v4, 0x19ef

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/7m8;->a(LX/0QB;)LX/7m8;

    move-result-object v4

    check-cast v4, LX/7m8;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {v6, v3, p0, v4, v5}, LX/8Mf;-><init>(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/0Ot;LX/7m8;LX/0ad;)V

    .line 1334965
    move-object v0, v6

    .line 1334966
    sput-object v0, LX/8Mf;->e:LX/8Mf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1334967
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1334968
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1334969
    :cond_1
    sget-object v0, LX/8Mf;->e:LX/8Mf;

    return-object v0

    .line 1334970
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1334971
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
