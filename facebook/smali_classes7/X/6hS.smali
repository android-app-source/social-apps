.class public final LX/6hS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:J

.field public final c:Z

.field public final d:Z

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Z


# direct methods
.method public constructor <init>(LX/6hR;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1128262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128263
    iget-object v0, p1, LX/6hR;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1128264
    iget-object v0, p1, LX/6hR;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/6hS;->a:J

    .line 1128265
    :goto_0
    iget-object v0, p1, LX/6hR;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1128266
    iget-object v0, p1, LX/6hR;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/6hS;->b:J

    .line 1128267
    :goto_1
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p1, LX/6hR;->d:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LX/6hS;->c:Z

    .line 1128268
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p1, LX/6hR;->e:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LX/6hS;->d:Z

    .line 1128269
    iget-object v0, p1, LX/6hR;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1128270
    iget-object v0, p1, LX/6hR;->f:Ljava/lang/String;

    iput-object v0, p0, LX/6hS;->e:Ljava/lang/String;

    .line 1128271
    :goto_2
    iget-object v0, p1, LX/6hR;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1128272
    iget-object v0, p1, LX/6hR;->g:Ljava/lang/String;

    iput-object v0, p0, LX/6hS;->f:Ljava/lang/String;

    .line 1128273
    :goto_3
    iget-object v0, p1, LX/6hR;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1128274
    iget-object v0, p1, LX/6hR;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/6hS;->g:Z

    .line 1128275
    :goto_4
    return-void

    .line 1128276
    :cond_0
    iput-wide v2, p0, LX/6hS;->a:J

    goto :goto_0

    .line 1128277
    :cond_1
    iput-wide v2, p0, LX/6hS;->b:J

    goto :goto_1

    .line 1128278
    :cond_2
    const-string v0, ""

    iput-object v0, p0, LX/6hS;->e:Ljava/lang/String;

    goto :goto_2

    .line 1128279
    :cond_3
    const-string v0, ""

    iput-object v0, p0, LX/6hS;->f:Ljava/lang/String;

    goto :goto_3

    .line 1128280
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6hS;->g:Z

    goto :goto_4
.end method
