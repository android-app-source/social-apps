.class public final LX/7L6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/FullScreenVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V
    .locals 0

    .prologue
    .line 1196815
    iput-object p1, p0, LX/7L6;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCompletion(Landroid/media/MediaPlayer;)V
    .locals 4

    .prologue
    .line 1196816
    iget-object v0, p0, LX/7L6;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-boolean v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->A:Z

    if-eqz v0, :cond_1

    .line 1196817
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 1196818
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    .line 1196819
    :cond_0
    :goto_0
    return-void

    .line 1196820
    :cond_1
    iget-object v0, p0, LX/7L6;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->f()V

    .line 1196821
    iget-object v0, p0, LX/7L6;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->as:Landroid/media/MediaPlayer$OnCompletionListener;

    if-eqz v0, :cond_2

    .line 1196822
    iget-object v0, p0, LX/7L6;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->as:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-interface {v0, p1}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    .line 1196823
    :cond_2
    iget-object v0, p0, LX/7L6;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ax:LX/16V;

    new-instance v1, LX/2qT;

    iget-object v2, p0, LX/7L6;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget v2, v2, Lcom/facebook/video/player/FullScreenVideoPlayer;->B:I

    sget-object v3, LX/7IF;->a:LX/7IF;

    invoke-direct {v1, v2, v3}, LX/2qT;-><init>(ILX/7IA;)V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 1196824
    iget-object v0, p0, LX/7L6;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-boolean v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ao:Z

    if-eqz v0, :cond_0

    .line 1196825
    iget-object v0, p0, LX/7L6;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a(LX/04g;)Z

    goto :goto_0
.end method
