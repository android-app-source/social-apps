.class public final enum LX/7xT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7xT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7xT;

.field public static final enum ITEM:LX/7xT;

.field public static final enum NOT_AVAILABLE_ITEM:LX/7xT;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1277413
    new-instance v0, LX/7xT;

    const-string v1, "ITEM"

    invoke-direct {v0, v1, v2}, LX/7xT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xT;->ITEM:LX/7xT;

    .line 1277414
    new-instance v0, LX/7xT;

    const-string v1, "NOT_AVAILABLE_ITEM"

    invoke-direct {v0, v1, v3}, LX/7xT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xT;->NOT_AVAILABLE_ITEM:LX/7xT;

    .line 1277415
    const/4 v0, 0x2

    new-array v0, v0, [LX/7xT;

    sget-object v1, LX/7xT;->ITEM:LX/7xT;

    aput-object v1, v0, v2

    sget-object v1, LX/7xT;->NOT_AVAILABLE_ITEM:LX/7xT;

    aput-object v1, v0, v3

    sput-object v0, LX/7xT;->$VALUES:[LX/7xT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1277412
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7xT;
    .locals 1

    .prologue
    .line 1277410
    const-class v0, LX/7xT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7xT;

    return-object v0
.end method

.method public static values()[LX/7xT;
    .locals 1

    .prologue
    .line 1277411
    sget-object v0, LX/7xT;->$VALUES:[LX/7xT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7xT;

    return-object v0
.end method
