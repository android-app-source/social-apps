.class public LX/8Me;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/8Me;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1334937
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1334938
    const-string v0, "compost/draft/?draft_id={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "draft_id"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/8Mc;

    invoke-direct {v1, p0}, LX/8Mc;-><init>(LX/8Me;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 1334939
    const-string v0, "compost/draft/"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/8Md;

    invoke-direct {v1, p0}, LX/8Md;-><init>(LX/8Me;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 1334940
    return-void
.end method

.method public static a(LX/0QB;)LX/8Me;
    .locals 3

    .prologue
    .line 1334941
    sget-object v0, LX/8Me;->a:LX/8Me;

    if-nez v0, :cond_1

    .line 1334942
    const-class v1, LX/8Me;

    monitor-enter v1

    .line 1334943
    :try_start_0
    sget-object v0, LX/8Me;->a:LX/8Me;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1334944
    if-eqz v2, :cond_0

    .line 1334945
    :try_start_1
    new-instance v0, LX/8Me;

    invoke-direct {v0}, LX/8Me;-><init>()V

    .line 1334946
    move-object v0, v0

    .line 1334947
    sput-object v0, LX/8Me;->a:LX/8Me;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1334948
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1334949
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1334950
    :cond_1
    sget-object v0, LX/8Me;->a:LX/8Me;

    return-object v0

    .line 1334951
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1334952
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
