.class public final LX/6us;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

.field public final synthetic b:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

.field public final synthetic c:LX/6uu;


# direct methods
.method public constructor <init>(LX/6uu;Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;Lcom/facebook/payments/ui/PrimaryCtaButtonView;)V
    .locals 0

    .prologue
    .line 1156222
    iput-object p1, p0, LX/6us;->c:LX/6uu;

    iput-object p2, p0, LX/6us;->a:Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    iput-object p3, p0, LX/6us;->b:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1156223
    iget-object v0, p0, LX/6us;->b:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b()V

    .line 1156224
    iget-object v0, p0, LX/6us;->b:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1156225
    new-instance v1, LX/31Y;

    invoke-direct {v1, v0}, LX/31Y;-><init>(Landroid/content/Context;)V

    const p0, 0x7f08003d

    invoke-virtual {v1, p0}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    const p0, 0x7f080016

    new-instance p1, LX/6ut;

    invoke-direct {p1}, LX/6ut;-><init>()V

    invoke-virtual {v1, p0, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 1156226
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1156227
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1156228
    const-string v1, "encoded_credential_id"

    iget-object v2, p0, LX/6us;->a:Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    invoke-virtual {v2}, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1156229
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1156230
    const-string v2, "extra_activity_result_data"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1156231
    iget-object v0, p0, LX/6us;->c:LX/6uu;

    iget-object v0, v0, LX/6uu;->d:LX/6qh;

    new-instance v2, LX/73T;

    sget-object v3, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v2, v3, v1}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {v0, v2}, LX/6qh;->a(LX/73T;)V

    .line 1156232
    return-void
.end method
