.class public LX/78i;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field private final a:LX/78g;

.field private final b:LX/78g;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:LX/78h;

.field private g:Landroid/graphics/Rect;

.field private h:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(LX/78g;LX/78g;IIILX/78h;)V
    .locals 1

    .prologue
    .line 1173297
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1173298
    iput-object p1, p0, LX/78i;->a:LX/78g;

    .line 1173299
    iput-object p2, p0, LX/78i;->b:LX/78g;

    .line 1173300
    iput p3, p0, LX/78i;->c:I

    .line 1173301
    iput p4, p0, LX/78i;->d:I

    .line 1173302
    iput p5, p0, LX/78i;->e:I

    .line 1173303
    iput-object p6, p0, LX/78i;->f:LX/78h;

    .line 1173304
    iget-object v0, p1, LX/78g;->c:Landroid/graphics/Rect;

    move-object v0, v0

    .line 1173305
    iput-object v0, p0, LX/78i;->g:Landroid/graphics/Rect;

    .line 1173306
    iget-object v0, p2, LX/78g;->c:Landroid/graphics/Rect;

    move-object v0, v0

    .line 1173307
    iput-object v0, p0, LX/78i;->h:Landroid/graphics/Rect;

    .line 1173308
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 5

    .prologue
    .line 1173309
    iget-object v0, p0, LX/78i;->f:LX/78h;

    sget-object v1, LX/78h;->EXPAND:LX/78h;

    if-ne v0, v1, :cond_1

    .line 1173310
    iget v0, p0, LX/78i;->c:I

    iget v1, p0, LX/78i;->d:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 1173311
    iget-object v1, p0, LX/78i;->g:Landroid/graphics/Rect;

    iget v2, p0, LX/78i;->c:I

    iget v3, p0, LX/78i;->d:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 1173312
    iget-object v1, p0, LX/78i;->g:Landroid/graphics/Rect;

    iget v2, p0, LX/78i;->e:I

    iget v3, p0, LX/78i;->c:I

    iget v4, p0, LX/78i;->d:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    add-int/2addr v2, v3

    sub-int v0, v2, v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1173313
    iget-object v0, p0, LX/78i;->a:LX/78g;

    iget-object v1, p0, LX/78i;->g:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, LX/78g;->setDrawableBounds(Landroid/graphics/Rect;)V

    .line 1173314
    iget-object v0, p0, LX/78i;->a:LX/78g;

    invoke-virtual {v0}, LX/78g;->invalidate()V

    .line 1173315
    iget v0, p0, LX/78i;->e:I

    iget v1, p0, LX/78i;->c:I

    sub-int/2addr v0, v1

    iget v1, p0, LX/78i;->d:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 1173316
    iget-object v1, p0, LX/78i;->h:Landroid/graphics/Rect;

    iget v2, p0, LX/78i;->c:I

    iget v3, p0, LX/78i;->d:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v3, p0, LX/78i;->e:I

    sub-int/2addr v2, v3

    add-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 1173317
    iget-object v1, p0, LX/78i;->h:Landroid/graphics/Rect;

    iget v2, p0, LX/78i;->c:I

    iget v3, p0, LX/78i;->d:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1173318
    iget-object v0, p0, LX/78i;->b:LX/78g;

    iget-object v1, p0, LX/78i;->h:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, LX/78g;->setDrawableBounds(Landroid/graphics/Rect;)V

    .line 1173319
    iget-object v0, p0, LX/78i;->b:LX/78g;

    invoke-virtual {v0}, LX/78g;->invalidate()V

    .line 1173320
    :cond_0
    :goto_0
    return-void

    .line 1173321
    :cond_1
    iget-object v0, p0, LX/78i;->f:LX/78h;

    sget-object v1, LX/78h;->COLLAPSE:LX/78h;

    if-ne v0, v1, :cond_0

    .line 1173322
    iget v0, p0, LX/78i;->c:I

    iget v1, p0, LX/78i;->d:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 1173323
    iget-object v1, p0, LX/78i;->g:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1173324
    iget-object v1, p0, LX/78i;->g:Landroid/graphics/Rect;

    iget v2, p0, LX/78i;->e:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1173325
    iget-object v0, p0, LX/78i;->a:LX/78g;

    iget-object v1, p0, LX/78i;->g:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, LX/78g;->setDrawableBounds(Landroid/graphics/Rect;)V

    .line 1173326
    iget-object v0, p0, LX/78i;->a:LX/78g;

    invoke-virtual {v0}, LX/78g;->invalidate()V

    .line 1173327
    iget v0, p0, LX/78i;->e:I

    iget v1, p0, LX/78i;->c:I

    sub-int/2addr v0, v1

    iget v1, p0, LX/78i;->d:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 1173328
    iget-object v1, p0, LX/78i;->h:Landroid/graphics/Rect;

    neg-int v2, v0

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 1173329
    iget-object v1, p0, LX/78i;->h:Landroid/graphics/Rect;

    iget v2, p0, LX/78i;->e:I

    sub-int v0, v2, v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1173330
    iget-object v0, p0, LX/78i;->b:LX/78g;

    iget-object v1, p0, LX/78i;->h:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, LX/78g;->setDrawableBounds(Landroid/graphics/Rect;)V

    .line 1173331
    iget-object v0, p0, LX/78i;->b:LX/78g;

    invoke-virtual {v0}, LX/78g;->invalidate()V

    goto :goto_0
.end method
