.class public final LX/8Ra;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 1344751
    iput-object p1, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1344745
    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/8Ra;->b:Z

    if-nez v0, :cond_1

    .line 1344746
    :cond_0
    :goto_0
    return-void

    .line 1344747
    :cond_1
    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->o(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)LX/8QY;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 1344748
    :goto_1
    if-nez v0, :cond_0

    .line 1344749
    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->d(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    goto :goto_0

    .line 1344750
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1344752
    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    iget-object v0, v0, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-ne v0, v1, :cond_0

    .line 1344753
    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1344754
    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    sget v1, LX/8Rk;->a:I

    invoke-virtual {v0, v1}, LX/8tB;->g(I)LX/621;

    move-result-object v0

    check-cast v0, LX/8Rn;

    .line 1344755
    invoke-virtual {v0}, LX/8Rn;->j()V

    .line 1344756
    :cond_0
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    .line 1344733
    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    invoke-virtual {v0}, LX/3Tf;->a()LX/333;

    move-result-object v0

    iget-object v1, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, LX/333;->a(Ljava/lang/CharSequence;)V

    .line 1344734
    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 1344735
    :goto_0
    iget-object v1, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v2, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v3, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v3, v3, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v2, v3}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v2

    .line 1344736
    iput-object v2, v1, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    .line 1344737
    if-nez v0, :cond_0

    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1344738
    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->s(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1344739
    :cond_0
    invoke-direct {p0}, LX/8Ra;->b()V

    .line 1344740
    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    iget-object v1, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    .line 1344741
    iput-object v1, v0, LX/8tB;->i:Ljava/util/List;

    .line 1344742
    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v1, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Ljava/util/List;)V

    .line 1344743
    return-void

    .line 1344744
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 1344729
    if-ge p4, p3, :cond_0

    .line 1344730
    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->o(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)LX/8QY;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1344731
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8Ra;->b:Z

    .line 1344732
    :cond_0
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 1344724
    if-ge p4, p3, :cond_0

    .line 1344725
    iget-object v0, p0, LX/8Ra;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d()V

    .line 1344726
    invoke-direct {p0}, LX/8Ra;->a()V

    .line 1344727
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8Ra;->b:Z

    .line 1344728
    :cond_0
    return-void
.end method
