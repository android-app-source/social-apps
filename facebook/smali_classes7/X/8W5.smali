.class public final enum LX/8W5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8W5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8W5;

.field public static final enum GAME_START:LX/8W5;

.field public static final enum REJECT_PROMISE:LX/8W5;

.field public static final enum RESOLVE_PROMISE:LX/8W5;

.field public static final enum RESTART:LX/8W5;


# instance fields
.field private mStringValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1353730
    new-instance v0, LX/8W5;

    const-string v1, "GAME_START"

    const-string v2, "gamestart"

    invoke-direct {v0, v1, v3, v2}, LX/8W5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8W5;->GAME_START:LX/8W5;

    .line 1353731
    new-instance v0, LX/8W5;

    const-string v1, "REJECT_PROMISE"

    const-string v2, "rejectpromise"

    invoke-direct {v0, v1, v4, v2}, LX/8W5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8W5;->REJECT_PROMISE:LX/8W5;

    .line 1353732
    new-instance v0, LX/8W5;

    const-string v1, "RESOLVE_PROMISE"

    const-string v2, "resolvepromise"

    invoke-direct {v0, v1, v5, v2}, LX/8W5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8W5;->RESOLVE_PROMISE:LX/8W5;

    .line 1353733
    new-instance v0, LX/8W5;

    const-string v1, "RESTART"

    const-string v2, "restart"

    invoke-direct {v0, v1, v6, v2}, LX/8W5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8W5;->RESTART:LX/8W5;

    .line 1353734
    const/4 v0, 0x4

    new-array v0, v0, [LX/8W5;

    sget-object v1, LX/8W5;->GAME_START:LX/8W5;

    aput-object v1, v0, v3

    sget-object v1, LX/8W5;->REJECT_PROMISE:LX/8W5;

    aput-object v1, v0, v4

    sget-object v1, LX/8W5;->RESOLVE_PROMISE:LX/8W5;

    aput-object v1, v0, v5

    sget-object v1, LX/8W5;->RESTART:LX/8W5;

    aput-object v1, v0, v6

    sput-object v0, LX/8W5;->$VALUES:[LX/8W5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1353726
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1353727
    iput-object p3, p0, LX/8W5;->mStringValue:Ljava/lang/String;

    .line 1353728
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8W5;
    .locals 1

    .prologue
    .line 1353735
    const-class v0, LX/8W5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8W5;

    return-object v0
.end method

.method public static values()[LX/8W5;
    .locals 1

    .prologue
    .line 1353729
    sget-object v0, LX/8W5;->$VALUES:[LX/8W5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8W5;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1353725
    iget-object v0, p0, LX/8W5;->mStringValue:Ljava/lang/String;

    return-object v0
.end method
