.class public final LX/76N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/76J;


# direct methods
.method public constructor <init>(LX/76J;)V
    .locals 0

    .prologue
    .line 1170992
    iput-object p1, p0, LX/76N;->a:LX/76J;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x61b571ab

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1170993
    const-string v1, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1170994
    const-string v1, "event"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, LX/2EU;->fromValue(I)LX/2EU;

    move-result-object v1

    .line 1170995
    iget-object v2, p0, LX/76N;->a:LX/76J;

    invoke-static {v2, v1}, LX/76J;->a$redex0(LX/76J;LX/2EU;)V

    .line 1170996
    :goto_0
    const v1, -0x7ed56e34

    invoke-static {v1, v0}, LX/02F;->e(II)V

    return-void

    .line 1170997
    :cond_0
    new-instance v1, LX/2gX;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v1, v2}, LX/2gX;-><init>(Landroid/os/Bundle;)V

    .line 1170998
    iget-object v2, v1, LX/2gX;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1170999
    iget-object v3, v1, LX/2gX;->b:[B

    move-object v1, v3

    .line 1171000
    iget-object v3, p0, LX/76N;->a:LX/76J;

    invoke-static {v3, v2, v1}, LX/76J;->b(LX/76J;Ljava/lang/String;[B)V

    goto :goto_0
.end method
