.class public LX/7Gl;
.super Landroid/text/style/BackgroundColorSpan;
.source ""

# interfaces
.implements LX/7Gh;


# instance fields
.field private final a:Lcom/facebook/tagging/model/TaggingProfile;

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/7Gk;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method public constructor <init>(IILX/7Gl;)V
    .locals 2

    .prologue
    .line 1189725
    invoke-direct {p0, p2}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    .line 1189726
    iput p1, p0, LX/7Gl;->c:I

    .line 1189727
    iget-object v0, p3, LX/7Gl;->a:Lcom/facebook/tagging/model/TaggingProfile;

    iput-object v0, p0, LX/7Gl;->a:Lcom/facebook/tagging/model/TaggingProfile;

    .line 1189728
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p3, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    .line 1189729
    return-void
.end method

.method public constructor <init>(IILcom/facebook/tagging/model/TaggingProfile;)V
    .locals 0

    .prologue
    .line 1189811
    invoke-direct {p0, p2}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    .line 1189812
    iput p1, p0, LX/7Gl;->c:I

    .line 1189813
    iput-object p3, p0, LX/7Gl;->a:Lcom/facebook/tagging/model/TaggingProfile;

    .line 1189814
    return-void
.end method

.method private static a(I)I
    .locals 1

    .prologue
    .line 1189815
    if-nez p0, :cond_0

    .line 1189816
    const/16 v0, 0x21

    .line 1189817
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x11

    goto :goto_0
.end method

.method public static b(Landroid/text/Editable;I)Z
    .locals 1

    .prologue
    .line 1189821
    if-ltz p1, :cond_0

    invoke-interface {p0}, Landroid/text/Editable;->length()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1189818
    iget-object v0, p0, LX/7Gl;->a:Lcom/facebook/tagging/model/TaggingProfile;

    .line 1189819
    iget-object p0, v0, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v0, p0

    .line 1189820
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/text/Editable;)I
    .locals 2

    .prologue
    .line 1189800
    iget-object v0, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/text/Editable;I)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1189801
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    .line 1189802
    invoke-direct {p0}, LX/7Gl;->e()Ljava/lang/String;

    move-result-object v1

    .line 1189803
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1189804
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1189805
    new-instance v5, LX/7Gk;

    invoke-direct {v5, p0, v4, p0}, LX/7Gk;-><init>(LX/7Gl;Ljava/lang/String;LX/7Gl;)V

    .line 1189806
    add-int v6, p2, v1

    add-int v7, p2, v1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v8}, LX/7Gl;->a(I)I

    move-result v8

    invoke-interface {p1, v5, v6, v7, v8}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 1189807
    iget-object v6, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1189808
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v1, v4

    .line 1189809
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1189810
    :cond_0
    return-void
.end method

.method public final a(Landroid/text/Editable;Z)V
    .locals 13

    .prologue
    const/16 v12, 0x20

    const/4 v6, -0x1

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1189743
    iget-object v0, p0, LX/7Gl;->a:Lcom/facebook/tagging/model/TaggingProfile;

    .line 1189744
    iget-object v1, v0, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v0, v1

    .line 1189745
    sget-object v1, LX/7Gr;->USER:LX/7Gr;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/7Gl;->a:Lcom/facebook/tagging/model/TaggingProfile;

    .line 1189746
    iget-object v1, v0, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v0, v1

    .line 1189747
    sget-object v1, LX/7Gr;->SELF:LX/7Gr;

    if-ne v0, v1, :cond_8

    :cond_0
    if-eqz p2, :cond_8

    .line 1189748
    iget-object v0, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v1, v2

    move v3, v2

    move v5, v6

    move v8, v6

    .line 1189749
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1189750
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Gk;

    .line 1189751
    invoke-interface {p1, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v9

    .line 1189752
    invoke-interface {p1, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    .line 1189753
    if-ltz v9, :cond_1

    if-ltz v7, :cond_1

    move v10, v4

    .line 1189754
    :goto_1
    if-nez v10, :cond_2

    .line 1189755
    invoke-interface {v11}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    move v10, v2

    .line 1189756
    goto :goto_1

    .line 1189757
    :cond_2
    invoke-virtual {v0, p1}, LX/7Gk;->a(Landroid/text/Editable;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 1189758
    if-ne v8, v6, :cond_12

    .line 1189759
    iget-object v3, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eq v0, v3, :cond_3

    if-gtz v9, :cond_4

    :cond_3
    move v1, v4

    move v3, v9

    .line 1189760
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->remove()V

    .line 1189761
    invoke-interface {p1, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    move v5, v7

    move v8, v3

    move v3, v4

    .line 1189762
    goto :goto_0

    .line 1189763
    :cond_4
    add-int/lit8 v3, v9, -0x1

    invoke-interface {p1, v3}, Landroid/text/Editable;->charAt(I)C

    move-result v3

    if-ne v3, v12, :cond_11

    .line 1189764
    add-int/lit8 v3, v9, -0x1

    goto :goto_2

    .line 1189765
    :cond_5
    if-eqz v1, :cond_10

    if-eqz v3, :cond_10

    if-lez v9, :cond_10

    add-int/lit8 v0, v9, -0x1

    invoke-interface {p1, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    if-ne v0, v12, :cond_10

    .line 1189766
    add-int/lit8 v5, v5, 0x1

    move v0, v5

    :goto_3
    move v3, v2

    move v5, v0

    .line 1189767
    goto :goto_0

    .line 1189768
    :cond_6
    if-ltz v8, :cond_7

    .line 1189769
    invoke-interface {p1, v8, v5}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 1189770
    :cond_7
    :goto_4
    iget-object v0, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_e

    .line 1189771
    iget-object v0, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Gk;

    .line 1189772
    invoke-interface {p1, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {p1, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v3

    invoke-static {v2}, LX/7Gl;->a(I)I

    move-result v4

    invoke-interface {p1, v0, v1, v3, v4}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 1189773
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1189774
    :cond_8
    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 1189775
    iget-object v0, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v3, 0x2

    if-ge v0, v3, :cond_13

    .line 1189776
    :cond_9
    :goto_5
    move v0, v1

    .line 1189777
    if-nez v0, :cond_a

    move v0, v4

    .line 1189778
    :goto_6
    iget-object v1, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    move v1, v0

    :goto_7
    if-ge v3, v5, :cond_b

    iget-object v0, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Gk;

    .line 1189779
    invoke-virtual {v0, p1}, LX/7Gk;->a(Landroid/text/Editable;)Z

    move-result v0

    if-nez v0, :cond_f

    move v0, v4

    .line 1189780
    :goto_8
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_7

    :cond_a
    move v0, v2

    .line 1189781
    goto :goto_6

    .line 1189782
    :cond_b
    if-eqz v1, :cond_7

    .line 1189783
    invoke-virtual {p0, p1}, LX/7Gl;->a(Landroid/text/Editable;)I

    move-result v3

    .line 1189784
    invoke-virtual {p0, p1}, LX/7Gl;->b(Landroid/text/Editable;)I

    move-result v4

    .line 1189785
    iget-object v0, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    :goto_9
    if-ge v1, v5, :cond_c

    iget-object v0, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Gk;

    .line 1189786
    invoke-interface {p1, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 1189787
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    .line 1189788
    :cond_c
    invoke-interface {p1, p0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 1189789
    if-eqz p2, :cond_d

    if-ltz v3, :cond_d

    if-ltz v4, :cond_d

    .line 1189790
    invoke-interface {p1, v3, v4}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 1189791
    :cond_d
    iget-object v0, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_4

    .line 1189792
    :cond_e
    return-void

    :cond_f
    move v0, v1

    goto :goto_8

    :cond_10
    move v0, v5

    goto/16 :goto_3

    :cond_11
    move v3, v9

    goto/16 :goto_2

    :cond_12
    move v3, v8

    goto/16 :goto_2

    .line 1189793
    :cond_13
    iget-object v0, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    move v3, v0

    move v0, v1

    .line 1189794
    :goto_a
    iget-object v6, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v0, v6, :cond_9

    .line 1189795
    iget-object v6, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {p1, v6}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    .line 1189796
    invoke-static {p1, v6}, LX/7Gl;->b(Landroid/text/Editable;I)Z

    move-result v7

    if-eqz v7, :cond_14

    invoke-static {p1, v3}, LX/7Gl;->b(Landroid/text/Editable;I)Z

    move-result v7

    if-eqz v7, :cond_14

    invoke-interface {p1, v3}, Landroid/text/Editable;->charAt(I)C

    move-result v7

    const/16 v8, 0x20

    if-ne v7, v8, :cond_14

    sub-int v3, v6, v3

    if-eq v3, v1, :cond_15

    :cond_14
    move v1, v5

    .line 1189797
    goto/16 :goto_5

    .line 1189798
    :cond_15
    iget-object v3, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v3}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v3

    .line 1189799
    add-int/lit8 v0, v0, 0x1

    goto :goto_a
.end method

.method public final b(Landroid/text/Editable;)I
    .locals 2

    .prologue
    .line 1189742
    iget-object v0, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    iget-object v1, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final b()J
    .locals 4

    .prologue
    .line 1189739
    iget-object v0, p0, LX/7Gl;->a:Lcom/facebook/tagging/model/TaggingProfile;

    .line 1189740
    iget-wide v2, v0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v0, v2

    .line 1189741
    return-wide v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 2

    .prologue
    .line 1189734
    sget-object v0, LX/7Gj;->a:[I

    iget-object v1, p0, LX/7Gl;->a:Lcom/facebook/tagging/model/TaggingProfile;

    .line 1189735
    iget-object p0, v1, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v1, p0

    .line 1189736
    invoke-virtual {v1}, LX/7Gr;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1189737
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x285feb

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    :goto_0
    return-object v0

    .line 1189738
    :pswitch_0
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x25d6af

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1189733
    iget-object v0, p0, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1189730
    invoke-super {p0, p1}, Landroid/text/style/BackgroundColorSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1189731
    iget v0, p0, LX/7Gl;->c:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1189732
    return-void
.end method
