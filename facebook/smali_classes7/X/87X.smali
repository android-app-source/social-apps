.class public LX/87X;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1300285
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    sput-object v0, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1300286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300287
    return-void
.end method

.method public static a(FF)F
    .locals 2

    .prologue
    .line 1300288
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 1300289
    invoke-virtual {v0, p0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1300290
    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v1

    .line 1300291
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    move-result v0

    .line 1300292
    int-to-float v0, v0

    div-float v0, p1, v0

    return v0
.end method

.method public static a(II)I
    .locals 3

    .prologue
    .line 1300293
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public static a(LX/5RY;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1300294
    sget-object v1, LX/87W;->b:[I

    invoke-virtual {p0}, LX/5RY;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1300295
    :goto_0
    :pswitch_0
    return v0

    .line 1300296
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/1c9;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
            "LX/1c9",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1300297
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, LX/1c9;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1300298
    invoke-virtual {p1, v1}, LX/1c9;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1300299
    invoke-static {p0, v0}, LX/87X;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1300300
    :goto_1
    return v1

    .line 1300301
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1300302
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/GradientDrawable;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1300303
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1300304
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1300305
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    .line 1300306
    const/4 v3, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_2
    packed-switch v3, :pswitch_data_0

    .line 1300307
    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    :goto_3
    move-object v3, v3

    .line 1300308
    const/4 v4, 0x2

    new-array v4, v4, [I

    invoke-static {p0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    aput v5, v4, v2

    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    aput v2, v4, v1

    invoke-direct {v0, v3, v4}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    return-object v0

    :cond_1
    move v0, v2

    .line 1300309
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1300310
    goto :goto_1

    .line 1300311
    :sswitch_0
    const-string v4, "BL_TR"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x0

    goto :goto_2

    :sswitch_1
    const-string v4, "BOTTOM_TOP"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x1

    goto :goto_2

    :sswitch_2
    const-string v4, "BR_TL"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x2

    goto :goto_2

    :sswitch_3
    const-string v4, "LEFT_RIGHT"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x3

    goto :goto_2

    :sswitch_4
    const-string v4, "RIGHT_LEFT"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x4

    goto :goto_2

    :sswitch_5
    const-string v4, "TL_BR"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x5

    goto :goto_2

    :sswitch_6
    const-string v4, "TOP_BOTTOM"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x6

    goto :goto_2

    :sswitch_7
    const-string v4, "TR_BL"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x7

    goto :goto_2

    .line 1300312
    :pswitch_0
    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->BL_TR:Landroid/graphics/drawable/GradientDrawable$Orientation;

    goto :goto_3

    .line 1300313
    :pswitch_1
    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->BOTTOM_TOP:Landroid/graphics/drawable/GradientDrawable$Orientation;

    goto :goto_3

    .line 1300314
    :pswitch_2
    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->BR_TL:Landroid/graphics/drawable/GradientDrawable$Orientation;

    goto :goto_3

    .line 1300315
    :pswitch_3
    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    goto :goto_3

    .line 1300316
    :pswitch_4
    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->RIGHT_LEFT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    goto :goto_3

    .line 1300317
    :pswitch_5
    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->TL_BR:Landroid/graphics/drawable/GradientDrawable$Orientation;

    goto :goto_3

    .line 1300318
    :pswitch_6
    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    goto :goto_3

    .line 1300319
    :pswitch_7
    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->TR_BL:Landroid/graphics/drawable/GradientDrawable$Orientation;

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        -0x474c0adf -> :sswitch_1
        -0x340c9b96 -> :sswitch_4
        -0x19e09c3c -> :sswitch_3
        0x3c60a93 -> :sswitch_0
        0x3c8c4c7 -> :sswitch_2
        0x4c3af77 -> :sswitch_5
        0x4c669ab -> :sswitch_7
        0x7625f075 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1300320
    if-ne p0, p1, :cond_1

    .line 1300321
    const/4 v0, 0x1

    .line 1300322
    :cond_0
    :goto_0
    return v0

    .line 1300323
    :cond_1
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 1300324
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getPresetId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getPresetId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1300325
    invoke-static {p0, p1}, LX/87Y;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result v0

    goto :goto_0

    .line 1300326
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getPresetId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getPresetId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1300327
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getPresetId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getPresetId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
