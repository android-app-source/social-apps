.class public LX/8BT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/8BS;

.field private final b:LX/8Bg;

.field private final c:LX/8Bc;

.field private final d:LX/8BX;

.field public final e:LX/6b9;

.field public f:Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;

.field public g:Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;

.field public h:Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;

.field public i:Lcom/facebook/photos/base/media/VideoItem;

.field public j:Lcom/facebook/media/upload/MediaUploadParameters;

.field public k:LX/8BO;

.field private l:LX/6b7;

.field public m:Z

.field public n:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/8Bg;LX/8Bc;LX/8BX;LX/8BO;LX/6b9;LX/6b7;)V
    .locals 1
    .param p6    # LX/6b7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1309260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309261
    iput-object p4, p0, LX/8BT;->k:LX/8BO;

    .line 1309262
    iput-object p1, p0, LX/8BT;->b:LX/8Bg;

    .line 1309263
    iput-object p2, p0, LX/8BT;->c:LX/8Bc;

    .line 1309264
    iput-object p3, p0, LX/8BT;->d:LX/8BX;

    .line 1309265
    iput-object p5, p0, LX/8BT;->e:LX/6b9;

    .line 1309266
    iput-object p6, p0, LX/8BT;->l:LX/6b7;

    .line 1309267
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8BT;->m:Z

    .line 1309268
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/media/MediaItem;Lcom/facebook/media/upload/MediaUploadParameters;Z)LX/8BM;
    .locals 10

    .prologue
    .line 1309269
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v1, LX/4gF;->VIDEO:LX/4gF;

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    const-string v1, "VideoUploadSession should be given a video file"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1309270
    iget-object v0, p0, LX/8BT;->i:Lcom/facebook/photos/base/media/VideoItem;

    if-nez v0, :cond_5

    .line 1309271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1309272
    iget-object v1, p2, Lcom/facebook/media/upload/MediaUploadParameters;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1309273
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_VideoUploadSessionState"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8BT;->n:Ljava/lang/String;

    .line 1309274
    check-cast p1, Lcom/facebook/photos/base/media/VideoItem;

    iput-object p1, p0, LX/8BT;->i:Lcom/facebook/photos/base/media/VideoItem;

    .line 1309275
    iput-object p2, p0, LX/8BT;->j:Lcom/facebook/media/upload/MediaUploadParameters;

    .line 1309276
    iget-object v0, p0, LX/8BT;->b:LX/8Bg;

    iget-object v1, p0, LX/8BT;->l:LX/6b7;

    .line 1309277
    new-instance p2, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v2

    check-cast v2, LX/11H;

    const-class p1, LX/8BR;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/8BR;

    invoke-direct {p2, v2, p1, v1}, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;-><init>(LX/11H;LX/8BR;LX/6b7;)V

    .line 1309278
    move-object v0, p2

    .line 1309279
    iput-object v0, p0, LX/8BT;->f:Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;

    .line 1309280
    iget-object v0, p0, LX/8BT;->c:LX/8Bc;

    iget-object v1, p0, LX/8BT;->l:LX/6b7;

    .line 1309281
    new-instance v3, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v4

    check-cast v4, LX/11H;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/7zS;->a(LX/0QB;)LX/7zS;

    move-result-object v6

    check-cast v6, LX/7zS;

    const-class v7, LX/8BR;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/8BR;

    invoke-static {v0}, LX/6b9;->a(LX/0QB;)LX/6b9;

    move-result-object v8

    check-cast v8, LX/6b9;

    move-object v9, v1

    invoke-direct/range {v3 .. v9}, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;-><init>(LX/11H;LX/0So;LX/7zS;LX/8BR;LX/6b9;LX/6b7;)V

    .line 1309282
    move-object v0, v3

    .line 1309283
    iput-object v0, p0, LX/8BT;->g:Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;

    .line 1309284
    iget-object v0, p0, LX/8BT;->d:LX/8BX;

    iget-object v1, p0, LX/8BT;->l:LX/6b7;

    .line 1309285
    new-instance v4, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v2

    check-cast v2, LX/11H;

    const-class v3, LX/8BR;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/8BR;

    invoke-direct {v4, v2, v3, v1}, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;-><init>(LX/11H;LX/8BR;LX/6b7;)V

    .line 1309286
    move-object v0, v4

    .line 1309287
    iput-object v0, p0, LX/8BT;->h:Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;

    .line 1309288
    iput-boolean p3, p0, LX/8BT;->m:Z

    .line 1309289
    iget-object v0, p0, LX/8BT;->e:LX/6b9;

    iget-object v1, p0, LX/8BT;->n:Ljava/lang/String;

    const-class v2, LX/8BS;

    invoke-virtual {v0, v1, v2}, LX/6b9;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8BS;

    .line 1309290
    if-eqz v0, :cond_4

    iget-object v1, v0, LX/8BS;->mVideoPath:Ljava/lang/String;

    iget-object v2, p0, LX/8BT;->i:Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1309291
    iput-object v0, p0, LX/8BT;->a:LX/8BS;

    .line 1309292
    :goto_1
    iget-object v0, p0, LX/8BT;->a:LX/8BS;

    iget-object v0, v0, LX/8BS;->mStartResponse:LX/8Bh;

    if-nez v0, :cond_0

    .line 1309293
    iget-object v0, p0, LX/8BT;->a:LX/8BS;

    iget-object v1, p0, LX/8BT;->f:Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;

    iget-object v2, p0, LX/8BT;->i:Lcom/facebook/photos/base/media/VideoItem;

    iget-object v3, p0, LX/8BT;->j:Lcom/facebook/media/upload/MediaUploadParameters;

    iget-object v4, p0, LX/8BT;->k:LX/8BO;

    invoke-virtual {v1, v2, v3, v4}, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->a(Lcom/facebook/photos/base/media/VideoItem;Lcom/facebook/media/upload/MediaUploadParameters;LX/8BO;)LX/8Bh;

    move-result-object v1

    iput-object v1, v0, LX/8BS;->mStartResponse:LX/8Bh;

    .line 1309294
    iget-object v0, p0, LX/8BT;->e:LX/6b9;

    iget-object v1, p0, LX/8BT;->n:Ljava/lang/String;

    iget-object v2, p0, LX/8BT;->a:LX/8BS;

    invoke-virtual {v0, v1, v2}, LX/6b9;->a(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1309295
    :cond_0
    iget-object v0, p0, LX/8BT;->k:LX/8BO;

    const-string v1, "Before transferring file"

    invoke-virtual {v0, v1}, LX/8BO;->a(Ljava/lang/String;)V

    .line 1309296
    iget-object v0, p0, LX/8BT;->a:LX/8BS;

    iget-boolean v0, v0, LX/8BS;->mVideoTransferred:Z

    if-nez v0, :cond_1

    .line 1309297
    iget-object v0, p0, LX/8BT;->g:Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;

    iget-object v1, p0, LX/8BT;->i:Lcom/facebook/photos/base/media/VideoItem;

    iget-object v2, p0, LX/8BT;->j:Lcom/facebook/media/upload/MediaUploadParameters;

    iget-object v3, p0, LX/8BT;->a:LX/8BS;

    iget-object v3, v3, LX/8BS;->mStartResponse:LX/8Bh;

    iget-object v4, p0, LX/8BT;->k:LX/8BO;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->a(Lcom/facebook/photos/base/media/VideoItem;Lcom/facebook/media/upload/MediaUploadParameters;LX/8Bh;LX/8BO;)V

    .line 1309298
    iget-object v0, p0, LX/8BT;->a:LX/8BS;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8BS;->mVideoTransferred:Z

    .line 1309299
    iget-object v0, p0, LX/8BT;->e:LX/6b9;

    iget-object v1, p0, LX/8BT;->n:Ljava/lang/String;

    iget-object v2, p0, LX/8BT;->a:LX/8BS;

    invoke-virtual {v0, v1, v2}, LX/6b9;->a(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1309300
    :cond_1
    iget-object v0, p0, LX/8BT;->k:LX/8BO;

    const-string v1, "After transferring file"

    invoke-virtual {v0, v1}, LX/8BO;->a(Ljava/lang/String;)V

    .line 1309301
    iget-object v0, p0, LX/8BT;->j:Lcom/facebook/media/upload/MediaUploadParameters;

    .line 1309302
    iget-boolean v1, v0, Lcom/facebook/media/upload/MediaUploadParameters;->c:Z

    move v0, v1

    .line 1309303
    if-nez v0, :cond_2

    .line 1309304
    iget-object v0, p0, LX/8BT;->a:LX/8BS;

    iget-boolean v0, v0, LX/8BS;->mVideoPosted:Z

    if-nez v0, :cond_2

    .line 1309305
    iget-object v0, p0, LX/8BT;->h:Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;

    iget-object v1, p0, LX/8BT;->a:LX/8BS;

    iget-object v1, v1, LX/8BS;->mStartResponse:LX/8Bh;

    iget-object v2, p0, LX/8BT;->j:Lcom/facebook/media/upload/MediaUploadParameters;

    iget-object v3, p0, LX/8BT;->k:LX/8BO;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->a(LX/8Bh;Lcom/facebook/media/upload/MediaUploadParameters;LX/8BO;)Ljava/lang/Boolean;

    .line 1309306
    iget-object v0, p0, LX/8BT;->a:LX/8BS;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8BS;->mVideoPosted:Z

    .line 1309307
    iget-object v0, p0, LX/8BT;->e:LX/6b9;

    iget-object v1, p0, LX/8BT;->n:Ljava/lang/String;

    iget-object v2, p0, LX/8BT;->a:LX/8BS;

    invoke-virtual {v0, v1, v2}, LX/6b9;->a(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1309308
    :cond_2
    new-instance v0, LX/8BM;

    iget-object v1, p0, LX/8BT;->a:LX/8BS;

    iget-object v1, v1, LX/8BS;->mStartResponse:LX/8Bh;

    .line 1309309
    iget-object v2, v1, LX/8Bh;->mSessionFbid:Ljava/lang/String;

    move-object v1, v2

    .line 1309310
    iget-object v2, p0, LX/8BT;->a:LX/8BS;

    iget-object v2, v2, LX/8BS;->mStartResponse:LX/8Bh;

    .line 1309311
    iget-object v3, v2, LX/8Bh;->mVideoFbid:Ljava/lang/String;

    move-object v2, v3

    .line 1309312
    invoke-direct {v0, v1, v2}, LX/8BM;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 1309313
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1309314
    :cond_4
    new-instance v0, LX/8BS;

    iget-object v1, p0, LX/8BT;->i:Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8BS;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/8BT;->a:LX/8BS;

    goto/16 :goto_1

    .line 1309315
    :cond_5
    iget-object v0, p0, LX/8BT;->i:Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Upload session should not be reused for different files."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto/16 :goto_1
.end method
