.class public LX/7RT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1206748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(II)Landroid/hardware/Camera;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 1206686
    new-instance v6, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v6}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    move v5, v3

    move-object v1, v0

    .line 1206687
    :goto_0
    const/4 v2, 0x2

    if-ge v5, v2, :cond_4

    move v2, v3

    .line 1206688
    :goto_1
    const/4 v4, 0x5

    if-ge v2, v4, :cond_3

    if-nez v1, :cond_3

    .line 1206689
    :try_start_0
    const/4 v8, 0x0

    .line 1206690
    const/4 v4, 0x0

    .line 1206691
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v9

    move v7, v8

    .line 1206692
    :goto_2
    if-ge v7, v9, :cond_0

    .line 1206693
    invoke-static {v7, v6}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1206694
    iget v10, v6, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v10, p0, :cond_a

    .line 1206695
    const v4, -0x47c97e84

    invoke-static {v7, v4}, LX/0J2;->a(II)Landroid/hardware/Camera;

    move-result-object v4

    .line 1206696
    :cond_0
    if-nez v4, :cond_2

    .line 1206697
    const-string v4, "[CAMERA_SETUP]"

    const-string v7, "No camera %d found; opening default"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v8

    invoke-static {v4, v7, v9}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1206698
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v7

    .line 1206699
    invoke-static {}, LX/0Bx;->c()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1206700
    invoke-static {v7}, LX/0Bx;->a(Landroid/hardware/Camera;)V

    .line 1206701
    :cond_1
    move-object v4, v7

    .line 1206702
    :cond_2
    move-object v1, v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1206703
    :cond_3
    if-nez v1, :cond_4

    .line 1206704
    const-string v2, "[CAMERA_SETUP]"

    const-string v4, "Unable to open the requested camera, trying the next one"

    invoke-static {v2, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1206705
    add-int/lit8 v2, p0, 0x1

    rem-int/lit8 p0, v2, 0x2

    .line 1206706
    const-wide/16 v8, 0x7d0

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V

    .line 1206707
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_0

    .line 1206708
    :catch_0
    move-exception v4

    .line 1206709
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v0, v4

    goto :goto_1

    .line 1206710
    :cond_4
    if-nez v1, :cond_5

    .line 1206711
    const-string v1, "Failure exception cannot be null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1206712
    throw v0

    .line 1206713
    :cond_5
    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 1206714
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedVideoSizes()Ljava/util/List;

    move-result-object v2

    .line 1206715
    if-nez v2, :cond_6

    .line 1206716
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v2

    .line 1206717
    :cond_6
    const/16 v3, 0x500

    const/16 v4, 0x2d0

    sget-object v5, LX/6ID;->SMALLER_THAN_OR_EQUAL_TO:LX/6ID;

    invoke-static {v2, v3, v4, v5}, LX/6IF;->a(Ljava/util/List;IILX/6ID;)Landroid/hardware/Camera$Size;

    move-result-object v2

    .line 1206718
    if-eqz v2, :cond_7

    .line 1206719
    iget v3, v2, Landroid/hardware/Camera$Size;->width:I

    iget v2, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v0, v3, v2}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 1206720
    :cond_7
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v2

    const-string v3, "continuous-video"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1206721
    const-string v2, "continuous-video"

    invoke-virtual {v0, v2}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 1206722
    :cond_8
    const/16 v2, 0x7530

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1206723
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object v3

    .line 1206724
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    .line 1206725
    aget v5, v3, v8

    aget v7, v3, v9

    if-ne v5, v7, :cond_9

    aget v5, v3, v8

    if-ne v5, v2, :cond_9

    .line 1206726
    aget v4, v3, v8

    aget v5, v3, v9

    invoke-virtual {v0, v4, v5}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    .line 1206727
    :goto_3
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/hardware/Camera$Parameters;->setRecordingHint(Z)V

    .line 1206728
    :try_start_1
    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1206729
    :goto_4
    invoke-static {v1, v6, p1}, LX/7RT;->a(Landroid/hardware/Camera;Landroid/hardware/Camera$CameraInfo;I)V

    .line 1206730
    return-object v1

    .line 1206731
    :catch_1
    move-exception v0

    .line 1206732
    const-string v2, "[CAMERA_SETUP]"

    const-string v3, "Unable to set the parameters for the camera. Moving along"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 1206733
    :cond_a
    :try_start_2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1206734
    :cond_b
    const/4 v3, 0x2

    new-array v3, v3, [I

    .line 1206735
    invoke-virtual {v0, v3}, Landroid/hardware/Camera$Parameters;->getPreviewFpsRange([I)V

    .line 1206736
    aget v4, v3, v8

    if-ne v4, v2, :cond_c

    .line 1206737
    goto :goto_3

    .line 1206738
    :cond_c
    aget v4, v3, v9

    if-ne v4, v2, :cond_d

    .line 1206739
    goto :goto_3

    .line 1206740
    :cond_d
    goto :goto_3
.end method

.method public static a(Landroid/hardware/Camera;Landroid/hardware/Camera$CameraInfo;I)V
    .locals 3

    .prologue
    .line 1206741
    iget v0, p1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    .line 1206742
    iget v1, p1, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1206743
    add-int/2addr v0, p2

    rem-int/lit16 v0, v0, 0x168

    .line 1206744
    rsub-int v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    .line 1206745
    :goto_0
    invoke-virtual {p0, v0}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 1206746
    return-void

    .line 1206747
    :cond_0
    sub-int/2addr v0, p2

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0
.end method
