.class public LX/8Be;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/8Bf;",
        "LX/8Bh;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1309660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 11

    .prologue
    .line 1309661
    check-cast p1, LX/8Bf;

    const/4 v8, 0x1

    .line 1309662
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1309663
    iget-object v1, p1, LX/8Bf;->b:Lcom/facebook/media/upload/MediaUploadParameters;

    move-object v1, v1

    .line 1309664
    iget-object v2, v1, Lcom/facebook/media/upload/MediaUploadParameters;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1309665
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1309666
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "composer_session_id"

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1309667
    :cond_0
    iget-object v3, v1, Lcom/facebook/media/upload/MediaUploadParameters;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1309668
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1309669
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "original_file_hash"

    invoke-direct {v4, v5, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1309670
    :cond_1
    iget-wide v9, p1, LX/8Bf;->a:J

    move-wide v4, v9

    .line 1309671
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_2

    .line 1309672
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "file_size"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v6, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1309673
    :cond_2
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "upload_phase"

    const-string v5, "start"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1309674
    iget-object v3, v1, Lcom/facebook/media/upload/MediaUploadParameters;->e:Ljava/util/List;

    if-nez v3, :cond_3

    .line 1309675
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1309676
    :goto_0
    move-object v3, v3

    .line 1309677
    invoke-virtual {v0, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1309678
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "v2.3/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1309679
    iget-object v4, v1, Lcom/facebook/media/upload/MediaUploadParameters;->b:Ljava/lang/String;

    move-object v1, v4

    .line 1309680
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/videos"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1309681
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v3

    const-string v4, "media-upload-video-chunk-start"

    .line 1309682
    iput-object v4, v3, LX/14O;->b:Ljava/lang/String;

    .line 1309683
    move-object v3, v3

    .line 1309684
    const-string v4, "POST"

    .line 1309685
    iput-object v4, v3, LX/14O;->c:Ljava/lang/String;

    .line 1309686
    move-object v3, v3

    .line 1309687
    iput-object v1, v3, LX/14O;->d:Ljava/lang/String;

    .line 1309688
    move-object v1, v3

    .line 1309689
    sget-object v3, LX/14S;->JSON:LX/14S;

    .line 1309690
    iput-object v3, v1, LX/14O;->k:LX/14S;

    .line 1309691
    move-object v1, v1

    .line 1309692
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1309693
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1309694
    move-object v0, v1

    .line 1309695
    iput-boolean v8, v0, LX/14O;->n:Z

    .line 1309696
    move-object v0, v0

    .line 1309697
    iput-boolean v8, v0, LX/14O;->p:Z

    .line 1309698
    move-object v0, v0

    .line 1309699
    iput-object v2, v0, LX/14O;->A:Ljava/lang/String;

    .line 1309700
    move-object v0, v0

    .line 1309701
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :cond_3
    iget-object v3, v1, Lcom/facebook/media/upload/MediaUploadParameters;->e:Ljava/util/List;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1309702
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1309703
    const/4 v8, 0x0

    .line 1309704
    const-string v1, "skip_upload"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1309705
    const-string v1, "skip_upload"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->F()Z

    move-result v8

    .line 1309706
    :cond_0
    new-instance v1, LX/8Bh;

    const-string v2, "upload_session_id"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v2

    const-string v3, "video_id"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-virtual {v3}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v3

    const-string v4, "start_offset"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->D()J

    move-result-wide v4

    const-string v6, "end_offset"

    invoke-virtual {v0, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->D()J

    move-result-wide v6

    invoke-direct/range {v1 .. v8}, LX/8Bh;-><init>(Ljava/lang/String;Ljava/lang/String;JJZ)V

    return-object v1
.end method
