.class public final enum LX/6xY;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6LU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6xY;",
        ">;",
        "LX/6LU",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6xY;

.field public static final enum CHECKOUT:LX/6xY;

.field public static final enum INVOICE:LX/6xY;

.field public static final enum PAYMENTS_SETTINGS:LX/6xY;


# instance fields
.field private mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1158916
    new-instance v0, LX/6xY;

    const-string v1, "CHECKOUT"

    const-string v2, "checkout"

    invoke-direct {v0, v1, v3, v2}, LX/6xY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xY;->CHECKOUT:LX/6xY;

    .line 1158917
    new-instance v0, LX/6xY;

    const-string v1, "INVOICE"

    const-string v2, "invoice"

    invoke-direct {v0, v1, v4, v2}, LX/6xY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xY;->INVOICE:LX/6xY;

    .line 1158918
    new-instance v0, LX/6xY;

    const-string v1, "PAYMENTS_SETTINGS"

    const-string v2, "payments_settings"

    invoke-direct {v0, v1, v5, v2}, LX/6xY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xY;->PAYMENTS_SETTINGS:LX/6xY;

    .line 1158919
    const/4 v0, 0x3

    new-array v0, v0, [LX/6xY;

    sget-object v1, LX/6xY;->CHECKOUT:LX/6xY;

    aput-object v1, v0, v3

    sget-object v1, LX/6xY;->INVOICE:LX/6xY;

    aput-object v1, v0, v4

    sget-object v1, LX/6xY;->PAYMENTS_SETTINGS:LX/6xY;

    aput-object v1, v0, v5

    sput-object v0, LX/6xY;->$VALUES:[LX/6xY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1158920
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1158921
    iput-object p3, p0, LX/6xY;->mValue:Ljava/lang/String;

    .line 1158922
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6xY;
    .locals 1

    .prologue
    .line 1158912
    const-class v0, LX/6xY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xY;

    return-object v0
.end method

.method public static values()[LX/6xY;
    .locals 1

    .prologue
    .line 1158915
    sget-object v0, LX/6xY;->$VALUES:[LX/6xY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6xY;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1158914
    invoke-virtual {p0}, LX/6xY;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1158913
    iget-object v0, p0, LX/6xY;->mValue:Ljava/lang/String;

    return-object v0
.end method
