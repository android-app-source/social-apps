.class public LX/6ma;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final fbpushdata:LX/6mK;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1145193
    new-instance v0, LX/1sv;

    const-string v1, "PushNotificationMessage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6ma;->b:LX/1sv;

    .line 1145194
    new-instance v0, LX/1sw;

    const-string v1, "fbpushdata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ma;->c:LX/1sw;

    .line 1145195
    sput-boolean v3, LX/6ma;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6mK;)V
    .locals 0

    .prologue
    .line 1145196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1145197
    iput-object p1, p0, LX/6ma;->fbpushdata:LX/6mK;

    .line 1145198
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1145199
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1145200
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1145201
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1145202
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PushNotificationMessage"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1145203
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145204
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145205
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145206
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145207
    const-string v4, "fbpushdata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145208
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145209
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145210
    iget-object v0, p0, LX/6ma;->fbpushdata:LX/6mK;

    if-nez v0, :cond_3

    .line 1145211
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145212
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145213
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145214
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1145215
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 1145216
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 1145217
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 1145218
    :cond_3
    iget-object v0, p0, LX/6ma;->fbpushdata:LX/6mK;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1145219
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1145220
    iget-object v0, p0, LX/6ma;->fbpushdata:LX/6mK;

    if-eqz v0, :cond_0

    .line 1145221
    sget-object v0, LX/6ma;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145222
    iget-object v0, p0, LX/6ma;->fbpushdata:LX/6mK;

    invoke-virtual {v0, p1}, LX/6mK;->a(LX/1su;)V

    .line 1145223
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1145224
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1145225
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1145226
    if-nez p1, :cond_1

    .line 1145227
    :cond_0
    :goto_0
    return v0

    .line 1145228
    :cond_1
    instance-of v1, p1, LX/6ma;

    if-eqz v1, :cond_0

    .line 1145229
    check-cast p1, LX/6ma;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1145230
    if-nez p1, :cond_3

    .line 1145231
    :cond_2
    :goto_1
    move v0, v2

    .line 1145232
    goto :goto_0

    .line 1145233
    :cond_3
    iget-object v0, p0, LX/6ma;->fbpushdata:LX/6mK;

    if-eqz v0, :cond_6

    move v0, v1

    .line 1145234
    :goto_2
    iget-object v3, p1, LX/6ma;->fbpushdata:LX/6mK;

    if-eqz v3, :cond_7

    move v3, v1

    .line 1145235
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1145236
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145237
    iget-object v0, p0, LX/6ma;->fbpushdata:LX/6mK;

    iget-object v3, p1, LX/6ma;->fbpushdata:LX/6mK;

    invoke-virtual {v0, v3}, LX/6mK;->a(LX/6mK;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 1145238
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1145239
    goto :goto_2

    :cond_7
    move v3, v2

    .line 1145240
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1145241
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1145242
    sget-boolean v0, LX/6ma;->a:Z

    .line 1145243
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6ma;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1145244
    return-object v0
.end method
