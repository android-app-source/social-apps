.class public final LX/6zt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wK",
        "<",
        "LX/708;",
        "Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1161639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161640
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;)LX/0Px;
    .locals 3

    .prologue
    .line 1161641
    check-cast p1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;

    .line 1161642
    iget-object v0, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 1161643
    check-cast v0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;

    .line 1161644
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1161645
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1161646
    iget-object p0, v1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    move-object v1, p0

    .line 1161647
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1161648
    sget-object v1, LX/708;->SELECT_PAYMENT_METHOD:LX/708;

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1161649
    :cond_0
    :goto_0
    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1161650
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f:LX/0Px;

    move-object v0, v1

    .line 1161651
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1161652
    sget-object v0, LX/708;->NEW_PAYMENT_OPTION:LX/708;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1161653
    sget-object v0, LX/708;->SINGLE_ROW_DIVIDER:LX/708;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1161654
    :cond_1
    sget-object v0, LX/708;->SECURITY_FOOTER:LX/708;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1161655
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1161656
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    iget-boolean v1, v1, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->c:Z

    if-eqz v1, :cond_0

    .line 1161657
    sget-object v1, LX/708;->COUNTRY_SELECTOR:LX/708;

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1161658
    sget-object v1, LX/708;->SINGLE_ROW_DIVIDER:LX/708;

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0
.end method
