.class public final LX/8aB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    .line 1366919
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1366920
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1366921
    :goto_0
    return v6

    .line 1366922
    :cond_0
    const-string v11, "location_radius"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1366923
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v1

    .line 1366924
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_5

    .line 1366925
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1366926
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1366927
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1366928
    const-string v11, "location_latlong"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1366929
    invoke-static {p0, p1}, LX/4aX;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1366930
    :cond_2
    const-string v11, "location_subtitle"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1366931
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1366932
    :cond_3
    const-string v11, "location_title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1366933
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1366934
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1366935
    :cond_5
    const/4 v10, 0x4

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1366936
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 1366937
    if-eqz v0, :cond_6

    move-object v0, p1

    .line 1366938
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1366939
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1366940
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1366941
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_0

    :cond_7
    move v0, v6

    move v7, v6

    move v8, v6

    move-wide v2, v4

    move v9, v6

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1366913
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1366914
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1366915
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/8aB;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366916
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1366917
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1366918
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1366888
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1366889
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1366890
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1366891
    invoke-static {p0, p1}, LX/8aB;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1366892
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1366893
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1366894
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1366895
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366896
    if-eqz v0, :cond_0

    .line 1366897
    const-string v1, "location_latlong"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366898
    invoke-static {p0, v0, p2}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 1366899
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1366900
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_1

    .line 1366901
    const-string v2, "location_radius"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366902
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1366903
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1366904
    if-eqz v0, :cond_2

    .line 1366905
    const-string v1, "location_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366906
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366907
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1366908
    if-eqz v0, :cond_3

    .line 1366909
    const-string v1, "location_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366910
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366911
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1366912
    return-void
.end method
