.class public final LX/7xZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;)V
    .locals 0

    .prologue
    .line 1277508
    iput-object p1, p0, LX/7xZ;->a:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, 0x50c8d430

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 1277503
    iget-object v0, p0, LX/7xZ;->a:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->p:LX/1nQ;

    iget-object v1, p0, LX/7xZ;->a:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->i:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    iget-object v1, v1, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->b:Ljava/lang/String;

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->TICKETING_ONSITE_FLOW:Lcom/facebook/events/common/ActionMechanism;

    iget-object v3, p0, LX/7xZ;->a:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    iget-object v3, v3, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->i:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    iget-object v3, v3, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->c:Ljava/lang/String;

    const-string v4, "event_ticketing"

    iget-object v5, p0, LX/7xZ;->a:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    iget-object v5, v5, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->i:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    iget-object v5, v5, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->a:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, LX/1nQ;->b(Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1277504
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1277505
    iget-object v1, p0, LX/7xZ;->a:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1277506
    iget-object v1, p0, LX/7xZ;->a:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->q:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/7xZ;->a:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    invoke-virtual {v2}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1277507
    const v0, -0x32a38b58

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
