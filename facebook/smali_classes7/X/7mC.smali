.class public LX/7mC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/composer/publish/common/PublishPostParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0SG;

.field private final c:LX/0lB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1236618
    const-class v0, LX/7mC;

    sput-object v0, LX/7mC;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1236619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236620
    iput-object p1, p0, LX/7mC;->b:LX/0SG;

    .line 1236621
    iput-object p2, p0, LX/7mC;->c:LX/0lB;

    .line 1236622
    return-void
.end method

.method public static a(LX/0QB;)LX/7mC;
    .locals 1

    .prologue
    .line 1236623
    invoke-static {p0}, LX/7mC;->b(LX/0QB;)LX/7mC;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1pN;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1236624
    invoke-virtual {p0}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/7mC;
    .locals 3

    .prologue
    .line 1236625
    new-instance v2, LX/7mC;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v1

    check-cast v1, LX/0lB;

    invoke-direct {v2, v0, v1}, LX/7mC;-><init>(LX/0SG;LX/0lB;)V

    .line 1236626
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/publish/common/PublishPostParams;)LX/14N;
    .locals 8

    .prologue
    .line 1236627
    iget-object v0, p0, LX/7mC;->b:LX/0SG;

    iget-object v1, p0, LX/7mC;->c:LX/0lB;

    invoke-static {p1, v0, v1}, LX/7mB;->a(Lcom/facebook/composer/publish/common/PublishPostParams;LX/0SG;LX/0lB;)Ljava/util/List;

    move-result-object v4

    .line 1236628
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1236629
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "name"

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->name:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236630
    :cond_0
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->caption:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1236631
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "caption"

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->caption:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236632
    :cond_1
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->description:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1236633
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "description"

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->description:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236634
    :cond_2
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->picture:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1236635
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "picture"

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->picture:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236636
    :cond_3
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isPhotoContainer:Z

    if-eqz v0, :cond_4

    .line 1236637
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "is_photo_container"

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236638
    :cond_4
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    if-eqz v0, :cond_5

    .line 1236639
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "greeting_card"

    iget-object v2, p0, LX/7mC;->c:LX/0lB;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-virtual {v2, v3}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236640
    :cond_5
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->e:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1236641
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "content_attachment"

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236642
    :cond_6
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    if-eqz v0, :cond_7

    .line 1236643
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "prompt_id"

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iget-object v2, v2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236644
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "prompt_type"

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iget-object v2, v2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptType:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236645
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "prompt_tracking_string"

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iget-object v2, v2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->trackingString:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236646
    :cond_7
    iget-wide v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->albumId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_8

    .line 1236647
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "album_id"

    iget-object v2, p0, LX/7mC;->c:LX/0lB;

    iget-wide v6, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->albumId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236648
    :cond_8
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isGroupMemberBioPost:Z

    if-eqz v0, :cond_9

    .line 1236649
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "is_member_bio_post"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236650
    :cond_9
    const-string v0, "%s/feed"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v6, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1236651
    new-instance v0, LX/14N;

    const-string v1, "graphObjectPosts"

    const-string v2, "POST"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)LX/14N;
    .locals 1

    .prologue
    .line 1236652
    check-cast p1, Lcom/facebook/composer/publish/common/PublishPostParams;

    invoke-virtual {p0, p1}, LX/7mC;->a(Lcom/facebook/composer/publish/common/PublishPostParams;)LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1236653
    invoke-static {p2}, LX/7mC;->a(LX/1pN;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
