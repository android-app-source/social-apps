.class public final LX/6tM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6E0;


# instance fields
.field public final a:LX/0W9;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/0W9;Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1154802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1154803
    iput-object p1, p0, LX/6tM;->a:LX/0W9;

    .line 1154804
    iput-object p2, p0, LX/6tM;->b:Landroid/content/Context;

    .line 1154805
    iput-object p3, p0, LX/6tM;->c:Landroid/content/res/Resources;

    .line 1154806
    return-void
.end method

.method public static a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;LX/6uW;)Lcom/facebook/payments/confirmation/ConfirmationCommonParams;
    .locals 1

    .prologue
    .line 1154807
    invoke-static {p0, p1, p2}, LX/6tM;->b(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;LX/6uW;)LX/6uM;

    move-result-object v0

    invoke-virtual {v0}, LX/6uM;->f()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6ws;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;
    .locals 2

    .prologue
    .line 1154834
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v0

    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0, v1}, LX/6wu;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6wu;

    move-result-object v0

    .line 1154835
    iput-object p1, v0, LX/6wu;->a:LX/6ws;

    .line 1154836
    move-object v0, v0

    .line 1154837
    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->w()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1154838
    iput-object v1, v0, LX/6wu;->c:LX/0am;

    .line 1154839
    move-object v0, v0

    .line 1154840
    invoke-virtual {v0}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/72g;)Lcom/facebook/payments/shipping/model/ShippingParams;
    .locals 2

    .prologue
    .line 1154841
    invoke-static {}, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->newBuilder()LX/72e;

    move-result-object v0

    .line 1154842
    iput-object p1, v0, LX/72e;->a:LX/72g;

    .line 1154843
    move-object v0, v0

    .line 1154844
    sget-object v1, LX/72f;->CHECKOUT:LX/72f;

    .line 1154845
    iput-object v1, v0, LX/72e;->e:LX/72f;

    .line 1154846
    move-object v0, v0

    .line 1154847
    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-static {v1}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v1

    .line 1154848
    iput-object v1, v0, LX/72e;->f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1154849
    move-object v0, v0

    .line 1154850
    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1154851
    iput-object v1, v0, LX/72e;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1154852
    move-object v0, v0

    .line 1154853
    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    .line 1154854
    iput-object v1, v0, LX/72e;->i:LX/6xg;

    .line 1154855
    move-object v0, v0

    .line 1154856
    invoke-virtual {v0}, LX/72e;->j()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;LX/0Px;)Z
    .locals 5
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1154857
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1154858
    :goto_0
    return v0

    .line 1154859
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    .line 1154860
    iget-object v4, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1154861
    const/4 v0, 0x1

    goto :goto_0

    .line 1154862
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 1154863
    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/6tM;
    .locals 4

    .prologue
    .line 1154864
    new-instance v3, LX/6tM;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v0

    check-cast v0, LX/0W9;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-direct {v3, v0, v1, v2}, LX/6tM;-><init>(LX/0W9;Landroid/content/Context;Landroid/content/res/Resources;)V

    .line 1154865
    return-object v3
.end method

.method public static b(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;LX/6uW;)LX/6uM;
    .locals 3

    .prologue
    .line 1154866
    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    .line 1154867
    sget-object v1, LX/6rp;->AUTHENTICATION:LX/6rp;

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->e()Lcom/facebook/payments/model/PaymentsPin;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/payments/model/PaymentsPin;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1154868
    :goto_0
    new-instance v1, LX/6uM;

    invoke-direct {v1}, LX/6uM;-><init>()V

    move-object v1, v1

    .line 1154869
    iput-object p2, v1, LX/6uM;->a:LX/6uW;

    .line 1154870
    move-object v1, v1

    .line 1154871
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v2

    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object p2

    iget-object p2, p2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v2, p2}, LX/6wu;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6wu;

    move-result-object v2

    sget-object p2, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    .line 1154872
    iput-object p2, v2, LX/6wu;->a:LX/6ws;

    .line 1154873
    move-object v2, v2

    .line 1154874
    invoke-virtual {v2}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v2

    move-object v2, v2

    .line 1154875
    iput-object v2, v1, LX/6uM;->d:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1154876
    move-object v1, v1

    .line 1154877
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;->a()Ljava/lang/String;

    move-result-object v2

    .line 1154878
    iput-object v2, v1, LX/6uM;->e:Ljava/lang/String;

    .line 1154879
    move-object v1, v1

    .line 1154880
    iput-boolean v0, v1, LX/6uM;->b:Z

    .line 1154881
    move-object v0, v1

    .line 1154882
    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    .line 1154883
    iput-object v1, v0, LX/6uM;->c:LX/6xg;

    .line 1154884
    move-object v0, v0

    .line 1154885
    return-object v0

    .line 1154886
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)Lcom/facebook/payments/confirmation/ConfirmationParams;
    .locals 1

    .prologue
    .line 1154887
    sget-object v0, LX/6uW;->SIMPLE:LX/6uW;

    invoke-static {p1, p2, v0}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;LX/6uW;)Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1154888
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    .line 1154889
    invoke-virtual {v0}, LX/0Rf;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1154890
    invoke-static {v0}, LX/0Ph;->d(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6vb;

    .line 1154891
    invoke-static {}, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->newBuilder()LX/6v2;

    move-result-object v1

    invoke-virtual {v0}, LX/6vb;->getContactInfoFormStyle()LX/6vY;

    move-result-object v0

    .line 1154892
    iput-object v0, v1, LX/6v2;->a:LX/6vY;

    .line 1154893
    move-object v0, v1

    .line 1154894
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-static {v1}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v1

    .line 1154895
    iput-object v1, v0, LX/6v2;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1154896
    move-object v0, v0

    .line 1154897
    invoke-virtual {v0}, LX/6v2;->f()Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    move-result-object v0

    .line 1154898
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;
    .locals 4
    .param p2    # Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1154899
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    invoke-virtual {v0}, LX/6xg;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-static {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a(Ljava/lang/String;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6xw;

    move-result-object v1

    if-nez p2, :cond_0

    sget-object v0, LX/6xZ;->ADD_CARD:LX/6xZ;

    .line 1154900
    :goto_0
    iput-object v0, v1, LX/6xw;->c:LX/6xZ;

    .line 1154901
    move-object v0, v1

    .line 1154902
    invoke-virtual {v0}, LX/6xw;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    move-result-object v0

    .line 1154903
    invoke-static {}, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->newBuilder()LX/6yR;

    move-result-object v1

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-static {v2}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v2

    .line 1154904
    iput-object v2, v1, LX/6yR;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1154905
    move-object v1, v1

    .line 1154906
    invoke-virtual {v1}, LX/6yR;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    move-result-object v1

    .line 1154907
    sget-object v2, LX/6yO;->SIMPLE:LX/6yO;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    invoke-static {v2, v0, v3}, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a(LX/6yO;Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;LX/6xg;)LX/6xy;

    move-result-object v0

    .line 1154908
    iput-object p2, v0, LX/6xy;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    .line 1154909
    move-object v0, v0

    .line 1154910
    iput-object v1, v0, LX/6xy;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    .line 1154911
    move-object v0, v0

    .line 1154912
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->t()Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    move-result-object v1

    .line 1154913
    iget-object v2, v1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->b:Lcom/facebook/common/locale/Country;

    move-object v1, v2

    .line 1154914
    iput-object v1, v0, LX/6xy;->g:Lcom/facebook/common/locale/Country;

    .line 1154915
    move-object v0, v0

    .line 1154916
    invoke-virtual {v0}, LX/6xy;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    return-object v0

    .line 1154917
    :cond_0
    sget-object v0, LX/6xZ;->UPDATE_CARD:LX/6xZ;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/71C;LX/0Px;)Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/CheckoutData;",
            "LX/71C;",
            "LX/0Px",
            "<",
            "LX/6zQ;",
            ">;)",
            "Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;"
        }
    .end annotation

    .prologue
    .line 1154918
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v1, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    .line 1154919
    sget-object v0, LX/6xZ;->SELECT_PAYMENT_METHOD:LX/6xZ;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-static {v0, v2}, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->a(LX/6xZ;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/718;

    move-result-object v0

    invoke-virtual {v1}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 1154920
    iput-object v2, v0, LX/718;->c:Ljava/lang/String;

    .line 1154921
    move-object v0, v0

    .line 1154922
    invoke-virtual {v0}, LX/718;->a()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v2

    .line 1154923
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->newBuilder()LX/71E;

    move-result-object v0

    sget-object v3, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    invoke-static {p1, v3}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6ws;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v3

    .line 1154924
    iput-object v3, v0, LX/71E;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1154925
    move-object v3, v0

    .line 1154926
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->s()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1154927
    sget-object v4, LX/708;->SELECT_PAYMENT_METHOD:LX/708;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->s()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/71E;->a(LX/6vZ;Ljava/lang/String;)LX/71E;

    .line 1154928
    :cond_0
    invoke-static {}, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;->newBuilder()LX/707;

    move-result-object v0

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/checkout/CheckoutCommonParams;->q:Ljava/lang/String;

    .line 1154929
    iput-object v4, v0, LX/707;->c:Ljava/lang/String;

    .line 1154930
    move-object v0, v0

    .line 1154931
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/checkout/CheckoutCommonParams;->i:Lorg/json/JSONObject;

    .line 1154932
    iput-object v4, v0, LX/707;->b:Lorg/json/JSONObject;

    .line 1154933
    move-object v0, v0

    .line 1154934
    invoke-virtual {v0}, LX/707;->e()Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;

    move-result-object v0

    .line 1154935
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->newBuilder()LX/71A;

    move-result-object v4

    .line 1154936
    iput-object v2, v4, LX/71A;->a:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 1154937
    move-object v2, v4

    .line 1154938
    invoke-virtual {v3}, LX/71E;->c()Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    move-result-object v3

    .line 1154939
    iput-object v3, v2, LX/71A;->e:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    .line 1154940
    move-object v2, v2

    .line 1154941
    iput-object p2, v2, LX/71A;->b:LX/71C;

    .line 1154942
    move-object v2, v2

    .line 1154943
    iput-object v1, v2, LX/71A;->c:LX/6xg;

    .line 1154944
    move-object v1, v2

    .line 1154945
    iget-object v2, p0, LX/6tM;->c:Landroid/content/res/Resources;

    const v3, 0x7f080c7a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1154946
    iput-object v2, v1, LX/71A;->d:Ljava/lang/String;

    .line 1154947
    move-object v1, v1

    .line 1154948
    iput-object v0, v1, LX/71A;->f:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    .line 1154949
    move-object v0, v1

    .line 1154950
    invoke-virtual {v0}, LX/71A;->h()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    .line 1154951
    invoke-static {}, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->newBuilder()LX/6zf;

    move-result-object v1

    .line 1154952
    iput-object v0, v1, LX/6zf;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 1154953
    move-object v0, v1

    .line 1154954
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-boolean v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->t:Z

    .line 1154955
    iput-boolean v1, v0, LX/6zf;->c:Z

    .line 1154956
    move-object v0, v0

    .line 1154957
    iput-object p3, v0, LX/6zf;->b:LX/0Px;

    .line 1154958
    move-object v0, v0

    .line 1154959
    invoke-virtual {v0}, LX/6zf;->e()Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;
    .locals 6

    .prologue
    .line 1154808
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v1, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    .line 1154809
    sget-object v0, LX/6xZ;->SELECT_CHECKOUT_OPTION:LX/6xZ;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-static {v0, v2}, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->a(LX/6xZ;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/718;

    move-result-object v0

    invoke-virtual {v1}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 1154810
    iput-object v2, v0, LX/718;->c:Ljava/lang/String;

    .line 1154811
    move-object v0, v0

    .line 1154812
    invoke-virtual {v0}, LX/718;->a()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v2

    .line 1154813
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->newBuilder()LX/71E;

    move-result-object v0

    sget-object v3, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    invoke-static {p1, v3}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6ws;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v3

    .line 1154814
    iput-object v3, v0, LX/71E;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1154815
    move-object v3, v0

    .line 1154816
    sget-object v4, LX/71X;->PAYMENTS_PICKER_OPTION:LX/71X;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->u()LX/0P1;

    move-result-object v0

    iget-object v5, p2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v3, v4, v0}, LX/71E;->a(LX/6vZ;Ljava/lang/String;)LX/71E;

    move-result-object v0

    invoke-virtual {v0}, LX/71E;->c()Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    move-result-object v0

    .line 1154817
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->newBuilder()LX/71A;

    move-result-object v3

    .line 1154818
    iput-object v0, v3, LX/71A;->e:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    .line 1154819
    move-object v0, v3

    .line 1154820
    iput-object v2, v0, LX/71A;->a:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 1154821
    move-object v0, v0

    .line 1154822
    sget-object v2, LX/71C;->PAYMENTS_OPTIONS_PICKER:LX/71C;

    .line 1154823
    iput-object v2, v0, LX/71A;->b:LX/71C;

    .line 1154824
    move-object v0, v0

    .line 1154825
    iput-object v1, v0, LX/71A;->c:LX/6xg;

    .line 1154826
    move-object v0, v0

    .line 1154827
    iget-object v1, p2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->d:Ljava/lang/String;

    .line 1154828
    iput-object v1, v0, LX/71A;->d:Ljava/lang/String;

    .line 1154829
    move-object v0, v0

    .line 1154830
    invoke-virtual {v0}, LX/71A;->h()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    .line 1154831
    iget-object v1, p2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->f:LX/0Px;

    invoke-static {v1}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v1

    new-instance v2, LX/6tL;

    invoke-direct {v2, p0}, LX/6tL;-><init>(LX/6tM;)V

    invoke-virtual {v1, v2}, LX/0wv;->a(LX/0QK;)LX/0wv;

    move-result-object v1

    invoke-virtual {v1}, LX/0wv;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 1154832
    new-instance v2, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;

    iget-object v3, p2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v0, v1}, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;-><init>(Ljava/lang/String;Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;LX/0Px;)V

    return-object v2

    .line 1154833
    :cond_0
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->u()LX/0P1;

    move-result-object v0

    iget-object v5, p2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    iget-object v0, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/71C;)Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;
    .locals 5

    .prologue
    .line 1154646
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v1, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    .line 1154647
    sget-object v0, LX/6xZ;->SELECT_SHIPPING_METHOD:LX/6xZ;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-static {v0, v2}, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->a(LX/6xZ;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/718;

    move-result-object v0

    invoke-virtual {v1}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 1154648
    iput-object v2, v0, LX/718;->c:Ljava/lang/String;

    .line 1154649
    move-object v0, v0

    .line 1154650
    invoke-virtual {v0}, LX/718;->a()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v2

    .line 1154651
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->newBuilder()LX/71E;

    move-result-object v0

    sget-object v3, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    invoke-static {p1, v3}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6ws;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v3

    .line 1154652
    iput-object v3, v0, LX/71E;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1154653
    move-object v3, v0

    .line 1154654
    sget-object v4, LX/72x;->SHIPPING_OPTIONS:LX/72x;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->j()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->j()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/ShippingOption;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingOption;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v4, v0}, LX/71E;->a(LX/6vZ;Ljava/lang/String;)LX/71E;

    move-result-object v0

    invoke-virtual {v0}, LX/71E;->c()Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    move-result-object v0

    .line 1154655
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->newBuilder()LX/71A;

    move-result-object v3

    .line 1154656
    iput-object v0, v3, LX/71A;->e:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    .line 1154657
    move-object v0, v3

    .line 1154658
    iput-object v2, v0, LX/71A;->a:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 1154659
    move-object v0, v0

    .line 1154660
    iput-object p2, v0, LX/71A;->b:LX/71C;

    .line 1154661
    move-object v0, v0

    .line 1154662
    iput-object v1, v0, LX/71A;->c:LX/6xg;

    .line 1154663
    move-object v0, v0

    .line 1154664
    iget-object v1, p0, LX/6tM;->c:Landroid/content/res/Resources;

    const v2, 0x7f081e68

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1154665
    iput-object v1, v0, LX/71A;->d:Ljava/lang/String;

    .line 1154666
    move-object v0, v0

    .line 1154667
    invoke-virtual {v0}, LX/71A;->h()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    .line 1154668
    new-instance v1, LX/72q;

    invoke-direct {v1}, LX/72q;-><init>()V

    move-object v1, v1

    .line 1154669
    iput-object v0, v1, LX/72q;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 1154670
    move-object v0, v1

    .line 1154671
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->k()LX/0Px;

    move-result-object v1

    .line 1154672
    iput-object v1, v0, LX/72q;->b:LX/0Px;

    .line 1154673
    move-object v0, v0

    .line 1154674
    new-instance v1, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;

    invoke-direct {v1, v0}, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;-><init>(LX/72q;)V

    move-object v0, v1

    .line 1154675
    return-object v0

    .line 1154676
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;
    .locals 9

    .prologue
    .line 1154677
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    .line 1154678
    sget-object v1, LX/6xZ;->SELECT_CONTACT_INFO:LX/6xZ;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-static {v1, v2}, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->a(LX/6xZ;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/718;

    move-result-object v1

    invoke-virtual {v1}, LX/718;->a()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v1

    .line 1154679
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->newBuilder()LX/71E;

    move-result-object v2

    sget-object v3, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    invoke-static {p1, v3}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6ws;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v3

    .line 1154680
    iput-object v3, v2, LX/71E;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1154681
    move-object v2, v2

    .line 1154682
    invoke-static {p1}, LX/6qv;->b(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;

    move-result-object v5

    .line 1154683
    new-instance v6, LX/0P2;

    invoke-direct {v6}, LX/0P2;-><init>()V

    .line 1154684
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1154685
    invoke-interface {v3}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->d()LX/6vb;

    move-result-object v8

    invoke-static {v8}, LX/6qv;->a(LX/6vb;)LX/6vZ;

    move-result-object v8

    invoke-interface {v3}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v8, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1154686
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 1154687
    :cond_0
    invoke-virtual {v6}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    move-object v3, v3

    .line 1154688
    iput-object v3, v2, LX/71E;->b:LX/0P1;

    .line 1154689
    move-object v2, v2

    .line 1154690
    invoke-virtual {v2}, LX/71E;->c()Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    move-result-object v2

    .line 1154691
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->newBuilder()LX/71A;

    move-result-object v3

    .line 1154692
    iput-object v2, v3, LX/71A;->e:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    .line 1154693
    move-object v2, v3

    .line 1154694
    iput-object v1, v2, LX/71A;->a:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 1154695
    move-object v1, v2

    .line 1154696
    sget-object v2, LX/71C;->CONTACT_INFORMATION:LX/71C;

    .line 1154697
    iput-object v2, v1, LX/71A;->b:LX/71C;

    .line 1154698
    move-object v1, v1

    .line 1154699
    iput-object v0, v1, LX/71A;->c:LX/6xg;

    .line 1154700
    move-object v0, v1

    .line 1154701
    iget-object v1, p0, LX/6tM;->c:Landroid/content/res/Resources;

    const v2, 0x7f081e3e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1154702
    iput-object v1, v0, LX/71A;->d:Ljava/lang/String;

    .line 1154703
    move-object v0, v0

    .line 1154704
    invoke-virtual {v0}, LX/71A;->h()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    .line 1154705
    invoke-static {}, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->newBuilder()LX/6w5;

    move-result-object v1

    .line 1154706
    iput-object v0, v1, LX/6w5;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 1154707
    move-object v0, v1

    .line 1154708
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    .line 1154709
    iput-object v1, v0, LX/6w5;->b:LX/0Rf;

    .line 1154710
    move-object v0, v0

    .line 1154711
    sget-object v1, LX/71H;->SELECTABLE:LX/71H;

    .line 1154712
    iput-object v1, v0, LX/6w5;->c:LX/71H;

    .line 1154713
    move-object v0, v0

    .line 1154714
    invoke-virtual {v0}, LX/6w5;->d()Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;
    .locals 12

    .prologue
    .line 1154715
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 1154716
    iget-object v9, p2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->f:LX/0Px;

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v10, :cond_1

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    .line 1154717
    iget-object v0, v5, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->d:LX/0Px;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v5, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->d:LX/0Px;

    invoke-static {v0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(LX/0Px;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v3

    .line 1154718
    :goto_1
    new-instance v0, Lcom/facebook/payments/selector/model/OptionSelectorRow;

    iget-object v1, v5, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    iget-object v2, v5, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->b:Ljava/lang/String;

    iget-boolean v4, v5, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->c:Z

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->u()LX/0P1;

    move-result-object v6

    iget-object v11, p2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    invoke-virtual {v6, v11}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0Px;

    invoke-static {v5, v6}, LX/6tM;->a(Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;LX/0Px;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/payments/selector/model/OptionSelectorRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;ZZ)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154719
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 1154720
    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    .line 1154721
    :cond_1
    iget-object v0, p2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->i:Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    const-string v1, "shipping_option"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1154722
    sget-object v0, LX/6xK;->SHIPPING_METHOD_FORM_CONTROLLER:LX/6xK;

    iget-object v1, p2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->i:Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;

    iget-object v1, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->a:Ljava/lang/String;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-static {v2}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/payments/form/model/PaymentsFormParams;->a(LX/6xK;Ljava/lang/String;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6xW;

    move-result-object v0

    new-instance v1, Lcom/facebook/payments/form/model/ShippingMethodFormData;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->e:Ljava/util/Currency;

    invoke-direct {v1, v2}, Lcom/facebook/payments/form/model/ShippingMethodFormData;-><init>(Ljava/util/Currency;)V

    .line 1154723
    iput-object v1, v0, LX/6xW;->e:Lcom/facebook/payments/form/model/PaymentsFormData;

    .line 1154724
    move-object v0, v0

    .line 1154725
    invoke-virtual {v0}, LX/6xW;->a()Lcom/facebook/payments/form/model/PaymentsFormParams;

    move-result-object v0

    .line 1154726
    new-instance v1, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;

    iget-object v2, p2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->i:Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;

    iget-object v2, v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->b:Ljava/lang/String;

    iget-object v3, p0, LX/6tM;->b:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/facebook/payments/form/PaymentsFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/form/model/PaymentsFormParams;)Landroid/content/Intent;

    move-result-object v0

    const/16 v3, 0x64

    invoke-direct {v1, v2, v0, v3}, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;-><init>(Ljava/lang/String;Landroid/content/Intent;I)V

    .line 1154727
    invoke-virtual {v8, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154728
    :cond_2
    new-instance v0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v1, p2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->d:Ljava/lang/String;

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    sget-object v3, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    invoke-static {p1, v3}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6ws;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v3

    iget-object v4, p2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;-><init>(Ljava/lang/String;LX/0Px;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;LX/72g;)Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;
    .locals 5

    .prologue
    .line 1154729
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v1, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    .line 1154730
    sget-object v0, LX/6xZ;->SELECT_SHIPPING_ADDRESS:LX/6xZ;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-static {v0, v2}, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->a(LX/6xZ;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/718;

    move-result-object v0

    invoke-virtual {v1}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 1154731
    iput-object v2, v0, LX/718;->c:Ljava/lang/String;

    .line 1154732
    move-object v0, v0

    .line 1154733
    invoke-virtual {v0}, LX/718;->a()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v2

    .line 1154734
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->newBuilder()LX/71E;

    move-result-object v0

    sget-object v3, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    invoke-static {p1, v3}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6ws;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v3

    .line 1154735
    iput-object v3, v0, LX/71E;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1154736
    move-object v3, v0

    .line 1154737
    sget-object v4, LX/729;->SHIPPING_ADDRESSES:LX/729;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->h()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/71E;->a(LX/6vZ;Ljava/lang/String;)LX/71E;

    move-result-object v0

    invoke-virtual {v0}, LX/71E;->c()Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    move-result-object v0

    .line 1154738
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->newBuilder()LX/71A;

    move-result-object v3

    .line 1154739
    iput-object v0, v3, LX/71A;->e:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    .line 1154740
    move-object v0, v3

    .line 1154741
    iput-object v2, v0, LX/71A;->a:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 1154742
    move-object v0, v0

    .line 1154743
    sget-object v3, LX/71C;->SIMPLE_SHIPPING_ADDRESS:LX/71C;

    .line 1154744
    iput-object v3, v0, LX/71A;->b:LX/71C;

    .line 1154745
    move-object v0, v0

    .line 1154746
    iput-object v1, v0, LX/71A;->c:LX/6xg;

    .line 1154747
    move-object v0, v0

    .line 1154748
    iget-object v1, p0, LX/6tM;->c:Landroid/content/res/Resources;

    const v3, 0x7f081e66

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1154749
    iput-object v1, v0, LX/71A;->d:Ljava/lang/String;

    .line 1154750
    move-object v0, v0

    .line 1154751
    new-instance v1, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;-><init>(Z)V

    .line 1154752
    iput-object v1, v0, LX/71A;->f:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    .line 1154753
    move-object v0, v0

    .line 1154754
    invoke-virtual {v0}, LX/71A;->h()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    .line 1154755
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v1

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v1, v3}, LX/6wu;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6wu;

    move-result-object v1

    sget-object v3, LX/6ws;->MODAL_BOTTOM:LX/6ws;

    .line 1154756
    iput-object v3, v1, LX/6wu;->a:LX/6ws;

    .line 1154757
    move-object v1, v1

    .line 1154758
    invoke-virtual {v1}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v1

    .line 1154759
    invoke-static {}, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->newBuilder()LX/72e;

    move-result-object v3

    .line 1154760
    iput-object p2, v3, LX/72e;->a:LX/72g;

    .line 1154761
    move-object v3, v3

    .line 1154762
    sget-object v4, LX/72f;->CHECKOUT:LX/72f;

    .line 1154763
    iput-object v4, v3, LX/72e;->e:LX/72f;

    .line 1154764
    move-object v3, v3

    .line 1154765
    iput-object v1, v3, LX/72e;->f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1154766
    move-object v1, v3

    .line 1154767
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->i()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    .line 1154768
    iput v3, v1, LX/72e;->g:I

    .line 1154769
    move-object v1, v1

    .line 1154770
    iget-object v2, v2, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1154771
    iput-object v2, v1, LX/72e;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1154772
    move-object v1, v1

    .line 1154773
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    .line 1154774
    iput-object v2, v1, LX/72e;->i:LX/6xg;

    .line 1154775
    move-object v1, v1

    .line 1154776
    invoke-virtual {v1}, LX/72e;->j()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v1

    .line 1154777
    invoke-static {}, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->newBuilder()LX/722;

    move-result-object v2

    .line 1154778
    iput-object v0, v2, LX/722;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 1154779
    move-object v0, v2

    .line 1154780
    iput-object v1, v0, LX/722;->b:Lcom/facebook/payments/shipping/model/ShippingParams;

    .line 1154781
    move-object v0, v0

    .line 1154782
    invoke-virtual {v0}, LX/722;->c()Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1154783
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    .line 1154784
    invoke-virtual {v0}, LX/0Rf;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 1154785
    invoke-virtual {p0, p1}, LX/6tM;->b(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    move-result-object v0

    .line 1154786
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;
    .locals 3

    .prologue
    .line 1154787
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->o()Lcom/facebook/payments/contactinfo/model/ContactInfo;

    move-result-object v0

    .line 1154788
    invoke-static {}, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->newBuilder()LX/6v2;

    move-result-object v1

    invoke-interface {v0}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->d()LX/6vb;

    move-result-object v2

    invoke-virtual {v2}, LX/6vb;->getContactInfoFormStyle()LX/6vY;

    move-result-object v2

    .line 1154789
    iput-object v2, v1, LX/6v2;->a:LX/6vY;

    .line 1154790
    move-object v1, v1

    .line 1154791
    iput-object v0, v1, LX/6v2;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1154792
    move-object v0, v1

    .line 1154793
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-static {v1}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v1

    .line 1154794
    iput-object v1, v0, LX/6v2;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1154795
    move-object v0, v0

    .line 1154796
    invoke-virtual {v0}, LX/6v2;->f()Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/model/ShippingParams;
    .locals 1

    .prologue
    .line 1154797
    sget-object v0, LX/72g;->SIMPLE:LX/72g;

    invoke-static {p1, v0}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/72g;)Lcom/facebook/payments/shipping/model/ShippingParams;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;
    .locals 1

    .prologue
    .line 1154798
    sget-object v0, LX/72g;->SIMPLE:LX/72g;

    invoke-virtual {p0, p1, v0}, LX/6tM;->b(Lcom/facebook/payments/checkout/model/CheckoutData;LX/72g;)Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final g(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;
    .locals 1

    .prologue
    .line 1154645
    sget-object v0, LX/71C;->SIMPLE_SHIPPING_OPTION_PICKER:LX/71C;

    invoke-virtual {p0, p1, v0}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/71C;)Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final h(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;
    .locals 2

    .prologue
    .line 1154799
    sget-object v0, LX/71C;->PAYMENT_METHODS:LX/71C;

    .line 1154800
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1154801
    invoke-virtual {p0, p1, v0, v1}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/71C;LX/0Px;)Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method
