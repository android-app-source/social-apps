.class public LX/7Ka;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2p5;

.field public final b:I

.field private final c:Landroid/graphics/Point;

.field public d:[Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/LinkedList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public e:[I

.field private f:[I

.field private g:[I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I


# direct methods
.method public constructor <init>(LX/2p5;ILandroid/graphics/Point;)V
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const/4 v1, 0x0

    .line 1196021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1196022
    iput-object p1, p0, LX/7Ka;->a:LX/2p5;

    .line 1196023
    iput p2, p0, LX/7Ka;->b:I

    .line 1196024
    iput-object p3, p0, LX/7Ka;->c:Landroid/graphics/Point;

    .line 1196025
    iput v1, p0, LX/7Ka;->h:I

    .line 1196026
    invoke-virtual {p1}, LX/2p5;->isHorizontal()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1196027
    iget v0, p3, Landroid/graphics/Point;->y:I

    iput v0, p0, LX/7Ka;->i:I

    .line 1196028
    iget v0, p3, Landroid/graphics/Point;->x:I

    iget v2, p0, LX/7Ka;->b:I

    int-to-double v2, v2

    mul-double/2addr v2, v4

    div-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    add-int/2addr v0, v2

    iput v0, p0, LX/7Ka;->j:I

    .line 1196029
    iget v0, p3, Landroid/graphics/Point;->y:I

    iput v0, p0, LX/7Ka;->k:I

    .line 1196030
    iget v0, p3, Landroid/graphics/Point;->x:I

    iget v2, p0, LX/7Ka;->b:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    iput v0, p0, LX/7Ka;->l:I

    .line 1196031
    :goto_0
    invoke-static {}, LX/2p6;->values()[LX/2p6;

    move-result-object v0

    array-length v2, v0

    .line 1196032
    new-array v0, v2, [Ljava/util/LinkedList;

    iput-object v0, p0, LX/7Ka;->d:[Ljava/util/LinkedList;

    .line 1196033
    new-array v0, v2, [I

    iput-object v0, p0, LX/7Ka;->e:[I

    .line 1196034
    new-array v0, v2, [I

    iput-object v0, p0, LX/7Ka;->f:[I

    .line 1196035
    new-array v0, v2, [I

    iput-object v0, p0, LX/7Ka;->g:[I

    move v0, v1

    .line 1196036
    :goto_1
    if-ge v0, v2, :cond_1

    .line 1196037
    iget-object v3, p0, LX/7Ka;->d:[Ljava/util/LinkedList;

    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    aput-object v4, v3, v0

    .line 1196038
    iget-object v3, p0, LX/7Ka;->e:[I

    aput v1, v3, v0

    .line 1196039
    iget-object v3, p0, LX/7Ka;->f:[I

    iget v4, p0, LX/7Ka;->b:I

    aput v4, v3, v0

    .line 1196040
    iget-object v3, p0, LX/7Ka;->g:[I

    const v4, 0x7fffffff

    aput v4, v3, v0

    .line 1196041
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1196042
    :cond_0
    iget v0, p3, Landroid/graphics/Point;->y:I

    iget v2, p0, LX/7Ka;->b:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    iput v0, p0, LX/7Ka;->i:I

    .line 1196043
    iget v0, p3, Landroid/graphics/Point;->x:I

    iput v0, p0, LX/7Ka;->j:I

    .line 1196044
    iget v0, p3, Landroid/graphics/Point;->y:I

    iget v2, p0, LX/7Ka;->b:I

    int-to-double v2, v2

    mul-double/2addr v2, v4

    div-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    add-int/2addr v0, v2

    iput v0, p0, LX/7Ka;->k:I

    .line 1196045
    iget v0, p3, Landroid/graphics/Point;->x:I

    iput v0, p0, LX/7Ka;->l:I

    goto :goto_0

    .line 1196046
    :cond_1
    return-void
.end method

.method private g()V
    .locals 5

    .prologue
    .line 1196047
    iget-object v1, p0, LX/7Ka;->f:[I

    sget-object v0, LX/2p6;->START:LX/2p6;

    invoke-virtual {v0}, LX/2p6;->ordinal()I

    move-result v2

    iget-object v0, p0, LX/7Ka;->e:[I

    sget-object v3, LX/2p6;->CENTER:LX/2p6;

    invoke-virtual {v3}, LX/2p6;->ordinal()I

    move-result v3

    aget v0, v0, v3

    if-lez v0, :cond_0

    iget v0, p0, LX/7Ka;->b:I

    iget-object v3, p0, LX/7Ka;->e:[I

    sget-object v4, LX/2p6;->CENTER:LX/2p6;

    invoke-virtual {v4}, LX/2p6;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    iget-object v3, p0, LX/7Ka;->e:[I

    sget-object v4, LX/2p6;->START:LX/2p6;

    invoke-virtual {v4}, LX/2p6;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sub-int/2addr v0, v3

    :goto_0
    aput v0, v1, v2

    .line 1196048
    return-void

    .line 1196049
    :cond_0
    iget v0, p0, LX/7Ka;->b:I

    iget-object v3, p0, LX/7Ka;->e:[I

    sget-object v4, LX/2p6;->END:LX/2p6;

    invoke-virtual {v4}, LX/2p6;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sub-int/2addr v0, v3

    iget-object v3, p0, LX/7Ka;->e:[I

    sget-object v4, LX/2p6;->START:LX/2p6;

    invoke-virtual {v4}, LX/2p6;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sub-int/2addr v0, v3

    goto :goto_0
.end method

.method private h()V
    .locals 5

    .prologue
    .line 1196050
    iget-object v0, p0, LX/7Ka;->e:[I

    sget-object v1, LX/2p6;->START:LX/2p6;

    invoke-virtual {v1}, LX/2p6;->ordinal()I

    move-result v1

    aget v0, v0, v1

    iget-object v1, p0, LX/7Ka;->e:[I

    sget-object v2, LX/2p6;->END:LX/2p6;

    invoke-virtual {v2}, LX/2p6;->ordinal()I

    move-result v2

    aget v1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1196051
    iget-object v1, p0, LX/7Ka;->f:[I

    sget-object v2, LX/2p6;->CENTER:LX/2p6;

    invoke-virtual {v2}, LX/2p6;->ordinal()I

    move-result v2

    iget v3, p0, LX/7Ka;->b:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v3, v0

    iget-object v3, p0, LX/7Ka;->e:[I

    sget-object v4, LX/2p6;->CENTER:LX/2p6;

    invoke-virtual {v4}, LX/2p6;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sub-int/2addr v0, v3

    aput v0, v1, v2

    .line 1196052
    return-void
.end method

.method private i()V
    .locals 5

    .prologue
    .line 1196053
    iget-object v1, p0, LX/7Ka;->f:[I

    sget-object v0, LX/2p6;->END:LX/2p6;

    invoke-virtual {v0}, LX/2p6;->ordinal()I

    move-result v2

    iget-object v0, p0, LX/7Ka;->e:[I

    sget-object v3, LX/2p6;->CENTER:LX/2p6;

    invoke-virtual {v3}, LX/2p6;->ordinal()I

    move-result v3

    aget v0, v0, v3

    if-lez v0, :cond_0

    iget v0, p0, LX/7Ka;->b:I

    iget-object v3, p0, LX/7Ka;->e:[I

    sget-object v4, LX/2p6;->CENTER:LX/2p6;

    invoke-virtual {v4}, LX/2p6;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    iget-object v3, p0, LX/7Ka;->e:[I

    sget-object v4, LX/2p6;->END:LX/2p6;

    invoke-virtual {v4}, LX/2p6;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sub-int/2addr v0, v3

    :goto_0
    aput v0, v1, v2

    .line 1196054
    return-void

    .line 1196055
    :cond_0
    iget v0, p0, LX/7Ka;->b:I

    iget-object v3, p0, LX/7Ka;->e:[I

    sget-object v4, LX/2p6;->START:LX/2p6;

    invoke-virtual {v4}, LX/2p6;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sub-int/2addr v0, v3

    iget-object v3, p0, LX/7Ka;->e:[I

    sget-object v4, LX/2p6;->END:LX/2p6;

    invoke-virtual {v4}, LX/2p6;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sub-int/2addr v0, v3

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;)LX/7Ka;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1196056
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/2p4;

    .line 1196057
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v2, v1

    .line 1196058
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v1, v3

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v3, v1

    .line 1196059
    iget-object v1, p0, LX/7Ka;->a:LX/2p5;

    invoke-virtual {v1}, LX/2p5;->isHorizontal()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    .line 1196060
    :goto_0
    iget-object v4, p0, LX/7Ka;->a:LX/2p5;

    invoke-virtual {v4}, LX/2p5;->isHorizontal()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1196061
    :goto_1
    iget-object v2, p0, LX/7Ka;->f:[I

    iget-object v4, v0, LX/2p4;->c:LX/2p6;

    invoke-virtual {v4}, LX/2p6;->ordinal()I

    move-result v4

    aget v2, v2, v4

    if-gt v1, v2, :cond_0

    iget v2, v0, LX/2p4;->b:I

    iget-object v4, p0, LX/7Ka;->g:[I

    iget-object v5, v0, LX/2p4;->c:LX/2p6;

    invoke-virtual {v5}, LX/2p6;->ordinal()I

    move-result v5

    aget v4, v4, v5

    if-le v2, v4, :cond_3

    .line 1196062
    :cond_0
    sget-object v0, LX/7KZ;->a:[I

    iget-object v2, p0, LX/7Ka;->a:LX/2p5;

    invoke-virtual {v2}, LX/2p5;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1196063
    new-instance v0, Landroid/graphics/Point;

    iget-object v2, p0, LX/7Ka;->c:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget v3, p0, LX/7Ka;->k:I

    invoke-direct {v0, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 1196064
    :goto_2
    new-instance v2, LX/7Ka;

    iget-object v3, p0, LX/7Ka;->a:LX/2p5;

    iget v4, p0, LX/7Ka;->b:I

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-direct {v2, v3, v1, v0}, LX/7Ka;-><init>(LX/2p5;ILandroid/graphics/Point;)V

    .line 1196065
    invoke-virtual {v2, p1}, LX/7Ka;->a(Landroid/view/View;)LX/7Ka;

    move-object v0, v2

    .line 1196066
    :goto_3
    return-object v0

    :cond_1
    move v1, v3

    .line 1196067
    goto :goto_0

    :cond_2
    move v3, v2

    .line 1196068
    goto :goto_1

    .line 1196069
    :pswitch_0
    new-instance v0, Landroid/graphics/Point;

    iget-object v2, p0, LX/7Ka;->c:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget v3, p0, LX/7Ka;->i:I

    invoke-direct {v0, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_2

    .line 1196070
    :pswitch_1
    new-instance v0, Landroid/graphics/Point;

    iget v2, p0, LX/7Ka;->j:I

    iget-object v3, p0, LX/7Ka;->c:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-direct {v0, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_2

    .line 1196071
    :pswitch_2
    new-instance v0, Landroid/graphics/Point;

    iget v2, p0, LX/7Ka;->l:I

    iget-object v3, p0, LX/7Ka;->c:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-direct {v0, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_2

    .line 1196072
    :cond_3
    iget-object v2, p0, LX/7Ka;->g:[I

    iget-object v4, v0, LX/2p4;->c:LX/2p6;

    invoke-virtual {v4}, LX/2p6;->ordinal()I

    move-result v4

    iget v5, v0, LX/2p4;->b:I

    aput v5, v2, v4

    .line 1196073
    iget v2, p0, LX/7Ka;->h:I

    if-le v3, v2, :cond_4

    .line 1196074
    iput v3, p0, LX/7Ka;->h:I

    .line 1196075
    sget-object v2, LX/7KZ;->a:[I

    iget-object v3, p0, LX/7Ka;->a:LX/2p5;

    invoke-virtual {v3}, LX/2p5;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 1196076
    iget v2, p0, LX/7Ka;->i:I

    iget v3, p0, LX/7Ka;->h:I

    add-int/2addr v2, v3

    iput v2, p0, LX/7Ka;->k:I

    .line 1196077
    :cond_4
    :goto_4
    iget-object v2, p0, LX/7Ka;->e:[I

    iget-object v3, v0, LX/2p4;->c:LX/2p6;

    invoke-virtual {v3}, LX/2p6;->ordinal()I

    move-result v3

    aget v4, v2, v3

    add-int/2addr v4, v1

    aput v4, v2, v3

    .line 1196078
    iget-object v2, p0, LX/7Ka;->f:[I

    iget-object v3, v0, LX/2p4;->c:LX/2p6;

    invoke-virtual {v3}, LX/2p6;->ordinal()I

    move-result v3

    aget v4, v2, v3

    sub-int v1, v4, v1

    aput v1, v2, v3

    .line 1196079
    sget-object v1, LX/7KZ;->b:[I

    iget-object v2, v0, LX/2p4;->c:LX/2p6;

    invoke-virtual {v2}, LX/2p6;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2

    .line 1196080
    :goto_5
    const/4 v0, 0x0

    goto :goto_3

    .line 1196081
    :pswitch_3
    iget v2, p0, LX/7Ka;->k:I

    iget v3, p0, LX/7Ka;->h:I

    sub-int/2addr v2, v3

    iput v2, p0, LX/7Ka;->i:I

    goto :goto_4

    .line 1196082
    :pswitch_4
    iget v2, p0, LX/7Ka;->l:I

    iget v3, p0, LX/7Ka;->h:I

    add-int/2addr v2, v3

    iput v2, p0, LX/7Ka;->j:I

    goto :goto_4

    .line 1196083
    :pswitch_5
    iget v2, p0, LX/7Ka;->j:I

    iget v3, p0, LX/7Ka;->h:I

    sub-int/2addr v2, v3

    iput v2, p0, LX/7Ka;->l:I

    goto :goto_4

    .line 1196084
    :pswitch_6
    iget-object v1, p0, LX/7Ka;->d:[Ljava/util/LinkedList;

    iget-object v0, v0, LX/2p4;->c:LX/2p6;

    invoke-virtual {v0}, LX/2p6;->ordinal()I

    move-result v0

    aget-object v0, v1, v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 1196085
    invoke-direct {p0}, LX/7Ka;->h()V

    .line 1196086
    invoke-direct {p0}, LX/7Ka;->i()V

    goto :goto_5

    .line 1196087
    :pswitch_7
    iget-object v1, p0, LX/7Ka;->d:[Ljava/util/LinkedList;

    iget-object v0, v0, LX/2p4;->c:LX/2p6;

    invoke-virtual {v0}, LX/2p6;->ordinal()I

    move-result v0

    aget-object v0, v1, v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 1196088
    invoke-direct {p0}, LX/7Ka;->g()V

    .line 1196089
    invoke-direct {p0}, LX/7Ka;->i()V

    goto :goto_5

    .line 1196090
    :pswitch_8
    iget-object v1, p0, LX/7Ka;->d:[Ljava/util/LinkedList;

    iget-object v0, v0, LX/2p4;->c:LX/2p6;

    invoke-virtual {v0}, LX/2p6;->ordinal()I

    move-result v0

    aget-object v0, v1, v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 1196091
    invoke-direct {p0}, LX/7Ka;->g()V

    .line 1196092
    invoke-direct {p0}, LX/7Ka;->h()V

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
