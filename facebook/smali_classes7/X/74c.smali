.class public LX/74c;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1168171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/ipc/media/data/LocalMediaData;LX/4gQ;)Lcom/facebook/ipc/media/data/LocalMediaData;
    .locals 2

    .prologue
    .line 1168172
    invoke-virtual {p0}, Lcom/facebook/ipc/media/data/LocalMediaData;->e()LX/4gN;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/ipc/media/data/LocalMediaData;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/data/MediaData;->k()LX/4gP;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v1

    invoke-virtual {v1}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v0

    invoke-virtual {v0}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/ipc/media/MediaItem;)Z
    .locals 2

    .prologue
    .line 1168173
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1168174
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v1

    .line 1168175
    sget-object v1, LX/4gQ;->Video:LX/4gQ;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/facebook/ipc/media/data/MimeType;->d:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1168176
    iget-object p0, v1, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    move-object v1, p0

    .line 1168177
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/media/data/MimeType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
