.class public LX/7MX;
.super LX/2oy;
.source ""


# instance fields
.field public a:LX/7N6;

.field public b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1198835
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7MX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1198836
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1198833
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7MX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198834
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1198829
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198830
    const/4 v0, 0x3

    iput v0, p0, LX/7MX;->b:I

    .line 1198831
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7MW;

    invoke-direct {v1, p0}, LX/7MW;-><init>(LX/7MX;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1198832
    return-void
.end method

.method public static a$redex0(LX/7MX;LX/2qV;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1198837
    iget-object v0, p0, LX/7MX;->a:LX/7N6;

    if-nez v0, :cond_0

    .line 1198838
    :goto_0
    return-void

    .line 1198839
    :cond_0
    sget-object v0, LX/7MV;->a:[I

    invoke-virtual {p1}, LX/2qV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1198840
    :cond_1
    iget v0, p0, LX/7MX;->b:I

    if-eq v0, v3, :cond_2

    .line 1198841
    iget-object v0, p0, LX/7MX;->a:LX/7N6;

    invoke-interface {v0}, LX/7N6;->b()V

    .line 1198842
    :cond_2
    iput v3, p0, LX/7MX;->b:I

    goto :goto_0

    .line 1198843
    :pswitch_0
    iget v0, p0, LX/7MX;->b:I

    if-eq v0, v2, :cond_3

    .line 1198844
    iget-object v0, p0, LX/7MX;->a:LX/7N6;

    invoke-interface {v0}, LX/7N6;->a()V

    .line 1198845
    :cond_3
    iput v2, p0, LX/7MX;->b:I

    goto :goto_0

    .line 1198846
    :pswitch_1
    iget v0, p0, LX/7MX;->b:I

    if-eq v0, v4, :cond_4

    .line 1198847
    iget-object v0, p0, LX/7MX;->a:LX/7N6;

    invoke-interface {v0}, LX/7N6;->c()V

    .line 1198848
    :cond_4
    iput v4, p0, LX/7MX;->b:I

    goto :goto_0

    .line 1198849
    :pswitch_2
    iget v0, p0, LX/7MX;->b:I

    if-eq v0, v2, :cond_5

    iget v0, p0, LX/7MX;->b:I

    if-ne v0, v5, :cond_1

    .line 1198850
    :cond_5
    iput v5, p0, LX/7MX;->b:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 1

    .prologue
    .line 1198824
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 1198825
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1198826
    iget-object p1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, p1

    .line 1198827
    invoke-static {p0, v0}, LX/7MX;->a$redex0(LX/7MX;LX/2qV;)V

    .line 1198828
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1198820
    iget-object v0, p0, LX/7MX;->a:LX/7N6;

    if-eqz v0, :cond_0

    .line 1198821
    const/4 v0, 0x3

    iput v0, p0, LX/7MX;->b:I

    .line 1198822
    iget-object v0, p0, LX/7MX;->a:LX/7N6;

    invoke-interface {v0}, LX/7N6;->c()V

    .line 1198823
    :cond_0
    return-void
.end method
