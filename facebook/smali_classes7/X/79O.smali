.class public final LX/79O;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;

.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;

.field public static f:Ljava/lang/String;

.field public static g:Ljava/lang/String;

.field public static h:Ljava/lang/String;

.field public static i:Ljava/lang/String;

.field public static j:Ljava/lang/String;

.field public static k:Ljava/lang/String;

.field public static l:Ljava/lang/String;

.field public static m:Ljava/lang/String;

.field public static n:Ljava/lang/String;

.field public static o:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1174040
    const-string v0, "NotPermitted"

    sput-object v0, LX/79O;->a:Ljava/lang/String;

    .line 1174041
    const-string v0, "NotCallable"

    sput-object v0, LX/79O;->b:Ljava/lang/String;

    .line 1174042
    const-string v0, "PresenceNotLoaded"

    sput-object v0, LX/79O;->c:Ljava/lang/String;

    .line 1174043
    const-string v0, "InvalidUser"

    sput-object v0, LX/79O;->d:Ljava/lang/String;

    .line 1174044
    const-string v0, "MessengerPromo"

    sput-object v0, LX/79O;->e:Ljava/lang/String;

    .line 1174045
    const-string v0, "NoNetwork"

    sput-object v0, LX/79O;->f:Ljava/lang/String;

    .line 1174046
    const-string v0, "NoMqttConnection"

    sput-object v0, LX/79O;->g:Ljava/lang/String;

    .line 1174047
    const-string v0, "NetworkDisallowsVoIP"

    sput-object v0, LX/79O;->h:Ljava/lang/String;

    .line 1174048
    const-string v0, "CallInProgress"

    sput-object v0, LX/79O;->i:Ljava/lang/String;

    .line 1174049
    const-string v0, "CallingDisabled"

    sput-object v0, LX/79O;->j:Ljava/lang/String;

    .line 1174050
    const-string v0, "CallCancelled"

    sput-object v0, LX/79O;->k:Ljava/lang/String;

    .line 1174051
    const-string v0, "BlockedCountry"

    sput-object v0, LX/79O;->l:Ljava/lang/String;

    .line 1174052
    const-string v0, "NoDevicePermission"

    sput-object v0, LX/79O;->m:Ljava/lang/String;

    .line 1174053
    const-string v0, "ZeroRatingCancel"

    sput-object v0, LX/79O;->n:Ljava/lang/String;

    .line 1174054
    const-string v0, "CallConfirmCancelled"

    sput-object v0, LX/79O;->o:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1174055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
