.class public final enum LX/8Lr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Lr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Lr;

.field public static final enum DRAFT_FEED_ENTRY_POINT:LX/8Lr;

.field public static final enum DRAFT_JEWEL_NOTIFICATION:LX/8Lr;

.field public static final enum DRAFT_JEWEL_NOTIFICATION_WITH_DRAFTID:LX/8Lr;

.field public static final enum DRAFT_PUSH_NOTIFICATION:LX/8Lr;

.field public static final enum OPTIMISTIC_STORY:LX/8Lr;

.field public static final enum SNACKBAR:LX/8Lr;

.field public static final enum UNKNOWN:LX/8Lr;

.field public static final enum UPLOAD_NOTIFICATION:LX/8Lr;


# instance fields
.field public final analyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1334042
    new-instance v0, LX/8Lr;

    const-string v1, "DRAFT_JEWEL_NOTIFICATION_WITH_DRAFTID"

    const-string v2, "draft_jewel_notification_with_draftid"

    invoke-direct {v0, v1, v4, v2}, LX/8Lr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Lr;->DRAFT_JEWEL_NOTIFICATION_WITH_DRAFTID:LX/8Lr;

    .line 1334043
    new-instance v0, LX/8Lr;

    const-string v1, "DRAFT_JEWEL_NOTIFICATION"

    const-string v2, "draft_jewel_notification"

    invoke-direct {v0, v1, v5, v2}, LX/8Lr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Lr;->DRAFT_JEWEL_NOTIFICATION:LX/8Lr;

    .line 1334044
    new-instance v0, LX/8Lr;

    const-string v1, "DRAFT_PUSH_NOTIFICATION"

    const-string v2, "draft_push_notification"

    invoke-direct {v0, v1, v6, v2}, LX/8Lr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Lr;->DRAFT_PUSH_NOTIFICATION:LX/8Lr;

    .line 1334045
    new-instance v0, LX/8Lr;

    const-string v1, "UPLOAD_NOTIFICATION"

    const-string v2, "upload_notification"

    invoke-direct {v0, v1, v7, v2}, LX/8Lr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Lr;->UPLOAD_NOTIFICATION:LX/8Lr;

    .line 1334046
    new-instance v0, LX/8Lr;

    const-string v1, "OPTIMISTIC_STORY"

    const-string v2, "optimistic_story"

    invoke-direct {v0, v1, v8, v2}, LX/8Lr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Lr;->OPTIMISTIC_STORY:LX/8Lr;

    .line 1334047
    new-instance v0, LX/8Lr;

    const-string v1, "DRAFT_FEED_ENTRY_POINT"

    const/4 v2, 0x5

    const-string v3, "draft_feed_entry_point"

    invoke-direct {v0, v1, v2, v3}, LX/8Lr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Lr;->DRAFT_FEED_ENTRY_POINT:LX/8Lr;

    .line 1334048
    new-instance v0, LX/8Lr;

    const-string v1, "SNACKBAR"

    const/4 v2, 0x6

    const-string v3, "snackbar"

    invoke-direct {v0, v1, v2, v3}, LX/8Lr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Lr;->SNACKBAR:LX/8Lr;

    .line 1334049
    new-instance v0, LX/8Lr;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x7

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/8Lr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Lr;->UNKNOWN:LX/8Lr;

    .line 1334050
    const/16 v0, 0x8

    new-array v0, v0, [LX/8Lr;

    sget-object v1, LX/8Lr;->DRAFT_JEWEL_NOTIFICATION_WITH_DRAFTID:LX/8Lr;

    aput-object v1, v0, v4

    sget-object v1, LX/8Lr;->DRAFT_JEWEL_NOTIFICATION:LX/8Lr;

    aput-object v1, v0, v5

    sget-object v1, LX/8Lr;->DRAFT_PUSH_NOTIFICATION:LX/8Lr;

    aput-object v1, v0, v6

    sget-object v1, LX/8Lr;->UPLOAD_NOTIFICATION:LX/8Lr;

    aput-object v1, v0, v7

    sget-object v1, LX/8Lr;->OPTIMISTIC_STORY:LX/8Lr;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/8Lr;->DRAFT_FEED_ENTRY_POINT:LX/8Lr;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8Lr;->SNACKBAR:LX/8Lr;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8Lr;->UNKNOWN:LX/8Lr;

    aput-object v2, v0, v1

    sput-object v0, LX/8Lr;->$VALUES:[LX/8Lr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1334051
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1334052
    iput-object p3, p0, LX/8Lr;->analyticsName:Ljava/lang/String;

    .line 1334053
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Lr;
    .locals 1

    .prologue
    .line 1334054
    const-class v0, LX/8Lr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Lr;

    return-object v0
.end method

.method public static values()[LX/8Lr;
    .locals 1

    .prologue
    .line 1334055
    sget-object v0, LX/8Lr;->$VALUES:[LX/8Lr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Lr;

    return-object v0
.end method
