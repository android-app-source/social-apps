.class public final LX/7Pu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/7Pv;


# direct methods
.method public constructor <init>(LX/7Pv;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1203471
    iput-object p1, p0, LX/7Pu;->b:LX/7Pv;

    iput-object p2, p0, LX/7Pu;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1203472
    iget-object v0, p0, LX/7Pu;->b:LX/7Pv;

    iget-object v0, v0, LX/7Pv;->a:LX/1Ln;

    instance-of v0, v0, LX/0po;

    if-nez v0, :cond_0

    .line 1203473
    iget-object v0, p0, LX/7Pu;->a:Landroid/content/Context;

    const-string v1, "Video cache unable to be cleared"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1203474
    :goto_0
    return v2

    .line 1203475
    :cond_0
    iget-object v0, p0, LX/7Pu;->b:LX/7Pv;

    iget-object v0, v0, LX/7Pv;->a:LX/1Ln;

    check-cast v0, LX/0po;

    invoke-interface {v0}, LX/0po;->b()V

    .line 1203476
    iget-object v0, p0, LX/7Pu;->a:Landroid/content/Context;

    const-string v1, "Video cache cleared"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
