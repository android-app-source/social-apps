.class public final LX/83m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:S

.field public final synthetic b:LX/2h8;

.field public final synthetic c:LX/2dj;


# direct methods
.method public constructor <init>(LX/2dj;SLX/2h8;)V
    .locals 0

    .prologue
    .line 1290620
    iput-object p1, p0, LX/83m;->c:LX/2dj;

    iput-short p2, p0, LX/83m;->a:S

    iput-object p3, p0, LX/83m;->b:LX/2h8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 1290621
    const-string v4, "failure_local"

    .line 1290622
    new-instance v5, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 1290623
    instance-of v0, p1, LX/4Ua;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1290624
    check-cast v0, LX/4Ua;

    .line 1290625
    const-string v1, "error_code"

    .line 1290626
    iget-object v2, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v2, v2

    .line 1290627
    invoke-virtual {v2}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v2

    invoke-virtual {v5, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1290628
    const-string v1, "error_domain"

    .line 1290629
    iget-object v2, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v2

    .line 1290630
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->h()LX/2Aa;

    move-result-object v0

    invoke-virtual {v0}, LX/2Aa;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1290631
    const-string v4, "failure_api"

    .line 1290632
    :cond_0
    const-string v0, "error_class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1290633
    const-string v0, "error_message"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1290634
    const-string v0, "stack_trace"

    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1290635
    iget-object v0, p0, LX/83m;->c:LX/2dj;

    iget-object v0, v0, LX/2dj;->m:LX/0if;

    sget-object v1, LX/0ig;->u:LX/0ih;

    iget-short v2, p0, LX/83m;->a:S

    int-to-long v2, v2

    invoke-virtual {v5}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 1290636
    iget-object v0, p0, LX/83m;->c:LX/2dj;

    iget-object v0, v0, LX/2dj;->m:LX/0if;

    sget-object v1, LX/0ig;->u:LX/0ih;

    iget-short v2, p0, LX/83m;->a:S

    int-to-long v2, v2

    iget-object v4, p0, LX/83m;->b:LX/2h8;

    iget-object v4, v4, LX/2h8;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 1290637
    iget-object v0, p0, LX/83m;->c:LX/2dj;

    iget-object v0, v0, LX/2dj;->m:LX/0if;

    sget-object v1, LX/0ig;->u:LX/0ih;

    iget-short v2, p0, LX/83m;->a:S

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->c(LX/0ih;J)V

    .line 1290638
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1290639
    iget-object v0, p0, LX/83m;->c:LX/2dj;

    iget-object v0, v0, LX/2dj;->m:LX/0if;

    sget-object v1, LX/0ig;->u:LX/0ih;

    iget-short v2, p0, LX/83m;->a:S

    int-to-long v2, v2

    const-string v4, "success"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 1290640
    iget-object v0, p0, LX/83m;->c:LX/2dj;

    iget-object v0, v0, LX/2dj;->m:LX/0if;

    sget-object v1, LX/0ig;->u:LX/0ih;

    iget-short v2, p0, LX/83m;->a:S

    int-to-long v2, v2

    iget-object v4, p0, LX/83m;->b:LX/2h8;

    iget-object v4, v4, LX/2h8;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 1290641
    iget-object v0, p0, LX/83m;->c:LX/2dj;

    iget-object v0, v0, LX/2dj;->m:LX/0if;

    sget-object v1, LX/0ig;->u:LX/0ih;

    iget-short v2, p0, LX/83m;->a:S

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->c(LX/0ih;J)V

    .line 1290642
    return-void
.end method
