.class public LX/7gP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3iB;


# instance fields
.field public a:LX/7Kf;

.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Lcom/facebook/video/engine/VideoPlayerParams;


# direct methods
.method public constructor <init>(LX/7Kf;)V
    .locals 1

    .prologue
    .line 1224231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224232
    iput-object p1, p0, LX/7gP;->a:LX/7Kf;

    .line 1224233
    iget-object v0, p0, LX/7gP;->a:LX/7Kf;

    invoke-interface {v0}, LX/7Kf;->getCoverImage()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v0

    iput-object v0, p0, LX/7gP;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1224234
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 1224197
    iget-object v0, p0, LX/7gP;->a:LX/7Kf;

    invoke-interface {v0}, LX/7Kf;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1224198
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1224199
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1224200
    iget-object v1, p0, LX/7gP;->a:LX/7Kf;

    invoke-interface {v1, v0}, LX/7Kf;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1224201
    iget-object v0, p0, LX/7gP;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    .line 1224202
    iget-object v0, p0, LX/7gP;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1224203
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1224204
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1224205
    iget-object v1, p0, LX/7gP;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1224206
    :cond_0
    return-void
.end method

.method public final a(LX/04D;ZLcom/facebook/video/engine/VideoPlayerParams;DLX/0P1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/04D;",
            "Z",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "D",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1224217
    const-string v0, "InlineVideoPlayerDelegate.configurePlayer"

    const v1, 0x441ac982

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1224218
    :try_start_0
    iget-object v0, p0, LX/7gP;->a:LX/7Kf;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/7Kf;->setIsVideoCompleted(Z)V

    .line 1224219
    iget-object v0, p0, LX/7gP;->a:LX/7Kf;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/7Kf;->setPauseMediaPlayerOnVideoPause(Z)V

    .line 1224220
    iget-object v0, p0, LX/7gP;->a:LX/7Kf;

    invoke-interface {v0, p1}, LX/7Kf;->setPlayerOrigin(LX/04D;)V

    .line 1224221
    const-string v0, "BaseInlineVideoPlayer.setVideoData"

    const v1, -0x5afb030d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1224222
    :try_start_1
    iget-object v0, p0, LX/7gP;->a:LX/7Kf;

    invoke-interface {v0, p3, p4, p5, p6}, LX/7Kf;->a(Lcom/facebook/video/engine/VideoPlayerParams;DLX/0P1;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1224223
    const v0, -0x64bf2c15

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V

    .line 1224224
    iget-object v0, p0, LX/7gP;->a:LX/7Kf;

    const/4 v1, 0x1

    sget-object p1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-interface {v0, v1, p1}, LX/7Kf;->a(ZLX/04g;)V

    .line 1224225
    iget-object v0, p0, LX/7gP;->a:LX/7Kf;

    invoke-interface {v0, p2}, LX/7Kf;->setAlwaysPlayVideoUnmuted(Z)V

    .line 1224226
    iput-object p3, p0, LX/7gP;->c:Lcom/facebook/video/engine/VideoPlayerParams;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1224227
    const v0, -0x3f1a5405

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1224228
    return-void

    .line 1224229
    :catchall_0
    move-exception v0

    const v1, -0x7db371bf

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1224230
    :catchall_1
    move-exception v0

    const v1, -0x2a861a7f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Landroid/view/View$OnClickListener;Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1224213
    if-nez p1, :cond_0

    .line 1224214
    iget-object v0, p0, LX/7gP;->a:LX/7Kf;

    invoke-interface {v0, p1}, LX/7Kf;->setOnClickPlayerListener(Landroid/view/View$OnClickListener;)V

    .line 1224215
    :goto_0
    return-void

    .line 1224216
    :cond_0
    iget-object v0, p0, LX/7gP;->a:LX/7Kf;

    new-instance v1, LX/7gO;

    invoke-direct {v1, p0, p1, p2}, LX/7gO;-><init>(LX/7gP;Landroid/view/View$OnClickListener;Landroid/view/View;)V

    invoke-interface {v0, v1}, LX/7Kf;->setOnClickPlayerListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final getCoverController()LX/1aZ;
    .locals 1

    .prologue
    .line 1224212
    iget-object v0, p0, LX/7gP;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7gP;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setBackgroundResource(I)V
    .locals 1

    .prologue
    .line 1224210
    iget-object v0, p0, LX/7gP;->a:LX/7Kf;

    invoke-interface {v0, p1}, LX/7Kf;->setBackgroundResource(I)V

    .line 1224211
    return-void
.end method

.method public final setCoverController(LX/1aZ;)V
    .locals 1

    .prologue
    .line 1224207
    iget-object v0, p0, LX/7gP;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    .line 1224208
    iget-object v0, p0, LX/7gP;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1224209
    :cond_0
    return-void
.end method
