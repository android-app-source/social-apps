.class public LX/6kK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;


# instance fields
.field public final callState:Ljava/lang/String;

.field public final initiator:LX/6kt;

.field public final messageMetadata:LX/6kn;

.field public final serverInfoData:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xc

    const/16 v4, 0xb

    const/4 v3, 0x1

    .line 1136078
    new-instance v0, LX/1sv;

    const-string v1, "DeltaRtcCallData"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kK;->b:LX/1sv;

    .line 1136079
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    invoke-direct {v0, v1, v5, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kK;->c:LX/1sw;

    .line 1136080
    new-instance v0, LX/1sw;

    const-string v1, "callState"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kK;->d:LX/1sw;

    .line 1136081
    new-instance v0, LX/1sw;

    const-string v1, "serverInfoData"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kK;->e:LX/1sw;

    .line 1136082
    new-instance v0, LX/1sw;

    const-string v1, "initiator"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kK;->f:LX/1sw;

    .line 1136083
    sput-boolean v3, LX/6kK;->a:Z

    return-void
.end method

.method private constructor <init>(LX/6kn;Ljava/lang/String;Ljava/lang/String;LX/6kt;)V
    .locals 0

    .prologue
    .line 1136168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1136169
    iput-object p1, p0, LX/6kK;->messageMetadata:LX/6kn;

    .line 1136170
    iput-object p2, p0, LX/6kK;->callState:Ljava/lang/String;

    .line 1136171
    iput-object p3, p0, LX/6kK;->serverInfoData:Ljava/lang/String;

    .line 1136172
    iput-object p4, p0, LX/6kK;->initiator:LX/6kt;

    .line 1136173
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1136165
    iget-object v0, p0, LX/6kK;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1136166
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kK;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1136167
    :cond_0
    return-void
.end method

.method public static b(LX/1su;)LX/6kK;
    .locals 8

    .prologue
    const/16 v7, 0xc

    const/16 v6, 0xb

    const/4 v0, 0x0

    .line 1136144
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v1, v0

    move-object v2, v0

    move-object v3, v0

    .line 1136145
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v4

    .line 1136146
    iget-byte v5, v4, LX/1sw;->b:B

    if-eqz v5, :cond_4

    .line 1136147
    iget-short v5, v4, LX/1sw;->c:S

    packed-switch v5, :pswitch_data_0

    .line 1136148
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1136149
    :pswitch_0
    iget-byte v5, v4, LX/1sw;->b:B

    if-ne v5, v7, :cond_0

    .line 1136150
    invoke-static {p0}, LX/6kn;->b(LX/1su;)LX/6kn;

    move-result-object v3

    goto :goto_0

    .line 1136151
    :cond_0
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1136152
    :pswitch_1
    iget-byte v5, v4, LX/1sw;->b:B

    if-ne v5, v6, :cond_1

    .line 1136153
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1136154
    :cond_1
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1136155
    :pswitch_2
    iget-byte v5, v4, LX/1sw;->b:B

    if-ne v5, v6, :cond_2

    .line 1136156
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1136157
    :cond_2
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1136158
    :pswitch_3
    iget-byte v5, v4, LX/1sw;->b:B

    if-ne v5, v7, :cond_3

    .line 1136159
    invoke-static {p0}, LX/6kt;->b(LX/1su;)LX/6kt;

    move-result-object v0

    goto :goto_0

    .line 1136160
    :cond_3
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1136161
    :cond_4
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1136162
    new-instance v4, LX/6kK;

    invoke-direct {v4, v3, v2, v1, v0}, LX/6kK;-><init>(LX/6kn;Ljava/lang/String;Ljava/lang/String;LX/6kt;)V

    .line 1136163
    invoke-direct {v4}, LX/6kK;->a()V

    .line 1136164
    return-object v4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1136174
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1136175
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v1, v0

    .line 1136176
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1136177
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaRtcCallData"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1136178
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136179
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136180
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136181
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136182
    const-string v4, "messageMetadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136183
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136184
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136185
    iget-object v4, p0, LX/6kK;->messageMetadata:LX/6kn;

    if-nez v4, :cond_6

    .line 1136186
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136187
    :goto_3
    iget-object v4, p0, LX/6kK;->callState:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1136188
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136189
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136190
    const-string v4, "callState"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136191
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136192
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136193
    iget-object v4, p0, LX/6kK;->callState:Ljava/lang/String;

    if-nez v4, :cond_7

    .line 1136194
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136195
    :cond_0
    :goto_4
    iget-object v4, p0, LX/6kK;->serverInfoData:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 1136196
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136197
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136198
    const-string v4, "serverInfoData"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136199
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136200
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136201
    iget-object v4, p0, LX/6kK;->serverInfoData:Ljava/lang/String;

    if-nez v4, :cond_8

    .line 1136202
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136203
    :cond_1
    :goto_5
    iget-object v4, p0, LX/6kK;->initiator:LX/6kt;

    if-eqz v4, :cond_2

    .line 1136204
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136205
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136206
    const-string v4, "initiator"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136207
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136208
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136209
    iget-object v0, p0, LX/6kK;->initiator:LX/6kt;

    if-nez v0, :cond_9

    .line 1136210
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136211
    :cond_2
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136212
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136213
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1136214
    :cond_3
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1136215
    :cond_4
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1136216
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1136217
    :cond_6
    iget-object v4, p0, LX/6kK;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1136218
    :cond_7
    iget-object v4, p0, LX/6kK;->callState:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1136219
    :cond_8
    iget-object v4, p0, LX/6kK;->serverInfoData:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1136220
    :cond_9
    iget-object v0, p0, LX/6kK;->initiator:LX/6kt;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1136124
    invoke-direct {p0}, LX/6kK;->a()V

    .line 1136125
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1136126
    iget-object v0, p0, LX/6kK;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1136127
    sget-object v0, LX/6kK;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136128
    iget-object v0, p0, LX/6kK;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1136129
    :cond_0
    iget-object v0, p0, LX/6kK;->callState:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1136130
    iget-object v0, p0, LX/6kK;->callState:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1136131
    sget-object v0, LX/6kK;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136132
    iget-object v0, p0, LX/6kK;->callState:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1136133
    :cond_1
    iget-object v0, p0, LX/6kK;->serverInfoData:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1136134
    iget-object v0, p0, LX/6kK;->serverInfoData:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1136135
    sget-object v0, LX/6kK;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136136
    iget-object v0, p0, LX/6kK;->serverInfoData:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1136137
    :cond_2
    iget-object v0, p0, LX/6kK;->initiator:LX/6kt;

    if-eqz v0, :cond_3

    .line 1136138
    iget-object v0, p0, LX/6kK;->initiator:LX/6kt;

    if-eqz v0, :cond_3

    .line 1136139
    sget-object v0, LX/6kK;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136140
    iget-object v0, p0, LX/6kK;->initiator:LX/6kt;

    invoke-virtual {v0, p1}, LX/6kt;->a(LX/1su;)V

    .line 1136141
    :cond_3
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1136142
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1136143
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1136088
    if-nez p1, :cond_1

    .line 1136089
    :cond_0
    :goto_0
    return v0

    .line 1136090
    :cond_1
    instance-of v1, p1, LX/6kK;

    if-eqz v1, :cond_0

    .line 1136091
    check-cast p1, LX/6kK;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1136092
    if-nez p1, :cond_3

    .line 1136093
    :cond_2
    :goto_1
    move v0, v2

    .line 1136094
    goto :goto_0

    .line 1136095
    :cond_3
    iget-object v0, p0, LX/6kK;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1136096
    :goto_2
    iget-object v3, p1, LX/6kK;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1136097
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1136098
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136099
    iget-object v0, p0, LX/6kK;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6kK;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1136100
    :cond_5
    iget-object v0, p0, LX/6kK;->callState:Ljava/lang/String;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1136101
    :goto_4
    iget-object v3, p1, LX/6kK;->callState:Ljava/lang/String;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1136102
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1136103
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136104
    iget-object v0, p0, LX/6kK;->callState:Ljava/lang/String;

    iget-object v3, p1, LX/6kK;->callState:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1136105
    :cond_7
    iget-object v0, p0, LX/6kK;->serverInfoData:Ljava/lang/String;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1136106
    :goto_6
    iget-object v3, p1, LX/6kK;->serverInfoData:Ljava/lang/String;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1136107
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1136108
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136109
    iget-object v0, p0, LX/6kK;->serverInfoData:Ljava/lang/String;

    iget-object v3, p1, LX/6kK;->serverInfoData:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1136110
    :cond_9
    iget-object v0, p0, LX/6kK;->initiator:LX/6kt;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1136111
    :goto_8
    iget-object v3, p1, LX/6kK;->initiator:LX/6kt;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1136112
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1136113
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136114
    iget-object v0, p0, LX/6kK;->initiator:LX/6kt;

    iget-object v3, p1, LX/6kK;->initiator:LX/6kt;

    invoke-virtual {v0, v3}, LX/6kt;->a(LX/6kt;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_b
    move v2, v1

    .line 1136115
    goto :goto_1

    :cond_c
    move v0, v2

    .line 1136116
    goto :goto_2

    :cond_d
    move v3, v2

    .line 1136117
    goto :goto_3

    :cond_e
    move v0, v2

    .line 1136118
    goto :goto_4

    :cond_f
    move v3, v2

    .line 1136119
    goto :goto_5

    :cond_10
    move v0, v2

    .line 1136120
    goto :goto_6

    :cond_11
    move v3, v2

    .line 1136121
    goto :goto_7

    :cond_12
    move v0, v2

    .line 1136122
    goto :goto_8

    :cond_13
    move v3, v2

    .line 1136123
    goto :goto_9
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1136087
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1136084
    sget-boolean v0, LX/6kK;->a:Z

    .line 1136085
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kK;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1136086
    return-object v0
.end method
