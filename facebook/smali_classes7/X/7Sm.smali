.class public LX/7Sm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Sb;


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 1209141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1209142
    iput p1, p0, LX/7Sm;->a:I

    .line 1209143
    iput p2, p0, LX/7Sm;->b:I

    .line 1209144
    return-void
.end method


# virtual methods
.method public final a()LX/7Sc;
    .locals 1

    .prologue
    .line 1209145
    sget-object v0, LX/7Sc;->INPUT_ROTATION:LX/7Sc;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1209146
    const/4 v0, 0x1

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1209147
    iget v0, p0, LX/7Sm;->a:I

    mul-int/lit8 v0, v0, 0x5a

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1209148
    if-ne p0, p1, :cond_1

    .line 1209149
    :cond_0
    :goto_0
    return v0

    .line 1209150
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1209151
    goto :goto_0

    .line 1209152
    :cond_3
    check-cast p1, LX/7Sm;

    .line 1209153
    iget v2, p0, LX/7Sm;->a:I

    iget v3, p1, LX/7Sm;->a:I

    if-ne v2, v3, :cond_4

    iget v2, p0, LX/7Sm;->b:I

    iget v3, p1, LX/7Sm;->b:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1209154
    iget v0, p0, LX/7Sm;->a:I

    .line 1209155
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/7Sm;->b:I

    add-int/2addr v0, v1

    .line 1209156
    return v0
.end method
