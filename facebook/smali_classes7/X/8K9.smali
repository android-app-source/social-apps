.class public LX/8K9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/8K9;


# instance fields
.field private a:LX/0uf;

.field private b:LX/0W3;

.field public c:LX/1jr;

.field private d:Ljava/lang/Object;

.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(LX/0uf;LX/0W3;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 1329875
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1329876
    iput v0, p0, LX/8K9;->e:I

    .line 1329877
    iput v0, p0, LX/8K9;->f:I

    .line 1329878
    iput v0, p0, LX/8K9;->g:I

    .line 1329879
    iput v0, p0, LX/8K9;->h:I

    .line 1329880
    iput-object p1, p0, LX/8K9;->a:LX/0uf;

    .line 1329881
    iput-object p2, p0, LX/8K9;->b:LX/0W3;

    .line 1329882
    return-void
.end method

.method public static a(LX/0QB;)LX/8K9;
    .locals 5

    .prologue
    .line 1329883
    sget-object v0, LX/8K9;->i:LX/8K9;

    if-nez v0, :cond_1

    .line 1329884
    const-class v1, LX/8K9;

    monitor-enter v1

    .line 1329885
    :try_start_0
    sget-object v0, LX/8K9;->i:LX/8K9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1329886
    if-eqz v2, :cond_0

    .line 1329887
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1329888
    new-instance p0, LX/8K9;

    invoke-static {v0}, LX/0ts;->a(LX/0QB;)LX/0uf;

    move-result-object v3

    check-cast v3, LX/0uf;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-direct {p0, v3, v4}, LX/8K9;-><init>(LX/0uf;LX/0W3;)V

    .line 1329889
    move-object v0, p0

    .line 1329890
    sput-object v0, LX/8K9;->i:LX/8K9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1329891
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1329892
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1329893
    :cond_1
    sget-object v0, LX/8K9;->i:LX/8K9;

    return-object v0

    .line 1329894
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1329895
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/8Ob;)V
    .locals 4

    .prologue
    .line 1329896
    new-instance v0, LX/8K8;

    invoke-direct {v0, p1}, LX/8K8;-><init>(LX/8Ob;)V

    .line 1329897
    iget-object v1, p0, LX/8K9;->a:LX/0uf;

    sget-wide v2, LX/0X5;->hG:J

    .line 1329898
    invoke-static {v1, v2, v3}, LX/0uf;->b(LX/0uf;J)LX/1jp;

    move-result-object p1

    invoke-static {v1, p1, v0}, LX/0uf;->a(LX/0uf;LX/1jp;LX/8K8;)LX/1jr;

    move-result-object p1

    move-object v0, p1

    .line 1329899
    iput-object v0, p0, LX/8K9;->c:LX/1jr;

    .line 1329900
    iget-object v0, p0, LX/8K9;->d:Ljava/lang/Object;

    iget-object v1, p0, LX/8K9;->c:LX/1jr;

    .line 1329901
    iget-object v2, v1, LX/1jr;->a:LX/1jp;

    move-object v1, v2

    .line 1329902
    if-eq v0, v1, :cond_0

    .line 1329903
    iget-object v0, p0, LX/8K9;->c:LX/1jr;

    const-string v1, "dimension"

    invoke-virtual {v0, v1}, LX/1jr;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/8K9;->e:I

    .line 1329904
    iget-object v0, p0, LX/8K9;->c:LX/1jr;

    const-string v1, "bitrate"

    invoke-virtual {v0, v1}, LX/1jr;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/8K9;->f:I

    .line 1329905
    iget-object v0, p0, LX/8K9;->c:LX/1jr;

    const-string v1, "bytes_threshold"

    invoke-virtual {v0, v1}, LX/1jr;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/8K9;->g:I

    .line 1329906
    iget-object v0, p0, LX/8K9;->c:LX/1jr;

    const-string v1, "ratio_threshold"

    invoke-virtual {v0, v1}, LX/1jr;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/8K9;->h:I

    .line 1329907
    iget-object v0, p0, LX/8K9;->c:LX/1jr;

    .line 1329908
    iget-object v1, v0, LX/1jr;->a:LX/1jp;

    move-object v0, v1

    .line 1329909
    iput-object v0, p0, LX/8K9;->d:Ljava/lang/Object;

    .line 1329910
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 1329911
    iget-object v0, p0, LX/8K9;->b:LX/0W3;

    sget-wide v2, LX/0X5;->hH:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 1329912
    iget-object v0, p0, LX/8K9;->c:LX/1jr;

    iget v1, p0, LX/8K9;->e:I

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, LX/1jr;->a(IJ)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final c()I
    .locals 4

    .prologue
    .line 1329913
    iget-object v0, p0, LX/8K9;->c:LX/1jr;

    iget v1, p0, LX/8K9;->f:I

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, LX/1jr;->a(IJ)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final d()I
    .locals 4

    .prologue
    .line 1329914
    iget-object v0, p0, LX/8K9;->c:LX/1jr;

    iget v1, p0, LX/8K9;->g:I

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, LX/1jr;->a(IJ)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final e()F
    .locals 4

    .prologue
    .line 1329915
    iget-object v0, p0, LX/8K9;->c:LX/1jr;

    iget v1, p0, LX/8K9;->h:I

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    invoke-virtual {v0, v1, v2, v3}, LX/1jr;->a(ID)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method
