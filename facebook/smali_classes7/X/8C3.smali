.class public LX/8C3;
.super LX/6LI;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/content/res/Resources;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/0dN;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1310085
    const-string v0, "NetworkEmpathyStatusNotification"

    invoke-direct {p0, v0}, LX/6LI;-><init>(Ljava/lang/String;)V

    .line 1310086
    iput-object p1, p0, LX/8C3;->a:Landroid/content/Context;

    .line 1310087
    iput-object p2, p0, LX/8C3;->b:Landroid/view/LayoutInflater;

    .line 1310088
    iput-object p3, p0, LX/8C3;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1310089
    iget-object v0, p0, LX/8C3;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/8C3;->c:Landroid/content/res/Resources;

    .line 1310090
    new-instance v0, LX/8C2;

    invoke-direct {v0, p0}, LX/8C2;-><init>(LX/8C3;)V

    iput-object v0, p0, LX/8C3;->e:LX/0dN;

    .line 1310091
    return-void
.end method

.method public static e(LX/8C3;)V
    .locals 3

    .prologue
    .line 1310092
    iget-object v0, p0, LX/8C3;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/4gn;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    move v0, v0

    .line 1310093
    if-eqz v0, :cond_0

    .line 1310094
    iget-object v0, p0, LX/6LI;->a:LX/6LN;

    move-object v0, v0

    .line 1310095
    invoke-virtual {v0, p0}, LX/6LN;->a(LX/6LI;)V

    .line 1310096
    :goto_0
    return-void

    .line 1310097
    :cond_0
    iget-object v0, p0, LX/6LI;->a:LX/6LN;

    move-object v0, v0

    .line 1310098
    invoke-virtual {v0, p0}, LX/6LN;->b(LX/6LI;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1310099
    iget-object v0, p0, LX/8C3;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f03018f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/banner/BasicBannerNotificationView;

    .line 1310100
    const-string v1, ""

    .line 1310101
    iget-object v2, p0, LX/8C3;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/4gn;->e:LX/0Tn;

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 1310102
    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 1310103
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " Until "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1310104
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 1310105
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/8C3;->a:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1310106
    :cond_0
    new-instance v2, LX/2M6;

    invoke-direct {v2}, LX/2M6;-><init>()V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[FB-ONLY] 2G Empathy Enabled"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1310107
    iput-object v1, v2, LX/2M6;->a:Ljava/lang/CharSequence;

    .line 1310108
    move-object v1, v2

    .line 1310109
    iget-object v2, p0, LX/8C3;->c:Landroid/content/res/Resources;

    const v3, 0x7f0a01fa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1310110
    iput-object v2, v1, LX/2M6;->c:Landroid/graphics/drawable/Drawable;

    .line 1310111
    move-object v1, v1

    .line 1310112
    iget-object v2, p0, LX/8C3;->c:Landroid/content/res/Resources;

    const v3, 0x7f0a01fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1310113
    iput v2, v1, LX/2M6;->i:I

    .line 1310114
    move-object v1, v1

    .line 1310115
    invoke-virtual {v1}, LX/2M6;->a()LX/6LR;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setParams(LX/6LR;)V

    .line 1310116
    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1310117
    invoke-static {p0}, LX/8C3;->e(LX/8C3;)V

    .line 1310118
    iget-object v0, p0, LX/8C3;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/4gn;->b:LX/0Tn;

    iget-object v2, p0, LX/8C3;->e:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 1310119
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1310120
    iget-object v0, p0, LX/8C3;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/4gn;->b:LX/0Tn;

    iget-object v2, p0, LX/8C3;->e:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;LX/0dN;)V

    .line 1310121
    return-void
.end method
