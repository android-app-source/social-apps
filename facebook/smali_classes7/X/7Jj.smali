.class public final enum LX/7Jj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Jj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Jj;

.field public static final enum DISMISS:LX/7Jj;

.field public static final enum ERROR_IO:LX/7Jj;

.field public static final enum MALFORMED:LX/7Jj;

.field public static final enum NO_SOURCE:LX/7Jj;

.field public static final enum PLAYBACK_EXCEPTION:LX/7Jj;

.field public static final enum PLAYERSERVICE_DEAD:LX/7Jj;

.field public static final enum SERVER_DIED:LX/7Jj;

.field public static final enum TIMED_OUT:LX/7Jj;

.field public static final enum UNKNOWN:LX/7Jj;

.field public static final enum UNSUPPORTED:LX/7Jj;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1194063
    new-instance v0, LX/7Jj;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v4, v2}, LX/7Jj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jj;->UNKNOWN:LX/7Jj;

    .line 1194064
    new-instance v0, LX/7Jj;

    const-string v1, "NO_SOURCE"

    const-string v2, "no_source"

    invoke-direct {v0, v1, v5, v2}, LX/7Jj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jj;->NO_SOURCE:LX/7Jj;

    .line 1194065
    new-instance v0, LX/7Jj;

    const-string v1, "SERVER_DIED"

    const-string v2, "server_died"

    invoke-direct {v0, v1, v6, v2}, LX/7Jj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jj;->SERVER_DIED:LX/7Jj;

    .line 1194066
    new-instance v0, LX/7Jj;

    const-string v1, "MALFORMED"

    const-string v2, "malformed"

    invoke-direct {v0, v1, v7, v2}, LX/7Jj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jj;->MALFORMED:LX/7Jj;

    .line 1194067
    new-instance v0, LX/7Jj;

    const-string v1, "ERROR_IO"

    const-string v2, "error_io"

    invoke-direct {v0, v1, v8, v2}, LX/7Jj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jj;->ERROR_IO:LX/7Jj;

    .line 1194068
    new-instance v0, LX/7Jj;

    const-string v1, "TIMED_OUT"

    const/4 v2, 0x5

    const-string v3, "timed_out"

    invoke-direct {v0, v1, v2, v3}, LX/7Jj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jj;->TIMED_OUT:LX/7Jj;

    .line 1194069
    new-instance v0, LX/7Jj;

    const-string v1, "UNSUPPORTED"

    const/4 v2, 0x6

    const-string v3, "unsupported"

    invoke-direct {v0, v1, v2, v3}, LX/7Jj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jj;->UNSUPPORTED:LX/7Jj;

    .line 1194070
    new-instance v0, LX/7Jj;

    const-string v1, "PLAYBACK_EXCEPTION"

    const/4 v2, 0x7

    const-string v3, "playback_exception"

    invoke-direct {v0, v1, v2, v3}, LX/7Jj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jj;->PLAYBACK_EXCEPTION:LX/7Jj;

    .line 1194071
    new-instance v0, LX/7Jj;

    const-string v1, "PLAYERSERVICE_DEAD"

    const/16 v2, 0x8

    const-string v3, "playerservice_dead"

    invoke-direct {v0, v1, v2, v3}, LX/7Jj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jj;->PLAYERSERVICE_DEAD:LX/7Jj;

    .line 1194072
    new-instance v0, LX/7Jj;

    const-string v1, "DISMISS"

    const/16 v2, 0x9

    const-string v3, "dismiss"

    invoke-direct {v0, v1, v2, v3}, LX/7Jj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jj;->DISMISS:LX/7Jj;

    .line 1194073
    const/16 v0, 0xa

    new-array v0, v0, [LX/7Jj;

    sget-object v1, LX/7Jj;->UNKNOWN:LX/7Jj;

    aput-object v1, v0, v4

    sget-object v1, LX/7Jj;->NO_SOURCE:LX/7Jj;

    aput-object v1, v0, v5

    sget-object v1, LX/7Jj;->SERVER_DIED:LX/7Jj;

    aput-object v1, v0, v6

    sget-object v1, LX/7Jj;->MALFORMED:LX/7Jj;

    aput-object v1, v0, v7

    sget-object v1, LX/7Jj;->ERROR_IO:LX/7Jj;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7Jj;->TIMED_OUT:LX/7Jj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7Jj;->UNSUPPORTED:LX/7Jj;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7Jj;->PLAYBACK_EXCEPTION:LX/7Jj;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7Jj;->PLAYERSERVICE_DEAD:LX/7Jj;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7Jj;->DISMISS:LX/7Jj;

    aput-object v2, v0, v1

    sput-object v0, LX/7Jj;->$VALUES:[LX/7Jj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1194074
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1194075
    iput-object p3, p0, LX/7Jj;->value:Ljava/lang/String;

    .line 1194076
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Jj;
    .locals 1

    .prologue
    .line 1194077
    const-class v0, LX/7Jj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Jj;

    return-object v0
.end method

.method public static values()[LX/7Jj;
    .locals 1

    .prologue
    .line 1194078
    sget-object v0, LX/7Jj;->$VALUES:[LX/7Jj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Jj;

    return-object v0
.end method
