.class public LX/6sE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "Lcom/facebook/payments/form/model/FormRowDefinition;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1153001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153002
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1153003
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1153004
    invoke-virtual {p2}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1153005
    const-string v3, "identifier"

    invoke-virtual {v0, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1153006
    const-string v3, "identifier"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/6xN;->forValue(Ljava/lang/String;)LX/6xN;

    move-result-object v3

    .line 1153007
    sget-object v4, LX/6xN;->UNKNOWN:LX/6xN;

    if-eq v3, v4, :cond_0

    .line 1153008
    const-string v4, "placeholder_text"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    const-string p0, "optional"

    invoke-virtual {v0, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    .line 1153009
    invoke-static {p0}, LX/16N;->g(LX/0lF;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, LX/6xe;->OPTIONAL:LX/6xe;

    :goto_1
    move-object p0, p1

    .line 1153010
    const-string p1, "type"

    invoke-virtual {v0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p1

    invoke-static {p1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/6xO;->of(Ljava/lang/String;)LX/6xO;

    move-result-object p1

    invoke-static {v3, v4, p0, p1}, Lcom/facebook/payments/form/model/FormFieldAttributes;->a(LX/6xN;Ljava/lang/String;LX/6xe;LX/6xO;)LX/6xM;

    move-result-object v4

    const-string p0, "prefilled_content"

    invoke-virtual {v0, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-static {p0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object p0

    .line 1153011
    iput-object p0, v4, LX/6xM;->e:Ljava/lang/String;

    .line 1153012
    move-object v4, v4

    .line 1153013
    const-string p0, "length"

    invoke-virtual {v0, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    const p1, 0x7fffffff

    invoke-static {p0, p1}, LX/16N;->a(LX/0lF;I)I

    move-result p0

    .line 1153014
    iput p0, v4, LX/6xM;->f:I

    .line 1153015
    move-object v4, v4

    .line 1153016
    const-string p0, "currency"

    invoke-virtual {v0, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-static {p0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object p0

    .line 1153017
    iput-object p0, v4, LX/6xM;->g:Ljava/lang/String;

    .line 1153018
    move-object v4, v4

    .line 1153019
    const-string p0, "price_tag"

    invoke-virtual {v0, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-static {p0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object p0

    .line 1153020
    iput-object p0, v4, LX/6xM;->h:Ljava/lang/String;

    .line 1153021
    move-object v4, v4

    .line 1153022
    invoke-virtual {v4}, LX/6xM;->a()Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v4

    move-object v0, v4

    .line 1153023
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 1153024
    :cond_1
    new-instance v0, Lcom/facebook/payments/form/model/FormRowDefinition;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/payments/form/model/FormRowDefinition;-><init>(LX/0Px;)V

    return-object v0

    :cond_2
    sget-object p1, LX/6xe;->REQUIRED:LX/6xe;

    goto :goto_1
.end method
