.class public LX/6wb;
.super LX/6tx;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6tx",
        "<",
        "Lcom/facebook/payments/contactinfo/protocol/model/GetPhoneNumberContactInfoResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1157899
    const-class v0, Lcom/facebook/payments/contactinfo/protocol/model/GetPhoneNumberContactInfoResult;

    invoke-direct {p0, p1, v0}, LX/6tx;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 1157900
    return-void
.end method

.method public static b(LX/0QB;)LX/6wb;
    .locals 2

    .prologue
    .line 1157875
    new-instance v1, LX/6wb;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/6wb;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 1157876
    return-object v1
.end method


# virtual methods
.method public final a(LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1157877
    invoke-virtual {p1}, LX/1pN;->j()V

    .line 1157878
    invoke-virtual {p1}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1157879
    const-string v1, "viewer"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 1157880
    const-string v2, "pay_account"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 1157881
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1157882
    const-string p0, "phones"

    invoke-static {v1, p0}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 1157883
    invoke-static {}, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->newBuilder()LX/6vj;

    move-result-object p1

    const-string v0, "id"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1157884
    iput-object v0, p1, LX/6vj;->a:Ljava/lang/String;

    .line 1157885
    move-object p1, p1

    .line 1157886
    const-string v0, "is_default"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    .line 1157887
    iput-boolean v0, p1, LX/6vj;->d:Z

    .line 1157888
    move-object p1, p1

    .line 1157889
    const-string v0, "intl_number_with_plus"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1157890
    iput-object v0, p1, LX/6vj;->b:Ljava/lang/String;

    .line 1157891
    move-object p1, p1

    .line 1157892
    const-string v0, "formatted_intl_number_with_plus"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1157893
    iput-object v0, p1, LX/6vj;->c:Ljava/lang/String;

    .line 1157894
    move-object p1, p1

    .line 1157895
    invoke-virtual {p1}, LX/6vj;->e()Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;

    move-result-object p1

    move-object v1, p1

    .line 1157896
    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1157897
    :cond_0
    new-instance v1, Lcom/facebook/payments/contactinfo/protocol/model/GetPhoneNumberContactInfoResult;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/payments/contactinfo/protocol/model/GetPhoneNumberContactInfoResult;-><init>(LX/0Px;)V

    move-object v0, v1

    .line 1157898
    return-object v0
.end method

.method public final b()LX/14N;
    .locals 4

    .prologue
    .line 1157858
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1157859
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "q"

    const-string v3, "viewer() {pay_account {phones {id, is_default, intl_number_with_plus, formatted_intl_number_with_plus}}}"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1157860
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "get_phone_number_contact_info"

    .line 1157861
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1157862
    move-object v1, v1

    .line 1157863
    const-string v2, "GET"

    .line 1157864
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1157865
    move-object v1, v1

    .line 1157866
    const-string v2, "graphql"

    .line 1157867
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1157868
    move-object v1, v1

    .line 1157869
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1157870
    move-object v0, v1

    .line 1157871
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1157872
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1157873
    move-object v0, v0

    .line 1157874
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157857
    const-string v0, "get_phone_number_contact_info"

    return-object v0
.end method
