.class public final enum LX/7Ct;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Ct;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Ct;

.field public static final enum BOTTOM:LX/7Ct;

.field public static final enum LEFT:LX/7Ct;

.field public static final enum NONE:LX/7Ct;

.field public static final enum RIGHT:LX/7Ct;

.field public static final enum TOP:LX/7Ct;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1181923
    new-instance v0, LX/7Ct;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v2}, LX/7Ct;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ct;->LEFT:LX/7Ct;

    .line 1181924
    new-instance v0, LX/7Ct;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v3}, LX/7Ct;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ct;->RIGHT:LX/7Ct;

    .line 1181925
    new-instance v0, LX/7Ct;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v4}, LX/7Ct;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ct;->TOP:LX/7Ct;

    .line 1181926
    new-instance v0, LX/7Ct;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v5}, LX/7Ct;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ct;->BOTTOM:LX/7Ct;

    .line 1181927
    new-instance v0, LX/7Ct;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v6}, LX/7Ct;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ct;->NONE:LX/7Ct;

    .line 1181928
    const/4 v0, 0x5

    new-array v0, v0, [LX/7Ct;

    sget-object v1, LX/7Ct;->LEFT:LX/7Ct;

    aput-object v1, v0, v2

    sget-object v1, LX/7Ct;->RIGHT:LX/7Ct;

    aput-object v1, v0, v3

    sget-object v1, LX/7Ct;->TOP:LX/7Ct;

    aput-object v1, v0, v4

    sget-object v1, LX/7Ct;->BOTTOM:LX/7Ct;

    aput-object v1, v0, v5

    sget-object v1, LX/7Ct;->NONE:LX/7Ct;

    aput-object v1, v0, v6

    sput-object v0, LX/7Ct;->$VALUES:[LX/7Ct;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1181929
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Ct;
    .locals 1

    .prologue
    .line 1181930
    const-class v0, LX/7Ct;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Ct;

    return-object v0
.end method

.method public static values()[LX/7Ct;
    .locals 1

    .prologue
    .line 1181931
    sget-object v0, LX/7Ct;->$VALUES:[LX/7Ct;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Ct;

    return-object v0
.end method
