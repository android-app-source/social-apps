.class public LX/6oL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1148102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148103
    new-instance v0, LX/6oK;

    invoke-direct {v0, p0, p1}, LX/6oK;-><init>(LX/6oL;Landroid/content/Context;)V

    invoke-static {v0}, LX/3R5;->a(LX/0Or;)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/6oL;->a:LX/0Ot;

    .line 1148104
    return-void
.end method

.method public static a(LX/0QB;)LX/6oL;
    .locals 4

    .prologue
    .line 1148090
    const-class v1, LX/6oL;

    monitor-enter v1

    .line 1148091
    :try_start_0
    sget-object v0, LX/6oL;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1148092
    sput-object v2, LX/6oL;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1148093
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1148094
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1148095
    new-instance p0, LX/6oL;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/6oL;-><init>(Landroid/content/Context;)V

    .line 1148096
    move-object v0, p0

    .line 1148097
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1148098
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6oL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1148099
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1148100
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Landroid/hardware/fingerprint/FingerprintManager;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 1148101
    iget-object v0, p0, LX/6oL;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/fingerprint/FingerprintManager;

    return-object v0
.end method
