.class public final LX/7y7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:LX/7y8;


# direct methods
.method public constructor <init>(LX/7y8;)V
    .locals 0

    .prologue
    .line 1278753
    iput-object p1, p0, LX/7y7;->a:LX/7y8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 6

    .prologue
    .line 1278744
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 1278745
    iget-object v0, p0, LX/7y7;->a:LX/7y8;

    iget-object v0, v0, LX/7y8;->p:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-object v1, p0, LX/7y7;->a:LX/7y8;

    iget-object v1, v1, LX/7y8;->o:LX/7ws;

    .line 1278746
    iget v2, v1, LX/7ws;->a:I

    move v1, v2

    .line 1278747
    iget-object v2, p0, LX/7y7;->a:LX/7y8;

    iget-object v2, v2, LX/7y8;->o:LX/7ws;

    .line 1278748
    iget-object v3, v2, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v2, v3

    .line 1278749
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->fa_()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v2

    iget-object v3, p0, LX/7y7;->a:LX/7y8;

    iget-object v3, v3, LX/7y8;->o:LX/7ws;

    .line 1278750
    iget-object v4, v3, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v3, v4

    .line 1278751
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->eZ_()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/facebook/events/tickets/modal/model/FieldItem;

    iget-object v5, p0, LX/7y7;->a:LX/7y8;

    iget-object v5, v5, LX/7y8;->q:LX/0Px;

    invoke-virtual {v5, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/7on;

    invoke-interface {v5}, LX/7on;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/events/tickets/modal/model/FieldItem;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, LX/7y7;->a:LX/7y8;

    invoke-virtual {v5}, LX/1a1;->e()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a(ILcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;Ljava/lang/String;Lcom/facebook/events/tickets/modal/model/FieldItem;I)V

    .line 1278752
    :cond_0
    return-void
.end method
