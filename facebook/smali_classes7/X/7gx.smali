.class public final LX/7gx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/7gz;

.field public b:LX/7gy;

.field public c:J

.field public d:Lcom/facebook/audience/model/AudienceControlData;

.field public e:Landroid/net/Uri;

.field public f:Landroid/net/Uri;

.field public g:Landroid/net/Uri;

.field public h:Z

.field public i:Ljava/lang/String;

.field public j:I

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:J

.field public n:LX/7gy;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1225146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225147
    return-void
.end method

.method public constructor <init>(LX/7h0;)V
    .locals 2

    .prologue
    .line 1225149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225150
    iget-object v0, p1, LX/7h0;->a:LX/7gz;

    iput-object v0, p0, LX/7gx;->a:LX/7gz;

    .line 1225151
    iget-object v0, p1, LX/7h0;->b:LX/7gy;

    iput-object v0, p0, LX/7gx;->b:LX/7gy;

    .line 1225152
    iget-wide v0, p1, LX/7h0;->c:J

    iput-wide v0, p0, LX/7gx;->c:J

    .line 1225153
    iget-object v0, p1, LX/7h0;->d:Lcom/facebook/audience/model/AudienceControlData;

    iput-object v0, p0, LX/7gx;->d:Lcom/facebook/audience/model/AudienceControlData;

    .line 1225154
    iget-object v0, p1, LX/7h0;->e:Landroid/net/Uri;

    iput-object v0, p0, LX/7gx;->e:Landroid/net/Uri;

    .line 1225155
    iget-object v0, p1, LX/7h0;->f:Landroid/net/Uri;

    iput-object v0, p0, LX/7gx;->f:Landroid/net/Uri;

    .line 1225156
    iget-object v0, p1, LX/7h0;->g:Landroid/net/Uri;

    iput-object v0, p0, LX/7gx;->g:Landroid/net/Uri;

    .line 1225157
    iget-boolean v0, p1, LX/7h0;->h:Z

    iput-boolean v0, p0, LX/7gx;->h:Z

    .line 1225158
    iget-object v0, p1, LX/7h0;->i:Ljava/lang/String;

    iput-object v0, p0, LX/7gx;->i:Ljava/lang/String;

    .line 1225159
    iget v0, p1, LX/7h0;->j:I

    iput v0, p0, LX/7gx;->j:I

    .line 1225160
    iget-object v0, p1, LX/7h0;->k:Ljava/lang/String;

    iput-object v0, p0, LX/7gx;->k:Ljava/lang/String;

    .line 1225161
    iget-boolean v0, p1, LX/7h0;->l:Z

    iput-boolean v0, p0, LX/7gx;->l:Z

    .line 1225162
    iget-wide v0, p1, LX/7h0;->m:J

    iput-wide v0, p0, LX/7gx;->m:J

    .line 1225163
    iget-object v0, p1, LX/7h0;->n:LX/7gy;

    move-object v0, v0

    .line 1225164
    iput-object v0, p0, LX/7gx;->n:LX/7gy;

    .line 1225165
    return-void
.end method


# virtual methods
.method public final a()LX/7h0;
    .locals 19

    .prologue
    .line 1225148
    new-instance v1, LX/7h0;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7gx;->a:LX/7gz;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7gx;->b:LX/7gy;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7gx;->c:J

    move-object/from16 v0, p0

    iget-object v6, v0, LX/7gx;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7gx;->e:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7gx;->f:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/7gx;->g:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-boolean v10, v0, LX/7gx;->h:Z

    move-object/from16 v0, p0

    iget-object v11, v0, LX/7gx;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v12, v0, LX/7gx;->j:I

    move-object/from16 v0, p0

    iget-object v13, v0, LX/7gx;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v14, v0, LX/7gx;->l:Z

    move-object/from16 v0, p0

    iget-wide v15, v0, LX/7gx;->m:J

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7gx;->n:LX/7gy;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-direct/range {v1 .. v18}, LX/7h0;-><init>(LX/7gz;LX/7gy;JLcom/facebook/audience/model/AudienceControlData;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;ZLjava/lang/String;ILjava/lang/String;ZJLX/7gy;B)V

    return-object v1
.end method
