.class public LX/7iy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1228919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1228920
    iput-object p1, p0, LX/7iy;->a:Landroid/net/Uri;

    .line 1228921
    iput-object p2, p0, LX/7iy;->b:Ljava/lang/String;

    .line 1228922
    iput-object p3, p0, LX/7iy;->c:Ljava/lang/String;

    .line 1228923
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1228924
    if-eqz p1, :cond_0

    instance-of v2, p1, LX/7iy;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 1228925
    :cond_1
    :goto_0
    return v0

    .line 1228926
    :cond_2
    if-eq p0, p1, :cond_1

    .line 1228927
    check-cast p1, LX/7iy;

    .line 1228928
    iget-object v2, p0, LX/7iy;->a:Landroid/net/Uri;

    .line 1228929
    iget-object v3, p1, LX/7iy;->a:Landroid/net/Uri;

    move-object v3, v3

    .line 1228930
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/7iy;->b:Ljava/lang/String;

    .line 1228931
    iget-object v3, p1, LX/7iy;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1228932
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/7iy;->c:Ljava/lang/String;

    .line 1228933
    iget-object v3, p1, LX/7iy;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1228934
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1228935
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/7iy;->a:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/7iy;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/7iy;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
