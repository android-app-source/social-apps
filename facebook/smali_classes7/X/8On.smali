.class public LX/8On;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/8OL;

.field public b:LX/8Ne;

.field private final c:LX/73w;

.field private final d:LX/73z;

.field private final e:LX/74b;

.field private final f:Lcom/facebook/photos/upload/operation/UploadOperation;

.field private final g:LX/0cX;

.field public h:LX/8Oo;

.field private i:LX/0lB;


# direct methods
.method public constructor <init>(LX/8OL;LX/8Ne;LX/73w;LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Oo;LX/0lB;LX/0cX;)V
    .locals 1

    .prologue
    .line 1340952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1340953
    const-string v0, "VideoUploaderExceptionHandler requires non null retry policy"

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340954
    iput-object p1, p0, LX/8On;->a:LX/8OL;

    .line 1340955
    iput-object p2, p0, LX/8On;->b:LX/8Ne;

    .line 1340956
    iput-object p3, p0, LX/8On;->c:LX/73w;

    .line 1340957
    iput-object p4, p0, LX/8On;->e:LX/74b;

    .line 1340958
    iput-object p5, p0, LX/8On;->f:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1340959
    iput-object p6, p0, LX/8On;->h:LX/8Oo;

    .line 1340960
    const/4 v0, 0x1

    .line 1340961
    new-instance p1, LX/73z;

    invoke-direct {p1, v0}, LX/73z;-><init>(Z)V

    move-object v0, p1

    .line 1340962
    iput-object v0, p0, LX/8On;->d:LX/73z;

    .line 1340963
    iput-object p7, p0, LX/8On;->i:LX/0lB;

    .line 1340964
    iput-object p8, p0, LX/8On;->g:LX/0cX;

    .line 1340965
    return-void
.end method


# virtual methods
.method public final a()LX/8Ne;
    .locals 1

    .prologue
    .line 1340966
    iget-object v0, p0, LX/8On;->b:LX/8Ne;

    return-object v0
.end method

.method public final a(LX/8Ne;)LX/8On;
    .locals 9

    .prologue
    .line 1340967
    iget-object v0, p0, LX/8On;->h:LX/8Oo;

    const-string v1, "You probably wanted this to have a progress listener. If not remove this check :)"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340968
    new-instance v0, LX/8On;

    iget-object v1, p0, LX/8On;->a:LX/8OL;

    iget-object v3, p0, LX/8On;->c:LX/73w;

    iget-object v4, p0, LX/8On;->e:LX/74b;

    iget-object v5, p0, LX/8On;->f:Lcom/facebook/photos/upload/operation/UploadOperation;

    iget-object v6, p0, LX/8On;->h:LX/8Oo;

    iget-object v7, p0, LX/8On;->i:LX/0lB;

    iget-object v8, p0, LX/8On;->g:LX/0cX;

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, LX/8On;-><init>(LX/8OL;LX/8Ne;LX/73w;LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Oo;LX/0lB;LX/0cX;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Exception;LX/8Oi;JLX/8O2;)Landroid/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Exception;",
            "LX/8Oi;",
            "J",
            "LX/8O2;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1340969
    iget-object v0, p0, LX/8On;->d:LX/73z;

    invoke-virtual {v0, p1}, LX/73z;->a(Ljava/lang/Exception;)V

    .line 1340970
    iget-object v0, p0, LX/8On;->h:LX/8Oo;

    if-eqz v0, :cond_0

    .line 1340971
    iget-object v0, p0, LX/8On;->h:LX/8Oo;

    iget-object v3, p0, LX/8On;->d:LX/73z;

    invoke-virtual {v0, v3, p2, p5}, LX/8Oo;->a(LX/73z;LX/8Oi;LX/8O2;)V

    .line 1340972
    :cond_0
    sget-object v0, LX/8Oi;->RECEIVE:LX/8Oi;

    if-ne p2, v0, :cond_1

    iget-object v0, p0, LX/8On;->b:LX/8Ne;

    invoke-interface {v0}, LX/8Ne;->b()I

    move-result v0

    int-to-long v4, v0

    cmp-long v0, p3, v4

    if-ltz v0, :cond_1

    iget-object v0, p0, LX/8On;->h:LX/8Oo;

    if-eqz v0, :cond_1

    .line 1340973
    iget-object v0, p0, LX/8On;->h:LX/8Oo;

    iget-object v3, p0, LX/8On;->d:LX/73z;

    invoke-virtual {v0, v3}, LX/8Oo;->a(LX/73z;)V

    .line 1340974
    :cond_1
    iget-object v0, p0, LX/8On;->a:LX/8OL;

    const-string v3, "while uploading video"

    invoke-virtual {v0, v3}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1340975
    iget-object v0, p0, LX/8On;->d:LX/73z;

    invoke-virtual {v0}, LX/73z;->e()I

    move-result v0

    .line 1340976
    if-eq v0, v2, :cond_2

    const/16 v3, 0x1770

    if-eq v0, v3, :cond_2

    const/16 v3, 0x64

    if-eq v0, v3, :cond_2

    const/16 v3, 0xc8

    if-ne v0, v3, :cond_3

    .line 1340977
    :cond_2
    iget-object v0, p0, LX/8On;->d:LX/73z;

    .line 1340978
    iget-object v1, v0, LX/73z;->g:Ljava/lang/Exception;

    move-object v0, v1

    .line 1340979
    throw v0

    .line 1340980
    :cond_3
    iget-object v0, p0, LX/8On;->a:LX/8OL;

    .line 1340981
    iget-boolean v3, v0, LX/8OL;->d:Z

    move v0, v3

    .line 1340982
    if-eqz v0, :cond_5

    .line 1340983
    sget-object v0, LX/8Oi;->START:LX/8Oi;

    if-ne p2, v0, :cond_4

    .line 1340984
    iget-object v0, p0, LX/8On;->c:LX/73w;

    iget-object v3, p0, LX/8On;->e:LX/74b;

    iget-object v4, p0, LX/8On;->f:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0, v3, v4}, LX/73w;->e(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1340985
    :cond_4
    iget-object v0, p0, LX/8On;->a:LX/8OL;

    const-string v3, "Cancel video upload"

    invoke-virtual {v0, v3}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1340986
    :cond_5
    const/4 v3, 0x0

    .line 1340987
    instance-of v0, p1, LX/2Oo;

    if-eqz v0, :cond_7

    .line 1340988
    check-cast p1, LX/2Oo;

    .line 1340989
    invoke-virtual {p1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    .line 1340990
    if-eqz v0, :cond_7

    .line 1340991
    iget v4, v0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorSubCode:I

    move v4, v4

    .line 1340992
    const v5, 0x14cc5d

    if-ne v4, v5, :cond_7

    .line 1340993
    :try_start_0
    iget-object v4, p0, LX/8On;->i:LX/0lB;

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->d()Ljava/lang/String;

    move-result-object v0

    const-class v5, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler$InvalidOffsetErrorData;

    invoke-virtual {v4, v0, v5}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler$InvalidOffsetErrorData;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1340994
    :goto_0
    if-eqz v0, :cond_7

    iget v4, v0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler$InvalidOffsetErrorData;->startOffset:I

    if-ltz v4, :cond_7

    iget v4, v0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler$InvalidOffsetErrorData;->endOffset:I

    if-ltz v4, :cond_7

    .line 1340995
    iget v1, v0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler$InvalidOffsetErrorData;->startOffset:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v3, v0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler$InvalidOffsetErrorData;->endOffset:I

    iget v0, v0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler$InvalidOffsetErrorData;->startOffset:I

    sub-int v0, v3, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move v1, v2

    .line 1340996
    :goto_1
    if-nez v1, :cond_6

    .line 1340997
    iget-object v1, p0, LX/8On;->b:LX/8Ne;

    iget-object v2, p0, LX/8On;->d:LX/73z;

    invoke-interface {v1, v2}, LX/8Ne;->a(LX/73z;)V

    .line 1340998
    :cond_6
    return-object v0

    :catch_0
    move-object v0, v1

    goto :goto_0

    :cond_7
    move-object v0, v1

    move v1, v3

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1340999
    iget-object v0, p0, LX/8On;->b:LX/8Ne;

    invoke-interface {v0}, LX/8Ne;->a()V

    .line 1341000
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1341001
    iget-object v0, p0, LX/8On;->b:LX/8Ne;

    invoke-interface {v0}, LX/8Ne;->b()I

    move-result v0

    return v0
.end method
