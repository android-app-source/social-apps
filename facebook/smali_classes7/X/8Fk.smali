.class public LX/8Fk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Fh;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/7SH;)V
    .locals 4
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/16 v3, 0xf

    .line 1318185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318186
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/8Fk;->a:Ljava/util/List;

    .line 1318187
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 1318188
    iget-object v1, p0, LX/8Fk;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, LX/7SH;->a(Landroid/net/Uri;)Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1318189
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1318190
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 1318182
    iget-object v0, p0, LX/8Fk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    .line 1318183
    invoke-virtual {v0, p1, p2}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a(II)V

    goto :goto_0

    .line 1318184
    :cond_0
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 2

    .prologue
    .line 1318179
    iget-object v0, p0, LX/8Fk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    .line 1318180
    invoke-virtual {v0, p1}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a(LX/5Pc;)V

    goto :goto_0

    .line 1318181
    :cond_0
    return-void
.end method

.method public final a([F[F[FJ)V
    .locals 8

    .prologue
    .line 1318176
    iget-object v0, p0, LX/8Fk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    .line 1318177
    invoke-virtual/range {v0 .. v5}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a([F[F[FJ)V

    goto :goto_0

    .line 1318178
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1318172
    iget-object v0, p0, LX/8Fk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    .line 1318173
    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->b()V

    goto :goto_0

    .line 1318174
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1318175
    const/4 v0, 0x1

    return v0
.end method
