.class public final LX/8IM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z

.field public n:Z

.field public o:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:I

.field public s:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$PhotoEncodingsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:I

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1323857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;
    .locals 23

    .prologue
    .line 1323858
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 1323859
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8IM;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1323860
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8IM;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1323861
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8IM;->c:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;

    invoke-static {v1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1323862
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8IM;->d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    invoke-static {v1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1323863
    move-object/from16 v0, p0

    iget-object v6, v0, LX/8IM;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-static {v1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1323864
    move-object/from16 v0, p0

    iget-object v7, v0, LX/8IM;->g:Ljava/lang/String;

    invoke-virtual {v1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1323865
    move-object/from16 v0, p0

    iget-object v8, v0, LX/8IM;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1323866
    move-object/from16 v0, p0

    iget-object v9, v0, LX/8IM;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1323867
    move-object/from16 v0, p0

    iget-object v10, v0, LX/8IM;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1323868
    move-object/from16 v0, p0

    iget-object v11, v0, LX/8IM;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v1, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1323869
    move-object/from16 v0, p0

    iget-object v12, v0, LX/8IM;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1323870
    move-object/from16 v0, p0

    iget-object v13, v0, LX/8IM;->o:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v1, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1323871
    move-object/from16 v0, p0

    iget-object v14, v0, LX/8IM;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v1, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1323872
    move-object/from16 v0, p0

    iget-object v15, v0, LX/8IM;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v1, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1323873
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8IM;->s:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1323874
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8IM;->t:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1323875
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8IM;->u:LX/0Px;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v18

    .line 1323876
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8IM;->w:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 1323877
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8IM;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1323878
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8IM;->y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1323879
    const/16 v22, 0x1a

    move/from16 v0, v22

    invoke-virtual {v1, v0}, LX/186;->c(I)V

    .line 1323880
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v1, v0, v2}, LX/186;->b(II)V

    .line 1323881
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 1323882
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 1323883
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 1323884
    const/4 v2, 0x4

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 1323885
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget v3, v0, LX/8IM;->f:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/186;->a(III)V

    .line 1323886
    const/4 v2, 0x6

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 1323887
    const/4 v2, 0x7

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 1323888
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 1323889
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 1323890
    const/16 v2, 0xa

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 1323891
    const/16 v2, 0xb

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 1323892
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/8IM;->m:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1323893
    const/16 v2, 0xd

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/8IM;->n:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1323894
    const/16 v2, 0xe

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 1323895
    const/16 v2, 0xf

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 1323896
    const/16 v2, 0x10

    invoke-virtual {v1, v2, v15}, LX/186;->b(II)V

    .line 1323897
    const/16 v2, 0x11

    move-object/from16 v0, p0

    iget v3, v0, LX/8IM;->r:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/186;->a(III)V

    .line 1323898
    const/16 v2, 0x12

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1323899
    const/16 v2, 0x13

    move/from16 v0, v17

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1323900
    const/16 v2, 0x14

    move/from16 v0, v18

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1323901
    const/16 v2, 0x15

    move-object/from16 v0, p0

    iget v3, v0, LX/8IM;->v:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/186;->a(III)V

    .line 1323902
    const/16 v2, 0x16

    move/from16 v0, v19

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1323903
    const/16 v2, 0x17

    move/from16 v0, v20

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1323904
    const/16 v2, 0x18

    move/from16 v0, v21

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1323905
    const/16 v2, 0x19

    move-object/from16 v0, p0

    iget v3, v0, LX/8IM;->z:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/186;->a(III)V

    .line 1323906
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 1323907
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 1323908
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1323909
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1323910
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1323911
    new-instance v2, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-direct {v2, v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;-><init>(LX/15i;)V

    .line 1323912
    return-object v2
.end method
