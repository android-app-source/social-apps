.class public final LX/6vV;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

.field public final synthetic c:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Z

.field public final synthetic f:LX/6vJ;


# direct methods
.method public constructor <init>(LX/6vJ;ZLcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1157017
    iput-object p1, p0, LX/6vV;->f:LX/6vJ;

    iput-boolean p2, p0, LX/6vV;->a:Z

    iput-object p3, p0, LX/6vV;->b:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    iput-object p4, p0, LX/6vV;->c:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    iput-object p5, p0, LX/6vV;->d:Ljava/lang/String;

    iput-boolean p6, p0, LX/6vV;->e:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1157018
    iget-object v1, p0, LX/6vV;->f:LX/6vJ;

    iget-object v0, p0, LX/6vV;->f:LX/6vJ;

    iget-object v2, v0, LX/6vJ;->a:Landroid/content/Context;

    iget-boolean v0, p0, LX/6vV;->a:Z

    if-eqz v0, :cond_0

    const v0, 0x7f081e39

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, p1, v0}, LX/6vJ;->a$redex0(LX/6vJ;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1157019
    return-void

    .line 1157020
    :cond_0
    const v0, 0x7f081e38

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1157021
    iget-object v0, p0, LX/6vV;->f:LX/6vJ;

    iget-object v1, p0, LX/6vV;->b:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    iget-object v2, p0, LX/6vV;->c:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    iget-object v3, p0, LX/6vV;->d:Ljava/lang/String;

    iget-boolean v4, p0, LX/6vV;->e:Z

    iget-boolean v5, p0, LX/6vV;->a:Z

    .line 1157022
    if-nez v4, :cond_0

    if-eqz v5, :cond_1

    .line 1157023
    :cond_0
    iget-object v6, v0, LX/6vJ;->c:LX/6qh;

    new-instance v7, LX/73T;

    sget-object p0, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v7, p0}, LX/73T;-><init>(LX/73S;)V

    invoke-virtual {v6, v7}, LX/6qh;->a(LX/73T;)V

    .line 1157024
    :goto_0
    return-void

    .line 1157025
    :cond_1
    move-object v6, v1

    .line 1157026
    iget-object v6, v6, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->a:LX/6vY;

    invoke-static {v3, v2, v6}, LX/6vJ;->a(Ljava/lang/String;Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;LX/6vY;)Lcom/facebook/payments/contactinfo/model/ContactInfo;

    move-result-object v6

    .line 1157027
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 1157028
    const-string p0, "contact_info"

    invoke-virtual {v7, p0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1157029
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1157030
    const-string p0, "extra_activity_result_data"

    invoke-virtual {v6, p0, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1157031
    iget-object v7, v0, LX/6vJ;->c:LX/6qh;

    new-instance p0, LX/73T;

    sget-object p1, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {p0, p1, v6}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {v7, p0}, LX/6qh;->a(LX/73T;)V

    goto :goto_0
.end method
