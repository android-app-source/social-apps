.class public LX/8Wi;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:LX/8Wp;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1354329
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1354330
    const/4 v0, -0x1

    iput v0, p0, LX/8Wi;->b:I

    .line 1354331
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1354341
    packed-switch p2, :pswitch_data_0

    .line 1354342
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1354343
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03076f

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1354344
    new-instance v0, LX/8Wh;

    invoke-direct {v0, p0, v1}, LX/8Wh;-><init>(LX/8Wi;Landroid/view/View;)V

    goto :goto_0

    .line 1354345
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030781

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1354346
    const v0, 0x7f0d140b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1354347
    new-instance v2, LX/8We;

    invoke-direct {v2, p0}, LX/8We;-><init>(LX/8Wi;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1354348
    new-instance v0, LX/8Wf;

    invoke-direct {v0, p0, v1}, LX/8Wf;-><init>(LX/8Wi;Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 1354349
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1354350
    check-cast p1, LX/8Wh;

    iget-object v0, p0, LX/8Wi;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Vb;

    const/4 p0, 0x4

    .line 1354351
    iget-object v1, v0, LX/8Vb;->i:LX/8Vd;

    if-eqz v1, :cond_1

    .line 1354352
    iget-object v1, p1, LX/8Wh;->n:Lcom/facebook/widget/tiles/ThreadTileView;

    iget-object v2, v0, LX/8Vb;->i:LX/8Vd;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 1354353
    :goto_0
    iget-object v1, v0, LX/8Vb;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1354354
    iget-object v1, p1, LX/8Wh;->o:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, v0, LX/8Vb;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354355
    :goto_1
    iget-object v1, p1, LX/8Wh;->m:Landroid/view/View;

    new-instance v2, LX/8Wg;

    invoke-direct {v2, p1, v0, p2}, LX/8Wg;-><init>(LX/8Wh;LX/8Vb;I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1354356
    :cond_0
    return-void

    .line 1354357
    :cond_1
    iget-object v1, p1, LX/8Wh;->n:Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-virtual {v1, p0}, Lcom/facebook/widget/tiles/ThreadTileView;->setVisibility(I)V

    goto :goto_0

    .line 1354358
    :cond_2
    iget-object v1, p1, LX/8Wh;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, p0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 1354338
    iget v0, p0, LX/8Wi;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/8Wi;->b:I

    if-ne p1, v0, :cond_0

    .line 1354339
    const/4 v0, 0x2

    .line 1354340
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 1354332
    iget-object v0, p0, LX/8Wi;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1354333
    const/4 v0, 0x0

    .line 1354334
    :goto_0
    return v0

    .line 1354335
    :cond_0
    iget v0, p0, LX/8Wi;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1354336
    iget-object v0, p0, LX/8Wi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    .line 1354337
    :cond_1
    iget-object v0, p0, LX/8Wi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, LX/8Wi;->b:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method
