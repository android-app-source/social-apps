.class public interface abstract LX/7oa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7oY;
.implements LX/7oZ;


# virtual methods
.method public abstract A()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract B()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract C()LX/7oW;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract D()LX/7oc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract E()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract F()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract H()LX/7od;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract I()LX/7oe;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract J()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract K()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract L()Lcom/facebook/graphql/enums/GraphQLEventType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract M()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract N()Lcom/facebook/graphql/enums/GraphQLEventVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract O()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract Q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract R()Z
.end method

.method public abstract S()Z
.end method

.method public abstract T()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract U()Z
.end method

.method public abstract V()LX/1oP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract W()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract X()I
.end method

.method public abstract Y()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "LX/7ob;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract Z()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract b()J
.end method

.method public abstract c()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract e()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract eQ_()Z
.end method

.method public abstract eR_()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract j()J
.end method

.method public abstract k()Z
.end method

.method public abstract l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract n()Z
.end method

.method public abstract o()J
.end method

.method public abstract p()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract r()Z
.end method

.method public abstract s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract t()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract u()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract v()Z
.end method

.method public abstract w()Z
.end method

.method public abstract x()Z
.end method

.method public abstract y()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract z()J
.end method
