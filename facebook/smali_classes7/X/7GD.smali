.class public abstract LX/7GD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0Uh;

.field private final c:LX/2Hu;

.field public final d:LX/01T;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/7GA;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/7GT;",
            ">;"
        }
    .end annotation
.end field

.field public i:I


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/2Hu;LX/01T;LX/0Or;LX/7GA;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/push/mqtt/service/MqttPushServiceClientManager;",
            "LX/01T;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/7GA;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1188969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1188970
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/7GD;->h:Ljava/util/Set;

    .line 1188971
    const/high16 v0, -0x80000000

    iput v0, p0, LX/7GD;->i:I

    .line 1188972
    iput-object p1, p0, LX/7GD;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1188973
    iput-object p2, p0, LX/7GD;->b:LX/0Uh;

    .line 1188974
    iput-object p3, p0, LX/7GD;->c:LX/2Hu;

    .line 1188975
    iput-object p4, p0, LX/7GD;->d:LX/01T;

    .line 1188976
    iput-object p5, p0, LX/7GD;->e:LX/0Or;

    .line 1188977
    iput-object p6, p0, LX/7GD;->f:LX/7GA;

    .line 1188978
    iput-object p7, p0, LX/7GD;->g:LX/0Ot;

    .line 1188979
    return-void
.end method

.method public static a(LX/7GD;LX/0m9;I)V
    .locals 4

    .prologue
    .line 1188981
    if-gtz p2, :cond_0

    .line 1188982
    iget-object v0, p0, LX/7GD;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "Sync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "api_version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1188983
    :cond_0
    const-string v0, "sync_api_version"

    invoke-virtual {p1, v0, p2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1188984
    iget-object v0, p0, LX/7GD;->b:LX/0Uh;

    const/16 v1, 0x13b

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_1

    .line 1188985
    const-string v0, "max_deltas_able_to_process"

    const/16 v1, 0x4e2

    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1188986
    :cond_1
    return-void
.end method

.method public static a(LX/7GD;LX/0m9;Ljava/lang/String;)V
    .locals 4
    .param p1    # LX/0m9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1188987
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1188988
    :cond_0
    :goto_0
    return-void

    .line 1188989
    :cond_1
    :try_start_0
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1188990
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 1188991
    const-string v2, "entity_fbid"

    invoke-virtual {p1, v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1188992
    :catch_0
    iget-object v0, p0, LX/7GD;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "Sync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid entity_fbid: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method

.method private b(IJLX/76J;Ljava/lang/String;)LX/76M;
    .locals 8
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "LX/76J",
            "<",
            "LX/7GC;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/76M",
            "<",
            "LX/7GC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1189004
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 1189005
    invoke-virtual {p0, v0}, LX/7GD;->a(LX/0m9;)V

    .line 1189006
    const-string v1, "initial_titan_sequence_id"

    invoke-virtual {v0, v1, p2, p3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 1189007
    const-string v1, "delta_batch_size"

    const/16 v2, 0x7d

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1189008
    const-string v1, "device_params"

    iget-object v2, p0, LX/7GD;->f:LX/7GA;

    invoke-virtual {v2}, LX/7GA;->a()LX/0m9;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1189009
    invoke-static {p0, v0, p5}, LX/7GD;->a(LX/7GD;LX/0m9;Ljava/lang/String;)V

    .line 1189010
    invoke-static {p0, v0, p1}, LX/7GD;->a(LX/7GD;LX/0m9;I)V

    .line 1189011
    invoke-virtual {p0, v0}, LX/7GD;->b(LX/0m9;)V

    .line 1189012
    iget-object v1, p0, LX/7GD;->c:LX/2Hu;

    invoke-virtual {v1}, LX/2Hu;->a()LX/2gV;

    move-result-object v1

    .line 1189013
    const-string v2, "/messenger_sync_create_queue"

    .line 1189014
    :try_start_0
    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/String;)[B

    move-result-object v4

    invoke-static {v1, v2, v4, p4}, LX/2gV;->b(LX/2gV;Ljava/lang/String;[BLX/76J;)LX/76M;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    move-result-object v4

    .line 1189015
    :goto_0
    move-object v0, v4

    .line 1189016
    iget-boolean v2, v0, LX/76M;->a:Z

    if-eqz v2, :cond_0

    .line 1189017
    invoke-static {p0}, LX/7GD;->f(LX/7GD;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1189018
    :cond_0
    invoke-virtual {v1}, LX/2gV;->f()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2gV;->f()V

    throw v0

    .line 1189019
    :catch_0
    move-exception v4

    .line 1189020
    iget-object v5, v1, LX/2gV;->d:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, LX/76M;->a(Ljava/lang/Exception;J)LX/76M;

    move-result-object v4

    goto :goto_0
.end method

.method private static f(LX/7GD;)V
    .locals 3

    .prologue
    .line 1188993
    iget-object v0, p0, LX/7GD;->h:Ljava/util/Set;

    invoke-virtual {p0}, LX/7GD;->a()LX/7GT;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1188994
    iget v0, p0, LX/7GD;->i:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1188995
    iget-object v0, p0, LX/7GD;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    invoke-static {p0}, LX/7GD;->h(LX/7GD;)LX/0Tn;

    move-result-object v1

    iget v2, p0, LX/7GD;->i:I

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1188996
    :cond_0
    invoke-virtual {p0}, LX/7GD;->d()V

    .line 1188997
    return-void
.end method

.method public static h(LX/7GD;)LX/0Tn;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1188998
    sget-object v0, LX/7GE;->c:LX/0Tn;

    .line 1188999
    iget-object v1, p0, LX/7GD;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1189000
    if-nez v1, :cond_0

    .line 1189001
    const-string v1, "null"

    .line 1189002
    :cond_0
    move-object v1, v1

    .line 1189003
    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p0}, LX/7GD;->a()LX/7GT;

    move-result-object v1

    iget-object v1, v1, LX/7GT;->apiString:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method


# virtual methods
.method public final a(IJLX/76J;Ljava/lang/String;)LX/76M;
    .locals 2
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "LX/76J",
            "<",
            "LX/7GC;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/76M",
            "<",
            "LX/7GC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1188980
    invoke-direct/range {p0 .. p5}, LX/7GD;->b(IJLX/76J;Ljava/lang/String;)LX/76M;

    move-result-object v0

    return-object v0
.end method

.method public abstract a()LX/7GT;
.end method

.method public a(LX/0m9;)V
    .locals 2

    .prologue
    .line 1188964
    const-string v0, "queue_type"

    invoke-virtual {p0}, LX/7GD;->a()LX/7GT;

    move-result-object v1

    iget-object v1, v1, LX/7GT;->apiString:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1188965
    return-void
.end method

.method public final a(ILjava/lang/String;JLjava/lang/String;)Z
    .locals 9
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x0

    .line 1188926
    invoke-virtual {p0}, LX/7GD;->a()LX/7GT;

    .line 1188927
    const/high16 v6, -0x80000000

    const/4 v1, 0x0

    .line 1188928
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 1188929
    invoke-virtual {p0, v2}, LX/7GD;->a(LX/0m9;)V

    .line 1188930
    const-string v0, "sync_token"

    invoke-virtual {v2, v0, p2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1188931
    const-string v0, "last_seq_id"

    invoke-virtual {v2, v0, p3, p4}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 1188932
    iget-object v0, p0, LX/7GD;->h:Ljava/util/Set;

    invoke-virtual {p0}, LX/7GD;->a()LX/7GT;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1188933
    iget-object v0, p0, LX/7GD;->f:LX/7GA;

    invoke-virtual {v0}, LX/7GA;->a()LX/0m9;

    move-result-object v3

    .line 1188934
    invoke-virtual {v3}, LX/0m9;->hashCode()I

    move-result v4

    .line 1188935
    iget-object v0, p0, LX/7GD;->b:LX/0Uh;

    const/16 v5, 0x1a0

    invoke-virtual {v0, v5, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1188936
    iget-object v0, p0, LX/7GD;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/7GD;->h(LX/7GD;)LX/0Tn;

    move-result-object v5

    invoke-interface {v0, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 1188937
    if-eq v0, v6, :cond_5

    .line 1188938
    if-ne v0, v4, :cond_5

    .line 1188939
    const/4 v0, 0x1

    .line 1188940
    :goto_0
    if-nez v0, :cond_0

    .line 1188941
    const-string v0, "device_params"

    invoke-virtual {v2, v0, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1188942
    iput v4, p0, LX/7GD;->i:I

    .line 1188943
    :cond_0
    iget-object v0, p0, LX/7GD;->d:LX/01T;

    sget-object v3, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v3, :cond_1

    iget-object v0, p0, LX/7GD;->b:LX/0Uh;

    const/16 v3, 0x13c

    invoke-virtual {v0, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1188944
    :cond_1
    invoke-static {p0, v2, p5}, LX/7GD;->a(LX/7GD;LX/0m9;Ljava/lang/String;)V

    .line 1188945
    :cond_2
    invoke-static {p0, v2, p1}, LX/7GD;->a(LX/7GD;LX/0m9;I)V

    .line 1188946
    invoke-virtual {p0, v2}, LX/7GD;->c(LX/0m9;)V

    .line 1188947
    move-object v0, v2

    .line 1188948
    iget-object v1, p0, LX/7GD;->c:LX/2Hu;

    invoke-virtual {v1}, LX/2Hu;->a()LX/2gV;

    move-result-object v1

    .line 1188949
    :try_start_0
    invoke-static {v0}, LX/7G4;->a(LX/0m9;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1188950
    new-instance v2, LX/1so;

    new-instance v3, LX/1sp;

    invoke-direct {v3}, LX/1sp;-><init>()V

    invoke-direct {v2, v3}, LX/1so;-><init>(LX/1sq;)V

    .line 1188951
    new-instance v3, LX/7G4;

    invoke-direct {v3}, LX/7G4;-><init>()V

    invoke-virtual {v3, v0}, LX/7G4;->b(LX/0m9;)LX/7G4;

    move-result-object v0

    .line 1188952
    iget-object v3, p0, LX/7GD;->b:LX/0Uh;

    const/16 v4, 0x15d

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v3, v3

    .line 1188953
    invoke-virtual {v0, v3}, LX/7G4;->a(Z)LX/7GX;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/1so;->a(LX/1u2;)[B

    move-result-object v0

    .line 1188954
    array-length v2, v0

    add-int/lit8 v2, v2, 0x1

    new-array v3, v2, [B

    .line 1188955
    const/4 v2, 0x0

    const/4 v4, 0x1

    array-length v5, v0

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1188956
    invoke-virtual {p0}, LX/7GD;->a()LX/7GT;

    .line 1188957
    const-string v2, "/t_ms_gd"

    const-wide/16 v4, 0x3a98

    const-wide/16 v6, 0x0

    invoke-virtual/range {v1 .. v7}, LX/2gV;->a(Ljava/lang/String;[BJJ)Z

    move-result v0

    .line 1188958
    :goto_1
    if-eqz v0, :cond_3

    .line 1188959
    invoke-static {p0}, LX/7GD;->f(LX/7GD;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1188960
    :cond_3
    invoke-virtual {v1}, LX/2gV;->f()V

    :goto_2
    return v0

    .line 1188961
    :cond_4
    :try_start_1
    const-string v2, "SyncMqttPublisher"

    const-string v3, "Sync get_diffs called with json payload, should never happen in prod."

    invoke-static {v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1188962
    const-string v2, "/messenger_sync_get_diffs"

    const-wide/16 v4, 0x3a98

    invoke-virtual {v1, v2, v0, v4, v5}, LX/2gV;->a(Ljava/lang/String;LX/0lF;J)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_1

    .line 1188963
    :catch_0
    invoke-virtual {v1}, LX/2gV;->f()V

    move v0, v8

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2gV;->f()V

    throw v0

    :cond_5
    move v0, v1

    goto/16 :goto_0
.end method

.method public b(LX/0m9;)V
    .locals 0

    .prologue
    .line 1188925
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 1188966
    return-void
.end method

.method public c(LX/0m9;)V
    .locals 0

    .prologue
    .line 1188967
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 1188968
    return-void
.end method
