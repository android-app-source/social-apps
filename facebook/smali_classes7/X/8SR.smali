.class public LX/8SR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/1oT;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public constructor <init>(LX/0Px;LX/1oT;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/1oT;",
            ">;",
            "LX/1oT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1346653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1346654
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/8SR;->a:LX/0Px;

    .line 1346655
    invoke-virtual {p0, p2}, LX/8SR;->b(LX/1oT;)V

    .line 1346656
    return-void
.end method

.method public static a(LX/8QJ;)LX/8SR;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1346652
    new-instance v0, LX/8SR;

    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    iget-object v2, p0, LX/8QJ;->a:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    iget-object v2, p0, LX/8QJ;->b:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/8QJ;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-direct {v0, v1, v2}, LX/8SR;-><init>(LX/0Px;LX/1oT;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/1oT;)I
    .locals 2

    .prologue
    .line 1346657
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/8SR;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1346658
    iget-object v0, p0, LX/8SR;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oU;

    invoke-static {p1, v0}, LX/2cA;->a(LX/1oU;LX/1oU;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1346659
    :goto_1
    return v1

    .line 1346660
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1346661
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public final b()LX/1oT;
    .locals 2

    .prologue
    .line 1346651
    iget-object v0, p0, LX/8SR;->a:LX/0Px;

    iget v1, p0, LX/8SR;->b:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oT;

    return-object v0
.end method

.method public final b(LX/1oT;)V
    .locals 2

    .prologue
    .line 1346646
    invoke-virtual {p0, p1}, LX/8SR;->a(LX/1oT;)I

    move-result v1

    .line 1346647
    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1346648
    iput v1, p0, LX/8SR;->b:I

    .line 1346649
    return-void

    .line 1346650
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
