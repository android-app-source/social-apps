.class public LX/6k2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final muteState:Ljava/lang/Integer;

.field public final threadKey:LX/6l9;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1134381
    new-instance v0, LX/1sv;

    const-string v1, "DeltaMuteThreadReactions"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6k2;->b:LX/1sv;

    .line 1134382
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k2;->c:LX/1sw;

    .line 1134383
    new-instance v0, LX/1sw;

    const-string v1, "muteState"

    const/16 v2, 0x8

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k2;->d:LX/1sw;

    .line 1134384
    sput-boolean v4, LX/6k2;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6l9;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1134385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1134386
    iput-object p1, p0, LX/6k2;->threadKey:LX/6l9;

    .line 1134387
    iput-object p2, p0, LX/6k2;->muteState:Ljava/lang/Integer;

    .line 1134388
    return-void
.end method

.method public static a(LX/6k2;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1134340
    iget-object v0, p0, LX/6k2;->threadKey:LX/6l9;

    if-nez v0, :cond_0

    .line 1134341
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'threadKey\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6k2;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1134342
    :cond_0
    iget-object v0, p0, LX/6k2;->muteState:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1134343
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'muteState\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6k2;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1134344
    :cond_1
    iget-object v0, p0, LX/6k2;->muteState:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    sget-object v0, LX/6lB;->a:LX/1sn;

    iget-object v1, p0, LX/6k2;->muteState:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1134345
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'muteState\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6k2;->muteState:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1134346
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1134347
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1134348
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1134349
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1134350
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaMuteThreadReactions"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1134351
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134352
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134353
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134354
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134355
    const-string v4, "threadKey"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134356
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134357
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134358
    iget-object v4, p0, LX/6k2;->threadKey:LX/6l9;

    if-nez v4, :cond_4

    .line 1134359
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134360
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134361
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134362
    const-string v4, "muteState"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134363
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134364
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134365
    iget-object v0, p0, LX/6k2;->muteState:Ljava/lang/Integer;

    if-nez v0, :cond_5

    .line 1134366
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134367
    :cond_0
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134368
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134369
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1134370
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1134371
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1134372
    :cond_3
    const-string v0, ""

    goto/16 :goto_2

    .line 1134373
    :cond_4
    iget-object v4, p0, LX/6k2;->threadKey:LX/6l9;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1134374
    :cond_5
    sget-object v0, LX/6lB;->b:Ljava/util/Map;

    iget-object v4, p0, LX/6k2;->muteState:Ljava/lang/Integer;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1134375
    if-eqz v0, :cond_6

    .line 1134376
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134377
    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134378
    :cond_6
    iget-object v4, p0, LX/6k2;->muteState:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1134379
    if-eqz v0, :cond_0

    .line 1134380
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1134329
    invoke-static {p0}, LX/6k2;->a(LX/6k2;)V

    .line 1134330
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1134331
    iget-object v0, p0, LX/6k2;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1134332
    sget-object v0, LX/6k2;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134333
    iget-object v0, p0, LX/6k2;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1134334
    :cond_0
    iget-object v0, p0, LX/6k2;->muteState:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1134335
    sget-object v0, LX/6k2;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134336
    iget-object v0, p0, LX/6k2;->muteState:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1134337
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1134338
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1134339
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1134307
    if-nez p1, :cond_1

    .line 1134308
    :cond_0
    :goto_0
    return v0

    .line 1134309
    :cond_1
    instance-of v1, p1, LX/6k2;

    if-eqz v1, :cond_0

    .line 1134310
    check-cast p1, LX/6k2;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1134311
    if-nez p1, :cond_3

    .line 1134312
    :cond_2
    :goto_1
    move v0, v2

    .line 1134313
    goto :goto_0

    .line 1134314
    :cond_3
    iget-object v0, p0, LX/6k2;->threadKey:LX/6l9;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1134315
    :goto_2
    iget-object v3, p1, LX/6k2;->threadKey:LX/6l9;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1134316
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1134317
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1134318
    iget-object v0, p0, LX/6k2;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6k2;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1134319
    :cond_5
    iget-object v0, p0, LX/6k2;->muteState:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1134320
    :goto_4
    iget-object v3, p1, LX/6k2;->muteState:Ljava/lang/Integer;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1134321
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1134322
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1134323
    iget-object v0, p0, LX/6k2;->muteState:Ljava/lang/Integer;

    iget-object v3, p1, LX/6k2;->muteState:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1134324
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1134325
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1134326
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1134327
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1134328
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1134306
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1134303
    sget-boolean v0, LX/6k2;->a:Z

    .line 1134304
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6k2;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1134305
    return-object v0
.end method
