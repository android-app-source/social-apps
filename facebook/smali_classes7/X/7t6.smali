.class public final LX/7t6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 1266930
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1266931
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1266932
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1266933
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1266934
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1266935
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1266936
    :goto_1
    move v1, v2

    .line 1266937
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1266938
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1266939
    :cond_1
    const-string v10, "length"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1266940
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v7, v4

    move v4, v3

    .line 1266941
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 1266942
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1266943
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1266944
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_2

    if-eqz v9, :cond_2

    .line 1266945
    const-string v10, "entity_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1266946
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto :goto_2

    .line 1266947
    :cond_3
    const-string v10, "offset"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1266948
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v3

    goto :goto_2

    .line 1266949
    :cond_4
    const-string v10, "value"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1266950
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_2

    .line 1266951
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1266952
    :cond_6
    const/4 v9, 0x4

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1266953
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 1266954
    if-eqz v4, :cond_7

    .line 1266955
    invoke-virtual {p1, v3, v7, v2}, LX/186;->a(III)V

    .line 1266956
    :cond_7
    if-eqz v1, :cond_8

    .line 1266957
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6, v2}, LX/186;->a(III)V

    .line 1266958
    :cond_8
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1266959
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_1

    :cond_9
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1266960
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1266961
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 1266962
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const/4 p3, 0x0

    .line 1266963
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1266964
    invoke-virtual {p0, v1, p3}, LX/15i;->g(II)I

    move-result v2

    .line 1266965
    if-eqz v2, :cond_0

    .line 1266966
    const-string v2, "entity_type"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266967
    invoke-virtual {p0, v1, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266968
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, p3}, LX/15i;->a(III)I

    move-result v2

    .line 1266969
    if-eqz v2, :cond_1

    .line 1266970
    const-string v3, "length"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266971
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 1266972
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2, p3}, LX/15i;->a(III)I

    move-result v2

    .line 1266973
    if-eqz v2, :cond_2

    .line 1266974
    const-string v3, "offset"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266975
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 1266976
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1266977
    if-eqz v2, :cond_3

    .line 1266978
    const-string v3, "value"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266979
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266980
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1266981
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1266982
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1266983
    return-void
.end method
