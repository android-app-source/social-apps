.class public final LX/7t8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1267011
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1267012
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1267013
    :goto_0
    return v1

    .line 1267014
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1267015
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1267016
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1267017
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1267018
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1267019
    const-string v6, "entities"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1267020
    invoke-static {p0, p1}, LX/7t6;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1267021
    :cond_2
    const-string v6, "font_size"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1267022
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_1

    .line 1267023
    :cond_3
    const-string v6, "inline_styles"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1267024
    invoke-static {p0, p1}, LX/7t7;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1267025
    :cond_4
    const-string v6, "plain_text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1267026
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1267027
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1267028
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1267029
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1267030
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1267031
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1267032
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 1267033
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1267034
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1267035
    if-eqz v0, :cond_0

    .line 1267036
    const-string v1, "entities"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267037
    invoke-static {p0, v0, p2, p3}, LX/7t6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1267038
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1267039
    if-eqz v0, :cond_1

    .line 1267040
    const-string v0, "font_size"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267041
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1267042
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1267043
    if-eqz v0, :cond_6

    .line 1267044
    const-string v1, "inline_styles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267045
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1267046
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 1267047
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 p3, 0x2

    const/4 v5, 0x0

    .line 1267048
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1267049
    invoke-virtual {p0, v2, v5, v5}, LX/15i;->a(III)I

    move-result v3

    .line 1267050
    if-eqz v3, :cond_2

    .line 1267051
    const-string v4, "length"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267052
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 1267053
    :cond_2
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 1267054
    if-eqz v3, :cond_3

    .line 1267055
    const-string v4, "offset"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267056
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 1267057
    :cond_3
    invoke-virtual {p0, v2, p3}, LX/15i;->g(II)I

    move-result v3

    .line 1267058
    if-eqz v3, :cond_4

    .line 1267059
    const-string v3, "style"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267060
    invoke-virtual {p0, v2, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1267061
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1267062
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1267063
    :cond_5
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1267064
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1267065
    if-eqz v0, :cond_7

    .line 1267066
    const-string v1, "plain_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267067
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1267068
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1267069
    return-void
.end method
