.class public LX/6oh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6oh;


# instance fields
.field private final a:LX/6oi;


# direct methods
.method public constructor <init>(LX/6oi;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1148416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148417
    iput-object p1, p0, LX/6oh;->a:LX/6oi;

    .line 1148418
    return-void
.end method

.method public static a(LX/0QB;)LX/6oh;
    .locals 4

    .prologue
    .line 1148419
    sget-object v0, LX/6oh;->b:LX/6oh;

    if-nez v0, :cond_1

    .line 1148420
    const-class v1, LX/6oh;

    monitor-enter v1

    .line 1148421
    :try_start_0
    sget-object v0, LX/6oh;->b:LX/6oh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1148422
    if-eqz v2, :cond_0

    .line 1148423
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1148424
    new-instance p0, LX/6oh;

    invoke-static {v0}, LX/6oi;->a(LX/0QB;)LX/6oi;

    move-result-object v3

    check-cast v3, LX/6oi;

    invoke-direct {p0, v3}, LX/6oh;-><init>(LX/6oi;)V

    .line 1148425
    move-object v0, p0

    .line 1148426
    sput-object v0, LX/6oh;->b:LX/6oh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1148427
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1148428
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1148429
    :cond_1
    sget-object v0, LX/6oh;->b:LX/6oh;

    return-object v0

    .line 1148430
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1148431
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    .line 1148432
    iget-object v0, p0, LX/6oh;->a:LX/6oi;

    invoke-virtual {v0}, LX/0Tr;->f()V

    .line 1148433
    return-void
.end method
