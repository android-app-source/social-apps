.class public LX/72e;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/72g;

.field public b:Lcom/facebook/common/locale/Country;

.field public c:LX/6xe;

.field public d:Lcom/facebook/payments/shipping/model/MailingAddress;

.field public e:LX/72f;

.field public f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public g:I

.field public h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

.field public i:LX/6xg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1164783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/shipping/model/ShippingCommonParams;)LX/72e;
    .locals 2

    .prologue
    .line 1164785
    iget-object v0, p1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->a:LX/72g;

    .line 1164786
    iput-object v0, p0, LX/72e;->a:LX/72g;

    .line 1164787
    move-object v0, p0

    .line 1164788
    iget-object v1, p1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->b:Lcom/facebook/common/locale/Country;

    .line 1164789
    iput-object v1, v0, LX/72e;->b:Lcom/facebook/common/locale/Country;

    .line 1164790
    move-object v0, v0

    .line 1164791
    iget-object v1, p1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->c:LX/6xe;

    .line 1164792
    iput-object v1, v0, LX/72e;->c:LX/6xe;

    .line 1164793
    move-object v0, v0

    .line 1164794
    iget-object v1, p1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1164795
    iput-object v1, v0, LX/72e;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1164796
    move-object v0, v0

    .line 1164797
    iget v1, p1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->g:I

    .line 1164798
    iput v1, v0, LX/72e;->g:I

    .line 1164799
    move-object v0, v0

    .line 1164800
    iget-object v1, p1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1164801
    iput-object v1, v0, LX/72e;->f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1164802
    move-object v0, v0

    .line 1164803
    iget-object v1, p1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1164804
    iput-object v1, v0, LX/72e;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1164805
    move-object v0, v0

    .line 1164806
    iget-object v1, p1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->i:LX/6xg;

    .line 1164807
    iput-object v1, v0, LX/72e;->i:LX/6xg;

    .line 1164808
    move-object v0, v0

    .line 1164809
    iget-object v1, p1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->e:LX/72f;

    .line 1164810
    iput-object v1, v0, LX/72e;->e:LX/72f;

    .line 1164811
    move-object v0, v0

    .line 1164812
    return-object v0
.end method

.method public final j()Lcom/facebook/payments/shipping/model/ShippingCommonParams;
    .locals 1

    .prologue
    .line 1164784
    new-instance v0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    invoke-direct {v0, p0}, Lcom/facebook/payments/shipping/model/ShippingCommonParams;-><init>(LX/72e;)V

    return-object v0
.end method
