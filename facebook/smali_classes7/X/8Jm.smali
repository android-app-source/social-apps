.class public LX/8Jm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TAGVIEW:",
        "Landroid/view/View;",
        ":",
        "LX/8Jj;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Landroid/graphics/RectF;


# instance fields
.field private b:Landroid/view/View;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8Jk;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8Jf;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TTAGVIEW;>;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/8Jg;

.field private final h:[F

.field private final i:Landroid/graphics/Matrix;

.field private j:LX/8Jh;

.field private k:Landroid/graphics/Rect;

.field private l:Z

.field private m:F


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1329264
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, LX/8Jm;->a:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;FLX/8Jg;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # F
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1329251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1329252
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    iput-object v0, p0, LX/8Jm;->c:Ljava/util/List;

    .line 1329253
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/8Jm;->d:Ljava/util/List;

    .line 1329254
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    iput-object v0, p0, LX/8Jm;->e:Ljava/util/List;

    .line 1329255
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, LX/8Jm;->h:[F

    .line 1329256
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/8Jm;->i:Landroid/graphics/Matrix;

    .line 1329257
    new-instance v0, LX/8Jh;

    invoke-direct {v0}, LX/8Jh;-><init>()V

    iput-object v0, p0, LX/8Jm;->j:LX/8Jh;

    .line 1329258
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/8Jm;->k:Landroid/graphics/Rect;

    .line 1329259
    iput-object p1, p0, LX/8Jm;->b:Landroid/view/View;

    .line 1329260
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/8Jm;->f:Ljava/util/List;

    .line 1329261
    iput-object p3, p0, LX/8Jm;->g:LX/8Jg;

    .line 1329262
    iput p2, p0, LX/8Jm;->m:F

    .line 1329263
    return-void
.end method

.method private static a(LX/8Jm;ILjava/util/List;[Landroid/graphics/Rect;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "LX/8Jf;",
            ">;[",
            "Landroid/graphics/Rect;",
            ")I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1329227
    add-int/lit8 v3, p1, 0x1

    move v4, v2

    .line 1329228
    :goto_0
    if-gt v4, p1, :cond_5

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_5

    .line 1329229
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Jf;

    invoke-virtual {v0}, LX/8Jf;->c()Landroid/graphics/Rect;

    move-result-object v5

    .line 1329230
    iget v0, v5, Landroid/graphics/Rect;->top:I

    if-ltz v0, :cond_1

    iget v0, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, LX/8Jm;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-gt v0, v1, :cond_1

    iget v0, v5, Landroid/graphics/Rect;->left:I

    if-ltz v0, :cond_0

    iget v0, v5, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, LX/8Jm;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    if-le v0, v1, :cond_2

    :cond_0
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Jf;

    .line 1329231
    iget-object v1, v0, LX/8Jf;->b:LX/8JG;

    move-object v0, v1

    .line 1329232
    sget-object v1, LX/8JG;->DOWN:LX/8JG;

    if-eq v0, v1, :cond_2

    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Jf;

    .line 1329233
    iget-object v1, v0, LX/8Jf;->b:LX/8JG;

    move-object v0, v1

    .line 1329234
    sget-object v1, LX/8JG;->UP:LX/8JG;

    if-eq v0, v1, :cond_2

    .line 1329235
    :cond_1
    add-int/lit8 v0, v3, -0x1

    .line 1329236
    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v3, v0

    goto :goto_0

    .line 1329237
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v1, v0

    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 1329238
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Jf;

    invoke-virtual {v0}, LX/8Jf;->c()Landroid/graphics/Rect;

    move-result-object v0

    .line 1329239
    invoke-static {p0, v5, v0}, LX/8Jm;->a(LX/8Jm;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1329240
    add-int/lit8 v1, v3, -0x1

    .line 1329241
    const/4 v0, 0x1

    .line 1329242
    :goto_3
    if-nez v0, :cond_6

    move v0, v2

    .line 1329243
    :goto_4
    array-length v3, p3

    if-ge v0, v3, :cond_6

    .line 1329244
    aget-object v3, p3, v0

    .line 1329245
    invoke-static {p0, v5, v3}, LX/8Jm;->a(LX/8Jm;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1329246
    add-int/lit8 v0, v1, -0x1

    .line 1329247
    goto :goto_1

    .line 1329248
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1329249
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1329250
    :cond_5
    return v3

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    move v0, v2

    move v1, v3

    goto :goto_3
.end method

.method private a(LX/8Jl;Landroid/graphics/PointF;IIFF)Landroid/graphics/PointF;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1329213
    iget-object v0, p0, LX/8Jm;->h:[F

    iget v1, p2, Landroid/graphics/PointF;->x:F

    aput v1, v0, v4

    .line 1329214
    iget-object v0, p0, LX/8Jm;->h:[F

    iget v1, p2, Landroid/graphics/PointF;->y:F

    aput v1, v0, v5

    .line 1329215
    iget-object v0, p1, LX/8Jl;->d:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    .line 1329216
    iget-object v0, p1, LX/8Jl;->d:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/8Jm;->h:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1329217
    :goto_0
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, LX/8Jm;->h:[F

    aget v1, v1, v4

    iget-object v2, p0, LX/8Jm;->h:[F

    aget v2, v2, v5

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0

    .line 1329218
    :cond_0
    iget-object v0, p1, LX/8Jl;->a:Landroid/graphics/Matrix;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1329219
    iget-object v0, p1, LX/8Jl;->b:Landroid/graphics/Matrix;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1329220
    iget-object v0, p1, LX/8Jl;->a:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/8Jm;->h:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1329221
    iget-object v0, p1, LX/8Jl;->b:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/8Jm;->h:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1329222
    iget-object v0, p0, LX/8Jm;->h:[F

    iget-object v1, p0, LX/8Jm;->h:[F

    aget v1, v1, v4

    int-to-float v2, p3

    sub-float/2addr v2, p6

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v1, p6}, Ljava/lang/Math;->max(FF)F

    move-result v1

    aput v1, v0, v4

    .line 1329223
    iget-object v0, p0, LX/8Jm;->h:[F

    iget-object v1, p0, LX/8Jm;->h:[F

    aget v1, v1, v5

    int-to-float v2, p4

    iget v3, p0, LX/8Jm;->m:F

    sub-float/2addr v2, v3

    sub-float/2addr v2, p5

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    aput v1, v0, v5

    .line 1329224
    iget-object v0, p1, LX/8Jl;->b:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/8Jm;->i:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1329225
    iget-object v0, p0, LX/8Jm;->i:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/8Jm;->h:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1329226
    iget-object v0, p1, LX/8Jl;->c:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/8Jm;->h:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    goto :goto_0
.end method

.method private static a(Landroid/graphics/RectF;LX/8Jl;)Landroid/graphics/Rect;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1329205
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 1329206
    iget-object v1, p1, LX/8Jl;->d:Landroid/graphics/Matrix;

    if-eqz v1, :cond_0

    .line 1329207
    iget-object v1, p1, LX/8Jl;->d:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0, p0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1329208
    :goto_0
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v1

    .line 1329209
    :cond_0
    iget-object v1, p1, LX/8Jl;->a:Landroid/graphics/Matrix;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1329210
    iget-object v1, p1, LX/8Jl;->c:Landroid/graphics/Matrix;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1329211
    iget-object v1, p1, LX/8Jl;->a:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0, p0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1329212
    iget-object v1, p1, LX/8Jl;->c:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    goto :goto_0
.end method

.method private static a(LX/8Jm;LX/8Jl;LX/8Jf;)V
    .locals 7

    .prologue
    .line 1329196
    iget-object v0, p2, LX/8Jf;->c:Landroid/graphics/PointF;

    move-object v2, v0

    .line 1329197
    iget-object v0, p0, LX/8Jm;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    iget-object v0, p0, LX/8Jm;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 1329198
    iget-object v0, p2, LX/8Jf;->f:LX/8Jh;

    iget-object v0, v0, LX/8Jh;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    move v5, v0

    .line 1329199
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LX/8Jm;->a(LX/8Jl;Landroid/graphics/PointF;IIFF)Landroid/graphics/PointF;

    move-result-object v0

    .line 1329200
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 1329201
    iget-object v2, p2, LX/8Jf;->d:Landroid/view/View;

    check-cast v2, LX/8Jj;

    iget-object v3, p2, LX/8Jf;->b:LX/8JG;

    iget-object v4, p2, LX/8Jf;->f:LX/8Jh;

    invoke-interface {v2, v3, v4}, LX/8Jj;->a(LX/8JG;LX/8Jh;)V

    .line 1329202
    iget-object v2, p2, LX/8Jf;->f:LX/8Jh;

    iget-object v2, v2, LX/8Jh;->a:Landroid/graphics/Rect;

    float-to-int v3, v1

    float-to-int v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 1329203
    iget-object v2, p2, LX/8Jf;->f:LX/8Jh;

    iget-object v2, v2, LX/8Jh;->b:Landroid/graphics/Rect;

    float-to-int v3, v1

    float-to-int v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 1329204
    return-void
.end method

.method public static a(LX/8Jm;LX/8Jl;Landroid/view/View;Landroid/graphics/PointF;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8Jl;",
            "TTAGVIEW;",
            "Landroid/graphics/PointF;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1329043
    new-instance v7, Landroid/graphics/RectF;

    sget-object v0, LX/8Jm;->a:Landroid/graphics/RectF;

    invoke-direct {v7, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1329044
    iget-object v0, p1, LX/8Jl;->d:Landroid/graphics/Matrix;

    if-eqz v0, :cond_2

    .line 1329045
    iget-object v0, p1, LX/8Jl;->d:Landroid/graphics/Matrix;

    invoke-virtual {v0, v7}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    :goto_0
    move-object v0, p2

    .line 1329046
    check-cast v0, LX/8Jj;

    move-object v1, p2

    check-cast v1, LX/8Jj;

    invoke-interface {v1}, LX/8Jj;->getArrowDirection()LX/8JG;

    move-result-object v1

    iget-object v2, p0, LX/8Jm;->j:LX/8Jh;

    invoke-interface {v0, v1, v2}, LX/8Jj;->a(LX/8JG;LX/8Jh;)V

    .line 1329047
    iget-object v0, p0, LX/8Jm;->j:LX/8Jh;

    iget-object v0, v0, LX/8Jh;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v8

    .line 1329048
    iget-object v0, p0, LX/8Jm;->j:LX/8Jh;

    iget-object v0, v0, LX/8Jh;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 1329049
    iget-object v1, p0, LX/8Jm;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v3

    iget-object v1, p0, LX/8Jm;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v5, v0

    move-object v0, p2

    check-cast v0, LX/8Jj;

    invoke-interface {v0}, LX/8Jj;->getArrowLength()I

    move-result v0

    int-to-float v6, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    invoke-direct/range {v0 .. v6}, LX/8Jm;->a(LX/8Jl;Landroid/graphics/PointF;IIFF)Landroid/graphics/PointF;

    move-result-object v1

    .line 1329050
    new-instance v2, Landroid/graphics/PointF;

    iget v0, v1, Landroid/graphics/PointF;->x:F

    iget v3, v1, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v0, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1329051
    iget-object v0, p0, LX/8Jm;->j:LX/8Jh;

    iget-object v0, v0, LX/8Jh;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v3, p0, LX/8Jm;->j:LX/8Jh;

    iget-object v3, v3, LX/8Jh;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v2, v0, v3}, Landroid/graphics/PointF;->offset(FF)V

    move-object v0, p2

    .line 1329052
    check-cast v0, LX/8Jj;

    invoke-interface {v0}, LX/8Jj;->getArrowDirection()LX/8JG;

    move-result-object v0

    sget-object v3, LX/8JG;->UP:LX/8JG;

    if-eq v0, v3, :cond_0

    move-object v0, p2

    check-cast v0, LX/8Jj;

    invoke-interface {v0}, LX/8Jj;->getArrowDirection()LX/8JG;

    move-result-object v0

    sget-object v3, LX/8JG;->DOWN:LX/8JG;

    if-ne v0, v3, :cond_1

    :cond_0
    if-nez p4, :cond_1

    .line 1329053
    iget-object v0, p0, LX/8Jm;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    int-to-float v3, v8

    const/high16 p0, 0x40000000    # 2.0f

    .line 1329054
    iget v4, v1, Landroid/graphics/PointF;->x:F

    iget v5, v7, Landroid/graphics/RectF;->left:F

    const/4 v6, 0x0

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    sub-float/2addr v4, v5

    .line 1329055
    iget v5, v7, Landroid/graphics/RectF;->right:F

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iget v6, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v6

    .line 1329056
    div-float v6, v3, p0

    cmpg-float v6, v4, v6

    if-gez v6, :cond_3

    .line 1329057
    div-float/2addr v4, v3

    .line 1329058
    :goto_1
    move v0, v4

    .line 1329059
    int-to-float v1, v8

    mul-float/2addr v0, v1

    div-int/lit8 v1, v8, 0x2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v1, v0

    move-object v0, p2

    .line 1329060
    check-cast v0, LX/8Jj;

    invoke-interface {v0, v1}, LX/8Jj;->a(I)V

    .line 1329061
    neg-int v0, v1

    int-to-float v0, v0

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/graphics/PointF;->offset(FF)V

    .line 1329062
    :cond_1
    iget v0, v2, Landroid/graphics/PointF;->x:F

    invoke-virtual {p2, v0}, Landroid/view/View;->setX(F)V

    .line 1329063
    iget v0, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {p2, v0}, Landroid/view/View;->setY(F)V

    .line 1329064
    return-void

    .line 1329065
    :cond_2
    iget-object v0, p1, LX/8Jl;->a:Landroid/graphics/Matrix;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1329066
    iget-object v0, p1, LX/8Jl;->c:Landroid/graphics/Matrix;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1329067
    iget-object v0, p1, LX/8Jl;->a:Landroid/graphics/Matrix;

    invoke-virtual {v0, v7}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1329068
    iget-object v0, p1, LX/8Jl;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, v7}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    goto/16 :goto_0

    .line 1329069
    :cond_3
    div-float v4, v3, p0

    cmpg-float v4, v5, v4

    if-gez v4, :cond_4

    .line 1329070
    sub-float v4, v3, v5

    div-float/2addr v4, v3

    goto :goto_1

    .line 1329071
    :cond_4
    const/high16 v4, 0x3f000000    # 0.5f

    goto :goto_1
.end method

.method public static a(LX/8Jm;LX/8Jl;Z)V
    .locals 0

    .prologue
    .line 1329193
    if-nez p1, :cond_0

    .line 1329194
    :goto_0
    return-void

    .line 1329195
    :cond_0
    invoke-direct {p0, p1, p2}, LX/8Jm;->b(LX/8Jl;Z)V

    goto :goto_0
.end method

.method private static a(LX/8Jm;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const v5, 0x3dcccccd    # 0.1f

    .line 1329186
    iget-object v1, p0, LX/8Jm;->k:Landroid/graphics/Rect;

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->setIntersect(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1329187
    :cond_0
    :goto_0
    return v0

    .line 1329188
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    mul-int/2addr v1, v2

    int-to-float v1, v1

    .line 1329189
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-float v2, v2

    .line 1329190
    iget-object v3, p0, LX/8Jm;->k:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, LX/8Jm;->k:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    mul-int/2addr v3, v4

    int-to-float v3, v3

    .line 1329191
    div-float v1, v3, v1

    cmpl-float v1, v1, v5

    if-gtz v1, :cond_2

    div-float v1, v3, v2

    cmpl-float v1, v1, v5

    if-lez v1, :cond_0

    .line 1329192
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(LX/8Jl;Z)V
    .locals 5

    .prologue
    .line 1329173
    iget-boolean v0, p0, LX/8Jm;->l:Z

    if-nez v0, :cond_1

    .line 1329174
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1329175
    iget-object v0, p0, LX/8Jm;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 1329176
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 1329177
    iget-object v0, p0, LX/8Jm;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1329178
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    if-nez v1, :cond_2

    .line 1329179
    :cond_0
    :goto_1
    return-void

    .line 1329180
    :cond_1
    invoke-direct {p0, p1, p2}, LX/8Jm;->d(LX/8Jl;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1329181
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8Jm;->l:Z

    goto :goto_1

    .line 1329182
    :cond_2
    iget-object v1, p0, LX/8Jm;->d:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Jf;

    .line 1329183
    iget-object v4, v1, LX/8Jf;->c:Landroid/graphics/PointF;

    move-object v1, v4

    .line 1329184
    invoke-static {p0, p1, v0, v1, p2}, LX/8Jm;->a(LX/8Jm;LX/8Jl;Landroid/view/View;Landroid/graphics/PointF;Z)V

    .line 1329185
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method private d(LX/8Jl;Z)Z
    .locals 18

    .prologue
    .line 1329108
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Jm;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    .line 1329109
    if-gtz v9, :cond_0

    .line 1329110
    const/4 v3, 0x1

    .line 1329111
    :goto_0
    return v3

    .line 1329112
    :cond_0
    invoke-static {}, LX/8JG;->values()[LX/8JG;

    move-result-object v3

    array-length v10, v3

    .line 1329113
    invoke-static {}, LX/8JG;->values()[LX/8JG;

    move-result-object v11

    .line 1329114
    const/4 v6, 0x0

    .line 1329115
    const/4 v5, 0x0

    .line 1329116
    new-array v12, v9, [LX/8JG;

    .line 1329117
    new-array v13, v9, [LX/8JG;

    .line 1329118
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Jm;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1329119
    new-array v14, v9, [Z

    .line 1329120
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Jm;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1009

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v8, v3

    .line 1329121
    const/4 v3, 0x0

    move v7, v3

    :goto_1
    if-ge v7, v9, :cond_3

    .line 1329122
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Jm;->e:Ljava/util/List;

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 1329123
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v4

    if-nez v4, :cond_2

    .line 1329124
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 1329125
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, LX/8Jm;->d:Ljava/util/List;

    check-cast v3, LX/8Jj;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8Jm;->c:Ljava/util/List;

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/8Jk;

    move-object/from16 v0, v16

    invoke-static {v3, v0, v4}, LX/8Jg;->a(LX/8Jj;Ljava/lang/Integer;LX/8Jk;)LX/8Jf;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1329126
    const/4 v3, 0x0

    aput-boolean v3, v14, v7

    .line 1329127
    sget-object v3, LX/8Jf;->a:LX/8JG;

    aput-object v3, v12, v7

    .line 1329128
    sget-object v3, LX/8Jf;->a:LX/8JG;

    aput-object v3, v13, v7

    .line 1329129
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    goto :goto_1

    .line 1329130
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Jm;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v15, v3, [Landroid/graphics/Rect;

    .line 1329131
    const/4 v3, 0x0

    move v4, v3

    :goto_2
    array-length v3, v15

    if-ge v4, v3, :cond_4

    .line 1329132
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Jm;->f:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    move-object/from16 v0, p1

    invoke-static {v3, v0}, LX/8Jm;->a(Landroid/graphics/RectF;LX/8Jl;)Landroid/graphics/Rect;

    move-result-object v3

    aput-object v3, v15, v4

    .line 1329133
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 1329134
    :cond_4
    const/4 v3, 0x0

    move v7, v3

    move v4, v5

    move v5, v6

    .line 1329135
    :goto_3
    if-ge v7, v9, :cond_c

    .line 1329136
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Jm;->e:Ljava/util/List;

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 1329137
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Jm;->d:Ljava/util/List;

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8Jf;

    .line 1329138
    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 1329139
    aget-object v4, v13, v7

    invoke-virtual {v4}, LX/8JG;->ordinal()I

    move-result v4

    move v6, v4

    move v4, v5

    :goto_4
    if-ge v6, v10, :cond_7

    .line 1329140
    aget-object v5, v11, v6

    aput-object v5, v13, v7

    .line 1329141
    invoke-virtual {v3}, LX/8Jf;->a()LX/8JG;

    move-result-object v16

    .line 1329142
    aget-object v5, v13, v7

    invoke-virtual {v3, v5}, LX/8Jf;->a(LX/8JG;)V

    .line 1329143
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v3}, LX/8Jm;->a(LX/8Jm;LX/8Jl;LX/8Jf;)V

    .line 1329144
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8Jm;->d:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-static {v0, v7, v5, v15}, LX/8Jm;->a(LX/8Jm;ILjava/util/List;[Landroid/graphics/Rect;)I

    move-result v5

    .line 1329145
    if-le v5, v4, :cond_6

    .line 1329146
    const/4 v4, 0x0

    :goto_5
    if-gt v4, v7, :cond_5

    .line 1329147
    aget-object v17, v13, v4

    aput-object v17, v12, v4

    .line 1329148
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_5
    move v4, v5

    .line 1329149
    :cond_6
    add-int/lit8 v17, v7, 0x1

    move/from16 v0, v17

    if-eq v5, v0, :cond_7

    .line 1329150
    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, LX/8Jf;->a(LX/8JG;)V

    .line 1329151
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_4

    :cond_7
    move v5, v4

    .line 1329152
    if-ge v6, v10, :cond_8

    .line 1329153
    add-int/lit8 v3, v7, 0x1

    .line 1329154
    add-int/lit8 v4, v9, -0x1

    if-ge v3, v4, :cond_b

    .line 1329155
    sget-object v4, LX/8Jf;->a:LX/8JG;

    aput-object v4, v13, v3

    move v7, v3

    move v4, v8

    goto :goto_3

    .line 1329156
    :cond_8
    if-lez v7, :cond_9

    add-int/lit8 v3, v7, -0x1

    aget-boolean v3, v14, v3

    if-nez v3, :cond_9

    add-int/lit8 v3, v7, -0x1

    aget-object v3, v13, v3

    invoke-virtual {v3}, LX/8JG;->ordinal()I

    move-result v3

    add-int/lit8 v4, v10, -0x1

    if-ge v3, v4, :cond_9

    .line 1329157
    add-int/lit8 v3, v7, -0x1

    .line 1329158
    aget-object v4, v13, v3

    invoke-virtual {v4}, LX/8JG;->ordinal()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    aget-object v4, v11, v4

    aput-object v4, v13, v3

    move v7, v3

    move v4, v8

    goto/16 :goto_3

    .line 1329159
    :cond_9
    const/4 v3, 0x0

    move v4, v3

    :goto_6
    if-gt v4, v8, :cond_a

    .line 1329160
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Jm;->d:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8Jf;

    aget-object v6, v12, v4

    invoke-virtual {v3, v6}, LX/8Jf;->a(LX/8JG;)V

    .line 1329161
    aget-object v3, v12, v4

    aput-object v3, v13, v4

    .line 1329162
    const/4 v3, 0x1

    aput-boolean v3, v14, v4

    .line 1329163
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Jm;->d:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8Jf;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v3}, LX/8Jm;->a(LX/8Jm;LX/8Jl;LX/8Jf;)V

    .line 1329164
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_6

    .line 1329165
    :cond_a
    add-int/lit8 v3, v8, 0x1

    :cond_b
    move v7, v3

    move v4, v8

    .line 1329166
    goto/16 :goto_3

    .line 1329167
    :cond_c
    const/4 v3, 0x0

    move v6, v3

    :goto_7
    if-ge v6, v9, :cond_d

    .line 1329168
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Jm;->e:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    move-object v4, v3

    .line 1329169
    check-cast v4, LX/8Jj;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/8Jm;->d:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/8Jf;

    invoke-virtual {v5}, LX/8Jf;->a()LX/8JG;

    move-result-object v5

    invoke-interface {v4, v5}, LX/8Jj;->setArrowDirection(LX/8JG;)V

    .line 1329170
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8Jm;->d:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/8Jf;

    invoke-virtual {v4}, LX/8Jf;->b()Landroid/graphics/PointF;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-static {v0, v1, v3, v4, v2}, LX/8Jm;->a(LX/8Jm;LX/8Jl;Landroid/view/View;Landroid/graphics/PointF;Z)V

    .line 1329171
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_7

    .line 1329172
    :cond_d
    const/4 v3, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 1329095
    iget-object v0, p0, LX/8Jm;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8Jm;->b:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1329096
    :cond_0
    :goto_0
    return-void

    .line 1329097
    :cond_1
    iget-object v0, p0, LX/8Jm;->b:Landroid/view/View;

    const/4 v6, 0x0

    .line 1329098
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1329099
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    if-gtz v1, :cond_3

    .line 1329100
    :cond_2
    const/4 v1, 0x0

    .line 1329101
    :goto_1
    move-object v0, v1

    .line 1329102
    if-eqz v0, :cond_0

    .line 1329103
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/8Jm;->b(LX/8Jl;Z)V

    goto :goto_0

    .line 1329104
    :cond_3
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 1329105
    sget-object v1, LX/8Jm;->a:Landroid/graphics/RectF;

    new-instance v3, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object v4, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v2, v1, v3, v4}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1329106
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 1329107
    new-instance v1, LX/8Jl;

    invoke-direct {v1, v2, v3, v3}, LX/8Jl;-><init>(Landroid/graphics/Matrix;Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    goto :goto_1
.end method

.method public final a(LX/7UZ;Z)V
    .locals 8

    .prologue
    .line 1329083
    const/4 v7, 0x0

    .line 1329084
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1329085
    invoke-interface {p1}, LX/7UZ;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1329086
    const/4 v0, 0x0

    .line 1329087
    :goto_0
    move-object v0, v0

    .line 1329088
    invoke-static {p0, v0, p2}, LX/8Jm;->a(LX/8Jm;LX/8Jl;Z)V

    .line 1329089
    return-void

    .line 1329090
    :cond_0
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 1329091
    invoke-interface {p1}, LX/7UZ;->getBaseMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    .line 1329092
    invoke-interface {p1}, LX/7UZ;->getPhotoDisplayMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    .line 1329093
    sget-object v0, LX/8Jm;->a:Landroid/graphics/RectF;

    new-instance v4, Landroid/graphics/RectF;

    invoke-interface {p1}, LX/7UZ;->getPhotoWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-interface {p1}, LX/7UZ;->getPhotoHeight()I

    move-result v6

    int-to-float v6, v6

    invoke-direct {v4, v7, v7, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object v5, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v0, v4, v5}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1329094
    new-instance v0, LX/8Jl;

    invoke-direct {v0, v1, v2, v3}, LX/8Jl;-><init>(Landroid/graphics/Matrix;Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/graphics/RectF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1329078
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1329079
    iget-object v0, p0, LX/8Jm;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1329080
    iget-object v0, p0, LX/8Jm;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1329081
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8Jm;->l:Z

    .line 1329082
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TTAGVIEW;",
            "LX/8Jk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1329072
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/8Jm;->e:Ljava/util/List;

    .line 1329073
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/8Jm;->c:Ljava/util/List;

    .line 1329074
    iget-object v0, p0, LX/8Jm;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1329075
    iget-object v2, p0, LX/8Jm;->c:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1329076
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8Jm;->l:Z

    .line 1329077
    return-void
.end method
