.class public LX/6wZ;
.super LX/6sV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sV",
        "<",
        "Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;",
        "Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1157789
    const-class v0, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;

    invoke-direct {p0, p1, v0}, LX/6sV;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 1157790
    return-void
.end method

.method public static b(LX/0QB;)LX/6wZ;
    .locals 2

    .prologue
    .line 1157791
    new-instance v1, LX/6wZ;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/6wZ;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 1157792
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1157793
    check-cast p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;

    .line 1157794
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1157795
    iget-object v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    if-eqz v0, :cond_0

    .line 1157796
    iget-object v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    check-cast v0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;

    .line 1157797
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "raw_input"

    iget-object v0, v0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1157798
    :cond_0
    iget-object v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    invoke-interface {v0}, Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;->a()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-boolean v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->c:Z

    if-eqz v0, :cond_3

    .line 1157799
    :cond_2
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "default"

    const-string v3, "1"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1157800
    :cond_3
    iget-boolean v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->d:Z

    if-eqz v0, :cond_4

    const-string v0, "DELETE"

    .line 1157801
    :goto_0
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "add_phone_number_contact_info"

    .line 1157802
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1157803
    move-object v2, v2

    .line 1157804
    iput-object v0, v2, LX/14O;->c:Ljava/lang/String;

    .line 1157805
    move-object v0, v2

    .line 1157806
    const-string v2, "%d"

    iget-object v3, p1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1157807
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 1157808
    move-object v0, v0

    .line 1157809
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1157810
    move-object v0, v0

    .line 1157811
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 1157812
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1157813
    move-object v0, v0

    .line 1157814
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1157815
    :cond_4
    const-string v0, "POST"

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157816
    const-string v0, "edit_phone_number_contact_info"

    return-object v0
.end method
