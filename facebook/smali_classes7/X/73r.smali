.class public final LX/73r;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:I

.field public final synthetic c:LX/73s;


# direct methods
.method public constructor <init>(LX/73s;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1166346
    iput-object p1, p0, LX/73r;->c:LX/73s;

    iput-object p2, p0, LX/73r;->a:Ljava/lang/String;

    iput p3, p0, LX/73r;->b:I

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1166339
    iget-object v0, p0, LX/73r;->c:LX/73s;

    iget-object v1, p0, LX/73r;->a:Ljava/lang/String;

    iget-object v2, p0, LX/73r;->c:LX/73s;

    iget-object v2, v2, LX/73s;->c:Landroid/content/Context;

    .line 1166340
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 1166341
    new-instance p0, Landroid/content/Intent;

    const-string p1, "android.intent.action.VIEW"

    invoke-direct {p0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    .line 1166342
    iget-object p0, v0, LX/73s;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v3, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1166343
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1166344
    iget v0, p0, LX/73r;->b:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1166345
    return-void
.end method
