.class public LX/7z8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public final a:LX/7yw;

.field public b:LX/7yp;

.field public c:I

.field public final d:LX/7yy;

.field public final e:LX/7zR;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field public h:LX/7z3;

.field public i:Ljava/net/URI;


# direct methods
.method public constructor <init>(LX/7yy;LX/7yw;LX/7yp;LX/7zR;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1280751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280752
    iput-object p1, p0, LX/7z8;->d:LX/7yy;

    .line 1280753
    iput-object p2, p0, LX/7z8;->a:LX/7yw;

    .line 1280754
    iput-object p3, p0, LX/7z8;->b:LX/7yp;

    .line 1280755
    iput-object p4, p0, LX/7z8;->e:LX/7zR;

    .line 1280756
    iget-object v0, p0, LX/7z8;->d:LX/7yy;

    .line 1280757
    iget-object v1, v0, LX/7yy;->f:Ljava/lang/String;

    move-object v0, v1

    .line 1280758
    iput-object v0, p0, LX/7z8;->f:Ljava/lang/String;

    .line 1280759
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://rupload.facebook.com/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/7z8;->a:LX/7yw;

    invoke-virtual {v1}, LX/7yw;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/7z8;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7z8;->g:Ljava/lang/String;

    .line 1280760
    :try_start_0
    new-instance v0, Ljava/net/URI;

    iget-object v1, p0, LX/7z8;->g:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/7z8;->i:Ljava/net/URI;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1280761
    :goto_0
    iput-object v2, p0, LX/7z8;->h:LX/7z3;

    .line 1280762
    const/4 v0, 0x0

    iput v0, p0, LX/7z8;->c:I

    .line 1280763
    return-void

    .line 1280764
    :catch_0
    iput-object v2, p0, LX/7z8;->i:Ljava/net/URI;

    goto :goto_0
.end method

.method public static a(LX/7z8;Ljava/util/Map;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1280765
    iget-object v0, p0, LX/7z8;->a:LX/7yw;

    .line 1280766
    iget-object v1, v0, LX/7yw;->f:LX/7yz;

    move-object v0, v1

    .line 1280767
    iget-object v1, v0, LX/7yz;->a:LX/7ys;

    .line 1280768
    iget-boolean v2, v1, LX/7ys;->b:Z

    move v1, v2

    .line 1280769
    if-eqz v1, :cond_3

    iget v1, v0, LX/7yz;->b:I

    iget-object v2, v0, LX/7yz;->a:LX/7ys;

    iget v2, v2, LX/7ys;->a:I

    if-ge v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1280770
    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_1

    .line 1280771
    :cond_0
    :goto_1
    return-void

    .line 1280772
    :cond_1
    iget-object v0, p0, LX/7z8;->a:LX/7yw;

    .line 1280773
    iget-object v1, v0, LX/7yw;->e:LX/7ys;

    move-object v0, v1

    .line 1280774
    iget-object v1, v0, LX/7ys;->d:LX/7yr;

    move-object v0, v1

    .line 1280775
    :try_start_0
    iget-object v1, p0, LX/7z8;->d:LX/7yy;

    invoke-virtual {v0}, LX/7yr;->getDigestInstanceString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/7z8;->a:LX/7yw;

    .line 1280776
    iget-object v4, v3, LX/7yw;->e:LX/7ys;

    move-object v3, v4

    .line 1280777
    iget v4, v3, LX/7ys;->c:I

    move v3, v4

    .line 1280778
    invoke-virtual {v1, v2, v3}, LX/7yy;->a(Ljava/lang/String;I)LX/7yx;

    move-result-object v1

    .line 1280779
    if-eqz v1, :cond_0

    .line 1280780
    iget-object v2, v1, LX/7yx;->a:[B

    move-object v2, v2

    .line 1280781
    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 1280782
    if-eqz p2, :cond_2

    .line 1280783
    const-string v3, "X-Entity-Digest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, LX/7yr;->getHeaderPrefix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280784
    :cond_2
    const-string v0, "X-Digest-Time-Ms"

    .line 1280785
    iget-wide v5, v1, LX/7yx;->b:J

    move-wide v2, v5

    .line 1280786
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1280787
    :catch_0
    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/7z8;I)V
    .locals 6

    .prologue
    .line 1280804
    iget v0, p0, LX/7z8;->c:I

    add-int/2addr v0, p1

    iput v0, p0, LX/7z8;->c:I

    .line 1280805
    iget-object v0, p0, LX/7z8;->b:LX/7yp;

    iget v1, p0, LX/7z8;->c:I

    int-to-float v1, v1

    iget-object v2, p0, LX/7z8;->d:LX/7yy;

    .line 1280806
    iget-wide v4, v2, LX/7yy;->d:J

    move-wide v2, v4

    .line 1280807
    long-to-float v2, v2

    div-float/2addr v1, v2

    invoke-interface {v0, v1}, LX/7yp;->a(F)V

    .line 1280808
    return-void
.end method

.method public static a$redex0(LX/7z8;Ljava/lang/String;Ljava/lang/Exception;Z)V
    .locals 7

    .prologue
    .line 1280788
    if-eqz p3, :cond_0

    iget-object v0, p0, LX/7z8;->a:LX/7yw;

    .line 1280789
    iget-object v1, v0, LX/7yw;->d:LX/7zA;

    .line 1280790
    iget v2, v1, LX/7zA;->b:I

    add-int/lit8 v0, v2, 0x1

    iput v0, v1, LX/7zA;->b:I

    iget-object v0, v1, LX/7zA;->a:LX/7yu;

    iget v0, v0, LX/7yu;->a:I

    if-ge v2, v0, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 1280791
    move v0, v1

    .line 1280792
    if-eqz v0, :cond_0

    .line 1280793
    :try_start_0
    iget-object v0, p0, LX/7z8;->a:LX/7yw;

    .line 1280794
    iget-object v1, v0, LX/7yw;->d:LX/7zA;

    .line 1280795
    iget v2, v1, LX/7zA;->c:I

    .line 1280796
    iget v3, v1, LX/7zA;->c:I

    shl-int/lit8 v3, v3, 0x1

    iget-object v0, v1, LX/7zA;->a:LX/7yu;

    iget v0, v0, LX/7yu;->c:I

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, v1, LX/7zA;->c:I

    .line 1280797
    move v1, v2

    .line 1280798
    move v0, v1

    .line 1280799
    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1280800
    :goto_1
    invoke-virtual {p0}, LX/7z8;->e()V

    .line 1280801
    :goto_2
    return-void

    .line 1280802
    :catch_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 1280803
    :cond_0
    iget-object v6, p0, LX/7z8;->b:LX/7yp;

    new-instance v0, LX/7zB;

    iget v1, p0, LX/7z8;->c:I

    int-to-long v2, v1

    const/4 v4, 0x0

    move-object v1, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/7zB;-><init>(Ljava/lang/String;JZLjava/lang/Exception;)V

    invoke-interface {v6, v0}, LX/7yp;->a(LX/7zB;)V

    goto :goto_2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)LX/7zG;
    .locals 14

    .prologue
    .line 1280709
    :try_start_0
    sget-object v6, LX/1jl;->a:LX/0lp;

    invoke-virtual {v6, p0}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v6

    .line 1280710
    invoke-virtual {v6}, LX/15w;->c()LX/15z;

    .line 1280711
    new-instance v7, LX/7zG;

    invoke-direct {v7}, LX/7zG;-><init>()V

    .line 1280712
    invoke-virtual {v6}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_3

    .line 1280713
    invoke-virtual {v6}, LX/15w;->f()LX/15w;

    .line 1280714
    const/4 v7, 0x0

    .line 1280715
    :cond_0
    move-object v6, v7

    .line 1280716
    move-object v0, v6

    .line 1280717
    if-eqz v0, :cond_1

    iget-wide v2, v0, LX/7zG;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    .line 1280718
    :cond_1
    new-instance v0, LX/7z7;

    invoke-direct {v0, p0}, LX/7z7;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1280719
    :catch_0
    new-instance v0, LX/7z7;

    invoke-direct {v0}, LX/7z7;-><init>()V

    throw v0

    .line 1280720
    :cond_2
    return-object v0

    .line 1280721
    :cond_3
    :goto_0
    invoke-virtual {v6}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_0

    .line 1280722
    invoke-virtual {v6}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1280723
    invoke-virtual {v6}, LX/15w;->c()LX/15z;

    .line 1280724
    const-string v11, "offset"

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1280725
    invoke-virtual {v6}, LX/15w;->F()J

    move-result-wide v12

    iput-wide v12, v7, LX/7zG;->a:J

    .line 1280726
    :goto_1
    invoke-virtual {v6}, LX/15w;->f()LX/15w;

    goto :goto_0

    .line 1280727
    :cond_4
    const-string v11, "duplicate"

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1280728
    invoke-virtual {v6}, LX/15w;->H()Z

    move-result v11

    iput-boolean v11, v7, LX/7zG;->b:Z

    goto :goto_1

    .line 1280729
    :cond_5
    goto :goto_1
.end method

.method public static b(LX/7z8;Ljava/lang/String;Z)LX/7zL;
    .locals 5

    .prologue
    .line 1280730
    :try_start_0
    invoke-static {p1}, LX/7zJ;->a(Ljava/lang/String;)LX/7zH;

    move-result-object v1

    .line 1280731
    if-nez v1, :cond_0

    .line 1280732
    new-instance v0, LX/7z7;

    invoke-direct {v0, p1}, LX/7z7;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1280733
    :catch_0
    new-instance v0, LX/7z7;

    invoke-direct {v0}, LX/7z7;-><init>()V

    throw v0

    .line 1280734
    :cond_0
    const/4 v0, 0x0

    .line 1280735
    :try_start_1
    sget-object v2, LX/7zF;->a:[I

    iget-object v3, p0, LX/7z8;->a:LX/7yw;

    .line 1280736
    iget-object v4, v3, LX/7yw;->a:LX/7yt;

    invoke-virtual {v4}, LX/7yt;->getJsonResponseFieldType()LX/7yv;

    move-result-object v4

    move-object v3, v4

    .line 1280737
    invoke-virtual {v3}, LX/7yv;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1280738
    :goto_0
    if-nez v0, :cond_3

    .line 1280739
    const-string v0, ""

    move-object v1, v0

    .line 1280740
    :goto_1
    iget-object v0, p0, LX/7z8;->a:LX/7yw;

    .line 1280741
    iget-object v2, v0, LX/7yw;->e:LX/7ys;

    move-object v0, v2

    .line 1280742
    iget-boolean v2, v0, LX/7ys;->b:Z

    move v0, v2

    .line 1280743
    if-nez v0, :cond_1

    .line 1280744
    sget-object v0, LX/7zK;->NOT_ATTEMPTED:LX/7zK;

    .line 1280745
    :goto_2
    new-instance v2, LX/7zL;

    invoke-direct {v2, v1, p1, v0}, LX/7zL;-><init>(Ljava/lang/String;Ljava/lang/String;LX/7zK;)V

    return-object v2

    .line 1280746
    :pswitch_0
    iget-object v0, v1, LX/7zH;->b:Ljava/lang/String;

    goto :goto_0

    .line 1280747
    :pswitch_1
    iget-object v0, v1, LX/7zH;->a:Ljava/lang/String;

    goto :goto_0

    .line 1280748
    :cond_1
    if-eqz p2, :cond_2

    .line 1280749
    sget-object v0, LX/7zK;->FOUND:LX/7zK;

    goto :goto_2

    .line 1280750
    :cond_2
    sget-object v0, LX/7zK;->NOT_FOUND:LX/7zK;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :cond_3
    move-object v1, v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(JZ)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1280678
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, LX/7z8;->a:LX/7yw;

    .line 1280679
    iget-object v2, v1, LX/7yw;->b:Ljava/util/Map;

    move-object v1, v2

    .line 1280680
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 1280681
    const-string v1, "X-Entity-Length"

    iget-object v2, p0, LX/7z8;->d:LX/7yy;

    .line 1280682
    iget-wide v5, v2, LX/7yy;->d:J

    move-wide v2, v5

    .line 1280683
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280684
    const-string v1, "Offset"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280685
    const-string v1, "X-Entity-Type"

    iget-object v2, p0, LX/7z8;->d:LX/7yy;

    .line 1280686
    iget-object v3, v2, LX/7yy;->e:Ljava/lang/String;

    move-object v2, v3

    .line 1280687
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280688
    const-string v1, "X-Entity-Name"

    iget-object v2, p0, LX/7z8;->d:LX/7yy;

    .line 1280689
    iget-object v3, v2, LX/7yy;->f:Ljava/lang/String;

    move-object v2, v3

    .line 1280690
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280691
    invoke-static {p0, v0, p3}, LX/7z8;->a(LX/7z8;Ljava/util/Map;Z)V

    .line 1280692
    return-object v0
.end method

.method public static g(LX/7z8;)V
    .locals 1

    .prologue
    .line 1280675
    const/4 v0, 0x0

    iput-object v0, p0, LX/7z8;->h:LX/7z3;

    .line 1280676
    iget-object v0, p0, LX/7z8;->b:LX/7yp;

    invoke-interface {v0}, LX/7yp;->b()V

    .line 1280677
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 1280693
    iget-object v0, p0, LX/7z8;->a:LX/7yw;

    invoke-virtual {v0}, LX/7yw;->h()V

    .line 1280694
    invoke-virtual {p0}, LX/7z8;->e()V

    .line 1280695
    iget-object v0, p0, LX/7z8;->b:LX/7yp;

    invoke-interface {v0}, LX/7yp;->a()V

    .line 1280696
    return-void
.end method

.method public final a(JZ)V
    .locals 7

    .prologue
    .line 1280704
    long-to-int v0, p1

    iput v0, p0, LX/7z8;->c:I

    .line 1280705
    iget-object v0, p0, LX/7z8;->e:LX/7zR;

    sget-object v1, LX/7z5;->POST:LX/7z5;

    invoke-direct {p0, p1, p2, p3}, LX/7z8;->b(JZ)Ljava/util/Map;

    move-result-object v2

    .line 1280706
    iget-object v3, p0, LX/7z8;->i:Ljava/net/URI;

    move-object v3, v3

    .line 1280707
    new-instance v4, LX/7z6;

    iget-object v5, p0, LX/7z8;->d:LX/7yy;

    invoke-direct {v4, v5, p1, p2}, LX/7z6;-><init>(LX/7yy;J)V

    new-instance v5, LX/7zE;

    invoke-direct {v5, p0, p3}, LX/7zE;-><init>(LX/7z8;Z)V

    invoke-virtual/range {v0 .. v5}, LX/7zR;->a(LX/7z5;Ljava/util/Map;Ljava/net/URI;LX/7z6;LX/7z4;)LX/7z3;

    move-result-object v0

    iput-object v0, p0, LX/7z8;->h:LX/7z3;

    .line 1280708
    return-void
.end method

.method public final e()V
    .locals 6

    .prologue
    .line 1280697
    iget-object v0, p0, LX/7z8;->e:LX/7zR;

    sget-object v1, LX/7z5;->GET:LX/7z5;

    .line 1280698
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1280699
    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, LX/7z8;->a(LX/7z8;Ljava/util/Map;Z)V

    .line 1280700
    move-object v2, v2

    .line 1280701
    iget-object v3, p0, LX/7z8;->i:Ljava/net/URI;

    move-object v3, v3

    .line 1280702
    const/4 v4, 0x0

    new-instance v5, LX/7zD;

    invoke-direct {v5, p0}, LX/7zD;-><init>(LX/7z8;)V

    invoke-virtual/range {v0 .. v5}, LX/7zR;->a(LX/7z5;Ljava/util/Map;Ljava/net/URI;LX/7z6;LX/7z4;)LX/7z3;

    move-result-object v0

    iput-object v0, p0, LX/7z8;->h:LX/7z3;

    .line 1280703
    return-void
.end method
