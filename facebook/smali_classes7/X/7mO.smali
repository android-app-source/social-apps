.class public LX/7mO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/7mO;


# instance fields
.field public final a:LX/7mK;

.field private final b:LX/0lB;

.field public final c:LX/0Uh;


# direct methods
.method public constructor <init>(LX/7mK;LX/0lB;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1236976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236977
    iput-object p1, p0, LX/7mO;->a:LX/7mK;

    .line 1236978
    iput-object p2, p0, LX/7mO;->b:LX/0lB;

    .line 1236979
    iput-object p3, p0, LX/7mO;->c:LX/0Uh;

    .line 1236980
    return-void
.end method

.method public static a(LX/0QB;)LX/7mO;
    .locals 6

    .prologue
    .line 1236963
    sget-object v0, LX/7mO;->d:LX/7mO;

    if-nez v0, :cond_1

    .line 1236964
    const-class v1, LX/7mO;

    monitor-enter v1

    .line 1236965
    :try_start_0
    sget-object v0, LX/7mO;->d:LX/7mO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1236966
    if-eqz v2, :cond_0

    .line 1236967
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1236968
    new-instance p0, LX/7mO;

    invoke-static {v0}, LX/7mK;->a(LX/0QB;)LX/7mK;

    move-result-object v3

    check-cast v3, LX/7mK;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lB;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, LX/7mO;-><init>(LX/7mK;LX/0lB;LX/0Uh;)V

    .line 1236969
    move-object v0, p0

    .line 1236970
    sput-object v0, LX/7mO;->d:LX/7mO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1236971
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1236972
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1236973
    :cond_1
    sget-object v0, LX/7mO;->d:LX/7mO;

    return-object v0

    .line 1236974
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1236975
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/database/Cursor;ILcom/facebook/graphql/model/GraphQLStory;)V
    .locals 7

    .prologue
    .line 1236912
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1236913
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    .line 1236914
    cmp-long v0, v4, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "The value in the \'creation_time\' column does not match that in the GraphQLStory"

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "\nCreation Time Column: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nGraphQLStory Creation Time: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1236915
    return-void

    .line 1236916
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(JLandroid/database/sqlite/SQLiteDatabase;I)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "I)",
            "LX/0Px",
            "<",
            "LX/7mj;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1236932
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1236933
    const-string v1, "draft_story"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1236934
    sget-object v1, LX/7mL;->b:LX/0U1;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0U1;->d(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 1236935
    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v1, p3

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 1236936
    if-nez v4, :cond_0

    .line 1236937
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1236938
    :goto_0
    return-object v0

    .line 1236939
    :cond_0
    sget-object v0, LX/7mL;->a:LX/0U1;

    invoke-virtual {v0, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    .line 1236940
    sget-object v0, LX/7mL;->b:LX/0U1;

    invoke-virtual {v0, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v6

    .line 1236941
    sget-object v0, LX/7mL;->c:LX/0U1;

    invoke-virtual {v0, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v7

    .line 1236942
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 1236943
    :goto_1
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1236944
    const/4 v0, 0x1

    if-gt p4, v0, :cond_1

    .line 1236945
    iget-object v0, p0, LX/7mO;->b:LX/0lB;

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1236946
    const-wide/16 v2, -0x1

    move-wide v9, v2

    move-object v2, v0

    move-wide v0, v9

    .line 1236947
    :goto_2
    invoke-static {v4, v6, v2}, LX/7mO;->a(Landroid/database/Cursor;ILcom/facebook/graphql/model/GraphQLStory;)V

    .line 1236948
    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, LX/7mj;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)LX/7mj;

    move-result-object v2

    .line 1236949
    invoke-virtual {v2, v0, v1}, LX/7mj;->a(J)Z

    .line 1236950
    invoke-virtual {v8, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch LX/28E; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1236951
    :catch_0
    move-exception v0

    .line 1236952
    :try_start_1
    new-instance v1, Ljava/io/IOException;

    const-string v2, "When deserializing JSON blob into GraphQLStory, we had a mapping issure"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1236953
    :catchall_0
    move-exception v0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1236954
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/7mO;->b:LX/0lB;

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;

    invoke-virtual {v0, v1, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;

    .line 1236955
    invoke-virtual {v0}, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 1236956
    invoke-virtual {v0}, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;->b()J
    :try_end_2
    .catch LX/28E; {:try_start_2 .. :try_end_2} :catch_0
    .catch LX/2aQ; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v0

    goto :goto_2

    .line 1236957
    :cond_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1236958
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1236959
    :catch_1
    move-exception v0

    .line 1236960
    new-instance v1, Ljava/io/IOException;

    const-string v2, "When deserializing JSON blob into GraphQLStory, we had a parsing error"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1236961
    :catch_2
    move-exception v0

    .line 1236962
    throw v0
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;LX/7mj;)V
    .locals 4

    .prologue
    .line 1236917
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1236918
    sget-object v1, LX/7mL;->a:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1236919
    sget-object v1, LX/7mL;->b:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1236920
    iget-object v2, p2, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v2, v2

    .line 1236921
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1236922
    iget-object v1, p0, LX/7mO;->c:LX/0Uh;

    invoke-static {v1}, LX/7mN;->a(LX/0Uh;)I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_0

    .line 1236923
    sget-object v1, LX/7mL;->c:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/7mO;->b:LX/0lB;

    .line 1236924
    iget-object v3, p2, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v3, v3

    .line 1236925
    invoke-virtual {v2, v3}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1236926
    :goto_0
    const-string v1, "draft_story"

    const-string v2, ""

    const v3, 0x71036b90

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0xee48846

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1236927
    return-void

    .line 1236928
    :cond_0
    invoke-static {p2}, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;->a(LX/7mj;)Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;

    move-result-object v1

    .line 1236929
    sget-object v2, LX/7mL;->c:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/7mO;->b:LX/0lB;

    invoke-virtual {v3, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1236930
    :catch_0
    move-exception v0

    .line 1236931
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "CompostDraftStory couldn\'t be serialized into JSON for storage"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
