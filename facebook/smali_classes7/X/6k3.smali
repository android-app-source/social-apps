.class public LX/6k3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final participants:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6kt;",
            ">;"
        }
    .end annotation
.end field

.field public final threadKey:LX/6l9;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1134389
    new-instance v0, LX/1sv;

    const-string v1, "DeltaNewGroupThread"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6k3;->b:LX/1sv;

    .line 1134390
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k3;->c:LX/1sw;

    .line 1134391
    new-instance v0, LX/1sw;

    const-string v1, "participants"

    const/16 v2, 0xf

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k3;->d:LX/1sw;

    .line 1134392
    sput-boolean v4, LX/6k3;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6l9;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6l9;",
            "Ljava/util/List",
            "<",
            "LX/6kt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1134393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1134394
    iput-object p1, p0, LX/6k3;->threadKey:LX/6l9;

    .line 1134395
    iput-object p2, p0, LX/6k3;->participants:Ljava/util/List;

    .line 1134396
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1134397
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1134398
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1134399
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1134400
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "DeltaNewGroupThread"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1134401
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134402
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134403
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134404
    const/4 v1, 0x1

    .line 1134405
    iget-object v5, p0, LX/6k3;->threadKey:LX/6l9;

    if-eqz v5, :cond_0

    .line 1134406
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134407
    const-string v1, "threadKey"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134408
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134409
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134410
    iget-object v1, p0, LX/6k3;->threadKey:LX/6l9;

    if-nez v1, :cond_6

    .line 1134411
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134412
    :goto_3
    const/4 v1, 0x0

    .line 1134413
    :cond_0
    iget-object v5, p0, LX/6k3;->participants:Ljava/util/List;

    if-eqz v5, :cond_2

    .line 1134414
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134415
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134416
    const-string v1, "participants"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134417
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134418
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134419
    iget-object v0, p0, LX/6k3;->participants:Ljava/util/List;

    if-nez v0, :cond_7

    .line 1134420
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134421
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134422
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134423
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1134424
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1134425
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1134426
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1134427
    :cond_6
    iget-object v1, p0, LX/6k3;->threadKey:LX/6l9;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1134428
    :cond_7
    iget-object v0, p0, LX/6k3;->participants:Ljava/util/List;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1134429
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1134430
    iget-object v0, p0, LX/6k3;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1134431
    iget-object v0, p0, LX/6k3;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1134432
    sget-object v0, LX/6k3;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134433
    iget-object v0, p0, LX/6k3;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1134434
    :cond_0
    iget-object v0, p0, LX/6k3;->participants:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1134435
    iget-object v0, p0, LX/6k3;->participants:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1134436
    sget-object v0, LX/6k3;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134437
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/6k3;->participants:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1134438
    iget-object v0, p0, LX/6k3;->participants:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kt;

    .line 1134439
    invoke-virtual {v0, p1}, LX/6kt;->a(LX/1su;)V

    goto :goto_0

    .line 1134440
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1134441
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1134442
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1134443
    if-nez p1, :cond_1

    .line 1134444
    :cond_0
    :goto_0
    return v0

    .line 1134445
    :cond_1
    instance-of v1, p1, LX/6k3;

    if-eqz v1, :cond_0

    .line 1134446
    check-cast p1, LX/6k3;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1134447
    if-nez p1, :cond_3

    .line 1134448
    :cond_2
    :goto_1
    move v0, v2

    .line 1134449
    goto :goto_0

    .line 1134450
    :cond_3
    iget-object v0, p0, LX/6k3;->threadKey:LX/6l9;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1134451
    :goto_2
    iget-object v3, p1, LX/6k3;->threadKey:LX/6l9;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1134452
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1134453
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1134454
    iget-object v0, p0, LX/6k3;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6k3;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1134455
    :cond_5
    iget-object v0, p0, LX/6k3;->participants:Ljava/util/List;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1134456
    :goto_4
    iget-object v3, p1, LX/6k3;->participants:Ljava/util/List;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1134457
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1134458
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1134459
    iget-object v0, p0, LX/6k3;->participants:Ljava/util/List;

    iget-object v3, p1, LX/6k3;->participants:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1134460
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1134461
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1134462
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1134463
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1134464
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1134465
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1134466
    sget-boolean v0, LX/6k3;->a:Z

    .line 1134467
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6k3;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1134468
    return-object v0
.end method
