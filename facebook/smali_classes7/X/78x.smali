.class public final LX/78x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/78y;


# direct methods
.method public constructor <init>(LX/78y;)V
    .locals 0

    .prologue
    .line 1173660
    iput-object p1, p0, LX/78x;->a:LX/78y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1173661
    iget-object v0, p0, LX/78x;->a:LX/78y;

    iget-boolean v0, v0, LX/78y;->c:Z

    if-eqz v0, :cond_1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 1173662
    :goto_0
    if-eqz v0, :cond_0

    .line 1173663
    iget-object v0, p0, LX/78x;->a:LX/78y;

    iget-object v0, v0, LX/78y;->b:Landroid/text/Spannable;

    if-nez v0, :cond_2

    move v0, v1

    .line 1173664
    :goto_1
    if-eqz v0, :cond_3

    .line 1173665
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    .line 1173666
    iget-object v3, p0, LX/78x;->a:LX/78y;

    iget-object v4, p0, LX/78x;->a:LX/78y;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, LX/78x;->a:LX/78y;

    invoke-virtual {v6}, LX/78y;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f081944

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v8, p0, LX/78x;->a:LX/78y;

    iget-object v8, v8, LX/78y;->a:Ljava/lang/String;

    aput-object v8, v1, v2

    invoke-virtual {v6, v7, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, LX/78y;->a(LX/78y;Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v1

    .line 1173667
    iput-object v1, v3, LX/78y;->b:Landroid/text/Spannable;

    .line 1173668
    iget-object v1, p0, LX/78x;->a:LX/78y;

    iget-object v1, v1, LX/78y;->b:Landroid/text/Spannable;

    invoke-interface {p1, v1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 1173669
    iget-object v1, p0, LX/78x;->a:LX/78y;

    invoke-virtual {v1, v0}, LX/78y;->setSelection(I)V

    .line 1173670
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 1173671
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1173672
    goto :goto_1

    .line 1173673
    :cond_3
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, LX/78x;->a:LX/78y;

    iget-object v1, v1, LX/78y;->b:Landroid/text/Spannable;

    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 1173674
    iget-object v0, p0, LX/78x;->a:LX/78y;

    const/4 v1, 0x0

    .line 1173675
    iput-object v1, v0, LX/78y;->b:Landroid/text/Spannable;

    .line 1173676
    invoke-interface {p1}, Landroid/text/Editable;->clear()V

    goto :goto_2
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1173677
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1173678
    return-void
.end method
