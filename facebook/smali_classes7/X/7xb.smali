.class public LX/7xb;
.super LX/62U;
.source ""


# instance fields
.field private m:Lcom/facebook/resources/ui/FbTextView;

.field private n:Lcom/facebook/resources/ui/FbTextView;

.field private o:Lcom/facebook/resources/ui/FbTextView;

.field public p:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/7xH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1277635
    invoke-direct {p0, p1}, LX/62U;-><init>(Landroid/view/View;)V

    .line 1277636
    const-class v0, LX/7xb;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, LX/7xb;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1277637
    const v0, 0x7f0d0e7b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7xb;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 1277638
    const v0, 0x7f0d0e7c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7xb;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1277639
    const v0, 0x7f0d0e7d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7xb;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 1277640
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/7xb;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/7xH;->a(LX/0QB;)LX/7xH;

    move-result-object p0

    check-cast p0, LX/7xH;

    iput-object v0, p1, LX/7xb;->p:Landroid/content/Context;

    iput-object p0, p1, LX/7xb;->q:LX/7xH;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;)V
    .locals 3

    .prologue
    .line 1277641
    iget-object v0, p1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1277642
    iget-object v0, p0, LX/7xb;->m:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/7xb;->q:LX/7xH;

    iget-object v2, p1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->i:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1, v2}, LX/7xH;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277643
    iget-object v0, p0, LX/7xb;->n:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277644
    :goto_0
    iget-object v0, p1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->d:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;->SOLD_OUT:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    if-ne v0, v1, :cond_2

    .line 1277645
    iget-object v0, p0, LX/7xb;->o:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/7xb;->p:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081f93

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277646
    :cond_0
    :goto_1
    return-void

    .line 1277647
    :cond_1
    iget-object v0, p0, LX/7xb;->m:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277648
    iget-object v0, p0, LX/7xb;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/7xb;->q:LX/7xH;

    iget-object v2, p1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->i:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1, v2}, LX/7xH;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1277649
    :cond_2
    iget-object v0, p1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->d:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;->POST_SALE:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    if-ne v0, v1, :cond_0

    .line 1277650
    iget-object v0, p0, LX/7xb;->o:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/7xb;->p:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081f94

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
