.class public final enum LX/7IJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7IJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7IJ;

.field public static final enum INSEEK:LX/7IJ;

.field public static final enum PAUSED:LX/7IJ;

.field public static final enum START:LX/7IJ;

.field public static final enum STOP:LX/7IJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1191597
    new-instance v0, LX/7IJ;

    const-string v1, "START"

    invoke-direct {v0, v1, v2}, LX/7IJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IJ;->START:LX/7IJ;

    .line 1191598
    new-instance v0, LX/7IJ;

    const-string v1, "STOP"

    invoke-direct {v0, v1, v3}, LX/7IJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IJ;->STOP:LX/7IJ;

    .line 1191599
    new-instance v0, LX/7IJ;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v4}, LX/7IJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IJ;->PAUSED:LX/7IJ;

    .line 1191600
    new-instance v0, LX/7IJ;

    const-string v1, "INSEEK"

    invoke-direct {v0, v1, v5}, LX/7IJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IJ;->INSEEK:LX/7IJ;

    .line 1191601
    const/4 v0, 0x4

    new-array v0, v0, [LX/7IJ;

    sget-object v1, LX/7IJ;->START:LX/7IJ;

    aput-object v1, v0, v2

    sget-object v1, LX/7IJ;->STOP:LX/7IJ;

    aput-object v1, v0, v3

    sget-object v1, LX/7IJ;->PAUSED:LX/7IJ;

    aput-object v1, v0, v4

    sget-object v1, LX/7IJ;->INSEEK:LX/7IJ;

    aput-object v1, v0, v5

    sput-object v0, LX/7IJ;->$VALUES:[LX/7IJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1191594
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7IJ;
    .locals 1

    .prologue
    .line 1191595
    const-class v0, LX/7IJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7IJ;

    return-object v0
.end method

.method public static values()[LX/7IJ;
    .locals 1

    .prologue
    .line 1191596
    sget-object v0, LX/7IJ;->$VALUES:[LX/7IJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7IJ;

    return-object v0
.end method
