.class public final LX/8Nk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Nj;


# instance fields
.field public final synthetic a:LX/8Nm;


# direct methods
.method public constructor <init>(LX/8Nm;)V
    .locals 0

    .prologue
    .line 1337869
    iput-object p1, p0, LX/8Nk;->a:LX/8Nm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 1337856
    iget-object v0, p0, LX/8Nk;->a:LX/8Nm;

    .line 1337857
    iget-object v1, v0, LX/8Nm;->b:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fetch_real_story_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, LX/8Nm;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/8Nm;->f:Ljava/lang/String;

    iget-boolean v4, v0, LX/8Nm;->g:Z

    const/4 p0, 0x0

    .line 1337858
    new-instance v6, Lcom/facebook/api/story/FetchSingleStoryParams;

    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 1337859
    if-eqz v4, :cond_0

    .line 1337860
    sget-object v7, LX/5Go;->PLATFORM_DEFAULT:LX/5Go;

    .line 1337861
    :goto_0
    move-object v7, v7

    .line 1337862
    const/16 v8, 0x19

    invoke-direct {v6, v3, v5, v7, v8}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;I)V

    .line 1337863
    iget-object v7, v0, LX/8Nm;->a:LX/0tX;

    iget-object v5, v0, LX/8Nm;->c:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/3HM;

    invoke-virtual {v5, v6, p0, p0}, LX/3HM;->a(Lcom/facebook/api/story/FetchSingleStoryParams;Ljava/lang/String;LX/3HP;)LX/0zO;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    move-object v3, v5

    .line 1337864
    new-instance v4, LX/8Nl;

    invoke-direct {v4, v0}, LX/8Nl;-><init>(LX/8Nm;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1337865
    return-void

    :cond_0
    sget-object v7, LX/5Go;->GRAPHQL_VIDEO_CREATION_STORY:LX/5Go;

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1337866
    iget-object v0, p0, LX/8Nk;->a:LX/8Nm;

    iget-object v0, v0, LX/8Nm;->e:LX/8Ku;

    if-eqz v0, :cond_0

    .line 1337867
    iget-object v0, p0, LX/8Nk;->a:LX/8Nm;

    iget-object v0, v0, LX/8Nm;->e:LX/8Ku;

    invoke-virtual {v0}, LX/8Ku;->a()V

    .line 1337868
    :cond_0
    return-void
.end method
