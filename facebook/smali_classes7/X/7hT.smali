.class public LX/7hT;
.super LX/7hR;
.source ""


# static fields
.field public static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "LX/7hT;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:I

.field private final b:LX/8YK;

.field public final c:Landroid/view/View;

.field public d:LX/7hP;

.field public e:LX/7hO;

.field public f:Z

.field public g:F

.field public h:F

.field public i:Z

.field public j:F

.field public k:F

.field public l:Z

.field public m:F

.field public n:F

.field public o:F

.field public p:Z

.field public q:F

.field public r:F

.field public s:F

.field public t:Z

.field public u:F

.field public v:F

.field public w:Z

.field public x:F

.field public y:F

.field public z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1226195
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/7hT;->a:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    .line 1226201
    invoke-direct {p0}, LX/7hR;-><init>()V

    .line 1226202
    iput-boolean v2, p0, LX/7hT;->f:Z

    .line 1226203
    iput-boolean v2, p0, LX/7hT;->i:Z

    .line 1226204
    iput-boolean v2, p0, LX/7hT;->l:Z

    .line 1226205
    iput-boolean v2, p0, LX/7hT;->p:Z

    .line 1226206
    iput-boolean v2, p0, LX/7hT;->t:Z

    .line 1226207
    iput-boolean v2, p0, LX/7hT;->w:Z

    .line 1226208
    iput v0, p0, LX/7hT;->z:I

    .line 1226209
    iput v0, p0, LX/7hT;->A:I

    .line 1226210
    invoke-static {}, LX/8YM;->b()LX/8YM;

    move-result-object v0

    invoke-virtual {v0}, LX/8YH;->a()LX/8YK;

    move-result-object v0

    sget-object v1, LX/7hM;->a:LX/8YL;

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/8YL;)LX/8YK;

    move-result-object v0

    new-instance v1, LX/7hS;

    invoke-direct {v1, p0}, LX/7hS;-><init>(LX/7hT;)V

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/7hQ;)LX/8YK;

    move-result-object v0

    iput-object v0, p0, LX/7hT;->b:LX/8YK;

    .line 1226211
    iput-object p1, p0, LX/7hT;->c:Landroid/view/View;

    .line 1226212
    return-void
.end method

.method public static a(Landroid/view/View;)LX/7hT;
    .locals 2

    .prologue
    .line 1226196
    sget-object v0, LX/7hT;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1226197
    sget-object v0, LX/7hT;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7hT;

    .line 1226198
    :goto_0
    return-object v0

    .line 1226199
    :cond_0
    new-instance v0, LX/7hT;

    invoke-direct {v0, p0}, LX/7hT;-><init>(Landroid/view/View;)V

    .line 1226200
    sget-object v1, LX/7hT;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static b(FFF)F
    .locals 1

    .prologue
    .line 1226190
    sub-float v0, p1, p0

    mul-float/2addr v0, p2

    add-float/2addr v0, p0

    return v0
.end method


# virtual methods
.method public final a(FF)LX/7hT;
    .locals 1

    .prologue
    .line 1226191
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7hT;->f:Z

    .line 1226192
    iput p1, p0, LX/7hT;->g:F

    .line 1226193
    iput p2, p0, LX/7hT;->h:F

    .line 1226194
    return-object p0
.end method

.method public final b(FF)LX/7hT;
    .locals 1

    .prologue
    .line 1226166
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7hT;->i:Z

    .line 1226167
    iput p1, p0, LX/7hT;->j:F

    .line 1226168
    iput p2, p0, LX/7hT;->k:F

    .line 1226169
    return-object p0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1226165
    iget-object v0, p0, LX/7hT;->b:LX/8YK;

    invoke-virtual {v0}, LX/8YK;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()LX/7hT;
    .locals 4

    .prologue
    .line 1226187
    sget-object v0, LX/7hT;->a:Ljava/util/HashMap;

    iget-object v1, p0, LX/7hT;->c:Landroid/view/View;

    invoke-virtual {v0, v1, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1226188
    iget-object v0, p0, LX/7hT;->b:LX/8YK;

    invoke-virtual {v0}, LX/8YK;->f()LX/8YK;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/8YK;->a(D)LX/8YK;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/8YK;->b(D)LX/8YK;

    .line 1226189
    return-object p0
.end method

.method public final e()LX/7hT;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v3, -0x1

    const-wide v4, 0x3f747ae147ae147bL    # 0.005

    const/4 v2, 0x0

    .line 1226170
    iget-object v0, p0, LX/7hT;->b:LX/8YK;

    invoke-virtual {v0}, LX/8YK;->f()LX/8YK;

    .line 1226171
    iget-object v0, p0, LX/7hT;->b:LX/8YK;

    sget-object v1, LX/7hM;->a:LX/8YL;

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/8YL;)LX/8YK;

    .line 1226172
    iget-object v0, p0, LX/7hT;->b:LX/8YK;

    .line 1226173
    iput-wide v4, v0, LX/8YK;->k:D

    .line 1226174
    iget-object v0, p0, LX/7hT;->b:LX/8YK;

    .line 1226175
    iput-wide v4, v0, LX/8YK;->l:D

    .line 1226176
    iget-object v0, p0, LX/7hT;->b:LX/8YK;

    .line 1226177
    iput-boolean v2, v0, LX/8YK;->c:Z

    .line 1226178
    iput-boolean v2, p0, LX/7hT;->f:Z

    .line 1226179
    iput-boolean v2, p0, LX/7hT;->i:Z

    .line 1226180
    iput-boolean v2, p0, LX/7hT;->t:Z

    .line 1226181
    iput-boolean v2, p0, LX/7hT;->w:Z

    .line 1226182
    iput v3, p0, LX/7hT;->z:I

    .line 1226183
    iput v3, p0, LX/7hT;->A:I

    .line 1226184
    iput-object v6, p0, LX/7hT;->d:LX/7hP;

    .line 1226185
    iput-object v6, p0, LX/7hT;->e:LX/7hO;

    .line 1226186
    return-object p0
.end method
