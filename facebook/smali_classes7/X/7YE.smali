.class public final LX/7YE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 1219370
    const/4 v5, 0x0

    .line 1219371
    const-wide/16 v6, 0x0

    .line 1219372
    const/4 v4, 0x0

    .line 1219373
    const/4 v3, 0x0

    .line 1219374
    const/4 v2, 0x0

    .line 1219375
    const/4 v1, 0x0

    .line 1219376
    const/4 v0, 0x0

    .line 1219377
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 1219378
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1219379
    const/4 v0, 0x0

    .line 1219380
    :goto_0
    return v0

    .line 1219381
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_6

    .line 1219382
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 1219383
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1219384
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v10, :cond_0

    if-eqz v0, :cond_0

    .line 1219385
    const-string v4, "code"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1219386
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v5, v0

    goto :goto_1

    .line 1219387
    :cond_1
    const-string v4, "full_price"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1219388
    const/4 v0, 0x1

    .line 1219389
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 1219390
    :cond_2
    const-string v4, "id"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1219391
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v9, v0

    goto :goto_1

    .line 1219392
    :cond_3
    const-string v4, "is_loan"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1219393
    const/4 v0, 0x1

    .line 1219394
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v6, v0

    move v8, v4

    goto :goto_1

    .line 1219395
    :cond_4
    const-string v4, "name"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1219396
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v7, v0

    goto :goto_1

    .line 1219397
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1219398
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1219399
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1219400
    if-eqz v1, :cond_7

    .line 1219401
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1219402
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1219403
    if-eqz v6, :cond_8

    .line 1219404
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->a(IZ)V

    .line 1219405
    :cond_8
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1219406
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_9
    move v8, v3

    move v9, v4

    move v11, v2

    move-wide v2, v6

    move v6, v0

    move v7, v11

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1219407
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1219408
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1219409
    if-eqz v0, :cond_0

    .line 1219410
    const-string v1, "code"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219411
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219412
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1219413
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_1

    .line 1219414
    const-string v2, "full_price"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219415
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1219416
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1219417
    if-eqz v0, :cond_2

    .line 1219418
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219419
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219420
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1219421
    if-eqz v0, :cond_3

    .line 1219422
    const-string v1, "is_loan"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219423
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1219424
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1219425
    if-eqz v0, :cond_4

    .line 1219426
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219427
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219428
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1219429
    return-void
.end method
