.class public final enum LX/8A3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8A3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8A3;

.field public static final enum ADMINISTER:LX/8A3;

.field public static final enum BASIC_ADMIN:LX/8A3;

.field public static final enum CREATE_ADS:LX/8A3;

.field public static final enum CREATE_CONTENT:LX/8A3;

.field public static final enum EDIT_PROFILE:LX/8A3;

.field public static final enum MODERATE_CONTENT:LX/8A3;


# instance fields
.field private mPermissionBit:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1305583
    new-instance v0, LX/8A3;

    const-string v1, "ADMINISTER"

    invoke-direct {v0, v1, v4, v4}, LX/8A3;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8A3;->ADMINISTER:LX/8A3;

    .line 1305584
    new-instance v0, LX/8A3;

    const-string v1, "EDIT_PROFILE"

    invoke-direct {v0, v1, v5, v5}, LX/8A3;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8A3;->EDIT_PROFILE:LX/8A3;

    .line 1305585
    new-instance v0, LX/8A3;

    const-string v1, "CREATE_CONTENT"

    invoke-direct {v0, v1, v6, v6}, LX/8A3;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8A3;->CREATE_CONTENT:LX/8A3;

    .line 1305586
    new-instance v0, LX/8A3;

    const-string v1, "MODERATE_CONTENT"

    invoke-direct {v0, v1, v7, v7}, LX/8A3;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8A3;->MODERATE_CONTENT:LX/8A3;

    .line 1305587
    new-instance v0, LX/8A3;

    const-string v1, "CREATE_ADS"

    invoke-direct {v0, v1, v8, v8}, LX/8A3;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8A3;->CREATE_ADS:LX/8A3;

    .line 1305588
    new-instance v0, LX/8A3;

    const-string v1, "BASIC_ADMIN"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/8A3;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/8A3;->BASIC_ADMIN:LX/8A3;

    .line 1305589
    const/4 v0, 0x6

    new-array v0, v0, [LX/8A3;

    sget-object v1, LX/8A3;->ADMINISTER:LX/8A3;

    aput-object v1, v0, v4

    sget-object v1, LX/8A3;->EDIT_PROFILE:LX/8A3;

    aput-object v1, v0, v5

    sget-object v1, LX/8A3;->CREATE_CONTENT:LX/8A3;

    aput-object v1, v0, v6

    sget-object v1, LX/8A3;->MODERATE_CONTENT:LX/8A3;

    aput-object v1, v0, v7

    sget-object v1, LX/8A3;->CREATE_ADS:LX/8A3;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/8A3;->BASIC_ADMIN:LX/8A3;

    aput-object v2, v0, v1

    sput-object v0, LX/8A3;->$VALUES:[LX/8A3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1305590
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1305591
    iput p3, p0, LX/8A3;->mPermissionBit:I

    .line 1305592
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8A3;
    .locals 1

    .prologue
    .line 1305593
    const-class v0, LX/8A3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8A3;

    return-object v0
.end method

.method public static values()[LX/8A3;
    .locals 1

    .prologue
    .line 1305594
    sget-object v0, LX/8A3;->$VALUES:[LX/8A3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8A3;

    return-object v0
.end method


# virtual methods
.method public final getBit()I
    .locals 1

    .prologue
    .line 1305595
    iget v0, p0, LX/8A3;->mPermissionBit:I

    return v0
.end method
