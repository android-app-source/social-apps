.class public final enum LX/8Bk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Bk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Bk;

.field public static final enum HTTP:LX/8Bk;

.field public static final enum MQTT:LX/8Bk;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1309776
    new-instance v0, LX/8Bk;

    const-string v1, "MQTT"

    invoke-direct {v0, v1, v2}, LX/8Bk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Bk;->MQTT:LX/8Bk;

    .line 1309777
    new-instance v0, LX/8Bk;

    const-string v1, "HTTP"

    invoke-direct {v0, v1, v3}, LX/8Bk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Bk;->HTTP:LX/8Bk;

    .line 1309778
    const/4 v0, 0x2

    new-array v0, v0, [LX/8Bk;

    sget-object v1, LX/8Bk;->MQTT:LX/8Bk;

    aput-object v1, v0, v2

    sget-object v1, LX/8Bk;->HTTP:LX/8Bk;

    aput-object v1, v0, v3

    sput-object v0, LX/8Bk;->$VALUES:[LX/8Bk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1309779
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Bk;
    .locals 1

    .prologue
    .line 1309780
    const-class v0, LX/8Bk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Bk;

    return-object v0
.end method

.method public static values()[LX/8Bk;
    .locals 1

    .prologue
    .line 1309781
    sget-object v0, LX/8Bk;->$VALUES:[LX/8Bk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Bk;

    return-object v0
.end method
