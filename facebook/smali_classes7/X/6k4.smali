.class public LX/6k4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;


# instance fields
.field public final attachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6jZ;",
            ">;"
        }
    .end annotation
.end field

.field public final body:Ljava/lang/String;

.field public final data:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final messageMetadata:LX/6kn;

.field public final stickerId:Ljava/lang/Long;

.field public final ttl:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1134679
    new-instance v0, LX/1sv;

    const-string v1, "DeltaNewMessage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6k4;->b:LX/1sv;

    .line 1134680
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k4;->c:LX/1sw;

    .line 1134681
    new-instance v0, LX/1sw;

    const-string v1, "body"

    const/16 v2, 0xb

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k4;->d:LX/1sw;

    .line 1134682
    new-instance v0, LX/1sw;

    const-string v1, "stickerId"

    const/16 v2, 0xa

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k4;->e:LX/1sw;

    .line 1134683
    new-instance v0, LX/1sw;

    const-string v1, "attachments"

    const/16 v2, 0xf

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k4;->f:LX/1sw;

    .line 1134684
    new-instance v0, LX/1sw;

    const-string v1, "ttl"

    const/16 v2, 0x8

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k4;->g:LX/1sw;

    .line 1134685
    new-instance v0, LX/1sw;

    const-string v1, "data"

    const/16 v2, 0xd

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k4;->h:LX/1sw;

    .line 1134686
    sput-boolean v4, LX/6k4;->a:Z

    return-void
.end method

.method private constructor <init>(LX/6kn;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Integer;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kn;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "LX/6jZ;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1134671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1134672
    iput-object p1, p0, LX/6k4;->messageMetadata:LX/6kn;

    .line 1134673
    iput-object p2, p0, LX/6k4;->body:Ljava/lang/String;

    .line 1134674
    iput-object p3, p0, LX/6k4;->stickerId:Ljava/lang/Long;

    .line 1134675
    iput-object p4, p0, LX/6k4;->attachments:Ljava/util/List;

    .line 1134676
    iput-object p5, p0, LX/6k4;->ttl:Ljava/lang/Integer;

    .line 1134677
    iput-object p6, p0, LX/6k4;->data:Ljava/util/Map;

    .line 1134678
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1134666
    iget-object v0, p0, LX/6k4;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1134667
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6k4;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1134668
    :cond_0
    iget-object v0, p0, LX/6k4;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    sget-object v0, LX/6kk;->a:LX/1sn;

    iget-object v1, p0, LX/6k4;->ttl:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1134669
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'ttl\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6k4;->ttl:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1134670
    :cond_1
    return-void
.end method

.method public static b(LX/1su;)LX/6k4;
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1134626
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v5, v6

    move-object v4, v6

    move-object v3, v6

    move-object v2, v6

    move-object v1, v6

    .line 1134627
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1134628
    iget-byte v8, v0, LX/1sw;->b:B

    if-eqz v8, :cond_9

    .line 1134629
    iget-short v8, v0, LX/1sw;->c:S

    packed-switch v8, :pswitch_data_0

    .line 1134630
    :pswitch_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1134631
    :pswitch_1
    iget-byte v8, v0, LX/1sw;->b:B

    const/16 v9, 0xc

    if-ne v8, v9, :cond_1

    .line 1134632
    invoke-static {p0}, LX/6kn;->b(LX/1su;)LX/6kn;

    move-result-object v1

    goto :goto_0

    .line 1134633
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1134634
    :pswitch_2
    iget-byte v8, v0, LX/1sw;->b:B

    const/16 v9, 0xb

    if-ne v8, v9, :cond_2

    .line 1134635
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1134636
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1134637
    :pswitch_3
    iget-byte v8, v0, LX/1sw;->b:B

    const/16 v9, 0xa

    if-ne v8, v9, :cond_3

    .line 1134638
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    .line 1134639
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1134640
    :pswitch_4
    iget-byte v8, v0, LX/1sw;->b:B

    const/16 v9, 0xf

    if-ne v8, v9, :cond_5

    .line 1134641
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v8

    .line 1134642
    new-instance v4, Ljava/util/ArrayList;

    iget v0, v8, LX/1u3;->b:I

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v7

    .line 1134643
    :goto_1
    iget v9, v8, LX/1u3;->b:I

    if-gez v9, :cond_4

    invoke-static {}, LX/1su;->t()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1134644
    :goto_2
    invoke-static {p0}, LX/6jZ;->b(LX/1su;)LX/6jZ;

    move-result-object v9

    .line 1134645
    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1134646
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1134647
    :cond_4
    iget v9, v8, LX/1u3;->b:I

    if-ge v0, v9, :cond_0

    goto :goto_2

    .line 1134648
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1134649
    :pswitch_5
    iget-byte v8, v0, LX/1sw;->b:B

    const/16 v9, 0x8

    if-ne v8, v9, :cond_6

    .line 1134650
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/16 :goto_0

    .line 1134651
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1134652
    :pswitch_6
    iget-byte v8, v0, LX/1sw;->b:B

    const/16 v9, 0xd

    if-ne v8, v9, :cond_8

    .line 1134653
    invoke-virtual {p0}, LX/1su;->g()LX/7H3;

    move-result-object v8

    .line 1134654
    new-instance v6, Ljava/util/HashMap;

    iget v0, v8, LX/7H3;->c:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/HashMap;-><init>(I)V

    move v0, v7

    .line 1134655
    :goto_3
    iget v9, v8, LX/7H3;->c:I

    if-gez v9, :cond_7

    invoke-static {}, LX/1su;->s()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1134656
    :goto_4
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v9

    .line 1134657
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v10

    .line 1134658
    invoke-interface {v6, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1134659
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1134660
    :cond_7
    iget v9, v8, LX/7H3;->c:I

    if-ge v0, v9, :cond_0

    goto :goto_4

    .line 1134661
    :cond_8
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1134662
    :cond_9
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1134663
    new-instance v0, LX/6k4;

    invoke-direct/range {v0 .. v6}, LX/6k4;-><init>(LX/6kn;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Integer;Ljava/util/Map;)V

    .line 1134664
    invoke-direct {v0}, LX/6k4;->a()V

    .line 1134665
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1134555
    if-eqz p2, :cond_5

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1134556
    :goto_0
    if-eqz p2, :cond_6

    const-string v0, "\n"

    move-object v2, v0

    .line 1134557
    :goto_1
    if-eqz p2, :cond_7

    const-string v0, " "

    move-object v1, v0

    .line 1134558
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "DeltaNewMessage"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1134559
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134560
    const-string v0, "("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134561
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134562
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134563
    const-string v0, "messageMetadata"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134564
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134565
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134566
    iget-object v0, p0, LX/6k4;->messageMetadata:LX/6kn;

    if-nez v0, :cond_8

    .line 1134567
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134568
    :goto_3
    iget-object v0, p0, LX/6k4;->body:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1134569
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134570
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134571
    const-string v0, "body"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134572
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134573
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134574
    iget-object v0, p0, LX/6k4;->body:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 1134575
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134576
    :cond_0
    :goto_4
    iget-object v0, p0, LX/6k4;->stickerId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1134577
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134578
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134579
    const-string v0, "stickerId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134580
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134581
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134582
    iget-object v0, p0, LX/6k4;->stickerId:Ljava/lang/Long;

    if-nez v0, :cond_a

    .line 1134583
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134584
    :cond_1
    :goto_5
    iget-object v0, p0, LX/6k4;->attachments:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 1134585
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134586
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134587
    const-string v0, "attachments"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134588
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134589
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134590
    iget-object v0, p0, LX/6k4;->attachments:Ljava/util/List;

    if-nez v0, :cond_b

    .line 1134591
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134592
    :cond_2
    :goto_6
    iget-object v0, p0, LX/6k4;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1134593
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134594
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134595
    const-string v0, "ttl"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134596
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134597
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134598
    iget-object v0, p0, LX/6k4;->ttl:Ljava/lang/Integer;

    if-nez v0, :cond_c

    .line 1134599
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134600
    :cond_3
    :goto_7
    iget-object v0, p0, LX/6k4;->data:Ljava/util/Map;

    if-eqz v0, :cond_4

    .line 1134601
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134602
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134603
    const-string v0, "data"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134604
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134605
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134606
    iget-object v0, p0, LX/6k4;->data:Ljava/util/Map;

    if-nez v0, :cond_e

    .line 1134607
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134608
    :cond_4
    :goto_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134609
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134610
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1134611
    :cond_5
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1134612
    :cond_6
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1134613
    :cond_7
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 1134614
    :cond_8
    iget-object v0, p0, LX/6k4;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1134615
    :cond_9
    iget-object v0, p0, LX/6k4;->body:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1134616
    :cond_a
    iget-object v0, p0, LX/6k4;->stickerId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1134617
    :cond_b
    iget-object v0, p0, LX/6k4;->attachments:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1134618
    :cond_c
    sget-object v0, LX/6kk;->b:Ljava/util/Map;

    iget-object v5, p0, LX/6k4;->ttl:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1134619
    if-eqz v0, :cond_d

    .line 1134620
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134621
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134622
    :cond_d
    iget-object v5, p0, LX/6k4;->ttl:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1134623
    if-eqz v0, :cond_3

    .line 1134624
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1134625
    :cond_e
    iget-object v0, p0, LX/6k4;->data:Ljava/util/Map;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8
.end method

.method public final a(LX/1su;)V
    .locals 4

    .prologue
    const/16 v3, 0xb

    .line 1134522
    invoke-direct {p0}, LX/6k4;->a()V

    .line 1134523
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1134524
    iget-object v0, p0, LX/6k4;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1134525
    sget-object v0, LX/6k4;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134526
    iget-object v0, p0, LX/6k4;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1134527
    :cond_0
    iget-object v0, p0, LX/6k4;->body:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1134528
    iget-object v0, p0, LX/6k4;->body:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1134529
    sget-object v0, LX/6k4;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134530
    iget-object v0, p0, LX/6k4;->body:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1134531
    :cond_1
    iget-object v0, p0, LX/6k4;->stickerId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1134532
    iget-object v0, p0, LX/6k4;->stickerId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1134533
    sget-object v0, LX/6k4;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134534
    iget-object v0, p0, LX/6k4;->stickerId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1134535
    :cond_2
    iget-object v0, p0, LX/6k4;->attachments:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 1134536
    iget-object v0, p0, LX/6k4;->attachments:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 1134537
    sget-object v0, LX/6k4;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134538
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/6k4;->attachments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1134539
    iget-object v0, p0, LX/6k4;->attachments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6jZ;

    .line 1134540
    invoke-virtual {v0, p1}, LX/6jZ;->a(LX/1su;)V

    goto :goto_0

    .line 1134541
    :cond_3
    iget-object v0, p0, LX/6k4;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1134542
    iget-object v0, p0, LX/6k4;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1134543
    sget-object v0, LX/6k4;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134544
    iget-object v0, p0, LX/6k4;->ttl:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1134545
    :cond_4
    iget-object v0, p0, LX/6k4;->data:Ljava/util/Map;

    if-eqz v0, :cond_5

    .line 1134546
    iget-object v0, p0, LX/6k4;->data:Ljava/util/Map;

    if-eqz v0, :cond_5

    .line 1134547
    sget-object v0, LX/6k4;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1134548
    new-instance v0, LX/7H3;

    iget-object v1, p0, LX/6k4;->data:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v3, v3, v1}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 1134549
    iget-object v0, p0, LX/6k4;->data:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1134550
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/1su;->a(Ljava/lang/String;)V

    .line 1134551
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 1134552
    :cond_5
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1134553
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1134554
    return-void
.end method

.method public final a(LX/6k4;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1134469
    if-nez p1, :cond_1

    .line 1134470
    :cond_0
    :goto_0
    return v2

    .line 1134471
    :cond_1
    iget-object v0, p0, LX/6k4;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1134472
    :goto_1
    iget-object v3, p1, LX/6k4;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1134473
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1134474
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1134475
    iget-object v0, p0, LX/6k4;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6k4;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1134476
    :cond_3
    iget-object v0, p0, LX/6k4;->body:Ljava/lang/String;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1134477
    :goto_3
    iget-object v3, p1, LX/6k4;->body:Ljava/lang/String;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1134478
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1134479
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1134480
    iget-object v0, p0, LX/6k4;->body:Ljava/lang/String;

    iget-object v3, p1, LX/6k4;->body:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1134481
    :cond_5
    iget-object v0, p0, LX/6k4;->stickerId:Ljava/lang/Long;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1134482
    :goto_5
    iget-object v3, p1, LX/6k4;->stickerId:Ljava/lang/Long;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1134483
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1134484
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1134485
    iget-object v0, p0, LX/6k4;->stickerId:Ljava/lang/Long;

    iget-object v3, p1, LX/6k4;->stickerId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1134486
    :cond_7
    iget-object v0, p0, LX/6k4;->attachments:Ljava/util/List;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1134487
    :goto_7
    iget-object v3, p1, LX/6k4;->attachments:Ljava/util/List;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1134488
    :goto_8
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1134489
    :cond_8
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1134490
    iget-object v0, p0, LX/6k4;->attachments:Ljava/util/List;

    iget-object v3, p1, LX/6k4;->attachments:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1134491
    :cond_9
    iget-object v0, p0, LX/6k4;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1134492
    :goto_9
    iget-object v3, p1, LX/6k4;->ttl:Ljava/lang/Integer;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1134493
    :goto_a
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1134494
    :cond_a
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1134495
    iget-object v0, p0, LX/6k4;->ttl:Ljava/lang/Integer;

    iget-object v3, p1, LX/6k4;->ttl:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1134496
    :cond_b
    iget-object v0, p0, LX/6k4;->data:Ljava/util/Map;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1134497
    :goto_b
    iget-object v3, p1, LX/6k4;->data:Ljava/util/Map;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1134498
    :goto_c
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1134499
    :cond_c
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1134500
    iget-object v0, p0, LX/6k4;->data:Ljava/util/Map;

    iget-object v3, p1, LX/6k4;->data:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_d
    move v2, v1

    .line 1134501
    goto/16 :goto_0

    :cond_e
    move v0, v2

    .line 1134502
    goto/16 :goto_1

    :cond_f
    move v3, v2

    .line 1134503
    goto/16 :goto_2

    :cond_10
    move v0, v2

    .line 1134504
    goto/16 :goto_3

    :cond_11
    move v3, v2

    .line 1134505
    goto/16 :goto_4

    :cond_12
    move v0, v2

    .line 1134506
    goto :goto_5

    :cond_13
    move v3, v2

    .line 1134507
    goto :goto_6

    :cond_14
    move v0, v2

    .line 1134508
    goto :goto_7

    :cond_15
    move v3, v2

    .line 1134509
    goto :goto_8

    :cond_16
    move v0, v2

    .line 1134510
    goto :goto_9

    :cond_17
    move v3, v2

    .line 1134511
    goto :goto_a

    :cond_18
    move v0, v2

    .line 1134512
    goto :goto_b

    :cond_19
    move v3, v2

    .line 1134513
    goto :goto_c
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1134518
    if-nez p1, :cond_1

    .line 1134519
    :cond_0
    :goto_0
    return v0

    .line 1134520
    :cond_1
    instance-of v1, p1, LX/6k4;

    if-eqz v1, :cond_0

    .line 1134521
    check-cast p1, LX/6k4;

    invoke-virtual {p0, p1}, LX/6k4;->a(LX/6k4;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1134517
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1134514
    sget-boolean v0, LX/6k4;->a:Z

    .line 1134515
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6k4;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1134516
    return-object v0
.end method
