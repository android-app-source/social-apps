.class public LX/6jQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1130690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/6jM;Lcom/facebook/messaging/model/messages/ParticipantInfo;LX/0Px;JZ)D
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6jM;",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;JZ)D"
        }
    .end annotation

    .prologue
    .line 1130685
    const-wide/16 v0, 0x0

    .line 1130686
    iget-object v2, p0, LX/6jM;->f:LX/6jO;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/6jM;->f:LX/6jO;

    iget-wide v2, v2, LX/6jO;->d:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 1130687
    iget-object v0, p0, LX/6jM;->f:LX/6jO;

    iget-wide v0, v0, LX/6jO;->c:D

    iget-object v2, p0, LX/6jM;->f:LX/6jO;

    iget-wide v2, v2, LX/6jO;->d:J

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, LX/6jP;->a(DJJ)D

    move-result-wide v0

    .line 1130688
    :cond_0
    invoke-static/range {p0 .. p5}, LX/6jQ;->b(LX/6jM;Lcom/facebook/messaging/model/messages/ParticipantInfo;LX/0Px;JZ)D

    move-result-wide v2

    add-double/2addr v0, v2

    .line 1130689
    return-wide v0
.end method

.method private static a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/ParticipantInfo;Z)D
    .locals 2

    .prologue
    .line 1130682
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/model/messages/ParticipantInfo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1130683
    const-wide/high16 v0, 0x4008000000000000L    # 3.0

    .line 1130684
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    goto :goto_0
.end method

.method private static b(LX/6jM;Lcom/facebook/messaging/model/messages/ParticipantInfo;LX/0Px;JZ)D
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6jM;",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;JZ)D"
        }
    .end annotation

    .prologue
    .line 1130665
    const-wide/16 v12, 0x0

    .line 1130666
    const-wide/16 v10, 0x0

    .line 1130667
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6jM;->f:LX/6jO;

    if-nez v2, :cond_0

    const-wide/16 v2, -0x1

    move-wide v8, v2

    .line 1130668
    :goto_0
    const-wide/16 v6, -0x1

    .line 1130669
    invoke-virtual/range {p2 .. p2}, LX/0Px;->size()I

    move-result v17

    const/4 v2, 0x0

    move/from16 v16, v2

    :goto_1
    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_4

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/facebook/messaging/model/messages/Message;

    .line 1130670
    iget-wide v2, v4, Lcom/facebook/messaging/model/messages/Message;->c:J

    cmp-long v2, v2, v8

    if-ltz v2, :cond_3

    iget-wide v2, v4, Lcom/facebook/messaging/model/messages/Message;->c:J

    cmp-long v2, v2, p3

    if-gtz v2, :cond_3

    move-wide v14, v12

    move-wide v12, v10

    move-wide v10, v6

    .line 1130671
    :goto_2
    iget-wide v2, v4, Lcom/facebook/messaging/model/messages/Message;->c:J

    cmp-long v2, v2, v10

    if-lez v2, :cond_2

    .line 1130672
    const-wide/high16 v2, 0x403e000000000000L    # 30.0

    invoke-static {v12, v13, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    add-double v12, v14, v2

    .line 1130673
    const-wide/16 v6, 0x0

    .line 1130674
    const-wide/16 v2, -0x1

    cmp-long v2, v10, v2

    if-nez v2, :cond_1

    .line 1130675
    iget-wide v2, v4, Lcom/facebook/messaging/model/messages/Message;->c:J

    const-wide/32 v10, 0x5265c00

    add-long/2addr v2, v10

    move-wide v10, v2

    move-wide v14, v12

    move-wide v12, v6

    goto :goto_2

    .line 1130676
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6jM;->f:LX/6jO;

    iget-wide v2, v2, LX/6jO;->d:J

    move-wide v8, v2

    goto :goto_0

    .line 1130677
    :cond_1
    const-wide/32 v2, 0x5265c00

    add-long/2addr v2, v10

    move-wide v10, v2

    move-wide v14, v12

    move-wide v12, v6

    goto :goto_2

    .line 1130678
    :cond_2
    move-object/from16 v0, p1

    move/from16 v1, p5

    invoke-static {v4, v0, v1}, LX/6jQ;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/ParticipantInfo;Z)D

    move-result-wide v2

    iget-wide v4, v4, Lcom/facebook/messaging/model/messages/Message;->c:J

    move-wide/from16 v6, p3

    invoke-static/range {v2 .. v7}, LX/6jP;->a(DJJ)D

    move-result-wide v2

    add-double/2addr v2, v12

    move-wide v6, v10

    move-wide v12, v14

    move-wide v10, v2

    .line 1130679
    :cond_3
    add-int/lit8 v2, v16, 0x1

    move/from16 v16, v2

    goto :goto_1

    .line 1130680
    :cond_4
    const-wide/high16 v2, 0x403e000000000000L    # 30.0

    invoke-static {v10, v11, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    add-double/2addr v2, v12

    .line 1130681
    return-wide v2
.end method
