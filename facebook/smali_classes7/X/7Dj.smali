.class public final enum LX/7Dj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Dj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Dj;

.field public static final enum COMPOSER:LX/7Dj;

.field public static final enum NEWSFEED:LX/7Dj;

.field public static final enum OTHER:LX/7Dj;

.field public static final enum PHOTO_VIEWER:LX/7Dj;

.field public static final enum TIMELINE:LX/7Dj;

.field public static final enum UNKNOWN:LX/7Dj;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1183552
    new-instance v0, LX/7Dj;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v4, v2}, LX/7Dj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Dj;->UNKNOWN:LX/7Dj;

    .line 1183553
    new-instance v0, LX/7Dj;

    const-string v1, "NEWSFEED"

    const-string v2, "newsfeed"

    invoke-direct {v0, v1, v5, v2}, LX/7Dj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Dj;->NEWSFEED:LX/7Dj;

    .line 1183554
    new-instance v0, LX/7Dj;

    const-string v1, "TIMELINE"

    const-string v2, "timeline"

    invoke-direct {v0, v1, v6, v2}, LX/7Dj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Dj;->TIMELINE:LX/7Dj;

    .line 1183555
    new-instance v0, LX/7Dj;

    const-string v1, "PHOTO_VIEWER"

    const-string v2, "photo_viewer"

    invoke-direct {v0, v1, v7, v2}, LX/7Dj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Dj;->PHOTO_VIEWER:LX/7Dj;

    .line 1183556
    new-instance v0, LX/7Dj;

    const-string v1, "COMPOSER"

    const-string v2, "composer"

    invoke-direct {v0, v1, v8, v2}, LX/7Dj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Dj;->COMPOSER:LX/7Dj;

    .line 1183557
    new-instance v0, LX/7Dj;

    const-string v1, "OTHER"

    const/4 v2, 0x5

    const-string v3, "other"

    invoke-direct {v0, v1, v2, v3}, LX/7Dj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Dj;->OTHER:LX/7Dj;

    .line 1183558
    const/4 v0, 0x6

    new-array v0, v0, [LX/7Dj;

    sget-object v1, LX/7Dj;->UNKNOWN:LX/7Dj;

    aput-object v1, v0, v4

    sget-object v1, LX/7Dj;->NEWSFEED:LX/7Dj;

    aput-object v1, v0, v5

    sget-object v1, LX/7Dj;->TIMELINE:LX/7Dj;

    aput-object v1, v0, v6

    sget-object v1, LX/7Dj;->PHOTO_VIEWER:LX/7Dj;

    aput-object v1, v0, v7

    sget-object v1, LX/7Dj;->COMPOSER:LX/7Dj;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7Dj;->OTHER:LX/7Dj;

    aput-object v2, v0, v1

    sput-object v0, LX/7Dj;->$VALUES:[LX/7Dj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1183561
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1183562
    iput-object p3, p0, LX/7Dj;->value:Ljava/lang/String;

    .line 1183563
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Dj;
    .locals 1

    .prologue
    .line 1183560
    const-class v0, LX/7Dj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Dj;

    return-object v0
.end method

.method public static values()[LX/7Dj;
    .locals 1

    .prologue
    .line 1183559
    sget-object v0, LX/7Dj;->$VALUES:[LX/7Dj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Dj;

    return-object v0
.end method
