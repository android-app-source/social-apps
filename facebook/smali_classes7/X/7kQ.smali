.class public final LX/7kQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1231764
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1231765
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1231766
    :goto_0
    return v1

    .line 1231767
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1231768
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 1231769
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1231770
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1231771
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1231772
    const-string v7, "commerce_merchant_settings"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1231773
    invoke-static {p0, p1}, LX/7kO;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1231774
    :cond_2
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1231775
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1231776
    :cond_3
    const-string v7, "intro_summary"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1231777
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1231778
    :cond_4
    const-string v7, "ordered_collections"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1231779
    invoke-static {p0, p1}, LX/7jn;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1231780
    :cond_5
    const-string v7, "page"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1231781
    invoke-static {p0, p1}, LX/7kP;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1231782
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1231783
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1231784
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1231785
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1231786
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1231787
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1231788
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1231789
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1231790
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1231791
    if-eqz v0, :cond_0

    .line 1231792
    const-string v1, "commerce_merchant_settings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1231793
    invoke-static {p0, v0, p2, p3}, LX/7kO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1231794
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1231795
    if-eqz v0, :cond_1

    .line 1231796
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1231797
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1231798
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1231799
    if-eqz v0, :cond_2

    .line 1231800
    const-string v1, "intro_summary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1231801
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1231802
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1231803
    if-eqz v0, :cond_3

    .line 1231804
    const-string v1, "ordered_collections"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1231805
    invoke-static {p0, v0, p2, p3}, LX/7jn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1231806
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1231807
    if-eqz v0, :cond_4

    .line 1231808
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1231809
    invoke-static {p0, v0, p2}, LX/7kP;->a(LX/15i;ILX/0nX;)V

    .line 1231810
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1231811
    return-void
.end method
