.class public LX/6qh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1151028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LX/73T;)V
    .locals 2

    .prologue
    .line 1151027
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not needed in simple callback!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1151026
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not needed in simple callback!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 1151025
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not needed in simple callback!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V
    .locals 2

    .prologue
    .line 1151024
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not needed in simple callback!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lcom/google/common/util/concurrent/ListenableFuture;Z)V
    .locals 2

    .prologue
    .line 1151023
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not needed in simple callback!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1151021
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not needed in simple callback!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 1151022
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not needed in simple callback!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
