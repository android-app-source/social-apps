.class public LX/7m9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/composer/publish/common/EditPostParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0lB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1236306
    const-class v0, LX/7m9;

    sput-object v0, LX/7m9;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1236307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236308
    iput-object p1, p0, LX/7m9;->b:LX/0lB;

    .line 1236309
    return-void
.end method

.method public static a(LX/0QB;)LX/7m9;
    .locals 1

    .prologue
    .line 1236310
    invoke-static {p0}, LX/7m9;->b(LX/0QB;)LX/7m9;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/7m9;
    .locals 2

    .prologue
    .line 1236311
    new-instance v1, LX/7m9;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lB;

    invoke-direct {v1, v0}, LX/7m9;-><init>(LX/0lB;)V

    .line 1236312
    return-object v1
.end method

.method private b(Lcom/facebook/composer/publish/common/EditPostParams;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/publish/common/EditPostParams;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1236313
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1236314
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "message"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getMessage()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {v3}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236315
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236316
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getTaggedIds()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1236317
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tags"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getTaggedIds()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/7mB;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236318
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getPlaceTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1236319
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "place"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getPlaceTag()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236320
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getPrivacy()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1236321
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "privacy"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getPrivacy()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236322
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getLinkEdit()Lcom/facebook/composer/publish/common/LinkEdit;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1236323
    :try_start_0
    iget-object v1, p0, LX/7m9;->b:LX/0lB;

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getLinkEdit()Lcom/facebook/composer/publish/common/LinkEdit;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1236324
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "link_edit"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 1236325
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getStickerEdit()Lcom/facebook/composer/publish/common/StickerEdit;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1236326
    :try_start_1
    iget-object v1, p0, LX/7m9;->b:LX/0lB;

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getStickerEdit()Lcom/facebook/composer/publish/common/StickerEdit;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1236327
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "sticker_edit"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch LX/28F; {:try_start_1 .. :try_end_1} :catch_1

    .line 1236328
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->hasMediaFbIds()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1236329
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "attached_media"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getMediaFbIds()LX/0Px;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getMediaCaptions()LX/0Px;

    move-result-object v4

    .line 1236330
    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, LX/7mB;->a(LX/0Px;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v5

    move-object v3, v5

    .line 1236331
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236332
    :cond_5
    :try_start_2
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1236333
    iget-object v1, p0, LX/7m9;->b:LX/0lB;

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1236334
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "product_item"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch LX/28F; {:try_start_2 .. :try_end_2} :catch_2

    .line 1236335
    :cond_6
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1236336
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getMinutiaeTag()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v1

    if-nez v1, :cond_d

    move-object v1, v2

    .line 1236337
    :goto_0
    move-object v1, v1

    .line 1236338
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1236339
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->shouldPublishUnpublishedContent()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1236340
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_published"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236341
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getSourceType()LX/21D;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 1236342
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "source_type"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getSourceType()LX/21D;

    move-result-object v3

    invoke-virtual {v3}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236343
    :cond_8
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    sget-object v2, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {v1, v2}, LX/87Y;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 1236344
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getPresetId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1236345
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "text_format_preset_id"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getPresetId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236346
    :goto_1
    return-object v0

    .line 1236347
    :catch_0
    move-exception v0

    .line 1236348
    sget-object v1, LX/7m9;->a:Ljava/lang/Class;

    const-string v2, "Link Edit JSON Serialization failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1236349
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1236350
    :catch_1
    move-exception v0

    .line 1236351
    sget-object v1, LX/7m9;->a:Ljava/lang/Class;

    const-string v2, "Sticker Edit JSON Serialization failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1236352
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1236353
    :catch_2
    move-exception v0

    .line 1236354
    sget-object v1, LX/7m9;->a:Ljava/lang/Class;

    const-string v2, "Product Item JSON Serialization failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1236355
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1236356
    :cond_9
    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-virtual {v1}, LX/0mC;->c()LX/0m9;

    move-result-object v1

    .line 1236357
    const-string v2, "color"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getColor()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, LX/5MA;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236358
    const-string v2, "background_color"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, LX/5MA;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236359
    const-string v2, "text_align"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getTextAlign()LX/5RS;

    move-result-object v3

    invoke-virtual {v3}, LX/5RS;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236360
    const-string v2, "font_weight"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v3

    invoke-virtual {v3}, LX/5RY;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236361
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 1236362
    const-string v2, "background_image_name"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236363
    :cond_a
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 1236364
    const-string v2, "background_gradient_color"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236365
    const-string v2, "background_gradient_direction"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientDirection()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236366
    :cond_b
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "text_format_metadata"

    invoke-virtual {v1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1236367
    :cond_c
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "text_format_preset_id"

    const-string v3, "0"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1236368
    :cond_d
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getMinutiaeTag()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v3

    .line 1236369
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "og_action_type_id"

    iget-object v5, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogActionTypeId:Ljava/lang/String;

    invoke-direct {v1, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236370
    sget-object v1, Lcom/facebook/ipc/composer/model/MinutiaeTag;->b:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-virtual {v3, v1}, Lcom/facebook/ipc/composer/model/MinutiaeTag;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    move-object v1, v2

    .line 1236371
    goto/16 :goto_0

    .line 1236372
    :cond_e
    const-string v1, "0"

    iget-object v4, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogActionTypeId:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1236373
    iget-object v1, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogObjectId:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 1236374
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "og_object_id"

    iget-object v5, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogObjectId:Ljava/lang/String;

    invoke-direct {v1, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236375
    :cond_f
    iget-object v1, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogPhrase:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 1236376
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "og_phrase"

    iget-object v5, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogPhrase:Ljava/lang/String;

    invoke-direct {v1, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236377
    :cond_10
    iget-object v1, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogIconId:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 1236378
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "og_icon_id"

    iget-object v5, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogIconId:Ljava/lang/String;

    invoke-direct {v1, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236379
    :cond_11
    iget-object v1, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogSuggestionMechanism:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 1236380
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "og_suggestion_mechanism"

    iget-object v5, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogSuggestionMechanism:Ljava/lang/String;

    invoke-direct {v1, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236381
    :cond_12
    iget-boolean v1, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogHideAttachment:Z

    if-eqz v1, :cond_13

    .line 1236382
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "og_hide_object_attachment"

    const-string v4, "true"

    invoke-direct {v1, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_13
    move-object v1, v2

    .line 1236383
    goto/16 :goto_0

    .line 1236384
    :cond_14
    const/4 v1, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1236385
    check-cast p1, Lcom/facebook/composer/publish/common/EditPostParams;

    .line 1236386
    invoke-direct {p0, p1}, LX/7m9;->b(Lcom/facebook/composer/publish/common/EditPostParams;)Ljava/util/List;

    move-result-object v4

    .line 1236387
    new-instance v0, LX/14N;

    const-string v1, "editPost"

    const-string v2, "POST"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getLegacyStoryApiId()Ljava/lang/String;

    move-result-object v3

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1236388
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1236389
    const/4 v0, 0x0

    return-object v0
.end method
