.class public final LX/7wb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V
    .locals 0

    .prologue
    .line 1275995
    iput-object p1, p0, LX/7wb;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x2

    const v0, -0x31f83d72

    invoke-static {v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1275996
    iget-object v1, p0, LX/7wb;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->v:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1275997
    iget-object v1, p0, LX/7wb;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->m:LX/7wG;

    iget-object v2, p0, LX/7wb;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    iget-object v2, v2, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-interface {v1, v2}, LX/7wG;->b(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V

    .line 1275998
    const v1, -0x7595cacb

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1275999
    :goto_0
    return-void

    .line 1276000
    :cond_0
    iget-object v1, p0, LX/7wb;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    iget-boolean v1, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->o:Z

    if-nez v1, :cond_2

    .line 1276001
    iget-object v1, p0, LX/7wb;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    .line 1276002
    iput-boolean v2, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->o:Z

    .line 1276003
    iget-object v1, p0, LX/7wb;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    .line 1276004
    iget-object v2, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-virtual {v2}, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a()LX/7wo;

    move-result-object v2

    sget-object v3, LX/7wp;->CHECKOUT:LX/7wp;

    invoke-interface {v2, v3}, LX/7wo;->a(LX/7wp;)LX/7wo;

    move-result-object v2

    invoke-interface {v2}, LX/7wo;->a()Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1276005
    iget-object v2, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->a:LX/1nQ;

    iget-object v3, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->i:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    iget-object v4, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget v4, v4, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->E:I

    .line 1276006
    iget-object v5, v2, LX/1nQ;->i:LX/0Zb;

    const-string p0, "event_buy_tickets_continue_button_tapped"

    const/4 p1, 0x0

    invoke-interface {v5, p0, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v5

    .line 1276007
    invoke-virtual {v5}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1276008
    const-string p0, "event_ticketing"

    invoke-virtual {v5, p0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    iget-object p0, v2, LX/1nQ;->j:LX/0kv;

    iget-object p1, v2, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {p0, p1}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string p0, "Event"

    invoke-virtual {v5, p0}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    iget-object p0, v3, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->b:Ljava/lang/String;

    invoke-virtual {v5, p0}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string p0, "event_id"

    iget-object p1, v3, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->b:Ljava/lang/String;

    invoke-virtual {v5, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string p0, "session_id"

    iget-object p1, v3, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->a:Ljava/lang/String;

    invoke-virtual {v5, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string p0, "purchased_tickets_count"

    invoke-virtual {v5, p0, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v5

    invoke-virtual {v5}, LX/0oG;->d()V

    .line 1276009
    :cond_1
    iget-object v2, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->m:LX/7wG;

    iget-object v3, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-interface {v2, v3}, LX/7wG;->a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V

    .line 1276010
    :cond_2
    const v1, -0x616678e8

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
