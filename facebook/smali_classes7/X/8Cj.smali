.class public final LX/8Cj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:I

.field public final d:LX/4Ab;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;Landroid/view/View$OnClickListener;I)V
    .locals 7
    .param p2    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "I)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1311674
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, LX/8Cj;-><init>(LX/0Px;Landroid/view/View$OnClickListener;ILX/4Ab;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1311675
    return-void
.end method

.method public constructor <init>(LX/0Px;Landroid/view/View$OnClickListener;ILX/4Ab;)V
    .locals 7
    .param p2    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "I",
            "LX/4Ab;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1311676
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, LX/8Cj;-><init>(LX/0Px;Landroid/view/View$OnClickListener;ILX/4Ab;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1311677
    return-void
.end method

.method public constructor <init>(LX/0Px;Landroid/view/View$OnClickListener;ILX/4Ab;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "I",
            "LX/4Ab;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1311678
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, LX/8Cj;-><init>(LX/0Px;Landroid/view/View$OnClickListener;ILX/4Ab;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/graphics/drawable/Drawable;)V

    .line 1311679
    return-void
.end method

.method private constructor <init>(LX/0Px;Landroid/view/View$OnClickListener;ILX/4Ab;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "I",
            "LX/4Ab;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Landroid/graphics/drawable/Drawable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1311680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1311681
    iput-object p1, p0, LX/8Cj;->a:LX/0Px;

    .line 1311682
    iput-object p2, p0, LX/8Cj;->b:Landroid/view/View$OnClickListener;

    .line 1311683
    iput p3, p0, LX/8Cj;->c:I

    .line 1311684
    iput-object p4, p0, LX/8Cj;->d:LX/4Ab;

    .line 1311685
    iput-object p5, p0, LX/8Cj;->e:Ljava/lang/Integer;

    .line 1311686
    iput-object p6, p0, LX/8Cj;->f:Ljava/lang/Integer;

    .line 1311687
    iput-object p7, p0, LX/8Cj;->g:Landroid/graphics/drawable/Drawable;

    .line 1311688
    return-void
.end method
