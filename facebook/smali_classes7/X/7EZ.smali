.class public LX/7EZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1184834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184835
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1184836
    check-cast p1, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;

    .line 1184837
    iget-object v0, p1, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->d:LX/0P1;

    move-object v0, v0

    .line 1184838
    new-instance v3, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 1184839
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1184840
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1184841
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;

    .line 1184842
    new-instance v5, LX/162;

    sget-object v6, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v6}, LX/162;-><init>(LX/0mC;)V

    .line 1184843
    invoke-virtual {v1}, Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1184844
    invoke-virtual {v5, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_1

    .line 1184845
    :cond_0
    invoke-virtual {v3, v2, v5}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_0

    .line 1184846
    :cond_1
    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1184847
    iget-object v1, p1, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->e:LX/0Px;

    move-object v1, v1

    .line 1184848
    new-instance v3, LX/162;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v2}, LX/162;-><init>(LX/0mC;)V

    .line 1184849
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;

    .line 1184850
    new-instance v5, LX/162;

    sget-object v6, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v6}, LX/162;-><init>(LX/0mC;)V

    .line 1184851
    invoke-virtual {v2}, Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1184852
    invoke-virtual {v5, v2}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_3

    .line 1184853
    :cond_2
    invoke-virtual {v3, v5}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_2

    .line 1184854
    :cond_3
    invoke-virtual {v3}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1184855
    iget-object v2, p1, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->f:LX/0Px;

    move-object v2, v2

    .line 1184856
    new-instance v4, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v3}, LX/162;-><init>(LX/0mC;)V

    .line 1184857
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 1184858
    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_4

    .line 1184859
    :cond_4
    invoke-virtual {v4}, LX/162;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 1184860
    iget-object v3, p1, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->g:LX/0P1;

    move-object v3, v3

    .line 1184861
    new-instance v6, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 1184862
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_5
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 1184863
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v6, v5, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_5

    .line 1184864
    :cond_5
    invoke-virtual {v6}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 1184865
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 1184866
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "answers"

    invoke-direct {v4, v6, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184867
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "pages"

    invoke-direct {v0, v4, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184868
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "session_blob"

    .line 1184869
    iget-object v4, p1, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1184870
    invoke-direct {v0, v1, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184871
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "page_numbers"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184872
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "context"

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184873
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "disable_event_logging"

    .line 1184874
    iget-boolean v2, p1, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->c:Z

    move v2, v2

    .line 1184875
    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184876
    new-instance v0, LX/14N;

    const-string v1, "postResponse"

    const-string v2, "POST"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1184877
    iget-object v4, p1, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1184878
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/responses"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1184879
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1184880
    const/4 v0, 0x0

    return-object v0
.end method
