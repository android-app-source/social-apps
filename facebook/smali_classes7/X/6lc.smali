.class public final LX/6lc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/6li;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/6li;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1142763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142764
    iput-object p1, p0, LX/6lc;->a:LX/0QB;

    .line 1142765
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/6li;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1142766
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/6lc;

    invoke-direct {v2, p0}, LX/6lc;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1142767
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/6lc;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1142768
    packed-switch p2, :pswitch_data_0

    .line 1142769
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1142770
    :pswitch_0
    new-instance v0, LX/IdO;

    const/16 v1, 0x26d1

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x26cf

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/IdO;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1142771
    move-object v0, v0

    .line 1142772
    :goto_0
    return-object v0

    .line 1142773
    :pswitch_1
    new-instance v0, LX/IdS;

    const/16 v1, 0x26d4

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x26d2

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/IdS;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1142774
    move-object v0, v0

    .line 1142775
    goto :goto_0

    .line 1142776
    :pswitch_2
    new-instance v1, LX/FEp;

    const/16 v0, 0x278a

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 v0, 0x2788

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-static {p1}, LX/FEb;->b(LX/0QB;)LX/FEb;

    move-result-object v0

    check-cast v0, LX/FEb;

    invoke-direct {v1, p0, p2, v0}, LX/FEp;-><init>(LX/0Ot;LX/0Ot;LX/FEb;)V

    .line 1142777
    move-object v0, v1

    .line 1142778
    goto :goto_0

    .line 1142779
    :pswitch_3
    new-instance v0, LX/FEu;

    const/16 v1, 0x278d

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x278b

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/FEu;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1142780
    move-object v0, v0

    .line 1142781
    goto :goto_0

    .line 1142782
    :pswitch_4
    new-instance v0, LX/JnL;

    const/16 v1, 0x2976

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x27c2

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/JnL;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1142783
    move-object v0, v0

    .line 1142784
    goto :goto_0

    .line 1142785
    :pswitch_5
    new-instance v0, LX/FJH;

    const/16 v1, 0x2827

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x2825

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/FJH;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1142786
    move-object v0, v0

    .line 1142787
    goto :goto_0

    .line 1142788
    :pswitch_6
    new-instance v0, LX/FJU;

    const/16 v1, 0x2827

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x2825

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/FJU;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1142789
    move-object v0, v0

    .line 1142790
    goto :goto_0

    .line 1142791
    :pswitch_7
    new-instance v0, LX/DnZ;

    const/16 v1, 0x2901

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x28ff

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/DnZ;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1142792
    move-object v0, v0

    .line 1142793
    goto/16 :goto_0

    .line 1142794
    :pswitch_8
    new-instance v0, LX/Dnd;

    const/16 v1, 0x2904

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x2902

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/Dnd;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1142795
    move-object v0, v0

    .line 1142796
    goto/16 :goto_0

    .line 1142797
    :pswitch_9
    new-instance v0, LX/Dnw;

    const/16 v1, 0x2913

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x2914

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/Dnw;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1142798
    move-object v0, v0

    .line 1142799
    goto/16 :goto_0

    .line 1142800
    :pswitch_a
    new-instance v0, LX/FLm;

    const/16 v1, 0x2976

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x296e

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/FLm;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1142801
    move-object v0, v0

    .line 1142802
    goto/16 :goto_0

    .line 1142803
    :pswitch_b
    new-instance v0, LX/FLo;

    const/16 v1, 0x296d

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x296b

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/FLo;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1142804
    move-object v0, v0

    .line 1142805
    goto/16 :goto_0

    .line 1142806
    :pswitch_c
    new-instance v0, LX/FLs;

    const/16 v1, 0x2976

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x296e

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/FLs;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1142807
    move-object v0, v0

    .line 1142808
    goto/16 :goto_0

    .line 1142809
    :pswitch_d
    new-instance v0, LX/FLu;

    const/16 v1, 0x2972

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x2970

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/FLu;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1142810
    move-object v0, v0

    .line 1142811
    goto/16 :goto_0

    .line 1142812
    :pswitch_e
    new-instance v0, LX/FLx;

    const/16 v1, 0x296d

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x296b

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/FLx;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1142813
    move-object v0, v0

    .line 1142814
    goto/16 :goto_0

    .line 1142815
    :pswitch_f
    new-instance v1, LX/JsV;

    const/16 v0, 0x2a52

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 v0, 0x2a50

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-static {p1}, LX/JsT;->b(LX/0QB;)LX/JsT;

    move-result-object v0

    check-cast v0, LX/JsT;

    invoke-direct {v1, p0, p2, v0}, LX/JsV;-><init>(LX/0Ot;LX/0Ot;LX/JsT;)V

    .line 1142816
    move-object v0, v1

    .line 1142817
    goto/16 :goto_0

    .line 1142818
    :pswitch_10
    new-instance v1, LX/JsY;

    const/16 v0, 0x2a54

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {p1}, LX/JsT;->b(LX/0QB;)LX/JsT;

    move-result-object v0

    check-cast v0, LX/JsT;

    invoke-direct {v1, p0, v0}, LX/JsY;-><init>(LX/0Ot;LX/JsT;)V

    .line 1142819
    move-object v0, v1

    .line 1142820
    goto/16 :goto_0

    .line 1142821
    :pswitch_11
    new-instance v1, LX/Jsc;

    const/16 v0, 0x2a56

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 v0, 0x2a50

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-static {p1}, LX/JsT;->b(LX/0QB;)LX/JsT;

    move-result-object v0

    check-cast v0, LX/JsT;

    invoke-direct {v1, p0, p2, v0}, LX/Jsc;-><init>(LX/0Ot;LX/0Ot;LX/JsT;)V

    .line 1142822
    move-object v0, v1

    .line 1142823
    goto/16 :goto_0

    .line 1142824
    :pswitch_12
    new-instance v1, LX/Jsf;

    const/16 v0, 0x2a58

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {p1}, LX/JsT;->b(LX/0QB;)LX/JsT;

    move-result-object v0

    check-cast v0, LX/JsT;

    invoke-direct {v1, p0, v0}, LX/Jsf;-><init>(LX/0Ot;LX/JsT;)V

    .line 1142825
    move-object v0, v1

    .line 1142826
    goto/16 :goto_0

    .line 1142827
    :pswitch_13
    new-instance v1, LX/Jsj;

    const/16 v0, 0x2a5a

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {p1}, LX/JsT;->b(LX/0QB;)LX/JsT;

    move-result-object v0

    check-cast v0, LX/JsT;

    invoke-direct {v1, p0, v0}, LX/Jsj;-><init>(LX/0Ot;LX/JsT;)V

    .line 1142828
    move-object v0, v1

    .line 1142829
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1142830
    const/16 v0, 0x14

    return v0
.end method
