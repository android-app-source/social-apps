.class public final LX/8au;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 1369376
    const/16 v18, 0x0

    .line 1369377
    const/16 v17, 0x0

    .line 1369378
    const/16 v16, 0x0

    .line 1369379
    const/4 v15, 0x0

    .line 1369380
    const/4 v14, 0x0

    .line 1369381
    const/4 v13, 0x0

    .line 1369382
    const/4 v12, 0x0

    .line 1369383
    const/4 v11, 0x0

    .line 1369384
    const/4 v10, 0x0

    .line 1369385
    const/4 v9, 0x0

    .line 1369386
    const/4 v8, 0x0

    .line 1369387
    const/4 v7, 0x0

    .line 1369388
    const/4 v6, 0x0

    .line 1369389
    const/4 v5, 0x0

    .line 1369390
    const/4 v4, 0x0

    .line 1369391
    const/4 v3, 0x0

    .line 1369392
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 1369393
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1369394
    const/4 v3, 0x0

    .line 1369395
    :goto_0
    return v3

    .line 1369396
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1369397
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_f

    .line 1369398
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v19

    .line 1369399
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1369400
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    if-eqz v19, :cond_1

    .line 1369401
    const-string v20, "bar_configs"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 1369402
    invoke-static/range {p0 .. p1}, LX/8av;->b(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 1369403
    :cond_2
    const-string v20, "border_config"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 1369404
    invoke-static/range {p0 .. p1}, LX/8av;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 1369405
    :cond_3
    const-string v20, "byline_area_config"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 1369406
    invoke-static/range {p0 .. p1}, LX/8av;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 1369407
    :cond_4
    const-string v20, "byline_config"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 1369408
    invoke-static/range {p0 .. p1}, LX/8aw;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 1369409
    :cond_5
    const-string v20, "cover_image_config"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 1369410
    invoke-static/range {p0 .. p1}, LX/8av;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 1369411
    :cond_6
    const-string v20, "custom_fonts"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 1369412
    invoke-static/range {p0 .. p1}, LX/8a1;->b(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 1369413
    :cond_7
    const-string v20, "description_config"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 1369414
    invoke-static/range {p0 .. p1}, LX/8aw;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1369415
    :cond_8
    const-string v20, "device_family"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 1369416
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 1369417
    :cond_9
    const-string v20, "headline_config"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 1369418
    invoke-static/range {p0 .. p1}, LX/8aw;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1369419
    :cond_a
    const-string v20, "layout_spec_version"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 1369420
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 1369421
    :cond_b
    const-string v20, "page_id"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 1369422
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 1369423
    :cond_c
    const-string v20, "show_bottom_gradient"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 1369424
    const/4 v4, 0x1

    .line 1369425
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto/16 :goto_1

    .line 1369426
    :cond_d
    const-string v20, "show_top_gradient"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 1369427
    const/4 v3, 0x1

    .line 1369428
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    goto/16 :goto_1

    .line 1369429
    :cond_e
    const-string v20, "source_image_config"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 1369430
    invoke-static/range {p0 .. p1}, LX/8av;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1369431
    :cond_f
    const/16 v19, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1369432
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1369433
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1369434
    const/16 v17, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1369435
    const/16 v16, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1369436
    const/4 v15, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1369437
    const/4 v14, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1369438
    const/4 v13, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1369439
    const/4 v12, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1369440
    const/16 v11, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1369441
    const/16 v10, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1369442
    const/16 v9, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1369443
    if-eqz v4, :cond_10

    .line 1369444
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, LX/186;->a(IZ)V

    .line 1369445
    :cond_10
    if-eqz v3, :cond_11

    .line 1369446
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->a(IZ)V

    .line 1369447
    :cond_11
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1369448
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
