.class public LX/8cc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Qc;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Qn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3Qc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3Qn;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1374713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1374714
    iput-object p1, p0, LX/8cc;->a:LX/0Ot;

    .line 1374715
    iput-object p2, p0, LX/8cc;->b:LX/0Ot;

    .line 1374716
    return-void
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 2

    .prologue
    .line 1374707
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1374708
    :try_start_0
    invoke-virtual {p1}, LX/2Jy;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1374709
    iget-object v0, p0, LX/8cc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Qc;

    invoke-virtual {v0}, LX/3Qc;->a()V

    .line 1374710
    iget-object v0, p0, LX/8cc;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Qn;

    invoke-virtual {v0}, LX/3Qn;->a()V

    .line 1374711
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1374712
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :catch_0
    :cond_0
    move-object v0, v1

    goto :goto_0
.end method
