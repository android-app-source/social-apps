.class public final LX/8D5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/8D6;

.field public final e:Z

.field public final f:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Landroid/view/View;LX/8D6;ZI)V
    .locals 1

    .prologue
    .line 1312209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1312210
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1312211
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1312212
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1312213
    iput p1, p0, LX/8D5;->a:I

    .line 1312214
    iput-object p2, p0, LX/8D5;->b:Ljava/lang/String;

    .line 1312215
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/8D5;->c:Ljava/lang/ref/WeakReference;

    .line 1312216
    iput-object p4, p0, LX/8D5;->d:LX/8D6;

    .line 1312217
    iput-boolean p5, p0, LX/8D5;->e:Z

    .line 1312218
    iput p6, p0, LX/8D5;->f:I

    .line 1312219
    return-void

    .line 1312220
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
