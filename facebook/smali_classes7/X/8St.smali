.class public final LX/8St;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/QuicksilverFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V
    .locals 0

    .prologue
    .line 1347419
    iput-object p1, p0, LX/8St;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;)V
    .locals 5

    .prologue
    .line 1347420
    invoke-virtual {p1}, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;->a()Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;

    move-result-object v0

    .line 1347421
    if-nez v0, :cond_0

    .line 1347422
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Could not retrieve user scope for instant game"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/8St;->a(Ljava/lang/Throwable;)V

    .line 1347423
    :goto_0
    return-void

    .line 1347424
    :cond_0
    iget-object v1, p0, LX/8St;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v1, v1, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v2, LX/8TE;->USER_SCOPE_FETCH:LX/8TE;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 1347425
    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 1347426
    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1347427
    :cond_1
    const-string v0, "{}"

    .line 1347428
    :cond_2
    :try_start_0
    iget-object v1, p0, LX/8St;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1347429
    iput-object v2, v1, Lcom/facebook/quicksilver/QuicksilverFragment;->n:Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1347430
    iget-object v0, p0, LX/8St;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    sget-object v1, LX/8Su;->READY:LX/8Su;

    .line 1347431
    iput-object v1, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->k:LX/8Su;

    .line 1347432
    iget-object v0, p0, LX/8St;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-static {v0}, Lcom/facebook/quicksilver/QuicksilverFragment;->t(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    goto :goto_0

    .line 1347433
    :catch_0
    iget-object v1, p0, LX/8St;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v1, v1, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v2, LX/8TE;->USER_SCOPE_FETCH:LX/8TE;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "JSON Exception while parsing player state string: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;)V

    .line 1347434
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Failed to parse fetched player data"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/8St;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1347435
    iget-object v0, p0, LX/8St;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v1, LX/8TE;->USER_SCOPE_FETCH:LX/8TE;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 1347436
    iget-object v0, p0, LX/8St;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    sget-object v1, LX/8Su;->FAILED:LX/8Su;

    .line 1347437
    iput-object v1, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->k:LX/8Su;

    .line 1347438
    iget-object v0, p0, LX/8St;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    const/4 v1, 0x0

    .line 1347439
    iput-object v1, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->n:Lorg/json/JSONObject;

    .line 1347440
    iget-object v0, p0, LX/8St;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    const-string v1, "Failed to fetch player data"

    invoke-static {v0, v1}, Lcom/facebook/quicksilver/QuicksilverFragment;->c(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;)V

    .line 1347441
    return-void
.end method
