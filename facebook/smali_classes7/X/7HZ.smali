.class public final enum LX/7HZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7HZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7HZ;

.field public static final enum ACTIVE:LX/7HZ;

.field public static final enum ERROR:LX/7HZ;

.field public static final enum IDLE:LX/7HZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1190836
    new-instance v0, LX/7HZ;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v2}, LX/7HZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7HZ;->ACTIVE:LX/7HZ;

    .line 1190837
    new-instance v0, LX/7HZ;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, LX/7HZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7HZ;->IDLE:LX/7HZ;

    .line 1190838
    new-instance v0, LX/7HZ;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, LX/7HZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7HZ;->ERROR:LX/7HZ;

    .line 1190839
    const/4 v0, 0x3

    new-array v0, v0, [LX/7HZ;

    sget-object v1, LX/7HZ;->ACTIVE:LX/7HZ;

    aput-object v1, v0, v2

    sget-object v1, LX/7HZ;->IDLE:LX/7HZ;

    aput-object v1, v0, v3

    sget-object v1, LX/7HZ;->ERROR:LX/7HZ;

    aput-object v1, v0, v4

    sput-object v0, LX/7HZ;->$VALUES:[LX/7HZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1190840
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7HZ;
    .locals 1

    .prologue
    .line 1190841
    const-class v0, LX/7HZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7HZ;

    return-object v0
.end method

.method public static values()[LX/7HZ;
    .locals 1

    .prologue
    .line 1190842
    sget-object v0, LX/7HZ;->$VALUES:[LX/7HZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7HZ;

    return-object v0
.end method
