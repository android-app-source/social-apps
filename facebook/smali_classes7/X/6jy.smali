.class public LX/6jy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final messageIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final shouldRetainThreadIfEmpty:Ljava/lang/Boolean;

.field public final threadKey:LX/6l9;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1133930
    new-instance v0, LX/1sv;

    const-string v1, "DeltaMessageDelete"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jy;->b:LX/1sv;

    .line 1133931
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jy;->c:LX/1sw;

    .line 1133932
    new-instance v0, LX/1sw;

    const-string v1, "messageIds"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jy;->d:LX/1sw;

    .line 1133933
    new-instance v0, LX/1sw;

    const-string v1, "shouldRetainThreadIfEmpty"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jy;->e:LX/1sw;

    .line 1133934
    sput-boolean v3, LX/6jy;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6l9;Ljava/util/List;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6l9;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1133834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1133835
    iput-object p1, p0, LX/6jy;->threadKey:LX/6l9;

    .line 1133836
    iput-object p2, p0, LX/6jy;->messageIds:Ljava/util/List;

    .line 1133837
    iput-object p3, p0, LX/6jy;->shouldRetainThreadIfEmpty:Ljava/lang/Boolean;

    .line 1133838
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1133890
    if-eqz p2, :cond_4

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1133891
    :goto_0
    if-eqz p2, :cond_5

    const-string v0, "\n"

    move-object v3, v0

    .line 1133892
    :goto_1
    if-eqz p2, :cond_6

    const-string v0, " "

    .line 1133893
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "DeltaMessageDelete"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1133894
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133895
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133896
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133897
    const/4 v1, 0x1

    .line 1133898
    iget-object v6, p0, LX/6jy;->threadKey:LX/6l9;

    if-eqz v6, :cond_0

    .line 1133899
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133900
    const-string v1, "threadKey"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133901
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133902
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133903
    iget-object v1, p0, LX/6jy;->threadKey:LX/6l9;

    if-nez v1, :cond_7

    .line 1133904
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1133905
    :cond_0
    iget-object v6, p0, LX/6jy;->messageIds:Ljava/util/List;

    if-eqz v6, :cond_a

    .line 1133906
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133907
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133908
    const-string v1, "messageIds"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133909
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133910
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133911
    iget-object v1, p0, LX/6jy;->messageIds:Ljava/util/List;

    if-nez v1, :cond_8

    .line 1133912
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133913
    :goto_4
    iget-object v1, p0, LX/6jy;->shouldRetainThreadIfEmpty:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 1133914
    if-nez v2, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133915
    :cond_2
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133916
    const-string v1, "shouldRetainThreadIfEmpty"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133917
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133918
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133919
    iget-object v0, p0, LX/6jy;->shouldRetainThreadIfEmpty:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    .line 1133920
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133921
    :cond_3
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133922
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133923
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1133924
    :cond_4
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1133925
    :cond_5
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1133926
    :cond_6
    const-string v0, ""

    goto/16 :goto_2

    .line 1133927
    :cond_7
    iget-object v1, p0, LX/6jy;->threadKey:LX/6l9;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1133928
    :cond_8
    iget-object v1, p0, LX/6jy;->messageIds:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1133929
    :cond_9
    iget-object v0, p0, LX/6jy;->shouldRetainThreadIfEmpty:Ljava/lang/Boolean;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_a
    move v2, v1

    goto/16 :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1133872
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1133873
    iget-object v0, p0, LX/6jy;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1133874
    iget-object v0, p0, LX/6jy;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1133875
    sget-object v0, LX/6jy;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133876
    iget-object v0, p0, LX/6jy;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1133877
    :cond_0
    iget-object v0, p0, LX/6jy;->messageIds:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1133878
    iget-object v0, p0, LX/6jy;->messageIds:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1133879
    sget-object v0, LX/6jy;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133880
    new-instance v0, LX/1u3;

    const/16 v1, 0xb

    iget-object v2, p0, LX/6jy;->messageIds:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1133881
    iget-object v0, p0, LX/6jy;->messageIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1133882
    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1133883
    :cond_1
    iget-object v0, p0, LX/6jy;->shouldRetainThreadIfEmpty:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1133884
    iget-object v0, p0, LX/6jy;->shouldRetainThreadIfEmpty:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1133885
    sget-object v0, LX/6jy;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133886
    iget-object v0, p0, LX/6jy;->shouldRetainThreadIfEmpty:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1133887
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1133888
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1133889
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1133843
    if-nez p1, :cond_1

    .line 1133844
    :cond_0
    :goto_0
    return v0

    .line 1133845
    :cond_1
    instance-of v1, p1, LX/6jy;

    if-eqz v1, :cond_0

    .line 1133846
    check-cast p1, LX/6jy;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1133847
    if-nez p1, :cond_3

    .line 1133848
    :cond_2
    :goto_1
    move v0, v2

    .line 1133849
    goto :goto_0

    .line 1133850
    :cond_3
    iget-object v0, p0, LX/6jy;->threadKey:LX/6l9;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1133851
    :goto_2
    iget-object v3, p1, LX/6jy;->threadKey:LX/6l9;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1133852
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1133853
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133854
    iget-object v0, p0, LX/6jy;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6jy;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133855
    :cond_5
    iget-object v0, p0, LX/6jy;->messageIds:Ljava/util/List;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1133856
    :goto_4
    iget-object v3, p1, LX/6jy;->messageIds:Ljava/util/List;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1133857
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1133858
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133859
    iget-object v0, p0, LX/6jy;->messageIds:Ljava/util/List;

    iget-object v3, p1, LX/6jy;->messageIds:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133860
    :cond_7
    iget-object v0, p0, LX/6jy;->shouldRetainThreadIfEmpty:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1133861
    :goto_6
    iget-object v3, p1, LX/6jy;->shouldRetainThreadIfEmpty:Ljava/lang/Boolean;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1133862
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1133863
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133864
    iget-object v0, p0, LX/6jy;->shouldRetainThreadIfEmpty:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6jy;->shouldRetainThreadIfEmpty:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 1133865
    goto :goto_1

    :cond_a
    move v0, v2

    .line 1133866
    goto :goto_2

    :cond_b
    move v3, v2

    .line 1133867
    goto :goto_3

    :cond_c
    move v0, v2

    .line 1133868
    goto :goto_4

    :cond_d
    move v3, v2

    .line 1133869
    goto :goto_5

    :cond_e
    move v0, v2

    .line 1133870
    goto :goto_6

    :cond_f
    move v3, v2

    .line 1133871
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1133842
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1133839
    sget-boolean v0, LX/6jy;->a:Z

    .line 1133840
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jy;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1133841
    return-object v0
.end method
