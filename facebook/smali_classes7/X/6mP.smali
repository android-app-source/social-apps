.class public LX/6mP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final isSuccess:Ljava/lang/Boolean;

.field public final queryResponse:Ljava/lang/String;

.field public final requestId:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1144324
    new-instance v0, LX/1sv;

    const-string v1, "GraphQLResponse"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mP;->b:LX/1sv;

    .line 1144325
    new-instance v0, LX/1sw;

    const-string v1, "requestId"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mP;->c:LX/1sw;

    .line 1144326
    new-instance v0, LX/1sw;

    const-string v1, "queryResponse"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mP;->d:LX/1sw;

    .line 1144327
    new-instance v0, LX/1sw;

    const-string v1, "isSuccess"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mP;->e:LX/1sw;

    .line 1144328
    sput-boolean v3, LX/6mP;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1144319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1144320
    iput-object p1, p0, LX/6mP;->requestId:Ljava/lang/Integer;

    .line 1144321
    iput-object p2, p0, LX/6mP;->queryResponse:Ljava/lang/String;

    .line 1144322
    iput-object p3, p0, LX/6mP;->isSuccess:Ljava/lang/Boolean;

    .line 1144323
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1144237
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1144238
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1144239
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1144240
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GraphQLResponse"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1144241
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144242
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144243
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144244
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144245
    const-string v4, "requestId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144246
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144247
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144248
    iget-object v4, p0, LX/6mP;->requestId:Ljava/lang/Integer;

    if-nez v4, :cond_3

    .line 1144249
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144250
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144251
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144252
    const-string v4, "queryResponse"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144253
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144254
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144255
    iget-object v4, p0, LX/6mP;->queryResponse:Ljava/lang/String;

    if-nez v4, :cond_4

    .line 1144256
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144257
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144258
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144259
    const-string v4, "isSuccess"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144260
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144261
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144262
    iget-object v0, p0, LX/6mP;->isSuccess:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    .line 1144263
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144264
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144265
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144266
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1144267
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1144268
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1144269
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1144270
    :cond_3
    iget-object v4, p0, LX/6mP;->requestId:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1144271
    :cond_4
    iget-object v4, p0, LX/6mP;->queryResponse:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1144272
    :cond_5
    iget-object v0, p0, LX/6mP;->isSuccess:Ljava/lang/Boolean;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1144306
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1144307
    iget-object v0, p0, LX/6mP;->requestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1144308
    sget-object v0, LX/6mP;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144309
    iget-object v0, p0, LX/6mP;->requestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1144310
    :cond_0
    iget-object v0, p0, LX/6mP;->queryResponse:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1144311
    sget-object v0, LX/6mP;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144312
    iget-object v0, p0, LX/6mP;->queryResponse:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1144313
    :cond_1
    iget-object v0, p0, LX/6mP;->isSuccess:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1144314
    sget-object v0, LX/6mP;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144315
    iget-object v0, p0, LX/6mP;->isSuccess:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1144316
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1144317
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1144318
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1144277
    if-nez p1, :cond_1

    .line 1144278
    :cond_0
    :goto_0
    return v0

    .line 1144279
    :cond_1
    instance-of v1, p1, LX/6mP;

    if-eqz v1, :cond_0

    .line 1144280
    check-cast p1, LX/6mP;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1144281
    if-nez p1, :cond_3

    .line 1144282
    :cond_2
    :goto_1
    move v0, v2

    .line 1144283
    goto :goto_0

    .line 1144284
    :cond_3
    iget-object v0, p0, LX/6mP;->requestId:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1144285
    :goto_2
    iget-object v3, p1, LX/6mP;->requestId:Ljava/lang/Integer;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1144286
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1144287
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144288
    iget-object v0, p0, LX/6mP;->requestId:Ljava/lang/Integer;

    iget-object v3, p1, LX/6mP;->requestId:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144289
    :cond_5
    iget-object v0, p0, LX/6mP;->queryResponse:Ljava/lang/String;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1144290
    :goto_4
    iget-object v3, p1, LX/6mP;->queryResponse:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1144291
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1144292
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144293
    iget-object v0, p0, LX/6mP;->queryResponse:Ljava/lang/String;

    iget-object v3, p1, LX/6mP;->queryResponse:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144294
    :cond_7
    iget-object v0, p0, LX/6mP;->isSuccess:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1144295
    :goto_6
    iget-object v3, p1, LX/6mP;->isSuccess:Ljava/lang/Boolean;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1144296
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1144297
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144298
    iget-object v0, p0, LX/6mP;->isSuccess:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6mP;->isSuccess:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 1144299
    goto :goto_1

    :cond_a
    move v0, v2

    .line 1144300
    goto :goto_2

    :cond_b
    move v3, v2

    .line 1144301
    goto :goto_3

    :cond_c
    move v0, v2

    .line 1144302
    goto :goto_4

    :cond_d
    move v3, v2

    .line 1144303
    goto :goto_5

    :cond_e
    move v0, v2

    .line 1144304
    goto :goto_6

    :cond_f
    move v3, v2

    .line 1144305
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1144276
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1144273
    sget-boolean v0, LX/6mP;->a:Z

    .line 1144274
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mP;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1144275
    return-object v0
.end method
