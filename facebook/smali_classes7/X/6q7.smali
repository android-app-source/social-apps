.class public LX/6q7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;",
        "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1150004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150005
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 1150006
    check-cast p1, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;

    .line 1150007
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1150008
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "pin"

    .line 1150009
    iget-object v3, p1, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1150010
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150011
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150012
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "check_payment_pins"

    .line 1150013
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1150014
    move-object v1, v1

    .line 1150015
    const-string v2, "GET"

    .line 1150016
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1150017
    move-object v1, v1

    .line 1150018
    const-string v2, "/%d"

    .line 1150019
    iget-wide v6, p1, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;->c:J

    move-wide v4, v6

    .line 1150020
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1150021
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1150022
    move-object v1, v1

    .line 1150023
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1150024
    move-object v0, v1

    .line 1150025
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 1150026
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1150027
    move-object v0, v0

    .line 1150028
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1150029
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1150030
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    return-object v0
.end method
