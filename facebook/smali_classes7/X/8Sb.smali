.class public LX/8Sb;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:LX/8Sa;

.field public final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1346808
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/8Sb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1346809
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1346810
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1346811
    const-class v0, LX/8Sb;

    invoke-static {v0, p0}, LX/8Sb;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1346812
    const v0, 0x7f03045c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1346813
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/8Sb;->setGravity(I)V

    .line 1346814
    const v0, 0x7f020418

    invoke-virtual {p0, v0}, LX/8Sb;->setBackgroundResource(I)V

    .line 1346815
    const v0, 0x7f0d0beb

    invoke-virtual {p0, v0}, LX/8Sb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8Sb;->c:Landroid/widget/TextView;

    .line 1346816
    const v0, 0x7f0d0343

    invoke-virtual {p0, v0}, LX/8Sb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/8Sb;->d:Landroid/widget/ImageView;

    .line 1346817
    const v0, 0x7f0d0d2d

    invoke-virtual {p0, v0}, LX/8Sb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/8Sb;->e:Landroid/widget/ImageView;

    .line 1346818
    invoke-virtual {p0}, LX/8Sb;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v0

    check-cast v0, LX/8Sa;

    iput-object v0, p0, LX/8Sb;->b:LX/8Sa;

    .line 1346819
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/8Sb;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object p0, p1, LX/8Sb;->a:LX/0wM;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1346820
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, LX/8Sb;->setClickable(Z)V

    .line 1346821
    iget-object v0, p0, LX/8Sb;->c:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1346822
    iget-object v0, p0, LX/8Sb;->b:LX/8Sa;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    sget-object v3, LX/8SZ;->GLYPH:LX/8SZ;

    invoke-virtual {v0, v2, v3}, LX/8Sa;->a(Lcom/facebook/graphql/model/GraphQLImage;LX/8SZ;)I

    move-result v0

    .line 1346823
    if-ltz v0, :cond_1

    .line 1346824
    iget-object v2, p0, LX/8Sb;->d:Landroid/widget/ImageView;

    iget-object v3, p0, LX/8Sb;->a:LX/0wM;

    const v4, -0x958e80

    invoke-virtual {v3, v0, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1346825
    iget-object v0, p0, LX/8Sb;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1346826
    :goto_1
    iget-object v0, p0, LX/8Sb;->e:Landroid/widget/ImageView;

    if-eqz p2, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1346827
    return-void

    :cond_0
    move v0, v1

    .line 1346828
    goto :goto_0

    .line 1346829
    :cond_1
    iget-object v0, p0, LX/8Sb;->d:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1346830
    :cond_2
    const/16 v1, 0x8

    goto :goto_2
.end method
