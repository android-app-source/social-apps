.class public LX/7UP;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/7UR;


# direct methods
.method public constructor <init>(LX/7UR;)V
    .locals 0

    .prologue
    .line 1212606
    iput-object p1, p0, LX/7UP;->a:LX/7UR;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1212607
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getPreviousSpan()F

    .line 1212608
    iget-object v1, p0, LX/7UP;->a:LX/7UR;

    iget v1, v1, LX/7UR;->d:F

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v2

    mul-float/2addr v1, v2

    .line 1212609
    iget-object v2, p0, LX/7UP;->a:LX/7UR;

    iget-boolean v2, v2, LX/7UR;->j:Z

    if-eqz v2, :cond_0

    .line 1212610
    iget-object v2, p0, LX/7UP;->a:LX/7UR;

    invoke-virtual {v2}, LX/7UQ;->getMaxZoom()F

    move-result v2

    iget-object v3, p0, LX/7UP;->a:LX/7UR;

    invoke-virtual {v3}, LX/7UQ;->getMinZoom()F

    move-result v3

    const v4, 0x3dcccccd    # 0.1f

    sub-float/2addr v3, v4

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 1212611
    iget-object v2, p0, LX/7UP;->a:LX/7UR;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v4

    invoke-virtual {v2, v1, v3, v4}, LX/7UQ;->a(FFF)V

    .line 1212612
    iget-object v2, p0, LX/7UP;->a:LX/7UR;

    iget-object v3, p0, LX/7UP;->a:LX/7UR;

    invoke-virtual {v3}, LX/7UQ;->getMaxZoom()F

    move-result v3

    iget-object v4, p0, LX/7UP;->a:LX/7UR;

    invoke-virtual {v4}, LX/7UQ;->getMinZoom()F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v4, v5

    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, v2, LX/7UR;->d:F

    .line 1212613
    iget-object v1, p0, LX/7UP;->a:LX/7UR;

    iput v0, v1, LX/7UR;->f:I

    .line 1212614
    iget-object v1, p0, LX/7UP;->a:LX/7UR;

    invoke-virtual {v1}, LX/7UR;->invalidate()V

    .line 1212615
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
