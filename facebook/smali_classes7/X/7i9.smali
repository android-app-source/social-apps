.class public final LX/7i9;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:Landroid/os/Bundle;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1227097
    iput-object p1, p0, LX/7i9;->c:Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;

    iput-object p2, p0, LX/7i9;->a:Landroid/os/Bundle;

    iput-object p3, p0, LX/7i9;->b:Ljava/lang/String;

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 10
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1227098
    iget-object v0, p0, LX/7i9;->c:Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;

    iget-object v0, v0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x1050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1227099
    iget-object v1, p0, LX/7i9;->c:Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;

    iget-object v1, v1, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->g:LX/1FZ;

    const/4 v2, 0x1

    .line 1227100
    const/4 v9, 0x0

    move-object v4, v1

    move-object v5, p1

    move v6, v0

    move v7, v0

    move v8, v2

    invoke-static/range {v4 .. v9}, LX/1FZ;->a(LX/1FZ;Landroid/graphics/Bitmap;IIZLjava/lang/Object;)LX/1FJ;

    move-result-object v4

    move-object v0, v4

    .line 1227101
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1227102
    iget-object v1, p0, LX/7i9;->c:Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;

    iget-object v1, v1, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->e:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    sget-object v2, LX/46b;->DEFAULT:LX/46b;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a(Landroid/graphics/Bitmap;LX/46b;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1227103
    iget-object v1, p0, LX/7i9;->c:Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;

    iget-object v2, p0, LX/7i9;->a:Landroid/os/Bundle;

    .line 1227104
    const-string v4, "JS_BRIDGE_URL"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1227105
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1227106
    iget-object v4, v1, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->d:LX/03V;

    sget-object v5, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->a:Ljava/lang/String;

    const-string v6, "Null url for adding to homescreen"

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227107
    :goto_0
    return-void

    .line 1227108
    :cond_0
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1227109
    invoke-static {v4}, LX/2yo;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v4

    .line 1227110
    new-instance v5, LX/7iB;

    iget-object v6, v1, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->c:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, v1, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v6, v7, v4}, LX/7iB;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/String;)V

    invoke-virtual {v5}, LX/7iB;->a()Landroid/content/Intent;

    move-result-object v5

    .line 1227111
    iget-object v4, v1, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->e:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    const-string v6, "JS_BRIDGE_APP_NAME"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v7, v0

    invoke-virtual/range {v4 .. v9}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a(Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;Z)V

    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1227112
    iget-object v0, p0, LX/7i9;->c:Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;

    iget-object v0, v0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->d:LX/03V;

    sget-object v1, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to fetch icon image from url: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/7i9;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227113
    return-void
.end method
