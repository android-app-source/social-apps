.class public LX/8TD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public a:LX/0Zb;

.field private b:LX/03V;

.field public c:LX/8TS;

.field public d:LX/0if;

.field public e:LX/8TY;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0ih;


# direct methods
.method public constructor <init>(LX/0Zb;LX/03V;LX/8TS;LX/0if;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1348276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1348277
    iput-object p1, p0, LX/8TD;->a:LX/0Zb;

    .line 1348278
    iput-object p2, p0, LX/8TD;->b:LX/03V;

    .line 1348279
    iput-object p3, p0, LX/8TD;->c:LX/8TS;

    .line 1348280
    iput-object p4, p0, LX/8TD;->d:LX/0if;

    .line 1348281
    return-void
.end method

.method public static a(LX/0QB;)LX/8TD;
    .locals 7

    .prologue
    .line 1348282
    const-class v1, LX/8TD;

    monitor-enter v1

    .line 1348283
    :try_start_0
    sget-object v0, LX/8TD;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1348284
    sput-object v2, LX/8TD;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1348285
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1348286
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1348287
    new-instance p0, LX/8TD;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v5

    check-cast v5, LX/8TS;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v6

    check-cast v6, LX/0if;

    invoke-direct {p0, v3, v4, v5, v6}, LX/8TD;-><init>(LX/0Zb;LX/03V;LX/8TS;LX/0if;)V

    .line 1348288
    invoke-static {v0}, LX/8TY;->a(LX/0QB;)LX/8TY;

    move-result-object v3

    check-cast v3, LX/8TY;

    .line 1348289
    iput-object v3, p0, LX/8TD;->e:LX/8TY;

    .line 1348290
    move-object v0, p0

    .line 1348291
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1348292
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8TD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1348293
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1348294
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1348167
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/8TD;Ljava/lang/Throwable;)Ljava/util/Map;
    .locals 4
    .param p0    # LX/8TD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Ljava/util/Map",
            "<",
            "LX/8TE;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1348231
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1348232
    iget-object v1, p0, LX/8TD;->c:LX/8TS;

    .line 1348233
    iget-object v2, v1, LX/8TS;->j:LX/8TP;

    move-object v1, v2

    .line 1348234
    if-eqz v1, :cond_0

    .line 1348235
    sget-object v1, LX/8TE;->CURRENT_ROUND_CONTEXT:LX/8TE;

    iget-object v2, p0, LX/8TD;->c:LX/8TS;

    .line 1348236
    iget-object v3, v2, LX/8TS;->j:LX/8TP;

    move-object v2, v3

    .line 1348237
    iget-object v3, v2, LX/8TP;->a:LX/8Ta;

    move-object v2, v3

    .line 1348238
    invoke-virtual {v2}, LX/8Ta;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348239
    sget-object v1, LX/8TE;->CURRENT_ROUND_CONTEXT_ID:LX/8TE;

    iget-object v2, p0, LX/8TD;->c:LX/8TS;

    .line 1348240
    iget-object v3, v2, LX/8TS;->j:LX/8TP;

    move-object v2, v3

    .line 1348241
    iget-object v3, v2, LX/8TP;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1348242
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348243
    :cond_0
    iget-object v1, p0, LX/8TD;->c:LX/8TS;

    .line 1348244
    iget-object v2, v1, LX/8TS;->e:LX/8TO;

    move-object v1, v2

    .line 1348245
    if-eqz v1, :cond_1

    .line 1348246
    sget-object v2, LX/8TE;->GAME_ID:LX/8TE;

    .line 1348247
    iget-object v3, v1, LX/8TO;->b:Ljava/lang/String;

    move-object v1, v3

    .line 1348248
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348249
    :cond_1
    iget-object v1, p0, LX/8TD;->c:LX/8TS;

    .line 1348250
    iget-object v2, v1, LX/8TS;->f:LX/8Tb;

    move-object v1, v2

    .line 1348251
    if-eqz v1, :cond_2

    .line 1348252
    sget-object v2, LX/8TE;->REFERRAL:LX/8TE;

    .line 1348253
    iget-object v3, v1, LX/8Tb;->b:LX/8TZ;

    move-object v3, v3

    .line 1348254
    invoke-virtual {v3}, LX/8TZ;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348255
    sget-object v2, LX/8TE;->REFERRAL_ID_DEPRECATED:LX/8TE;

    .line 1348256
    iget-object v3, v1, LX/8Tb;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1348257
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348258
    sget-object v2, LX/8TE;->REFERRAL_CONTEXT_ID:LX/8TE;

    .line 1348259
    iget-object v3, v1, LX/8Tb;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1348260
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348261
    sget-object v2, LX/8TE;->REFERRAL_CONTEXT:LX/8TE;

    .line 1348262
    iget-object v3, v1, LX/8Tb;->c:LX/8Ta;

    move-object v1, v3

    .line 1348263
    invoke-virtual {v1}, LX/8Ta;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348264
    :cond_2
    if-eqz p1, :cond_3

    .line 1348265
    sget-object v1, LX/8TE;->EXCEPTION_MESSAGE:LX/8TE;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348266
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 1348267
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p1, v2}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 1348268
    sget-object v2, LX/8TE;->EXCEPTION_TRACE:LX/8TE;

    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348269
    :cond_3
    sget-object v1, LX/8TE;->GAME_SESSION_ID:LX/8TE;

    iget-object v2, p0, LX/8TD;->c:LX/8TS;

    .line 1348270
    iget-object v3, v2, LX/8TS;->q:Ljava/lang/String;

    move-object v2, v3

    .line 1348271
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348272
    sget-object v1, LX/8TE;->GAME_PLAY_ID:LX/8TE;

    iget-object v2, p0, LX/8TD;->c:LX/8TS;

    .line 1348273
    iget-object v3, v2, LX/8TS;->r:Ljava/lang/String;

    move-object v2, v3

    .line 1348274
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348275
    return-object v0
.end method

.method public static a(LX/8TD;LX/8TE;LX/1rQ;)V
    .locals 5
    .param p1    # LX/8TE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1348211
    iget-object v0, p0, LX/8TD;->d:LX/0if;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8TD;->f:LX/0ih;

    if-nez v0, :cond_1

    .line 1348212
    :cond_0
    sget-object v0, LX/8TE;->INITIALIZATION_ERROR:LX/8TE;

    const-string v1, "Funnel isn\'t initialized yet, please verify setFunnelDefinition is called beforelogging funnel event"

    invoke-virtual {p0, v0, v1}, LX/8TD;->a(LX/8TE;Ljava/lang/String;)V

    .line 1348213
    :goto_0
    return-void

    .line 1348214
    :cond_1
    iget-object v0, p0, LX/8TD;->c:LX/8TS;

    .line 1348215
    iget-object v1, v0, LX/8TS;->j:LX/8TP;

    move-object v0, v1

    .line 1348216
    iget-object v1, p0, LX/8TD;->c:LX/8TS;

    .line 1348217
    iget-object v2, v1, LX/8TS;->r:Ljava/lang/String;

    move-object v1, v2

    .line 1348218
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 1348219
    :try_start_0
    sget-object v3, LX/8TE;->FUNNEL_ACTION_TAG_CONTEXT:LX/8TE;

    iget-object v3, v3, LX/8TE;->value:Ljava/lang/String;

    .line 1348220
    iget-object v4, v0, LX/8TP;->a:LX/8Ta;

    move-object v4, v4

    .line 1348221
    invoke-virtual {v4}, LX/8Ta;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1348222
    sget-object v3, LX/8TE;->FUNNEL_ACTION_TAG_CONTEXT_ID:LX/8TE;

    iget-object v3, v3, LX/8TE;->value:Ljava/lang/String;

    .line 1348223
    iget-object v4, v0, LX/8TP;->b:Ljava/lang/String;

    move-object v0, v4

    .line 1348224
    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1348225
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1348226
    sget-object v0, LX/8TE;->FUNNEL_ACTION_TAG_GAME_PLAY_ID:LX/8TE;

    iget-object v0, v0, LX/8TE;->value:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1348227
    :cond_2
    iget-object v0, p0, LX/8TD;->d:LX/0if;

    iget-object v1, p0, LX/8TD;->f:LX/0ih;

    iget-object v3, p1, LX/8TE;->value:Ljava/lang/String;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    goto :goto_0

    .line 1348228
    :catch_0
    move-exception v0

    .line 1348229
    :try_start_1
    sget-object v1, LX/8TE;->FUNNEL_ACTION_TAG_CREATION_FAILURE:LX/8TE;

    const-string v3, "Funnel action tag JSON creation failed"

    invoke-virtual {p0, v1, v3, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1348230
    iget-object v0, p0, LX/8TD;->d:LX/0if;

    iget-object v1, p0, LX/8TD;->f:LX/0ih;

    iget-object v3, p1, LX/8TE;->value:Ljava/lang/String;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/8TD;->d:LX/0if;

    iget-object v3, p0, LX/8TD;->f:LX/0ih;

    iget-object v4, p1, LX/8TE;->value:Ljava/lang/String;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v4, v2, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    throw v0
.end method

.method public static b(LX/8TD;Ljava/lang/String;Ljava/lang/Throwable;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1348204
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "quicksilver"

    .line 1348205
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1348206
    move-object v2, v0

    .line 1348207
    invoke-static {p0, p2}, LX/8TD;->a(LX/8TD;Ljava/lang/Throwable;)Ljava/util/Map;

    move-result-object v0

    .line 1348208
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1348209
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8TE;

    iget-object v1, v1, LX/8TE;->value:Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 1348210
    :cond_0
    return-object v2
.end method


# virtual methods
.method public final a(LX/8TE;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1348295
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1348296
    return-void
.end method

.method public final a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 5
    .param p3    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1348191
    iget-object v0, p0, LX/8TD;->b:LX/03V;

    iget-object v1, p1, LX/8TE;->value:Ljava/lang/String;

    .line 1348192
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 1348193
    invoke-static {p0, p3}, LX/8TD;->a(LX/8TD;Ljava/lang/Throwable;)Ljava/util/Map;

    move-result-object v2

    .line 1348194
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1348195
    :try_start_0
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8TE;

    iget-object v3, v3, LX/8TE;->value:Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v4, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1348196
    :catch_0
    move-exception v2

    .line 1348197
    const-string v3, "QuicksilverLogger"

    const-string p2, "Exception while getting soft error"

    invoke-static {v3, p2, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1348198
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "quicksilver_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 1348199
    iput-object p3, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1348200
    move-object v2, v2

    .line 1348201
    invoke-virtual {v2}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    move-object v1, v2

    .line 1348202
    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1348203
    return-void
.end method

.method public final a(LX/8TE;ZLjava/lang/Throwable;)V
    .locals 3
    .param p3    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1348187
    if-eqz p2, :cond_0

    const-string v0, "quicksilver_query_success"

    :goto_0
    invoke-static {p0, v0, p3}, LX/8TD;->b(LX/8TD;Ljava/lang/String;Ljava/lang/Throwable;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/8TE;->QUERY_TYPE:LX/8TE;

    iget-object v1, v1, LX/8TE;->value:Ljava/lang/String;

    iget-object v2, p1, LX/8TE;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1348188
    iget-object v1, p0, LX/8TD;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1348189
    return-void

    .line 1348190
    :cond_0
    const-string v0, "quicksilver_query_failure"

    goto :goto_0
.end method

.method public final a(LX/8TE;ZLjava/lang/Throwable;Ljava/util/Map;)V
    .locals 3
    .param p3    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8TE;",
            "Z",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1348183
    if-eqz p2, :cond_0

    const-string v0, "quicksilver_mutation_success"

    :goto_0
    invoke-static {p0, v0, p3}, LX/8TD;->b(LX/8TD;Ljava/lang/String;Ljava/lang/Throwable;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/8TE;->MUTATION_TYPE:LX/8TE;

    iget-object v1, v1, LX/8TE;->value:Ljava/lang/String;

    iget-object v2, p1, LX/8TE;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1348184
    iget-object v1, p0, LX/8TD;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1348185
    return-void

    .line 1348186
    :cond_0
    const-string v0, "quicksilver_mutation_failure"

    goto :goto_0
.end method

.method public final a(LX/8TI;LX/8TE;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1348179
    sget-object v0, LX/8TI;->GAME_SHARE:LX/8TI;

    if-ne p1, v0, :cond_0

    const-string v0, "game_share_send"

    :goto_0
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/8TD;->b(LX/8TD;Ljava/lang/String;Ljava/lang/Throwable;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/8TE;->DESTINATION:LX/8TE;

    iget-object v1, v1, LX/8TE;->value:Ljava/lang/String;

    iget-object v2, p2, LX/8TE;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/8TE;->DESTINATION_ID:LX/8TE;

    iget-object v1, v1, LX/8TE;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1348180
    iget-object v1, p0, LX/8TD;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1348181
    return-void

    .line 1348182
    :cond_0
    const-string v0, "game_score_share_send"

    goto :goto_0
.end method

.method public final a(LX/8TX;I)V
    .locals 4

    .prologue
    .line 1348170
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    sget-object v1, LX/8TE;->FUNNEL_ACTION_TAG_CONTEXT_ID:LX/8TE;

    iget-object v1, v1, LX/8TE;->value:Ljava/lang/String;

    iget-object v2, p0, LX/8TD;->c:LX/8TS;

    .line 1348171
    iget-object v3, v2, LX/8TS;->j:LX/8TP;

    move-object v2, v3

    .line 1348172
    iget-object v3, v2, LX/8TP;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1348173
    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    sget-object v1, LX/8TE;->FUNNEL_ACTION_TAG_CONTEXT:LX/8TE;

    iget-object v1, v1, LX/8TE;->value:Ljava/lang/String;

    iget-object v2, p0, LX/8TD;->c:LX/8TS;

    .line 1348174
    iget-object v3, v2, LX/8TS;->j:LX/8TP;

    move-object v2, v3

    .line 1348175
    iget-object v3, v2, LX/8TP;->a:LX/8Ta;

    move-object v2, v3

    .line 1348176
    invoke-virtual {v2}, LX/8Ta;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    sget-object v1, LX/8TE;->FUNNEL_ACTION_TAG_INDEX:LX/8TE;

    iget-object v1, v1, LX/8TE;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v0

    sget-object v1, LX/8TE;->FUNNEL_ACTION_TAG_SOURCE:LX/8TE;

    iget-object v1, v1, LX/8TE;->value:Ljava/lang/String;

    iget-object v2, p1, LX/8TX;->loggingTag:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 1348177
    sget-object v1, LX/8TE;->FUNNEL_GAME_PLAY_START:LX/8TE;

    invoke-static {p0, v1, v0}, LX/8TD;->a(LX/8TD;LX/8TE;LX/1rQ;)V

    .line 1348178
    return-void
.end method

.method public final b(LX/8TE;)V
    .locals 1

    .prologue
    .line 1348168
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/8TD;->a(LX/8TD;LX/8TE;LX/1rQ;)V

    .line 1348169
    return-void
.end method
