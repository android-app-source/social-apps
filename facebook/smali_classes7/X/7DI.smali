.class public final LX/7DI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/19o;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/03z;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:D

.field public h:D

.field public i:Lcom/facebook/spherical/model/GuidedTourParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:D

.field public l:D


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 1182533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182534
    iput v0, p0, LX/7DI;->c:I

    .line 1182535
    iput v0, p0, LX/7DI;->d:I

    .line 1182536
    iput v0, p0, LX/7DI;->e:I

    .line 1182537
    iput v0, p0, LX/7DI;->f:I

    .line 1182538
    iput-wide v2, p0, LX/7DI;->g:D

    .line 1182539
    iput-wide v2, p0, LX/7DI;->h:D

    .line 1182540
    iput-boolean v0, p0, LX/7DI;->j:Z

    .line 1182541
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, LX/7DI;->k:D

    .line 1182542
    iput-wide v2, p0, LX/7DI;->l:D

    return-void
.end method


# virtual methods
.method public final a(D)LX/7DI;
    .locals 1

    .prologue
    .line 1182531
    iput-wide p1, p0, LX/7DI;->g:D

    .line 1182532
    return-object p0
.end method

.method public final a(I)LX/7DI;
    .locals 0

    .prologue
    .line 1182529
    iput p1, p0, LX/7DI;->c:I

    .line 1182530
    return-object p0
.end method

.method public final a(LX/03z;)LX/7DI;
    .locals 0
    .param p1    # LX/03z;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1182508
    iput-object p1, p0, LX/7DI;->b:LX/03z;

    .line 1182509
    return-object p0
.end method

.method public final a(LX/19o;)LX/7DI;
    .locals 0
    .param p1    # LX/19o;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1182527
    iput-object p1, p0, LX/7DI;->a:LX/19o;

    .line 1182528
    return-object p0
.end method

.method public final a(Lcom/facebook/spherical/model/GuidedTourParams;)LX/7DI;
    .locals 0
    .param p1    # Lcom/facebook/spherical/model/GuidedTourParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1182525
    iput-object p1, p0, LX/7DI;->i:Lcom/facebook/spherical/model/GuidedTourParams;

    .line 1182526
    return-object p0
.end method

.method public final a(Z)LX/7DI;
    .locals 0

    .prologue
    .line 1182523
    iput-boolean p1, p0, LX/7DI;->j:Z

    .line 1182524
    return-object p0
.end method

.method public final a()Lcom/facebook/spherical/model/SphericalVideoParams;
    .locals 1

    .prologue
    .line 1182522
    new-instance v0, Lcom/facebook/spherical/model/SphericalVideoParams;

    invoke-direct {v0, p0}, Lcom/facebook/spherical/model/SphericalVideoParams;-><init>(LX/7DI;)V

    return-object v0
.end method

.method public final b(D)LX/7DI;
    .locals 1

    .prologue
    .line 1182520
    iput-wide p1, p0, LX/7DI;->h:D

    .line 1182521
    return-object p0
.end method

.method public final b(I)LX/7DI;
    .locals 0

    .prologue
    .line 1182518
    iput p1, p0, LX/7DI;->d:I

    .line 1182519
    return-object p0
.end method

.method public final c(D)LX/7DI;
    .locals 1

    .prologue
    .line 1182516
    iput-wide p1, p0, LX/7DI;->k:D

    .line 1182517
    return-object p0
.end method

.method public final c(I)LX/7DI;
    .locals 0

    .prologue
    .line 1182514
    iput p1, p0, LX/7DI;->e:I

    .line 1182515
    return-object p0
.end method

.method public final d(D)LX/7DI;
    .locals 1

    .prologue
    .line 1182512
    iput-wide p1, p0, LX/7DI;->l:D

    .line 1182513
    return-object p0
.end method

.method public final d(I)LX/7DI;
    .locals 0

    .prologue
    .line 1182510
    iput p1, p0, LX/7DI;->f:I

    .line 1182511
    return-object p0
.end method
