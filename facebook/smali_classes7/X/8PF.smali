.class public LX/8PF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final a:LX/2Qx;

.field private final b:Landroid/content/Context;

.field public final c:LX/0m9;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field private f:Z

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Landroid/net/Uri;

.field private final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Qx;Landroid/content/Context;LX/0m9;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1341506
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341507
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/8PF;->g:Ljava/util/Map;

    .line 1341508
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/8PF;->h:Ljava/util/Set;

    .line 1341509
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/8PF;->j:Ljava/util/Set;

    .line 1341510
    iput-object p1, p0, LX/8PF;->a:LX/2Qx;

    .line 1341511
    iput-object p2, p0, LX/8PF;->b:Landroid/content/Context;

    .line 1341512
    iput-object p3, p0, LX/8PF;->c:LX/0m9;

    .line 1341513
    iput-object p4, p0, LX/8PF;->d:Ljava/lang/String;

    .line 1341514
    iput-object p5, p0, LX/8PF;->e:Ljava/lang/String;

    .line 1341515
    return-void
.end method

.method private static a(LX/8PF;LX/0lF;Ljava/lang/String;Z)LX/0lF;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1341471
    if-nez p2, :cond_1

    move v3, v1

    .line 1341472
    :goto_0
    if-eqz p2, :cond_2

    iget-object v0, p0, LX/8PF;->e:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1341473
    :goto_1
    iget-object v4, p0, LX/8PF;->i:Landroid/net/Uri;

    if-nez v4, :cond_3

    if-nez v3, :cond_0

    if-eqz v0, :cond_3

    .line 1341474
    :cond_0
    :goto_2
    invoke-virtual {p1}, LX/0lF;->h()Z

    move-result v0

    if-eqz v0, :cond_6

    if-eqz p3, :cond_6

    .line 1341475
    check-cast p1, LX/162;

    .line 1341476
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1341477
    invoke-virtual {p1}, LX/0lF;->e()I

    move-result v3

    move v0, v2

    .line 1341478
    :goto_3
    if-ge v0, v3, :cond_5

    .line 1341479
    invoke-virtual {p1, v0}, LX/0lF;->a(I)LX/0lF;

    move-result-object v4

    invoke-static {p0, v4, p2, v2}, LX/8PF;->a(LX/8PF;LX/0lF;Ljava/lang/String;Z)LX/0lF;

    move-result-object v4

    .line 1341480
    if-nez v4, :cond_4

    .line 1341481
    const/4 v0, 0x0

    .line 1341482
    :goto_4
    return-object v0

    :cond_1
    move v3, v2

    .line 1341483
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1341484
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1341485
    goto :goto_2

    .line 1341486
    :cond_4
    invoke-virtual {v1, v4}, LX/162;->a(LX/0lF;)LX/162;

    .line 1341487
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    move p3, v2

    .line 1341488
    :goto_5
    if-eqz p3, :cond_c

    .line 1341489
    new-instance v0, LX/162;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v2}, LX/162;-><init>(LX/0mC;)V

    .line 1341490
    invoke-virtual {v0, v1}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_4

    .line 1341491
    :cond_6
    invoke-virtual {p1}, LX/0lF;->i()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1341492
    const-string v0, "url"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1341493
    new-instance v0, LX/8PD;

    const-string v1, "Image node does not have \'url\' property."

    invoke-direct {v0, v1}, LX/8PD;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1341494
    :cond_7
    if-eqz v1, :cond_8

    .line 1341495
    const-string v0, "url"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/8PF;->i:Landroid/net/Uri;

    :cond_8
    move-object v1, p1

    .line 1341496
    goto :goto_5

    .line 1341497
    :cond_9
    invoke-virtual {p1}, LX/0lF;->o()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1341498
    new-instance v0, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 1341499
    invoke-virtual {p1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v2

    .line 1341500
    const-string v3, "url"

    invoke-virtual {v0, v3, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1341501
    if-eqz v1, :cond_a

    .line 1341502
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, LX/8PF;->i:Landroid/net/Uri;

    :cond_a
    move-object v1, v0

    .line 1341503
    goto :goto_5

    .line 1341504
    :cond_b
    new-instance v0, LX/8PD;

    const-string v1, "Unable to parse image node."

    invoke-direct {v0, v1}, LX/8PD;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    move-object v0, v1

    .line 1341505
    goto :goto_4
.end method

.method private a(LX/0m9;)V
    .locals 3

    .prologue
    .line 1341466
    iget-object v0, p0, LX/8PF;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1341467
    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/0m9;

    .line 1341468
    const-string v2, "fbsdk:create_object"

    invoke-virtual {v0, v2}, LX/0m9;->g(Ljava/lang/String;)LX/0lF;

    .line 1341469
    const-string v2, "type"

    invoke-virtual {v0, v2}, LX/0m9;->g(Ljava/lang/String;)LX/0lF;

    goto :goto_0

    .line 1341470
    :cond_0
    return-void
.end method

.method private a(LX/162;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1341447
    :try_start_0
    invoke-virtual {p1}, LX/0lF;->e()I

    move-result v9

    move v8, v1

    .line 1341448
    :goto_0
    if-ge v8, v9, :cond_4

    .line 1341449
    invoke-virtual {p1, v8}, LX/0lF;->a(I)LX/0lF;

    move-result-object v1

    move-object v0, v1

    check-cast v0, LX/0m9;

    move-object v7, v0

    .line 1341450
    const-string v1, "url"

    invoke-virtual {v7, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    .line 1341451
    if-eqz v1, :cond_3

    .line 1341452
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1341453
    const-string v2, "content:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "file:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catch LX/42w; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_2

    .line 1341454
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/8PF;->a:LX/2Qx;

    iget-object v2, p0, LX/8PF;->b:Landroid/content/Context;

    const/16 v4, 0x3c0

    const/16 v5, 0x3c0

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, LX/2Qx;->a(Landroid/content/Context;Landroid/net/Uri;IIZ)Landroid/graphics/Bitmap;
    :try_end_1
    .catch LX/42z; {:try_start_1 .. :try_end_1} :catch_1
    .catch LX/42w; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    .line 1341455
    if-nez v1, :cond_1

    .line 1341456
    :try_start_2
    new-instance v1, LX/8PD;

    const-string v2, "Error retrieving image attachment."

    invoke-direct {v1, v2}, LX/8PD;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch LX/42w; {:try_start_2 .. :try_end_2} :catch_0

    .line 1341457
    :catch_0
    move-exception v1

    .line 1341458
    new-instance v2, LX/8PD;

    const-string v3, "Error retrieving image attachment."

    invoke-direct {v2, v3, v1}, LX/8PD;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 1341459
    :catch_1
    :try_start_3
    new-instance v1, LX/8PD;

    const-string v2, "Error retrieving image attachment.  Check that the NativeAppCallContentProvider in your AndroidManifest.xml contains android:exported=\"true\"."

    invoke-direct {v1, v2}, LX/8PD;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1341460
    :cond_1
    iget-object v2, p0, LX/8PF;->g:Ljava/util/Map;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1341461
    :cond_2
    const-string v1, "user_generated"

    invoke-virtual {v7, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 1341462
    if-eqz v1, :cond_3

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0lF;->a(Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1341463
    iget-object v1, p0, LX/8PF;->j:Ljava/util/Set;

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch LX/42w; {:try_start_3 .. :try_end_3} :catch_0

    .line 1341464
    :cond_3
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_0

    .line 1341465
    :cond_4
    return-void
.end method

.method public static b(LX/0m9;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1341436
    const-string v0, "image"

    invoke-virtual {p0, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1341437
    :cond_0
    return-void

    .line 1341438
    :cond_1
    const-string v0, "image"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/162;

    .line 1341439
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v3

    .line 1341440
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    .line 1341441
    invoke-virtual {v0, v2}, LX/0lF;->a(I)LX/0lF;

    move-result-object v1

    check-cast v1, LX/0m9;

    .line 1341442
    const-string v4, "url"

    invoke-virtual {v1, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v4

    .line 1341443
    if-eqz v4, :cond_2

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1341444
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1341445
    const-string v5, "url"

    invoke-virtual {v1, v5, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1341446
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method public static c(LX/0m9;)V
    .locals 1

    .prologue
    .line 1341433
    const-string v0, "image"

    invoke-virtual {p0, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1341434
    const-string v0, "image"

    invoke-virtual {p0, v0}, LX/0m9;->g(Ljava/lang/String;)LX/0lF;

    .line 1341435
    :cond_0
    return-void
.end method

.method public static i(LX/8PF;)V
    .locals 2

    .prologue
    .line 1341431
    iget-boolean v0, p0, LX/8PF;->f:Z

    const-string v1, "OpenGraphRequest::validate was not called."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1341432
    return-void
.end method

.method private j()V
    .locals 5

    .prologue
    .line 1341420
    iget-object v0, p0, LX/8PF;->c:LX/0m9;

    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v2

    .line 1341421
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1341422
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1341423
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    invoke-virtual {v1}, LX/0lF;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1341424
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0m9;

    .line 1341425
    const-string v3, "fbsdk:create_object"

    invoke-virtual {v1, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "fbsdk:create_object"

    invoke-virtual {v1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0lF;->a(Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1341426
    const-string v3, "type"

    invoke-virtual {v1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 1341427
    if-eqz v1, :cond_1

    invoke-virtual {v1}, LX/0lF;->o()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1341428
    :cond_1
    new-instance v1, LX/8PD;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to determine type of Open Graph object: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/8PD;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1341429
    :cond_2
    iget-object v1, p0, LX/8PF;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1341430
    :cond_3
    return-void
.end method

.method private k()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1341399
    iget-object v0, p0, LX/8PF;->c:LX/0m9;

    const-string v1, "image"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1341400
    if-eqz v0, :cond_1

    .line 1341401
    const/4 v1, 0x0

    invoke-static {p0, v0, v1, v5}, LX/8PF;->a(LX/8PF;LX/0lF;Ljava/lang/String;Z)LX/0lF;

    move-result-object v0

    check-cast v0, LX/162;

    .line 1341402
    if-nez v0, :cond_0

    .line 1341403
    new-instance v0, LX/8PD;

    const-string v1, "Unable to process attached image property"

    invoke-direct {v0, v1}, LX/8PD;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1341404
    :cond_0
    invoke-direct {p0, v0}, LX/8PF;->a(LX/162;)V

    .line 1341405
    iget-object v1, p0, LX/8PF;->c:LX/0m9;

    const-string v2, "image"

    invoke-virtual {v1, v2, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1341406
    :cond_1
    iget-object v0, p0, LX/8PF;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1341407
    iget-object v1, p0, LX/8PF;->c:LX/0m9;

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    check-cast v1, LX/0m9;

    .line 1341408
    const-string v2, "image"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 1341409
    if-nez v2, :cond_3

    .line 1341410
    const-string v2, "og:image"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 1341411
    if-eqz v2, :cond_3

    .line 1341412
    const-string v4, "og:image"

    invoke-virtual {v1, v4}, LX/0m9;->g(Ljava/lang/String;)LX/0lF;

    .line 1341413
    :cond_3
    if-eqz v2, :cond_2

    .line 1341414
    invoke-static {p0, v2, v0, v5}, LX/8PF;->a(LX/8PF;LX/0lF;Ljava/lang/String;Z)LX/0lF;

    move-result-object v2

    check-cast v2, LX/162;

    .line 1341415
    if-nez v2, :cond_4

    .line 1341416
    new-instance v1, LX/8PD;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to process attached image property on "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/8PD;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1341417
    :cond_4
    invoke-direct {p0, v2}, LX/8PF;->a(LX/162;)V

    .line 1341418
    const-string v0, "image"

    invoke-virtual {v1, v0, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_0

    .line 1341419
    :cond_5
    return-void
.end method


# virtual methods
.method public final a()LX/0m9;
    .locals 3

    .prologue
    .line 1341391
    invoke-static {p0}, LX/8PF;->i(LX/8PF;)V

    .line 1341392
    iget-object v0, p0, LX/8PF;->c:LX/0m9;

    invoke-virtual {v0}, LX/0m9;->I()LX/0m9;

    move-result-object v0

    .line 1341393
    invoke-direct {p0, v0}, LX/8PF;->a(LX/0m9;)V

    .line 1341394
    invoke-static {v0}, LX/8PF;->c(LX/0m9;)V

    .line 1341395
    iget-object v1, p0, LX/8PF;->h:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1341396
    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    check-cast v1, LX/0m9;

    .line 1341397
    invoke-static {v1}, LX/8PF;->c(LX/0m9;)V

    goto :goto_0

    .line 1341398
    :cond_0
    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)LX/0m9;
    .locals 9

    .prologue
    .line 1341359
    invoke-static {p0}, LX/8PF;->i(LX/8PF;)V

    .line 1341360
    iget-object v0, p0, LX/8PF;->c:LX/0m9;

    invoke-virtual {v0}, LX/0m9;->I()LX/0m9;

    move-result-object v0

    .line 1341361
    invoke-direct {p0, v0}, LX/8PF;->a(LX/0m9;)V

    .line 1341362
    if-nez p1, :cond_4

    .line 1341363
    :cond_0
    const/4 v4, 0x0

    .line 1341364
    const-string v1, "image"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    check-cast v1, LX/162;

    .line 1341365
    if-eqz v1, :cond_3

    .line 1341366
    invoke-virtual {v1}, LX/0lF;->e()I

    move-result v5

    move v3, v4

    .line 1341367
    :goto_0
    if-ge v3, v5, :cond_2

    .line 1341368
    invoke-virtual {v1, v3}, LX/0lF;->a(I)LX/0lF;

    move-result-object v2

    check-cast v2, LX/0m9;

    .line 1341369
    invoke-virtual {v2}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v6

    .line 1341370
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1341371
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1341372
    const-string v7, "image[%d][%s]"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v8, v4

    const/4 p0, 0x1

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p1

    aput-object p1, v8, p0

    invoke-static {v7, v8}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 1341373
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0lF;

    invoke-virtual {v0, v7, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_1

    .line 1341374
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 1341375
    :cond_2
    const-string v1, "image"

    invoke-virtual {v0, v1}, LX/0m9;->g(Ljava/lang/String;)LX/0lF;

    .line 1341376
    :cond_3
    return-object v0

    .line 1341377
    :cond_4
    invoke-static {v0, p1}, LX/8PF;->b(LX/0m9;Landroid/os/Bundle;)V

    .line 1341378
    iget-object v1, p0, LX/8PF;->h:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1341379
    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    check-cast v1, LX/0m9;

    .line 1341380
    invoke-static {v1, p1}, LX/8PF;->b(LX/0m9;Landroid/os/Bundle;)V

    goto :goto_2
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1341390
    iget-object v0, p0, LX/8PF;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341388
    invoke-static {p0}, LX/8PF;->i(LX/8PF;)V

    .line 1341389
    iget-object v0, p0, LX/8PF;->g:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final f()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341386
    invoke-static {p0}, LX/8PF;->i(LX/8PF;)V

    .line 1341387
    iget-object v0, p0, LX/8PF;->j:Ljava/util/Set;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1341381
    iget-boolean v0, p0, LX/8PF;->f:Z

    if-eqz v0, :cond_0

    .line 1341382
    :goto_0
    return-void

    .line 1341383
    :cond_0
    invoke-direct {p0}, LX/8PF;->j()V

    .line 1341384
    invoke-direct {p0}, LX/8PF;->k()V

    .line 1341385
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8PF;->f:Z

    goto :goto_0
.end method
