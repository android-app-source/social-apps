.class public LX/76b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Lc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Lc",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field

.field private final b:LX/3LP;

.field private final c:LX/0TD;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:LX/0Sh;

.field private final f:LX/2RQ;

.field public g:LX/3Mb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Mb",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3LP;LX/0TD;Ljava/util/concurrent/Executor;LX/0Sh;LX/2RQ;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1171136
    iput-object p1, p0, LX/76b;->b:LX/3LP;

    .line 1171137
    iput-object p2, p0, LX/76b;->c:LX/0TD;

    .line 1171138
    iput-object p3, p0, LX/76b;->d:Ljava/util/concurrent/Executor;

    .line 1171139
    iput-object p4, p0, LX/76b;->e:LX/0Sh;

    .line 1171140
    iput-object p5, p0, LX/76b;->f:LX/2RQ;

    .line 1171141
    return-void
.end method

.method public static a$redex0(LX/76b;Ljava/util/Collection;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1171147
    iget-object v0, p0, LX/76b;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1171148
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1171149
    iget-object v1, p0, LX/76b;->f:LX/2RQ;

    invoke-virtual {v1, p1}, LX/2RQ;->b(Ljava/util/Collection;)LX/2RR;

    move-result-object v1

    .line 1171150
    iget-object v2, p0, LX/76b;->b:LX/3LP;

    invoke-virtual {v2, v1}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 1171151
    :try_start_0
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1171152
    invoke-interface {v1}, LX/3On;->close()V

    .line 1171153
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1171154
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/3On;->close()V

    throw v0
.end method

.method public static b(LX/0QB;)LX/76b;
    .locals 6

    .prologue
    .line 1171155
    new-instance v0, LX/76b;

    invoke-static {p0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v1

    check-cast v1, LX/3LP;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v5

    check-cast v5, LX/2RQ;

    invoke-direct/range {v0 .. v5}, LX/76b;-><init>(LX/3LP;LX/0TD;Ljava/util/concurrent/Executor;LX/0Sh;LX/2RQ;)V

    .line 1171156
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1171142
    iget-object v0, p0, LX/76b;->c:LX/0TD;

    new-instance v1, LX/76Z;

    invoke-direct {v1, p0, p1}, LX/76Z;-><init>(LX/76b;Ljava/util/List;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1171143
    new-instance v1, LX/76a;

    invoke-direct {v1, p0}, LX/76a;-><init>(LX/76b;)V

    .line 1171144
    iget-object v2, p0, LX/76b;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1171145
    invoke-static {v0, v1}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v0

    iput-object v0, p0, LX/76b;->a:LX/1Mv;

    .line 1171146
    return-void
.end method
