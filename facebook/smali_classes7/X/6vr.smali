.class public LX/6vr;
.super LX/6E7;
.source ""

# interfaces
.implements LX/6vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E7;",
        "LX/6vq",
        "<",
        "LX/6vn;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/6vn;

.field public b:Landroid/widget/TextView;

.field public c:Lcom/facebook/payments/ui/CallToActionSummaryView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1157209
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1157210
    const v0, 0x7f03009d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1157211
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/6vr;->setOrientation(I)V

    .line 1157212
    const v0, 0x7f0d04a3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/6vr;->b:Landroid/widget/TextView;

    .line 1157213
    const v0, 0x7f0d04a4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/CallToActionSummaryView;

    iput-object v0, p0, LX/6vr;->c:Lcom/facebook/payments/ui/CallToActionSummaryView;

    .line 1157214
    iget-object v0, p0, LX/6vr;->c:Lcom/facebook/payments/ui/CallToActionSummaryView;

    invoke-virtual {p0}, LX/6vr;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f081e2e

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->setText(Ljava/lang/String;)V

    .line 1157215
    invoke-virtual {p0}, LX/6vr;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b13ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1157216
    iget-object v1, p0, LX/6vr;->c:Lcom/facebook/payments/ui/CallToActionSummaryView;

    const/4 p1, 0x0

    invoke-virtual {v1, v0, p1, v0, v0}, Lcom/facebook/payments/ui/CallToActionSummaryView;->setPadding(IIII)V

    .line 1157217
    return-void
.end method


# virtual methods
.method public final a(LX/6vn;)V
    .locals 3

    .prologue
    .line 1157218
    iput-object p1, p0, LX/6vr;->a:LX/6vn;

    .line 1157219
    iget-object v0, p0, LX/6vr;->a:LX/6vn;

    iget-object v0, v0, LX/6vn;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1157220
    move-object v0, v0

    .line 1157221
    iget-object v1, p0, LX/6vr;->b:Landroid/widget/TextView;

    iget-object v2, p0, LX/6vr;->a:LX/6vn;

    iget-object v2, v2, LX/6vn;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1157222
    iget-object v1, p0, LX/6vr;->b:Landroid/widget/TextView;

    new-instance v2, LX/6vo;

    invoke-direct {v2, p0, v0}, LX/6vo;-><init>(LX/6vr;Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1157223
    iget-object v1, p0, LX/6vr;->c:Lcom/facebook/payments/ui/CallToActionSummaryView;

    invoke-virtual {p0}, LX/6vr;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1157224
    sget-object p0, LX/6vp;->a:[I

    iget-object p1, v0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->a:LX/6vY;

    invoke-virtual {p1}, LX/6vY;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_0

    .line 1157225
    const p0, 0x7f081e2e

    :goto_0
    move v0, p0

    .line 1157226
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->setText(Ljava/lang/String;)V

    .line 1157227
    return-void

    .line 1157228
    :pswitch_0
    const p0, 0x7f081e2f

    goto :goto_0

    .line 1157229
    :pswitch_1
    const p0, 0x7f081e30

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 1157230
    return-void
.end method
