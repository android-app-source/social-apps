.class public final LX/7P6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7P5;


# instance fields
.field public final synthetic a:Lcom/facebook/video/server/NetworkRangeWriter;


# direct methods
.method public constructor <init>(Lcom/facebook/video/server/NetworkRangeWriter;)V
    .locals 0

    .prologue
    .line 1202470
    iput-object p1, p0, LX/7P6;->a:Lcom/facebook/video/server/NetworkRangeWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/15D;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/15D",
            "<",
            "LX/4nJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1202447
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    iget-object v1, p0, LX/7P6;->a:Lcom/facebook/video/server/NetworkRangeWriter;

    iget-object v1, v1, Lcom/facebook/video/server/NetworkRangeWriter;->b:Ljava/net/URL;

    invoke-virtual {v1}, Ljava/net/URL;->toURI()Ljava/net/URI;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 1202448
    const-string v1, "Range"

    const-string v2, "bytes=0-1"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1202449
    iget-object v1, p0, LX/7P6;->a:Lcom/facebook/video/server/NetworkRangeWriter;

    invoke-static {v1, v0}, Lcom/facebook/video/server/NetworkRangeWriter;->a$redex0(Lcom/facebook/video/server/NetworkRangeWriter;Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 1202450
    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 1202451
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v1

    .line 1202452
    iput-object v0, v1, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 1202453
    move-object v0, v1

    .line 1202454
    iget-object v1, p0, LX/7P6;->a:Lcom/facebook/video/server/NetworkRangeWriter;

    iget-object v1, v1, Lcom/facebook/video/server/NetworkRangeWriter;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 1202455
    iput-object v1, v0, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1202456
    move-object v0, v0

    .line 1202457
    const-string v1, "getHeadersForVideo-PARTIAL"

    .line 1202458
    iput-object v1, v0, LX/15E;->c:Ljava/lang/String;

    .line 1202459
    move-object v0, v0

    .line 1202460
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1202461
    iput-object v1, v0, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1202462
    move-object v0, v0

    .line 1202463
    new-instance v1, LX/7P9;

    iget-object v2, p0, LX/7P6;->a:Lcom/facebook/video/server/NetworkRangeWriter;

    invoke-direct {v1, v2}, LX/7P9;-><init>(Lcom/facebook/video/server/NetworkRangeWriter;)V

    .line 1202464
    iput-object v1, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 1202465
    move-object v0, v0

    .line 1202466
    sget-object v1, LX/14P;->RETRY_SAFE:LX/14P;

    .line 1202467
    iput-object v1, v0, LX/15E;->j:LX/14P;

    .line 1202468
    move-object v0, v0

    .line 1202469
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    return-object v0
.end method
