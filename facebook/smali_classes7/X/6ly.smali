.class public abstract LX/6ly;
.super LX/0gG;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Item:",
        "Ljava/lang/Object;",
        ">",
        "LX/0gG;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0ja;

.field private d:LX/7HI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/6ly",
            "<TItem;>.ViewTransaction",
            "List;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method public constructor <init>(LX/0ja;)V
    .locals 2

    .prologue
    .line 1143117
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 1143118
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/6ly;->a:Ljava/util/Map;

    .line 1143119
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/6ly;->b:Ljava/util/Map;

    .line 1143120
    new-instance v0, LX/7HI;

    invoke-direct {v0, p0}, LX/7HI;-><init>(LX/6ly;)V

    iput-object v0, p0, LX/6ly;->d:LX/7HI;

    .line 1143121
    iput-object p1, p0, LX/6ly;->c:LX/0ja;

    .line 1143122
    return-void
.end method

.method private static a(LX/7HH;ILjava/lang/Integer;Ljava/lang/Class;)V
    .locals 5
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Class;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ly",
            "<TItem;>.QueuedViewAction;I",
            "Ljava/lang/Integer;",
            "Ljava/lang/Class;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1143114
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_0

    if-eqz p3, :cond_0

    .line 1143115
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Object is being added to pager twice: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/7HH;->b:Ljava/lang/Object;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1143116
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1143113
    iget-boolean v0, p0, LX/6ly;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6ly;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public abstract a(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TItem;"
        }
    .end annotation
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1143109
    invoke-virtual {p0, p2}, LX/6ly;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 1143110
    iget-object v1, p0, LX/6ly;->d:LX/7HI;

    .line 1143111
    iget-object v2, v1, LX/7HI;->b:Ljava/util/List;

    new-instance v3, LX/7HH;

    iget-object p0, v1, LX/7HI;->a:LX/6ly;

    sget-object p1, LX/7HG;->ADD:LX/7HG;

    invoke-direct {v3, p0, p1, v0, p2}, LX/7HH;-><init>(LX/6ly;LX/7HG;Ljava/lang/Object;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1143112
    return-object v0
.end method

.method public abstract a(Landroid/view/View;Ljava/lang/Object;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TItem;I)V"
        }
    .end annotation
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 1143106
    iget-object v0, p0, LX/6ly;->d:LX/7HI;

    .line 1143107
    iget-object v1, v0, LX/7HI;->b:Ljava/util/List;

    new-instance v2, LX/7HH;

    iget-object p0, v0, LX/7HI;->a:LX/6ly;

    sget-object p1, LX/7HG;->REMOVE:LX/7HG;

    invoke-direct {v2, p0, p1, p3, p2}, LX/7HH;-><init>(LX/6ly;LX/7HG;Ljava/lang/Object;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1143108
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1143105
    const v0, 0x7f0d00f8

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 1143100
    invoke-virtual {p0}, LX/6ly;->d()LX/2eR;

    move-result-object v1

    .line 1143101
    iget-object v0, p0, LX/6ly;->c:LX/0ja;

    invoke-interface {v1}, LX/2eR;->a()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0ja;->a(Ljava/lang/Class;)Landroid/view/View;

    move-result-object v0

    .line 1143102
    if-nez v0, :cond_0

    .line 1143103
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {v1, v0}, LX/2eS;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    .line 1143104
    :cond_0
    return-object v0
.end method

.method public b(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TItem;)V"
        }
    .end annotation

    .prologue
    .line 1143056
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;)V
    .locals 8

    .prologue
    .line 1143060
    move-object v0, p1

    check-cast v0, Lcom/facebook/widget/CustomViewPager;

    .line 1143061
    iget-object v1, p0, LX/6ly;->d:LX/7HI;

    .line 1143062
    iget-object v2, v1, LX/7HI;->b:Ljava/util/List;

    invoke-static {v2}, LX/0Ph;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v2

    move-object v1, v2

    .line 1143063
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7HH;

    .line 1143064
    sget-object v2, LX/7HF;->a:[I

    iget-object v3, v1, LX/7HH;->a:LX/7HG;

    invoke-virtual {v3}, LX/7HG;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1143065
    :pswitch_0
    invoke-virtual {p0}, LX/6ly;->d()LX/2eR;

    move-result-object v2

    .line 1143066
    const-string v3, "RecyclablePagerAdapter.add %s"

    invoke-interface {v2}, LX/2eR;->a()Ljava/lang/Class;

    move-result-object v2

    const v5, 0x667face8

    invoke-static {v3, v2, v5}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 1143067
    :try_start_0
    iget v2, v1, LX/7HH;->c:I

    invoke-virtual {p0, p1, v2}, LX/6ly;->b(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v5

    .line 1143068
    const v2, 0x7f0d00f8

    iget-object v3, v1, LX/7HH;->b:Ljava/lang/Object;

    invoke-virtual {v5, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1143069
    invoke-virtual {p0}, LX/6ly;->d()LX/2eR;

    move-result-object v2

    invoke-interface {v2}, LX/2eR;->a()Ljava/lang/Class;

    move-result-object v3

    .line 1143070
    iget-object v2, p0, LX/6ly;->b:Ljava/util/Map;

    iget-object v6, v1, LX/7HH;->b:Ljava/lang/Object;

    iget v7, v1, LX/7HH;->c:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1143071
    iget-object v6, p0, LX/6ly;->a:Ljava/util/Map;

    iget-object v7, v1, LX/7HH;->b:Ljava/lang/Object;

    invoke-interface {v6, v7, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Class;

    .line 1143072
    iget v6, v1, LX/7HH;->c:I

    invoke-static {v1, v6, v2, v3}, LX/6ly;->a(LX/7HH;ILjava/lang/Integer;Ljava/lang/Class;)V

    .line 1143073
    iget-object v2, v1, LX/7HH;->b:Ljava/lang/Object;

    iget v1, v1, LX/7HH;->c:I

    invoke-virtual {p0, v5, v2, v1}, LX/6ly;->a(Landroid/view/View;Ljava/lang/Object;I)V

    .line 1143074
    const-string v1, "RecyclablePagerAdapter.ViewPager.addView"

    const v2, -0xdc69763

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1143075
    :try_start_1
    invoke-virtual {v0, v5}, Lcom/facebook/widget/CustomViewPager;->addView(Landroid/view/View;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1143076
    const v1, -0x4b1fae2c

    :try_start_2
    invoke-static {v1}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1143077
    const v1, -0x30c145ae

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 1143078
    :catchall_0
    move-exception v0

    const v1, 0x6a229243

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1143079
    :catchall_1
    move-exception v0

    const v1, 0x7530768

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 1143080
    :pswitch_1
    iget-object v2, v1, LX/7HH;->b:Ljava/lang/Object;

    .line 1143081
    invoke-virtual {v0}, Lcom/facebook/widget/CustomViewPager;->getChildCount()I

    move-result v6

    .line 1143082
    const/4 v3, 0x0

    move v5, v3

    :goto_1
    if-ge v5, v6, :cond_3

    .line 1143083
    invoke-virtual {v0, v5}, Lcom/facebook/widget/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1143084
    invoke-virtual {p0, v3, v2}, LX/0gG;->a(Landroid/view/View;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1143085
    :goto_2
    move-object v2, v3

    .line 1143086
    if-eqz v2, :cond_0

    .line 1143087
    iget-object v3, v1, LX/7HH;->b:Ljava/lang/Object;

    invoke-virtual {p0, v2, v3}, LX/6ly;->b(Landroid/view/View;Ljava/lang/Object;)V

    .line 1143088
    iget-object v3, v1, LX/7HH;->b:Ljava/lang/Object;

    .line 1143089
    iget-object v5, p0, LX/6ly;->a:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Class;

    .line 1143090
    if-eqz v5, :cond_0

    .line 1143091
    iget-object v6, p0, LX/6ly;->c:LX/0ja;

    invoke-virtual {v6, v5, v2, v0}, LX/0ja;->a(Ljava/lang/Class;Landroid/view/View;LX/0h1;)V

    .line 1143092
    :cond_0
    iget-object v2, p0, LX/6ly;->b:Ljava/util/Map;

    iget-object v3, v1, LX/7HH;->b:Ljava/lang/Object;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1143093
    iget-object v2, p0, LX/6ly;->a:Ljava/util/Map;

    iget-object v1, v1, LX/7HH;->b:Ljava/lang/Object;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1143094
    :cond_1
    iget-object v0, p0, LX/6ly;->d:LX/7HI;

    .line 1143095
    iget-object v1, v0, LX/7HI;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1143096
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6ly;->e:Z

    .line 1143097
    return-void

    .line 1143098
    :cond_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_1

    .line 1143099
    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public abstract d()LX/2eR;
.end method

.method public final kV_()V
    .locals 1

    .prologue
    .line 1143057
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6ly;->e:Z

    .line 1143058
    invoke-super {p0}, LX/0gG;->kV_()V

    .line 1143059
    return-void
.end method
