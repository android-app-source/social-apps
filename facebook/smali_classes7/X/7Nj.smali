.class public final LX/7Nj;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/7Lp;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;)V
    .locals 0

    .prologue
    .line 1200600
    iput-object p1, p0, LX/7Nj;->a:Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/7Lp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1200601
    const-class v0, LX/7Lp;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1200602
    check-cast p1, LX/7Lp;

    .line 1200603
    sget-object v0, LX/7Nh;->a:[I

    iget-object v1, p1, LX/7Lp;->a:LX/7Lo;

    invoke-virtual {v1}, LX/7Lo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1200604
    :goto_0
    return-void

    .line 1200605
    :pswitch_0
    iget-object v0, p0, LX/7Nj;->a:Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1200606
    iget-object v0, p0, LX/7Nj;->a:Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1200607
    :cond_0
    iget-object v0, p0, LX/7Nj;->a:Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
