.class public final LX/84X;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/84Y;

.field public final synthetic b:J

.field public final synthetic c:Z

.field public final synthetic d:LX/2dp;


# direct methods
.method public constructor <init>(LX/2dp;LX/84Y;JZ)V
    .locals 1

    .prologue
    .line 1290957
    iput-object p1, p0, LX/84X;->d:LX/2dp;

    iput-object p2, p0, LX/84X;->a:LX/84Y;

    iput-wide p3, p0, LX/84X;->b:J

    iput-boolean p5, p0, LX/84X;->c:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 1290958
    iget-object v0, p0, LX/84X;->a:LX/84Y;

    if-eqz v0, :cond_0

    .line 1290959
    iget-object v0, p0, LX/84X;->a:LX/84Y;

    invoke-interface {v0}, LX/84Y;->a()V

    .line 1290960
    :cond_0
    iget-object v0, p0, LX/84X;->d:LX/2dp;

    iget-object v1, v0, LX/2dp;->c:LX/2do;

    new-instance v2, LX/2iD;

    iget-wide v4, p0, LX/84X;->b:J

    iget-boolean v0, p0, LX/84X;->c:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v2, v4, v5, v0}, LX/2iD;-><init>(JZ)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1290961
    iget-object v0, p0, LX/84X;->d:LX/2dp;

    iget-object v0, v0, LX/2dp;->a:Landroid/content/Context;

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1290962
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1290963
    iget-object v0, p0, LX/84X;->d:LX/2dp;

    iget-object v0, v0, LX/2dp;->e:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1290964
    :cond_1
    return-void

    .line 1290965
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1290966
    return-void
.end method
