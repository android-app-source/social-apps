.class public LX/8NN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/8NO;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1337058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/8NN;
    .locals 1

    .prologue
    .line 1337020
    new-instance v0, LX/8NN;

    invoke-direct {v0}, LX/8NN;-><init>()V

    .line 1337021
    move-object v0, v0

    .line 1337022
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1337023
    check-cast p1, LX/8NO;

    const/4 v6, 0x1

    .line 1337024
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1337025
    iget-object v1, p1, LX/8NO;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1337026
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "composer_session_id"

    .line 1337027
    iget-object v4, p1, LX/8NO;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1337028
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337029
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "v2.3/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1337030
    iget-object v3, p1, LX/8NO;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1337031
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/videos"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1337032
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "upload_phase"

    const-string v5, "cancel"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337033
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "upload_session_id"

    .line 1337034
    iget-object v5, p1, LX/8NO;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1337035
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337036
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v3

    const-string v4, "upload-video-chunk-cancel"

    .line 1337037
    iput-object v4, v3, LX/14O;->b:Ljava/lang/String;

    .line 1337038
    move-object v3, v3

    .line 1337039
    const-string v4, "POST"

    .line 1337040
    iput-object v4, v3, LX/14O;->c:Ljava/lang/String;

    .line 1337041
    move-object v3, v3

    .line 1337042
    iput-object v2, v3, LX/14O;->d:Ljava/lang/String;

    .line 1337043
    move-object v2, v3

    .line 1337044
    sget-object v3, LX/14S;->JSON:LX/14S;

    .line 1337045
    iput-object v3, v2, LX/14O;->k:LX/14S;

    .line 1337046
    move-object v2, v2

    .line 1337047
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1337048
    iput-object v0, v2, LX/14O;->g:Ljava/util/List;

    .line 1337049
    move-object v0, v2

    .line 1337050
    iput-boolean v6, v0, LX/14O;->n:Z

    .line 1337051
    move-object v0, v0

    .line 1337052
    iput-boolean v6, v0, LX/14O;->p:Z

    .line 1337053
    move-object v0, v0

    .line 1337054
    iput-object v1, v0, LX/14O;->A:Ljava/lang/String;

    .line 1337055
    move-object v0, v0

    .line 1337056
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1337057
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
