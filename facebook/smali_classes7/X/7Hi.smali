.class public LX/7Hi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/7B6;

.field public final b:LX/7Hc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Hc",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final c:LX/7HY;

.field public final d:LX/7Ha;


# direct methods
.method public constructor <init>(LX/7B6;LX/7Hc;LX/7HY;LX/7Ha;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7B6;",
            "LX/7Hc",
            "<TT;>;",
            "LX/7HY;",
            "LX/7Ha;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1191058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1191059
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1191060
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1191061
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1191062
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1191063
    iput-object p1, p0, LX/7Hi;->a:LX/7B6;

    .line 1191064
    iput-object p2, p0, LX/7Hi;->b:LX/7Hc;

    .line 1191065
    iput-object p3, p0, LX/7Hi;->c:LX/7HY;

    .line 1191066
    iput-object p4, p0, LX/7Hi;->d:LX/7Ha;

    .line 1191067
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1191068
    instance-of v0, p1, LX/7Hi;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/7Hi;

    .line 1191069
    iget-object v1, v0, LX/7Hi;->a:LX/7B6;

    move-object v0, v1

    .line 1191070
    iget-object v1, p0, LX/7Hi;->a:LX/7B6;

    move-object v1, v1

    .line 1191071
    invoke-virtual {v0, v1}, LX/7B6;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/7Hi;

    .line 1191072
    iget-object v1, v0, LX/7Hi;->b:LX/7Hc;

    move-object v0, v1

    .line 1191073
    iget-object v1, p0, LX/7Hi;->b:LX/7Hc;

    move-object v1, v1

    .line 1191074
    invoke-virtual {v0, v1}, LX/7Hc;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/7Hi;

    .line 1191075
    iget-object v1, v0, LX/7Hi;->c:LX/7HY;

    move-object v0, v1

    .line 1191076
    iget-object v1, p0, LX/7Hi;->c:LX/7HY;

    move-object v1, v1

    .line 1191077
    if-ne v0, v1, :cond_0

    check-cast p1, LX/7Hi;

    .line 1191078
    iget-object v0, p1, LX/7Hi;->d:LX/7Ha;

    move-object v0, v0

    .line 1191079
    iget-object v1, p0, LX/7Hi;->d:LX/7Ha;

    move-object v1, v1

    .line 1191080
    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1191081
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 1191082
    iget-object v2, p0, LX/7Hi;->a:LX/7B6;

    move-object v2, v2

    .line 1191083
    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 1191084
    iget-object v2, p0, LX/7Hi;->b:LX/7Hc;

    move-object v2, v2

    .line 1191085
    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 1191086
    iget-object v2, p0, LX/7Hi;->c:LX/7HY;

    move-object v2, v2

    .line 1191087
    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 1191088
    iget-object v2, p0, LX/7Hi;->d:LX/7Ha;

    move-object v2, v2

    .line 1191089
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
