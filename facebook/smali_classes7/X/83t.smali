.class public final LX/83t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestAcceptCoreMutationFieldsModel;",
        ">;",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2dj;


# direct methods
.method public constructor <init>(LX/2dj;)V
    .locals 0

    .prologue
    .line 1290684
    iput-object p1, p0, LX/83t;->a:LX/2dj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290677
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1290678
    if-eqz p1, :cond_0

    .line 1290679
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1290680
    check-cast v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestAcceptCoreMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestAcceptCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestAcceptCoreMutationFieldsModel$FriendRequesterModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1290681
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1290682
    check-cast v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestAcceptCoreMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestAcceptCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestAcceptCoreMutationFieldsModel$FriendRequesterModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestAcceptCoreMutationFieldsModel$FriendRequesterModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 1290683
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
