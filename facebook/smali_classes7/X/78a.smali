.class public final LX/78a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/7FB;

.field public final synthetic b:Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;LX/7FB;)V
    .locals 0

    .prologue
    .line 1173049
    iput-object p1, p0, LX/78a;->b:Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;

    iput-object p2, p0, LX/78a;->a:LX/7FB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x6e841eab

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1173050
    iget-object v1, p0, LX/78a;->b:Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;

    iget-object v2, p0, LX/78a;->a:LX/7FB;

    .line 1173051
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7FB;->a(Ljava/lang/Boolean;)V

    .line 1173052
    iget-object v3, v1, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->t:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v3}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->h()V

    .line 1173053
    iget-object v1, p0, LX/78a;->b:Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1173054
    new-instance v1, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;

    invoke-direct {v1}, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;-><init>()V

    .line 1173055
    iget-object v2, p0, LX/78a;->b:Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;

    iget-object v2, v2, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->t:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 1173056
    iput-object v2, v1, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->s:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 1173057
    iget-object v2, p0, LX/78a;->b:Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;

    .line 1173058
    iget-object v3, v2, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v3

    .line 1173059
    sget-object v3, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1173060
    iget-object v1, p0, LX/78a;->b:Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;

    iget-object v1, v1, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->t:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    sget-object v2, LX/7FJ;->START:LX/7FJ;

    invoke-virtual {v1, v2}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(LX/7FJ;)V

    .line 1173061
    const v1, 0x19aa0275

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
