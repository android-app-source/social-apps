.class public LX/761;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public bypassProxyDomains:Ljava/lang/String;

.field public connectTimeout:I

.field public pingRespTimeout:I

.field public proxyAddress:Ljava/lang/String;

.field public proxyFallbackEnabled:Z

.field public proxyPort:I

.field public proxyUserAgent:Ljava/lang/String;

.field public secureProxyAddress:Ljava/lang/String;

.field public secureProxyPort:I

.field public verifyCertificates:Z

.field public zlibCompression:Z


# direct methods
.method public constructor <init>(IIZZLjava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1170431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1170432
    iput p1, p0, LX/761;->pingRespTimeout:I

    .line 1170433
    iput p2, p0, LX/761;->connectTimeout:I

    .line 1170434
    iput-boolean p3, p0, LX/761;->verifyCertificates:Z

    .line 1170435
    iput-boolean p4, p0, LX/761;->zlibCompression:Z

    .line 1170436
    iput-object p5, p0, LX/761;->proxyAddress:Ljava/lang/String;

    .line 1170437
    iput p6, p0, LX/761;->proxyPort:I

    .line 1170438
    iput-object p7, p0, LX/761;->secureProxyAddress:Ljava/lang/String;

    .line 1170439
    iput p8, p0, LX/761;->secureProxyPort:I

    .line 1170440
    iput-object p9, p0, LX/761;->bypassProxyDomains:Ljava/lang/String;

    .line 1170441
    iput-object p10, p0, LX/761;->proxyUserAgent:Ljava/lang/String;

    .line 1170442
    iput-boolean p11, p0, LX/761;->proxyFallbackEnabled:Z

    .line 1170443
    return-void
.end method
