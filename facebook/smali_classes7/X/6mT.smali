.class public LX/6mT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final coordinates:LX/6mM;

.field public final isCurrentLocation:Ljava/lang/Boolean;

.field public final placeId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 1144502
    new-instance v0, LX/1sv;

    const-string v1, "LocationAttachment"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mT;->b:LX/1sv;

    .line 1144503
    new-instance v0, LX/1sw;

    const-string v1, "coordinates"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mT;->c:LX/1sw;

    .line 1144504
    new-instance v0, LX/1sw;

    const-string v1, "isCurrentLocation"

    invoke-direct {v0, v1, v3, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mT;->d:LX/1sw;

    .line 1144505
    new-instance v0, LX/1sw;

    const-string v1, "placeId"

    const/16 v2, 0xa

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mT;->e:LX/1sw;

    .line 1144506
    sput-boolean v4, LX/6mT;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6mM;Ljava/lang/Boolean;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1144507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1144508
    iput-object p1, p0, LX/6mT;->coordinates:LX/6mM;

    .line 1144509
    iput-object p2, p0, LX/6mT;->isCurrentLocation:Ljava/lang/Boolean;

    .line 1144510
    iput-object p3, p0, LX/6mT;->placeId:Ljava/lang/Long;

    .line 1144511
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1144512
    if-eqz p2, :cond_4

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1144513
    :goto_0
    if-eqz p2, :cond_5

    const-string v0, "\n"

    move-object v3, v0

    .line 1144514
    :goto_1
    if-eqz p2, :cond_6

    const-string v0, " "

    .line 1144515
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "LocationAttachment"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1144516
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144517
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144518
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144519
    const/4 v1, 0x1

    .line 1144520
    iget-object v6, p0, LX/6mT;->coordinates:LX/6mM;

    if-eqz v6, :cond_0

    .line 1144521
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144522
    const-string v1, "coordinates"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144523
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144524
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144525
    iget-object v1, p0, LX/6mT;->coordinates:LX/6mM;

    if-nez v1, :cond_7

    .line 1144526
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1144527
    :cond_0
    iget-object v6, p0, LX/6mT;->isCurrentLocation:Ljava/lang/Boolean;

    if-eqz v6, :cond_a

    .line 1144528
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144529
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144530
    const-string v1, "isCurrentLocation"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144531
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144532
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144533
    iget-object v1, p0, LX/6mT;->isCurrentLocation:Ljava/lang/Boolean;

    if-nez v1, :cond_8

    .line 1144534
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144535
    :goto_4
    iget-object v1, p0, LX/6mT;->placeId:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 1144536
    if-nez v2, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144537
    :cond_2
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144538
    const-string v1, "placeId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144539
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144540
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144541
    iget-object v0, p0, LX/6mT;->placeId:Ljava/lang/Long;

    if-nez v0, :cond_9

    .line 1144542
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144543
    :cond_3
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144544
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144545
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1144546
    :cond_4
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1144547
    :cond_5
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1144548
    :cond_6
    const-string v0, ""

    goto/16 :goto_2

    .line 1144549
    :cond_7
    iget-object v1, p0, LX/6mT;->coordinates:LX/6mM;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1144550
    :cond_8
    iget-object v1, p0, LX/6mT;->isCurrentLocation:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1144551
    :cond_9
    iget-object v0, p0, LX/6mT;->placeId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_a
    move v2, v1

    goto/16 :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1144552
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1144553
    iget-object v0, p0, LX/6mT;->coordinates:LX/6mM;

    if-eqz v0, :cond_0

    .line 1144554
    iget-object v0, p0, LX/6mT;->coordinates:LX/6mM;

    if-eqz v0, :cond_0

    .line 1144555
    sget-object v0, LX/6mT;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144556
    iget-object v0, p0, LX/6mT;->coordinates:LX/6mM;

    invoke-virtual {v0, p1}, LX/6mM;->a(LX/1su;)V

    .line 1144557
    :cond_0
    iget-object v0, p0, LX/6mT;->isCurrentLocation:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1144558
    iget-object v0, p0, LX/6mT;->isCurrentLocation:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1144559
    sget-object v0, LX/6mT;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144560
    iget-object v0, p0, LX/6mT;->isCurrentLocation:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1144561
    :cond_1
    iget-object v0, p0, LX/6mT;->placeId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1144562
    iget-object v0, p0, LX/6mT;->placeId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1144563
    sget-object v0, LX/6mT;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144564
    iget-object v0, p0, LX/6mT;->placeId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1144565
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1144566
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1144567
    return-void
.end method

.method public final a(LX/6mT;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1144568
    if-nez p1, :cond_1

    .line 1144569
    :cond_0
    :goto_0
    return v2

    .line 1144570
    :cond_1
    iget-object v0, p0, LX/6mT;->coordinates:LX/6mM;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1144571
    :goto_1
    iget-object v3, p1, LX/6mT;->coordinates:LX/6mM;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1144572
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1144573
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1144574
    iget-object v0, p0, LX/6mT;->coordinates:LX/6mM;

    iget-object v3, p1, LX/6mT;->coordinates:LX/6mM;

    invoke-virtual {v0, v3}, LX/6mM;->a(LX/6mM;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1144575
    :cond_3
    iget-object v0, p0, LX/6mT;->isCurrentLocation:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1144576
    :goto_3
    iget-object v3, p1, LX/6mT;->isCurrentLocation:Ljava/lang/Boolean;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1144577
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1144578
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1144579
    iget-object v0, p0, LX/6mT;->isCurrentLocation:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6mT;->isCurrentLocation:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1144580
    :cond_5
    iget-object v0, p0, LX/6mT;->placeId:Ljava/lang/Long;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1144581
    :goto_5
    iget-object v3, p1, LX/6mT;->placeId:Ljava/lang/Long;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1144582
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1144583
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1144584
    iget-object v0, p0, LX/6mT;->placeId:Ljava/lang/Long;

    iget-object v3, p1, LX/6mT;->placeId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_7
    move v2, v1

    .line 1144585
    goto :goto_0

    :cond_8
    move v0, v2

    .line 1144586
    goto :goto_1

    :cond_9
    move v3, v2

    .line 1144587
    goto :goto_2

    :cond_a
    move v0, v2

    .line 1144588
    goto :goto_3

    :cond_b
    move v3, v2

    .line 1144589
    goto :goto_4

    :cond_c
    move v0, v2

    .line 1144590
    goto :goto_5

    :cond_d
    move v3, v2

    .line 1144591
    goto :goto_6
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1144592
    if-nez p1, :cond_1

    .line 1144593
    :cond_0
    :goto_0
    return v0

    .line 1144594
    :cond_1
    instance-of v1, p1, LX/6mT;

    if-eqz v1, :cond_0

    .line 1144595
    check-cast p1, LX/6mT;

    invoke-virtual {p0, p1}, LX/6mT;->a(LX/6mT;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1144596
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1144597
    sget-boolean v0, LX/6mT;->a:Z

    .line 1144598
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mT;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1144599
    return-object v0
.end method
