.class public LX/7JN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/098;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:LX/162;

.field private final e:Z

.field private final f:Z

.field private final g:Z

.field private final h:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public final i:I

.field private final j:I

.field public final k:LX/04D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:LX/1bf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:LX/1bf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/video/engine/VideoDataSource;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:I

.field public p:I

.field public q:I

.field private r:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;LX/1bf;LX/1bf;LX/162;ZZZLcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;LX/04D;IIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/video/engine/VideoDataSource;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/1bf;",
            "LX/1bf;",
            "LX/162;",
            "ZZZ",
            "Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;",
            "LX/04D;",
            "IIZ)V"
        }
    .end annotation

    .prologue
    .line 1193514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1193515
    invoke-direct {p0, p2}, LX/7JN;->a(LX/0Px;)V

    .line 1193516
    iput-object p1, p0, LX/7JN;->a:Ljava/lang/String;

    .line 1193517
    iput-object p5, p0, LX/7JN;->l:LX/1bf;

    .line 1193518
    iput-object p6, p0, LX/7JN;->m:LX/1bf;

    .line 1193519
    iput-object p3, p0, LX/7JN;->b:Ljava/lang/String;

    .line 1193520
    iput-object p4, p0, LX/7JN;->c:Ljava/lang/String;

    .line 1193521
    iput-object p7, p0, LX/7JN;->d:LX/162;

    .line 1193522
    iput-boolean p8, p0, LX/7JN;->e:Z

    .line 1193523
    iput-boolean p9, p0, LX/7JN;->f:Z

    .line 1193524
    iput-boolean p10, p0, LX/7JN;->g:Z

    .line 1193525
    iput p14, p0, LX/7JN;->j:I

    .line 1193526
    iput-object p12, p0, LX/7JN;->k:LX/04D;

    .line 1193527
    iput p13, p0, LX/7JN;->i:I

    .line 1193528
    iput-object p11, p0, LX/7JN;->h:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1193529
    iput-boolean p15, p0, LX/7JN;->r:Z

    .line 1193530
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;LX/1bf;LX/1bf;LX/162;ZZZLcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;LX/04D;IIZB)V
    .locals 0

    .prologue
    .line 1193513
    invoke-direct/range {p0 .. p15}, LX/7JN;-><init>(Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;LX/1bf;LX/1bf;LX/162;ZZZLcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;LX/04D;IIZ)V

    return-void
.end method

.method private a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/video/engine/VideoDataSource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1193508
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1193509
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, LX/7JN;->q:I

    .line 1193510
    :goto_0
    return-void

    .line 1193511
    :cond_1
    iput-object p1, p0, LX/7JN;->n:LX/0Px;

    .line 1193512
    const/4 v0, 0x0

    iput v0, p0, LX/7JN;->q:I

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1193507
    iget-boolean v0, p0, LX/7JN;->g:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1193494
    iget-boolean v0, p0, LX/7JN;->f:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1193506
    iget-boolean v0, p0, LX/7JN;->e:Z

    return v0
.end method

.method public final d()LX/19o;
    .locals 1

    .prologue
    .line 1193505
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1193504
    iget-boolean v0, p0, LX/7JN;->r:Z

    return v0
.end method

.method public final f()LX/03z;
    .locals 1

    .prologue
    .line 1193503
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .locals 1

    .prologue
    .line 1193502
    iget-object v0, p0, LX/7JN;->h:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 1193501
    iget v0, p0, LX/7JN;->j:I

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 1193500
    iget v0, p0, LX/7JN;->i:I

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1193499
    iget-object v0, p0, LX/7JN;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final q()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1193498
    iget-object v0, p0, LX/7JN;->d:LX/162;

    return-object v0
.end method

.method public final r()LX/04D;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1193497
    iget-object v0, p0, LX/7JN;->k:LX/04D;

    return-object v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 1193496
    iget v0, p0, LX/7JN;->p:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1193495
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VideoID: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/7JN;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Position: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/7JN;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isLive: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/7JN;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", PlayerOrigin: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/7JN;->k:LX/04D;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
