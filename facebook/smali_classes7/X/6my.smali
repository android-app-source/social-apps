.class public LX/6my;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/071;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:LX/6mt;

.field private final c:LX/1tU;

.field public final d:LX/01o;

.field private final e:LX/6mu;

.field private final f:Lcom/facebook/proxygen/MQTTClientCallback;

.field public g:LX/076;

.field private h:LX/6mv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1147017
    const-class v0, LX/6my;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6my;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(IILcom/facebook/proxygen/MQTTClientFactory;LX/01o;ZZLX/6mu;Ljava/util/concurrent/Executor;Lcom/facebook/proxygen/EventBase;)V
    .locals 3

    .prologue
    .line 1146939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146940
    new-instance v0, LX/6mw;

    invoke-direct {v0, p0}, LX/6mw;-><init>(LX/6my;)V

    iput-object v0, p0, LX/6my;->f:Lcom/facebook/proxygen/MQTTClientCallback;

    .line 1146941
    iput-object p4, p0, LX/6my;->d:LX/01o;

    .line 1146942
    new-instance v0, LX/1tU;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/1tU;-><init>(LX/05K;)V

    iput-object v0, p0, LX/6my;->c:LX/1tU;

    .line 1146943
    iput-object p7, p0, LX/6my;->e:LX/6mu;

    .line 1146944
    new-instance v0, LX/75w;

    invoke-direct {v0}, LX/75w;-><init>()V

    .line 1146945
    packed-switch p1, :pswitch_data_0

    .line 1146946
    sget-object v1, LX/75v;->NONE:LX/75v;

    :goto_0
    move-object v1, v1

    .line 1146947
    iput-object v1, v0, LX/75w;->publishFormat:LX/75v;

    .line 1146948
    iput p2, v0, LX/75w;->keepaliveSecs:I

    .line 1146949
    const-string v1, ""

    .line 1146950
    iput-object v1, v0, LX/75w;->clientId:Ljava/lang/String;

    .line 1146951
    const/4 v1, 0x1

    .line 1146952
    iput-boolean v1, v0, LX/75w;->enableTopicEncoding:Z

    .line 1146953
    new-instance v1, Lcom/facebook/proxygen/MQTTClient;

    iget-object v2, p0, LX/6my;->f:Lcom/facebook/proxygen/MQTTClientCallback;

    invoke-direct {v1, p3, v2, v0}, Lcom/facebook/proxygen/MQTTClient;-><init>(Lcom/facebook/proxygen/MQTTClientFactory;Lcom/facebook/proxygen/MQTTClientCallback;LX/75w;)V

    .line 1146954
    if-eqz p6, :cond_0

    .line 1146955
    invoke-static {p9}, Lcom/facebook/proxygen/RadioStatusMonitor;->createOrGetMonitorInstance(Lcom/facebook/proxygen/EventBase;)Lcom/facebook/proxygen/RadioStatusMonitor;

    move-result-object v0

    .line 1146956
    iput-object v0, v1, Lcom/facebook/proxygen/MQTTClient;->mRadioStatusMonitor:Lcom/facebook/proxygen/RadioStatusMonitor;

    .line 1146957
    :cond_0
    if-eqz p5, :cond_1

    .line 1146958
    iput-object p7, v1, Lcom/facebook/proxygen/MQTTClient;->mLogger:Lcom/facebook/proxygen/AnalyticsLogger;

    .line 1146959
    new-instance v0, LX/6mv;

    invoke-direct {v0}, LX/6mv;-><init>()V

    iput-object v0, p0, LX/6my;->h:LX/6mv;

    .line 1146960
    iget-object v0, p0, LX/6my;->h:LX/6mv;

    .line 1146961
    iput-object v0, v1, Lcom/facebook/proxygen/MQTTClient;->mByteEventLogger:Lcom/facebook/proxygen/ByteEventLogger;

    .line 1146962
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/proxygen/MQTTClient;->init()V

    .line 1146963
    new-instance v0, LX/6mt;

    invoke-direct {v0, v1, p8}, LX/6mt;-><init>(Lcom/facebook/proxygen/MQTTClient;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, LX/6my;->b:LX/6mt;

    .line 1146964
    return-void

    .line 1146965
    :pswitch_0
    sget-object v1, LX/75v;->ZLIB:LX/75v;

    goto :goto_0

    .line 1146966
    :pswitch_1
    sget-object v1, LX/75v;->ZLIB_OPTIONAL:LX/75v;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic a(LX/6my;LX/07S;I)LX/07W;
    .locals 3

    .prologue
    .line 1147018
    new-instance v1, LX/07R;

    invoke-direct {v1, p1}, LX/07R;-><init>(LX/07S;)V

    .line 1147019
    new-instance v2, LX/0B6;

    invoke-direct {v2, p2}, LX/0B6;-><init>(I)V

    .line 1147020
    sget-object v0, LX/6mx;->b:[I

    invoke-virtual {p1}, LX/07S;->ordinal()I

    move-result p0

    aget v0, v0, p0

    packed-switch v0, :pswitch_data_0

    .line 1147021
    new-instance v0, LX/07W;

    const/4 p0, 0x0

    invoke-direct {v0, v1, v2, p0}, LX/07W;-><init>(LX/07R;Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    move-object v0, v0

    .line 1147022
    return-object v0

    .line 1147023
    :pswitch_0
    new-instance v0, LX/0B7;

    invoke-direct {v0, v1, v2}, LX/0B7;-><init>(LX/07R;LX/0B6;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static synthetic a(LX/6my;Lcom/facebook/proxygen/MQTTClientError;)Ljava/lang/Throwable;
    .locals 2

    .prologue
    .line 1147024
    iget-object v0, p1, Lcom/facebook/proxygen/MQTTClientError;->mErrMsg:Ljava/lang/String;

    move-object v1, v0

    .line 1147025
    sget-object v0, LX/6mx;->a:[I

    .line 1147026
    iget-object p0, p1, Lcom/facebook/proxygen/MQTTClientError;->mErrType:Lcom/facebook/proxygen/MQTTClientError$MQTTErrorType;

    move-object p0, p0

    .line 1147027
    invoke-virtual {p0}, Lcom/facebook/proxygen/MQTTClientError$MQTTErrorType;->ordinal()I

    move-result p0

    aget v0, v0, p0

    packed-switch v0, :pswitch_data_0

    .line 1147028
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    :goto_0
    move-object v0, v0

    .line 1147029
    return-object v0

    .line 1147030
    :pswitch_0
    new-instance v0, Ljava/util/zip/DataFormatException;

    invoke-direct {v0, v1}, Ljava/util/zip/DataFormatException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1147031
    :pswitch_1
    new-instance v0, Ljava/net/SocketException;

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1147032
    :pswitch_2
    new-instance v0, Ljava/net/SocketException;

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1147033
    :pswitch_3
    new-instance v0, Ljava/util/zip/DataFormatException;

    invoke-direct {v0, v1}, Ljava/util/zip/DataFormatException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1147034
    :pswitch_4
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1147035
    :pswitch_5
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/07X;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1147052
    invoke-virtual {p1}, LX/07X;->a()LX/0B5;

    move-result-object v0

    iget-object v0, v0, LX/0B5;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 1147036
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "cleanUp"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1147037
    iget-object v0, p0, LX/6my;->b:LX/6mt;

    .line 1147038
    new-instance v1, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$5;

    invoke-direct {v1, v0}, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$5;-><init>(LX/6mt;)V

    .line 1147039
    iget-object v2, v0, LX/6mt;->c:Ljava/util/concurrent/Executor;

    const v3, 0x781099d5

    invoke-static {v2, v1, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1147040
    iget-object v0, p0, LX/6my;->b:LX/6mt;

    .line 1147041
    new-instance v1, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$10;

    invoke-direct {v1, v0}, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$10;-><init>(LX/6mt;)V

    .line 1147042
    iget-object v2, v0, LX/6mt;->c:Ljava/util/concurrent/Executor;

    const v3, -0x6034c945

    invoke-static {v2, v1, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1147043
    iget-object v0, p0, LX/6my;->g:LX/076;

    sget-object v1, LX/074;->DISCONNECTED:LX/074;

    invoke-virtual {v0, v1}, LX/076;->b(LX/074;)V

    .line 1147044
    iget-object v0, p0, LX/6my;->g:LX/076;

    invoke-virtual {v0}, LX/076;->c()V

    .line 1147045
    return-void
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 1147046
    return-void
.end method

.method public final a(LX/076;LX/075;)V
    .locals 1

    .prologue
    .line 1147047
    iput-object p1, p0, LX/6my;->g:LX/076;

    .line 1147048
    iget-object v0, p0, LX/6my;->h:LX/6mv;

    if-eqz v0, :cond_0

    .line 1147049
    iget-object v0, p0, LX/6my;->h:LX/6mv;

    .line 1147050
    iput-object p2, v0, LX/6mv;->a:LX/075;

    .line 1147051
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;IZLX/079;IZ)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 1147000
    :try_start_0
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "connect host=%s, port=%d, userAuth=%b, keeepAlive=%d, ssl=%b"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v2, v3

    const/4 v3, 0x3

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    const/4 v3, 0x4

    invoke-static {p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1147001
    invoke-static {p4}, LX/1tU;->a(LX/079;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1147002
    iget-object v0, p0, LX/6my;->b:LX/6mt;

    array-length v5, v3

    move-object v1, p1

    move v2, p2

    move v6, p6

    invoke-virtual/range {v0 .. v6}, LX/6mt;->a(Ljava/lang/String;I[BIIZ)V

    .line 1147003
    iget-object v0, p0, LX/6my;->g:LX/076;

    sget-object v1, LX/07S;->CONNECT:LX/07S;

    invoke-virtual {v1}, LX/07S;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, LX/076;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1147004
    iget-object v0, p0, LX/6my;->e:LX/6mu;

    if-eqz v0, :cond_0

    .line 1147005
    iget-object v0, p0, LX/6my;->e:LX/6mu;

    .line 1147006
    iget-object v1, v0, LX/6mu;->b:LX/059;

    if-eqz v1, :cond_0

    .line 1147007
    iget-object v1, v0, LX/6mu;->b:LX/059;

    invoke-virtual {v1}, LX/059;->b()LX/0HW;

    move-result-object v1

    iput-object v1, v0, LX/6mu;->d:LX/0HW;

    .line 1147008
    :cond_0
    :goto_0
    return-void

    .line 1147009
    :catch_0
    move-exception v0

    .line 1147010
    sget-object v1, LX/6my;->a:Ljava/lang/String;

    const-string v2, "Failed to encode connectPayload=%s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p4, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1147011
    iget-object v1, p0, LX/6my;->g:LX/076;

    new-instance v2, LX/07k;

    sget-object v3, LX/0Hs;->FAILED_CONNECT_MESSAGE:LX/0Hs;

    invoke-direct {v2, v3, v0}, LX/07k;-><init>(LX/0Hs;Ljava/lang/Exception;)V

    invoke-virtual {v1, v2}, LX/076;->a(LX/07k;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[BII)V
    .locals 11

    .prologue
    .line 1147012
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "publish, topic=%s, qos=%d, msgId=%d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1147013
    iget-object v0, p0, LX/6my;->b:LX/6mt;

    .line 1147014
    new-instance v5, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$4;

    move-object v6, v0

    move-object v7, p1

    move-object v8, p2

    move v9, p3

    move v10, p4

    invoke-direct/range {v5 .. v10}, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$4;-><init>(LX/6mt;Ljava/lang/String;[BII)V

    .line 1147015
    iget-object v6, v0, LX/6mt;->c:Ljava/util/concurrent/Executor;

    const v7, -0x3198e175    # -9.6938464E8f

    invoke-static {v6, v5, v7}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1147016
    return-void
.end method

.method public final a(Ljava/util/List;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;I)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1146989
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 1146990
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [I

    .line 1146991
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1146992
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0AF;

    .line 1146993
    iget-object v4, v0, LX/0AF;->a:Ljava/lang/String;

    aput-object v4, v2, v1

    .line 1146994
    iget v0, v0, LX/0AF;->b:I

    aput v0, v3, v1

    .line 1146995
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1146996
    :cond_0
    iget-object v0, p0, LX/6my;->b:LX/6mt;

    .line 1146997
    new-instance v1, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$2;

    invoke-direct {v1, v0, v2, v3}, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$2;-><init>(LX/6mt;[Ljava/lang/String;[I)V

    .line 1146998
    iget-object v4, v0, LX/6mt;->c:Ljava/util/concurrent/Executor;

    const p0, 0x5fa5004d

    invoke-static {v4, v1, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1146999
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1146986
    iget-object v0, p0, LX/6my;->b:LX/6mt;

    .line 1146987
    iget-object v1, v0, LX/6mt;->c:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$9;

    invoke-direct {v2, v0}, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$9;-><init>(LX/6mt;)V

    const p0, -0x23efb22f

    invoke-static {v1, v2, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1146988
    return-void
.end method

.method public final b(Ljava/util/List;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1146979
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 1146980
    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1146981
    sget-object v1, LX/6my;->a:Ljava/lang/String;

    const-string v2, "Unsubscribe topics=%s, msgId=%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146982
    iget-object v1, p0, LX/6my;->b:LX/6mt;

    .line 1146983
    new-instance v2, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$3;

    invoke-direct {v2, v1, v0, p2}, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$3;-><init>(LX/6mt;[Ljava/lang/String;I)V

    .line 1146984
    iget-object v3, v1, LX/6mt;->c:Ljava/util/concurrent/Executor;

    const v4, -0x3f2ed5f1

    invoke-static {v3, v2, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1146985
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1146974
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "pingOnce"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146975
    iget-object v0, p0, LX/6my;->b:LX/6mt;

    .line 1146976
    new-instance v1, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$7;

    invoke-direct {v1, v0}, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$7;-><init>(LX/6mt;)V

    .line 1146977
    iget-object v2, v0, LX/6mt;->c:Ljava/util/concurrent/Executor;

    const p0, 0x24bcc7ad

    invoke-static {v2, v1, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1146978
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1146969
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "pingResp"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146970
    iget-object v0, p0, LX/6my;->b:LX/6mt;

    .line 1146971
    new-instance v1, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$8;

    invoke-direct {v1, v0}, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$8;-><init>(LX/6mt;)V

    .line 1146972
    iget-object v2, v0, LX/6mt;->c:Ljava/util/concurrent/Executor;

    const p0, -0xc35b387

    invoke-static {v2, v1, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1146973
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1146968
    const-string v0, ""

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1146967
    const-string v0, ""

    return-object v0
.end method

.method public final g()B
    .locals 1

    .prologue
    .line 1146938
    const/4 v0, 0x4

    return v0
.end method
