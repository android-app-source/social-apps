.class public LX/7j6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/17Y;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1229158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1229159
    iput-object p1, p0, LX/7j6;->a:Landroid/content/Context;

    .line 1229160
    iput-object p2, p0, LX/7j6;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1229161
    iput-object p3, p0, LX/7j6;->c:LX/17Y;

    .line 1229162
    return-void
.end method

.method public static a(LX/0QB;)LX/7j6;
    .locals 6

    .prologue
    .line 1229181
    const-class v1, LX/7j6;

    monitor-enter v1

    .line 1229182
    :try_start_0
    sget-object v0, LX/7j6;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1229183
    sput-object v2, LX/7j6;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1229184
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1229185
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1229186
    new-instance p0, LX/7j6;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-direct {p0, v3, v4, v5}, LX/7j6;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    .line 1229187
    move-object v0, p0

    .line 1229188
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1229189
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/7j6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1229190
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1229191
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Landroid/content/Intent;J)Landroid/content/Intent;
    .locals 1
    .param p0    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1229178
    if-eqz p0, :cond_0

    .line 1229179
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p0, v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1229180
    :cond_0
    return-object p0
.end method

.method private static a(LX/7j6;JLjava/util/Currency;LX/7ja;ILcom/facebook/auth/viewercontext/ViewerContext;Z)V
    .locals 3
    .param p3    # Ljava/util/Currency;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1229168
    if-eqz p4, :cond_0

    invoke-interface {p4}, LX/7ja;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p2, v0}, LX/7j6;->b(LX/7j6;JLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1229169
    :goto_0
    if-nez v0, :cond_1

    .line 1229170
    :goto_1
    return-void

    .line 1229171
    :cond_0
    invoke-direct {p0, p1, p2}, LX/7j6;->c(J)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 1229172
    :cond_1
    const-string v1, "extra_currency"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1229173
    const-string v1, "extra_admin_product_item"

    invoke-static {v0, v1, p4}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1229174
    const-string v1, "extra_featured_products_count"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1229175
    const-string v1, "extra_has_empty_catalog"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1229176
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1229177
    iget-object v1, p0, LX/7j6;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/7j6;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1
.end method

.method public static b(LX/7j6;JLjava/lang/String;)Landroid/content/Intent;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1229163
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1229164
    sget-object v0, LX/0ax;->fK:Ljava/lang/String;

    invoke-static {v0, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1229165
    iget-object v1, p0, LX/7j6;->c:LX/17Y;

    iget-object v2, p0, LX/7j6;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1229166
    invoke-static {v0, p1, p2}, LX/7j6;->a(Landroid/content/Intent;J)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 1229167
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(J)Landroid/content/Intent;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1229156
    iget-object v0, p0, LX/7j6;->c:LX/17Y;

    iget-object v1, p0, LX/7j6;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->fL:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1229157
    invoke-static {v0, p1, p2}, LX/7j6;->a(Landroid/content/Intent;J)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 1229152
    sget-object v0, LX/0ax;->fO:Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1229153
    iget-object v1, p0, LX/7j6;->c:LX/17Y;

    iget-object v2, p0, LX/7j6;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1229154
    iget-object v1, p0, LX/7j6;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/7j6;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1229155
    return-void
.end method

.method public final a(JLjava/util/Currency;ILcom/facebook/auth/viewercontext/ViewerContext;I)V
    .locals 9

    .prologue
    .line 1229192
    if-nez p6, :cond_0

    const/4 v8, 0x1

    .line 1229193
    :goto_0
    const/4 v5, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v6, p4

    move-object v7, p5

    invoke-static/range {v1 .. v8}, LX/7j6;->a(LX/7j6;JLjava/util/Currency;LX/7ja;ILcom/facebook/auth/viewercontext/ViewerContext;Z)V

    .line 1229194
    return-void

    .line 1229195
    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public final a(JLjava/util/Currency;LX/7ja;ILcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 9
    .param p4    # LX/7ja;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1229150
    const/4 v8, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    invoke-static/range {v1 .. v8}, LX/7j6;->a(LX/7j6;JLjava/util/Currency;LX/7ja;ILcom/facebook/auth/viewercontext/ViewerContext;Z)V

    .line 1229151
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1229146
    sget-object v0, LX/0ax;->fN:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1229147
    iget-object v1, p0, LX/7j6;->c:LX/17Y;

    invoke-interface {v1, p1, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1229148
    iget-object v1, p0, LX/7j6;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1229149
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1229116
    sget-object v0, LX/0ax;->fO:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1229117
    iget-object v1, p0, LX/7j6;->c:LX/17Y;

    invoke-interface {v1, p1, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1229118
    const-string v1, "extra_finish_on_launch_view_shop"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1229119
    iget-object v1, p0, LX/7j6;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1229120
    return-void
.end method

.method public final a(Ljava/lang/String;LX/7iP;)V
    .locals 3

    .prologue
    .line 1229141
    sget-object v0, LX/0ax;->fP:Ljava/lang/String;

    const-string v1, "0"

    iget-object v2, p2, LX/7iP;->value:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1229142
    iget-object v1, p0, LX/7j6;->c:LX/17Y;

    iget-object v2, p0, LX/7j6;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1229143
    if-nez v0, :cond_0

    .line 1229144
    :goto_0
    return-void

    .line 1229145
    :cond_0
    iget-object v1, p0, LX/7j6;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/7j6;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1229132
    sget-object v0, LX/0ax;->fT:Ljava/lang/String;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "unknown"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, LX/7j6;->a:Landroid/content/Context;

    const v4, 0x7f0814cb

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "0"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1229133
    iget-object v1, p0, LX/7j6;->c:LX/17Y;

    iget-object v2, p0, LX/7j6;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1229134
    if-nez v0, :cond_0

    .line 1229135
    :goto_0
    return-void

    .line 1229136
    :cond_0
    if-eqz p2, :cond_1

    .line 1229137
    const-string v1, "merchant_page_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1229138
    :cond_1
    if-eqz p3, :cond_2

    .line 1229139
    const-string v1, "arg_init_product_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1229140
    :cond_2
    iget-object v1, p0, LX/7j6;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/7j6;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ZLX/7iP;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/7iP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1229125
    sget-object v1, LX/0ax;->fS:Ljava/lang/String;

    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p1, v2, v0

    const/4 v0, 0x1

    const-string v3, "0"

    aput-object v3, v2, v0

    const/4 v3, 0x2

    if-nez p4, :cond_1

    const-string v0, "unknown"

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x3

    if-nez p2, :cond_0

    const-string p2, "0"

    :cond_0
    aput-object p2, v2, v0

    const/4 v0, 0x4

    const-string v3, "0"

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1229126
    iget-object v1, p0, LX/7j6;->c:LX/17Y;

    iget-object v2, p0, LX/7j6;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1229127
    if-nez v0, :cond_2

    .line 1229128
    :goto_1
    return-void

    .line 1229129
    :cond_1
    iget-object v0, p4, LX/7iP;->value:Ljava/lang/String;

    goto :goto_0

    .line 1229130
    :cond_2
    const-string v1, "extra_finish_on_launch_edit_shop"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1229131
    iget-object v1, p0, LX/7j6;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/7j6;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1
.end method

.method public final b(J)Landroid/content/Intent;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1229121
    invoke-direct {p0, p1, p2}, LX/7j6;->c(J)Landroid/content/Intent;

    move-result-object v0

    .line 1229122
    if-eqz v0, :cond_0

    .line 1229123
    const-string v1, "extra_requires_initial_fetch"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1229124
    :cond_0
    return-object v0
.end method
