.class public LX/7vq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ds;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:LX/6tk;

.field private final c:LX/6tK;

.field public final d:LX/7xH;

.field public final e:LX/7xQ;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/6tk;LX/6tK;LX/7xH;LX/7xQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1275162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275163
    iput-object p1, p0, LX/7vq;->a:Landroid/content/res/Resources;

    .line 1275164
    iput-object p2, p0, LX/7vq;->b:LX/6tk;

    .line 1275165
    iput-object p3, p0, LX/7vq;->c:LX/6tK;

    .line 1275166
    iput-object p4, p0, LX/7vq;->d:LX/7xH;

    .line 1275167
    iput-object p5, p0, LX/7vq;->e:LX/7xQ;

    .line 1275168
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/CheckoutData;",
            ")",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1275161
    iget-object v0, p0, LX/7vq;->c:LX/6tK;

    invoke-virtual {v0, p1}, LX/6tK;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/CheckoutData;",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1275155
    iget-object v0, p0, LX/7vq;->c:LX/6tK;

    iget-object v1, p0, LX/7vq;->b:LX/6tk;

    .line 1275156
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->p()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275157
    if-nez v2, :cond_0

    .line 1275158
    const v2, 0x7f081d47

    .line 1275159
    :goto_0
    move v2, v2

    .line 1275160
    invoke-virtual {v1, p1, v2}, LX/6tk;->a(Lcom/facebook/payments/checkout/model/CheckoutData;I)LX/6sw;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, LX/6tK;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;LX/6sw;)LX/0Px;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object p0, v2, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    iget-object v2, v2, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->d:Ljava/lang/String;

    invoke-static {p0, v2}, LX/7xE;->b(LX/0Px;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f081fc6

    goto :goto_0

    :cond_1
    const v2, 0x7f081d47

    goto :goto_0
.end method

.method public final a(LX/6so;Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6E3;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1275129
    sget-object v0, LX/7vp;->a:[I

    invoke-virtual {p1}, LX/6so;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1275130
    iget-object v0, p0, LX/7vq;->c:LX/6tK;

    invoke-virtual {v0, p1, p2}, LX/6tK;->a(LX/6so;Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6E3;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1275131
    :pswitch_0
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->p()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275132
    if-nez v0, :cond_0

    .line 1275133
    const/4 v0, 0x0

    .line 1275134
    :goto_1
    move-object v0, v0

    .line 1275135
    goto :goto_0

    .line 1275136
    :pswitch_1
    const/4 v1, 0x0

    .line 1275137
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->p()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275138
    if-nez v0, :cond_2

    move-object v0, v1

    .line 1275139
    :goto_2
    move-object v0, v0

    .line 1275140
    goto :goto_0

    .line 1275141
    :cond_0
    iget-object v1, p0, LX/7vq;->e:LX/7xQ;

    invoke-virtual {v1, v0}, LX/7xQ;->a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)LX/7xP;

    move-result-object v1

    .line 1275142
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1275143
    iget-object v3, v1, LX/7xP;->b:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1275144
    iget-object v3, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->d:Ljava/lang/String;

    invoke-static {v3, v0}, LX/7xE;->b(LX/0Px;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1275145
    iget-object v0, v1, LX/7xP;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1275146
    iget-object v1, p0, LX/7vq;->d:LX/7xH;

    invoke-virtual {v1, v0}, LX/7xH;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v1

    .line 1275147
    new-instance v3, LX/73b;

    iget-object p1, p0, LX/7vq;->a:Landroid/content/res/Resources;

    const p2, 0x7f081d45

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x1

    invoke-direct {v3, p1, v1, p2}, LX/73b;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v0, v3

    .line 1275148
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1275149
    :cond_1
    new-instance v0, LX/6t8;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6t8;-><init>(LX/0Px;)V

    goto :goto_1

    .line 1275150
    :cond_2
    iget-object v2, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    iget-object v3, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/7xE;->b(LX/0Px;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1275151
    iget-object v2, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1275152
    :goto_3
    if-eqz v2, :cond_4

    new-instance v1, LX/7wB;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->l:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, LX/7wB;-><init>(Ljava/lang/String;LX/3Ab;)V

    move-object v0, v1

    goto :goto_2

    .line 1275153
    :cond_3
    iget-object v2, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    goto :goto_3

    :cond_4
    move-object v0, v1

    .line 1275154
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
