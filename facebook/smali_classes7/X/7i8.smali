.class public LX/7i8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/RequestUpdateProductHistoryJSBridgeCall;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/1Bf;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Bf;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1227081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1227082
    iput-object p1, p0, LX/7i8;->a:Landroid/content/Context;

    .line 1227083
    iput-object p2, p0, LX/7i8;->b:LX/1Bf;

    .line 1227084
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1227096
    const-string v0, "requestProductUrl"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 4

    .prologue
    .line 1227085
    check-cast p1, Lcom/facebook/browserextensions/ipc/RequestUpdateProductHistoryJSBridgeCall;

    .line 1227086
    const-string v0, "productUrl"

    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v0, v0

    .line 1227087
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1227088
    :goto_0
    return-void

    .line 1227089
    :cond_0
    const-string v1, "browserLocale"

    invoke-virtual {p1, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v1, v1

    .line 1227090
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1227091
    const-string v3, "EXTRA_IX_PRODUCT_URL"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227092
    const-string v0, "EXTRA_IX_PRODUCT_LOCALE"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227093
    iget-object v0, p0, LX/7i8;->b:LX/1Bf;

    iget-object v1, p0, LX/7i8;->a:Landroid/content/Context;

    .line 1227094
    invoke-static {v0}, LX/1Bf;->c(LX/1Bf;)Z

    move-result v3

    invoke-static {v1, v3, v2}, LX/049;->d(Landroid/content/Context;ZLandroid/os/Bundle;)V

    .line 1227095
    goto :goto_0
.end method
