.class public final LX/70p;
.super LX/6qh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/picker/PickerScreenFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/picker/PickerScreenFragment;)V
    .locals 0

    .prologue
    .line 1162810
    iput-object p1, p0, LX/70p;->a:Lcom/facebook/payments/picker/PickerScreenFragment;

    invoke-direct {p0}, LX/6qh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/73T;)V
    .locals 7

    .prologue
    .line 1162783
    iget-object v0, p0, LX/70p;->a:Lcom/facebook/payments/picker/PickerScreenFragment;

    .line 1162784
    sget-object v1, LX/70s;->a:[I

    .line 1162785
    iget-object v2, p1, LX/73T;->a:LX/73S;

    move-object v2, v2

    .line 1162786
    invoke-virtual {v2}, LX/73S;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1162787
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1162788
    :pswitch_1
    const-string v1, "extra_reset_data"

    invoke-virtual {p1, v1}, LX/73T;->a(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 1162789
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1162790
    instance-of v2, v1, Lcom/facebook/payments/picker/model/CoreClientData;

    if-eqz v2, :cond_1

    .line 1162791
    iget-object v2, v0, Lcom/facebook/payments/picker/PickerScreenFragment;->u:LX/6vv;

    iget-object v3, v0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    check-cast v1, Lcom/facebook/payments/picker/model/CoreClientData;

    invoke-virtual {v2, v3, v1}, LX/6vv;->a(Lcom/facebook/payments/picker/model/PickerRunTimeData;Lcom/facebook/payments/picker/model/CoreClientData;)V

    goto :goto_0

    .line 1162792
    :cond_1
    instance-of v2, v1, Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    if-eqz v2, :cond_0

    .line 1162793
    iget-object v2, v0, Lcom/facebook/payments/picker/PickerScreenFragment;->u:LX/6vv;

    iget-object v3, v0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    check-cast v1, Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    .line 1162794
    check-cast v3, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;

    invoke-virtual {v2, v3, v1}, LX/6vv;->a(Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;)V

    .line 1162795
    goto :goto_0

    .line 1162796
    :pswitch_2
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Landroid/app/Activity;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 1162797
    if-eqz v1, :cond_0

    .line 1162798
    const-string v2, "extra_activity_result_data"

    invoke-virtual {p1, v2}, LX/73T;->a(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    .line 1162799
    invoke-static {v0, v2}, Lcom/facebook/payments/picker/PickerScreenFragment;->a(Lcom/facebook/payments/picker/PickerScreenFragment;Landroid/content/Intent;)V

    .line 1162800
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 1162801
    :pswitch_3
    iget-object v2, v0, Lcom/facebook/payments/picker/PickerScreenFragment;->u:LX/6vv;

    iget-object v3, v0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    const-string v1, "extra_section_type"

    invoke-virtual {p1, v1}, LX/73T;->b(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LX/6vZ;

    const-string v4, "extra_user_action"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, LX/73T;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1162802
    check-cast v3, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;

    .line 1162803
    iget-object v5, v2, LX/6vv;->a:LX/70m;

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1162804
    iget-object v5, v2, LX/6vv;->a:LX/70m;

    invoke-virtual {v3}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v6

    .line 1162805
    iget-object p0, v3, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    move-object p0, p0

    .line 1162806
    iget-object v0, v3, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 1162807
    iget-object p1, v3, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->d:LX/0P1;

    move-object p1, p1

    .line 1162808
    invoke-static {p1, v1, v4}, LX/6vv;->a(LX/0P1;LX/6vZ;Ljava/lang/String;)LX/0P1;

    move-result-object p1

    invoke-virtual {v2, v6, p0, v0, p1}, LX/6vv;->a(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)Lcom/facebook/payments/picker/model/PickerRunTimeData;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/70m;->b(Lcom/facebook/payments/picker/model/PickerRunTimeData;)V

    .line 1162809
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 1162781
    iget-object v0, p0, LX/70p;->a:Lcom/facebook/payments/picker/PickerScreenFragment;

    iget-object v0, v0, Lcom/facebook/payments/picker/PickerScreenFragment;->l:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/70p;->a:Lcom/facebook/payments/picker/PickerScreenFragment;

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1162782
    return-void
.end method

.method public final a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V
    .locals 2

    .prologue
    .line 1162775
    iget-object v0, p0, LX/70p;->a:Lcom/facebook/payments/picker/PickerScreenFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbListFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "payments_dialog_fragment"

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1162776
    return-void
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1162779
    iget-object v0, p0, LX/70p;->a:Lcom/facebook/payments/picker/PickerScreenFragment;

    iget-object v0, v0, Lcom/facebook/payments/picker/PickerScreenFragment;->l:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/70p;->a:Lcom/facebook/payments/picker/PickerScreenFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1162780
    return-void
.end method

.method public final b(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 1162777
    iget-object v0, p0, LX/70p;->a:Lcom/facebook/payments/picker/PickerScreenFragment;

    iget-object v0, v0, Lcom/facebook/payments/picker/PickerScreenFragment;->l:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/70p;->a:Lcom/facebook/payments/picker/PickerScreenFragment;

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1162778
    return-void
.end method
