.class public LX/8bK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0oz;

.field private b:LX/0p3;


# direct methods
.method public constructor <init>(LX/0oz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1371915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1371916
    iput-object p1, p0, LX/8bK;->a:LX/0oz;

    .line 1371917
    return-void
.end method

.method public static a(LX/0QB;)LX/8bK;
    .locals 4

    .prologue
    .line 1371918
    const-class v1, LX/8bK;

    monitor-enter v1

    .line 1371919
    :try_start_0
    sget-object v0, LX/8bK;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1371920
    sput-object v2, LX/8bK;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1371921
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1371922
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1371923
    new-instance p0, LX/8bK;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v3

    check-cast v3, LX/0oz;

    invoke-direct {p0, v3}, LX/8bK;-><init>(LX/0oz;)V

    .line 1371924
    move-object v0, p0

    .line 1371925
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1371926
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8bK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1371927
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1371928
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0p3;
    .locals 1

    .prologue
    .line 1371929
    iget-object v0, p0, LX/8bK;->b:LX/0p3;

    if-nez v0, :cond_0

    .line 1371930
    iget-object v0, p0, LX/8bK;->a:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    iput-object v0, p0, LX/8bK;->b:LX/0p3;

    .line 1371931
    :cond_0
    iget-object v0, p0, LX/8bK;->b:LX/0p3;

    return-object v0
.end method
