.class public final LX/7jp;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1230485
    const-class v1, Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;

    const v0, 0x670e058

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "CommerceStoreCreateMutation"

    const-string v6, "85c198dc47c1eb2df81aa29eeed1ce59"

    const-string v7, "commerce_contact_merchant_store_create"

    const-string v8, "4"

    const-string v9, "10155069963206729"

    const/4 v10, 0x0

    .line 1230486
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1230487
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1230488
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1230489
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1230490
    sparse-switch v0, :sswitch_data_0

    .line 1230491
    :goto_0
    return-object p1

    .line 1230492
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1230493
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1230494
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1230495
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1230496
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1230497
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6ef528d5 -> :sswitch_0
        -0x2a0a3d40 -> :sswitch_3
        0x101fb19 -> :sswitch_2
        0x5fb57ca -> :sswitch_4
        0x683094a -> :sswitch_5
        0x1d1017c1 -> :sswitch_1
    .end sparse-switch
.end method
