.class public final enum LX/8Ly;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Ly;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Ly;

.field public static final enum FETCH_DRAFTS:LX/8Ly;

.field public static final enum FETCH_FATAL:LX/8Ly;

.field public static final enum FETCH_PENDING:LX/8Ly;

.field public static final enum FETCH_UPLOADED:LX/8Ly;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1334108
    new-instance v0, LX/8Ly;

    const-string v1, "FETCH_PENDING"

    invoke-direct {v0, v1, v2}, LX/8Ly;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Ly;->FETCH_PENDING:LX/8Ly;

    .line 1334109
    new-instance v0, LX/8Ly;

    const-string v1, "FETCH_UPLOADED"

    invoke-direct {v0, v1, v3}, LX/8Ly;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Ly;->FETCH_UPLOADED:LX/8Ly;

    .line 1334110
    new-instance v0, LX/8Ly;

    const-string v1, "FETCH_DRAFTS"

    invoke-direct {v0, v1, v4}, LX/8Ly;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Ly;->FETCH_DRAFTS:LX/8Ly;

    .line 1334111
    new-instance v0, LX/8Ly;

    const-string v1, "FETCH_FATAL"

    invoke-direct {v0, v1, v5}, LX/8Ly;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Ly;->FETCH_FATAL:LX/8Ly;

    .line 1334112
    const/4 v0, 0x4

    new-array v0, v0, [LX/8Ly;

    sget-object v1, LX/8Ly;->FETCH_PENDING:LX/8Ly;

    aput-object v1, v0, v2

    sget-object v1, LX/8Ly;->FETCH_UPLOADED:LX/8Ly;

    aput-object v1, v0, v3

    sget-object v1, LX/8Ly;->FETCH_DRAFTS:LX/8Ly;

    aput-object v1, v0, v4

    sget-object v1, LX/8Ly;->FETCH_FATAL:LX/8Ly;

    aput-object v1, v0, v5

    sput-object v0, LX/8Ly;->$VALUES:[LX/8Ly;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1334113
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Ly;
    .locals 1

    .prologue
    .line 1334114
    const-class v0, LX/8Ly;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Ly;

    return-object v0
.end method

.method public static values()[LX/8Ly;
    .locals 1

    .prologue
    .line 1334115
    sget-object v0, LX/8Ly;->$VALUES:[LX/8Ly;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Ly;

    return-object v0
.end method
