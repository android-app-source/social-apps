.class public final LX/75a;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;)LX/15i;
    .locals 13

    .prologue
    .line 1169717
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1169718
    const-wide/16 v6, 0x0

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 1169719
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_5

    .line 1169720
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1169721
    :goto_0
    move v1, v3

    .line 1169722
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1169723
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    return-object v0

    .line 1169724
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_3

    .line 1169725
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1169726
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1169727
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 1169728
    const-string v11, "expiration_time"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1169729
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v4

    move v2, v8

    goto :goto_1

    .line 1169730
    :cond_1
    const-string v11, "token"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1169731
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1169732
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1169733
    :cond_3
    const/4 v10, 0x2

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1169734
    if-eqz v2, :cond_4

    move-object v2, v0

    .line 1169735
    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1169736
    :cond_4
    invoke-virtual {v0, v8, v9}, LX/186;->b(II)V

    .line 1169737
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_0

    :cond_5
    move v2, v3

    move v9, v3

    move-wide v4, v6

    goto :goto_1
.end method
