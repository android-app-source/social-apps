.class public final LX/8S1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0Px",
        "<",
        "LX/8QL;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/HashSet;

.field public final synthetic b:LX/0Pz;

.field public final synthetic c:Ljava/util/HashSet;

.field public final synthetic d:LX/0Pz;

.field public final synthetic e:Lcom/facebook/privacy/selector/CustomPrivacyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/CustomPrivacyFragment;Ljava/util/HashSet;LX/0Pz;Ljava/util/HashSet;LX/0Pz;)V
    .locals 0

    .prologue
    .line 1345839
    iput-object p1, p0, LX/8S1;->e:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    iput-object p2, p0, LX/8S1;->a:Ljava/util/HashSet;

    iput-object p3, p0, LX/8S1;->b:LX/0Pz;

    iput-object p4, p0, LX/8S1;->c:Ljava/util/HashSet;

    iput-object p5, p0, LX/8S1;->d:LX/0Pz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1345840
    iget-object v0, p0, LX/8S1;->e:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->h:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    sget-object v1, LX/3Oq;->FRIENDS:LX/0Px;

    .line 1345841
    iput-object v1, v0, LX/2RR;->c:Ljava/util/Collection;

    .line 1345842
    move-object v0, v0

    .line 1345843
    sget-object v1, LX/2RS;->NAME:LX/2RS;

    .line 1345844
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 1345845
    move-object v0, v0

    .line 1345846
    const/4 v1, 0x0

    .line 1345847
    iput-boolean v1, v0, LX/2RR;->o:Z

    .line 1345848
    move-object v0, v0

    .line 1345849
    iget-object v1, p0, LX/8S1;->e:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->c:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 1345850
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, LX/3On;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1345851
    invoke-interface {v1}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-static {v0}, LX/8uo;->a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 1345852
    iget-object v2, p0, LX/8S1;->a:Ljava/util/HashSet;

    .line 1345853
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1345854
    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1345855
    iget-object v2, p0, LX/8S1;->b:LX/0Pz;

    new-instance v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-direct {v3, v0}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/User;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345856
    :cond_1
    iget-object v2, p0, LX/8S1;->c:Ljava/util/HashSet;

    .line 1345857
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1345858
    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1345859
    iget-object v2, p0, LX/8S1;->d:LX/0Pz;

    new-instance v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-direct {v3, v0}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/User;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1345860
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/3On;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, LX/3On;->close()V

    .line 1345861
    const/4 v0, 0x0

    return-object v0
.end method
