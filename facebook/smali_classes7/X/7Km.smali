.class public final LX/7Km;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/player/CountdownRingContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/CountdownRingContainer;)V
    .locals 2

    .prologue
    .line 1196365
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1196366
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/7Km;->a:Ljava/lang/ref/WeakReference;

    .line 1196367
    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    .line 1196350
    iget-object v0, p0, LX/7Km;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/CountdownRingContainer;

    .line 1196351
    if-nez v0, :cond_0

    .line 1196352
    :goto_0
    return-void

    .line 1196353
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/video/player/CountdownRingContainer;->invalidate()V

    .line 1196354
    invoke-static {v0}, Lcom/facebook/video/player/CountdownRingContainer;->getElapsedMillisSinceCountdownStart(Lcom/facebook/video/player/CountdownRingContainer;)J

    move-result-wide v2

    iget-wide v4, v0, Lcom/facebook/video/player/CountdownRingContainer;->l:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    .line 1196355
    invoke-virtual {p0}, LX/7Km;->b()V

    .line 1196356
    invoke-static {v0}, Lcom/facebook/video/player/CountdownRingContainer;->d(Lcom/facebook/video/player/CountdownRingContainer;)V

    goto :goto_0

    .line 1196357
    :cond_1
    invoke-virtual {p0}, LX/7Km;->a()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1196358
    const/4 v0, 0x1

    const-wide/16 v2, 0x10

    invoke-virtual {p0, v0, v2, v3}, LX/7Km;->sendEmptyMessageDelayed(IJ)Z

    .line 1196359
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1196360
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/7Km;->removeMessages(I)V

    .line 1196361
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 1196362
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1196363
    :goto_0
    return-void

    .line 1196364
    :pswitch_0
    invoke-direct {p0}, LX/7Km;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
