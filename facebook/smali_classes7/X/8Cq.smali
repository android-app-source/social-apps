.class public final LX/8Cq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/net/Uri;

.field public c:I

.field public d:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field public e:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1311857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1311858
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)LX/8Cq;
    .locals 2

    .prologue
    .line 1311859
    iget-object v0, p0, LX/8Cq;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Thumbnail Uri is already defined."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1311860
    iput-object p1, p0, LX/8Cq;->b:Landroid/net/Uri;

    .line 1311861
    return-object p0

    .line 1311862
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)LX/8Cq;
    .locals 2

    .prologue
    .line 1311863
    iget-object v0, p0, LX/8Cq;->b:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Thumbnail Uri is already defined."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1311864
    iput-object p1, p0, LX/8Cq;->a:Ljava/lang/String;

    .line 1311865
    return-object p0

    .line 1311866
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()LX/8Cr;
    .locals 6

    .prologue
    .line 1311867
    new-instance v0, LX/8Cr;

    iget-object v1, p0, LX/8Cq;->a:Ljava/lang/String;

    iget-object v2, p0, LX/8Cq;->b:Landroid/net/Uri;

    iget v3, p0, LX/8Cq;->c:I

    iget v4, p0, LX/8Cq;->d:I

    iget v5, p0, LX/8Cq;->e:I

    invoke-direct/range {v0 .. v5}, LX/8Cr;-><init>(Ljava/lang/String;Landroid/net/Uri;III)V

    return-object v0
.end method
