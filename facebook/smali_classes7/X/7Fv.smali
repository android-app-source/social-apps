.class public LX/7Fv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/7Fu;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile b:LX/7Fv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1188564
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/7Fv;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1188565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/7Fv;
    .locals 3

    .prologue
    .line 1188566
    sget-object v0, LX/7Fv;->b:LX/7Fv;

    if-nez v0, :cond_1

    .line 1188567
    const-class v1, LX/7Fv;

    monitor-enter v1

    .line 1188568
    :try_start_0
    sget-object v0, LX/7Fv;->b:LX/7Fv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1188569
    if-eqz v2, :cond_0

    .line 1188570
    :try_start_1
    new-instance v0, LX/7Fv;

    invoke-direct {v0}, LX/7Fv;-><init>()V

    .line 1188571
    move-object v0, v0

    .line 1188572
    sput-object v0, LX/7Fv;->b:LX/7Fv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1188573
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1188574
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1188575
    :cond_1
    sget-object v0, LX/7Fv;->b:LX/7Fv;

    return-object v0

    .line 1188576
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1188577
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1188578
    sget-object v0, LX/7Fv;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
