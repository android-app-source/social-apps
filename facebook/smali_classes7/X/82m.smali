.class public LX/82m;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0ih;


# instance fields
.field public final b:LX/0if;

.field public final c:LX/5Ou;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1288454
    sget-object v0, LX/0ig;->t:LX/0ih;

    sput-object v0, LX/82m;->a:LX/0ih;

    return-void
.end method

.method public constructor <init>(LX/0if;LX/5Ou;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1288455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1288456
    iput-object p1, p0, LX/82m;->b:LX/0if;

    .line 1288457
    iput-object p2, p0, LX/82m;->c:LX/5Ou;

    .line 1288458
    return-void
.end method

.method public static a(LX/82m;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1288459
    iget-object v0, p0, LX/82m;->b:LX/0if;

    sget-object v1, LX/82m;->a:LX/0ih;

    invoke-virtual {v0, v1, p1, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 1288460
    return-void
.end method

.method public static b(LX/0QB;)LX/82m;
    .locals 3

    .prologue
    .line 1288461
    new-instance v2, LX/82m;

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    invoke-static {p0}, LX/5Ou;->a(LX/0QB;)LX/5Ou;

    move-result-object v1

    check-cast v1, LX/5Ou;

    invoke-direct {v2, v0, v1}, LX/82m;-><init>(LX/0if;LX/5Ou;)V

    .line 1288462
    return-object v2
.end method


# virtual methods
.method public final a(LX/0Px;LX/8s1;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "LX/8s1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1288463
    iget-object v0, p0, LX/82m;->b:LX/0if;

    sget-object v1, LX/82m;->a:LX/0ih;

    const-string v2, "reactors_loaded"

    const-string v3, "reactors_extra_payload"

    const/4 v4, 0x0

    .line 1288464
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v8

    .line 1288465
    const-string v5, "inverted_button_color"

    iget-object v6, p0, LX/82m;->c:LX/5Ou;

    invoke-virtual {v6}, LX/5Ou;->d()Z

    move-result v6

    invoke-virtual {v8, v5, v6}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    .line 1288466
    const-string v5, "button_type"

    .line 1288467
    invoke-static {p2}, LX/8s1;->isCommentMentionSupported(LX/8s1;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, LX/82m;->c:LX/5Ou;

    invoke-virtual {v6}, LX/5Ou;->a()Z

    move-result v6

    if-nez v6, :cond_3

    .line 1288468
    :cond_0
    const-string v6, "friend_button"

    .line 1288469
    :goto_0
    move-object v6, v6

    .line 1288470
    invoke-virtual {v8, v5, v6}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 1288471
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v9

    move v7, v4

    move v5, v4

    move v6, v4

    :goto_1
    if-ge v7, v9, :cond_1

    invoke-virtual {p1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1288472
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v10

    .line 1288473
    sget-object v11, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v11, v10}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1288474
    add-int/lit8 v6, v6, 0x1

    .line 1288475
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->ay()I

    move-result v4

    if-lez v4, :cond_2

    .line 1288476
    add-int/lit8 v4, v5, 0x1

    move v5, v6

    .line 1288477
    :goto_2
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move v6, v5

    move v5, v4

    goto :goto_1

    .line 1288478
    :cond_1
    const-string v4, "num_friend"

    invoke-virtual {v8, v4, v6}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    .line 1288479
    const-string v4, "num_non_friend"

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v7

    sub-int v6, v7, v6

    invoke-virtual {v8, v4, v6}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    .line 1288480
    const-string v4, "num_friend_with_unread"

    invoke-virtual {v8, v4, v5}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    .line 1288481
    move-object v4, v8

    .line 1288482
    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1288483
    return-void

    :cond_2
    move v4, v5

    move v5, v6

    goto :goto_2

    .line 1288484
    :cond_3
    iget-object v6, p0, LX/82m;->c:LX/5Ou;

    invoke-virtual {v6}, LX/5Ou;->b()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1288485
    const-string v6, "message_button"

    goto :goto_0

    .line 1288486
    :cond_4
    iget-object v6, p0, LX/82m;->c:LX/5Ou;

    invoke-virtual {v6}, LX/5Ou;->c()Z

    move-result v6

    if-eqz v6, :cond_5

    const-string v6, "mention_button"

    goto :goto_0

    :cond_5
    const-string v6, "comment_button"

    goto :goto_0
.end method

.method public final a(LX/82l;LX/1zt;)V
    .locals 5

    .prologue
    .line 1288487
    iget-object v0, p0, LX/82m;->b:LX/0if;

    sget-object v1, LX/82m;->a:LX/0ih;

    invoke-virtual {p1}, LX/82l;->getAction()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reaction_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1288488
    iget v4, p2, LX/1zt;->e:I

    move v4, v4

    .line 1288489
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 1288490
    return-void
.end method
