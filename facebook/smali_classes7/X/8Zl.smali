.class public final LX/8Zl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1365241
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 1365242
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1365243
    :goto_0
    return v1

    .line 1365244
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1365245
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 1365246
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1365247
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1365248
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1365249
    const-string v9, "external_url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1365250
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1365251
    :cond_2
    const-string v9, "external_url_owning_profile"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1365252
    invoke-static {p0, p1}, LX/8Zf;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1365253
    :cond_3
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1365254
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1365255
    :cond_4
    const-string v9, "link_media"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1365256
    invoke-static {p0, p1}, LX/8Zh;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1365257
    :cond_5
    const-string v9, "relatedArticleInstantArticle"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1365258
    invoke-static {p0, p1}, LX/8Zi;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1365259
    :cond_6
    const-string v9, "summary"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1365260
    invoke-static {p0, p1}, LX/8Zj;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1365261
    :cond_7
    const-string v9, "title"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1365262
    invoke-static {p0, p1}, LX/8Zk;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1365263
    :cond_8
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1365264
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1365265
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1365266
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1365267
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1365268
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1365269
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1365270
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1365271
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1365272
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1365273
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365274
    if-eqz v0, :cond_0

    .line 1365275
    const-string v1, "external_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365276
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365277
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1365278
    if-eqz v0, :cond_1

    .line 1365279
    const-string v1, "external_url_owning_profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365280
    invoke-static {p0, v0, p2}, LX/8Zf;->a(LX/15i;ILX/0nX;)V

    .line 1365281
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365282
    if-eqz v0, :cond_2

    .line 1365283
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365284
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365285
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1365286
    if-eqz v0, :cond_3

    .line 1365287
    const-string v1, "link_media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365288
    invoke-static {p0, v0, p2, p3}, LX/8Zh;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1365289
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1365290
    if-eqz v0, :cond_4

    .line 1365291
    const-string v1, "relatedArticleInstantArticle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365292
    invoke-static {p0, v0, p2, p3}, LX/8Zi;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1365293
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1365294
    if-eqz v0, :cond_5

    .line 1365295
    const-string v1, "summary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365296
    invoke-static {p0, v0, p2}, LX/8Zj;->a(LX/15i;ILX/0nX;)V

    .line 1365297
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1365298
    if-eqz v0, :cond_6

    .line 1365299
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365300
    invoke-static {p0, v0, p2}, LX/8Zk;->a(LX/15i;ILX/0nX;)V

    .line 1365301
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1365302
    return-void
.end method
