.class public LX/7zZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1AX;


# instance fields
.field public final a:I

.field public final b:LX/0hB;

.field public final c:Landroid/graphics/Rect;

.field public final d:Landroid/graphics/Rect;

.field public e:I

.field public f:I

.field public g:Z

.field public h:I


# direct methods
.method public constructor <init>(LX/0hB;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1281206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1281207
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    .line 1281208
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/7zZ;->d:Landroid/graphics/Rect;

    .line 1281209
    iput v2, p0, LX/7zZ;->e:I

    .line 1281210
    iput v2, p0, LX/7zZ;->f:I

    .line 1281211
    const/16 v0, 0x3c

    iput v0, p0, LX/7zZ;->a:I

    .line 1281212
    iput-object p1, p0, LX/7zZ;->b:LX/0hB;

    .line 1281213
    return-void
.end method

.method public static a(LX/7zZ;Ljava/util/Set;I)Landroid/view/View;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(",
            "Ljava/util/Set",
            "<TV;>;I)TV;"
        }
    .end annotation

    .prologue
    .line 1281196
    const/4 v2, 0x0

    .line 1281197
    const/4 v1, -0x1

    .line 1281198
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1281199
    iget-object v4, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v4}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1281200
    iget-object v4, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    if-eqz v4, :cond_0

    .line 1281201
    if-eqz v2, :cond_2

    if-nez p2, :cond_1

    iget-object v4, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    if-lt v4, v1, :cond_2

    :cond_1
    const/4 v4, 0x1

    if-ne p2, v4, :cond_5

    iget-object v4, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    if-le v4, v1, :cond_5

    .line 1281202
    :cond_2
    if-nez p2, :cond_3

    iget-object v1, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    :goto_1
    move-object v2, v0

    .line 1281203
    goto :goto_0

    .line 1281204
    :cond_3
    iget-object v1, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    .line 1281205
    :cond_4
    return-object v2

    :cond_5
    move-object v0, v2

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/7zZ;
    .locals 2

    .prologue
    .line 1281194
    new-instance v1, LX/7zZ;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v0

    check-cast v0, LX/0hB;

    invoke-direct {v1, v0}, LX/7zZ;-><init>(LX/0hB;)V

    .line 1281195
    return-object v1
.end method

.method public static b(LX/7zZ;Ljava/util/Set;I)Landroid/view/View;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(",
            "Ljava/util/Set",
            "<TV;>;I)TV;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    .line 1281184
    const/4 v2, 0x0

    .line 1281185
    const/4 v1, -0x1

    .line 1281186
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1281187
    iget-object v4, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v4}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1281188
    iget-object v4, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    if-eqz v4, :cond_0

    .line 1281189
    if-eqz v2, :cond_2

    if-ne p2, v5, :cond_1

    iget-object v4, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-lt v4, v1, :cond_2

    :cond_1
    const/4 v4, 0x3

    if-ne p2, v4, :cond_5

    iget-object v4, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    if-le v4, v1, :cond_5

    .line 1281190
    :cond_2
    if-ne p2, v5, :cond_3

    iget-object v1, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    :goto_1
    move-object v2, v0

    .line 1281191
    goto :goto_0

    .line 1281192
    :cond_3
    iget-object v1, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    goto :goto_1

    .line 1281193
    :cond_4
    return-object v2

    :cond_5
    move-object v0, v2

    goto :goto_1
.end method

.method public static b(Landroid/view/View;)Landroid/view/ViewGroup;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1281177
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 1281178
    :goto_0
    if-eqz v1, :cond_1

    instance-of v0, v1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 1281179
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->shouldDelayChildPressedState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1281180
    check-cast v1, Landroid/view/ViewGroup;

    .line 1281181
    :goto_1
    return-object v1

    .line 1281182
    :cond_0
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_0

    .line 1281183
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static c(LX/7zZ;)V
    .locals 2

    .prologue
    .line 1281098
    iget-object v0, p0, LX/7zZ;->b:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    iput v0, p0, LX/7zZ;->h:I

    .line 1281099
    iget-object v0, p0, LX/7zZ;->b:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v0

    .line 1281100
    iget v1, p0, LX/7zZ;->a:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/7zZ;->e:I

    .line 1281101
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Set;Z)Landroid/view/View;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(",
            "Ljava/util/Set",
            "<TV;>;Z)TV;"
        }
    .end annotation

    .prologue
    .line 1281102
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1281103
    const/4 v0, 0x0

    .line 1281104
    :cond_0
    :goto_0
    return-object v0

    .line 1281105
    :cond_1
    const/4 v1, 0x0

    const/4 p2, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 1281106
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1281107
    :cond_2
    :goto_1
    move-object v0, v1

    .line 1281108
    if-eqz v0, :cond_0

    .line 1281109
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1281110
    iget-object v1, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1281111
    iget-object v1, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v3, v1, Landroid/graphics/Rect;->top:I

    .line 1281112
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1281113
    iget-object v5, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    invoke-virtual {v1, v5}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1281114
    iget-object v5, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    if-ne v3, v5, :cond_3

    .line 1281115
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1281116
    :cond_4
    move-object v0, v2

    .line 1281117
    const/4 v3, 0x0

    const/4 p2, 0x1

    .line 1281118
    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1281119
    :cond_5
    :goto_3
    move-object v0, v3

    .line 1281120
    goto :goto_0

    .line 1281121
    :cond_6
    iget-boolean v0, p0, LX/7zZ;->g:Z

    if-nez v0, :cond_7

    .line 1281122
    iget-object v0, p0, LX/7zZ;->b:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v0

    .line 1281123
    iget v2, p0, LX/7zZ;->f:I

    if-ne v2, v0, :cond_11

    .line 1281124
    :cond_7
    :goto_4
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v4

    :cond_8
    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1281125
    iget-object v3, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1281126
    iget-object v3, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    if-eqz v3, :cond_8

    .line 1281127
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-ne v3, v5, :cond_9

    move-object v1, v0

    .line 1281128
    goto :goto_1

    .line 1281129
    :cond_9
    iget-object v3, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v7, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v7

    div-int/lit8 v3, v3, 0x2

    .line 1281130
    iget-object v7, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v7

    .line 1281131
    iget v7, p0, LX/7zZ;->e:I

    sub-int/2addr v3, v7

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1281132
    if-eqz v1, :cond_a

    if-ge v3, v2, :cond_10

    :cond_a
    move v1, v3

    :goto_6
    move v2, v1

    move-object v1, v0

    .line 1281133
    goto :goto_5

    .line 1281134
    :cond_b
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1281135
    invoke-static {v1}, LX/7zZ;->b(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    .line 1281136
    if-eqz v0, :cond_2

    .line 1281137
    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->canScrollHorizontally(I)Z

    move-result v2

    if-nez v2, :cond_c

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->canScrollHorizontally(I)Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_c
    move v2, v5

    .line 1281138
    :goto_7
    if-eqz v2, :cond_d

    .line 1281139
    invoke-static {v0}, LX/7zZ;->b(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    .line 1281140
    :cond_d
    if-eqz v0, :cond_2

    .line 1281141
    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->canScrollVertically(I)Z

    move-result v2

    if-nez v2, :cond_f

    .line 1281142
    invoke-static {p0, p1, v4}, LX/7zZ;->a(LX/7zZ;Ljava/util/Set;I)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_1

    :cond_e
    move v2, v4

    .line 1281143
    goto :goto_7

    .line 1281144
    :cond_f
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->canScrollVertically(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1281145
    invoke-static {p0, p1, v5}, LX/7zZ;->a(LX/7zZ;Ljava/util/Set;I)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_1

    :cond_10
    move-object v0, v1

    move v1, v2

    goto :goto_6

    .line 1281146
    :cond_11
    iput v0, p0, LX/7zZ;->f:I

    .line 1281147
    iget v2, p0, LX/7zZ;->a:I

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/7zZ;->e:I

    goto/16 :goto_4

    .line 1281148
    :cond_12
    iget-boolean v1, p0, LX/7zZ;->g:Z

    if-eqz v1, :cond_14

    iget v1, p0, LX/7zZ;->h:I

    move v2, v1

    .line 1281149
    :goto_8
    div-int/lit8 v8, v2, 0x2

    .line 1281150
    const/4 v6, 0x0

    .line 1281151
    const/4 v4, 0x0

    .line 1281152
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_13
    :goto_9
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1281153
    iget-object v5, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    invoke-virtual {v1, v5}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1281154
    iget-object v5, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    if-eqz v5, :cond_13

    .line 1281155
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v5

    if-ne v5, p2, :cond_15

    move-object v3, v1

    .line 1281156
    goto/16 :goto_3

    .line 1281157
    :cond_14
    iget-object v1, p0, LX/7zZ;->b:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    move v2, v1

    goto :goto_8

    .line 1281158
    :cond_15
    iget-object v5, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v7

    add-int/2addr v5, v7

    if-lt v5, v2, :cond_17

    iget-object v5, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v5, v7

    .line 1281159
    :goto_a
    sub-int/2addr v5, v8

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v7

    .line 1281160
    iget-object v5, p0, LX/7zZ;->d:Landroid/graphics/Rect;

    invoke-virtual {v1, v5}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1281161
    iget-object v5, p0, LX/7zZ;->d:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 1281162
    iget-object p1, p0, LX/7zZ;->d:Landroid/graphics/Rect;

    invoke-virtual {v1, p1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1281163
    iget-object p1, p0, LX/7zZ;->d:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    .line 1281164
    if-nez p1, :cond_1b

    .line 1281165
    const/4 v5, 0x0

    .line 1281166
    :goto_b
    move v5, v5

    .line 1281167
    cmpg-float p1, v4, v5

    if-ltz p1, :cond_16

    cmpl-float p1, v4, v5

    if-nez p1, :cond_1a

    if-ge v7, v6, :cond_1a

    :cond_16
    move v3, v5

    move v4, v7

    :goto_c
    move v6, v4

    move v4, v3

    move-object v3, v1

    .line 1281168
    goto :goto_9

    .line 1281169
    :cond_17
    iget-object v5, p0, LX/7zZ;->c:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v5, v7

    goto :goto_a

    .line 1281170
    :cond_18
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1281171
    invoke-static {v3}, LX/7zZ;->b(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v1

    .line 1281172
    if-eqz v1, :cond_5

    .line 1281173
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->canScrollHorizontally(I)Z

    move-result v2

    if-nez v2, :cond_19

    .line 1281174
    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, LX/7zZ;->b(LX/7zZ;Ljava/util/Set;I)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_3

    .line 1281175
    :cond_19
    invoke-virtual {v1, p2}, Landroid/view/ViewGroup;->canScrollHorizontally(I)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1281176
    const/4 v1, 0x3

    invoke-static {p0, v0, v1}, LX/7zZ;->b(LX/7zZ;Ljava/util/Set;I)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_3

    :cond_1a
    move-object v1, v3

    move v3, v4

    move v4, v6

    goto :goto_c

    :cond_1b
    int-to-float v5, v5

    int-to-float p1, p1

    div-float/2addr v5, p1

    goto :goto_b
.end method
