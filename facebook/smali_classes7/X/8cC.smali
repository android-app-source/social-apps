.class public LX/8cC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Qe;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8cD;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Be;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1374073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1374074
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1374075
    iput-object v0, p0, LX/8cC;->a:LX/0Ot;

    .line 1374076
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1374077
    iput-object v0, p0, LX/8cC;->b:LX/0Ot;

    .line 1374078
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1374079
    iput-object v0, p0, LX/8cC;->c:LX/0Ot;

    .line 1374080
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1374081
    iput-object v0, p0, LX/8cC;->d:LX/0Ot;

    .line 1374082
    return-void
.end method

.method public static b(LX/8cC;)V
    .locals 2

    .prologue
    .line 1374083
    iget-object v0, p0, LX/8cC;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Qe;

    invoke-virtual {v0}, LX/3Qe;->b()V

    .line 1374084
    :try_start_0
    iget-object v0, p0, LX/8cC;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Be;

    invoke-virtual {v0}, LX/7Be;->a()V

    .line 1374085
    iget-object v0, p0, LX/8cC;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Qe;

    invoke-virtual {v0}, LX/3Qe;->c()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1374086
    return-void

    .line 1374087
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1374088
    iget-object v0, p0, LX/8cC;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Qe;

    invoke-virtual {v0}, LX/3Qe;->d()V

    .line 1374089
    throw v1
.end method
