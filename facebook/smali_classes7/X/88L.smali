.class public final LX/88L;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1302061
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1302062
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1302063
    :goto_0
    return v1

    .line 1302064
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1302065
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1302066
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1302067
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1302068
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1302069
    const-string v4, "__type__"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "__typename"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1302070
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    goto :goto_1

    .line 1302071
    :cond_3
    const-string v4, "profile_picture"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1302072
    const/4 v3, 0x0

    .line 1302073
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_9

    .line 1302074
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1302075
    :goto_2
    move v0, v3

    .line 1302076
    goto :goto_1

    .line 1302077
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1302078
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1302079
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1302080
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1302081
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1302082
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 1302083
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1302084
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1302085
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_7

    if-eqz v4, :cond_7

    .line 1302086
    const-string v5, "uri"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1302087
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_3

    .line 1302088
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1302089
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1302090
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v0, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1302091
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1302092
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1302093
    if-eqz v0, :cond_0

    .line 1302094
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1302095
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1302096
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1302097
    if-eqz v0, :cond_2

    .line 1302098
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1302099
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1302100
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1302101
    if-eqz v1, :cond_1

    .line 1302102
    const-string p1, "uri"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1302103
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1302104
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1302105
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1302106
    return-void
.end method
