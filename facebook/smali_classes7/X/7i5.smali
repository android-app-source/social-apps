.class public LX/7i5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CF;
.implements LX/6CA;
.implements LX/6CG;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/7i5;


# instance fields
.field public final a:LX/0if;

.field public final b:LX/0ih;

.field private final c:LX/03V;

.field private final d:LX/6DH;


# direct methods
.method public constructor <init>(LX/0if;LX/03V;LX/6DH;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1227026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1227027
    iput-object p1, p0, LX/7i5;->a:LX/0if;

    .line 1227028
    sget-object v0, LX/0ig;->aW:LX/0ih;

    iput-object v0, p0, LX/7i5;->b:LX/0ih;

    .line 1227029
    iput-object p2, p0, LX/7i5;->c:LX/03V;

    .line 1227030
    iput-object p3, p0, LX/7i5;->d:LX/6DH;

    .line 1227031
    return-void
.end method

.method public static a(LX/0QB;)LX/7i5;
    .locals 6

    .prologue
    .line 1227013
    sget-object v0, LX/7i5;->e:LX/7i5;

    if-nez v0, :cond_1

    .line 1227014
    const-class v1, LX/7i5;

    monitor-enter v1

    .line 1227015
    :try_start_0
    sget-object v0, LX/7i5;->e:LX/7i5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1227016
    if-eqz v2, :cond_0

    .line 1227017
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1227018
    new-instance p0, LX/7i5;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/6DH;->a(LX/0QB;)LX/6DH;

    move-result-object v5

    check-cast v5, LX/6DH;

    invoke-direct {p0, v3, v4, v5}, LX/7i5;-><init>(LX/0if;LX/03V;LX/6DH;)V

    .line 1227019
    move-object v0, p0

    .line 1227020
    sput-object v0, LX/7i5;->e:LX/7i5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1227021
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1227022
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1227023
    :cond_1
    sget-object v0, LX/7i5;->e:LX/7i5;

    return-object v0

    .line 1227024
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1227025
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(Landroid/os/Bundle;)LX/1rQ;
    .locals 3

    .prologue
    .line 1227012
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "page_id"

    const-string v2, "JS_BRIDGE_PAGE_ID"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "source"

    const-string v2, "JS_BRIDGE_LOG_SOURCE"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "ad_id"

    const-string v2, "JS_BRIDGE_AD_ID"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)LX/1rQ;
    .locals 4

    .prologue
    .line 1227006
    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v0, v0

    .line 1227007
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v2, "website_url"

    .line 1227008
    iget-object v3, p0, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v3, v3

    .line 1227009
    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    const-string v2, "api_endpoint"

    .line 1227010
    iget-object v3, p0, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1227011
    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    const-string v2, "page_id"

    const-string v3, "JS_BRIDGE_PAGE_ID"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    const-string v2, "source"

    const-string v3, "JS_BRIDGE_LOG_SOURCE"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    const-string v2, "ad_id"

    const-string v3, "JS_BRIDGE_AD_ID"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 5

    .prologue
    .line 1227004
    iget-object v0, p0, LX/7i5;->a:LX/0if;

    iget-object v1, p0, LX/7i5;->b:LX/0ih;

    const-string v2, "browser_extensions_native_bridge_called"

    const/4 v3, 0x0

    invoke-static {p1}, LX/7i5;->b(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)LX/1rQ;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1227005
    return-void
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;IZ)V
    .locals 5

    .prologue
    .line 1226918
    invoke-static {p1}, LX/7i5;->b(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)LX/1rQ;

    move-result-object v0

    const-string v1, "error_code"

    invoke-virtual {v0, v1, p2}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v0

    const-string v1, "callback_result"

    invoke-virtual {v0, v1, p3}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v0

    .line 1226919
    iget-object v1, p0, LX/7i5;->a:LX/0if;

    iget-object v2, p0, LX/7i5;->b:LX/0ih;

    const-string v3, "browser_extensions_native_bridge_result"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1226920
    return-void
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1227002
    iget-object v0, p0, LX/7i5;->a:LX/0if;

    iget-object v1, p0, LX/7i5;->b:LX/0ih;

    const/4 v2, 0x0

    invoke-static {p1}, LX/7i5;->b(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)LX/1rQ;

    move-result-object v3

    invoke-virtual {v0, v1, p2, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1227003
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1226947
    iget-object v0, p0, LX/7i5;->a:LX/0if;

    iget-object v1, p0, LX/7i5;->b:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 1226948
    invoke-static {p2}, LX/7i5;->b(Landroid/os/Bundle;)LX/1rQ;

    move-result-object v0

    const-string v1, "website_url"

    invoke-virtual {v0, v1, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 1226949
    iget-object v1, p0, LX/7i5;->a:LX/0if;

    iget-object v2, p0, LX/7i5;->b:LX/0ih;

    const-string v3, "browser_extensions_browser_open"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1226950
    iget-object v0, p0, LX/7i5;->d:LX/6DH;

    const-string v1, "JS_BRIDGE_APP_ID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "JS_BRIDGE_PAGE_ID"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "JS_BRIDGE_PSID"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1226951
    iput-object v1, v0, LX/6DH;->e:Ljava/lang/String;

    .line 1226952
    iput-object v2, v0, LX/6DH;->f:Ljava/lang/String;

    .line 1226953
    iput-object v3, v0, LX/6DH;->g:Ljava/lang/String;

    .line 1226954
    iget-object v4, v0, LX/6DH;->e:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1226955
    iget-object v4, v0, LX/6DH;->b:LX/03V;

    sget-object v5, LX/6DH;->a:Ljava/lang/String;

    const-string p0, "Failed to log launch event because app id is invalid."

    invoke-virtual {v4, v5, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1226956
    :goto_0
    return-void

    .line 1226957
    :cond_0
    iget-object v4, v0, LX/6DH;->f:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1226958
    iget-object v4, v0, LX/6DH;->b:LX/03V;

    sget-object v5, LX/6DH;->a:Ljava/lang/String;

    const-string p0, "Failed to log launch event because page id is invalid"

    invoke-virtual {v4, v5, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1226959
    :cond_1
    iget-object v4, v0, LX/6DH;->g:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1226960
    iget-object v4, v0, LX/6DH;->b:LX/03V;

    sget-object v5, LX/6DH;->a:Ljava/lang/String;

    const-string p0, "Failed to log launch event because page scope user id is invalid"

    invoke-virtual {v4, v5, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1226961
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v0, LX/6DH;->e:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x2f

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "activities"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, LX/6DH;->h:Ljava/lang/String;

    .line 1226962
    const-string v4, "instant_experiences_launch"

    .line 1226963
    const/4 v5, 0x0

    .line 1226964
    iget-object p0, v0, LX/6DH;->i:LX/0Uh;

    const/16 p1, 0x510

    const/4 p2, 0x0

    invoke-virtual {p0, p1, p2}, LX/0Uh;->a(IZ)Z

    move-result p0

    if-nez p0, :cond_3

    .line 1226965
    :goto_1
    goto :goto_0

    .line 1226966
    :cond_3
    if-nez v5, :cond_4

    .line 1226967
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 1226968
    :cond_4
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    .line 1226969
    :try_start_0
    const-string p0, "_eventName"

    invoke-virtual {p1, p0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1226970
    :goto_2
    invoke-virtual {v5, p1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1226971
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 1226972
    new-instance p1, Lorg/apache/http/message/BasicNameValuePair;

    const-string p2, "event"

    const-string v1, "CUSTOM_APP_EVENTS"

    invoke-direct {p1, p2, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1226973
    new-instance p1, Lorg/apache/http/message/BasicNameValuePair;

    const-string p2, "advertiser_tracking_enabled"

    const-string v1, "0"

    invoke-direct {p1, p2, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1226974
    new-instance p1, Lorg/apache/http/message/BasicNameValuePair;

    const-string p2, "application_tracking_enabled"

    const-string v1, "0"

    invoke-direct {p1, p2, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1226975
    new-instance p1, Lorg/apache/http/message/BasicNameValuePair;

    const-string p2, "page_id"

    iget-object v1, v0, LX/6DH;->f:Ljava/lang/String;

    invoke-direct {p1, p2, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1226976
    new-instance p1, Lorg/apache/http/message/BasicNameValuePair;

    const-string p2, "page_scoped_user_id"

    iget-object v1, v0, LX/6DH;->g:Ljava/lang/String;

    invoke-direct {p1, p2, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1226977
    new-instance p1, Lorg/json/JSONArray;

    invoke-direct {p1}, Lorg/json/JSONArray;-><init>()V

    .line 1226978
    const-string p2, "ix1"

    invoke-virtual {p1, p2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1226979
    new-instance p2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "extinfo"

    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v1, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1226980
    move-object p1, p0

    .line 1226981
    new-instance p0, Lorg/apache/http/message/BasicNameValuePair;

    const-string p2, "custom_events"

    invoke-virtual {v5}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1226982
    const/4 p0, 0x0

    .line 1226983
    :try_start_1
    iget-object p2, v0, LX/6DH;->c:Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;

    iget-object v1, v0, LX/6DH;->h:Ljava/lang/String;

    .line 1226984
    const/4 v2, 0x0

    .line 1226985
    iget-object v3, p2, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;->d:LX/6D1;

    const-string v4, "v2.7"

    invoke-virtual {v3, v4}, LX/6D1;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1226986
    new-instance v4, Landroid/net/Uri$Builder;

    invoke-direct {v4}, Landroid/net/Uri$Builder;-><init>()V

    const-string v5, "https"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "graph.facebook.com"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 1226987
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 1226988
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 1226989
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1226990
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "access_token"

    invoke-direct {v3, v5, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1226991
    :cond_5
    new-instance v3, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v3, p1}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V

    invoke-virtual {v4, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1226992
    const-string v3, "browser_extensions_async_graph_api_post"

    invoke-static {p2, v3, v4}, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;->a(Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;)LX/15D;

    move-result-object v3

    .line 1226993
    iget-object v4, p2, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v4, v3}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v3

    .line 1226994
    iget-object v4, v3, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v3, v4

    .line 1226995
    move-object v2, v3

    .line 1226996
    move-object p0, v2
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1226997
    :goto_3
    new-instance p1, LX/6DG;

    invoke-direct {p1, v0}, LX/6DG;-><init>(LX/6DH;)V

    iget-object p2, v0, LX/6DH;->d:Ljava/util/concurrent/Executor;

    invoke-static {p0, p1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_1

    .line 1226998
    :catch_0
    move-exception p0

    .line 1226999
    iget-object p2, v0, LX/6DH;->b:LX/03V;

    sget-object v1, LX/6DH;->a:Ljava/lang/String;

    const-string v2, "Failed to create JSONObject for custom event."

    invoke-virtual {p2, v1, v2, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 1227000
    :catch_1
    move-exception p1

    .line 1227001
    iget-object p2, v0, LX/6DH;->b:LX/03V;

    sget-object v1, LX/6DH;->a:Ljava/lang/String;

    const-string v2, "Failed to create graph api post request."

    invoke-virtual {p2, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1226942
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "error_tag"

    invoke-virtual {v0, v1, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "error_message"

    invoke-virtual {v0, v1, p2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "page_id"

    invoke-virtual {v0, v1, p3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "ad_id"

    if-eqz p4, :cond_0

    :goto_0
    invoke-virtual {v0, v1, p4}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 1226943
    iget-object v1, p0, LX/7i5;->a:LX/0if;

    iget-object v2, p0, LX/7i5;->b:LX/0ih;

    const-string v3, "browser_extensions_error"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1226944
    iget-object v0, p0, LX/7i5;->c:LX/03V;

    invoke-virtual {v0, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1226945
    return-void

    .line 1226946
    :cond_0
    const-string p4, ""

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1226939
    invoke-static {p2}, LX/7i5;->b(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)LX/1rQ;

    move-result-object v0

    const-string v1, "permission_requested"

    const-string v2, ","

    invoke-static {v2, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 1226940
    iget-object v1, p0, LX/7i5;->a:LX/0if;

    iget-object v2, p0, LX/7i5;->b:LX/0ih;

    const-string v3, "browser_extensions_permission_dialog_shown"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1226941
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1226936
    invoke-static {p2}, LX/7i5;->b(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)LX/1rQ;

    move-result-object v0

    const-string v1, "autofill_fields_requested"

    const-string v2, ","

    invoke-static {v2, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 1226937
    iget-object v1, p0, LX/7i5;->a:LX/0if;

    iget-object v2, p0, LX/7i5;->b:LX/0ih;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1226938
    return-void
.end method

.method public final a(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 1226935
    invoke-static {p1}, LX/6D3;->a(Landroid/os/Bundle;)LX/6Cx;

    move-result-object v0

    sget-object v1, LX/6Cx;->INSTANT_EXPERIENCE:LX/6Cx;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1226931
    iget-object v0, p0, LX/7i5;->a:LX/0if;

    iget-object v1, p0, LX/7i5;->b:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->b(LX/0ih;)V

    .line 1226932
    invoke-static {p2}, LX/7i5;->b(Landroid/os/Bundle;)LX/1rQ;

    move-result-object v0

    const-string v1, "website_url"

    invoke-virtual {v0, v1, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 1226933
    iget-object v1, p0, LX/7i5;->a:LX/0if;

    iget-object v2, p0, LX/7i5;->b:LX/0ih;

    const-string v3, "browser_extensions_browser_resumed"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1226934
    return-void
.end method

.method public final b(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1226928
    invoke-static {p2}, LX/7i5;->b(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)LX/1rQ;

    move-result-object v0

    const-string v1, "autofill_fields_requested"

    const-string v2, ","

    invoke-static {v2, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 1226929
    iget-object v1, p0, LX/7i5;->a:LX/0if;

    iget-object v2, p0, LX/7i5;->b:LX/0ih;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1226930
    return-void
.end method

.method public final c(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1226924
    invoke-static {p2}, LX/7i5;->b(Landroid/os/Bundle;)LX/1rQ;

    move-result-object v0

    const-string v1, "website_url"

    invoke-virtual {v0, v1, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 1226925
    iget-object v1, p0, LX/7i5;->a:LX/0if;

    iget-object v2, p0, LX/7i5;->b:LX/0ih;

    const-string v3, "browser_extensions_browser_closed"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1226926
    iget-object v0, p0, LX/7i5;->a:LX/0if;

    iget-object v1, p0, LX/7i5;->b:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 1226927
    return-void
.end method

.method public final c(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1226921
    invoke-static {p2}, LX/7i5;->b(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)LX/1rQ;

    move-result-object v0

    const-string v1, "autofill_fields_filled"

    const-string v2, ","

    invoke-static {v2, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 1226922
    iget-object v1, p0, LX/7i5;->a:LX/0if;

    iget-object v2, p0, LX/7i5;->b:LX/0ih;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1226923
    return-void
.end method
