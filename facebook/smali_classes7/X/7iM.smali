.class public final enum LX/7iM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7iM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7iM;

.field public static final enum COLLECTION_LOAD_TIME:LX/7iM;

.field public static final enum NON_PDP_PRODUCT_TAG_CLICK:LX/7iM;

.field public static final enum NON_STORE_PRODUCT_MINI_END_CARD_CLICK:LX/7iM;

.field public static final enum PDFY_ACTIVITY:LX/7iM;

.field public static final enum PDP_PRODUCT_TAG_CLICK:LX/7iM;

.field public static final enum PRODUCT_ITEM_CLICK:LX/7iM;

.field public static final enum STORE_PRODUCT_MINI_END_CARD_CLICK:LX/7iM;

.field public static final enum VIEW_PAGE_STORE_ENTRYPOINT:LX/7iM;

.field public static final enum VIEW_PRODUCT_COLLECTION:LX/7iM;

.field public static final enum VIEW_PRODUCT_DETAILS:LX/7iM;

.field public static final enum VIEW_PRODUCT_STOREFRONT:LX/7iM;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1227415
    new-instance v0, LX/7iM;

    const-string v1, "PRODUCT_ITEM_CLICK"

    const-string v2, "commerce_collection_item_click"

    invoke-direct {v0, v1, v4, v2}, LX/7iM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iM;->PRODUCT_ITEM_CLICK:LX/7iM;

    .line 1227416
    new-instance v0, LX/7iM;

    const-string v1, "COLLECTION_LOAD_TIME"

    const-string v2, "commerce_collection_load_time"

    invoke-direct {v0, v1, v5, v2}, LX/7iM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iM;->COLLECTION_LOAD_TIME:LX/7iM;

    .line 1227417
    new-instance v0, LX/7iM;

    const-string v1, "VIEW_PAGE_STORE_ENTRYPOINT"

    const-string v2, "commerce_view_page_store_entry_point"

    invoke-direct {v0, v1, v6, v2}, LX/7iM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iM;->VIEW_PAGE_STORE_ENTRYPOINT:LX/7iM;

    .line 1227418
    new-instance v0, LX/7iM;

    const-string v1, "VIEW_PRODUCT_STOREFRONT"

    const-string v2, "commerce_view_product_store_front"

    invoke-direct {v0, v1, v7, v2}, LX/7iM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iM;->VIEW_PRODUCT_STOREFRONT:LX/7iM;

    .line 1227419
    new-instance v0, LX/7iM;

    const-string v1, "VIEW_PRODUCT_COLLECTION"

    const-string v2, "commerce_view_product_collection"

    invoke-direct {v0, v1, v8, v2}, LX/7iM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iM;->VIEW_PRODUCT_COLLECTION:LX/7iM;

    .line 1227420
    new-instance v0, LX/7iM;

    const-string v1, "VIEW_PRODUCT_DETAILS"

    const/4 v2, 0x5

    const-string v3, "commerce_view_product_details"

    invoke-direct {v0, v1, v2, v3}, LX/7iM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iM;->VIEW_PRODUCT_DETAILS:LX/7iM;

    .line 1227421
    new-instance v0, LX/7iM;

    const-string v1, "PDP_PRODUCT_TAG_CLICK"

    const/4 v2, 0x6

    const-string v3, "commerce_pdp_product_tag_click"

    invoke-direct {v0, v1, v2, v3}, LX/7iM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iM;->PDP_PRODUCT_TAG_CLICK:LX/7iM;

    .line 1227422
    new-instance v0, LX/7iM;

    const-string v1, "NON_PDP_PRODUCT_TAG_CLICK"

    const/4 v2, 0x7

    const-string v3, "commerce_non_pdp_product_tag_click"

    invoke-direct {v0, v1, v2, v3}, LX/7iM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iM;->NON_PDP_PRODUCT_TAG_CLICK:LX/7iM;

    .line 1227423
    new-instance v0, LX/7iM;

    const-string v1, "STORE_PRODUCT_MINI_END_CARD_CLICK"

    const/16 v2, 0x8

    const-string v3, "commerce_store_product_mini_end_card_click"

    invoke-direct {v0, v1, v2, v3}, LX/7iM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iM;->STORE_PRODUCT_MINI_END_CARD_CLICK:LX/7iM;

    .line 1227424
    new-instance v0, LX/7iM;

    const-string v1, "NON_STORE_PRODUCT_MINI_END_CARD_CLICK"

    const/16 v2, 0x9

    const-string v3, "commerce_non_store_product_mini_end_card_click"

    invoke-direct {v0, v1, v2, v3}, LX/7iM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iM;->NON_STORE_PRODUCT_MINI_END_CARD_CLICK:LX/7iM;

    .line 1227425
    new-instance v0, LX/7iM;

    const-string v1, "PDFY_ACTIVITY"

    const/16 v2, 0xa

    const-string v3, "commerce_pdfy_activity"

    invoke-direct {v0, v1, v2, v3}, LX/7iM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iM;->PDFY_ACTIVITY:LX/7iM;

    .line 1227426
    const/16 v0, 0xb

    new-array v0, v0, [LX/7iM;

    sget-object v1, LX/7iM;->PRODUCT_ITEM_CLICK:LX/7iM;

    aput-object v1, v0, v4

    sget-object v1, LX/7iM;->COLLECTION_LOAD_TIME:LX/7iM;

    aput-object v1, v0, v5

    sget-object v1, LX/7iM;->VIEW_PAGE_STORE_ENTRYPOINT:LX/7iM;

    aput-object v1, v0, v6

    sget-object v1, LX/7iM;->VIEW_PRODUCT_STOREFRONT:LX/7iM;

    aput-object v1, v0, v7

    sget-object v1, LX/7iM;->VIEW_PRODUCT_COLLECTION:LX/7iM;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7iM;->VIEW_PRODUCT_DETAILS:LX/7iM;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7iM;->PDP_PRODUCT_TAG_CLICK:LX/7iM;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7iM;->NON_PDP_PRODUCT_TAG_CLICK:LX/7iM;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7iM;->STORE_PRODUCT_MINI_END_CARD_CLICK:LX/7iM;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7iM;->NON_STORE_PRODUCT_MINI_END_CARD_CLICK:LX/7iM;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7iM;->PDFY_ACTIVITY:LX/7iM;

    aput-object v2, v0, v1

    sput-object v0, LX/7iM;->$VALUES:[LX/7iM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1227427
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1227428
    iput-object p3, p0, LX/7iM;->value:Ljava/lang/String;

    .line 1227429
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7iM;
    .locals 1

    .prologue
    .line 1227430
    const-class v0, LX/7iM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7iM;

    return-object v0
.end method

.method public static values()[LX/7iM;
    .locals 1

    .prologue
    .line 1227431
    sget-object v0, LX/7iM;->$VALUES:[LX/7iM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7iM;

    return-object v0
.end method
