.class public LX/7hw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1226748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1226749
    iput-object p1, p0, LX/7hw;->a:LX/0Zb;

    .line 1226750
    return-void
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7i0;)V
    .locals 4

    .prologue
    .line 1226734
    const-string v0, "mime_type"

    iget-object v1, p1, LX/7i0;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1226735
    const-string v0, "charset"

    iget-object v1, p1, LX/7i0;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1226736
    const-string v0, "expires"

    iget-wide v2, p1, LX/7i0;->c:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1226737
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;JIIILX/7i0;)V
    .locals 2
    .param p7    # LX/7i0;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1226738
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "inapp_browser_prefetch_timing"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1226739
    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1226740
    const-string v1, "duration"

    invoke-virtual {v0, v1, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1226741
    const-string v1, "redirects"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1226742
    const-string v1, "status"

    invoke-virtual {v0, v1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1226743
    const-string v1, "link_context"

    invoke-virtual {v0, v1, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1226744
    if-eqz p7, :cond_0

    .line 1226745
    invoke-static {v0, p7}, LX/7hw;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7i0;)V

    .line 1226746
    :cond_0
    iget-object v1, p0, LX/7hw;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1226747
    return-void
.end method
