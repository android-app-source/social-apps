.class public LX/6tB;
.super LX/6E8;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E8",
        "<",
        "Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;",
        "LX/6tA;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private l:LX/6qh;

.field private m:LX/6tA;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;)V
    .locals 0

    .prologue
    .line 1154082
    invoke-direct {p0, p1}, LX/6E8;-><init>(LX/6E6;)V

    .line 1154083
    return-void
.end method


# virtual methods
.method public final a(LX/6E2;)V
    .locals 2

    .prologue
    .line 1154073
    check-cast p1, LX/6tA;

    .line 1154074
    iget-object v0, p0, LX/6tB;->l:LX/6qh;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1154075
    iput-object p1, p0, LX/6tB;->m:LX/6tA;

    .line 1154076
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;

    .line 1154077
    iget-object v1, p0, LX/6tB;->l:LX/6qh;

    .line 1154078
    iput-object v1, v0, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a:LX/6qh;

    .line 1154079
    iget-object v1, p1, LX/6tA;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;->setText(Ljava/lang/CharSequence;)V

    .line 1154080
    invoke-virtual {v0, p0}, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1154081
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1154071
    iput-object p1, p0, LX/6tB;->l:LX/6qh;

    .line 1154072
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x1b48662c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1154069
    iget-object v1, p0, LX/6tB;->l:LX/6qh;

    iget-object v2, p0, LX/6tB;->m:LX/6tA;

    iget-object v2, v2, LX/6tA;->c:Landroid/content/Intent;

    iget-object v3, p0, LX/6tB;->m:LX/6tA;

    iget v3, v3, LX/6tA;->d:I

    invoke-virtual {v1, v2, v3}, LX/6qh;->a(Landroid/content/Intent;I)V

    .line 1154070
    const v1, -0x35514d4a    # -5724507.0f

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
