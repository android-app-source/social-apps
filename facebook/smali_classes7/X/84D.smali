.class public final LX/84D;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/84H;

.field public final synthetic b:J

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic d:LX/3UJ;


# direct methods
.method public constructor <init>(LX/3UJ;LX/84H;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 1

    .prologue
    .line 1290853
    iput-object p1, p0, LX/84D;->d:LX/3UJ;

    iput-object p2, p0, LX/84D;->a:LX/84H;

    iput-wide p3, p0, LX/84D;->b:J

    iput-object p5, p0, LX/84D;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 1290854
    iget-object v0, p0, LX/84D;->a:LX/84H;

    if-eqz v0, :cond_0

    .line 1290855
    iget-object v0, p0, LX/84D;->a:LX/84H;

    .line 1290856
    instance-of v1, p1, LX/4Ua;

    move v1, v1

    .line 1290857
    invoke-interface {v0, v1}, LX/84H;->a(Z)V

    .line 1290858
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1290859
    iget-object v1, p0, LX/84D;->d:LX/3UJ;

    iget-object v1, v1, LX/3UJ;->b:LX/2do;

    new-instance v2, LX/2f2;

    iget-wide v4, p0, LX/84D;->b:J

    const/4 v3, 0x0

    invoke-direct {v2, v4, v5, v0, v3}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1290860
    iget-object v0, p0, LX/84D;->d:LX/3UJ;

    iget-object v0, v0, LX/3UJ;->c:LX/2hZ;

    invoke-virtual {v0, p1}, LX/2hZ;->a(Ljava/lang/Throwable;)V

    .line 1290861
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1290862
    iget-object v0, p0, LX/84D;->a:LX/84H;

    if-eqz v0, :cond_0

    .line 1290863
    iget-object v0, p0, LX/84D;->a:LX/84H;

    invoke-interface {v0}, LX/84H;->a()V

    .line 1290864
    :cond_0
    iget-object v0, p0, LX/84D;->d:LX/3UJ;

    iget-object v0, v0, LX/3UJ;->b:LX/2do;

    new-instance v1, LX/2f2;

    iget-wide v2, p0, LX/84D;->b:J

    iget-object v4, p0, LX/84D;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1290865
    return-void
.end method
