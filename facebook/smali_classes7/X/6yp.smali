.class public LX/6yp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private a:Z

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1160472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160473
    return-void
.end method

.method public static a(LX/0QB;)LX/6yp;
    .locals 1

    .prologue
    .line 1160474
    new-instance v0, LX/6yp;

    invoke-direct {v0}, LX/6yp;-><init>()V

    .line 1160475
    move-object v0, v0

    .line 1160476
    return-object v0
.end method


# virtual methods
.method public final declared-synchronized afterTextChanged(Landroid/text/Editable;)V
    .locals 7

    .prologue
    .line 1160477
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/6yp;->a:Z

    if-nez v0, :cond_2

    .line 1160478
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6yp;->a:Z

    .line 1160479
    iget-boolean v0, p0, LX/6yp;->b:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/6yp;->c:I

    if-lez v0, :cond_0

    iget v0, p0, LX/6yp;->c:I

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1160480
    iget v0, p0, LX/6yp;->c:I

    add-int/lit8 v0, v0, -0x2

    if-ltz v0, :cond_3

    iget v0, p0, LX/6yp;->c:I

    add-int/lit8 v0, v0, -0x2

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget v0, p0, LX/6yp;->c:I

    add-int/lit8 v0, v0, -0x2

    invoke-interface {p1, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    const/16 v1, 0x30

    if-ne v0, v1, :cond_3

    .line 1160481
    iget v0, p0, LX/6yp;->c:I

    add-int/lit8 v0, v0, -0x2

    iget v1, p0, LX/6yp;->c:I

    invoke-interface {p1, v0, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 1160482
    :cond_0
    :goto_0
    const/16 v6, 0x31

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 1160483
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    .line 1160484
    const/4 v2, 0x5

    if-gt v0, v2, :cond_1

    if-gtz v0, :cond_4

    .line 1160485
    :cond_1
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6yp;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1160486
    :cond_2
    monitor-exit p0

    return-void

    .line 1160487
    :cond_3
    :try_start_1
    iget v0, p0, LX/6yp;->c:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, LX/6yp;->c:I

    invoke-interface {p1, v0, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1160488
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v0, v1

    .line 1160489
    :goto_2
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-ge v0, v2, :cond_7

    .line 1160490
    invoke-interface {p1, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    const/16 v3, 0x30

    if-lt v2, v3, :cond_5

    invoke-interface {p1, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    const/16 v3, 0x39

    if-le v2, v3, :cond_6

    .line 1160491
    :cond_5
    add-int/lit8 v2, v0, 0x1

    invoke-interface {p1, v0, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_2

    .line 1160492
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1160493
    :cond_7
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    .line 1160494
    if-ne v0, v5, :cond_8

    invoke-interface {p1, v1}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    if-gt v2, v6, :cond_9

    :cond_8
    if-ne v0, v4, :cond_a

    invoke-interface {p1, v1}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    if-ne v0, v6, :cond_a

    invoke-interface {p1, v5}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    const/16 v2, 0x32

    if-le v0, v2, :cond_a

    .line 1160495
    :cond_9
    const-string v0, "0"

    invoke-interface {p1, v1, v0}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 1160496
    :cond_a
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lt v0, v4, :cond_1

    .line 1160497
    const-string v0, "/"

    invoke-interface {p1, v4, v4, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1160498
    iget-boolean v0, p0, LX/6yp;->a:Z

    if-nez v0, :cond_0

    .line 1160499
    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    .line 1160500
    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    .line 1160501
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-le v2, v4, :cond_1

    if-ne p3, v4, :cond_1

    if-nez p4, :cond_1

    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v3, 0x2f

    if-ne v2, v3, :cond_1

    if-ne v0, v1, :cond_1

    .line 1160502
    iput-boolean v4, p0, LX/6yp;->b:Z

    .line 1160503
    iput p2, p0, LX/6yp;->c:I

    .line 1160504
    :cond_0
    :goto_0
    return-void

    .line 1160505
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6yp;->b:Z

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1160506
    return-void
.end method
