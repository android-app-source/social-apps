.class public LX/7vv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6uN;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6uN",
        "<",
        "Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;",
        "Lcom/facebook/payments/confirmation/SimpleConfirmationData;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/6ub;


# direct methods
.method public constructor <init>(LX/6ub;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1275266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275267
    iput-object p1, p0, LX/7vv;->a:LX/6ub;

    .line 1275268
    return-void
.end method

.method public static a(LX/0QB;)LX/7vv;
    .locals 4

    .prologue
    .line 1275251
    const-class v1, LX/7vv;

    monitor-enter v1

    .line 1275252
    :try_start_0
    sget-object v0, LX/7vv;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1275253
    sput-object v2, LX/7vv;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1275254
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1275255
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1275256
    new-instance p0, LX/7vv;

    invoke-static {v0}, LX/6ub;->a(LX/0QB;)LX/6ub;

    move-result-object v3

    check-cast v3, LX/6ub;

    invoke-direct {p0, v3}, LX/7vv;-><init>(LX/6ub;)V

    .line 1275257
    move-object v0, p0

    .line 1275258
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1275259
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/7vv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1275260
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1275261
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/confirmation/ConfirmationParams;)Lcom/facebook/payments/confirmation/ConfirmationData;
    .locals 3

    .prologue
    .line 1275264
    check-cast p1, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;

    .line 1275265
    new-instance v0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;

    new-instance v1, Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;-><init>(Z)V

    invoke-direct {v0, p1, v1}, Lcom/facebook/payments/confirmation/SimpleConfirmationData;-><init>(Lcom/facebook/payments/confirmation/ConfirmationParams;Lcom/facebook/payments/confirmation/ProductConfirmationData;)V

    return-object v0
.end method

.method public final a(LX/6uO;)V
    .locals 1

    .prologue
    .line 1275262
    iget-object v0, p0, LX/7vv;->a:LX/6ub;

    invoke-virtual {v0, p1}, LX/6ub;->a(LX/6uO;)V

    .line 1275263
    return-void
.end method
