.class public LX/6q2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1149858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1149859
    iput-object p1, p0, LX/6q2;->a:Ljava/lang/String;

    .line 1149860
    return-void
.end method


# virtual methods
.method public a(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 1149857
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 1149856
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 1149855
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public d(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 1149854
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public e(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 1149861
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public f(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 1149853
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public g(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 1149852
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public h(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 1149851
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 1149817
    iget-object v0, p0, LX/6q2;->a:Ljava/lang/String;

    const v1, -0x7111c66c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1149818
    :try_start_0
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1149819
    const-string v1, "set_payment_pin"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1149820
    invoke-virtual {p0, p1, p2}, LX/6q2;->a(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1149821
    const v1, 0x390fa2f1

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 1149822
    :cond_0
    :try_start_1
    const-string v1, "fetch_payment_pin"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1149823
    invoke-virtual {p0, p1, p2}, LX/6q2;->b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1149824
    const v1, 0x59f1b06d

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 1149825
    :cond_1
    :try_start_2
    const-string v1, "update_payment_pin_status"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1149826
    invoke-virtual {p0, p1, p2}, LX/6q2;->c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 1149827
    const v1, 0x548dee26

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 1149828
    :cond_2
    :try_start_3
    const-string v1, "delete_payment_pin"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1149829
    invoke-virtual {p0, p1, p2}, LX/6q2;->d(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 1149830
    const v1, -0x1e939a6e

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 1149831
    :cond_3
    :try_start_4
    const-string v1, "check_payment_pin"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1149832
    invoke-virtual {p0, p1, p2}, LX/6q2;->e(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    .line 1149833
    const v1, 0x5530d9c0

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 1149834
    :cond_4
    :try_start_5
    const-string v1, "fetch_payment_pin_status"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1149835
    invoke-virtual {p0, p1, p2}, LX/6q2;->f(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 1149836
    const v1, 0x57ef7d07

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 1149837
    :cond_5
    :try_start_6
    const-string v1, "fetch_page_info"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1149838
    invoke-virtual {p0, p1, p2}, LX/6q2;->g(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    .line 1149839
    const v1, 0x66ab1b2d

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 1149840
    :cond_6
    :try_start_7
    const-string v1, "create_fingerprint_nonce"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1149841
    invoke-virtual {p0, p1, p2}, LX/6q2;->h(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v0

    .line 1149842
    const v1, 0x7ea91e7c

    invoke-static {v1}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 1149843
    :cond_7
    :try_start_8
    const-string v1, "verify_fingerprint_nonce"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1149844
    invoke-virtual {p0, p1, p2}, LX/6q2;->i(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v0

    .line 1149845
    const v1, 0x13cdf3c0

    invoke-static {v1}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 1149846
    :cond_8
    :try_start_9
    const-string v1, "disable_fingerprint_nonce"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1149847
    invoke-virtual {p0, p1, p2}, LX/6q2;->j(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v0

    .line 1149848
    const v1, -0x5c99ced4

    invoke-static {v1}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 1149849
    :cond_9
    :try_start_a
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-object v0

    .line 1149850
    const v1, -0x59c6d7db

    invoke-static {v1}, LX/02m;->a(I)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    const v1, -0xc2b5ab8

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public i(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 1149816
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public j(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 1149815
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method
