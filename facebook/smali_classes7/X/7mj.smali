.class public LX/7mj;
.super LX/7mi;
.source ""


# static fields
.field private static final a:LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rl",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/String;

.field public c:J

.field private d:LX/7mh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1237418
    new-instance v0, LX/7mg;

    invoke-direct {v0}, LX/7mg;-><init>()V

    sput-object v0, LX/7mj;->a:LX/0Rl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 2

    .prologue
    .line 1237419
    invoke-direct {p0, p2}, LX/7mi;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1237420
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1237421
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot use GraphQLStory from server"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1237422
    iput-object p1, p0, LX/7mj;->b:Ljava/lang/String;

    .line 1237423
    sget-object v0, LX/7mh;->NORMAL:LX/7mh;

    iput-object v0, p0, LX/7mj;->d:LX/7mh;

    .line 1237424
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/7mj;->c:J

    .line 1237425
    return-void

    .line 1237426
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)LX/7mj;
    .locals 1

    .prologue
    .line 1237405
    new-instance v0, LX/7mj;

    invoke-direct {v0, p0, p1}, LX/7mj;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    return-object v0
.end method

.method private static b(J)Z
    .locals 2

    .prologue
    .line 1237409
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)Z
    .locals 3

    .prologue
    .line 1237410
    invoke-static {p1, p2}, LX/7mj;->b(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1237411
    sget-object v0, LX/7mh;->SCHEDULED_POST:LX/7mh;

    iput-object v0, p0, LX/7mj;->d:LX/7mh;

    .line 1237412
    iput-wide p1, p0, LX/7mj;->c:J

    .line 1237413
    const/4 v0, 0x1

    .line 1237414
    :goto_0
    return v0

    .line 1237415
    :cond_0
    sget-object v0, LX/7mh;->NORMAL:LX/7mh;

    iput-object v0, p0, LX/7mj;->d:LX/7mh;

    .line 1237416
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/7mj;->c:J

    .line 1237417
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1237408
    iget-object v0, p0, LX/7mj;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1237407
    iget-object v0, p0, LX/7mj;->d:LX/7mh;

    sget-object v1, LX/7mh;->SCHEDULED_POST:LX/7mh;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, LX/7mj;->c:J

    invoke-static {v0, v1}, LX/7mj;->b(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 1237406
    iget-wide v0, p0, LX/7mj;->c:J

    return-wide v0
.end method
