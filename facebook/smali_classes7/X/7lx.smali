.class public LX/7lx;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/0lB;

.field private final d:LX/6G2;

.field public final e:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field private final f:Landroid/os/Handler;

.field public final g:Ljava/lang/String;

.field public final h:LX/0W3;

.field public final i:LX/8iM;

.field public j:LX/C9S;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1236121
    const-class v0, LX/7lx;

    sput-object v0, LX/7lx;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0lB;LX/6G2;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Landroid/os/Handler;Ljava/lang/String;LX/0W3;LX/8iM;)V
    .locals 0
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1236122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236123
    iput-object p1, p0, LX/7lx;->b:Landroid/content/Context;

    .line 1236124
    iput-object p2, p0, LX/7lx;->c:LX/0lB;

    .line 1236125
    iput-object p3, p0, LX/7lx;->d:LX/6G2;

    .line 1236126
    iput-object p4, p0, LX/7lx;->e:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1236127
    iput-object p5, p0, LX/7lx;->f:Landroid/os/Handler;

    .line 1236128
    iput-object p6, p0, LX/7lx;->g:Ljava/lang/String;

    .line 1236129
    iput-object p7, p0, LX/7lx;->h:LX/0W3;

    .line 1236130
    iput-object p8, p0, LX/7lx;->i:LX/8iM;

    .line 1236131
    return-void
.end method

.method public static a$redex0(LX/7lx;Lcom/facebook/composer/publish/common/PendingStory;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 1236132
    new-instance v0, LX/7lt;

    invoke-direct {v0, p0, p1}, LX/7lt;-><init>(LX/7lx;Lcom/facebook/composer/publish/common/PendingStory;)V

    return-object v0
.end method

.method public static b$redex0(LX/7lx;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 4

    .prologue
    .line 1236133
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v0

    .line 1236134
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v1

    .line 1236135
    new-instance v2, Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;

    invoke-direct {v2, v1, v0}, Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;-><init>(Lcom/facebook/composer/publish/common/ErrorDetails;Lcom/facebook/composer/publish/common/PostParamsWrapper;)V

    .line 1236136
    iget-object v0, p0, LX/7lx;->d:LX/6G2;

    invoke-static {}, LX/6FY;->newBuilder()LX/6FX;

    move-result-object v1

    iget-object v3, p0, LX/7lx;->b:Landroid/content/Context;

    invoke-virtual {v1, v3}, LX/6FX;->a(Landroid/content/Context;)LX/6FX;

    move-result-object v1

    sget-object v3, LX/6Fb;->POST_FAILURE:LX/6Fb;

    invoke-virtual {v1, v3}, LX/6FX;->a(LX/6Fb;)LX/6FX;

    move-result-object v1

    new-instance v3, LX/7lw;

    invoke-direct {v3, p0, v2}, LX/7lw;-><init>(LX/7lx;Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;)V

    invoke-static {v3}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6FX;->a(LX/0Rf;)LX/6FX;

    move-result-object v1

    const-wide v2, 0x104e02b717630L

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6FX;->a(Ljava/lang/Long;)LX/6FX;

    move-result-object v1

    invoke-virtual {v1}, LX/6FX;->a()LX/6FY;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6G2;->a(LX/6FY;)V

    .line 1236137
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1236138
    const/4 v1, 0x0

    .line 1236139
    iget-object v0, p0, LX/7lx;->b:Landroid/content/Context;

    .line 1236140
    :goto_0
    if-eqz v0, :cond_7

    instance-of v2, v0, Landroid/app/Activity;

    if-nez v2, :cond_7

    .line 1236141
    instance-of v2, v0, Landroid/view/ContextThemeWrapper;

    if-eqz v2, :cond_0

    .line 1236142
    check-cast v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {v0}, Landroid/view/ContextThemeWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    .line 1236143
    :cond_0
    instance-of v2, v0, LX/31Z;

    if-eqz v2, :cond_1

    .line 1236144
    check-cast v0, LX/31Z;

    invoke-virtual {v0}, LX/31Z;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1236145
    :goto_1
    move v0, v0

    .line 1236146
    if-nez v0, :cond_3

    .line 1236147
    :cond_2
    :goto_2
    return-void

    .line 1236148
    :cond_3
    iget-object v0, p0, LX/7lx;->e:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    iget-object v1, p0, LX/7lx;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v2

    .line 1236149
    if-eqz v2, :cond_2

    .line 1236150
    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v3

    .line 1236151
    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v4

    .line 1236152
    if-eqz v4, :cond_4

    iget-object v0, v4, Lcom/facebook/composer/publish/common/ErrorDetails;->userMessage:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, p0, LX/7lx;->b:Landroid/content/Context;

    const v1, 0x7f0810a7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1236153
    :goto_3
    if-eqz v4, :cond_6

    .line 1236154
    iget v0, v4, Lcom/facebook/composer/publish/common/ErrorDetails;->errorSubcode:I

    const v5, 0x156cd0

    if-ne v0, v5, :cond_9

    iget-object v0, p0, LX/7lx;->i:LX/8iM;

    invoke-virtual {v0}, LX/8iM;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 1236155
    if-eqz v0, :cond_6

    iget-object v0, v4, Lcom/facebook/composer/publish/common/ErrorDetails;->userTitle:Ljava/lang/String;

    .line 1236156
    :goto_5
    new-instance v5, Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;

    invoke-direct {v5, v4, v3}, Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;-><init>(Lcom/facebook/composer/publish/common/ErrorDetails;Lcom/facebook/composer/publish/common/PostParamsWrapper;)V

    .line 1236157
    iget-object v3, p0, LX/7lx;->f:Landroid/os/Handler;

    new-instance v4, Lcom/facebook/composer/publish/PostFailureDialogController$1;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/facebook/composer/publish/PostFailureDialogController$1;-><init>(LX/7lx;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V

    const v0, 0x4dd31e03    # 4.42744928E8f

    invoke-static {v3, v4, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_2

    .line 1236158
    :cond_5
    iget-object v0, v4, Lcom/facebook/composer/publish/common/ErrorDetails;->userMessage:Ljava/lang/String;

    move-object v1, v0

    goto :goto_3

    .line 1236159
    :cond_6
    const/4 v0, 0x0

    goto :goto_5

    :cond_7
    if-eqz v0, :cond_8

    const/4 v0, 0x1

    goto :goto_1

    :cond_8
    move v0, v1

    goto :goto_1

    :cond_9
    const/4 v0, 0x0

    goto :goto_4
.end method
