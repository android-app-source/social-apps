.class public final LX/7Od;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0lF;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7Of;


# direct methods
.method public constructor <init>(LX/7Of;)V
    .locals 0

    .prologue
    .line 1201918
    iput-object p1, p0, LX/7Od;->a:LX/7Of;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1201919
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1201920
    check-cast p1, LX/0lF;

    .line 1201921
    if-eqz p1, :cond_0

    .line 1201922
    invoke-virtual {p1}, LX/0lF;->B()Ljava/lang/String;

    .line 1201923
    iget-object v0, p0, LX/7Od;->a:LX/7Of;

    iget-object v1, p0, LX/7Od;->a:LX/7Of;

    iget-object v1, v1, LX/7Of;->f:LX/0lC;

    invoke-virtual {p1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 1201924
    :try_start_0
    invoke-virtual {v1, v2}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 1201925
    sget-object v4, LX/7Oe;->a:Ljava/lang/String;

    invoke-virtual {v5, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1201926
    :try_start_1
    sget-object v6, LX/7Oe;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 1201927
    :goto_0
    new-instance v6, LX/7Oe;

    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    :goto_1
    if-eqz v3, :cond_2

    invoke-virtual {v3}, LX/0lF;->D()J

    move-result-wide v3

    :goto_2
    invoke-direct {v6, v5, v3, v4}, LX/7Oe;-><init>(Ljava/lang/String;J)V

    move-object v1, v6

    .line 1201928
    iput-object v1, v0, LX/7Of;->e:LX/7Oe;

    .line 1201929
    :cond_0
    return-void

    .line 1201930
    :catch_0
    move-exception v4

    move-object v5, v4

    move-object v4, v3

    .line 1201931
    :goto_3
    sget-object v6, LX/7Of;->a:Ljava/lang/String;

    const-string v7, "failed to parse payload: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v2, v8, v9

    invoke-static {v6, v5, v7, v8}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1201932
    :cond_1
    const-string v4, ""

    move-object v5, v4

    goto :goto_1

    :cond_2
    const-wide/16 v3, 0x0

    goto :goto_2

    .line 1201933
    :catch_1
    move-exception v5

    goto :goto_3
.end method
