.class public LX/7zl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

.field public b:J

.field public c:Z

.field private d:Z

.field public e:D

.field public f:D

.field public g:I

.field public h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Z


# direct methods
.method public constructor <init>(Lcom/facebook/feed/model/ClientFeedUnitEdge;JZZDDIILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1281334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1281335
    iput-object p1, p0, LX/7zl;->a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 1281336
    iput-wide p2, p0, LX/7zl;->b:J

    .line 1281337
    iput-boolean p4, p0, LX/7zl;->c:Z

    .line 1281338
    iput-boolean p5, p0, LX/7zl;->d:Z

    .line 1281339
    iput-wide p6, p0, LX/7zl;->e:D

    .line 1281340
    iput-wide p8, p0, LX/7zl;->f:D

    .line 1281341
    iput p10, p0, LX/7zl;->g:I

    .line 1281342
    iput p11, p0, LX/7zl;->h:I

    .line 1281343
    iput-object p12, p0, LX/7zl;->i:Ljava/lang/String;

    .line 1281344
    iput-object p13, p0, LX/7zl;->j:Ljava/lang/String;

    .line 1281345
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1281348
    iget-wide v0, p0, LX/7zl;->b:J

    return-wide v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1281346
    iput-boolean p1, p0, LX/7zl;->k:Z

    .line 1281347
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1281329
    iget-boolean v0, p0, LX/7zl;->c:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1281327
    iget-boolean v0, p0, LX/7zl;->c:Z

    move v0, v0

    .line 1281328
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7zl;->a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1281330
    iget-boolean v0, p0, LX/7zl;->c:Z

    move v0, v0

    .line 1281331
    if-nez v0, :cond_0

    .line 1281332
    const/4 v0, -0x1

    .line 1281333
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/7zl;->a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/HasGapRule;

    invoke-interface {v0}, Lcom/facebook/graphql/model/HasGapRule;->n()I

    move-result v0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1281326
    iget-boolean v0, p0, LX/7zl;->d:Z

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1281325
    iget-object v0, p0, LX/7zl;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1281324
    iget-object v0, p0, LX/7zl;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 1281323
    iget-boolean v0, p0, LX/7zl;->k:Z

    return v0
.end method
