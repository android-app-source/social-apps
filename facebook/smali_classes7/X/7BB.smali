.class public final LX/7BB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/search/api/GraphSearchQueryTabModifier;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1177957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1177958
    const/4 v0, 0x2

    new-array v0, v0, [Z

    .line 1177959
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 1177960
    new-instance v1, LX/7BC;

    invoke-direct {v1}, LX/7BC;-><init>()V

    const/4 v2, 0x0

    aget-boolean v2, v0, v2

    .line 1177961
    iput-boolean v2, v1, LX/7BC;->b:Z

    .line 1177962
    move-object v1, v1

    .line 1177963
    const/4 v2, 0x1

    aget-boolean v0, v0, v2

    .line 1177964
    iput-boolean v0, v1, LX/7BC;->a:Z

    .line 1177965
    move-object v0, v1

    .line 1177966
    invoke-virtual {v0}, LX/7BC;->a()Lcom/facebook/search/api/GraphSearchQueryTabModifier;

    move-result-object v0

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1177967
    new-array v0, p1, [Lcom/facebook/search/api/GraphSearchQueryTabModifier;

    return-object v0
.end method
