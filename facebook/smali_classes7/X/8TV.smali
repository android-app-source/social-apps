.class public final enum LX/8TV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8TV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8TV;

.field public static final enum END_SCREEN:LX/8TV;

.field public static final enum IN_GAME:LX/8TV;

.field public static final enum START_SCREEN:LX/8TV;


# instance fields
.field private final state:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1348714
    new-instance v0, LX/8TV;

    const-string v1, "START_SCREEN"

    const-string v2, "start_screen"

    invoke-direct {v0, v1, v3, v2}, LX/8TV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TV;->START_SCREEN:LX/8TV;

    .line 1348715
    new-instance v0, LX/8TV;

    const-string v1, "IN_GAME"

    const-string v2, "in_game"

    invoke-direct {v0, v1, v4, v2}, LX/8TV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TV;->IN_GAME:LX/8TV;

    .line 1348716
    new-instance v0, LX/8TV;

    const-string v1, "END_SCREEN"

    const-string v2, "end_screen"

    invoke-direct {v0, v1, v5, v2}, LX/8TV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TV;->END_SCREEN:LX/8TV;

    .line 1348717
    const/4 v0, 0x3

    new-array v0, v0, [LX/8TV;

    sget-object v1, LX/8TV;->START_SCREEN:LX/8TV;

    aput-object v1, v0, v3

    sget-object v1, LX/8TV;->IN_GAME:LX/8TV;

    aput-object v1, v0, v4

    sget-object v1, LX/8TV;->END_SCREEN:LX/8TV;

    aput-object v1, v0, v5

    sput-object v0, LX/8TV;->$VALUES:[LX/8TV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1348721
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1348722
    iput-object p3, p0, LX/8TV;->state:Ljava/lang/String;

    .line 1348723
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8TV;
    .locals 1

    .prologue
    .line 1348720
    const-class v0, LX/8TV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8TV;

    return-object v0
.end method

.method public static values()[LX/8TV;
    .locals 1

    .prologue
    .line 1348719
    sget-object v0, LX/8TV;->$VALUES:[LX/8TV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8TV;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1348718
    iget-object v0, p0, LX/8TV;->state:Ljava/lang/String;

    return-object v0
.end method
