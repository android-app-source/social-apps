.class public final LX/8AY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "viewerDoesNotLikeSentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "viewerDoesNotLikeSentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "viewerLikesSentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "viewerLikesSentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1307089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1307090
    return-void
.end method

.method public static a(Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;)LX/8AY;
    .locals 4

    .prologue
    .line 1307113
    new-instance v0, LX/8AY;

    invoke-direct {v0}, LX/8AY;-><init>()V

    .line 1307114
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8AY;->a:Ljava/lang/String;

    .line 1307115
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/8AY;->b:Z

    .line 1307116
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8AY;->c:Ljava/lang/String;

    .line 1307117
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8AY;->d:Ljava/lang/String;

    .line 1307118
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eH_()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/8AY;->e:LX/15i;

    iput v1, v0, LX/8AY;->f:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1307119
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eI_()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iput-object v2, v0, LX/8AY;->g:LX/15i;

    iput v1, v0, LX/8AY;->h:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1307120
    return-object v0

    .line 1307121
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1307122
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v10, 0x0

    const/4 v2, 0x0

    .line 1307091
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1307092
    iget-object v1, p0, LX/8AY;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1307093
    iget-object v3, p0, LX/8AY;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1307094
    iget-object v5, p0, LX/8AY;->d:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1307095
    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v7, p0, LX/8AY;->e:LX/15i;

    iget v8, p0, LX/8AY;->f:I

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v6, 0x2c262842

    invoke-static {v7, v8, v6}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;

    move-result-object v6

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1307096
    sget-object v7, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v7

    :try_start_1
    iget-object v8, p0, LX/8AY;->g:LX/15i;

    iget v9, p0, LX/8AY;->h:I

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v7, 0xb40934

    invoke-static {v8, v9, v7}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;

    move-result-object v7

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1307097
    const/4 v8, 0x6

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1307098
    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 1307099
    iget-boolean v1, p0, LX/8AY;->b:Z

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1307100
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1307101
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1307102
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1307103
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1307104
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1307105
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1307106
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1307107
    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1307108
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1307109
    new-instance v1, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    invoke-direct {v1, v0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;-><init>(LX/15i;)V

    .line 1307110
    return-object v1

    .line 1307111
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1307112
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
