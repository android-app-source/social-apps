.class public final LX/72I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

.field public final synthetic b:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;)V
    .locals 0

    .prologue
    .line 1164086
    iput-object p1, p0, LX/72I;->b:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iput-object p2, p0, LX/72I;->a:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1164087
    iget-object v0, p0, LX/72I;->a:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    .line 1164088
    iget-object v0, p0, LX/72I;->b:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->d:LX/72M;

    iget-object v1, p0, LX/72I;->b:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    invoke-virtual {v1}, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/72M;->a(Z)V

    .line 1164089
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1164090
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1164091
    return-void
.end method
