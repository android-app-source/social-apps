.class public final LX/7Ff;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    .line 1187746
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1187747
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1187748
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1187749
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1187750
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_7

    .line 1187751
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1187752
    :goto_1
    move v1, v2

    .line 1187753
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1187754
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1187755
    :cond_1
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 1187756
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1187757
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1187758
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1187759
    const-string v8, "page_index"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1187760
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v6, v4

    move v4, v3

    goto :goto_2

    .line 1187761
    :cond_2
    const-string v8, "response_option_numeric_value"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1187762
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    move v5, v1

    move v1, v3

    goto :goto_2

    .line 1187763
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1187764
    :cond_4
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1187765
    if-eqz v4, :cond_5

    .line 1187766
    invoke-virtual {p1, v2, v6, v2}, LX/186;->a(III)V

    .line 1187767
    :cond_5
    if-eqz v1, :cond_6

    .line 1187768
    invoke-virtual {p1, v3, v5, v2}, LX/186;->a(III)V

    .line 1187769
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_7
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1187730
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1187731
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1187732
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const/4 p3, 0x0

    .line 1187733
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1187734
    invoke-virtual {p0, v1, p3, p3}, LX/15i;->a(III)I

    move-result v2

    .line 1187735
    if-eqz v2, :cond_0

    .line 1187736
    const-string v3, "page_index"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187737
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 1187738
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, p3}, LX/15i;->a(III)I

    move-result v2

    .line 1187739
    if-eqz v2, :cond_1

    .line 1187740
    const-string v3, "response_option_numeric_value"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187741
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 1187742
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1187743
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1187744
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1187745
    return-void
.end method
