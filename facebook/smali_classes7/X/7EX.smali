.class public LX/7EX;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "LX/7F3;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/7FT;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/view/View$OnClickListener;

.field public c:Ljava/lang/Runnable;

.field private final d:Landroid/view/View$OnClickListener;

.field private e:Ljava/lang/String;

.field private final f:Landroid/view/View$OnFocusChangeListener;

.field private final g:Landroid/view/View$OnFocusChangeListener;

.field private final h:Landroid/view/View$OnFocusChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "LX/7F3;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1184789
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1184790
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7EX;->a:Ljava/util/HashMap;

    .line 1184791
    new-instance v0, LX/7ES;

    invoke-direct {v0, p0}, LX/7ES;-><init>(LX/7EX;)V

    iput-object v0, p0, LX/7EX;->d:Landroid/view/View$OnClickListener;

    .line 1184792
    new-instance v0, LX/7ET;

    invoke-direct {v0, p0}, LX/7ET;-><init>(LX/7EX;)V

    iput-object v0, p0, LX/7EX;->f:Landroid/view/View$OnFocusChangeListener;

    .line 1184793
    new-instance v0, LX/7EU;

    invoke-direct {v0, p0}, LX/7EU;-><init>(LX/7EX;)V

    iput-object v0, p0, LX/7EX;->g:Landroid/view/View$OnFocusChangeListener;

    .line 1184794
    new-instance v0, LX/7EV;

    invoke-direct {v0, p0}, LX/7EV;-><init>(LX/7EX;)V

    iput-object v0, p0, LX/7EX;->h:Landroid/view/View$OnFocusChangeListener;

    .line 1184795
    return-void
.end method

.method private a(LX/7FD;Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;Landroid/view/View$OnFocusChangeListener;)V
    .locals 2

    .prologue
    .line 1184782
    invoke-virtual {p1}, LX/7FD;->b()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->setChecked(Z)V

    .line 1184783
    iput-object p3, p2, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->f:Landroid/view/View$OnFocusChangeListener;

    .line 1184784
    iget-object v0, p2, LX/7FT;->a:LX/7F3;

    move-object v0, v0

    .line 1184785
    iget-object v1, v0, LX/7F3;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1184786
    iget-object v1, p0, LX/7EX;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/7FD;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1184787
    invoke-virtual {p2}, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->a()V

    .line 1184788
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/7EX;Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 1184776
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, LX/7FT;

    .line 1184777
    if-eqz p2, :cond_0

    .line 1184778
    iget-object p1, v0, LX/7FT;->a:LX/7F3;

    move-object v0, p1

    .line 1184779
    iget-object p1, v0, LX/7F3;->b:Ljava/lang/String;

    move-object v0, p1

    .line 1184780
    iput-object v0, p0, LX/7EX;->e:Ljava/lang/String;

    .line 1184781
    :cond_0
    return-void
.end method

.method public static c(LX/7EX;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1184796
    move-object v0, p1

    check-cast v0, Landroid/widget/Checkable;

    .line 1184797
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 1184798
    check-cast p1, LX/7FT;

    .line 1184799
    iget-object v1, p1, LX/7FT;->a:LX/7F3;

    move-object v1, v1

    .line 1184800
    if-eqz v1, :cond_1

    .line 1184801
    iget-object v1, p1, LX/7FT;->a:LX/7F3;

    move-object v1, v1

    .line 1184802
    iget-object v2, v1, LX/7F3;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1184803
    iget-object v1, p0, LX/7EX;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1184804
    iget-object v1, p0, LX/7EX;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7FT;

    .line 1184805
    if-eqz v1, :cond_0

    if-eq v1, v0, :cond_0

    .line 1184806
    check-cast v1, Landroid/widget/Checkable;

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 1184807
    :cond_0
    iget-object v0, p0, LX/7EX;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1184808
    :cond_1
    iget-object v0, p0, LX/7EX;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    .line 1184809
    iget-object v0, p0, LX/7EX;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1184810
    :cond_2
    iget-object v0, p1, LX/7FT;->a:LX/7F3;

    move-object v0, v0

    .line 1184811
    iget-object v1, v0, LX/7F3;->a:LX/7F7;

    move-object v0, v1

    .line 1184812
    sget-object v1, LX/7F7;->RADIOWRITEIN:LX/7F7;

    if-eq v0, v1, :cond_3

    .line 1184813
    iget-object v0, p1, LX/7FT;->a:LX/7F3;

    move-object v0, v0

    .line 1184814
    invoke-virtual {p0, v0}, LX/7EX;->getPosition(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {p1}, LX/7FT;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 1184815
    :goto_0
    invoke-virtual {p0}, LX/7EX;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 1184816
    invoke-virtual {p0, v1}, LX/7EX;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7F3;

    .line 1184817
    iget-object p1, v2, LX/7F3;->a:LX/7F7;

    move-object v2, p1

    .line 1184818
    sget-object p1, LX/7F7;->QUESTION:LX/7F7;

    if-ne v2, p1, :cond_4

    .line 1184819
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    .line 1184820
    :cond_3
    return-void

    .line 1184821
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1184773
    invoke-virtual {p0, p1}, LX/7EX;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7F3;

    .line 1184774
    iget-object p0, v0, LX/7F3;->a:LX/7F7;

    invoke-virtual {p0}, LX/7F7;->ordinal()I

    move-result p0

    move v0, p0

    .line 1184775
    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 1184711
    invoke-virtual {p0, p1}, LX/7EX;->getItemViewType(I)I

    move-result v0

    invoke-static {v0}, LX/7F7;->fromInt(I)LX/7F7;

    move-result-object v4

    .line 1184712
    if-nez p2, :cond_0

    .line 1184713
    sget-object v0, LX/7EW;->a:[I

    invoke-virtual {v4}, LX/7F7;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    move-object v2, p2

    .line 1184714
    :goto_0
    invoke-virtual {p0, p1}, LX/7EX;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7F3;

    move-object v1, v2

    .line 1184715
    check-cast v1, LX/7FT;

    .line 1184716
    if-eqz v1, :cond_1

    .line 1184717
    invoke-virtual {v1, v0}, LX/7FT;->a(LX/7F3;)V

    .line 1184718
    :cond_1
    sget-object v1, LX/7F7;->CHECKBOX:LX/7F7;

    if-ne v4, v1, :cond_2

    move-object v1, v0

    .line 1184719
    check-cast v1, LX/7F4;

    invoke-virtual {v1}, LX/7F4;->b()Z

    move-result v3

    move-object v1, v2

    .line 1184720
    check-cast v1, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;

    invoke-virtual {v1, v3}, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->setChecked(Z)V

    .line 1184721
    :cond_2
    sget-object v1, LX/7F7;->CHECKBOXWRITEIN:LX/7F7;

    if-ne v4, v1, :cond_3

    move-object v1, v0

    .line 1184722
    check-cast v1, LX/7FD;

    move-object v3, v2

    check-cast v3, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;

    iget-object v5, p0, LX/7EX;->h:Landroid/view/View$OnFocusChangeListener;

    invoke-direct {p0, v1, v3, v5}, LX/7EX;->a(LX/7FD;Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;Landroid/view/View$OnFocusChangeListener;)V

    .line 1184723
    :cond_3
    sget-object v1, LX/7F7;->RADIO:LX/7F7;

    if-ne v4, v1, :cond_4

    move-object v1, v2

    .line 1184724
    check-cast v1, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;

    move-object v3, v0

    .line 1184725
    check-cast v3, LX/7FB;

    .line 1184726
    invoke-virtual {v3}, LX/7FB;->b()Z

    move-result v5

    invoke-virtual {v1, v5}, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->setChecked(Z)V

    .line 1184727
    invoke-virtual {v3}, LX/7FB;->b()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1184728
    iget-object v3, p0, LX/7EX;->a:Ljava/util/HashMap;

    .line 1184729
    iget-object v5, v0, LX/7F3;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1184730
    invoke-virtual {v3, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1184731
    :cond_4
    sget-object v1, LX/7F7;->RADIOWRITEIN:LX/7F7;

    if-ne v4, v1, :cond_6

    move-object v1, v2

    .line 1184732
    check-cast v1, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;

    move-object v3, v0

    .line 1184733
    check-cast v3, LX/7FD;

    .line 1184734
    invoke-virtual {v3}, LX/7FD;->b()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1184735
    iget-object v5, p0, LX/7EX;->a:Ljava/util/HashMap;

    .line 1184736
    iget-object p1, v0, LX/7F3;->b:Ljava/lang/String;

    move-object v0, p1

    .line 1184737
    invoke-virtual {v5, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1184738
    :cond_5
    iget-object v0, p0, LX/7EX;->g:Landroid/view/View$OnFocusChangeListener;

    invoke-direct {p0, v3, v1, v0}, LX/7EX;->a(LX/7FD;Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;Landroid/view/View$OnFocusChangeListener;)V

    .line 1184739
    :cond_6
    sget-object v0, LX/7F7;->EDITTEXT:LX/7F7;

    if-ne v4, v0, :cond_7

    move-object v0, v2

    .line 1184740
    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;

    .line 1184741
    iget-object v1, p0, LX/7EX;->f:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->setItemOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1184742
    iget-object v1, v0, LX/7FT;->a:LX/7F3;

    move-object v1, v1

    .line 1184743
    iget-object v3, v1, LX/7F3;->b:Ljava/lang/String;

    move-object v1, v3

    .line 1184744
    iget-object v3, p0, LX/7EX;->e:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1184745
    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->a()V

    .line 1184746
    :cond_7
    return-object v2

    .line 1184747
    :pswitch_0
    invoke-static {p3}, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;

    move-result-object v1

    .line 1184748
    iget-object v0, p0, LX/7EX;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_8

    move-object v0, v1

    .line 1184749
    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;

    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->a()V

    .line 1184750
    :cond_8
    iget-object v0, p0, LX/7EX;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v2, v1

    .line 1184751
    goto/16 :goto_0

    .line 1184752
    :pswitch_1
    invoke-static {p3}, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;

    move-result-object p2

    .line 1184753
    iget-object v0, p0, LX/7EX;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v2, p2

    .line 1184754
    goto/16 :goto_0

    .line 1184755
    :pswitch_2
    invoke-static {p3, v4}, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->a(Landroid/view/ViewGroup;LX/7F7;)Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;

    move-result-object p2

    .line 1184756
    iget-object v0, p0, LX/7EX;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v2, p2

    .line 1184757
    goto/16 :goto_0

    .line 1184758
    :pswitch_3
    invoke-static {p3}, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;

    move-result-object p2

    move-object v2, p2

    .line 1184759
    goto/16 :goto_0

    .line 1184760
    :pswitch_4
    invoke-static {p3}, Lcom/facebook/structuredsurvey/views/SurveyDividerListItemView;->a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyDividerListItemView;

    move-result-object p2

    move-object v2, p2

    .line 1184761
    goto/16 :goto_0

    .line 1184762
    :pswitch_5
    invoke-static {p3}, Lcom/facebook/structuredsurvey/views/SurveySpaceListItemView;->a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveySpaceListItemView;

    move-result-object p2

    move-object v2, p2

    .line 1184763
    goto/16 :goto_0

    .line 1184764
    :pswitch_6
    invoke-static {p3}, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;

    move-result-object p2

    move-object v2, p2

    .line 1184765
    goto/16 :goto_0

    .line 1184766
    :pswitch_7
    invoke-static {p3}, Lcom/facebook/structuredsurvey/views/SurveyMessageListItemView;->a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyMessageListItemView;

    move-result-object p2

    move-object v2, p2

    .line 1184767
    goto/16 :goto_0

    .line 1184768
    :pswitch_8
    invoke-static {p3}, Lcom/facebook/structuredsurvey/views/SurveyImageBlockListItemView;->a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyImageBlockListItemView;

    move-result-object p2

    .line 1184769
    const v0, 0x7f0d2e4d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    .line 1184770
    iget-object v1, p0, LX/7EX;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v2, p2

    .line 1184771
    goto/16 :goto_0

    .line 1184772
    :pswitch_9
    invoke-static {p3}, Lcom/facebook/structuredsurvey/views/SurveyNotificationListItemView;->a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyNotificationListItemView;

    move-result-object p2

    move-object v2, p2

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1184710
    invoke-static {}, LX/7F7;->values()[LX/7F7;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
