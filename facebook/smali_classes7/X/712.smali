.class public final LX/712;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wC;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1163014
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163015
    return-void
.end method

.method public static a(LX/0QB;)LX/712;
    .locals 1

    .prologue
    .line 1163016
    new-instance v0, LX/712;

    invoke-direct {v0}, LX/712;-><init>()V

    .line 1163017
    move-object v0, v0

    .line 1163018
    return-object v0
.end method

.method private static a(LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1163019
    if-eqz p1, :cond_0

    .line 1163020
    :goto_0
    return-object p1

    .line 1163021
    :cond_0
    invoke-interface {p0}, LX/6vm;->a()LX/71I;

    move-result-object v0

    sget-object v1, LX/71I;->SINGLE_ROW_DIVIDER:LX/71I;

    if-ne v0, v1, :cond_1

    .line 1163022
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031341

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 1163023
    :cond_1
    invoke-interface {p0}, LX/6vm;->a()LX/71I;

    move-result-object v0

    sget-object v1, LX/71I;->SPACED_DOUBLE_ROW_DIVIDER:LX/71I;

    if-ne v0, v1, :cond_2

    .line 1163024
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031392

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 1163025
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid rowType provided for divider: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, LX/6vm;->a()LX/71I;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1163026
    sget-object v0, LX/711;->a:[I

    invoke-interface {p2}, LX/6vm;->a()LX/71I;

    move-result-object v1

    invoke-virtual {v1}, LX/71I;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1163027
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal row type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, LX/6vm;->a()LX/71I;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1163028
    :pswitch_0
    check-cast p2, LX/71G;

    .line 1163029
    if-nez p3, :cond_0

    new-instance p3, LX/70u;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/70u;-><init>(Landroid/content/Context;)V

    .line 1163030
    :goto_0
    iput-object p1, p3, LX/70u;->b:LX/6qh;

    .line 1163031
    invoke-virtual {p3, p2}, LX/70u;->a(LX/71G;)V

    .line 1163032
    move-object v0, p3

    .line 1163033
    :goto_1
    return-object v0

    .line 1163034
    :pswitch_1
    invoke-static {p2, p3, p4}, LX/712;->a(LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 1163035
    :pswitch_2
    if-nez p3, :cond_1

    new-instance p3, Lcom/facebook/payments/picker/HeaderItemView;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, Lcom/facebook/payments/picker/HeaderItemView;-><init>(Landroid/content/Context;)V

    .line 1163036
    :goto_2
    invoke-virtual {p3, p2}, Lcom/facebook/payments/picker/HeaderItemView;->a(LX/6vm;)V

    .line 1163037
    move-object v0, p3

    .line 1163038
    goto :goto_1

    .line 1163039
    :pswitch_3
    if-eqz p3, :cond_2

    .line 1163040
    :goto_3
    move-object v0, p3

    .line 1163041
    goto :goto_1

    .line 1163042
    :cond_0
    check-cast p3, LX/70u;

    goto :goto_0

    .line 1163043
    :cond_1
    check-cast p3, Lcom/facebook/payments/picker/HeaderItemView;

    goto :goto_2

    :cond_2
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030f5c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
