.class public final LX/76G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0q0;


# instance fields
.field public final synthetic a:LX/2Hk;


# direct methods
.method public constructor <init>(LX/2Hk;)V
    .locals 0

    .prologue
    .line 1170872
    iput-object p1, p0, LX/76G;->a:LX/2Hk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 6

    .prologue
    .line 1170862
    if-eqz p1, :cond_1

    .line 1170863
    iget-object v0, p0, LX/76G;->a:LX/2Hk;

    iget-object v0, v0, LX/2Hk;->q:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 1170864
    iget-object v0, p0, LX/76G;->a:LX/2Hk;

    iget-object v0, v0, LX/2Hk;->q:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 1170865
    iget-object v0, p0, LX/76G;->a:LX/2Hk;

    const/4 v1, 0x0

    .line 1170866
    iput-object v1, v0, LX/2Hk;->q:Ljava/util/concurrent/ScheduledFuture;

    .line 1170867
    :cond_0
    :goto_0
    return-void

    .line 1170868
    :cond_1
    iget-object v0, p0, LX/76G;->a:LX/2Hk;

    iget-object v0, v0, LX/2Hk;->q:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_0

    .line 1170869
    iget-object v0, p0, LX/76G;->a:LX/2Hk;

    iget-object v1, p0, LX/76G;->a:LX/2Hk;

    iget-object v1, v1, LX/2Hk;->a:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v2, p0, LX/76G;->a:LX/2Hk;

    iget-object v2, v2, LX/2Hk;->t:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3a98

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v4, v5, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    .line 1170870
    iput-object v1, v0, LX/2Hk;->q:Ljava/util/concurrent/ScheduledFuture;

    .line 1170871
    goto :goto_0
.end method
