.class public LX/7gM;
.super LX/7gK;
.source ""

# interfaces
.implements LX/3FQ;
.implements LX/3iB;


# instance fields
.field public a:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final c:Landroid/widget/ImageView;

.field public d:LX/0yL;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1224136
    invoke-direct {p0, p1}, LX/7gK;-><init>(Landroid/content/Context;)V

    .line 1224137
    const-class v0, LX/7gM;

    invoke-static {v0, p0}, LX/7gM;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1224138
    const v0, 0x7f03157e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1224139
    const v0, 0x7f0d3060

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/7gM;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1224140
    const v0, 0x7f0d3038

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/7gM;->c:Landroid/widget/ImageView;

    .line 1224141
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/7gM;

    invoke-static {p0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object p0

    check-cast p0, LX/0yc;

    iput-object p0, p1, LX/7gM;->a:LX/0yc;

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 1224161
    iget-object v0, p0, LX/7gM;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1224162
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1224163
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1224164
    iget-object v1, p0, LX/7gM;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1224165
    return-void
.end method

.method public getAudioChannelLayout()LX/03z;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1224168
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCoverController()LX/1aZ;
    .locals 1

    .prologue
    .line 1224166
    iget-object v0, p0, LX/7gM;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v0

    return-object v0
.end method

.method public getLastStartPosition()I
    .locals 1

    .prologue
    .line 1224167
    const/4 v0, 0x0

    return v0
.end method

.method public getProjectionType()LX/19o;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1224159
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSeekPosition()I
    .locals 1

    .prologue
    .line 1224160
    const/4 v0, 0x0

    return v0
.end method

.method public getTransitionNode()LX/3FT;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1224158
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVideoStoryPersistentState()LX/2oM;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1224157
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7cf00e96

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1224150
    invoke-super {p0}, LX/7gK;->onAttachedToWindow()V

    .line 1224151
    iget-object v1, p0, LX/7gM;->a:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1224152
    iget-object v1, p0, LX/7gM;->c:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1224153
    iget-object v1, p0, LX/7gM;->d:LX/0yL;

    if-nez v1, :cond_0

    .line 1224154
    new-instance v1, LX/7gL;

    invoke-direct {v1, p0}, LX/7gL;-><init>(LX/7gM;)V

    iput-object v1, p0, LX/7gM;->d:LX/0yL;

    .line 1224155
    :cond_0
    iget-object v1, p0, LX/7gM;->a:LX/0yc;

    iget-object v2, p0, LX/7gM;->d:LX/0yL;

    invoke-virtual {v1, v2}, LX/0yc;->a(LX/0yL;)V

    .line 1224156
    :cond_1
    const/16 v1, 0x2d

    const v2, -0x70c9bcd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x508a7438

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1224146
    invoke-super {p0}, LX/7gK;->onDetachedFromWindow()V

    .line 1224147
    iget-object v1, p0, LX/7gM;->d:LX/0yL;

    if-eqz v1, :cond_0

    .line 1224148
    iget-object v1, p0, LX/7gM;->a:LX/0yc;

    iget-object v2, p0, LX/7gM;->d:LX/0yL;

    invoke-virtual {v1, v2}, LX/0yc;->b(LX/0yL;)V

    .line 1224149
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x7d4a43d1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1

    .prologue
    .line 1224144
    iget-object v0, p0, LX/7gM;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundResource(I)V

    .line 1224145
    return-void
.end method

.method public setCoverController(LX/1aZ;)V
    .locals 1

    .prologue
    .line 1224142
    iget-object v0, p0, LX/7gM;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1224143
    return-void
.end method
