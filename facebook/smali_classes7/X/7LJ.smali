.class public final LX/7LJ;
.super LX/7Ji;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/InlineVideoView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/InlineVideoView;)V
    .locals 0

    .prologue
    .line 1197751
    iput-object p1, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    invoke-direct {p0}, LX/7Ji;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/player/InlineVideoView;B)V
    .locals 0

    .prologue
    .line 1197750
    invoke-direct {p0, p1}, LX/7LJ;-><init>(Lcom/facebook/video/player/InlineVideoView;)V

    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 1197748
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->d:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/video/player/InlineVideoView$InlineVideoPlayerListener$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/player/InlineVideoView$InlineVideoPlayerListener$1;-><init>(LX/7LJ;)V

    const v2, -0x49f9bbc9    # -2.0007262E-6f

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1197749
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1197745
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197746
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0}, LX/2pf;->a()V

    .line 1197747
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1197739
    invoke-direct {p0}, LX/7LJ;->h()V

    .line 1197740
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    const/4 v1, 0x1

    .line 1197741
    iput-boolean v1, v0, Lcom/facebook/video/player/InlineVideoView;->l:Z

    .line 1197742
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197743
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(I)V

    .line 1197744
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 1197729
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onVideoSizeUpdated: w = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", h = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1197730
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    .line 1197731
    iput p1, v0, Lcom/facebook/video/player/InlineVideoView;->q:I

    .line 1197732
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    .line 1197733
    iput p2, v0, Lcom/facebook/video/player/InlineVideoView;->r:I

    .line 1197734
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->y:LX/7LL;

    sget-object v1, LX/7LL;->NONE:LX/7LL;

    if-eq v0, v1, :cond_0

    .line 1197735
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    invoke-virtual {v0}, Lcom/facebook/video/player/InlineVideoView;->requestLayout()V

    .line 1197736
    :cond_0
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_1

    .line 1197737
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->a(II)V

    .line 1197738
    :cond_1
    return-void
.end method

.method public final a(LX/04g;)V
    .locals 1

    .prologue
    .line 1197725
    invoke-direct {p0}, LX/7LJ;->h()V

    .line 1197726
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197727
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(LX/04g;)V

    .line 1197728
    :cond_0
    return-void
.end method

.method public final a(LX/04g;Z)V
    .locals 2

    .prologue
    .line 1197721
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onBeforeVideoPlay: success = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1197722
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197723
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->a(LX/04g;Z)V

    .line 1197724
    :cond_0
    return-void
.end method

.method public final a(LX/2qD;)V
    .locals 1

    .prologue
    .line 1197718
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197719
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(LX/2qD;)V

    .line 1197720
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1197752
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197753
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(Landroid/graphics/Bitmap;)V

    .line 1197754
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;LX/7Jj;)V
    .locals 1

    .prologue
    .line 1197715
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197716
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->a(Ljava/lang/String;LX/7Jj;)V

    .line 1197717
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1197712
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197713
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0}, LX/2pf;->b()V

    .line 1197714
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1197709
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197710
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->b(I)V

    .line 1197711
    :cond_0
    return-void
.end method

.method public final b(LX/04g;)V
    .locals 1

    .prologue
    .line 1197705
    invoke-direct {p0}, LX/7LJ;->h()V

    .line 1197706
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197707
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->b(LX/04g;)V

    .line 1197708
    :cond_0
    return-void
.end method

.method public final b(LX/04g;Z)V
    .locals 2

    .prologue
    .line 1197701
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onBeforeVideoPause: success = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1197702
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197703
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->b(LX/04g;Z)V

    .line 1197704
    :cond_0
    return-void
.end method

.method public final b(LX/2qD;)V
    .locals 1

    .prologue
    .line 1197698
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197699
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->b(LX/2qD;)V

    .line 1197700
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1197695
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197696
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0}, LX/2pf;->c()V

    .line 1197697
    :cond_0
    return-void
.end method

.method public final c(LX/04g;)V
    .locals 3

    .prologue
    .line 1197691
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197692
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->c(LX/04g;)V

    .line 1197693
    :cond_0
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->d:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/video/player/InlineVideoView$InlineVideoPlayerListener$2;

    invoke-direct {v1, p0}, Lcom/facebook/video/player/InlineVideoView$InlineVideoPlayerListener$2;-><init>(LX/7LJ;)V

    const v2, -0x2bf3d794

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1197694
    return-void
.end method

.method public final c(LX/04g;Z)V
    .locals 2

    .prologue
    .line 1197687
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onBeforeVideoStop: success = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1197688
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_0

    .line 1197689
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->c(LX/04g;Z)V

    .line 1197690
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1197681
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0}, LX/2q8;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1197682
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    sget-object v1, LX/04g;->BY_MANAGER:LX/04g;

    invoke-interface {v0, v1}, LX/2q7;->c(LX/04g;)V

    .line 1197683
    :cond_0
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    invoke-static {v0}, Lcom/facebook/video/player/InlineVideoView;->g(Lcom/facebook/video/player/InlineVideoView;)V

    .line 1197684
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    if-eqz v0, :cond_1

    .line 1197685
    iget-object v0, p0, LX/7LJ;->a:Lcom/facebook/video/player/InlineVideoView;

    iget-object v0, v0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    invoke-interface {v0}, LX/2pf;->d()V

    .line 1197686
    :cond_1
    return-void
.end method
