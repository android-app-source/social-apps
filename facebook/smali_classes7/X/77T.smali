.class public LX/77T;
.super LX/2g7;
.source ""


# instance fields
.field public final a:LX/13N;

.field public final b:LX/0SG;


# direct methods
.method public constructor <init>(LX/13N;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171845
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1171846
    iput-object p1, p0, LX/77T;->a:LX/13N;

    .line 1171847
    iput-object p2, p0, LX/77T;->b:LX/0SG;

    .line 1171848
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 13
    .param p1    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1171849
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1171850
    iget-object v2, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    .line 1171851
    invoke-virtual {p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->b()Ljava/util/Map;

    move-result-object v0

    const-string v1, "event_count"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1171852
    iget-boolean v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->passIfNotSupported:Z

    .line 1171853
    :goto_0
    return v0

    .line 1171854
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->b()Ljava/util/Map;

    move-result-object v0

    const-string v1, "event_count"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1171855
    invoke-virtual {p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->b()Ljava/util/Map;

    move-result-object v0

    const-string v1, "event"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1171856
    invoke-virtual {p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->b()Ljava/util/Map;

    move-result-object v1

    const-string v3, "metric"

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1171857
    sget-object v3, LX/77Q;->a:[I

    invoke-static {v0}, LX/77R;->fromString(Ljava/lang/String;)LX/77R;

    move-result-object v0

    invoke-virtual {v0}, LX/77R;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    .line 1171858
    iget-boolean v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->passIfNotSupported:Z

    goto :goto_0

    .line 1171859
    :pswitch_0
    sget-object v0, LX/2fy;->IMPRESSION:LX/2fy;

    .line 1171860
    :goto_1
    sget-object v3, LX/77Q;->b:[I

    invoke-static {v1}, LX/77S;->fromString(Ljava/lang/String;)LX/77S;

    move-result-object v1

    invoke-virtual {v1}, LX/77S;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_1

    .line 1171861
    iget-boolean v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->passIfNotSupported:Z

    goto :goto_0

    .line 1171862
    :pswitch_1
    sget-object v0, LX/2fy;->PRIMARY_ACTION:LX/2fy;

    goto :goto_1

    .line 1171863
    :pswitch_2
    sget-object v0, LX/2fy;->SECONDARY_ACTION:LX/2fy;

    goto :goto_1

    .line 1171864
    :pswitch_3
    sget-object v0, LX/2fy;->DISMISS_ACTION:LX/2fy;

    goto :goto_1

    .line 1171865
    :pswitch_4
    iget-object v6, p0, LX/77T;->a:LX/13N;

    invoke-virtual {v6, v2, v0}, LX/13N;->b(Ljava/lang/String;LX/2fy;)J

    move-result-wide v6

    .line 1171866
    iget-object v8, p0, LX/77T;->b:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    .line 1171867
    const-wide/16 v10, 0x3e8

    mul-long/2addr v10, v4

    .line 1171868
    sub-long v6, v8, v6

    cmp-long v6, v6, v10

    if-ltz v6, :cond_1

    const/4 v6, 0x1

    :goto_2
    move v0, v6

    .line 1171869
    goto :goto_0

    .line 1171870
    :pswitch_5
    iget-object v6, p0, LX/77T;->a:LX/13N;

    invoke-virtual {v6, v2, v0}, LX/13N;->b(Ljava/lang/String;LX/2fy;)J

    move-result-wide v6

    .line 1171871
    iget-object v8, p0, LX/77T;->b:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    .line 1171872
    const-wide/16 v10, 0x3e8

    mul-long/2addr v10, v4

    .line 1171873
    sub-long v6, v8, v6

    cmp-long v6, v6, v10

    if-gtz v6, :cond_2

    const/4 v6, 0x1

    :goto_3
    move v0, v6

    .line 1171874
    goto/16 :goto_0

    .line 1171875
    :pswitch_6
    iget-object v6, p0, LX/77T;->a:LX/13N;

    invoke-virtual {v6, v2, v0}, LX/13N;->a(Ljava/lang/String;LX/2fy;)I

    move-result v6

    int-to-long v6, v6

    .line 1171876
    cmp-long v6, v6, v4

    if-ltz v6, :cond_3

    const/4 v6, 0x1

    :goto_4
    move v0, v6

    .line 1171877
    goto/16 :goto_0

    .line 1171878
    :pswitch_7
    iget-object v6, p0, LX/77T;->a:LX/13N;

    invoke-virtual {v6, v2, v0}, LX/13N;->a(Ljava/lang/String;LX/2fy;)I

    move-result v6

    int-to-long v6, v6

    .line 1171879
    cmp-long v6, v6, v4

    if-gtz v6, :cond_4

    const/4 v6, 0x1

    :goto_5
    move v0, v6

    .line 1171880
    goto/16 :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    goto :goto_3

    :cond_3
    const/4 v6, 0x0

    goto :goto_4

    :cond_4
    const/4 v6, 0x0

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
