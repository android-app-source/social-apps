.class public final LX/7lq;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/publish/common/PublishPostParams;

.field public final synthetic b:LX/7ln;

.field public final synthetic c:Landroid/os/Bundle;

.field public final synthetic d:Z

.field public final synthetic e:Landroid/content/Intent;

.field public final synthetic f:LX/7ll;

.field public final synthetic g:Ljava/lang/String;

.field public final synthetic h:Lcom/facebook/composer/publish/common/PendingStory;

.field public final synthetic i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Lcom/facebook/composer/publish/common/PublishPostParams;LX/7ln;Landroid/os/Bundle;ZLandroid/content/Intent;LX/7ll;Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 0

    .prologue
    .line 1235725
    iput-object p1, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iput-object p2, p0, LX/7lq;->a:Lcom/facebook/composer/publish/common/PublishPostParams;

    iput-object p3, p0, LX/7lq;->b:LX/7ln;

    iput-object p4, p0, LX/7lq;->c:Landroid/os/Bundle;

    iput-boolean p5, p0, LX/7lq;->d:Z

    iput-object p6, p0, LX/7lq;->e:Landroid/content/Intent;

    iput-object p7, p0, LX/7lq;->f:LX/7ll;

    iput-object p8, p0, LX/7lq;->g:Ljava/lang/String;

    iput-object p9, p0, LX/7lq;->h:Lcom/facebook/composer/publish/common/PendingStory;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    .line 1235726
    iget-boolean v0, p0, LX/7lq;->d:Z

    if-eqz v0, :cond_1

    sget-object v0, LX/21D;->PAGE_FEED:LX/21D;

    invoke-virtual {v0}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/7lq;->a:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-object v1, v1, Lcom/facebook/composer/publish/common/PublishPostParams;->sourceType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1235727
    const v0, 0x7f0813fd

    .line 1235728
    iget-object v1, p0, LX/7lq;->a:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-boolean v1, v1, Lcom/facebook/composer/publish/common/PublishPostParams;->isBackoutDraft:Z

    if-eqz v1, :cond_0

    .line 1235729
    const v0, 0x7f0814a0

    .line 1235730
    :cond_0
    iget-object v1, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v1, v1, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->j:LX/4nT;

    iget-object v2, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v2, v2, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->v:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4nT;->a(Ljava/lang/String;)V

    .line 1235731
    iget-object v0, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->r:LX/0ie;

    iget-object v1, p0, LX/7lq;->a:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-object v1, v1, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    .line 1235732
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "composer_session_id"

    invoke-virtual {v2, v3, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v2

    .line 1235733
    iget-object v3, v0, LX/0ie;->a:LX/0if;

    sget-object v4, LX/0ig;->ar:LX/0ih;

    sget-object v5, LX/8D1;->SHARE:LX/8D1;

    invoke-virtual {v5}, LX/8D1;->name()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1235734
    :goto_0
    iget-object v0, p0, LX/7lq;->f:LX/7ll;

    iget-object v1, p0, LX/7lq;->g:Ljava/lang/String;

    iget-object v2, p0, LX/7lq;->h:Lcom/facebook/composer/publish/common/PendingStory;

    invoke-interface {v0, v1, v2}, LX/7ll;->a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V

    .line 1235735
    return-void

    .line 1235736
    :cond_1
    iget-object v0, p0, LX/7lq;->a:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-boolean v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->isBackoutDraft:Z

    if-eqz v0, :cond_2

    .line 1235737
    iget-object v0, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->j:LX/4nT;

    iget-object v1, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v1, v1, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->p:LX/0ad;

    sget-char v2, LX/1EB;->m:C

    const v3, 0x7f0814a0

    iget-object v4, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v4, v4, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->v:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4nT;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1235738
    :cond_2
    iget-object v0, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->r:LX/0ie;

    iget-object v1, p0, LX/7lq;->a:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-object v1, v1, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    .line 1235739
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "composer_session_id"

    invoke-virtual {v2, v3, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v2

    .line 1235740
    iget-object v3, v0, LX/0ie;->a:LX/0if;

    sget-object v4, LX/0ig;->ar:LX/0ih;

    sget-object v5, LX/8D1;->POST:LX/8D1;

    invoke-virtual {v5}, LX/8D1;->name()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1235741
    goto :goto_0
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 11

    .prologue
    const/4 v4, 0x1

    .line 1235742
    iget-object v0, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v1, p0, LX/7lq;->a:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-object v1, v1, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Lcom/facebook/fbservice/service/ServiceException;Ljava/lang/String;)Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    .line 1235743
    iget-boolean v1, v0, Lcom/facebook/composer/publish/common/ErrorDetails;->isSentryWarningWithUserConfirmationRequired:Z

    if-eqz v1, :cond_1

    .line 1235744
    iget-object v1, p0, LX/7lq;->a:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-boolean v1, v1, Lcom/facebook/composer/publish/common/PublishPostParams;->canHandleSentryWarning:Z

    invoke-static {v1}, LX/0nE;->a(Z)V

    .line 1235745
    iget-object v1, p0, LX/7lq;->b:LX/7ln;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235746
    iget-object v1, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v1, v1, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->q:LX/7m2;

    iget-object v2, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v2, v2, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->v:Landroid/content/Context;

    .line 1235747
    new-instance p1, LX/7m1;

    invoke-static {v1}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-static {v1}, LX/0gd;->a(LX/0QB;)LX/0gd;

    move-result-object v4

    check-cast v4, LX/0gd;

    invoke-direct {p1, v2, v3, v4}, LX/7m1;-><init>(Landroid/content/Context;Landroid/os/Handler;LX/0gd;)V

    .line 1235748
    move-object v1, p1

    .line 1235749
    iget-object v2, v0, Lcom/facebook/composer/publish/common/ErrorDetails;->userTitle:Ljava/lang/String;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/ErrorDetails;->userMessage:Ljava/lang/String;

    iget-object v3, p0, LX/7lq;->b:LX/7ln;

    .line 1235750
    iget-object v4, v1, LX/7m1;->c:LX/0gd;

    sget-object p0, LX/0ge;->MEME_BUSTING_SENTRY_WARNING_RECEIVED:LX/0ge;

    const/4 p1, 0x0

    invoke-virtual {v4, p0, p1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 1235751
    new-instance v4, LX/0ju;

    iget-object p0, v1, LX/7m1;->a:Landroid/content/Context;

    invoke-direct {v4, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v4

    iget-object p0, v1, LX/7m1;->a:Landroid/content/Context;

    const p1, 0x7f08001a

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    new-instance p1, LX/7m0;

    invoke-direct {p1, v1, v3}, LX/7m0;-><init>(LX/7m1;LX/7ln;)V

    invoke-virtual {v4, p0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v4

    iget-object p0, v1, LX/7m1;->a:Landroid/content/Context;

    const p1, 0x7f080017

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    new-instance p1, LX/7lz;

    invoke-direct {p1, v1, v3}, LX/7lz;-><init>(LX/7m1;LX/7ln;)V

    invoke-virtual {v4, p0, p1}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v4

    .line 1235752
    iget-object p0, v1, LX/7m1;->b:Landroid/os/Handler;

    new-instance p1, Lcom/facebook/composer/publish/SentryWarningDialogController$3;

    invoke-direct {p1, v1, v4}, Lcom/facebook/composer/publish/SentryWarningDialogController$3;-><init>(LX/7m1;LX/0ju;)V

    const v4, -0x7608281

    invoke-static {p0, p1, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1235753
    :cond_0
    :goto_0
    return-void

    .line 1235754
    :cond_1
    iget-object v1, p0, LX/7lq;->c:Landroid/os/Bundle;

    const-string v2, "suppress_failure_notification"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1235755
    iget-object v2, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v2, v2, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->f:LX/1CW;

    invoke-virtual {v2, p1, v4, v4}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v2

    .line 1235756
    iget-object v3, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v4, p0, LX/7lq;->a:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-object v4, v4, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    .line 1235757
    invoke-static {v3, v4, v0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a$redex0(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Ljava/lang/String;Lcom/facebook/composer/publish/common/ErrorDetails;)V

    .line 1235758
    iget-boolean v0, v0, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    if-nez v0, :cond_2

    .line 1235759
    iget-object v0, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->o:LX/7ly;

    iget-object v1, p0, LX/7lq;->a:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-object v1, v1, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/7ly;->a(Ljava/lang/String;)LX/7lx;

    move-result-object v0

    invoke-virtual {v0}, LX/7lx;->a()V

    goto :goto_0

    .line 1235760
    :cond_2
    if-eqz v1, :cond_3

    iget-boolean v0, p0, LX/7lq;->d:Z

    if-nez v0, :cond_3

    .line 1235761
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 1235762
    sget-object v3, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-eq v0, v3, :cond_0

    .line 1235763
    :cond_3
    iget-boolean v0, p0, LX/7lq;->d:Z

    if-eqz v0, :cond_5

    .line 1235764
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 1235765
    sget-object v3, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v0, v3, :cond_5

    .line 1235766
    iget-object v0, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->j:LX/4nT;

    iget-object v2, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v2, v2, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->v:Landroid/content/Context;

    const v3, 0x7f081438

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/4nT;->a(Ljava/lang/String;)V

    .line 1235767
    :goto_1
    iget-object v0, p0, LX/7lq;->e:Landroid/content/Intent;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->h:LX/Gvn;

    if-eqz v0, :cond_4

    if-nez v1, :cond_4

    .line 1235768
    iget-object v0, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->h:LX/Gvn;

    iget-object v1, p0, LX/7lq;->e:Landroid/content/Intent;

    .line 1235769
    iget-object v5, v0, LX/Gvn;->c:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/BAa;

    iget-object v6, v0, LX/Gvn;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f083566

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/BAa;->a(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v5

    iget-object v6, v0, LX/Gvn;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f083568

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/BAa;->c(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v5

    iget-object v6, v0, LX/Gvn;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f083567

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/BAa;->d(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v5

    const v6, 0x7f0218e4

    invoke-virtual {v5, v6}, LX/BAa;->a(I)LX/BAa;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, LX/BAa;->a(J)LX/BAa;

    move-result-object v7

    .line 1235770
    new-instance v5, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-direct {v5}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;-><init>()V

    sget-object v6, Lcom/facebook/notifications/constants/NotificationType;->ERROR_CLIENT_NOTIFICATION:Lcom/facebook/notifications/constants/NotificationType;

    .line 1235771
    iput-object v6, v5, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->b:Lcom/facebook/notifications/constants/NotificationType;

    .line 1235772
    move-object v10, v5

    .line 1235773
    iget-object v5, v0, LX/Gvn;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2c4;

    sget-object v6, Lcom/facebook/notifications/constants/NotificationType;->POST_FAILED:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v9, LX/8D4;->SERVICE:LX/8D4;

    move-object v8, v1

    invoke-virtual/range {v5 .. v10}, LX/2c4;->a(Lcom/facebook/notifications/constants/NotificationType;LX/BAa;Landroid/content/Intent;LX/8D4;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 1235774
    :cond_4
    iget-object v0, p0, LX/7lq;->f:LX/7ll;

    iget-object v1, p0, LX/7lq;->g:Ljava/lang/String;

    iget-object v2, p0, LX/7lq;->h:Lcom/facebook/composer/publish/common/PendingStory;

    invoke-interface {v0, v1, v2, p1}, LX/7ll;->a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;Lcom/facebook/fbservice/service/ServiceException;)V

    goto/16 :goto_0

    .line 1235775
    :cond_5
    iget-object v0, p0, LX/7lq;->i:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->j:LX/4nT;

    invoke-virtual {v0, v2}, LX/4nT;->a(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1235776
    invoke-direct {p0}, LX/7lq;->a()V

    return-void
.end method
