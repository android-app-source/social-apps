.class public final LX/7Ns;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V
    .locals 0

    .prologue
    .line 1200754
    iput-object p1, p0, LX/7Ns;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x422d3d12

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1200755
    iget-object v1, p0, LX/7Ns;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    const/4 p1, 0x2

    .line 1200756
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->R:LX/7Ny;

    sget-object p0, LX/7Ny;->CASTING_STOPPED:LX/7Ny;

    if-ne v3, p0, :cond_1

    .line 1200757
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->r:LX/7J3;

    const-string p0, "play"

    invoke-virtual {v3, p0, p1}, LX/7J3;->a(Ljava/lang/String;I)V

    .line 1200758
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v3}, LX/37Y;->r()V

    .line 1200759
    :cond_0
    :goto_0
    const v1, 0x55a0f574

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1200760
    :cond_1
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->R:LX/7Ny;

    sget-object p0, LX/7Ny;->CASTING_CURRENT:LX/7Ny;

    if-ne v3, p0, :cond_0

    .line 1200761
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v3}, LX/37Y;->s()LX/38j;

    move-result-object v3

    invoke-virtual {v3}, LX/38j;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1200762
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->r:LX/7J3;

    const-string p0, "pause"

    const/4 p1, 0x3

    invoke-virtual {v3, p0, p1}, LX/7J3;->a(Ljava/lang/String;I)V

    .line 1200763
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v3}, LX/37Y;->q()V

    goto :goto_0

    .line 1200764
    :cond_2
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->r:LX/7J3;

    const-string p0, "play"

    invoke-virtual {v3, p0, p1}, LX/7J3;->a(Ljava/lang/String;I)V

    .line 1200765
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v3}, LX/37Y;->r()V

    goto :goto_0
.end method
