.class public final LX/6rY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field public i:Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;

.field public j:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1152344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152345
    iput-object p1, p0, LX/6rY;->a:Ljava/lang/String;

    .line 1152346
    iput-object p2, p0, LX/6rY;->b:Ljava/lang/String;

    .line 1152347
    iput-object p3, p0, LX/6rY;->c:Ljava/lang/String;

    .line 1152348
    iput-object p4, p0, LX/6rY;->d:Ljava/lang/String;

    .line 1152349
    iput-object p5, p0, LX/6rY;->e:LX/0Px;

    .line 1152350
    iput-object p6, p0, LX/6rY;->f:LX/0Px;

    .line 1152351
    return-void
.end method

.method public static a(Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)LX/6rY;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1152352
    new-instance v0, LX/6rY;

    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->e:LX/0Px;

    iget-object v6, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->f:LX/0Px;

    invoke-direct/range {v0 .. v6}, LX/6rY;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;)V

    iget-boolean v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->g:Z

    .line 1152353
    iput-boolean v1, v0, LX/6rY;->g:Z

    .line 1152354
    move-object v0, v0

    .line 1152355
    iget-boolean v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->h:Z

    .line 1152356
    iput-boolean v1, v0, LX/6rY;->h:Z

    .line 1152357
    move-object v0, v0

    .line 1152358
    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->i:Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;

    .line 1152359
    iput-object v1, v0, LX/6rY;->i:Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;

    .line 1152360
    move-object v0, v0

    .line 1152361
    iget-boolean v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->j:Z

    .line 1152362
    iput-boolean v1, v0, LX/6rY;->j:Z

    .line 1152363
    move-object v0, v0

    .line 1152364
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;
    .locals 2

    .prologue
    .line 1152365
    new-instance v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    invoke-direct {v0, p0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;-><init>(LX/6rY;)V

    return-object v0
.end method
