.class public final LX/8Pb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "LX/8QW;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/PrivacyOperationsClient;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/PrivacyOperationsClient;)V
    .locals 0

    .prologue
    .line 1341895
    iput-object p1, p0, LX/8Pb;->a:Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1341896
    check-cast p1, Ljava/util/List;

    const/4 v2, 0x0

    .line 1341897
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    :cond_0
    move-object v0, v2

    .line 1341898
    :goto_0
    return-object v0

    .line 1341899
    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1341900
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8QJ;

    .line 1341901
    if-eqz v0, :cond_2

    if-nez v1, :cond_3

    :cond_2
    move-object v0, v2

    .line 1341902
    goto :goto_0

    .line 1341903
    :cond_3
    new-instance v2, LX/8QV;

    invoke-direct {v2}, LX/8QV;-><init>()V

    .line 1341904
    iput-object v0, v2, LX/8QV;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1341905
    move-object v0, v2

    .line 1341906
    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    .line 1341907
    iget-object v2, v1, LX/8QJ;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0, v2}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v2

    .line 1341908
    new-instance v0, LX/8QW;

    iget-boolean v1, v1, LX/8QJ;->d:Z

    invoke-direct {v0, v2, v1}, LX/8QW;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;Z)V

    goto :goto_0
.end method
