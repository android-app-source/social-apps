.class public LX/6wz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wy;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wy",
        "<",
        "Lcom/facebook/payments/form/model/AmountFormData;",
        ">;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/5fv;

.field public final c:LX/6x7;

.field public final d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public e:LX/6x9;

.field private f:LX/6qh;

.field public g:Lcom/facebook/payments/currency/CurrencyAmount;

.field public h:Lcom/facebook/payments/currency/CurrencyAmount;

.field public i:Ljava/lang/String;

.field public j:LX/5fu;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/5fv;LX/6x7;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1158227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158228
    iput-object p1, p0, LX/6wz;->a:Landroid/content/Context;

    .line 1158229
    iput-object p2, p0, LX/6wz;->b:LX/5fv;

    .line 1158230
    iput-object p3, p0, LX/6wz;->c:LX/6x7;

    .line 1158231
    new-instance v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, LX/6wz;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/6wz;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1158232
    return-void
.end method

.method public static a(LX/0QB;)LX/6wz;
    .locals 6

    .prologue
    .line 1158233
    const-class v1, LX/6wz;

    monitor-enter v1

    .line 1158234
    :try_start_0
    sget-object v0, LX/6wz;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1158235
    sput-object v2, LX/6wz;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1158236
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1158237
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1158238
    new-instance p0, LX/6wz;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v4

    check-cast v4, LX/5fv;

    invoke-static {v0}, LX/6x7;->b(LX/0QB;)LX/6x7;

    move-result-object v5

    check-cast v5, LX/6x7;

    invoke-direct {p0, v3, v4, v5}, LX/6wz;-><init>(Landroid/content/Context;LX/5fv;LX/6x7;)V

    .line 1158239
    move-object v0, p0

    .line 1158240
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1158241
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6wz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1158242
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1158243
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1158244
    invoke-virtual {p0}, LX/6wz;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1158245
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1158246
    const-string v1, "extra_currency_amount"

    iget-object v2, p0, LX/6wz;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    .line 1158247
    new-instance v3, Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v4, p0, LX/6wz;->i:Ljava/lang/String;

    new-instance v5, Ljava/math/BigDecimal;

    invoke-direct {v5, v2}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4, v5}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    move-object v2, v3

    .line 1158248
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1158249
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1158250
    const-string v2, "extra_activity_result_data"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1158251
    iget-object v0, p0, LX/6wz;->f:LX/6qh;

    new-instance v2, LX/73T;

    sget-object v3, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v2, v3, v1}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {v0, v2}, LX/6qh;->a(LX/73T;)V

    .line 1158252
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1158253
    iput-object p1, p0, LX/6wz;->f:LX/6qh;

    .line 1158254
    return-void
.end method

.method public final a(LX/6x9;)V
    .locals 0

    .prologue
    .line 1158255
    iput-object p1, p0, LX/6wz;->e:LX/6x9;

    .line 1158256
    return-void
.end method

.method public final a(LX/6xD;Lcom/facebook/payments/form/model/PaymentsFormData;)V
    .locals 7

    .prologue
    .line 1158257
    check-cast p2, Lcom/facebook/payments/form/model/AmountFormData;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1158258
    iget-object v0, p2, Lcom/facebook/payments/form/model/AmountFormData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v0, v0

    .line 1158259
    iput-object v0, p0, LX/6wz;->g:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1158260
    iget-object v0, p2, Lcom/facebook/payments/form/model/AmountFormData;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v0, v0

    .line 1158261
    iput-object v0, p0, LX/6wz;->h:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1158262
    iget-object v0, p2, Lcom/facebook/payments/form/model/AmountFormData;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1158263
    iput-object v0, p0, LX/6wz;->i:Ljava/lang/String;

    .line 1158264
    iget-object v0, p2, Lcom/facebook/payments/form/model/AmountFormData;->c:Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-object v0, v0

    .line 1158265
    iget-object v0, v0, Lcom/facebook/payments/form/model/FormFieldAttributes;->e:LX/6xO;

    invoke-virtual {v0}, LX/6xO;->getInputType()I

    move-result v0

    sget-object v1, LX/6xO;->PRICE:LX/6xO;

    invoke-virtual {v1}, LX/6xO;->getInputType()I

    move-result v1

    if-ne v0, v1, :cond_0

    sget-object v0, LX/5fu;->NO_CURRENCY_SYMBOL:LX/5fu;

    :goto_0
    iput-object v0, p0, LX/6wz;->j:LX/5fu;

    .line 1158266
    iget-object v0, p0, LX/6wz;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    new-instance v1, LX/6wx;

    invoke-direct {v1, p0}, LX/6wx;-><init>(LX/6wz;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a(Landroid/text/TextWatcher;)V

    .line 1158267
    iget-object v0, p2, Lcom/facebook/payments/form/model/AmountFormData;->c:Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-object v0, v0

    .line 1158268
    iget-object v1, p0, LX/6wz;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const/16 v4, 0x30

    invoke-virtual {v1, v4}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setGravity(I)V

    .line 1158269
    iget-object v1, p0, LX/6wz;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v4, v0, Lcom/facebook/payments/form/model/FormFieldAttributes;->e:LX/6xO;

    invoke-virtual {v4}, LX/6xO;->getInputType()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputType(I)V

    .line 1158270
    iget-object v1, p0, LX/6wz;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v0, v0, Lcom/facebook/payments/form/model/FormFieldAttributes;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1158271
    iget-object v0, p0, LX/6wz;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v4, p0, LX/6wz;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00d5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v1, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1158272
    iget-object v0, p0, LX/6wz;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, LX/6wz;->c:LX/6x7;

    invoke-virtual {v1}, LX/6x7;->b()I

    move-result v1

    iget-object v4, p0, LX/6wz;->c:LX/6x7;

    invoke-virtual {v4}, LX/6x7;->b()I

    move-result v4

    iget-object v5, p0, LX/6wz;->c:LX/6x7;

    invoke-virtual {v5}, LX/6x7;->b()I

    move-result v5

    iget-object v6, p0, LX/6wz;->c:LX/6x7;

    invoke-virtual {v6}, LX/6x7;->b()I

    move-result v6

    invoke-virtual {v0, v1, v4, v5, v6}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setPadding(IIII)V

    .line 1158273
    new-array v0, v2, [Landroid/view/View;

    iget-object v1, p0, LX/6wz;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    aput-object v1, v0, v3

    invoke-virtual {p1, v0}, LX/6xD;->a([Landroid/view/View;)V

    .line 1158274
    new-array v0, v2, [Landroid/view/View;

    new-instance v1, LX/73W;

    iget-object v2, p0, LX/6wz;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/73W;-><init>(Landroid/content/Context;)V

    aput-object v1, v0, v3

    invoke-virtual {p1, v0}, LX/6xD;->a([Landroid/view/View;)V

    .line 1158275
    return-void

    .line 1158276
    :cond_0
    sget-object v0, LX/5fu;->NO_CURRENCY_SYMBOL_NOR_EMPTY_DECIMALS:LX/5fu;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1158277
    iget-object v1, p0, LX/6wz;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    .line 1158278
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1158279
    :cond_0
    :goto_0
    return v0

    .line 1158280
    :cond_1
    :try_start_0
    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 1158281
    new-instance v1, Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v3, p0, LX/6wz;->i:Ljava/lang/String;

    invoke-direct {v1, v3, v2}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 1158282
    iget-object v2, p0, LX/6wz;->h:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/6wz;->h:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1, v2}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Lcom/facebook/payments/currency/CurrencyAmount;)I

    move-result v2

    if-ltz v2, :cond_0

    .line 1158283
    :cond_2
    iget-object v2, p0, LX/6wz;->g:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/6wz;->g:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1, v2}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Lcom/facebook/payments/currency/CurrencyAmount;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-gtz v1, :cond_0

    .line 1158284
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 1158285
    :catch_0
    goto :goto_0
.end method
