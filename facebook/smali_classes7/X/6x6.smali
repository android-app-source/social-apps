.class public LX/6x6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/6wz;

.field private b:LX/6x2;

.field private c:LX/6x4;

.field private d:LX/6xF;


# direct methods
.method public constructor <init>(LX/6wz;LX/6x2;LX/6x4;LX/6xF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1158449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158450
    iput-object p1, p0, LX/6x6;->a:LX/6wz;

    .line 1158451
    iput-object p2, p0, LX/6x6;->b:LX/6x2;

    .line 1158452
    iput-object p3, p0, LX/6x6;->c:LX/6x4;

    .line 1158453
    iput-object p4, p0, LX/6x6;->d:LX/6xF;

    .line 1158454
    return-void
.end method


# virtual methods
.method public final a(LX/6xK;)LX/6wy;
    .locals 3

    .prologue
    .line 1158455
    sget-object v0, LX/6x5;->a:[I

    invoke-virtual {p1}, LX/6xK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1158456
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1158457
    :pswitch_0
    iget-object v0, p0, LX/6x6;->a:LX/6wz;

    .line 1158458
    :goto_0
    return-object v0

    .line 1158459
    :pswitch_1
    iget-object v0, p0, LX/6x6;->b:LX/6x2;

    goto :goto_0

    .line 1158460
    :pswitch_2
    iget-object v0, p0, LX/6x6;->c:LX/6x4;

    goto :goto_0

    .line 1158461
    :pswitch_3
    iget-object v0, p0, LX/6x6;->d:LX/6xF;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
