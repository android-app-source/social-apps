.class public final LX/7H1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1sq;


# instance fields
.field public readLength_:I

.field public strictRead_:Z

.field public strictWrite_:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1190245
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LX/7H1;-><init>(ZZ)V

    .line 1190246
    return-void
.end method

.method private constructor <init>(ZZ)V
    .locals 1

    .prologue
    .line 1190230
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7H1;-><init>(ZZI)V

    .line 1190231
    return-void
.end method

.method private constructor <init>(ZZI)V
    .locals 1

    .prologue
    .line 1190238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1190239
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7H1;->strictRead_:Z

    .line 1190240
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7H1;->strictWrite_:Z

    .line 1190241
    iput-boolean p1, p0, LX/7H1;->strictRead_:Z

    .line 1190242
    iput-boolean p2, p0, LX/7H1;->strictWrite_:Z

    .line 1190243
    iput p3, p0, LX/7H1;->readLength_:I

    .line 1190244
    return-void
.end method


# virtual methods
.method public final a(LX/1ss;)LX/1su;
    .locals 3

    .prologue
    .line 1190232
    new-instance v0, LX/7H2;

    iget-boolean v1, p0, LX/7H1;->strictRead_:Z

    iget-boolean v2, p0, LX/7H1;->strictWrite_:Z

    invoke-direct {v0, p1, v1, v2}, LX/7H2;-><init>(LX/1ss;ZZ)V

    .line 1190233
    iget v1, p0, LX/7H1;->readLength_:I

    if-eqz v1, :cond_0

    .line 1190234
    iget v1, p0, LX/7H1;->readLength_:I

    .line 1190235
    iput v1, v0, LX/7H2;->c:I

    .line 1190236
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/7H2;->d:Z

    .line 1190237
    :cond_0
    return-object v0
.end method
