.class public final LX/7Xi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2r3;


# direct methods
.method public constructor <init>(LX/2r3;)V
    .locals 0

    .prologue
    .line 1218616
    iput-object p1, p0, LX/7Xi;->a:LX/2r3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1218617
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1218618
    check-cast p1, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;

    .line 1218619
    invoke-virtual {p1}, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1218620
    iget-object v0, p0, LX/7Xi;->a:LX/2r3;

    iget-object v0, v0, LX/2r3;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.zero.ACTION_ZERO_REFRESH_TOKEN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "zero_token_request_reason"

    sget-object v3, LX/32P;->INCENTIVE_PROVISIONED_FORCE_FETCH:LX/32P;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1218621
    :cond_0
    return-void
.end method
