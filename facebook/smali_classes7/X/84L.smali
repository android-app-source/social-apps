.class public final LX/84L;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic d:LX/2h7;

.field public final synthetic e:LX/2hX;


# direct methods
.method public constructor <init>(LX/2hX;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/2h7;)V
    .locals 0

    .prologue
    .line 1290900
    iput-object p1, p0, LX/84L;->e:LX/2hX;

    iput-wide p2, p0, LX/84L;->a:J

    iput-object p4, p0, LX/84L;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object p5, p0, LX/84L;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object p6, p0, LX/84L;->d:LX/2h7;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1290888
    iget-object v0, p0, LX/84L;->e:LX/2hX;

    iget-wide v2, p0, LX/84L;->a:J

    iget-object v1, p0, LX/84L;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, LX/2hX;->b(LX/2hX;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 1290889
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-object v1, p0, LX/84L;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1290890
    iget-object v0, p0, LX/84L;->e:LX/2hX;

    invoke-virtual {v0, p1}, LX/2hY;->a(Ljava/lang/Throwable;)V

    .line 1290891
    :cond_0
    :goto_0
    return-void

    .line 1290892
    :cond_1
    iget-object v0, p0, LX/84L;->e:LX/2hX;

    .line 1290893
    iget-boolean v1, v0, LX/2hY;->d:Z

    move v0, v1

    .line 1290894
    if-nez v0, :cond_0

    .line 1290895
    iget-object v0, p0, LX/84L;->e:LX/2hX;

    iget-object v0, v0, LX/2hY;->b:LX/2hZ;

    iget-object v1, p0, LX/84L;->e:LX/2hX;

    iget-wide v2, p0, LX/84L;->a:J

    iget-object v4, p0, LX/84L;->d:LX/2h7;

    .line 1290896
    new-instance p0, LX/84N;

    invoke-direct {p0, v1, v2, v3, v4}, LX/84N;-><init>(LX/2hX;JLX/2h7;)V

    move-object v1, p0

    .line 1290897
    invoke-virtual {v0, p1, v1}, LX/2hZ;->a(Ljava/lang/Throwable;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1290898
    iget-object v0, p0, LX/84L;->e:LX/2hX;

    iget-wide v2, p0, LX/84L;->a:J

    iget-object v1, p0, LX/84L;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, LX/2hX;->b(LX/2hX;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 1290899
    return-void
.end method
