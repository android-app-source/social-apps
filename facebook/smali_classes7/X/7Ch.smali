.class public LX/7Ch;
.super LX/7Cg;
.source ""


# static fields
.field private static final c:LX/7Cp;


# instance fields
.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private t:F

.field private u:F

.field private v:F

.field private w:F

.field private x:F


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1181439
    new-instance v0, LX/7Cp;

    const/4 v1, 0x4

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, LX/7Cp;-><init>([F)V

    sput-object v0, LX/7Ch;->c:LX/7Cp;

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/view/WindowManager;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1181440
    invoke-direct {p0, p1}, LX/7Cg;-><init>(Landroid/view/WindowManager;)V

    .line 1181441
    iput v0, p0, LX/7Ch;->u:F

    .line 1181442
    iput v0, p0, LX/7Ch;->v:F

    .line 1181443
    iput v0, p0, LX/7Ch;->w:F

    .line 1181444
    iput v0, p0, LX/7Ch;->x:F

    .line 1181445
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1181446
    sub-float v0, v2, p1

    iget v1, p0, LX/7Ch;->d:F

    mul-float/2addr v0, v1

    iget v1, p0, LX/7Ch;->e:F

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    iput v0, p0, LX/7Ch;->w:F

    .line 1181447
    sub-float v0, v2, p1

    iget v1, p0, LX/7Ch;->f:F

    mul-float/2addr v0, v1

    iget v1, p0, LX/7Ch;->g:F

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    iput v0, p0, LX/7Ch;->v:F

    .line 1181448
    return-void
.end method

.method public final a([FLX/3IO;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 1181449
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->a:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181450
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->b:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181451
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->j:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181452
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->i:[F

    invoke-virtual {v0, v1}, LX/7Cp;->b([F)V

    .line 1181453
    iget-object v0, p0, LX/7Ce;->i:[F

    aget v0, v0, v6

    iput v0, p0, LX/7Ch;->t:F

    .line 1181454
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    invoke-virtual {v0}, LX/7Cp;->a()V

    .line 1181455
    iget-object v0, p0, LX/7Ce;->l:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->i:[F

    aget v1, v1, v7

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1, v4, v4, v5}, LX/7Cp;->a(FFFF)V

    .line 1181456
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->l:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->c(LX/7Cp;)V

    .line 1181457
    iget-object v0, p0, LX/7Ce;->l:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->i:[F

    aget v1, v1, v6

    iget v2, p0, LX/7Ch;->w:F

    add-float/2addr v1, v2

    iget v2, p0, LX/7Ch;->x:F

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1, v5, v4, v4}, LX/7Cp;->a(FFFF)V

    .line 1181458
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->l:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->c(LX/7Cp;)V

    .line 1181459
    iget-object v0, p0, LX/7Ce;->l:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->i:[F

    aget v1, v1, v8

    iget v2, p0, LX/7Ch;->v:F

    add-float/2addr v1, v2

    iget v2, p0, LX/7Ch;->u:F

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1, v4, v5, v4}, LX/7Cp;->a(FFFF)V

    .line 1181460
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->l:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->c(LX/7Cp;)V

    .line 1181461
    iget-object v0, p2, LX/3IO;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1181462
    iget-object v0, p0, LX/7Ce;->i:[F

    aget v0, v0, v8

    iget v1, p0, LX/7Ch;->v:F

    add-float/2addr v0, v1

    iget v1, p0, LX/7Ch;->u:F

    add-float/2addr v0, v1

    iget v1, p0, LX/7Ce;->p:F

    sub-float/2addr v0, v1

    iput v0, p2, LX/3IO;->e:F

    .line 1181463
    iget-object v0, p0, LX/7Ce;->i:[F

    aget v0, v0, v6

    iget v1, p0, LX/7Ch;->w:F

    add-float/2addr v0, v1

    iget v1, p0, LX/7Ch;->x:F

    add-float/2addr v0, v1

    neg-float v0, v0

    iput v0, p2, LX/3IO;->d:F

    .line 1181464
    iget-object v0, p0, LX/7Ce;->i:[F

    aget v0, v0, v7

    iput v0, p2, LX/3IO;->f:F

    .line 1181465
    iget-object v0, p2, LX/3IO;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1181466
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    invoke-virtual {v0, p1}, LX/7Cp;->c([F)V

    .line 1181467
    return-void
.end method

.method public final b(FF)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1181468
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iput v2, v0, LX/7Cp;->a:F

    .line 1181469
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iput p1, v0, LX/7Cp;->b:F

    .line 1181470
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    neg-float v1, p2

    iput v1, v0, LX/7Cp;->c:F

    .line 1181471
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iput v2, v0, LX/7Cp;->d:F

    .line 1181472
    iget-object v0, p0, LX/7Ce;->l:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->b:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181473
    iget-object v0, p0, LX/7Ce;->l:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->j:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181474
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->l:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->c(LX/7Cp;)V

    .line 1181475
    iget-object v0, p0, LX/7Ce;->l:LX/7Cp;

    invoke-virtual {v0}, LX/7Cp;->b()V

    .line 1181476
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->l:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181477
    iget v0, p0, LX/7Ch;->x:F

    iget-object v1, p0, LX/7Ce;->k:LX/7Cp;

    iget v1, v1, LX/7Cp;->d:F

    add-float/2addr v0, v1

    iput v0, p0, LX/7Ch;->x:F

    .line 1181478
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    sget-object v1, LX/7Ch;->c:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181479
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->l:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->c(LX/7Cp;)V

    .line 1181480
    iget-object v0, p0, LX/7Ce;->l:LX/7Cp;

    invoke-virtual {v0}, LX/7Cp;->b()V

    .line 1181481
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->l:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181482
    iget v0, p0, LX/7Ch;->u:F

    iget-object v1, p0, LX/7Ce;->k:LX/7Cp;

    iget v1, v1, LX/7Cp;->c:F

    mul-float/2addr v1, p1

    iget-object v2, p0, LX/7Ce;->k:LX/7Cp;

    iget v2, v2, LX/7Cp;->b:F

    mul-float/2addr v2, p2

    add-float/2addr v1, v2

    float-to-double v2, v1

    iget v1, p0, LX/7Ch;->t:F

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-float v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, LX/7Ch;->u:F

    .line 1181483
    return-void
.end method

.method public final c(FF)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    .line 1181484
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->a:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181485
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Cg;->b:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181486
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->j:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181487
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->i:[F

    invoke-virtual {v0, v1}, LX/7Cp;->b([F)V

    .line 1181488
    iget v0, p0, LX/7Ch;->w:F

    iget v1, p0, LX/7Ch;->x:F

    add-float/2addr v0, v1

    iput v0, p0, LX/7Ch;->d:F

    .line 1181489
    iget v0, p0, LX/7Ch;->v:F

    iget v1, p0, LX/7Ch;->u:F

    add-float/2addr v0, v1

    rem-float/2addr v0, v3

    iput v0, p0, LX/7Ch;->f:F

    .line 1181490
    iget v0, p0, LX/7Ce;->p:F

    add-float/2addr v0, p2

    rem-float/2addr v0, v3

    .line 1181491
    iget-object v1, p0, LX/7Ce;->i:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    sub-float v1, p1, v1

    iput v1, p0, LX/7Ch;->e:F

    .line 1181492
    iget-object v1, p0, LX/7Ce;->i:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, LX/7Ch;->g:F

    .line 1181493
    iget v0, p0, LX/7Ch;->g:F

    iget v1, p0, LX/7Ch;->f:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1181494
    iget v0, p0, LX/7Ch;->g:F

    sub-float/2addr v0, v3

    iput v0, p0, LX/7Ch;->g:F

    .line 1181495
    :cond_0
    :goto_0
    iput v4, p0, LX/7Ch;->u:F

    .line 1181496
    iput v4, p0, LX/7Ch;->x:F

    .line 1181497
    return-void

    .line 1181498
    :cond_1
    iget v0, p0, LX/7Ch;->g:F

    iget v1, p0, LX/7Ch;->f:F

    sub-float/2addr v0, v1

    const/high16 v1, -0x3ccc0000    # -180.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1181499
    iget v0, p0, LX/7Ch;->g:F

    add-float/2addr v0, v3

    iput v0, p0, LX/7Ch;->g:F

    goto :goto_0
.end method

.method public final d(FF)V
    .locals 0

    .prologue
    .line 1181500
    invoke-virtual {p0, p1, p2}, LX/7Ch;->f(FF)V

    .line 1181501
    return-void
.end method

.method public final f(FF)V
    .locals 1

    .prologue
    .line 1181502
    iget v0, p0, LX/7Ch;->w:F

    add-float/2addr v0, p1

    iput v0, p0, LX/7Ch;->w:F

    .line 1181503
    iget v0, p0, LX/7Ch;->v:F

    add-float/2addr v0, p2

    iput v0, p0, LX/7Ch;->v:F

    .line 1181504
    return-void
.end method
