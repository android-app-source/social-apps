.class public abstract LX/6sV;
.super LX/6sU;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PARAM::",
        "Landroid/os/Parcelable;",
        "RESU",
        "LT::Landroid/os/Parcelable;",
        ">",
        "LX/6sU",
        "<TPARAM;TRESU",
        "LT;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/common/PaymentNetworkOperationHelper;",
            "Ljava/lang/Class",
            "<TRESU",
            "LT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1153441
    invoke-direct {p0, p1, p2}, LX/6sU;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 1153442
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcelable;LX/1pN;)Landroid/os/Parcelable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAM;",
            "LX/1pN;",
            ")TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 1153443
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    iget-object v1, p0, LX/6sU;->b:Ljava/lang/Class;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1153444
    check-cast p1, Landroid/os/Parcelable;

    invoke-virtual {p0, p1, p2}, LX/6sV;->a(Landroid/os/Parcelable;LX/1pN;)Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method
