.class public LX/6kV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;


# instance fields
.field public final answered:Ljava/lang/Boolean;

.field public final duration:Ljava/lang/Long;

.field public final messageMetadata:LX/6kn;

.field public final startTime:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1137639
    new-instance v0, LX/1sv;

    const-string v1, "DeltaVideoCall"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kV;->b:LX/1sv;

    .line 1137640
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kV;->c:LX/1sw;

    .line 1137641
    new-instance v0, LX/1sw;

    const-string v1, "answered"

    invoke-direct {v0, v1, v4, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kV;->d:LX/1sw;

    .line 1137642
    new-instance v0, LX/1sw;

    const-string v1, "startTime"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kV;->e:LX/1sw;

    .line 1137643
    new-instance v0, LX/1sw;

    const-string v1, "duration"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kV;->f:LX/1sw;

    .line 1137644
    sput-boolean v3, LX/6kV;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6kn;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1137645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1137646
    iput-object p1, p0, LX/6kV;->messageMetadata:LX/6kn;

    .line 1137647
    iput-object p2, p0, LX/6kV;->answered:Ljava/lang/Boolean;

    .line 1137648
    iput-object p3, p0, LX/6kV;->startTime:Ljava/lang/Long;

    .line 1137649
    iput-object p4, p0, LX/6kV;->duration:Ljava/lang/Long;

    .line 1137650
    return-void
.end method

.method public static a(LX/6kV;)V
    .locals 4

    .prologue
    .line 1137651
    iget-object v0, p0, LX/6kV;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1137652
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kV;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1137653
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1137592
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1137593
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v1, v0

    .line 1137594
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1137595
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaVideoCall"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1137596
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137597
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137598
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137599
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137600
    const-string v4, "messageMetadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137601
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137602
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137603
    iget-object v4, p0, LX/6kV;->messageMetadata:LX/6kn;

    if-nez v4, :cond_6

    .line 1137604
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137605
    :goto_3
    iget-object v4, p0, LX/6kV;->answered:Ljava/lang/Boolean;

    if-eqz v4, :cond_0

    .line 1137606
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137607
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137608
    const-string v4, "answered"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137609
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137610
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137611
    iget-object v4, p0, LX/6kV;->answered:Ljava/lang/Boolean;

    if-nez v4, :cond_7

    .line 1137612
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137613
    :cond_0
    :goto_4
    iget-object v4, p0, LX/6kV;->startTime:Ljava/lang/Long;

    if-eqz v4, :cond_1

    .line 1137614
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137615
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137616
    const-string v4, "startTime"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137617
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137618
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137619
    iget-object v4, p0, LX/6kV;->startTime:Ljava/lang/Long;

    if-nez v4, :cond_8

    .line 1137620
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137621
    :cond_1
    :goto_5
    iget-object v4, p0, LX/6kV;->duration:Ljava/lang/Long;

    if-eqz v4, :cond_2

    .line 1137622
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137623
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137624
    const-string v4, "duration"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137625
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137626
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137627
    iget-object v0, p0, LX/6kV;->duration:Ljava/lang/Long;

    if-nez v0, :cond_9

    .line 1137628
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137629
    :cond_2
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137630
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137631
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1137632
    :cond_3
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1137633
    :cond_4
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1137634
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1137635
    :cond_6
    iget-object v4, p0, LX/6kV;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1137636
    :cond_7
    iget-object v4, p0, LX/6kV;->answered:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1137637
    :cond_8
    iget-object v4, p0, LX/6kV;->startTime:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1137638
    :cond_9
    iget-object v0, p0, LX/6kV;->duration:Ljava/lang/Long;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1137532
    invoke-static {p0}, LX/6kV;->a(LX/6kV;)V

    .line 1137533
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1137534
    iget-object v0, p0, LX/6kV;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1137535
    sget-object v0, LX/6kV;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1137536
    iget-object v0, p0, LX/6kV;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1137537
    :cond_0
    iget-object v0, p0, LX/6kV;->answered:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1137538
    iget-object v0, p0, LX/6kV;->answered:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1137539
    sget-object v0, LX/6kV;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1137540
    iget-object v0, p0, LX/6kV;->answered:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1137541
    :cond_1
    iget-object v0, p0, LX/6kV;->startTime:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1137542
    iget-object v0, p0, LX/6kV;->startTime:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1137543
    sget-object v0, LX/6kV;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1137544
    iget-object v0, p0, LX/6kV;->startTime:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1137545
    :cond_2
    iget-object v0, p0, LX/6kV;->duration:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1137546
    iget-object v0, p0, LX/6kV;->duration:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1137547
    sget-object v0, LX/6kV;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1137548
    iget-object v0, p0, LX/6kV;->duration:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1137549
    :cond_3
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1137550
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1137551
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1137556
    if-nez p1, :cond_1

    .line 1137557
    :cond_0
    :goto_0
    return v0

    .line 1137558
    :cond_1
    instance-of v1, p1, LX/6kV;

    if-eqz v1, :cond_0

    .line 1137559
    check-cast p1, LX/6kV;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1137560
    if-nez p1, :cond_3

    .line 1137561
    :cond_2
    :goto_1
    move v0, v2

    .line 1137562
    goto :goto_0

    .line 1137563
    :cond_3
    iget-object v0, p0, LX/6kV;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1137564
    :goto_2
    iget-object v3, p1, LX/6kV;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1137565
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1137566
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1137567
    iget-object v0, p0, LX/6kV;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6kV;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1137568
    :cond_5
    iget-object v0, p0, LX/6kV;->answered:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1137569
    :goto_4
    iget-object v3, p1, LX/6kV;->answered:Ljava/lang/Boolean;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1137570
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1137571
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1137572
    iget-object v0, p0, LX/6kV;->answered:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6kV;->answered:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1137573
    :cond_7
    iget-object v0, p0, LX/6kV;->startTime:Ljava/lang/Long;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1137574
    :goto_6
    iget-object v3, p1, LX/6kV;->startTime:Ljava/lang/Long;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1137575
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1137576
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1137577
    iget-object v0, p0, LX/6kV;->startTime:Ljava/lang/Long;

    iget-object v3, p1, LX/6kV;->startTime:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1137578
    :cond_9
    iget-object v0, p0, LX/6kV;->duration:Ljava/lang/Long;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1137579
    :goto_8
    iget-object v3, p1, LX/6kV;->duration:Ljava/lang/Long;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1137580
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1137581
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1137582
    iget-object v0, p0, LX/6kV;->duration:Ljava/lang/Long;

    iget-object v3, p1, LX/6kV;->duration:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_b
    move v2, v1

    .line 1137583
    goto :goto_1

    :cond_c
    move v0, v2

    .line 1137584
    goto :goto_2

    :cond_d
    move v3, v2

    .line 1137585
    goto :goto_3

    :cond_e
    move v0, v2

    .line 1137586
    goto :goto_4

    :cond_f
    move v3, v2

    .line 1137587
    goto :goto_5

    :cond_10
    move v0, v2

    .line 1137588
    goto :goto_6

    :cond_11
    move v3, v2

    .line 1137589
    goto :goto_7

    :cond_12
    move v0, v2

    .line 1137590
    goto :goto_8

    :cond_13
    move v3, v2

    .line 1137591
    goto :goto_9
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1137555
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1137552
    sget-boolean v0, LX/6kV;->a:Z

    .line 1137553
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kV;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1137554
    return-object v0
.end method
