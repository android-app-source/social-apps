.class public final enum LX/71l;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/71l;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/71l;

.field public static final enum ADD_CUSTOM_OPTION_SELECTOR_ROW:LX/71l;

.field public static final enum CHECKBOX_OPTION_SELECTOR:LX/71l;

.field public static final enum DIVIDER_ROW:LX/71l;

.field public static final enum FOOTER_VIEW:LX/71l;


# instance fields
.field private final mSelectable:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1163679
    new-instance v0, LX/71l;

    const-string v1, "ADD_CUSTOM_OPTION_SELECTOR_ROW"

    invoke-direct {v0, v1, v2, v2}, LX/71l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/71l;->ADD_CUSTOM_OPTION_SELECTOR_ROW:LX/71l;

    .line 1163680
    new-instance v0, LX/71l;

    const-string v1, "CHECKBOX_OPTION_SELECTOR"

    invoke-direct {v0, v1, v3, v3}, LX/71l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/71l;->CHECKBOX_OPTION_SELECTOR:LX/71l;

    .line 1163681
    new-instance v0, LX/71l;

    const-string v1, "DIVIDER_ROW"

    invoke-direct {v0, v1, v4, v2}, LX/71l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/71l;->DIVIDER_ROW:LX/71l;

    .line 1163682
    new-instance v0, LX/71l;

    const-string v1, "FOOTER_VIEW"

    invoke-direct {v0, v1, v5, v2}, LX/71l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/71l;->FOOTER_VIEW:LX/71l;

    .line 1163683
    const/4 v0, 0x4

    new-array v0, v0, [LX/71l;

    sget-object v1, LX/71l;->ADD_CUSTOM_OPTION_SELECTOR_ROW:LX/71l;

    aput-object v1, v0, v2

    sget-object v1, LX/71l;->CHECKBOX_OPTION_SELECTOR:LX/71l;

    aput-object v1, v0, v3

    sget-object v1, LX/71l;->DIVIDER_ROW:LX/71l;

    aput-object v1, v0, v4

    sget-object v1, LX/71l;->FOOTER_VIEW:LX/71l;

    aput-object v1, v0, v5

    sput-object v0, LX/71l;->$VALUES:[LX/71l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 1163676
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1163677
    iput-boolean p3, p0, LX/71l;->mSelectable:Z

    .line 1163678
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/71l;
    .locals 1

    .prologue
    .line 1163673
    const-class v0, LX/71l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/71l;

    return-object v0
.end method

.method public static values()[LX/71l;
    .locals 1

    .prologue
    .line 1163675
    sget-object v0, LX/71l;->$VALUES:[LX/71l;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/71l;

    return-object v0
.end method


# virtual methods
.method public final isSelectable()Z
    .locals 1

    .prologue
    .line 1163674
    iget-boolean v0, p0, LX/71l;->mSelectable:Z

    return v0
.end method
