.class public final LX/7t5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 35

    .prologue
    .line 1266829
    const-wide/16 v26, 0x0

    .line 1266830
    const/16 v25, 0x0

    .line 1266831
    const/16 v24, 0x0

    .line 1266832
    const/16 v23, 0x0

    .line 1266833
    const/16 v22, 0x0

    .line 1266834
    const/16 v21, 0x0

    .line 1266835
    const/16 v20, 0x0

    .line 1266836
    const/16 v19, 0x0

    .line 1266837
    const/16 v18, 0x0

    .line 1266838
    const/4 v15, 0x0

    .line 1266839
    const-wide/16 v16, 0x0

    .line 1266840
    const/4 v14, 0x0

    .line 1266841
    const/4 v13, 0x0

    .line 1266842
    const/4 v12, 0x0

    .line 1266843
    const/4 v11, 0x0

    .line 1266844
    const/4 v10, 0x0

    .line 1266845
    const/4 v9, 0x0

    .line 1266846
    const/4 v8, 0x0

    .line 1266847
    const/4 v7, 0x0

    .line 1266848
    const/4 v6, 0x0

    .line 1266849
    const/4 v5, 0x0

    .line 1266850
    const/4 v4, 0x0

    .line 1266851
    const/4 v3, 0x0

    .line 1266852
    const/4 v2, 0x0

    .line 1266853
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_1a

    .line 1266854
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1266855
    const/4 v2, 0x0

    .line 1266856
    :goto_0
    return v2

    .line 1266857
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_13

    .line 1266858
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1266859
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1266860
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1266861
    const-string v6, "end_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1266862
    const/4 v2, 0x1

    .line 1266863
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1266864
    :cond_1
    const-string v6, "eventProfilePicture"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1266865
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto :goto_1

    .line 1266866
    :cond_2
    const-string v6, "event_buy_ticket_display_url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1266867
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v29, v2

    goto :goto_1

    .line 1266868
    :cond_3
    const-string v6, "event_place"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1266869
    invoke-static/range {p0 .. p1}, LX/7rw;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto :goto_1

    .line 1266870
    :cond_4
    const-string v6, "event_ticket_provider"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1266871
    invoke-static/range {p0 .. p1}, LX/7sf;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto :goto_1

    .line 1266872
    :cond_5
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1266873
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 1266874
    :cond_6
    const-string v6, "is_all_day"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1266875
    const/4 v2, 0x1

    .line 1266876
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v12, v2

    move/from16 v25, v6

    goto/16 :goto_1

    .line 1266877
    :cond_7
    const-string v6, "is_official"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1266878
    const/4 v2, 0x1

    .line 1266879
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v11, v2

    move/from16 v24, v6

    goto/16 :goto_1

    .line 1266880
    :cond_8
    const-string v6, "name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1266881
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 1266882
    :cond_9
    const-string v6, "registration_settings"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1266883
    invoke-static/range {p0 .. p1}, LX/7t4;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 1266884
    :cond_a
    const-string v6, "start_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1266885
    const/4 v2, 0x1

    .line 1266886
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v10, v2

    move-wide/from16 v20, v6

    goto/16 :goto_1

    .line 1266887
    :cond_b
    const-string v6, "supports_multi_tier_purchase"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1266888
    const/4 v2, 0x1

    .line 1266889
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move/from16 v19, v6

    goto/16 :goto_1

    .line 1266890
    :cond_c
    const-string v6, "ticket_seat_selection_note"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1266891
    invoke-static/range {p0 .. p1}, LX/7sg;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 1266892
    :cond_d
    const-string v6, "ticket_settings"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 1266893
    invoke-static/range {p0 .. p1}, LX/7sj;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 1266894
    :cond_e
    const-string v6, "ticket_tiers"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 1266895
    invoke-static/range {p0 .. p1}, LX/7sn;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 1266896
    :cond_f
    const-string v6, "timezone"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 1266897
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 1266898
    :cond_10
    const-string v6, "total_purchased_tickets"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 1266899
    const/4 v2, 0x1

    .line 1266900
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v14, v6

    goto/16 :goto_1

    .line 1266901
    :cond_11
    const-string v6, "viewer_watch_status"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 1266902
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 1266903
    :cond_12
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1266904
    :cond_13
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1266905
    if-eqz v3, :cond_14

    .line 1266906
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1266907
    :cond_14
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1266908
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1266909
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1266910
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1266911
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1266912
    if-eqz v12, :cond_15

    .line 1266913
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1266914
    :cond_15
    if-eqz v11, :cond_16

    .line 1266915
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1266916
    :cond_16
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1266917
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1266918
    if-eqz v10, :cond_17

    .line 1266919
    const/16 v3, 0xa

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1266920
    :cond_17
    if-eqz v9, :cond_18

    .line 1266921
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1266922
    :cond_18
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1266923
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1266924
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1266925
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1266926
    if-eqz v8, :cond_19

    .line 1266927
    const/16 v2, 0x10

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14, v3}, LX/186;->a(III)V

    .line 1266928
    :cond_19
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1266929
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_1a
    move/from16 v28, v23

    move/from16 v29, v24

    move/from16 v30, v25

    move/from16 v23, v18

    move/from16 v24, v19

    move/from16 v25, v20

    move/from16 v18, v13

    move/from16 v19, v14

    move v13, v8

    move v14, v9

    move v8, v2

    move v9, v3

    move v3, v7

    move-wide/from16 v31, v16

    move/from16 v16, v11

    move/from16 v17, v12

    move v11, v5

    move v12, v6

    move/from16 v33, v21

    move-wide/from16 v20, v31

    move/from16 v34, v22

    move/from16 v22, v15

    move v15, v10

    move v10, v4

    move-wide/from16 v4, v26

    move/from16 v27, v34

    move/from16 v26, v33

    goto/16 :goto_1
.end method
