.class public final LX/7wA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1275488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1275489
    new-instance v0, Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;

    invoke-direct {v0, p1}, Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1275490
    new-array v0, p1, [Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;

    return-object v0
.end method
