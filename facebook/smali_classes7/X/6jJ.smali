.class public LX/6jJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6jJ;


# instance fields
.field public final a:LX/3MK;


# direct methods
.method public constructor <init>(LX/3MK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1130521
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130522
    iput-object p1, p0, LX/6jJ;->a:LX/3MK;

    .line 1130523
    return-void
.end method

.method public static a(LX/0QB;)LX/6jJ;
    .locals 4

    .prologue
    .line 1130508
    sget-object v0, LX/6jJ;->b:LX/6jJ;

    if-nez v0, :cond_1

    .line 1130509
    const-class v1, LX/6jJ;

    monitor-enter v1

    .line 1130510
    :try_start_0
    sget-object v0, LX/6jJ;->b:LX/6jJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1130511
    if-eqz v2, :cond_0

    .line 1130512
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1130513
    new-instance p0, LX/6jJ;

    invoke-static {v0}, LX/3MK;->a(LX/0QB;)LX/3MK;

    move-result-object v3

    check-cast v3, LX/3MK;

    invoke-direct {p0, v3}, LX/6jJ;-><init>(LX/3MK;)V

    .line 1130514
    move-object v0, p0

    .line 1130515
    sput-object v0, LX/6jJ;->b:LX/6jJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1130516
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1130517
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1130518
    :cond_1
    sget-object v0, LX/6jJ;->b:LX/6jJ;

    return-object v0

    .line 1130519
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1130520
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1130499
    sget-object v0, LX/3MN;->a:LX/0U1;

    .line 1130500
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1130501
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 1130502
    iget-object v1, p0, LX/6jJ;->a:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, -0x6da7f44

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1130503
    :try_start_0
    iget-object v1, p0, LX/6jJ;->a:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "block_table"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1130504
    iget-object v0, p0, LX/6jJ;->a:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1130505
    iget-object v0, p0, LX/6jJ;->a:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, 0x2472204f

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1130506
    return-void

    .line 1130507
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/6jJ;->a:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, 0x2d9b1a44

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 1130481
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1130482
    sget-object v1, LX/3MN;->b:LX/0U1;

    .line 1130483
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1130484
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1130485
    sget-object v1, LX/3MN;->a:LX/0U1;

    .line 1130486
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1130487
    invoke-static {v1, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 1130488
    iget-object v2, p0, LX/6jJ;->a:LX/3MK;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const v3, -0x79c15ac

    invoke-static {v2, v3}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1130489
    :try_start_0
    iget-object v2, p0, LX/6jJ;->a:LX/3MK;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "block_table"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v0, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1130490
    if-nez v1, :cond_0

    .line 1130491
    sget-object v1, LX/3MN;->a:LX/0U1;

    .line 1130492
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1130493
    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1130494
    iget-object v1, p0, LX/6jJ;->a:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "block_table"

    const/4 v3, 0x0

    const v4, -0x39b57251

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x148f531e

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1130495
    :cond_0
    iget-object v0, p0, LX/6jJ;->a:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1130496
    iget-object v0, p0, LX/6jJ;->a:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, -0x35dabb90    # -2707740.0f

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1130497
    return-void

    .line 1130498
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/6jJ;->a:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, 0x78204b8c

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
