.class public final LX/7rI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1262394
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1262395
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1262396
    :goto_0
    return v1

    .line 1262397
    :cond_0
    const-string v7, "viewer_friend_count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1262398
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 1262399
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_5

    .line 1262400
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1262401
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1262402
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1262403
    const-string v7, "edges"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1262404
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1262405
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_2

    .line 1262406
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_2

    .line 1262407
    invoke-static {p0, p1}, LX/7rH;->b(LX/15w;LX/186;)I

    move-result v6

    .line 1262408
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1262409
    :cond_2
    invoke-static {v5, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 1262410
    goto :goto_1

    .line 1262411
    :cond_3
    const-string v7, "page_info"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1262412
    invoke-static {p0, p1}, LX/4aB;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1262413
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1262414
    :cond_5
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1262415
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1262416
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1262417
    if-eqz v0, :cond_6

    .line 1262418
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 1262419
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1262420
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1262421
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1262422
    if-eqz v0, :cond_1

    .line 1262423
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1262424
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1262425
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1262426
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/7rH;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1262427
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1262428
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1262429
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1262430
    if-eqz v0, :cond_2

    .line 1262431
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1262432
    invoke-static {p0, v0, p2}, LX/4aB;->a(LX/15i;ILX/0nX;)V

    .line 1262433
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1262434
    if-eqz v0, :cond_3

    .line 1262435
    const-string v1, "viewer_friend_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1262436
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1262437
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1262438
    return-void
.end method
