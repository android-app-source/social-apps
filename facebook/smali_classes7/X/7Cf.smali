.class public LX/7Cf;
.super LX/7Ce;
.source ""


# static fields
.field private static final t:LX/7Cp;


# instance fields
.field private final A:LX/7Cp;

.field private final B:LX/7Cp;

.field public a:F

.field public b:F

.field public c:F

.field public d:F

.field public e:F

.field public f:F

.field public g:LX/7DG;

.field private u:F

.field private v:F

.field private w:F

.field private x:F

.field private final y:[F

.field private z:LX/7Cp;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1181322
    new-instance v0, LX/7Cp;

    const v1, 0x3fc90fdb

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v3, v3}, LX/7Cp;-><init>(FFFF)V

    sput-object v0, LX/7Cf;->t:LX/7Cp;

    return-void
.end method

.method public constructor <init>(Landroid/view/WindowManager;LX/7DG;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1181323
    invoke-direct {p0, p1}, LX/7Ce;-><init>(Landroid/view/WindowManager;)V

    .line 1181324
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, LX/7Cf;->y:[F

    .line 1181325
    new-instance v0, LX/7Cp;

    invoke-direct {v0}, LX/7Cp;-><init>()V

    iput-object v0, p0, LX/7Cf;->A:LX/7Cp;

    .line 1181326
    new-instance v0, LX/7Cp;

    invoke-direct {v0}, LX/7Cp;-><init>()V

    iput-object v0, p0, LX/7Cf;->B:LX/7Cp;

    .line 1181327
    iput v1, p0, LX/7Cf;->a:F

    .line 1181328
    iput v1, p0, LX/7Cf;->b:F

    .line 1181329
    iput-object p2, p0, LX/7Cf;->g:LX/7DG;

    .line 1181330
    iget-object v0, p0, LX/7Cf;->g:LX/7DG;

    .line 1181331
    iget v1, v0, LX/7DG;->b:F

    move v0, v1

    .line 1181332
    iput v0, p0, LX/7Cf;->c:F

    .line 1181333
    iget-object v0, p0, LX/7Cf;->g:LX/7DG;

    .line 1181334
    iget v1, v0, LX/7DG;->a:F

    move v0, v1

    .line 1181335
    iput v0, p0, LX/7Cf;->d:F

    .line 1181336
    iget-object v0, p0, LX/7Cf;->g:LX/7DG;

    .line 1181337
    iget v1, v0, LX/7DG;->c:F

    move v0, v1

    .line 1181338
    iput v0, p0, LX/7Cf;->e:F

    .line 1181339
    iget-object v0, p0, LX/7Cf;->g:LX/7DG;

    .line 1181340
    iget v1, v0, LX/7DG;->d:F

    move v0, v1

    .line 1181341
    iput v0, p0, LX/7Cf;->f:F

    .line 1181342
    iget-object v0, p0, LX/7Cf;->g:LX/7DG;

    .line 1181343
    iget-boolean v1, v0, LX/7DG;->f:Z

    move v0, v1

    .line 1181344
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1181345
    return-void
.end method

.method public static a(FFFF)F
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x41200000    # 10.0f

    .line 1181355
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1181356
    cmpg-float v1, p1, v3

    if-gez v1, :cond_1

    add-float v1, p2, v2

    cmpg-float v1, p0, v1

    if-gez v1, :cond_1

    cmpl-float v1, p0, p2

    if-lez v1, :cond_1

    .line 1181357
    sub-float v0, p0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float/2addr v0, v2

    .line 1181358
    :cond_0
    :goto_0
    return v0

    .line 1181359
    :cond_1
    cmpl-float v1, p1, v3

    if-lez v1, :cond_0

    sub-float v1, p3, v2

    cmpl-float v1, p0, v1

    if-lez v1, :cond_0

    cmpg-float v1, p0, p3

    if-gez v1, :cond_0

    .line 1181360
    sub-float v0, p0, p3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float/2addr v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(F)V
    .locals 4

    .prologue
    const/high16 v3, 0x43b40000    # 360.0f

    .line 1181346
    iget v0, p0, LX/7Cf;->v:F

    iget v1, p0, LX/7Cf;->u:F

    sub-float v1, v0, v1

    .line 1181347
    iget v0, p0, LX/7Cf;->x:F

    iget v2, p0, LX/7Cf;->w:F

    sub-float/2addr v0, v2

    .line 1181348
    const/high16 v2, 0x43340000    # 180.0f

    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    .line 1181349
    sub-float/2addr v0, v3

    .line 1181350
    :cond_0
    :goto_0
    iget v2, p0, LX/7Cf;->u:F

    mul-float/2addr v1, p1

    add-float/2addr v1, v2

    iput v1, p0, LX/7Cf;->a:F

    .line 1181351
    iget v1, p0, LX/7Cf;->w:F

    mul-float/2addr v0, p1

    add-float/2addr v0, v1

    iput v0, p0, LX/7Cf;->b:F

    .line 1181352
    return-void

    .line 1181353
    :cond_1
    const/high16 v2, -0x3ccc0000    # -180.0f

    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    .line 1181354
    add-float/2addr v0, v3

    goto :goto_0
.end method

.method public final a(FF)V
    .locals 0

    .prologue
    .line 1181303
    invoke-super {p0, p1, p2}, LX/7Ce;->a(FF)V

    .line 1181304
    iput p1, p0, LX/7Cf;->a:F

    .line 1181305
    iput p2, p0, LX/7Cf;->b:F

    .line 1181306
    iput p1, p0, LX/7Cf;->v:F

    .line 1181307
    iput p2, p0, LX/7Cf;->x:F

    .line 1181308
    return-void
.end method

.method public a(FFZ)V
    .locals 3

    .prologue
    .line 1181309
    iget v0, p0, LX/7Cf;->a:F

    iget v1, p0, LX/7Cf;->e:F

    iget v2, p0, LX/7Cf;->f:F

    invoke-static {v0, p1, v1, v2}, LX/7Cf;->a(FFFF)F

    move-result v0

    .line 1181310
    iget v1, p0, LX/7Cf;->a:F

    mul-float/2addr v0, p1

    add-float/2addr v0, v1

    iput v0, p0, LX/7Cf;->a:F

    .line 1181311
    iget v0, p0, LX/7Cf;->a:F

    iget v1, p0, LX/7Cf;->e:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LX/7Cf;->a:F

    .line 1181312
    iget v0, p0, LX/7Cf;->a:F

    iget v1, p0, LX/7Cf;->f:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LX/7Cf;->a:F

    .line 1181313
    iget-object v0, p0, LX/7Cf;->g:LX/7DG;

    .line 1181314
    iget-boolean v1, v0, LX/7DG;->e:Z

    move v0, v1

    .line 1181315
    if-eqz v0, :cond_0

    .line 1181316
    iget v0, p0, LX/7Cf;->b:F

    iget v1, p0, LX/7Cf;->d:F

    iget v2, p0, LX/7Cf;->c:F

    invoke-static {v0, p2, v1, v2}, LX/7Cf;->a(FFFF)F

    move-result v0

    .line 1181317
    iget v1, p0, LX/7Cf;->b:F

    mul-float/2addr v0, p2

    add-float/2addr v0, v1

    iput v0, p0, LX/7Cf;->b:F

    .line 1181318
    iget v0, p0, LX/7Cf;->b:F

    iget v1, p0, LX/7Cf;->d:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LX/7Cf;->b:F

    .line 1181319
    iget v0, p0, LX/7Cf;->b:F

    iget v1, p0, LX/7Cf;->c:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LX/7Cf;->b:F

    .line 1181320
    :goto_0
    return-void

    .line 1181321
    :cond_0
    iget v0, p0, LX/7Cf;->b:F

    add-float/2addr v0, p2

    iput v0, p0, LX/7Cf;->b:F

    goto :goto_0
.end method

.method public final a(Landroid/hardware/SensorEvent;)V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 1181276
    iget-object v0, p0, LX/7Ce;->i:[F

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-static {v0, v1}, Landroid/hardware/SensorManager;->getQuaternionFromVector([F[F)V

    .line 1181277
    iget-object v0, p0, LX/7Cf;->z:LX/7Cp;

    if-nez v0, :cond_0

    .line 1181278
    new-instance v0, LX/7Cp;

    invoke-direct {v0}, LX/7Cp;-><init>()V

    iput-object v0, p0, LX/7Cf;->z:LX/7Cp;

    .line 1181279
    iget-object v0, p0, LX/7Cf;->z:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->i:[F

    invoke-virtual {v0, v1}, LX/7Cp;->a([F)V

    .line 1181280
    :cond_0
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->i:[F

    invoke-virtual {v0, v1}, LX/7Cp;->a([F)V

    .line 1181281
    iget-object v0, p0, LX/7Ce;->l:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->k:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181282
    iget-object v0, p0, LX/7Ce;->l:LX/7Cp;

    sget-object v1, LX/7Cf;->t:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->c(LX/7Cp;)V

    .line 1181283
    iget-object v0, p0, LX/7Ce;->l:LX/7Cp;

    invoke-virtual {v0}, LX/7Cp;->b()V

    .line 1181284
    iget-object v0, p0, LX/7Ce;->l:LX/7Cp;

    iget-object v1, p0, LX/7Cf;->y:[F

    invoke-virtual {v0, v1}, LX/7Cp;->b([F)V

    .line 1181285
    iget-object v0, p0, LX/7Cf;->y:[F

    aget v0, v0, v5

    .line 1181286
    iget-object v1, p0, LX/7Cf;->A:LX/7Cp;

    iget-object v2, p0, LX/7Cf;->z:LX/7Cp;

    iget-object v3, p0, LX/7Ce;->k:LX/7Cp;

    iget v4, p0, LX/7Ce;->q:F

    invoke-virtual {v1, v2, v3, v4}, LX/7Cp;->a(LX/7Cp;LX/7Cp;F)V

    .line 1181287
    iget-object v1, p0, LX/7Cf;->z:LX/7Cp;

    iget-object v2, p0, LX/7Cf;->A:LX/7Cp;

    invoke-virtual {v1, v2}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181288
    invoke-virtual {p0}, LX/7Ce;->d()V

    .line 1181289
    iget-object v1, p0, LX/7Cf;->A:LX/7Cp;

    invoke-virtual {v1}, LX/7Cp;->b()V

    .line 1181290
    iget-object v1, p0, LX/7Cf;->A:LX/7Cp;

    iget-object v2, p0, LX/7Ce;->j:LX/7Cp;

    invoke-virtual {v1, v2}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181291
    iget-object v1, p0, LX/7Ce;->l:LX/7Cp;

    iget-object v2, p0, LX/7Cf;->A:LX/7Cp;

    invoke-virtual {v1, v2}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181292
    iget-object v1, p0, LX/7Ce;->l:LX/7Cp;

    invoke-virtual {v1}, LX/7Cp;->b()V

    .line 1181293
    iget-object v1, p0, LX/7Ce;->l:LX/7Cp;

    iget-object v2, p0, LX/7Cf;->B:LX/7Cp;

    invoke-virtual {v1, v2}, LX/7Cp;->b(LX/7Cp;)V

    .line 1181294
    iget-object v1, p0, LX/7Cf;->B:LX/7Cp;

    iget-object v2, p0, LX/7Cf;->A:LX/7Cp;

    invoke-virtual {v1, v2}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181295
    iget-object v1, p0, LX/7Ce;->l:LX/7Cp;

    iget-object v2, p0, LX/7Cf;->y:[F

    invoke-virtual {v1, v2}, LX/7Cp;->b([F)V

    .line 1181296
    iget-object v1, p0, LX/7Cf;->y:[F

    aget v1, v1, v5

    neg-float v1, v1

    .line 1181297
    iget-object v2, p0, LX/7Cf;->y:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    neg-float v2, v2

    float-to-double v2, v2

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    iget-object v0, p0, LX/7Cf;->y:[F

    aget v0, v0, v8

    float-to-double v6, v0

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v0, v2

    .line 1181298
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x41700000    # 15.0f

    iget v4, p0, LX/7Ce;->q:F

    mul-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 1181299
    if-eqz v2, :cond_1

    .line 1181300
    iget-object v0, p0, LX/7Cf;->z:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->k:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->a(LX/7Cp;)V

    .line 1181301
    :goto_1
    return-void

    .line 1181302
    :cond_1
    invoke-virtual {p0, v1, v0, v8}, LX/7Cf;->a(FFZ)V

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a([FLX/3IO;)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 1181266
    iget-object v0, p2, LX/3IO;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1181267
    iget v0, p0, LX/7Cf;->b:F

    iget v1, p0, LX/7Ce;->p:F

    sub-float/2addr v0, v1

    iput v0, p2, LX/3IO;->e:F

    .line 1181268
    iput v4, p2, LX/3IO;->f:F

    .line 1181269
    iget v0, p0, LX/7Cf;->a:F

    neg-float v0, v0

    iput v0, p2, LX/3IO;->d:F

    .line 1181270
    iget-object v0, p2, LX/3IO;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1181271
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget v1, p0, LX/7Cf;->a:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1, v5, v4, v4}, LX/7Cp;->a(FFFF)V

    .line 1181272
    iget-object v0, p0, LX/7Ce;->l:LX/7Cp;

    iget v1, p0, LX/7Cf;->b:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1, v4, v5, v4}, LX/7Cp;->a(FFFF)V

    .line 1181273
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    iget-object v1, p0, LX/7Ce;->l:LX/7Cp;

    invoke-virtual {v0, v1}, LX/7Cp;->c(LX/7Cp;)V

    .line 1181274
    iget-object v0, p0, LX/7Ce;->k:LX/7Cp;

    invoke-virtual {v0, p1}, LX/7Cp;->c([F)V

    .line 1181275
    return-void
.end method

.method public final a_(FF)V
    .locals 0

    .prologue
    .line 1181263
    iput p1, p0, LX/7Cf;->c:F

    .line 1181264
    iput p2, p0, LX/7Cf;->d:F

    .line 1181265
    return-void
.end method

.method public final b(FF)V
    .locals 3

    .prologue
    .line 1181261
    neg-float v0, p2

    neg-float v1, p1

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, LX/7Cf;->a(FFZ)V

    .line 1181262
    return-void
.end method

.method public final c(FF)V
    .locals 2

    .prologue
    .line 1181256
    iget v0, p0, LX/7Cf;->a:F

    iput v0, p0, LX/7Cf;->u:F

    .line 1181257
    iget v0, p0, LX/7Cf;->b:F

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/7Cq;->a(FZ)F

    move-result v0

    iput v0, p0, LX/7Cf;->w:F

    .line 1181258
    iput p1, p0, LX/7Cf;->v:F

    .line 1181259
    iget v0, p0, LX/7Ce;->p:F

    add-float/2addr v0, p2

    iput v0, p0, LX/7Cf;->x:F

    .line 1181260
    return-void
.end method

.method public final d(FF)V
    .locals 1

    .prologue
    .line 1181254
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/7Cf;->a(FFZ)V

    .line 1181255
    return-void
.end method
