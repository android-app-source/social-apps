.class public LX/8QM;
.super LX/8QL;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8QL",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public e:I

.field public f:I

.field public g:I

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(LX/8vA;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1342925
    invoke-direct {p0, p1}, LX/8QL;-><init>(LX/8vA;)V

    .line 1342926
    const/4 v0, 0x0

    iput-object v0, p0, LX/8QM;->j:Ljava/lang/Integer;

    .line 1342927
    iput v1, p0, LX/8QM;->e:I

    .line 1342928
    iput v1, p0, LX/8QM;->f:I

    .line 1342929
    iput v1, p0, LX/8QM;->g:I

    .line 1342930
    return-void
.end method

.method public constructor <init>(LX/8vA;IIILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1342917
    invoke-direct {p0, p1}, LX/8QL;-><init>(LX/8vA;)V

    .line 1342918
    const/4 v0, 0x0

    iput-object v0, p0, LX/8QM;->j:Ljava/lang/Integer;

    .line 1342919
    iput p2, p0, LX/8QM;->e:I

    .line 1342920
    iput p3, p0, LX/8QM;->f:I

    .line 1342921
    iput p4, p0, LX/8QM;->g:I

    .line 1342922
    iput-object p5, p0, LX/8QM;->h:Ljava/lang/String;

    .line 1342923
    iput-object p6, p0, LX/8QM;->i:Ljava/lang/String;

    .line 1342924
    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;IIILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1342907
    sget-object v0, LX/8vA;->PRIVACY:LX/8vA;

    invoke-direct {p0, v0}, LX/8QL;-><init>(LX/8vA;)V

    .line 1342908
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1342909
    iput-object p1, p0, LX/8QM;->j:Ljava/lang/Integer;

    .line 1342910
    iput p2, p0, LX/8QM;->e:I

    .line 1342911
    iput p3, p0, LX/8QM;->f:I

    .line 1342912
    iput p4, p0, LX/8QM;->g:I

    .line 1342913
    iput-object p5, p0, LX/8QM;->h:Ljava/lang/String;

    .line 1342914
    iput-object p6, p0, LX/8QM;->i:Ljava/lang/String;

    .line 1342915
    return-void

    .line 1342916
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1342906
    iget-object v0, p0, LX/8QM;->h:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic c()Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1342905
    invoke-virtual {p0}, LX/8QM;->n()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1342904
    iget v0, p0, LX/8QM;->e:I

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 1342931
    iget v0, p0, LX/8QM;->f:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1342898
    if-ne p1, p0, :cond_0

    .line 1342899
    const/4 v0, 0x1

    .line 1342900
    :goto_0
    return v0

    .line 1342901
    :cond_0
    instance-of v0, p1, LX/8QM;

    if-nez v0, :cond_1

    .line 1342902
    const/4 v0, 0x0

    goto :goto_0

    .line 1342903
    :cond_1
    iget-object v0, p0, LX/8QM;->j:Ljava/lang/Integer;

    check-cast p1, LX/8QM;

    invoke-virtual {p1}, LX/8QM;->n()Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 1342897
    iget v0, p0, LX/8QM;->g:I

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 1342896
    iget v0, p0, LX/8QM;->f:I

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1342895
    const/4 v0, 0x0

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 1342894
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/8QM;->j:Ljava/lang/Integer;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1342892
    iget-object v0, p0, LX/8QM;->i:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1342893
    iget-object v0, p0, LX/8QM;->j:Ljava/lang/Integer;

    return-object v0
.end method
