.class public LX/7Cb;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:LX/0Sh;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field private e:Landroid/media/MediaPlayer;

.field public f:Z

.field public g:LX/3RY;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1181159
    const-class v0, LX/7Cb;

    sput-object v0, LX/7Cb;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Sh;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1181153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1181154
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7Cb;->f:Z

    .line 1181155
    iput-object p1, p0, LX/7Cb;->b:Landroid/content/Context;

    .line 1181156
    iput-object p2, p0, LX/7Cb;->c:LX/0Sh;

    .line 1181157
    iput-object p3, p0, LX/7Cb;->d:Ljava/util/concurrent/ExecutorService;

    .line 1181158
    return-void
.end method

.method private a(IZF)V
    .locals 2

    .prologue
    .line 1181144
    iget-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 1181145
    iget-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    iget-boolean v1, p0, LX/7Cb;->f:Z

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 1181146
    iget-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    new-instance v1, LX/7CZ;

    invoke-direct {v1, p0}, LX/7CZ;-><init>(LX/7Cb;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 1181147
    iget-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    new-instance v1, LX/7Ca;

    invoke-direct {v1, p0}, LX/7Ca;-><init>(LX/7Cb;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1181148
    if-eqz p2, :cond_0

    .line 1181149
    iget-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 1181150
    :cond_0
    iget-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p3, p3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 1181151
    iget-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 1181152
    return-void
.end method

.method private static a(Landroid/content/res/AssetFileDescriptor;)V
    .locals 1

    .prologue
    .line 1181140
    if-nez p0, :cond_0

    .line 1181141
    :goto_0
    return-void

    .line 1181142
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1181143
    :catch_0
    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/7Cb;
    .locals 4

    .prologue
    .line 1181138
    new-instance v3, LX/7Cb;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v3, v0, v1, v2}, LX/7Cb;-><init>(Landroid/content/Context;LX/0Sh;Ljava/util/concurrent/ExecutorService;)V

    .line 1181139
    return-object v3
.end method

.method public static b(LX/7Cb;)V
    .locals 3

    .prologue
    .line 1181160
    iget-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 1181161
    :try_start_0
    iget-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 1181162
    iget-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 1181163
    const/4 v0, 0x0

    iput-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1181164
    :cond_0
    :goto_0
    iget-object v0, p0, LX/7Cb;->g:LX/3RY;

    if-eqz v0, :cond_1

    .line 1181165
    iget-object v0, p0, LX/7Cb;->g:LX/3RY;

    invoke-interface {v0, p0}, LX/3RY;->a(LX/7Cb;)V

    .line 1181166
    :cond_1
    return-void

    .line 1181167
    :catch_0
    move-exception v0

    .line 1181168
    sget-object v1, LX/7Cb;->a:Ljava/lang/Class;

    const-string v2, "MediaPlayer release failed: "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static b(LX/7Cb;IIF)V
    .locals 7

    .prologue
    .line 1181125
    :try_start_0
    iget-object v0, p0, LX/7Cb;->b:Landroid/content/Context;

    invoke-static {v0, p1}, LX/6LH;->a(Landroid/content/Context;I)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 1181126
    if-eqz v6, :cond_0

    .line 1181127
    :try_start_1
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    .line 1181128
    iget-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 1181129
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0, p3}, LX/7Cb;->a(IZF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1181130
    :try_start_2
    invoke-static {v6}, LX/7Cb;->a(Landroid/content/res/AssetFileDescriptor;)V

    .line 1181131
    :goto_0
    return-void

    .line 1181132
    :catchall_0
    move-exception v0

    invoke-static {v6}, LX/7Cb;->a(Landroid/content/res/AssetFileDescriptor;)V

    throw v0

    .line 1181133
    :catch_0
    goto :goto_0

    .line 1181134
    :cond_0
    iget-object v0, p0, LX/7Cb;->b:Landroid/content/Context;

    invoke-static {v0, p1}, LX/6LH;->b(Landroid/content/Context;I)Ljava/io/FileDescriptor;

    move-result-object v0

    .line 1181135
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    .line 1181136
    iget-object v1, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V

    .line 1181137
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0, p3}, LX/7Cb;->a(IZF)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method public static b(LX/7Cb;Landroid/net/Uri;IF)V
    .locals 3
    .param p0    # LX/7Cb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 1181116
    if-eqz p1, :cond_0

    .line 1181117
    :try_start_0
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    .line 1181118
    iget-object v1, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    iget-object v2, p0, LX/7Cb;->b:Landroid/content/Context;

    invoke-virtual {v1, v2, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1181119
    :goto_0
    invoke-direct {p0, p2, v0, p3}, LX/7Cb;->a(IZF)V

    .line 1181120
    :goto_1
    return-void

    .line 1181121
    :cond_0
    iget-object v0, p0, LX/7Cb;->b:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1181122
    const/4 v0, 0x0

    goto :goto_0

    .line 1181123
    :catch_0
    move-exception v0

    .line 1181124
    sget-object v1, LX/7Cb;->a:Ljava/lang/Class;

    const-string v2, "MediaPlayer create failed: "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1181110
    iget-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 1181111
    :try_start_0
    iget-object v0, p0, LX/7Cb;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1181112
    :cond_0
    :goto_0
    invoke-static {p0}, LX/7Cb;->b(LX/7Cb;)V

    .line 1181113
    return-void

    .line 1181114
    :catch_0
    move-exception v0

    .line 1181115
    sget-object v1, LX/7Cb;->a:Ljava/lang/Class;

    const-string v2, "MediaPlayer failed to stop: %s"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;I)V
    .locals 1
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1181108
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1, p2, v0}, LX/7Cb;->a(Landroid/net/Uri;IF)V

    .line 1181109
    return-void
.end method

.method public final a(Landroid/net/Uri;IF)V
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1181102
    iget-object v0, p0, LX/7Cb;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1181103
    invoke-static {p0, p1, p2, p3}, LX/7Cb;->b(LX/7Cb;Landroid/net/Uri;IF)V

    .line 1181104
    :goto_0
    return-void

    .line 1181105
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/7Cb;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/sounds/SoundPlayer$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/sounds/SoundPlayer$1;-><init>(LX/7Cb;Landroid/net/Uri;IF)V

    const v2, 0x3a6ffb60

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1181106
    :catch_0
    move-exception v0

    .line 1181107
    sget-object v1, LX/7Cb;->a:Ljava/lang/Class;

    const-string v2, "Attempt to play sound rejected by executor service"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
