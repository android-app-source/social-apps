.class public final LX/7NO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/7Mz;


# direct methods
.method public constructor <init>(LX/7Mz;)V
    .locals 0

    .prologue
    .line 1200239
    iput-object p1, p0, LX/7NO;->a:LX/7Mz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/34b;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1200254
    invoke-virtual {p1, p2}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 1200255
    iget-object v1, p0, LX/7NO;->a:LX/7Mz;

    invoke-static {v1}, LX/7Mz;->getCurrentToggleViewText(LX/7Mz;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 1200256
    invoke-virtual {v0, v2}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    .line 1200257
    if-eqz v1, :cond_0

    .line 1200258
    invoke-virtual {v0, v2}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object v1

    iget-object v2, p0, LX/7NO;->a:LX/7Mz;

    iget-object v2, v2, LX/7Mz;->v:Landroid/graphics/drawable/Drawable;

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1200259
    :cond_0
    new-instance v1, LX/7NN;

    invoke-direct {v1, p0}, LX/7NN;-><init>(LX/7NO;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1200260
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x2

    const v1, -0x45596d3e

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 1200240
    iget-object v0, p0, LX/7NO;->a:LX/7Mz;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1200241
    new-instance v8, LX/34b;

    iget-object v0, p0, LX/7NO;->a:LX/7Mz;

    invoke-virtual {v0}, LX/7Mz;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v8, v0}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 1200242
    const v0, 0x7f080d6c

    invoke-virtual {v8, v0}, LX/34b;->f(I)V

    .line 1200243
    iput-boolean v2, v8, LX/34b;->d:Z

    .line 1200244
    iget-object v0, p0, LX/7NO;->a:LX/7Mz;

    iget-object v0, v0, LX/7Mr;->f:LX/1C2;

    iget-object v1, p0, LX/7NO;->a:LX/7Mz;

    iget-object v1, v1, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, p0, LX/7NO;->a:LX/7Mz;

    iget-object v2, v2, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v3, p0, LX/7NO;->a:LX/7Mz;

    iget-object v3, v3, LX/2oy;->j:LX/2pb;

    invoke-virtual {v3}, LX/2pb;->s()LX/04D;

    move-result-object v3

    iget-object v4, p0, LX/7NO;->a:LX/7Mz;

    iget-object v4, v4, LX/2oy;->j:LX/2pb;

    .line 1200245
    iget-object v5, v4, LX/2pb;->D:LX/04G;

    move-object v4, v5

    .line 1200246
    iget-object v5, p0, LX/7NO;->a:LX/7Mz;

    iget-object v5, v5, LX/2oy;->j:LX/2pb;

    invoke-virtual {v5}, LX/2pb;->h()I

    iget-object v5, p0, LX/7NO;->a:LX/7Mz;

    iget-object v5, v5, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/1C2;->a(Ljava/lang/String;LX/0lF;LX/04D;LX/04G;ZLjava/lang/String;)LX/1C2;

    .line 1200247
    iget-object v0, p0, LX/7NO;->a:LX/7Mz;

    iget-object v0, v0, LX/7Mz;->w:Ljava/lang/String;

    invoke-direct {p0, v8, v0}, LX/7NO;->a(LX/34b;Ljava/lang/String;)V

    .line 1200248
    iget-object v0, p0, LX/7NO;->a:LX/7Mz;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 1200249
    iget-object v0, p0, LX/7NO;->a:LX/7Mz;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1200250
    invoke-direct {p0, v8, v0}, LX/7NO;->a(LX/34b;Ljava/lang/String;)V

    goto :goto_0

    .line 1200251
    :cond_0
    iget-object v0, p0, LX/7NO;->a:LX/7Mz;

    iget-object v0, v0, LX/7Mz;->u:LX/3Af;

    invoke-virtual {v0, v8}, LX/3Af;->a(LX/1OM;)V

    .line 1200252
    iget-object v0, p0, LX/7NO;->a:LX/7Mz;

    iget-object v0, v0, LX/7Mz;->u:LX/3Af;

    invoke-virtual {v0}, LX/3Af;->show()V

    .line 1200253
    const v0, -0x66d5e996

    invoke-static {v0, v7}, LX/02F;->a(II)V

    return-void
.end method
