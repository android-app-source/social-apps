.class public final LX/7e6;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/7e6;


# instance fields
.field public final b:Z

.field public final c:Lcom/google/android/gms/nearby/messages/Strategy;

.field public final d:Lcom/google/android/gms/nearby/messages/MessageFilter;

.field public final e:LX/7e4;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/7e5;

    invoke-direct {v0}, LX/7e5;-><init>()V

    invoke-virtual {v0}, LX/7e5;->a()LX/7e6;

    move-result-object v0

    sput-object v0, LX/7e6;->a:LX/7e6;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/nearby/messages/Strategy;Lcom/google/android/gms/nearby/messages/MessageFilter;LX/7e4;Z)V
    .locals 0
    .param p3    # LX/7e4;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LX/7e6;->c:Lcom/google/android/gms/nearby/messages/Strategy;

    iput-object p2, p0, LX/7e6;->d:Lcom/google/android/gms/nearby/messages/MessageFilter;

    iput-object p3, p0, LX/7e6;->e:LX/7e4;

    iput-boolean p4, p0, LX/7e6;->b:Z

    return-void
.end method
