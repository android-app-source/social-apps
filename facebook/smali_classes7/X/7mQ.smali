.class public final LX/7mQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7mW;


# direct methods
.method public constructor <init>(LX/7mW;)V
    .locals 0

    .prologue
    .line 1237008
    iput-object p1, p0, LX/7mQ;->a:LX/7mW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1237009
    iget-object v0, p0, LX/7mQ;->a:LX/7mW;

    iget-object v1, v0, LX/7mV;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 1237010
    :try_start_0
    iget-object v0, p0, LX/7mQ;->a:LX/7mW;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    .line 1237011
    iput-object v2, v0, LX/7mW;->k:LX/0am;

    .line 1237012
    iget-object v0, p0, LX/7mQ;->a:LX/7mW;

    sget-object v2, LX/7mU;->INIT:LX/7mU;

    .line 1237013
    iput-object v2, v0, LX/7mW;->j:LX/7mU;

    .line 1237014
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1237015
    check-cast p1, Ljava/lang/Boolean;

    .line 1237016
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1237017
    :cond_0
    iget-object v0, p0, LX/7mQ;->a:LX/7mW;

    iget-object v1, v0, LX/7mV;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 1237018
    :try_start_0
    iget-object v0, p0, LX/7mQ;->a:LX/7mW;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    .line 1237019
    iput-object v2, v0, LX/7mW;->k:LX/0am;

    .line 1237020
    iget-object v0, p0, LX/7mQ;->a:LX/7mW;

    sget-object v2, LX/7mU;->INIT:LX/7mU;

    .line 1237021
    iput-object v2, v0, LX/7mW;->j:LX/7mU;

    .line 1237022
    monitor-exit v1

    .line 1237023
    :cond_1
    return-void

    .line 1237024
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
