.class public final LX/7NG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/2pb;


# direct methods
.method public constructor <init>(LX/2pb;)V
    .locals 0

    .prologue
    .line 1200108
    iput-object p1, p0, LX/7NG;->a:LX/2pb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, 0x58e3fb78

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1200109
    invoke-interface {p3}, LX/0Yf;->isInitialStickyBroadcast()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/7NG;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->y:LX/2qV;

    invoke-virtual {v0}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1200110
    const-string v0, "state"

    const/4 v2, 0x1

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/45B;->CONNECTED:LX/45B;

    .line 1200111
    :goto_0
    iget-object v2, p0, LX/7NG;->a:LX/2pb;

    iget-object v2, v2, LX/2pb;->H:LX/2oj;

    if-eqz v2, :cond_0

    .line 1200112
    iget-object v2, p0, LX/7NG;->a:LX/2pb;

    iget-object v2, v2, LX/2pb;->H:LX/2oj;

    new-instance v3, LX/7M5;

    invoke-direct {v3, v0}, LX/7M5;-><init>(LX/45B;)V

    invoke-virtual {v2, v3}, LX/2oj;->a(LX/2ol;)V

    .line 1200113
    :cond_0
    iget-object v2, p0, LX/7NG;->a:LX/2pb;

    invoke-static {v2, v0}, LX/2pb;->a$redex0(LX/2pb;LX/45B;)V

    .line 1200114
    :cond_1
    const v0, -0x696c1e88

    invoke-static {v0, v1}, LX/02F;->e(II)V

    return-void

    .line 1200115
    :cond_2
    sget-object v0, LX/45B;->DISCONNECTED:LX/45B;

    goto :goto_0
.end method
