.class public final LX/8Qn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:LX/8Qu;


# direct methods
.method public constructor <init>(LX/8Qu;)V
    .locals 0

    .prologue
    .line 1343713
    iput-object p1, p0, LX/8Qn;->a:LX/8Qu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 1343682
    iget-object v0, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v0, v0, LX/8Qu;->g:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Qp;

    .line 1343683
    instance-of v1, v0, LX/8Qq;

    if-eqz v1, :cond_3

    .line 1343684
    check-cast v0, LX/8Qq;

    .line 1343685
    iget-object v1, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v1, v1, LX/8Qu;->e:LX/8Qz;

    iget v3, v0, LX/8Qq;->f:I

    invoke-interface {v1, v3}, LX/8Qz;->a(I)V

    .line 1343686
    iget v1, v0, LX/8Qq;->e:I

    const/16 v3, 0x2712

    if-ne v1, v3, :cond_1

    .line 1343687
    iget-object v0, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v0, v0, LX/8Qu;->b:LX/2c9;

    const-string v1, "friends_except"

    invoke-virtual {v0, v1, v2, v5}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    .line 1343688
    iget-object v0, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v0, v0, LX/8Qu;->f:LX/8R4;

    invoke-interface {v0}, LX/8R4;->a()V

    .line 1343689
    :cond_0
    :goto_0
    return-void

    .line 1343690
    :cond_1
    iget v1, v0, LX/8Qq;->e:I

    const/16 v3, 0x2713

    if-ne v1, v3, :cond_2

    .line 1343691
    iget-object v0, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v0, v0, LX/8Qu;->b:LX/2c9;

    const-string v1, "specific_friends"

    invoke-virtual {v0, v1, v2, v5}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    .line 1343692
    iget-object v0, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v0, v0, LX/8Qu;->f:LX/8R4;

    invoke-interface {v0}, LX/8R4;->b()V

    goto :goto_0

    .line 1343693
    :cond_2
    iget-object v1, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v1, v1, LX/8Qu;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343694
    iget-object v3, v1, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    move-object v1, v3

    .line 1343695
    iget v0, v0, LX/8Qq;->f:I

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1343696
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v3, "optionType"

    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 1343697
    iget-object v1, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v1, v1, LX/8Qu;->b:LX/2c9;

    const-string v3, "basic_option"

    invoke-virtual {v1, v3, v2, v0}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    goto :goto_0

    .line 1343698
    :cond_3
    instance-of v1, v0, LX/8Qr;

    if-eqz v1, :cond_6

    .line 1343699
    check-cast v0, LX/8Qr;

    .line 1343700
    iget-boolean v1, v0, LX/8Qr;->d:Z

    if-eqz v1, :cond_0

    .line 1343701
    iget-object v1, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v4, v1, LX/8Qu;->b:LX/2c9;

    iget-boolean v1, v0, LX/8Qr;->c:Z

    if-eqz v1, :cond_4

    const-string v1, "disable_tag_expansion"

    :goto_1
    invoke-virtual {v4, v1, v2, v5}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    .line 1343702
    iget-object v1, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v1, v1, LX/8Qu;->e:LX/8Qz;

    iget-boolean v0, v0, LX/8Qr;->c:Z

    if-nez v0, :cond_5

    move v0, v2

    :goto_2
    invoke-interface {v1, v0}, LX/8Qz;->a(Z)V

    goto :goto_0

    .line 1343703
    :cond_4
    const-string v1, "enable_tag_expansion"

    goto :goto_1

    :cond_5
    move v0, v3

    .line 1343704
    goto :goto_2

    .line 1343705
    :cond_6
    instance-of v0, v0, LX/8Qt;

    if-eqz v0, :cond_0

    .line 1343706
    iget-object v0, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v0, v0, LX/8Qu;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343707
    iget-boolean v1, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->k:Z

    move v0, v1

    .line 1343708
    if-eqz v0, :cond_7

    .line 1343709
    iget-object v0, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v0, v0, LX/8Qu;->b:LX/2c9;

    const-string v1, "expand_more"

    invoke-virtual {v0, v1, v2, v5}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    .line 1343710
    iget-object v0, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v0, v0, LX/8Qu;->e:LX/8Qz;

    invoke-interface {v0, v3}, LX/8Qz;->b(Z)V

    goto/16 :goto_0

    .line 1343711
    :cond_7
    iget-object v0, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v0, v0, LX/8Qu;->b:LX/2c9;

    const-string v1, "see_all_lists"

    invoke-virtual {v0, v1, v2, v5}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    .line 1343712
    iget-object v0, p0, LX/8Qn;->a:LX/8Qu;

    iget-object v0, v0, LX/8Qu;->f:LX/8R4;

    invoke-interface {v0}, LX/8R4;->c()V

    goto/16 :goto_0
.end method
