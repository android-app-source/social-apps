.class public LX/74G;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field private final b:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1167719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1167720
    iput-object p1, p0, LX/74G;->b:LX/0Zb;

    .line 1167721
    return-void
.end method

.method public static a(LX/0QB;)LX/74G;
    .locals 1

    .prologue
    .line 1167718
    invoke-static {p0}, LX/74G;->b(LX/0QB;)LX/74G;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/74F;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1167717
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p0}, LX/74F;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/74G;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 1167722
    const-string v0, "composer"

    .line 1167723
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1167724
    iget-object v0, p0, LX/74G;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1167725
    iget-object v0, p0, LX/74G;->a:Ljava/lang/String;

    .line 1167726
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1167727
    :cond_0
    iget-object v0, p0, LX/74G;->b:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1167728
    return-void
.end method

.method public static b(LX/0QB;)LX/74G;
    .locals 2

    .prologue
    .line 1167715
    new-instance v1, LX/74G;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/74G;-><init>(LX/0Zb;)V

    .line 1167716
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1167713
    sget-object v0, LX/74F;->LAUNCH_GALLERY:LX/74F;

    invoke-static {v0}, LX/74G;->a(LX/74F;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0, v0}, LX/74G;->a(LX/74G;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1167714
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1167711
    sget-object v0, LX/74F;->CLOSE_GALLERY_PICKER:LX/74F;

    invoke-static {v0}, LX/74G;->a(LX/74F;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0, v0}, LX/74G;->a(LX/74G;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1167712
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1167709
    sget-object v0, LX/74F;->CLOSE_GALLERY_COMPOSER:LX/74F;

    invoke-static {v0}, LX/74G;->a(LX/74F;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0, v0}, LX/74G;->a(LX/74G;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1167710
    return-void
.end method
