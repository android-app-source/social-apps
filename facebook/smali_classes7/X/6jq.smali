.class public LX/6jq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final deltaNewMessage:LX/6k4;

.field public final genieFbId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1133107
    new-instance v0, LX/1sv;

    const-string v1, "DeltaGenieMessage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jq;->b:LX/1sv;

    .line 1133108
    new-instance v0, LX/1sw;

    const-string v1, "deltaNewMessage"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jq;->c:LX/1sw;

    .line 1133109
    new-instance v0, LX/1sw;

    const-string v1, "genieFbId"

    const/16 v2, 0xa

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jq;->d:LX/1sw;

    .line 1133110
    sput-boolean v4, LX/6jq;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6k4;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1133111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1133112
    iput-object p1, p0, LX/6jq;->deltaNewMessage:LX/6k4;

    .line 1133113
    iput-object p2, p0, LX/6jq;->genieFbId:Ljava/lang/Long;

    .line 1133114
    return-void
.end method

.method public static a(LX/6jq;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1133115
    iget-object v0, p0, LX/6jq;->deltaNewMessage:LX/6k4;

    if-nez v0, :cond_0

    .line 1133116
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'deltaNewMessage\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jq;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1133117
    :cond_0
    iget-object v0, p0, LX/6jq;->genieFbId:Ljava/lang/Long;

    if-nez v0, :cond_1

    .line 1133118
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'genieFbId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jq;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1133119
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1133120
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1133121
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1133122
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1133123
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaGenieMessage"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1133124
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133125
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133126
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133127
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133128
    const-string v4, "deltaNewMessage"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133129
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133130
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133131
    iget-object v4, p0, LX/6jq;->deltaNewMessage:LX/6k4;

    if-nez v4, :cond_3

    .line 1133132
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133133
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133134
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133135
    const-string v4, "genieFbId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133136
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133137
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133138
    iget-object v0, p0, LX/6jq;->genieFbId:Ljava/lang/Long;

    if-nez v0, :cond_4

    .line 1133139
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133140
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133141
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133142
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1133143
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1133144
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1133145
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1133146
    :cond_3
    iget-object v4, p0, LX/6jq;->deltaNewMessage:LX/6k4;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1133147
    :cond_4
    iget-object v0, p0, LX/6jq;->genieFbId:Ljava/lang/Long;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1133148
    invoke-static {p0}, LX/6jq;->a(LX/6jq;)V

    .line 1133149
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1133150
    iget-object v0, p0, LX/6jq;->deltaNewMessage:LX/6k4;

    if-eqz v0, :cond_0

    .line 1133151
    sget-object v0, LX/6jq;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133152
    iget-object v0, p0, LX/6jq;->deltaNewMessage:LX/6k4;

    invoke-virtual {v0, p1}, LX/6k4;->a(LX/1su;)V

    .line 1133153
    :cond_0
    iget-object v0, p0, LX/6jq;->genieFbId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1133154
    sget-object v0, LX/6jq;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133155
    iget-object v0, p0, LX/6jq;->genieFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1133156
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1133157
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1133158
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1133159
    if-nez p1, :cond_1

    .line 1133160
    :cond_0
    :goto_0
    return v0

    .line 1133161
    :cond_1
    instance-of v1, p1, LX/6jq;

    if-eqz v1, :cond_0

    .line 1133162
    check-cast p1, LX/6jq;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1133163
    if-nez p1, :cond_3

    .line 1133164
    :cond_2
    :goto_1
    move v0, v2

    .line 1133165
    goto :goto_0

    .line 1133166
    :cond_3
    iget-object v0, p0, LX/6jq;->deltaNewMessage:LX/6k4;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1133167
    :goto_2
    iget-object v3, p1, LX/6jq;->deltaNewMessage:LX/6k4;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1133168
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1133169
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133170
    iget-object v0, p0, LX/6jq;->deltaNewMessage:LX/6k4;

    iget-object v3, p1, LX/6jq;->deltaNewMessage:LX/6k4;

    invoke-virtual {v0, v3}, LX/6k4;->a(LX/6k4;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133171
    :cond_5
    iget-object v0, p0, LX/6jq;->genieFbId:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1133172
    :goto_4
    iget-object v3, p1, LX/6jq;->genieFbId:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1133173
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1133174
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133175
    iget-object v0, p0, LX/6jq;->genieFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/6jq;->genieFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1133176
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1133177
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1133178
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1133179
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1133180
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1133181
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1133182
    sget-boolean v0, LX/6jq;->a:Z

    .line 1133183
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jq;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1133184
    return-object v0
.end method
