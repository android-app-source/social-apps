.class public LX/6sy;
.super LX/6E8;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E8",
        "<",
        "Lcom/facebook/payments/ui/PrimaryCtaButtonView;",
        "LX/6sw;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private l:LX/6qh;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/ui/PrimaryCtaButtonView;)V
    .locals 0

    .prologue
    .line 1153873
    invoke-direct {p0, p1}, LX/6E8;-><init>(LX/6E6;)V

    .line 1153874
    return-void
.end method


# virtual methods
.method public final a(LX/6E2;)V
    .locals 3

    .prologue
    .line 1153882
    check-cast p1, LX/6sw;

    .line 1153883
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    .line 1153884
    iget-object v1, p0, LX/6sy;->l:LX/6qh;

    .line 1153885
    iput-object v1, v0, LX/73U;->a:LX/6qh;

    .line 1153886
    sget-object v1, LX/6sx;->a:[I

    iget-object v2, p1, LX/6sw;->a:LX/6sv;

    invoke-virtual {v2}, LX/6sv;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1153887
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown state seen: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, LX/6sw;->a:LX/6sv;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1153888
    :pswitch_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setEnabled(Z)V

    .line 1153889
    iget-object v1, p1, LX/6sw;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setCtaButtonText(Ljava/lang/CharSequence;)V

    .line 1153890
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1153891
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b()V

    .line 1153892
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->f()V

    .line 1153893
    :goto_0
    return-void

    .line 1153894
    :pswitch_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setEnabled(Z)V

    .line 1153895
    iget-object v1, p1, LX/6sw;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setCtaButtonText(Ljava/lang/CharSequence;)V

    .line 1153896
    invoke-virtual {v0, p0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1153897
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b()V

    .line 1153898
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->f()V

    .line 1153899
    goto :goto_0

    .line 1153900
    :pswitch_2
    const/4 v1, 0x0

    .line 1153901
    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setCtaButtonText(Ljava/lang/CharSequence;)V

    .line 1153902
    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1153903
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setEnabled(Z)V

    .line 1153904
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->f()V

    .line 1153905
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->a()V

    .line 1153906
    goto :goto_0

    .line 1153907
    :pswitch_3
    const/4 v1, 0x0

    .line 1153908
    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setCtaButtonText(Ljava/lang/CharSequence;)V

    .line 1153909
    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1153910
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setEnabled(Z)V

    .line 1153911
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b()V

    .line 1153912
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->e()V

    .line 1153913
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1153880
    iput-object p1, p0, LX/6sy;->l:LX/6qh;

    .line 1153881
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x75ccf814

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1153875
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1153876
    const-string v2, "extra_mutation"

    const-string v3, "mutation_pay_button"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153877
    new-instance v2, LX/73T;

    sget-object v3, LX/73S;->MUTATION:LX/73S;

    invoke-direct {v2, v3, v1}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    .line 1153878
    iget-object v1, p0, LX/6sy;->l:LX/6qh;

    invoke-virtual {v1, v2}, LX/6qh;->a(LX/73T;)V

    .line 1153879
    const v1, 0x581b68d1

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
