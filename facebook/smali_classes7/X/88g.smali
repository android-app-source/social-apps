.class public LX/88g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/88f;


# static fields
.field public static final a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1302837
    new-instance v0, LX/89I;

    const-wide/16 v2, 0x1

    sget-object v1, LX/2rw;->UNDIRECTED:LX/2rw;

    invoke-direct {v0, v2, v3, v1}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    sput-object v0, LX/88g;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1302836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z
    .locals 4

    .prologue
    .line 1302835
    if-eqz p0, :cond_0

    iget-wide v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    sget-object v2, LX/88g;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/88g;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c()LX/88g;
    .locals 1

    .prologue
    .line 1302832
    new-instance v0, LX/88g;

    invoke-direct {v0}, LX/88g;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1302834
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1302833
    const-string v0, "SellComposerPluginConfig"

    return-object v0
.end method
