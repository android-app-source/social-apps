.class public LX/7DG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:F

.field public final b:F

.field public final c:F

.field public final d:F

.field public final e:Z

.field public final f:Z


# direct methods
.method public constructor <init>(LX/7DF;)V
    .locals 1

    .prologue
    .line 1182496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182497
    iget v0, p1, LX/7DF;->a:F

    iput v0, p0, LX/7DG;->a:F

    .line 1182498
    iget v0, p1, LX/7DF;->b:F

    iput v0, p0, LX/7DG;->b:F

    .line 1182499
    iget v0, p1, LX/7DF;->c:F

    iput v0, p0, LX/7DG;->c:F

    .line 1182500
    iget v0, p1, LX/7DF;->d:F

    iput v0, p0, LX/7DG;->d:F

    .line 1182501
    iget-boolean v0, p1, LX/7DF;->f:Z

    iput-boolean v0, p0, LX/7DG;->f:Z

    .line 1182502
    iget-boolean v0, p1, LX/7DF;->e:Z

    iput-boolean v0, p0, LX/7DG;->e:Z

    .line 1182503
    return-void
.end method

.method public static a(FFF)F
    .locals 1

    .prologue
    .line 1182504
    invoke-static {p1, p0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method
