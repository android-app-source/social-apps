.class public final LX/7Cs;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/7Cu;


# direct methods
.method public constructor <init>(LX/7Cu;)V
    .locals 0

    .prologue
    .line 1181911
    iput-object p1, p0, LX/7Cs;->a:LX/7Cu;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 8

    .prologue
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 1181912
    invoke-super {p0, p1}, LX/0xh;->a(LX/0wd;)V

    .line 1181913
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    .line 1181914
    iget-object v2, p0, LX/7Cs;->a:LX/7Cu;

    iget-object v2, v2, LX/7Cu;->x:LX/7Ct;

    sget-object v3, LX/7Ct;->LEFT:LX/7Ct;

    if-ne v2, v3, :cond_1

    .line 1181915
    iget-object v2, p0, LX/7Cs;->a:LX/7Cu;

    iget-object v3, p0, LX/7Cs;->a:LX/7Cu;

    iget v3, v3, LX/7Cf;->d:F

    float-to-double v4, v3

    sub-double v0, v6, v0

    iget-object v3, p0, LX/7Cs;->a:LX/7Cu;

    iget v3, v3, LX/7Cf;->b:F

    iget-object v6, p0, LX/7Cs;->a:LX/7Cu;

    iget v6, v6, LX/7Cf;->d:F

    sub-float/2addr v3, v6

    float-to-double v6, v3

    mul-double/2addr v0, v6

    add-double/2addr v0, v4

    double-to-float v0, v0

    iput v0, v2, LX/7Cu;->b:F

    .line 1181916
    :cond_0
    :goto_0
    return-void

    .line 1181917
    :cond_1
    iget-object v2, p0, LX/7Cs;->a:LX/7Cu;

    iget-object v2, v2, LX/7Cu;->x:LX/7Ct;

    sget-object v3, LX/7Ct;->RIGHT:LX/7Ct;

    if-ne v2, v3, :cond_0

    .line 1181918
    iget-object v2, p0, LX/7Cs;->a:LX/7Cu;

    iget-object v3, p0, LX/7Cs;->a:LX/7Cu;

    iget v3, v3, LX/7Cf;->c:F

    float-to-double v4, v3

    sub-double v0, v6, v0

    iget-object v3, p0, LX/7Cs;->a:LX/7Cu;

    iget v3, v3, LX/7Cf;->b:F

    iget-object v6, p0, LX/7Cs;->a:LX/7Cu;

    iget v6, v6, LX/7Cf;->c:F

    sub-float/2addr v3, v6

    float-to-double v6, v3

    mul-double/2addr v0, v6

    add-double/2addr v0, v4

    double-to-float v0, v0

    iput v0, v2, LX/7Cu;->b:F

    goto :goto_0
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 1181919
    invoke-super {p0, p1}, LX/0xh;->b(LX/0wd;)V

    .line 1181920
    iget-object v0, p0, LX/7Cs;->a:LX/7Cu;

    const/4 v1, 0x0

    .line 1181921
    iput-boolean v1, v0, LX/7Cu;->B:Z

    .line 1181922
    return-void
.end method
