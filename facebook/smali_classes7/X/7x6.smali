.class public LX/7x6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:LX/0tX;

.field private final c:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

.field private final d:Ljava/util/concurrent/Executor;

.field public final e:LX/03V;

.field public final f:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0tX;Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Ljava/util/concurrent/Executor;LX/03V;LX/0ad;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1277001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1277002
    iput-object p1, p0, LX/7x6;->a:Landroid/content/res/Resources;

    .line 1277003
    iput-object p2, p0, LX/7x6;->b:LX/0tX;

    .line 1277004
    iput-object p3, p0, LX/7x6;->c:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    .line 1277005
    iput-object p4, p0, LX/7x6;->d:Ljava/util/concurrent/Executor;

    .line 1277006
    iput-object p5, p0, LX/7x6;->e:LX/03V;

    .line 1277007
    iput-object p6, p0, LX/7x6;->f:LX/0ad;

    .line 1277008
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V
    .locals 5

    .prologue
    .line 1277009
    new-instance v0, LX/0v6;

    const-string v1, "event_ticketing"

    invoke-direct {v0, v1}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 1277010
    new-instance v1, LX/7oE;

    invoke-direct {v1}, LX/7oE;-><init>()V

    move-object v2, v1

    .line 1277011
    iget-object v1, p0, LX/7x6;->f:LX/0ad;

    sget-short v3, LX/347;->h:S

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x14

    .line 1277012
    :goto_0
    const-string v3, "event_id"

    invoke-virtual {v2, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1277013
    const-string v3, "profile_image_size"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1277014
    const-string v3, "number_of_fetched_ticket_tiers"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1277015
    const-string v1, "number_of_registration_settings"

    const-string v3, "1"

    invoke-virtual {v2, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1277016
    const-string v1, "seat_map_image_size"

    iget-object v3, p0, LX/7x6;->a:Landroid/content/res/Resources;

    const v4, 0x7f0b1477

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1277017
    const-string v1, "seat_map_thumbnail_size"

    iget-object v3, p0, LX/7x6;->a:Landroid/content/res/Resources;

    const v4, 0x7f0b1478

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1277018
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-static {v1}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v1, v1

    .line 1277019
    iget-object v2, p0, LX/7x6;->c:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-virtual {v2}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1277020
    const/4 v3, 0x2

    new-array v3, v3, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    invoke-static {v3}, LX/0Vg;->b([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, LX/7x5;

    invoke-direct {v4, p0, v1, v2, p3}, LX/7x5;-><init>(LX/7x6;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V

    iget-object v1, p0, LX/7x6;->d:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1277021
    iget-object v1, p0, LX/7x6;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0v6;)V

    .line 1277022
    return-void

    .line 1277023
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method
