.class public LX/6mt;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/facebook/proxygen/MQTTClient;

.field public final c:Ljava/util/concurrent/Executor;

.field public volatile d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1146790
    const-class v0, LX/6mt;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6mt;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/proxygen/MQTTClient;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 1146791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146792
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6mt;->d:Z

    .line 1146793
    iput-object p1, p0, LX/6mt;->b:Lcom/facebook/proxygen/MQTTClient;

    .line 1146794
    iput-object p2, p0, LX/6mt;->c:Ljava/util/concurrent/Executor;

    .line 1146795
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I[BIIZ)V
    .locals 8

    .prologue
    .line 1146796
    new-instance v0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;-><init>(LX/6mt;Ljava/lang/String;I[BIIZ)V

    .line 1146797
    iget-object v1, p0, LX/6mt;->c:Ljava/util/concurrent/Executor;

    const v2, 0x6146b79c

    invoke-static {v1, v0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1146798
    return-void
.end method
