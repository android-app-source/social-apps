.class public final enum LX/7gY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gY;

.field public static final enum FIRST_VIEW:LX/7gY;

.field public static final enum HAS_MY_STORY:LX/7gY;

.field public static final enum HAS_NEW_CONTENT:LX/7gY;

.field public static final enum MEDIA_ID:LX/7gY;

.field public static final enum MEDIA_INDEX:LX/7gY;

.field public static final enum MEDIA_TYPE:LX/7gY;

.field public static final enum NEW_STORY_COUNT:LX/7gY;

.field public static final enum SOURCE:LX/7gY;

.field public static final enum STORY_COUNT:LX/7gY;

.field public static final enum STORY_INDEX:LX/7gY;

.field public static final enum STORY_OWNER:LX/7gY;

.field public static final enum STORY_OWNER_TYPE:LX/7gY;

.field public static final enum STORY_SIZE:LX/7gY;

.field public static final enum TRAY_SESSION_ID:LX/7gY;

.field public static final enum VIEWER_COUNT:LX/7gY;

.field public static final enum VIEWER_SESSION_ID:LX/7gY;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1224476
    new-instance v0, LX/7gY;

    const-string v1, "TRAY_SESSION_ID"

    const-string v2, "tray_session_id"

    invoke-direct {v0, v1, v4, v2}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->TRAY_SESSION_ID:LX/7gY;

    .line 1224477
    new-instance v0, LX/7gY;

    const-string v1, "HAS_MY_STORY"

    const-string v2, "has_my_story"

    invoke-direct {v0, v1, v5, v2}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->HAS_MY_STORY:LX/7gY;

    .line 1224478
    new-instance v0, LX/7gY;

    const-string v1, "STORY_COUNT"

    const-string v2, "number_stories"

    invoke-direct {v0, v1, v6, v2}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->STORY_COUNT:LX/7gY;

    .line 1224479
    new-instance v0, LX/7gY;

    const-string v1, "NEW_STORY_COUNT"

    const-string v2, "number_new_stories"

    invoke-direct {v0, v1, v7, v2}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->NEW_STORY_COUNT:LX/7gY;

    .line 1224480
    new-instance v0, LX/7gY;

    const-string v1, "SOURCE"

    const-string v2, "source"

    invoke-direct {v0, v1, v8, v2}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->SOURCE:LX/7gY;

    .line 1224481
    new-instance v0, LX/7gY;

    const-string v1, "STORY_OWNER"

    const/4 v2, 0x5

    const-string v3, "story_owner"

    invoke-direct {v0, v1, v2, v3}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->STORY_OWNER:LX/7gY;

    .line 1224482
    new-instance v0, LX/7gY;

    const-string v1, "STORY_INDEX"

    const/4 v2, 0x6

    const-string v3, "story_index"

    invoke-direct {v0, v1, v2, v3}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->STORY_INDEX:LX/7gY;

    .line 1224483
    new-instance v0, LX/7gY;

    const-string v1, "HAS_NEW_CONTENT"

    const/4 v2, 0x7

    const-string v3, "has_new_content"

    invoke-direct {v0, v1, v2, v3}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->HAS_NEW_CONTENT:LX/7gY;

    .line 1224484
    new-instance v0, LX/7gY;

    const-string v1, "STORY_OWNER_TYPE"

    const/16 v2, 0x8

    const-string v3, "story_owner_type"

    invoke-direct {v0, v1, v2, v3}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->STORY_OWNER_TYPE:LX/7gY;

    .line 1224485
    new-instance v0, LX/7gY;

    const-string v1, "VIEWER_SESSION_ID"

    const/16 v2, 0x9

    const-string v3, "viewer_session_id"

    invoke-direct {v0, v1, v2, v3}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->VIEWER_SESSION_ID:LX/7gY;

    .line 1224486
    new-instance v0, LX/7gY;

    const-string v1, "STORY_SIZE"

    const/16 v2, 0xa

    const-string v3, "story_size"

    invoke-direct {v0, v1, v2, v3}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->STORY_SIZE:LX/7gY;

    .line 1224487
    new-instance v0, LX/7gY;

    const-string v1, "MEDIA_ID"

    const/16 v2, 0xb

    const-string v3, "media_id"

    invoke-direct {v0, v1, v2, v3}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->MEDIA_ID:LX/7gY;

    .line 1224488
    new-instance v0, LX/7gY;

    const-string v1, "MEDIA_INDEX"

    const/16 v2, 0xc

    const-string v3, "media_index"

    invoke-direct {v0, v1, v2, v3}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->MEDIA_INDEX:LX/7gY;

    .line 1224489
    new-instance v0, LX/7gY;

    const-string v1, "MEDIA_TYPE"

    const/16 v2, 0xd

    const-string v3, "media_type"

    invoke-direct {v0, v1, v2, v3}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->MEDIA_TYPE:LX/7gY;

    .line 1224490
    new-instance v0, LX/7gY;

    const-string v1, "FIRST_VIEW"

    const/16 v2, 0xe

    const-string v3, "first_view"

    invoke-direct {v0, v1, v2, v3}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->FIRST_VIEW:LX/7gY;

    .line 1224491
    new-instance v0, LX/7gY;

    const-string v1, "VIEWER_COUNT"

    const/16 v2, 0xf

    const-string v3, "number_viewers"

    invoke-direct {v0, v1, v2, v3}, LX/7gY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gY;->VIEWER_COUNT:LX/7gY;

    .line 1224492
    const/16 v0, 0x10

    new-array v0, v0, [LX/7gY;

    sget-object v1, LX/7gY;->TRAY_SESSION_ID:LX/7gY;

    aput-object v1, v0, v4

    sget-object v1, LX/7gY;->HAS_MY_STORY:LX/7gY;

    aput-object v1, v0, v5

    sget-object v1, LX/7gY;->STORY_COUNT:LX/7gY;

    aput-object v1, v0, v6

    sget-object v1, LX/7gY;->NEW_STORY_COUNT:LX/7gY;

    aput-object v1, v0, v7

    sget-object v1, LX/7gY;->SOURCE:LX/7gY;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7gY;->STORY_OWNER:LX/7gY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7gY;->STORY_INDEX:LX/7gY;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7gY;->HAS_NEW_CONTENT:LX/7gY;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7gY;->STORY_OWNER_TYPE:LX/7gY;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7gY;->VIEWER_SESSION_ID:LX/7gY;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7gY;->STORY_SIZE:LX/7gY;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7gY;->MEDIA_ID:LX/7gY;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7gY;->MEDIA_INDEX:LX/7gY;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/7gY;->MEDIA_TYPE:LX/7gY;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/7gY;->FIRST_VIEW:LX/7gY;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/7gY;->VIEWER_COUNT:LX/7gY;

    aput-object v2, v0, v1

    sput-object v0, LX/7gY;->$VALUES:[LX/7gY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1224493
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1224494
    iput-object p3, p0, LX/7gY;->mName:Ljava/lang/String;

    .line 1224495
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gY;
    .locals 1

    .prologue
    .line 1224496
    const-class v0, LX/7gY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gY;

    return-object v0
.end method

.method public static values()[LX/7gY;
    .locals 1

    .prologue
    .line 1224497
    sget-object v0, LX/7gY;->$VALUES:[LX/7gY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gY;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224498
    iget-object v0, p0, LX/7gY;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224499
    iget-object v0, p0, LX/7gY;->mName:Ljava/lang/String;

    return-object v0
.end method
