.class public final enum LX/7iP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7iP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7iP;

.field public static final enum BOOKMARKS:LX/7iP;

.field public static final enum GLOBAL_SEARCH:LX/7iP;

.field public static final enum MINI_PRODUCT_CARD:LX/7iP;

.field public static final enum MINI_PRODUCT_END_CARD:LX/7iP;

.field public static final enum PAGE:LX/7iP;

.field public static final enum PAGE_STORE_FRONT_CTA:LX/7iP;

.field public static final enum PDFY:LX/7iP;

.field public static final enum PRODUCT_TAG:LX/7iP;

.field public static final enum SHARE:LX/7iP;

.field public static final enum SHOPPING:LX/7iP;

.field public static final enum UNKNOWN:LX/7iP;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1227454
    new-instance v0, LX/7iP;

    const-string v1, "SHARE"

    const-string v2, "share"

    invoke-direct {v0, v1, v4, v2}, LX/7iP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iP;->SHARE:LX/7iP;

    .line 1227455
    new-instance v0, LX/7iP;

    const-string v1, "PAGE"

    const-string v2, "page"

    invoke-direct {v0, v1, v5, v2}, LX/7iP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iP;->PAGE:LX/7iP;

    .line 1227456
    new-instance v0, LX/7iP;

    const-string v1, "SHOPPING"

    const-string v2, "shopping"

    invoke-direct {v0, v1, v6, v2}, LX/7iP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iP;->SHOPPING:LX/7iP;

    .line 1227457
    new-instance v0, LX/7iP;

    const-string v1, "BOOKMARKS"

    const-string v2, "bookmarks"

    invoke-direct {v0, v1, v7, v2}, LX/7iP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iP;->BOOKMARKS:LX/7iP;

    .line 1227458
    new-instance v0, LX/7iP;

    const-string v1, "GLOBAL_SEARCH"

    const-string v2, "global_search"

    invoke-direct {v0, v1, v8, v2}, LX/7iP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iP;->GLOBAL_SEARCH:LX/7iP;

    .line 1227459
    new-instance v0, LX/7iP;

    const-string v1, "PRODUCT_TAG"

    const/4 v2, 0x5

    const-string v3, "product_tag"

    invoke-direct {v0, v1, v2, v3}, LX/7iP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iP;->PRODUCT_TAG:LX/7iP;

    .line 1227460
    new-instance v0, LX/7iP;

    const-string v1, "MINI_PRODUCT_CARD"

    const/4 v2, 0x6

    const-string v3, "mini_product_card"

    invoke-direct {v0, v1, v2, v3}, LX/7iP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iP;->MINI_PRODUCT_CARD:LX/7iP;

    .line 1227461
    new-instance v0, LX/7iP;

    const-string v1, "MINI_PRODUCT_END_CARD"

    const/4 v2, 0x7

    const-string v3, "mini_product_end_card"

    invoke-direct {v0, v1, v2, v3}, LX/7iP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iP;->MINI_PRODUCT_END_CARD:LX/7iP;

    .line 1227462
    new-instance v0, LX/7iP;

    const-string v1, "PAGE_STORE_FRONT_CTA"

    const/16 v2, 0x8

    const-string v3, "page_store_front_cta"

    invoke-direct {v0, v1, v2, v3}, LX/7iP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iP;->PAGE_STORE_FRONT_CTA:LX/7iP;

    .line 1227463
    new-instance v0, LX/7iP;

    const-string v1, "PDFY"

    const/16 v2, 0x9

    const-string v3, "pdfy"

    invoke-direct {v0, v1, v2, v3}, LX/7iP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iP;->PDFY:LX/7iP;

    .line 1227464
    new-instance v0, LX/7iP;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xa

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/7iP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iP;->UNKNOWN:LX/7iP;

    .line 1227465
    const/16 v0, 0xb

    new-array v0, v0, [LX/7iP;

    sget-object v1, LX/7iP;->SHARE:LX/7iP;

    aput-object v1, v0, v4

    sget-object v1, LX/7iP;->PAGE:LX/7iP;

    aput-object v1, v0, v5

    sget-object v1, LX/7iP;->SHOPPING:LX/7iP;

    aput-object v1, v0, v6

    sget-object v1, LX/7iP;->BOOKMARKS:LX/7iP;

    aput-object v1, v0, v7

    sget-object v1, LX/7iP;->GLOBAL_SEARCH:LX/7iP;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7iP;->PRODUCT_TAG:LX/7iP;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7iP;->MINI_PRODUCT_CARD:LX/7iP;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7iP;->MINI_PRODUCT_END_CARD:LX/7iP;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7iP;->PAGE_STORE_FRONT_CTA:LX/7iP;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7iP;->PDFY:LX/7iP;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7iP;->UNKNOWN:LX/7iP;

    aput-object v2, v0, v1

    sput-object v0, LX/7iP;->$VALUES:[LX/7iP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1227470
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1227471
    iput-object p3, p0, LX/7iP;->value:Ljava/lang/String;

    .line 1227472
    return-void
.end method

.method public static getFromValue(Ljava/lang/String;)LX/7iP;
    .locals 1

    .prologue
    .line 1227468
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7iP;->valueOf(Ljava/lang/String;)LX/7iP;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1227469
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, LX/7iP;->UNKNOWN:LX/7iP;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/7iP;
    .locals 1

    .prologue
    .line 1227467
    const-class v0, LX/7iP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7iP;

    return-object v0
.end method

.method public static values()[LX/7iP;
    .locals 1

    .prologue
    .line 1227466
    sget-object v0, LX/7iP;->$VALUES:[LX/7iP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7iP;

    return-object v0
.end method
