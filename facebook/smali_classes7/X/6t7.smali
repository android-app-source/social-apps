.class public LX/6t7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6sl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6sl",
        "<",
        "LX/6t8;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6t8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/6t8;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1154042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1154043
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The bundle of PriceTableCheckoutRow found to be empty."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1154044
    iput-object p1, p0, LX/6t7;->a:LX/0Px;

    .line 1154045
    return-void

    .line 1154046
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/6t8;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1154047
    iget-object v0, p0, LX/6t7;->a:LX/0Px;

    return-object v0
.end method

.method public final b()LX/6so;
    .locals 1

    .prologue
    .line 1154041
    sget-object v0, LX/6so;->PRICE_TABLE:LX/6so;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1154040
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1154039
    const/4 v0, 0x0

    return v0
.end method
