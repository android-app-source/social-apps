.class public LX/7Bf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/7Bf;


# instance fields
.field private final a:LX/7Bc;

.field private final b:LX/7Bi;

.field private final c:LX/7CV;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/7Bc;LX/7Bi;LX/7CV;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1179574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1179575
    iput-object p1, p0, LX/7Bf;->a:LX/7Bc;

    .line 1179576
    iput-object p2, p0, LX/7Bf;->b:LX/7Bi;

    .line 1179577
    iput-object p3, p0, LX/7Bf;->c:LX/7CV;

    .line 1179578
    iput-object p4, p0, LX/7Bf;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1179579
    return-void
.end method

.method private static a(LX/7Bf;Landroid/database/sqlite/SQLiteDatabase;LX/0Px;Z)I
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "LX/0Px",
            "<",
            "LX/7C0;",
            ">;Z)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1179358
    if-eqz p3, :cond_1

    .line 1179359
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "INSERT INTO entities ( "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/7Br;->b:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->c:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->d:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->e:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->f:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->g:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->h:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->i:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->j:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->k:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->l:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->m:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->n:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->o:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->p:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " )  SELECT ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? WHERE NOT EXISTS ( SELECT "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->b:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " FROM entities"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " WHERE "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->b:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " = ?) "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    move-object v0, v0

    .line 1179360
    move-object v6, v0

    .line 1179361
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "INSERT INTO entities_data ("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/7Bn;->a:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Bn;->b:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") VALUES (?, ?)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    move-object v4, v0

    .line 1179362
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "INSERT INTO entities_phonetic_data ("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/7Bp;->a:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Bp;->b:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") VALUES (?, ?)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    move-object v5, v0

    .line 1179363
    :try_start_0
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v9

    move v8, v1

    move v0, v1

    :goto_1
    if-ge v8, v9, :cond_2

    invoke-virtual {p2, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7C0;

    .line 1179364
    invoke-static {v1, v6, p3}, LX/7Bf;->a(LX/7C0;Landroid/database/sqlite/SQLiteStatement;Z)J

    move-result-wide v2

    .line 1179365
    const-wide/16 v10, -0x1

    cmp-long v7, v2, v10

    if-eqz v7, :cond_0

    .line 1179366
    add-int/lit8 v7, v0, 0x1

    move-object v0, p0

    .line 1179367
    invoke-direct/range {v0 .. v5}, LX/7Bf;->a(LX/7C0;JLandroid/database/sqlite/SQLiteStatement;Landroid/database/sqlite/SQLiteStatement;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v7

    .line 1179368
    :cond_0
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_1

    .line 1179369
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "INSERT INTO entities ("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/7Br;->b:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->c:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->d:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->e:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->f:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->g:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->h:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->i:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->j:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->k:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->l:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->m:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->n:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->o:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/7Br;->p:LX/0U1;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    move-object v0, v0

    .line 1179370
    move-object v6, v0

    goto/16 :goto_0

    .line 1179371
    :cond_2
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1179372
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1179373
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1179374
    return v0

    .line 1179375
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1179376
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1179377
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v0
.end method

.method private static a(LX/7C0;Landroid/database/sqlite/SQLiteStatement;Z)J
    .locals 10

    .prologue
    const-wide/16 v2, 0x1

    const-wide/16 v4, 0x0

    .line 1179521
    const/4 v0, 0x1

    .line 1179522
    iget-object v1, p0, LX/7C0;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1179523
    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179524
    const/4 v0, 0x2

    .line 1179525
    iget-object v1, p0, LX/7C0;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1179526
    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179527
    const/4 v0, 0x3

    .line 1179528
    iget-object v1, p0, LX/7C0;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1179529
    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179530
    const/4 v0, 0x4

    .line 1179531
    iget-object v1, p0, LX/7C0;->g:Ljava/lang/String;

    move-object v1, v1

    .line 1179532
    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179533
    const/4 v0, 0x5

    .line 1179534
    iget-object v1, p0, LX/7C0;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1179535
    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179536
    const/4 v0, 0x6

    .line 1179537
    iget-object v1, p0, LX/7C0;->e:Landroid/net/Uri;

    move-object v1, v1

    .line 1179538
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179539
    const/4 v0, 0x7

    .line 1179540
    iget-object v1, p0, LX/7C0;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v1, v1

    .line 1179541
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179542
    const/16 v0, 0x8

    .line 1179543
    iget-object v1, p0, LX/7C0;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v1, v1

    .line 1179544
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179545
    const/16 v6, 0x9

    .line 1179546
    iget-boolean v0, p0, LX/7C0;->k:Z

    move v0, v0

    .line 1179547
    if-eqz v0, :cond_1

    move-wide v0, v2

    :goto_0
    invoke-virtual {p1, v6, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1179548
    const/16 v0, 0xa

    .line 1179549
    iget-object v1, p0, LX/7C0;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-object v1, v1

    .line 1179550
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179551
    const/16 v0, 0xb

    .line 1179552
    iget-wide v8, p0, LX/7C0;->o:D

    move-wide v6, v8

    .line 1179553
    invoke-virtual {p1, v0, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindDouble(ID)V

    .line 1179554
    const/16 v6, 0xc

    .line 1179555
    iget-boolean v0, p0, LX/7C0;->h:Z

    move v0, v0

    .line 1179556
    if-eqz v0, :cond_2

    move-wide v0, v2

    :goto_1
    invoke-virtual {p1, v6, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1179557
    const/16 v0, 0xd

    .line 1179558
    iget-object v1, p0, LX/7C0;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object v1, v1

    .line 1179559
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179560
    const/16 v0, 0xe

    .line 1179561
    iget-object v1, p0, LX/7C0;->p:Ljava/lang/String;

    move-object v1, v1

    .line 1179562
    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179563
    const/16 v0, 0xf

    .line 1179564
    iget-boolean v1, p0, LX/7C0;->n:Z

    move v1, v1

    .line 1179565
    if-eqz v1, :cond_3

    :goto_2
    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1179566
    if-eqz p2, :cond_0

    .line 1179567
    const/16 v0, 0x10

    .line 1179568
    iget-object v1, p0, LX/7C0;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1179569
    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179570
    :cond_0
    const v0, 0x5d282a03

    invoke-static {v0}, LX/03h;->a(I)V

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v0

    const v2, 0x307b23ef

    invoke-static {v2}, LX/03h;->a(I)V

    return-wide v0

    :cond_1
    move-wide v0, v4

    .line 1179571
    goto :goto_0

    :cond_2
    move-wide v0, v4

    .line 1179572
    goto :goto_1

    :cond_3
    move-wide v2, v4

    .line 1179573
    goto :goto_2
.end method

.method public static a(LX/0QB;)LX/7Bf;
    .locals 7

    .prologue
    .line 1179508
    sget-object v0, LX/7Bf;->e:LX/7Bf;

    if-nez v0, :cond_1

    .line 1179509
    const-class v1, LX/7Bf;

    monitor-enter v1

    .line 1179510
    :try_start_0
    sget-object v0, LX/7Bf;->e:LX/7Bf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1179511
    if-eqz v2, :cond_0

    .line 1179512
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1179513
    new-instance p0, LX/7Bf;

    invoke-static {v0}, LX/7Bc;->a(LX/0QB;)LX/7Bc;

    move-result-object v3

    check-cast v3, LX/7Bc;

    invoke-static {v0}, LX/7Bi;->b(LX/0QB;)LX/7Bi;

    move-result-object v4

    check-cast v4, LX/7Bi;

    invoke-static {v0}, LX/7CV;->b(LX/0QB;)LX/7CV;

    move-result-object v5

    check-cast v5, LX/7CV;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5, v6}, LX/7Bf;-><init>(LX/7Bc;LX/7Bi;LX/7CV;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1179514
    move-object v0, p0

    .line 1179515
    sput-object v0, LX/7Bf;->e:LX/7Bf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1179516
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1179517
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1179518
    :cond_1
    sget-object v0, LX/7Bf;->e:LX/7Bf;

    return-object v0

    .line 1179519
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1179520
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/7C0;JLandroid/database/sqlite/SQLiteStatement;Landroid/database/sqlite/SQLiteStatement;)V
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1179493
    iget-object v0, p1, LX/7C0;->m:LX/0Px;

    move-object v3, v0

    .line 1179494
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1179495
    invoke-virtual {p4, v5, p2, p3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1179496
    invoke-static {p4, v6, v0}, LX/7Bf;->b(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179497
    const v0, -0x12214d4a

    invoke-static {v0}, LX/03h;->a(I)V

    invoke-virtual {p4}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    const v0, 0x4a219b5a    # 2647766.5f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1179498
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1179499
    :cond_0
    iget-object v0, p1, LX/7C0;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1179500
    if-nez v0, :cond_2

    .line 1179501
    :cond_1
    return-void

    .line 1179502
    :cond_2
    iget-object v2, p0, LX/7Bf;->c:LX/7CV;

    invoke-virtual {v2, v0}, LX/7CV;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1179503
    if-eqz v0, :cond_3

    .line 1179504
    invoke-virtual {p5, v5, p2, p3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1179505
    invoke-static {p5, v6, v0}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179506
    const v0, -0x17d20579

    invoke-static {v0}, LX/03h;->a(I)V

    invoke-virtual {p5}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    const v0, -0x352f9c89    # -6828475.5f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1179507
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method private static a(LX/7C2;Landroid/database/sqlite/SQLiteStatement;Landroid/database/sqlite/SQLiteStatement;)V
    .locals 10

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 1179465
    iget-object v0, p0, LX/7C2;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1179466
    invoke-static {p1, v6, v0}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179467
    iget-object v0, p0, LX/7C2;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1179468
    invoke-static {p1, v7, v0}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179469
    const/4 v0, 0x3

    .line 1179470
    iget-object v1, p0, LX/7C2;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1179471
    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179472
    const/4 v0, 0x4

    .line 1179473
    iget-object v1, p0, LX/7C2;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1179474
    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179475
    const/4 v0, 0x5

    .line 1179476
    iget-object v1, p0, LX/7C2;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1179477
    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179478
    const/4 v0, 0x6

    .line 1179479
    iget-wide v8, p0, LX/7C2;->g:D

    move-wide v2, v8

    .line 1179480
    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindDouble(ID)V

    .line 1179481
    const/4 v0, 0x7

    .line 1179482
    iget-object v1, p0, LX/7C2;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1179483
    invoke-static {p1, v0, v1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179484
    const v0, -0x7f59dc44

    invoke-static {v0}, LX/03h;->a(I)V

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v2

    const v0, -0x67ffa7ef

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1179485
    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    .line 1179486
    :cond_0
    return-void

    .line 1179487
    :cond_1
    iget-object v0, p0, LX/7C2;->f:LX/0Px;

    move-object v4, v0

    .line 1179488
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1179489
    invoke-virtual {p2, v6, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1179490
    invoke-static {p2, v7, v0}, LX/7Bf;->b(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179491
    const v0, -0x5205b03

    invoke-static {v0}, LX/03h;->a(I)V

    invoke-virtual {p2}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    const v0, -0xc19a0c5

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1179492
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "LX/0Px",
            "<",
            "LX/7C2;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1179455
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "INSERT INTO keywords ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/7Bv;->b:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/7Bv;->c:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/7Bv;->d:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/7Bv;->e:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/7Bv;->f:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/7Bv;->g:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/7Bv;->h:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") VALUES (?, ?, ?, ?, ?, ?, ?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    move-object v2, v0

    .line 1179456
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "INSERT INTO keywords_data ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/7Bt;->a:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/7Bt;->b:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") VALUES (?, ?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    move-object v3, v0

    .line 1179457
    :try_start_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7C2;

    .line 1179458
    invoke-static {v0, v2, v3}, LX/7Bf;->a(LX/7C2;Landroid/database/sqlite/SQLiteStatement;Landroid/database/sqlite/SQLiteStatement;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1179459
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1179460
    :cond_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1179461
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1179462
    return-void

    .line 1179463
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1179464
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1179451
    if-nez p2, :cond_0

    .line 1179452
    invoke-virtual {p0, p1}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 1179453
    :goto_0
    return-void

    .line 1179454
    :cond_0
    invoke-virtual {p0, p1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private static b(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1179580
    if-nez p2, :cond_0

    .line 1179581
    invoke-virtual {p0, p1}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 1179582
    :goto_0
    return-void

    .line 1179583
    :cond_0
    invoke-static {p2}, LX/1u4;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 1179584
    invoke-virtual {p0, p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/7C0;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1179444
    iget-object v0, p0, LX/7Bf;->a:LX/7Bc;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1179445
    const v0, -0x100924d1

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1179446
    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, v1, p1, v0}, LX/7Bf;->a(LX/7Bf;Landroid/database/sqlite/SQLiteDatabase;LX/0Px;Z)I

    move-result v0

    .line 1179447
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1179448
    const v2, 0x17066c4e

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1179449
    return v0

    .line 1179450
    :catchall_0
    move-exception v0

    const v2, -0x4db06024

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(LX/7By;)V
    .locals 4

    .prologue
    .line 1179431
    iget-object v0, p0, LX/7Bf;->a:LX/7Bc;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1179432
    const v0, -0x45b1417

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1179433
    :try_start_0
    const-string v0, "entities"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1179434
    const-string v0, "entities_data"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1179435
    const-string v0, "entities_phonetic_data"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1179436
    iget-object v0, p0, LX/7Bf;->b:LX/7Bi;

    sget-object v2, LX/7Bg;->a:LX/7Bh;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v2, v3}, LX/2Iu;->b(LX/0To;I)V

    .line 1179437
    iget-object v0, p1, LX/7By;->c:LX/0Px;

    move-object v0, v0

    .line 1179438
    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/7Bf;->a(LX/7Bf;Landroid/database/sqlite/SQLiteDatabase;LX/0Px;Z)I

    .line 1179439
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1179440
    const v0, -0x2be5ff88

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1179441
    iget-object v0, p0, LX/7Bf;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3Qm;->g:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1179442
    return-void

    .line 1179443
    :catchall_0
    move-exception v0

    const v2, 0x61d3fb00

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(LX/7C3;)V
    .locals 4

    .prologue
    .line 1179419
    iget-object v0, p0, LX/7Bf;->a:LX/7Bc;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1179420
    const v0, 0x78b5407a

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1179421
    :try_start_0
    const-string v0, "keywords"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1179422
    const-string v0, "keywords_data"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1179423
    iget-object v0, p0, LX/7Bf;->b:LX/7Bi;

    sget-object v2, LX/7Bg;->a:LX/7Bh;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v2, v3}, LX/2Iu;->b(LX/0To;I)V

    .line 1179424
    iget-object v0, p1, LX/7C3;->a:LX/0Px;

    move-object v0, v0

    .line 1179425
    invoke-static {v1, v0}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteDatabase;LX/0Px;)V

    .line 1179426
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1179427
    const v0, 0x11d6c818

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1179428
    iget-object v0, p0, LX/7Bf;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3Qm;->g:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1179429
    return-void

    .line 1179430
    :catchall_0
    move-exception v0

    const v2, -0x31005a09

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 3

    .prologue
    .line 1179411
    iget-object v0, p0, LX/7Bf;->a:LX/7Bc;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1179412
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UPDATE entities SET "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/7Br;->i:LX/0U1;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = ? WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/7Br;->b:LX/0U1;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    move-object v1, v1

    .line 1179413
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179414
    const/4 v0, 0x2

    invoke-static {v1, v0, p1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179415
    const v0, 0x7bae6b82

    invoke-static {v0}, LX/03h;->a(I)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    const v0, 0x3d078212

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1179416
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1179417
    return-void

    .line 1179418
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 3

    .prologue
    .line 1179403
    iget-object v0, p0, LX/7Bf;->a:LX/7Bc;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1179404
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UPDATE entities SET "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/7Br;->k:LX/0U1;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = ? WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/7Br;->b:LX/0U1;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    move-object v1, v1

    .line 1179405
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179406
    const/4 v0, 0x2

    invoke-static {v1, v0, p1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179407
    const v0, -0x3eae2a5

    invoke-static {v0}, LX/03h;->a(I)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    const v0, 0x41130c11    # 9.190446f

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1179408
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1179409
    return-void

    .line 1179410
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 1179394
    iget-object v0, p0, LX/7Bf;->a:LX/7Bc;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1179395
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UPDATE entities SET "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/7Br;->j:LX/0U1;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = ? WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/7Br;->b:LX/0U1;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    move-object v2, v1

    .line 1179396
    const/4 v3, 0x1

    if-eqz p2, :cond_0

    const-wide/16 v0, 0x1

    :goto_0
    :try_start_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1179397
    const/4 v0, 0x2

    invoke-static {v2, v0, p1}, LX/7Bf;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 1179398
    const v0, -0x3c0e0647

    invoke-static {v0}, LX/03h;->a(I)V

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    const v0, 0x7811ea25    # 1.1838001E34f

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1179399
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1179400
    return-void

    .line 1179401
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 1179402
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v0
.end method

.method public final b(LX/7By;)V
    .locals 8

    .prologue
    .line 1179378
    iget-object v0, p0, LX/7Bf;->a:LX/7Bc;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1179379
    const v0, 0x47c565cb

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1179380
    :try_start_0
    iget-object v0, p1, LX/7By;->c:LX/0Px;

    move-object v0, v0

    .line 1179381
    const/4 v1, 0x1

    invoke-static {p0, v2, v0, v1}, LX/7Bf;->a(LX/7Bf;Landroid/database/sqlite/SQLiteDatabase;LX/0Px;Z)I

    .line 1179382
    iget-object v0, p1, LX/7By;->d:LX/0Px;

    move-object v3, v0

    .line 1179383
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1179384
    const/4 p1, 0x1

    const/4 p0, 0x0

    .line 1179385
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DELETE FROM entities_data WHERE "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, LX/7Bn;->a:LX/0U1;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " IN (SELECT "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, LX/7Br;->a:LX/0U1;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " FROM entities"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " WHERE "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, LX/7Br;->b:LX/0U1;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " = ? )"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 1179386
    new-array v6, p1, [Ljava/lang/String;

    aput-object v0, v6, p0

    const v7, 0x683f0fc8

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    const v5, -0x368fe603

    invoke-static {v5}, LX/03h;->a(I)V

    .line 1179387
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DELETE FROM entities WHERE "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, LX/7Br;->b:LX/0U1;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " = ? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 1179388
    new-array v6, p1, [Ljava/lang/String;

    aput-object v0, v6, p0

    const v7, 0x75ec4991

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    const v5, -0x60a5e04e

    invoke-static {v5}, LX/03h;->a(I)V

    .line 1179389
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 1179390
    :cond_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1179391
    const v0, 0x9e2d5fb

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1179392
    return-void

    .line 1179393
    :catchall_0
    move-exception v0

    const v1, -0x3e8a70e7

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
