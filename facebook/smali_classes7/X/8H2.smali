.class public LX/8H2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/data/method/DeletePhotoParams;",
        "Lcom/facebook/photos/data/method/DeletePhotoResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320390
    const-class v0, LX/8H2;

    sput-object v0, LX/8H2;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320392
    return-void
.end method

.method public static a(LX/0QB;)LX/8H2;
    .locals 1

    .prologue
    .line 1320393
    new-instance v0, LX/8H2;

    invoke-direct {v0}, LX/8H2;-><init>()V

    .line 1320394
    move-object v0, v0

    .line 1320395
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1320396
    check-cast p1, Lcom/facebook/photos/data/method/DeletePhotoParams;

    .line 1320397
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1320398
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320399
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    sget-object v2, LX/8H2;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1320400
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1320401
    move-object v1, v1

    .line 1320402
    const-string v2, "DELETE"

    .line 1320403
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1320404
    move-object v1, v1

    .line 1320405
    iget-object v2, p1, Lcom/facebook/photos/data/method/DeletePhotoParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1320406
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1320407
    move-object v1, v1

    .line 1320408
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1320409
    move-object v0, v1

    .line 1320410
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1320411
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1320412
    move-object v0, v0

    .line 1320413
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1320414
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1320415
    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1320416
    new-instance v1, Lcom/facebook/photos/data/method/DeletePhotoResponse;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v1, v0}, Lcom/facebook/photos/data/method/DeletePhotoResponse;-><init>(Z)V

    return-object v1
.end method
