.class public LX/6nP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/6nR;


# direct methods
.method public constructor <init>(LX/6nR;)V
    .locals 0

    .prologue
    .line 1147312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1147313
    iput-object p1, p0, LX/6nP;->a:LX/6nR;

    .line 1147314
    return-void
.end method

.method public static a(Ljava/util/List;)LX/0Px;
    .locals 5
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1147315
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1147316
    if-nez p0, :cond_0

    .line 1147317
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1147318
    :goto_0
    return-object v0

    .line 1147319
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1147320
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1147321
    const-string v4, "--"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1147322
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1147323
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 1147324
    :cond_1
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1147325
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
