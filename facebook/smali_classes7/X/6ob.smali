.class public final LX/6ob;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;)V
    .locals 0

    .prologue
    .line 1148266
    iput-object p1, p0, LX/6ob;->a:Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1148267
    sget-object v0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->a:Ljava/lang/Class;

    const-string v1, "Fetch of payment pin failed."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1148268
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1148269
    check-cast p1, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1148270
    invoke-virtual {p1}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6ob;->a:Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->g:LX/6oc;

    if-eqz v0, :cond_0

    .line 1148271
    iget-object v0, p0, LX/6ob;->a:Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->g:LX/6oc;

    invoke-interface {v0}, LX/6oc;->a()V

    .line 1148272
    :cond_0
    return-void
.end method
