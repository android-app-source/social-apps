.class public final LX/849;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:J

.field public final synthetic c:LX/3UJ;


# direct methods
.method public constructor <init>(LX/3UJ;ZJ)V
    .locals 1

    .prologue
    .line 1290810
    iput-object p1, p0, LX/849;->c:LX/3UJ;

    iput-boolean p2, p0, LX/849;->a:Z

    iput-wide p3, p0, LX/849;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1290806
    const/4 v5, 0x0

    .line 1290807
    iget-boolean v0, p0, LX/849;->a:Z

    if-eqz v0, :cond_0

    .line 1290808
    iget-object v0, p0, LX/849;->c:LX/3UJ;

    iget-object v1, v0, LX/3UJ;->a:LX/2dj;

    iget-wide v2, p0, LX/849;->b:J

    sget-object v4, LX/2h8;->SUGGESTION:LX/2h8;

    move-object v6, v5

    invoke-virtual/range {v1 .. v6}, LX/2dj;->b(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1290809
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/849;->c:LX/3UJ;

    iget-object v0, v0, LX/3UJ;->a:LX/2dj;

    iget-wide v2, p0, LX/849;->b:J

    invoke-virtual {v0, v2, v3}, LX/2dj;->a(J)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
