.class public LX/7xe;
.super LX/62U;
.source ""


# instance fields
.field private final m:Lcom/facebook/resources/ui/FbTextView;

.field private final n:Lcom/facebook/resources/ui/FbTextView;

.field private final o:Lcom/facebook/resources/ui/FbTextView;

.field public final p:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 1277673
    invoke-direct {p0, p1}, LX/62U;-><init>(Landroid/view/View;)V

    .line 1277674
    const v0, 0x7f0d0e7b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7xe;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 1277675
    const v0, 0x7f0d0e7c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7xe;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1277676
    const v0, 0x7f0d0e7d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7xe;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 1277677
    const v0, 0x7f0d0e7e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1277678
    const v1, 0x7f0304a1

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1277679
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    iput-object v0, p0, LX/7xe;->p:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    .line 1277680
    iget-object v0, p0, LX/7xe;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081fa0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277681
    iget-object v0, p0, LX/7xe;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277682
    iget-object v0, p0, LX/7xe;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277683
    return-void
.end method
