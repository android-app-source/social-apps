.class public final enum LX/8Ta;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Ta;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Ta;

.field public static final enum Group:LX/8Ta;

.field public static final enum None:LX/8Ta;

.field public static final enum Story:LX/8Ta;

.field public static final enum Thread:LX/8Ta;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1348798
    new-instance v0, LX/8Ta;

    const-string v1, "Thread"

    invoke-direct {v0, v1, v2}, LX/8Ta;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Ta;->Thread:LX/8Ta;

    .line 1348799
    new-instance v0, LX/8Ta;

    const-string v1, "Story"

    invoke-direct {v0, v1, v3}, LX/8Ta;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Ta;->Story:LX/8Ta;

    .line 1348800
    new-instance v0, LX/8Ta;

    const-string v1, "Group"

    invoke-direct {v0, v1, v4}, LX/8Ta;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Ta;->Group:LX/8Ta;

    .line 1348801
    new-instance v0, LX/8Ta;

    const-string v1, "None"

    invoke-direct {v0, v1, v5}, LX/8Ta;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Ta;->None:LX/8Ta;

    .line 1348802
    const/4 v0, 0x4

    new-array v0, v0, [LX/8Ta;

    sget-object v1, LX/8Ta;->Thread:LX/8Ta;

    aput-object v1, v0, v2

    sget-object v1, LX/8Ta;->Story:LX/8Ta;

    aput-object v1, v0, v3

    sget-object v1, LX/8Ta;->Group:LX/8Ta;

    aput-object v1, v0, v4

    sget-object v1, LX/8Ta;->None:LX/8Ta;

    aput-object v1, v0, v5

    sput-object v0, LX/8Ta;->$VALUES:[LX/8Ta;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1348803
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Ta;
    .locals 1

    .prologue
    .line 1348804
    const-class v0, LX/8Ta;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Ta;

    return-object v0
.end method

.method public static values()[LX/8Ta;
    .locals 1

    .prologue
    .line 1348805
    sget-object v0, LX/8Ta;->$VALUES:[LX/8Ta;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Ta;

    return-object v0
.end method
