.class public final enum LX/6xk;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6LU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6xk;",
        ">;",
        "LX/6LU",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6xk;

.field public static final enum AMOUNT:LX/6xk;

.field public static final enum CONTACT_NAME:LX/6xk;

.field public static final enum CONTACT_NUMBER_ID:LX/6xk;

.field public static final enum CREDENTIAL_ID:LX/6xk;

.field public static final enum CSC:LX/6xk;

.field public static final enum CURRENCY:LX/6xk;

.field public static final enum EMAIL_ADDRESS_ID:LX/6xk;

.field public static final enum EXTRA_DATA:LX/6xk;

.field public static final enum MAILING_ADDRESS_ID:LX/6xk;

.field public static final enum MERCHANT_DESCRIPTOR:LX/6xk;

.field public static final enum NMOR_PAYMENT_METHOD:LX/6xk;

.field public static final enum ORDER_ID:LX/6xk;

.field public static final enum PAYMENT_TYPE:LX/6xk;

.field public static final enum RECEIVER_ID:LX/6xk;

.field public static final enum REQUEST_ID:LX/6xk;

.field public static final enum SECURITY_BIOMETRIC_NONCE:LX/6xk;

.field public static final enum SECURITY_DEVICE_ID:LX/6xk;

.field public static final enum SECURITY_PIN:LX/6xk;

.field public static final enum SHIPPING_OPTION_ID:LX/6xk;

.field public static final enum TAX_AMOUNT:LX/6xk;

.field public static final enum TAX_CURRENCY:LX/6xk;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1159134
    new-instance v0, LX/6xk;

    const-string v1, "AMOUNT"

    invoke-direct {v0, v1, v3}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->AMOUNT:LX/6xk;

    .line 1159135
    new-instance v0, LX/6xk;

    const-string v1, "CONTACT_NAME"

    invoke-direct {v0, v1, v4}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->CONTACT_NAME:LX/6xk;

    .line 1159136
    new-instance v0, LX/6xk;

    const-string v1, "CONTACT_NUMBER_ID"

    invoke-direct {v0, v1, v5}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->CONTACT_NUMBER_ID:LX/6xk;

    .line 1159137
    new-instance v0, LX/6xk;

    const-string v1, "CREDENTIAL_ID"

    invoke-direct {v0, v1, v6}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->CREDENTIAL_ID:LX/6xk;

    .line 1159138
    new-instance v0, LX/6xk;

    const-string v1, "CSC"

    invoke-direct {v0, v1, v7}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->CSC:LX/6xk;

    .line 1159139
    new-instance v0, LX/6xk;

    const-string v1, "CURRENCY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->CURRENCY:LX/6xk;

    .line 1159140
    new-instance v0, LX/6xk;

    const-string v1, "EMAIL_ADDRESS_ID"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->EMAIL_ADDRESS_ID:LX/6xk;

    .line 1159141
    new-instance v0, LX/6xk;

    const-string v1, "EXTRA_DATA"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->EXTRA_DATA:LX/6xk;

    .line 1159142
    new-instance v0, LX/6xk;

    const-string v1, "MAILING_ADDRESS_ID"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->MAILING_ADDRESS_ID:LX/6xk;

    .line 1159143
    new-instance v0, LX/6xk;

    const-string v1, "MERCHANT_DESCRIPTOR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->MERCHANT_DESCRIPTOR:LX/6xk;

    .line 1159144
    new-instance v0, LX/6xk;

    const-string v1, "NMOR_PAYMENT_METHOD"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->NMOR_PAYMENT_METHOD:LX/6xk;

    .line 1159145
    new-instance v0, LX/6xk;

    const-string v1, "ORDER_ID"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->ORDER_ID:LX/6xk;

    .line 1159146
    new-instance v0, LX/6xk;

    const-string v1, "PAYMENT_TYPE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->PAYMENT_TYPE:LX/6xk;

    .line 1159147
    new-instance v0, LX/6xk;

    const-string v1, "RECEIVER_ID"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->RECEIVER_ID:LX/6xk;

    .line 1159148
    new-instance v0, LX/6xk;

    const-string v1, "REQUEST_ID"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->REQUEST_ID:LX/6xk;

    .line 1159149
    new-instance v0, LX/6xk;

    const-string v1, "SECURITY_BIOMETRIC_NONCE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->SECURITY_BIOMETRIC_NONCE:LX/6xk;

    .line 1159150
    new-instance v0, LX/6xk;

    const-string v1, "SECURITY_DEVICE_ID"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->SECURITY_DEVICE_ID:LX/6xk;

    .line 1159151
    new-instance v0, LX/6xk;

    const-string v1, "SECURITY_PIN"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->SECURITY_PIN:LX/6xk;

    .line 1159152
    new-instance v0, LX/6xk;

    const-string v1, "SHIPPING_OPTION_ID"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->SHIPPING_OPTION_ID:LX/6xk;

    .line 1159153
    new-instance v0, LX/6xk;

    const-string v1, "TAX_AMOUNT"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->TAX_AMOUNT:LX/6xk;

    .line 1159154
    new-instance v0, LX/6xk;

    const-string v1, "TAX_CURRENCY"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/6xk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xk;->TAX_CURRENCY:LX/6xk;

    .line 1159155
    const/16 v0, 0x15

    new-array v0, v0, [LX/6xk;

    sget-object v1, LX/6xk;->AMOUNT:LX/6xk;

    aput-object v1, v0, v3

    sget-object v1, LX/6xk;->CONTACT_NAME:LX/6xk;

    aput-object v1, v0, v4

    sget-object v1, LX/6xk;->CONTACT_NUMBER_ID:LX/6xk;

    aput-object v1, v0, v5

    sget-object v1, LX/6xk;->CREDENTIAL_ID:LX/6xk;

    aput-object v1, v0, v6

    sget-object v1, LX/6xk;->CSC:LX/6xk;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6xk;->CURRENCY:LX/6xk;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6xk;->EMAIL_ADDRESS_ID:LX/6xk;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6xk;->EXTRA_DATA:LX/6xk;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6xk;->MAILING_ADDRESS_ID:LX/6xk;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6xk;->MERCHANT_DESCRIPTOR:LX/6xk;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6xk;->NMOR_PAYMENT_METHOD:LX/6xk;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/6xk;->ORDER_ID:LX/6xk;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/6xk;->PAYMENT_TYPE:LX/6xk;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/6xk;->RECEIVER_ID:LX/6xk;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/6xk;->REQUEST_ID:LX/6xk;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/6xk;->SECURITY_BIOMETRIC_NONCE:LX/6xk;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/6xk;->SECURITY_DEVICE_ID:LX/6xk;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/6xk;->SECURITY_PIN:LX/6xk;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/6xk;->SHIPPING_OPTION_ID:LX/6xk;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/6xk;->TAX_AMOUNT:LX/6xk;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/6xk;->TAX_CURRENCY:LX/6xk;

    aput-object v2, v0, v1

    sput-object v0, LX/6xk;->$VALUES:[LX/6xk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1159133
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6xk;
    .locals 1

    .prologue
    .line 1159128
    const-class v0, LX/6xk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xk;

    return-object v0
.end method

.method public static values()[LX/6xk;
    .locals 1

    .prologue
    .line 1159132
    sget-object v0, LX/6xk;->$VALUES:[LX/6xk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6xk;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1159131
    invoke-virtual {p0}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1159130
    invoke-virtual {p0}, LX/6xk;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1159129
    invoke-virtual {p0}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
