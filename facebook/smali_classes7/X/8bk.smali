.class public LX/8bk;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1372286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1372287
    return-void
.end method

.method public static a(LX/15i;I)Lcom/facebook/user/model/Name;
    .locals 13
    .param p0    # LX/15i;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # I
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "convert"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v12, 0x1

    const/4 v3, 0x0

    const/4 v11, 0x0

    .line 1372288
    const v0, -0x79163e77

    const-string v1, "The phoneticName cannot be null."

    invoke-static {p0, p1, v0, v1}, LX/3Sm;->a(LX/15i;IILjava/lang/Object;)LX/1vs;

    .line 1372289
    invoke-virtual {p0, p1, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 1372290
    const v0, -0x5c2ca6a6

    invoke-static {p0, p1, v11, v0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 1372291
    :goto_0
    if-nez v5, :cond_1

    .line 1372292
    invoke-static {}, Lcom/facebook/user/model/Name;->k()Lcom/facebook/user/model/Name;

    move-result-object v0

    .line 1372293
    :goto_1
    return-object v0

    .line 1372294
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 1372295
    :cond_1
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v6

    move-object v2, v3

    move-object v4, v3

    :goto_2
    invoke-interface {v6}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1372296
    invoke-virtual {v1, v0, v12}, LX/15i;->j(II)I

    move-result v7

    .line 1372297
    invoke-virtual {v1, v0, v11}, LX/15i;->j(II)I

    move-result v8

    .line 1372298
    const/4 v9, 0x2

    const-class v10, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-virtual {v1, v0, v9, v10, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    .line 1372299
    invoke-virtual {v5, v11, v7}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v1

    .line 1372300
    invoke-virtual {v5, v1, v8}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v7

    .line 1372301
    invoke-virtual {v5, v1, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1372302
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->FIRST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-static {v0, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    move-object v4, v1

    .line 1372303
    goto :goto_2

    .line 1372304
    :cond_2
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->LAST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-static {v0, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v1

    :goto_3
    move-object v2, v0

    .line 1372305
    goto :goto_2

    .line 1372306
    :cond_3
    new-instance v0, Lcom/facebook/user/model/Name;

    invoke-direct {v0, v4, v2, v5}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    move-object v0, v2

    goto :goto_3
.end method
