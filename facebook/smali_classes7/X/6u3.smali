.class public LX/6u3;
.super LX/0QF;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PARAM::",
        "Landroid/os/Parcelable;",
        "RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "LX/0QF",
        "<TPARAM;TRESU",
        "LT;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0QJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QJ",
            "<TPARAM;TRESU",
            "LT;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/6sU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/6sU",
            "<TPARAM;TRESU",
            "LT;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0TD;

.field private final d:LX/0Sh;


# direct methods
.method public constructor <init>(LX/6sU;LX/0QN;LX/0TD;LX/0Sh;)V
    .locals 1
    .param p1    # LX/6sU;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0QN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6sU",
            "<TPARAM;TRESU",
            "LT;",
            ">;",
            "LX/0QN",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;",
            "LX/0TD;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1155459
    invoke-direct {p0}, LX/0QF;-><init>()V

    .line 1155460
    iput-object p1, p0, LX/6u3;->b:LX/6sU;

    .line 1155461
    iput-object p3, p0, LX/6u3;->c:LX/0TD;

    .line 1155462
    iput-object p4, p0, LX/6u3;->d:LX/0Sh;

    .line 1155463
    if-nez p2, :cond_0

    .line 1155464
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object p2

    .line 1155465
    :cond_0
    new-instance v0, LX/6u1;

    invoke-direct {v0, p0}, LX/6u1;-><init>(LX/6u3;)V

    invoke-virtual {p2, v0}, LX/0QN;->a(LX/0QM;)LX/0QJ;

    move-result-object v0

    iput-object v0, p0, LX/6u3;->a:LX/0QJ;

    .line 1155466
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcelable;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAM;)TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 1155467
    iget-object v0, p0, LX/6u3;->d:LX/0Sh;

    const-string v1, "Calling this method may trigger synchronous loading, so it should not be done on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 1155468
    invoke-super {p0, p1}, LX/0QF;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAM;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1155469
    invoke-virtual {p0, p1}, LX/0QG;->b(Ljava/lang/Object;)V

    .line 1155470
    invoke-virtual {p0, p1}, LX/6u3;->c(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAM;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1155471
    invoke-virtual {p0, p1}, LX/0QG;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1155472
    if-eqz v0, :cond_0

    .line 1155473
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1155474
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6u3;->c:LX/0TD;

    new-instance v1, LX/6u2;

    invoke-direct {v1, p0, p1}, LX/6u2;-><init>(LX/6u3;Landroid/os/Parcelable;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1155475
    check-cast p1, Landroid/os/Parcelable;

    invoke-virtual {p0, p1}, LX/6u3;->a(Landroid/os/Parcelable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()LX/0QI;
    .locals 1

    .prologue
    .line 1155476
    invoke-virtual {p0}, LX/6u3;->f()LX/0QJ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1155477
    invoke-virtual {p0}, LX/6u3;->f()LX/0QJ;

    move-result-object v0

    return-object v0
.end method

.method public final f()LX/0QJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QJ",
            "<TPARAM;TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1155478
    iget-object v0, p0, LX/6u3;->a:LX/0QJ;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1155479
    const-string v0, "Cached(%s)"

    iget-object v1, p0, LX/6u3;->b:LX/6sU;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
