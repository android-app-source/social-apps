.class public final enum LX/8GI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8GI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8GI;

.field public static final enum FILTER_TO_FILTER:LX/8GI;

.field public static final enum FILTER_TO_FRAME:LX/8GI;

.field public static final enum FRAME_TO_FILTER:LX/8GI;

.field public static final enum FRAME_TO_FRAME:LX/8GI;

.field public static final enum NONE:LX/8GI;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1318765
    new-instance v0, LX/8GI;

    const-string v1, "FRAME_TO_FRAME"

    invoke-direct {v0, v1, v2}, LX/8GI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8GI;->FRAME_TO_FRAME:LX/8GI;

    .line 1318766
    new-instance v0, LX/8GI;

    const-string v1, "FILTER_TO_FILTER"

    invoke-direct {v0, v1, v3}, LX/8GI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8GI;->FILTER_TO_FILTER:LX/8GI;

    .line 1318767
    new-instance v0, LX/8GI;

    const-string v1, "FRAME_TO_FILTER"

    invoke-direct {v0, v1, v4}, LX/8GI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8GI;->FRAME_TO_FILTER:LX/8GI;

    .line 1318768
    new-instance v0, LX/8GI;

    const-string v1, "FILTER_TO_FRAME"

    invoke-direct {v0, v1, v5}, LX/8GI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8GI;->FILTER_TO_FRAME:LX/8GI;

    .line 1318769
    new-instance v0, LX/8GI;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v6}, LX/8GI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8GI;->NONE:LX/8GI;

    .line 1318770
    const/4 v0, 0x5

    new-array v0, v0, [LX/8GI;

    sget-object v1, LX/8GI;->FRAME_TO_FRAME:LX/8GI;

    aput-object v1, v0, v2

    sget-object v1, LX/8GI;->FILTER_TO_FILTER:LX/8GI;

    aput-object v1, v0, v3

    sget-object v1, LX/8GI;->FRAME_TO_FILTER:LX/8GI;

    aput-object v1, v0, v4

    sget-object v1, LX/8GI;->FILTER_TO_FRAME:LX/8GI;

    aput-object v1, v0, v5

    sget-object v1, LX/8GI;->NONE:LX/8GI;

    aput-object v1, v0, v6

    sput-object v0, LX/8GI;->$VALUES:[LX/8GI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1318771
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getType(LX/5jK;LX/5iG;LX/5iG;LX/5iG;)LX/8GI;
    .locals 1

    .prologue
    .line 1318772
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1318773
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1318774
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1318775
    invoke-virtual {p0}, LX/5jK;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1318776
    invoke-virtual {p1}, LX/5iG;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1318777
    invoke-virtual {p2}, LX/5iG;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1318778
    sget-object v0, LX/8GI;->FRAME_TO_FRAME:LX/8GI;

    .line 1318779
    :goto_0
    return-object v0

    .line 1318780
    :cond_0
    sget-object v0, LX/8GI;->FILTER_TO_FRAME:LX/8GI;

    goto :goto_0

    .line 1318781
    :cond_1
    invoke-virtual {p2}, LX/5iG;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1318782
    sget-object v0, LX/8GI;->FRAME_TO_FILTER:LX/8GI;

    goto :goto_0

    .line 1318783
    :cond_2
    sget-object v0, LX/8GI;->FILTER_TO_FILTER:LX/8GI;

    goto :goto_0

    .line 1318784
    :cond_3
    invoke-virtual {p0}, LX/5jK;->d()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1318785
    invoke-virtual {p3}, LX/5iG;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1318786
    invoke-virtual {p2}, LX/5iG;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1318787
    sget-object v0, LX/8GI;->FRAME_TO_FRAME:LX/8GI;

    goto :goto_0

    .line 1318788
    :cond_4
    sget-object v0, LX/8GI;->FILTER_TO_FRAME:LX/8GI;

    goto :goto_0

    .line 1318789
    :cond_5
    invoke-virtual {p2}, LX/5iG;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1318790
    sget-object v0, LX/8GI;->FRAME_TO_FILTER:LX/8GI;

    goto :goto_0

    .line 1318791
    :cond_6
    sget-object v0, LX/8GI;->FILTER_TO_FILTER:LX/8GI;

    goto :goto_0

    .line 1318792
    :cond_7
    sget-object v0, LX/8GI;->NONE:LX/8GI;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/8GI;
    .locals 1

    .prologue
    .line 1318793
    const-class v0, LX/8GI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8GI;

    return-object v0
.end method

.method public static values()[LX/8GI;
    .locals 1

    .prologue
    .line 1318794
    sget-object v0, LX/8GI;->$VALUES:[LX/8GI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8GI;

    return-object v0
.end method
