.class public final LX/725;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wC;


# instance fields
.field private final a:LX/712;


# direct methods
.method public constructor <init>(LX/712;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1163898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163899
    iput-object p1, p0, LX/725;->a:LX/712;

    .line 1163900
    return-void
.end method

.method public static b(LX/0QB;)LX/725;
    .locals 2

    .prologue
    .line 1163919
    new-instance v1, LX/725;

    invoke-static {p0}, LX/712;->a(LX/0QB;)LX/712;

    move-result-object v0

    check-cast v0, LX/712;

    invoke-direct {v1, v0}, LX/725;-><init>(LX/712;)V

    .line 1163920
    return-object v1
.end method


# virtual methods
.method public final a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1163901
    sget-object v0, LX/724;->a:[I

    invoke-interface {p2}, LX/6vm;->a()LX/71I;

    move-result-object v1

    invoke-virtual {v1}, LX/71I;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1163902
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal row type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, LX/6vm;->a()LX/71I;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1163903
    :pswitch_0
    check-cast p2, LX/72c;

    .line 1163904
    if-nez p3, :cond_0

    new-instance p3, LX/71v;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/71v;-><init>(Landroid/content/Context;)V

    .line 1163905
    :goto_0
    invoke-virtual {p3, p1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1163906
    invoke-virtual {p3, p2}, LX/71v;->a(LX/72c;)V

    .line 1163907
    move-object v0, p3

    .line 1163908
    :goto_1
    return-object v0

    .line 1163909
    :pswitch_1
    check-cast p2, LX/71o;

    .line 1163910
    if-nez p3, :cond_1

    new-instance p3, LX/71p;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/71p;-><init>(Landroid/content/Context;)V

    .line 1163911
    :goto_2
    iput-object p1, p3, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a:LX/6qh;

    .line 1163912
    iput-object p2, p3, LX/71p;->a:LX/71o;

    .line 1163913
    invoke-virtual {p3, p3}, LX/71p;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1163914
    move-object v0, p3

    .line 1163915
    goto :goto_1

    .line 1163916
    :pswitch_2
    iget-object v0, p0, LX/725;->a:LX/712;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/712;->a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 1163917
    :cond_0
    check-cast p3, LX/71v;

    goto :goto_0

    .line 1163918
    :cond_1
    check-cast p3, LX/71p;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
