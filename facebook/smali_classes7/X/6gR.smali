.class public final LX/6gR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1125174
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1125175
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125176
    :goto_0
    return v1

    .line 1125177
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125178
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1125179
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1125180
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1125181
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1125182
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1125183
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1125184
    :cond_2
    const-string v5, "section_title"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1125185
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1125186
    :cond_3
    const-string v5, "section_units"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1125187
    const/4 v4, 0x0

    .line 1125188
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_c

    .line 1125189
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125190
    :goto_2
    move v0, v4

    .line 1125191
    goto :goto_1

    .line 1125192
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1125193
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1125194
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1125195
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1125196
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 1125197
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125198
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_b

    .line 1125199
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1125200
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1125201
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_7

    if-eqz v7, :cond_7

    .line 1125202
    const-string v8, "edges"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1125203
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1125204
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_8

    .line 1125205
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_8

    .line 1125206
    const/4 v8, 0x0

    .line 1125207
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_10

    .line 1125208
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125209
    :goto_5
    move v7, v8

    .line 1125210
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1125211
    :cond_8
    invoke-static {v6, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 1125212
    goto :goto_3

    .line 1125213
    :cond_9
    const-string v8, "order_token"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1125214
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 1125215
    :cond_a
    const-string v8, "page_info"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1125216
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1125217
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v9, :cond_16

    .line 1125218
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125219
    :goto_6
    move v0, v7

    .line 1125220
    goto :goto_3

    .line 1125221
    :cond_b
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1125222
    invoke-virtual {p1, v4, v6}, LX/186;->b(II)V

    .line 1125223
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v5}, LX/186;->b(II)V

    .line 1125224
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1125225
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_2

    :cond_c
    move v0, v4

    move v5, v4

    move v6, v4

    goto/16 :goto_3

    .line 1125226
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125227
    :cond_e
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_f

    .line 1125228
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1125229
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1125230
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_e

    if-eqz v9, :cond_e

    .line 1125231
    const-string v10, "node"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 1125232
    invoke-static {p0, p1}, LX/6gQ;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_7

    .line 1125233
    :cond_f
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1125234
    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1125235
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_5

    :cond_10
    move v7, v8

    goto :goto_7

    .line 1125236
    :cond_11
    const-string v12, "has_next_page"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_13

    .line 1125237
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v9, v0

    move v0, v8

    .line 1125238
    :cond_12
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_14

    .line 1125239
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1125240
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1125241
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_12

    if-eqz v11, :cond_12

    .line 1125242
    const-string v12, "end_cursor"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_11

    .line 1125243
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_8

    .line 1125244
    :cond_13
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_8

    .line 1125245
    :cond_14
    const/4 v11, 0x2

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1125246
    invoke-virtual {p1, v7, v10}, LX/186;->b(II)V

    .line 1125247
    if-eqz v0, :cond_15

    .line 1125248
    invoke-virtual {p1, v8, v9}, LX/186;->a(IZ)V

    .line 1125249
    :cond_15
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_6

    :cond_16
    move v0, v7

    move v9, v7

    move v10, v7

    goto :goto_8
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1125250
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1125251
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1125252
    if-eqz v0, :cond_0

    .line 1125253
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125254
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1125255
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1125256
    if-eqz v0, :cond_1

    .line 1125257
    const-string v1, "section_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125258
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1125259
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125260
    if-eqz v0, :cond_9

    .line 1125261
    const-string v1, "section_units"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125262
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1125263
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1125264
    if-eqz v1, :cond_4

    .line 1125265
    const-string v2, "edges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125266
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1125267
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 1125268
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    .line 1125269
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1125270
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1125271
    if-eqz v4, :cond_2

    .line 1125272
    const-string p1, "node"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125273
    invoke-static {p0, v4, p2, p3}, LX/6gQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1125274
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1125275
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1125276
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1125277
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1125278
    if-eqz v1, :cond_5

    .line 1125279
    const-string v2, "order_token"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125280
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1125281
    :cond_5
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1125282
    if-eqz v1, :cond_8

    .line 1125283
    const-string v2, "page_info"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125284
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1125285
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1125286
    if-eqz v2, :cond_6

    .line 1125287
    const-string v3, "end_cursor"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125288
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1125289
    :cond_6
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1125290
    if-eqz v2, :cond_7

    .line 1125291
    const-string v3, "has_next_page"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125292
    invoke-virtual {p2, v2}, LX/0nX;->a(Z)V

    .line 1125293
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1125294
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1125295
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1125296
    return-void
.end method
