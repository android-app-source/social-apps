.class public final LX/7Oh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Di;


# instance fields
.field public final a:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/3Dd;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/io/OutputStream;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1201989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1201990
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/7Oh;->a:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1201991
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/7Oh;->b:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1201992
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/7Oh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1201993
    return-void
.end method


# virtual methods
.method public final a(LX/3Dd;)Ljava/io/OutputStream;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 1201978
    iget-object v0, p0, LX/7Oh;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v2, 0x392f19a6

    invoke-static {v0, p1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1201979
    :try_start_0
    iget-object v0, p0, LX/7Oh;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const-wide/16 v2, 0xc8

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v2, v3, v4}, LX/0Sa;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1201980
    :goto_0
    return-object v0

    .line 1201981
    :catch_0
    move-exception v0

    .line 1201982
    sget-object v2, LX/7Oj;->a:Ljava/lang/String;

    const-string v3, "This should not happen! Exception is never set on this future"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1201983
    iget-object v0, p0, LX/7Oh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const v3, 0x533caa9e

    invoke-static {v0, v2, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    move-object v0, v1

    .line 1201984
    goto :goto_0

    .line 1201985
    :catch_1
    move-exception v0

    .line 1201986
    sget-object v2, LX/7Oj;->a:Ljava/lang/String;

    const-string v3, "writeTo was not called on time after getMetadata!"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1201987
    iget-object v0, p0, LX/7Oh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const v3, -0x68e52979

    invoke-static {v0, v2, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    move-object v0, v1

    .line 1201988
    goto :goto_0
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 1

    .prologue
    .line 1201994
    iget-object v0, p0, LX/7Oh;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1201995
    iget-object v0, p0, LX/7Oh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1201996
    return-void
.end method

.method public final a(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 1201975
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1201976
    iget-object v0, p0, LX/7Oh;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x21995ab8

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1201977
    return-void
.end method

.method public final a(Ljava/io/OutputStream;Ljava/io/IOException;)V
    .locals 3

    .prologue
    .line 1201971
    if-eqz p2, :cond_0

    .line 1201972
    iget-object v0, p0, LX/7Oh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p2}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1201973
    :goto_0
    return-void

    .line 1201974
    :cond_0
    iget-object v0, p0, LX/7Oh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const v2, 0x32ff886b

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1201970
    iget-object v0, p0, LX/7Oh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-static {v0}, LX/7Oj;->b(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
