.class public final enum LX/78H;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/78H;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/78H;

.field public static final enum AboveThreshold:LX/78H;

.field public static final enum BelowThreshold:LX/78H;

.field public static final enum Insignificant:LX/78H;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1172532
    new-instance v0, LX/78H;

    const-string v1, "Insignificant"

    invoke-direct {v0, v1, v2}, LX/78H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/78H;->Insignificant:LX/78H;

    new-instance v0, LX/78H;

    const-string v1, "AboveThreshold"

    invoke-direct {v0, v1, v3}, LX/78H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/78H;->AboveThreshold:LX/78H;

    new-instance v0, LX/78H;

    const-string v1, "BelowThreshold"

    invoke-direct {v0, v1, v4}, LX/78H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/78H;->BelowThreshold:LX/78H;

    .line 1172533
    const/4 v0, 0x3

    new-array v0, v0, [LX/78H;

    sget-object v1, LX/78H;->Insignificant:LX/78H;

    aput-object v1, v0, v2

    sget-object v1, LX/78H;->AboveThreshold:LX/78H;

    aput-object v1, v0, v3

    sget-object v1, LX/78H;->BelowThreshold:LX/78H;

    aput-object v1, v0, v4

    sput-object v0, LX/78H;->$VALUES:[LX/78H;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1172534
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/78H;
    .locals 1

    .prologue
    .line 1172535
    const-class v0, LX/78H;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/78H;

    return-object v0
.end method

.method public static values()[LX/78H;
    .locals 1

    .prologue
    .line 1172536
    sget-object v0, LX/78H;->$VALUES:[LX/78H;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/78H;

    return-object v0
.end method
