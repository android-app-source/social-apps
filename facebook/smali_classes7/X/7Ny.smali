.class public final enum LX/7Ny;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Ny;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Ny;

.field public static final enum CASTING_CURRENT:LX/7Ny;

.field public static final enum CASTING_LOADING:LX/7Ny;

.field public static final enum CASTING_STOPPED:LX/7Ny;

.field public static final enum CONNECTING:LX/7Ny;

.field public static final enum DISCONNECTED:LX/7Ny;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1200809
    new-instance v0, LX/7Ny;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v2}, LX/7Ny;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ny;->DISCONNECTED:LX/7Ny;

    .line 1200810
    new-instance v0, LX/7Ny;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v3}, LX/7Ny;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ny;->CONNECTING:LX/7Ny;

    .line 1200811
    new-instance v0, LX/7Ny;

    const-string v1, "CASTING_LOADING"

    invoke-direct {v0, v1, v4}, LX/7Ny;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ny;->CASTING_LOADING:LX/7Ny;

    .line 1200812
    new-instance v0, LX/7Ny;

    const-string v1, "CASTING_CURRENT"

    invoke-direct {v0, v1, v5}, LX/7Ny;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ny;->CASTING_CURRENT:LX/7Ny;

    .line 1200813
    new-instance v0, LX/7Ny;

    const-string v1, "CASTING_STOPPED"

    invoke-direct {v0, v1, v6}, LX/7Ny;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ny;->CASTING_STOPPED:LX/7Ny;

    .line 1200814
    const/4 v0, 0x5

    new-array v0, v0, [LX/7Ny;

    sget-object v1, LX/7Ny;->DISCONNECTED:LX/7Ny;

    aput-object v1, v0, v2

    sget-object v1, LX/7Ny;->CONNECTING:LX/7Ny;

    aput-object v1, v0, v3

    sget-object v1, LX/7Ny;->CASTING_LOADING:LX/7Ny;

    aput-object v1, v0, v4

    sget-object v1, LX/7Ny;->CASTING_CURRENT:LX/7Ny;

    aput-object v1, v0, v5

    sget-object v1, LX/7Ny;->CASTING_STOPPED:LX/7Ny;

    aput-object v1, v0, v6

    sput-object v0, LX/7Ny;->$VALUES:[LX/7Ny;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1200815
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Ny;
    .locals 1

    .prologue
    .line 1200816
    const-class v0, LX/7Ny;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Ny;

    return-object v0
.end method

.method public static values()[LX/7Ny;
    .locals 1

    .prologue
    .line 1200817
    sget-object v0, LX/7Ny;->$VALUES:[LX/7Ny;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Ny;

    return-object v0
.end method
