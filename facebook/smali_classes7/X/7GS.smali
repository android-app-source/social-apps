.class public LX/7GS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/7GS;


# instance fields
.field private final a:LX/0Xl;

.field private final b:LX/0So;

.field private final c:LX/1fU;

.field private final d:LX/7GV;


# direct methods
.method public constructor <init>(LX/0Xl;LX/0So;LX/1fU;LX/7GV;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1189192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189193
    iput-object p1, p0, LX/7GS;->a:LX/0Xl;

    .line 1189194
    iput-object p2, p0, LX/7GS;->b:LX/0So;

    .line 1189195
    iput-object p3, p0, LX/7GS;->c:LX/1fU;

    .line 1189196
    iput-object p4, p0, LX/7GS;->d:LX/7GV;

    .line 1189197
    return-void
.end method

.method public static a(LX/0QB;)LX/7GS;
    .locals 7

    .prologue
    .line 1189198
    sget-object v0, LX/7GS;->e:LX/7GS;

    if-nez v0, :cond_1

    .line 1189199
    const-class v1, LX/7GS;

    monitor-enter v1

    .line 1189200
    :try_start_0
    sget-object v0, LX/7GS;->e:LX/7GS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1189201
    if-eqz v2, :cond_0

    .line 1189202
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1189203
    new-instance p0, LX/7GS;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0}, LX/1fU;->a(LX/0QB;)LX/1fU;

    move-result-object v5

    check-cast v5, LX/1fU;

    invoke-static {v0}, LX/7GV;->a(LX/0QB;)LX/7GV;

    move-result-object v6

    check-cast v6, LX/7GV;

    invoke-direct {p0, v3, v4, v5, v6}, LX/7GS;-><init>(LX/0Xl;LX/0So;LX/1fU;LX/7GV;)V

    .line 1189204
    move-object v0, p0

    .line 1189205
    sput-object v0, LX/7GS;->e:LX/7GS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1189206
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1189207
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1189208
    :cond_1
    sget-object v0, LX/7GS;->e:LX/7GS;

    return-object v0

    .line 1189209
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1189210
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/7GQ;)LX/76J;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RESPONSE:",
            "Ljava/lang/Object;",
            "PAY",
            "LOAD:Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "LX/7GQ",
            "<TRESPONSE;TPAY",
            "LOAD;",
            ">;)",
            "LX/76J",
            "<TRESPONSE;>;"
        }
    .end annotation

    .prologue
    .line 1189211
    new-instance v0, LX/7GR;

    iget-object v3, p0, LX/7GS;->a:LX/0Xl;

    iget-object v4, p0, LX/7GS;->b:LX/0So;

    iget-object v5, p0, LX/7GS;->c:LX/1fU;

    move-object v1, p0

    move-object v2, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, LX/7GR;-><init>(LX/7GS;Ljava/lang/String;LX/0Xl;LX/0So;LX/1fU;LX/7GQ;)V

    return-object v0
.end method
