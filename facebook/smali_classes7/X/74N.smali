.class public final enum LX/74N;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/74N;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/74N;

.field public static final enum FEED:LX/74N;

.field public static final enum FULL_SCREEN_GALLERY:LX/74N;

.field public static final enum PERMALINK:LX/74N;

.field public static final enum SNOWFLAKE:LX/74N;

.field public static final enum TAB_VIEW:LX/74N;

.field public static final enum TIMELINE:LX/74N;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1167801
    new-instance v0, LX/74N;

    const-string v1, "FEED"

    const-string v2, "feed"

    invoke-direct {v0, v1, v4, v2}, LX/74N;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74N;->FEED:LX/74N;

    .line 1167802
    new-instance v0, LX/74N;

    const-string v1, "TIMELINE"

    const-string v2, "timeline"

    invoke-direct {v0, v1, v5, v2}, LX/74N;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74N;->TIMELINE:LX/74N;

    .line 1167803
    new-instance v0, LX/74N;

    const-string v1, "SNOWFLAKE"

    const-string v2, "snowflake"

    invoke-direct {v0, v1, v6, v2}, LX/74N;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74N;->SNOWFLAKE:LX/74N;

    .line 1167804
    new-instance v0, LX/74N;

    const-string v1, "FULL_SCREEN_GALLERY"

    const-string v2, "full_screen_gallery"

    invoke-direct {v0, v1, v7, v2}, LX/74N;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74N;->FULL_SCREEN_GALLERY:LX/74N;

    .line 1167805
    new-instance v0, LX/74N;

    const-string v1, "PERMALINK"

    const-string v2, "permalink"

    invoke-direct {v0, v1, v8, v2}, LX/74N;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74N;->PERMALINK:LX/74N;

    .line 1167806
    new-instance v0, LX/74N;

    const-string v1, "TAB_VIEW"

    const/4 v2, 0x5

    const-string v3, "tab_view"

    invoke-direct {v0, v1, v2, v3}, LX/74N;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74N;->TAB_VIEW:LX/74N;

    .line 1167807
    const/4 v0, 0x6

    new-array v0, v0, [LX/74N;

    sget-object v1, LX/74N;->FEED:LX/74N;

    aput-object v1, v0, v4

    sget-object v1, LX/74N;->TIMELINE:LX/74N;

    aput-object v1, v0, v5

    sget-object v1, LX/74N;->SNOWFLAKE:LX/74N;

    aput-object v1, v0, v6

    sget-object v1, LX/74N;->FULL_SCREEN_GALLERY:LX/74N;

    aput-object v1, v0, v7

    sget-object v1, LX/74N;->PERMALINK:LX/74N;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/74N;->TAB_VIEW:LX/74N;

    aput-object v2, v0, v1

    sput-object v0, LX/74N;->$VALUES:[LX/74N;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1167808
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1167809
    iput-object p3, p0, LX/74N;->value:Ljava/lang/String;

    .line 1167810
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/74N;
    .locals 1

    .prologue
    .line 1167811
    const-class v0, LX/74N;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/74N;

    return-object v0
.end method

.method public static values()[LX/74N;
    .locals 1

    .prologue
    .line 1167812
    sget-object v0, LX/74N;->$VALUES:[LX/74N;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/74N;

    return-object v0
.end method
