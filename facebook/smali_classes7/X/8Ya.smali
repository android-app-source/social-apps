.class public final LX/8Ya;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mFonts:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fonts"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final mVersion:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "version"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "name"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "version"
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "fonts"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1356372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1356373
    iput-object p1, p0, LX/8Ya;->mName:Ljava/lang/String;

    .line 1356374
    iput-object p2, p0, LX/8Ya;->mVersion:Ljava/lang/String;

    .line 1356375
    iput-object p3, p0, LX/8Ya;->mFonts:Ljava/util/List;

    .line 1356376
    return-void
.end method


# virtual methods
.method public final c()Ljava/util/List;
    .locals 6
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/8YY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1356377
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1356378
    iget-object v0, p0, LX/8Ya;->mFonts:Ljava/util/List;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1356379
    iget-object v0, p0, LX/8Ya;->mFonts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1356380
    new-instance v3, LX/8YY;

    iget-object v4, p0, LX/8Ya;->mName:Ljava/lang/String;

    iget-object v5, p0, LX/8Ya;->mVersion:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v0}, LX/8YY;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1356381
    :cond_0
    return-object v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1356382
    instance-of v0, p1, LX/8Ya;

    if-eqz v0, :cond_1

    .line 1356383
    check-cast p1, LX/8Ya;

    .line 1356384
    iget-object v0, p0, LX/8Ya;->mName:Ljava/lang/String;

    iget-object v1, p1, LX/8Ya;->mName:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8Ya;->mVersion:Ljava/lang/String;

    iget-object v1, p1, LX/8Ya;->mVersion:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8Ya;->mFonts:Ljava/util/List;

    iget-object v1, p1, LX/8Ya;->mFonts:Ljava/util/List;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1356385
    :goto_0
    return v0

    .line 1356386
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1356387
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1356388
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/8Ya;->mName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/8Ya;->mVersion:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/8Ya;->mFonts:Ljava/util/List;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
