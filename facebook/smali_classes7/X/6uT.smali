.class public final enum LX/6uT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6uT;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6uT;

.field public static final enum ACTIVATE_SECURITY_PIN:LX/6uT;

.field public static final enum CHECK_MARK:LX/6uT;

.field public static final enum DIVIDER:LX/6uT;

.field public static final enum PRODUCT_PURCHASE_SECTION:LX/6uT;

.field public static final enum PRODUCT_USER_ENGAGE_OPTION:LX/6uT;

.field public static final enum SEE_RECEIPT:LX/6uT;

.field public static final enum VIEW_PURCHASED_ITEMS:LX/6uT;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1155899
    new-instance v0, LX/6uT;

    const-string v1, "ACTIVATE_SECURITY_PIN"

    invoke-direct {v0, v1, v3}, LX/6uT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6uT;->ACTIVATE_SECURITY_PIN:LX/6uT;

    .line 1155900
    new-instance v0, LX/6uT;

    const-string v1, "CHECK_MARK"

    invoke-direct {v0, v1, v4}, LX/6uT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6uT;->CHECK_MARK:LX/6uT;

    .line 1155901
    new-instance v0, LX/6uT;

    const-string v1, "DIVIDER"

    invoke-direct {v0, v1, v5}, LX/6uT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6uT;->DIVIDER:LX/6uT;

    .line 1155902
    new-instance v0, LX/6uT;

    const-string v1, "PRODUCT_PURCHASE_SECTION"

    invoke-direct {v0, v1, v6}, LX/6uT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6uT;->PRODUCT_PURCHASE_SECTION:LX/6uT;

    .line 1155903
    new-instance v0, LX/6uT;

    const-string v1, "PRODUCT_USER_ENGAGE_OPTION"

    invoke-direct {v0, v1, v7}, LX/6uT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6uT;->PRODUCT_USER_ENGAGE_OPTION:LX/6uT;

    .line 1155904
    new-instance v0, LX/6uT;

    const-string v1, "SEE_RECEIPT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6uT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6uT;->SEE_RECEIPT:LX/6uT;

    .line 1155905
    new-instance v0, LX/6uT;

    const-string v1, "VIEW_PURCHASED_ITEMS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/6uT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6uT;->VIEW_PURCHASED_ITEMS:LX/6uT;

    .line 1155906
    const/4 v0, 0x7

    new-array v0, v0, [LX/6uT;

    sget-object v1, LX/6uT;->ACTIVATE_SECURITY_PIN:LX/6uT;

    aput-object v1, v0, v3

    sget-object v1, LX/6uT;->CHECK_MARK:LX/6uT;

    aput-object v1, v0, v4

    sget-object v1, LX/6uT;->DIVIDER:LX/6uT;

    aput-object v1, v0, v5

    sget-object v1, LX/6uT;->PRODUCT_PURCHASE_SECTION:LX/6uT;

    aput-object v1, v0, v6

    sget-object v1, LX/6uT;->PRODUCT_USER_ENGAGE_OPTION:LX/6uT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6uT;->SEE_RECEIPT:LX/6uT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6uT;->VIEW_PURCHASED_ITEMS:LX/6uT;

    aput-object v2, v0, v1

    sput-object v0, LX/6uT;->$VALUES:[LX/6uT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1155907
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6uT;
    .locals 1

    .prologue
    .line 1155908
    const-class v0, LX/6uT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6uT;

    return-object v0
.end method

.method public static values()[LX/6uT;
    .locals 1

    .prologue
    .line 1155909
    sget-object v0, LX/6uT;->$VALUES:[LX/6uT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6uT;

    return-object v0
.end method
