.class public LX/7Sp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:[S

.field private b:LX/5Pg;

.field private c:LX/5Pg;

.field private d:LX/5PZ;

.field private e:LX/5PR;

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(I)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 1209172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1209173
    mul-int/lit8 v0, p1, 0x6

    new-array v0, v0, [S

    iput-object v0, p0, LX/7Sp;->a:[S

    move v0, v1

    move v2, v1

    .line 1209174
    :goto_0
    iget-object v3, p0, LX/7Sp;->a:[S

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 1209175
    iget-object v3, p0, LX/7Sp;->a:[S

    add-int/lit8 v4, v0, 0x0

    add-int/lit8 v5, v2, 0x0

    int-to-short v5, v5

    aput-short v5, v3, v4

    .line 1209176
    iget-object v3, p0, LX/7Sp;->a:[S

    add-int/lit8 v4, v0, 0x1

    add-int/lit8 v5, v2, 0x1

    int-to-short v5, v5

    aput-short v5, v3, v4

    .line 1209177
    iget-object v3, p0, LX/7Sp;->a:[S

    add-int/lit8 v4, v0, 0x2

    add-int/lit8 v5, v2, 0x2

    int-to-short v5, v5

    aput-short v5, v3, v4

    .line 1209178
    iget-object v3, p0, LX/7Sp;->a:[S

    add-int/lit8 v4, v0, 0x3

    add-int/lit8 v5, v2, 0x2

    int-to-short v5, v5

    aput-short v5, v3, v4

    .line 1209179
    iget-object v3, p0, LX/7Sp;->a:[S

    add-int/lit8 v4, v0, 0x4

    add-int/lit8 v5, v2, 0x3

    int-to-short v5, v5

    aput-short v5, v3, v4

    .line 1209180
    iget-object v3, p0, LX/7Sp;->a:[S

    add-int/lit8 v4, v0, 0x5

    add-int/lit8 v5, v2, 0x0

    int-to-short v5, v5

    aput-short v5, v3, v4

    .line 1209181
    add-int/lit8 v0, v0, 0x6

    add-int/lit8 v2, v2, 0x4

    int-to-short v2, v2

    goto :goto_0

    .line 1209182
    :cond_0
    new-instance v0, LX/5PZ;

    iget-object v2, p0, LX/7Sp;->a:[S

    invoke-direct {v0, v2}, LX/5PZ;-><init>([S)V

    iput-object v0, p0, LX/7Sp;->d:LX/5PZ;

    .line 1209183
    new-instance v0, LX/5Pg;

    mul-int/lit8 v2, p1, 0x8

    new-array v2, v2, [F

    invoke-direct {v0, v2, v6}, LX/5Pg;-><init>([FI)V

    iput-object v0, p0, LX/7Sp;->b:LX/5Pg;

    .line 1209184
    new-instance v0, LX/5Pg;

    mul-int/lit8 v2, p1, 0x8

    new-array v2, v2, [F

    invoke-direct {v0, v2, v6}, LX/5Pg;-><init>([FI)V

    iput-object v0, p0, LX/7Sp;->c:LX/5Pg;

    .line 1209185
    new-instance v0, LX/5PQ;

    mul-int/lit8 v2, p1, 0x4

    invoke-direct {v0, v2}, LX/5PQ;-><init>(I)V

    const/4 v2, 0x4

    .line 1209186
    iput v2, v0, LX/5PQ;->a:I

    .line 1209187
    move-object v0, v0

    .line 1209188
    const-string v2, "aPosition"

    iget-object v3, p0, LX/7Sp;->b:LX/5Pg;

    invoke-virtual {v0, v2, v3}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    const-string v2, "aTextureCoord"

    iget-object v3, p0, LX/7Sp;->c:LX/5Pg;

    invoke-virtual {v0, v2, v3}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    iget-object v2, p0, LX/7Sp;->d:LX/5PZ;

    invoke-virtual {v0, v2}, LX/5PQ;->a(LX/5PY;)LX/5PQ;

    move-result-object v0

    invoke-virtual {v0}, LX/5PQ;->a()LX/5PR;

    move-result-object v0

    iput-object v0, p0, LX/7Sp;->e:LX/5PR;

    .line 1209189
    iput p1, p0, LX/7Sp;->f:I

    .line 1209190
    iput v1, p0, LX/7Sp;->g:I

    .line 1209191
    return-void
.end method

.method public static a(LX/7Sp;)V
    .locals 1

    .prologue
    .line 1209192
    const/4 v0, 0x0

    iput v0, p0, LX/7Sp;->g:I

    .line 1209193
    iget-object v0, p0, LX/7Sp;->b:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    .line 1209194
    iget-object v0, p0, LX/7Sp;->c:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    .line 1209195
    return-void
.end method


# virtual methods
.method public final a(FFFFLX/7Sr;LX/5Pa;)V
    .locals 6

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1209196
    iget v0, p0, LX/7Sp;->g:I

    iget v1, p0, LX/7Sp;->f:I

    if-ne v0, v1, :cond_0

    .line 1209197
    invoke-virtual {p0, p6}, LX/7Sp;->a(LX/5Pa;)V

    .line 1209198
    invoke-static {p0}, LX/7Sp;->a(LX/7Sp;)V

    .line 1209199
    :cond_0
    div-float v0, p3, v2

    .line 1209200
    div-float v1, p4, v2

    .line 1209201
    sub-float v2, p1, v0

    .line 1209202
    sub-float v3, p2, v1

    .line 1209203
    add-float/2addr v0, p1

    .line 1209204
    add-float/2addr v1, p2

    .line 1209205
    iget-object v4, p0, LX/7Sp;->b:LX/5Pg;

    iget-object v4, v4, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v2}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209206
    iget-object v4, p0, LX/7Sp;->b:LX/5Pg;

    iget-object v4, v4, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209207
    iget-object v4, p0, LX/7Sp;->c:LX/5Pg;

    iget-object v4, v4, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    iget v5, p5, LX/7Sr;->a:F

    invoke-virtual {v4, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209208
    iget-object v4, p0, LX/7Sp;->c:LX/5Pg;

    iget-object v4, v4, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    iget v5, p5, LX/7Sr;->d:F

    invoke-virtual {v4, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209209
    iget-object v4, p0, LX/7Sp;->b:LX/5Pg;

    iget-object v4, v4, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209210
    iget-object v4, p0, LX/7Sp;->b:LX/5Pg;

    iget-object v4, v4, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209211
    iget-object v3, p0, LX/7Sp;->c:LX/5Pg;

    iget-object v3, v3, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    iget v4, p5, LX/7Sr;->c:F

    invoke-virtual {v3, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209212
    iget-object v3, p0, LX/7Sp;->c:LX/5Pg;

    iget-object v3, v3, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    iget v4, p5, LX/7Sr;->d:F

    invoke-virtual {v3, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209213
    iget-object v3, p0, LX/7Sp;->b:LX/5Pg;

    iget-object v3, v3, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v3, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209214
    iget-object v0, p0, LX/7Sp;->b:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209215
    iget-object v0, p0, LX/7Sp;->c:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    iget v3, p5, LX/7Sr;->c:F

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209216
    iget-object v0, p0, LX/7Sp;->c:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    iget v3, p5, LX/7Sr;->b:F

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209217
    iget-object v0, p0, LX/7Sp;->b:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209218
    iget-object v0, p0, LX/7Sp;->b:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209219
    iget-object v0, p0, LX/7Sp;->c:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    iget v1, p5, LX/7Sr;->a:F

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209220
    iget-object v0, p0, LX/7Sp;->c:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    iget v1, p5, LX/7Sr;->b:F

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 1209221
    iget v0, p0, LX/7Sp;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7Sp;->g:I

    .line 1209222
    return-void
.end method

.method public final a(LX/5Pa;)V
    .locals 2

    .prologue
    .line 1209223
    iget v0, p0, LX/7Sp;->g:I

    if-lez v0, :cond_0

    .line 1209224
    iget-object v0, p0, LX/7Sp;->d:LX/5PZ;

    iget v1, p0, LX/7Sp;->g:I

    mul-int/lit8 v1, v1, 0x6

    iput v1, v0, LX/5PZ;->d:I

    .line 1209225
    iget-object v0, p0, LX/7Sp;->b:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    .line 1209226
    iget-object v0, p0, LX/7Sp;->c:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    .line 1209227
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 1209228
    const/16 v0, 0x302

    const/16 v1, 0x303

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 1209229
    iget-object v0, p0, LX/7Sp;->e:LX/5PR;

    invoke-virtual {p1, v0}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    .line 1209230
    :cond_0
    return-void
.end method
