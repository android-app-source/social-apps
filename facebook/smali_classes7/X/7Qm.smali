.class public LX/7Qm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Qf;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0lF;

.field public final c:LX/7Qh;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0lF;Ljava/lang/String;IILjava/lang/String;)V
    .locals 1

    .prologue
    .line 1204780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204781
    iput-object p1, p0, LX/7Qm;->a:Ljava/lang/String;

    .line 1204782
    iput-object p2, p0, LX/7Qm;->b:LX/0lF;

    .line 1204783
    new-instance v0, LX/7Qh;

    invoke-direct {v0, p3, p4, p5, p6}, LX/7Qh;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    iput-object v0, p0, LX/7Qm;->c:LX/7Qh;

    .line 1204784
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 1204785
    iget-object v0, p0, LX/7Qm;->c:LX/7Qh;

    invoke-virtual {v0, p1}, LX/7Qh;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1204786
    sget-object v0, LX/0JS;->EVENT_TARGET:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    const-string v1, "story"

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204787
    sget-object v0, LX/0JS;->TARGET_ID:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget-object v1, p0, LX/7Qm;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204788
    sget-object v0, LX/0JS;->TRACKING:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget-object v1, p0, LX/7Qm;->b:LX/0lF;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204789
    return-void
.end method
