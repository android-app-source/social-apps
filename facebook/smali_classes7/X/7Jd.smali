.class public final enum LX/7Jd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Jd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Jd;

.field public static final enum BUFFERED_VIDEO_PLAYBACK:LX/7Jd;

.field public static final enum BUFFERING_OPTION_SHOWN:LX/7Jd;

.field public static final enum DOWNLOAD_ABORTED:LX/7Jd;

.field public static final enum DOWNLOAD_CANCELLED:LX/7Jd;

.field public static final enum DOWNLOAD_CLICKED:LX/7Jd;

.field public static final enum DOWNLOAD_COMPLETED:LX/7Jd;

.field public static final enum DOWNLOAD_DELETED:LX/7Jd;

.field public static final enum DOWNLOAD_FAILED:LX/7Jd;

.field public static final enum DOWNLOAD_FAILED_NO_SPACE:LX/7Jd;

.field public static final enum DOWNLOAD_PAUSED:LX/7Jd;

.field public static final enum DOWNLOAD_PLUGIN_LOAD:LX/7Jd;

.field public static final enum DOWNLOAD_QUEUED:LX/7Jd;

.field public static final enum DOWNLOAD_REQUESTED:LX/7Jd;

.field public static final enum DOWNLOAD_STARTED:LX/7Jd;

.field public static final enum DOWNLOAD_STOPPED:LX/7Jd;

.field public static final enum PLAYBACK_BLOCKED:LX/7Jd;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1193958
    new-instance v0, LX/7Jd;

    const-string v1, "DOWNLOAD_CLICKED"

    const-string v2, "offline_video_download_clicked"

    invoke-direct {v0, v1, v4, v2}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->DOWNLOAD_CLICKED:LX/7Jd;

    .line 1193959
    new-instance v0, LX/7Jd;

    const-string v1, "DOWNLOAD_REQUESTED"

    const-string v2, "offline_video_download_requested"

    invoke-direct {v0, v1, v5, v2}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->DOWNLOAD_REQUESTED:LX/7Jd;

    .line 1193960
    new-instance v0, LX/7Jd;

    const-string v1, "DOWNLOAD_QUEUED"

    const-string v2, "offline_video_download_queued"

    invoke-direct {v0, v1, v6, v2}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->DOWNLOAD_QUEUED:LX/7Jd;

    .line 1193961
    new-instance v0, LX/7Jd;

    const-string v1, "DOWNLOAD_STARTED"

    const-string v2, "offline_video_download_started"

    invoke-direct {v0, v1, v7, v2}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->DOWNLOAD_STARTED:LX/7Jd;

    .line 1193962
    new-instance v0, LX/7Jd;

    const-string v1, "DOWNLOAD_STOPPED"

    const-string v2, "offline_video_download_stopped"

    invoke-direct {v0, v1, v8, v2}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->DOWNLOAD_STOPPED:LX/7Jd;

    .line 1193963
    new-instance v0, LX/7Jd;

    const-string v1, "DOWNLOAD_PAUSED"

    const/4 v2, 0x5

    const-string v3, "offline_video_download_paused"

    invoke-direct {v0, v1, v2, v3}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->DOWNLOAD_PAUSED:LX/7Jd;

    .line 1193964
    new-instance v0, LX/7Jd;

    const-string v1, "DOWNLOAD_COMPLETED"

    const/4 v2, 0x6

    const-string v3, "offline_video_download_completed"

    invoke-direct {v0, v1, v2, v3}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->DOWNLOAD_COMPLETED:LX/7Jd;

    .line 1193965
    new-instance v0, LX/7Jd;

    const-string v1, "DOWNLOAD_ABORTED"

    const/4 v2, 0x7

    const-string v3, "offline_video_download_aborted"

    invoke-direct {v0, v1, v2, v3}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->DOWNLOAD_ABORTED:LX/7Jd;

    .line 1193966
    new-instance v0, LX/7Jd;

    const-string v1, "DOWNLOAD_CANCELLED"

    const/16 v2, 0x8

    const-string v3, "offline_video_download_cancelled"

    invoke-direct {v0, v1, v2, v3}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->DOWNLOAD_CANCELLED:LX/7Jd;

    .line 1193967
    new-instance v0, LX/7Jd;

    const-string v1, "DOWNLOAD_DELETED"

    const/16 v2, 0x9

    const-string v3, "offline_video_download_deleted"

    invoke-direct {v0, v1, v2, v3}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->DOWNLOAD_DELETED:LX/7Jd;

    .line 1193968
    new-instance v0, LX/7Jd;

    const-string v1, "DOWNLOAD_FAILED"

    const/16 v2, 0xa

    const-string v3, "offline_video_download_failed"

    invoke-direct {v0, v1, v2, v3}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->DOWNLOAD_FAILED:LX/7Jd;

    .line 1193969
    new-instance v0, LX/7Jd;

    const-string v1, "PLAYBACK_BLOCKED"

    const/16 v2, 0xb

    const-string v3, "offline_video_playback_blocked"

    invoke-direct {v0, v1, v2, v3}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->PLAYBACK_BLOCKED:LX/7Jd;

    .line 1193970
    new-instance v0, LX/7Jd;

    const-string v1, "DOWNLOAD_PLUGIN_LOAD"

    const/16 v2, 0xc

    const-string v3, "offline_video_download_plugin_load"

    invoke-direct {v0, v1, v2, v3}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->DOWNLOAD_PLUGIN_LOAD:LX/7Jd;

    .line 1193971
    new-instance v0, LX/7Jd;

    const-string v1, "DOWNLOAD_FAILED_NO_SPACE"

    const/16 v2, 0xd

    const-string v3, "offline_video_download_failed_nospace"

    invoke-direct {v0, v1, v2, v3}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->DOWNLOAD_FAILED_NO_SPACE:LX/7Jd;

    .line 1193972
    new-instance v0, LX/7Jd;

    const-string v1, "BUFFERING_OPTION_SHOWN"

    const/16 v2, 0xe

    const-string v3, "offline_video_buffering_option_shown"

    invoke-direct {v0, v1, v2, v3}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->BUFFERING_OPTION_SHOWN:LX/7Jd;

    .line 1193973
    new-instance v0, LX/7Jd;

    const-string v1, "BUFFERED_VIDEO_PLAYBACK"

    const/16 v2, 0xf

    const-string v3, "offline_video_buffered_video_playback"

    invoke-direct {v0, v1, v2, v3}, LX/7Jd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jd;->BUFFERED_VIDEO_PLAYBACK:LX/7Jd;

    .line 1193974
    const/16 v0, 0x10

    new-array v0, v0, [LX/7Jd;

    sget-object v1, LX/7Jd;->DOWNLOAD_CLICKED:LX/7Jd;

    aput-object v1, v0, v4

    sget-object v1, LX/7Jd;->DOWNLOAD_REQUESTED:LX/7Jd;

    aput-object v1, v0, v5

    sget-object v1, LX/7Jd;->DOWNLOAD_QUEUED:LX/7Jd;

    aput-object v1, v0, v6

    sget-object v1, LX/7Jd;->DOWNLOAD_STARTED:LX/7Jd;

    aput-object v1, v0, v7

    sget-object v1, LX/7Jd;->DOWNLOAD_STOPPED:LX/7Jd;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7Jd;->DOWNLOAD_PAUSED:LX/7Jd;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7Jd;->DOWNLOAD_COMPLETED:LX/7Jd;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7Jd;->DOWNLOAD_ABORTED:LX/7Jd;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7Jd;->DOWNLOAD_CANCELLED:LX/7Jd;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7Jd;->DOWNLOAD_DELETED:LX/7Jd;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7Jd;->DOWNLOAD_FAILED:LX/7Jd;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7Jd;->PLAYBACK_BLOCKED:LX/7Jd;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7Jd;->DOWNLOAD_PLUGIN_LOAD:LX/7Jd;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/7Jd;->DOWNLOAD_FAILED_NO_SPACE:LX/7Jd;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/7Jd;->BUFFERING_OPTION_SHOWN:LX/7Jd;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/7Jd;->BUFFERED_VIDEO_PLAYBACK:LX/7Jd;

    aput-object v2, v0, v1

    sput-object v0, LX/7Jd;->$VALUES:[LX/7Jd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1193975
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1193976
    iput-object p3, p0, LX/7Jd;->value:Ljava/lang/String;

    .line 1193977
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Jd;
    .locals 1

    .prologue
    .line 1193978
    const-class v0, LX/7Jd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Jd;

    return-object v0
.end method

.method public static values()[LX/7Jd;
    .locals 1

    .prologue
    .line 1193979
    sget-object v0, LX/7Jd;->$VALUES:[LX/7Jd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Jd;

    return-object v0
.end method
