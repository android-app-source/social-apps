.class public final enum LX/8HP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8HP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8HP;

.field public static final enum ADD_OPERATION:LX/8HP;

.field public static final enum DELETE_OPERATION:LX/8HP;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:LX/8HP;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1320936
    new-instance v0, LX/8HP;

    const-string v1, "ADD_OPERATION"

    invoke-direct {v0, v1, v2}, LX/8HP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8HP;->ADD_OPERATION:LX/8HP;

    .line 1320937
    new-instance v0, LX/8HP;

    const-string v1, "DELETE_OPERATION"

    invoke-direct {v0, v1, v3}, LX/8HP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8HP;->DELETE_OPERATION:LX/8HP;

    .line 1320938
    new-instance v0, LX/8HP;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v4}, LX/8HP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8HP;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:LX/8HP;

    .line 1320939
    const/4 v0, 0x3

    new-array v0, v0, [LX/8HP;

    sget-object v1, LX/8HP;->ADD_OPERATION:LX/8HP;

    aput-object v1, v0, v2

    sget-object v1, LX/8HP;->DELETE_OPERATION:LX/8HP;

    aput-object v1, v0, v3

    sget-object v1, LX/8HP;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:LX/8HP;

    aput-object v1, v0, v4

    sput-object v0, LX/8HP;->$VALUES:[LX/8HP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1320940
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8HP;
    .locals 1

    .prologue
    .line 1320941
    const-class v0, LX/8HP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8HP;

    return-object v0
.end method

.method public static values()[LX/8HP;
    .locals 1

    .prologue
    .line 1320942
    sget-object v0, LX/8HP;->$VALUES:[LX/8HP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8HP;

    return-object v0
.end method
