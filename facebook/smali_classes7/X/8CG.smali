.class public LX/8CG;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/messaging/permissions/PermissionRequestIconView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1310617
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1310618
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/8CG;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1310619
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1310620
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1310621
    invoke-direct {p0, p1, p2}, LX/8CG;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1310622
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1310623
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1310624
    invoke-direct {p0, p1, p2}, LX/8CG;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1310625
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1310626
    const v0, 0x7f030343

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1310627
    const v0, 0x7f0d0ac1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/permissions/PermissionRequestIconView;

    iput-object v0, p0, LX/8CG;->a:Lcom/facebook/messaging/permissions/PermissionRequestIconView;

    .line 1310628
    sget-object v0, LX/03r;->PermissionRequestView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1310629
    const/16 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1310630
    iget-object v2, p0, LX/8CG;->a:Lcom/facebook/messaging/permissions/PermissionRequestIconView;

    invoke-virtual {v2, v1}, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->setIcon(I)V

    .line 1310631
    invoke-virtual {p0}, LX/8CG;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x1

    invoke-static {v1, v0, v2}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    .line 1310632
    iget-object v2, p0, LX/8CG;->a:Lcom/facebook/messaging/permissions/PermissionRequestIconView;

    invoke-virtual {v2, v1}, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->setText(Ljava/lang/String;)V

    .line 1310633
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1310634
    return-void
.end method


# virtual methods
.method public setButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1310635
    iget-object v0, p0, LX/8CG;->a:Lcom/facebook/messaging/permissions/PermissionRequestIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->setButtonListener(Landroid/view/View$OnClickListener;)V

    .line 1310636
    return-void
.end method
