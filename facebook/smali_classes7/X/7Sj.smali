.class public LX/7Sj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Sb;


# instance fields
.field public a:LX/7Si;


# direct methods
.method public constructor <init>(LX/7Si;)V
    .locals 0

    .prologue
    .line 1209111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1209112
    iput-object p1, p0, LX/7Sj;->a:LX/7Si;

    .line 1209113
    return-void
.end method


# virtual methods
.method public final a()LX/7Sc;
    .locals 1

    .prologue
    .line 1209114
    sget-object v0, LX/7Sc;->INPUT_FACING:LX/7Sc;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1209115
    const/4 v0, 0x1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1209116
    if-ne p0, p1, :cond_1

    .line 1209117
    :cond_0
    :goto_0
    return v0

    .line 1209118
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1209119
    goto :goto_0

    .line 1209120
    :cond_3
    check-cast p1, LX/7Sj;

    .line 1209121
    iget-object v2, p0, LX/7Sj;->a:LX/7Si;

    iget-object v3, p1, LX/7Sj;->a:LX/7Si;

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1209122
    iget-object v0, p0, LX/7Sj;->a:LX/7Si;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7Sj;->a:LX/7Si;

    invoke-virtual {v0}, LX/7Si;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
