.class public final LX/8MX;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "LX/8Ka;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

.field private b:Ljava/lang/String;

.field public c:I


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1334488
    iput-object p1, p0, LX/8MX;->a:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    invoke-direct {p0}, LX/8KR;-><init>()V

    .line 1334489
    iput-object p2, p0, LX/8MX;->b:Ljava/lang/String;

    .line 1334490
    iput p3, p0, LX/8MX;->c:I

    .line 1334491
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/8Ka;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1334492
    const-class v0, LX/8Ka;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1334493
    check-cast p1, LX/8Ka;

    .line 1334494
    iget-object v0, p1, LX/8Ka;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1334495
    iget-object v1, p0, LX/8MX;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1334496
    iget-object v0, p0, LX/8MX;->a:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder$CompostMultiUploadBusSubscriber$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder$CompostMultiUploadBusSubscriber$1;-><init>(LX/8MX;LX/8Ka;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1334497
    :cond_0
    return-void
.end method
