.class public final enum LX/7iQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7iQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7iQ;

.field public static final enum DID_OPEN_STOREFRONT_FROM_PAGE_HEADER:LX/7iQ;

.field public static final enum EXIT_COLLECTION_OPENED:LX/7iQ;

.field public static final enum MESSAGE_TO_BUY_CLICK:LX/7iQ;

.field public static final enum OFFSITE_CHECKOUT_LINK_CLICK:LX/7iQ;

.field public static final enum PDFY_PRODUCT_CLICK:LX/7iQ;

.field public static final enum PDFY_PRODUCT_SAVE:LX/7iQ;

.field public static final enum PDFY_PRODUCT_SHARE_CLICK:LX/7iQ;

.field public static final enum PDFY_PRODUCT_UNSAVE:LX/7iQ;

.field public static final enum PDFY_PRODUCT_VIEW:LX/7iQ;

.field public static final enum PDFY_UNIT_VIEW:LX/7iQ;

.field public static final enum RECOMMENDED_PRODUCT_OPENED:LX/7iQ;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1227473
    new-instance v0, LX/7iQ;

    const-string v1, "DID_OPEN_STOREFRONT_FROM_PAGE_HEADER"

    const-string v2, "did_open_store_front_from_page_header"

    invoke-direct {v0, v1, v4, v2}, LX/7iQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iQ;->DID_OPEN_STOREFRONT_FROM_PAGE_HEADER:LX/7iQ;

    .line 1227474
    new-instance v0, LX/7iQ;

    const-string v1, "EXIT_COLLECTION_OPENED"

    const-string v2, "exit_collection_opened"

    invoke-direct {v0, v1, v5, v2}, LX/7iQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iQ;->EXIT_COLLECTION_OPENED:LX/7iQ;

    .line 1227475
    new-instance v0, LX/7iQ;

    const-string v1, "RECOMMENDED_PRODUCT_OPENED"

    const-string v2, "recommended_product_opened"

    invoke-direct {v0, v1, v6, v2}, LX/7iQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iQ;->RECOMMENDED_PRODUCT_OPENED:LX/7iQ;

    .line 1227476
    new-instance v0, LX/7iQ;

    const-string v1, "MESSAGE_TO_BUY_CLICK"

    const-string v2, "message_to_buy_tapped"

    invoke-direct {v0, v1, v7, v2}, LX/7iQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iQ;->MESSAGE_TO_BUY_CLICK:LX/7iQ;

    .line 1227477
    new-instance v0, LX/7iQ;

    const-string v1, "OFFSITE_CHECKOUT_LINK_CLICK"

    const-string v2, "product_details_offsite_link_click"

    invoke-direct {v0, v1, v8, v2}, LX/7iQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iQ;->OFFSITE_CHECKOUT_LINK_CLICK:LX/7iQ;

    .line 1227478
    new-instance v0, LX/7iQ;

    const-string v1, "PDFY_PRODUCT_CLICK"

    const/4 v2, 0x5

    const-string v3, "pdfy_product_click"

    invoke-direct {v0, v1, v2, v3}, LX/7iQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iQ;->PDFY_PRODUCT_CLICK:LX/7iQ;

    .line 1227479
    new-instance v0, LX/7iQ;

    const-string v1, "PDFY_PRODUCT_SAVE"

    const/4 v2, 0x6

    const-string v3, "pdfy_product_save"

    invoke-direct {v0, v1, v2, v3}, LX/7iQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iQ;->PDFY_PRODUCT_SAVE:LX/7iQ;

    .line 1227480
    new-instance v0, LX/7iQ;

    const-string v1, "PDFY_PRODUCT_SHARE_CLICK"

    const/4 v2, 0x7

    const-string v3, "pdfy_product_share_click"

    invoke-direct {v0, v1, v2, v3}, LX/7iQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iQ;->PDFY_PRODUCT_SHARE_CLICK:LX/7iQ;

    .line 1227481
    new-instance v0, LX/7iQ;

    const-string v1, "PDFY_PRODUCT_UNSAVE"

    const/16 v2, 0x8

    const-string v3, "pdfy_product_unsave"

    invoke-direct {v0, v1, v2, v3}, LX/7iQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iQ;->PDFY_PRODUCT_UNSAVE:LX/7iQ;

    .line 1227482
    new-instance v0, LX/7iQ;

    const-string v1, "PDFY_PRODUCT_VIEW"

    const/16 v2, 0x9

    const-string v3, "pdfy_product_view"

    invoke-direct {v0, v1, v2, v3}, LX/7iQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iQ;->PDFY_PRODUCT_VIEW:LX/7iQ;

    .line 1227483
    new-instance v0, LX/7iQ;

    const-string v1, "PDFY_UNIT_VIEW"

    const/16 v2, 0xa

    const-string v3, "pdfy_unit_view"

    invoke-direct {v0, v1, v2, v3}, LX/7iQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iQ;->PDFY_UNIT_VIEW:LX/7iQ;

    .line 1227484
    const/16 v0, 0xb

    new-array v0, v0, [LX/7iQ;

    sget-object v1, LX/7iQ;->DID_OPEN_STOREFRONT_FROM_PAGE_HEADER:LX/7iQ;

    aput-object v1, v0, v4

    sget-object v1, LX/7iQ;->EXIT_COLLECTION_OPENED:LX/7iQ;

    aput-object v1, v0, v5

    sget-object v1, LX/7iQ;->RECOMMENDED_PRODUCT_OPENED:LX/7iQ;

    aput-object v1, v0, v6

    sget-object v1, LX/7iQ;->MESSAGE_TO_BUY_CLICK:LX/7iQ;

    aput-object v1, v0, v7

    sget-object v1, LX/7iQ;->OFFSITE_CHECKOUT_LINK_CLICK:LX/7iQ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7iQ;->PDFY_PRODUCT_CLICK:LX/7iQ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7iQ;->PDFY_PRODUCT_SAVE:LX/7iQ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7iQ;->PDFY_PRODUCT_SHARE_CLICK:LX/7iQ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7iQ;->PDFY_PRODUCT_UNSAVE:LX/7iQ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7iQ;->PDFY_PRODUCT_VIEW:LX/7iQ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7iQ;->PDFY_UNIT_VIEW:LX/7iQ;

    aput-object v2, v0, v1

    sput-object v0, LX/7iQ;->$VALUES:[LX/7iQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1227485
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1227486
    iput-object p3, p0, LX/7iQ;->value:Ljava/lang/String;

    .line 1227487
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7iQ;
    .locals 1

    .prologue
    .line 1227488
    const-class v0, LX/7iQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7iQ;

    return-object v0
.end method

.method public static values()[LX/7iQ;
    .locals 1

    .prologue
    .line 1227489
    sget-object v0, LX/7iQ;->$VALUES:[LX/7iQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7iQ;

    return-object v0
.end method
