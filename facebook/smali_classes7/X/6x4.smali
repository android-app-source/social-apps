.class public LX/6x4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wy;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wy",
        "<",
        "Lcom/facebook/payments/form/model/NoteFormData;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/6x7;

.field public c:LX/6x9;

.field public d:Lcom/facebook/fig/textinput/FigEditText;

.field private e:LX/6qh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6x7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1158409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158410
    iput-object p1, p0, LX/6x4;->a:Landroid/content/Context;

    .line 1158411
    iput-object p2, p0, LX/6x4;->b:LX/6x7;

    .line 1158412
    return-void
.end method

.method public static a(LX/0QB;)LX/6x4;
    .locals 5

    .prologue
    .line 1158398
    const-class v1, LX/6x4;

    monitor-enter v1

    .line 1158399
    :try_start_0
    sget-object v0, LX/6x4;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1158400
    sput-object v2, LX/6x4;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1158401
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1158402
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1158403
    new-instance p0, LX/6x4;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/6x7;->b(LX/0QB;)LX/6x7;

    move-result-object v4

    check-cast v4, LX/6x7;

    invoke-direct {p0, v3, v4}, LX/6x4;-><init>(Landroid/content/Context;LX/6x7;)V

    .line 1158404
    move-object v0, p0

    .line 1158405
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1158406
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6x4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1158407
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1158408
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1158371
    invoke-virtual {p0}, LX/6x4;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1158372
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1158373
    const-string v1, "extra_note"

    iget-object v2, p0, LX/6x4;->d:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v2}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1158374
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1158375
    const-string v2, "extra_activity_result_data"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1158376
    iget-object v0, p0, LX/6x4;->e:LX/6qh;

    new-instance v2, LX/73T;

    sget-object v3, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v2, v3, v1}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {v0, v2}, LX/6qh;->a(LX/73T;)V

    .line 1158377
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1158413
    iput-object p1, p0, LX/6x4;->e:LX/6qh;

    .line 1158414
    return-void
.end method

.method public final a(LX/6x9;)V
    .locals 0

    .prologue
    .line 1158396
    iput-object p1, p0, LX/6x4;->c:LX/6x9;

    .line 1158397
    return-void
.end method

.method public final a(LX/6xD;Lcom/facebook/payments/form/model/PaymentsFormData;)V
    .locals 5

    .prologue
    .line 1158379
    check-cast p2, Lcom/facebook/payments/form/model/NoteFormData;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1158380
    iget-object v0, p2, Lcom/facebook/payments/form/model/NoteFormData;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-object v0, v0

    .line 1158381
    new-instance v1, Lcom/facebook/fig/textinput/FigEditText;

    iget-object v2, p0, LX/6x4;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/fig/textinput/FigEditText;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/6x4;->d:Lcom/facebook/fig/textinput/FigEditText;

    .line 1158382
    iget-object v1, p0, LX/6x4;->d:Lcom/facebook/fig/textinput/FigEditText;

    const v2, 0x7f0d0180

    invoke-virtual {v1, v2}, Lcom/facebook/fig/textinput/FigEditText;->setId(I)V

    .line 1158383
    iget-object v1, p0, LX/6x4;->d:Lcom/facebook/fig/textinput/FigEditText;

    const/16 v2, 0x30

    invoke-virtual {v1, v2}, Lcom/facebook/fig/textinput/FigEditText;->setGravity(I)V

    .line 1158384
    iget-object v1, p0, LX/6x4;->d:Lcom/facebook/fig/textinput/FigEditText;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/facebook/fig/textinput/FigEditText;->setMinLines(I)V

    .line 1158385
    iget-object v1, p0, LX/6x4;->d:Lcom/facebook/fig/textinput/FigEditText;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/fig/textinput/FigEditText;->setType(I)V

    .line 1158386
    iget-object v1, p0, LX/6x4;->d:Lcom/facebook/fig/textinput/FigEditText;

    iget v2, v0, Lcom/facebook/payments/form/model/FormFieldAttributes;->f:I

    invoke-virtual {v1, v2}, Lcom/facebook/fig/textinput/FigEditText;->setCharLimit(I)V

    .line 1158387
    iget-object v2, p0, LX/6x4;->d:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v1, v0, Lcom/facebook/payments/form/model/FormFieldAttributes;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/6x4;->a:Landroid/content/Context;

    const p2, 0x7f081e3f

    invoke-virtual {v1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/facebook/fig/textinput/FigEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1158388
    iget-object v1, p0, LX/6x4;->d:Lcom/facebook/fig/textinput/FigEditText;

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Lcom/facebook/fig/textinput/FigEditText;->setBackgroundResource(I)V

    .line 1158389
    iget-object v1, p0, LX/6x4;->d:Lcom/facebook/fig/textinput/FigEditText;

    new-instance v2, LX/6x3;

    invoke-direct {v2, p0, v0}, LX/6x3;-><init>(LX/6x4;Lcom/facebook/payments/form/model/FormFieldAttributes;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fig/textinput/FigEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1158390
    iget-object v1, p0, LX/6x4;->d:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v2, v0, Lcom/facebook/payments/form/model/FormFieldAttributes;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1158391
    new-array v0, v4, [Landroid/view/View;

    iget-object v1, p0, LX/6x4;->d:Lcom/facebook/fig/textinput/FigEditText;

    aput-object v1, v0, v3

    invoke-virtual {p1, v0}, LX/6xD;->a([Landroid/view/View;)V

    .line 1158392
    new-array v0, v4, [Landroid/view/View;

    new-instance v1, LX/73W;

    iget-object v2, p0, LX/6x4;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/73W;-><init>(Landroid/content/Context;)V

    aput-object v1, v0, v3

    invoke-virtual {p1, v0}, LX/6xD;->a([Landroid/view/View;)V

    .line 1158393
    new-array v0, v4, [Landroid/view/View;

    iget-object v1, p0, LX/6x4;->b:LX/6x7;

    invoke-virtual {v1}, LX/6x7;->a()LX/73Y;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p1, v0}, LX/6xD;->a([Landroid/view/View;)V

    .line 1158394
    return-void

    .line 1158395
    :cond_0
    iget-object v1, v0, Lcom/facebook/payments/form/model/FormFieldAttributes;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1158378
    iget-object v0, p0, LX/6x4;->b:LX/6x7;

    invoke-virtual {v0}, LX/6x7;->d()Z

    move-result v0

    return v0
.end method
