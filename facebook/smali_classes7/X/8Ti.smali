.class public final LX/8Ti;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8Tj;

.field public final synthetic b:LX/8Tk;


# direct methods
.method public constructor <init>(LX/8Tk;LX/8Tj;)V
    .locals 0

    .prologue
    .line 1348934
    iput-object p1, p0, LX/8Ti;->b:LX/8Tk;

    iput-object p2, p0, LX/8Ti;->a:LX/8Tj;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1348935
    iget-object v0, p0, LX/8Ti;->b:LX/8Tk;

    iget-object v0, v0, LX/8Tk;->e:LX/8TD;

    sget-object v1, LX/8TE;->FRIENDS_LEADERBOARD:LX/8TE;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 1348936
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 1348937
    check-cast p1, Ljava/util/List;

    const/4 v3, 0x0

    .line 1348938
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v2, v3

    move-object v4, v3

    move-object v5, v3

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1348939
    if-eqz v0, :cond_0

    .line 1348940
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1348941
    instance-of v7, v1, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel;

    if-eqz v7, :cond_3

    .line 1348942
    iget-object v0, p0, LX/8Ti;->b:LX/8Tk;

    iget-object v0, v0, LX/8Tk;->e:LX/8TD;

    sget-object v5, LX/8TE;->FRIENDS_LEADERBOARD:LX/8TE;

    const/4 v7, 0x1

    invoke-virtual {v0, v5, v7, v3}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 1348943
    iget-object v5, p0, LX/8Ti;->b:LX/8Tk;

    move-object v0, v1

    check-cast v0, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel;

    const/4 v7, 0x0

    .line 1348944
    if-nez v0, :cond_6

    .line 1348945
    :goto_1
    move-object v5, v7

    .line 1348946
    if-eqz v5, :cond_1

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    const/16 v7, 0xa

    if-gt v0, v7, :cond_0

    .line 1348947
    :cond_1
    iget-object v0, p0, LX/8Ti;->b:LX/8Tk;

    check-cast v1, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel;

    const/4 v4, 0x0

    .line 1348948
    invoke-virtual {v1}, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel;->a()Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel$FriendsModel;

    move-result-object v7

    if-nez v7, :cond_9

    .line 1348949
    :cond_2
    :goto_2
    move-object v0, v4

    .line 1348950
    move-object v4, v0

    goto :goto_0

    .line 1348951
    :cond_3
    instance-of v7, v1, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;

    if-eqz v7, :cond_5

    .line 1348952
    check-cast v1, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;

    .line 1348953
    if-eqz v0, :cond_5

    .line 1348954
    invoke-virtual {v1}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    :goto_3
    move-object v2, v0

    .line 1348955
    goto :goto_0

    .line 1348956
    :cond_4
    iget-object v0, p0, LX/8Ti;->a:LX/8Tj;

    invoke-interface {v0, v5, v4, v2}, LX/8Tj;->a(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    .line 1348957
    return-void

    :cond_5
    move-object v0, v2

    goto :goto_3

    .line 1348958
    :cond_6
    iget-object v8, v5, LX/8Tk;->f:LX/8TY;

    invoke-virtual {v8}, LX/8TY;->c()Z

    move-result v8

    if-eqz v8, :cond_7

    iget-object v8, v5, LX/8Tk;->g:LX/8TS;

    .line 1348959
    iget-boolean p1, v8, LX/8TS;->i:Z

    move v8, p1

    .line 1348960
    if-eqz v8, :cond_8

    .line 1348961
    :cond_7
    iget-object v7, v5, LX/8Tk;->a:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f08235b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    .line 1348962
    :goto_4
    iget-object v7, v5, LX/8Tk;->j:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/8Tn;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel;->j()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    move-result-object p1

    invoke-virtual {v7, p1, v8}, LX/8Tn;->a(Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    goto :goto_1

    :cond_8
    move-object v8, v7

    goto :goto_4

    .line 1348963
    :cond_9
    invoke-virtual {v1}, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel;->a()Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel$FriendsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel$FriendsModel;->a()LX/0Px;

    move-result-object v9

    .line 1348964
    if-eqz v9, :cond_2

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v7

    if-eqz v7, :cond_2

    .line 1348965
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 1348966
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v4, 0x0

    move v8, v4

    :goto_5
    if-ge v8, v10, :cond_a

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel$FriendsModel$NodesModel;

    .line 1348967
    invoke-static {}, LX/8Vb;->b()LX/8Va;

    move-result-object v11

    invoke-virtual {v4}, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel$FriendsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v12

    .line 1348968
    iput-object v12, v11, LX/8Va;->c:Ljava/lang/String;

    .line 1348969
    move-object v11, v11

    .line 1348970
    invoke-virtual {v4}, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel$FriendsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v12

    .line 1348971
    iput-object v12, v11, LX/8Va;->a:Ljava/lang/String;

    .line 1348972
    move-object v11, v11

    .line 1348973
    iget-object v12, v0, LX/8Tk;->a:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const p1, 0x7f082360

    invoke-virtual {v12, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1348974
    iput-object v12, v11, LX/8Va;->h:Ljava/lang/String;

    .line 1348975
    move-object v11, v11

    .line 1348976
    iget-object v12, v0, LX/8Tk;->b:LX/8Ve;

    invoke-virtual {v4}, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel$FriendsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4}, LX/8Ve;->a(Ljava/lang/String;)LX/8Vd;

    move-result-object v4

    .line 1348977
    iput-object v4, v11, LX/8Va;->k:LX/8Vd;

    .line 1348978
    move-object v4, v11

    .line 1348979
    iget-object v11, v0, LX/8Tk;->a:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f08235b

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1348980
    iput-object v11, v4, LX/8Va;->l:Ljava/lang/String;

    .line 1348981
    move-object v4, v4

    .line 1348982
    invoke-virtual {v4}, LX/8Va;->a()LX/8Vb;

    move-result-object v4

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1348983
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_5

    :cond_a
    move-object v4, v7

    .line 1348984
    goto/16 :goto_2
.end method
