.class public final enum LX/6zT;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6LU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6zT;",
        ">;",
        "LX/6LU",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6zT;

.field public static final enum CIB:LX/6zT;

.field public static final enum MIB:LX/6zT;

.field public static final enum UNKNOWN:LX/6zT;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1161149
    new-instance v0, LX/6zT;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/6zT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6zT;->UNKNOWN:LX/6zT;

    .line 1161150
    new-instance v0, LX/6zT;

    const-string v1, "MIB"

    invoke-direct {v0, v1, v3}, LX/6zT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6zT;->MIB:LX/6zT;

    .line 1161151
    new-instance v0, LX/6zT;

    const-string v1, "CIB"

    invoke-direct {v0, v1, v4}, LX/6zT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6zT;->CIB:LX/6zT;

    .line 1161152
    const/4 v0, 0x3

    new-array v0, v0, [LX/6zT;

    sget-object v1, LX/6zT;->UNKNOWN:LX/6zT;

    aput-object v1, v0, v2

    sget-object v1, LX/6zT;->MIB:LX/6zT;

    aput-object v1, v0, v3

    sget-object v1, LX/6zT;->CIB:LX/6zT;

    aput-object v1, v0, v4

    sput-object v0, LX/6zT;->$VALUES:[LX/6zT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1161148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forValue(Ljava/lang/String;)LX/6zT;
    .locals 2

    .prologue
    .line 1161158
    invoke-static {}, LX/6zT;->values()[LX/6zT;

    move-result-object v0

    invoke-static {v0, p0}, LX/6LV;->a([LX/6LU;Ljava/lang/Object;)LX/6LU;

    move-result-object v0

    sget-object v1, LX/6zT;->UNKNOWN:LX/6zT;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6zT;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6zT;
    .locals 1

    .prologue
    .line 1161159
    const-class v0, LX/6zT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6zT;

    return-object v0
.end method

.method public static values()[LX/6zT;
    .locals 1

    .prologue
    .line 1161156
    sget-object v0, LX/6zT;->$VALUES:[LX/6zT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6zT;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1161157
    invoke-virtual {p0}, LX/6zT;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1161155
    invoke-virtual {p0}, LX/6zT;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1161154
    invoke-virtual {p0}, LX/6zT;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1161153
    invoke-virtual {p0}, LX/6zT;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
