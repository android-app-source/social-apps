.class public LX/7Sx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:Landroid/graphics/RectF;

.field public final i:LX/7Sv;

.field public final j:I

.field public final k:Z

.field public final l:Z

.field public final m:I

.field public final n:I

.field public final o:I

.field public final p:LX/7Sy;

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;"
        }
    .end annotation
.end field

.field public final r:I


# direct methods
.method public constructor <init>(IIIIIIILandroid/graphics/RectF;LX/7Sv;IIILX/7Sy;Ljava/util/List;ZIIZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIIIII",
            "Landroid/graphics/RectF;",
            "LX/7Sv;",
            "III",
            "LX/7Sy;",
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;ZIIZ)V"
        }
    .end annotation

    .prologue
    .line 1209398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1209399
    if-eqz p15, :cond_0

    if-eqz p15, :cond_1

    if-lez p16, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1209400
    iput p1, p0, LX/7Sx;->a:I

    .line 1209401
    iput p2, p0, LX/7Sx;->b:I

    .line 1209402
    iput p3, p0, LX/7Sx;->c:I

    .line 1209403
    iput p4, p0, LX/7Sx;->d:I

    .line 1209404
    iput p5, p0, LX/7Sx;->e:I

    .line 1209405
    iput p6, p0, LX/7Sx;->f:I

    .line 1209406
    iput p7, p0, LX/7Sx;->g:I

    .line 1209407
    iput-object p8, p0, LX/7Sx;->h:Landroid/graphics/RectF;

    .line 1209408
    iput-object p9, p0, LX/7Sx;->i:LX/7Sv;

    .line 1209409
    iput p10, p0, LX/7Sx;->j:I

    .line 1209410
    iput p11, p0, LX/7Sx;->n:I

    .line 1209411
    iput p12, p0, LX/7Sx;->o:I

    .line 1209412
    iput-object p13, p0, LX/7Sx;->p:LX/7Sy;

    .line 1209413
    move-object/from16 v0, p14

    iput-object v0, p0, LX/7Sx;->q:Ljava/util/List;

    .line 1209414
    move/from16 v0, p16

    iput v0, p0, LX/7Sx;->m:I

    .line 1209415
    move/from16 v0, p15

    iput-boolean v0, p0, LX/7Sx;->k:Z

    .line 1209416
    move/from16 v0, p17

    iput v0, p0, LX/7Sx;->r:I

    .line 1209417
    move/from16 v0, p18

    iput-boolean v0, p0, LX/7Sx;->l:Z

    .line 1209418
    return-void

    .line 1209419
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1209420
    iget v0, p0, LX/7Sx;->f:I

    iget v1, p0, LX/7Sx;->g:I

    add-int/2addr v0, v1

    .line 1209421
    rem-int/lit16 v0, v0, 0x168

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1209422
    const-class v0, LX/7Sx;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "sourceWidth"

    iget v2, p0, LX/7Sx;->a:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "sourceHeight"

    iget v2, p0, LX/7Sx;->b:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "sourceRotationDegreesClockwise"

    iget v2, p0, LX/7Sx;->c:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "targetWidht"

    iget v2, p0, LX/7Sx;->d:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "targetHeight"

    iget v2, p0, LX/7Sx;->e:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "targetRotationDegreesClockwise"

    iget v2, p0, LX/7Sx;->f:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "outputRotationDegreesClockwise"

    iget v2, p0, LX/7Sx;->g:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "cropRectangle"

    iget-object v2, p0, LX/7Sx;->h:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "videoMirroringMode"

    iget-object v2, p0, LX/7Sx;->i:LX/7Sv;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "bitRate"

    iget v2, p0, LX/7Sx;->j:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "frameRate"

    iget v2, p0, LX/7Sx;->n:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "iframeinterval"

    iget v2, p0, LX/7Sx;->o:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "isAudioTranscodeEnabled"

    iget-boolean v2, p0, LX/7Sx;->k:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "audioBitrate"

    iget v2, p0, LX/7Sx;->m:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "videoBitrateMode"

    iget v2, p0, LX/7Sx;->r:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "resetAudioVideoTrack"

    iget-boolean v2, p0, LX/7Sx;->l:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
