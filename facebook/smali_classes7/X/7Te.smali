.class public final LX/7Te;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "LX/7Tl;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7Tj;


# direct methods
.method public constructor <init>(LX/7Tj;Landroid/content/Context;I[LX/7Tl;)V
    .locals 0

    .prologue
    .line 1210855
    iput-object p1, p0, LX/7Te;->a:LX/7Tj;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1210856
    if-nez p2, :cond_0

    .line 1210857
    invoke-virtual {p0}, LX/7Te;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030398

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 1210858
    :cond_0
    iget-object v0, p0, LX/7Te;->a:LX/7Tj;

    iget-object v0, v0, LX/7Tj;->t:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Tl;

    .line 1210859
    const v1, 0x7f0d0b82

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1210860
    iget-object v2, v0, LX/7Tl;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1210861
    iget-object v1, p0, LX/7Te;->a:LX/7Tj;

    iget-boolean v1, v1, LX/7Tj;->a:Z

    if-eqz v1, :cond_1

    .line 1210862
    const v1, 0x7f0d0b83

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1210863
    iget-object v0, v0, LX/7Tl;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1210864
    :cond_1
    return-object p2
.end method
