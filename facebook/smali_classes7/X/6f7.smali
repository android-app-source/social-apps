.class public LX/6f7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

.field public B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

.field public C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

.field public D:Z

.field public E:Lcom/facebook/share/model/ComposerAppAttribution;

.field public F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

.field public G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/messaging/model/mms/MmsData;

.field public M:Z

.field public N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Z

.field public P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;",
            ">;"
        }
    .end annotation
.end field

.field public R:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/5ds;",
            "Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public S:Z

.field public T:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public U:I

.field public V:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/ProfileRange;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public c:J

.field public d:J

.field public e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

.field public f:Ljava/lang/String;

.field public g:J

.field public h:Z

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/attachment/Attachment;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/share/Share;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;

.field public l:LX/2uW;

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/lang/String;

.field public o:Z

.field public p:Ljava/lang/String;

.field public q:LX/6f2;

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field

.field public s:Lcom/facebook/messaging/model/share/SentShareAttachment;

.field public t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lcom/facebook/messaging/model/send/SendError;

.field public v:Lcom/facebook/messaging/model/messages/Publicity;

.field public w:LX/6f3;

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field public z:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1119200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119201
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1119202
    iput-object v0, p0, LX/6f7;->i:Ljava/util/List;

    .line 1119203
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1119204
    iput-object v0, p0, LX/6f7;->j:Ljava/util/List;

    .line 1119205
    sget-object v0, LX/2uW;->REGULAR:LX/2uW;

    iput-object v0, p0, LX/6f7;->l:LX/2uW;

    .line 1119206
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1119207
    iput-object v0, p0, LX/6f7;->m:Ljava/util/List;

    .line 1119208
    sget-object v0, LX/6f2;->API:LX/6f2;

    iput-object v0, p0, LX/6f7;->q:LX/6f2;

    .line 1119209
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1119210
    iput-object v0, p0, LX/6f7;->r:Ljava/util/List;

    .line 1119211
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/6f7;->t:Ljava/util/Map;

    .line 1119212
    sget-object v0, Lcom/facebook/messaging/model/send/SendError;->a:Lcom/facebook/messaging/model/send/SendError;

    iput-object v0, p0, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 1119213
    sget-object v0, Lcom/facebook/messaging/model/messages/Publicity;->a:Lcom/facebook/messaging/model/messages/Publicity;

    iput-object v0, p0, LX/6f7;->v:Lcom/facebook/messaging/model/messages/Publicity;

    .line 1119214
    sget-object v0, LX/6f3;->UNKNOWN:LX/6f3;

    iput-object v0, p0, LX/6f7;->w:LX/6f3;

    .line 1119215
    sget-object v0, Lcom/facebook/messaging/model/mms/MmsData;->a:Lcom/facebook/messaging/model/mms/MmsData;

    iput-object v0, p0, LX/6f7;->L:Lcom/facebook/messaging/model/mms/MmsData;

    .line 1119216
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1119217
    iput-object v0, p0, LX/6f7;->Q:Ljava/util/List;

    .line 1119218
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/6f7;->R:Ljava/util/Map;

    .line 1119219
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v0

    iput-object v0, p0, LX/6f7;->T:LX/0Xu;

    .line 1119220
    const/4 v0, -0x1

    iput v0, p0, LX/6f7;->U:I

    .line 1119221
    return-void
.end method


# virtual methods
.method public final L()Lcom/facebook/messaging/model/mms/MmsData;
    .locals 1

    .prologue
    .line 1119176
    iget-object v0, p0, LX/6f7;->L:Lcom/facebook/messaging/model/mms/MmsData;

    return-object v0
.end method

.method public final W()Lcom/facebook/messaging/model/messages/Message;
    .locals 1

    .prologue
    .line 1119177
    new-instance v0, Lcom/facebook/messaging/model/messages/Message;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/messages/Message;-><init>(LX/6f7;)V

    return-object v0
.end method

.method public final a(I)LX/6f7;
    .locals 0

    .prologue
    .line 1119178
    iput p1, p0, LX/6f7;->U:I

    .line 1119179
    return-object p0
.end method

.method public final a(J)LX/6f7;
    .locals 1

    .prologue
    .line 1119180
    iput-wide p1, p0, LX/6f7;->c:J

    .line 1119181
    return-object p0
.end method

.method public final a(LX/0Px;)LX/6f7;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/ProfileRange;",
            ">;)",
            "LX/6f7;"
        }
    .end annotation

    .prologue
    .line 1119182
    iput-object p1, p0, LX/6f7;->V:LX/0Px;

    .line 1119183
    return-object p0
.end method

.method public final a(LX/0Xu;)LX/6f7;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/UserKey;",
            ">;)",
            "LX/6f7;"
        }
    .end annotation

    .prologue
    .line 1119184
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1119185
    new-instance v0, LX/0vV;

    invoke-direct {v0, p1}, LX/0vV;-><init>(LX/0Xu;)V

    move-object v0, v0

    .line 1119186
    iput-object v0, p0, LX/6f7;->T:LX/0Xu;

    .line 1119187
    return-object p0
.end method

.method public final a(LX/2uW;)LX/6f7;
    .locals 0

    .prologue
    .line 1119188
    iput-object p1, p0, LX/6f7;->l:LX/2uW;

    .line 1119189
    return-object p0
.end method

.method public final a(LX/6f2;)LX/6f7;
    .locals 0

    .prologue
    .line 1119190
    iput-object p1, p0, LX/6f7;->q:LX/6f2;

    .line 1119191
    return-object p0
.end method

.method public final a(LX/6f3;)LX/6f7;
    .locals 1

    .prologue
    .line 1119192
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6f3;

    iput-object v0, p0, LX/6f7;->w:LX/6f3;

    .line 1119193
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)LX/6f7;
    .locals 0
    .param p1    # Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1119194
    iput-object p1, p0, LX/6f7;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    .line 1119195
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/attribution/ContentAppAttribution;)LX/6f7;
    .locals 0

    .prologue
    .line 1119196
    iput-object p1, p0, LX/6f7;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 1119197
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;)LX/6f7;
    .locals 0

    .prologue
    .line 1119198
    iput-object p1, p0, LX/6f7;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 1119199
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;
    .locals 2

    .prologue
    .line 1119222
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iput-object v0, p0, LX/6f7;->a:Ljava/lang/String;

    .line 1119223
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1119224
    iget-wide v0, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    iput-wide v0, p0, LX/6f7;->c:J

    .line 1119225
    iget-wide v0, p1, Lcom/facebook/messaging/model/messages/Message;->d:J

    iput-wide v0, p0, LX/6f7;->d:J

    .line 1119226
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iput-object v0, p0, LX/6f7;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1119227
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    iput-object v0, p0, LX/6f7;->f:Ljava/lang/String;

    .line 1119228
    iget-wide v0, p1, Lcom/facebook/messaging/model/messages/Message;->g:J

    iput-wide v0, p0, LX/6f7;->g:J

    .line 1119229
    iget-boolean v0, p1, Lcom/facebook/messaging/model/messages/Message;->h:Z

    iput-boolean v0, p0, LX/6f7;->h:Z

    .line 1119230
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    iput-object v0, p0, LX/6f7;->i:Ljava/util/List;

    .line 1119231
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    iput-object v0, p0, LX/6f7;->j:Ljava/util/List;

    .line 1119232
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    iput-object v0, p0, LX/6f7;->k:Ljava/lang/String;

    .line 1119233
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    iput-object v0, p0, LX/6f7;->l:LX/2uW;

    .line 1119234
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->m:LX/0Px;

    iput-object v0, p0, LX/6f7;->m:Ljava/util/List;

    .line 1119235
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    iput-object v0, p0, LX/6f7;->n:Ljava/lang/String;

    .line 1119236
    iget-boolean v0, p1, Lcom/facebook/messaging/model/messages/Message;->o:Z

    iput-boolean v0, p0, LX/6f7;->o:Z

    .line 1119237
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->p:Ljava/lang/String;

    iput-object v0, p0, LX/6f7;->p:Ljava/lang/String;

    .line 1119238
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->q:LX/6f2;

    iput-object v0, p0, LX/6f7;->q:LX/6f2;

    .line 1119239
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->r:LX/6f3;

    iput-object v0, p0, LX/6f7;->w:LX/6f3;

    .line 1119240
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    iput-object v0, p0, LX/6f7;->r:Ljava/util/List;

    .line 1119241
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iput-object v0, p0, LX/6f7;->s:Lcom/facebook/messaging/model/share/SentShareAttachment;

    .line 1119242
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    invoke-static {v0}, LX/0PM;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/6f7;->t:Ljava/util/Map;

    .line 1119243
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iput-object v0, p0, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 1119244
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->s:Lcom/facebook/messaging/model/messages/Publicity;

    iput-object v0, p0, LX/6f7;->v:Lcom/facebook/messaging/model/messages/Publicity;

    .line 1119245
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->x:Ljava/lang/String;

    iput-object v0, p0, LX/6f7;->x:Ljava/lang/String;

    .line 1119246
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->y:Ljava/lang/String;

    iput-object v0, p0, LX/6f7;->y:Ljava/lang/String;

    .line 1119247
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->z:LX/0P1;

    iput-object v0, p0, LX/6f7;->z:Ljava/util/Map;

    .line 1119248
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iput-object v0, p0, LX/6f7;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    .line 1119249
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    iput-object v0, p0, LX/6f7;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    .line 1119250
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    iput-object v0, p0, LX/6f7;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    .line 1119251
    iget-boolean v0, p1, Lcom/facebook/messaging/model/messages/Message;->D:Z

    iput-boolean v0, p0, LX/6f7;->D:Z

    .line 1119252
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, LX/6f7;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1119253
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    iput-object v0, p0, LX/6f7;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 1119254
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    iput-object v0, p0, LX/6f7;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    .line 1119255
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    iput-object v0, p0, LX/6f7;->H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    .line 1119256
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    iput-object v0, p0, LX/6f7;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 1119257
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    iput-object v0, p0, LX/6f7;->J:Ljava/lang/Integer;

    .line 1119258
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->K:Ljava/lang/Long;

    iput-object v0, p0, LX/6f7;->K:Ljava/lang/Long;

    .line 1119259
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    iput-object v0, p0, LX/6f7;->L:Lcom/facebook/messaging/model/mms/MmsData;

    .line 1119260
    iget-boolean v0, p1, Lcom/facebook/messaging/model/messages/Message;->M:Z

    iput-boolean v0, p0, LX/6f7;->M:Z

    .line 1119261
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->N:Ljava/lang/String;

    iput-object v0, p0, LX/6f7;->N:Ljava/lang/String;

    .line 1119262
    iget-boolean v0, p1, Lcom/facebook/messaging/model/messages/Message;->O:Z

    iput-boolean v0, p0, LX/6f7;->O:Z

    .line 1119263
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->P:Ljava/lang/String;

    iput-object v0, p0, LX/6f7;->P:Ljava/lang/String;

    .line 1119264
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->Q:LX/0Px;

    iput-object v0, p0, LX/6f7;->Q:Ljava/util/List;

    .line 1119265
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    iput-object v0, p0, LX/6f7;->R:Ljava/util/Map;

    .line 1119266
    iget-boolean v0, p1, Lcom/facebook/messaging/model/messages/Message;->S:Z

    iput-boolean v0, p0, LX/6f7;->S:Z

    .line 1119267
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->T:LX/18f;

    iput-object v0, p0, LX/6f7;->T:LX/0Xu;

    .line 1119268
    iget v0, p1, Lcom/facebook/messaging/model/messages/Message;->V:I

    iput v0, p0, LX/6f7;->U:I

    .line 1119269
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->U:LX/0Px;

    iput-object v0, p0, LX/6f7;->V:LX/0Px;

    .line 1119270
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6f7;
    .locals 0
    .param p1    # Lcom/facebook/messaging/model/messages/ParticipantInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1119271
    iput-object p1, p0, LX/6f7;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1119272
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Publicity;)LX/6f7;
    .locals 0

    .prologue
    .line 1119273
    iput-object p1, p0, LX/6f7;->v:Lcom/facebook/messaging/model/messages/Publicity;

    .line 1119274
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/mms/MmsData;)LX/6f7;
    .locals 1

    .prologue
    .line 1119275
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1119276
    iput-object p1, p0, LX/6f7;->L:Lcom/facebook/messaging/model/mms/MmsData;

    .line 1119277
    return-object p0

    .line 1119278
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/send/SendError;)LX/6f7;
    .locals 0

    .prologue
    .line 1119279
    iput-object p1, p0, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 1119280
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/6f7;
    .locals 0

    .prologue
    .line 1119281
    iput-object p1, p0, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1119282
    return-object p0
.end method

.method public final a(Ljava/lang/Integer;)LX/6f7;
    .locals 0

    .prologue
    .line 1119283
    iput-object p1, p0, LX/6f7;->J:Ljava/lang/Integer;

    .line 1119284
    return-object p0
.end method

.method public final a(Ljava/lang/Long;)LX/6f7;
    .locals 0
    .param p1    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1119285
    iput-object p1, p0, LX/6f7;->K:Ljava/lang/Long;

    .line 1119286
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/6f7;
    .locals 2

    .prologue
    .line 1119170
    if-eqz p1, :cond_0

    const-string v0, "m_mid"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1119171
    const-string v0, "MessageBuilder"

    const-string v1, "Encountered a legacy message id."

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1119172
    :cond_0
    iput-object p1, p0, LX/6f7;->a:Ljava/lang/String;

    .line 1119173
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/6f7;
    .locals 1

    .prologue
    .line 1119174
    iget-object v0, p0, LX/6f7;->t:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1119175
    return-object p0
.end method

.method public final a(Ljava/util/List;)LX/6f7;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/attachment/Attachment;",
            ">;)",
            "LX/6f7;"
        }
    .end annotation

    .prologue
    .line 1119123
    iput-object p1, p0, LX/6f7;->i:Ljava/util/List;

    .line 1119124
    return-object p0
.end method

.method public final a(Ljava/util/Map;)LX/6f7;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/6f7;"
        }
    .end annotation

    .prologue
    .line 1119125
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1119126
    invoke-static {p1}, LX/0PM;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/6f7;->t:Ljava/util/Map;

    .line 1119127
    return-object p0
.end method

.method public final a(Z)LX/6f7;
    .locals 0

    .prologue
    .line 1119128
    iput-boolean p1, p0, LX/6f7;->h:Z

    .line 1119129
    return-object p0
.end method

.method public final b(J)LX/6f7;
    .locals 1

    .prologue
    .line 1119130
    iput-wide p1, p0, LX/6f7;->d:J

    .line 1119131
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/6f7;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1119132
    iput-object p1, p0, LX/6f7;->f:Ljava/lang/String;

    .line 1119133
    return-object p0
.end method

.method public final b(Ljava/util/Map;)LX/6f7;
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/5ds;",
            "Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;",
            ">;)",
            "LX/6f7;"
        }
    .end annotation

    .prologue
    .line 1119134
    iput-object p1, p0, LX/6f7;->R:Ljava/util/Map;

    .line 1119135
    iget-object v0, p0, LX/6f7;->R:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 1119136
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/6f7;->R:Ljava/util/Map;

    .line 1119137
    :cond_0
    return-object p0
.end method

.method public final b(Z)LX/6f7;
    .locals 0

    .prologue
    .line 1119138
    iput-boolean p1, p0, LX/6f7;->o:Z

    .line 1119139
    return-object p0
.end method

.method public final c(J)LX/6f7;
    .locals 1

    .prologue
    .line 1119140
    iput-wide p1, p0, LX/6f7;->g:J

    .line 1119141
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/6f7;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1119142
    iput-object p1, p0, LX/6f7;->k:Ljava/lang/String;

    .line 1119143
    return-object p0
.end method

.method public final c(Ljava/util/List;)LX/6f7;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;)",
            "LX/6f7;"
        }
    .end annotation

    .prologue
    .line 1119144
    iput-object p1, p0, LX/6f7;->m:Ljava/util/List;

    .line 1119145
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/6f7;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1119168
    iput-object p1, p0, LX/6f7;->n:Ljava/lang/String;

    .line 1119169
    return-object p0
.end method

.method public final d(Ljava/util/List;)LX/6f7;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;)",
            "LX/6f7;"
        }
    .end annotation

    .prologue
    .line 1119146
    iput-object p1, p0, LX/6f7;->r:Ljava/util/List;

    .line 1119147
    return-object p0
.end method

.method public final d(Z)LX/6f7;
    .locals 0

    .prologue
    .line 1119148
    iput-boolean p1, p0, LX/6f7;->M:Z

    .line 1119149
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/6f7;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1119150
    iput-object p1, p0, LX/6f7;->p:Ljava/lang/String;

    .line 1119151
    return-object p0
.end method

.method public final e(Ljava/util/List;)LX/6f7;
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;",
            ">;)",
            "LX/6f7;"
        }
    .end annotation

    .prologue
    .line 1119152
    if-nez p1, :cond_0

    .line 1119153
    sget-object p1, LX/0Q7;->a:LX/0Px;

    move-object p1, p1

    .line 1119154
    :cond_0
    iput-object p1, p0, LX/6f7;->Q:Ljava/util/List;

    .line 1119155
    return-object p0
.end method

.method public final e(Z)LX/6f7;
    .locals 0

    .prologue
    .line 1119156
    iput-boolean p1, p0, LX/6f7;->O:Z

    .line 1119157
    return-object p0
.end method

.method public final e()Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .locals 1

    .prologue
    .line 1119158
    iget-object v0, p0, LX/6f7;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)LX/6f7;
    .locals 0

    .prologue
    .line 1119159
    iput-object p1, p0, LX/6f7;->N:Ljava/lang/String;

    .line 1119160
    return-object p0
.end method

.method public final f(Z)LX/6f7;
    .locals 0

    .prologue
    .line 1119161
    iput-boolean p1, p0, LX/6f7;->S:Z

    .line 1119162
    return-object p0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1119163
    iget-object v0, p0, LX/6f7;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)LX/6f7;
    .locals 0

    .prologue
    .line 1119164
    iput-object p1, p0, LX/6f7;->P:Ljava/lang/String;

    .line 1119165
    return-object p0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1119166
    iget-object v0, p0, LX/6f7;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final l()LX/2uW;
    .locals 1

    .prologue
    .line 1119167
    iget-object v0, p0, LX/6f7;->l:LX/2uW;

    return-object v0
.end method
