.class public final LX/8CE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/doodle/ColourPicker;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/doodle/ColourPicker;)V
    .locals 0

    .prologue
    .line 1310547
    iput-object p1, p0, LX/8CE;->a:Lcom/facebook/messaging/doodle/ColourPicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/messaging/doodle/ColourPicker;B)V
    .locals 0

    .prologue
    .line 1310548
    invoke-direct {p0, p1}, LX/8CE;-><init>(Lcom/facebook/messaging/doodle/ColourPicker;)V

    return-void
.end method

.method public static a(IIF)I
    .locals 4

    .prologue
    .line 1310549
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    .line 1310550
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v1

    .line 1310551
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    .line 1310552
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v3

    sub-int/2addr v3, v0

    int-to-float v3, v3

    mul-float/2addr v3, p2

    float-to-int v3, v3

    add-int/2addr v0, v3

    .line 1310553
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v3

    sub-int/2addr v3, v1

    int-to-float v3, v3

    mul-float/2addr v3, p2

    float-to-int v3, v3

    add-int/2addr v1, v3

    .line 1310554
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    sub-int/2addr v3, v2

    int-to-float v3, v3

    mul-float/2addr v3, p2

    float-to-int v3, v3

    add-int/2addr v2, v3

    .line 1310555
    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    return v0
.end method
