.class public final LX/6fF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6eo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6eo",
        "<",
        "Lcom/facebook/messaging/model/messages/MessengerCartInfoProperties;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1119620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;
    .locals 2

    .prologue
    .line 1119621
    const-string v0, "item_count"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "call_to_action"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/messages/MessengerCartInfoProperties;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/MessengerCartInfoProperties;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lorg/json/JSONObject;)Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;
    .locals 2

    .prologue
    .line 1119622
    :try_start_0
    const-string v0, "item_count"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "call_to_action"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/messages/MessengerCartInfoProperties;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/MessengerCartInfoProperties;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1119623
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1119624
    new-instance v1, Lcom/facebook/messaging/model/messages/MessengerCartInfoProperties;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const-class v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-direct {v1, v2, v0}, Lcom/facebook/messaging/model/messages/MessengerCartInfoProperties;-><init>(ILcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)V

    return-object v1
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1119625
    new-array v0, p1, [Lcom/facebook/messaging/model/messages/MessengerCartInfoProperties;

    return-object v0
.end method
