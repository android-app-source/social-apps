.class public final LX/72J;
.super LX/6qh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)V
    .locals 0

    .prologue
    .line 1164227
    iput-object p1, p0, LX/72J;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    invoke-direct {p0}, LX/6qh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/73T;)V
    .locals 3

    .prologue
    .line 1164228
    iget-object v0, p0, LX/72J;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    const/4 p0, -0x1

    .line 1164229
    sget-object v1, LX/72P;->a:[I

    .line 1164230
    iget-object v2, p1, LX/73T;->a:LX/73S;

    move-object v2, v2

    .line 1164231
    invoke-virtual {v2}, LX/73S;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1164232
    :cond_0
    :goto_0
    return-void

    .line 1164233
    :pswitch_0
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    .line 1164234
    if-eqz v2, :cond_0

    .line 1164235
    const-string v1, "extra_activity_result_data"

    invoke-virtual {p1, v1}, LX/73T;->a(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 1164236
    if-eqz v1, :cond_1

    .line 1164237
    invoke-virtual {v2, p0, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1164238
    :goto_1
    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 1164239
    :cond_1
    invoke-virtual {v2, p0}, Landroid/app/Activity;->setResult(I)V

    goto :goto_1

    .line 1164240
    :pswitch_1
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->e:LX/72U;

    iget-object v2, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->a:LX/72g;

    invoke-virtual {v1, v2}, LX/72U;->a(LX/72g;)LX/72T;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->z:LX/6qh;

    .line 1164241
    iput-object v2, v1, LX/72T;->a:LX/6qh;

    .line 1164242
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->e:LX/72U;

    iget-object v2, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->a:LX/72g;

    invoke-virtual {v1, v2}, LX/72U;->a(LX/72g;)LX/72T;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-static {v0}, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->q(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    move-result-object p0

    invoke-virtual {v1, v2, p1, p0}, LX/72T;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/73T;Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V
    .locals 2

    .prologue
    .line 1164243
    iget-object v0, p0, LX/72J;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "shipping_dialog_fragment_tag"

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1164244
    return-void
.end method

.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;Z)V
    .locals 3

    .prologue
    .line 1164245
    iget-object v0, p0, LX/72J;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    .line 1164246
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 1164247
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1164248
    :cond_0
    iput-object p1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1164249
    if-nez p2, :cond_1

    .line 1164250
    :goto_0
    return-void

    .line 1164251
    :cond_1
    invoke-static {v0}, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->o(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)V

    .line 1164252
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/72O;

    invoke-direct {v2, v0}, LX/72O;-><init>(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)V

    iget-object p0, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
