.class public final LX/8L3;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "LX/8Kk;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8L4;


# direct methods
.method public constructor <init>(LX/8L4;)V
    .locals 0

    .prologue
    .line 1331102
    iput-object p1, p0, LX/8L3;->a:LX/8L4;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/8Kk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1331103
    const-class v0, LX/8Kk;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 8

    .prologue
    .line 1331104
    check-cast p1, LX/8Kk;

    const/high16 v6, 0x41200000    # 10.0f

    .line 1331105
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 1331106
    invoke-virtual {v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1331107
    iget v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->O:I

    move v1, v1

    .line 1331108
    iget v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->P:I

    move v2, v2

    .line 1331109
    iget-object v3, p1, LX/0b5;->b:LX/8KZ;

    move-object v3, v3

    .line 1331110
    sget-object v4, LX/8KZ;->PROCESSING:LX/8KZ;

    if-ne v3, v4, :cond_1

    .line 1331111
    :cond_0
    :goto_0
    return-void

    .line 1331112
    :cond_1
    mul-int/lit8 v3, v1, 0x64

    int-to-float v3, v3

    .line 1331113
    iget v4, p1, LX/0b5;->c:F

    move v4, v4

    .line 1331114
    add-float/2addr v3, v4

    int-to-float v4, v2

    div-float/2addr v3, v4

    .line 1331115
    iget-object v4, p0, LX/8L3;->a:LX/8L4;

    iget-object v4, v4, LX/8L4;->f:LX/8Ko;

    iget-object v5, p0, LX/8L3;->a:LX/8L4;

    iget-object v5, v5, LX/8L4;->b:Landroid/content/Context;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v4, v5, v1, v2}, LX/8Ko;->a(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v1

    .line 1331116
    iget-object v2, p0, LX/8L3;->a:LX/8L4;

    iget-object v2, v2, LX/8L4;->q:LX/8L0;

    float-to-int v4, v3

    iget-object v5, p0, LX/8L3;->a:LX/8L4;

    invoke-static {v5, v1, v0}, LX/8L4;->a$redex0(LX/8L4;Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, LX/8L3;->a:LX/8L4;

    invoke-static {v5, v0}, LX/8L4;->g(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v2, p1, v4, v1, v5}, LX/8L0;->a(LX/0b5;ILjava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v1

    .line 1331117
    iget-object v2, p0, LX/8L3;->a:LX/8L4;

    invoke-static {v2, v0, v1}, LX/8L4;->a$redex0(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/app/Notification;)V

    .line 1331118
    mul-float v1, v3, v6

    float-to-int v1, v1

    .line 1331119
    iget-object v2, p0, LX/8L3;->a:LX/8L4;

    invoke-static {v2, v0, v1}, LX/8L4;->a$redex0(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;I)V

    goto :goto_0

    .line 1331120
    :cond_2
    const/high16 v4, 0x42c80000    # 100.0f

    .line 1331121
    iget v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->Y:F

    iget v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->Y:F

    sub-float v2, v4, v2

    iget v3, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->Z:F

    mul-float/2addr v2, v3

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    move v1, v1

    .line 1331122
    iget-boolean v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v2, v2

    .line 1331123
    if-nez v2, :cond_3

    .line 1331124
    iget-object v2, p0, LX/8L3;->a:LX/8L4;

    iget-object v2, v2, LX/8L4;->q:LX/8L0;

    float-to-int v3, v1

    .line 1331125
    iget-object v5, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->d:Ljava/lang/String;

    move-object v5, v5

    .line 1331126
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1331127
    const/4 v5, 0x0

    .line 1331128
    :goto_1
    move-object v5, v5

    .line 1331129
    move-object v4, v5

    .line 1331130
    iget-object v5, p0, LX/8L3;->a:LX/8L4;

    invoke-static {v5, v0}, LX/8L4;->g(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v2, p1, v3, v4, v5}, LX/8L0;->a(LX/0b5;ILjava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v2

    .line 1331131
    iget-object v3, p0, LX/8L3;->a:LX/8L4;

    invoke-static {v3, v0, v2}, LX/8L4;->a$redex0(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/app/Notification;)V

    .line 1331132
    :cond_3
    iget-object v2, p1, LX/0b5;->b:LX/8KZ;

    move-object v2, v2

    .line 1331133
    sget-object v3, LX/8KZ;->UPLOADING:LX/8KZ;

    if-eq v2, v3, :cond_4

    sget-object v3, LX/8KZ;->PROCESSING:LX/8KZ;

    if-ne v2, v3, :cond_0

    .line 1331134
    :cond_4
    mul-float/2addr v1, v6

    float-to-int v1, v1

    .line 1331135
    iget-object v2, p0, LX/8L3;->a:LX/8L4;

    invoke-static {v2, v0, v1}, LX/8L4;->a$redex0(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;I)V

    goto :goto_0

    .line 1331136
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v7, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1331137
    invoke-static {v5, v7}, LX/8L4;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1331138
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method
