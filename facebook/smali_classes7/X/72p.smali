.class public LX/72p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6w3;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6w3",
        "<",
        "Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerRunTimeData;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1164980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164981
    return-void
.end method


# virtual methods
.method public final a(LX/6qh;LX/70k;)V
    .locals 0

    .prologue
    .line 1164982
    return-void
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1164983
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shipping Option picker screen does not start any activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
