.class public LX/8TC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1348148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/8TC;
    .locals 1

    .prologue
    .line 1348149
    new-instance v0, LX/8TC;

    invoke-direct {v0}, LX/8TC;-><init>()V

    .line 1348150
    move-object v0, v0

    .line 1348151
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Ljava/util/List;)LX/5OM;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "LX/8To;",
            ">;)",
            "LX/5OM;"
        }
    .end annotation

    .prologue
    .line 1348152
    new-instance v1, LX/6WS;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 1348153
    invoke-virtual {v1, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1348154
    sget-object v0, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v1, v0}, LX/0ht;->a(LX/3AV;)V

    .line 1348155
    invoke-virtual {v1}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 1348156
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8To;

    .line 1348157
    iget v4, v0, LX/8To;->a:I

    move v4, v4

    .line 1348158
    const/4 v5, 0x0

    .line 1348159
    iget-object v6, v0, LX/8To;->b:Ljava/lang/String;

    move-object v6, v6

    .line 1348160
    invoke-virtual {v2, v4, v5, v6}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v4

    .line 1348161
    iget-object v5, v0, LX/8To;->c:Ljava/lang/Integer;

    move-object v5, v5

    .line 1348162
    if-eqz v5, :cond_0

    .line 1348163
    iget-object v5, v0, LX/8To;->c:Ljava/lang/Integer;

    move-object v0, v5

    .line 1348164
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 1348165
    :cond_1
    new-instance v0, LX/8TB;

    invoke-direct {v0, p0, v1}, LX/8TB;-><init>(LX/8TC;LX/5OM;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1348166
    return-object v1
.end method
