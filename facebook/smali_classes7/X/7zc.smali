.class public LX/7zc;
.super LX/2oV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/3FR;",
        ">",
        "LX/2oV",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:LX/2oM;

.field private final b:LX/2oO;

.field public final c:LX/093;

.field public final d:Lcom/facebook/video/engine/VideoPlayerParams;

.field public final e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

.field public final f:LX/04D;

.field public final g:LX/1Bv;

.field public final h:LX/1C2;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7zb;",
            ">;"
        }
    .end annotation
.end field

.field private j:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/2oM;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;LX/1Bv;LX/1C2;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/2oM;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/093;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/video/engine/VideoPlayerParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/video/analytics/VideoFeedStoryInfo;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/04D;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1281266
    invoke-direct {p0, p1}, LX/2oV;-><init>(Ljava/lang/String;)V

    .line 1281267
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7zc;->j:Z

    .line 1281268
    iput-object p2, p0, LX/7zc;->a:LX/2oM;

    .line 1281269
    invoke-interface {p2}, LX/2oM;->b()LX/2oO;

    move-result-object v0

    iput-object v0, p0, LX/7zc;->b:LX/2oO;

    .line 1281270
    iput-object p3, p0, LX/7zc;->c:LX/093;

    .line 1281271
    iput-object p4, p0, LX/7zc;->d:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1281272
    iput-object p5, p0, LX/7zc;->e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1281273
    iput-object p6, p0, LX/7zc;->f:LX/04D;

    .line 1281274
    iput-object p7, p0, LX/7zc;->g:LX/1Bv;

    .line 1281275
    iput-object p8, p0, LX/7zc;->h:LX/1C2;

    .line 1281276
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/7zc;->i:Ljava/util/List;

    .line 1281277
    return-void
.end method

.method private a(Landroid/view/View;LX/04g;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "LX/04g;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1281259
    iget-boolean v0, p0, LX/7zc;->j:Z

    if-nez v0, :cond_0

    .line 1281260
    :goto_0
    return-void

    .line 1281261
    :cond_0
    iput-boolean v2, p0, LX/7zc;->j:Z

    .line 1281262
    iget-object v1, p0, LX/7zc;->a:LX/2oM;

    move-object v0, p1

    check-cast v0, LX/3FR;

    invoke-interface {v0}, LX/3FR;->getSeekPosition()I

    move-result v0

    invoke-interface {v1, v0}, LX/2oM;->a(I)V

    .line 1281263
    iget-object v0, p0, LX/7zc;->c:LX/093;

    invoke-virtual {v0, v2}, LX/093;->a(Z)V

    .line 1281264
    invoke-direct {p0, p1, p2}, LX/7zc;->b(Landroid/view/View;LX/04g;)V

    .line 1281265
    check-cast p1, LX/3FR;

    invoke-interface {p1}, LX/3FR;->y()V

    goto :goto_0
.end method

.method private b(Landroid/view/View;LX/04g;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "LX/04g;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1281255
    check-cast p1, LX/3FR;

    invoke-interface {p1, p2}, LX/3FR;->b(LX/04g;)V

    .line 1281256
    iget-object v0, p0, LX/7zc;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7zb;

    .line 1281257
    invoke-interface {v0}, LX/7zb;->b()V

    goto :goto_0

    .line 1281258
    :cond_0
    return-void
.end method

.method private c(Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 1281278
    iget-object v0, p0, LX/7zc;->a:LX/2oM;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-interface {v0, v1}, LX/2oM;->a(LX/04g;)V

    move-object v0, p1

    .line 1281279
    check-cast v0, LX/3FR;

    invoke-interface {v0}, LX/3FR;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 1281280
    iget-object v0, p0, LX/7zc;->e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    .line 1281281
    iput-object v1, v0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    .line 1281282
    check-cast p1, LX/3FR;

    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    iget-object v1, p0, LX/7zc;->a:LX/2oM;

    invoke-interface {v1}, LX/2oM;->a()I

    move-result v1

    invoke-interface {p1, v0, v1}, LX/3FR;->a(LX/04g;I)V

    .line 1281283
    iget-object v0, p0, LX/7zc;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7zb;

    .line 1281284
    invoke-interface {v0}, LX/7zb;->a()V

    goto :goto_0

    .line 1281285
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/7zb;)V
    .locals 1

    .prologue
    .line 1281253
    iget-object v0, p0, LX/7zc;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1281254
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1281250
    check-cast p1, Landroid/view/View;

    .line 1281251
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-direct {p0, p1, v0}, LX/7zc;->a(Landroid/view/View;LX/04g;)V

    .line 1281252
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/04g;)V
    .locals 0

    .prologue
    .line 1281249
    check-cast p1, Landroid/view/View;

    invoke-direct {p0, p1, p2}, LX/7zc;->a(Landroid/view/View;LX/04g;)V

    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1281230
    check-cast p1, Landroid/view/View;

    const/4 v0, 0x1

    .line 1281231
    iget-boolean v1, p0, LX/7zc;->j:Z

    if-eqz v1, :cond_0

    .line 1281232
    :goto_0
    return-void

    .line 1281233
    :cond_0
    iput-boolean v0, p0, LX/7zc;->j:Z

    .line 1281234
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1281235
    iget-object v2, p0, LX/7zc;->b:LX/2oO;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/7zc;->b:LX/2oO;

    invoke-virtual {v2, v1}, LX/2oO;->a(Ljava/util/Set;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1281236
    :goto_1
    iget-object v4, p0, LX/7zc;->c:LX/093;

    iget-object v5, p0, LX/7zc;->g:LX/1Bv;

    invoke-virtual {v5}, LX/1Bv;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, LX/093;->a(Ljava/util/Set;Ljava/lang/String;)V

    .line 1281237
    iget-object v4, p0, LX/7zc;->h:LX/1C2;

    iget-object v5, p0, LX/7zc;->c:LX/093;

    iget-object v6, p0, LX/7zc;->d:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v6, v6, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    .line 1281238
    iget-object v7, p0, LX/2oV;->a:Ljava/lang/String;

    move-object v7, v7

    .line 1281239
    iget-object v8, p0, LX/7zc;->f:LX/04D;

    iget-object v9, p0, LX/7zc;->e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1281240
    iget-object v10, v9, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    move-object v9, v10

    .line 1281241
    iget-object v10, p0, LX/7zc;->d:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v4 .. v10}, LX/1C2;->a(LX/093;LX/0lF;Ljava/lang/String;LX/04D;LX/04H;LX/098;)LX/1C2;

    .line 1281242
    iget-object v4, p0, LX/7zc;->c:LX/093;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/093;->a(Z)V

    .line 1281243
    if-eqz v0, :cond_3

    .line 1281244
    invoke-direct {p0, p1}, LX/7zc;->c(Landroid/view/View;)V

    .line 1281245
    :cond_1
    check-cast p1, LX/3FR;

    invoke-interface {p1}, LX/3FR;->x()V

    goto :goto_0

    .line 1281246
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1281247
    :cond_3
    iget-object v0, p0, LX/7zc;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7zb;

    .line 1281248
    iget-object v3, p0, LX/7zc;->b:LX/2oO;

    invoke-interface {v0, v1, v3}, LX/7zb;->a(Ljava/util/Set;LX/2oO;)V

    goto :goto_2
.end method
