.class public LX/8Oo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4ck;
.implements LX/2BD;


# instance fields
.field public final a:Lcom/facebook/photos/upload/operation/UploadOperation;

.field public final b:LX/73w;

.field public final c:LX/74b;

.field public final d:LX/8OL;

.field public final e:LX/0b3;

.field private final f:LX/0SG;

.field private g:J

.field private h:I

.field public i:J

.field private j:Ljava/util/concurrent/atomic/AtomicLong;

.field public k:I

.field public l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field public n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/8Nx;",
            ">;"
        }
    .end annotation
.end field

.field private o:Z

.field public p:Z


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/74b;LX/8OL;LX/0b3;LX/0SG;Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1341091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341092
    iput-wide v2, p0, LX/8Oo;->g:J

    .line 1341093
    iput v1, p0, LX/8Oo;->h:I

    .line 1341094
    iput-wide v2, p0, LX/8Oo;->i:J

    .line 1341095
    iput v1, p0, LX/8Oo;->k:I

    .line 1341096
    iput-object p1, p0, LX/8Oo;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1341097
    iput-object p2, p0, LX/8Oo;->b:LX/73w;

    .line 1341098
    iput-object p3, p0, LX/8Oo;->c:LX/74b;

    .line 1341099
    iput-object p4, p0, LX/8Oo;->d:LX/8OL;

    .line 1341100
    iput-object p5, p0, LX/8Oo;->e:LX/0b3;

    .line 1341101
    iput-object p6, p0, LX/8Oo;->f:LX/0SG;

    .line 1341102
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LX/8Oo;->j:Ljava/util/concurrent/atomic/AtomicLong;

    .line 1341103
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/8Oo;->n:Ljava/util/Map;

    .line 1341104
    iput-boolean v1, p0, LX/8Oo;->o:Z

    .line 1341105
    iput-boolean p7, p0, LX/8Oo;->p:Z

    .line 1341106
    return-void
.end method

.method private declared-synchronized b(Ljava/lang/String;JJ)V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    .line 1341078
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8Oo;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Nx;

    .line 1341079
    if-eqz v0, :cond_1

    .line 1341080
    iget-object v1, p0, LX/8Oo;->j:Ljava/util/concurrent/atomic/AtomicLong;

    iget-wide v2, v0, LX/8Nx;->a:J

    sub-long v2, p2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    move-result-wide v2

    .line 1341081
    iput-wide p2, v0, LX/8Nx;->a:J

    .line 1341082
    iput-wide p4, v0, LX/8Nx;->b:J

    .line 1341083
    iget-object v0, p0, LX/8Oo;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 1341084
    iget-wide v0, p0, LX/8Oo;->g:J

    sub-long v0, v4, v0

    const-wide/16 v6, 0x3e8

    cmp-long v0, v0, v6

    if-gez v0, :cond_0

    cmp-long v0, p2, p4

    if-ltz v0, :cond_1

    .line 1341085
    :cond_0
    iget-boolean v0, p0, LX/8Oo;->p:Z

    if-eqz v0, :cond_2

    long-to-double v0, v2

    mul-double/2addr v0, v8

    iget-wide v2, p0, LX/8Oo;->i:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 1341086
    :goto_0
    iget-object v1, p0, LX/8Oo;->e:LX/0b3;

    new-instance v2, LX/8Kk;

    iget-object v3, p0, LX/8Oo;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    sget-object v6, LX/8KZ;->UPLOADING:LX/8KZ;

    invoke-direct {v2, v3, v6, v0}, LX/8Kk;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1341087
    iput-wide v4, p0, LX/8Oo;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1341088
    :cond_1
    monitor-exit p0

    return-void

    .line 1341089
    :cond_2
    long-to-double v0, v2

    mul-double/2addr v0, v8

    :try_start_1
    iget-wide v2, p0, LX/8Oo;->i:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    long-to-double v2, v2

    div-double/2addr v0, v2

    double-to-int v0, v0

    int-to-float v0, v0

    goto :goto_0

    .line 1341090
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1341076
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8Oo;->o:Z

    .line 1341077
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 1341074
    iget-object v0, p0, LX/8Oo;->m:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2}, LX/8Oo;->a(Ljava/lang/String;J)V

    .line 1341075
    return-void
.end method

.method public final a(JJ)V
    .locals 7

    .prologue
    .line 1341072
    iget-object v1, p0, LX/8Oo;->m:Ljava/lang/String;

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, LX/8Oo;->a(Ljava/lang/String;JJ)V

    .line 1341073
    return-void
.end method

.method public final a(LX/73z;)V
    .locals 8

    .prologue
    .line 1341024
    iget-object v0, p0, LX/8Oo;->b:LX/73w;

    iget-object v1, p0, LX/8Oo;->c:LX/74b;

    iget-object v2, p0, LX/8Oo;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    iget-wide v4, p0, LX/8Oo;->i:J

    iget v6, p0, LX/8Oo;->k:I

    move-object v7, p1

    invoke-virtual/range {v0 .. v7}, LX/73w;->a(LX/74b;JJILX/73y;)V

    .line 1341025
    return-void
.end method

.method public final a(LX/73z;LX/8Oi;LX/8O2;)V
    .locals 11

    .prologue
    .line 1341053
    iget-object v0, p0, LX/8Oo;->d:LX/8OL;

    .line 1341054
    iget-boolean v1, v0, LX/8OL;->d:Z

    move v0, v1

    .line 1341055
    if-eqz v0, :cond_1

    .line 1341056
    iget-object v0, p0, LX/8Oo;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1341057
    iget-object v1, p0, LX/8Oo;->b:LX/73w;

    iget-object v2, p0, LX/8Oo;->c:LX/74b;

    iget-object v3, p0, LX/8Oo;->l:Ljava/lang/String;

    iget-object v0, p0, LX/8Oo;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    iget-wide v6, p0, LX/8Oo;->i:J

    iget v8, p0, LX/8Oo;->k:I

    move-object v9, p3

    invoke-virtual/range {v1 .. v9}, LX/73w;->a(LX/74b;Ljava/lang/String;JJILX/8O2;)V

    .line 1341058
    iget-object v0, p0, LX/8Oo;->b:LX/73w;

    iget-object v1, p0, LX/8Oo;->c:LX/74b;

    iget-object v2, p0, LX/8Oo;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    iget-wide v4, p0, LX/8Oo;->i:J

    iget v6, p0, LX/8Oo;->k:I

    invoke-virtual/range {v0 .. v6}, LX/73w;->a(LX/74b;JJI)V

    .line 1341059
    :cond_0
    :goto_0
    return-void

    .line 1341060
    :cond_1
    if-eqz p1, :cond_0

    .line 1341061
    if-eqz p3, :cond_2

    .line 1341062
    iget-object v0, p0, LX/8Oo;->n:Ljava/util/Map;

    iget-object v1, p3, LX/8O2;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Nx;

    .line 1341063
    if-eqz v0, :cond_2

    .line 1341064
    iget-object v1, p0, LX/8Oo;->j:Ljava/util/concurrent/atomic/AtomicLong;

    iget-wide v2, v0, LX/8Nx;->a:J

    neg-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->getAndAdd(J)J

    .line 1341065
    iget-object v0, p0, LX/8Oo;->n:Ljava/util/Map;

    iget-object v1, p3, LX/8O2;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1341066
    :cond_2
    sget-object v0, LX/8Oi;->START:LX/8Oi;

    if-ne p2, v0, :cond_3

    .line 1341067
    iget-object v0, p0, LX/8Oo;->b:LX/73w;

    iget-object v1, p0, LX/8Oo;->c:LX/74b;

    iget-object v2, p0, LX/8Oo;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0, v1, p1, v2}, LX/73w;->a(LX/74b;LX/73y;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto :goto_0

    .line 1341068
    :cond_3
    sget-object v0, LX/8Oi;->RECEIVE:LX/8Oi;

    if-ne p2, v0, :cond_4

    .line 1341069
    iget-object v1, p0, LX/8Oo;->b:LX/73w;

    iget-object v2, p0, LX/8Oo;->c:LX/74b;

    iget-object v3, p0, LX/8Oo;->l:Ljava/lang/String;

    iget-object v0, p0, LX/8Oo;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    iget-wide v6, p0, LX/8Oo;->i:J

    iget v8, p0, LX/8Oo;->k:I

    move-object v9, p3

    move-object v10, p1

    invoke-virtual/range {v1 .. v10}, LX/73w;->a(LX/74b;Ljava/lang/String;JJILX/8O2;LX/73y;)V

    goto :goto_0

    .line 1341070
    :cond_4
    sget-object v0, LX/8Oi;->POST:LX/8Oi;

    if-ne p2, v0, :cond_0

    .line 1341071
    iget-object v0, p0, LX/8Oo;->b:LX/73w;

    iget-object v1, p0, LX/8Oo;->c:LX/74b;

    iget-object v2, p0, LX/8Oo;->l:Ljava/lang/String;

    iget-object v3, p0, LX/8Oo;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0, v1, v2, p1, v3}, LX/73w;->a(LX/74b;Ljava/lang/String;LX/73y;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 1341107
    iget-object v1, p0, LX/8Oo;->n:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Nx;

    .line 1341108
    if-eqz v1, :cond_0

    .line 1341109
    iget-object v2, p0, LX/8Oo;->b:LX/73w;

    iget-object v3, p0, LX/8Oo;->c:LX/74b;

    iget-object v4, p0, LX/8Oo;->l:Ljava/lang/String;

    iget-wide v5, v1, LX/8Nx;->c:J

    iget-wide v7, v1, LX/8Nx;->a:J

    iget v9, p0, LX/8Oo;->k:I

    iget-wide v10, p0, LX/8Oo;->i:J

    move-object v12, p1

    invoke-virtual/range {v2 .. v12}, LX/73w;->a(LX/74b;Ljava/lang/String;JJIJLjava/lang/String;)V

    .line 1341110
    :cond_0
    iget-object v0, p0, LX/8Oo;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1341111
    return-void
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 1341048
    iget-boolean v0, p0, LX/8Oo;->o:Z

    const-string v1, "Should not be receiving callbacks if notifyCallbackUsed hasn\'t been called"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1341049
    iget-object v0, p0, LX/8Oo;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Nx;

    .line 1341050
    if-eqz v0, :cond_0

    .line 1341051
    iget-wide v2, v0, LX/8Nx;->a:J

    add-long/2addr v2, p2

    iget-wide v4, v0, LX/8Nx;->b:J

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/8Oo;->b(Ljava/lang/String;JJ)V

    .line 1341052
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;JJ)V
    .locals 2

    .prologue
    .line 1341045
    iget-boolean v0, p0, LX/8Oo;->o:Z

    if-nez v0, :cond_0

    .line 1341046
    invoke-direct/range {p0 .. p5}, LX/8Oo;->b(Ljava/lang/String;JJ)V

    .line 1341047
    :cond_0
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;JJJILjava/lang/String;)V
    .locals 12

    .prologue
    .line 1341037
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/8Oo;->l:Ljava/lang/String;

    .line 1341038
    move-wide/from16 v0, p6

    iput-wide v0, p0, LX/8Oo;->i:J

    .line 1341039
    move/from16 v0, p8

    iput v0, p0, LX/8Oo;->k:I

    .line 1341040
    move-object/from16 v0, p9

    iput-object v0, p0, LX/8Oo;->m:Ljava/lang/String;

    .line 1341041
    iget-object v2, p0, LX/8Oo;->n:Ljava/util/Map;

    iget-object v3, p0, LX/8Oo;->m:Ljava/lang/String;

    new-instance v4, LX/8Nx;

    move-wide/from16 v0, p4

    invoke-direct {v4, p2, p3, v0, v1}, LX/8Nx;-><init>(JJ)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1341042
    iget-object v2, p0, LX/8Oo;->b:LX/73w;

    iget-object v3, p0, LX/8Oo;->c:LX/74b;

    iget-object v4, p0, LX/8Oo;->l:Ljava/lang/String;

    iget v5, p0, LX/8Oo;->h:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/8Oo;->h:I

    iget v10, p0, LX/8Oo;->k:I

    move-wide v6, p2

    move-wide/from16 v8, p4

    move-object/from16 v11, p9

    invoke-virtual/range {v2 .. v11}, LX/73w;->a(LX/74b;Ljava/lang/String;IJJILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1341043
    monitor-exit p0

    return-void

    .line 1341044
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public final b()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1341030
    iput-object v1, p0, LX/8Oo;->l:Ljava/lang/String;

    .line 1341031
    iput-wide v2, p0, LX/8Oo;->i:J

    .line 1341032
    const/4 v0, 0x0

    iput v0, p0, LX/8Oo;->k:I

    .line 1341033
    iget-object v0, p0, LX/8Oo;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 1341034
    iput-object v1, p0, LX/8Oo;->m:Ljava/lang/String;

    .line 1341035
    iget-object v0, p0, LX/8Oo;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1341036
    return-void
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 1341028
    iget-object v0, p0, LX/8Oo;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 1341029
    return-void
.end method

.method public final c(J)V
    .locals 1

    .prologue
    .line 1341026
    iput-wide p1, p0, LX/8Oo;->i:J

    .line 1341027
    return-void
.end method
