.class public final LX/7yA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:I

.field public final synthetic c:LX/7yB;


# direct methods
.method public constructor <init>(LX/7yB;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1278762
    iput-object p1, p0, LX/7yA;->c:LX/7yB;

    iput-object p2, p0, LX/7yA;->a:Ljava/lang/String;

    iput p3, p0, LX/7yA;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 6

    .prologue
    .line 1278763
    if-eqz p2, :cond_3

    .line 1278764
    iget-object v0, p0, LX/7yA;->c:LX/7yB;

    iget-object v0, v0, LX/7yB;->q:Ljava/util/Set;

    iget-object v1, p0, LX/7yA;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1278765
    :goto_0
    iget-object v0, p0, LX/7yA;->c:LX/7yB;

    iget-object v0, v0, LX/7yB;->p:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-object v1, p0, LX/7yA;->c:LX/7yB;

    iget-object v1, v1, LX/7yB;->o:LX/7ws;

    .line 1278766
    iget v2, v1, LX/7ws;->a:I

    move v1, v2

    .line 1278767
    iget-object v2, p0, LX/7yA;->c:LX/7yB;

    iget-object v2, v2, LX/7yB;->o:LX/7ws;

    .line 1278768
    iget-object v3, v2, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v2, v3

    .line 1278769
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->fa_()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v2

    iget-object v3, p0, LX/7yA;->c:LX/7yB;

    iget-object v3, v3, LX/7yB;->o:LX/7ws;

    .line 1278770
    iget-object v4, v3, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v3, v4

    .line 1278771
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->eZ_()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/facebook/events/tickets/modal/model/FieldItem;

    iget-object v5, p0, LX/7yA;->c:LX/7yB;

    iget-object v5, v5, LX/7yB;->q:Ljava/util/Set;

    invoke-static {v5}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/events/tickets/modal/model/FieldItem;-><init>(LX/0Rf;)V

    iget-object v5, p0, LX/7yA;->c:LX/7yB;

    invoke-virtual {v5}, LX/1a1;->e()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a(ILcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;Ljava/lang/String;Lcom/facebook/events/tickets/modal/model/FieldItem;I)V

    .line 1278772
    iget-object v0, p0, LX/7yA;->c:LX/7yB;

    iget v1, p0, LX/7yA;->b:I

    iget-object v2, p0, LX/7yA;->c:LX/7yB;

    iget-object v2, v2, LX/7yB;->q:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v4, 0x0

    .line 1278773
    if-ge v2, v1, :cond_0

    .line 1278774
    :goto_1
    iget-object v3, v0, LX/7yB;->m:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v3}, Lcom/facebook/widget/CustomLinearLayout;->getChildCount()I

    move-result v3

    if-ge v4, v3, :cond_2

    .line 1278775
    iget-object v3, v0, LX/7yB;->m:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v3, v4}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbCheckBox;

    .line 1278776
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/facebook/resources/ui/FbCheckBox;->setEnabled(Z)V

    .line 1278777
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    :cond_0
    move v5, v4

    .line 1278778
    :goto_2
    iget-object v3, v0, LX/7yB;->m:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v3}, Lcom/facebook/widget/CustomLinearLayout;->getChildCount()I

    move-result v3

    if-ge v5, v3, :cond_2

    .line 1278779
    iget-object v3, v0, LX/7yB;->m:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v3, v5}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbCheckBox;

    .line 1278780
    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbCheckBox;->isChecked()Z

    move-result p0

    if-nez p0, :cond_1

    .line 1278781
    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbCheckBox;->setEnabled(Z)V

    .line 1278782
    :cond_1
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    .line 1278783
    :cond_2
    return-void

    .line 1278784
    :cond_3
    iget-object v0, p0, LX/7yA;->c:LX/7yB;

    iget-object v0, v0, LX/7yB;->q:Ljava/util/Set;

    iget-object v1, p0, LX/7yA;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method
