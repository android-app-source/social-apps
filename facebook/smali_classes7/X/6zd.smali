.class public final LX/6zd;
.super LX/6vv;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6vv",
        "<",
        "Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;",
        "Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;",
        "Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;",
        "Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;",
        "LX/708;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1161299
    invoke-direct {p0}, LX/6vv;-><init>()V

    .line 1161300
    return-void
.end method

.method public static a(LX/0QB;)LX/6zd;
    .locals 3

    .prologue
    .line 1161284
    const-class v1, LX/6zd;

    monitor-enter v1

    .line 1161285
    :try_start_0
    sget-object v0, LX/6zd;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1161286
    sput-object v2, LX/6zd;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1161287
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1161288
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1161289
    new-instance v0, LX/6zd;

    invoke-direct {v0}, LX/6zd;-><init>()V

    .line 1161290
    move-object v0, v0

    .line 1161291
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1161292
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6zd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1161293
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1161294
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 1161295
    check-cast p1, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    .line 1161296
    new-instance v0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;

    invoke-direct {v0, p1}, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;-><init>(Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 1161297
    check-cast p1, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    check-cast p2, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;

    check-cast p3, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;

    .line 1161298
    new-instance v0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;-><init>(Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;LX/0P1;)V

    return-object v0
.end method
