.class public final LX/7ZM;
.super LX/7ZL;
.source ""


# instance fields
.field public final synthetic a:LX/7ZN;


# direct methods
.method public constructor <init>(LX/7ZN;)V
    .locals 0

    iput-object p1, p0, LX/7ZM;->a:LX/7ZN;

    invoke-direct {p0}, LX/7ZL;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    sget-object v0, LX/7ZN;->a:LX/7Z9;

    const-string v1, "onRemoteDisplayEnded"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/7Z9;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, LX/7ZM;->a:LX/7ZN;

    iget-object v1, v0, LX/7ZN;->c:Landroid/hardware/display/VirtualDisplay;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/7ZN;->c:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v1}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v1, LX/7ZN;->a:LX/7Z9;

    iget-object v2, v0, LX/7ZN;->c:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v2}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getDisplayId()I

    move-result v2

    new-instance p0, Ljava/lang/StringBuilder;

    const/16 p1, 0x26

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string p1, "releasing virtual display: "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, p0}, LX/7Z9;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v1, v0, LX/7ZN;->c:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v1}, Landroid/hardware/display/VirtualDisplay;->release()V

    const/4 v1, 0x0

    iput-object v1, v0, LX/7ZN;->c:Landroid/hardware/display/VirtualDisplay;

    :cond_1
    return-void
.end method
