.class public LX/7RV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/7RS;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

.field public e:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

.field public f:Landroid/media/MediaCodec$BufferInfo;

.field public volatile g:Landroid/media/MediaCodec;

.field private h:J

.field public i:J

.field private j:J

.field private k:I

.field public l:Z

.field private m:J

.field public n:Landroid/media/MediaCodec$BufferInfo;

.field public volatile o:Landroid/media/MediaCodec;

.field public p:LX/7Re;

.field private q:J

.field private r:J

.field private s:I

.field private t:LX/0So;

.field private u:LX/7RW;

.field public final v:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/0Ab;

.field public x:Landroid/media/MediaFormat;

.field public y:Landroid/media/MediaFormat;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1206952
    const-class v0, LX/7RV;

    sput-object v0, LX/7RV;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0So;LX/7RS;LX/0Ab;LX/7RW;)V
    .locals 1

    .prologue
    .line 1206942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1206943
    const/16 v0, 0x3e8

    iput v0, p0, LX/7RV;->a:I

    .line 1206944
    const/4 v0, -0x1

    iput v0, p0, LX/7RV;->k:I

    .line 1206945
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7RV;->l:Z

    .line 1206946
    iput-object p1, p0, LX/7RV;->t:LX/0So;

    .line 1206947
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/7RV;->c:Ljava/lang/ref/WeakReference;

    .line 1206948
    iput-object p3, p0, LX/7RV;->w:LX/0Ab;

    .line 1206949
    iput-object p4, p0, LX/7RV;->u:LX/7RW;

    .line 1206950
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7RV;->v:Ljava/util/Map;

    .line 1206951
    return-void
.end method

.method public static c(F)I
    .locals 1

    .prologue
    .line 1206941
    const/high16 v0, 0x41800000    # 16.0f

    div-float v0, p0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x10

    return v0
.end method

.method private m()V
    .locals 12

    .prologue
    .line 1206899
    iget-object v0, p0, LX/7RV;->o:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    move-object v9, v0

    .line 1206900
    :cond_0
    :goto_0
    iget-object v0, p0, LX/7RV;->o:Landroid/media/MediaCodec;

    iget-object v1, p0, LX/7RV;->n:Landroid/media/MediaCodec$BufferInfo;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v7

    .line 1206901
    const/4 v0, -0x1

    if-eq v7, v0, :cond_c

    .line 1206902
    const/4 v0, -0x3

    if-ne v7, v0, :cond_1

    .line 1206903
    iget-object v0, p0, LX/7RV;->o:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    move-object v9, v0

    goto :goto_0

    .line 1206904
    :cond_1
    const/4 v0, -0x2

    if-ne v7, v0, :cond_2

    .line 1206905
    iget-object v0, p0, LX/7RV;->o:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v0

    iput-object v0, p0, LX/7RV;->x:Landroid/media/MediaFormat;

    goto :goto_0

    .line 1206906
    :cond_2
    if-gez v7, :cond_3

    .line 1206907
    sget-object v0, LX/7RV;->b:Ljava/lang/Class;

    const-string v1, "unexpected result from encoder.dequeueOutputBuffer: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1206908
    :cond_3
    iget-wide v0, p0, LX/7RV;->m:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 1206909
    iget-object v0, p0, LX/7RV;->n:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v0, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, LX/7RV;->m:J

    .line 1206910
    :cond_4
    iget-object v0, p0, LX/7RV;->n:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v0, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, LX/7RV;->q:J

    .line 1206911
    iget-object v0, p0, LX/7RV;->n:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v0, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iget-wide v2, p0, LX/7RV;->m:J

    sub-long v2, v0, v2

    .line 1206912
    array-length v0, v9

    if-lt v7, v0, :cond_5

    .line 1206913
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Encoder index out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1206914
    :cond_5
    aget-object v1, v9, v7

    .line 1206915
    iget-object v0, p0, LX/7RV;->n:Landroid/media/MediaCodec$BufferInfo;

    iget v0, v0, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v0

    iget-object v4, p0, LX/7RV;->n:Landroid/media/MediaCodec$BufferInfo;

    iget v4, v4, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget-object v5, p0, LX/7RV;->n:Landroid/media/MediaCodec$BufferInfo;

    iget v5, v5, Landroid/media/MediaCodec$BufferInfo;->size:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v4}, Ljava/nio/Buffer;->limit(I)Ljava/nio/Buffer;

    .line 1206916
    if-nez v1, :cond_6

    .line 1206917
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "encoderOutputBuffer %s was null"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1206918
    :cond_6
    iget-wide v4, p0, LX/7RV;->r:J

    add-long/2addr v4, v2

    long-to-int v4, v4

    .line 1206919
    const-wide/16 v10, 0x0

    cmp-long v0, v2, v10

    if-gez v0, :cond_7

    .line 1206920
    iget-wide v2, p0, LX/7RV;->r:J

    long-to-int v0, v2

    add-int/lit8 v4, v0, 0x1

    .line 1206921
    :cond_7
    iget-boolean v0, p0, LX/7RV;->l:Z

    if-eqz v0, :cond_9

    iget v0, p0, LX/7RV;->k:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_9

    iget v0, p0, LX/7RV;->s:I

    iget v2, p0, LX/7RV;->k:I

    rem-int/2addr v0, v2

    if-eqz v0, :cond_8

    iget v0, p0, LX/7RV;->s:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_9

    .line 1206922
    :cond_8
    iget-object v0, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iget v2, v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->bitRate:I

    .line 1206923
    iget-object v0, p0, LX/7RV;->e:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    iget v3, v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->bitRate:I

    .line 1206924
    iget-object v0, p0, LX/7RV;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7RS;

    invoke-interface {v0, v2, v3, v4}, LX/7RS;->a(III)I

    move-result v0

    .line 1206925
    if-eq v0, v2, :cond_9

    .line 1206926
    new-instance v2, LX/60r;

    iget-object v3, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    invoke-direct {v2, v3}, LX/60r;-><init>(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;)V

    .line 1206927
    iput v0, v2, LX/60r;->c:I

    .line 1206928
    move-object v0, v2

    .line 1206929
    invoke-virtual {v0}, LX/60r;->a()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    move-result-object v0

    iput-object v0, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    .line 1206930
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1206931
    const-string v2, "video-bitrate"

    iget-object v3, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iget v3, v3, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->bitRate:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1206932
    iget-object v2, p0, LX/7RV;->o:Landroid/media/MediaCodec;

    invoke-virtual {v2, v0}, Landroid/media/MediaCodec;->setParameters(Landroid/os/Bundle;)V

    .line 1206933
    :cond_9
    iget-object v0, p0, LX/7RV;->n:Landroid/media/MediaCodec$BufferInfo;

    iget v0, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_a

    .line 1206934
    iget-object v0, p0, LX/7RV;->n:Landroid/media/MediaCodec$BufferInfo;

    const/4 v2, 0x2

    iput v2, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    .line 1206935
    :cond_a
    iget-object v0, p0, LX/7RV;->u:LX/7RW;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    iget-object v5, p0, LX/7RV;->n:Landroid/media/MediaCodec$BufferInfo;

    iget v6, v5, Landroid/media/MediaCodec$BufferInfo;->flags:I

    iget-object v8, p0, LX/7RV;->n:Landroid/media/MediaCodec$BufferInfo;

    move v5, v4

    invoke-interface/range {v0 .. v8}, LX/7RW;->b(Ljava/nio/ByteBuffer;IIIIIILandroid/media/MediaCodec$BufferInfo;)V

    .line 1206936
    iget-object v0, p0, LX/7RV;->w:LX/0Ab;

    if-eqz v0, :cond_b

    .line 1206937
    iget-object v0, p0, LX/7RV;->w:LX/0Ab;

    int-to-long v2, v4

    invoke-virtual {v0, v2, v3}, LX/0Ab;->b(J)V

    .line 1206938
    :cond_b
    iget-object v0, p0, LX/7RV;->o:Landroid/media/MediaCodec;

    const/4 v1, 0x0

    invoke-virtual {v0, v7, v1}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 1206939
    iget-object v0, p0, LX/7RV;->n:Landroid/media/MediaCodec$BufferInfo;

    iget v0, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 1206940
    :cond_c
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 1206887
    const/4 v0, 0x0

    iput v0, p0, LX/7RV;->s:I

    .line 1206888
    iput-object v1, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    .line 1206889
    iput-object v1, p0, LX/7RV;->e:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    .line 1206890
    iput-wide v2, p0, LX/7RV;->h:J

    .line 1206891
    iput-wide v2, p0, LX/7RV;->m:J

    .line 1206892
    iput-wide v2, p0, LX/7RV;->j:J

    .line 1206893
    iput-wide v2, p0, LX/7RV;->q:J

    .line 1206894
    iput-wide v2, p0, LX/7RV;->i:J

    .line 1206895
    iput-wide v2, p0, LX/7RV;->r:J

    .line 1206896
    iput-object v1, p0, LX/7RV;->x:Landroid/media/MediaFormat;

    .line 1206897
    iput-object v1, p0, LX/7RV;->y:Landroid/media/MediaFormat;

    .line 1206898
    return-void
.end method

.method public final a(F)V
    .locals 14

    .prologue
    .line 1206953
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 1206954
    iget-object v2, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    cmpg-float v2, p1, v2

    if-gez v2, :cond_6

    .line 1206955
    :cond_0
    :goto_0
    iget-object v0, p0, LX/7RV;->v:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1206956
    const/16 v2, 0x1aa

    .line 1206957
    new-instance v1, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v1}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object v1, p0, LX/7RV;->n:Landroid/media/MediaCodec$BufferInfo;

    .line 1206958
    new-instance v1, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v1}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object v1, p0, LX/7RV;->f:Landroid/media/MediaCodec$BufferInfo;

    .line 1206959
    iget-object v1, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iget-object v3, p0, LX/7RV;->v:Ljava/util/Map;

    const/16 v6, 0x1aa

    const/4 v9, 0x1

    .line 1206960
    const/high16 v7, 0x80000

    .line 1206961
    const/16 v8, 0x1e

    .line 1206962
    const-string v5, "baseline"

    .line 1206963
    if-eqz v1, :cond_c

    .line 1206964
    iget v10, v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->width:I

    .line 1206965
    iget v6, v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->height:I

    .line 1206966
    iget v7, v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->bitRate:I

    .line 1206967
    iget v8, v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->frameRate:I

    .line 1206968
    iget-object v11, v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->videoProfile:Ljava/lang/String;

    if-eqz v11, :cond_1

    .line 1206969
    iget-object v5, v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->videoProfile:Ljava/lang/String;

    .line 1206970
    :cond_1
    iget v11, v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->iFrameInterval:I

    if-lez v11, :cond_e

    .line 1206971
    iget v9, v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->iFrameInterval:I

    move-object v13, v5

    move v5, v10

    move-object v10, v13

    .line 1206972
    :goto_1
    const-string v11, "high"

    .line 1206973
    invoke-virtual {v11, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 1206974
    const/4 v11, 0x1

    .line 1206975
    :goto_2
    move v10, v11

    .line 1206976
    if-eqz v10, :cond_d

    .line 1206977
    const/4 v10, 0x1

    :try_start_0
    invoke-static/range {v5 .. v10}, LX/7Rj;->a(IIIIIZ)Landroid/media/MediaCodec;

    move-result-object v10

    .line 1206978
    if-eqz v3, :cond_2

    .line 1206979
    const-string v11, "video_encoding_profile"

    const-string v12, "high"

    invoke-interface {v3, v11, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    move-object v5, v10

    .line 1206980
    :cond_3
    :goto_3
    move-object v1, v5

    .line 1206981
    iput-object v1, p0, LX/7RV;->o:Landroid/media/MediaCodec;

    .line 1206982
    iget-object v1, p0, LX/7RV;->e:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    invoke-static {v1}, LX/7RR;->a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;)Landroid/media/MediaCodec;

    move-result-object v1

    iput-object v1, p0, LX/7RV;->g:Landroid/media/MediaCodec;

    .line 1206983
    iget-object v1, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    if-nez v1, :cond_a

    move v1, v2

    .line 1206984
    :goto_4
    iget-object v3, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    if-nez v3, :cond_b

    .line 1206985
    :goto_5
    new-instance v3, LX/7Re;

    iget-object v4, p0, LX/7RV;->o:Landroid/media/MediaCodec;

    invoke-virtual {v4}, Landroid/media/MediaCodec;->createInputSurface()Landroid/view/Surface;

    move-result-object v4

    invoke-direct {v3, v4, v1, v2}, LX/7Re;-><init>(Landroid/view/Surface;II)V

    iput-object v3, p0, LX/7RV;->p:LX/7Re;

    .line 1206986
    iget-object v0, p0, LX/7RV;->g:Landroid/media/MediaCodec;

    if-eqz v0, :cond_4

    .line 1206987
    iget-object v0, p0, LX/7RV;->g:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V

    .line 1206988
    :cond_4
    iget-object v0, p0, LX/7RV;->o:Landroid/media/MediaCodec;

    if-eqz v0, :cond_5

    .line 1206989
    iget-object v0, p0, LX/7RV;->o:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V

    .line 1206990
    :cond_5
    return-void

    .line 1206991
    :cond_6
    cmpg-float v2, p1, v3

    if-gez v2, :cond_8

    .line 1206992
    iget-object v2, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iget v2, v2, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->width:I

    rem-int/lit8 v2, v2, 0x10

    if-nez v2, :cond_7

    :goto_6
    const-string v1, "The width needs to be a multiple of 16"

    invoke-static {v0, v1}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 1206993
    iget-object v0, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iget v0, v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->width:I

    int-to-float v0, v0

    div-float/2addr v0, p1

    invoke-static {v0}, LX/7RV;->c(F)I

    move-result v0

    .line 1206994
    new-instance v1, LX/60r;

    iget-object v2, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    invoke-direct {v1, v2}, LX/60r;-><init>(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;)V

    .line 1206995
    iput v0, v1, LX/60r;->b:I

    .line 1206996
    move-object v0, v1

    .line 1206997
    invoke-virtual {v0}, LX/60r;->a()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    move-result-object v0

    iput-object v0, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 1206998
    goto :goto_6

    .line 1206999
    :cond_8
    cmpl-float v2, p1, v3

    if-lez v2, :cond_0

    .line 1207000
    iget-object v2, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iget v2, v2, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->height:I

    rem-int/lit8 v2, v2, 0x10

    if-nez v2, :cond_9

    :goto_7
    const-string v1, "The height needs to be a multiple of 16"

    invoke-static {v0, v1}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 1207001
    iget-object v0, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iget v0, v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->height:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    invoke-static {v0}, LX/7RV;->c(F)I

    move-result v0

    .line 1207002
    new-instance v1, LX/60r;

    iget-object v2, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    invoke-direct {v1, v2}, LX/60r;-><init>(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;)V

    .line 1207003
    iput v0, v1, LX/60r;->a:I

    .line 1207004
    move-object v0, v1

    .line 1207005
    invoke-virtual {v0}, LX/60r;->a()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    move-result-object v0

    iput-object v0, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 1207006
    goto :goto_7

    .line 1207007
    :cond_a
    iget-object v1, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iget v1, v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->width:I

    goto/16 :goto_4

    .line 1207008
    :cond_b
    iget-object v2, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iget v2, v2, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->height:I

    goto/16 :goto_5

    .line 1207009
    :cond_c
    sget-object v10, LX/7Rj;->a:Ljava/lang/String;

    const-string v11, "VideoStreamingConfig is null. Using default values"

    invoke-static {v10, v11}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v10, v5

    move v5, v6

    goto/16 :goto_1

    .line 1207010
    :catch_0
    move-exception v10

    .line 1207011
    sget-object v11, LX/7Rj;->a:Ljava/lang/String;

    const-string v12, "Error getting videoencoder for high profile. Fall back to baseline "

    invoke-static {v11, v12, v10}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1207012
    :cond_d
    const/4 v10, 0x0

    invoke-static/range {v5 .. v10}, LX/7Rj;->a(IIIIIZ)Landroid/media/MediaCodec;

    move-result-object v5

    .line 1207013
    if-eqz v3, :cond_3

    .line 1207014
    const-string v6, "video_encoding_profile"

    const-string v7, "baseline"

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    :cond_e
    move-object v13, v5

    move v5, v10

    move-object v10, v13

    goto/16 :goto_1

    :cond_f
    const/4 v11, 0x0

    goto/16 :goto_2
.end method

.method public final a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;)V
    .locals 0

    .prologue
    .line 1206884
    iput-object p1, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    .line 1206885
    iput-object p2, p0, LX/7RV;->e:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    .line 1206886
    return-void
.end method

.method public final a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;I)V
    .locals 0

    .prologue
    .line 1206880
    iput-object p1, p0, LX/7RV;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    .line 1206881
    iput-object p2, p0, LX/7RV;->e:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    .line 1206882
    iput p3, p0, LX/7RV;->k:I

    .line 1206883
    return-void
.end method

.method public final a([BIZ)V
    .locals 12

    .prologue
    .line 1206848
    iget-object v0, p0, LX/7RV;->g:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1206849
    iget-object v1, p0, LX/7RV;->g:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 1206850
    iget-object v1, p0, LX/7RV;->g:Landroid/media/MediaCodec;

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v1

    .line 1206851
    if-ltz v1, :cond_0

    .line 1206852
    aget-object v0, v0, v1

    .line 1206853
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1206854
    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2, p2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 1206855
    iget-object v0, p0, LX/7RV;->g:Landroid/media/MediaCodec;

    const/4 v2, 0x0

    iget-object v3, p0, LX/7RV;->t:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const-wide/16 v8, 0x3e8

    mul-long/2addr v4, v8

    const/4 v6, 0x0

    move v3, p2

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 1206856
    :cond_0
    if-eqz p3, :cond_1

    .line 1206857
    iget-object v0, p0, LX/7RV;->g:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->signalEndOfInputStream()V

    :cond_1
    move-object v9, v7

    .line 1206858
    :cond_2
    :goto_0
    iget-object v0, p0, LX/7RV;->g:Landroid/media/MediaCodec;

    iget-object v1, p0, LX/7RV;->f:Landroid/media/MediaCodec$BufferInfo;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v7

    .line 1206859
    const/4 v0, -0x1

    if-ne v7, v0, :cond_4

    .line 1206860
    if-nez p3, :cond_2

    .line 1206861
    :cond_3
    return-void

    .line 1206862
    :cond_4
    const/4 v0, -0x3

    if-ne v7, v0, :cond_5

    .line 1206863
    iget-object v0, p0, LX/7RV;->g:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    move-object v9, v0

    goto :goto_0

    .line 1206864
    :cond_5
    const/4 v0, -0x2

    if-ne v7, v0, :cond_6

    .line 1206865
    iget-object v0, p0, LX/7RV;->g:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v0

    iput-object v0, p0, LX/7RV;->y:Landroid/media/MediaFormat;

    goto :goto_0

    .line 1206866
    :cond_6
    if-gez v7, :cond_7

    .line 1206867
    sget-object v0, LX/7RV;->b:Ljava/lang/Class;

    const-string v1, "unexpected result from encoder.dequeueOutputBuffer: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1206868
    :cond_7
    aget-object v1, v9, v7

    .line 1206869
    if-nez v1, :cond_8

    .line 1206870
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "encoderOutputBuffer "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1206871
    :cond_8
    iget-wide v2, p0, LX/7RV;->h:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_9

    .line 1206872
    iget-object v0, p0, LX/7RV;->f:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v2, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iput-wide v2, p0, LX/7RV;->h:J

    .line 1206873
    :cond_9
    iget-object v0, p0, LX/7RV;->f:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v2, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iget-wide v4, p0, LX/7RV;->h:J

    sub-long/2addr v2, v4

    .line 1206874
    iget-object v0, p0, LX/7RV;->f:Landroid/media/MediaCodec$BufferInfo;

    iget v0, v0, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v0

    iget-object v4, p0, LX/7RV;->f:Landroid/media/MediaCodec$BufferInfo;

    iget v4, v4, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {v0, v4}, Ljava/nio/Buffer;->limit(I)Ljava/nio/Buffer;

    .line 1206875
    iget-object v0, p0, LX/7RV;->f:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v4, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v10, 0x3e8

    div-long/2addr v4, v10

    iput-wide v4, p0, LX/7RV;->j:J

    .line 1206876
    iget-wide v4, p0, LX/7RV;->i:J

    add-long/2addr v2, v4

    long-to-int v4, v2

    .line 1206877
    iget-object v0, p0, LX/7RV;->u:LX/7RW;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    iget-object v5, p0, LX/7RV;->f:Landroid/media/MediaCodec$BufferInfo;

    iget v6, v5, Landroid/media/MediaCodec$BufferInfo;->flags:I

    iget-object v8, p0, LX/7RV;->f:Landroid/media/MediaCodec$BufferInfo;

    move v5, v4

    invoke-interface/range {v0 .. v8}, LX/7RW;->a(Ljava/nio/ByteBuffer;IIIIIILandroid/media/MediaCodec$BufferInfo;)V

    .line 1206878
    iget-object v0, p0, LX/7RV;->g:Landroid/media/MediaCodec;

    const/4 v1, 0x0

    invoke-virtual {v0, v7, v1}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 1206879
    iget-object v0, p0, LX/7RV;->f:Landroid/media/MediaCodec$BufferInfo;

    iget v0, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_3

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1206831
    iget-object v0, p0, LX/7RV;->p:LX/7Re;

    if-eqz v0, :cond_1

    .line 1206832
    iget-object v0, p0, LX/7RV;->p:LX/7Re;

    .line 1206833
    iget-object v1, v0, LX/7Re;->c:Landroid/view/Surface;

    if-eqz v1, :cond_0

    .line 1206834
    iget-object v1, v0, LX/7Re;->c:Landroid/view/Surface;

    invoke-virtual {v1}, Landroid/view/Surface;->release()V

    .line 1206835
    :cond_0
    const/4 v1, -0x1

    iput v1, v0, LX/7Re;->b:I

    iput v1, v0, LX/7Re;->a:I

    .line 1206836
    :cond_1
    iget-object v0, p0, LX/7RV;->g:Landroid/media/MediaCodec;

    .line 1206837
    if-eqz v0, :cond_2

    .line 1206838
    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 1206839
    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V

    .line 1206840
    :cond_2
    iget-object v0, p0, LX/7RV;->o:Landroid/media/MediaCodec;

    .line 1206841
    if-eqz v0, :cond_4

    .line 1206842
    const/4 v1, 0x0

    :goto_0
    const/4 p0, 0x4

    if-ge v1, p0, :cond_3

    .line 1206843
    :try_start_0
    invoke-virtual {v0}, Landroid/media/MediaCodec;->flush()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1206844
    :cond_3
    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 1206845
    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V

    .line 1206846
    :cond_4
    return-void

    .line 1206847
    :catch_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final c()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1206824
    iget-wide v0, p0, LX/7RV;->i:J

    iget-wide v2, p0, LX/7RV;->j:J

    iget-wide v4, p0, LX/7RV;->h:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/7RV;->i:J

    .line 1206825
    iget-wide v0, p0, LX/7RV;->r:J

    iget-wide v2, p0, LX/7RV;->q:J

    iget-wide v4, p0, LX/7RV;->m:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/7RV;->r:J

    .line 1206826
    iget-wide v0, p0, LX/7RV;->i:J

    iget-wide v2, p0, LX/7RV;->r:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1206827
    iput-wide v0, p0, LX/7RV;->r:J

    iput-wide v0, p0, LX/7RV;->i:J

    .line 1206828
    iput-wide v6, p0, LX/7RV;->h:J

    .line 1206829
    iput-wide v6, p0, LX/7RV;->m:J

    .line 1206830
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1206821
    iget v0, p0, LX/7RV;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7RV;->s:I

    .line 1206822
    invoke-direct {p0}, LX/7RV;->m()V

    .line 1206823
    return-void
.end method
