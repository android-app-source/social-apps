.class public LX/8NC;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1336336
    const-class v0, LX/8NC;

    sput-object v0, LX/8NC;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1336322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0e6;Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;)V"
        }
    .end annotation

    .prologue
    .line 1336323
    const-string v0, "log_api_requests"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1336324
    :try_start_0
    invoke-interface {p0, p1}, LX/0e6;->a(Ljava/lang/Object;)LX/14N;

    move-result-object v2

    .line 1336325
    iget-object v0, v2, LX/14N;->k:LX/14S;

    move-object v0, v0

    .line 1336326
    invoke-virtual {v0}, LX/14S;->name()Ljava/lang/String;

    .line 1336327
    invoke-virtual {v2}, LX/14N;->h()LX/0Px;

    move-result-object v3

    .line 1336328
    if-eqz v3, :cond_0

    .line 1336329
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 1336330
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    .line 1336331
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1336332
    :cond_0
    invoke-virtual {v2}, LX/14N;->m()Ljava/util/List;

    move-result-object v0

    .line 1336333
    if-eqz v0, :cond_1

    .line 1336334
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1336335
    :catch_0
    :cond_1
    return-void
.end method
