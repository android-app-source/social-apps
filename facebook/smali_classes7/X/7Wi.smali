.class public LX/7Wi;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public final a:LX/0Xl;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Xl;)V
    .locals 1
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1216755
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1216756
    iput-object p2, p0, LX/7Wi;->a:LX/0Xl;

    .line 1216757
    new-instance v0, LX/7Wh;

    invoke-direct {v0, p0}, LX/7Wh;-><init>(LX/7Wi;)V

    invoke-virtual {p0, v0}, LX/7Wi;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1216758
    const v0, 0x7f080e95

    invoke-virtual {p0, v0}, LX/7Wi;->setTitle(I)V

    .line 1216759
    return-void
.end method

.method public static b(LX/0QB;)LX/7Wi;
    .locals 3

    .prologue
    .line 1216760
    new-instance v2, LX/7Wi;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v1

    check-cast v1, LX/0Xl;

    invoke-direct {v2, v0, v1}, LX/7Wi;-><init>(Landroid/content/Context;LX/0Xl;)V

    .line 1216761
    return-object v2
.end method
