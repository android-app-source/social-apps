.class public LX/7QP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:[LX/7QR;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1204193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204194
    const/4 v0, 0x0

    new-array v0, v0, [LX/7QR;

    iput-object v0, p0, LX/7QP;->a:[LX/7QR;

    .line 1204195
    return-void
.end method

.method public constructor <init>(LX/7QP;)V
    .locals 5

    .prologue
    .line 1204186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204187
    invoke-virtual {p1}, LX/7QP;->a()I

    move-result v1

    .line 1204188
    new-array v0, v1, [LX/7QR;

    iput-object v0, p0, LX/7QP;->a:[LX/7QR;

    .line 1204189
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1204190
    iget-object v2, p0, LX/7QP;->a:[LX/7QR;

    new-instance v3, LX/7QR;

    invoke-virtual {p1, v0}, LX/7QP;->a(I)LX/7QR;

    move-result-object v4

    invoke-direct {v3, v4}, LX/7QR;-><init>(LX/7QR;)V

    aput-object v3, v2, v0

    .line 1204191
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1204192
    :cond_0
    return-void
.end method

.method public constructor <init>([LX/7QR;)V
    .locals 1

    .prologue
    .line 1204180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204181
    if-nez p1, :cond_0

    .line 1204182
    const/4 v0, 0x0

    new-array v0, v0, [LX/7QR;

    iput-object v0, p0, LX/7QP;->a:[LX/7QR;

    .line 1204183
    :goto_0
    return-void

    .line 1204184
    :cond_0
    iput-object p1, p0, LX/7QP;->a:[LX/7QR;

    .line 1204185
    iget-object v0, p0, LX/7QP;->a:[LX/7QR;

    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1204196
    iget-object v0, p0, LX/7QP;->a:[LX/7QR;

    array-length v0, v0

    return v0
.end method

.method public final a(I)LX/7QR;
    .locals 1

    .prologue
    .line 1204179
    iget-object v0, p0, LX/7QP;->a:[LX/7QR;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1204178
    iget-object v0, p0, LX/7QP;->a:[LX/7QR;

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
