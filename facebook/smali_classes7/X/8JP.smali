.class public final LX/8JP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4mR;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/tagging/shared/TagTypeahead;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V
    .locals 0

    .prologue
    .line 1328483
    iput-object p1, p0, LX/8JP;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1328484
    iget-object v0, p0, LX/8JP;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-static {v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->f(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    .line 1328485
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1328486
    iget-object v0, p0, LX/8JP;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4mU;->a(LX/4mR;)V

    .line 1328487
    iget-object v0, p0, LX/8JP;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->o:LX/8Hs;

    invoke-virtual {v0}, LX/8Hs;->c()V

    .line 1328488
    iget-object v0, p0, LX/8JP;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    const/4 v1, 0x1

    .line 1328489
    iput-boolean v1, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->t:Z

    .line 1328490
    iget-object v0, p0, LX/8JP;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-boolean v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8JP;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->v:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1328491
    iget-object v0, p0, LX/8JP;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-static {v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->i(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    .line 1328492
    iget-object v0, p0, LX/8JP;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    const/4 v1, 0x0

    .line 1328493
    iput-boolean v1, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->z:Z

    .line 1328494
    :cond_0
    return-void
.end method
