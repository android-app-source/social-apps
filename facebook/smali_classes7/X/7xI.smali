.class public final synthetic LX/7xI;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I

.field public static final synthetic d:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1277279
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;->values()[Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/7xI;->d:[I

    :try_start_0
    sget-object v0, LX/7xI;->d:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;->RADIO_BUTTON:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_c

    :goto_0
    :try_start_1
    sget-object v0, LX/7xI;->d:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;->INLINE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_b

    .line 1277280
    :goto_1
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->values()[Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/7xI;->c:[I

    :try_start_2
    sget-object v0, LX/7xI;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_a

    :goto_2
    :try_start_3
    sget-object v0, LX/7xI;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->CONTACT_INFO:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_9

    :goto_3
    :try_start_4
    sget-object v0, LX/7xI;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->TEXT:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_8

    :goto_4
    :try_start_5
    sget-object v0, LX/7xI;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->CHECKBOX:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_7

    :goto_5
    :try_start_6
    sget-object v0, LX/7xI;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->SELECTION_STRING:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    .line 1277281
    :goto_6
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->values()[Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/7xI;->b:[I

    :try_start_7
    sget-object v0, LX/7xI;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->FORM_FIELD:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_5

    :goto_7
    :try_start_8
    sget-object v0, LX/7xI;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->SEPARATOR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_4

    :goto_8
    :try_start_9
    sget-object v0, LX/7xI;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->IMAGE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_3

    :goto_9
    :try_start_a
    sget-object v0, LX/7xI;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_2

    .line 1277282
    :goto_a
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->values()[Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/7xI;->a:[I

    :try_start_b
    sget-object v0, LX/7xI;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->PER_TICKET:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1

    :goto_b
    :try_start_c
    sget-object v0, LX/7xI;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->PER_ORDER:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_0

    :goto_c
    return-void

    :catch_0
    goto :goto_c

    :catch_1
    goto :goto_b

    :catch_2
    goto :goto_a

    :catch_3
    goto :goto_9

    :catch_4
    goto :goto_8

    :catch_5
    goto :goto_7

    :catch_6
    goto :goto_6

    :catch_7
    goto :goto_5

    :catch_8
    goto :goto_4

    :catch_9
    goto :goto_3

    :catch_a
    goto/16 :goto_2

    :catch_b
    goto/16 :goto_1

    :catch_c
    goto/16 :goto_0
.end method
