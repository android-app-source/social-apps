.class public final LX/6tK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ds;


# instance fields
.field private final a:LX/6ta;

.field private final b:LX/6tk;

.field private final c:LX/6tn;

.field private final d:LX/6tq;

.field private final e:LX/6to;

.field private final f:LX/6tj;

.field private final g:LX/6ti;

.field private final h:LX/6tp;

.field private final i:LX/6tl;

.field private final j:LX/6te;

.field private final k:LX/6td;

.field private final l:LX/6tm;

.field private final m:LX/6r9;


# direct methods
.method public constructor <init>(LX/6ta;LX/6tk;LX/6tn;LX/6tq;LX/6to;LX/6tj;LX/6ti;LX/6tp;LX/6tl;LX/6te;LX/6td;LX/6tm;LX/6r9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1154602
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1154603
    iput-object p1, p0, LX/6tK;->a:LX/6ta;

    .line 1154604
    iput-object p2, p0, LX/6tK;->b:LX/6tk;

    .line 1154605
    iput-object p3, p0, LX/6tK;->c:LX/6tn;

    .line 1154606
    iput-object p4, p0, LX/6tK;->d:LX/6tq;

    .line 1154607
    iput-object p5, p0, LX/6tK;->e:LX/6to;

    .line 1154608
    iput-object p6, p0, LX/6tK;->f:LX/6tj;

    .line 1154609
    iput-object p7, p0, LX/6tK;->g:LX/6ti;

    .line 1154610
    iput-object p8, p0, LX/6tK;->h:LX/6tp;

    .line 1154611
    iput-object p9, p0, LX/6tK;->i:LX/6tl;

    .line 1154612
    iput-object p10, p0, LX/6tK;->j:LX/6te;

    .line 1154613
    iput-object p11, p0, LX/6tK;->k:LX/6td;

    .line 1154614
    iput-object p12, p0, LX/6tK;->l:LX/6tm;

    .line 1154615
    iput-object p13, p0, LX/6tK;->m:LX/6r9;

    .line 1154616
    return-void
.end method

.method public static a(LX/0Pz;LX/0Rf;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/6so;",
            ">;",
            "LX/0Rf",
            "<",
            "LX/6rp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1154585
    sget-object v0, LX/6rp;->CONTACT_NAME:LX/6rp;

    invoke-virtual {p1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1154586
    sget-object v0, LX/6so;->CONTACT_NAME:LX/6so;

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154587
    :cond_0
    sget-object v0, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-virtual {p1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1154588
    sget-object v0, LX/6so;->CONTACT_INFORMATION:LX/6so;

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154589
    :cond_1
    sget-object v0, LX/6rp;->MAILING_ADDRESS:LX/6rp;

    invoke-virtual {p1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1154590
    sget-object v0, LX/6so;->MAILING_ADDRESS:LX/6so;

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154591
    :cond_2
    sget-object v0, LX/6rp;->SHIPPING_OPTION:LX/6rp;

    invoke-virtual {p1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1154592
    sget-object v0, LX/6so;->SHIPPING_OPTION:LX/6so;

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154593
    :cond_3
    sget-object v0, LX/6rp;->CHECKOUT_OPTIONS:LX/6rp;

    invoke-virtual {p1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1154594
    sget-object v0, LX/6so;->CHECKOUT_OPTION:LX/6so;

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154595
    :cond_4
    sget-object v0, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    invoke-virtual {p1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1154596
    sget-object v0, LX/6so;->PAYMENT_METHOD:LX/6so;

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154597
    :cond_5
    sget-object v0, LX/6rp;->NOTE:LX/6rp;

    invoke-virtual {p1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1154598
    sget-object v0, LX/6so;->NOTE:LX/6so;

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154599
    :cond_6
    sget-object v0, LX/6rp;->PRICE_SELECTOR:LX/6rp;

    invoke-virtual {p1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1154600
    sget-object v0, LX/6so;->PRICE_SELECTOR:LX/6so;

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154601
    :cond_7
    return-void
.end method

.method public static b(LX/0QB;)LX/6tK;
    .locals 15

    .prologue
    .line 1154548
    new-instance v0, LX/6tK;

    .line 1154549
    new-instance v4, LX/6ta;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/6r9;->a(LX/0QB;)LX/6r9;

    move-result-object v2

    check-cast v2, LX/6r9;

    invoke-static {p0}, LX/3GL;->b(LX/0QB;)LX/3GL;

    move-result-object v3

    check-cast v3, LX/3GL;

    invoke-direct {v4, v1, v2, v3}, LX/6ta;-><init>(Landroid/content/Context;LX/6r9;LX/3GL;)V

    .line 1154550
    move-object v1, v4

    .line 1154551
    check-cast v1, LX/6ta;

    invoke-static {p0}, LX/6tk;->b(LX/0QB;)LX/6tk;

    move-result-object v2

    check-cast v2, LX/6tk;

    .line 1154552
    new-instance v6, LX/6tn;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {p0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object v4

    check-cast v4, LX/6xb;

    invoke-static {p0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v5

    check-cast v5, LX/5fv;

    invoke-direct {v6, v3, v4, v5}, LX/6tn;-><init>(Landroid/content/res/Resources;LX/6xb;LX/5fv;)V

    .line 1154553
    move-object v3, v6

    .line 1154554
    check-cast v3, LX/6tn;

    .line 1154555
    new-instance v6, LX/6tq;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {p0}, LX/6r9;->a(LX/0QB;)LX/6r9;

    move-result-object v5

    check-cast v5, LX/6r9;

    invoke-direct {v6, v4, v5}, LX/6tq;-><init>(Landroid/content/res/Resources;LX/6r9;)V

    .line 1154556
    move-object v4, v6

    .line 1154557
    check-cast v4, LX/6tq;

    .line 1154558
    new-instance v7, LX/6to;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {p0}, LX/3GL;->b(LX/0QB;)LX/3GL;

    move-result-object v6

    check-cast v6, LX/3GL;

    invoke-direct {v7, v5, v6}, LX/6to;-><init>(Landroid/content/res/Resources;LX/3GL;)V

    .line 1154559
    move-object v5, v7

    .line 1154560
    check-cast v5, LX/6to;

    .line 1154561
    new-instance v8, LX/6tj;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    const-class v7, Landroid/content/Context;

    invoke-interface {p0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-direct {v8, v6, v7}, LX/6tj;-><init>(Landroid/content/res/Resources;Landroid/content/Context;)V

    .line 1154562
    move-object v6, v8

    .line 1154563
    check-cast v6, LX/6tj;

    .line 1154564
    new-instance v9, LX/6ti;

    const-class v7, Landroid/content/Context;

    invoke-interface {p0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {p0}, LX/6r9;->a(LX/0QB;)LX/6r9;

    move-result-object v8

    check-cast v8, LX/6r9;

    invoke-direct {v9, v7, v8}, LX/6ti;-><init>(Landroid/content/Context;LX/6r9;)V

    .line 1154565
    move-object v7, v9

    .line 1154566
    check-cast v7, LX/6ti;

    .line 1154567
    new-instance v10, LX/6tp;

    const-class v8, Landroid/content/Context;

    invoke-interface {p0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {p0}, LX/6r9;->a(LX/0QB;)LX/6r9;

    move-result-object v9

    check-cast v9, LX/6r9;

    invoke-direct {v10, v8, v9}, LX/6tp;-><init>(Landroid/content/Context;LX/6r9;)V

    .line 1154568
    move-object v8, v10

    .line 1154569
    check-cast v8, LX/6tp;

    .line 1154570
    new-instance v11, LX/6tl;

    const-class v9, Landroid/content/Context;

    invoke-interface {p0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-static {p0}, LX/6r9;->a(LX/0QB;)LX/6r9;

    move-result-object v10

    check-cast v10, LX/6r9;

    invoke-direct {v11, v9, v10}, LX/6tl;-><init>(Landroid/content/Context;LX/6r9;)V

    .line 1154571
    move-object v9, v11

    .line 1154572
    check-cast v9, LX/6tl;

    .line 1154573
    new-instance v12, LX/6te;

    const-class v10, Landroid/content/Context;

    invoke-interface {p0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static {p0}, LX/6r9;->a(LX/0QB;)LX/6r9;

    move-result-object v11

    check-cast v11, LX/6r9;

    invoke-direct {v12, v10, v11}, LX/6te;-><init>(Landroid/content/Context;LX/6r9;)V

    .line 1154574
    move-object v10, v12

    .line 1154575
    check-cast v10, LX/6te;

    .line 1154576
    new-instance v13, LX/6td;

    const-class v11, Landroid/content/Context;

    invoke-interface {p0, v11}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Context;

    invoke-static {p0}, LX/6r9;->a(LX/0QB;)LX/6r9;

    move-result-object v12

    check-cast v12, LX/6r9;

    invoke-direct {v13, v11, v12}, LX/6td;-><init>(Landroid/content/Context;LX/6r9;)V

    .line 1154577
    move-object v11, v13

    .line 1154578
    check-cast v11, LX/6td;

    .line 1154579
    new-instance v14, LX/6tm;

    invoke-direct {v14}, LX/6tm;-><init>()V

    .line 1154580
    const-class v12, Landroid/content/Context;

    invoke-interface {p0, v12}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/content/Context;

    invoke-static {p0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v13

    check-cast v13, LX/5fv;

    .line 1154581
    iput-object v12, v14, LX/6tm;->a:Landroid/content/Context;

    iput-object v13, v14, LX/6tm;->b:LX/5fv;

    .line 1154582
    move-object v12, v14

    .line 1154583
    check-cast v12, LX/6tm;

    invoke-static {p0}, LX/6r9;->a(LX/0QB;)LX/6r9;

    move-result-object v13

    check-cast v13, LX/6r9;

    invoke-direct/range {v0 .. v13}, LX/6tK;-><init>(LX/6ta;LX/6tk;LX/6tn;LX/6tq;LX/6to;LX/6tj;LX/6ti;LX/6tp;LX/6tl;LX/6te;LX/6td;LX/6tm;LX/6r9;)V

    .line 1154584
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/CheckoutData;",
            ")",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1154617
    iget-object v0, p0, LX/6tK;->m:LX/6r9;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->f(LX/6qw;)LX/6Ds;

    move-result-object v3

    .line 1154618
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1154619
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->x:LX/0Px;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->x:LX/0Px;

    move-object v1, v0

    .line 1154620
    :goto_0
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_4

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6so;

    .line 1154621
    invoke-interface {v3, v0, p1}, LX/6Ds;->a(LX/6so;Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6E3;

    move-result-object v0

    .line 1154622
    instance-of v6, v0, LX/6sl;

    if-eqz v6, :cond_3

    .line 1154623
    check-cast v0, LX/6sl;

    invoke-interface {v0}, LX/6sl;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1154624
    :cond_0
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1154625
    :cond_1
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 1154626
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->f:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    if-eqz v1, :cond_5

    .line 1154627
    sget-object v1, LX/6so;->ENTITY:LX/6so;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154628
    :cond_2
    :goto_3
    sget-object v1, LX/6so;->EXPANDING_ELLIPSIZING_TEXT:LX/6so;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154629
    sget-object v1, LX/6so;->PRICE_TABLE:LX/6so;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154630
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    invoke-static {v0, v1}, LX/6tK;->a(LX/0Pz;LX/0Rf;)V

    .line 1154631
    sget-object v1, LX/6so;->TERMS_AND_POLICIES:LX/6so;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154632
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1154633
    move-object v1, v0

    goto :goto_0

    .line 1154634
    :cond_3
    if-eqz v0, :cond_0

    .line 1154635
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1154636
    :cond_4
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-interface {v3, p1, v0}, LX/6Ds;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 1154637
    :cond_5
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->h:LX/0Px;

    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1154638
    sget-object v1, LX/6so;->PURCHASE_REVIEW_CELL:LX/6so;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/CheckoutData;",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1154174
    iget-object v0, p0, LX/6tK;->b:LX/6tk;

    .line 1154175
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->y:Ljava/lang/String;

    .line 1154176
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1154177
    iget-object v1, v0, LX/6tk;->a:Landroid/content/res/Resources;

    const v2, 0x7f081d47

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1154178
    :cond_0
    invoke-static {v0, p1, v1}, LX/6tk;->a(LX/6tk;Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)LX/6sw;

    move-result-object v1

    move-object v0, v1

    .line 1154179
    invoke-virtual {p0, p1, p2, v0}, LX/6tK;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;LX/6sw;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;LX/6sw;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/CheckoutData;",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;",
            "LX/6sw;",
            ")",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1154546
    iget-object v0, p0, LX/6tK;->m:LX/6r9;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->a(LX/6qw;)LX/6qa;

    move-result-object v0

    invoke-interface {v0}, LX/6qa;->a()Z

    move-result v0

    .line 1154547
    new-instance v1, LX/6tU;

    new-instance v2, LX/6tY;

    new-instance v3, LX/6tW;

    new-instance v4, LX/6tZ;

    new-instance v5, LX/6tX;

    invoke-direct {v5, p2}, LX/6tX;-><init>(LX/0Px;)V

    invoke-direct {v4, v5}, LX/6tZ;-><init>(LX/6tS;)V

    invoke-direct {v3, v4, v0}, LX/6tW;-><init>(LX/6tS;Z)V

    invoke-static {}, LX/6tf;->a()LX/6sp;

    move-result-object v0

    invoke-direct {v2, v3, v0}, LX/6tY;-><init>(LX/6tS;LX/6sp;)V

    invoke-direct {v1, v2, p3}, LX/6tU;-><init>(LX/6tS;LX/6sw;)V

    invoke-virtual {v1}, LX/6tT;->a()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/6so;Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6E3;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1154180
    sget-object v0, LX/6tJ;->a:[I

    invoke-virtual {p1}, LX/6so;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1154181
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown CheckoutRowType seen "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1154182
    :pswitch_0
    iget-object v0, p0, LX/6tK;->a:LX/6ta;

    const/4 v2, 0x0

    .line 1154183
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    sget-object v3, LX/6rp;->CHECKOUT_OPTIONS:LX/6rp;

    invoke-virtual {v1, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    move-object v1, v2

    .line 1154184
    :goto_0
    move-object v0, v1

    .line 1154185
    :goto_1
    return-object v0

    .line 1154186
    :pswitch_1
    iget-object v0, p0, LX/6tK;->k:LX/6td;

    .line 1154187
    invoke-static {p2}, LX/6qv;->c(Lcom/facebook/payments/checkout/model/CheckoutData;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v1

    .line 1154188
    invoke-static {}, LX/0Rj;->isNull()LX/0Rl;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0wv;->b(LX/0Rl;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1154189
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    .line 1154190
    invoke-static {}, LX/73O;->newBuilder()LX/73P;

    move-result-object v2

    sget-object v3, LX/73N;->FLOATING_LABEL_TEXT:LX/73N;

    .line 1154191
    iput-object v3, v2, LX/73P;->a:LX/73N;

    .line 1154192
    move-object v2, v2

    .line 1154193
    iget-object v3, v0, LX/6td;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v1}, LX/6td;->a(LX/0Rf;)I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1154194
    iput-object v1, v2, LX/73P;->b:Ljava/lang/String;

    .line 1154195
    move-object v1, v2

    .line 1154196
    new-instance v2, LX/6tC;

    sget-object v3, LX/6so;->CHECKOUT_OPTION:LX/6so;

    invoke-virtual {v1}, LX/73P;->e()LX/73O;

    move-result-object v1

    invoke-direct {v2, v3, v1}, LX/6tC;-><init>(LX/6so;LX/73O;)V

    move-object v1, v2

    .line 1154197
    :goto_2
    move-object v0, v1

    .line 1154198
    goto :goto_1

    .line 1154199
    :pswitch_2
    iget-object v0, p0, LX/6tK;->j:LX/6te;

    .line 1154200
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->o()Lcom/facebook/payments/contactinfo/model/ContactInfo;

    move-result-object v1

    .line 1154201
    if-nez v1, :cond_11

    .line 1154202
    const/4 v1, 0x0

    .line 1154203
    :goto_3
    move-object v0, v1

    .line 1154204
    goto :goto_1

    .line 1154205
    :pswitch_3
    invoke-static {}, LX/6tf;->a()LX/6sp;

    move-result-object v0

    goto :goto_1

    .line 1154206
    :pswitch_4
    const/4 v1, 0x0

    .line 1154207
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->f:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    if-nez v0, :cond_12

    .line 1154208
    :goto_4
    move-object v0, v1

    .line 1154209
    goto :goto_1

    .line 1154210
    :pswitch_5
    const-string v0, "title"

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/6th;->a(Ljava/lang/String;Ljava/lang/String;)LX/6E3;

    move-result-object v0

    move-object v0, v0

    .line 1154211
    goto :goto_1

    .line 1154212
    :pswitch_6
    iget-object v0, p0, LX/6tK;->g:LX/6ti;

    .line 1154213
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->h()LX/0am;

    move-result-object v1

    .line 1154214
    if-nez v1, :cond_15

    .line 1154215
    invoke-static {}, LX/73O;->newBuilder()LX/73P;

    move-result-object v1

    sget-object v2, LX/73N;->FLOATING_LABEL_TEXT:LX/73N;

    .line 1154216
    iput-object v2, v1, LX/73P;->a:LX/73N;

    .line 1154217
    move-object v1, v1

    .line 1154218
    iget-object v2, v0, LX/6ti;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p2, 0x7f081e69

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1154219
    iput-object v2, v1, LX/73P;->b:Ljava/lang/String;

    .line 1154220
    move-object v1, v1

    .line 1154221
    new-instance v2, LX/6tC;

    sget-object p2, LX/6so;->CHECKOUT_OPTION:LX/6so;

    invoke-virtual {v1}, LX/73P;->e()LX/73O;

    move-result-object v1

    invoke-direct {v2, p2, v1}, LX/6tC;-><init>(LX/6so;LX/73O;)V

    move-object v1, v2

    .line 1154222
    :goto_5
    move-object v0, v1

    .line 1154223
    goto/16 :goto_1

    .line 1154224
    :pswitch_7
    iget-object v0, p0, LX/6tK;->f:LX/6tj;

    const/16 v8, 0x72

    .line 1154225
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    sget-object v2, LX/6rp;->NOTE:LX/6rp;

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 1154226
    const/4 v1, 0x0

    .line 1154227
    :goto_6
    move-object v0, v1

    .line 1154228
    goto/16 :goto_1

    .line 1154229
    :pswitch_8
    iget-object v0, p0, LX/6tK;->i:LX/6tl;

    .line 1154230
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->s()LX/0am;

    move-result-object v1

    .line 1154231
    if-nez v1, :cond_1a

    .line 1154232
    invoke-static {}, LX/73O;->newBuilder()LX/73P;

    move-result-object v1

    sget-object v2, LX/73N;->FLOATING_LABEL_TEXT:LX/73N;

    .line 1154233
    iput-object v2, v1, LX/73P;->a:LX/73N;

    .line 1154234
    move-object v1, v1

    .line 1154235
    iget-object v2, v0, LX/6tl;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p2, 0x7f081d41

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1154236
    iput-object v2, v1, LX/73P;->b:Ljava/lang/String;

    .line 1154237
    move-object v1, v1

    .line 1154238
    new-instance v2, LX/6tC;

    sget-object p2, LX/6so;->CHECKOUT_OPTION:LX/6so;

    invoke-virtual {v1}, LX/73P;->e()LX/73O;

    move-result-object v1

    invoke-direct {v2, p2, v1}, LX/6tC;-><init>(LX/6so;LX/73O;)V

    move-object v1, v2

    .line 1154239
    :goto_7
    move-object v0, v1

    .line 1154240
    goto/16 :goto_1

    .line 1154241
    :pswitch_9
    iget-object v0, p0, LX/6tK;->l:LX/6tm;

    .line 1154242
    new-instance v3, LX/6xH;

    invoke-direct {v3}, LX/6xH;-><init>()V

    move-object v3, v3

    .line 1154243
    const-string v4, "USD"

    .line 1154244
    iput-object v4, v3, LX/6xH;->b:Ljava/lang/String;

    .line 1154245
    move-object v3, v3

    .line 1154246
    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v5, "USD"

    const-wide/32 v7, 0x186a0

    invoke-direct {v4, v5, v7, v8}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    .line 1154247
    iput-object v4, v3, LX/6xH;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1154248
    move-object v3, v3

    .line 1154249
    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v5, "USD"

    const-wide/16 v7, 0x64

    invoke-direct {v4, v5, v7, v8}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    .line 1154250
    iput-object v4, v3, LX/6xH;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1154251
    move-object v3, v3

    .line 1154252
    const-string v4, "Custom"

    .line 1154253
    iput-object v4, v3, LX/6xH;->f:Ljava/lang/String;

    .line 1154254
    move-object v3, v3

    .line 1154255
    sget-object v4, LX/6xN;->PRICE:LX/6xN;

    const-string v5, "Donation Amount"

    sget-object v6, LX/6xe;->REQUIRED:LX/6xe;

    sget-object v7, LX/6xO;->PRICE_NO_DECIMALS:LX/6xO;

    invoke-static {v4, v5, v6, v7}, Lcom/facebook/payments/form/model/FormFieldAttributes;->a(LX/6xN;Ljava/lang/String;LX/6xe;LX/6xO;)LX/6xM;

    move-result-object v4

    invoke-virtual {v4}, LX/6xM;->a()Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v4

    .line 1154256
    iput-object v4, v3, LX/6xH;->c:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 1154257
    move-object v3, v3

    .line 1154258
    new-instance v4, Lcom/facebook/payments/form/model/AmountFormData;

    invoke-direct {v4, v3}, Lcom/facebook/payments/form/model/AmountFormData;-><init>(LX/6xH;)V

    move-object v7, v4

    .line 1154259
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1154260
    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v5, "USD"

    const-wide/16 v9, 0x3e8

    invoke-direct {v4, v5, v9, v10}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1154261
    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v5, "USD"

    const-wide/16 v9, 0xa28

    invoke-direct {v4, v5, v9, v10}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1154262
    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v5, "USD"

    const-wide/16 v9, 0xe74

    invoke-direct {v4, v5, v9, v10}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1154263
    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v5, "USD"

    const-wide/16 v9, 0xfa0

    invoke-direct {v4, v5, v9, v10}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1154264
    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v5, "USD"

    const-wide/16 v9, 0x170c

    invoke-direct {v4, v5, v9, v10}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1154265
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v4, 0x0

    move v5, v4

    :goto_8
    if-ge v5, v6, :cond_1f

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1154266
    iget-object v8, v4, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v4, v8

    .line 1154267
    invoke-static {v4}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/math/BigDecimal;)Z

    move-result v4

    if-nez v4, :cond_1e

    .line 1154268
    sget-object v4, LX/5fu;->DEFAULT:LX/5fu;

    .line 1154269
    :goto_9
    move-object v4, v4

    .line 1154270
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1154271
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v5, 0x0

    move v6, v5

    :goto_a
    if-ge v6, v9, :cond_0

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1154272
    iget-object v10, v0, LX/6tm;->b:LX/5fv;

    invoke-virtual {v10, v5, v4}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;

    move-result-object v5

    .line 1154273
    new-instance v10, LX/6t0;

    sget-object v11, LX/6t2;->PRICE:LX/6t2;

    invoke-direct {v10, v5, v11}, LX/6t0;-><init>(Ljava/lang/String;LX/6t2;)V

    move-object v5, v10

    .line 1154274
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1154275
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_a

    .line 1154276
    :cond_0
    move-object v5, v8

    .line 1154277
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->A()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v6

    .line 1154278
    if-nez v6, :cond_1c

    .line 1154279
    iget-object v3, v7, Lcom/facebook/payments/form/model/AmountFormData;->f:Ljava/lang/String;

    move-object v3, v3

    .line 1154280
    :goto_b
    new-instance v4, LX/6t0;

    sget-object v8, LX/6t2;->CUSTOM:LX/6t2;

    invoke-direct {v4, v3, v8}, LX/6t0;-><init>(Ljava/lang/String;LX/6t2;)V

    move-object v3, v4

    .line 1154281
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1154282
    if-nez v6, :cond_1d

    .line 1154283
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->z()Ljava/lang/Integer;

    move-result-object v6

    .line 1154284
    :goto_c
    new-instance v3, LX/6t4;

    const-string v4, "Donation Amount"

    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    .line 1154285
    iget-object v8, v0, LX/6tm;->a:Landroid/content/Context;

    sget-object v9, LX/6xK;->AMOUNT_FORM_CONTROLLER:LX/6xK;

    iget-object v10, v0, LX/6tm;->a:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f081d55

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->c()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/facebook/payments/form/model/PaymentsFormParams;->a(LX/6xK;Ljava/lang/String;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6xW;

    move-result-object v9

    .line 1154286
    iput-object v7, v9, LX/6xW;->e:Lcom/facebook/payments/form/model/PaymentsFormData;

    .line 1154287
    move-object v9, v9

    .line 1154288
    iget-object v10, v0, LX/6tm;->a:Landroid/content/Context;

    const v11, 0x7f081e45

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1154289
    iput-object v10, v9, LX/6xW;->f:Ljava/lang/String;

    .line 1154290
    move-object v9, v9

    .line 1154291
    invoke-virtual {v9}, LX/6xW;->a()Lcom/facebook/payments/form/model/PaymentsFormParams;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/facebook/payments/form/PaymentsFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/form/model/PaymentsFormParams;)Landroid/content/Intent;

    move-result-object v8

    move-object v7, v8

    .line 1154292
    const/16 v8, 0x75

    invoke-direct/range {v3 .. v8}, LX/6t4;-><init>(Ljava/lang/String;LX/0Px;Ljava/lang/Integer;Landroid/content/Intent;I)V

    move-object v0, v3

    .line 1154293
    goto/16 :goto_1

    .line 1154294
    :pswitch_a
    iget-object v0, p0, LX/6tK;->c:LX/6tn;

    .line 1154295
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    const/4 v6, 0x0

    .line 1154296
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->g:LX/0Px;

    .line 1154297
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1154298
    invoke-static {v2, v3}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    .line 1154299
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-object v8, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->v:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v7, v6

    move-object v3, v2

    .line 1154300
    :goto_d
    if-ge v7, v9, :cond_2

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    .line 1154301
    iget-boolean v4, v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->h:Z

    if-eqz v4, :cond_20

    .line 1154302
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->u()LX/0P1;

    move-result-object v4

    iget-object v2, v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    invoke-virtual {v4, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    .line 1154303
    if-eqz v2, :cond_20

    .line 1154304
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v10

    move v5, v6

    move-object v4, v3

    :goto_e
    if-ge v5, v10, :cond_1

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    .line 1154305
    iget-object v3, v3, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->d:LX/0Px;

    invoke-static {v4, v3}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(LX/0Px;LX/0Px;)LX/0Px;

    move-result-object v4

    .line 1154306
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_e

    :cond_1
    move-object v2, v4

    .line 1154307
    :goto_f
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move-object v3, v2

    goto :goto_d

    .line 1154308
    :cond_2
    move-object v2, v3

    .line 1154309
    const/4 v5, 0x0

    .line 1154310
    invoke-static {v2}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 1154311
    const/4 v3, 0x0

    .line 1154312
    :goto_10
    move-object v1, v3

    .line 1154313
    move-object v0, v1

    .line 1154314
    goto/16 :goto_1

    .line 1154315
    :pswitch_b
    iget-object v0, p0, LX/6tK;->h:LX/6tp;

    .line 1154316
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->j()LX/0am;

    move-result-object v1

    .line 1154317
    if-nez v1, :cond_23

    .line 1154318
    invoke-static {}, LX/73O;->newBuilder()LX/73P;

    move-result-object v1

    sget-object v2, LX/73N;->FLOATING_LABEL_TEXT:LX/73N;

    .line 1154319
    iput-object v2, v1, LX/73P;->a:LX/73N;

    .line 1154320
    move-object v1, v1

    .line 1154321
    iget-object v2, v0, LX/6tp;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p2, 0x7f081d3f

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1154322
    iput-object v2, v1, LX/73P;->b:Ljava/lang/String;

    .line 1154323
    move-object v1, v1

    .line 1154324
    new-instance v2, LX/6tC;

    sget-object p2, LX/6so;->CHECKOUT_OPTION:LX/6so;

    invoke-virtual {v1}, LX/73P;->e()LX/73O;

    move-result-object v1

    invoke-direct {v2, p2, v1}, LX/6tC;-><init>(LX/6so;LX/73O;)V

    move-object v1, v2

    .line 1154325
    :goto_11
    move-object v0, v1

    .line 1154326
    goto/16 :goto_1

    .line 1154327
    :pswitch_c
    iget-object v0, p0, LX/6tK;->e:LX/6to;

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 1154328
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v4, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->h:LX/0Px;

    .line 1154329
    if-eqz v4, :cond_3

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_25

    .line 1154330
    :cond_3
    const/4 v1, 0x0

    .line 1154331
    :goto_12
    move-object v0, v1

    .line 1154332
    goto/16 :goto_1

    .line 1154333
    :pswitch_d
    iget-object v0, p0, LX/6tK;->d:LX/6tq;

    const/4 v2, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1154334
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v3, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->k:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    .line 1154335
    iget-boolean v1, v3, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->c:Z

    if-eqz v1, :cond_2b

    move-object v1, v2

    .line 1154336
    :goto_13
    move-object v0, v1

    .line 1154337
    goto/16 :goto_1

    .line 1154338
    :cond_4
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v4, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->v:LX/0Px;

    .line 1154339
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1154340
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v3, v1

    :goto_14
    if-ge v3, v6, :cond_5

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    .line 1154341
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->u()LX/0P1;

    move-result-object v7

    iget-object v8, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Collection;

    invoke-static {v7}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1154342
    new-instance v7, LX/6tA;

    sget-object v8, LX/6so;->ACTIONABLE_CHECKOUT_OPTION:LX/6so;

    iget-object v9, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->c:Ljava/lang/String;

    invoke-static {v0, p2, v1}, LX/6ta;->c(LX/6ta;Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Landroid/content/Intent;

    move-result-object v10

    invoke-static {v1}, LX/6ta;->a(Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)I

    move-result p0

    invoke-direct {v7, v8, v9, v10, p0}, LX/6tA;-><init>(LX/6so;Ljava/lang/String;Landroid/content/Intent;I)V

    move-object v7, v7

    .line 1154343
    :goto_15
    move-object v1, v7

    .line 1154344
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154345
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_14

    .line 1154346
    :cond_5
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1154347
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    move-object v1, v2

    goto/16 :goto_0

    :cond_6
    new-instance v1, LX/6sn;

    invoke-direct {v1, v3}, LX/6sn;-><init>(LX/0Px;)V

    goto/16 :goto_0

    :cond_7
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->u()LX/0P1;

    move-result-object v7

    iget-object v8, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0Px;

    .line 1154348
    invoke-static {}, LX/73O;->newBuilder()LX/73P;

    move-result-object v8

    sget-object v9, LX/73N;->FLOATING_LABEL_TEXT:LX/73N;

    .line 1154349
    iput-object v9, v8, LX/73P;->a:LX/73N;

    .line 1154350
    move-object v8, v8

    .line 1154351
    iget-object v9, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->b:Ljava/lang/String;

    .line 1154352
    iput-object v9, v8, LX/73P;->b:Ljava/lang/String;

    .line 1154353
    move-object v8, v8

    .line 1154354
    const/4 v10, 0x0

    .line 1154355
    invoke-virtual {v7}, LX/0Px;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_8

    const/4 v9, 0x1

    :goto_16
    const-string p0, "Empty selected option should display action text"

    invoke-static {v9, p0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1154356
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 1154357
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result p1

    :goto_17
    if-ge v10, p1, :cond_9

    invoke-virtual {v7, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    .line 1154358
    iget-object v9, v9, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->b:Ljava/lang/String;

    invoke-virtual {p0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1154359
    add-int/lit8 v10, v10, 0x1

    goto :goto_17

    :cond_8
    move v9, v10

    .line 1154360
    goto :goto_16

    .line 1154361
    :cond_9
    iget-object v9, v0, LX/6ta;->c:LX/3GL;

    invoke-virtual {v9, p0}, LX/3GL;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v9

    move-object v9, v9

    .line 1154362
    iput-object v9, v8, LX/73P;->c:Ljava/lang/String;

    .line 1154363
    move-object v8, v8

    .line 1154364
    invoke-virtual {v8}, LX/73P;->e()LX/73O;

    move-result-object v8

    .line 1154365
    new-instance v9, LX/6tC;

    sget-object v10, LX/6so;->CHECKOUT_OPTION:LX/6so;

    invoke-static {v0, p2, v1}, LX/6ta;->c(LX/6ta;Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Landroid/content/Intent;

    move-result-object p0

    invoke-static {v1}, LX/6ta;->a(Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)I

    move-result p1

    invoke-direct {v9, v10, v8, p0, p1}, LX/6tC;-><init>(LX/6so;LX/73O;Landroid/content/Intent;I)V

    move-object v7, v9

    .line 1154366
    goto :goto_15

    .line 1154367
    :cond_a
    invoke-static {}, LX/0Rj;->notNull()LX/0Rl;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0wv;->c(LX/0Rl;)Z

    move-result v2

    if-eqz v2, :cond_c

    sget-object v2, LX/6td;->a:LX/0Rl;

    invoke-static {v2}, LX/0Rj;->not(LX/0Rl;)LX/0Rl;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0wv;->b(LX/0Rl;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1154368
    iget-object v2, v0, LX/6td;->c:LX/6r9;

    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v2, v3}, LX/6r9;->e(LX/6qw;)LX/6E0;

    move-result-object v2

    .line 1154369
    iget-object v3, v0, LX/6td;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    sget-object v5, LX/6td;->a:LX/0Rl;

    invoke-virtual {v1, v5}, LX/0wv;->a(LX/0Rl;)LX/0wv;

    move-result-object v5

    invoke-virtual {v5}, LX/0wv;->b()LX/0Px;

    move-result-object v5

    invoke-static {v4, v5}, LX/6td;->a(LX/0Rf;Ljava/util/List;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1154370
    invoke-interface {v2, p2}, LX/6E0;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    move-result-object v5

    .line 1154371
    invoke-interface {v2, p2}, LX/6E0;->c(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    move-result-object v3

    .line 1154372
    if-nez v5, :cond_b

    if-eqz v3, :cond_e

    :cond_b
    const/4 v2, 0x1

    :goto_18
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1154373
    if-eqz v5, :cond_f

    iget-object v2, v0, LX/6td;->b:Landroid/content/Context;

    invoke-static {v2, v5}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;)Landroid/content/Intent;

    move-result-object v2

    move-object v3, v2

    .line 1154374
    :goto_19
    if-eqz v5, :cond_10

    const/16 v2, 0x6c

    .line 1154375
    :goto_1a
    new-instance v5, LX/6tA;

    sget-object v6, LX/6so;->ACTIONABLE_CHECKOUT_OPTION:LX/6so;

    invoke-direct {v5, v6, v4, v3, v2}, LX/6tA;-><init>(LX/6so;Ljava/lang/String;Landroid/content/Intent;I)V

    move-object v1, v5

    .line 1154376
    goto/16 :goto_2

    .line 1154377
    :cond_c
    sget-object v2, LX/6td;->a:LX/0Rl;

    invoke-virtual {v1, v2}, LX/0wv;->c(LX/0Rl;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1154378
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    .line 1154379
    invoke-static {}, LX/73O;->newBuilder()LX/73P;

    move-result-object v2

    sget-object v3, LX/73N;->FLOATING_LABEL_TEXT:LX/73N;

    .line 1154380
    iput-object v3, v2, LX/73P;->a:LX/73N;

    .line 1154381
    move-object v2, v2

    .line 1154382
    iget-object v3, v0, LX/6td;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v1}, LX/6td;->a(LX/0Rf;)I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1154383
    iput-object v1, v2, LX/73P;->b:Ljava/lang/String;

    .line 1154384
    move-object v1, v2

    .line 1154385
    const-string v2, ", "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, LX/6qv;->b(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1154386
    iput-object v2, v1, LX/73P;->c:Ljava/lang/String;

    .line 1154387
    move-object v1, v1

    .line 1154388
    iget-object v2, v0, LX/6td;->c:LX/6r9;

    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v2, v3}, LX/6r9;->e(LX/6qw;)LX/6E0;

    move-result-object v2

    invoke-interface {v2, p2}, LX/6E0;->b(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    move-result-object v2

    .line 1154389
    new-instance v3, LX/6tC;

    sget-object v4, LX/6so;->CHECKOUT_OPTION:LX/6so;

    invoke-virtual {v1}, LX/73P;->e()LX/73O;

    move-result-object v1

    iget-object v5, v0, LX/6td;->b:Landroid/content/Context;

    invoke-static {v5, v2}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;

    move-result-object v2

    const/16 v5, 0x6b

    invoke-direct {v3, v4, v1, v2, v5}, LX/6tC;-><init>(LX/6so;LX/73O;Landroid/content/Intent;I)V

    move-object v1, v3

    .line 1154390
    goto/16 :goto_2

    .line 1154391
    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 1154392
    :cond_e
    const/4 v2, 0x0

    goto :goto_18

    .line 1154393
    :cond_f
    iget-object v2, v0, LX/6td;->b:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;

    move-result-object v2

    move-object v3, v2

    goto :goto_19

    .line 1154394
    :cond_10
    const/16 v2, 0x6b

    goto :goto_1a

    .line 1154395
    :cond_11
    invoke-static {}, LX/73O;->newBuilder()LX/73P;

    move-result-object v2

    sget-object v3, LX/73N;->FLOATING_LABEL_TEXT:LX/73N;

    .line 1154396
    iput-object v3, v2, LX/73P;->a:LX/73N;

    .line 1154397
    move-object v2, v2

    .line 1154398
    iget-object v3, v0, LX/6te;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081e2a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1154399
    iput-object v3, v2, LX/73P;->b:Ljava/lang/String;

    .line 1154400
    move-object v2, v2

    .line 1154401
    check-cast v1, Lcom/facebook/payments/contactinfo/model/NameContactInfo;

    .line 1154402
    iget-object v3, v1, Lcom/facebook/payments/contactinfo/model/NameContactInfo;->a:Ljava/lang/String;

    move-object v1, v3

    .line 1154403
    iput-object v1, v2, LX/73P;->c:Ljava/lang/String;

    .line 1154404
    move-object v2, v2

    .line 1154405
    iget-object v1, v0, LX/6te;->b:LX/6r9;

    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v1, v3}, LX/6r9;->e(LX/6qw;)LX/6E0;

    move-result-object v1

    invoke-interface {v1, p2}, LX/6E0;->d(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    move-result-object v3

    .line 1154406
    new-instance v1, LX/6tC;

    sget-object v4, LX/6so;->CHECKOUT_OPTION:LX/6so;

    invoke-virtual {v2}, LX/73P;->e()LX/73O;

    move-result-object v2

    iget-object v5, v0, LX/6te;->a:Landroid/content/Context;

    invoke-static {v5, v3}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;)Landroid/content/Intent;

    move-result-object v3

    const/16 v5, 0x6d

    invoke-direct {v1, v4, v2, v3, v5}, LX/6tC;-><init>(LX/6so;LX/73O;Landroid/content/Intent;I)V

    goto/16 :goto_3

    .line 1154407
    :cond_12
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1154408
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v3, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->f:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1154409
    iget-object v0, v3, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;->a:Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;

    .line 1154410
    new-instance v4, LX/6sq;

    iget-object v5, v0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->a:Ljava/lang/String;

    iget-object v6, v0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->b:Ljava/lang/String;

    iget-object v7, v0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->c:Ljava/lang/String;

    if-eqz v7, :cond_14

    iget-object v0, v0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_1b
    invoke-direct {v4, v5, v6, v0}, LX/6sq;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 1154411
    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154412
    iget-object v0, v3, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 1154413
    new-instance v0, LX/6su;

    iget-object v3, v3, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, LX/6su;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1154414
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154415
    :cond_13
    new-instance v1, LX/6tG;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-direct {v1, v0}, LX/6tG;-><init>(LX/0Px;)V

    goto/16 :goto_4

    :cond_14
    move-object v0, v1

    .line 1154416
    goto :goto_1b

    .line 1154417
    :cond_15
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_16

    .line 1154418
    iget-object v1, v0, LX/6ti;->b:LX/6r9;

    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v1, v2}, LX/6r9;->e(LX/6qw;)LX/6E0;

    move-result-object v1

    invoke-interface {v1, p2}, LX/6E0;->e(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/model/ShippingParams;

    move-result-object v1

    .line 1154419
    new-instance v2, LX/6tA;

    sget-object v3, LX/6so;->ACTIONABLE_CHECKOUT_OPTION:LX/6so;

    iget-object v4, v0, LX/6ti;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f081e65

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, LX/6ti;->a:Landroid/content/Context;

    invoke-static {v5, v1}, Lcom/facebook/payments/shipping/form/ShippingAddressActivity;->a(Landroid/content/Context;Lcom/facebook/payments/shipping/model/ShippingParams;)Landroid/content/Intent;

    move-result-object v1

    const/16 v5, 0x68

    invoke-direct {v2, v3, v4, v1, v5}, LX/6tA;-><init>(LX/6so;Ljava/lang/String;Landroid/content/Intent;I)V

    move-object v1, v2

    .line 1154420
    goto/16 :goto_5

    .line 1154421
    :cond_16
    invoke-static {}, LX/73O;->newBuilder()LX/73P;

    move-result-object v2

    sget-object v3, LX/73N;->FLOATING_LABEL_TEXT:LX/73N;

    .line 1154422
    iput-object v3, v2, LX/73P;->a:LX/73N;

    .line 1154423
    move-object v2, v2

    .line 1154424
    iget-object v3, v0, LX/6ti;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081e69

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1154425
    iput-object v3, v2, LX/73P;->b:Ljava/lang/String;

    .line 1154426
    move-object v3, v2

    .line 1154427
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/shipping/model/MailingAddress;

    const-string v4, "%s (%s, %s, %s, %s, %s, %s)"

    invoke-interface {v2, v4}, Lcom/facebook/payments/shipping/model/MailingAddress;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1154428
    iput-object v2, v3, LX/73P;->c:Ljava/lang/String;

    .line 1154429
    move-object v2, v3

    .line 1154430
    iget-object v3, v0, LX/6ti;->b:LX/6r9;

    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v3, v4}, LX/6r9;->e(LX/6qw;)LX/6E0;

    move-result-object v3

    invoke-interface {v3, p2}, LX/6E0;->f(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;

    move-result-object v3

    .line 1154431
    new-instance v4, LX/6tC;

    sget-object v5, LX/6so;->CHECKOUT_OPTION:LX/6so;

    invoke-virtual {v2}, LX/73P;->e()LX/73O;

    move-result-object v2

    iget-object v6, v0, LX/6ti;->a:Landroid/content/Context;

    invoke-static {v6, v3}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;

    move-result-object v3

    const/16 v6, 0x67

    invoke-direct {v4, v5, v2, v3, v6}, LX/6tC;-><init>(LX/6so;LX/73O;Landroid/content/Intent;I)V

    move-object v1, v4

    .line 1154432
    goto/16 :goto_5

    .line 1154433
    :cond_17
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v2, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->w:Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    .line 1154434
    iget-object v1, v0, LX/6tj;->a:Landroid/content/res/Resources;

    const v3, 0x7f081d51

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1154435
    invoke-virtual {v2}, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_18

    iget-object v1, v0, LX/6tj;->a:Landroid/content/res/Resources;

    const v4, 0x7f081e43

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1154436
    :goto_1c
    iget-object v4, v0, LX/6tj;->b:Landroid/content/Context;

    sget-object v5, LX/6xK;->NOTE_FORM_CONTROLLER:LX/6xK;

    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v6

    invoke-static {v5, v3, v6}, Lcom/facebook/payments/form/model/PaymentsFormParams;->a(LX/6xK;Ljava/lang/String;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6xW;

    move-result-object v5

    .line 1154437
    new-instance v6, LX/6xT;

    invoke-direct {v6}, LX/6xT;-><init>()V

    move-object v6, v6

    .line 1154438
    iget-object v7, v2, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 1154439
    iput-object v7, v6, LX/6xT;->b:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 1154440
    move-object v6, v6

    .line 1154441
    new-instance v7, Lcom/facebook/payments/form/model/NoteFormData;

    invoke-direct {v7, v6}, Lcom/facebook/payments/form/model/NoteFormData;-><init>(LX/6xT;)V

    move-object v6, v7

    .line 1154442
    iput-object v6, v5, LX/6xW;->e:Lcom/facebook/payments/form/model/PaymentsFormData;

    .line 1154443
    move-object v5, v5

    .line 1154444
    iput-object v1, v5, LX/6xW;->f:Ljava/lang/String;

    .line 1154445
    move-object v1, v5

    .line 1154446
    invoke-virtual {v1}, LX/6xW;->a()Lcom/facebook/payments/form/model/PaymentsFormParams;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/facebook/payments/form/PaymentsFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/form/model/PaymentsFormParams;)Landroid/content/Intent;

    move-result-object v4

    .line 1154447
    invoke-virtual {v2}, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 1154448
    new-instance v1, LX/6tA;

    sget-object v2, LX/6so;->ACTIONABLE_CHECKOUT_OPTION:LX/6so;

    invoke-direct {v1, v2, v3, v4, v8}, LX/6tA;-><init>(LX/6so;Ljava/lang/String;Landroid/content/Intent;I)V

    goto/16 :goto_6

    .line 1154449
    :cond_18
    iget-object v1, v0, LX/6tj;->a:Landroid/content/res/Resources;

    const v4, 0x7f081e44

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1c

    .line 1154450
    :cond_19
    invoke-static {}, LX/73O;->newBuilder()LX/73P;

    move-result-object v1

    sget-object v3, LX/73N;->FLOATING_LABEL_TEXT:LX/73N;

    .line 1154451
    iput-object v3, v1, LX/73P;->a:LX/73N;

    .line 1154452
    move-object v1, v1

    .line 1154453
    iget-object v3, v0, LX/6tj;->a:Landroid/content/res/Resources;

    const v5, 0x7f081d52

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1154454
    iput-object v3, v1, LX/73P;->b:Ljava/lang/String;

    .line 1154455
    move-object v1, v1

    .line 1154456
    invoke-virtual {v2}, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;->b()Ljava/lang/String;

    move-result-object v2

    .line 1154457
    iput-object v2, v1, LX/73P;->c:Ljava/lang/String;

    .line 1154458
    move-object v1, v1

    .line 1154459
    invoke-virtual {v1}, LX/73P;->e()LX/73O;

    move-result-object v2

    .line 1154460
    new-instance v1, LX/6tC;

    sget-object v3, LX/6so;->CHECKOUT_OPTION:LX/6so;

    invoke-direct {v1, v3, v2, v4, v8}, LX/6tC;-><init>(LX/6so;LX/73O;Landroid/content/Intent;I)V

    goto/16 :goto_6

    .line 1154461
    :cond_1a
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_1b

    .line 1154462
    iget-object v1, v0, LX/6tl;->b:LX/6r9;

    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v1, v2}, LX/6r9;->e(LX/6qw;)LX/6E0;

    move-result-object v1

    invoke-interface {v1, p2}, LX/6E0;->h(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    move-result-object v1

    .line 1154463
    new-instance v2, LX/6tA;

    sget-object v3, LX/6so;->ACTIONABLE_CHECKOUT_OPTION:LX/6so;

    iget-object v4, v0, LX/6tl;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080c78

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, LX/6tl;->a:Landroid/content/Context;

    invoke-static {v5, v1}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;

    move-result-object v1

    const/16 v5, 0x65

    invoke-direct {v2, v3, v4, v1, v5}, LX/6tA;-><init>(LX/6so;Ljava/lang/String;Landroid/content/Intent;I)V

    move-object v1, v2

    .line 1154464
    goto/16 :goto_7

    .line 1154465
    :cond_1b
    invoke-static {}, LX/73O;->newBuilder()LX/73P;

    move-result-object v2

    sget-object v3, LX/73N;->FLOATING_LABEL_TEXT:LX/73N;

    .line 1154466
    iput-object v3, v2, LX/73P;->a:LX/73N;

    .line 1154467
    move-object v2, v2

    .line 1154468
    iget-object v3, v0, LX/6tl;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081d41

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1154469
    iput-object v3, v2, LX/73P;->b:Ljava/lang/String;

    .line 1154470
    move-object v3, v2

    .line 1154471
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    iget-object v4, v0, LX/6tl;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    .line 1154472
    iput-object v2, v3, LX/73P;->c:Ljava/lang/String;

    .line 1154473
    move-object v2, v3

    .line 1154474
    iget-object v3, v0, LX/6tl;->b:LX/6r9;

    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v3, v4}, LX/6r9;->e(LX/6qw;)LX/6E0;

    move-result-object v3

    invoke-interface {v3, p2}, LX/6E0;->h(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    move-result-object v3

    .line 1154475
    new-instance v4, LX/6tC;

    sget-object v5, LX/6so;->CHECKOUT_OPTION:LX/6so;

    invoke-virtual {v2}, LX/73P;->e()LX/73O;

    move-result-object v2

    iget-object v6, v0, LX/6tl;->a:Landroid/content/Context;

    invoke-static {v6, v3}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;

    move-result-object v3

    const/16 v6, 0x64

    invoke-direct {v4, v5, v2, v3, v6}, LX/6tC;-><init>(LX/6so;LX/73O;Landroid/content/Intent;I)V

    move-object v1, v4

    .line 1154476
    goto/16 :goto_7

    .line 1154477
    :cond_1c
    iget-object v3, v0, LX/6tm;->b:LX/5fv;

    invoke-virtual {v3, v6, v4}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_b

    .line 1154478
    :cond_1d
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/16 :goto_c

    .line 1154479
    :cond_1e
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_8

    .line 1154480
    :cond_1f
    sget-object v4, LX/5fu;->NO_EMPTY_DECIMALS:LX/5fu;

    goto/16 :goto_9

    :cond_20
    move-object v2, v3

    goto/16 :goto_f

    .line 1154481
    :cond_21
    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    .line 1154482
    invoke-virtual {v3}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->c()Z

    move-result v4

    if-eqz v4, :cond_22

    .line 1154483
    iget-object v3, v3, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b:LX/0Px;

    const/4 v4, 0x1

    invoke-static {v0, v1, v3, v4}, LX/6tn;->a(LX/6tn;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/0Px;Z)LX/6t8;

    move-result-object v4

    .line 1154484
    invoke-static {v0, v1, v2, v5}, LX/6tn;->a(LX/6tn;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/0Px;Z)LX/6t8;

    move-result-object v5

    .line 1154485
    new-instance v3, LX/6t7;

    invoke-static {v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-direct {v3, v4}, LX/6t7;-><init>(LX/0Px;)V

    goto/16 :goto_10

    .line 1154486
    :cond_22
    invoke-static {v0, v1, v2, v5}, LX/6tn;->a(LX/6tn;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/0Px;Z)LX/6t8;

    move-result-object v3

    goto/16 :goto_10

    .line 1154487
    :cond_23
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_24

    .line 1154488
    iget-object v1, v0, LX/6tp;->b:LX/6r9;

    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v1, v2}, LX/6r9;->e(LX/6qw;)LX/6E0;

    move-result-object v1

    invoke-interface {v1, p2}, LX/6E0;->g(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;

    move-result-object v1

    .line 1154489
    new-instance v2, LX/6tA;

    sget-object v3, LX/6so;->ACTIONABLE_CHECKOUT_OPTION:LX/6so;

    iget-object v4, v0, LX/6tp;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f081d40

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, LX/6tp;->a:Landroid/content/Context;

    invoke-static {v5, v1}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;

    move-result-object v1

    const/16 v5, 0x66

    invoke-direct {v2, v3, v4, v1, v5}, LX/6tA;-><init>(LX/6so;Ljava/lang/String;Landroid/content/Intent;I)V

    move-object v1, v2

    .line 1154490
    goto/16 :goto_11

    .line 1154491
    :cond_24
    invoke-static {}, LX/73O;->newBuilder()LX/73P;

    move-result-object v2

    sget-object v3, LX/73N;->FLOATING_LABEL_TEXT:LX/73N;

    .line 1154492
    iput-object v3, v2, LX/73P;->a:LX/73N;

    .line 1154493
    move-object v2, v2

    .line 1154494
    iget-object v3, v0, LX/6tp;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081d3f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1154495
    iput-object v3, v2, LX/73P;->b:Ljava/lang/String;

    .line 1154496
    move-object v3, v2

    .line 1154497
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/shipping/model/ShippingOption;

    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/ShippingOption;->b()Ljava/lang/String;

    move-result-object v2

    .line 1154498
    iput-object v2, v3, LX/73P;->c:Ljava/lang/String;

    .line 1154499
    move-object v2, v3

    .line 1154500
    iget-object v3, v0, LX/6tp;->b:LX/6r9;

    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v3, v4}, LX/6r9;->e(LX/6qw;)LX/6E0;

    move-result-object v3

    invoke-interface {v3, p2}, LX/6E0;->g(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;

    move-result-object v3

    .line 1154501
    new-instance v4, LX/6tC;

    sget-object v5, LX/6so;->CHECKOUT_OPTION:LX/6so;

    invoke-virtual {v2}, LX/73P;->e()LX/73O;

    move-result-object v2

    iget-object v6, v0, LX/6tp;->a:Landroid/content/Context;

    invoke-static {v6, v3}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;

    move-result-object v3

    const/16 v6, 0x66

    invoke-direct {v4, v5, v2, v3, v6}, LX/6tC;-><init>(LX/6so;LX/73O;Landroid/content/Intent;I)V

    move-object v1, v4

    .line 1154502
    goto/16 :goto_11

    .line 1154503
    :cond_25
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v6, :cond_28

    .line 1154504
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;

    .line 1154505
    iget-object v2, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->a:Ljava/lang/String;

    invoke-static {v2}, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->a(Ljava/lang/String;)LX/73R;

    move-result-object v2

    iget-object v4, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->b:Ljava/lang/String;

    .line 1154506
    iput-object v4, v2, LX/73R;->d:Ljava/lang/String;

    .line 1154507
    move-object v2, v2

    .line 1154508
    iget-object v4, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->c:Ljava/lang/String;

    .line 1154509
    iput-object v4, v2, LX/73R;->e:Ljava/lang/String;

    .line 1154510
    move-object v4, v2

    .line 1154511
    iget-object v2, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->e:Ljava/lang/String;

    if-nez v2, :cond_27

    .line 1154512
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1154513
    :goto_1d
    iput-object v2, v4, LX/73R;->c:LX/0Px;

    .line 1154514
    move-object v2, v4

    .line 1154515
    iget-object v4, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->d:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_26

    .line 1154516
    iget-object v4, v0, LX/6to;->a:Landroid/content/res/Resources;

    const v5, 0x7f081d50

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v1, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->d:Ljava/lang/String;

    aput-object v1, v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1154517
    iput-object v1, v2, LX/73R;->f:Ljava/lang/String;

    .line 1154518
    :cond_26
    move-object v1, v2

    .line 1154519
    :goto_1e
    new-instance v2, LX/6tE;

    invoke-virtual {v1}, LX/73R;->a()Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    move-result-object v1

    invoke-direct {v2, v1}, LX/6tE;-><init>(Lcom/facebook/payments/ui/MediaGridTextLayoutParams;)V

    move-object v1, v2

    goto/16 :goto_12

    .line 1154520
    :cond_27
    iget-object v2, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->e:Ljava/lang/String;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    goto :goto_1d

    .line 1154521
    :cond_28
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1154522
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 1154523
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v7

    move v2, v3

    :goto_1f
    if-ge v2, v7, :cond_2a

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;

    .line 1154524
    iget-object v3, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->a:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1154525
    iget-object v3, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->e:Ljava/lang/String;

    if-eqz v3, :cond_29

    .line 1154526
    iget-object v1, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->e:Ljava/lang/String;

    invoke-virtual {v6, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1154527
    :cond_29
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1f

    .line 1154528
    :cond_2a
    iget-object v1, v0, LX/6to;->b:LX/3GL;

    invoke-virtual {v1, v5}, LX/3GL;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->a(Ljava/lang/String;)LX/73R;

    move-result-object v1

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1154529
    iput-object v2, v1, LX/73R;->c:LX/0Px;

    .line 1154530
    move-object v1, v1

    .line 1154531
    goto :goto_1e

    .line 1154532
    :cond_2b
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->y:Ljava/lang/String;

    .line 1154533
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2c

    .line 1154534
    iget-object v1, v0, LX/6tq;->a:Landroid/content/res/Resources;

    const v4, 0x7f081d47

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1154535
    :cond_2c
    iget-object v4, v0, LX/6tq;->b:LX/6r9;

    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v5

    iget-object v5, v5, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v4, v5}, LX/6r9;->b(LX/6qw;)LX/6qd;

    move-result-object v4

    invoke-interface {v4, p2}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Z

    move-result v4

    .line 1154536
    if-nez v4, :cond_2d

    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->d()Z

    move-result v4

    if-nez v4, :cond_2d

    move-object v1, v2

    .line 1154537
    goto/16 :goto_13

    .line 1154538
    :cond_2d
    iget-object v2, v3, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->f:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2e

    iget-object v2, v3, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->e:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2e

    .line 1154539
    iget-object v2, v0, LX/6tq;->a:Landroid/content/res/Resources;

    const v4, 0x7f081d4c

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v3, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->e:Ljava/lang/String;

    aput-object v6, v5, v7

    iget-object v6, v3, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->f:Ljava/lang/String;

    aput-object v6, v5, v8

    aput-object v1, v5, v9

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1154540
    :goto_20
    new-instance v2, LX/6tN;

    iget-object v3, v3, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->d:Landroid/net/Uri;

    invoke-direct {v2, v1, v3}, LX/6tN;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object v1, v2

    goto/16 :goto_13

    .line 1154541
    :cond_2e
    iget-object v2, v3, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->f:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2f

    .line 1154542
    iget-object v2, v0, LX/6tq;->a:Landroid/content/res/Resources;

    const v4, 0x7f081d4a

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, v3, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->f:Ljava/lang/String;

    aput-object v6, v5, v7

    aput-object v1, v5, v8

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_20

    .line 1154543
    :cond_2f
    iget-object v2, v3, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->e:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_30

    .line 1154544
    iget-object v2, v0, LX/6tq;->a:Landroid/content/res/Resources;

    const v4, 0x7f081d4b

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, v3, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->e:Ljava/lang/String;

    aput-object v6, v5, v7

    aput-object v1, v5, v8

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_20

    .line 1154545
    :cond_30
    iget-object v2, v0, LX/6tq;->a:Landroid/content/res/Resources;

    const v4, 0x7f081d49

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v1, v5, v7

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_20

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
