.class public final enum LX/7Sc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Sc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Sc;

.field public static final enum COLOR_FILTER_CHANGE:LX/7Sc;

.field public static final enum DOODLE_DATA:LX/7Sc;

.field public static final enum FPS_TOGGLE_EVENT:LX/7Sc;

.field public static final enum INPUT_FACING:LX/7Sc;

.field public static final enum INPUT_PREVIEW:LX/7Sc;

.field public static final enum INPUT_PREVIEW_SIZE:LX/7Sc;

.field public static final enum INPUT_ROTATION:LX/7Sc;

.field public static final enum MSQRD_EFFECT:LX/7Sc;

.field public static final enum PARTICLES_CONFIG:LX/7Sc;

.field public static final enum SHADER_FILTER:LX/7Sc;

.field public static final enum STYLE_TRANSFER:LX/7Sc;

.field public static final enum UNDEFINED:LX/7Sc;


# instance fields
.field private final isSystemEvent:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1209015
    new-instance v0, LX/7Sc;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v3, v4}, LX/7Sc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7Sc;->UNDEFINED:LX/7Sc;

    .line 1209016
    new-instance v0, LX/7Sc;

    const-string v1, "COLOR_FILTER_CHANGE"

    invoke-direct {v0, v1, v4, v3}, LX/7Sc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7Sc;->COLOR_FILTER_CHANGE:LX/7Sc;

    .line 1209017
    new-instance v0, LX/7Sc;

    const-string v1, "DOODLE_DATA"

    invoke-direct {v0, v1, v5, v3}, LX/7Sc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7Sc;->DOODLE_DATA:LX/7Sc;

    .line 1209018
    new-instance v0, LX/7Sc;

    const-string v1, "INPUT_PREVIEW"

    invoke-direct {v0, v1, v6, v4}, LX/7Sc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7Sc;->INPUT_PREVIEW:LX/7Sc;

    .line 1209019
    new-instance v0, LX/7Sc;

    const-string v1, "INPUT_PREVIEW_SIZE"

    invoke-direct {v0, v1, v7, v4}, LX/7Sc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7Sc;->INPUT_PREVIEW_SIZE:LX/7Sc;

    .line 1209020
    new-instance v0, LX/7Sc;

    const-string v1, "INPUT_FACING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v4}, LX/7Sc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7Sc;->INPUT_FACING:LX/7Sc;

    .line 1209021
    new-instance v0, LX/7Sc;

    const-string v1, "INPUT_ROTATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4}, LX/7Sc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7Sc;->INPUT_ROTATION:LX/7Sc;

    .line 1209022
    new-instance v0, LX/7Sc;

    const-string v1, "PARTICLES_CONFIG"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/7Sc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7Sc;->PARTICLES_CONFIG:LX/7Sc;

    .line 1209023
    new-instance v0, LX/7Sc;

    const-string v1, "MSQRD_EFFECT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/7Sc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7Sc;->MSQRD_EFFECT:LX/7Sc;

    .line 1209024
    new-instance v0, LX/7Sc;

    const-string v1, "SHADER_FILTER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, LX/7Sc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7Sc;->SHADER_FILTER:LX/7Sc;

    .line 1209025
    new-instance v0, LX/7Sc;

    const-string v1, "FPS_TOGGLE_EVENT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/7Sc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7Sc;->FPS_TOGGLE_EVENT:LX/7Sc;

    .line 1209026
    new-instance v0, LX/7Sc;

    const-string v1, "STYLE_TRANSFER"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v3}, LX/7Sc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7Sc;->STYLE_TRANSFER:LX/7Sc;

    .line 1209027
    const/16 v0, 0xc

    new-array v0, v0, [LX/7Sc;

    sget-object v1, LX/7Sc;->UNDEFINED:LX/7Sc;

    aput-object v1, v0, v3

    sget-object v1, LX/7Sc;->COLOR_FILTER_CHANGE:LX/7Sc;

    aput-object v1, v0, v4

    sget-object v1, LX/7Sc;->DOODLE_DATA:LX/7Sc;

    aput-object v1, v0, v5

    sget-object v1, LX/7Sc;->INPUT_PREVIEW:LX/7Sc;

    aput-object v1, v0, v6

    sget-object v1, LX/7Sc;->INPUT_PREVIEW_SIZE:LX/7Sc;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7Sc;->INPUT_FACING:LX/7Sc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7Sc;->INPUT_ROTATION:LX/7Sc;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7Sc;->PARTICLES_CONFIG:LX/7Sc;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7Sc;->MSQRD_EFFECT:LX/7Sc;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7Sc;->SHADER_FILTER:LX/7Sc;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7Sc;->FPS_TOGGLE_EVENT:LX/7Sc;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7Sc;->STYLE_TRANSFER:LX/7Sc;

    aput-object v2, v0, v1

    sput-object v0, LX/7Sc;->$VALUES:[LX/7Sc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 1209028
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1209029
    iput-boolean p3, p0, LX/7Sc;->isSystemEvent:Z

    .line 1209030
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Sc;
    .locals 1

    .prologue
    .line 1209031
    const-class v0, LX/7Sc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Sc;

    return-object v0
.end method

.method public static values()[LX/7Sc;
    .locals 1

    .prologue
    .line 1209032
    sget-object v0, LX/7Sc;->$VALUES:[LX/7Sc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Sc;

    return-object v0
.end method


# virtual methods
.method public final isSystemEvent()Z
    .locals 1

    .prologue
    .line 1209033
    iget-boolean v0, p0, LX/7Sc;->isSystemEvent:Z

    return v0
.end method
