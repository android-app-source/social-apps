.class public final enum LX/7CL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7CL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7CL;

.field public static final enum BACK_BUTTON:LX/7CL;

.field public static final enum CLEAR_BUTTON:LX/7CL;

.field public static final enum EDIT_TEXT:LX/7CL;

.field public static final enum UP_BUTTON:LX/7CL;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1180453
    new-instance v0, LX/7CL;

    const-string v1, "BACK_BUTTON"

    const-string v2, "end_back_button"

    invoke-direct {v0, v1, v3, v2}, LX/7CL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7CL;->BACK_BUTTON:LX/7CL;

    .line 1180454
    new-instance v0, LX/7CL;

    const-string v1, "UP_BUTTON"

    const-string v2, "up_button"

    invoke-direct {v0, v1, v4, v2}, LX/7CL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7CL;->UP_BUTTON:LX/7CL;

    .line 1180455
    new-instance v0, LX/7CL;

    const-string v1, "CLEAR_BUTTON"

    const-string v2, "clear_button"

    invoke-direct {v0, v1, v5, v2}, LX/7CL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7CL;->CLEAR_BUTTON:LX/7CL;

    .line 1180456
    new-instance v0, LX/7CL;

    const-string v1, "EDIT_TEXT"

    const-string v2, "edit_text"

    invoke-direct {v0, v1, v6, v2}, LX/7CL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7CL;->EDIT_TEXT:LX/7CL;

    .line 1180457
    const/4 v0, 0x4

    new-array v0, v0, [LX/7CL;

    sget-object v1, LX/7CL;->BACK_BUTTON:LX/7CL;

    aput-object v1, v0, v3

    sget-object v1, LX/7CL;->UP_BUTTON:LX/7CL;

    aput-object v1, v0, v4

    sget-object v1, LX/7CL;->CLEAR_BUTTON:LX/7CL;

    aput-object v1, v0, v5

    sget-object v1, LX/7CL;->EDIT_TEXT:LX/7CL;

    aput-object v1, v0, v6

    sput-object v0, LX/7CL;->$VALUES:[LX/7CL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1180458
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1180459
    iput-object p3, p0, LX/7CL;->name:Ljava/lang/String;

    .line 1180460
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7CL;
    .locals 1

    .prologue
    .line 1180461
    const-class v0, LX/7CL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7CL;

    return-object v0
.end method

.method public static values()[LX/7CL;
    .locals 1

    .prologue
    .line 1180462
    sget-object v0, LX/7CL;->$VALUES:[LX/7CL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7CL;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1180463
    iget-object v0, p0, LX/7CL;->name:Ljava/lang/String;

    return-object v0
.end method
