.class public LX/6sW;
.super LX/6sV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sV",
        "<",
        "Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;",
        "Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:LX/6xb;

.field private final d:LX/0dC;

.field private final e:LX/6sj;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/6xb;LX/0dC;LX/6sj;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1153448
    const-class v0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;

    invoke-direct {p0, p1, v0}, LX/6sV;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 1153449
    iput-object p2, p0, LX/6sW;->c:LX/6xb;

    .line 1153450
    iput-object p3, p0, LX/6sW;->d:LX/0dC;

    .line 1153451
    iput-object p4, p0, LX/6sW;->e:LX/6sj;

    .line 1153452
    return-void
.end method

.method private static a(LX/1pN;)Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;
    .locals 4

    .prologue
    .line 1153501
    invoke-virtual {p0}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1153502
    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1153503
    new-instance v1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;

    const-string v2, "id"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "extra_data"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;-><init>(Ljava/lang/String;LX/0lF;)V

    return-object v1
.end method

.method public static b(LX/0QB;)LX/6sW;
    .locals 5

    .prologue
    .line 1153453
    new-instance v4, LX/6sW;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-static {p0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object v1

    check-cast v1, LX/6xb;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v2

    check-cast v2, LX/0dC;

    invoke-static {p0}, LX/6sj;->a(LX/0QB;)LX/6sj;

    move-result-object v3

    check-cast v3, LX/6sj;

    invoke-direct {v4, v0, v1, v2, v3}, LX/6sW;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/6xb;LX/0dC;LX/6sj;)V

    .line 1153454
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 1153455
    check-cast p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;

    .line 1153456
    iget-object v0, p0, LX/6sW;->c:LX/6xb;

    iget-object v1, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    const-string v2, "request_id"

    iget-object v3, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/6xb;->c(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;Ljava/lang/String;)V

    .line 1153457
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1153458
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153459
    invoke-static {v1, p1}, LX/6sX;->a(Ljava/util/ArrayList;Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;)V

    .line 1153460
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->RECEIVER_ID:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->e:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153461
    iget-object v0, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v0, :cond_0

    .line 1153462
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->CURRENCY:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1153463
    iget-object v4, v3, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v3, v4

    .line 1153464
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153465
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->AMOUNT:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1153466
    iget-object v4, v3, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v3, v4

    .line 1153467
    invoke-virtual {v3}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153468
    :cond_0
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->REQUEST_ID:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->g:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153469
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->MERCHANT_DESCRIPTOR:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->h:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153470
    iget-object v0, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->j:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    if-eqz v0, :cond_1

    .line 1153471
    iget-object v0, p0, LX/6sW;->e:LX/6sj;

    iget-object v2, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->j:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->b()LX/6zU;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/6sj;->a(LX/6zU;)LX/6sf;

    move-result-object v0

    iget-object v2, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->j:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-interface {v0, v2}, LX/6sf;->a(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)LX/0Px;

    move-result-object v0

    .line 1153472
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 1153473
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1153474
    :cond_1
    iget-object v0, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->k:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v0, :cond_2

    .line 1153475
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->TAX_CURRENCY:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->k:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1153476
    iget-object v4, v3, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v3, v4

    .line 1153477
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153478
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->TAX_AMOUNT:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->k:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1153479
    iget-object v4, v3, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v3, v4

    .line 1153480
    invoke-virtual {v3}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153481
    :cond_2
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->EMAIL_ADDRESS_ID:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->l:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153482
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->CONTACT_NAME:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->m:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153483
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->CONTACT_NUMBER_ID:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->n:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153484
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->CSC:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->s:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153485
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->SECURITY_PIN:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->q:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153486
    iget-object v0, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->r:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1153487
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->SECURITY_BIOMETRIC_NONCE:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->r:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153488
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->SECURITY_DEVICE_ID:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/6sW;->d:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153489
    :cond_3
    const-string v0, "/me/payments"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, LX/73t;->a(Ljava/lang/String;[Ljava/lang/Object;)LX/14O;

    move-result-object v0

    const-string v2, "checkout_charge"

    .line 1153490
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 1153491
    move-object v0, v0

    .line 1153492
    const-string v2, "POST"

    .line 1153493
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 1153494
    move-object v0, v0

    .line 1153495
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1153496
    move-object v0, v0

    .line 1153497
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1153498
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1153499
    move-object v0, v0

    .line 1153500
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/os/Parcelable;LX/1pN;)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 1153445
    invoke-static {p2}, LX/6sW;->a(LX/1pN;)Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1153446
    invoke-static {p2}, LX/6sW;->a(LX/1pN;)Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1153447
    const-string v0, "checkout_charge"

    return-object v0
.end method
