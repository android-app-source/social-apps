.class public final LX/7UY;
.super LX/7UP;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;)V
    .locals 0

    .prologue
    .line 1213023
    iput-object p1, p0, LX/7UY;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-direct {p0, p1}, LX/7UP;-><init>(LX/7UR;)V

    return-void
.end method


# virtual methods
.method public final onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4

    .prologue
    .line 1213024
    iget-object v0, p0, LX/7UY;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget v0, v0, LX/7UR;->d:F

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    mul-float/2addr v0, v1

    .line 1213025
    iget-object v1, p0, LX/7UY;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget-object v1, v1, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 1213026
    :cond_0
    iget-object v1, p0, LX/7UY;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget-boolean v1, v1, LX/7UR;->j:Z

    if-eqz v1, :cond_1

    .line 1213027
    iget-object v1, p0, LX/7UY;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-virtual {v1}, LX/7UQ;->getMaxZoom()F

    move-result v1

    iget-object v2, p0, LX/7UY;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-virtual {v2}, LX/7UQ;->getMinZoom()F

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1213028
    iget-object v1, p0, LX/7UY;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, LX/7UQ;->a(FFF)V

    .line 1213029
    iget-object v1, p0, LX/7UY;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget-object v2, p0, LX/7UY;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-virtual {v2}, LX/7UQ;->getMaxZoom()F

    move-result v2

    iget-object v3, p0, LX/7UY;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-virtual {v3}, LX/7UQ;->getMinZoom()F

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, v1, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->d:F

    .line 1213030
    iget-object v0, p0, LX/7UY;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    const/4 v1, -0x1

    iput v1, v0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->f:I

    .line 1213031
    iget-object v0, p0, LX/7UY;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->invalidate()V

    .line 1213032
    const/4 v0, 0x1

    .line 1213033
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .prologue
    .line 1213020
    iget-object v0, p0, LX/7UY;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget-object v0, v0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7US;

    .line 1213021
    invoke-interface {v0}, LX/7US;->d()V

    goto :goto_0

    .line 1213022
    :cond_0
    invoke-super {p0, p1}, LX/7UP;->onScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    move-result v0

    return v0
.end method

.method public final onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2

    .prologue
    .line 1213017
    iget-object v0, p0, LX/7UY;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget-object v0, v0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 1213018
    :cond_0
    invoke-super {p0, p1}, LX/7UP;->onScaleEnd(Landroid/view/ScaleGestureDetector;)V

    .line 1213019
    return-void
.end method
