.class public LX/7iw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/7j6;

.field public final b:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/7iP;


# direct methods
.method public constructor <init>(LX/7j6;Lcom/facebook/fbui/widget/contentview/ContentView;)V
    .locals 1
    .param p2    # Lcom/facebook/fbui/widget/contentview/ContentView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1228901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1228902
    sget-object v0, LX/7iP;->UNKNOWN:LX/7iP;

    iput-object v0, p0, LX/7iw;->d:LX/7iP;

    .line 1228903
    iput-object p1, p0, LX/7iw;->a:LX/7j6;

    .line 1228904
    iput-object p2, p0, LX/7iw;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1228905
    iget-object v0, p0, LX/7iw;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    new-instance p1, LX/7iv;

    invoke-direct {p1, p0}, LX/7iv;-><init>(LX/7iw;)V

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1228906
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1228891
    iget-object v0, p0, LX/7iw;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1228892
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1228899
    iget-object v0, p0, LX/7iw;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1228900
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 1228896
    iget-object v0, p0, LX/7iw;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f009f

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1228897
    iget-object v1, p0, LX/7iw;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 1228898
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1228893
    iget-object v1, p0, LX/7iw;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setVisibility(I)V

    .line 1228894
    return-void

    .line 1228895
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
