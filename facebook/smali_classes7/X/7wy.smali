.class public final LX/7wy;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7wz;


# direct methods
.method public constructor <init>(LX/7wz;)V
    .locals 0

    .prologue
    .line 1276866
    iput-object p1, p0, LX/7wy;->a:LX/7wz;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1276867
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1276868
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1276869
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1276870
    if-nez v0, :cond_0

    .line 1276871
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null GraphQL result"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/7wy;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1276872
    :goto_0
    return-void

    .line 1276873
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1276874
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1276875
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null Event GraphQL result"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/7wy;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1276876
    :cond_1
    iget-object v0, p0, LX/7wy;->a:LX/7wz;

    iget-object v9, v0, LX/7wz;->a:Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;

    .line 1276877
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1276878
    move-object v8, v0

    check-cast v8, LX/7og;

    .line 1276879
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1276880
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->eR_()Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, LX/7wy;->a:LX/7wz;

    iget-object v0, v0, LX/7wz;->e:LX/7xH;

    .line 1276881
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1276882
    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->eQ_()Z

    move-result v1

    .line 1276883
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 1276884
    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->j()J

    move-result-wide v2

    .line 1276885
    iget-object v4, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 1276886
    check-cast v4, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->b()J

    move-result-wide v4

    .line 1276887
    iget-object v6, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v6, v6

    .line 1276888
    check-cast v6, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;

    invoke-virtual {v6}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/7xH;->a(ZJJLcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;)Ljava/lang/String;

    move-result-object v3

    .line 1276889
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1276890
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;->j()Ljava/lang/String;

    move-result-object v4

    .line 1276891
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1276892
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 1276893
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1276894
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;->k()Ljava/lang/String;

    move-result-object v6

    .line 1276895
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1276896
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v7

    move-object v0, v9

    move-object v1, v8

    move-object v2, v10

    invoke-virtual/range {v0 .. v7}, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->a(LX/7og;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)V

    goto/16 :goto_0
.end method
