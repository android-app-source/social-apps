.class public LX/7D5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1182222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)LX/5PR;
    .locals 13
    .param p0    # Lcom/facebook/bitmaps/SphericalPhotoMetadata;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 1182223
    const/16 v0, 0x10

    new-array v0, v0, [F

    .line 1182224
    if-eqz p0, :cond_8

    .line 1182225
    const/4 v10, 0x1

    .line 1182226
    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1182227
    iget-wide v11, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mPoseRollDegrees:D

    move-wide v4, v11

    .line 1182228
    double-to-float v2, v4

    const/high16 v5, -0x40800000    # -1.0f

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 1182229
    iget-wide v11, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mPosePitchDegrees:D

    move-wide v4, v11

    .line 1182230
    double-to-float v6, v4

    move-object v4, v0

    move v5, v1

    move v8, v3

    move v9, v3

    invoke-static/range {v4 .. v9}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 1182231
    iget v1, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaTopPixels:I

    move v1, v1

    .line 1182232
    int-to-float v1, v1

    .line 1182233
    iget v2, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoHeightPixels:I

    move v2, v2

    .line 1182234
    int-to-float v2, v2

    div-float v3, v1, v2

    .line 1182235
    iget v1, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaImageHeightPixels:I

    move v1, v1

    .line 1182236
    int-to-float v1, v1

    .line 1182237
    iget v2, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoHeightPixels:I

    move v2, v2

    .line 1182238
    int-to-float v2, v2

    div-float v4, v1, v2

    .line 1182239
    iget v1, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaLeftPixels:I

    move v1, v1

    .line 1182240
    int-to-float v1, v1

    .line 1182241
    iget v2, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoWidthPixels:I

    move v2, v2

    .line 1182242
    int-to-float v2, v2

    div-float v5, v1, v2

    .line 1182243
    iget v1, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaImageWidthPixels:I

    move v1, v1

    .line 1182244
    int-to-float v1, v1

    .line 1182245
    iget v2, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoWidthPixels:I

    move v2, v2

    .line 1182246
    int-to-float v2, v2

    div-float v6, v1, v2

    move v1, v10

    .line 1182247
    :goto_0
    new-instance v2, LX/5PQ;

    const/16 v7, 0xb43

    invoke-direct {v2, v7}, LX/5PQ;-><init>(I)V

    const/4 v7, 0x4

    .line 1182248
    iput v7, v2, LX/5PQ;->a:I

    .line 1182249
    move-object v7, v2

    .line 1182250
    const-string v8, "aPosition"

    move-object v2, v0

    invoke-static/range {v1 .. v6}, LX/7D5;->a(Z[FFFFF)LX/5Pg;

    move-result-object v0

    invoke-virtual {v7, v8, v0}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    const/16 v12, 0x1e

    const/4 v7, 0x0

    .line 1182251
    const/16 v2, 0x1518

    new-array v9, v2, [S

    move v8, v7

    move v2, v7

    .line 1182252
    :goto_1
    if-ge v8, v12, :cond_5

    move v6, v7

    .line 1182253
    :goto_2
    if-ge v6, v12, :cond_4

    .line 1182254
    mul-int/lit8 v3, v8, 0x1f

    add-int/2addr v3, v6

    int-to-short v10, v3

    .line 1182255
    add-int/lit8 v3, v10, 0x1e

    add-int/lit8 v3, v3, 0x1

    int-to-short v4, v3

    .line 1182256
    add-int/lit8 v3, v10, 0x1

    int-to-short v3, v3

    .line 1182257
    add-int/lit8 v5, v4, 0x1

    int-to-short v5, v5

    .line 1182258
    add-int/lit8 v11, v2, 0x1

    aput-short v10, v9, v2

    .line 1182259
    add-int/lit8 v10, v11, 0x1

    if-eqz v1, :cond_0

    move v2, v3

    :goto_3
    aput-short v2, v9, v11

    .line 1182260
    add-int/lit8 v11, v10, 0x1

    if-eqz v1, :cond_1

    move v2, v4

    :goto_4
    aput-short v2, v9, v10

    .line 1182261
    add-int/lit8 v10, v11, 0x1

    aput-short v4, v9, v11

    .line 1182262
    add-int/lit8 v11, v10, 0x1

    if-eqz v1, :cond_2

    move v2, v3

    :goto_5
    aput-short v2, v9, v10

    .line 1182263
    add-int/lit8 v4, v11, 0x1

    if-eqz v1, :cond_3

    :goto_6
    aput-short v5, v9, v11

    .line 1182264
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v2, v4

    goto :goto_2

    :cond_0
    move v2, v4

    .line 1182265
    goto :goto_3

    :cond_1
    move v2, v3

    .line 1182266
    goto :goto_4

    :cond_2
    move v2, v5

    .line 1182267
    goto :goto_5

    :cond_3
    move v5, v3

    .line 1182268
    goto :goto_6

    .line 1182269
    :cond_4
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    goto :goto_1

    .line 1182270
    :cond_5
    new-instance v2, LX/5PY;

    invoke-direct {v2, v9}, LX/5PY;-><init>([S)V

    move-object v1, v2

    .line 1182271
    invoke-virtual {v0, v1}, LX/5PQ;->a(LX/5PY;)LX/5PQ;

    move-result-object v0

    const-string v1, "aTextureCoord"

    const/16 v11, 0x1e

    const/high16 v10, 0x41f00000    # 30.0f

    const/4 v3, 0x0

    .line 1182272
    const/16 v2, 0x782

    new-array v6, v2, [F

    move v5, v3

    move v2, v3

    .line 1182273
    :goto_7
    if-gt v5, v11, :cond_7

    move v4, v2

    move v2, v3

    .line 1182274
    :goto_8
    if-gt v2, v11, :cond_6

    .line 1182275
    int-to-float v7, v2

    div-float/2addr v7, v10

    .line 1182276
    int-to-float v8, v5

    div-float/2addr v8, v10

    .line 1182277
    add-int/lit8 v9, v4, 0x1

    aput v7, v6, v4

    .line 1182278
    add-int/lit8 v4, v9, 0x1

    aput v8, v6, v9

    .line 1182279
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 1182280
    :cond_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v4

    goto :goto_7

    .line 1182281
    :cond_7
    new-instance v2, LX/5Pg;

    const/4 v3, 0x2

    invoke-direct {v2, v6, v3}, LX/5Pg;-><init>([FI)V

    move-object v2, v2

    .line 1182282
    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    invoke-virtual {v0}, LX/5PQ;->a()LX/5PR;

    move-result-object v0

    return-object v0

    :cond_8
    move v6, v7

    move v5, v3

    move v4, v7

    goto/16 :goto_0
.end method

.method private static a(Z[FFFFF)LX/5Pg;
    .locals 13

    .prologue
    .line 1182283
    const/16 v0, 0xb43

    new-array v11, v0, [F

    .line 1182284
    const/4 v0, 0x4

    new-array v4, v0, [F

    .line 1182285
    const/4 v0, 0x4

    new-array v0, v0, [F

    .line 1182286
    const/4 v2, 0x0

    .line 1182287
    const/4 v1, 0x0

    move v10, v1

    :goto_0
    const/16 v1, 0x1e

    if-gt v10, v1, :cond_7

    .line 1182288
    int-to-float v1, v10

    const/high16 v3, 0x41f00000    # 30.0f

    div-float/2addr v1, v3

    .line 1182289
    if-eqz p0, :cond_1

    .line 1182290
    :goto_1
    mul-float v1, v1, p3

    add-float/2addr v1, p2

    .line 1182291
    const v3, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v1, v3

    .line 1182292
    float-to-double v6, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    double-to-float v7, v6

    .line 1182293
    float-to-double v8, v1

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v6, v8

    .line 1182294
    const/4 v1, 0x0

    move v8, v1

    move v9, v2

    :goto_2
    const/16 v1, 0x1e

    if-gt v8, v1, :cond_6

    .line 1182295
    int-to-float v1, v8

    const/high16 v2, 0x41f00000    # 30.0f

    div-float/2addr v1, v2

    .line 1182296
    if-eqz p0, :cond_2

    .line 1182297
    :goto_3
    mul-float v1, v1, p5

    add-float v1, v1, p4

    .line 1182298
    const v2, 0x40c90fdb

    mul-float/2addr v1, v2

    .line 1182299
    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v5, v2

    .line 1182300
    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 1182301
    if-eqz p0, :cond_3

    neg-float v2, v5

    mul-float v3, v2, v7

    .line 1182302
    :goto_4
    if-eqz p0, :cond_4

    move v2, v6

    .line 1182303
    :goto_5
    if-eqz p0, :cond_5

    mul-float/2addr v1, v7

    .line 1182304
    :goto_6
    if-eqz p0, :cond_0

    .line 1182305
    const/4 v5, 0x0

    aput v3, v4, v5

    .line 1182306
    const/4 v3, 0x1

    aput v2, v4, v3

    .line 1182307
    const/4 v2, 0x2

    aput v1, v4, v2

    .line 1182308
    const/4 v1, 0x3

    const/4 v2, 0x0

    aput v2, v4, v1

    .line 1182309
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 1182310
    const/4 v1, 0x0

    aget v3, v0, v1

    .line 1182311
    const/4 v1, 0x1

    aget v2, v0, v1

    .line 1182312
    const/4 v1, 0x2

    aget v1, v0, v1

    .line 1182313
    :cond_0
    add-int/lit8 v5, v9, 0x1

    const/high16 v12, 0x40000000    # 2.0f

    mul-float/2addr v3, v12

    aput v3, v11, v9

    .line 1182314
    add-int/lit8 v3, v5, 0x1

    const/high16 v9, 0x40000000    # 2.0f

    mul-float/2addr v2, v9

    aput v2, v11, v5

    .line 1182315
    add-int/lit8 v2, v3, 0x1

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v1, v5

    aput v1, v11, v3

    .line 1182316
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move v9, v2

    goto :goto_2

    .line 1182317
    :cond_1
    const/high16 v3, 0x3f000000    # 0.5f

    sub-float/2addr v1, v3

    goto :goto_1

    .line 1182318
    :cond_2
    const/high16 v2, 0x3f000000    # 0.5f

    sub-float/2addr v1, v2

    goto :goto_3

    .line 1182319
    :cond_3
    mul-float v3, v1, v6

    goto :goto_4

    :cond_4
    move v2, v7

    .line 1182320
    goto :goto_5

    .line 1182321
    :cond_5
    mul-float v1, v5, v6

    goto :goto_6

    .line 1182322
    :cond_6
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    move v2, v9

    goto/16 :goto_0

    .line 1182323
    :cond_7
    new-instance v0, LX/5Pg;

    const/4 v1, 0x3

    invoke-direct {v0, v11, v1}, LX/5Pg;-><init>([FI)V

    return-object v0
.end method
