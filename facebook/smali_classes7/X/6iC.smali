.class public final enum LX/6iC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6iC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6iC;

.field public static final enum NEED_MORE_RECENT_MESSAGES:LX/6iC;

.field public static final enum NEED_OLDER_MESSAGES:LX/6iC;

.field public static final enum NOT_IN_MEMORY_CACHE:LX/6iC;

.field public static final enum NOT_MOSTLY_CACHED:LX/6iC;


# instance fields
.field public final parcelValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1129037
    new-instance v0, LX/6iC;

    const-string v1, "NOT_MOSTLY_CACHED"

    invoke-direct {v0, v1, v2, v2}, LX/6iC;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6iC;->NOT_MOSTLY_CACHED:LX/6iC;

    .line 1129038
    new-instance v0, LX/6iC;

    const-string v1, "NEED_MORE_RECENT_MESSAGES"

    invoke-direct {v0, v1, v3, v3}, LX/6iC;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6iC;->NEED_MORE_RECENT_MESSAGES:LX/6iC;

    .line 1129039
    new-instance v0, LX/6iC;

    const-string v1, "NEED_OLDER_MESSAGES"

    invoke-direct {v0, v1, v4, v4}, LX/6iC;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6iC;->NEED_OLDER_MESSAGES:LX/6iC;

    .line 1129040
    new-instance v0, LX/6iC;

    const-string v1, "NOT_IN_MEMORY_CACHE"

    invoke-direct {v0, v1, v5, v5}, LX/6iC;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6iC;->NOT_IN_MEMORY_CACHE:LX/6iC;

    .line 1129041
    const/4 v0, 0x4

    new-array v0, v0, [LX/6iC;

    sget-object v1, LX/6iC;->NOT_MOSTLY_CACHED:LX/6iC;

    aput-object v1, v0, v2

    sget-object v1, LX/6iC;->NEED_MORE_RECENT_MESSAGES:LX/6iC;

    aput-object v1, v0, v3

    sget-object v1, LX/6iC;->NEED_OLDER_MESSAGES:LX/6iC;

    aput-object v1, v0, v4

    sget-object v1, LX/6iC;->NOT_IN_MEMORY_CACHE:LX/6iC;

    aput-object v1, v0, v5

    sput-object v0, LX/6iC;->$VALUES:[LX/6iC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1129034
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1129035
    iput p3, p0, LX/6iC;->parcelValue:I

    .line 1129036
    return-void
.end method

.method public static fromParcelValue(I)LX/6iC;
    .locals 1

    .prologue
    .line 1129042
    packed-switch p0, :pswitch_data_0

    .line 1129043
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1129044
    :pswitch_0
    sget-object v0, LX/6iC;->NOT_MOSTLY_CACHED:LX/6iC;

    .line 1129045
    :goto_0
    return-object v0

    .line 1129046
    :pswitch_1
    sget-object v0, LX/6iC;->NEED_MORE_RECENT_MESSAGES:LX/6iC;

    goto :goto_0

    .line 1129047
    :pswitch_2
    sget-object v0, LX/6iC;->NEED_OLDER_MESSAGES:LX/6iC;

    goto :goto_0

    .line 1129048
    :pswitch_3
    sget-object v0, LX/6iC;->NOT_IN_MEMORY_CACHE:LX/6iC;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/6iC;
    .locals 1

    .prologue
    .line 1129033
    const-class v0, LX/6iC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6iC;

    return-object v0
.end method

.method public static values()[LX/6iC;
    .locals 1

    .prologue
    .line 1129032
    sget-object v0, LX/6iC;->$VALUES:[LX/6iC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6iC;

    return-object v0
.end method
