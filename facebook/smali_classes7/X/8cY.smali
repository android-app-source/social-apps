.class public final LX/8cY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/7By;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/8ca;

.field public final synthetic c:Ljava/lang/StringBuffer;

.field public final synthetic d:LX/3Qc;


# direct methods
.method public constructor <init>(LX/3Qc;Ljava/lang/String;LX/8ca;Ljava/lang/StringBuffer;)V
    .locals 0

    .prologue
    .line 1374617
    iput-object p1, p0, LX/8cY;->d:LX/3Qc;

    iput-object p2, p0, LX/8cY;->a:Ljava/lang/String;

    iput-object p3, p0, LX/8cY;->b:LX/8ca;

    iput-object p4, p0, LX/8cY;->c:Ljava/lang/StringBuffer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1374618
    iget-object v0, p0, LX/8cY;->d:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->e:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_DB_BOOTSTRAP_ENTITY_FAIL:LX/3Ql;

    invoke-virtual {v0, v1, p1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 1374619
    iget-object v0, p0, LX/8cY;->d:LX/3Qc;

    invoke-static {v0}, LX/3Qc;->h(LX/3Qc;)V

    .line 1374620
    iget-object v0, p0, LX/8cY;->d:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->i:LX/3Qe;

    iget-object v1, p0, LX/8cY;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3Qe;->f(Ljava/lang/String;)V

    .line 1374621
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1374622
    check-cast p1, LX/7By;

    .line 1374623
    iget-object v0, p0, LX/8cY;->d:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->i:LX/3Qe;

    iget-object v1, p0, LX/8cY;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3Qe;->e(Ljava/lang/String;)V

    .line 1374624
    :try_start_0
    iget-object v0, p0, LX/8cY;->d:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->m:LX/0Sy;

    invoke-virtual {v0}, LX/0Sy;->c()V

    .line 1374625
    iget-object v0, p0, LX/8cY;->d:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Bf;

    .line 1374626
    iget-object v1, p1, LX/7By;->c:LX/0Px;

    move-object v1, v1

    .line 1374627
    invoke-virtual {v0, v1}, LX/7Bf;->a(LX/0Px;)I

    move-result v1

    .line 1374628
    iget-object v0, p0, LX/8cY;->d:LX/3Qc;

    iget-object v2, v0, LX/3Qc;->i:LX/3Qe;

    iget-object v3, p0, LX/8cY;->a:Ljava/lang/String;

    const-string v4, "source"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, LX/8cY;->b:LX/8ca;

    invoke-virtual {v5}, LX/8ca;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-lez v1, :cond_1

    const-string v0, ""

    :goto_0
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/3Qe;->a(Ljava/lang/String;LX/0P1;)V

    .line 1374629
    if-lez v1, :cond_0

    .line 1374630
    iget-object v0, p0, LX/8cY;->d:LX/3Qc;

    iget-object v2, p0, LX/8cY;->d:LX/3Qc;

    invoke-static {v2}, LX/3Qc;->n(LX/3Qc;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, LX/3Qc;->a$redex0(LX/3Qc;I)V

    .line 1374631
    iget-object v0, p0, LX/8cY;->d:LX/3Qc;

    iget-object v1, p0, LX/8cY;->c:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/3Qc;->a$redex0(LX/3Qc;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1374632
    :cond_0
    :goto_1
    iget-object v0, p0, LX/8cY;->d:LX/3Qc;

    invoke-static {v0}, LX/3Qc;->h(LX/3Qc;)V

    .line 1374633
    return-void

    .line 1374634
    :cond_1
    :try_start_1
    const-string v0, "-AlreadyExists"
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1374635
    :catch_0
    iget-object v0, p0, LX/8cY;->d:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->i:LX/3Qe;

    iget-object v1, p0, LX/8cY;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3Qe;->f(Ljava/lang/String;)V

    goto :goto_1
.end method
