.class public final LX/8BS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public mStartResponse:LX/8Bh;

.field public mVideoPath:Ljava/lang/String;

.field public mVideoPosted:Z

.field public mVideoTransferred:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1309256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309257
    iput-object p1, p0, LX/8BS;->mVideoPath:Ljava/lang/String;

    .line 1309258
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8BS;->mVideoPosted:Z

    iput-boolean v0, p0, LX/8BS;->mVideoTransferred:Z

    .line 1309259
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    .prologue
    .line 1309246
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/8BS;->mVideoPath:Ljava/lang/String;

    .line 1309247
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Bh;

    iput-object v0, p0, LX/8BS;->mStartResponse:LX/8Bh;

    .line 1309248
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, LX/8BS;->mVideoTransferred:Z

    .line 1309249
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, LX/8BS;->mVideoPosted:Z

    .line 1309250
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 1309251
    iget-object v0, p0, LX/8BS;->mVideoPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 1309252
    iget-object v0, p0, LX/8BS;->mStartResponse:LX/8Bh;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 1309253
    iget-boolean v0, p0, LX/8BS;->mVideoTransferred:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    .line 1309254
    iget-boolean v0, p0, LX/8BS;->mVideoPosted:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    .line 1309255
    return-void
.end method
