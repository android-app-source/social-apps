.class public abstract LX/7G8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/7GB;

.field public final b:LX/7GD;

.field public final c:LX/2Sx;

.field public final d:LX/7G1;

.field public final e:LX/6Po;

.field private final f:LX/0SG;

.field private final g:LX/7Ge;

.field private final h:Ljava/util/concurrent/ScheduledExecutorService;

.field public final i:Ljava/lang/String;

.field private final j:LX/7Fx;

.field private k:Ljava/util/concurrent/Future;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7GB;LX/7GD;LX/2Sx;LX/7G1;LX/6Po;LX/0SG;LX/7Ge;Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/String;LX/7Fx;)V
    .locals 0
    .param p8    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param

    .prologue
    .line 1188827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1188828
    iput-object p1, p0, LX/7G8;->a:LX/7GB;

    .line 1188829
    iput-object p2, p0, LX/7G8;->b:LX/7GD;

    .line 1188830
    iput-object p3, p0, LX/7G8;->c:LX/2Sx;

    .line 1188831
    iput-object p4, p0, LX/7G8;->d:LX/7G1;

    .line 1188832
    iput-object p5, p0, LX/7G8;->e:LX/6Po;

    .line 1188833
    iput-object p6, p0, LX/7G8;->f:LX/0SG;

    .line 1188834
    iput-object p7, p0, LX/7G8;->g:LX/7Ge;

    .line 1188835
    iput-object p8, p0, LX/7G8;->h:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1188836
    iput-object p9, p0, LX/7G8;->i:Ljava/lang/String;

    .line 1188837
    iput-object p10, p0, LX/7G8;->j:LX/7Fx;

    .line 1188838
    return-void
.end method

.method private a(JILX/7Fx;LX/7G7;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "LX/7Fx",
            "<**>;",
            "LX/7G7;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1188780
    iget-object v0, p0, LX/7G8;->k:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7G8;->k:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1188781
    :goto_0
    return-void

    .line 1188782
    :cond_0
    iget-object v6, p0, LX/7G8;->h:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Lcom/facebook/sync/connection/SyncConnectionHandler$1;

    move-object v1, p0

    move v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/facebook/sync/connection/SyncConnectionHandler$1;-><init>(LX/7G8;ILX/7Fx;LX/7G7;Lcom/facebook/common/callercontext/CallerContext;)V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v6, v0, p1, p2, v1}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/7G8;->k:Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public static b(LX/7G8;ILjava/lang/String;J)Z
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1188783
    new-instance v1, LX/7G6;

    move-object v2, p0

    move-object v3, p2

    move-wide v4, p3

    move v6, p1

    invoke-direct/range {v1 .. v6}, LX/7G6;-><init>(LX/7G8;Ljava/lang/String;JI)V

    .line 1188784
    iget-object v3, p0, LX/7G8;->g:LX/7Ge;

    const-wide/32 v4, 0xea60

    const-wide/16 v6, 0x3e8

    move-object v8, v1

    invoke-virtual/range {v3 .. v8}, LX/7Ge;->a(JJLX/7G5;)LX/7Gd;

    move-result-object v0

    invoke-virtual {v0}, LX/7Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1188785
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/7GT;
    .locals 1

    .prologue
    .line 1188786
    iget-object v0, p0, LX/7G8;->b:LX/7GD;

    invoke-virtual {v0}, LX/7GD;->a()LX/7GT;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILX/7Fx;LX/7G7;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/7Fx",
            "<**>;",
            "LX/7G7;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    .line 1188787
    sget-object v0, LX/7G7;->REFRESH_CONNECTION:LX/7G7;

    if-ne p3, v0, :cond_0

    .line 1188788
    iget-object v0, p0, LX/7G8;->b:LX/7GD;

    .line 1188789
    iget-object v1, v0, LX/7GD;->h:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 1188790
    invoke-virtual {v0}, LX/7GD;->c()V

    .line 1188791
    :cond_0
    iget-object v0, p0, LX/7G8;->a:LX/7GB;

    .line 1188792
    invoke-interface {p2}, LX/7Fx;->b()Z

    move-result v1

    .line 1188793
    if-eqz v1, :cond_a

    invoke-virtual {v0, p2}, LX/7GB;->b(LX/7Fx;)Z

    move-result v1

    if-nez v1, :cond_a

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1188794
    if-eqz v0, :cond_1

    .line 1188795
    invoke-interface {p2}, LX/7Fx;->c()Lcom/facebook/sync/analytics/FullRefreshReason;

    move-result-object v0

    invoke-virtual {p0, v0, p4}, LX/7G8;->a(Lcom/facebook/sync/analytics/FullRefreshReason;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 1188796
    :goto_1
    return-object v0

    .line 1188797
    :cond_1
    invoke-interface {p2}, LX/7Fx;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1188798
    iget-object v0, p0, LX/7G8;->a:LX/7GB;

    invoke-virtual {v0, p2}, LX/7GB;->c(LX/7Fx;)J

    move-result-wide v2

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, LX/7G8;->a(JILX/7Fx;LX/7G7;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1188799
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    const-string v1, "ensureSync full refresh delayed because it was already performed recently."

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1

    .line 1188800
    :cond_2
    invoke-interface {p2}, LX/7Fx;->e()Ljava/lang/String;

    move-result-object v1

    .line 1188801
    invoke-interface {p2}, LX/7Fx;->a()J

    move-result-wide v2

    .line 1188802
    if-eqz v1, :cond_3

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    .line 1188803
    :goto_2
    iget-object v4, p0, LX/7G8;->i:Ljava/lang/String;

    invoke-virtual {p0}, LX/7G8;->a()LX/7GT;

    move-result-object v5

    invoke-static {v4, v5}, LX/7G9;->a(Ljava/lang/String;LX/7GT;)LX/7G9;

    move-result-object v4

    .line 1188804
    if-nez v0, :cond_5

    sget-object v0, LX/7G7;->ENSURE:LX/7G7;

    if-ne p3, v0, :cond_5

    iget-object v0, p0, LX/7G8;->c:LX/2Sx;

    invoke-virtual {v0, v4}, LX/2Sx;->a(LX/7G9;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1188805
    invoke-virtual {p0}, LX/7G8;->a()LX/7GT;

    .line 1188806
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1188807
    goto :goto_1

    .line 1188808
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 1188809
    :cond_5
    iget-object v0, p0, LX/7G8;->c:LX/2Sx;

    invoke-virtual {v0, v4}, LX/2Sx;->b(LX/7G9;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1188810
    const-wide/32 v2, 0xea60

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, LX/7G8;->a(JILX/7Fx;LX/7G7;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1188811
    const-string v0, "Queue %s for %s is temporarily not available. Backing off."

    invoke-virtual {p0}, LX/7G8;->a()LX/7GT;

    move-result-object v1

    iget-object v1, v1, LX/7GT;->apiString:Ljava/lang/String;

    iget-object v2, p0, LX/7G8;->i:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1188812
    sget-object v1, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1

    .line 1188813
    :cond_6
    if-nez v1, :cond_7

    .line 1188814
    sget-object v0, Lcom/facebook/sync/analytics/FullRefreshReason;->a:Lcom/facebook/sync/analytics/FullRefreshReason;

    invoke-virtual {p0, v0, p4}, LX/7G8;->a(Lcom/facebook/sync/analytics/FullRefreshReason;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1

    .line 1188815
    :cond_7
    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-gez v0, :cond_8

    .line 1188816
    sget-object v0, Lcom/facebook/sync/analytics/FullRefreshReason;->b:Lcom/facebook/sync/analytics/FullRefreshReason;

    invoke-virtual {p0, v0, p4}, LX/7G8;->a(Lcom/facebook/sync/analytics/FullRefreshReason;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_1

    .line 1188817
    :cond_8
    iget-object v0, p0, LX/7G8;->c:LX/2Sx;

    invoke-virtual {v0}, LX/2Sx;->a()Z

    move-result v0

    if-nez v0, :cond_b

    .line 1188818
    invoke-virtual {p0}, LX/7G8;->a()LX/7GT;

    .line 1188819
    const/4 v0, 0x0

    .line 1188820
    :goto_3
    move v0, v0

    .line 1188821
    if-eqz v0, :cond_9

    .line 1188822
    iget-object v0, p0, LX/7G8;->c:LX/2Sx;

    iget-object v1, p0, LX/7G8;->f:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v4, v2, v3}, LX/2Sx;->a(LX/7G9;J)V

    .line 1188823
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1188824
    goto/16 :goto_1

    .line 1188825
    :cond_9
    const-string v0, "resumeQueueConnectionIfMqttConnected failed for %s queue. Not connected to sync.  viewerContextUserId = %s"

    invoke-virtual {p0}, LX/7G8;->a()LX/7GT;

    move-result-object v1

    iget-object v1, v1, LX/7GT;->apiString:Ljava/lang/String;

    iget-object v2, p0, LX/7G8;->i:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1188826
    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    invoke-static {v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_1

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_b
    invoke-static {p0, p1, v1, v2, v3}, LX/7G8;->b(LX/7G8;ILjava/lang/String;J)Z

    move-result v0

    goto :goto_3
.end method

.method public abstract a(Lcom/facebook/sync/analytics/FullRefreshReason;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method
