.class public LX/70V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/70U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/70U",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1162340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1162341
    return-void
.end method


# virtual methods
.method public final a()LX/6zQ;
    .locals 1

    .prologue
    .line 1162342
    sget-object v0, LX/6zQ;->NEW_CREDIT_CARD:LX/6zQ;

    return-object v0
.end method

.method public final a(LX/0lF;)Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;
    .locals 3

    .prologue
    .line 1162343
    const-string v0, "type"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1162344
    const-string v0, "type"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1162345
    invoke-static {v0}, LX/6zQ;->forValue(Ljava/lang/String;)LX/6zQ;

    move-result-object v0

    sget-object v1, LX/6zQ;->NEW_CREDIT_CARD:LX/6zQ;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1162346
    const-string v0, "collect_zip"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1162347
    invoke-static {}, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->newBuilder()LX/6zN;

    move-result-object v1

    const-string v2, "provider"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 1162348
    iput-object v2, v1, LX/6zN;->a:Ljava/lang/String;

    .line 1162349
    move-object v1, v1

    .line 1162350
    invoke-static {v0}, LX/6xe;->forValue(Ljava/lang/String;)LX/6xe;

    move-result-object v0

    .line 1162351
    iput-object v0, v1, LX/6zN;->c:LX/6xe;

    .line 1162352
    move-object v0, v1

    .line 1162353
    new-instance v2, LX/0cA;

    invoke-direct {v2}, LX/0cA;-><init>()V

    .line 1162354
    const-string v1, "available_card_types"

    invoke-static {p1, v1}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 1162355
    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->forValue(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v1

    .line 1162356
    invoke-virtual {v2, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_1

    .line 1162357
    :cond_0
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v2

    .line 1162358
    invoke-virtual {v2}, LX/0Rf;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1162359
    move-object v1, v2

    .line 1162360
    iput-object v1, v0, LX/6zN;->d:LX/0Rf;

    .line 1162361
    move-object v0, v0

    .line 1162362
    const-string v1, "available_card_categories"

    invoke-static {p1, v1}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v1

    .line 1162363
    new-instance v2, LX/0cA;

    invoke-direct {v2}, LX/0cA;-><init>()V

    .line 1162364
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 1162365
    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/6zJ;->forValue(Ljava/lang/String;)LX/6zJ;

    move-result-object v1

    .line 1162366
    invoke-virtual {v2, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_3

    .line 1162367
    :cond_1
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v2

    .line 1162368
    invoke-virtual {v2}, LX/0Rf;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_4
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1162369
    move-object v1, v2

    .line 1162370
    iput-object v1, v0, LX/6zN;->b:LX/0Rf;

    .line 1162371
    move-object v0, v0

    .line 1162372
    invoke-virtual {v0}, LX/6zN;->e()Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;

    move-result-object v0

    return-object v0

    .line 1162373
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1162374
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 1162375
    :cond_4
    const/4 v1, 0x0

    goto :goto_4
.end method
