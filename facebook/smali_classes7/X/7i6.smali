.class public final enum LX/7i6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7i6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7i6;

.field public static final enum HOME_SCREEN:LX/7i6;

.field public static final enum OFFERS:LX/7i6;

.field public static final enum ORGANIC_SHARE:LX/7i6;

.field public static final enum PAGE_CTA:LX/7i6;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1227032
    new-instance v0, LX/7i6;

    const-string v1, "ORGANIC_SHARE"

    const-string v2, "organic_share"

    invoke-direct {v0, v1, v3, v2}, LX/7i6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7i6;->ORGANIC_SHARE:LX/7i6;

    .line 1227033
    new-instance v0, LX/7i6;

    const-string v1, "OFFERS"

    const-string v2, "offers"

    invoke-direct {v0, v1, v4, v2}, LX/7i6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7i6;->OFFERS:LX/7i6;

    .line 1227034
    new-instance v0, LX/7i6;

    const-string v1, "PAGE_CTA"

    const-string v2, "page_cta"

    invoke-direct {v0, v1, v5, v2}, LX/7i6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7i6;->PAGE_CTA:LX/7i6;

    .line 1227035
    new-instance v0, LX/7i6;

    const-string v1, "HOME_SCREEN"

    const-string v2, "home_screen"

    invoke-direct {v0, v1, v6, v2}, LX/7i6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7i6;->HOME_SCREEN:LX/7i6;

    .line 1227036
    const/4 v0, 0x4

    new-array v0, v0, [LX/7i6;

    sget-object v1, LX/7i6;->ORGANIC_SHARE:LX/7i6;

    aput-object v1, v0, v3

    sget-object v1, LX/7i6;->OFFERS:LX/7i6;

    aput-object v1, v0, v4

    sget-object v1, LX/7i6;->PAGE_CTA:LX/7i6;

    aput-object v1, v0, v5

    sget-object v1, LX/7i6;->HOME_SCREEN:LX/7i6;

    aput-object v1, v0, v6

    sput-object v0, LX/7i6;->$VALUES:[LX/7i6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1227038
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1227039
    iput-object p3, p0, LX/7i6;->value:Ljava/lang/String;

    .line 1227040
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7i6;
    .locals 1

    .prologue
    .line 1227041
    const-class v0, LX/7i6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7i6;

    return-object v0
.end method

.method public static values()[LX/7i6;
    .locals 1

    .prologue
    .line 1227037
    sget-object v0, LX/7i6;->$VALUES:[LX/7i6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7i6;

    return-object v0
.end method
