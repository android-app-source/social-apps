.class public final enum LX/7gf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gf;

.field public static final enum NEWS_FEED:LX/7gf;

.field public static final enum RANKED_FRIENDS:LX/7gf;

.field public static final enum SEARCH_FRIENDS:LX/7gf;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1224726
    new-instance v0, LX/7gf;

    const-string v1, "NEWS_FEED"

    const-string v2, "new_feed"

    invoke-direct {v0, v1, v3, v2}, LX/7gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gf;->NEWS_FEED:LX/7gf;

    .line 1224727
    new-instance v0, LX/7gf;

    const-string v1, "RANKED_FRIENDS"

    const-string v2, "ranked_friends"

    invoke-direct {v0, v1, v4, v2}, LX/7gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gf;->RANKED_FRIENDS:LX/7gf;

    .line 1224728
    new-instance v0, LX/7gf;

    const-string v1, "SEARCH_FRIENDS"

    const-string v2, "search_friends"

    invoke-direct {v0, v1, v5, v2}, LX/7gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gf;->SEARCH_FRIENDS:LX/7gf;

    .line 1224729
    const/4 v0, 0x3

    new-array v0, v0, [LX/7gf;

    sget-object v1, LX/7gf;->NEWS_FEED:LX/7gf;

    aput-object v1, v0, v3

    sget-object v1, LX/7gf;->RANKED_FRIENDS:LX/7gf;

    aput-object v1, v0, v4

    sget-object v1, LX/7gf;->SEARCH_FRIENDS:LX/7gf;

    aput-object v1, v0, v5

    sput-object v0, LX/7gf;->$VALUES:[LX/7gf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1224730
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1224731
    iput-object p3, p0, LX/7gf;->mName:Ljava/lang/String;

    .line 1224732
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gf;
    .locals 1

    .prologue
    .line 1224733
    const-class v0, LX/7gf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gf;

    return-object v0
.end method

.method public static values()[LX/7gf;
    .locals 1

    .prologue
    .line 1224734
    sget-object v0, LX/7gf;->$VALUES:[LX/7gf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gf;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224735
    iget-object v0, p0, LX/7gf;->mName:Ljava/lang/String;

    return-object v0
.end method
