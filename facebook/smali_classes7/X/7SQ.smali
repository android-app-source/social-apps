.class public final LX/7SQ;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;


# direct methods
.method public constructor <init>(Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;)V
    .locals 0

    .prologue
    .line 1208786
    iput-object p1, p0, LX/7SQ;->a:Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1208787
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1208788
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1208789
    iget-object v1, p0, LX/7SQ;->a:Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;

    iget-object v1, v1, Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;->a:LX/7SS;

    iget-object v1, v1, LX/7SS;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1208790
    :try_start_0
    iget-object v1, p0, LX/7SQ;->a:Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;

    iget-object v2, v1, Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;->a:LX/7SS;

    if-nez v0, :cond_1

    const/4 v1, 0x0

    .line 1208791
    :goto_0
    iput-object v1, v2, LX/7SS;->j:LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1208792
    iget-object v1, p0, LX/7SQ;->a:Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;

    iget-object v1, v1, Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;->a:LX/7SS;

    iget-object v1, v1, LX/7SS;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1208793
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1208794
    :cond_0
    return-void

    .line 1208795
    :cond_1
    :try_start_1
    invoke-virtual {v0}, LX/1FJ;->b()LX/1FJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 1208796
    :catchall_0
    move-exception v1

    iget-object v2, p0, LX/7SQ;->a:Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;

    iget-object v2, v2, Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;->a:LX/7SS;

    iget-object v2, v2, LX/7SS;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1208797
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    throw v1
.end method

.method public final f(LX/1ca;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1208798
    invoke-interface {p1}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object v0

    .line 1208799
    if-nez v0, :cond_0

    .line 1208800
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "DataSourceFailed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 1208801
    :cond_0
    const-class v1, LX/7SS;

    const-string v2, "Failed to retrieve texture image: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/7SQ;->a:Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;

    iget-object v5, v5, Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;->a:LX/7SS;

    iget-object v5, v5, LX/7SS;->c:LX/7SR;

    iget-object v5, v5, LX/7SR;->a:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1208802
    return-void
.end method
