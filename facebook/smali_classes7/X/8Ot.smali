.class public final LX/8Ot;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;)V
    .locals 0

    .prologue
    .line 1341188
    iput-object p1, p0, LX/8Ot;->a:Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 4
    .param p1    # Lcom/facebook/fbservice/service/OperationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1341189
    if-eqz p1, :cond_0

    .line 1341190
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableMap()Ljava/util/HashMap;

    move-result-object v0

    .line 1341191
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1341192
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/server/handler/ParcelableString;

    .line 1341193
    iget-object v1, v0, Lcom/facebook/platform/server/handler/ParcelableString;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1341194
    iget-object v1, p0, LX/8Ot;->a:Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;

    iget-object v1, v1, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->p:LX/17Y;

    iget-object v2, p0, LX/8Ot;->a:Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;

    iget-object v3, p0, LX/8Ot;->a:Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;

    invoke-static {v3, v0}, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->a(Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1341195
    iget-object v1, p0, LX/8Ot;->a:Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;

    iget-object v1, v1, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->r:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/8Ot;->a:Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1341196
    :cond_0
    iget-object v0, p0, LX/8Ot;->a:Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;

    invoke-virtual {v0}, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->finish()V

    .line 1341197
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1341198
    iget-object v0, p0, LX/8Ot;->a:Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;

    invoke-virtual {v0}, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->finish()V

    .line 1341199
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1341200
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/8Ot;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
