.class public LX/8LX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field private final c:LX/8GZ;

.field private final d:LX/0oz;

.field private final e:LX/03V;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/8GZ;LX/0oz;LX/03V;LX/0Or;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            ">;",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            "LX/8GZ;",
            "LX/0oz;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1333280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1333281
    iput-object p1, p0, LX/8LX;->a:LX/0Or;

    .line 1333282
    iput-object p2, p0, LX/8LX;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 1333283
    iput-object p3, p0, LX/8LX;->c:LX/8GZ;

    .line 1333284
    iput-object p4, p0, LX/8LX;->d:LX/0oz;

    .line 1333285
    iput-object p5, p0, LX/8LX;->e:LX/03V;

    .line 1333286
    iput-object p6, p0, LX/8LX;->f:LX/0Or;

    .line 1333287
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)LX/73w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;"
        }
    .end annotation

    .prologue
    .line 1333289
    iget-object v0, p0, LX/8LX;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/73w;

    .line 1333290
    invoke-virtual {v0, p1}, LX/73w;->a(Ljava/lang/String;)V

    .line 1333291
    invoke-virtual {v0, p2}, LX/73w;->a(Ljava/util/List;)V

    .line 1333292
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1333293
    iget-object v2, p0, LX/8LX;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 1333294
    invoke-static {v2}, Lcom/facebook/http/common/FbHttpRequestProcessor;->j(Lcom/facebook/http/common/FbHttpRequestProcessor;)LX/1hk;

    move-result-object p0

    invoke-interface {p0}, LX/1hk;->e()Ljava/lang/String;

    move-result-object p0

    move-object v2, p0

    .line 1333295
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1333296
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1333297
    :cond_0
    const/4 v2, 0x0

    iput-object v2, v0, LX/73w;->q:Ljava/lang/String;

    .line 1333298
    :goto_0
    return-object v0

    .line 1333299
    :cond_1
    const-string v2, ","

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v1, p0, p1

    invoke-static {v2, p0}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/73w;->q:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/8LX;
    .locals 1

    .prologue
    .line 1333288
    invoke-static {p0}, LX/8LX;->b(LX/0QB;)LX/8LX;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/8LX;Ljava/util/List;Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;I)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    .line 1333251
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1333252
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1333253
    iget-object v1, v0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v1, v1

    .line 1333254
    invoke-interface {v1}, Lcom/facebook/photos/base/tagging/TagTarget;->e()Landroid/graphics/PointF;

    move-result-object v1

    .line 1333255
    iget v5, v1, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v5, v7

    if-ltz v5, :cond_0

    iget v5, v1, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v8

    if-gtz v5, :cond_0

    iget v5, v1, Landroid/graphics/PointF;->y:F

    cmpg-float v5, v5, v7

    if-ltz v5, :cond_0

    iget v5, v1, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v8

    if-lez v5, :cond_5

    .line 1333256
    :cond_0
    if-eqz p2, :cond_1

    .line 1333257
    iget-object v0, p2, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1333258
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unexpected failure: invalid tags while uploading. \ntag position: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\nphoto orientation: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\n"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1333259
    if-eqz v0, :cond_7

    .line 1333260
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "crop area: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\nauto-enhance value: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\ntext count: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    :goto_3
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\nsticker count: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1333261
    :goto_5
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 1333262
    iget-object v1, p0, LX/8LX;->e:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto/16 :goto_0

    .line 1333263
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1333264
    :cond_2
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v7, v7, v8, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v1}, Landroid/graphics/RectF;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    .line 1333265
    :cond_5
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1333266
    :cond_6
    return-object v3

    :cond_7
    move-object v0, v1

    goto :goto_5
.end method

.method public static b(LX/0QB;)LX/8LX;
    .locals 7

    .prologue
    .line 1333272
    new-instance v0, LX/8LX;

    const/16 v1, 0x2e05

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v2

    check-cast v2, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {p0}, LX/8GZ;->a(LX/0QB;)LX/8GZ;

    move-result-object v3

    check-cast v3, LX/8GZ;

    invoke-static {p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v4

    check-cast v4, LX/0oz;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    const/16 v6, 0x15e7

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/8LX;-><init>(LX/0Or;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/8GZ;LX/0oz;LX/03V;LX/0Or;)V

    .line 1333273
    return-object v0
.end method

.method public static c(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/434;
    .locals 4

    .prologue
    const/16 v3, 0xb4

    const/4 v2, 0x1

    .line 1333274
    sget-object v0, LX/8LW;->a:[I

    .line 1333275
    iget-object v1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v1, v1

    .line 1333276
    invoke-virtual {v1}, LX/8LS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1333277
    new-instance v0, LX/434;

    invoke-direct {v0, v2, v2}, LX/434;-><init>(II)V

    :goto_0
    return-object v0

    .line 1333278
    :pswitch_0
    new-instance v0, LX/434;

    invoke-direct {v0, v3, v3}, LX/434;-><init>(II)V

    goto :goto_0

    .line 1333279
    :pswitch_1
    new-instance v0, LX/434;

    const/16 v1, 0x190

    const/16 v2, 0x96

    invoke-direct {v0, v1, v2}, LX/434;-><init>(II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;J)LX/73w;
    .locals 7

    .prologue
    .line 1333268
    invoke-virtual {p0, p1}, LX/8LX;->d(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/73w;

    move-result-object v0

    .line 1333269
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ak()J

    move-result-wide v2

    sub-long v2, p2, v2

    .line 1333270
    iget-object v4, v0, LX/73w;->g:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    sub-long/2addr v4, v2

    iput-wide v4, v0, LX/73w;->r:J

    .line 1333271
    return-object v0
.end method

.method public final a(Ljava/lang/String;)LX/73w;
    .locals 1

    .prologue
    .line 1333267
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, v0}, LX/8LX;->a(Ljava/lang/String;Ljava/util/List;)LX/73w;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/util/List;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1333190
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v19

    .line 1333191
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->z()LX/0Px;

    move-result-object v20

    .line 1333192
    invoke-static/range {p1 .. p1}, LX/8LX;->c(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/434;

    move-result-object v21

    .line 1333193
    const/4 v5, 0x0

    .line 1333194
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->y()LX/0Px;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, LX/0Px;->size()I

    move-result v23

    const/4 v4, 0x0

    move/from16 v17, v4

    move/from16 v18, v5

    :goto_0
    move/from16 v0, v17

    move/from16 v1, v23

    if-ge v0, v1, :cond_8

    move-object/from16 v0, v22

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/ipc/media/MediaItem;

    .line 1333195
    sget-object v5, LX/4gF;->VIDEO:LX/4gF;

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/4gF;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1333196
    add-int/lit8 v4, v18, 0x1

    .line 1333197
    :goto_1
    add-int/lit8 v5, v17, 0x1

    move/from16 v17, v5

    move/from16 v18, v4

    goto :goto_0

    .line 1333198
    :cond_0
    const/16 v16, 0x0

    .line 1333199
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v24

    .line 1333200
    const/4 v15, 0x0

    .line 1333201
    const/4 v9, 0x0

    .line 1333202
    const/4 v11, 0x0

    .line 1333203
    const/4 v10, 0x0

    .line 1333204
    const/high16 v8, 0x3f000000    # 0.5f

    .line 1333205
    const/high16 v6, 0x3f000000    # 0.5f

    .line 1333206
    const/4 v7, 0x0

    .line 1333207
    const/4 v5, 0x0

    .line 1333208
    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v12

    sget-object v13, LX/4gF;->PHOTO:LX/4gF;

    if-ne v12, v13, :cond_d

    move-object v5, v4

    .line 1333209
    check-cast v5, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1333210
    invoke-virtual {v5}, Lcom/facebook/photos/base/media/PhotoItem;->t()Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-result-object v14

    .line 1333211
    invoke-virtual {v5}, Lcom/facebook/photos/base/media/PhotoItem;->v()Z

    move-result v13

    .line 1333212
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ax()Lcom/facebook/photos/tagging/store/TagStoreCopy;

    move-result-object v10

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/facebook/photos/tagging/store/TagStoreCopy;->b(Lcom/facebook/ipc/media/MediaIdKey;)LX/0Px;

    move-result-object v11

    .line 1333213
    invoke-virtual {v5}, Lcom/facebook/photos/base/media/PhotoItem;->x()Ljava/lang/String;

    move-result-object v12

    .line 1333214
    invoke-virtual {v5}, Lcom/facebook/photos/base/media/PhotoItem;->r()LX/74w;

    move-result-object v5

    .line 1333215
    instance-of v10, v5, Lcom/facebook/photos/base/photos/LocalPhoto;

    if-eqz v10, :cond_1

    .line 1333216
    check-cast v5, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1333217
    invoke-virtual {v5}, Lcom/facebook/photos/base/photos/LocalPhoto;->c()I

    move-result v16

    .line 1333218
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ay()Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;

    move-result-object v10

    .line 1333219
    invoke-virtual {v5}, LX/74x;->a()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v5

    .line 1333220
    invoke-virtual {v10, v5}, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;->a(Lcom/facebook/ipc/media/MediaIdKey;)Z

    move-result v25

    if-eqz v25, :cond_1

    .line 1333221
    invoke-virtual {v10, v5}, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;->b(Lcom/facebook/ipc/media/MediaIdKey;)LX/0Px;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, LX/0Px;->size()I

    move-result v26

    const/4 v5, 0x0

    move v10, v5

    :goto_2
    move/from16 v0, v26

    if-ge v10, v0, :cond_1

    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1333222
    invoke-virtual {v5}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1333223
    add-int/lit8 v5, v10, 0x1

    move v10, v5

    goto :goto_2

    .line 1333224
    :cond_1
    if-eqz v20, :cond_c

    .line 1333225
    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/Bundle;

    .line 1333226
    const-class v6, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v6}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1333227
    const-string v6, "creative_editing_metadata"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1333228
    const-string v8, "caption"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1333229
    const-string v8, "focusX"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v9

    .line 1333230
    const-string v8, "focusY"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v8

    .line 1333231
    if-eqz v6, :cond_b

    .line 1333232
    new-instance v5, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    move-object/from16 v0, v24

    invoke-direct {v5, v6, v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;-><init>(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Ljava/util/List;)V

    move v6, v8

    move v7, v9

    move-object v8, v10

    .line 1333233
    :goto_3
    new-instance v10, Landroid/graphics/RectF;

    const/4 v9, 0x0

    const/16 v24, 0x0

    const/high16 v25, 0x3f800000    # 1.0f

    const/high16 v26, 0x3f800000    # 1.0f

    move/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v10, v9, v0, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1333234
    const/4 v9, 0x0

    .line 1333235
    if-eqz v11, :cond_9

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_9

    .line 1333236
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v24

    if-eqz v24, :cond_2

    .line 1333237
    invoke-virtual {v5}, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v9

    invoke-static {v9}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v10

    .line 1333238
    const/4 v9, 0x1

    .line 1333239
    :cond_2
    if-nez v9, :cond_3

    if-eqz v16, :cond_a

    .line 1333240
    :cond_3
    move-object/from16 v0, p0

    iget-object v9, v0, LX/8LX;->c:LX/8GZ;

    move/from16 v0, v16

    invoke-virtual {v9, v10, v0}, LX/8GZ;->a(Landroid/graphics/RectF;I)V

    .line 1333241
    move-object/from16 v0, p0

    iget-object v9, v0, LX/8LX;->c:LX/8GZ;

    invoke-virtual {v9, v11}, LX/8GZ;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    .line 1333242
    :goto_4
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v9, v5, v1}, LX/8LX;->a(LX/8LX;Ljava/util/List;Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;I)Ljava/util/List;

    move-result-object v9

    .line 1333243
    if-eqz v9, :cond_9

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_9

    .line 1333244
    invoke-static {v9}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v9

    move-object v10, v14

    move-object v11, v8

    move v8, v7

    move-object v7, v5

    move-object v5, v12

    move-object v12, v9

    move v9, v13

    move/from16 v13, v16

    .line 1333245
    :goto_5
    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v14

    .line 1333246
    new-instance v15, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->o()J

    move-result-wide v24

    move-wide/from16 v0, v24

    invoke-direct {v15, v14, v0, v1}, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;-><init>(Ljava/lang/String;J)V

    .line 1333247
    new-instance v16, LX/8NJ;

    move-object/from16 v0, v16

    invoke-direct {v0, v15}, LX/8NJ;-><init>(Lcom/facebook/photos/upload/protocol/UploadPhotoSource;)V

    if-nez v11, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->B()Ljava/lang/String;

    move-result-object v14

    :goto_6
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, LX/8NJ;->b(Ljava/lang/String;)LX/8NJ;

    move-result-object v14

    invoke-virtual {v14, v8}, LX/8NJ;->a(F)LX/8NJ;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/8NJ;->b(F)LX/8NJ;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/8NJ;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;)LX/8NJ;

    move-result-object v7

    if-eqz v11, :cond_5

    const/4 v6, 0x1

    :goto_7
    invoke-virtual {v7, v6}, LX/8NJ;->c(Z)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->E()J

    move-result-wide v14

    invoke-virtual {v6, v14, v15}, LX/8NJ;->b(J)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->F()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->a(Ljava/lang/String;)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->H()Z

    move-result v7

    invoke-virtual {v6, v7}, LX/8NJ;->a(Z)LX/8NJ;

    move-result-object v6

    invoke-virtual {v6, v9}, LX/8NJ;->b(Z)LX/8NJ;

    move-result-object v6

    invoke-virtual {v6, v10}, LX/8NJ;->a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->C()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, LX/8NJ;->a(J)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->M()LX/0Px;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->a(LX/0Px;)LX/8NJ;

    move-result-object v6

    invoke-virtual {v6, v12}, LX/8NJ;->b(LX/0Px;)LX/8NJ;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->y()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    const/4 v8, 0x1

    if-ne v6, v8, :cond_6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ah()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v6

    :goto_8
    invoke-virtual {v7, v6}, LX/8NJ;->a(Lcom/facebook/ipc/composer/model/MinutiaeTag;)LX/8NJ;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->y()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    const/4 v8, 0x1

    if-ne v6, v8, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ai()Ljava/lang/String;

    move-result-object v6

    :goto_9
    invoke-virtual {v7, v6}, LX/8NJ;->c(Ljava/lang/String;)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->D()Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->d(Ljava/lang/String;)LX/8NJ;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x5f

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->e(Ljava/lang/String;)LX/8NJ;

    move-result-object v6

    invoke-virtual {v6, v13}, LX/8NJ;->a(I)LX/8NJ;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, LX/8NJ;->a(LX/434;)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->V()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aj()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, LX/8NJ;->c(J)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->X()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->a(Lcom/facebook/share/model/ComposerAppAttribution;)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->Y()Z

    move-result v7

    invoke-virtual {v6, v7}, LX/8NJ;->d(Z)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->Z()Z

    move-result v7

    invoke-virtual {v6, v7}, LX/8NJ;->e(Z)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aE()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->f(Ljava/lang/String;)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->an()LX/5Rn;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->a(LX/5Rn;)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ao()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, LX/8NJ;->d(J)LX/8NJ;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, LX/8LX;->d:LX/0oz;

    invoke-virtual {v7}, LX/0oz;->c()LX/0p3;

    move-result-object v7

    invoke-virtual {v7}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->g(Ljava/lang/String;)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aA()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->h(Ljava/lang/String;)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aB()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->i(Ljava/lang/String;)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aC()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, LX/8NJ;->e(J)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aD()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->j(Ljava/lang/String;)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aF()Z

    move-result v7

    invoke-virtual {v6, v7}, LX/8NJ;->f(Z)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aK()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;)LX/8NJ;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aL()LX/0Px;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/8NJ;->c(LX/0Px;)LX/8NJ;

    move-result-object v6

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/8NJ;->k(Ljava/lang/String;)LX/8NJ;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aM()Z

    move-result v6

    invoke-virtual {v4, v6}, LX/8NJ;->g(Z)LX/8NJ;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aN()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/8NJ;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)LX/8NJ;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->G()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/8NJ;->l(Ljava/lang/String;)LX/8NJ;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/8NJ;->m(Ljava/lang/String;)LX/8NJ;

    move-result-object v4

    invoke-virtual {v4}, LX/8NJ;->a()Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1333248
    add-int/lit8 v4, v18, 0x1

    goto/16 :goto_1

    :cond_4
    move-object v14, v11

    .line 1333249
    goto/16 :goto_6

    :cond_5
    const/4 v6, 0x0

    goto/16 :goto_7

    :cond_6
    sget-object v6, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    goto/16 :goto_8

    :cond_7
    const/4 v6, 0x0

    goto/16 :goto_9

    .line 1333250
    :cond_8
    return-object v19

    :cond_9
    move v9, v13

    move-object v10, v14

    move-object v11, v8

    move v8, v7

    move/from16 v13, v16

    move-object v7, v5

    move-object v5, v12

    move-object v12, v15

    goto/16 :goto_5

    :cond_a
    move-object v9, v11

    goto/16 :goto_4

    :cond_b
    move-object v5, v7

    move v6, v8

    move v7, v9

    move-object v8, v10

    goto/16 :goto_3

    :cond_c
    move-object v5, v7

    move v7, v8

    move-object v8, v9

    goto/16 :goto_3

    :cond_d
    move-object v12, v15

    move/from16 v13, v16

    move-object/from16 v27, v9

    move v9, v10

    move-object v10, v11

    move-object/from16 v11, v27

    goto/16 :goto_5
.end method

.method public final b(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/5M9;
    .locals 10

    .prologue
    .line 1333099
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-object v0, v0

    .line 1333100
    iget-object v3, v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->e:Ljava/lang/String;

    .line 1333101
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->c:LX/0Px;

    move-object v0, v0

    .line 1333102
    if-eqz v0, :cond_1

    .line 1333103
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->c:LX/0Px;

    move-object v0, v0

    .line 1333104
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 1333105
    :goto_0
    iget-wide v8, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->j:J

    move-wide v4, v8

    .line 1333106
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_2

    .line 1333107
    iget-wide v8, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->j:J

    move-wide v4, v8

    .line 1333108
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1333109
    :goto_1
    new-instance v4, LX/5M9;

    invoke-direct {v4}, LX/5M9;-><init>()V

    iget-object v0, p0, LX/8LX;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 1333110
    iput-wide v6, v4, LX/5M9;->k:J

    .line 1333111
    move-object v0, v4

    .line 1333112
    iget-wide v8, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v4, v8

    .line 1333113
    iput-wide v4, v0, LX/5M9;->b:J

    .line 1333114
    move-object v0, v0

    .line 1333115
    iget-object v4, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v4, v4

    .line 1333116
    iput-object v4, v0, LX/5M9;->G:Ljava/lang/String;

    .line 1333117
    move-object v0, v0

    .line 1333118
    iput-object v3, v0, LX/5M9;->h:Ljava/lang/String;

    .line 1333119
    move-object v0, v0

    .line 1333120
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->aq:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-object v3, v3

    .line 1333121
    iput-object v3, v0, LX/5M9;->i:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 1333122
    move-object v0, v0

    .line 1333123
    invoke-virtual {v0, v1}, LX/5M9;->d(LX/0Px;)LX/5M9;

    move-result-object v0

    .line 1333124
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->e:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-object v1, v1

    .line 1333125
    iput-object v1, v0, LX/5M9;->H:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1333126
    move-object v0, v0

    .line 1333127
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1333128
    iput-object v1, v0, LX/5M9;->J:Ljava/lang/String;

    .line 1333129
    move-object v0, v0

    .line 1333130
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1333131
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v3, v3

    .line 1333132
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "_status"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1333133
    iput-object v1, v0, LX/5M9;->s:Ljava/lang/String;

    .line 1333134
    move-object v0, v0

    .line 1333135
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1333136
    iput-object v1, v0, LX/5M9;->c:Ljava/lang/String;

    .line 1333137
    move-object v0, v0

    .line 1333138
    iput-object v2, v0, LX/5M9;->d:Ljava/lang/String;

    .line 1333139
    move-object v0, v0

    .line 1333140
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->k:Ljava/lang/String;

    move-object v1, v1

    .line 1333141
    iput-object v1, v0, LX/5M9;->T:Ljava/lang/String;

    .line 1333142
    move-object v0, v0

    .line 1333143
    iget-boolean v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->m:Z

    move v1, v1

    .line 1333144
    iput-boolean v1, v0, LX/5M9;->S:Z

    .line 1333145
    move-object v0, v0

    .line 1333146
    iget-wide v8, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->y:J

    move-wide v2, v8

    .line 1333147
    iput-wide v2, v0, LX/5M9;->u:J

    .line 1333148
    move-object v0, v0

    .line 1333149
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->z:LX/5Rn;

    move-object v1, v1

    .line 1333150
    iput-object v1, v0, LX/5M9;->p:LX/5Rn;

    .line 1333151
    move-object v0, v0

    .line 1333152
    iget-wide v8, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->A:J

    move-wide v2, v8

    .line 1333153
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/5M9;->a(Ljava/lang/Long;)LX/5M9;

    move-result-object v0

    .line 1333154
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-object v1, v1

    .line 1333155
    iput-object v1, v0, LX/5M9;->M:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1333156
    move-object v0, v0

    .line 1333157
    iget-wide v8, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->L:J

    move-wide v2, v8

    .line 1333158
    iput-wide v2, v0, LX/5M9;->N:J

    .line 1333159
    move-object v0, v0

    .line 1333160
    iget-boolean v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->D:Z

    move v1, v1

    .line 1333161
    iput-boolean v1, v0, LX/5M9;->t:Z

    .line 1333162
    move-object v0, v0

    .line 1333163
    iget-object v1, p0, LX/8LX;->d:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->c()LX/0p3;

    move-result-object v1

    invoke-virtual {v1}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v1

    .line 1333164
    iput-object v1, v0, LX/5M9;->R:Ljava/lang/String;

    .line 1333165
    move-object v0, v0

    .line 1333166
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->an:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-object v1, v1

    .line 1333167
    iput-object v1, v0, LX/5M9;->ag:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1333168
    move-object v0, v0

    .line 1333169
    iget-boolean v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ap:Z

    move v1, v1

    .line 1333170
    iput-boolean v1, v0, LX/5M9;->ah:Z

    .line 1333171
    move-object v0, v0

    .line 1333172
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->l:Ljava/lang/String;

    move-object v1, v1

    .line 1333173
    iput-object v1, v0, LX/5M9;->g:Ljava/lang/String;

    .line 1333174
    move-object v0, v0

    .line 1333175
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->B:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object v1, v1

    .line 1333176
    if-eqz v1, :cond_0

    .line 1333177
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->B:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object v1, v1

    .line 1333178
    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v1

    .line 1333179
    iput-object v1, v0, LX/5M9;->x:Ljava/lang/String;

    .line 1333180
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->B:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object v1, v1

    .line 1333181
    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v1

    .line 1333182
    iput-object v1, v0, LX/5M9;->y:Ljava/lang/String;

    .line 1333183
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->B:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object v1, v1

    .line 1333184
    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v1

    .line 1333185
    iput-object v1, v0, LX/5M9;->z:Ljava/lang/String;

    .line 1333186
    :cond_0
    return-object v0

    .line 1333187
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1333188
    move-object v1, v0

    goto/16 :goto_0

    .line 1333189
    :cond_2
    const/4 v0, 0x0

    move-object v2, v0

    goto/16 :goto_1
.end method

.method public final d(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/73w;
    .locals 2

    .prologue
    .line 1333096
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 1333097
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->M:Ljava/util/List;

    move-object v1, v1

    .line 1333098
    invoke-direct {p0, v0, v1}, LX/8LX;->a(Ljava/lang/String;Ljava/util/List;)LX/73w;

    move-result-object v0

    return-object v0
.end method
