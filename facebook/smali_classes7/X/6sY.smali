.class public LX/6sY;
.super LX/6sV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sV",
        "<",
        "Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:LX/6rs;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/6rs;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1153512
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    invoke-direct {p0, p1, v0}, LX/6sV;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 1153513
    iput-object p2, p0, LX/6sY;->c:LX/6rs;

    .line 1153514
    return-void
.end method

.method private a(LX/1pN;)Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;
    .locals 3

    .prologue
    .line 1153515
    iget-object v0, p0, LX/6sY;->c:LX/6rs;

    const-string v1, "1.1"

    invoke-virtual {v0, v1}, LX/6rs;->d(Ljava/lang/String;)LX/6rr;

    move-result-object v0

    const-string v1, "1.1"

    invoke-virtual {p1}, LX/1pN;->d()LX/0lF;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    return-object v0
.end method

.method public static b(LX/0QB;)LX/6sY;
    .locals 3

    .prologue
    .line 1153516
    new-instance v2, LX/6sY;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-static {p0}, LX/6rs;->b(LX/0QB;)LX/6rs;

    move-result-object v1

    check-cast v1, LX/6rs;

    invoke-direct {v2, v0, v1}, LX/6sY;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/6rs;)V

    .line 1153517
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1153518
    check-cast p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;

    .line 1153519
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1153520
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153521
    invoke-static {v0, p1}, LX/6sX;->a(Ljava/util/ArrayList;Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;)V

    .line 1153522
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-class v2, LX/6sY;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 1153523
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1153524
    move-object v1, v1

    .line 1153525
    const-string v2, "payments/update_checkout"

    .line 1153526
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1153527
    move-object v1, v1

    .line 1153528
    const-string v2, "POST"

    .line 1153529
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1153530
    move-object v1, v1

    .line 1153531
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1153532
    move-object v0, v1

    .line 1153533
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1153534
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1153535
    move-object v0, v0

    .line 1153536
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/os/Parcelable;LX/1pN;)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 1153537
    invoke-direct {p0, p2}, LX/6sY;->a(LX/1pN;)Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1153538
    invoke-direct {p0, p2}, LX/6sY;->a(LX/1pN;)Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1153539
    const-string v0, "checkout_update"

    return-object v0
.end method
