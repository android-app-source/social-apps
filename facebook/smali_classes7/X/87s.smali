.class public final LX/87s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/87u;

.field public final synthetic b:LX/87v;


# direct methods
.method public constructor <init>(LX/87v;LX/87u;)V
    .locals 0

    .prologue
    .line 1300725
    iput-object p1, p0, LX/87s;->b:LX/87v;

    iput-object p2, p0, LX/87s;->a:LX/87u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1300726
    iget-object v0, p0, LX/87s;->a:LX/87u;

    if-eqz v0, :cond_0

    .line 1300727
    iget-object v0, p0, LX/87s;->b:LX/87v;

    iget-object v0, v0, LX/87v;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/goodfriends/data/GoodFriendsMetadataFetcher$1$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/goodfriends/data/GoodFriendsMetadataFetcher$1$2;-><init>(LX/87s;Ljava/lang/Throwable;)V

    const v2, -0x3662a704    # -1288991.5f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1300728
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1300729
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1300730
    if-nez p1, :cond_1

    .line 1300731
    :cond_0
    :goto_0
    return-void

    .line 1300732
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1300733
    check-cast v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;

    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;->a()Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel$NodesModel;

    .line 1300734
    iget-object v1, p0, LX/87s;->a:LX/87u;

    if-eqz v1, :cond_0

    .line 1300735
    iget-object v1, p0, LX/87s;->b:LX/87v;

    iget-object v1, v1, LX/87v;->b:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/facebook/goodfriends/data/GoodFriendsMetadataFetcher$1$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/goodfriends/data/GoodFriendsMetadataFetcher$1$1;-><init>(LX/87s;Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel$NodesModel;)V

    const v0, 0x4d45892c    # 2.07131328E8f

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
