.class public final LX/77z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)V
    .locals 0

    .prologue
    .line 1172319
    iput-object p1, p0, LX/77z;->a:Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 2

    .prologue
    .line 1172320
    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->FIG_DIALOG:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    iget-object v1, p0, LX/77z;->a:Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    iget-object v1, v1, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->template:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-virtual {v0, v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1172321
    iget-object v0, p0, LX/77z;->a:Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    .line 1172322
    iget-object v1, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 1172323
    if-eqz v1, :cond_2

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    .line 1172324
    :goto_0
    if-eqz v1, :cond_4

    .line 1172325
    iget-object v1, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->templateParameters:LX/0P1;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->templateParameters:LX/0P1;

    const-string p0, "fig_button_layout"

    invoke-virtual {v1, p0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1172326
    iget-object v1, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->templateParameters:LX/0P1;

    const-string p0, "fig_button_layout"

    invoke-virtual {v1, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1172327
    const-string p0, "AUTOMATIC"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {v0}, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1172328
    invoke-static {v0}, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->w(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)V

    .line 1172329
    :cond_0
    const-string p0, "VERTICAL_STACK"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1172330
    invoke-static {v0}, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->w(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)V

    .line 1172331
    :cond_1
    :goto_1
    return-void

    .line 1172332
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 1172333
    :cond_3
    invoke-static {v0}, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1172334
    invoke-static {v0}, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->w(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)V

    goto :goto_1

    .line 1172335
    :cond_4
    iget-object v1, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 1172336
    const/4 p0, -0x1

    iput p0, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1172337
    iget-object p0, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method
