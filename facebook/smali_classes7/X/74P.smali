.class public final enum LX/74P;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/74P;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/74P;

.field public static final enum CLICK:LX/74P;

.field public static final enum HARDWARE_CLICK:LX/74P;

.field public static final enum LOAD:LX/74P;

.field public static final enum LONGPRESS:LX/74P;

.field public static final enum SCROLL:LX/74P;

.field public static final enum SWIPE:LX/74P;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1167868
    new-instance v0, LX/74P;

    const-string v1, "SWIPE"

    const-string v2, "swipe"

    invoke-direct {v0, v1, v4, v2}, LX/74P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74P;->SWIPE:LX/74P;

    .line 1167869
    new-instance v0, LX/74P;

    const-string v1, "SCROLL"

    const-string v2, "scroll"

    invoke-direct {v0, v1, v5, v2}, LX/74P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74P;->SCROLL:LX/74P;

    .line 1167870
    new-instance v0, LX/74P;

    const-string v1, "LOAD"

    const-string v2, "load"

    invoke-direct {v0, v1, v6, v2}, LX/74P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74P;->LOAD:LX/74P;

    .line 1167871
    new-instance v0, LX/74P;

    const-string v1, "CLICK"

    const-string v2, "click"

    invoke-direct {v0, v1, v7, v2}, LX/74P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74P;->CLICK:LX/74P;

    .line 1167872
    new-instance v0, LX/74P;

    const-string v1, "LONGPRESS"

    const-string v2, "longpress"

    invoke-direct {v0, v1, v8, v2}, LX/74P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74P;->LONGPRESS:LX/74P;

    .line 1167873
    new-instance v0, LX/74P;

    const-string v1, "HARDWARE_CLICK"

    const/4 v2, 0x5

    const-string v3, "hardware_click"

    invoke-direct {v0, v1, v2, v3}, LX/74P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74P;->HARDWARE_CLICK:LX/74P;

    .line 1167874
    const/4 v0, 0x6

    new-array v0, v0, [LX/74P;

    sget-object v1, LX/74P;->SWIPE:LX/74P;

    aput-object v1, v0, v4

    sget-object v1, LX/74P;->SCROLL:LX/74P;

    aput-object v1, v0, v5

    sget-object v1, LX/74P;->LOAD:LX/74P;

    aput-object v1, v0, v6

    sget-object v1, LX/74P;->CLICK:LX/74P;

    aput-object v1, v0, v7

    sget-object v1, LX/74P;->LONGPRESS:LX/74P;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/74P;->HARDWARE_CLICK:LX/74P;

    aput-object v2, v0, v1

    sput-object v0, LX/74P;->$VALUES:[LX/74P;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1167865
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1167866
    iput-object p3, p0, LX/74P;->value:Ljava/lang/String;

    .line 1167867
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/74P;
    .locals 1

    .prologue
    .line 1167863
    const-class v0, LX/74P;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/74P;

    return-object v0
.end method

.method public static values()[LX/74P;
    .locals 1

    .prologue
    .line 1167864
    sget-object v0, LX/74P;->$VALUES:[LX/74P;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/74P;

    return-object v0
.end method
