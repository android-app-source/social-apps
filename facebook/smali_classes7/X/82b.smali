.class public LX/82b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Fe;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2Fe",
        "<",
        "LX/82a;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1288302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1288303
    return-void
.end method


# virtual methods
.method public final a(LX/2WT;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1288304
    const/4 v6, 0x0

    .line 1288305
    new-instance v0, LX/82a;

    const-string v1, "enabled_bump_all_unseen"

    invoke-virtual {p1, v1, v6}, LX/2WT;->a(Ljava/lang/String;Z)Z

    move-result v1

    const-string v2, "enabled_nsp_bump"

    invoke-virtual {p1, v2, v6}, LX/2WT;->a(Ljava/lang/String;Z)Z

    move-result v2

    const-string v3, "bump_limit"

    invoke-virtual {p1, v3, v6}, LX/2WT;->a(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "is_all_seen_refresh_enabled"

    invoke-virtual {p1, v4, v6}, LX/2WT;->a(Ljava/lang/String;Z)Z

    move-result v4

    const-string v5, "is_low_engagement_refresh_enabled"

    invoke-virtual {p1, v5, v6}, LX/2WT;->a(Ljava/lang/String;Z)Z

    move-result v5

    const-string v6, "refresh_interval"

    const-wide v8, 0x7fffffffffffffffL

    invoke-virtual {p1, v6, v8, v9}, LX/2WT;->a(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v8, "connection_quality"

    const-string v9, "EXCELLENT"

    invoke-virtual {p1, v8, v9}, LX/2WT;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, LX/82a;-><init>(ZZIZZJLjava/lang/String;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1288306
    const-string v0, "android_newsfeed_refresh_events_1_29"

    return-object v0
.end method
