.class public final LX/7se;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 20

    .prologue
    .line 1265418
    const-wide/16 v16, 0x0

    .line 1265419
    const/4 v14, 0x0

    .line 1265420
    const/4 v13, 0x0

    .line 1265421
    const/4 v12, 0x0

    .line 1265422
    const/4 v11, 0x0

    .line 1265423
    const/4 v10, 0x0

    .line 1265424
    const/4 v9, 0x0

    .line 1265425
    const/4 v8, 0x0

    .line 1265426
    const-wide/16 v6, 0x0

    .line 1265427
    const/4 v5, 0x0

    .line 1265428
    const/4 v4, 0x0

    .line 1265429
    const/4 v3, 0x0

    .line 1265430
    const/4 v2, 0x0

    .line 1265431
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v18, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    if-eq v15, v0, :cond_f

    .line 1265432
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1265433
    const/4 v2, 0x0

    .line 1265434
    :goto_0
    return v2

    .line 1265435
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_b

    .line 1265436
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1265437
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1265438
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1265439
    const-string v6, "end_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1265440
    const/4 v2, 0x1

    .line 1265441
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1265442
    :cond_1
    const-string v6, "eventProfilePicture"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1265443
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 1265444
    :cond_2
    const-string v6, "event_place"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1265445
    invoke-static/range {p0 .. p1}, LX/7rw;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 1265446
    :cond_3
    const-string v6, "event_ticket_provider"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1265447
    invoke-static/range {p0 .. p1}, LX/7sa;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 1265448
    :cond_4
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1265449
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 1265450
    :cond_5
    const-string v6, "is_all_day"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1265451
    const/4 v2, 0x1

    .line 1265452
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move v15, v6

    goto :goto_1

    .line 1265453
    :cond_6
    const-string v6, "name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1265454
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 1265455
    :cond_7
    const-string v6, "purchased_ticket_orders"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1265456
    invoke-static/range {p0 .. p1}, LX/7sd;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 1265457
    :cond_8
    const-string v6, "start_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1265458
    const/4 v2, 0x1

    .line 1265459
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v8, v2

    move-wide v12, v6

    goto/16 :goto_1

    .line 1265460
    :cond_9
    const-string v6, "timezone"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1265461
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 1265462
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1265463
    :cond_b
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1265464
    if-eqz v3, :cond_c

    .line 1265465
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1265466
    :cond_c
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1265467
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1265468
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1265469
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1265470
    if-eqz v9, :cond_d

    .line 1265471
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->a(IZ)V

    .line 1265472
    :cond_d
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1265473
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1265474
    if-eqz v8, :cond_e

    .line 1265475
    const/16 v3, 0x8

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1265476
    :cond_e
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1265477
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_f
    move v15, v10

    move/from16 v18, v13

    move/from16 v19, v14

    move v10, v5

    move v14, v9

    move v9, v3

    move v3, v4

    move-wide/from16 v4, v16

    move/from16 v16, v11

    move/from16 v17, v12

    move v11, v8

    move-wide v12, v6

    move v8, v2

    goto/16 :goto_1
.end method
