.class public LX/7lf;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Ljava/lang/Void;",
        "LX/5tj;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1235397
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 1235398
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1235399
    const-class v0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5tj;

    .line 1235400
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/5tj;->bh_()LX/1VU;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/5tj;->bh_()LX/1VU;

    move-result-object v1

    invoke-interface {v1}, LX/1VU;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/5tj;->c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/5tj;->c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->a()LX/1Fd;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/5tj;->c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->a()LX/1Fd;

    move-result-object v1

    invoke-interface {v1}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/5tj;->c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->b()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/5tj;->c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->b()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/5tj;->c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->b()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1235401
    :cond_0
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Invalid JSON result"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1235402
    :cond_1
    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1235403
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 1235404
    new-instance v0, LX/7lY;

    invoke-direct {v0}, LX/7lY;-><init>()V

    move-object v0, v0

    .line 1235405
    const-string v1, "review_id"

    const-string v2, "{result=post_review:$.id}"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1235406
    return-object v0
.end method
