.class public LX/7G1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/7G1;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0SG;

.field private final c:LX/6Po;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;LX/6Po;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1188663
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1188664
    iput-object p1, p0, LX/7G1;->a:LX/0Zb;

    .line 1188665
    iput-object p2, p0, LX/7G1;->b:LX/0SG;

    .line 1188666
    iput-object p3, p0, LX/7G1;->c:LX/6Po;

    .line 1188667
    return-void
.end method

.method public static a(LX/0QB;)LX/7G1;
    .locals 6

    .prologue
    .line 1188668
    sget-object v0, LX/7G1;->d:LX/7G1;

    if-nez v0, :cond_1

    .line 1188669
    const-class v1, LX/7G1;

    monitor-enter v1

    .line 1188670
    :try_start_0
    sget-object v0, LX/7G1;->d:LX/7G1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1188671
    if-eqz v2, :cond_0

    .line 1188672
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1188673
    new-instance p0, LX/7G1;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/6Po;->a(LX/0QB;)LX/6Po;

    move-result-object v5

    check-cast v5, LX/6Po;

    invoke-direct {p0, v3, v4, v5}, LX/7G1;-><init>(LX/0Zb;LX/0SG;LX/6Po;)V

    .line 1188674
    move-object v0, p0

    .line 1188675
    sput-object v0, LX/7G1;->d:LX/7G1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1188676
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1188677
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1188678
    :cond_1
    sget-object v0, LX/7G1;->d:LX/7G1;

    return-object v0

    .line 1188679
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1188680
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/7GT;JIZ)V
    .locals 4

    .prologue
    .line 1188681
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "handle_deltas_perf"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "is_success"

    invoke-virtual {v0, v1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "deltas_count"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "total_time"

    iget-object v2, p0, LX/7G1;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1188682
    const-string v1, "queue_type"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "android_sync"

    .line 1188683
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1188684
    iget-object v1, p0, LX/7G1;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1188685
    return-void
.end method

.method public final a(LX/7GT;Lcom/facebook/sync/analytics/FullRefreshReason;)V
    .locals 4

    .prologue
    .line 1188686
    iget-object v0, p0, LX/7G1;->c:LX/6Po;

    sget-object v1, LX/7GY;->c:LX/6Pr;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "full_refresh ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, LX/7GT;->apiString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 1188687
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "sync_full_refresh"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "queue_type"

    iget-object v2, p1, LX/7GT;->apiString:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "reason_type"

    iget-object v2, p2, Lcom/facebook/sync/analytics/FullRefreshReason;->h:LX/7Fz;

    invoke-virtual {v2}, LX/7Fz;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "reason_msg"

    iget-object v2, p2, Lcom/facebook/sync/analytics/FullRefreshReason;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "android_sync"

    .line 1188688
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1188689
    move-object v0, v0

    .line 1188690
    iget-object v1, p0, LX/7G1;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1188691
    return-void
.end method

.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7GT;)V
    .locals 2

    .prologue
    .line 1188692
    const-string v0, "queue_type"

    invoke-virtual {p1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "android_sync"

    .line 1188693
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1188694
    iget-object v0, p0, LX/7G1;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1188695
    return-void
.end method
