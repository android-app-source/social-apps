.class public final enum LX/7TZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7TZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7TZ;

.field public static final enum VIEW_TYPE_BOTTOM_PADDING:LX/7TZ;

.field public static final enum VIEW_TYPE_DESCRIPTION_ONLY_ITEM:LX/7TZ;

.field public static final enum VIEW_TYPE_REGULAR_ITEM:LX/7TZ;

.field public static final enum VIEW_TYPE_UNKNOWN:LX/7TZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1210791
    new-instance v0, LX/7TZ;

    const-string v1, "VIEW_TYPE_REGULAR_ITEM"

    invoke-direct {v0, v1, v2}, LX/7TZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TZ;->VIEW_TYPE_REGULAR_ITEM:LX/7TZ;

    .line 1210792
    new-instance v0, LX/7TZ;

    const-string v1, "VIEW_TYPE_DESCRIPTION_ONLY_ITEM"

    invoke-direct {v0, v1, v3}, LX/7TZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TZ;->VIEW_TYPE_DESCRIPTION_ONLY_ITEM:LX/7TZ;

    .line 1210793
    new-instance v0, LX/7TZ;

    const-string v1, "VIEW_TYPE_BOTTOM_PADDING"

    invoke-direct {v0, v1, v4}, LX/7TZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TZ;->VIEW_TYPE_BOTTOM_PADDING:LX/7TZ;

    .line 1210794
    new-instance v0, LX/7TZ;

    const-string v1, "VIEW_TYPE_UNKNOWN"

    invoke-direct {v0, v1, v5}, LX/7TZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TZ;->VIEW_TYPE_UNKNOWN:LX/7TZ;

    .line 1210795
    const/4 v0, 0x4

    new-array v0, v0, [LX/7TZ;

    sget-object v1, LX/7TZ;->VIEW_TYPE_REGULAR_ITEM:LX/7TZ;

    aput-object v1, v0, v2

    sget-object v1, LX/7TZ;->VIEW_TYPE_DESCRIPTION_ONLY_ITEM:LX/7TZ;

    aput-object v1, v0, v3

    sget-object v1, LX/7TZ;->VIEW_TYPE_BOTTOM_PADDING:LX/7TZ;

    aput-object v1, v0, v4

    sget-object v1, LX/7TZ;->VIEW_TYPE_UNKNOWN:LX/7TZ;

    aput-object v1, v0, v5

    sput-object v0, LX/7TZ;->$VALUES:[LX/7TZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1210796
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7TZ;
    .locals 1

    .prologue
    .line 1210797
    const-class v0, LX/7TZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7TZ;

    return-object v0
.end method

.method public static values()[LX/7TZ;
    .locals 1

    .prologue
    .line 1210798
    sget-object v0, LX/7TZ;->$VALUES:[LX/7TZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7TZ;

    return-object v0
.end method
