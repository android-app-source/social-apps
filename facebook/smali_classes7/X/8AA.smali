.class public final LX/8AA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

.field public b:LX/8A9;

.field public c:J

.field public d:LX/8A6;

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/8AB;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1305835
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305836
    sget-object v0, LX/8A9;->LAUNCH_COMPOSER:LX/8A9;

    iput-object v0, p0, LX/8AA;->b:LX/8A9;

    .line 1305837
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/8AA;->c:J

    .line 1305838
    new-instance v0, LX/8A6;

    invoke-direct {v0}, LX/8A6;-><init>()V

    invoke-virtual {v0}, LX/8A6;->o()LX/8A6;

    move-result-object v0

    iput-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305839
    iput-boolean v2, p0, LX/8AA;->e:Z

    .line 1305840
    iput-boolean v2, p0, LX/8AA;->f:Z

    .line 1305841
    iput-boolean v3, p0, LX/8AA;->g:Z

    .line 1305842
    iput-boolean v3, p0, LX/8AA;->h:Z

    .line 1305843
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1305844
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305845
    iput-object p1, v0, LX/8A6;->e:LX/8AB;

    .line 1305846
    return-void
.end method


# virtual methods
.method public final a()LX/8AA;
    .locals 1

    .prologue
    .line 1305847
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8AA;->f:Z

    .line 1305848
    return-object p0
.end method

.method public final a(II)LX/8AA;
    .locals 4

    .prologue
    .line 1305849
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    const/4 v2, 0x1

    .line 1305850
    if-ltz p1, :cond_1

    if-ge p2, p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v1, v2

    :goto_0
    const-string v3, "min >= 0 AND (max >= min OR max == NO_MAX)"

    invoke-static {v1, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1305851
    iput-boolean v2, v0, LX/8A6;->w:Z

    .line 1305852
    iput p1, v0, LX/8A6;->x:I

    .line 1305853
    iput p2, v0, LX/8A6;->y:I

    .line 1305854
    return-object p0

    .line 1305855
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(LX/0Px;)LX/8AA;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)",
            "LX/8AA;"
        }
    .end annotation

    .prologue
    .line 1305856
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305857
    iput-object p1, v0, LX/8A6;->d:LX/0Px;

    .line 1305858
    return-object p0
.end method

.method public final a(LX/8A9;)LX/8AA;
    .locals 2

    .prologue
    .line 1305859
    iput-object p1, p0, LX/8AA;->b:LX/8A9;

    .line 1305860
    sget-object v0, LX/8A8;->a:[I

    invoke-virtual {p1}, LX/8A9;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1305861
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305862
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/8A6;->h:Z

    .line 1305863
    :goto_0
    return-object p0

    .line 1305864
    :pswitch_0
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    invoke-virtual {v0}, LX/8A6;->o()LX/8A6;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)LX/8AA;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1305865
    iput-object p1, p0, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1305866
    return-object p0
.end method

.method public final a(Z)LX/8AA;
    .locals 2

    .prologue
    .line 1305884
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305885
    iget-boolean v1, v0, LX/8A6;->w:Z

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1305886
    iput-boolean p1, v0, LX/8A6;->z:Z

    .line 1305887
    return-object p0
.end method

.method public final b()LX/8AA;
    .locals 2

    .prologue
    .line 1305867
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305868
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8A6;->i:Z

    .line 1305869
    return-object p0
.end method

.method public final b(II)LX/8AA;
    .locals 1

    .prologue
    .line 1305870
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    invoke-virtual {v0, p1, p2}, LX/8A6;->a(II)LX/8A6;

    .line 1305871
    return-object p0
.end method

.method public final c()LX/8AA;
    .locals 2

    .prologue
    .line 1305872
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305873
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8A6;->j:Z

    .line 1305874
    return-object p0
.end method

.method public final d()LX/8AA;
    .locals 2

    .prologue
    .line 1305875
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305876
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/8A6;->j:Z

    .line 1305877
    return-object p0
.end method

.method public final e()LX/8AA;
    .locals 2

    .prologue
    .line 1305878
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305879
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8A6;->n:Z

    .line 1305880
    return-object p0
.end method

.method public final f()LX/8AA;
    .locals 2

    .prologue
    .line 1305832
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305833
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8A6;->o:Z

    .line 1305834
    return-object p0
.end method

.method public final g()LX/8AA;
    .locals 2

    .prologue
    .line 1305881
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305882
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8A6;->p:Z

    .line 1305883
    return-object p0
.end method

.method public final h()LX/8AA;
    .locals 2

    .prologue
    .line 1305785
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305786
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8A6;->q:Z

    .line 1305787
    return-object p0
.end method

.method public final i()LX/8AA;
    .locals 2

    .prologue
    .line 1305788
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305789
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/8A6;->a:Z

    .line 1305790
    return-object p0
.end method

.method public final j()LX/8AA;
    .locals 3

    .prologue
    .line 1305791
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305792
    iget-object v1, v0, LX/8A6;->b:LX/4gI;

    sget-object v2, LX/4gI;->ALL:LX/4gI;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1305793
    sget-object v1, LX/4gI;->PHOTO_ONLY:LX/4gI;

    iput-object v1, v0, LX/8A6;->b:LX/4gI;

    .line 1305794
    return-object p0

    .line 1305795
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final k()LX/8AA;
    .locals 3

    .prologue
    .line 1305796
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305797
    iget-object v1, v0, LX/8A6;->b:LX/4gI;

    sget-object v2, LX/4gI;->ALL:LX/4gI;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1305798
    sget-object v1, LX/4gI;->VIDEO_ONLY:LX/4gI;

    iput-object v1, v0, LX/8A6;->b:LX/4gI;

    .line 1305799
    return-object p0

    .line 1305800
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final l()LX/8AA;
    .locals 2

    .prologue
    .line 1305801
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305802
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/8A6;->c:Z

    .line 1305803
    return-object p0
.end method

.method public final m()LX/8AA;
    .locals 2

    .prologue
    .line 1305804
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305805
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/8A6;->k:Z

    .line 1305806
    return-object p0
.end method

.method public final n()LX/8AA;
    .locals 2

    .prologue
    .line 1305807
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305808
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/8A6;->l:Z

    .line 1305809
    return-object p0
.end method

.method public final o()LX/8AA;
    .locals 3

    .prologue
    .line 1305810
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305811
    iget v1, v0, LX/8A6;->u:I

    if-gtz v1, :cond_0

    .line 1305812
    const/4 v1, 0x1

    iget v2, v0, LX/8A6;->v:I

    invoke-virtual {v0, v1, v2}, LX/8A6;->a(II)LX/8A6;

    .line 1305813
    :cond_0
    return-object p0
.end method

.method public final p()LX/8AA;
    .locals 2

    .prologue
    .line 1305814
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305815
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8A6;->f:Z

    .line 1305816
    return-object p0
.end method

.method public final q()LX/8AA;
    .locals 2

    .prologue
    .line 1305817
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305818
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8A6;->g:Z

    .line 1305819
    return-object p0
.end method

.method public final r()LX/8AA;
    .locals 1

    .prologue
    .line 1305820
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8AA;->e:Z

    .line 1305821
    return-object p0
.end method

.method public final s()LX/8AA;
    .locals 2

    .prologue
    .line 1305822
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305823
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8A6;->r:Z

    .line 1305824
    return-object p0
.end method

.method public final t()LX/8AA;
    .locals 2

    .prologue
    .line 1305825
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305826
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/8A6;->r:Z

    .line 1305827
    return-object p0
.end method

.method public final v()LX/8AA;
    .locals 2

    .prologue
    .line 1305828
    iget-object v0, p0, LX/8AA;->d:LX/8A6;

    .line 1305829
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8A6;->t:Z

    .line 1305830
    return-object p0
.end method

.method public final w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;
    .locals 2

    .prologue
    .line 1305831
    new-instance v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;-><init>(LX/8AA;)V

    return-object v0
.end method
