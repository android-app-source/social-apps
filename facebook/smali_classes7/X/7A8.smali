.class public final LX/7A8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1175702
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1175703
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1175704
    :goto_0
    return v1

    .line 1175705
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1175706
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1175707
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1175708
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1175709
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1175710
    const-string v4, "edges"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1175711
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1175712
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 1175713
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 1175714
    invoke-static {p0, p1}, LX/7AG;->b(LX/15w;LX/186;)I

    move-result v3

    .line 1175715
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1175716
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1175717
    goto :goto_1

    .line 1175718
    :cond_3
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1175719
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1175720
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_a

    .line 1175721
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1175722
    :goto_3
    move v0, v3

    .line 1175723
    goto :goto_1

    .line 1175724
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1175725
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1175726
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1175727
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1175728
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_8

    .line 1175729
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1175730
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1175731
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_6

    if-eqz v6, :cond_6

    .line 1175732
    const-string v7, "has_next_page"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1175733
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v4

    goto :goto_4

    .line 1175734
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 1175735
    :cond_8
    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1175736
    if-eqz v0, :cond_9

    .line 1175737
    invoke-virtual {p1, v3, v5}, LX/186;->a(IZ)V

    .line 1175738
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_a
    move v0, v3

    move v5, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1175739
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1175740
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1175741
    if-eqz v0, :cond_1

    .line 1175742
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175743
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1175744
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1175745
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/7AG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1175746
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1175747
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1175748
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1175749
    if-eqz v0, :cond_3

    .line 1175750
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175751
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1175752
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1175753
    if-eqz v1, :cond_2

    .line 1175754
    const-string v2, "has_next_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175755
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1175756
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1175757
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1175758
    return-void
.end method
