.class public final LX/6oa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;)V
    .locals 0

    .prologue
    .line 1148260
    iput-object p1, p0, LX/6oa;->a:Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x153915de

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1148261
    iget-object v1, p0, LX/6oa;->a:Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;

    .line 1148262
    iget-object v2, v1, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v2}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1148263
    :goto_0
    const/16 v1, 0x27

    const v2, 0x304ef9ea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1148264
    :cond_0
    iget-object v2, v1, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->c:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-virtual {v2}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1148265
    iget-object v2, v1, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance p0, LX/6ob;

    invoke-direct {p0, v1}, LX/6ob;-><init>(Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;)V

    iget-object p1, v1, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->d:Ljava/util/concurrent/Executor;

    invoke-static {v2, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
