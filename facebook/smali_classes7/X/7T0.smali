.class public LX/7T0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/5Pc;

.field public c:Landroid/graphics/SurfaceTexture;

.field private d:Landroid/view/Surface;

.field public e:Landroid/view/Surface;

.field public f:Landroid/opengl/EGLDisplay;

.field public g:Landroid/opengl/EGLContext;

.field public h:Landroid/opengl/EGLSurface;

.field public i:LX/7T2;

.field public j:LX/7T1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1209513
    const-class v0, LX/7T0;

    sput-object v0, LX/7T0;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/5Pc;Landroid/view/Surface;LX/7Sx;)V
    .locals 2

    .prologue
    .line 1209514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1209515
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    iput-object v0, p0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    .line 1209516
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    iput-object v0, p0, LX/7T0;->g:Landroid/opengl/EGLContext;

    .line 1209517
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    iput-object v0, p0, LX/7T0;->h:Landroid/opengl/EGLSurface;

    .line 1209518
    iput-object p1, p0, LX/7T0;->b:LX/5Pc;

    .line 1209519
    iput-object p2, p0, LX/7T0;->d:Landroid/view/Surface;

    .line 1209520
    invoke-direct {p0}, LX/7T0;->f()V

    .line 1209521
    invoke-direct {p0}, LX/7T0;->g()V

    .line 1209522
    new-instance v0, LX/7T2;

    iget-object v1, p0, LX/7T0;->b:LX/5Pc;

    sget-object p1, LX/60w;->RGBA:LX/60w;

    invoke-direct {v0, v1, p3, p1}, LX/7T2;-><init>(LX/5Pc;LX/7Sx;LX/60w;)V

    iput-object v0, p0, LX/7T0;->i:LX/7T2;

    .line 1209523
    iget-object v0, p0, LX/7T0;->i:LX/7T2;

    invoke-virtual {v0}, LX/7T2;->b()V

    .line 1209524
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, LX/7T0;->i:LX/7T2;

    invoke-virtual {v1}, LX/7T2;->a()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, LX/7T0;->c:Landroid/graphics/SurfaceTexture;

    .line 1209525
    new-instance v0, LX/7T1;

    iget-object v1, p0, LX/7T0;->c:Landroid/graphics/SurfaceTexture;

    iget-object p1, p0, LX/7T0;->i:LX/7T2;

    const/16 p2, 0x1388

    invoke-direct {v0, v1, p1, p2}, LX/7T1;-><init>(Landroid/graphics/SurfaceTexture;LX/7T2;I)V

    iput-object v0, p0, LX/7T0;->j:LX/7T1;

    .line 1209526
    iget-object v0, p0, LX/7T0;->c:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, LX/7T0;->j:LX/7T1;

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 1209527
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, LX/7T0;->c:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, LX/7T0;->e:Landroid/view/Surface;

    .line 1209528
    return-void
.end method

.method private f()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1209485
    invoke-static {v2}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    .line 1209486
    iget-object v0, p0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-ne v0, v1, :cond_0

    .line 1209487
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to get EGL14 display"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209488
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1209489
    iget-object v1, p0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    invoke-static {v1, v0, v2, v0, v5}, Landroid/opengl/EGL14;->eglInitialize(Landroid/opengl/EGLDisplay;[II[II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1209490
    const/4 v0, 0x0

    iput-object v0, p0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    .line 1209491
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to initialize EGL14"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209492
    :cond_1
    const/16 v0, 0xb

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    .line 1209493
    new-array v3, v5, [Landroid/opengl/EGLConfig;

    .line 1209494
    new-array v6, v5, [I

    .line 1209495
    iget-object v0, p0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    move v4, v2

    move v7, v2

    invoke-static/range {v0 .. v7}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1209496
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to find RGB888+recordable ES2 EGL config"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209497
    :cond_2
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    .line 1209498
    iget-object v1, p0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    aget-object v4, v3, v2

    sget-object v6, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v1, v4, v6, v0, v2}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v0

    iput-object v0, p0, LX/7T0;->g:Landroid/opengl/EGLContext;

    .line 1209499
    const-string v0, "eglCreateContext"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 1209500
    iget-object v0, p0, LX/7T0;->g:Landroid/opengl/EGLContext;

    if-nez v0, :cond_3

    .line 1209501
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "null context"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209502
    :cond_3
    new-array v0, v5, [I

    const/16 v1, 0x3038

    aput v1, v0, v2

    .line 1209503
    iget-object v1, p0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    aget-object v3, v3, v2

    iget-object v4, p0, LX/7T0;->d:Landroid/view/Surface;

    invoke-static {v1, v3, v4, v0, v2}, Landroid/opengl/EGL14;->eglCreateWindowSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Ljava/lang/Object;[II)Landroid/opengl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, LX/7T0;->h:Landroid/opengl/EGLSurface;

    .line 1209504
    const-string v0, "eglCreateWindowSurface"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 1209505
    iget-object v0, p0, LX/7T0;->h:Landroid/opengl/EGLSurface;

    if-nez v0, :cond_4

    .line 1209506
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "surface was null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209507
    :cond_4
    return-void

    .line 1209508
    :array_0
    .array-data 4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3040
        0x4
        0x3142
        0x1
        0x3038
    .end array-data

    .line 1209509
    :array_1
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method private g()V
    .locals 4

    .prologue
    .line 1209510
    iget-object v0, p0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, LX/7T0;->h:Landroid/opengl/EGLSurface;

    iget-object v2, p0, LX/7T0;->h:Landroid/opengl/EGLSurface;

    iget-object v3, p0, LX/7T0;->g:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1209511
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglMakeCurrent failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209512
    :cond_0
    return-void
.end method
