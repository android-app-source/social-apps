.class public final enum LX/8TZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8TZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8TZ;

.field public static final enum FB_BOOKMARK:LX/8TZ;

.field public static final enum FB_FEED:LX/8TZ;

.field public static final enum FB_FEED_ADS:LX/8TZ;

.field public static final enum FB_FEED_ORGANIC:LX/8TZ;

.field public static final enum FB_FEED_SCREENSHOT:LX/8TZ;

.field public static final enum FB_GROUP_MALL:LX/8TZ;

.field public static final enum FB_GROUP_MALL_SCREENSHOT:LX/8TZ;

.field public static final enum FB_GROUP_NEWSFEED:LX/8TZ;

.field public static final enum FB_GROUP_NEWSFEED_SCREENSHOT:LX/8TZ;

.field public static final enum FB_HOMESCREEN_SHORTCUT:LX/8TZ;

.field public static final enum GAME_RECOMMENDATION:LX/8TZ;

.field public static final enum MESSENGER_ADMIN_MESSAGE:LX/8TZ;

.field public static final enum MESSENGER_COMPOSER:LX/8TZ;

.field public static final enum MESSENGER_GAME_EMOJI:LX/8TZ;

.field public static final enum MESSENGER_GAME_INBOX_UNIT:LX/8TZ;

.field public static final enum MESSENGER_GAME_SCORE_SHARE:LX/8TZ;

.field public static final enum MESSENGER_GAME_SEARCH:LX/8TZ;

.field public static final enum MESSENGER_GAME_SHARE:LX/8TZ;

.field public static final enum UNKNOWN:LX/8TZ;


# instance fields
.field private final source:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1348778
    new-instance v0, LX/8TZ;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v4, v2}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->UNKNOWN:LX/8TZ;

    .line 1348779
    new-instance v0, LX/8TZ;

    const-string v1, "FB_BOOKMARK"

    const-string v2, "fb_bookmark"

    invoke-direct {v0, v1, v5, v2}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->FB_BOOKMARK:LX/8TZ;

    .line 1348780
    new-instance v0, LX/8TZ;

    const-string v1, "FB_FEED"

    const-string v2, "fb_feed"

    invoke-direct {v0, v1, v6, v2}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->FB_FEED:LX/8TZ;

    .line 1348781
    new-instance v0, LX/8TZ;

    const-string v1, "FB_FEED_ADS"

    const-string v2, "fb_feed_ads"

    invoke-direct {v0, v1, v7, v2}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->FB_FEED_ADS:LX/8TZ;

    .line 1348782
    new-instance v0, LX/8TZ;

    const-string v1, "FB_FEED_ORGANIC"

    const-string v2, "fb_feed_organic"

    invoke-direct {v0, v1, v8, v2}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->FB_FEED_ORGANIC:LX/8TZ;

    .line 1348783
    new-instance v0, LX/8TZ;

    const-string v1, "FB_FEED_SCREENSHOT"

    const/4 v2, 0x5

    const-string v3, "fb_feed_screenshot"

    invoke-direct {v0, v1, v2, v3}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->FB_FEED_SCREENSHOT:LX/8TZ;

    .line 1348784
    new-instance v0, LX/8TZ;

    const-string v1, "FB_GROUP_MALL"

    const/4 v2, 0x6

    const-string v3, "fb_group_mall"

    invoke-direct {v0, v1, v2, v3}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->FB_GROUP_MALL:LX/8TZ;

    .line 1348785
    new-instance v0, LX/8TZ;

    const-string v1, "FB_GROUP_NEWSFEED"

    const/4 v2, 0x7

    const-string v3, "fb_group_newsfeed"

    invoke-direct {v0, v1, v2, v3}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->FB_GROUP_NEWSFEED:LX/8TZ;

    .line 1348786
    new-instance v0, LX/8TZ;

    const-string v1, "FB_GROUP_MALL_SCREENSHOT"

    const/16 v2, 0x8

    const-string v3, "fb_group_mall_screenshot"

    invoke-direct {v0, v1, v2, v3}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->FB_GROUP_MALL_SCREENSHOT:LX/8TZ;

    .line 1348787
    new-instance v0, LX/8TZ;

    const-string v1, "FB_GROUP_NEWSFEED_SCREENSHOT"

    const/16 v2, 0x9

    const-string v3, "fb_group_newsfeed_screenshot"

    invoke-direct {v0, v1, v2, v3}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->FB_GROUP_NEWSFEED_SCREENSHOT:LX/8TZ;

    .line 1348788
    new-instance v0, LX/8TZ;

    const-string v1, "FB_HOMESCREEN_SHORTCUT"

    const/16 v2, 0xa

    const-string v3, "fb_homescreen_shortcut"

    invoke-direct {v0, v1, v2, v3}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->FB_HOMESCREEN_SHORTCUT:LX/8TZ;

    .line 1348789
    new-instance v0, LX/8TZ;

    const-string v1, "MESSENGER_ADMIN_MESSAGE"

    const/16 v2, 0xb

    const-string v3, "messenger_admin_message"

    invoke-direct {v0, v1, v2, v3}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->MESSENGER_ADMIN_MESSAGE:LX/8TZ;

    .line 1348790
    new-instance v0, LX/8TZ;

    const-string v1, "MESSENGER_COMPOSER"

    const/16 v2, 0xc

    const-string v3, "messenger_composer"

    invoke-direct {v0, v1, v2, v3}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->MESSENGER_COMPOSER:LX/8TZ;

    .line 1348791
    new-instance v0, LX/8TZ;

    const-string v1, "MESSENGER_GAME_EMOJI"

    const/16 v2, 0xd

    const-string v3, "messenger_game_emoji"

    invoke-direct {v0, v1, v2, v3}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->MESSENGER_GAME_EMOJI:LX/8TZ;

    .line 1348792
    new-instance v0, LX/8TZ;

    const-string v1, "MESSENGER_GAME_INBOX_UNIT"

    const/16 v2, 0xe

    const-string v3, "messenger_game_inbox_unit"

    invoke-direct {v0, v1, v2, v3}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->MESSENGER_GAME_INBOX_UNIT:LX/8TZ;

    .line 1348793
    new-instance v0, LX/8TZ;

    const-string v1, "MESSENGER_GAME_SCORE_SHARE"

    const/16 v2, 0xf

    const-string v3, "messenger_game_score_share"

    invoke-direct {v0, v1, v2, v3}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->MESSENGER_GAME_SCORE_SHARE:LX/8TZ;

    .line 1348794
    new-instance v0, LX/8TZ;

    const-string v1, "MESSENGER_GAME_SEARCH"

    const/16 v2, 0x10

    const-string v3, "messenger_game_search"

    invoke-direct {v0, v1, v2, v3}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->MESSENGER_GAME_SEARCH:LX/8TZ;

    .line 1348795
    new-instance v0, LX/8TZ;

    const-string v1, "MESSENGER_GAME_SHARE"

    const/16 v2, 0x11

    const-string v3, "messenger_game_share"

    invoke-direct {v0, v1, v2, v3}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->MESSENGER_GAME_SHARE:LX/8TZ;

    .line 1348796
    new-instance v0, LX/8TZ;

    const-string v1, "GAME_RECOMMENDATION"

    const/16 v2, 0x12

    const-string v3, "game_recommendation"

    invoke-direct {v0, v1, v2, v3}, LX/8TZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TZ;->GAME_RECOMMENDATION:LX/8TZ;

    .line 1348797
    const/16 v0, 0x13

    new-array v0, v0, [LX/8TZ;

    sget-object v1, LX/8TZ;->UNKNOWN:LX/8TZ;

    aput-object v1, v0, v4

    sget-object v1, LX/8TZ;->FB_BOOKMARK:LX/8TZ;

    aput-object v1, v0, v5

    sget-object v1, LX/8TZ;->FB_FEED:LX/8TZ;

    aput-object v1, v0, v6

    sget-object v1, LX/8TZ;->FB_FEED_ADS:LX/8TZ;

    aput-object v1, v0, v7

    sget-object v1, LX/8TZ;->FB_FEED_ORGANIC:LX/8TZ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/8TZ;->FB_FEED_SCREENSHOT:LX/8TZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8TZ;->FB_GROUP_MALL:LX/8TZ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8TZ;->FB_GROUP_NEWSFEED:LX/8TZ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8TZ;->FB_GROUP_MALL_SCREENSHOT:LX/8TZ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8TZ;->FB_GROUP_NEWSFEED_SCREENSHOT:LX/8TZ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8TZ;->FB_HOMESCREEN_SHORTCUT:LX/8TZ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8TZ;->MESSENGER_ADMIN_MESSAGE:LX/8TZ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/8TZ;->MESSENGER_COMPOSER:LX/8TZ;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/8TZ;->MESSENGER_GAME_EMOJI:LX/8TZ;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/8TZ;->MESSENGER_GAME_INBOX_UNIT:LX/8TZ;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/8TZ;->MESSENGER_GAME_SCORE_SHARE:LX/8TZ;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/8TZ;->MESSENGER_GAME_SEARCH:LX/8TZ;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/8TZ;->MESSENGER_GAME_SHARE:LX/8TZ;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/8TZ;->GAME_RECOMMENDATION:LX/8TZ;

    aput-object v2, v0, v1

    sput-object v0, LX/8TZ;->$VALUES:[LX/8TZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1348775
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1348776
    iput-object p3, p0, LX/8TZ;->source:Ljava/lang/String;

    .line 1348777
    return-void
.end method

.method public static fromSerializable(Ljava/io/Serializable;)LX/8TZ;
    .locals 1

    .prologue
    .line 1348769
    instance-of v0, p0, LX/8TZ;

    if-eqz v0, :cond_0

    .line 1348770
    check-cast p0, LX/8TZ;

    .line 1348771
    :goto_0
    return-object p0

    .line 1348772
    :cond_0
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1348773
    check-cast p0, Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/8TZ;->valueOf(Ljava/lang/String;)LX/8TZ;

    move-result-object p0

    goto :goto_0

    .line 1348774
    :cond_1
    sget-object p0, LX/8TZ;->UNKNOWN:LX/8TZ;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/8TZ;
    .locals 1

    .prologue
    .line 1348768
    const-class v0, LX/8TZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8TZ;

    return-object v0
.end method

.method public static values()[LX/8TZ;
    .locals 1

    .prologue
    .line 1348767
    sget-object v0, LX/8TZ;->$VALUES:[LX/8TZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8TZ;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1348766
    iget-object v0, p0, LX/8TZ;->source:Ljava/lang/String;

    return-object v0
.end method
