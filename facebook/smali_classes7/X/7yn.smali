.class public LX/7yn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/view/View;

.field public b:Landroid/view/ViewGroup;

.field public c:Landroid/graphics/Rect;

.field public d:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 1280302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280303
    iput-object p1, p0, LX/7yn;->a:Landroid/view/View;

    .line 1280304
    iput-object p2, p0, LX/7yn;->c:Landroid/graphics/Rect;

    .line 1280305
    iput-object p3, p0, LX/7yn;->d:Landroid/graphics/Rect;

    .line 1280306
    const/4 v0, 0x0

    iput-object v0, p0, LX/7yn;->b:Landroid/view/ViewGroup;

    .line 1280307
    return-void
.end method

.method public static c(LX/7yn;I)I
    .locals 1

    .prologue
    .line 1280297
    iget-object v0, p0, LX/7yn;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-ge p1, v0, :cond_1

    .line 1280298
    iget-object v0, p0, LX/7yn;->c:Landroid/graphics/Rect;

    iget p1, v0, Landroid/graphics/Rect;->left:I

    .line 1280299
    :cond_0
    :goto_0
    return p1

    .line 1280300
    :cond_1
    iget-object v0, p0, LX/7yn;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    if-lt p1, v0, :cond_0

    .line 1280301
    iget-object v0, p0, LX/7yn;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    add-int/lit8 p1, v0, -0x1

    goto :goto_0
.end method

.method public static d(LX/7yn;I)I
    .locals 1

    .prologue
    .line 1280314
    iget-object v0, p0, LX/7yn;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-ge p1, v0, :cond_1

    .line 1280315
    iget-object v0, p0, LX/7yn;->c:Landroid/graphics/Rect;

    iget p1, v0, Landroid/graphics/Rect;->top:I

    .line 1280316
    :cond_0
    :goto_0
    return p1

    .line 1280317
    :cond_1
    iget-object v0, p0, LX/7yn;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-lt p1, v0, :cond_0

    .line 1280318
    iget-object v0, p0, LX/7yn;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 p1, v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(II)I
    .locals 4

    .prologue
    .line 1280308
    invoke-static {p0, p1}, LX/7yn;->c(LX/7yn;I)I

    move-result v0

    .line 1280309
    invoke-static {p0, p2}, LX/7yn;->d(LX/7yn;I)I

    move-result v1

    .line 1280310
    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-double v2, v0

    .line 1280311
    sub-int v0, p2, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-double v0, v0

    .line 1280312
    mul-double/2addr v2, v2

    mul-double/2addr v0, v0

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 1280313
    double-to-int v0, v0

    return v0
.end method
