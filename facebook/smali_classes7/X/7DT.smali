.class public final LX/7DT;
.super LX/7Cy;
.source ""

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field public final l:Ljava/lang/String;

.field public final synthetic m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;


# direct methods
.method public constructor <init>(Lcom/facebook/spherical/photo/SphericalPhotoTextureView;Landroid/view/TextureView$SurfaceTextureListener;)V
    .locals 1

    .prologue
    .line 1182928
    iput-object p1, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    .line 1182929
    invoke-direct {p0, p1, p2}, LX/7Cy;-><init>(LX/2qW;Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 1182930
    const-class v0, LX/7DT;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7DT;->l:Ljava/lang/String;

    .line 1182931
    return-void
.end method

.method private c()Lcom/facebook/spherical/photo/GlPhotoRenderThread;
    .locals 13

    .prologue
    .line 1182932
    iget-object v0, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v1, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v2, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v2, v2, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->g:LX/7DU;

    iget-object v2, v2, LX/7Cz;->b:LX/19o;

    invoke-static {v1, v2}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->a(Lcom/facebook/spherical/photo/SphericalPhotoTextureView;LX/19o;)LX/7D0;

    move-result-object v1

    .line 1182933
    iput-object v1, v0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->h:LX/7D0;

    .line 1182934
    iget-object v12, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    new-instance v0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    iget-object v1, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v1}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/7Cy;->d:Landroid/graphics/SurfaceTexture;

    iget-object v3, p0, LX/7Cy;->e:Ljava/lang/Runnable;

    iget-object v4, p0, LX/7Cy;->f:Ljava/lang/Runnable;

    iget v5, p0, LX/7Cy;->i:I

    iget v6, p0, LX/7Cy;->j:I

    iget-object v7, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v7, v7, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->h:LX/7D0;

    iget-object v8, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v8, v8, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->c:LX/0Ot;

    iget-object v9, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v9, v9, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->d:LX/0SG;

    iget-object v10, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v10, v10, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->e:LX/0wW;

    iget-object v11, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v11, v11, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->g:LX/7DU;

    invoke-direct/range {v0 .. v11}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;-><init>(Landroid/content/Context;Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;Ljava/lang/Runnable;IILX/7D0;LX/0Ot;LX/0SG;LX/0wW;LX/7DU;)V

    .line 1182935
    iput-object v0, v12, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    .line 1182936
    iget-object v0, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v0, v0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    return-object v0
.end method


# virtual methods
.method public final synthetic b()Lcom/facebook/spherical/GlMediaRenderThread;
    .locals 1

    .prologue
    .line 1182937
    invoke-direct {p0}, LX/7DT;->c()Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    move-result-object v0

    return-object v0
.end method

.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 1182938
    invoke-super {p0, p1, p2, p3}, LX/7Cy;->onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V

    .line 1182939
    iget-object v0, p0, LX/7Cy;->b:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V

    .line 1182940
    return-void
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 1182941
    iget-object v0, p0, LX/7Cy;->b:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-interface {v0, p1}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z

    .line 1182942
    invoke-super {p0, p1}, LX/7Cy;->onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z

    move-result v0

    return v0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    .prologue
    .line 1182943
    iget-object v0, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v0, v0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    if-eqz v0, :cond_0

    .line 1182944
    iget-object v0, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v0, v0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    iget-object v1, p0, LX/7DT;->m:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v1, v1, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->g:LX/7DU;

    iget-object v1, v1, LX/7DU;->l:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->a(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;)V

    .line 1182945
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/7Cy;->onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V

    .line 1182946
    return-void
.end method
