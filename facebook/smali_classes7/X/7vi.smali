.class public LX/7vi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0tX;

.field public final c:LX/0kv;

.field public final d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/concurrent/Executor;

.field private final f:LX/0gX;

.field public final g:LX/7xQ;

.field private h:LX/0gM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0tX;LX/0kv;LX/1Ck;Ljava/util/concurrent/Executor;LX/0gX;LX/7xQ;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1275035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275036
    iput-object p1, p0, LX/7vi;->a:Landroid/content/Context;

    .line 1275037
    iput-object p2, p0, LX/7vi;->b:LX/0tX;

    .line 1275038
    iput-object p3, p0, LX/7vi;->c:LX/0kv;

    .line 1275039
    iput-object p4, p0, LX/7vi;->d:LX/1Ck;

    .line 1275040
    iput-object p5, p0, LX/7vi;->e:Ljava/util/concurrent/Executor;

    .line 1275041
    iput-object p6, p0, LX/7vi;->f:LX/0gX;

    .line 1275042
    iput-object p7, p0, LX/7vi;->g:LX/7xQ;

    .line 1275043
    return-void
.end method

.method public static a$redex0(LX/7vi;Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;LX/7vr;)V
    .locals 6

    .prologue
    .line 1275003
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->n()Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1275004
    sget-object v0, LX/7vh;->a:[I

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->n()Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1275005
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->m()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->m()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1275006
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->m()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 1275007
    iget-object v2, p2, LX/7vr;->c:LX/7vs;

    iget-object v3, p2, LX/7vr;->b:Lcom/facebook/payments/checkout/model/CheckoutData;

    iget-object p0, p2, LX/7vr;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a()LX/7wo;

    move-result-object p0

    sget-object p1, LX/7wp;->BUYING:LX/7wp;

    invoke-interface {p0, p1}, LX/7wo;->a(LX/7wp;)LX/7wo;

    move-result-object p0

    invoke-interface {p0, v1}, LX/7wo;->a(Ljava/lang/String;)LX/7wo;

    move-result-object p0

    invoke-interface {p0, v0}, LX/7wo;->b(Ljava/lang/String;)LX/7wo;

    move-result-object p0

    invoke-interface {p0}, LX/7wo;->a()Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-result-object p0

    invoke-static {v2, v3, p0}, LX/7vs;->b(LX/7vs;Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V

    .line 1275008
    iget-object v2, p2, LX/7vr;->c:LX/7vs;

    iget-object v2, v2, LX/7vs;->e:LX/6Ex;

    invoke-interface {v2, v0}, LX/6Ex;->a(Ljava/lang/String;)V

    .line 1275009
    :cond_0
    :goto_0
    return-void

    .line 1275010
    :pswitch_0
    invoke-virtual {p0}, LX/7vi;->a()V

    .line 1275011
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->m()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->m()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1275012
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->m()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/7vr;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1275013
    :cond_1
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Ticket Purchase Failed : %s"

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->n()Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, LX/7vr;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1275014
    :pswitch_1
    invoke-virtual {p0}, LX/7vi;->a()V

    .line 1275015
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->m()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    .line 1275016
    iget-object v1, p2, LX/7vr;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-virtual {v1}, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a()LX/7wo;

    move-result-object v1

    .line 1275017
    sget-object v2, LX/7wp;->BOUGHT:LX/7wp;

    invoke-interface {v1, v2}, LX/7wo;->a(LX/7wp;)LX/7wo;

    .line 1275018
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_2

    .line 1275019
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/7wo;->c(Ljava/lang/String;)LX/7wo;

    .line 1275020
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/7wo;->d(Ljava/lang/String;)LX/7wo;

    .line 1275021
    :cond_2
    invoke-interface {v1, v0}, LX/7wo;->a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)LX/7wo;

    .line 1275022
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/7wo;->a(Ljava/lang/String;)LX/7wo;

    .line 1275023
    const/4 v3, 0x0

    .line 1275024
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->k()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-nez v2, :cond_3

    move v2, v3

    .line 1275025
    :goto_1
    move v2, v2

    .line 1275026
    invoke-interface {v1, v2}, LX/7wo;->a(Z)LX/7wo;

    .line 1275027
    iget-object v2, p2, LX/7vr;->c:LX/7vs;

    iget-object v3, p2, LX/7vr;->b:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, LX/7wo;->a()Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-result-object v1

    invoke-static {v2, v3, v1}, LX/7vs;->b(LX/7vs;Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V

    .line 1275028
    iget-object v1, p2, LX/7vr;->c:LX/7vs;

    iget-object v1, v1, LX/7vs;->e:LX/6Ex;

    new-instance v2, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->l()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v5}, LX/0m9;-><init>(LX/0mC;)V

    const-string v5, "event_ticketing_receipt_url"

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->o()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;-><init>(Ljava/lang/String;LX/0lF;)V

    invoke-interface {v1, v2}, LX/6Ex;->a(Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)V

    .line 1275029
    goto/16 :goto_0

    .line 1275030
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->k()LX/1vs;

    move-result-object v2

    iget-object v4, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class v5, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel;

    invoke-virtual {v4, v2, v3, v5}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    :goto_2
    const/4 v4, 0x0

    invoke-static {v2, v4}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel;

    .line 1275031
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    goto :goto_1

    .line 1275032
    :cond_4
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1275033
    goto :goto_2

    :cond_5
    move v2, v3

    .line 1275034
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(LX/4Ua;LX/7vr;)V
    .locals 1

    .prologue
    .line 1274995
    iget-object v0, p0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v0

    .line 1274996
    if-eqz v0, :cond_0

    .line 1274997
    iget-object v0, p0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v0

    .line 1274998
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1274999
    iget-object v0, p0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v0

    .line 1275000
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/7vr;->a(Ljava/lang/String;)V

    .line 1275001
    :goto_0
    return-void

    .line 1275002
    :cond_0
    invoke-virtual {p1, p0}, LX/7vr;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static b(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V
    .locals 7

    .prologue
    .line 1275044
    iget-boolean v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->p:Z

    if-nez v0, :cond_1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1275045
    iget-object v5, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    move v1, v3

    :goto_0
    if-ge v4, v6, :cond_0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    .line 1275046
    iget v0, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    if-lez v0, :cond_3

    .line 1275047
    add-int/lit8 v0, v1, 0x1

    .line 1275048
    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_0

    .line 1275049
    :cond_0
    if-ne v1, v2, :cond_2

    move v0, v2

    :goto_2
    move v0, v0

    .line 1275050
    if-nez v0, :cond_1

    .line 1275051
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Purchased ticket tiers are more than one in single tier supported event."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1275052
    :cond_1
    return-void

    :cond_2
    move v0, v3

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1274977
    iget-object v0, p0, LX/7vi;->h:LX/0gM;

    if-eqz v0, :cond_0

    .line 1274978
    iget-object v0, p0, LX/7vi;->f:LX/0gX;

    const/4 v1, 0x1

    new-array v1, v1, [LX/0gM;

    const/4 v2, 0x0

    iget-object v3, p0, LX/7vi;->h:LX/0gM;

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gX;->a(Ljava/util/Set;)V

    .line 1274979
    :cond_0
    iget-object v0, p0, LX/7vi;->d:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1274980
    return-void
.end method

.method public final a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;LX/7vr;)V
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1274981
    invoke-static {p1}, LX/7vi;->b(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V

    .line 1274982
    iget-object v7, p0, LX/7vi;->d:LX/1Ck;

    const-string v8, "event_ticket_purchase_async"

    new-instance v0, LX/7vd;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p5

    move-object v4, p4

    move-object v5, p3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, LX/7vd;-><init>(LX/7vi;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, LX/7ve;

    invoke-direct {v1, p0, p6}, LX/7ve;-><init>(LX/7vi;LX/7vr;)V

    invoke-virtual {v7, v8, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1274983
    return-void
.end method

.method public final a(Ljava/lang/String;LX/7vr;)V
    .locals 4

    .prologue
    .line 1274984
    iget-object v0, p0, LX/7vi;->h:LX/0gM;

    if-eqz v0, :cond_0

    .line 1274985
    :goto_0
    return-void

    .line 1274986
    :cond_0
    new-instance v0, LX/7oC;

    invoke-direct {v0}, LX/7oC;-><init>()V

    move-object v0, v0

    .line 1274987
    const-string v1, "input"

    new-instance v2, LX/4Ea;

    invoke-direct {v2}, LX/4Ea;-><init>()V

    .line 1274988
    const-string v3, "event_ticket_order_id"

    invoke-virtual {v2, v3, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274989
    move-object v2, v2

    .line 1274990
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1274991
    const-string v1, "should_fetch_ticket_tiers"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1274992
    :try_start_0
    iget-object v1, p0, LX/7vi;->f:LX/0gX;

    new-instance v2, LX/7vf;

    invoke-direct {v2, p0, p2}, LX/7vf;-><init>(LX/7vi;LX/7vr;)V

    invoke-virtual {v1, v0, v2}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;

    move-result-object v0

    iput-object v0, p0, LX/7vi;->h:LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1274993
    :catch_0
    move-exception v0

    .line 1274994
    invoke-virtual {p2, v0}, LX/7vr;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
