.class public final enum LX/7K6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7K6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7K6;

.field public static final enum COUNT:LX/7K6;

.field public static final enum ID_CHANGED:LX/7K6;

.field public static final enum ID_NULL:LX/7K6;

.field public static final enum TIMEOUT:LX/7K6;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1194484
    new-instance v0, LX/7K6;

    const-string v1, "COUNT"

    invoke-direct {v0, v1, v2}, LX/7K6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7K6;->COUNT:LX/7K6;

    .line 1194485
    new-instance v0, LX/7K6;

    const-string v1, "TIMEOUT"

    invoke-direct {v0, v1, v3}, LX/7K6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7K6;->TIMEOUT:LX/7K6;

    .line 1194486
    new-instance v0, LX/7K6;

    const-string v1, "ID_NULL"

    invoke-direct {v0, v1, v4}, LX/7K6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7K6;->ID_NULL:LX/7K6;

    .line 1194487
    new-instance v0, LX/7K6;

    const-string v1, "ID_CHANGED"

    invoke-direct {v0, v1, v5}, LX/7K6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7K6;->ID_CHANGED:LX/7K6;

    .line 1194488
    const/4 v0, 0x4

    new-array v0, v0, [LX/7K6;

    sget-object v1, LX/7K6;->COUNT:LX/7K6;

    aput-object v1, v0, v2

    sget-object v1, LX/7K6;->TIMEOUT:LX/7K6;

    aput-object v1, v0, v3

    sget-object v1, LX/7K6;->ID_NULL:LX/7K6;

    aput-object v1, v0, v4

    sget-object v1, LX/7K6;->ID_CHANGED:LX/7K6;

    aput-object v1, v0, v5

    sput-object v0, LX/7K6;->$VALUES:[LX/7K6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1194490
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7K6;
    .locals 1

    .prologue
    .line 1194491
    const-class v0, LX/7K6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7K6;

    return-object v0
.end method

.method public static values()[LX/7K6;
    .locals 1

    .prologue
    .line 1194489
    sget-object v0, LX/7K6;->$VALUES:[LX/7K6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7K6;

    return-object v0
.end method
