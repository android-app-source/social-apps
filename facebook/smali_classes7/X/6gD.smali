.class public LX/6gD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetInterfaces$MomentsAppInvitationActionLinkFragment;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetInterfaces$MomentsAppInvitationActionLinkFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1121464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1121465
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/6gD;->c:Ljava/util/List;

    .line 1121466
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1121467
    iput-object v0, p0, LX/6gD;->g:LX/0Px;

    .line 1121468
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1121469
    iput-object v0, p0, LX/6gD;->h:LX/0Px;

    return-void
.end method

.method public static newBuilder()LX/6gD;
    .locals 1

    .prologue
    .line 1121470
    new-instance v0, LX/6gD;

    invoke-direct {v0}, LX/6gD;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(LX/0Px;)LX/6gD;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetInterfaces$MomentsAppInvitationActionLinkFragment;",
            ">;)",
            "LX/6gD;"
        }
    .end annotation

    .prologue
    .line 1121471
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/6gD;->g:LX/0Px;

    .line 1121472
    return-object p0
.end method

.method public final a(Ljava/util/List;)LX/6gD;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/6gD;"
        }
    .end annotation

    .prologue
    .line 1121473
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LX/6gD;->c:Ljava/util/List;

    .line 1121474
    return-object p0
.end method

.method public final b(LX/0Px;)LX/6gD;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetInterfaces$MomentsAppInvitationActionLinkFragment;",
            ">;)",
            "LX/6gD;"
        }
    .end annotation

    .prologue
    .line 1121475
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/6gD;->h:LX/0Px;

    .line 1121476
    return-object p0
.end method

.method public final i()LX/6gC;
    .locals 1

    .prologue
    .line 1121477
    new-instance v0, LX/6gC;

    invoke-direct {v0, p0}, LX/6gC;-><init>(LX/6gD;)V

    return-object v0
.end method
