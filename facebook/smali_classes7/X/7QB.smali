.class public final LX/7QB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DeviceAutoplaySettingUpdateModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1mC;

.field public final synthetic b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final synthetic c:LX/1mD;


# direct methods
.method public constructor <init>(LX/1mD;LX/1mC;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0

    .prologue
    .line 1203720
    iput-object p1, p0, LX/7QB;->c:LX/1mD;

    iput-object p2, p0, LX/7QB;->a:LX/1mC;

    iput-object p3, p0, LX/7QB;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1203718
    iget-object v0, p0, LX/7QB;->c:LX/1mD;

    const-string v1, "Failed to write the client autoplay setting to the server."

    invoke-virtual {v0, v1, p1}, LX/1mD;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1203719
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1203721
    check-cast p1, Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DeviceAutoplaySettingUpdateModel;

    .line 1203722
    iget-object v0, p0, LX/7QB;->c:LX/1mD;

    iget-object v0, v0, LX/1mD;->b:LX/1mQ;

    .line 1203723
    iget-object v1, v0, LX/1mQ;->c:LX/1mR;

    const-string v2, "AUTOPLAY_SETTING_READ_QUERY"

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1203724
    iget-object v0, p0, LX/7QB;->c:LX/1mD;

    iget-object v1, p0, LX/7QB;->a:LX/1mC;

    .line 1203725
    iput-object v1, v0, LX/1mD;->e:LX/1mC;

    .line 1203726
    invoke-virtual {p1}, Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DeviceAutoplaySettingUpdateModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1203727
    invoke-virtual {p1}, Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DeviceAutoplaySettingUpdateModel;->a()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 1203728
    iget-object v4, p0, LX/7QB;->c:LX/1mD;

    iget-object v5, p0, LX/7QB;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v6, 0x0

    const-class v7, Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;

    invoke-virtual {v1, v0, v6, v7, v8}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;

    const/4 v1, 0x2

    invoke-virtual {v3, v2, v1}, LX/15i;->h(II)Z

    move-result v1

    const/4 v2, 0x0

    .line 1203729
    invoke-static {v5}, LX/1mS;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/03R;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/03R;->asBoolean(Z)Z

    move-result v3

    .line 1203730
    invoke-static {v0}, LX/1mD;->a(Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;)LX/1mC;

    move-result-object v6

    .line 1203731
    if-nez v3, :cond_2

    .line 1203732
    iput-object v6, v4, LX/1mD;->e:LX/1mC;

    .line 1203733
    iget-object v3, v4, LX/1mD;->e:LX/1mC;

    invoke-static {v5, v3}, LX/1mS;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1mC;)V

    .line 1203734
    if-nez v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    invoke-static {v5, v2}, LX/1mS;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;Z)V

    .line 1203735
    :cond_1
    :goto_0
    return-void

    .line 1203736
    :cond_2
    if-eqz v3, :cond_1

    iget-object v2, v4, LX/1mD;->e:LX/1mC;

    if-eq v2, v6, :cond_1

    .line 1203737
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Server outcome did not match the request. Sent "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v4, LX/1mD;->e:LX/1mC;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " received "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1203738
    iget-object v3, v4, LX/1mD;->c:LX/03V;

    sget-object v6, LX/1mD;->a:Ljava/lang/String;

    invoke-virtual {v3, v6, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1203739
    goto :goto_0
.end method
