.class public LX/7mD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/composer/publish/common/PublishPostParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0SG;

.field private final c:LX/0lB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1236654
    const-class v0, LX/7mD;

    sput-object v0, LX/7mD;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1236655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236656
    iput-object p1, p0, LX/7mD;->b:LX/0SG;

    .line 1236657
    iput-object p2, p0, LX/7mD;->c:LX/0lB;

    .line 1236658
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1236659
    check-cast p1, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 1236660
    iget-wide v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_8

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1236661
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1236662
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1236663
    iget-object v0, p0, LX/7mD;->b:LX/0SG;

    iget-object v1, p0, LX/7mD;->c:LX/0lB;

    invoke-static {p1, v0, v1}, LX/7mB;->a(Lcom/facebook/composer/publish/common/PublishPostParams;LX/0SG;LX/0lB;)Ljava/util/List;

    move-result-object v4

    .line 1236664
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "id"

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236665
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "to"

    iget-wide v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236666
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->tracking:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1236667
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "tracking"

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->tracking:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236668
    :cond_0
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1236669
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "name"

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->name:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236670
    :cond_1
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->caption:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1236671
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "caption"

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->caption:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236672
    :cond_2
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->description:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1236673
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "description"

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->description:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236674
    :cond_3
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->picture:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1236675
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "picture"

    iget-object v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->picture:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236676
    :cond_4
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isThrowbackPost:Z

    if-eqz v0, :cond_5

    .line 1236677
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "is_throwback_post"

    iget-boolean v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isThrowbackPost:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236678
    :cond_5
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->reshareOriginalPost:Z

    if-eqz v0, :cond_6

    .line 1236679
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "reshare_original_post"

    iget-boolean v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->reshareOriginalPost:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236680
    :cond_6
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isMemeShare:Z

    if-eqz v0, :cond_7

    .line 1236681
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "is_meme_share"

    iget-boolean v2, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isMemeShare:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236682
    :cond_7
    new-instance v0, LX/14N;

    const-string v1, "graphObjectShare"

    const-string v2, "POST"

    const-string v3, "sharedposts"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 1236683
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1236684
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
