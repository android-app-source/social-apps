.class public final LX/7QZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/7Qb;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/7Qb;)V
    .locals 1

    .prologue
    .line 1204562
    iput-object p1, p0, LX/7QZ;->a:LX/7Qb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204563
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7QZ;->b:Ljava/util/Map;

    .line 1204564
    new-instance v0, Lcom/facebook/video/videohome/liveupdates/BroadcastStatusUpdateManager$PollingQueue$1;

    invoke-direct {v0, p0}, Lcom/facebook/video/videohome/liveupdates/BroadcastStatusUpdateManager$PollingQueue$1;-><init>(LX/7QZ;)V

    iput-object v0, p0, LX/7QZ;->c:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 1204552
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7QZ;->a:LX/7Qb;

    invoke-static {v0}, LX/7Qb;->h(LX/7Qb;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1204553
    iget-object v0, p0, LX/7QZ;->a:LX/7Qb;

    iget-object v0, v0, LX/7Qb;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "BroadcastStatusUpdateManager.pollNow()"

    const-string v2, "App is in bg"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1204554
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1204555
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/7QZ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1204556
    iget-object v0, p0, LX/7QZ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 1204557
    iget-object v0, p0, LX/7QZ;->a:LX/7Qb;

    iget-object v0, v0, LX/7Qb;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x3f7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1204558
    iget-object v0, p0, LX/7QZ;->a:LX/7Qb;

    iget-object v0, v0, LX/7Qb;->b:LX/7Qe;

    iget-object v1, p0, LX/7QZ;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7Qe;->a(Ljava/util/Collection;)V

    .line 1204559
    :goto_1
    iget-object v0, p0, LX/7QZ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1204560
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1204561
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/7QZ;->a:LX/7Qb;

    iget-object v0, v0, LX/7Qb;->b:LX/7Qe;

    iget-object v1, p0, LX/7QZ;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7Qe;->a(Ljava/util/Set;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized a(LX/7Qa;)V
    .locals 3

    .prologue
    .line 1204549
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7QZ;->b:Ljava/util/Map;

    iget-object v1, p1, LX/7Qa;->c:Ljava/lang/String;

    iget-object v2, p1, LX/7Qa;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1204550
    monitor-exit p0

    return-void

    .line 1204551
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1204565
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7QZ;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1204566
    monitor-exit p0

    return-void

    .line 1204567
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/7Qa;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1204545
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Qa;

    .line 1204546
    invoke-virtual {p0, v0}, LX/7QZ;->a(LX/7Qa;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1204547
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1204548
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1204532
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7QZ;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 5

    .prologue
    .line 1204541
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7QZ;->a:LX/7Qb;

    iget-object v0, v0, LX/7Qb;->j:Landroid/os/Handler;

    iget-object v1, p0, LX/7QZ;->c:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1204542
    iget-object v0, p0, LX/7QZ;->a:LX/7Qb;

    iget-object v0, v0, LX/7Qb;->j:Landroid/os/Handler;

    iget-object v1, p0, LX/7QZ;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    const v4, 0x796645d0

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1204543
    monitor-exit p0

    return-void

    .line 1204544
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1204533
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1204534
    iget-object v0, p0, LX/7QZ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1204535
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1204536
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1204537
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1204538
    :cond_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1204539
    iget-object v2, p0, LX/7QZ;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1204540
    :cond_2
    monitor-exit p0

    return-void
.end method
