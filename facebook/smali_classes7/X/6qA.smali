.class public LX/6qA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0dC;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0dC;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1150089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150090
    iput-object p1, p0, LX/6qA;->a:LX/0dC;

    .line 1150091
    iput-object p2, p0, LX/6qA;->b:LX/0Or;

    .line 1150092
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1150093
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "device_id"

    iget-object v2, p0, LX/6qA;->a:LX/0dC;

    invoke-virtual {v2}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1150094
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "disable_fingerprint_nonce_method"

    .line 1150095
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1150096
    move-object v1, v1

    .line 1150097
    const-string v2, "POST"

    .line 1150098
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1150099
    move-object v1, v1

    .line 1150100
    const-string v2, "%s/%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/6qA;->b:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "p2p_disable_touch_id_nonces"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1150101
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1150102
    move-object v1, v1

    .line 1150103
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1150104
    move-object v0, v1

    .line 1150105
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1150106
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1150107
    move-object v0, v0

    .line 1150108
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1150109
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1150110
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1150111
    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const-string v2, "Expected response in the form of {\"success\": true} but was %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1150112
    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
