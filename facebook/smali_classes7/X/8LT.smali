.class public LX/8LT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/upload/annotation/VideoUploadForceRawTranscodingBitrateEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1332578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1332579
    iput-object p1, p0, LX/8LT;->a:LX/0ad;

    .line 1332580
    iput-object p2, p0, LX/8LT;->b:LX/0Or;

    .line 1332581
    return-void
.end method

.method public static a(Landroid/os/Bundle;)I
    .locals 1
    .param p0    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1332582
    invoke-static {p0}, LX/8LT;->b(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1332583
    const/16 v0, 0x500

    .line 1332584
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/8LT;
    .locals 1

    .prologue
    .line 1332585
    invoke-static {p0}, LX/8LT;->b(LX/0QB;)LX/8LT;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8LT;
    .locals 3

    .prologue
    .line 1332586
    new-instance v1, LX/8LT;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    const/16 v2, 0x154e

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/8LT;-><init>(LX/0ad;LX/0Or;)V

    .line 1332587
    return-object v1
.end method

.method private static b(Landroid/os/Bundle;)Z
    .locals 2
    .param p0    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1332588
    if-eqz p0, :cond_0

    const-string v0, "raw"

    const-string v1, "video_upload_quality"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1332589
    iget-object v0, p0, LX/8LT;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1332590
    const/4 v0, -0x2

    .line 1332591
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ipc/media/MediaItem;Landroid/os/Bundle;)I
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1332592
    invoke-static {p2}, LX/8LT;->b(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1332593
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1332594
    iget v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v0, v1

    .line 1332595
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1332596
    iget v2, v1, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v1, v2

    .line 1332597
    invoke-static {v0, v1}, LX/7St;->b(II)I

    move-result v0

    .line 1332598
    :goto_0
    return v0

    .line 1332599
    :cond_0
    if-nez p2, :cond_2

    .line 1332600
    const/4 v0, 0x0

    .line 1332601
    :goto_1
    move v0, v0

    .line 1332602
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8LT;->a:LX/0ad;

    sget-short v1, LX/8tG;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1332603
    iget-object v0, p0, LX/8LT;->a:LX/0ad;

    sget v1, LX/8tG;->a:I

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0

    .line 1332604
    :cond_1
    invoke-virtual {p0}, LX/8LT;->a()I

    move-result v0

    goto :goto_0

    .line 1332605
    :cond_2
    const-class v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1332606
    const-string v0, "video_creative_editing_metadata"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1332607
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1332608
    if-nez v0, :cond_4

    .line 1332609
    :cond_3
    :goto_2
    move v0, v1

    .line 1332610
    goto :goto_1

    .line 1332611
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object p2

    if-eqz p2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object p2

    iget-boolean p2, p2, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->isTrimSpecified:Z

    if-eqz p2, :cond_5

    move v1, v2

    .line 1332612
    goto :goto_2

    .line 1332613
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getRotationAngle()I

    move-result p2

    if-eqz p2, :cond_6

    move v1, v2

    .line 1332614
    goto :goto_2

    .line 1332615
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getCropRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object p2

    if-eqz p2, :cond_7

    move v1, v2

    .line 1332616
    goto :goto_2

    .line 1332617
    :cond_7
    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->isVideoMuted()Z

    move-result p2

    if-eqz p2, :cond_8

    move v1, v2

    .line 1332618
    goto :goto_2

    .line 1332619
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayUri()Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_9

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayId()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_a

    :cond_9
    move v1, v2

    .line 1332620
    goto :goto_2

    .line 1332621
    :cond_a
    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getMsqrdMaskId()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_b

    move v1, v2

    .line 1332622
    goto :goto_2

    .line 1332623
    :cond_b
    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->shouldFlipHorizontally()Z

    move-result p2

    if-eqz p2, :cond_c

    move v1, v2

    .line 1332624
    goto :goto_2

    .line 1332625
    :cond_c
    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getGLRendererConfigs()LX/0Px;

    move-result-object p2

    if-eqz p2, :cond_3

    move v1, v2

    .line 1332626
    goto :goto_2
.end method
