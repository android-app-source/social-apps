.class public final LX/6gz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1126960
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 1126961
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1126962
    :goto_0
    return v1

    .line 1126963
    :cond_0
    const-string v10, "optical_flow_disabled"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1126964
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    .line 1126965
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_7

    .line 1126966
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1126967
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1126968
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 1126969
    const-string v10, "emitters"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1126970
    invoke-static {p0, p1}, LX/6gw;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1126971
    :cond_2
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1126972
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1126973
    :cond_3
    const-string v10, "instructions"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1126974
    invoke-static {p0, p1}, LX/6gx;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1126975
    :cond_4
    const-string v10, "name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1126976
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1126977
    :cond_5
    const-string v10, "thumbnail_image"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1126978
    invoke-static {p0, p1}, LX/6gy;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1126979
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1126980
    :cond_7
    const/4 v9, 0x6

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1126981
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1126982
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 1126983
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1126984
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1126985
    if-eqz v0, :cond_8

    .line 1126986
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 1126987
    :cond_8
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1126988
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1126933
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1126934
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1126935
    if-eqz v0, :cond_0

    .line 1126936
    const-string v1, "emitters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126937
    invoke-static {p0, v0, p2, p3}, LX/6gw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1126938
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1126939
    if-eqz v0, :cond_1

    .line 1126940
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126941
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1126942
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1126943
    if-eqz v0, :cond_2

    .line 1126944
    const-string v1, "instructions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126945
    invoke-static {p0, v0, p2}, LX/6gx;->a(LX/15i;ILX/0nX;)V

    .line 1126946
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1126947
    if-eqz v0, :cond_3

    .line 1126948
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126949
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1126950
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1126951
    if-eqz v0, :cond_4

    .line 1126952
    const-string v1, "optical_flow_disabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126953
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1126954
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1126955
    if-eqz v0, :cond_5

    .line 1126956
    const-string v1, "thumbnail_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126957
    invoke-static {p0, v0, p2}, LX/6gy;->a(LX/15i;ILX/0nX;)V

    .line 1126958
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1126959
    return-void
.end method
