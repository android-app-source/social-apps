.class public LX/7TI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/io/File;

.field public b:Ljava/io/File;

.field public c:LX/2Md;

.field public d:Landroid/graphics/RectF;

.field public e:LX/7Sv;

.field public f:I

.field public g:I

.field public h:LX/60y;

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:LX/7Sy;

.field public m:I

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/61B;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1210304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1210305
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, LX/7TI;->d:Landroid/graphics/RectF;

    .line 1210306
    sget-object v0, LX/7Sv;->NONE:LX/7Sv;

    iput-object v0, p0, LX/7TI;->e:LX/7Sv;

    .line 1210307
    const/4 v0, -0x1

    iput v0, p0, LX/7TI;->f:I

    .line 1210308
    const/4 v0, -0x2

    iput v0, p0, LX/7TI;->g:I

    .line 1210309
    iput-boolean v1, p0, LX/7TI;->i:Z

    .line 1210310
    iput-boolean v1, p0, LX/7TI;->j:Z

    .line 1210311
    iput-boolean v1, p0, LX/7TI;->k:Z

    .line 1210312
    iput-object v4, p0, LX/7TI;->l:LX/7Sy;

    .line 1210313
    iput v1, p0, LX/7TI;->m:I

    .line 1210314
    iput-object v4, p0, LX/7TI;->n:LX/0Px;

    .line 1210315
    return-void
.end method


# virtual methods
.method public final c(I)LX/7TI;
    .locals 2

    .prologue
    .line 1210316
    if-eqz p1, :cond_0

    const/16 v0, 0x5a

    if-eq p1, v0, :cond_0

    const/16 v0, 0xb4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x10e

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "OutputRotationDegreesClockwise Must be one of 0, 90, 180, 270"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1210317
    iput p1, p0, LX/7TI;->m:I

    .line 1210318
    return-object p0

    .line 1210319
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()LX/7TH;
    .locals 1

    .prologue
    .line 1210320
    new-instance v0, LX/7TH;

    invoke-direct {v0, p0}, LX/7TH;-><init>(LX/7TI;)V

    return-object v0
.end method
