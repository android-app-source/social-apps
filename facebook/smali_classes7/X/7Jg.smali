.class public LX/7Jg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/net/Uri;

.field public c:Landroid/net/Uri;

.field public d:J

.field public e:J

.field public f:J

.field public g:J

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:LX/1A0;

.field public m:J

.field public n:LX/2ft;

.field public o:Z

.field public p:J

.field public q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1194021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194022
    sget-object v0, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    iput-object v0, p0, LX/7Jg;->l:LX/1A0;

    .line 1194023
    sget-object v0, LX/2ft;->NONE:LX/2ft;

    iput-object v0, p0, LX/7Jg;->n:LX/2ft;

    .line 1194024
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLX/2ft;Landroid/net/Uri;Z)V
    .locals 2

    .prologue
    .line 1194010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194011
    iput-object p1, p0, LX/7Jg;->a:Ljava/lang/String;

    .line 1194012
    iput-wide p2, p0, LX/7Jg;->d:J

    .line 1194013
    iput-object p4, p0, LX/7Jg;->n:LX/2ft;

    .line 1194014
    const-string v0, "remote-uri"

    invoke-virtual {p5, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1194015
    if-eqz v0, :cond_0

    .line 1194016
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/7Jg;->b:Landroid/net/Uri;

    .line 1194017
    :goto_0
    sget-object v0, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    iput-object v0, p0, LX/7Jg;->l:LX/1A0;

    .line 1194018
    iput-boolean p6, p0, LX/7Jg;->o:Z

    .line 1194019
    return-void

    .line 1194020
    :cond_0
    iput-object p5, p0, LX/7Jg;->b:Landroid/net/Uri;

    goto :goto_0
.end method
