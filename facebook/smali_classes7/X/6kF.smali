.class public LX/6kF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final threadKeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6l9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1135511
    new-instance v0, LX/1sv;

    const-string v1, "DeltaPinnedGroups"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kF;->b:LX/1sv;

    .line 1135512
    new-instance v0, LX/1sw;

    const-string v1, "threadKeys"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kF;->c:LX/1sw;

    .line 1135513
    sput-boolean v3, LX/6kF;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6l9;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1135514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1135515
    iput-object p1, p0, LX/6kF;->threadKeys:Ljava/util/List;

    .line 1135516
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1135517
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1135518
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1135519
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1135520
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaPinnedGroups"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1135521
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135522
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135523
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135524
    iget-object v4, p0, LX/6kF;->threadKeys:Ljava/util/List;

    if-eqz v4, :cond_0

    .line 1135525
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135526
    const-string v4, "threadKeys"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135527
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135528
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135529
    iget-object v0, p0, LX/6kF;->threadKeys:Ljava/util/List;

    if-nez v0, :cond_4

    .line 1135530
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135531
    :cond_0
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135532
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135533
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1135534
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 1135535
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 1135536
    :cond_3
    const-string v0, ""

    goto :goto_2

    .line 1135537
    :cond_4
    iget-object v0, p0, LX/6kF;->threadKeys:Ljava/util/List;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1135538
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1135539
    iget-object v0, p0, LX/6kF;->threadKeys:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1135540
    iget-object v0, p0, LX/6kF;->threadKeys:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1135541
    sget-object v0, LX/6kF;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135542
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/6kF;->threadKeys:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1135543
    iget-object v0, p0, LX/6kF;->threadKeys:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6l9;

    .line 1135544
    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    goto :goto_0

    .line 1135545
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1135546
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1135547
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1135548
    if-nez p1, :cond_1

    .line 1135549
    :cond_0
    :goto_0
    return v0

    .line 1135550
    :cond_1
    instance-of v1, p1, LX/6kF;

    if-eqz v1, :cond_0

    .line 1135551
    check-cast p1, LX/6kF;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1135552
    if-nez p1, :cond_3

    .line 1135553
    :cond_2
    :goto_1
    move v0, v2

    .line 1135554
    goto :goto_0

    .line 1135555
    :cond_3
    iget-object v0, p0, LX/6kF;->threadKeys:Ljava/util/List;

    if-eqz v0, :cond_6

    move v0, v1

    .line 1135556
    :goto_2
    iget-object v3, p1, LX/6kF;->threadKeys:Ljava/util/List;

    if-eqz v3, :cond_7

    move v3, v1

    .line 1135557
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1135558
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135559
    iget-object v0, p0, LX/6kF;->threadKeys:Ljava/util/List;

    iget-object v3, p1, LX/6kF;->threadKeys:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 1135560
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1135561
    goto :goto_2

    :cond_7
    move v3, v2

    .line 1135562
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1135563
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1135564
    sget-boolean v0, LX/6kF;->a:Z

    .line 1135565
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kF;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1135566
    return-object v0
.end method
