.class public final LX/7it;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/commerce/core/intent/MerchantInfoViewData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1228810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1228808
    new-instance v0, Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    invoke-direct {v0, p1}, Lcom/facebook/commerce/core/intent/MerchantInfoViewData;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1228809
    new-array v0, p1, [Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    return-object v0
.end method
