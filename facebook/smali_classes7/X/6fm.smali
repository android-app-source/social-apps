.class public final LX/6fm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadJoinRequest;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1120407
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120408
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1120409
    iput-object v0, p0, LX/6fm;->e:LX/0Px;

    .line 1120410
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)LX/6fm;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadJoinRequest;",
            ">;)",
            "LX/6fm;"
        }
    .end annotation

    .prologue
    .line 1120397
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/6fm;->e:LX/0Px;

    .line 1120398
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/RoomThreadData;)LX/6fm;
    .locals 1

    .prologue
    .line 1120400
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/RoomThreadData;->a:Landroid/net/Uri;

    iput-object v0, p0, LX/6fm;->a:Landroid/net/Uri;

    .line 1120401
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/RoomThreadData;->b:Z

    iput-boolean v0, p0, LX/6fm;->b:Z

    .line 1120402
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/RoomThreadData;->c:Z

    iput-boolean v0, p0, LX/6fm;->c:Z

    .line 1120403
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/RoomThreadData;->d:Z

    iput-boolean v0, p0, LX/6fm;->d:Z

    .line 1120404
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/RoomThreadData;->e:LX/0Px;

    iput-object v0, p0, LX/6fm;->e:LX/0Px;

    .line 1120405
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/RoomThreadData;->f:Ljava/lang/String;

    iput-object v0, p0, LX/6fm;->f:Ljava/lang/String;

    .line 1120406
    return-object p0
.end method

.method public final a()Lcom/facebook/messaging/model/threads/RoomThreadData;
    .locals 2

    .prologue
    .line 1120399
    new-instance v0, Lcom/facebook/messaging/model/threads/RoomThreadData;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/threads/RoomThreadData;-><init>(LX/6fm;)V

    return-object v0
.end method
