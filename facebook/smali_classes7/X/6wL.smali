.class public LX/6wL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wK",
        "<",
        "LX/6va;",
        "Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1157554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157555
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;)LX/0Px;
    .locals 3

    .prologue
    .line 1157556
    check-cast p1, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;

    .line 1157557
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1157558
    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    iget-object v0, v0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->b:LX/0Rf;

    .line 1157559
    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v2

    .line 1157560
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1157561
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6vb;

    invoke-virtual {v0}, LX/6vb;->getSectionType()LX/6va;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1157562
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1157563
    sget-object v0, LX/6va;->DOUBLE_ROW_DIVIDER:LX/6va;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1157564
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6vb;

    invoke-virtual {v0}, LX/6vb;->getSectionType()LX/6va;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1157565
    :cond_1
    sget-object v0, LX/6va;->SINGLE_ROW_DIVIDER:LX/6va;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1157566
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
