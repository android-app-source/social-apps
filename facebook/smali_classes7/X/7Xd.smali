.class public final LX/7Xd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/zero/service/ZeroHeaderRequestManager;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/service/ZeroHeaderRequestManager;)V
    .locals 0

    .prologue
    .line 1218561
    iput-object p1, p0, LX/7Xd;->a:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1218563
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_0

    .line 1218564
    :goto_0
    return-void

    .line 1218565
    :cond_0
    iget-object v0, p0, LX/7Xd;->a:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    .line 1218566
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 1218567
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "zero_header_request_params_fetch_failed"

    invoke-direct {v1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "zero_module"

    .line 1218568
    iput-object p0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1218569
    move-object p0, v1

    .line 1218570
    const-string v1, "exception_message"

    invoke-virtual {p0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1218571
    iget-object v1, v0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-interface {v1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1218572
    goto :goto_0
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1218562
    return-void
.end method
