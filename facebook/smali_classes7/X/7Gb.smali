.class public abstract LX/7Gb;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/7G8;

.field public final c:LX/2Sx;

.field private final d:LX/7GB;

.field private final e:LX/7G3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1189585
    const-class v0, LX/7Gb;

    sput-object v0, LX/7Gb;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/7G8;LX/2Sx;LX/7GB;LX/7G3;)V
    .locals 0

    .prologue
    .line 1189586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189587
    iput-object p1, p0, LX/7Gb;->b:LX/7G8;

    .line 1189588
    iput-object p2, p0, LX/7Gb;->c:LX/2Sx;

    .line 1189589
    iput-object p3, p0, LX/7Gb;->d:LX/7GB;

    .line 1189590
    iput-object p4, p0, LX/7Gb;->e:LX/7G3;

    .line 1189591
    return-void
.end method

.method public static a(LX/7Gb;)LX/7GT;
    .locals 1

    .prologue
    .line 1189592
    iget-object v0, p0, LX/7Gb;->b:LX/7G8;

    invoke-virtual {v0}, LX/7G8;->a()LX/7GT;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;LX/7Fx;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/Exception;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/7Fx",
            "<**>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/lang/Exception;",
            ")",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1189593
    invoke-static {p4}, LX/7Gb;->a(Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1189594
    invoke-static {p4}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v0

    invoke-static {v0, p4}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 1189595
    :goto_0
    return-object v0

    .line 1189596
    :cond_0
    iget-object v0, p0, LX/7Gb;->e:LX/7G3;

    invoke-static {p0}, LX/7Gb;->a(LX/7Gb;)LX/7GT;

    move-result-object v1

    .line 1189597
    iget-object v5, v0, LX/7G3;->b:LX/6Po;

    sget-object v6, LX/7GY;->b:LX/6Pr;

    const-string v7, "Uncaught sync exception on %s queue!!! - %s"

    iget-object v8, v1, LX/7GT;->apiString:Ljava/lang/String;

    invoke-static {v7, v8, p4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 1189598
    iget-object v5, v0, LX/7G3;->a:LX/03V;

    const-string v6, "sync_delta_handling"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Exception in delta handling. queue_type = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v1, LX/7GT;->apiString:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7, p4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1189599
    iget-object v0, p0, LX/7Gb;->d:LX/7GB;

    invoke-virtual {v0, p2}, LX/7GB;->b(LX/7Fx;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1189600
    new-array v0, v4, [Ljava/lang/Object;

    invoke-static {p0}, LX/7Gb;->a(LX/7Gb;)LX/7GT;

    move-result-object v1

    iget-object v1, v1, LX/7GT;->apiString:Ljava/lang/String;

    aput-object v1, v0, v2

    aput-object p1, v0, v3

    .line 1189601
    sget-object v0, Lcom/facebook/sync/analytics/FullRefreshReason;->d:Lcom/facebook/sync/analytics/FullRefreshReason;

    invoke-static {p2, v0}, LX/7GB;->a(LX/7Fx;Lcom/facebook/sync/analytics/FullRefreshReason;)V

    .line 1189602
    invoke-interface {p2}, LX/7Fx;->a()J

    move-result-wide v5

    .line 1189603
    const-wide/16 v7, -0x1

    cmp-long v7, v5, v7

    if-eqz v7, :cond_1

    .line 1189604
    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    invoke-interface {p2, v5, v6}, LX/7Fx;->a(J)V

    .line 1189605
    :cond_1
    invoke-static {p4}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v0

    invoke-static {v0, p4}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 1189606
    :cond_2
    new-array v0, v4, [Ljava/lang/Object;

    invoke-static {p0}, LX/7Gb;->a(LX/7Gb;)LX/7GT;

    move-result-object v1

    iget-object v1, v1, LX/7GT;->apiString:Ljava/lang/String;

    aput-object v1, v0, v2

    aput-object p1, v0, v3

    .line 1189607
    iget-object v0, p0, LX/7Gb;->b:LX/7G8;

    .line 1189608
    new-instance v1, Lcom/facebook/sync/analytics/FullRefreshReason;

    sget-object v2, LX/7Fz;->UNCAUGHT_EXCEPTION:LX/7Fz;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "exception = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/sync/analytics/FullRefreshReason;-><init>(LX/7Fz;Ljava/lang/String;)V

    move-object v1, v1

    .line 1189609
    invoke-virtual {v0, v1, p3}, LX/7G8;->a(Lcom/facebook/sync/analytics/FullRefreshReason;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/Exception;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1189610
    invoke-static {p0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1189611
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1189612
    :goto_0
    if-eqz p0, :cond_0

    .line 1189613
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1189614
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_0

    .line 1189615
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    move-object v0, v0

    .line 1189616
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 1189617
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    instance-of v3, v3, LX/2Oo;

    if-eqz v3, :cond_1

    .line 1189618
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    .line 1189619
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v2

    const/16 v3, 0xbe

    if-ne v2, v3, :cond_2

    .line 1189620
    iget v2, v0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorSubCode:I

    move v0, v2

    .line 1189621
    const/16 v2, 0x1eb

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    .line 1189622
    :goto_1
    return v0

    :cond_2
    move v0, v1

    .line 1189623
    goto :goto_1

    :cond_3
    move v0, v1

    .line 1189624
    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILX/7Fx;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/Exception;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "LX/7Fx",
            "<**>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/lang/Exception;",
            ")",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    .line 1189625
    instance-of v0, p5, LX/7GZ;

    if-eqz v0, :cond_0

    move-object v5, p5

    .line 1189626
    check-cast v5, LX/7GZ;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 1189627
    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-static {v0}, LX/7Gb;->a(LX/7Gb;)LX/7GT;

    move-result-object p2

    iget-object p2, p2, LX/7GT;->apiString:Ljava/lang/String;

    aput-object p2, p0, p1

    .line 1189628
    iget-object p0, v0, LX/7Gb;->c:LX/2Sx;

    invoke-static {v0}, LX/7Gb;->a(LX/7Gb;)LX/7GT;

    move-result-object p1

    invoke-static {v1, p1}, LX/7G9;->a(Ljava/lang/String;LX/7GT;)LX/7G9;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/2Sx;->c(LX/7G9;)V

    .line 1189629
    sget-object p0, Lcom/facebook/sync/analytics/FullRefreshReason;->e:Lcom/facebook/sync/analytics/FullRefreshReason;

    invoke-static {v3, p0}, LX/7GB;->a(LX/7Fx;Lcom/facebook/sync/analytics/FullRefreshReason;)V

    .line 1189630
    iget-object p0, v0, LX/7Gb;->b:LX/7G8;

    sget-object p1, LX/7G7;->ENSURE:LX/7G7;

    invoke-virtual {p0, v2, v3, p1, v4}, LX/7G8;->a(ILX/7Fx;LX/7G7;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object p0

    move-object v0, p0

    .line 1189631
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p3, p4, p5}, LX/7Gb;->a(Ljava/lang/String;LX/7Fx;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/Exception;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method
