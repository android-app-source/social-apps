.class public final LX/6lz;
.super LX/6ly;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6ly",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesInterfaces$XMAAttachmentStoryFields$Subattachments;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6lj;

.field private final b:LX/2eR;

.field public final c:LX/6lx;

.field private final d:[I

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesInterfaces$XMAAttachmentStoryFields$Subattachments;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/6lf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/6ll;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ja;LX/6lj;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1143123
    invoke-direct {p0, p2}, LX/6ly;-><init>(LX/0ja;)V

    .line 1143124
    iput-object p3, p0, LX/6lz;->a:LX/6lj;

    .line 1143125
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1143126
    iput-object v0, p0, LX/6lz;->e:LX/0Px;

    .line 1143127
    new-instance v0, LX/6lx;

    invoke-direct {v0, p1}, LX/6lx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/6lz;->c:LX/6lx;

    .line 1143128
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, LX/6lz;->d:[I

    .line 1143129
    new-instance v0, LX/6lw;

    invoke-direct {v0, p0}, LX/6lw;-><init>(LX/6lz;)V

    iput-object v0, p0, LX/6lz;->b:LX/2eR;

    .line 1143130
    return-void
.end method


# virtual methods
.method public final synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1143131
    invoke-virtual {p0, p1}, LX/6lz;->e(I)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;I)V
    .locals 10

    .prologue
    .line 1143132
    instance-of v0, p1, LX/6lx;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1143133
    check-cast p1, LX/6lx;

    .line 1143134
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0}, LX/6lx;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1143135
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    iget-object v1, p0, LX/6lz;->d:[I

    .line 1143136
    iget-object v2, p1, LX/6lx;->c:LX/4oI;

    if-nez v2, :cond_0

    .line 1143137
    invoke-virtual {p1}, LX/6lx;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1143138
    new-instance v3, LX/4oI;

    invoke-direct {v3}, LX/4oI;-><init>()V

    iput-object v3, p1, LX/6lx;->c:LX/4oI;

    .line 1143139
    iget-object v3, p1, LX/6lx;->c:LX/4oI;

    const v4, 0x7f0b07be

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1143140
    int-to-float v5, v4

    iget-object v6, v3, LX/4oI;->c:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v6

    cmpl-float v5, v5, v6

    if-nez v5, :cond_8

    .line 1143141
    :goto_0
    iget-object v3, p1, LX/6lx;->c:LX/4oI;

    const v4, 0x7f0a01bf

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1143142
    iget-object v4, v3, LX/4oI;->c:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getColor()I

    move-result v4

    if-ne v2, v4, :cond_9

    .line 1143143
    :goto_1
    iget-object v2, p1, LX/6lx;->c:LX/4oI;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/4oI;->a(I)V

    .line 1143144
    iget-object v2, p1, LX/6lx;->c:LX/4oI;

    invoke-virtual {p1, v2}, LX/6lx;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 1143145
    :cond_0
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1143146
    array-length v2, v1

    const/4 v3, 0x4

    if-ne v2, v3, :cond_5

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1143147
    if-nez p3, :cond_6

    .line 1143148
    const/4 v2, 0x0

    aget v5, v1, v2

    .line 1143149
    iget v4, p1, LX/6lx;->a:I

    .line 1143150
    iget v3, p1, LX/6lx;->a:I

    .line 1143151
    const/4 v2, 0x3

    aget v2, v1, v2

    .line 1143152
    :goto_3
    iget-object v6, p1, LX/6lx;->c:LX/4oI;

    int-to-float v7, v5

    int-to-float v8, v4

    int-to-float v9, v3

    int-to-float p2, v2

    invoke-virtual {v6, v7, v8, v9, p2}, LX/4oI;->a(FFFF)V

    .line 1143153
    const/16 v6, 0x8

    new-array v6, v6, [F

    const/4 v7, 0x0

    int-to-float v8, v5

    aput v8, v6, v7

    const/4 v7, 0x1

    int-to-float v5, v5

    aput v5, v6, v7

    const/4 v5, 0x2

    int-to-float v7, v4

    aput v7, v6, v5

    const/4 v5, 0x3

    int-to-float v4, v4

    aput v4, v6, v5

    const/4 v4, 0x4

    int-to-float v5, v3

    aput v5, v6, v4

    const/4 v4, 0x5

    int-to-float v3, v3

    aput v3, v6, v4

    const/4 v3, 0x6

    int-to-float v4, v2

    aput v4, v6, v3

    const/4 v3, 0x7

    int-to-float v2, v2

    aput v2, v6, v3

    iput-object v6, p1, LX/6lx;->d:[F

    .line 1143154
    invoke-virtual {p1}, LX/6lx;->getChildCount()I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/6lx;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 1143155
    :goto_4
    iget-object v0, p0, LX/6lz;->f:LX/6lf;

    if-eqz v0, :cond_2

    .line 1143156
    iget-object v0, p0, LX/6lz;->f:LX/6lf;

    instance-of v0, v0, LX/6lf;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1143157
    iget-object v0, p0, LX/6lz;->f:LX/6lf;

    check-cast v0, LX/6lf;

    .line 1143158
    if-nez v1, :cond_4

    .line 1143159
    invoke-virtual {v0, p1}, LX/6lf;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1143160
    iget-object v1, p0, LX/6lz;->g:LX/6ll;

    if-eqz v1, :cond_1

    instance-of v1, v2, LX/6lq;

    if-eqz v1, :cond_1

    move-object v1, v2

    .line 1143161
    check-cast v1, LX/6lq;

    iget-object v3, p0, LX/6lz;->g:LX/6ll;

    invoke-interface {v1, v3}, LX/6lq;->setXMACallback(LX/6ll;)V

    .line 1143162
    :cond_1
    invoke-virtual {p1, v2}, LX/6lx;->addView(Landroid/view/View;)V

    .line 1143163
    :goto_5
    invoke-virtual {p0, p3}, LX/6lz;->e(I)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/6lf;->a(Landroid/view/View;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;)V

    .line 1143164
    :cond_2
    return-void

    .line 1143165
    :cond_3
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_4

    :cond_4
    move-object v2, v1

    goto :goto_5

    .line 1143166
    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    .line 1143167
    :cond_6
    add-int/lit8 v2, v0, -0x1

    if-ne p3, v2, :cond_7

    .line 1143168
    iget v5, p1, LX/6lx;->a:I

    .line 1143169
    const/4 v2, 0x1

    aget v4, v1, v2

    .line 1143170
    const/4 v2, 0x2

    aget v3, v1, v2

    .line 1143171
    iget v2, p1, LX/6lx;->a:I

    goto :goto_3

    .line 1143172
    :cond_7
    iget v5, p1, LX/6lx;->a:I

    .line 1143173
    iget v4, p1, LX/6lx;->a:I

    .line 1143174
    iget v3, p1, LX/6lx;->a:I

    .line 1143175
    iget v2, p1, LX/6lx;->a:I

    goto/16 :goto_3

    .line 1143176
    :cond_8
    iget-object v5, v3, LX/4oI;->c:Landroid/graphics/Paint;

    int-to-float v6, v4

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1143177
    invoke-virtual {v3}, LX/4oI;->invalidateSelf()V

    goto/16 :goto_0

    .line 1143178
    :cond_9
    iget-object v4, v3, LX/4oI;->c:Landroid/graphics/Paint;

    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1143179
    invoke-virtual {v3}, LX/4oI;->invalidateSelf()V

    goto/16 :goto_1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1143180
    iget-object v0, p0, LX/6lz;->f:LX/6lf;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6lz;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/View;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1143181
    instance-of v0, p1, LX/6lx;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1143182
    check-cast p1, LX/6lx;

    .line 1143183
    iget-object v0, p0, LX/6lz;->f:LX/6lf;

    if-eqz v0, :cond_0

    .line 1143184
    invoke-virtual {p1}, LX/6lx;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/6lx;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1143185
    :goto_0
    if-eqz v0, :cond_0

    .line 1143186
    invoke-virtual {p1, v0}, LX/6lx;->removeView(Landroid/view/View;)V

    .line 1143187
    iget-object v1, p0, LX/6lz;->f:LX/6lf;

    invoke-virtual {v1, v0}, LX/6lf;->a(Landroid/view/View;)V

    .line 1143188
    :cond_0
    return-void

    .line 1143189
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()LX/2eR;
    .locals 1

    .prologue
    .line 1143190
    iget-object v0, p0, LX/6lz;->b:LX/2eR;

    return-object v0
.end method

.method public final e(I)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;
    .locals 1

    .prologue
    .line 1143191
    iget-object v0, p0, LX/6lz;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;

    return-object v0
.end method
