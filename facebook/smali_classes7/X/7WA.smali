.class public final LX/7WA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/64J;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Landroid/os/Bundle;

.field public final synthetic d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1215914
    iput-object p1, p0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iput-object p2, p0, LX/7WA;->a:Ljava/lang/String;

    iput-object p3, p0, LX/7WA;->b:Ljava/lang/String;

    iput-object p4, p0, LX/7WA;->c:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;)V
    .locals 3
    .param p1    # Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1215901
    iget-object v0, p0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->z:LX/64L;

    invoke-virtual {v0, p0}, LX/64L;->b(LX/64J;)V

    .line 1215902
    iget-object v0, p0, LX/7WA;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1215903
    iget-object v0, p0, LX/7WA;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0yi;->valueOf(Ljava/lang/String;)LX/0yi;

    move-result-object v0

    .line 1215904
    new-instance v1, LX/7W9;

    invoke-direct {v1, p0}, LX/7W9;-><init>(LX/7WA;)V

    .line 1215905
    iget-object v2, p0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v2, v2, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->x:LX/0yP;

    invoke-virtual {v2, v1}, LX/0yP;->a(LX/0yJ;)V

    .line 1215906
    iget-object v1, p0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v1, v1, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->x:LX/0yP;

    sget-object v2, LX/32P;->OPTIN:LX/32P;

    invoke-virtual {v1, v0, v2}, LX/0yP;->a(LX/0yi;LX/32P;)V

    .line 1215907
    :goto_0
    iget-object v0, p0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    .line 1215908
    invoke-static {v0, p1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->a$redex0(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;)V

    .line 1215909
    iget-object v0, p0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    invoke-static {v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->s(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;)V

    .line 1215910
    return-void

    .line 1215911
    :cond_0
    iget-object v0, p0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v1, p0, LX/7WA;->b:Ljava/lang/String;

    iget-object v2, p0, LX/7WA;->c:Landroid/os/Bundle;

    .line 1215912
    invoke-static {v0, v1, v2}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->a$redex0(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1215913
    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1215898
    iget-object v0, p0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->z:LX/64L;

    invoke-virtual {v0, p0}, LX/64L;->b(LX/64J;)V

    .line 1215899
    iget-object v0, p0, LX/7WA;->d:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    invoke-virtual {v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->finish()V

    .line 1215900
    return-void
.end method
