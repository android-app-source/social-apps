.class public final LX/7V3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:LX/7V4;


# direct methods
.method public constructor <init>(LX/7V4;)V
    .locals 0

    .prologue
    .line 1213984
    iput-object p1, p0, LX/7V3;->a:LX/7V4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 8

    .prologue
    .line 1213985
    iget-object v0, p0, LX/7V3;->a:LX/7V4;

    .line 1213986
    const/4 v1, 0x0

    .line 1213987
    iget-object v2, v0, LX/7V4;->h:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/7V4;->h:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isShown()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, v0, LX/7V4;->m:Z

    if-nez v2, :cond_3

    .line 1213988
    :cond_0
    :goto_0
    move v1, v1

    .line 1213989
    if-nez v1, :cond_2

    .line 1213990
    iget-object v1, v0, LX/7V4;->j:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/7V4;->j:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1213991
    :cond_1
    :goto_1
    return-void

    .line 1213992
    :cond_2
    goto :goto_1

    .line 1213993
    :cond_3
    invoke-static {v0}, LX/7V4;->d(LX/7V4;)[I

    move-result-object v2

    .line 1213994
    aget v2, v2, v1

    .line 1213995
    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 1213996
    :cond_4
    iget-object v1, v0, LX/7V4;->j:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_1
.end method
