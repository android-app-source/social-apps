.class public final LX/6sd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

.field public final b:LX/6xg;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/payments/currency/CurrencyAmount;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:LX/0m9;

.field public i:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

.field public j:Lcom/facebook/payments/currency/CurrencyAmount;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xg;)V
    .locals 1

    .prologue
    .line 1153569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153570
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6sd;->f:Ljava/lang/String;

    .line 1153571
    iput-object p1, p0, LX/6sd;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1153572
    iput-object p2, p0, LX/6sd;->b:LX/6xg;

    .line 1153573
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;
    .locals 2

    .prologue
    .line 1153574
    new-instance v0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;

    invoke-direct {v0, p0}, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;-><init>(LX/6sd;)V

    return-object v0
.end method
