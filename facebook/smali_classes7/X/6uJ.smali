.class public LX/6uJ;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/6E8;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6uc;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6uG;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/payments/confirmation/ConfirmationParams;

.field public d:LX/6qh;


# direct methods
.method public constructor <init>(LX/6uc;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1155723
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1155724
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1155725
    iput-object v0, p0, LX/6uJ;->b:LX/0Px;

    .line 1155726
    iput-object p1, p0, LX/6uJ;->a:LX/6uc;

    .line 1155727
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 1155716
    iget-object v0, p0, LX/6uJ;->a:LX/6uc;

    iget-object v1, p0, LX/6uJ;->c:Lcom/facebook/payments/confirmation/ConfirmationParams;

    invoke-interface {v1}, Lcom/facebook/payments/confirmation/ConfirmationParams;->a()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->a:LX/6uW;

    .line 1155717
    iget-object p0, v0, LX/6uc;->a:LX/0P1;

    invoke-virtual {p0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1155718
    :goto_0
    iget-object p0, v0, LX/6uc;->a:LX/0P1;

    invoke-virtual {p0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/6F1;

    iget-object p0, p0, LX/6F1;->e:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/6uU;

    move-object v0, p0

    .line 1155719
    invoke-static {}, LX/6uT;->values()[LX/6uT;

    move-result-object v1

    aget-object v1, v1, p2

    invoke-interface {v0, p1, v1}, LX/6uU;->a(Landroid/view/ViewGroup;LX/6uT;)LX/6E8;

    move-result-object v0

    return-object v0

    .line 1155720
    :cond_0
    sget-object v1, LX/6uW;->SIMPLE:LX/6uW;

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 1155728
    check-cast p1, LX/6E8;

    .line 1155729
    iget-object v0, p0, LX/6uJ;->d:LX/6qh;

    invoke-virtual {p1, v0}, LX/6E8;->a(LX/6qh;)V

    .line 1155730
    iget-object v0, p0, LX/6uJ;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6E2;

    invoke-virtual {p1, v0}, LX/6E8;->a(LX/6E2;)V

    .line 1155731
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1155722
    iget-object v0, p0, LX/6uJ;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6uG;

    invoke-interface {v0}, LX/6uG;->d()LX/6uT;

    move-result-object v0

    invoke-virtual {v0}, LX/6uT;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1155721
    iget-object v0, p0, LX/6uJ;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
