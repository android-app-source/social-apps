.class public LX/7kY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/7jB;

.field public final c:LX/7ka;

.field private final d:LX/1mR;


# direct methods
.method public constructor <init>(LX/0tX;LX/7jB;LX/7ka;LX/1mR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1232108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232109
    iput-object p1, p0, LX/7kY;->a:LX/0tX;

    .line 1232110
    iput-object p2, p0, LX/7kY;->b:LX/7jB;

    .line 1232111
    iput-object p3, p0, LX/7kY;->c:LX/7ka;

    .line 1232112
    iput-object p4, p0, LX/7kY;->d:LX/1mR;

    .line 1232113
    return-void
.end method

.method public static a(LX/0QB;)LX/7kY;
    .locals 5

    .prologue
    .line 1232114
    new-instance v4, LX/7kY;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/7jB;->a(LX/0QB;)LX/7jB;

    move-result-object v1

    check-cast v1, LX/7jB;

    invoke-static {p0}, LX/7ka;->a(LX/0QB;)LX/7ka;

    move-result-object v2

    check-cast v2, LX/7ka;

    invoke-static {p0}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v3

    check-cast v3, LX/1mR;

    invoke-direct {v4, v0, v1, v2, v3}, LX/7kY;-><init>(LX/0tX;LX/7jB;LX/7ka;LX/1mR;)V

    .line 1232115
    move-object v0, v4

    .line 1232116
    return-object v0
.end method

.method public static a$redex0(LX/7kY;)V
    .locals 2

    .prologue
    .line 1232117
    iget-object v0, p0, LX/7kY;->d:LX/1mR;

    const-string v1, "GraphQLCommerceRequestTag"

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1232118
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;)V
    .locals 5

    .prologue
    .line 1232119
    iget-object v0, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1232120
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1232121
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1232122
    invoke-static {p1}, LX/7jZ;->c(Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;)V

    .line 1232123
    new-instance v0, LX/4DX;

    invoke-direct {v0}, LX/4DX;-><init>()V

    .line 1232124
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1232125
    const-string v2, "actor_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232126
    move-object v0, v0

    .line 1232127
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1232128
    const-string v2, "page_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232129
    move-object v0, v0

    .line 1232130
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1232131
    if-eqz v1, :cond_0

    .line 1232132
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1232133
    const-string v2, "name"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232134
    :cond_0
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1232135
    if-eqz v1, :cond_1

    .line 1232136
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1232137
    const-string v2, "description"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232138
    :cond_1
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1232139
    if-eqz v1, :cond_2

    .line 1232140
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1232141
    const-string v2, "group_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232142
    :cond_2
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->g:Ljava/lang/Integer;

    move-object v1, v1

    .line 1232143
    if-eqz v1, :cond_3

    .line 1232144
    new-instance v1, LX/4IW;

    invoke-direct {v1}, LX/4IW;-><init>()V

    .line 1232145
    iget-object v2, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->g:Ljava/lang/Integer;

    move-object v2, v2

    .line 1232146
    invoke-virtual {v1, v2}, LX/4IW;->a(Ljava/lang/Integer;)LX/4IW;

    move-result-object v1

    .line 1232147
    iget-object v2, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1232148
    invoke-virtual {v1, v2}, LX/4IW;->a(Ljava/lang/String;)LX/4IW;

    move-result-object v1

    .line 1232149
    const-string v2, "product_item_price"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1232150
    :cond_3
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    move-object v1, v1

    .line 1232151
    if-eqz v1, :cond_4

    .line 1232152
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    move-object v1, v1

    .line 1232153
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1232154
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    move-object v1, v1

    .line 1232155
    const-string v2, "image_ids"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1232156
    :cond_4
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->j:LX/03R;

    move-object v1, v1

    .line 1232157
    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_e

    .line 1232158
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4DX;->a(Ljava/lang/Boolean;)LX/4DX;

    .line 1232159
    :cond_5
    :goto_0
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->k:LX/03R;

    move-object v1, v1

    .line 1232160
    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_f

    .line 1232161
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4DX;->b(Ljava/lang/Boolean;)LX/4DX;

    .line 1232162
    :cond_6
    :goto_1
    move-object v0, v0

    .line 1232163
    new-instance v1, LX/7jL;

    invoke-direct {v1}, LX/7jL;-><init>()V

    move-object v1, v1

    .line 1232164
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/7jL;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    move-object v0, v1

    .line 1232165
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->n:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v1, v1

    .line 1232166
    iput-object v1, v0, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1232167
    iget-object v1, p0, LX/7kY;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1232168
    new-instance v1, LX/7kW;

    invoke-direct {v1, p0, p1}, LX/7kW;-><init>(LX/7kY;Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1232169
    :goto_2
    return-void

    .line 1232170
    :cond_7
    invoke-static {p1}, LX/7jZ;->c(Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;)V

    .line 1232171
    new-instance v0, LX/4DY;

    invoke-direct {v0}, LX/4DY;-><init>()V

    .line 1232172
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1232173
    const-string v2, "id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232174
    move-object v0, v0

    .line 1232175
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1232176
    const-string v2, "actor_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232177
    move-object v0, v0

    .line 1232178
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1232179
    if-eqz v1, :cond_8

    .line 1232180
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1232181
    const-string v2, "name"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232182
    :cond_8
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1232183
    if-eqz v1, :cond_9

    .line 1232184
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1232185
    const-string v2, "description"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232186
    :cond_9
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1232187
    if-eqz v1, :cond_a

    .line 1232188
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1232189
    const-string v2, "group_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232190
    :cond_a
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->g:Ljava/lang/Integer;

    move-object v1, v1

    .line 1232191
    if-eqz v1, :cond_b

    .line 1232192
    new-instance v1, LX/4IW;

    invoke-direct {v1}, LX/4IW;-><init>()V

    .line 1232193
    iget-object v2, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->g:Ljava/lang/Integer;

    move-object v2, v2

    .line 1232194
    invoke-virtual {v1, v2}, LX/4IW;->a(Ljava/lang/Integer;)LX/4IW;

    move-result-object v1

    .line 1232195
    iget-object v2, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1232196
    invoke-virtual {v1, v2}, LX/4IW;->a(Ljava/lang/String;)LX/4IW;

    move-result-object v1

    .line 1232197
    const-string v2, "product_item_price"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1232198
    :cond_b
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    move-object v1, v1

    .line 1232199
    if-eqz v1, :cond_c

    .line 1232200
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    move-object v1, v1

    .line 1232201
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_c

    .line 1232202
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    move-object v1, v1

    .line 1232203
    const-string v2, "image_ids"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1232204
    :cond_c
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->j:LX/03R;

    move-object v1, v1

    .line 1232205
    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_10

    .line 1232206
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4DY;->a(Ljava/lang/Boolean;)LX/4DY;

    .line 1232207
    :cond_d
    :goto_3
    move-object v0, v0

    .line 1232208
    new-instance v1, LX/7jT;

    invoke-direct {v1}, LX/7jT;-><init>()V

    move-object v1, v1

    .line 1232209
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/7jT;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    move-object v0, v1

    .line 1232210
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->n:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v1, v1

    .line 1232211
    iput-object v1, v0, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1232212
    iget-object v1, p0, LX/7kY;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1232213
    new-instance v1, LX/7kX;

    invoke-direct {v1, p0, p1}, LX/7kX;-><init>(LX/7kY;Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1232214
    goto/16 :goto_2

    .line 1232215
    :cond_e
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->j:LX/03R;

    move-object v1, v1

    .line 1232216
    sget-object v2, LX/03R;->NO:LX/03R;

    if-ne v1, v2, :cond_5

    .line 1232217
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4DX;->a(Ljava/lang/Boolean;)LX/4DX;

    goto/16 :goto_0

    .line 1232218
    :cond_f
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->k:LX/03R;

    move-object v1, v1

    .line 1232219
    sget-object v2, LX/03R;->NO:LX/03R;

    if-ne v1, v2, :cond_6

    .line 1232220
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4DX;->b(Ljava/lang/Boolean;)LX/4DX;

    goto/16 :goto_1

    .line 1232221
    :cond_10
    iget-object v1, p1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->j:LX/03R;

    move-object v1, v1

    .line 1232222
    sget-object v2, LX/03R;->NO:LX/03R;

    if-ne v1, v2, :cond_d

    .line 1232223
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4DY;->a(Ljava/lang/Boolean;)LX/4DY;

    goto :goto_3
.end method
