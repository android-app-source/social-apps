.class public final enum LX/88p;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/88p;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/88p;

.field public static final enum BASIC_INFO:LX/88p;

.field public static final enum COMMUNITY:LX/88p;

.field public static final enum CONFIRM_MUTATION:LX/88p;

.field public static final enum CREATE_GENERAL_CHANNEL:LX/88p;

.field public static final enum INITIAL_FEED_NEWER:LX/88p;

.field public static final enum INITIAL_FEED_OLDER:LX/88p;

.field public static final enum TAIL:LX/88p;


# instance fields
.field private final mPriorityValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x3

    .line 1302961
    new-instance v0, LX/88p;

    const-string v1, "INITIAL_FEED_NEWER"

    invoke-direct {v0, v1, v4, v4}, LX/88p;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/88p;->INITIAL_FEED_NEWER:LX/88p;

    .line 1302962
    new-instance v0, LX/88p;

    const-string v1, "INITIAL_FEED_OLDER"

    invoke-direct {v0, v1, v5, v4}, LX/88p;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/88p;->INITIAL_FEED_OLDER:LX/88p;

    .line 1302963
    new-instance v0, LX/88p;

    const-string v1, "BASIC_INFO"

    invoke-direct {v0, v1, v6, v5}, LX/88p;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/88p;->BASIC_INFO:LX/88p;

    .line 1302964
    new-instance v0, LX/88p;

    const-string v1, "COMMUNITY"

    invoke-direct {v0, v1, v3, v6}, LX/88p;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/88p;->COMMUNITY:LX/88p;

    .line 1302965
    new-instance v0, LX/88p;

    const-string v1, "CREATE_GENERAL_CHANNEL"

    invoke-direct {v0, v1, v7, v3}, LX/88p;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/88p;->CREATE_GENERAL_CHANNEL:LX/88p;

    new-instance v0, LX/88p;

    const-string v1, "TAIL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/88p;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/88p;->TAIL:LX/88p;

    new-instance v0, LX/88p;

    const-string v1, "CONFIRM_MUTATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/88p;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/88p;->CONFIRM_MUTATION:LX/88p;

    .line 1302966
    const/4 v0, 0x7

    new-array v0, v0, [LX/88p;

    sget-object v1, LX/88p;->INITIAL_FEED_NEWER:LX/88p;

    aput-object v1, v0, v4

    sget-object v1, LX/88p;->INITIAL_FEED_OLDER:LX/88p;

    aput-object v1, v0, v5

    sget-object v1, LX/88p;->BASIC_INFO:LX/88p;

    aput-object v1, v0, v6

    sget-object v1, LX/88p;->COMMUNITY:LX/88p;

    aput-object v1, v0, v3

    sget-object v1, LX/88p;->CREATE_GENERAL_CHANNEL:LX/88p;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/88p;->TAIL:LX/88p;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/88p;->CONFIRM_MUTATION:LX/88p;

    aput-object v2, v0, v1

    sput-object v0, LX/88p;->$VALUES:[LX/88p;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1302967
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1302968
    iput p3, p0, LX/88p;->mPriorityValue:I

    .line 1302969
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/88p;
    .locals 1

    .prologue
    .line 1302970
    const-class v0, LX/88p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/88p;

    return-object v0
.end method

.method public static values()[LX/88p;
    .locals 1

    .prologue
    .line 1302971
    sget-object v0, LX/88p;->$VALUES:[LX/88p;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/88p;

    return-object v0
.end method


# virtual methods
.method public final getPriorityValue()I
    .locals 1

    .prologue
    .line 1302972
    iget v0, p0, LX/88p;->mPriorityValue:I

    return v0
.end method
