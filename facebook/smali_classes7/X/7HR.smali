.class public final LX/7HR;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/7Hc",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7B6;

.field public final synthetic b:LX/7HT;


# direct methods
.method public constructor <init>(LX/7HT;LX/7B6;)V
    .locals 0

    .prologue
    .line 1190707
    iput-object p1, p0, LX/7HR;->b:LX/7HT;

    iput-object p2, p0, LX/7HR;->a:LX/7B6;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 2

    .prologue
    .line 1190704
    iget-object v0, p0, LX/7HR;->b:LX/7HT;

    iget-object v1, p0, LX/7HR;->a:LX/7B6;

    invoke-virtual {v0, v1, p1}, LX/7HT;->a(LX/7B6;Ljava/util/concurrent/CancellationException;)V

    .line 1190705
    iget-object v0, p0, LX/7HR;->b:LX/7HT;

    invoke-static {v0}, LX/7HT;->f(LX/7HT;)V

    .line 1190706
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1190701
    iget-object v0, p0, LX/7HR;->b:LX/7HT;

    iget-object v1, p0, LX/7HR;->a:LX/7B6;

    invoke-virtual {v0, v1, p1}, LX/7HT;->a(LX/7B6;Ljava/lang/Throwable;)V

    .line 1190702
    iget-object v0, p0, LX/7HR;->b:LX/7HT;

    sget-object v1, LX/7HZ;->ERROR:LX/7HZ;

    invoke-virtual {v0, v1}, LX/7HT;->a(LX/7HZ;)V

    .line 1190703
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1190695
    check-cast p1, LX/7Hc;

    .line 1190696
    iget-object v0, p0, LX/7HR;->b:LX/7HT;

    invoke-virtual {v0}, LX/7HT;->b()LX/7HY;

    move-result-object v0

    .line 1190697
    invoke-virtual {v0}, LX/7HY;->isRemote()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1190698
    sget-object v0, LX/7HY;->REMOTE:LX/7HY;

    .line 1190699
    :cond_0
    iget-object v1, p0, LX/7HR;->b:LX/7HT;

    new-instance v2, LX/7Hi;

    iget-object v3, p0, LX/7HR;->a:LX/7B6;

    sget-object v4, LX/7Ha;->EXACT:LX/7Ha;

    invoke-direct {v2, v3, p1, v0, v4}, LX/7Hi;-><init>(LX/7B6;LX/7Hc;LX/7HY;LX/7Ha;)V

    invoke-virtual {v1, v2}, LX/7HT;->a(LX/7Hi;)V

    .line 1190700
    return-void
.end method
