.class public final LX/8QV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

.field public b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1343197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1343198
    iput-object v1, p0, LX/8QV;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1343199
    iput-object v1, p0, LX/8QV;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1343200
    iput-boolean v0, p0, LX/8QV;->c:Z

    .line 1343201
    iput-boolean v0, p0, LX/8QV;->d:Z

    .line 1343202
    return-void
.end method

.method public constructor <init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1343203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1343204
    iput-object v1, p0, LX/8QV;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1343205
    iput-object v1, p0, LX/8QV;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1343206
    iput-boolean v0, p0, LX/8QV;->c:Z

    .line 1343207
    iput-boolean v0, p0, LX/8QV;->d:Z

    .line 1343208
    if-nez p1, :cond_0

    .line 1343209
    :goto_0
    return-void

    .line 1343210
    :cond_0
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iput-object v0, p0, LX/8QV;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1343211
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, LX/8QV;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1343212
    iget-boolean v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    iput-boolean v0, p0, LX/8QV;->c:Z

    .line 1343213
    iget-boolean v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->c:Z

    iput-boolean v0, p0, LX/8QV;->d:Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;
    .locals 1

    .prologue
    .line 1343214
    iput-object p1, p0, LX/8QV;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1343215
    iget-object v0, p0, LX/8QV;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8QV;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1343216
    const-string v0, ""

    .line 1343217
    iget-object p1, p0, LX/8QV;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-nez p1, :cond_1

    new-instance p1, LX/4YH;

    invoke-direct {p1}, LX/4YH;-><init>()V

    .line 1343218
    :goto_0
    iput-object v0, p1, LX/4YH;->h:Ljava/lang/String;

    .line 1343219
    move-object p1, p1

    .line 1343220
    invoke-virtual {p1}, LX/4YH;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object p1

    iput-object p1, p0, LX/8QV;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1343221
    :cond_0
    return-object p0

    .line 1343222
    :cond_1
    iget-object p1, p0, LX/8QV;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1}, LX/4YH;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/4YH;

    move-result-object p1

    goto :goto_0
.end method

.method public final b()Lcom/facebook/privacy/model/SelectablePrivacyData;
    .locals 2

    .prologue
    .line 1343223
    new-instance v0, Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, p0}, Lcom/facebook/privacy/model/SelectablePrivacyData;-><init>(LX/8QV;)V

    return-object v0
.end method
