.class public final LX/8Rj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Qe;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 1344785
    iput-object p1, p0, LX/8Rj;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1344786
    if-nez p1, :cond_0

    .line 1344787
    :goto_0
    return-void

    .line 1344788
    :cond_0
    iget-object v0, p0, LX/8Rj;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v1, p0, LX/8Rj;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    .line 1344789
    invoke-static {v0, p1, v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a$redex0(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Ljava/util/List;Ljava/util/List;)V

    .line 1344790
    iget-object v0, p0, LX/8Rj;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v1, p0, LX/8Rj;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-static {v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)LX/8QN;

    move-result-object v1

    iget-object v2, p0, LX/8Rj;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v2, v2, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(LX/8QL;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    .line 1344791
    iget-object v0, p0, LX/8Rj;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v1, p0, LX/8Rj;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-static {v0, v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    goto :goto_0
.end method
