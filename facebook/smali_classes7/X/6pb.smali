.class public final LX/6pb;
.super LX/6pP;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

.field public final synthetic b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

.field public final synthetic c:LX/6pe;


# direct methods
.method public constructor <init>(LX/6pe;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;)V
    .locals 0

    .prologue
    .line 1149515
    iput-object p1, p0, LX/6pb;->c:LX/6pe;

    iput-object p2, p0, LX/6pb;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iput-object p3, p0, LX/6pb;->b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    invoke-direct {p0}, LX/6pP;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1149516
    iget-object v0, p0, LX/6pb;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    sget-object v1, LX/6pM;->CREATE:LX/6pM;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(LX/6pM;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1149517
    iget-object v0, p0, LX/6pb;->c:LX/6pe;

    iget-object v1, p0, LX/6pb;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v2, p0, LX/6pb;->b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    .line 1149518
    iget-object v3, v0, LX/6pe;->a:LX/6p6;

    invoke-virtual {v1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->c()Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    move-result-object v4

    new-instance p0, LX/6pc;

    invoke-direct {p0, v0, v2, v1, p1}, LX/6pc;-><init>(LX/6pe;Lcom/facebook/payments/auth/pin/EnterPinFragment;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Ljava/lang/String;)V

    .line 1149519
    new-instance v0, LX/6ov;

    invoke-direct {v0, v3, p1, v4}, LX/6ov;-><init>(LX/6p6;Ljava/lang/String;Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;)V

    .line 1149520
    invoke-static {}, LX/6p5;->a()LX/6p4;

    move-result-object v1

    const-string v2, "p2p_pin_set"

    .line 1149521
    iput-object v2, v1, LX/6p4;->a:Ljava/lang/String;

    .line 1149522
    move-object v1, v1

    .line 1149523
    const-string v2, "p2p_pin_set_fail"

    .line 1149524
    iput-object v2, v1, LX/6p4;->b:Ljava/lang/String;

    .line 1149525
    move-object v1, v1

    .line 1149526
    invoke-virtual {v1}, LX/6p4;->a()LX/6p5;

    move-result-object v1

    .line 1149527
    iget-object v2, v3, LX/6p6;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v3, v2, v0, v1, p0}, LX/6p6;->a(LX/6p6;Lcom/google/common/util/concurrent/ListenableFuture;LX/0QR;LX/6p5;LX/6nn;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, v3, LX/6p6;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1149528
    :goto_0
    return-void

    .line 1149529
    :cond_0
    iget-object v0, p0, LX/6pb;->b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/EnterPinFragment;->c()V

    goto :goto_0
.end method
