.class public final enum LX/74z;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/74z;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/74z;

.field public static final enum SCREENNAIL:LX/74z;

.field public static final enum THUMBNAIL:LX/74z;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1168753
    new-instance v0, LX/74z;

    const-string v1, "THUMBNAIL"

    invoke-direct {v0, v1, v2}, LX/74z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74z;->THUMBNAIL:LX/74z;

    .line 1168754
    new-instance v0, LX/74z;

    const-string v1, "SCREENNAIL"

    invoke-direct {v0, v1, v3}, LX/74z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74z;->SCREENNAIL:LX/74z;

    .line 1168755
    const/4 v0, 0x2

    new-array v0, v0, [LX/74z;

    sget-object v1, LX/74z;->THUMBNAIL:LX/74z;

    aput-object v1, v0, v2

    sget-object v1, LX/74z;->SCREENNAIL:LX/74z;

    aput-object v1, v0, v3

    sput-object v0, LX/74z;->$VALUES:[LX/74z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1168752
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/74z;
    .locals 1

    .prologue
    .line 1168756
    const-class v0, LX/74z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/74z;

    return-object v0
.end method

.method public static values()[LX/74z;
    .locals 1

    .prologue
    .line 1168751
    sget-object v0, LX/74z;->$VALUES:[LX/74z;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/74z;

    return-object v0
.end method
