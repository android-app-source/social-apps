.class public final LX/7UE;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/widget/images/UrlImage;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/images/UrlImage;)V
    .locals 0

    .prologue
    .line 1211682
    iput-object p1, p0, LX/7UE;->a:Lcom/facebook/widget/images/UrlImage;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1211683
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1211684
    :goto_0
    return-void

    .line 1211685
    :cond_0
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1211686
    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 1211687
    :goto_1
    if-nez v0, :cond_1

    .line 1211688
    iget-object v1, p0, LX/7UE;->a:Lcom/facebook/widget/images/UrlImage;

    const/4 v2, 0x1

    .line 1211689
    iput-boolean v2, v1, Lcom/facebook/widget/images/UrlImage;->aj:Z

    .line 1211690
    :cond_1
    iget-object v1, p0, LX/7UE;->a:Lcom/facebook/widget/images/UrlImage;

    invoke-virtual {v1, v0, p1}, Lcom/facebook/widget/images/UrlImage;->a(Landroid/graphics/drawable/Drawable;LX/1ca;)V

    goto :goto_0

    .line 1211691
    :cond_2
    iget-object v1, p0, LX/7UE;->a:Lcom/facebook/widget/images/UrlImage;

    iget-object v1, v1, Lcom/facebook/widget/images/UrlImage;->ae:LX/1eu;

    invoke-virtual {v1, v0}, LX/1eu;->a(LX/1FJ;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1
.end method

.method public final f(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1211692
    iget-object v0, p0, LX/7UE;->a:Lcom/facebook/widget/images/UrlImage;

    const/4 v1, 0x1

    .line 1211693
    iput-boolean v1, v0, Lcom/facebook/widget/images/UrlImage;->aj:Z

    .line 1211694
    iget-object v0, p0, LX/7UE;->a:Lcom/facebook/widget/images/UrlImage;

    invoke-interface {p1}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/widget/images/UrlImage;->a(Ljava/lang/Throwable;LX/1ca;)V

    .line 1211695
    return-void
.end method
