.class public LX/74n;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/ContentResolver;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/74h;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1168542
    const-class v0, LX/74n;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/74n;->a:Ljava/lang/String;

    .line 1168543
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "mediaprovider_uri"

    aput-object v2, v0, v1

    sput-object v0, LX/74n;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0ad;)V
    .locals 0
    .param p1    # LX/0Ot;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Landroid/content/ContentResolver;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/74h;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1168537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1168538
    iput-object p1, p0, LX/74n;->c:LX/0Ot;

    .line 1168539
    iput-object p2, p0, LX/74n;->d:LX/0Ot;

    .line 1168540
    iput-object p3, p0, LX/74n;->e:LX/0ad;

    .line 1168541
    return-void
.end method

.method public static a(LX/0QB;)LX/74n;
    .locals 1

    .prologue
    .line 1168536
    invoke-static {p0}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/74l;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1168421
    sget-object v0, LX/74i;->b:[I

    invoke-virtual {p0}, LX/74l;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1168422
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1168423
    :pswitch_0
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1168424
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Landroid/net/Uri;J)Lcom/facebook/ipc/media/MediaItem;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1168492
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 1168493
    invoke-static {v1}, LX/46I;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/ipc/media/data/MimeType;->a(Ljava/lang/String;)Lcom/facebook/ipc/media/data/MimeType;

    move-result-object v2

    .line 1168494
    sget-object v3, Lcom/facebook/ipc/media/data/MimeType;->e:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v3, v2}, Lcom/facebook/ipc/media/data/MimeType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1168495
    :goto_0
    return-object v0

    .line 1168496
    :cond_0
    new-instance v3, LX/4gP;

    invoke-direct {v3}, LX/4gP;-><init>()V

    new-instance v4, Lcom/facebook/ipc/media/MediaIdKey;

    invoke-direct {v4, v1, p1, p2}, Lcom/facebook/ipc/media/MediaIdKey;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/4gP;->a(Lcom/facebook/ipc/media/data/MimeType;)LX/4gP;

    move-result-object v3

    .line 1168497
    new-instance v4, LX/4gN;

    invoke-direct {v4}, LX/4gN;-><init>()V

    .line 1168498
    iput-wide p1, v4, LX/4gN;->d:J

    .line 1168499
    move-object v4, v4

    .line 1168500
    sget-object v5, LX/74i;->a:[I

    .line 1168501
    iget-object v6, v2, Lcom/facebook/ipc/media/data/MimeType;->mRawType:Ljava/lang/String;

    move-object v6, v6

    .line 1168502
    invoke-static {v6}, Lcom/facebook/ipc/media/MediaItem;->b(Ljava/lang/String;)LX/4gF;

    move-result-object v6

    invoke-virtual {v6}, LX/4gF;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1168503
    sget-object v1, LX/74n;->a:Ljava/lang/String;

    const-string v2, "Could not create fallback MediaItem for URI %s"

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1168504
    :pswitch_0
    sget-object v0, Lcom/facebook/ipc/media/data/MimeType;->d:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/media/data/MimeType;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 1168505
    if-eqz v2, :cond_3

    sget-object v0, LX/4gQ;->Video:LX/4gQ;

    :goto_1
    invoke-virtual {v3, v0}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    .line 1168506
    invoke-static {v1}, LX/2Qx;->b(Ljava/lang/String;)I

    move-result v0

    .line 1168507
    if-lez v0, :cond_1

    .line 1168508
    iput v0, v3, LX/4gP;->e:I

    .line 1168509
    :cond_1
    invoke-static {v1}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v1

    .line 1168510
    iget v5, v1, LX/434;->b:I

    if-lez v5, :cond_2

    iget v5, v1, LX/434;->a:I

    if-lez v5, :cond_2

    .line 1168511
    iget v5, v1, LX/434;->b:I

    .line 1168512
    iput v5, v3, LX/4gP;->f:I

    .line 1168513
    move-object v5, v3

    .line 1168514
    iget v6, v1, LX/434;->a:I

    .line 1168515
    iput v6, v5, LX/4gP;->g:I

    .line 1168516
    move-object v5, v5

    .line 1168517
    iget v6, v1, LX/434;->b:I

    iget v1, v1, LX/434;->a:I

    invoke-static {v6, v1, v0}, LX/74d;->a(III)F

    move-result v0

    .line 1168518
    iput v0, v5, LX/4gP;->h:F

    .line 1168519
    :cond_2
    invoke-virtual {v3}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    .line 1168520
    if-eqz v2, :cond_4

    .line 1168521
    new-instance v0, LX/74m;

    invoke-direct {v0}, LX/74m;-><init>()V

    invoke-virtual {v4}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v1

    .line 1168522
    iput-object v1, v0, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1168523
    move-object v0, v0

    .line 1168524
    invoke-virtual {v0}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v0

    goto/16 :goto_0

    .line 1168525
    :cond_3
    sget-object v0, LX/4gQ;->Photo:LX/4gQ;

    goto :goto_1

    .line 1168526
    :cond_4
    new-instance v0, LX/74k;

    invoke-direct {v0}, LX/74k;-><init>()V

    invoke-virtual {v4}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v1

    .line 1168527
    iput-object v1, v0, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1168528
    move-object v0, v0

    .line 1168529
    invoke-virtual {v0}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v0

    goto/16 :goto_0

    .line 1168530
    :pswitch_1
    sget-object v0, LX/4gQ;->Video:LX/4gQ;

    invoke-virtual {v3, v0}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    .line 1168531
    invoke-virtual {v3}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    .line 1168532
    new-instance v0, LX/74m;

    invoke-direct {v0}, LX/74m;-><init>()V

    invoke-virtual {v4}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v1

    .line 1168533
    iput-object v1, v0, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1168534
    move-object v0, v0

    .line 1168535
    invoke-virtual {v0}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/net/Uri;LX/74j;LX/74l;)Lcom/facebook/ipc/media/MediaItem;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1168459
    if-nez p1, :cond_1

    .line 1168460
    :cond_0
    :goto_0
    return-object v0

    .line 1168461
    :cond_1
    invoke-static {p1}, LX/1be;->b(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1168462
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 1168463
    invoke-static {v0}, LX/46I;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1168464
    if-nez v3, :cond_c

    .line 1168465
    invoke-static {p0, v0, p3}, LX/74n;->b(LX/74n;Ljava/lang/String;LX/74l;)Landroid/database/Cursor;

    move-result-object v3

    .line 1168466
    if-eqz v3, :cond_b

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1168467
    :goto_1
    move-object v0, v3

    .line 1168468
    move-object v4, v0

    .line 1168469
    :goto_2
    if-eqz v4, :cond_2

    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1168470
    :cond_2
    invoke-virtual {p2}, LX/74j;->getValue()J

    move-result-wide v6

    invoke-static {p1, v6, v7}, LX/74n;->a(Landroid/net/Uri;J)Lcom/facebook/ipc/media/MediaItem;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 1168471
    if-eqz v4, :cond_3

    .line 1168472
    invoke-interface {v4}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1168473
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v3

    goto :goto_0

    .line 1168474
    :cond_4
    invoke-static {p1}, LX/1be;->c(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1168475
    invoke-static {p0, p1}, LX/74n;->b(LX/74n;Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v0

    move-object v4, v0

    goto :goto_2

    .line 1168476
    :cond_5
    sget-object v1, LX/74n;->a:Ljava/lang/String;

    const-string v2, "Unsupported URI scheme of %s. Supported schemes are \'file\' and \'content\'"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    move v0, v2

    .line 1168477
    goto :goto_3

    .line 1168478
    :cond_7
    :try_start_1
    iget-object v0, p0, LX/74n;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/74h;

    invoke-virtual {v0, v4}, LX/74h;->b(Landroid/database/Cursor;)Lcom/facebook/ipc/media/MediaItem;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1168479
    if-eqz v4, :cond_0

    .line 1168480
    invoke-interface {v4}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_8

    :goto_4
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1168481
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_8
    move v1, v2

    .line 1168482
    goto :goto_4

    .line 1168483
    :catchall_0
    move-exception v0

    if-eqz v4, :cond_9

    .line 1168484
    invoke-interface {v4}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_a

    :goto_5
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1168485
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v0

    :cond_a
    move v1, v2

    .line 1168486
    goto :goto_5

    .line 1168487
    :cond_b
    invoke-static {p0, v0, p3}, LX/74n;->c(LX/74n;Ljava/lang/String;LX/74l;)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_1

    .line 1168488
    :cond_c
    sget-object v4, LX/74i;->a:[I

    invoke-static {v3}, Lcom/facebook/ipc/media/MediaItem;->b(Ljava/lang/String;)LX/4gF;

    move-result-object v3

    invoke-virtual {v3}, LX/4gF;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    .line 1168489
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 1168490
    :pswitch_0
    invoke-static {p0, v0, p3}, LX/74n;->b(LX/74n;Ljava/lang/String;LX/74l;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_1

    .line 1168491
    :pswitch_1
    invoke-static {p0, v0, p3}, LX/74n;->c(LX/74n;Ljava/lang/String;LX/74l;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 1168458
    invoke-static {p0}, LX/1be;->b(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/1be;->c(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/74n;
    .locals 4

    .prologue
    .line 1168456
    new-instance v1, LX/74n;

    invoke-interface {p0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v0

    const/16 v2, 0x13

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v0, 0x2e12

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v1, v2, v3, v0}, LX/74n;-><init>(LX/0Ot;LX/0Ot;LX/0ad;)V

    .line 1168457
    return-object v1
.end method

.method private static b(LX/74n;Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1168451
    invoke-static {p0, p1}, LX/74n;->c(LX/74n;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 1168452
    if-eqz v0, :cond_0

    move-object p1, v0

    .line 1168453
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/74n;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/74h;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, LX/74h;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1168454
    if-eqz v0, :cond_1

    .line 1168455
    :goto_0
    return-object v0

    :catch_0
    :cond_1
    iget-object v0, p0, LX/74n;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/74h;

    invoke-virtual {v0, p1, v3, v3}, LX/74h;->b(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/74n;Ljava/lang/String;LX/74l;)Landroid/database/Cursor;
    .locals 5

    .prologue
    .line 1168450
    iget-object v0, p0, LX/74n;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/74h;

    invoke-static {p2}, LX/74n;->a(LX/74l;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "_data = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, LX/74h;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/74l;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1168446
    sget-object v0, LX/74i;->b:[I

    invoke-virtual {p0}, LX/74l;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1168447
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1168448
    :pswitch_0
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1168449
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static c(LX/74n;Ljava/lang/String;LX/74l;)Landroid/database/Cursor;
    .locals 5

    .prologue
    .line 1168445
    iget-object v0, p0, LX/74n;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/74h;

    invoke-static {p2}, LX/74n;->b(LX/74l;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "_data = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, LX/74h;->b(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static c(LX/74n;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1168429
    const-string v0, "downloads"

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1168430
    :try_start_0
    iget-object v0, p0, LX/74n;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    sget-object v2, LX/74n;->b:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1168431
    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1168432
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1168433
    if-eqz v0, :cond_3

    .line 1168434
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    move-object v0, v6

    .line 1168435
    :goto_0
    if-eqz v1, :cond_0

    .line 1168436
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1168437
    :cond_0
    :goto_1
    return-object v0

    .line 1168438
    :catch_0
    move-object v0, v6

    :goto_2
    :try_start_2
    sget-object v1, LX/74n;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not retrieve the media content uri from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1168439
    if-eqz v0, :cond_2

    .line 1168440
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_1

    .line 1168441
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v6, :cond_1

    .line 1168442
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 1168443
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_3

    :catchall_2
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    goto :goto_3

    .line 1168444
    :catch_1
    move-object v0, v1

    goto :goto_2

    :cond_2
    move-object v0, v6

    goto :goto_1

    :cond_3
    move-object v0, v6

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1168426
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1168427
    invoke-static {v0}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1168428
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1168425
    sget-object v0, LX/74l;->EXTERNAL:LX/74l;

    invoke-direct {p0, p1, p2, v0}, LX/74n;->a(Landroid/net/Uri;LX/74j;LX/74l;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    return-object v0
.end method
