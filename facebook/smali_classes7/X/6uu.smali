.class public LX/6uu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/1Ck;

.field public final c:LX/70L;

.field public d:LX/6qh;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1Ck;LX/70L;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1156236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156237
    iput-object p1, p0, LX/6uu;->a:Landroid/content/res/Resources;

    .line 1156238
    iput-object p2, p0, LX/6uu;->b:LX/1Ck;

    .line 1156239
    iput-object p3, p0, LX/6uu;->c:LX/70L;

    .line 1156240
    return-void
.end method

.method public static a(Lcom/facebook/payments/consent/model/ConsentExtraData;)I
    .locals 3
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 1156241
    instance-of v0, p0, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;

    if-eqz v0, :cond_0

    .line 1156242
    const v0, 0x7f030f21

    return v0

    .line 1156243
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t get layout id for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/payments/consent/model/ConsentExtraData;)V
    .locals 8

    .prologue
    .line 1156244
    instance-of v0, p2, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;

    if-eqz v0, :cond_0

    .line 1156245
    check-cast p2, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;

    const/4 v1, 0x1

    .line 1156246
    iget-object v2, p2, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;->a:Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    .line 1156247
    iget-object v0, v2, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->c:LX/6zT;

    sget-object v3, LX/6zT;->MIB:LX/6zT;

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1156248
    const v0, 0x7f0d24b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1156249
    iget-object v3, v2, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1156250
    const v0, 0x7f0d24ba

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1156251
    new-instance v3, LX/6uq;

    invoke-direct {v3, p0, v2, p1}, LX/6uq;-><init>(LX/6uu;Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;Landroid/view/View;)V

    .line 1156252
    new-instance v4, LX/47x;

    iget-object v5, p0, LX/6uu;->a:Landroid/content/res/Resources;

    invoke-direct {v4, v5}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1156253
    iget-object v5, p0, LX/6uu;->a:Landroid/content/res/Resources;

    const v6, 0x7f081e6e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1156254
    const-string v5, "[[paypal_policies]]"

    iget-object v6, p0, LX/6uu;->a:Landroid/content/res/Resources;

    const v7, 0x7f081e6d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x21

    invoke-virtual {v4, v5, v6, v3, v7}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 1156255
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1156256
    invoke-virtual {v4}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1156257
    const v0, 0x7f0d1285

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    .line 1156258
    iget-object v3, p2, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;->b:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 1156259
    const v3, 0x7f081e6c

    invoke-virtual {v0, v3}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setCtaButtonText(I)V

    .line 1156260
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->f()V

    .line 1156261
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b()V

    .line 1156262
    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setEnabled(Z)V

    .line 1156263
    new-instance v1, LX/6ur;

    invoke-direct {v1, p0, v2, v0}, LX/6ur;-><init>(LX/6uu;Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;Lcom/facebook/payments/ui/PrimaryCtaButtonView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1156264
    return-void

    .line 1156265
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t init the view for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1156266
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1156267
    :cond_2
    iget-object v3, p2, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setCtaButtonText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final b(Lcom/facebook/payments/consent/model/ConsentExtraData;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1156268
    instance-of v0, p1, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;

    if-eqz v0, :cond_0

    .line 1156269
    iget-object v0, p0, LX/6uu;->a:Landroid/content/res/Resources;

    const v1, 0x7f081d81

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1156270
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t get title for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
