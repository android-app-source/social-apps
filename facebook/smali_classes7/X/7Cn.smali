.class public final LX/7Cn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field public final synthetic a:Lcom/facebook/spherical/GlMediaRenderThread;


# direct methods
.method public constructor <init>(Lcom/facebook/spherical/GlMediaRenderThread;)V
    .locals 0

    .prologue
    .line 1181549
    iput-object p1, p0, LX/7Cn;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 1181564
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3

    .prologue
    .line 1181550
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/16 v1, 0xf

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_3

    .line 1181551
    :cond_0
    iget-object v0, p0, LX/7Cn;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget-object v0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v0, v0, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1181552
    iget-object v0, p0, LX/7Cn;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget v0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->Q:I

    if-gtz v0, :cond_1

    .line 1181553
    iget-object v0, p0, LX/7Cn;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget-object v0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    invoke-virtual {v0, p1}, LX/7Ce;->a(Landroid/hardware/SensorEvent;)V

    .line 1181554
    iget-object v0, p0, LX/7Cn;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget v0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->Q:I

    if-nez v0, :cond_1

    .line 1181555
    iget-object v0, p0, LX/7Cn;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget-object v1, p0, LX/7Cn;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget v1, v1, Lcom/facebook/spherical/GlMediaRenderThread;->v:F

    iget-object v2, p0, LX/7Cn;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget v2, v2, Lcom/facebook/spherical/GlMediaRenderThread;->w:F

    invoke-virtual {v0, v1, v2}, Lcom/facebook/spherical/GlMediaRenderThread;->e(FF)V

    .line 1181556
    iget-object v0, p0, LX/7Cn;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget-object v0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, LX/7Ce;->a(F)V

    .line 1181557
    iget-object v0, p0, LX/7Cn;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    const/4 v1, 0x1

    .line 1181558
    iput-boolean v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->O:Z

    .line 1181559
    :cond_1
    iget-object v0, p0, LX/7Cn;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget v0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->Q:I

    if-ltz v0, :cond_2

    .line 1181560
    iget-object v0, p0, LX/7Cn;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 1181561
    iget v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->Q:I

    add-int/lit8 v2, v1, -0x1

    iput v2, v0, Lcom/facebook/spherical/GlMediaRenderThread;->Q:I

    .line 1181562
    :cond_2
    iget-object v0, p0, LX/7Cn;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget-object v0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v0, v0, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1181563
    :cond_3
    return-void
.end method
