.class public final LX/8cX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$FetchBootstrapKeywordsModel;",
        ">;",
        "LX/7C3;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3Qo;


# direct methods
.method public constructor <init>(LX/3Qo;)V
    .locals 0

    .prologue
    .line 1374596
    iput-object p1, p0, LX/8cX;->a:LX/3Qo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1374597
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1374598
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1374599
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1374600
    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$FetchBootstrapKeywordsModel;

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$FetchBootstrapKeywordsModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1374601
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1374602
    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$FetchBootstrapKeywordsModel;

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$FetchBootstrapKeywordsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v3, 0x0

    const v4, -0x64278fd2

    invoke-static {v1, v0, v3, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 1374603
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    .line 1374604
    :try_start_0
    iget-object v0, p0, LX/8cX;->a:LX/3Qo;

    iget-object v0, v0, LX/3Qo;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8cB;

    invoke-virtual {v0, v1, v4}, LX/8cB;->a(LX/15i;I)LX/7C2;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1374605
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1374606
    iget-object v0, p0, LX/8cX;->a:LX/3Qo;

    iget-object v0, v0, LX/3Qo;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sc;

    invoke-virtual {v0, v1}, LX/2Sc;->a(LX/7C4;)V

    goto :goto_1

    .line 1374607
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 1374608
    :cond_1
    new-instance v0, LX/7C3;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7C3;-><init>(LX/0Px;)V

    return-object v0
.end method
