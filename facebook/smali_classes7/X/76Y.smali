.class public final LX/76Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/76W;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/76W;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1171096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1171097
    iput-object p1, p0, LX/76Y;->a:LX/0QB;

    .line 1171098
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1171099
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/76Y;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1171100
    packed-switch p2, :pswitch_data_0

    .line 1171101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1171102
    :pswitch_0
    new-instance v0, LX/EkJ;

    invoke-direct {v0}, LX/EkJ;-><init>()V

    .line 1171103
    move-object v0, v0

    .line 1171104
    move-object v0, v0

    .line 1171105
    :goto_0
    return-object v0

    .line 1171106
    :pswitch_1
    new-instance v0, LX/JJM;

    invoke-direct {v0}, LX/JJM;-><init>()V

    .line 1171107
    move-object v0, v0

    .line 1171108
    move-object v0, v0

    .line 1171109
    goto :goto_0

    .line 1171110
    :pswitch_2
    new-instance v0, LX/Ja8;

    invoke-direct {v0}, LX/Ja8;-><init>()V

    .line 1171111
    move-object v0, v0

    .line 1171112
    move-object v0, v0

    .line 1171113
    goto :goto_0

    .line 1171114
    :pswitch_3
    new-instance v0, LX/Jat;

    invoke-direct {v0}, LX/Jat;-><init>()V

    .line 1171115
    move-object v0, v0

    .line 1171116
    move-object v0, v0

    .line 1171117
    goto :goto_0

    .line 1171118
    :pswitch_4
    new-instance v0, LX/G5u;

    invoke-direct {v0}, LX/G5u;-><init>()V

    .line 1171119
    move-object v0, v0

    .line 1171120
    move-object v0, v0

    .line 1171121
    goto :goto_0

    .line 1171122
    :pswitch_5
    new-instance v0, LX/G6K;

    invoke-direct {v0}, LX/G6K;-><init>()V

    .line 1171123
    move-object v0, v0

    .line 1171124
    move-object v0, v0

    .line 1171125
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1171126
    const/4 v0, 0x6

    return v0
.end method
