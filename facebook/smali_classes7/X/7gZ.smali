.class public final enum LX/7gZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gZ;

.field public static final enum EVENT:LX/7gZ;

.field public static final enum FRIEND:LX/7gZ;

.field public static final enum LOCATION:LX/7gZ;

.field public static final enum PAGE:LX/7gZ;

.field public static final enum SELF:LX/7gZ;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1224505
    new-instance v0, LX/7gZ;

    const-string v1, "SELF"

    const-string v2, "self"

    invoke-direct {v0, v1, v3, v2}, LX/7gZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gZ;->SELF:LX/7gZ;

    .line 1224506
    new-instance v0, LX/7gZ;

    const-string v1, "FRIEND"

    const-string v2, "friend"

    invoke-direct {v0, v1, v4, v2}, LX/7gZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gZ;->FRIEND:LX/7gZ;

    .line 1224507
    new-instance v0, LX/7gZ;

    const-string v1, "LOCATION"

    const-string v2, "location"

    invoke-direct {v0, v1, v5, v2}, LX/7gZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gZ;->LOCATION:LX/7gZ;

    .line 1224508
    new-instance v0, LX/7gZ;

    const-string v1, "EVENT"

    const-string v2, "event"

    invoke-direct {v0, v1, v6, v2}, LX/7gZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gZ;->EVENT:LX/7gZ;

    .line 1224509
    new-instance v0, LX/7gZ;

    const-string v1, "PAGE"

    const-string v2, "page"

    invoke-direct {v0, v1, v7, v2}, LX/7gZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gZ;->PAGE:LX/7gZ;

    .line 1224510
    const/4 v0, 0x5

    new-array v0, v0, [LX/7gZ;

    sget-object v1, LX/7gZ;->SELF:LX/7gZ;

    aput-object v1, v0, v3

    sget-object v1, LX/7gZ;->FRIEND:LX/7gZ;

    aput-object v1, v0, v4

    sget-object v1, LX/7gZ;->LOCATION:LX/7gZ;

    aput-object v1, v0, v5

    sget-object v1, LX/7gZ;->EVENT:LX/7gZ;

    aput-object v1, v0, v6

    sget-object v1, LX/7gZ;->PAGE:LX/7gZ;

    aput-object v1, v0, v7

    sput-object v0, LX/7gZ;->$VALUES:[LX/7gZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1224502
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1224503
    iput-object p3, p0, LX/7gZ;->mName:Ljava/lang/String;

    .line 1224504
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gZ;
    .locals 1

    .prologue
    .line 1224511
    const-class v0, LX/7gZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gZ;

    return-object v0
.end method

.method public static values()[LX/7gZ;
    .locals 1

    .prologue
    .line 1224501
    sget-object v0, LX/7gZ;->$VALUES:[LX/7gZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gZ;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224500
    iget-object v0, p0, LX/7gZ;->mName:Ljava/lang/String;

    return-object v0
.end method
