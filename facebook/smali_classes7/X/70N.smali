.class public LX/70N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/70M;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/70M",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1162281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1162282
    return-void
.end method


# virtual methods
.method public final a()LX/6zQ;
    .locals 1

    .prologue
    .line 1162280
    sget-object v0, LX/6zQ;->NEW_CREDIT_CARD:LX/6zQ;

    return-object v0
.end method

.method public final a(LX/0lF;)Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;
    .locals 3

    .prologue
    .line 1162262
    const-string v0, "available_card_types"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 1162263
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1162264
    invoke-virtual {v1}, LX/0lF;->h()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1162265
    invoke-virtual {v1}, LX/0lF;->e()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1162266
    new-instance v2, LX/0cA;

    invoke-direct {v2}, LX/0cA;-><init>()V

    .line 1162267
    invoke-virtual {v1}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1162268
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1162269
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1162270
    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->forValue(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v0

    .line 1162271
    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_1

    .line 1162272
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1162273
    :cond_1
    invoke-static {}, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->newBuilder()LX/6zN;

    move-result-object v0

    sget-object v1, LX/6xe;->REQUIRED:LX/6xe;

    .line 1162274
    iput-object v1, v0, LX/6zN;->c:LX/6xe;

    .line 1162275
    move-object v0, v0

    .line 1162276
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    .line 1162277
    iput-object v1, v0, LX/6zN;->d:LX/0Rf;

    .line 1162278
    move-object v0, v0

    .line 1162279
    invoke-virtual {v0}, LX/6zN;->e()Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;

    move-result-object v0

    return-object v0
.end method
