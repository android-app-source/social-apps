.class public final LX/8QH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:I

.field public g:Z

.field public h:Z

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 1342765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1342766
    iput v0, p0, LX/8QH;->e:I

    .line 1342767
    iput v0, p0, LX/8QH;->f:I

    .line 1342768
    iput-boolean v1, p0, LX/8QH;->g:Z

    .line 1342769
    iput-boolean v1, p0, LX/8QH;->h:Z

    .line 1342770
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8QH;->k:Z

    .line 1342771
    return-void
.end method

.method public constructor <init>(Lcom/facebook/privacy/model/AudiencePickerModel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 1342747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1342748
    iput v0, p0, LX/8QH;->e:I

    .line 1342749
    iput v0, p0, LX/8QH;->f:I

    .line 1342750
    iput-boolean v1, p0, LX/8QH;->g:Z

    .line 1342751
    iput-boolean v1, p0, LX/8QH;->h:Z

    .line 1342752
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8QH;->k:Z

    .line 1342753
    iget-object v0, p1, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    iput-object v0, p0, LX/8QH;->a:LX/0Px;

    .line 1342754
    iget-object v0, p1, Lcom/facebook/privacy/model/AudiencePickerModel;->b:LX/0Px;

    iput-object v0, p0, LX/8QH;->b:LX/0Px;

    .line 1342755
    iget-object v0, p1, Lcom/facebook/privacy/model/AudiencePickerModel;->c:LX/0Px;

    iput-object v0, p0, LX/8QH;->c:LX/0Px;

    .line 1342756
    iget-object v0, p1, Lcom/facebook/privacy/model/AudiencePickerModel;->d:LX/0Px;

    iput-object v0, p0, LX/8QH;->d:LX/0Px;

    .line 1342757
    iget v0, p1, Lcom/facebook/privacy/model/AudiencePickerModel;->e:I

    iput v0, p0, LX/8QH;->e:I

    .line 1342758
    iget v0, p1, Lcom/facebook/privacy/model/AudiencePickerModel;->f:I

    iput v0, p0, LX/8QH;->f:I

    .line 1342759
    iget-boolean v0, p1, Lcom/facebook/privacy/model/AudiencePickerModel;->g:Z

    iput-boolean v0, p0, LX/8QH;->g:Z

    .line 1342760
    iget-boolean v0, p1, Lcom/facebook/privacy/model/AudiencePickerModel;->h:Z

    iput-boolean v0, p0, LX/8QH;->h:Z

    .line 1342761
    iget-object v0, p1, Lcom/facebook/privacy/model/AudiencePickerModel;->i:LX/0Px;

    iput-object v0, p0, LX/8QH;->i:LX/0Px;

    .line 1342762
    iget-object v0, p1, Lcom/facebook/privacy/model/AudiencePickerModel;->j:LX/0Px;

    iput-object v0, p0, LX/8QH;->j:LX/0Px;

    .line 1342763
    iget-boolean v0, p1, Lcom/facebook/privacy/model/AudiencePickerModel;->k:Z

    iput-boolean v0, p0, LX/8QH;->k:Z

    .line 1342764
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/privacy/model/AudiencePickerModel;
    .locals 2

    .prologue
    .line 1342746
    new-instance v0, Lcom/facebook/privacy/model/AudiencePickerModel;

    invoke-direct {v0, p0}, Lcom/facebook/privacy/model/AudiencePickerModel;-><init>(LX/8QH;)V

    return-object v0
.end method
