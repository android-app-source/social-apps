.class public LX/6kc;
.super LX/6kT;
.source ""


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0xc

    const/4 v2, 0x1

    .line 1140082
    sput-boolean v2, LX/6kc;->a:Z

    .line 1140083
    new-instance v0, LX/1sv;

    const-string v1, "GenericMapMutationOp"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kc;->b:LX/1sv;

    .line 1140084
    new-instance v0, LX/1sw;

    const-string v1, "newValue"

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kc;->c:LX/1sw;

    .line 1140085
    new-instance v0, LX/1sw;

    const-string v1, "mutation"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kc;->d:LX/1sw;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1140080
    invoke-direct {p0}, LX/6kT;-><init>()V

    .line 1140081
    return-void
.end method

.method private c()LX/6kd;
    .locals 3

    .prologue
    .line 1139994
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 1139995
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1139996
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1139997
    check-cast v0, LX/6kd;

    return-object v0

    .line 1139998
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'newValue\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1139999
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 1140000
    invoke-virtual {p0, v2}, LX/6kc;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private d()LX/6kb;
    .locals 3

    .prologue
    .line 1140073
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 1140074
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1140075
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140076
    check-cast v0, LX/6kb;

    return-object v0

    .line 1140077
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'mutation\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1140078
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 1140079
    invoke-virtual {p0, v2}, LX/6kc;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(LX/1su;LX/1sw;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1140064
    iget-short v1, p2, LX/1sw;->c:S

    packed-switch v1, :pswitch_data_0

    .line 1140065
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    .line 1140066
    :goto_0
    return-object v0

    .line 1140067
    :pswitch_0
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/6kc;->c:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_0

    .line 1140068
    invoke-static {p1}, LX/6kd;->b(LX/1su;)LX/6kd;

    move-result-object v0

    goto :goto_0

    .line 1140069
    :cond_0
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1140070
    :pswitch_1
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/6kc;->d:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_1

    .line 1140071
    invoke-static {p1}, LX/6kb;->b(LX/1su;)LX/6kb;

    move-result-object v0

    goto :goto_0

    .line 1140072
    :cond_1
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 1140031
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1140032
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1140033
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1140034
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "GenericMapMutationOp"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1140035
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140036
    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140037
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140038
    iget v5, p0, LX/6kT;->setField_:I

    move v5, v5

    .line 1140039
    if-ne v5, v1, :cond_0

    .line 1140040
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140041
    const-string v1, "newValue"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140042
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140043
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140044
    invoke-direct {p0}, LX/6kc;->c()LX/6kd;

    move-result-object v1

    if-nez v1, :cond_6

    .line 1140045
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140046
    :goto_3
    const/4 v1, 0x0

    .line 1140047
    :cond_0
    iget v5, p0, LX/6kT;->setField_:I

    move v5, v5

    .line 1140048
    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    .line 1140049
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140050
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140051
    const-string v1, "mutation"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140052
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140053
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140054
    invoke-direct {p0}, LX/6kc;->d()LX/6kb;

    move-result-object v0

    if-nez v0, :cond_7

    .line 1140055
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140056
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140057
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140058
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1140059
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1140060
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1140061
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1140062
    :cond_6
    invoke-direct {p0}, LX/6kc;->c()LX/6kd;

    move-result-object v1

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1140063
    :cond_7
    invoke-direct {p0}, LX/6kc;->d()LX/6kb;

    move-result-object v0

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;S)V
    .locals 3

    .prologue
    .line 1140022
    packed-switch p2, :pswitch_data_0

    .line 1140023
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot write union with unknown field "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1140024
    :pswitch_0
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140025
    check-cast v0, LX/6kd;

    .line 1140026
    invoke-virtual {v0, p1}, LX/6kT;->a(LX/1su;)V

    .line 1140027
    :goto_0
    return-void

    .line 1140028
    :pswitch_1
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140029
    check-cast v0, LX/6kb;

    .line 1140030
    invoke-virtual {v0, p1}, LX/6kT;->a(LX/1su;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/6kc;)Z
    .locals 2

    .prologue
    .line 1140012
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 1140013
    iget v1, p1, LX/6kT;->setField_:I

    move v1, v1

    .line 1140014
    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_1

    .line 1140015
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140016
    check-cast v0, [B

    check-cast v0, [B

    .line 1140017
    iget-object v1, p1, LX/6kT;->value_:Ljava/lang/Object;

    move-object v1, v1

    .line 1140018
    check-cast v1, [B

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 1140019
    :cond_1
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1140020
    iget-object v1, p1, LX/6kT;->value_:Ljava/lang/Object;

    move-object v1, v1

    .line 1140021
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)LX/1sw;
    .locals 3

    .prologue
    .line 1140008
    packed-switch p1, :pswitch_data_0

    .line 1140009
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown field id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1140010
    :pswitch_0
    sget-object v0, LX/6kc;->c:LX/1sw;

    .line 1140011
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, LX/6kc;->d:LX/1sw;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1140005
    instance-of v0, p1, LX/6kc;

    if-eqz v0, :cond_0

    .line 1140006
    check-cast p1, LX/6kc;

    invoke-virtual {p0, p1}, LX/6kc;->a(LX/6kc;)Z

    move-result v0

    .line 1140007
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1140004
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1140001
    sget-boolean v0, LX/6kc;->a:Z

    .line 1140002
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kc;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1140003
    return-object v0
.end method
