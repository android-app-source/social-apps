.class public LX/7m8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/7m8;


# instance fields
.field public final a:LX/0Xl;


# direct methods
.method public constructor <init>(LX/0Xl;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1236303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236304
    iput-object p1, p0, LX/7m8;->a:LX/0Xl;

    .line 1236305
    return-void
.end method

.method public static a(LX/0QB;)LX/7m8;
    .locals 4

    .prologue
    .line 1236290
    sget-object v0, LX/7m8;->b:LX/7m8;

    if-nez v0, :cond_1

    .line 1236291
    const-class v1, LX/7m8;

    monitor-enter v1

    .line 1236292
    :try_start_0
    sget-object v0, LX/7m8;->b:LX/7m8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1236293
    if-eqz v2, :cond_0

    .line 1236294
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1236295
    new-instance p0, LX/7m8;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-direct {p0, v3}, LX/7m8;-><init>(LX/0Xl;)V

    .line 1236296
    move-object v0, p0

    .line 1236297
    sput-object v0, LX/7m8;->b:LX/7m8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1236298
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1236299
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1236300
    :cond_1
    sget-object v0, LX/7m8;->b:LX/7m8;

    return-object v0

    .line 1236301
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1236302
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/7m7;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;JLcom/facebook/composer/publish/common/ErrorDetails;)V
    .locals 3

    .prologue
    .line 1236280
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1236281
    const-string v1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1236282
    const-string v1, "extra_result"

    invoke-virtual {p1}, LX/7m7;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1236283
    const-string v1, "extra_legacy_api_post_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1236284
    const-string v1, "graphql_story"

    invoke-static {v0, v1, p3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1236285
    const-string v1, "extra_request_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1236286
    const-string v1, "extra_target_id"

    invoke-virtual {v0, v1, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1236287
    const-string v1, "extra_error_details"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1236288
    iget-object v1, p0, LX/7m8;->a:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1236289
    return-void
.end method

.method public final a(Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 4

    .prologue
    .line 1236271
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1236272
    const-string v0, "com.facebook.STREAM_PUBLISH_START"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1236273
    const-string v0, "extra_feed_story"

    invoke-static {v1, v0, p2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1236274
    const-string v0, "extra_request_id"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1236275
    const-string v0, "extra_target_id"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->c()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1236276
    const-string v2, "extra_has_explicit_place"

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->i()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->i()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->isExplicitLocation:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1236277
    iget-object v0, p0, LX/7m8;->a:LX/0Xl;

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1236278
    return-void

    .line 1236279
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;JI)V
    .locals 2

    .prologue
    .line 1236264
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1236265
    const-string v1, "com.facebook.STREAM_PUBLISH_PROGRESS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1236266
    const-string v1, "extra_request_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1236267
    const-string v1, "extra_target_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1236268
    const-string v1, "extra_percent_progress"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1236269
    iget-object v1, p0, LX/7m8;->a:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1236270
    return-void
.end method
