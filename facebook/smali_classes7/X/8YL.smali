.class public LX/8YL;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static c:LX/8YL;


# instance fields
.field public a:D

.field public b:D


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1356160
    const-wide/high16 v0, 0x4044000000000000L    # 40.0

    const-wide/high16 v2, 0x401c000000000000L    # 7.0

    invoke-static {v0, v1, v2, v3}, LX/8YL;->a(DD)LX/8YL;

    move-result-object v0

    sput-object v0, LX/8YL;->c:LX/8YL;

    return-void
.end method

.method public constructor <init>(DD)V
    .locals 1

    .prologue
    .line 1356166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1356167
    iput-wide p1, p0, LX/8YL;->b:D

    .line 1356168
    iput-wide p3, p0, LX/8YL;->a:D

    .line 1356169
    return-void
.end method

.method public static a(DD)LX/8YL;
    .locals 10

    .prologue
    .line 1356161
    new-instance v0, LX/8YL;

    const-wide/16 v6, 0x0

    .line 1356162
    cmpl-double v8, p0, v6

    if-nez v8, :cond_0

    :goto_0
    move-wide v2, v6

    .line 1356163
    const-wide/16 v6, 0x0

    .line 1356164
    cmpl-double v8, p2, v6

    if-nez v8, :cond_1

    :goto_1
    move-wide v4, v6

    .line 1356165
    invoke-direct {v0, v2, v3, v4, v5}, LX/8YL;-><init>(DD)V

    return-object v0

    :cond_0
    const-wide/high16 v6, 0x403e000000000000L    # 30.0

    sub-double v6, p0, v6

    const-wide v8, 0x400cf5c28f5c28f6L    # 3.62

    mul-double/2addr v6, v8

    const-wide v8, 0x4068400000000000L    # 194.0

    add-double/2addr v6, v8

    goto :goto_0

    :cond_1
    const-wide/high16 v6, 0x4020000000000000L    # 8.0

    sub-double v6, p2, v6

    const-wide/high16 v8, 0x4008000000000000L    # 3.0

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4039000000000000L    # 25.0

    add-double/2addr v6, v8

    goto :goto_1
.end method
