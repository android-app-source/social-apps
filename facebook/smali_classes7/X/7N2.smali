.class public LX/7N2;
.super LX/2oy;
.source ""


# instance fields
.field public a:Landroid/animation/AnimatorSet;

.field private final b:Z

.field public final c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field private final d:LX/7N1;

.field private final e:Lcom/facebook/fbui/glyph/GlyphView;

.field public f:LX/1CD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1199734
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7N2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1199735
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1199732
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7N2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199733
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 1199722
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199723
    new-instance v0, LX/7N1;

    invoke-direct {v0, p0}, LX/7N1;-><init>(LX/7N2;)V

    iput-object v0, p0, LX/7N2;->d:LX/7N1;

    .line 1199724
    const-class v0, LX/7N2;

    invoke-static {v0, p0}, LX/7N2;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1199725
    const v0, 0x7f03073e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1199726
    const v0, 0x7f0d1351

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iput-object v0, p0, LX/7N2;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 1199727
    const v0, 0x7f0d1352

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/7N2;->e:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1199728
    iget-object v0, p0, LX/7N2;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 1199729
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    iget-object v1, p0, LX/7N2;->d:LX/7N1;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199730
    iget-object v0, p0, LX/7N2;->n:LX/0W3;

    sget-wide v2, LX/0X5;->d:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    iput-boolean v0, p0, LX/7N2;->b:Z

    .line 1199731
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/7N2;

    invoke-static {p0}, LX/1CD;->a(LX/0QB;)LX/1CD;

    move-result-object v1

    check-cast v1, LX/1CD;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object p0

    check-cast p0, LX/0W3;

    iput-object v1, p1, LX/7N2;->f:LX/1CD;

    iput-object p0, p1, LX/7N2;->n:LX/0W3;

    return-void
.end method

.method private g()V
    .locals 8

    .prologue
    const/4 v4, 0x3

    const-wide/16 v6, 0x7d0

    .line 1199707
    iget-object v0, p0, LX/7N2;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 1199708
    iget-object v0, p0, LX/7N2;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setAlpha(F)V

    .line 1199709
    iget-object v0, p0, LX/7N2;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const-string v1, "alpha"

    new-array v2, v4, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1199710
    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 1199711
    iget-object v1, p0, LX/7N2;->e:Lcom/facebook/fbui/glyph/GlyphView;

    const-string v2, "scaleX"

    new-array v3, v4, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1199712
    iget-object v2, p0, LX/7N2;->e:Lcom/facebook/fbui/glyph/GlyphView;

    const-string v3, "scaleY"

    new-array v4, v4, [F

    fill-array-data v4, :array_2

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1199713
    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 1199714
    invoke-virtual {v2, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 1199715
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v3, p0, LX/7N2;->a:Landroid/animation/AnimatorSet;

    .line 1199716
    iget-object v3, p0, LX/7N2;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1199717
    iget-object v0, p0, LX/7N2;->a:Landroid/animation/AnimatorSet;

    new-instance v1, LX/7N0;

    invoke-direct {v1, p0}, LX/7N0;-><init>(LX/7N2;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1199718
    iget-object v0, p0, LX/7N2;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 1199719
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 1199720
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3fc00000    # 1.5f
        0x3f800000    # 1.0f
    .end array-data

    .line 1199721
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x3fc00000    # 1.5f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 1199700
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/7N2;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7N2;->f:LX/1CD;

    invoke-virtual {v0}, LX/1CD;->a()LX/45B;

    move-result-object v0

    sget-object v1, LX/45B;->CONNECTED:LX/45B;

    if-ne v0, v1, :cond_1

    .line 1199701
    :cond_0
    invoke-virtual {p0}, LX/2oy;->n()V

    .line 1199702
    :goto_0
    return-void

    .line 1199703
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7N2;->l:Z

    .line 1199704
    invoke-direct {p0}, LX/7N2;->g()V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1199705
    iget-object v0, p0, LX/7N2;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 1199706
    return-void
.end method
