.class public LX/7Q7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;


# static fields
.field public static final a:LX/0Tn;


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/1mC;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1203586
    sget-object v0, LX/11b;->a:LX/0Tn;

    const-string v1, "autoplay/count_seen_autoplay_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7Q7;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/1mC;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .param p1    # LX/1mC;
        .annotation runtime Lcom/facebook/video/settings/DefaultAutoPlaySettingsFromServer;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1203587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1203588
    iput-object p3, p0, LX/7Q7;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1203589
    iput-object p2, p0, LX/7Q7;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1203590
    iput-object p1, p0, LX/7Q7;->d:LX/1mC;

    .line 1203591
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1203592
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 2

    .prologue
    .line 1203593
    iget-object v0, p0, LX/7Q7;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/7Q7;->a:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->c(LX/0Tn;)Ljava/lang/Object;

    move-result-object v0

    .line 1203594
    const/4 v1, 0x0

    .line 1203595
    instance-of p1, v0, Ljava/lang/Integer;

    if-eqz p1, :cond_1

    .line 1203596
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1203597
    :goto_0
    move v0, v0

    .line 1203598
    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 1203599
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 1203600
    :goto_1
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 1203601
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1203602
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1203603
    const-string v0, "3507"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1203604
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_AUTOPLAY:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
