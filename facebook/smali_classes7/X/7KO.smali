.class public final LX/7KO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# instance fields
.field public final synthetic a:LX/7KP;


# direct methods
.method public constructor <init>(LX/7KP;)V
    .locals 0

    .prologue
    .line 1195785
    iput-object p1, p0, LX/7KO;->a:LX/7KP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 3
    .param p1    # Ljava/lang/Runnable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1195786
    new-instance v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$11$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$11$1;-><init>(LX/7KO;Ljava/lang/Runnable;)V

    .line 1195787
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ExoVideoPlayerServiceListenerCallback-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1195788
    const v2, -0x6929cfe4

    invoke-static {v0, v1, v2}, LX/00l;->a(Ljava/lang/Runnable;Ljava/lang/String;I)Ljava/lang/Thread;

    move-result-object v0

    return-object v0
.end method
