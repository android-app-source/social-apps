.class public final LX/7He;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Sp;
.implements LX/2Sq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/2Sp;",
        "LX/2Sq",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public a:LX/7B6;

.field public final synthetic b:LX/7Hg;

.field private final c:LX/7HS;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7HS",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final d:LX/7Hg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Hg",
            "<TT;>;"
        }
    .end annotation
.end field

.field public e:LX/7HZ;

.field private final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/7B6;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/7Hf;

.field private final h:I


# direct methods
.method public constructor <init>(LX/7Hg;LX/7HS;LX/7Hg;LX/7Hf;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7HS",
            "<TT;>;",
            "LX/7Hg",
            "<TT;>;",
            "LX/7Hf;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1190962
    iput-object p1, p0, LX/7He;->b:LX/7Hg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1190963
    sget-object v0, LX/7HZ;->IDLE:LX/7HZ;

    iput-object v0, p0, LX/7He;->e:LX/7HZ;

    .line 1190964
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7He;->f:Ljava/util/HashMap;

    .line 1190965
    sget-object v0, LX/7B6;->a:LX/7B6;

    iput-object v0, p0, LX/7He;->a:LX/7B6;

    .line 1190966
    iput-object p2, p0, LX/7He;->c:LX/7HS;

    .line 1190967
    iput-object p3, p0, LX/7He;->d:LX/7Hg;

    .line 1190968
    iput-object p4, p0, LX/7He;->g:LX/7Hf;

    .line 1190969
    iput p5, p0, LX/7He;->h:I

    .line 1190970
    return-void
.end method

.method public static a$redex0(LX/7He;I)V
    .locals 6

    .prologue
    .line 1190959
    iget-object v0, p0, LX/7He;->b:LX/7Hg;

    iget-object v0, v0, LX/7Hg;->l:LX/7B6;

    .line 1190960
    iget-object v1, p0, LX/7He;->b:LX/7Hg;

    iget-object v1, v1, LX/7Hg;->d:LX/0Sh;

    new-instance v2, Lcom/facebook/ui/typeahead/SynchronousTypeaheadFetchStrategy$TypeaheadFetchHandler$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/facebook/ui/typeahead/SynchronousTypeaheadFetchStrategy$TypeaheadFetchHandler$1;-><init>(LX/7He;LX/7B6;I)V

    int-to-long v4, p1

    invoke-virtual {v1, v2, v4, v5}, LX/0Sh;->b(Ljava/lang/Runnable;J)V

    .line 1190961
    return-void
.end method

.method public static a$redex0(LX/7He;LX/0P1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1190955
    iget-object v0, p0, LX/7He;->c:LX/7HS;

    invoke-interface {v0, p1}, LX/7HS;->a(LX/0P1;)V

    .line 1190956
    invoke-direct {p0}, LX/7He;->d()V

    .line 1190957
    sget-object v0, LX/7HZ;->IDLE:LX/7HZ;

    invoke-direct {p0, v0}, LX/7He;->b(LX/7HZ;)V

    .line 1190958
    return-void
.end method

.method private b(LX/7HZ;)V
    .locals 2

    .prologue
    .line 1190941
    iget-object v0, p0, LX/7He;->e:LX/7HZ;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, LX/7He;->e:LX/7HZ;

    sget-object v1, LX/7HZ;->ERROR:LX/7HZ;

    invoke-virtual {v0, v1}, LX/7HZ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/7HZ;->IDLE:LX/7HZ;

    invoke-virtual {p1, v0}, LX/7HZ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1190942
    :cond_0
    :goto_0
    return-void

    .line 1190943
    :cond_1
    iput-object p1, p0, LX/7He;->e:LX/7HZ;

    .line 1190944
    iget-object v0, p0, LX/7He;->b:LX/7Hg;

    .line 1190945
    sget-object v1, LX/7HZ;->ACTIVE:LX/7HZ;

    invoke-static {v0, v1}, LX/7Hg;->a(LX/7Hg;LX/7HZ;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1190946
    sget-object v1, LX/7HZ;->ACTIVE:LX/7HZ;

    .line 1190947
    :goto_1
    iget-object p0, v0, LX/7Hg;->m:LX/7HZ;

    if-ne v1, p0, :cond_5

    .line 1190948
    :cond_2
    :goto_2
    goto :goto_0

    .line 1190949
    :cond_3
    sget-object v1, LX/7HZ;->ERROR:LX/7HZ;

    invoke-static {v0, v1}, LX/7Hg;->a(LX/7Hg;LX/7HZ;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1190950
    sget-object v1, LX/7HZ;->ERROR:LX/7HZ;

    goto :goto_1

    .line 1190951
    :cond_4
    sget-object v1, LX/7HZ;->IDLE:LX/7HZ;

    goto :goto_1

    .line 1190952
    :cond_5
    iput-object v1, v0, LX/7Hg;->m:LX/7HZ;

    .line 1190953
    iget-object v1, v0, LX/7Hg;->k:LX/2Sp;

    if-eqz v1, :cond_2

    .line 1190954
    iget-object v1, v0, LX/7Hg;->k:LX/2Sp;

    iget-object p0, v0, LX/7Hg;->m:LX/7HZ;

    invoke-interface {v1, p0}, LX/2Sp;->a(LX/7HZ;)V

    goto :goto_2
.end method

.method public static b(LX/7He;)V
    .locals 5

    .prologue
    .line 1190933
    iget-object v0, p0, LX/7He;->c:LX/7HS;

    invoke-interface {v0}, LX/7HS;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1190934
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/7He;->b$redex0(LX/7He;I)V

    .line 1190935
    :goto_0
    return-void

    .line 1190936
    :cond_0
    iget-object v0, p0, LX/7He;->b:LX/7Hg;

    iget-object v0, v0, LX/7Hg;->f:LX/7Hd;

    .line 1190937
    iget-object v1, v0, LX/7Hd;->b:LX/0W3;

    sget-wide v3, LX/0X5;->hn:J

    const/4 v2, 0x0

    invoke-interface {v1, v3, v4, v2}, LX/0W4;->a(JZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1190938
    iget-object v1, v0, LX/7Hd;->a:LX/0uf;

    sget-wide v3, LX/0X5;->hm:J

    invoke-virtual {v1, v3, v4}, LX/0uf;->a(J)LX/1jr;

    move-result-object v1

    const-string v2, "throttling_delay"

    const-wide/16 v3, 0x96

    invoke-virtual {v1, v2, v3, v4}, LX/1jr;->a(Ljava/lang/String;J)J

    move-result-wide v1

    long-to-int v1, v1

    .line 1190939
    :goto_1
    move v0, v1

    .line 1190940
    invoke-static {p0, v0}, LX/7He;->a$redex0(LX/7He;I)V

    goto :goto_0

    :cond_1
    const/16 v1, 0x96

    goto :goto_1
.end method

.method public static b$redex0(LX/7He;I)V
    .locals 4

    .prologue
    .line 1190907
    iget-object v0, p0, LX/7He;->b:LX/7Hg;

    iget-object v0, v0, LX/7Hg;->l:LX/7B6;

    .line 1190908
    iget-object v1, v0, LX/7B6;->b:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/7He;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1190909
    iget-object v1, p0, LX/7He;->f:Ljava/util/HashMap;

    iget v2, p0, LX/7He;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1190910
    invoke-virtual {v0}, LX/7B6;->c()LX/7B2;

    move-result-object v1

    const-string v2, "DURATION_MS"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/7B2;->a(Ljava/util/Map;)LX/7B2;

    move-result-object v1

    invoke-virtual {v1}, LX/7B2;->a()LX/7B6;

    move-result-object v1

    .line 1190911
    iget-object v0, p0, LX/7He;->c:LX/7HS;

    invoke-interface {v0, v1}, LX/7HS;->b(LX/7B6;)V

    .line 1190912
    sget-object v0, LX/7HZ;->ACTIVE:LX/7HZ;

    invoke-direct {p0, v0}, LX/7He;->b(LX/7HZ;)V

    .line 1190913
    :cond_0
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1190930
    iget-object v0, p0, LX/7He;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1190931
    sget-object v0, LX/7B6;->a:LX/7B6;

    iput-object v0, p0, LX/7He;->a:LX/7B6;

    .line 1190932
    return-void
.end method


# virtual methods
.method public final a(LX/7HZ;)V
    .locals 1

    .prologue
    .line 1190926
    sget-object v0, LX/7HZ;->ERROR:LX/7HZ;

    if-ne p1, v0, :cond_0

    .line 1190927
    invoke-direct {p0}, LX/7He;->d()V

    .line 1190928
    :cond_0
    invoke-direct {p0, p1}, LX/7He;->b(LX/7HZ;)V

    .line 1190929
    return-void
.end method

.method public final a(LX/7Hi;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1190914
    iget-object v0, p1, LX/7Hi;->a:LX/7B6;

    move-object v2, v0

    .line 1190915
    iput-object v2, p0, LX/7He;->a:LX/7B6;

    .line 1190916
    iget-object v0, p0, LX/7He;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/7He;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1190917
    :goto_0
    add-int/lit8 v0, v0, -0x1

    .line 1190918
    if-lez v0, :cond_2

    .line 1190919
    iget-object v3, p0, LX/7He;->f:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1190920
    :goto_1
    iget-object v0, p0, LX/7He;->b:LX/7Hg;

    iget-object v0, v0, LX/7Hg;->l:LX/7B6;

    invoke-virtual {v0, v2}, LX/7B6;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1190921
    invoke-static {p0, v1}, LX/7He;->b$redex0(LX/7He;I)V

    .line 1190922
    :cond_0
    iget-object v0, p0, LX/7He;->d:LX/7Hg;

    iget-object v1, p0, LX/7He;->g:LX/7Hf;

    invoke-virtual {v0, p1, v1}, LX/7Hg;->a(LX/7Hi;LX/7Hf;)V

    .line 1190923
    return-void

    :cond_1
    move v0, v1

    .line 1190924
    goto :goto_0

    .line 1190925
    :cond_2
    iget-object v0, p0, LX/7He;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method
