.class public final LX/8eK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1382423
    const/4 v12, 0x0

    .line 1382424
    const/4 v11, 0x0

    .line 1382425
    const/4 v10, 0x0

    .line 1382426
    const/4 v9, 0x0

    .line 1382427
    const/4 v8, 0x0

    .line 1382428
    const/4 v7, 0x0

    .line 1382429
    const/4 v6, 0x0

    .line 1382430
    const/4 v5, 0x0

    .line 1382431
    const/4 v4, 0x0

    .line 1382432
    const/4 v3, 0x0

    .line 1382433
    const/4 v2, 0x0

    .line 1382434
    const/4 v1, 0x0

    .line 1382435
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1382436
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1382437
    const/4 v1, 0x0

    .line 1382438
    :goto_0
    return v1

    .line 1382439
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1382440
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_d

    .line 1382441
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1382442
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1382443
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1382444
    const-string v14, "debug_info_native_template_view"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1382445
    invoke-static/range {p0 .. p1}, LX/5ex;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1382446
    :cond_2
    const-string v14, "dense_node_story"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1382447
    invoke-static/range {p0 .. p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1382448
    :cond_3
    const-string v14, "logging_unit_id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1382449
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1382450
    :cond_4
    const-string v14, "module_role"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1382451
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 1382452
    :cond_5
    const-string v14, "native_template_view"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1382453
    invoke-static/range {p0 .. p1}, LX/5ex;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1382454
    :cond_6
    const-string v14, "node"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1382455
    invoke-static/range {p0 .. p1}, LX/8eJ;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1382456
    :cond_7
    const-string v14, "node_story"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1382457
    invoke-static/range {p0 .. p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1382458
    :cond_8
    const-string v14, "result_decoration"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1382459
    invoke-static/range {p0 .. p1}, LX/8gQ;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1382460
    :cond_9
    const-string v14, "result_display_styles"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 1382461
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1382462
    :cond_a
    const-string v14, "result_role"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 1382463
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto/16 :goto_1

    .line 1382464
    :cond_b
    const-string v14, "see_more_query"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_c

    .line 1382465
    invoke-static/range {p0 .. p1}, LX/8fn;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1382466
    :cond_c
    const-string v14, "story_decoration"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1382467
    invoke-static/range {p0 .. p1}, LX/8gC;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 1382468
    :cond_d
    const/16 v13, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1382469
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1382470
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1382471
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1382472
    const/4 v10, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1382473
    const/4 v9, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1382474
    const/4 v8, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1382475
    const/4 v7, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1382476
    const/4 v6, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1382477
    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1382478
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1382479
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 1382480
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1382481
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v4, 0x9

    const/16 v3, 0x8

    const/4 v2, 0x3

    .line 1382482
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1382483
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1382484
    if-eqz v0, :cond_0

    .line 1382485
    const-string v1, "debug_info_native_template_view"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382486
    invoke-static {p0, v0, p2, p3}, LX/5ex;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1382487
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1382488
    if-eqz v0, :cond_1

    .line 1382489
    const-string v1, "dense_node_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382490
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1382491
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1382492
    if-eqz v0, :cond_2

    .line 1382493
    const-string v1, "logging_unit_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382494
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1382495
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1382496
    if-eqz v0, :cond_3

    .line 1382497
    const-string v0, "module_role"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382498
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1382499
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1382500
    if-eqz v0, :cond_4

    .line 1382501
    const-string v1, "native_template_view"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382502
    invoke-static {p0, v0, p2, p3}, LX/5ex;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1382503
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1382504
    if-eqz v0, :cond_5

    .line 1382505
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382506
    invoke-static {p0, v0, p2, p3}, LX/8eJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1382507
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1382508
    if-eqz v0, :cond_6

    .line 1382509
    const-string v1, "node_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382510
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1382511
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1382512
    if-eqz v0, :cond_7

    .line 1382513
    const-string v1, "result_decoration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382514
    invoke-static {p0, v0, p2, p3}, LX/8gQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1382515
    :cond_7
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1382516
    if-eqz v0, :cond_8

    .line 1382517
    const-string v0, "result_display_styles"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382518
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1382519
    :cond_8
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1382520
    if-eqz v0, :cond_9

    .line 1382521
    const-string v0, "result_role"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382522
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1382523
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1382524
    if-eqz v0, :cond_a

    .line 1382525
    const-string v1, "see_more_query"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382526
    invoke-static {p0, v0, p2, p3}, LX/8fn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1382527
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1382528
    if-eqz v0, :cond_b

    .line 1382529
    const-string v1, "story_decoration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382530
    invoke-static {p0, v0, p2, p3}, LX/8gC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1382531
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1382532
    return-void
.end method
