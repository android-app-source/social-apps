.class public LX/81m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/api/feedcache/memory/ToggleSaveParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1286579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1286580
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1286581
    check-cast p1, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;

    .line 1286582
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1286583
    iget-object v0, p1, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1286584
    iget-object v0, p1, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1286585
    iget-object v0, p1, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1286586
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "action"

    const-string v2, "ADD"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1286587
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "item_id"

    iget-object v2, p1, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1286588
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "curation_surface"

    iget-object v2, p1, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->f:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1286589
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "curation_mechanism"

    iget-object v2, p1, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->g:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1286590
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "privacy"

    const-string v2, "{\"value\":\"SELF\"}"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1286591
    iget-object v0, p1, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1286592
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "source_story_id"

    iget-object v2, p1, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1286593
    :cond_0
    new-instance v0, LX/14N;

    const-string v1, "placeSave"

    const-string v2, "POST"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "/"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->d:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/items"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v5, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1286594
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1286595
    invoke-virtual {p2}, LX/1pN;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
