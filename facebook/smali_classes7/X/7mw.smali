.class public LX/7mw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/delights/floating/DelightsFireworks;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Ar;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1237545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x7d702bba

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1237546
    iget-object v1, p0, LX/7mw;->c:LX/1Ar;

    invoke-virtual {v1}, LX/1Ar;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1237547
    iget-object v1, p0, LX/7mw;->a:LX/0Zb;

    const-string v2, "factory_delights_nye_2017_comment_tapped"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 1237548
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 1237549
    iget-object v1, p0, LX/7mw;->b:Lcom/facebook/delights/floating/DelightsFireworks;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/delights/floating/DelightsFireworks;->b(Landroid/content/Context;)V

    .line 1237550
    :cond_0
    const v1, -0x68d125d0

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
