.class public LX/7xH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/7xH;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:LX/5fv;

.field private final c:LX/6RZ;

.field public final d:LX/7xC;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/5fv;LX/6RZ;LX/7xC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1277259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1277260
    iput-object p1, p0, LX/7xH;->a:Landroid/content/res/Resources;

    .line 1277261
    iput-object p2, p0, LX/7xH;->b:LX/5fv;

    .line 1277262
    iput-object p3, p0, LX/7xH;->c:LX/6RZ;

    .line 1277263
    iput-object p4, p0, LX/7xH;->d:LX/7xC;

    .line 1277264
    return-void
.end method

.method public static a(LX/0QB;)LX/7xH;
    .locals 7

    .prologue
    .line 1277265
    sget-object v0, LX/7xH;->e:LX/7xH;

    if-nez v0, :cond_1

    .line 1277266
    const-class v1, LX/7xH;

    monitor-enter v1

    .line 1277267
    :try_start_0
    sget-object v0, LX/7xH;->e:LX/7xH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1277268
    if-eqz v2, :cond_0

    .line 1277269
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1277270
    new-instance p0, LX/7xH;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v4

    check-cast v4, LX/5fv;

    invoke-static {v0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v5

    check-cast v5, LX/6RZ;

    invoke-static {v0}, LX/7xC;->a(LX/0QB;)LX/7xC;

    move-result-object v6

    check-cast v6, LX/7xC;

    invoke-direct {p0, v3, v4, v5, v6}, LX/7xH;-><init>(Landroid/content/res/Resources;LX/5fv;LX/6RZ;LX/7xC;)V

    .line 1277271
    move-object v0, p0

    .line 1277272
    sput-object v0, LX/7xH;->e:LX/7xH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1277273
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1277274
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1277275
    :cond_1
    sget-object v0, LX/7xH;->e:LX/7xH;

    return-object v0

    .line 1277276
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1277277
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/7om;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1277258
    invoke-interface {p0}, LX/7ol;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/7ol;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p0}, LX/7ol;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " * "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/7xH;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 5

    .prologue
    .line 1277255
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1277256
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    iget-object v2, p0, LX/7xH;->a:Landroid/content/res/Resources;

    const v3, 0x7f0a0095

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {v1}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1277257
    return-object v0
.end method


# virtual methods
.method public final a(ZILcom/facebook/events/tickets/modal/model/EventTicketTierModel;)Ljava/lang/CharSequence;
    .locals 12

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1277230
    iget-wide v0, p3, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->e:J

    .line 1277231
    iget-wide v2, p3, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->f:J

    .line 1277232
    iget-object v4, p3, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->d:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;->PRE_SALE:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    if-ne v4, v5, :cond_0

    .line 1277233
    iget-object v2, p0, LX/7xH;->d:LX/7xC;

    invoke-virtual {v2, v0, v1}, LX/7xC;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 1277234
    :goto_0
    return-object v0

    .line 1277235
    :cond_0
    iget v0, p3, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->h:I

    const/4 v5, 0x1

    .line 1277236
    if-gt v0, v5, :cond_5

    .line 1277237
    const-string v1, ""

    .line 1277238
    :cond_1
    :goto_1
    move-object v0, v1

    .line 1277239
    invoke-static {v2, v3}, LX/7xC;->c(J)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1277240
    const-string v1, ""

    .line 1277241
    :goto_2
    move-object v1, v1

    .line 1277242
    iget v2, p3, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->g:I

    .line 1277243
    if-eqz p1, :cond_2

    if-gt p2, v2, :cond_7

    .line 1277244
    :cond_2
    const-string v3, ""

    .line 1277245
    :goto_3
    move-object v2, v3

    .line 1277246
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1277247
    new-array v3, v9, [Ljava/lang/CharSequence;

    aput-object v0, v3, v6

    const-string v0, " \u2022 "

    aput-object v0, v3, v7

    aput-object v1, v3, v8

    invoke-static {v3}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1277248
    :goto_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1277249
    new-array v1, v9, [Ljava/lang/CharSequence;

    aput-object v0, v1, v6

    const-string v0, " \u2022 "

    aput-object v0, v1, v7

    aput-object v2, v1, v8

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 1277250
    :cond_3
    new-array v3, v8, [Ljava/lang/CharSequence;

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    invoke-static {v3}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_4

    .line 1277251
    :cond_4
    new-array v1, v8, [Ljava/lang/CharSequence;

    aput-object v0, v1, v6

    aput-object v2, v1, v7

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 1277252
    :cond_5
    iget-object v1, p0, LX/7xH;->a:Landroid/content/res/Resources;

    const v4, 0x7f081f95

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v5, v10

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1277253
    if-eqz p1, :cond_1

    if-ge p2, v0, :cond_1

    .line 1277254
    invoke-static {p0, v1}, LX/7xH;->b(LX/7xH;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    goto :goto_1

    :cond_6
    iget-object v1, p0, LX/7xH;->d:LX/7xC;

    invoke-virtual {v1, v2, v3}, LX/7xC;->b(J)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_7
    iget-object v3, p0, LX/7xH;->a:Landroid/content/res/Resources;

    const v4, 0x7f081f96

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v5, v10

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, LX/7xH;->b(LX/7xH;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    goto/16 :goto_3
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1277278
    iget-object v0, p0, LX/7xH;->a:Landroid/content/res/Resources;

    const v1, 0x7f081fd9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1277227
    invoke-virtual {p1}, Lcom/facebook/payments/currency/CurrencyAmount;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277228
    iget-object v0, p0, LX/7xH;->a:Landroid/content/res/Resources;

    const v1, 0x7f081fc7    # 1.8094E38f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1277229
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/7xH;->a:Landroid/content/res/Resources;

    const v1, 0x7f081f8d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/7xH;->b:LX/5fv;

    invoke-virtual {v4, p1}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1277221
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277222
    iget-object v0, p0, LX/7xH;->a:Landroid/content/res/Resources;

    const v1, 0x7f0f00e7

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1277223
    :goto_0
    return-object v0

    .line 1277224
    :cond_0
    if-ne p2, v4, :cond_1

    .line 1277225
    iget-object v0, p0, LX/7xH;->a:Landroid/content/res/Resources;

    const v1, 0x7f0f00e8

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1277226
    :cond_1
    iget-object v0, p0, LX/7xH;->a:Landroid/content/res/Resources;

    const v1, 0x7f0f00e8

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    aput-object p1, v2, v4

    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(ZJJLcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1277218
    iget-object v1, p0, LX/7xH;->c:LX/6RZ;

    new-instance v2, Ljava/util/Date;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    const-wide/16 v4, 0x0

    cmp-long v0, p4, v4

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/Date;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, p4, p5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    :goto_0
    invoke-virtual {v1, p1, v2, v0}, LX/6RZ;->b(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1277219
    if-eqz p6, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \u2022 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p6}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0

    .line 1277220
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1277214
    iget-object v0, p0, LX/7xH;->b:LX/5fv;

    invoke-virtual {v0, p1}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1277215
    invoke-virtual {p1}, Lcom/facebook/payments/currency/CurrencyAmount;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277216
    iget-object v0, p0, LX/7xH;->a:Landroid/content/res/Resources;

    const v1, 0x7f081fc7    # 1.8094E38f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1277217
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, LX/7xH;->b(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
