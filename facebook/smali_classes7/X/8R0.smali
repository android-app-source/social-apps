.class public final LX/8R0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Qz;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/AudiencePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/AudiencePickerFragment;)V
    .locals 0

    .prologue
    .line 1343914
    iput-object p1, p0, LX/8R0;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1343907
    iget-object v0, p0, LX/8R0;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v1, p0, LX/8R0;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/model/AudiencePickerModel;->l()LX/8QH;

    move-result-object v1

    .line 1343908
    iput p1, v1, LX/8QH;->e:I

    .line 1343909
    move-object v1, v1

    .line 1343910
    invoke-virtual {v1}, LX/8QH;->a()Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v1

    .line 1343911
    iput-object v1, v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343912
    iget-object v0, p0, LX/8R0;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->c:LX/8Qu;

    const v1, 0x1e38760

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1343913
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1343915
    iget-object v1, p0, LX/8R0;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v0, p0, LX/8R0;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    invoke-virtual {v0}, Lcom/facebook/privacy/model/AudiencePickerModel;->l()LX/8QH;

    move-result-object v2

    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 1343916
    :goto_0
    iput-boolean v0, v2, LX/8QH;->h:Z

    .line 1343917
    move-object v0, v2

    .line 1343918
    invoke-virtual {v0}, LX/8QH;->a()Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v0

    .line 1343919
    iput-object v0, v1, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343920
    iget-object v0, p0, LX/8R0;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->c:LX/8Qu;

    const v1, 0x28c475d2

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1343921
    return-void

    .line 1343922
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1343900
    iget-object v0, p0, LX/8R0;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v1, p0, LX/8R0;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/model/AudiencePickerModel;->l()LX/8QH;

    move-result-object v1

    .line 1343901
    iput-boolean p1, v1, LX/8QH;->k:Z

    .line 1343902
    move-object v1, v1

    .line 1343903
    invoke-virtual {v1}, LX/8QH;->a()Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v1

    .line 1343904
    iput-object v1, v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343905
    iget-object v0, p0, LX/8R0;->a:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->c:LX/8Qu;

    const v1, 0x64c8a418

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1343906
    return-void
.end method
