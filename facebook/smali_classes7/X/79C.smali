.class public final enum LX/79C;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/79C;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/79C;

.field public static final enum CRITIC_REVIEW:LX/79C;

.field public static final enum CRITIC_REVIEW_PUBLISHER_CONTAINER:LX/79C;

.field public static final enum DELETE_DIALOG_CONFIRMATION_BUTTON:LX/79C;

.field public static final enum PLACE_REVIEW_ATTACHMENT:LX/79C;

.field public static final enum REVIEW_CHEVRON:LX/79C;

.field public static final enum REVIEW_CREATOR_NAME:LX/79C;

.field public static final enum REVIEW_CREATOR_PROFILE_PICTURE:LX/79C;

.field public static final enum REVIEW_DELETE_MENU_OPTION:LX/79C;

.field public static final enum REVIEW_EDIT_MENU_OPTION:LX/79C;

.field public static final enum REVIEW_PAGE_NAME:LX/79C;

.field public static final enum REVIEW_REPORT_MENU_OPTION:LX/79C;

.field public static final enum REVIEW_ROW_VIEW:LX/79C;

.field public static final enum REVIEW_TEXT_COLLAPSE:LX/79C;

.field public static final enum REVIEW_TEXT_EXPAND:LX/79C;

.field public static final enum USER_REVIEWS_ATTACHMENT_SAVE:LX/79C;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1173904
    new-instance v0, LX/79C;

    const-string v1, "DELETE_DIALOG_CONFIRMATION_BUTTON"

    invoke-direct {v0, v1, v3}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->DELETE_DIALOG_CONFIRMATION_BUTTON:LX/79C;

    .line 1173905
    new-instance v0, LX/79C;

    const-string v1, "CRITIC_REVIEW_PUBLISHER_CONTAINER"

    invoke-direct {v0, v1, v4}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->CRITIC_REVIEW_PUBLISHER_CONTAINER:LX/79C;

    .line 1173906
    new-instance v0, LX/79C;

    const-string v1, "CRITIC_REVIEW"

    invoke-direct {v0, v1, v5}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->CRITIC_REVIEW:LX/79C;

    .line 1173907
    new-instance v0, LX/79C;

    const-string v1, "REVIEW_CHEVRON"

    invoke-direct {v0, v1, v6}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->REVIEW_CHEVRON:LX/79C;

    .line 1173908
    new-instance v0, LX/79C;

    const-string v1, "REVIEW_CREATOR_NAME"

    invoke-direct {v0, v1, v7}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->REVIEW_CREATOR_NAME:LX/79C;

    .line 1173909
    new-instance v0, LX/79C;

    const-string v1, "REVIEW_CREATOR_PROFILE_PICTURE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->REVIEW_CREATOR_PROFILE_PICTURE:LX/79C;

    .line 1173910
    new-instance v0, LX/79C;

    const-string v1, "REVIEW_DELETE_MENU_OPTION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->REVIEW_DELETE_MENU_OPTION:LX/79C;

    .line 1173911
    new-instance v0, LX/79C;

    const-string v1, "REVIEW_EDIT_MENU_OPTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->REVIEW_EDIT_MENU_OPTION:LX/79C;

    .line 1173912
    new-instance v0, LX/79C;

    const-string v1, "REVIEW_REPORT_MENU_OPTION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->REVIEW_REPORT_MENU_OPTION:LX/79C;

    .line 1173913
    new-instance v0, LX/79C;

    const-string v1, "REVIEW_ROW_VIEW"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->REVIEW_ROW_VIEW:LX/79C;

    .line 1173914
    new-instance v0, LX/79C;

    const-string v1, "REVIEW_PAGE_NAME"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->REVIEW_PAGE_NAME:LX/79C;

    .line 1173915
    new-instance v0, LX/79C;

    const-string v1, "REVIEW_TEXT_EXPAND"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->REVIEW_TEXT_EXPAND:LX/79C;

    .line 1173916
    new-instance v0, LX/79C;

    const-string v1, "REVIEW_TEXT_COLLAPSE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->REVIEW_TEXT_COLLAPSE:LX/79C;

    .line 1173917
    new-instance v0, LX/79C;

    const-string v1, "PLACE_REVIEW_ATTACHMENT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->PLACE_REVIEW_ATTACHMENT:LX/79C;

    .line 1173918
    new-instance v0, LX/79C;

    const-string v1, "USER_REVIEWS_ATTACHMENT_SAVE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/79C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79C;->USER_REVIEWS_ATTACHMENT_SAVE:LX/79C;

    .line 1173919
    const/16 v0, 0xf

    new-array v0, v0, [LX/79C;

    sget-object v1, LX/79C;->DELETE_DIALOG_CONFIRMATION_BUTTON:LX/79C;

    aput-object v1, v0, v3

    sget-object v1, LX/79C;->CRITIC_REVIEW_PUBLISHER_CONTAINER:LX/79C;

    aput-object v1, v0, v4

    sget-object v1, LX/79C;->CRITIC_REVIEW:LX/79C;

    aput-object v1, v0, v5

    sget-object v1, LX/79C;->REVIEW_CHEVRON:LX/79C;

    aput-object v1, v0, v6

    sget-object v1, LX/79C;->REVIEW_CREATOR_NAME:LX/79C;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/79C;->REVIEW_CREATOR_PROFILE_PICTURE:LX/79C;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/79C;->REVIEW_DELETE_MENU_OPTION:LX/79C;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/79C;->REVIEW_EDIT_MENU_OPTION:LX/79C;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/79C;->REVIEW_REPORT_MENU_OPTION:LX/79C;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/79C;->REVIEW_ROW_VIEW:LX/79C;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/79C;->REVIEW_PAGE_NAME:LX/79C;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/79C;->REVIEW_TEXT_EXPAND:LX/79C;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/79C;->REVIEW_TEXT_COLLAPSE:LX/79C;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/79C;->PLACE_REVIEW_ATTACHMENT:LX/79C;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/79C;->USER_REVIEWS_ATTACHMENT_SAVE:LX/79C;

    aput-object v2, v0, v1

    sput-object v0, LX/79C;->$VALUES:[LX/79C;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1173903
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/79C;
    .locals 1

    .prologue
    .line 1173900
    const-class v0, LX/79C;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/79C;

    return-object v0
.end method

.method public static values()[LX/79C;
    .locals 1

    .prologue
    .line 1173902
    sget-object v0, LX/79C;->$VALUES:[LX/79C;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/79C;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1173901
    invoke-virtual {p0}, LX/79C;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
