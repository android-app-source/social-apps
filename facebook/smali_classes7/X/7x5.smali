.class public final LX/7x5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic b:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic c:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

.field public final synthetic d:LX/7x6;


# direct methods
.method public constructor <init>(LX/7x6;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V
    .locals 0

    .prologue
    .line 1276968
    iput-object p1, p0, LX/7x5;->d:LX/7x6;

    iput-object p2, p0, LX/7x5;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p3, p0, LX/7x5;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p4, p0, LX/7x5;->c:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1276969
    iget-object v0, p0, LX/7x5;->d:LX/7x6;

    iget-object v0, v0, LX/7x6;->e:LX/03V;

    const-class v1, LX/7x6;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1276970
    iget-object v0, p0, LX/7x5;->c:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    .line 1276971
    iget-object v1, v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-virtual {v1}, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a()LX/7wo;

    move-result-object v1

    sget-object p0, LX/7wp;->ERROR:LX/7wp;

    invoke-interface {v1, p0}, LX/7wo;->a(LX/7wp;)LX/7wo;

    move-result-object v1

    invoke-interface {v1}, LX/7wo;->a()Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1276972
    invoke-static {v0}, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->r(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V

    .line 1276973
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1276974
    :try_start_0
    iget-object v0, p0, LX/7x5;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    const v1, -0x5e7086f9

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1276975
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->s()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->s()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1276976
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Got empty result from GraphQL"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/7x5;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1276977
    :goto_0
    return-void

    .line 1276978
    :catch_0
    move-exception v0

    .line 1276979
    invoke-virtual {p0, v0}, LX/7x5;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1276980
    :catch_1
    move-exception v0

    .line 1276981
    invoke-virtual {p0, v0}, LX/7x5;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1276982
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/7x5;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    const v2, -0xaf2761e

    invoke-static {v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/auth/pin/model/PaymentPin;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_3

    .line 1276983
    iget-object v2, p0, LX/7x5;->c:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    .line 1276984
    invoke-static {v2, v0, v1}, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->b(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;Lcom/facebook/payments/auth/pin/model/PaymentPin;)Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-result-object v3

    iput-object v3, v2, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1276985
    iget-object v3, v2, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v3, v3, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    const/4 p1, 0x0

    .line 1276986
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v1

    move v0, p1

    :goto_1
    if-ge v0, v1, :cond_4

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    .line 1276987
    iget-object p0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->i:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p0}, Lcom/facebook/payments/currency/CurrencyAmount;->e()Z

    move-result p0

    if-nez p0, :cond_3

    move p0, p1

    .line 1276988
    :goto_2
    move v3, p0

    .line 1276989
    iput-boolean v3, v2, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->p:Z

    .line 1276990
    invoke-static {v2}, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->r(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V

    .line 1276991
    invoke-static {v2}, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->o(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V

    .line 1276992
    iget-object v3, v2, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-boolean v3, v3, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->p:Z

    if-eqz v3, :cond_2

    .line 1276993
    invoke-static {v2}, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->k(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->a(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;Z)V

    .line 1276994
    :cond_2
    goto :goto_0

    .line 1276995
    :catch_2
    move-exception v0

    .line 1276996
    invoke-virtual {p0, v0}, LX/7x5;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1276997
    :catch_3
    move-exception v0

    .line 1276998
    invoke-virtual {p0, v0}, LX/7x5;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1276999
    :cond_3
    add-int/lit8 p0, v0, 0x1

    move v0, p0

    goto :goto_1

    .line 1277000
    :cond_4
    const/4 p0, 0x1

    goto :goto_2
.end method
