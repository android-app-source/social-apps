.class public LX/790;
.super Landroid/widget/ListView;
.source ""


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/78n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1173739
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 1173740
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1173741
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1173742
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1173743
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1173744
    return-void
.end method


# virtual methods
.method public setForkOptions(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/78n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1173745
    iput-object p1, p0, LX/790;->a:Ljava/util/List;

    .line 1173746
    new-instance v0, LX/78z;

    invoke-direct {v0, p0}, LX/78z;-><init>(LX/790;)V

    invoke-virtual {p0, v0}, LX/790;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1173747
    return-void
.end method
