.class public final LX/7HE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/emoji/model/Emoji;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/7HD;


# direct methods
.method private constructor <init>(IILjava/util/List;LX/7HD;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/emoji/model/Emoji;",
            ">;",
            "LX/7HD;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1190501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1190502
    iput p1, p0, LX/7HE;->a:I

    .line 1190503
    iput p2, p0, LX/7HE;->b:I

    .line 1190504
    iput-object p3, p0, LX/7HE;->c:Ljava/util/List;

    .line 1190505
    iput-object p4, p0, LX/7HE;->d:LX/7HD;

    .line 1190506
    return-void
.end method

.method public static a(IILjava/util/List;)LX/7HE;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/emoji/model/Emoji;",
            ">;)",
            "LX/7HE;"
        }
    .end annotation

    .prologue
    .line 1190507
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1190508
    new-instance v0, LX/7HE;

    sget-object v1, LX/7HD;->PREDEFINED:LX/7HD;

    invoke-direct {v0, p0, p1, p2, v1}, LX/7HE;-><init>(IILjava/util/List;LX/7HD;)V

    return-object v0
.end method
