.class public LX/7iV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/17V;

.field private final c:LX/1g7;


# direct methods
.method public constructor <init>(LX/0Zb;LX/17V;LX/1g7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1227663
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1227664
    iput-object p1, p0, LX/7iV;->a:LX/0Zb;

    .line 1227665
    iput-object p2, p0, LX/7iV;->b:LX/17V;

    .line 1227666
    iput-object p3, p0, LX/7iV;->c:LX/1g7;

    .line 1227667
    return-void
.end method

.method public static a(LX/0QB;)LX/7iV;
    .locals 1

    .prologue
    .line 1227662
    invoke-static {p0}, LX/7iV;->b(LX/0QB;)LX/7iV;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/162;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1227658
    if-eqz p0, :cond_0

    invoke-virtual {p0, v0}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1227659
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LX/0lF;->a(I)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1227660
    sget-object v1, LX/7iL;->TOP_LEVEL_POST_ID:LX/7iL;

    iget-object v1, v1, LX/7iL;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1227661
    :goto_0
    return-object v0

    :catch_0
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1227651
    const-string v0, ""

    .line 1227652
    if-eqz p0, :cond_0

    .line 1227653
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v0

    .line 1227654
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x25d6af

    if-ne v1, v2, :cond_0

    .line 1227655
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "shop"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1227656
    :cond_0
    return-object v0

    .line 1227657
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/7iV;ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;LX/7iM;LX/7iP;LX/7iN;)V
    .locals 4

    .prologue
    .line 1227624
    iget-object v0, p0, LX/7iV;->c:LX/1g7;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p5, p7, p6, v1}, LX/1g7;->a(LX/7iM;LX/7iN;LX/7iP;Ljava/lang/Long;)LX/7iT;

    move-result-object v0

    .line 1227625
    iput-object p3, v0, LX/7iT;->n:Ljava/lang/String;

    .line 1227626
    iput-object p4, v0, LX/7iT;->o:Ljava/lang/String;

    .line 1227627
    const v1, 0xa7c5482

    if-ne p1, v1, :cond_1

    .line 1227628
    invoke-virtual {v0, p2}, LX/7iT;->c(Ljava/lang/Long;)V

    .line 1227629
    :cond_0
    :goto_0
    invoke-virtual {v0}, LX/7iT;->b()V

    .line 1227630
    return-void

    .line 1227631
    :cond_1
    const v1, 0x25d6af

    if-ne p1, v1, :cond_0

    .line 1227632
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7iT;->k:Ljava/lang/String;

    .line 1227633
    goto :goto_0
.end method

.method private static a(LX/7iV;Landroid/view/View;Ljava/lang/String;LX/162;Z)V
    .locals 2

    .prologue
    .line 1227646
    iget-object v0, p0, LX/7iV;->b:LX/17V;

    const-string v1, "native_newsfeed"

    invoke-virtual {v0, p2, p4, p3, v1}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1227647
    invoke-static {v0}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1227648
    invoke-static {v0, p1}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 1227649
    :cond_0
    iget-object v1, p0, LX/7iV;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1227650
    return-void
.end method

.method public static b(LX/0QB;)LX/7iV;
    .locals 4

    .prologue
    .line 1227644
    new-instance v3, LX/7iV;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v1

    check-cast v1, LX/17V;

    const-class v2, LX/1g7;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/1g7;

    invoke-direct {v3, v0, v1, v2}, LX/7iV;-><init>(LX/0Zb;LX/17V;LX/1g7;)V

    .line 1227645
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLNode;LX/162;LX/7iM;LX/7iP;LX/7iN;ZLjava/lang/String;)V
    .locals 8

    .prologue
    .line 1227640
    invoke-static {p2}, LX/7iV;->a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0, p3, p7}, LX/7iV;->a(LX/7iV;Landroid/view/View;Ljava/lang/String;LX/162;Z)V

    .line 1227641
    if-eqz p2, :cond_0

    .line 1227642
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {p3}, LX/7iV;->a(LX/162;)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object/from16 v3, p8

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-static/range {v0 .. v7}, LX/7iV;->a(LX/7iV;ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;LX/7iM;LX/7iP;LX/7iN;)V

    .line 1227643
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;LX/162;LX/7iM;LX/7iP;LX/7iN;ZLjava/lang/String;)V
    .locals 11

    .prologue
    .line 1227634
    const-string v2, ""

    .line 1227635
    if-eqz p2, :cond_0

    .line 1227636
    invoke-virtual {p2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v10

    .line 1227637
    invoke-virtual {p2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    invoke-virtual {p2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p3}, LX/7iV;->a(LX/162;)Ljava/lang/String;

    move-result-object v6

    move-object v2, p0

    move-object/from16 v5, p8

    move-object v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    invoke-static/range {v2 .. v9}, LX/7iV;->a(LX/7iV;ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;LX/7iM;LX/7iP;LX/7iN;)V

    move-object v2, v10

    .line 1227638
    :cond_0
    move/from16 v0, p7

    invoke-static {p0, p1, v2, p3, v0}, LX/7iV;->a(LX/7iV;Landroid/view/View;Ljava/lang/String;LX/162;Z)V

    .line 1227639
    return-void
.end method
