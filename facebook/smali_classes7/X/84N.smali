.class public final LX/84N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/2h7;

.field public final synthetic c:LX/2hX;


# direct methods
.method public constructor <init>(LX/2hX;JLX/2h7;)V
    .locals 0

    .prologue
    .line 1290907
    iput-object p1, p0, LX/84N;->c:LX/2hX;

    iput-wide p2, p0, LX/84N;->a:J

    iput-object p4, p0, LX/84N;->b:LX/2h7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    .line 1290908
    iget-object v0, p0, LX/84N;->c:LX/2hX;

    iget-object v1, v0, LX/2hY;->a:LX/2dj;

    iget-wide v2, p0, LX/84N;->a:J

    iget-object v0, p0, LX/84N;->b:LX/2h7;

    iget-object v4, v0, LX/2h7;->friendRequestHowFound:LX/2h8;

    iget-object v0, p0, LX/84N;->b:LX/2h7;

    iget-object v5, v0, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, LX/2dj;->a(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1290909
    iget-object v1, p0, LX/84N;->c:LX/2hX;

    iget-wide v2, p0, LX/84N;->a:J

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v5, 0x1

    invoke-static {v1, v2, v3, v4, v5}, LX/2hX;->b(LX/2hX;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 1290910
    iget-object v1, p0, LX/84N;->c:LX/2hX;

    iget-object v1, v1, LX/2hX;->d:LX/0Sh;

    new-instance v2, LX/84M;

    invoke-direct {v2, p0}, LX/84M;-><init>(LX/84N;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1290911
    return-void
.end method
