.class public final enum LX/89g;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/89g;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/89g;

.field public static final enum NOTIFICATION_CACHE:LX/89g;

.field public static final enum OFFLINE_VIDEO_CACHE:LX/89g;

.field public static final enum VIDEOHOME_NOTIFICATION_CACHE:LX/89g;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1305093
    new-instance v0, LX/89g;

    const-string v1, "NOTIFICATION_CACHE"

    invoke-direct {v0, v1, v2}, LX/89g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89g;->NOTIFICATION_CACHE:LX/89g;

    .line 1305094
    new-instance v0, LX/89g;

    const-string v1, "VIDEOHOME_NOTIFICATION_CACHE"

    invoke-direct {v0, v1, v3}, LX/89g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89g;->VIDEOHOME_NOTIFICATION_CACHE:LX/89g;

    .line 1305095
    new-instance v0, LX/89g;

    const-string v1, "OFFLINE_VIDEO_CACHE"

    invoke-direct {v0, v1, v4}, LX/89g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89g;->OFFLINE_VIDEO_CACHE:LX/89g;

    .line 1305096
    const/4 v0, 0x3

    new-array v0, v0, [LX/89g;

    sget-object v1, LX/89g;->NOTIFICATION_CACHE:LX/89g;

    aput-object v1, v0, v2

    sget-object v1, LX/89g;->VIDEOHOME_NOTIFICATION_CACHE:LX/89g;

    aput-object v1, v0, v3

    sget-object v1, LX/89g;->OFFLINE_VIDEO_CACHE:LX/89g;

    aput-object v1, v0, v4

    sput-object v0, LX/89g;->$VALUES:[LX/89g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1305099
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/89g;
    .locals 1

    .prologue
    .line 1305098
    const-class v0, LX/89g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/89g;

    return-object v0
.end method

.method public static values()[LX/89g;
    .locals 1

    .prologue
    .line 1305097
    sget-object v0, LX/89g;->$VALUES:[LX/89g;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/89g;

    return-object v0
.end method
