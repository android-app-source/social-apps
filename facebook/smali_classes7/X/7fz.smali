.class public final enum LX/7fz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7fz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7fz;

.field public static final enum DETACH_ON_SWITCH:LX/7fz;

.field public static final enum HIDE_ON_SWITCH:LX/7fz;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1223767
    new-instance v0, LX/7fz;

    const-string v1, "DETACH_ON_SWITCH"

    invoke-direct {v0, v1, v2}, LX/7fz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7fz;->DETACH_ON_SWITCH:LX/7fz;

    .line 1223768
    new-instance v0, LX/7fz;

    const-string v1, "HIDE_ON_SWITCH"

    invoke-direct {v0, v1, v3}, LX/7fz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7fz;->HIDE_ON_SWITCH:LX/7fz;

    .line 1223769
    const/4 v0, 0x2

    new-array v0, v0, [LX/7fz;

    sget-object v1, LX/7fz;->DETACH_ON_SWITCH:LX/7fz;

    aput-object v1, v0, v2

    sget-object v1, LX/7fz;->HIDE_ON_SWITCH:LX/7fz;

    aput-object v1, v0, v3

    sput-object v0, LX/7fz;->$VALUES:[LX/7fz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1223770
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7fz;
    .locals 1

    .prologue
    .line 1223766
    const-class v0, LX/7fz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7fz;

    return-object v0
.end method

.method public static values()[LX/7fz;
    .locals 1

    .prologue
    .line 1223765
    sget-object v0, LX/7fz;->$VALUES:[LX/7fz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7fz;

    return-object v0
.end method
