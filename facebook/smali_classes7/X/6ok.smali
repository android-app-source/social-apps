.class public LX/6ok;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0Tz;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/6ok;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1148456
    const-class v0, LX/6ok;

    sput-object v0, LX/6ok;->a:Ljava/lang/Class;

    .line 1148457
    new-instance v0, LX/6oj;

    invoke-direct {v0}, LX/6oj;-><init>()V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/6ok;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1148458
    const-string v0, "payment_pin"

    const/4 v1, 0x1

    sget-object v2, LX/6ok;->b:LX/0Px;

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 1148459
    return-void
.end method

.method public static a(LX/0QB;)LX/6ok;
    .locals 3

    .prologue
    .line 1148460
    sget-object v0, LX/6ok;->c:LX/6ok;

    if-nez v0, :cond_1

    .line 1148461
    const-class v1, LX/6ok;

    monitor-enter v1

    .line 1148462
    :try_start_0
    sget-object v0, LX/6ok;->c:LX/6ok;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1148463
    if-eqz v2, :cond_0

    .line 1148464
    :try_start_1
    new-instance v0, LX/6ok;

    invoke-direct {v0}, LX/6ok;-><init>()V

    .line 1148465
    move-object v0, v0

    .line 1148466
    sput-object v0, LX/6ok;->c:LX/6ok;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1148467
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1148468
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1148469
    :cond_1
    sget-object v0, LX/6ok;->c:LX/6ok;

    return-object v0

    .line 1148470
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1148471
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 1148472
    sget-object v0, LX/6ok;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p2, :cond_0

    sget-object v0, LX/6ok;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tz;

    .line 1148473
    iget-object p3, v0, LX/0Tz;->a:Ljava/lang/String;

    move-object v0, p3

    .line 1148474
    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const p3, 0x397b26f8

    invoke-static {p3}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x32659a8f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1148475
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1148476
    :cond_0
    invoke-virtual {p0, p1}, LX/0Tv;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1148477
    return-void
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 1148478
    return-void
.end method
