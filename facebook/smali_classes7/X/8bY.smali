.class public final enum LX/8bY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8bY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8bY;

.field public static final enum DP:LX/8bY;

.field public static final enum PIXEL:LX/8bY;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1372136
    new-instance v0, LX/8bY;

    const-string v1, "PIXEL"

    invoke-direct {v0, v1, v2}, LX/8bY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8bY;->PIXEL:LX/8bY;

    .line 1372137
    new-instance v0, LX/8bY;

    const-string v1, "DP"

    invoke-direct {v0, v1, v3}, LX/8bY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8bY;->DP:LX/8bY;

    .line 1372138
    const/4 v0, 0x2

    new-array v0, v0, [LX/8bY;

    sget-object v1, LX/8bY;->PIXEL:LX/8bY;

    aput-object v1, v0, v2

    sget-object v1, LX/8bY;->DP:LX/8bY;

    aput-object v1, v0, v3

    sput-object v0, LX/8bY;->$VALUES:[LX/8bY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1372139
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8bY;
    .locals 1

    .prologue
    .line 1372140
    const-class v0, LX/8bY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8bY;

    return-object v0
.end method

.method public static values()[LX/8bY;
    .locals 1

    .prologue
    .line 1372141
    sget-object v0, LX/8bY;->$VALUES:[LX/8bY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8bY;

    return-object v0
.end method
