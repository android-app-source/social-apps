.class public final enum LX/7gq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gq;

.field public static final enum FAILED:LX/7gq;

.field public static final enum SEEN:LX/7gq;

.field public static final enum SENDING:LX/7gq;

.field public static final enum SENT:LX/7gq;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1224965
    new-instance v0, LX/7gq;

    const-string v1, "SENT"

    invoke-direct {v0, v1, v2}, LX/7gq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gq;->SENT:LX/7gq;

    .line 1224966
    new-instance v0, LX/7gq;

    const-string v1, "SEEN"

    invoke-direct {v0, v1, v3}, LX/7gq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gq;->SEEN:LX/7gq;

    .line 1224967
    new-instance v0, LX/7gq;

    const-string v1, "SENDING"

    invoke-direct {v0, v1, v4}, LX/7gq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gq;->SENDING:LX/7gq;

    .line 1224968
    new-instance v0, LX/7gq;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, LX/7gq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gq;->FAILED:LX/7gq;

    .line 1224969
    const/4 v0, 0x4

    new-array v0, v0, [LX/7gq;

    sget-object v1, LX/7gq;->SENT:LX/7gq;

    aput-object v1, v0, v2

    sget-object v1, LX/7gq;->SEEN:LX/7gq;

    aput-object v1, v0, v3

    sget-object v1, LX/7gq;->SENDING:LX/7gq;

    aput-object v1, v0, v4

    sget-object v1, LX/7gq;->FAILED:LX/7gq;

    aput-object v1, v0, v5

    sput-object v0, LX/7gq;->$VALUES:[LX/7gq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1224970
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gq;
    .locals 1

    .prologue
    .line 1224971
    const-class v0, LX/7gq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gq;

    return-object v0
.end method

.method public static values()[LX/7gq;
    .locals 1

    .prologue
    .line 1224972
    sget-object v0, LX/7gq;->$VALUES:[LX/7gq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gq;

    return-object v0
.end method
