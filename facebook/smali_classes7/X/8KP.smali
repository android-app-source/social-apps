.class public final LX/8KP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V
    .locals 0

    .prologue
    .line 1330012
    iput-object p1, p0, LX/8KP;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1330013
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1330014
    iget-object v0, p0, LX/8KP;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v1, p0, LX/8KP;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v1, v1, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    const-string v2, "onCancelRequest"

    const-string v3, "ensureUploadAlive"

    .line 1330015
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->a$redex0(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;Ljava/lang/String;)V

    .line 1330016
    iget-object v0, p0, LX/8KP;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v0, v0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->q:LX/1EZ;

    iget-object v1, p0, LX/8KP;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v1, v1, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    sget-object v2, LX/8Kx;->UserRetry:LX/8Kx;

    const-string v3, "Continue, not cancel"

    invoke-virtual {v0, v1, v2, v3}, LX/1EZ;->c(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V

    .line 1330017
    iget-object v0, p0, LX/8KP;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    invoke-virtual {v0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->finish()V

    .line 1330018
    return-void
.end method
