.class public final LX/8MY;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "LX/8Kb;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1334501
    iput-object p1, p0, LX/8MY;->a:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    invoke-direct {p0}, LX/8KR;-><init>()V

    .line 1334502
    iput-object p2, p0, LX/8MY;->b:Ljava/lang/String;

    .line 1334503
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/8Kb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1334504
    const-class v0, LX/8Kb;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1334505
    check-cast p1, LX/8Kb;

    .line 1334506
    iget-object v0, p1, LX/8Kb;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1334507
    iget-object v1, p0, LX/8MY;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1334508
    iget-object v0, p0, LX/8MY;->a:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder$CompostSingleUploadBusSubscriber$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder$CompostSingleUploadBusSubscriber$1;-><init>(LX/8MY;LX/8Kb;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1334509
    :cond_0
    return-void
.end method
