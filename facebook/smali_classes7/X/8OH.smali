.class public final LX/8OH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8OG;


# instance fields
.field private final a:LX/8O7;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(LX/8O7;)V
    .locals 0

    .prologue
    .line 1338552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1338553
    iput-object p1, p0, LX/8OH;->a:LX/8O7;

    .line 1338554
    return-void
.end method

.method private static a(IIF)I
    .locals 2

    .prologue
    .line 1338555
    add-int/lit8 v0, p0, -0x1

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, p2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    add-float/2addr v0, v1

    .line 1338556
    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    int-to-float v1, p1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(F)V
    .locals 4

    .prologue
    .line 1338557
    iget v0, p0, LX/8OH;->b:I

    iget v1, p0, LX/8OH;->c:I

    invoke-static {v0, v1, p1}, LX/8OH;->a(IIF)I

    move-result v0

    .line 1338558
    iget-object v1, p0, LX/8OH;->a:LX/8O7;

    iget v2, p0, LX/8OH;->b:I

    iget v3, p0, LX/8OH;->c:I

    invoke-virtual {v1, v2, v3, v0}, LX/8O7;->a(III)V

    .line 1338559
    return-void
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 1338560
    iput p1, p0, LX/8OH;->b:I

    .line 1338561
    iput p2, p0, LX/8OH;->c:I

    .line 1338562
    iget v0, p0, LX/8OH;->b:I

    iget v1, p0, LX/8OH;->c:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/8OH;->a(IIF)I

    move-result v0

    .line 1338563
    iget-object v1, p0, LX/8OH;->a:LX/8O7;

    iget v2, p0, LX/8OH;->b:I

    iget v3, p0, LX/8OH;->c:I

    invoke-virtual {v1, v2, v3, v0}, LX/8O7;->a(III)V

    .line 1338564
    return-void
.end method
