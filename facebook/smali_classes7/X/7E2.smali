.class public LX/7E2;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static b:Ljava/lang/String;

.field public static c:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1184192
    const-class v0, LX/7E2;

    sput-object v0, LX/7E2;->a:Ljava/lang/Class;

    .line 1184193
    const-string v0, ""

    sput-object v0, LX/7E2;->b:Ljava/lang/String;

    .line 1184194
    const/4 v0, 0x0

    sput v0, LX/7E2;->c:F

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1184190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184191
    return-void
.end method

.method public static a(Ljava/lang/String;LX/0W3;)LX/7E1;
    .locals 6

    .prologue
    .line 1184186
    invoke-static {p0, p1}, LX/7E2;->b(Ljava/lang/String;LX/0W3;)Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-result-object v1

    .line 1184187
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->a()D

    move-result-wide v2

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    cmpl-double v0, v2, v4

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    .line 1184188
    :goto_0
    new-instance v2, LX/7E1;

    invoke-direct {v2, v1, v0}, LX/7E1;-><init>(Lcom/facebook/bitmaps/SphericalPhotoMetadata;Z)V

    return-object v2

    .line 1184189
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/media/ExifInterface;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1184181
    if-nez p0, :cond_1

    .line 1184182
    const-string v0, ""

    .line 1184183
    :cond_0
    :goto_0
    return-object v0

    .line 1184184
    :cond_1
    invoke-virtual {p0, p1}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1184185
    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0
.end method

.method private static b(Landroid/media/ExifInterface;Ljava/lang/String;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1184177
    if-nez p0, :cond_1

    .line 1184178
    :cond_0
    :goto_0
    return v0

    .line 1184179
    :cond_1
    invoke-virtual {p0, p1}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1184180
    if-eqz v1, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;LX/0W3;)Lcom/facebook/bitmaps/SphericalPhotoMetadata;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 1184137
    :try_start_0
    new-instance v0, Landroid/media/ExifInterface;

    invoke-direct {v0, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v5, v0

    .line 1184138
    :goto_0
    const-string v0, "Make"

    invoke-static {v5, v0}, LX/7E2;->a(Landroid/media/ExifInterface;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1184139
    const-string v1, "Model"

    invoke-static {v5, v1}, LX/7E2;->a(Landroid/media/ExifInterface;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1184140
    const-string v2, "FocalLength"

    invoke-static {v5, v2}, LX/7E2;->a(Landroid/media/ExifInterface;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1184141
    const-string v3, "ImageWidth"

    invoke-static {v5, v3}, LX/7E2;->b(Landroid/media/ExifInterface;Ljava/lang/String;)I

    move-result v3

    .line 1184142
    const-string v4, "ImageLength"

    invoke-static {v5, v4}, LX/7E2;->b(Landroid/media/ExifInterface;Ljava/lang/String;)I

    move-result v4

    .line 1184143
    const-string v6, "Orientation"

    invoke-static {v5, v6}, LX/7E2;->b(Landroid/media/ExifInterface;Ljava/lang/String;)I

    move-result v6

    .line 1184144
    invoke-static {p0}, LX/43P;->a(Ljava/lang/String;)[B

    move-result-object v7

    .line 1184145
    if-eqz v7, :cond_3

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v7}, Ljava/lang/String;-><init>([B)V

    .line 1184146
    :goto_1
    new-instance v8, Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    invoke-direct {v8}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;-><init>()V

    .line 1184147
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    long-to-float v10, v10

    .line 1184148
    sget-object v11, LX/7E2;->b:Ljava/lang/String;

    const-string v12, ""

    if-eq v11, v12, :cond_0

    sget v11, LX/7E2;->c:F

    sub-float/2addr v10, v11

    const v11, 0x49dbba00    # 1800000.0f

    cmpl-float v10, v10, v11

    if-lez v10, :cond_1

    .line 1184149
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    long-to-float v10, v10

    sput v10, LX/7E2;->c:F

    .line 1184150
    invoke-static {p1}, LX/7E2;->b(LX/0W3;)Ljava/lang/String;

    move-result-object v10

    sput-object v10, LX/7E2;->b:Ljava/lang/String;

    .line 1184151
    :cond_1
    sget-object v10, LX/7E2;->b:Ljava/lang/String;

    move-object v7, v10

    .line 1184152
    invoke-static/range {v0 .. v8}, Lcom/facebook/bitmaps/NativeSphericalProcessing;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ILjava/lang/String;Lcom/facebook/bitmaps/SphericalPhotoMetadata;)Z

    move-result v0

    .line 1184153
    if-eqz v0, :cond_2

    .line 1184154
    invoke-virtual {v8}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->a()D

    move-result-wide v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_4

    const/4 v0, 0x1

    .line 1184155
    :cond_2
    :goto_2
    if-eqz v0, :cond_5

    :goto_3
    return-object v8

    .line 1184156
    :catch_0
    move-exception v0

    .line 1184157
    sget-object v1, LX/7E2;->a:Ljava/lang/Class;

    const-string v2, "Error reading EXIF metadata from the file"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v5, v9

    goto :goto_0

    .line 1184158
    :cond_3
    const-string v5, ""

    goto :goto_1

    .line 1184159
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    move-object v8, v9

    .line 1184160
    goto :goto_3
.end method

.method public static b(LX/0W3;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1184161
    :try_start_0
    sget-wide v0, LX/0X5;->en:J

    invoke-interface {p0, v0, v1}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v0

    .line 1184162
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 1184163
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1184164
    new-instance v2, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v2, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1184165
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 1184166
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 1184167
    const/16 v4, 0x1000

    new-array v4, v4, [C

    .line 1184168
    :goto_0
    invoke-virtual {v3, v4}, Ljava/io/InputStreamReader;->read([C)I

    move-result v5

    const/4 p0, -0x1

    if-eq v5, p0, :cond_0

    .line 1184169
    const/4 p0, 0x0

    invoke-virtual {v0, v4, p0, v5}, Ljava/io/StringWriter;->write([CII)V

    goto :goto_0

    .line 1184170
    :cond_0
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1184171
    invoke-virtual {v0}, Ljava/io/StringWriter;->close()V

    .line 1184172
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    .line 1184173
    move-object v0, v4

    .line 1184174
    invoke-virtual {v2}, Ljava/util/zip/GZIPInputStream;->close()V

    .line 1184175
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1184176
    :goto_1
    return-object v0

    :catch_0
    const-string v0, ""

    goto :goto_1
.end method
