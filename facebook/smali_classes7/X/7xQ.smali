.class public LX/7xQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/7xH;


# direct methods
.method private constructor <init>(LX/7xH;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1277349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1277350
    iput-object p1, p0, LX/7xQ;->a:LX/7xH;

    .line 1277351
    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;Ljava/util/Map;I)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/7xN;",
            ">;I)",
            "Lcom/facebook/payments/currency/CurrencyAmount;"
        }
    .end annotation

    .prologue
    .line 1277352
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel$AmountModel;

    move-result-object v1

    .line 1277353
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel$AmountModel;->j()I

    move-result v0

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel$AmountModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel$AmountModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, LX/7xE;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    .line 1277354
    sget-object v2, LX/7xM;->a:[I

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1277355
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel$AmountModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    :goto_0
    :pswitch_0
    move-object v2, v0

    .line 1277356
    invoke-virtual {v2}, Lcom/facebook/payments/currency/CurrencyAmount;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v2

    .line 1277357
    :goto_1
    return-object v0

    .line 1277358
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1277359
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    .line 1277360
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;->j()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/7xN;

    iget-object v1, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;

    iget-object v0, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    invoke-direct {v4, v1, v0}, LX/7xN;-><init>(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;Lcom/facebook/payments/currency/CurrencyAmount;)V

    invoke-interface {p1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    move-object v0, v2

    .line 1277361
    goto :goto_1

    .line 1277362
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/7xN;

    invoke-direct {v1, p0, v2}, LX/7xN;-><init>(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;Lcom/facebook/payments/currency/CurrencyAmount;)V

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1277363
    :pswitch_1
    invoke-virtual {v0, p2}, Lcom/facebook/payments/currency/CurrencyAmount;->a(I)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Ljava/util/Map;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/7xN;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/7xN;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1277364
    new-instance v0, Ljava/util/LinkedList;

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 1277365
    new-instance v1, LX/7xL;

    invoke-direct {v1}, LX/7xL;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1277366
    return-object v0
.end method

.method public static b(LX/0QB;)LX/7xQ;
    .locals 2

    .prologue
    .line 1277367
    new-instance v1, LX/7xQ;

    invoke-static {p0}, LX/7xH;->a(LX/0QB;)LX/7xH;

    move-result-object v0

    check-cast v0, LX/7xH;

    invoke-direct {v1, v0}, LX/7xQ;-><init>(LX/7xH;)V

    .line 1277368
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)LX/7xP;
    .locals 1

    .prologue
    .line 1277369
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1277370
    invoke-virtual {p0, p1, v0}, LX/7xQ;->a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;Ljava/util/List;)LX/7xP;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;Ljava/util/List;)LX/7xP;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;",
            "Ljava/util/List",
            "<",
            "LX/7xO;",
            ">;)",
            "LX/7xP;"
        }
    .end annotation

    .prologue
    .line 1277371
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1277372
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 1277373
    const/4 v4, 0x0

    .line 1277374
    new-instance v2, Lcom/facebook/payments/currency/CurrencyAmount;

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->d:Ljava/lang/String;

    sget-object v3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-direct {v2, v1, v3}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 1277375
    new-instance v3, Lcom/facebook/payments/currency/CurrencyAmount;

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->d:Ljava/lang/String;

    sget-object v5, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-direct {v3, v1, v5}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 1277376
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    const/4 v1, 0x0

    move v6, v1

    move v5, v4

    :goto_0
    if-ge v6, v11, :cond_2

    invoke-virtual {v10, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    .line 1277377
    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->d:Ljava/lang/String;

    iget-object v12, v1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->i:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v12}, Lcom/facebook/payments/currency/CurrencyAmount;->b()Ljava/math/BigDecimal;

    move-result-object v12

    invoke-direct {v4, v7, v12}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 1277378
    iget v12, v1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    .line 1277379
    add-int v7, v5, v12

    .line 1277380
    if-eqz v12, :cond_6

    .line 1277381
    invoke-virtual {v4, v12}, Lcom/facebook/payments/currency/CurrencyAmount;->a(I)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v13

    .line 1277382
    iget-object v14, v1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->m:LX/0Px;

    invoke-virtual {v14}, LX/0Px;->size()I

    move-result v15

    const/4 v4, 0x0

    move-object v5, v2

    :goto_1
    if-ge v4, v15, :cond_0

    invoke-virtual {v14, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;

    .line 1277383
    invoke-static {v2, v9, v12}, LX/7xQ;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;Ljava/util/Map;I)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v2

    .line 1277384
    invoke-virtual {v5, v2}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v5

    .line 1277385
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 1277386
    :cond_0
    invoke-virtual {v5, v13}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v4

    .line 1277387
    invoke-virtual {v3, v13}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v2

    .line 1277388
    sget-object v3, LX/7xO;->GENERATE_SUBTOTAL:LX/7xO;

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1277389
    move-object/from16 v0, p0

    iget-object v3, v0, LX/7xQ;->a:LX/7xH;

    iget-object v5, v1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->c:Ljava/lang/String;

    invoke-virtual {v3, v5, v12}, LX/7xH;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 1277390
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7xQ;->a:LX/7xH;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->i:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1, v12}, Lcom/facebook/payments/currency/CurrencyAmount;->a(I)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/7xH;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v1

    .line 1277391
    new-instance v5, LX/73b;

    invoke-direct {v5, v3, v1}, LX/73b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    :cond_1
    move-object v1, v2

    move-object v2, v4

    .line 1277392
    :goto_2
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v5, v7

    move-object v3, v1

    goto :goto_0

    .line 1277393
    :cond_2
    if-lez v5, :cond_3

    .line 1277394
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->t:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v1, 0x0

    move v4, v1

    :goto_3
    if-ge v4, v7, :cond_3

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;

    .line 1277395
    invoke-static {v1, v9, v5}, LX/7xQ;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;Ljava/util/Map;I)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    .line 1277396
    invoke-virtual {v2, v1}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v2

    .line 1277397
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    :cond_3
    move-object v4, v2

    .line 1277398
    invoke-static {v9}, LX/7xQ;->a(Ljava/util/Map;)Ljava/util/List;

    move-result-object v1

    .line 1277399
    sget-object v2, LX/7xO;->GENERATE_SUBTOTAL:LX/7xO;

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v4, v3}, Lcom/facebook/payments/currency/CurrencyAmount;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1277400
    move-object/from16 v0, p0

    iget-object v2, v0, LX/7xQ;->a:LX/7xH;

    invoke-virtual {v2}, LX/7xH;->a()Ljava/lang/String;

    move-result-object v2

    .line 1277401
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7xQ;->a:LX/7xH;

    invoke-virtual {v5, v3}, LX/7xH;->b(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v3

    .line 1277402
    new-instance v5, LX/73b;

    invoke-direct {v5, v2, v3}, LX/73b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1277403
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7xN;

    .line 1277404
    new-instance v5, LX/73b;

    iget-object v2, v1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, LX/7xQ;->a:LX/7xH;

    iget-object v1, v1, LX/3rL;->b:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v6, v1}, LX/7xH;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v2, v1}, LX/73b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 1277405
    :cond_5
    new-instance v1, LX/7xP;

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v4, v2}, LX/7xP;-><init>(Lcom/facebook/payments/currency/CurrencyAmount;LX/0Px;)V

    return-object v1

    :cond_6
    move-object v1, v3

    goto/16 :goto_2
.end method
