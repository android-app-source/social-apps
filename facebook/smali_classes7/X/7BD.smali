.class public LX/7BD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1177979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;)LX/0Px;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1177980
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1177981
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;

    .line 1177982
    iget-object v3, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->externalUrl:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 1177983
    iget-object v4, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->resultType:Ljava/lang/String;

    if-eqz v4, :cond_4

    iget-object v4, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->resultType:Ljava/lang/String;

    const-string v5, "keywords_v2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->resultType:Ljava/lang/String;

    const-string v5, "keywords"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1177984
    :cond_1
    const/4 v7, 0x0

    .line 1177985
    iget-object v4, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->photoUri:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    sget-object v4, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 1177986
    :goto_1
    iget-object v5, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->nativeAndroidUrl:Ljava/lang/String;

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    sget-object v5, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 1177987
    :goto_2
    invoke-static {}, Lcom/facebook/search/api/SearchTypeaheadResult;->newBuilder()LX/7BL;

    move-result-object v6

    iget-object v8, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->category:Ljava/lang/String;

    .line 1177988
    iput-object v8, v6, LX/7BL;->a:Ljava/lang/String;

    .line 1177989
    move-object v6, v6

    .line 1177990
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1177991
    iput-object v8, v6, LX/7BL;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1177992
    move-object v6, v6

    .line 1177993
    iput-boolean v7, v6, LX/7BL;->o:Z

    .line 1177994
    move-object v6, v6

    .line 1177995
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->NOT_VERIFIED:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1177996
    iput-object v8, v6, LX/7BL;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1177997
    move-object v8, v6

    .line 1177998
    iget-object v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->fragments:Ljava/util/List;

    if-eqz v6, :cond_8

    iget-object v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->fragments:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_8

    iget-object v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->fragments:Ljava/util/List;

    .line 1177999
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 1178000
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/search/api/model/GraphSearchQueryFragment;

    .line 1178001
    iget-object v9, v9, Lcom/facebook/search/api/model/GraphSearchQueryFragment;->text:Ljava/lang/String;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1178002
    :cond_2
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v6, v9

    .line 1178003
    :goto_4
    iput-object v6, v8, LX/7BL;->l:Ljava/lang/String;

    .line 1178004
    move-object v6, v8

    .line 1178005
    iget-object v8, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->subtext:Ljava/lang/String;

    .line 1178006
    iput-object v8, v6, LX/7BL;->g:Ljava/lang/String;

    .line 1178007
    move-object v6, v6

    .line 1178008
    iget-object v8, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->boldedSubtext:Ljava/lang/String;

    .line 1178009
    iput-object v8, v6, LX/7BL;->h:Ljava/lang/String;

    .line 1178010
    move-object v6, v6

    .line 1178011
    iget-object v8, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->keywordType:Ljava/lang/String;

    .line 1178012
    iput-object v8, v6, LX/7BL;->i:Ljava/lang/String;

    .line 1178013
    move-object v6, v6

    .line 1178014
    iget-object v8, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->keywordSource:Ljava/lang/String;

    .line 1178015
    iput-object v8, v6, LX/7BL;->j:Ljava/lang/String;

    .line 1178016
    move-object v6, v6

    .line 1178017
    iget-object v8, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->entityData:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    .line 1178018
    iput-object v8, v6, LX/7BL;->k:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    .line 1178019
    move-object v6, v6

    .line 1178020
    sget-object v8, LX/7BK;->KEYWORD_SUGGESTION:LX/7BK;

    .line 1178021
    iput-object v8, v6, LX/7BL;->m:LX/7BK;

    .line 1178022
    move-object v6, v6

    .line 1178023
    iget-object v8, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->semantic:Ljava/lang/String;

    .line 1178024
    iput-object v8, v6, LX/7BL;->t:Ljava/lang/String;

    .line 1178025
    move-object v6, v6

    .line 1178026
    iput-object v4, v6, LX/7BL;->f:Landroid/net/Uri;

    .line 1178027
    move-object v4, v6

    .line 1178028
    iget-object v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->isScoped:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/7BL;->a(Ljava/lang/Boolean;)LX/7BL;

    move-result-object v4

    iget-object v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->logInfo:Ljava/util/Map;

    .line 1178029
    iput-object v6, v4, LX/7BL;->y:Ljava/util/Map;

    .line 1178030
    move-object v4, v4

    .line 1178031
    iget-object v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->entityId:Ljava/lang/String;

    .line 1178032
    iput-object v6, v4, LX/7BL;->z:Ljava/lang/String;

    .line 1178033
    move-object v4, v4

    .line 1178034
    iget-boolean v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->isConnected:Z

    .line 1178035
    iput-boolean v6, v4, LX/7BL;->A:Z

    .line 1178036
    move-object v6, v4

    .line 1178037
    iget-object v4, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->matchedPos:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    move v4, v7

    .line 1178038
    :goto_5
    iput v4, v6, LX/7BL;->v:I

    .line 1178039
    move-object v4, v6

    .line 1178040
    iget-object v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->matchedLength:Ljava/lang/String;

    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1178041
    :goto_6
    iput v7, v4, LX/7BL;->w:I

    .line 1178042
    move-object v4, v4

    .line 1178043
    iget-boolean v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->isLive:Z

    .line 1178044
    iput-boolean v6, v4, LX/7BL;->x:Z

    .line 1178045
    move-object v4, v4

    .line 1178046
    iput-object v5, v4, LX/7BL;->d:Landroid/net/Uri;

    .line 1178047
    move-object v4, v4

    .line 1178048
    invoke-virtual {v4}, LX/7BL;->a()Lcom/facebook/search/api/SearchTypeaheadResult;

    move-result-object v4

    move-object v4, v4

    .line 1178049
    :goto_7
    move-object v0, v4

    .line 1178050
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 1178051
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1178052
    :cond_4
    iget-object v4, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->externalUrl:Ljava/lang/String;

    if-eqz v4, :cond_5

    .line 1178053
    invoke-static {}, Lcom/facebook/search/api/SearchTypeaheadResult;->newBuilder()LX/7BL;

    move-result-object v4

    invoke-virtual {v4}, LX/7BL;->a()Lcom/facebook/search/api/SearchTypeaheadResult;

    move-result-object v4

    move-object v4, v4

    .line 1178054
    goto :goto_7

    .line 1178055
    :cond_5
    const/4 v7, 0x0

    .line 1178056
    iget-object v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->friendshipStatus:Ljava/lang/String;

    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    move-object v6, v7

    .line 1178057
    :goto_8
    iget-object v8, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->photoUri:Ljava/lang/String;

    invoke-static {v8}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_c

    sget-object v8, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 1178058
    :goto_9
    iget-object v9, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->nativeAndroidUrl:Ljava/lang/String;

    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_d

    sget-object v9, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 1178059
    :goto_a
    iget-object v10, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->grammarType:Ljava/lang/String;

    invoke-static {v10}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_e

    move-object v10, v7

    .line 1178060
    :goto_b
    invoke-static {}, Lcom/facebook/search/api/SearchTypeaheadResult;->newBuilder()LX/7BL;

    move-result-object v11

    iget-object v12, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->category:Ljava/lang/String;

    .line 1178061
    iput-object v12, v11, LX/7BL;->a:Ljava/lang/String;

    .line 1178062
    move-object v12, v11

    .line 1178063
    iget-object v11, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->fallbackPath:Ljava/lang/String;

    invoke-static {v11}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_f

    move-object v11, v7

    .line 1178064
    :goto_c
    iput-object v11, v12, LX/7BL;->c:Landroid/net/Uri;

    .line 1178065
    move-object v11, v12

    .line 1178066
    iput-object v6, v11, LX/7BL;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1178067
    move-object v6, v11

    .line 1178068
    iget-object v11, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->isVerified:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    .line 1178069
    iput-boolean v11, v6, LX/7BL;->o:Z

    .line 1178070
    move-object v6, v6

    .line 1178071
    iget-object v11, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->verificationStatus:Ljava/lang/String;

    invoke-static {v11}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v11

    .line 1178072
    iput-object v11, v6, LX/7BL;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1178073
    move-object v11, v6

    .line 1178074
    iget-object v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->path:Ljava/lang/String;

    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_10

    move-object v6, v7

    .line 1178075
    :goto_d
    iput-object v6, v11, LX/7BL;->e:Landroid/net/Uri;

    .line 1178076
    move-object v6, v11

    .line 1178077
    iput-object v8, v6, LX/7BL;->f:Landroid/net/Uri;

    .line 1178078
    move-object v6, v6

    .line 1178079
    iget-object v8, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->subtext:Ljava/lang/String;

    .line 1178080
    iput-object v8, v6, LX/7BL;->g:Ljava/lang/String;

    .line 1178081
    move-object v6, v6

    .line 1178082
    iget-object v8, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->name:Ljava/lang/String;

    .line 1178083
    iput-object v8, v6, LX/7BL;->l:Ljava/lang/String;

    .line 1178084
    move-object v6, v6

    .line 1178085
    iput-object v10, v6, LX/7BL;->m:LX/7BK;

    .line 1178086
    move-object v6, v6

    .line 1178087
    iget-object v8, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->semantic:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 1178088
    iput-wide v10, v6, LX/7BL;->n:J

    .line 1178089
    move-object v6, v6

    .line 1178090
    iput-object v7, v6, LX/7BL;->r:Ljava/util/List;

    .line 1178091
    move-object v6, v6

    .line 1178092
    sget-object v7, LX/0Q7;->a:LX/0Px;

    move-object v7, v7

    .line 1178093
    iput-object v7, v6, LX/7BL;->s:LX/0Px;

    .line 1178094
    move-object v6, v6

    .line 1178095
    iget-object v7, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->semantic:Ljava/lang/String;

    .line 1178096
    iput-object v7, v6, LX/7BL;->t:Ljava/lang/String;

    .line 1178097
    move-object v6, v6

    .line 1178098
    iget-object v7, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->isScoped:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/7BL;->a(Ljava/lang/Boolean;)LX/7BL;

    move-result-object v6

    iget-boolean v7, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->isLive:Z

    .line 1178099
    iput-boolean v7, v6, LX/7BL;->x:Z

    .line 1178100
    move-object v6, v6

    .line 1178101
    iget-object v7, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->logInfo:Ljava/util/Map;

    .line 1178102
    iput-object v7, v6, LX/7BL;->y:Ljava/util/Map;

    .line 1178103
    move-object v6, v6

    .line 1178104
    iput-object v9, v6, LX/7BL;->d:Landroid/net/Uri;

    .line 1178105
    move-object v6, v6

    .line 1178106
    invoke-virtual {v6}, LX/7BL;->a()Lcom/facebook/search/api/SearchTypeaheadResult;

    move-result-object v6

    move-object v4, v6

    .line 1178107
    goto/16 :goto_7

    .line 1178108
    :cond_6
    iget-object v4, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->photoUri:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_1

    .line 1178109
    :cond_7
    iget-object v5, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->nativeAndroidUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    goto/16 :goto_2

    .line 1178110
    :cond_8
    iget-object v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->text:Ljava/lang/String;

    goto/16 :goto_4

    :cond_9
    iget-object v4, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->matchedPos:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_5

    :cond_a
    iget-object v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->matchedLength:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_6

    .line 1178111
    :cond_b
    iget-object v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->friendshipStatus:Ljava/lang/String;

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    goto/16 :goto_8

    .line 1178112
    :cond_c
    iget-object v8, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->photoUri:Ljava/lang/String;

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    goto/16 :goto_9

    .line 1178113
    :cond_d
    iget-object v9, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->nativeAndroidUrl:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    goto/16 :goto_a

    .line 1178114
    :cond_e
    iget-object v10, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->grammarType:Ljava/lang/String;

    .line 1178115
    const/4 v11, 0x1

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, LX/7BK;->valueOf(Ljava/lang/String;)LX/7BK;

    move-result-object v11

    move-object v10, v11

    .line 1178116
    goto/16 :goto_b

    .line 1178117
    :cond_f
    iget-object v11, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->fallbackPath:Ljava/lang/String;

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    goto/16 :goto_c

    :cond_10
    iget-object v6, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->path:Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    goto/16 :goto_d
.end method
