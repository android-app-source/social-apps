.class public LX/7j7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field public final b:LX/17Y;

.field public final c:LX/7j6;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/7j6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1229210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1229211
    iput-object p1, p0, LX/7j7;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1229212
    iput-object p2, p0, LX/7j7;->b:LX/17Y;

    .line 1229213
    iput-object p3, p0, LX/7j7;->c:LX/7j6;

    .line 1229214
    return-void
.end method

.method public static a(LX/0QB;)LX/7j7;
    .locals 1

    .prologue
    .line 1229209
    invoke-static {p0}, LX/7j7;->b(LX/0QB;)LX/7j7;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/7j7;Ljava/lang/String;LX/7iP;)V
    .locals 1

    .prologue
    .line 1229207
    iget-object v0, p0, LX/7j7;->c:LX/7j6;

    invoke-virtual {v0, p1, p2}, LX/7j6;->a(Ljava/lang/String;LX/7iP;)V

    .line 1229208
    return-void
.end method

.method public static a(LX/7j7;Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1229204
    iget-object v0, p0, LX/7j7;->b:LX/17Y;

    sget-object v1, LX/0ax;->aF:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p2, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1229205
    iget-object v1, p0, LX/7j7;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1229206
    return-void
.end method

.method public static b(LX/0QB;)LX/7j7;
    .locals 4

    .prologue
    .line 1229202
    new-instance v3, LX/7j7;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v1

    check-cast v1, LX/17Y;

    invoke-static {p0}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v2

    check-cast v2, LX/7j6;

    invoke-direct {v3, v0, v1, v2}, LX/7j7;-><init>(Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/7j6;)V

    .line 1229203
    return-object v3
.end method

.method public static b(LX/7j7;Ljava/lang/String;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1229196
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1229197
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1229198
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1229199
    const-string v1, "iab_click_source"

    const-string v2, "product_tagging"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1229200
    iget-object v1, p0, LX/7j7;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1229201
    return-void
.end method
