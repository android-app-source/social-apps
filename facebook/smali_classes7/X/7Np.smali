.class public final LX/7Np;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V
    .locals 0

    .prologue
    .line 1200718
    iput-object p1, p0, LX/7Np;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 5

    .prologue
    const/16 v4, 0x18

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1200719
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-eq v2, v4, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x19

    if-eq v2, v3, :cond_1

    .line 1200720
    :cond_0
    :goto_0
    return v1

    .line 1200721
    :cond_1
    iget-object v2, p0, LX/7Np;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    iget-object v2, v2, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v2}, LX/37Y;->g()LX/38p;

    move-result-object v2

    invoke-virtual {v2}, LX/38p;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1200722
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-eqz v2, :cond_2

    move v1, v0

    .line 1200723
    goto :goto_0

    .line 1200724
    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-ne v2, v4, :cond_3

    .line 1200725
    :goto_1
    iget-object v1, p0, LX/7Np;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    iget-object v2, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->r:LX/7J3;

    if-eqz v0, :cond_4

    const-string v1, "volume_up"

    :goto_2
    invoke-virtual {v2, v1}, LX/7J3;->a(Ljava/lang/String;)LX/7J2;

    move-result-object v1

    .line 1200726
    if-eqz v0, :cond_5

    iget-object v0, p0, LX/7Np;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->l()Z

    move-result v0

    .line 1200727
    :goto_3
    invoke-virtual {v1, v0}, LX/7J2;->a(Z)V

    move v1, v0

    .line 1200728
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1200729
    goto :goto_1

    .line 1200730
    :cond_4
    const-string v1, "volume_down"

    goto :goto_2

    .line 1200731
    :cond_5
    iget-object v0, p0, LX/7Np;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->m()Z

    move-result v0

    goto :goto_3
.end method
