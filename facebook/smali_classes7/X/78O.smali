.class public final LX/78O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V
    .locals 0

    .prologue
    .line 1172607
    iput-object p1, p0, LX/78O;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x8ca0e80

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1172608
    iget-object v1, p0, LX/78O;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    .line 1172609
    new-instance v4, Landroid/view/animation/AlphaAnimation;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1172610
    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1172611
    const-wide/16 v6, 0xc8

    invoke-virtual {v4, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1172612
    new-instance v5, LX/78M;

    invoke-direct {v5, v1}, LX/78M;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V

    invoke-virtual {v4, v5}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1172613
    iget-object v5, v1, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    invoke-virtual {v5, v4}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1172614
    iget-object v1, p0, LX/78O;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v1, v1, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    sget-object v2, LX/7FJ;->INVITATION_OPENED:LX/7FJ;

    invoke-virtual {v1, v2}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(LX/7FJ;)V

    .line 1172615
    const v1, -0x24b62b85

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
