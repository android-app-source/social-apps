.class public final LX/7wV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;)V
    .locals 0

    .prologue
    .line 1275776
    iput-object p1, p0, LX/7wV;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x2

    const v0, 0x656d2921    # 6.999747E22f

    invoke-static {v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1275777
    iget-object v1, p0, LX/7wV;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    const/4 v5, 0x0

    .line 1275778
    iget-object v4, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->c:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v7, v4, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->w:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v6, v5

    :goto_0
    if-ge v6, v8, :cond_9

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;

    .line 1275779
    iget-object v9, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->h:LX/0Rf;

    .line 1275780
    iget-object p1, v4, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->d:Ljava/util/Set;

    invoke-interface {p1, v9}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result p1

    move v4, p1

    .line 1275781
    if-nez v4, :cond_8

    move v4, v5

    .line 1275782
    :goto_1
    move v1, v4

    .line 1275783
    if-eqz v1, :cond_0

    iget-object v1, p0, LX/7wV;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-boolean v1, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->i:Z

    if-nez v1, :cond_0

    .line 1275784
    iget-object v1, p0, LX/7wV;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1275785
    iput-boolean v2, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->i:Z

    .line 1275786
    iget-object v1, p0, LX/7wV;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->f:LX/7wG;

    iget-object v2, p0, LX/7wV;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-object v2, v2, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->c:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-interface {v1, v2}, LX/7wG;->a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V

    .line 1275787
    const v1, 0x37111dad

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1275788
    :goto_2
    return-void

    .line 1275789
    :cond_0
    iget-object v1, p0, LX/7wV;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->d:LX/7xy;

    const/4 v5, -0x1

    .line 1275790
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1275791
    const/4 v2, 0x0

    move v3, v2

    move v4, v5

    :goto_3
    iget-object v2, v1, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_6

    .line 1275792
    iget-object v2, v1, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7ws;

    .line 1275793
    iget-object v7, v2, LX/7ws;->c:LX/7wO;

    move-object v7, v7

    .line 1275794
    sget-object v8, LX/7wO;->ERROR:LX/7wO;

    if-eq v7, v8, :cond_5

    .line 1275795
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1275796
    iget-object v7, v2, LX/7ws;->c:LX/7wO;

    sget-object v8, LX/7wO;->FIELD_TEXT:LX/7wO;

    if-eq v7, v8, :cond_1

    iget-object v7, v2, LX/7ws;->c:LX/7wO;

    sget-object v8, LX/7wO;->FIELD_CHECKBOX:LX/7wO;

    if-eq v7, v8, :cond_1

    iget-object v7, v2, LX/7ws;->c:LX/7wO;

    sget-object v8, LX/7wO;->FIELD_SELECT_DROPDOWN:LX/7wO;

    if-eq v7, v8, :cond_1

    iget-object v7, v2, LX/7ws;->c:LX/7wO;

    sget-object v8, LX/7wO;->FIELD_SELECT_EXPANDED:LX/7wO;

    if-eq v7, v8, :cond_1

    iget-object v7, v2, LX/7ws;->c:LX/7wO;

    sget-object v8, LX/7wO;->FIELD_SELECT_MULTIPLE:LX/7wO;

    if-ne v7, v8, :cond_a

    :cond_1
    const/4 v7, 0x1

    :goto_4
    move v7, v7

    .line 1275797
    if-eqz v7, :cond_5

    .line 1275798
    iget-object v7, v2, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v7, v7

    .line 1275799
    invoke-virtual {v7}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->b()Z

    move-result v7

    if-nez v7, :cond_2

    .line 1275800
    iget-object v7, v2, LX/7ws;->d:Ljava/lang/String;

    move-object v7, v7

    .line 1275801
    if-eqz v7, :cond_b

    .line 1275802
    iget-object v7, v2, LX/7ws;->d:Ljava/lang/String;

    move-object v7, v7

    .line 1275803
    const-string v8, "address2"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    :cond_2
    const/4 v7, 0x1

    :goto_5
    move v7, v7

    .line 1275804
    if-nez v7, :cond_5

    .line 1275805
    iget-object v7, v1, LX/7xy;->d:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    invoke-static {v7, v2}, LX/7xD;->d(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;LX/7ws;)Z

    move-result v7

    if-nez v7, :cond_3

    iget-object v7, v1, LX/7xy;->d:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    invoke-static {v7, v2}, LX/7xD;->f(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;LX/7ws;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 1275806
    :cond_3
    if-ne v4, v5, :cond_4

    move v4, v3

    .line 1275807
    :cond_4
    const/4 v7, 0x1

    .line 1275808
    iput-boolean v7, v2, LX/7ws;->e:Z

    .line 1275809
    iget-object v7, v1, LX/7xy;->d:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    invoke-static {v7, v2}, LX/7xD;->e(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;LX/7ws;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/7ws;->a(Ljava/lang/String;)LX/7ws;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1275810
    :cond_5
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 1275811
    :cond_6
    iput-object v6, v1, LX/7xy;->a:Ljava/util/List;

    .line 1275812
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 1275813
    move v1, v4

    .line 1275814
    const/4 v2, -0x1

    if-eq v1, v2, :cond_7

    .line 1275815
    iget-object v2, p0, LX/7wV;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-object v2, v2, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 1275816
    :cond_7
    const v1, -0x1d301db9

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto/16 :goto_2

    .line 1275817
    :cond_8
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto/16 :goto_0

    .line 1275818
    :cond_9
    const/4 v4, 0x1

    goto/16 :goto_1

    :cond_a
    const/4 v7, 0x0

    goto :goto_4

    :cond_b
    const/4 v7, 0x0

    goto :goto_5
.end method
