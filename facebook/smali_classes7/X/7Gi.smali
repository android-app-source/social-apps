.class public LX/7Gi;
.super Landroid/text/style/BackgroundColorSpan;
.source ""

# interfaces
.implements LX/7Gh;


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 1189700
    invoke-direct {p0, p2}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    .line 1189701
    iput p1, p0, LX/7Gi;->a:I

    .line 1189702
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/Editable;)I
    .locals 1

    .prologue
    .line 1189707
    invoke-interface {p1, p0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final b(Landroid/text/Editable;)I
    .locals 1

    .prologue
    .line 1189706
    invoke-interface {p1, p0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1189703
    invoke-super {p0, p1}, Landroid/text/style/BackgroundColorSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1189704
    iget v0, p0, LX/7Gi;->a:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1189705
    return-void
.end method
