.class public final LX/8YS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/8Ya;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8YU;

.field public final synthetic b:LX/8Yb;

.field public final synthetic c:Lcom/facebook/richdocument/fonts/FetchFontExecutor;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/fonts/FetchFontExecutor;LX/8YU;LX/8Yb;)V
    .locals 0

    .prologue
    .line 1356224
    iput-object p1, p0, LX/8YS;->c:Lcom/facebook/richdocument/fonts/FetchFontExecutor;

    iput-object p2, p0, LX/8YS;->a:LX/8YU;

    iput-object p3, p0, LX/8YS;->b:LX/8Yb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1356206
    iget-object v0, p0, LX/8YS;->c:Lcom/facebook/richdocument/fonts/FetchFontExecutor;

    iget-object v1, p0, LX/8YS;->a:LX/8YU;

    .line 1356207
    iget-object v2, v1, LX/8YU;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1356208
    iget-object v2, p0, LX/8YS;->a:LX/8YU;

    .line 1356209
    iget-object v3, v2, LX/8YU;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1356210
    iget-object v3, p0, LX/8YS;->a:LX/8YU;

    .line 1356211
    iget-object v4, v3, LX/8YU;->c:Ljava/lang/String;

    move-object v3, v4

    .line 1356212
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/richdocument/fonts/FetchFontExecutor;->a$redex0(Lcom/facebook/richdocument/fonts/FetchFontExecutor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/8Ya;

    move-result-object v1

    .line 1356213
    iget-object v0, p0, LX/8YS;->c:Lcom/facebook/richdocument/fonts/FetchFontExecutor;

    iget-object v0, v0, Lcom/facebook/richdocument/fonts/FetchFontExecutor;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Yc;

    iget-object v2, p0, LX/8YS;->b:LX/8Yb;

    .line 1356214
    iget-object v3, v0, LX/8Yc;->e:Landroid/util/LruCache;

    invoke-virtual {v3, v2, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8Ya;

    .line 1356215
    iget-object v0, p0, LX/8YS;->c:Lcom/facebook/richdocument/fonts/FetchFontExecutor;

    iget-object v0, v0, Lcom/facebook/richdocument/fonts/FetchFontExecutor;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Yc;

    .line 1356216
    :try_start_0
    iget-object v2, v0, LX/8Yc;->b:Ljava/io/File;

    move-object v3, v2

    .line 1356217
    iget-object v2, v0, LX/8Yc;->e:Landroid/util/LruCache;

    invoke-virtual {v2}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v2

    .line 1356218
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1356219
    iget-object v2, v0, LX/8Yc;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0lC;

    new-instance p0, LX/8YZ;

    invoke-direct {p0, v4}, LX/8YZ;-><init>(Ljava/util/List;)V

    invoke-virtual {v2, v3, p0}, LX/0lC;->a(Ljava/io/File;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1356220
    :goto_0
    return-object v1

    .line 1356221
    :catch_0
    move-exception v2

    move-object v3, v2

    .line 1356222
    const-string v4, "Failed to save font resource cache file fontResourceCache.json"

    .line 1356223
    iget-object v2, v0, LX/8Yc;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    sget-object p0, LX/8Yc;->a:Ljava/lang/Class;

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0, v4, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
