.class public LX/8N9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/7hC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1336026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1336027
    return-void
.end method

.method public static a(LX/0QB;)LX/8N9;
    .locals 3

    .prologue
    .line 1336073
    new-instance v1, LX/8N9;

    invoke-direct {v1}, LX/8N9;-><init>()V

    .line 1336074
    invoke-static {p0}, LX/7hC;->a(LX/0QB;)LX/7hC;

    move-result-object v0

    check-cast v0, LX/7hC;

    const/16 v2, 0x15e7

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 1336075
    iput-object v0, v1, LX/8N9;->a:LX/7hC;

    iput-object v2, v1, LX/8N9;->b:LX/0Or;

    .line 1336076
    move-object v0, v1

    .line 1336077
    return-object v0
.end method

.method public static a(LX/2VK;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1336078
    const-string v0, "createShot"

    invoke-interface {p0, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/upload/protocol/ShotCreateResult;

    .line 1336079
    iget-object v1, v0, Lcom/facebook/audience/upload/protocol/ShotCreateResult;->a:LX/0Px;

    move-object v0, v1

    .line 1336080
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/Long;)LX/2Vj;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Ljava/lang/Long;",
            ")",
            "LX/2Vj",
            "<",
            "LX/7hE;",
            "Lcom/facebook/audience/upload/protocol/ShotCreateResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1336028
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->am:Lcom/facebook/audience/model/UploadShot;

    move-object v0, v0

    .line 1336029
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/facebook/audience/model/UploadShot;

    .line 1336030
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    .line 1336031
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1336032
    iput-object v1, v0, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1336033
    invoke-virtual {v7}, Lcom/facebook/audience/model/UploadShot;->getCreatedAtTime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v8, v0

    .line 1336034
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 1336035
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-object v0, v0

    .line 1336036
    if-eqz v0, :cond_0

    .line 1336037
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-object v0, v0

    .line 1336038
    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1336039
    sget-object v0, LX/7hG;->composer_post:LX/7hG;

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336040
    :cond_0
    invoke-virtual {v7}, Lcom/facebook/audience/model/UploadShot;->getAudience()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1336041
    sget-object v0, LX/7hG;->direct_inbox:LX/7hG;

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336042
    :cond_1
    invoke-virtual {v7}, Lcom/facebook/audience/model/UploadShot;->getIsPrivate()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1336043
    sget-object v0, LX/7hG;->your_day:LX/7hG;

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336044
    :cond_2
    new-instance v0, LX/7hD;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Lcom/facebook/audience/model/UploadShot;->getMediaType()LX/7gi;

    move-result-object v2

    sget-object v3, LX/7gi;->PHOTO:LX/7gi;

    if-ne v2, v3, :cond_3

    const-string v2, "photo"

    :goto_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/8N9;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v7}, Lcom/facebook/audience/model/UploadShot;->getBackstagePostType()LX/7h9;

    move-result-object v5

    invoke-virtual {v5}, LX/7h9;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7}, Lcom/facebook/audience/model/UploadShot;->getReplyThreadId()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/7hD;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/facebook/audience/model/UploadShot;->getCaption()Ljava/lang/String;

    move-result-object v1

    .line 1336045
    iput-object v1, v0, LX/7hD;->f:Ljava/lang/String;

    .line 1336046
    move-object v0, v0

    .line 1336047
    invoke-virtual {v7}, Lcom/facebook/audience/model/UploadShot;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 1336048
    iput-object v1, v0, LX/7hD;->i:Ljava/lang/String;

    .line 1336049
    move-object v0, v0

    .line 1336050
    invoke-virtual {v7}, Lcom/facebook/audience/model/UploadShot;->getReactionId()Ljava/lang/String;

    move-result-object v1

    .line 1336051
    iput-object v1, v0, LX/7hD;->b:Ljava/lang/String;

    .line 1336052
    move-object v0, v0

    .line 1336053
    iput v8, v0, LX/7hD;->g:I

    .line 1336054
    move-object v0, v0

    .line 1336055
    invoke-virtual {v7}, Lcom/facebook/audience/model/UploadShot;->getAudience()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7h8;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    .line 1336056
    iput-object v1, v0, LX/7hD;->k:LX/0Px;

    .line 1336057
    move-object v0, v0

    .line 1336058
    invoke-virtual {v7}, Lcom/facebook/audience/model/UploadShot;->getIsPrivate()Z

    move-result v1

    .line 1336059
    iput-boolean v1, v0, LX/7hD;->l:Z

    .line 1336060
    move-object v0, v0

    .line 1336061
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1336062
    iput-object v1, v0, LX/7hD;->m:LX/0Px;

    .line 1336063
    move-object v0, v0

    .line 1336064
    invoke-virtual {v7}, Lcom/facebook/audience/model/UploadShot;->getInspirationPromptAnalytics()LX/0Px;

    move-result-object v1

    .line 1336065
    iput-object v1, v0, LX/7hD;->n:LX/0Px;

    .line 1336066
    move-object v0, v0

    .line 1336067
    invoke-virtual {v0}, LX/7hD;->a()LX/7hE;

    move-result-object v0

    .line 1336068
    iget-object v1, p0, LX/8N9;->a:LX/7hC;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "createShot"

    .line 1336069
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 1336070
    move-object v0, v0

    .line 1336071
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    return-object v0

    .line 1336072
    :cond_3
    const-string v2, "video"

    goto :goto_0
.end method
