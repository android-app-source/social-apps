.class public final enum LX/8RD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8RD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8RD;

.field public static final enum FRIENDS_EXCEPT:LX/8RD;

.field public static final enum SPECIFIC_FRIENDS:LX/8RD;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1344324
    new-instance v0, LX/8RD;

    const-string v1, "FRIENDS_EXCEPT"

    invoke-direct {v0, v1, v2}, LX/8RD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8RD;->FRIENDS_EXCEPT:LX/8RD;

    .line 1344325
    new-instance v0, LX/8RD;

    const-string v1, "SPECIFIC_FRIENDS"

    invoke-direct {v0, v1, v3}, LX/8RD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8RD;->SPECIFIC_FRIENDS:LX/8RD;

    .line 1344326
    const/4 v0, 0x2

    new-array v0, v0, [LX/8RD;

    sget-object v1, LX/8RD;->FRIENDS_EXCEPT:LX/8RD;

    aput-object v1, v0, v2

    sget-object v1, LX/8RD;->SPECIFIC_FRIENDS:LX/8RD;

    aput-object v1, v0, v3

    sput-object v0, LX/8RD;->$VALUES:[LX/8RD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1344328
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8RD;
    .locals 1

    .prologue
    .line 1344329
    const-class v0, LX/8RD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8RD;

    return-object v0
.end method

.method public static values()[LX/8RD;
    .locals 1

    .prologue
    .line 1344327
    sget-object v0, LX/8RD;->$VALUES:[LX/8RD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8RD;

    return-object v0
.end method
