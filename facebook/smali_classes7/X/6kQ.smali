.class public LX/6kQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final expireTime:Ljava/lang/Long;

.field public final threadKey:LX/6l9;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1136666
    new-instance v0, LX/1sv;

    const-string v1, "DeltaThreadMuteSettings"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kQ;->b:LX/1sv;

    .line 1136667
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kQ;->c:LX/1sw;

    .line 1136668
    new-instance v0, LX/1sw;

    const-string v1, "expireTime"

    const/16 v2, 0xa

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kQ;->d:LX/1sw;

    .line 1136669
    sput-boolean v4, LX/6kQ;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6l9;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1136670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1136671
    iput-object p1, p0, LX/6kQ;->threadKey:LX/6l9;

    .line 1136672
    iput-object p2, p0, LX/6kQ;->expireTime:Ljava/lang/Long;

    .line 1136673
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1136674
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1136675
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1136676
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1136677
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "DeltaThreadMuteSettings"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1136678
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136679
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136680
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136681
    const/4 v1, 0x1

    .line 1136682
    iget-object v5, p0, LX/6kQ;->threadKey:LX/6l9;

    if-eqz v5, :cond_0

    .line 1136683
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136684
    const-string v1, "threadKey"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136685
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136686
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136687
    iget-object v1, p0, LX/6kQ;->threadKey:LX/6l9;

    if-nez v1, :cond_6

    .line 1136688
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136689
    :goto_3
    const/4 v1, 0x0

    .line 1136690
    :cond_0
    iget-object v5, p0, LX/6kQ;->expireTime:Ljava/lang/Long;

    if-eqz v5, :cond_2

    .line 1136691
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136692
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136693
    const-string v1, "expireTime"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136694
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136695
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136696
    iget-object v0, p0, LX/6kQ;->expireTime:Ljava/lang/Long;

    if-nez v0, :cond_7

    .line 1136697
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136698
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136699
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136700
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1136701
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1136702
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1136703
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1136704
    :cond_6
    iget-object v1, p0, LX/6kQ;->threadKey:LX/6l9;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1136705
    :cond_7
    iget-object v0, p0, LX/6kQ;->expireTime:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1136706
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1136707
    iget-object v0, p0, LX/6kQ;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1136708
    iget-object v0, p0, LX/6kQ;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1136709
    sget-object v0, LX/6kQ;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136710
    iget-object v0, p0, LX/6kQ;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1136711
    :cond_0
    iget-object v0, p0, LX/6kQ;->expireTime:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1136712
    iget-object v0, p0, LX/6kQ;->expireTime:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1136713
    sget-object v0, LX/6kQ;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136714
    iget-object v0, p0, LX/6kQ;->expireTime:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1136715
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1136716
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1136717
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1136718
    if-nez p1, :cond_1

    .line 1136719
    :cond_0
    :goto_0
    return v0

    .line 1136720
    :cond_1
    instance-of v1, p1, LX/6kQ;

    if-eqz v1, :cond_0

    .line 1136721
    check-cast p1, LX/6kQ;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1136722
    if-nez p1, :cond_3

    .line 1136723
    :cond_2
    :goto_1
    move v0, v2

    .line 1136724
    goto :goto_0

    .line 1136725
    :cond_3
    iget-object v0, p0, LX/6kQ;->threadKey:LX/6l9;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1136726
    :goto_2
    iget-object v3, p1, LX/6kQ;->threadKey:LX/6l9;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1136727
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1136728
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136729
    iget-object v0, p0, LX/6kQ;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6kQ;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1136730
    :cond_5
    iget-object v0, p0, LX/6kQ;->expireTime:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1136731
    :goto_4
    iget-object v3, p1, LX/6kQ;->expireTime:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1136732
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1136733
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136734
    iget-object v0, p0, LX/6kQ;->expireTime:Ljava/lang/Long;

    iget-object v3, p1, LX/6kQ;->expireTime:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1136735
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1136736
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1136737
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1136738
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1136739
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1136740
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1136741
    sget-boolean v0, LX/6kQ;->a:Z

    .line 1136742
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kQ;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1136743
    return-object v0
.end method
