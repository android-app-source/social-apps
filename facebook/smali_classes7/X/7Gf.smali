.class public final enum LX/7Gf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Gf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Gf;

.field public static final enum AHEAD:LX/7Gf;

.field public static final enum ALREADY_PROCESSED:LX/7Gf;

.field public static final enum EXPECTED:LX/7Gf;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1189673
    new-instance v0, LX/7Gf;

    const-string v1, "EXPECTED"

    invoke-direct {v0, v1, v2}, LX/7Gf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Gf;->EXPECTED:LX/7Gf;

    .line 1189674
    new-instance v0, LX/7Gf;

    const-string v1, "AHEAD"

    invoke-direct {v0, v1, v3}, LX/7Gf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Gf;->AHEAD:LX/7Gf;

    .line 1189675
    new-instance v0, LX/7Gf;

    const-string v1, "ALREADY_PROCESSED"

    invoke-direct {v0, v1, v4}, LX/7Gf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Gf;->ALREADY_PROCESSED:LX/7Gf;

    .line 1189676
    const/4 v0, 0x3

    new-array v0, v0, [LX/7Gf;

    sget-object v1, LX/7Gf;->EXPECTED:LX/7Gf;

    aput-object v1, v0, v2

    sget-object v1, LX/7Gf;->AHEAD:LX/7Gf;

    aput-object v1, v0, v3

    sget-object v1, LX/7Gf;->ALREADY_PROCESSED:LX/7Gf;

    aput-object v1, v0, v4

    sput-object v0, LX/7Gf;->$VALUES:[LX/7Gf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1189677
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Gf;
    .locals 1

    .prologue
    .line 1189678
    const-class v0, LX/7Gf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Gf;

    return-object v0
.end method

.method public static values()[LX/7Gf;
    .locals 1

    .prologue
    .line 1189679
    sget-object v0, LX/7Gf;->$VALUES:[LX/7Gf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Gf;

    return-object v0
.end method
