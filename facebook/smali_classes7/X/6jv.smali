.class public LX/6jv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;


# instance fields
.field public final actionTimestamp:Ljava/lang/Long;

.field public final folders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final threadKeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6l9;",
            ">;"
        }
    .end annotation
.end field

.field public final watermarkTimestamp:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xf

    const/16 v4, 0xa

    const/4 v3, 0x1

    .line 1133571
    new-instance v0, LX/1sv;

    const-string v1, "DeltaMarkRead"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jv;->b:LX/1sv;

    .line 1133572
    new-instance v0, LX/1sw;

    const-string v1, "threadKeys"

    invoke-direct {v0, v1, v5, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jv;->c:LX/1sw;

    .line 1133573
    new-instance v0, LX/1sw;

    const-string v1, "folders"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jv;->d:LX/1sw;

    .line 1133574
    new-instance v0, LX/1sw;

    const-string v1, "watermarkTimestamp"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jv;->e:LX/1sw;

    .line 1133575
    new-instance v0, LX/1sw;

    const-string v1, "actionTimestamp"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jv;->f:LX/1sw;

    .line 1133576
    sput-boolean v3, LX/6jv;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6l9;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1133577
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1133578
    iput-object p1, p0, LX/6jv;->threadKeys:Ljava/util/List;

    .line 1133579
    iput-object p2, p0, LX/6jv;->folders:Ljava/util/List;

    .line 1133580
    iput-object p3, p0, LX/6jv;->watermarkTimestamp:Ljava/lang/Long;

    .line 1133581
    iput-object p4, p0, LX/6jv;->actionTimestamp:Ljava/lang/Long;

    .line 1133582
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1133583
    if-eqz p2, :cond_6

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1133584
    :goto_0
    if-eqz p2, :cond_7

    const-string v0, "\n"

    move-object v3, v0

    .line 1133585
    :goto_1
    if-eqz p2, :cond_8

    const-string v0, " "

    .line 1133586
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "DeltaMarkRead"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1133587
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133588
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133589
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133590
    const/4 v1, 0x1

    .line 1133591
    iget-object v6, p0, LX/6jv;->threadKeys:Ljava/util/List;

    if-eqz v6, :cond_0

    .line 1133592
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133593
    const-string v1, "threadKeys"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133594
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133595
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133596
    iget-object v1, p0, LX/6jv;->threadKeys:Ljava/util/List;

    if-nez v1, :cond_9

    .line 1133597
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1133598
    :cond_0
    iget-object v6, p0, LX/6jv;->folders:Ljava/util/List;

    if-eqz v6, :cond_2

    .line 1133599
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133600
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133601
    const-string v1, "folders"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133602
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133603
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133604
    iget-object v1, p0, LX/6jv;->folders:Ljava/util/List;

    if-nez v1, :cond_a

    .line 1133605
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 1133606
    :cond_2
    iget-object v6, p0, LX/6jv;->watermarkTimestamp:Ljava/lang/Long;

    if-eqz v6, :cond_d

    .line 1133607
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133608
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133609
    const-string v1, "watermarkTimestamp"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133610
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133611
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133612
    iget-object v1, p0, LX/6jv;->watermarkTimestamp:Ljava/lang/Long;

    if-nez v1, :cond_b

    .line 1133613
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133614
    :goto_5
    iget-object v1, p0, LX/6jv;->actionTimestamp:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 1133615
    if-nez v2, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133616
    :cond_4
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133617
    const-string v1, "actionTimestamp"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133618
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133619
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133620
    iget-object v0, p0, LX/6jv;->actionTimestamp:Ljava/lang/Long;

    if-nez v0, :cond_c

    .line 1133621
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133622
    :cond_5
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133623
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133624
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1133625
    :cond_6
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1133626
    :cond_7
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1133627
    :cond_8
    const-string v0, ""

    goto/16 :goto_2

    .line 1133628
    :cond_9
    iget-object v1, p0, LX/6jv;->threadKeys:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1133629
    :cond_a
    iget-object v1, p0, LX/6jv;->folders:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1133630
    :cond_b
    iget-object v1, p0, LX/6jv;->watermarkTimestamp:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1133631
    :cond_c
    iget-object v0, p0, LX/6jv;->actionTimestamp:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    :cond_d
    move v2, v1

    goto/16 :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1133632
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1133633
    iget-object v0, p0, LX/6jv;->threadKeys:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1133634
    iget-object v0, p0, LX/6jv;->threadKeys:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1133635
    sget-object v0, LX/6jv;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133636
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/6jv;->threadKeys:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1133637
    iget-object v0, p0, LX/6jv;->threadKeys:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6l9;

    .line 1133638
    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    goto :goto_0

    .line 1133639
    :cond_0
    iget-object v0, p0, LX/6jv;->folders:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1133640
    iget-object v0, p0, LX/6jv;->folders:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1133641
    sget-object v0, LX/6jv;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133642
    new-instance v0, LX/1u3;

    const/16 v1, 0x8

    iget-object v2, p0, LX/6jv;->folders:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1133643
    iget-object v0, p0, LX/6jv;->folders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1133644
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    goto :goto_1

    .line 1133645
    :cond_1
    iget-object v0, p0, LX/6jv;->watermarkTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1133646
    iget-object v0, p0, LX/6jv;->watermarkTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1133647
    sget-object v0, LX/6jv;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133648
    iget-object v0, p0, LX/6jv;->watermarkTimestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1133649
    :cond_2
    iget-object v0, p0, LX/6jv;->actionTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1133650
    iget-object v0, p0, LX/6jv;->actionTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1133651
    sget-object v0, LX/6jv;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133652
    iget-object v0, p0, LX/6jv;->actionTimestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1133653
    :cond_3
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1133654
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1133655
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1133656
    if-nez p1, :cond_1

    .line 1133657
    :cond_0
    :goto_0
    return v0

    .line 1133658
    :cond_1
    instance-of v1, p1, LX/6jv;

    if-eqz v1, :cond_0

    .line 1133659
    check-cast p1, LX/6jv;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1133660
    if-nez p1, :cond_3

    .line 1133661
    :cond_2
    :goto_1
    move v0, v2

    .line 1133662
    goto :goto_0

    .line 1133663
    :cond_3
    iget-object v0, p0, LX/6jv;->threadKeys:Ljava/util/List;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1133664
    :goto_2
    iget-object v3, p1, LX/6jv;->threadKeys:Ljava/util/List;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1133665
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1133666
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133667
    iget-object v0, p0, LX/6jv;->threadKeys:Ljava/util/List;

    iget-object v3, p1, LX/6jv;->threadKeys:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133668
    :cond_5
    iget-object v0, p0, LX/6jv;->folders:Ljava/util/List;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1133669
    :goto_4
    iget-object v3, p1, LX/6jv;->folders:Ljava/util/List;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1133670
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1133671
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133672
    iget-object v0, p0, LX/6jv;->folders:Ljava/util/List;

    iget-object v3, p1, LX/6jv;->folders:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133673
    :cond_7
    iget-object v0, p0, LX/6jv;->watermarkTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1133674
    :goto_6
    iget-object v3, p1, LX/6jv;->watermarkTimestamp:Ljava/lang/Long;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1133675
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1133676
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133677
    iget-object v0, p0, LX/6jv;->watermarkTimestamp:Ljava/lang/Long;

    iget-object v3, p1, LX/6jv;->watermarkTimestamp:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133678
    :cond_9
    iget-object v0, p0, LX/6jv;->actionTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1133679
    :goto_8
    iget-object v3, p1, LX/6jv;->actionTimestamp:Ljava/lang/Long;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1133680
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1133681
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133682
    iget-object v0, p0, LX/6jv;->actionTimestamp:Ljava/lang/Long;

    iget-object v3, p1, LX/6jv;->actionTimestamp:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_b
    move v2, v1

    .line 1133683
    goto :goto_1

    :cond_c
    move v0, v2

    .line 1133684
    goto :goto_2

    :cond_d
    move v3, v2

    .line 1133685
    goto :goto_3

    :cond_e
    move v0, v2

    .line 1133686
    goto :goto_4

    :cond_f
    move v3, v2

    .line 1133687
    goto :goto_5

    :cond_10
    move v0, v2

    .line 1133688
    goto :goto_6

    :cond_11
    move v3, v2

    .line 1133689
    goto :goto_7

    :cond_12
    move v0, v2

    .line 1133690
    goto :goto_8

    :cond_13
    move v3, v2

    .line 1133691
    goto :goto_9
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1133692
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1133693
    sget-boolean v0, LX/6jv;->a:Z

    .line 1133694
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jv;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1133695
    return-object v0
.end method
