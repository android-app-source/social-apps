.class public LX/7kl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0SG;

.field public final c:LX/7kk;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1232423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232424
    new-instance v0, LX/7kk;

    invoke-direct {v0, p0}, LX/7kk;-><init>(LX/7kl;)V

    iput-object v0, p0, LX/7kl;->c:LX/7kk;

    .line 1232425
    iput-object p1, p0, LX/7kl;->a:LX/0Zb;

    .line 1232426
    iput-object p2, p0, LX/7kl;->b:LX/0SG;

    .line 1232427
    return-void
.end method

.method public static b(LX/0QB;)LX/7kl;
    .locals 3

    .prologue
    .line 1232421
    new-instance v2, LX/7kl;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-direct {v2, v0, v1}, LX/7kl;-><init>(LX/0Zb;LX/0SG;)V

    .line 1232422
    return-object v2
.end method


# virtual methods
.method public final a(LX/2tG;)V
    .locals 3

    .prologue
    .line 1232419
    iget-object v0, p0, LX/7kl;->c:LX/7kk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "device_location_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/2tG;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/7kk;->a$redex0(LX/7kk;Ljava/lang/String;)V

    .line 1232420
    return-void
.end method

.method public final a(ZLX/2tG;)V
    .locals 4

    .prologue
    .line 1232415
    if-eqz p1, :cond_0

    const-string v0, "use_prefixed_location"

    .line 1232416
    :goto_0
    iget-object v1, p0, LX/7kl;->c:LX/7kk;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "place_list_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, LX/2tG;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/7kk;->a$redex0(LX/7kk;Ljava/lang/String;)V

    .line 1232417
    return-void

    .line 1232418
    :cond_0
    const-string v0, "use_device_location"

    goto :goto_0
.end method
