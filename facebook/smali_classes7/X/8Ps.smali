.class public LX/8Ps;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0SG;

.field public b:Lcom/facebook/privacy/PrivacyOperationsClient;


# direct methods
.method public constructor <init>(LX/0SG;Lcom/facebook/privacy/PrivacyOperationsClient;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1342251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1342252
    iput-object p1, p0, LX/8Ps;->a:LX/0SG;

    .line 1342253
    iput-object p2, p0, LX/8Ps;->b:Lcom/facebook/privacy/PrivacyOperationsClient;

    .line 1342254
    return-void
.end method

.method public static a(LX/8Ps;)Ljava/lang/Long;
    .locals 4

    .prologue
    .line 1342257
    iget-object v0, p0, LX/8Ps;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1oU;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1342258
    if-nez p0, :cond_0

    .line 1342259
    const-string v0, "InlinePrivacySurveyLoggingController"

    const-string v1, "Trying to log an empty option for inline privacy survey"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1342260
    const/4 v0, 0x0

    .line 1342261
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, LX/1oU;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5nc;LX/1oU;LX/1oU;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1342255
    iget-object v0, p0, LX/8Ps;->b:Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {p0}, LX/8Ps;->a(LX/8Ps;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {p2}, LX/8Ps;->a(LX/1oU;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p3}, LX/8Ps;->a(LX/1oU;)Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/5nc;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1342256
    return-void
.end method
