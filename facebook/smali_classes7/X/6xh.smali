.class public final enum LX/6xh;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6LU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6xh;",
        ">;",
        "LX/6LU",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6xh;

.field public static final enum MOCK:LX/6xh;

.field public static final enum PAGES_COMMERCE:LX/6xh;

.field public static final enum UNKNOWN:LX/6xh;


# instance fields
.field public final paymentItemType:LX/6xg;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1159115
    new-instance v0, LX/6xh;

    const-string v1, "MOCK"

    sget-object v2, LX/6xg;->MOR_DUMMY_THIRD_PARTY:LX/6xg;

    invoke-direct {v0, v1, v3, v2}, LX/6xh;-><init>(Ljava/lang/String;ILX/6xg;)V

    sput-object v0, LX/6xh;->MOCK:LX/6xh;

    .line 1159116
    new-instance v0, LX/6xh;

    const-string v1, "PAGES_COMMERCE"

    sget-object v2, LX/6xg;->NMOR_PAGES_COMMERCE:LX/6xg;

    invoke-direct {v0, v1, v4, v2}, LX/6xh;-><init>(Ljava/lang/String;ILX/6xg;)V

    sput-object v0, LX/6xh;->PAGES_COMMERCE:LX/6xh;

    .line 1159117
    new-instance v0, LX/6xh;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v5, v2}, LX/6xh;-><init>(Ljava/lang/String;ILX/6xg;)V

    sput-object v0, LX/6xh;->UNKNOWN:LX/6xh;

    .line 1159118
    const/4 v0, 0x3

    new-array v0, v0, [LX/6xh;

    sget-object v1, LX/6xh;->MOCK:LX/6xh;

    aput-object v1, v0, v3

    sget-object v1, LX/6xh;->PAGES_COMMERCE:LX/6xh;

    aput-object v1, v0, v4

    sget-object v1, LX/6xh;->UNKNOWN:LX/6xh;

    aput-object v1, v0, v5

    sput-object v0, LX/6xh;->$VALUES:[LX/6xh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/6xg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6xg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1159112
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1159113
    iput-object p3, p0, LX/6xh;->paymentItemType:LX/6xg;

    .line 1159114
    return-void
.end method

.method public static forValue(Ljava/lang/String;)LX/6xh;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1159107
    invoke-static {}, LX/6xh;->values()[LX/6xh;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/6LV;->a([LX/6LU;Ljava/lang/Object;)LX/6LU;

    move-result-object v0

    sget-object v1, LX/6xh;->UNKNOWN:LX/6xh;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6xh;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6xh;
    .locals 1

    .prologue
    .line 1159111
    const-class v0, LX/6xh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xh;

    return-object v0
.end method

.method public static values()[LX/6xh;
    .locals 1

    .prologue
    .line 1159119
    sget-object v0, LX/6xh;->$VALUES:[LX/6xh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6xh;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1159110
    invoke-virtual {p0}, LX/6xh;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1159109
    invoke-virtual {p0}, LX/6xh;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1159108
    invoke-virtual {p0}, LX/6xh;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
