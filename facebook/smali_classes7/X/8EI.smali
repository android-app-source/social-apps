.class public LX/8EI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final b:LX/0id;

.field public final c:LX/8E2;

.field public final d:LX/8Do;

.field public final e:J

.field public f:Z

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0id;LX/8E2;LX/8Do;J)V
    .locals 1
    .param p5    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1313539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1313540
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8EI;->g:Ljava/util/List;

    .line 1313541
    iput-object p1, p0, LX/8EI;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1313542
    iput-object p2, p0, LX/8EI;->b:LX/0id;

    .line 1313543
    iput-object p3, p0, LX/8EI;->c:LX/8E2;

    .line 1313544
    iput-object p4, p0, LX/8EI;->d:LX/8Do;

    .line 1313545
    iput-wide p5, p0, LX/8EI;->e:J

    .line 1313546
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x13007d

    .line 1313547
    const-string v0, "%s:%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1313548
    iget-object v1, p0, LX/8EI;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1313549
    iget-object v1, p0, LX/8EI;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 1313550
    :goto_0
    return-void

    .line 1313551
    :cond_0
    iget-object v1, p0, LX/8EI;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1313552
    iget-boolean v0, p0, LX/8EI;->f:Z

    if-eqz v0, :cond_0

    .line 1313553
    :goto_0
    return-void

    .line 1313554
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8EI;->f:Z

    .line 1313555
    iget-object v0, p0, LX/8EI;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x13007d

    const/16 v2, 0x7a

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1313556
    iget-object v0, p0, LX/8EI;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x13007d

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1313557
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 1313558
    iget-object v0, p0, LX/8EI;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x13007d

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1313559
    return-void
.end method
