.class public final LX/6p9;
.super LX/6nn;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6nn",
        "<",
        "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V
    .locals 0

    .prologue
    .line 1148910
    iput-object p1, p0, LX/6p9;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    invoke-direct {p0}, LX/6nn;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1148911
    iget-object v0, p0, LX/6p9;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    .line 1148912
    iget-object v1, v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->k:Landroid/widget/ProgressBar;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1148913
    return-void
.end method

.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 1148914
    iget-object v0, p0, LX/6p9;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    invoke-static {v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->v(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    .line 1148915
    iget-object v0, p0, LX/6p9;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v0, v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->e:Landroid/content/Context;

    new-instance v1, LX/6p8;

    invoke-direct {v1, p0}, LX/6p8;-><init>(LX/6p9;)V

    invoke-static {v0, p1, v1}, LX/6up;->a(Landroid/content/Context;Lcom/facebook/fbservice/service/ServiceException;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1148916
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1148917
    check-cast p1, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1148918
    iget-object v0, p0, LX/6p9;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    invoke-static {v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->v(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    .line 1148919
    iget-object v0, p0, LX/6p9;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    invoke-static {v0, p1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a$redex0(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/model/PaymentPin;)V

    .line 1148920
    return-void
.end method
