.class public final LX/6oF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;)V
    .locals 0

    .prologue
    .line 1147959
    iput-object p1, p0, LX/6oF;->a:Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1147960
    invoke-static {p1}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v0

    .line 1147961
    const-string v1, "FingerprintNuxDialogFragment"

    const-string v2, "Failed to create nonce"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1147962
    iget-object v1, p0, LX/6oF;->a:Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1147963
    iget-object v2, v0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v2, v2

    .line 1147964
    sget-object v3, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v2, v3, :cond_0

    if-eqz v1, :cond_0

    .line 1147965
    invoke-static {v1, v0}, LX/6up;->a(Landroid/content/Context;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1147966
    :cond_0
    iget-object v0, p0, LX/6oF;->a:Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1147967
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1147968
    check-cast p1, Ljava/lang/String;

    .line 1147969
    iget-object v0, p0, LX/6oF;->a:Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;

    iget-object v0, v0, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->o:LX/6oC;

    invoke-virtual {v0, p1}, LX/6oC;->a(Ljava/lang/String;)V

    .line 1147970
    iget-object v0, p0, LX/6oF;->a:Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;

    iget-object v0, v0, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->n:LX/6o9;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/6o9;->a(Z)V

    .line 1147971
    iget-object v0, p0, LX/6oF;->a:Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1147972
    iget-object v0, p0, LX/6oF;->a:Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;

    iget-object v0, v0, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->p:LX/6o8;

    .line 1147973
    const v1, 0x7f081df6

    const v2, 0x7f081df7

    .line 1147974
    new-instance v3, LX/31Y;

    iget-object p0, v0, LX/6o8;->a:Landroid/content/Context;

    invoke-direct {v3, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v3

    iget-object p0, v0, LX/6o8;->a:Landroid/content/Context;

    const p1, 0x7f080016

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    new-instance p1, LX/6o7;

    invoke-direct {p1, v0}, LX/6o7;-><init>(LX/6o8;)V

    invoke-virtual {v3, p0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->a()LX/2EJ;

    move-result-object v3

    invoke-virtual {v3}, LX/2EJ;->show()V

    .line 1147975
    return-void
.end method
