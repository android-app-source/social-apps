.class public final LX/8Uo;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8TL;

.field public final synthetic b:LX/8Up;


# direct methods
.method public constructor <init>(LX/8Up;LX/8TL;)V
    .locals 0

    .prologue
    .line 1351329
    iput-object p1, p0, LX/8Uo;->b:LX/8Up;

    iput-object p2, p0, LX/8Uo;->a:LX/8TL;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1351320
    iget-object v0, p0, LX/8Uo;->a:LX/8TL;

    invoke-virtual {v0, p1}, LX/8TL;->a(Ljava/lang/Throwable;)V

    .line 1351321
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1351322
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1351323
    if-nez p1, :cond_0

    .line 1351324
    iget-object v0, p0, LX/8Uo;->a:LX/8TL;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Empty result"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/8TL;->a(Ljava/lang/Throwable;)V

    .line 1351325
    :goto_0
    return-void

    .line 1351326
    :cond_0
    iget-object v1, p0, LX/8Uo;->a:LX/8TL;

    .line 1351327
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1351328
    check-cast v0, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;

    invoke-virtual {v1, v0}, LX/8TL;->a(Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;)V

    goto :goto_0
.end method
