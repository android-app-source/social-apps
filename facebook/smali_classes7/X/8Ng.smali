.class public LX/8Ng;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Ne;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Sh;

.field private final c:LX/0ad;

.field private d:I

.field private e:[I

.field private final f:LX/0kb;

.field private final g:LX/8Oq;

.field private final h:LX/0cX;

.field private i:Z

.field private j:I

.field private k:Z

.field private l:Ljava/util/concurrent/Semaphore;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1337775
    const-class v0, LX/8Ng;

    sput-object v0, LX/8Ng;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/0kb;LX/8Oq;LX/0ad;LX/0cX;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1337776
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1337777
    iput-object p1, p0, LX/8Ng;->b:LX/0Sh;

    .line 1337778
    iput-object p3, p0, LX/8Ng;->g:LX/8Oq;

    .line 1337779
    iput-object p5, p0, LX/8Ng;->h:LX/0cX;

    .line 1337780
    iput v0, p0, LX/8Ng;->j:I

    .line 1337781
    iput-boolean v0, p0, LX/8Ng;->k:Z

    .line 1337782
    const/4 v0, 0x0

    iput-object v0, p0, LX/8Ng;->l:Ljava/util/concurrent/Semaphore;

    .line 1337783
    iput-object p2, p0, LX/8Ng;->f:LX/0kb;

    .line 1337784
    iput-object p4, p0, LX/8Ng;->c:LX/0ad;

    .line 1337785
    return-void
.end method

.method private declared-synchronized d()V
    .locals 5

    .prologue
    .line 1337769
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8Ng;->e:[I

    if-nez v0, :cond_0

    .line 1337770
    iget-object v0, p0, LX/8Ng;->c:LX/0ad;

    sget-short v1, LX/8Jz;->a:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/8Ng;->i:Z

    .line 1337771
    iget-object v0, p0, LX/8Ng;->c:LX/0ad;

    sget v1, LX/8Jz;->g:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/8Ng;->d:I

    .line 1337772
    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget-object v2, p0, LX/8Ng;->c:LX/0ad;

    sget v3, LX/8Jz;->b:I

    const/4 v4, 0x5

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/8Ng;->c:LX/0ad;

    sget v3, LX/8Jz;->c:I

    const/16 v4, 0x14

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/8Ng;->c:LX/0ad;

    sget v3, LX/8Jz;->d:I

    const/16 v4, 0x3c

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/8Ng;->c:LX/0ad;

    sget v3, LX/8Jz;->e:I

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/8Ng;->c:LX/0ad;

    sget v3, LX/8Jz;->f:I

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, LX/8Ng;->e:[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1337773
    :cond_0
    monitor-exit p0

    return-void

    .line 1337774
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1337766
    iput v0, p0, LX/8Ng;->j:I

    .line 1337767
    iput-boolean v0, p0, LX/8Ng;->k:Z

    .line 1337768
    return-void
.end method

.method public final a(LX/73z;)V
    .locals 4

    .prologue
    .line 1337786
    invoke-direct {p0}, LX/8Ng;->d()V

    .line 1337787
    iget-boolean v0, p1, LX/73z;->m:Z

    move v0, v0

    .line 1337788
    if-nez v0, :cond_0

    .line 1337789
    iget-object v0, p1, LX/73z;->g:Ljava/lang/Exception;

    move-object v0, v0

    .line 1337790
    throw v0

    .line 1337791
    :cond_0
    iget-boolean v0, p1, LX/73z;->l:Z

    move v0, v0

    .line 1337792
    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/8Ng;->k:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/8Ng;->j:I

    iget v1, p0, LX/8Ng;->d:I

    if-ge v0, v1, :cond_2

    :cond_1
    iget-boolean v0, p0, LX/8Ng;->i:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/8Ng;->f:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1337793
    :cond_2
    iget-object v0, p1, LX/73z;->g:Ljava/lang/Exception;

    move-object v0, v0

    .line 1337794
    throw v0

    .line 1337795
    :cond_3
    iget v0, p0, LX/8Ng;->j:I

    iget-object v1, p0, LX/8Ng;->e:[I

    array-length v1, v1

    if-ge v0, v1, :cond_6

    iget-object v0, p0, LX/8Ng;->e:[I

    iget v1, p0, LX/8Ng;->j:I

    aget v0, v0, v1

    if-ltz v0, :cond_6

    .line 1337796
    iget-object v0, p0, LX/8Ng;->g:LX/8Oq;

    invoke-virtual {v0}, LX/8Oq;->b()V

    .line 1337797
    iget-object v0, p0, LX/8Ng;->e:[I

    iget v1, p0, LX/8Ng;->j:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/8Ng;->j:I

    aget v0, v0, v1

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    .line 1337798
    :try_start_0
    iget-object v2, p0, LX/8Ng;->l:Ljava/util/concurrent/Semaphore;

    if-eqz v2, :cond_4

    .line 1337799
    iget-object v2, p0, LX/8Ng;->l:Ljava/util/concurrent/Semaphore;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1, v3}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1337800
    iget-object v0, p0, LX/8Ng;->l:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1337801
    new-instance v0, Ljava/util/concurrent/CancellationException;

    const-string v1, "Cancel during retry wait"

    invoke-direct {v0, v1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1337802
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/8Ng;->g:LX/8Oq;

    invoke-virtual {v1}, LX/8Oq;->a()V

    throw v0

    .line 1337803
    :cond_4
    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1337804
    :cond_5
    iget-object v0, p0, LX/8Ng;->g:LX/8Oq;

    invoke-virtual {v0}, LX/8Oq;->a()V

    .line 1337805
    return-void

    .line 1337806
    :cond_6
    iget-object v0, p1, LX/73z;->g:Ljava/lang/Exception;

    move-object v0, v0

    .line 1337807
    throw v0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 1337763
    invoke-static {p1}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v0

    .line 1337764
    invoke-virtual {p0, v0}, LX/8Ng;->a(LX/73z;)V

    .line 1337765
    return-void
.end method

.method public final a(Ljava/util/concurrent/Semaphore;)V
    .locals 0

    .prologue
    .line 1337761
    iput-object p1, p0, LX/8Ng;->l:Ljava/util/concurrent/Semaphore;

    .line 1337762
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1337759
    iput-boolean p1, p0, LX/8Ng;->k:Z

    .line 1337760
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1337757
    const/4 v0, 0x5

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1337758
    iget-boolean v0, p0, LX/8Ng;->k:Z

    return v0
.end method
