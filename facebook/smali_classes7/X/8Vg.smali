.class public LX/8Vg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/8TD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1353285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/8Vg;
    .locals 4

    .prologue
    .line 1353272
    const-class v1, LX/8Vg;

    monitor-enter v1

    .line 1353273
    :try_start_0
    sget-object v0, LX/8Vg;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1353274
    sput-object v2, LX/8Vg;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1353275
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1353276
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1353277
    new-instance p0, LX/8Vg;

    invoke-direct {p0}, LX/8Vg;-><init>()V

    .line 1353278
    invoke-static {v0}, LX/8TD;->a(LX/0QB;)LX/8TD;

    move-result-object v3

    check-cast v3, LX/8TD;

    .line 1353279
    iput-object v3, p0, LX/8Vg;->a:LX/8TD;

    .line 1353280
    move-object v0, p0

    .line 1353281
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1353282
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8Vg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1353283
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1353284
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Landroid/content/Context;)Ljava/io/File;
    .locals 2

    .prologue
    .line 1353265
    new-instance v0, Landroid/content/ContextWrapper;

    invoke-direct {v0, p0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 1353266
    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 1353267
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "quicksilver/scoreshare"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1353268
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1353269
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1353270
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 1353271
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 7
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1353213
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1353214
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 1353215
    :try_start_1
    new-instance v0, Ljava/io/File;

    invoke-static {p1}, LX/8Vg;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    const-string v4, "temp_file.jpg"

    invoke-direct {v0, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1353216
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1353217
    const/16 v4, 0x400

    :try_start_2
    new-array v4, v4, [B

    .line 1353218
    :goto_0
    invoke-virtual {v3, v4}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_3

    .line 1353219
    const/4 v6, 0x0

    invoke-virtual {v2, v4, v6, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 1353220
    :catch_0
    move-exception v0

    .line 1353221
    :goto_1
    :try_start_3
    iget-object v4, p0, LX/8Vg;->a:LX/8TD;

    sget-object v5, LX/8TE;->SCREENSHOT_RETRIEVE_FAILURE:LX/8TE;

    const-string v6, "Exception while writing stream to file"

    invoke-virtual {v4, v5, v6, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1353222
    if-eqz v2, :cond_0

    .line 1353223
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 1353224
    :cond_0
    if-eqz v3, :cond_1

    .line 1353225
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_1
    :goto_2
    move-object v0, v1

    .line 1353226
    :cond_2
    :goto_3
    return-object v0

    .line 1353227
    :cond_3
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 1353228
    if-eqz v3, :cond_2

    .line 1353229
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_3

    .line 1353230
    :catch_1
    move-exception v1

    .line 1353231
    iget-object v2, p0, LX/8Vg;->a:LX/8TD;

    sget-object v3, LX/8TE;->SCREENSHOT_RETRIEVE_FAILURE:LX/8TE;

    const-string v4, "Exception during closing streams while getting file handle"

    invoke-virtual {v2, v3, v4, v1}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 1353232
    :catch_2
    move-exception v0

    .line 1353233
    iget-object v2, p0, LX/8Vg;->a:LX/8TD;

    sget-object v3, LX/8TE;->SCREENSHOT_RETRIEVE_FAILURE:LX/8TE;

    const-string v4, "Exception during closing streams while getting file handle"

    invoke-virtual {v2, v3, v4, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1353234
    :catchall_0
    move-exception v0

    move-object v3, v1

    .line 1353235
    :goto_4
    if-eqz v1, :cond_4

    .line 1353236
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 1353237
    :cond_4
    if-eqz v3, :cond_5

    .line 1353238
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 1353239
    :cond_5
    :goto_5
    throw v0

    .line 1353240
    :catch_3
    move-exception v1

    .line 1353241
    iget-object v2, p0, LX/8Vg;->a:LX/8TD;

    sget-object v3, LX/8TE;->SCREENSHOT_RETRIEVE_FAILURE:LX/8TE;

    const-string v4, "Exception during closing streams while getting file handle"

    invoke-virtual {v2, v3, v4, v1}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 1353242
    :catchall_1
    move-exception v0

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_4

    .line 1353243
    :catch_4
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    goto :goto_1

    :catch_5
    move-exception v0

    move-object v2, v1

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Landroid/graphics/Bitmap;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1353244
    new-instance v3, Ljava/io/File;

    invoke-static {p1}, LX/8Vg;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    const-string v1, "instant_games_screenshot.jpg"

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1353245
    const/4 v2, 0x0

    .line 1353246
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1353247
    :try_start_1
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p2, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1353248
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1353249
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".quicksilver.fileprovider"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v3}, Landroid/support/v4/content/FileProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1353250
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1353251
    :catch_0
    move-exception v0

    .line 1353252
    iget-object v1, p0, LX/8Vg;->a:LX/8TD;

    sget-object v2, LX/8TE;->SCREENSHOT_HANDLE_GENERATION_FAILURE:LX/8TE;

    const-string v4, "Exception during saving image to temporary stream"

    invoke-virtual {v1, v2, v4, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1353253
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 1353254
    :goto_1
    :try_start_3
    iget-object v2, p0, LX/8Vg;->a:LX/8TD;

    sget-object v4, LX/8TE;->SCREENSHOT_HANDLE_GENERATION_FAILURE:LX/8TE;

    const-string v5, "Exception during compressing image"

    invoke-virtual {v2, v4, v5, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1353255
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1353256
    :catch_2
    move-exception v0

    .line 1353257
    iget-object v1, p0, LX/8Vg;->a:LX/8TD;

    sget-object v2, LX/8TE;->SCREENSHOT_HANDLE_GENERATION_FAILURE:LX/8TE;

    const-string v4, "Exception during saving image to temporary stream"

    invoke-virtual {v1, v2, v4, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1353258
    :catchall_0
    move-exception v0

    move-object v1, v2

    .line 1353259
    :goto_2
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1353260
    :goto_3
    throw v0

    .line 1353261
    :catch_3
    move-exception v1

    .line 1353262
    iget-object v2, p0, LX/8Vg;->a:LX/8TD;

    sget-object v3, LX/8TE;->SCREENSHOT_HANDLE_GENERATION_FAILURE:LX/8TE;

    const-string v4, "Exception during saving image to temporary stream"

    invoke-virtual {v2, v3, v4, v1}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 1353263
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 1353264
    :catch_4
    move-exception v0

    goto :goto_1
.end method
