.class public LX/7h2;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/audience/model/Reply;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1225211
    new-instance v0, LX/7h1;

    invoke-direct {v0}, LX/7h1;-><init>()V

    sput-object v0, LX/7h2;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1225212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/audience/model/ReplyThread;)Lcom/facebook/audience/model/Reply;
    .locals 2

    .prologue
    .line 1225213
    iget-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v0, v0

    .line 1225214
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1225215
    iget-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    move-object v0, v0

    .line 1225216
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/Reply;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/audience/model/ReplyThread;LX/0Px;)Lcom/facebook/audience/model/ReplyThread;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/audience/model/ReplyThread;",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/Reply;",
            ">;)",
            "Lcom/facebook/audience/model/ReplyThread;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1225217
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1225218
    :goto_0
    return-object p0

    .line 1225219
    :cond_0
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1225220
    iget-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v4, v0

    .line 1225221
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/Reply;

    .line 1225222
    iget-object v6, v0, Lcom/facebook/audience/model/Reply;->g:Ljava/lang/String;

    move-object v6, v6

    .line 1225223
    invoke-interface {v3, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1225224
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1225225
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    :goto_2
    if-ge v1, v2, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/Reply;

    .line 1225226
    iget-object v4, v0, Lcom/facebook/audience/model/Reply;->g:Ljava/lang/String;

    move-object v4, v4

    .line 1225227
    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1225228
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1225229
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1225230
    sget-object v1, LX/7h2;->a:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1225231
    invoke-static {p0}, Lcom/facebook/audience/model/ReplyThread;->a(Lcom/facebook/audience/model/ReplyThread;)LX/7gs;

    move-result-object v1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1225232
    iput-object v0, v1, LX/7gs;->e:LX/0Px;

    .line 1225233
    move-object v0, v1

    .line 1225234
    invoke-virtual {v0}, LX/7gs;->a()Lcom/facebook/audience/model/ReplyThread;

    move-result-object p0

    goto :goto_0
.end method
