.class public LX/8L4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile B:LX/8L4;


# instance fields
.field public A:LX/2HB;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "itself"
    .end annotation
.end field

.field public final a:Z

.field public final b:Landroid/content/Context;

.field private final c:Landroid/app/NotificationManager;

.field public final d:LX/0b3;

.field public final e:LX/8I0;

.field public final f:LX/8Ko;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/8Kn;

.field public final i:Lcom/facebook/content/SecureContextHelper;

.field public final j:LX/03V;

.field private final k:LX/17Y;

.field public final l:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation
.end field

.field private final m:LX/1RW;

.field private final n:LX/8Kz;

.field private final o:LX/8L3;

.field private final p:LX/8L1;

.field public final q:LX/8L0;

.field public final r:Landroid/app/PendingIntent;

.field public final s:LX/7m8;

.field private t:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final u:LX/0ad;

.field private final v:LX/0SG;

.field private final w:LX/0kb;

.field public final x:LX/7ma;

.field public final y:Z

.field public z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/NotificationManager;LX/0b3;LX/8I0;LX/8Ko;LX/0Or;LX/8Kn;Lcom/facebook/content/SecureContextHelper;LX/03V;LX/7m8;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/17Y;Ljava/lang/String;LX/0ad;LX/0SG;LX/0kb;LX/7ma;LX/1RW;)V
    .locals 5
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/upload/gatekeeper/IsMediaUploadCancelEnabled;
        .end annotation
    .end param
    .param p13    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/app/NotificationManager;",
            "LX/0b3;",
            "Lcom/facebook/photos/intent/PhotosViewIntentBuilder;",
            "Lcom/facebook/photos/upload/manager/UploadNotificationConfiguration;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/photos/upload/intent/VideoUploadSuccessIntent;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/7m8;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/17Y;",
            "Ljava/lang/String;",
            "LX/0ad;",
            "LX/0SG;",
            "LX/0kb;",
            "LX/7ma;",
            "LX/1RW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1331445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1331446
    const-string v1, "MediaUpload"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    iput-boolean v1, p0, LX/8L4;->a:Z

    .line 1331447
    new-instance v1, LX/8Kz;

    invoke-direct {v1, p0}, LX/8Kz;-><init>(LX/8L4;)V

    iput-object v1, p0, LX/8L4;->n:LX/8Kz;

    .line 1331448
    new-instance v1, LX/8L3;

    invoke-direct {v1, p0}, LX/8L3;-><init>(LX/8L4;)V

    iput-object v1, p0, LX/8L4;->o:LX/8L3;

    .line 1331449
    new-instance v1, LX/8L1;

    invoke-direct {v1, p0}, LX/8L1;-><init>(LX/8L4;)V

    iput-object v1, p0, LX/8L4;->p:LX/8L1;

    .line 1331450
    new-instance v1, LX/8L0;

    invoke-direct {v1, p0}, LX/8L0;-><init>(LX/8L4;)V

    iput-object v1, p0, LX/8L4;->q:LX/8L0;

    .line 1331451
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/8L4;->z:Z

    .line 1331452
    iput-object p1, p0, LX/8L4;->b:Landroid/content/Context;

    .line 1331453
    iput-object p2, p0, LX/8L4;->c:Landroid/app/NotificationManager;

    .line 1331454
    iput-object p3, p0, LX/8L4;->d:LX/0b3;

    .line 1331455
    iput-object p4, p0, LX/8L4;->e:LX/8I0;

    .line 1331456
    iput-object p5, p0, LX/8L4;->f:LX/8Ko;

    .line 1331457
    iput-object p6, p0, LX/8L4;->g:LX/0Or;

    .line 1331458
    iput-object p7, p0, LX/8L4;->h:LX/8Kn;

    .line 1331459
    iput-object p8, p0, LX/8L4;->i:Lcom/facebook/content/SecureContextHelper;

    .line 1331460
    iput-object p9, p0, LX/8L4;->j:LX/03V;

    .line 1331461
    iput-object p10, p0, LX/8L4;->s:LX/7m8;

    .line 1331462
    move-object/from16 v0, p12

    iput-object v0, p0, LX/8L4;->k:LX/17Y;

    .line 1331463
    move-object/from16 v0, p13

    iput-object v0, p0, LX/8L4;->l:Ljava/lang/String;

    .line 1331464
    move-object/from16 v0, p11

    iput-object v0, p0, LX/8L4;->t:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1331465
    move-object/from16 v0, p14

    iput-object v0, p0, LX/8L4;->u:LX/0ad;

    .line 1331466
    move-object/from16 v0, p15

    iput-object v0, p0, LX/8L4;->v:LX/0SG;

    .line 1331467
    move-object/from16 v0, p16

    iput-object v0, p0, LX/8L4;->w:LX/0kb;

    .line 1331468
    move-object/from16 v0, p17

    iput-object v0, p0, LX/8L4;->x:LX/7ma;

    .line 1331469
    move-object/from16 v0, p18

    iput-object v0, p0, LX/8L4;->m:LX/1RW;

    .line 1331470
    invoke-direct {p0}, LX/8L4;->d()Z

    move-result v1

    iput-boolean v1, p0, LX/8L4;->y:Z

    .line 1331471
    iget-object v1, p0, LX/8L4;->d:LX/0b3;

    iget-object v2, p0, LX/8L4;->n:LX/8Kz;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 1331472
    iget-object v1, p0, LX/8L4;->d:LX/0b3;

    iget-object v2, p0, LX/8L4;->o:LX/8L3;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 1331473
    iget-object v1, p0, LX/8L4;->d:LX/0b3;

    iget-object v2, p0, LX/8L4;->p:LX/8L1;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 1331474
    iget-object v1, p0, LX/8L4;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/03R;->asBoolean(Z)Z

    move-result v1

    iput-boolean v1, p0, LX/8L4;->z:Z

    .line 1331475
    iget-object v1, p0, LX/8L4;->b:Landroid/content/Context;

    sget-object v2, LX/0ax;->cL:Ljava/lang/String;

    move-object/from16 v0, p11

    invoke-interface {v0, v1, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1331476
    if-nez v1, :cond_0

    .line 1331477
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1331478
    :cond_0
    iget-object v2, p0, LX/8L4;->b:Landroid/content/Context;

    const/16 v3, 0x6019

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v1, v4}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, p0, LX/8L4;->r:Landroid/app/PendingIntent;

    .line 1331479
    new-instance v1, LX/2HB;

    iget-object v2, p0, LX/8L4;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/2HB;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/8L4;->A:LX/2HB;

    .line 1331480
    return-void
.end method

.method public static a(LX/0QB;)LX/8L4;
    .locals 3

    .prologue
    .line 1331481
    sget-object v0, LX/8L4;->B:LX/8L4;

    if-nez v0, :cond_1

    .line 1331482
    const-class v1, LX/8L4;

    monitor-enter v1

    .line 1331483
    :try_start_0
    sget-object v0, LX/8L4;->B:LX/8L4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1331484
    if-eqz v2, :cond_0

    .line 1331485
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/8L4;->b(LX/0QB;)LX/8L4;

    move-result-object v0

    sput-object v0, LX/8L4;->B:LX/8L4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1331486
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1331487
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1331488
    :cond_1
    sget-object v0, LX/8L4;->B:LX/8L4;

    return-object v0

    .line 1331489
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1331490
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/8L4;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 1331491
    iget-object v0, p0, LX/8L4;->b:Landroid/content/Context;

    const/16 v1, 0x6019

    invoke-static {p0}, LX/8L4;->b(LX/8L4;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    .line 1331492
    const-string v0, "@["

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1331493
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1331494
    :goto_0
    return-void

    .line 1331495
    :cond_0
    const-string v0, "@\\[(\\d+):(\\d+:)?((\\w|\\s)+)\\]"

    const/16 v1, 0x40

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 1331496
    const/4 v0, 0x0

    .line 1331497
    :goto_1
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1331498
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1331499
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1331500
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    goto :goto_1

    .line 1331501
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static a$redex0(LX/8L4;Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1331427
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1331428
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1331429
    :goto_0
    return-object p1

    .line 1331430
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1331431
    invoke-static {v0, v1}, LX/8L4;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1331432
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1331433
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 1331434
    :cond_1
    iget-object v0, p0, LX/8L4;->b:Landroid/content/Context;

    const v2, 0x7f082020

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static a$redex0(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;I)V
    .locals 7

    .prologue
    .line 1331502
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->N:Ljava/lang/String;

    move-object v0, v0

    .line 1331503
    if-nez v0, :cond_0

    .line 1331504
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 1331505
    :goto_0
    iget-object v1, p0, LX/8L4;->s:LX/7m8;

    .line 1331506
    iget-wide v5, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v2, v5

    .line 1331507
    const/16 v4, 0x3e7

    invoke-static {p2, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {v1, v0, v2, v3, v4}, LX/7m8;->a(Ljava/lang/String;JI)V

    .line 1331508
    return-void

    .line 1331509
    :cond_0
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->N:Ljava/lang/String;

    move-object v0, v0

    .line 1331510
    goto :goto_0
.end method

.method public static a$redex0(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/app/Notification;)V
    .locals 1

    .prologue
    .line 1331511
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1331512
    :goto_0
    return-void

    .line 1331513
    :cond_0
    invoke-direct {p0, p1, p2}, LX/8L4;->b(Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/app/Notification;)V

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/8L4;
    .locals 20

    .prologue
    .line 1331514
    new-instance v1, LX/8L4;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/1s4;->a(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    invoke-static/range {p0 .. p0}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v4

    check-cast v4, LX/0b3;

    invoke-static/range {p0 .. p0}, LX/8I0;->a(LX/0QB;)LX/8I0;

    move-result-object v5

    check-cast v5, LX/8I0;

    invoke-static/range {p0 .. p0}, LX/8LN;->a(LX/0QB;)LX/8Ko;

    move-result-object v6

    check-cast v6, LX/8Ko;

    const/16 v7, 0x345

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/8Kn;->a(LX/0QB;)LX/8Kn;

    move-result-object v8

    check-cast v8, LX/8Kn;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static/range {p0 .. p0}, LX/7m8;->a(LX/0QB;)LX/7m8;

    move-result-object v11

    check-cast v11, LX/7m8;

    invoke-static/range {p0 .. p0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v12

    check-cast v12, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static/range {p0 .. p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v13

    check-cast v13, LX/17Y;

    invoke-static/range {p0 .. p0}, LX/0dG;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v15

    check-cast v15, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v16

    check-cast v16, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v17

    check-cast v17, LX/0kb;

    invoke-static/range {p0 .. p0}, LX/7ma;->a(LX/0QB;)LX/7ma;

    move-result-object v18

    check-cast v18, LX/7ma;

    invoke-static/range {p0 .. p0}, LX/1RW;->a(LX/0QB;)LX/1RW;

    move-result-object v19

    check-cast v19, LX/1RW;

    invoke-direct/range {v1 .. v19}, LX/8L4;-><init>(Landroid/content/Context;Landroid/app/NotificationManager;LX/0b3;LX/8I0;LX/8Ko;LX/0Or;LX/8Kn;Lcom/facebook/content/SecureContextHelper;LX/03V;LX/7m8;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/17Y;Ljava/lang/String;LX/0ad;LX/0SG;LX/0kb;LX/7ma;LX/1RW;)V

    .line 1331515
    return-object v1
.end method

.method public static b(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/content/Intent;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 1331516
    invoke-static {p1}, LX/8L4;->m(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1331517
    invoke-static {p1}, LX/8L4;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, LX/8L4;->n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/8L4;->y:Z

    if-nez v0, :cond_0

    .line 1331518
    invoke-static {p0}, LX/8L4;->a(LX/8L4;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1331519
    :goto_0
    return-object v0

    .line 1331520
    :cond_0
    iget-object v0, p0, LX/8L4;->b:Landroid/content/Context;

    const/16 v1, 0x6019

    new-instance v2, LX/8KU;

    iget-object v3, p0, LX/8L4;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/8KU;-><init>(Landroid/content/Context;)V

    const-string v3, "upload_options"

    .line 1331521
    iput-object v3, v2, LX/8KU;->b:Ljava/lang/String;

    .line 1331522
    move-object v2, v2

    .line 1331523
    iput-object p1, v2, LX/8KU;->c:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1331524
    move-object v2, v2

    .line 1331525
    iput-object p2, v2, LX/8KU;->d:Landroid/content/Intent;

    .line 1331526
    move-object v2, v2

    .line 1331527
    invoke-virtual {v2}, LX/8KU;->a()Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(LX/8L4;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1331528
    iget-object v0, p0, LX/8L4;->b:Landroid/content/Context;

    sget-object v1, LX/8Lr;->UPLOAD_NOTIFICATION:LX/8Lr;

    invoke-static {v0, v1}, Lcom/facebook/photos/upload/progresspage/CompostActivity;->a(Landroid/content/Context;LX/8Lr;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/app/Notification;)V
    .locals 3

    .prologue
    .line 1331529
    iget-object v0, p0, LX/8L4;->f:LX/8Ko;

    invoke-static {p0, p1}, LX/8L4;->n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v1

    invoke-virtual {v0, v1}, LX/8Ko;->a(Z)V

    .line 1331530
    iget-object v0, p0, LX/8L4;->c:Landroid/app/NotificationManager;

    const-string v1, "UploadNotificationManager"

    iget-object v2, p0, LX/8L4;->f:LX/8Ko;

    invoke-virtual {v2, p1}, LX/8Ko;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)I

    move-result v2

    invoke-virtual {v0, v1, v2, p2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 1331531
    return-void
.end method

.method public static c(LX/8L4;)Landroid/app/PendingIntent;
    .locals 5

    .prologue
    .line 1331532
    iget-object v0, p0, LX/8L4;->b:Landroid/content/Context;

    const/16 v1, 0x6019

    iget-object v2, p0, LX/8L4;->k:LX/17Y;

    iget-object v3, p0, LX/8L4;->b:Landroid/content/Context;

    sget-object v4, LX/0ax;->gp:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 1331435
    const-string v0, "UploadNotificationManager"

    const-string v1, "getSuccessNotificationPendingIntent"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1331436
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/8L4;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/photos/upload/service/PhotosUploadHelperService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1331437
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "com.facebook.photos.upload.service.success."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1331438
    iget-object v2, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, v2

    .line 1331439
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1331440
    invoke-static {p1}, LX/8L4;->m(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1331441
    const-string v1, "uploadOp"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1331442
    const-string v1, "success_result"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1331443
    iget-object v1, p0, LX/8L4;->b:Landroid/content/Context;

    const/16 v2, 0x6019

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1331444
    return-object v0
.end method

.method private d()Z
    .locals 3

    .prologue
    .line 1331533
    iget-object v0, p0, LX/8L4;->u:LX/0ad;

    sget-short v1, LX/1aO;->S:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static f(Lcom/facebook/photos/upload/operation/UploadOperation;)Z
    .locals 1

    .prologue
    .line 1331139
    invoke-virtual {p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v0

    return v0
.end method

.method public static g(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Landroid/app/PendingIntent;
    .locals 2

    .prologue
    .line 1331140
    iget-boolean v0, p0, LX/8L4;->y:Z

    if-eqz v0, :cond_0

    .line 1331141
    sget-object v0, LX/8Ky;->a:[I

    invoke-static {p0, p1}, LX/8L4;->o(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)LX/8L2;

    move-result-object v1

    invoke-virtual {v1}, LX/8L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1331142
    iget-object v0, p0, LX/8L4;->r:Landroid/app/PendingIntent;

    .line 1331143
    :goto_0
    return-object v0

    .line 1331144
    :pswitch_0
    invoke-static {p0}, LX/8L4;->a(LX/8L4;)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0

    .line 1331145
    :pswitch_1
    invoke-direct {p0, p1}, LX/8L4;->l(Lcom/facebook/photos/upload/operation/UploadOperation;)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0

    .line 1331146
    :pswitch_2
    invoke-direct {p0, p1}, LX/8L4;->k(Lcom/facebook/photos/upload/operation/UploadOperation;)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0

    .line 1331147
    :cond_0
    invoke-static {p1}, LX/8L4;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, LX/8L4;->l(Lcom/facebook/photos/upload/operation/UploadOperation;)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, LX/8L4;->k(Lcom/facebook/photos/upload/operation/UploadOperation;)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static i(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 3

    .prologue
    .line 1331148
    invoke-static {p0, p1}, LX/8L4;->n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8L4;->x:LX/7ma;

    invoke-virtual {v0}, LX/7ma;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1331149
    :cond_0
    iget-object v0, p0, LX/8L4;->f:LX/8Ko;

    invoke-static {p0, p1}, LX/8L4;->n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v1

    invoke-virtual {v0, v1}, LX/8Ko;->a(Z)V

    .line 1331150
    iget-object v0, p0, LX/8L4;->c:Landroid/app/NotificationManager;

    const-string v1, "UploadNotificationManager"

    iget-object v2, p0, LX/8L4;->f:LX/8Ko;

    invoke-virtual {v2, p1}, LX/8Ko;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 1331151
    :cond_1
    return-void
.end method

.method private k(Lcom/facebook/photos/upload/operation/UploadOperation;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 1331152
    iget-boolean v0, p0, LX/8L4;->z:Z

    if-nez v0, :cond_0

    .line 1331153
    iget-object v0, p0, LX/8L4;->r:Landroid/app/PendingIntent;

    .line 1331154
    :goto_0
    return-object v0

    .line 1331155
    :cond_0
    iget-boolean v0, p0, LX/8L4;->y:Z

    if-nez v0, :cond_1

    invoke-static {p0, p1}, LX/8L4;->n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1331156
    invoke-static {p0}, LX/8L4;->a(LX/8L4;)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0

    .line 1331157
    :cond_1
    invoke-static {p1}, LX/8L4;->m(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1331158
    iget-object v0, p0, LX/8L4;->b:Landroid/content/Context;

    const/16 v1, 0x6019

    new-instance v2, LX/8KU;

    iget-object v3, p0, LX/8L4;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/8KU;-><init>(Landroid/content/Context;)V

    const-string v3, "cancel_request"

    .line 1331159
    iput-object v3, v2, LX/8KU;->b:Ljava/lang/String;

    .line 1331160
    move-object v2, v2

    .line 1331161
    iput-object p1, v2, LX/8KU;->c:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1331162
    move-object v2, v2

    .line 1331163
    invoke-virtual {v2}, LX/8KU;->a()Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method private l(Lcom/facebook/photos/upload/operation/UploadOperation;)Landroid/app/PendingIntent;
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1331164
    iget-wide v6, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v0, v6

    .line 1331165
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1331166
    iget-wide v6, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v0, v6

    .line 1331167
    iget-object v2, p0, LX/8L4;->l:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1331168
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1331169
    const-string v1, "profile_video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1331170
    :cond_0
    iget-object v0, p0, LX/8L4;->r:Landroid/app/PendingIntent;

    .line 1331171
    :goto_0
    return-object v0

    .line 1331172
    :cond_1
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1331173
    const-string v1, "profile_video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1331174
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    new-array v1, v5, [Ljava/lang/Object;

    .line 1331175
    iget-wide v6, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v2, v6

    .line 1331176
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1331177
    :goto_1
    iget-object v1, p0, LX/8L4;->t:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/8L4;->b:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1331178
    iget-object v1, p0, LX/8L4;->b:Landroid/content/Context;

    const/16 v2, 0x6019

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0

    .line 1331179
    :cond_2
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1331180
    const-string v1, "wall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1331181
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    new-array v1, v5, [Ljava/lang/Object;

    .line 1331182
    iget-wide v6, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v2, v6

    .line 1331183
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1331184
    :cond_3
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1331185
    const-string v1, "event"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1331186
    sget-object v0, LX/0ax;->B:Ljava/lang/String;

    new-array v1, v5, [Ljava/lang/Object;

    .line 1331187
    iget-wide v6, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v2, v6

    .line 1331188
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1331189
    :cond_4
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1331190
    const-string v1, "group"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1331191
    sget-object v0, LX/0ax;->C:Ljava/lang/String;

    new-array v1, v5, [Ljava/lang/Object;

    .line 1331192
    iget-wide v6, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v2, v6

    .line 1331193
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1331194
    :cond_5
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1331195
    const-string v1, "page"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1331196
    sget-object v0, LX/0ax;->aE:Ljava/lang/String;

    new-array v1, v5, [Ljava/lang/Object;

    .line 1331197
    iget-wide v6, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v2, v6

    .line 1331198
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 1331199
    :cond_6
    iget-object v0, p0, LX/8L4;->r:Landroid/app/PendingIntent;

    goto/16 :goto_0
.end method

.method private static m(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 2

    .prologue
    .line 1331200
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1331201
    sget-object v1, LX/8LS;->PROFILE_VIDEO:LX/8LS;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->A()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1331202
    invoke-virtual {p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->A()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "thumbnail_bitmap"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1331203
    :cond_0
    return-void
.end method

.method public static n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z
    .locals 4

    .prologue
    .line 1331204
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1331205
    sget-object v1, LX/8LS;->PROFILE_PIC:LX/8LS;

    if-eq v0, v1, :cond_0

    .line 1331206
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1331207
    sget-object v1, LX/8LS;->COVER_PHOTO:LX/8LS;

    if-eq v0, v1, :cond_0

    .line 1331208
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    move-object v0, v0

    .line 1331209
    if-eqz v0, :cond_1

    .line 1331210
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    move-object v0, v0

    .line 1331211
    sget-object v1, LX/21D;->NEWSFEED:LX/21D;

    invoke-virtual {v1}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1331212
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    move-object v0, v0

    .line 1331213
    sget-object v1, LX/21D;->TIMELINE:LX/21D;

    invoke-virtual {v1}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1331214
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    move-object v0, v0

    .line 1331215
    sget-object v1, LX/21D;->GROUP_FEED:LX/21D;

    invoke-virtual {v1}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1331216
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    move-object v0, v0

    .line 1331217
    sget-object v1, LX/21D;->EVENT:LX/21D;

    invoke-virtual {v1}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1331218
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    move-object v0, v0

    .line 1331219
    sget-object v1, LX/21D;->PAGE_FEED:LX/21D;

    invoke-virtual {v1}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1331220
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    move-object v0, v0

    .line 1331221
    sget-object v1, LX/21D;->COMPOST:LX/21D;

    invoke-virtual {v1}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1331222
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    move-object v0, v0

    .line 1331223
    sget-object v1, LX/21D;->THIRD_PARTY_APP_VIA_INTENT:LX/21D;

    invoke-virtual {v1}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1331224
    :goto_0
    iget-object v1, p0, LX/8L4;->m:LX/1RW;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1331225
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    move-object v3, v3

    .line 1331226
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1331227
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v3, v3

    .line 1331228
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1331229
    iget-object v3, v1, LX/1RW;->a:LX/0Zb;

    const-string p0, "is_whitelisted"

    invoke-static {v1, p0}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "is_whitelisted"

    invoke-virtual {p0, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "details"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v3, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1331230
    return v0

    .line 1331231
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)LX/8L2;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1331232
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v0, v0

    .line 1331233
    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->af()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1331234
    :cond_0
    sget-object v0, LX/8L2;->DISABLED:LX/8L2;

    .line 1331235
    :goto_0
    return-object v0

    .line 1331236
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ac()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1331237
    sget-object v0, LX/8L2;->BACKSTAGE:LX/8L2;

    goto :goto_0

    .line 1331238
    :cond_2
    invoke-static {p0, p1}, LX/8L4;->n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    .line 1331239
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1331240
    if-eqz v0, :cond_3

    iget-object v0, p0, LX/8L4;->u:LX/0ad;

    sget-short v1, LX/1aO;->U:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1331241
    sget-object v0, LX/8L2;->COMPOST:LX/8L2;

    goto :goto_0

    .line 1331242
    :cond_3
    sget-object v0, LX/8L2;->DEFAULT:LX/8L2;

    goto :goto_0

    .line 1331243
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1331244
    if-eqz v0, :cond_5

    iget-object v0, p0, LX/8L4;->u:LX/0ad;

    sget-short v1, LX/1aO;->Y:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1331245
    sget-object v0, LX/8L2;->COMPOST:LX/8L2;

    goto :goto_0

    .line 1331246
    :cond_5
    sget-object v0, LX/8L2;->VIDEO:LX/8L2;

    goto :goto_0

    .line 1331247
    :cond_6
    if-eqz v0, :cond_7

    .line 1331248
    sget-object v0, LX/8L2;->COMPOST:LX/8L2;

    goto :goto_0

    .line 1331249
    :cond_7
    sget-object v0, LX/8L2;->DEFAULT:LX/8L2;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1331250
    invoke-static {p1}, LX/8L4;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->k()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1331251
    :cond_0
    iget-boolean v2, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v2, v2

    .line 1331252
    if-eqz v2, :cond_2

    .line 1331253
    :cond_1
    :goto_0
    return-void

    .line 1331254
    :cond_2
    :try_start_0
    iget-object v2, p0, LX/8L4;->f:LX/8Ko;

    .line 1331255
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, LX/8L4;->b:Landroid/content/Context;

    const-class v5, Lcom/facebook/photos/upload/service/PhotosUploadHelperService;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1331256
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "com.facebook.photos.upload.service.retry."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1331257
    iget-object v5, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v5, v5

    .line 1331258
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1331259
    const-string v4, "uploadOp"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1331260
    move-object v5, v3

    .line 1331261
    iget-boolean v3, p0, LX/8L4;->y:Z

    if-eqz v3, :cond_6

    .line 1331262
    sget-object v3, LX/8Ky;->a:[I

    invoke-static {p0, p1}, LX/8L4;->o(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)LX/8L2;

    move-result-object v4

    invoke-virtual {v4}, LX/8L2;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1331263
    :pswitch_0
    iget-object v3, p0, LX/8L4;->r:Landroid/app/PendingIntent;

    .line 1331264
    :goto_1
    move-object v6, v3

    .line 1331265
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->k()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1331266
    iget-object v3, p0, LX/8L4;->b:Landroid/content/Context;

    invoke-virtual {v2, v3, p1}, LX/8Ko;->a(Landroid/content/Context;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;

    move-result-object v4

    .line 1331267
    iget-object v3, p0, LX/8L4;->b:Landroid/content/Context;

    invoke-virtual {v2, v3}, LX/8Ko;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, p1}, LX/8L4;->a$redex0(LX/8L4;Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;

    move-result-object v3

    .line 1331268
    invoke-virtual {v2}, LX/8Ko;->a()I

    move-result v2

    .line 1331269
    :goto_2
    invoke-static {p0, p1}, LX/8L4;->i(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1331270
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->h()Z

    move-result v7

    .line 1331271
    new-instance v8, LX/2HB;

    iget-object v9, p0, LX/8L4;->b:Landroid/content/Context;

    invoke-direct {v8, v9}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v2}, LX/2HB;->a(I)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    if-eqz v7, :cond_5

    invoke-static {p0, p1}, LX/8L4;->n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v3

    if-nez v3, :cond_5

    :goto_3
    invoke-virtual {v2, v0}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v0

    .line 1331272
    iput-object v6, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1331273
    move-object v0, v0

    .line 1331274
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    .line 1331275
    invoke-direct {p0, p1, v0}, LX/8L4;->b(Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/app/Notification;)V

    .line 1331276
    iget-boolean v0, p0, LX/8L4;->a:Z

    if-eqz v0, :cond_3

    .line 1331277
    :cond_3
    iget-object v0, p0, LX/8L4;->d:LX/0b3;

    new-instance v1, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v5, v2, v7}, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/content/Intent;ZZ)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1331278
    :catch_0
    move-exception v0

    .line 1331279
    iget-object v1, p0, LX/8L4;->j:LX/03V;

    const-string v2, "Upload failure throwable"

    invoke-virtual {v1, v2, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1331280
    :cond_4
    :try_start_1
    iget-object v3, p0, LX/8L4;->b:Landroid/content/Context;

    .line 1331281
    const v4, 0x7f082030

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v3}, LX/8Ko;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v3, v4, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 1331282
    iget-object v3, p0, LX/8L4;->b:Landroid/content/Context;

    invoke-virtual {v2, v3}, LX/8Ko;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, p1}, LX/8L4;->a$redex0(LX/8L4;Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;

    move-result-object v3

    .line 1331283
    invoke-virtual {v2}, LX/8Ko;->b()I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    goto :goto_2

    :cond_5
    move v0, v1

    .line 1331284
    goto :goto_3

    .line 1331285
    :pswitch_1
    invoke-static {p0}, LX/8L4;->a(LX/8L4;)Landroid/app/PendingIntent;

    move-result-object v3

    goto/16 :goto_1

    .line 1331286
    :pswitch_2
    invoke-static {p0}, LX/8L4;->c(LX/8L4;)Landroid/app/PendingIntent;

    move-result-object v3

    goto/16 :goto_1

    .line 1331287
    :pswitch_3
    invoke-static {p0, p1, v5}, LX/8L4;->b(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v3

    goto/16 :goto_1

    .line 1331288
    :cond_6
    invoke-static {p0, p1, v5}, LX/8L4;->b(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v3

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 1331289
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v0, v0

    .line 1331290
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1331291
    invoke-static {p0, p1}, LX/8L4;->i(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1331292
    invoke-static {p1}, LX/8L4;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p0, p1}, LX/8L4;->n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/8L4;->y:Z

    if-nez v0, :cond_2

    .line 1331293
    invoke-static {p0}, LX/8L4;->b(LX/8L4;)Landroid/content/Intent;

    move-result-object v0

    .line 1331294
    :goto_1
    if-eqz v0, :cond_0

    .line 1331295
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1331296
    iget-object v1, p0, LX/8L4;->i:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/8L4;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1331297
    :cond_0
    return-void

    .line 1331298
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1331299
    :cond_2
    const/4 v4, 0x0

    .line 1331300
    iget-boolean v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v3, v3

    .line 1331301
    if-nez v3, :cond_4

    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 1331302
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1331303
    iget-object v3, p0, LX/8L4;->h:LX/8Kn;

    invoke-virtual {v3, p1, p2}, LX/8Kn;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 1331304
    :cond_3
    :goto_3
    move-object v0, v3

    .line 1331305
    goto :goto_1

    .line 1331306
    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    .line 1331307
    :cond_5
    iget-boolean v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->n:Z

    move v3, v3

    .line 1331308
    if-eqz v3, :cond_6

    move-object v3, v4

    .line 1331309
    goto :goto_3

    .line 1331310
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ad()Z

    move-result v3

    if-eqz v3, :cond_7

    move-object v3, v4

    .line 1331311
    goto :goto_3

    .line 1331312
    :cond_7
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v3, v3

    .line 1331313
    sget-object v5, LX/8LS;->PROFILE_PIC:LX/8LS;

    if-eq v3, v5, :cond_8

    .line 1331314
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v3, v3

    .line 1331315
    sget-object v5, LX/8LS;->COVER_PHOTO:LX/8LS;

    if-ne v3, v5, :cond_b

    .line 1331316
    :cond_8
    iget-wide v7, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v3, v7

    .line 1331317
    iget-object v5, p0, LX/8L4;->l:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_a

    .line 1331318
    iget-object v3, p0, LX/8L4;->e:LX/8I0;

    .line 1331319
    iget-object v4, v3, LX/8I0;->c:LX/17Y;

    iget-object v5, v3, LX/8I0;->b:Landroid/content/Context;

    sget-object v6, LX/0ax;->dg:Ljava/lang/String;

    invoke-interface {v4, v5, v6}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    move-object v3, v4

    .line 1331320
    :goto_4
    if-nez v3, :cond_3

    .line 1331321
    const/4 v5, 0x0

    .line 1331322
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1331323
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v3, v3

    .line 1331324
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1331325
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    move-object v3, v3

    .line 1331326
    sget-object v4, LX/8LR;->PHOTO_REVIEW:LX/8LR;

    if-ne v3, v4, :cond_12

    .line 1331327
    new-instance v3, LX/8NM;

    sget-object v4, LX/8NL;->PHOTO_REVIEW:LX/8NL;

    invoke-direct {v3, v4, p2, v5}, LX/8NM;-><init>(LX/8NL;Ljava/lang/String;Ljava/lang/String;)V

    .line 1331328
    :goto_5
    move-object v3, v3

    .line 1331329
    iget-object v4, v3, LX/8NM;->c:LX/8NL;

    move-object v4, v4

    .line 1331330
    sget-object v5, LX/8NL;->TARGET_POST:LX/8NL;

    if-ne v4, v5, :cond_e

    .line 1331331
    iget-object v4, p0, LX/8L4;->e:LX/8I0;

    .line 1331332
    iget-object v5, v3, LX/8NM;->a:Ljava/lang/String;

    move-object v3, v5

    .line 1331333
    sget-object v5, LX/0ax;->bV:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1331334
    iget-object v6, v4, LX/8I0;->c:LX/17Y;

    iget-object v7, v4, LX/8I0;->b:Landroid/content/Context;

    invoke-interface {v6, v7, v5}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 1331335
    if-eqz v5, :cond_9

    .line 1331336
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "com.facebook.photos.photogallery."

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1331337
    :cond_9
    move-object v3, v5

    .line 1331338
    goto/16 :goto_3

    .line 1331339
    :cond_a
    iget-object v3, p0, LX/8L4;->e:LX/8I0;

    .line 1331340
    iget-wide v7, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v5, v7

    .line 1331341
    invoke-virtual {v3, v5, v6}, LX/8I0;->a(J)Landroid/content/Intent;

    move-result-object v3

    goto :goto_4

    .line 1331342
    :cond_b
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v3, v3

    .line 1331343
    sget-object v5, LX/8LS;->PLACE_PHOTO:LX/8LS;

    if-ne v3, v5, :cond_c

    .line 1331344
    iget-object v3, p0, LX/8L4;->e:LX/8I0;

    .line 1331345
    iget-wide v7, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->j:J

    move-wide v5, v7

    .line 1331346
    invoke-virtual {v3, v5, v6}, LX/8I0;->a(J)Landroid/content/Intent;

    move-result-object v3

    goto :goto_4

    .line 1331347
    :cond_c
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v3, v3

    .line 1331348
    sget-object v5, LX/8LS;->MENU_PHOTO:LX/8LS;

    if-ne v3, v5, :cond_d

    .line 1331349
    iget-object v3, p0, LX/8L4;->e:LX/8I0;

    .line 1331350
    iget-wide v7, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->j:J

    move-wide v5, v7

    .line 1331351
    sget-object v4, LX/0ax;->eE:Ljava/lang/String;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v4, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1331352
    iget-object v7, v3, LX/8I0;->c:LX/17Y;

    iget-object v8, v3, LX/8I0;->b:Landroid/content/Context;

    invoke-interface {v7, v8, v4}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    move-object v3, v4

    .line 1331353
    goto/16 :goto_4

    .line 1331354
    :cond_d
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v3, v3

    .line 1331355
    sget-object v5, LX/8LS;->PRODUCT_IMAGE:LX/8LS;

    if-ne v3, v5, :cond_11

    move-object v3, v4

    .line 1331356
    goto/16 :goto_3

    .line 1331357
    :cond_e
    iget-object v4, v3, LX/8NM;->c:LX/8NL;

    move-object v4, v4

    .line 1331358
    sget-object v5, LX/8NL;->PHOTO_REVIEW:LX/8NL;

    if-ne v4, v5, :cond_f

    .line 1331359
    iget-object v3, p0, LX/8L4;->e:LX/8I0;

    .line 1331360
    iget-object v4, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->w:Lcom/facebook/composer/protocol/PostReviewParams;

    move-object v4, v4

    .line 1331361
    iget-wide v5, v4, Lcom/facebook/composer/protocol/PostReviewParams;->b:J

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    .line 1331362
    iget-object v5, v3, LX/8I0;->c:LX/17Y;

    iget-object v6, v3, LX/8I0;->b:Landroid/content/Context;

    sget-object v7, LX/0ax;->aI:Ljava/lang/String;

    invoke-static {v7, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 1331363
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "com.facebook.photos.photogallery."

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1331364
    move-object v3, v5

    .line 1331365
    goto/16 :goto_3

    .line 1331366
    :cond_f
    iget-object v4, v3, LX/8NM;->c:LX/8NL;

    move-object v4, v4

    .line 1331367
    sget-object v5, LX/8NL;->PHOTO_STORY:LX/8NL;

    if-ne v4, v5, :cond_10

    .line 1331368
    iget-object v5, v3, LX/8NM;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1331369
    iget-object v6, v3, LX/8NM;->a:Ljava/lang/String;

    move-object v3, v6

    .line 1331370
    sget-object v6, LX/0ax;->bA:Ljava/lang/String;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    const/4 v8, 0x1

    aput-object v5, v7, v8

    invoke-static {v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1331371
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 1331372
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1331373
    move-object v3, v7

    .line 1331374
    goto/16 :goto_3

    .line 1331375
    :cond_10
    iget-object v4, p0, LX/8L4;->e:LX/8I0;

    .line 1331376
    iget-object v5, v3, LX/8NM;->a:Ljava/lang/String;

    move-object v3, v5

    .line 1331377
    sget-object v5, LX/0ax;->bV:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1331378
    iget-object v6, v4, LX/8I0;->c:LX/17Y;

    iget-object v7, v4, LX/8I0;->b:Landroid/content/Context;

    invoke-interface {v6, v7, v5}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 1331379
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "com.facebook.photos.photogallery."

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1331380
    move-object v3, v5

    .line 1331381
    goto/16 :goto_3

    :cond_11
    move-object v3, v4

    goto/16 :goto_4

    .line 1331382
    :cond_12
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    move-object v3, v3

    .line 1331383
    sget-object v4, LX/8LR;->SINGLE_PHOTO:LX/8LR;

    if-eq v3, v4, :cond_13

    .line 1331384
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    move-object v3, v3

    .line 1331385
    sget-object v4, LX/8LR;->STATUS:LX/8LR;

    if-eq v3, v4, :cond_13

    .line 1331386
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    move-object v3, v3

    .line 1331387
    sget-object v4, LX/8LR;->VIDEO_STATUS:LX/8LR;

    if-ne v3, v4, :cond_15

    .line 1331388
    :cond_13
    const/16 v3, 0x5f

    invoke-virtual {p2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 1331389
    if-lez v3, :cond_14

    .line 1331390
    const/4 v4, 0x0

    invoke-virtual {p2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1331391
    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1331392
    new-instance v3, LX/8NM;

    sget-object v6, LX/8NL;->PHOTO_STORY:LX/8NL;

    invoke-direct {v3, v6, v5, v4}, LX/8NM;-><init>(LX/8NL;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1331393
    :cond_14
    new-instance v3, LX/8NM;

    sget-object v4, LX/8NL;->SINGLE_PHOTO:LX/8NL;

    invoke-direct {v3, v4, p2, v5}, LX/8NM;-><init>(LX/8NL;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1331394
    :cond_15
    new-instance v3, LX/8NM;

    sget-object v4, LX/8NL;->TARGET_POST:LX/8NL;

    invoke-direct {v3, v4, p2, v5}, LX/8NM;-><init>(LX/8NL;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3
    .param p4    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1331395
    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1331396
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v0, v0

    .line 1331397
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1331398
    iget-object v0, p0, LX/8L4;->d:LX/0b3;

    new-instance v1, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1331399
    :goto_0
    return-void

    .line 1331400
    :catch_0
    move-exception v0

    .line 1331401
    iget-object v1, p0, LX/8L4;->j:LX/03V;

    const-string v2, "Media Server Processing Success throwable"

    invoke-virtual {v1, v2, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1331402
    invoke-static {p0, p1}, LX/8L4;->i(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1331403
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v0, v0

    .line 1331404
    if-eqz v0, :cond_1

    .line 1331405
    :cond_0
    :goto_0
    return-void

    .line 1331406
    :cond_1
    iget-object v0, p0, LX/8L4;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    iput-boolean v0, p0, LX/8L4;->z:Z

    .line 1331407
    iget-object v0, p0, LX/8L4;->f:LX/8Ko;

    .line 1331408
    invoke-static {p0, p1}, LX/8L4;->n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v2

    invoke-virtual {v0, v2}, LX/8Ko;->a(Z)V

    .line 1331409
    new-instance v2, LX/2HB;

    iget-object v3, p0, LX/8L4;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, LX/8Ko;->a()I

    move-result v3

    invoke-virtual {v2, v3}, LX/2HB;->a(I)LX/2HB;

    move-result-object v2

    iget-object v3, p0, LX/8L4;->b:Landroid/content/Context;

    invoke-virtual {v0, v3, p1}, LX/8Ko;->a(Landroid/content/Context;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    iget-object v3, p0, LX/8L4;->b:Landroid/content/Context;

    invoke-virtual {v0, v3}, LX/8Ko;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1}, LX/8L4;->a$redex0(LX/8L4;Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    invoke-static {p0, p1}, LX/8L4;->g(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 1331410
    iput-object v2, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1331411
    move-object v2, v0

    .line 1331412
    iget-boolean v0, p0, LX/8L4;->z:Z

    if-eqz v0, :cond_2

    invoke-static {p0, p1}, LX/8L4;->n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v0

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    .line 1331413
    invoke-static {p0, p1, v0}, LX/8L4;->a$redex0(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/app/Notification;)V

    .line 1331414
    :try_start_0
    iget-object v0, p0, LX/8L4;->d:LX/0b3;

    new-instance v1, Lcom/facebook/photos/upload/event/MediaUploadEnqueuedEvent;

    invoke-direct {v1, p1}, Lcom/facebook/photos/upload/event/MediaUploadEnqueuedEvent;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1331415
    :goto_2
    iget-boolean v0, p0, LX/8L4;->a:Z

    if-eqz v0, :cond_0

    .line 1331416
    goto :goto_0

    .line 1331417
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1331418
    :catch_0
    move-exception v0

    .line 1331419
    iget-object v1, p0, LX/8L4;->j:LX/03V;

    const-string v2, "Upload enqueued broadcast throwable"

    invoke-virtual {v1, v2, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public final c(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 5

    .prologue
    .line 1331420
    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1331421
    invoke-static {p0, p1}, LX/8L4;->i(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1331422
    iget-boolean v0, p0, LX/8L4;->a:Z

    if-eqz v0, :cond_0

    .line 1331423
    :cond_0
    iget-object v0, p0, LX/8L4;->d:LX/0b3;

    new-instance v1, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v1, p1, v2, v3, v4}, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/content/Intent;ZZ)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1331424
    :goto_0
    return-void

    .line 1331425
    :catch_0
    move-exception v0

    .line 1331426
    iget-object v1, p0, LX/8L4;->j:LX/03V;

    const-string v2, "Upload cancel throwable"

    invoke-virtual {v1, v2, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
