.class public LX/8Qx;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Landroid/widget/ListAdapter;


# instance fields
.field private a:Landroid/content/Context;

.field private b:LX/8Sa;

.field public c:Lcom/facebook/privacy/model/AudiencePickerModel;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field public e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8Sa;Lcom/facebook/privacy/model/AudiencePickerModel;)V
    .locals 1

    .prologue
    .line 1343850
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1343851
    const/4 v0, -0x1

    iput v0, p0, LX/8Qx;->e:I

    .line 1343852
    iput-object p1, p0, LX/8Qx;->a:Landroid/content/Context;

    .line 1343853
    iput-object p2, p0, LX/8Qx;->b:LX/8Sa;

    .line 1343854
    iput-object p3, p0, LX/8Qx;->c:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343855
    iget-object v0, p0, LX/8Qx;->c:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343856
    iget-object p1, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->b:LX/0Px;

    move-object v0, p1

    .line 1343857
    iput-object v0, p0, LX/8Qx;->d:Ljava/util/List;

    .line 1343858
    iget-object v0, p0, LX/8Qx;->d:Ljava/util/List;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1343859
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 1343860
    iget-object v0, p0, LX/8Qx;->c:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343861
    iget-object p0, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->b:LX/0Px;

    move-object v0, p0

    .line 1343862
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1343863
    iget-object v0, p0, LX/8Qx;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1343864
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1343865
    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    .line 1343866
    if-nez p2, :cond_0

    .line 1343867
    new-instance v0, LX/8SS;

    iget-object v1, p0, LX/8Qx;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/8SS;-><init>(Landroid/content/Context;)V

    .line 1343868
    :goto_0
    invoke-virtual {p0, p1}, LX/8Qx;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1343869
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->y_()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/8Qx;->b:LX/8Sa;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    sget-object v6, LX/8SZ;->GLYPH:LX/8SZ;

    invoke-virtual {v3, v5, v6}, LX/8Sa;->a(Lcom/facebook/graphql/model/GraphQLImage;LX/8SZ;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 1343870
    iget v5, p0, LX/8Qx;->e:I

    if-ltz v5, :cond_3

    .line 1343871
    iget v5, p0, LX/8Qx;->e:I

    if-ne p1, v5, :cond_2

    const/4 v5, 0x1

    .line 1343872
    :goto_1
    move v4, v5

    .line 1343873
    if-eqz v4, :cond_1

    sget-object v4, LX/03R;->YES:LX/03R;

    :goto_2
    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/8SS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;LX/03R;ZZ)V

    .line 1343874
    return-object v0

    .line 1343875
    :cond_0
    check-cast p2, LX/8SS;

    move-object v0, p2

    goto :goto_0

    .line 1343876
    :cond_1
    sget-object v4, LX/03R;->NO:LX/03R;

    goto :goto_2

    .line 1343877
    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    .line 1343878
    :cond_3
    iget-object v5, p0, LX/8Qx;->c:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343879
    iget-object v6, v5, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    move-object v5, v6

    .line 1343880
    iget-object v6, p0, LX/8Qx;->c:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1343881
    iget p0, v6, Lcom/facebook/privacy/model/AudiencePickerModel;->e:I

    move v6, p0

    .line 1343882
    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1343883
    invoke-static {v4, v5}, LX/2cA;->a(LX/1oU;LX/1oU;)Z

    move-result v5

    goto :goto_1
.end method
