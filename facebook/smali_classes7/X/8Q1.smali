.class public LX/8Q1;
.super LX/4or;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3lZ;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public c:Lcom/facebook/privacy/PrivacyOperationsClient;

.field public d:LX/339;

.field public e:Ljava/util/concurrent/ExecutorService;

.field public f:LX/17W;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 1342383
    invoke-direct {p0, p1}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 1342384
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v1, p0

    check-cast v1, LX/8Q1;

    const/16 v2, 0xfb6

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v4

    check-cast v4, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {v0}, LX/339;->a(LX/0QB;)LX/339;

    move-result-object v5

    check-cast v5, LX/339;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v0

    check-cast v0, LX/17W;

    iput-object v2, v1, LX/8Q1;->a:LX/0Ot;

    iput-object v3, v1, LX/8Q1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v4, v1, LX/8Q1;->c:Lcom/facebook/privacy/PrivacyOperationsClient;

    iput-object v5, v1, LX/8Q1;->d:LX/339;

    iput-object v6, v1, LX/8Q1;->e:Ljava/util/concurrent/ExecutorService;

    iput-object v0, v1, LX/8Q1;->f:LX/17W;

    .line 1342385
    const-string v0, "Privacy Education Settings"

    invoke-virtual {p0, v0}, LX/8Q1;->setTitle(Ljava/lang/CharSequence;)V

    .line 1342386
    const-string v0, "List of device-specific privacy education settings."

    invoke-virtual {p0, v0}, LX/8Q1;->setSummary(Ljava/lang/CharSequence;)V

    .line 1342387
    const-string v0, "Ignore the radio buttons."

    invoke-virtual {p0, v0}, LX/8Q1;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 1342388
    invoke-static {}, LX/8Q0;->values()[LX/8Q0;

    move-result-object v0

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/String;

    .line 1342389
    const/4 v0, 0x0

    :goto_0
    invoke-static {}, LX/8Q0;->values()[LX/8Q0;

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1342390
    invoke-static {}, LX/8Q0;->values()[LX/8Q0;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v2}, LX/8Q0;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1342391
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1342392
    :cond_0
    invoke-virtual {p0, v1}, LX/8Q1;->setEntries([Ljava/lang/CharSequence;)V

    .line 1342393
    invoke-virtual {p0, v1}, LX/8Q1;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 1342394
    const-string v0, "privacy_education_preference"

    invoke-virtual {p0, v0}, LX/8Q1;->setKey(Ljava/lang/String;)V

    .line 1342395
    new-instance v0, LX/8Py;

    invoke-direct {v0, p0}, LX/8Py;-><init>(LX/8Q1;)V

    invoke-virtual {p0, v0}, LX/8Q1;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1342396
    return-void
.end method
