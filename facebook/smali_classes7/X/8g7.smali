.class public LX/8g7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1386562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;)Ljava/lang/CharSequence;
    .locals 4
    .param p0    # Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTextFromSnippet"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1386563
    if-nez p0, :cond_1

    .line 1386564
    :cond_0
    :goto_0
    return-object v0

    .line 1386565
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1386566
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1386567
    if-eqz v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v2, v1, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1386568
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
