.class public final LX/8Ba;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public mChunkIndex:I

.field public mCurrentEndOffset:J

.field public mCurrentStartOffset:J

.field public mVideoPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;JJ)V
    .locals 2

    .prologue
    .line 1309466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309467
    const/4 v0, 0x0

    iput v0, p0, LX/8Ba;->mChunkIndex:I

    .line 1309468
    iput-object p1, p0, LX/8Ba;->mVideoPath:Ljava/lang/String;

    .line 1309469
    iput-wide p2, p0, LX/8Ba;->mCurrentStartOffset:J

    .line 1309470
    iput-wide p4, p0, LX/8Ba;->mCurrentEndOffset:J

    .line 1309471
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 1309472
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/8Ba;->mVideoPath:Ljava/lang/String;

    .line 1309473
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, LX/8Ba;->mCurrentStartOffset:J

    .line 1309474
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, LX/8Ba;->mCurrentEndOffset:J

    .line 1309475
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, LX/8Ba;->mChunkIndex:I

    .line 1309476
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    .prologue
    .line 1309477
    iget-object v0, p0, LX/8Ba;->mVideoPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 1309478
    iget-wide v0, p0, LX/8Ba;->mCurrentStartOffset:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 1309479
    iget-wide v0, p0, LX/8Ba;->mCurrentEndOffset:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 1309480
    iget v0, p0, LX/8Ba;->mChunkIndex:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 1309481
    return-void
.end method
