.class public LX/6sZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private final a:LX/18V;

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6sU",
            "<**>;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/18V;Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "Ljava/lang/Iterable",
            "<",
            "LX/6sU",
            "<**>;>;)V"
        }
    .end annotation

    .prologue
    .line 1153554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153555
    iput-object p1, p0, LX/6sZ;->a:LX/18V;

    .line 1153556
    invoke-static {p2}, LX/0Px;->copyOf(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/6sZ;->b:LX/0Px;

    .line 1153557
    return-void
.end method

.method public varargs constructor <init>(LX/18V;[LX/6sU;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "[",
            "LX/6sU",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 1153540
    invoke-static {p2}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/6sZ;-><init>(LX/18V;Ljava/lang/Iterable;)V

    .line 1153541
    return-void
.end method


# virtual methods
.method public handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 1153542
    iget-object v0, p0, LX/6sZ;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/6sZ;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6sU;

    .line 1153543
    iget-object v3, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v3, v3

    .line 1153544
    invoke-virtual {v0}, LX/6sU;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    move v3, v4

    .line 1153545
    if-eqz v3, :cond_0

    .line 1153546
    iget-object v1, p0, LX/6sZ;->a:LX/18V;

    .line 1153547
    iget-object v2, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 1153548
    invoke-static {v0}, LX/6sU;->a(LX/6sU;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .line 1153549
    invoke-virtual {v1, v0, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1153550
    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    move-object v0, v2

    .line 1153551
    :goto_1
    return-object v0

    .line 1153552
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1153553
    :cond_1
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1
.end method
