.class public final LX/7Ew;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1185574
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_a

    .line 1185575
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1185576
    :goto_0
    return v1

    .line 1185577
    :cond_0
    const-string v12, "creation_time"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1185578
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    .line 1185579
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_8

    .line 1185580
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1185581
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1185582
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1185583
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1185584
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 1185585
    :cond_3
    const-string v12, "actors"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1185586
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1185587
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, v12, :cond_4

    .line 1185588
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, v12, :cond_4

    .line 1185589
    invoke-static {p0, p1}, LX/7Es;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1185590
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1185591
    :cond_4
    invoke-static {v9, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v9

    move v9, v9

    .line 1185592
    goto :goto_1

    .line 1185593
    :cond_5
    const-string v12, "app_icon"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1185594
    invoke-static {p0, p1}, LX/7Et;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1185595
    :cond_6
    const-string v12, "titleForSummary"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1185596
    invoke-static {p0, p1}, LX/7Ev;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1185597
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1185598
    :cond_8
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1185599
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 1185600
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 1185601
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1185602
    if-eqz v0, :cond_9

    .line 1185603
    const/4 v1, 0x3

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1185604
    :cond_9
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1185605
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v7, v1

    move-wide v2, v4

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1185606
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1185607
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1185608
    if-eqz v0, :cond_0

    .line 1185609
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185610
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1185611
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1185612
    if-eqz v0, :cond_2

    .line 1185613
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185614
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1185615
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 1185616
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/7Es;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1185617
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1185618
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1185619
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1185620
    if-eqz v0, :cond_4

    .line 1185621
    const-string v1, "app_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185622
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1185623
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1185624
    if-eqz v1, :cond_3

    .line 1185625
    const-string v4, "uri"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185626
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1185627
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1185628
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1185629
    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 1185630
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185631
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1185632
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1185633
    if-eqz v0, :cond_c

    .line 1185634
    const-string v1, "titleForSummary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185635
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1185636
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1185637
    if-eqz v1, :cond_6

    .line 1185638
    const-string v2, "aggregated_ranges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185639
    invoke-static {p0, v1, p2, p3}, LX/7Eu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1185640
    :cond_6
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1185641
    if-eqz v1, :cond_a

    .line 1185642
    const-string v2, "ranges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185643
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1185644
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_9

    .line 1185645
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    const/4 p3, 0x0

    .line 1185646
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1185647
    invoke-virtual {p0, v3, p3, p3}, LX/15i;->a(III)I

    move-result v4

    .line 1185648
    if-eqz v4, :cond_7

    .line 1185649
    const-string p1, "length"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185650
    invoke-virtual {p2, v4}, LX/0nX;->b(I)V

    .line 1185651
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4, p3}, LX/15i;->a(III)I

    move-result v4

    .line 1185652
    if-eqz v4, :cond_8

    .line 1185653
    const-string p1, "offset"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185654
    invoke-virtual {p2, v4}, LX/0nX;->b(I)V

    .line 1185655
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1185656
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1185657
    :cond_9
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1185658
    :cond_a
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1185659
    if-eqz v1, :cond_b

    .line 1185660
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185661
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1185662
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1185663
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1185664
    return-void
.end method
