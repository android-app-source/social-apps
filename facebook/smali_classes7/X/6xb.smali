.class public LX/6xb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/6xb;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0Zb;

.field private c:LX/6xa;


# direct methods
.method public constructor <init>(LX/0SG;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1159045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159046
    iput-object p1, p0, LX/6xb;->a:LX/0SG;

    .line 1159047
    iput-object p2, p0, LX/6xb;->b:LX/0Zb;

    .line 1159048
    return-void
.end method

.method public static a(LX/0QB;)LX/6xb;
    .locals 5

    .prologue
    .line 1159032
    sget-object v0, LX/6xb;->d:LX/6xb;

    if-nez v0, :cond_1

    .line 1159033
    const-class v1, LX/6xb;

    monitor-enter v1

    .line 1159034
    :try_start_0
    sget-object v0, LX/6xb;->d:LX/6xb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1159035
    if-eqz v2, :cond_0

    .line 1159036
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1159037
    new-instance p0, LX/6xb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, LX/6xb;-><init>(LX/0SG;LX/0Zb;)V

    .line 1159038
    move-object v0, p0

    .line 1159039
    sput-object v0, LX/6xb;->d:LX/6xb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1159040
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1159041
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1159042
    :cond_1
    sget-object v0, LX/6xb;->d:LX/6xb;

    return-object v0

    .line 1159043
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1159044
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/6xZ;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6
    .param p3    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1158962
    iget-object v0, p0, LX/6xb;->b:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p2, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1158963
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1158964
    const-string v1, "payflows"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1158965
    iget-object v1, p0, LX/6xb;->c:LX/6xa;

    .line 1158966
    const-string v2, "session_id"

    iget-object v3, v1, LX/6xa;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v3, v3, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1158967
    const-string v2, "flow_name"

    iget-object v3, v1, LX/6xa;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v3, v3, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a:LX/6xY;

    invoke-virtual {v3}, LX/6xY;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1158968
    const-string v2, "context_id"

    .line 1158969
    iget-object v3, v1, LX/6xa;->b:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1158970
    iget-object v3, v1, LX/6xa;->b:Ljava/util/Map;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1158971
    :cond_0
    iget-object v3, v1, LX/6xa;->b:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v3, v3

    .line 1158972
    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1158973
    const-string v2, "flow_step"

    invoke-virtual {p1}, LX/6xZ;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1158974
    iget-object v2, v1, LX/6xa;->d:Ljava/util/Map;

    invoke-virtual {v0, v2}, LX/0oG;->a(Ljava/util/Map;)LX/0oG;

    .line 1158975
    iget-object v2, v1, LX/6xa;->c:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    invoke-virtual {v0, v2}, LX/0oG;->a(Ljava/util/Map;)LX/0oG;

    .line 1158976
    const-string v1, "event_type"

    const-string v2, "client"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1158977
    const-string v1, "event_name"

    .line 1158978
    const-string v2, "payflows_"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1158979
    const/16 v2, 0x9

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 1158980
    :cond_1
    move-object v2, p2

    .line 1158981
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1158982
    const-string v1, "client_time"

    iget-object v2, p0, LX/6xb;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1158983
    if-nez p3, :cond_3

    .line 1158984
    :goto_0
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1158985
    :cond_2
    return-void

    .line 1158986
    :cond_3
    const-string v1, "error_stacktrace"

    invoke-static {p3}, LX/23D;->b(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1158987
    const-class v1, LX/2Oo;

    invoke-static {p3, v1}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v1

    check-cast v1, LX/2Oo;

    .line 1158988
    if-eqz v1, :cond_4

    .line 1158989
    const-string v2, "error_code"

    invoke-virtual {v1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1158990
    const-string v2, "error_message"

    invoke-virtual {v1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    goto :goto_0

    .line 1158991
    :cond_4
    const-class v1, Lcom/facebook/fbservice/service/ServiceException;

    invoke-static {p3, v1}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbservice/service/ServiceException;

    .line 1158992
    if-eqz v1, :cond_5

    .line 1158993
    const-string v2, "error_code"

    .line 1158994
    iget-object v3, v1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v1, v3

    .line 1158995
    invoke-virtual {v1}, LX/1nY;->getAsInt()I

    move-result v1

    invoke-virtual {v0, v2, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1158996
    :cond_5
    const-string v1, "error_message"

    invoke-static {p3}, LX/1Bz;->getRootCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    goto :goto_0
.end method

.method private a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)V
    .locals 3

    .prologue
    .line 1159027
    iget-object v0, p0, LX/6xb;->c:LX/6xa;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6xb;->c:LX/6xa;

    .line 1159028
    iget-object v1, v0, LX/6xa;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v1, v1, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1159029
    if-eqz v0, :cond_1

    .line 1159030
    :cond_0
    new-instance v0, LX/6xa;

    invoke-direct {v0, p1}, LX/6xa;-><init>(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)V

    iput-object v0, p0, LX/6xb;->c:LX/6xa;

    .line 1159031
    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1159022
    const-string v0, "payflows_init"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Use logInitEvent(...) for INIT event"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1159023
    invoke-direct {p0, p1}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)V

    .line 1159024
    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0}, LX/6xb;->a(LX/6xZ;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1159025
    return-void

    .line 1159026
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1159019
    invoke-direct {p0, p1}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)V

    .line 1159020
    const-string v0, "payflows_fail"

    invoke-direct {p0, p2, v0, p3}, LX/6xb;->a(LX/6xZ;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1159021
    return-void
.end method

.method public final a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xg;LX/6xZ;Landroid/os/Bundle;)V
    .locals 3
    .param p4    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1159014
    if-eqz p4, :cond_0

    .line 1159015
    :goto_0
    return-void

    .line 1159016
    :cond_0
    invoke-direct {p0, p1}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)V

    .line 1159017
    iget-object v0, p0, LX/6xb;->c:LX/6xa;

    const-string v1, "product"

    invoke-virtual {p2}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/6xa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159018
    const-string v0, "payflows_init"

    const/4 v1, 0x0

    invoke-direct {p0, p3, v0, v1}, LX/6xb;->a(LX/6xZ;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1159012
    const-string v0, "payment_method_id"

    invoke-virtual {p0, p1, v0, p2}, LX/6xb;->c(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;Ljava/lang/String;)V

    .line 1159013
    return-void
.end method

.method public final a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1159006
    const-string v0, "shipping_option"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1159007
    invoke-virtual {p0, p1, p3}, LX/6xb;->d(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;)V

    .line 1159008
    :cond_0
    :goto_0
    invoke-virtual {p0, p1, p2, p3}, LX/6xb;->c(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;Ljava/lang/String;)V

    .line 1159009
    return-void

    .line 1159010
    :cond_1
    const-string v0, "mailing_address"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1159011
    invoke-virtual {p0, p1, p3}, LX/6xb;->c(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1159004
    const-string v0, "payment_method_type"

    invoke-virtual {p0, p1, v0, p2}, LX/6xb;->c(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;Ljava/lang/String;)V

    .line 1159005
    return-void
.end method

.method public final c(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1159002
    const-string v0, "mailing_address_id"

    invoke-virtual {p0, p1, v0, p2}, LX/6xb;->c(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;Ljava/lang/String;)V

    .line 1159003
    return-void
.end method

.method public final c(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1158999
    invoke-direct {p0, p1}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)V

    .line 1159000
    iget-object v0, p0, LX/6xb;->c:LX/6xa;

    invoke-virtual {v0, p2, p3}, LX/6xa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159001
    return-void
.end method

.method public final d(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1158997
    const-string v0, "shipping_option_id"

    invoke-virtual {p0, p1, v0, p2}, LX/6xb;->c(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;Ljava/lang/String;)V

    .line 1158998
    return-void
.end method
