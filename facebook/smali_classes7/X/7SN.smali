.class public LX/7SN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/61B;


# instance fields
.field private a:LX/5Pb;

.field private final b:LX/5PR;

.field private final c:LX/5Pg;

.field private d:F

.field private e:F

.field private f:F

.field private g:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1208753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1208754
    iput v0, p0, LX/7SN;->d:F

    .line 1208755
    iput v0, p0, LX/7SN;->e:F

    .line 1208756
    iput v0, p0, LX/7SN;->f:F

    .line 1208757
    const/high16 v0, 0x437f0000    # 255.0f

    iput v0, p0, LX/7SN;->g:F

    .line 1208758
    new-instance v0, LX/5Pg;

    const/16 v1, 0x8

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/5Pg;-><init>([FI)V

    iput-object v0, p0, LX/7SN;->c:LX/5Pg;

    .line 1208759
    new-instance v0, LX/5PQ;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/5PQ;-><init>(I)V

    const/4 v1, 0x5

    .line 1208760
    iput v1, v0, LX/5PQ;->a:I

    .line 1208761
    move-object v0, v0

    .line 1208762
    const-string v1, "aPosition"

    iget-object v2, p0, LX/7SN;->c:LX/5Pg;

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    invoke-virtual {v0}, LX/5PQ;->a()LX/5PR;

    move-result-object v0

    iput-object v0, p0, LX/7SN;->b:LX/5PR;

    .line 1208763
    return-void

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 1208764
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 2

    .prologue
    .line 1208765
    const v0, 0x7f070081

    const v1, 0x7f070080

    invoke-interface {p1, v0, v1}, LX/5Pc;->a(II)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, LX/7SN;->a:LX/5Pb;

    .line 1208766
    return-void
.end method

.method public final a([F[F[FJ)V
    .locals 6

    .prologue
    .line 1208767
    iget-object v0, p0, LX/7SN;->a:LX/5Pb;

    invoke-virtual {v0}, LX/5Pb;->a()LX/5Pa;

    move-result-object v0

    const-string v1, "vColor"

    iget v2, p0, LX/7SN;->d:F

    iget v3, p0, LX/7SN;->e:F

    iget v4, p0, LX/7SN;->f:F

    iget v5, p0, LX/7SN;->g:F

    invoke-virtual/range {v0 .. v5}, LX/5Pa;->a(Ljava/lang/String;FFFF)LX/5Pa;

    move-result-object v0

    iget-object v1, p0, LX/7SN;->b:LX/5PR;

    invoke-virtual {v0, v1}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    .line 1208768
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1208769
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1208770
    const/4 v0, 0x1

    return v0
.end method
