.class public final enum LX/8WQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8WQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8WQ;

.field public static final enum DIRECT_CHALLENGE:LX/8WQ;

.field public static final enum DISABLED:LX/8WQ;

.field public static final enum SHOW_CHALLENGE_POPOVER:LX/8WQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1354034
    new-instance v0, LX/8WQ;

    const-string v1, "SHOW_CHALLENGE_POPOVER"

    invoke-direct {v0, v1, v2}, LX/8WQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8WQ;->SHOW_CHALLENGE_POPOVER:LX/8WQ;

    .line 1354035
    new-instance v0, LX/8WQ;

    const-string v1, "DIRECT_CHALLENGE"

    invoke-direct {v0, v1, v3}, LX/8WQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8WQ;->DIRECT_CHALLENGE:LX/8WQ;

    .line 1354036
    new-instance v0, LX/8WQ;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v4}, LX/8WQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8WQ;->DISABLED:LX/8WQ;

    .line 1354037
    const/4 v0, 0x3

    new-array v0, v0, [LX/8WQ;

    sget-object v1, LX/8WQ;->SHOW_CHALLENGE_POPOVER:LX/8WQ;

    aput-object v1, v0, v2

    sget-object v1, LX/8WQ;->DIRECT_CHALLENGE:LX/8WQ;

    aput-object v1, v0, v3

    sget-object v1, LX/8WQ;->DISABLED:LX/8WQ;

    aput-object v1, v0, v4

    sput-object v0, LX/8WQ;->$VALUES:[LX/8WQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1354038
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8WQ;
    .locals 1

    .prologue
    .line 1354039
    const-class v0, LX/8WQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8WQ;

    return-object v0
.end method

.method public static values()[LX/8WQ;
    .locals 1

    .prologue
    .line 1354040
    sget-object v0, LX/8WQ;->$VALUES:[LX/8WQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8WQ;

    return-object v0
.end method
