.class public LX/8OY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1339627
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1339628
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8OY;->c:Z

    .line 1339629
    sget-object v0, LX/2Mg;->h:LX/2Mg;

    iget v0, v0, LX/2Mg;->b:I

    iput v0, p0, LX/8OY;->a:I

    .line 1339630
    sget-object v0, LX/2Mg;->h:LX/2Mg;

    iget v0, v0, LX/2Mg;->a:I

    iput v0, p0, LX/8OY;->b:I

    return-void
.end method

.method public static a(ZILcom/facebook/photos/upload/operation/TranscodeInfo;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1339631
    const/4 v1, 0x0

    .line 1339632
    if-nez p0, :cond_0

    iget-boolean v2, p2, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isServerSettingsAvailable:Z

    if-eqz v2, :cond_0

    iget-wide v2, p2, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeBitrate:J

    sget v4, Lcom/facebook/photos/upload/operation/TranscodeInfo;->a:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v1, v0

    .line 1339633
    :cond_0
    const/4 v2, -0x2

    if-ne p1, v2, :cond_1

    .line 1339634
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static e(LX/8OY;)Z
    .locals 2

    .prologue
    .line 1339635
    iget v0, p0, LX/8OY;->b:I

    sget-object v1, LX/2Mg;->h:LX/2Mg;

    iget v1, v1, LX/2Mg;->a:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c()Z
    .locals 2

    .prologue
    .line 1339636
    iget v0, p0, LX/8OY;->a:I

    sget-object v1, LX/2Mg;->h:LX/2Mg;

    iget v1, v1, LX/2Mg;->b:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
