.class public final LX/8YF;
.super LX/8YD;
.source ""


# instance fields
.field public final b:Landroid/os/Handler;

.field public final c:Ljava/lang/Runnable;

.field public d:Z

.field public e:J


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 1355974
    invoke-direct {p0}, LX/8YD;-><init>()V

    .line 1355975
    iput-object p1, p0, LX/8YF;->b:Landroid/os/Handler;

    .line 1355976
    new-instance v0, Lcom/facebook/rebound/AndroidSpringLooperFactory$LegacyAndroidSpringLooper$1;

    invoke-direct {v0, p0}, Lcom/facebook/rebound/AndroidSpringLooperFactory$LegacyAndroidSpringLooper$1;-><init>(LX/8YF;)V

    iput-object v0, p0, LX/8YF;->c:Ljava/lang/Runnable;

    .line 1355977
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1355978
    iget-boolean v0, p0, LX/8YF;->d:Z

    if-eqz v0, :cond_0

    .line 1355979
    :goto_0
    return-void

    .line 1355980
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8YF;->d:Z

    .line 1355981
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/8YF;->e:J

    .line 1355982
    iget-object v0, p0, LX/8YF;->b:Landroid/os/Handler;

    iget-object v1, p0, LX/8YF;->c:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1355983
    iget-object v0, p0, LX/8YF;->b:Landroid/os/Handler;

    iget-object v1, p0, LX/8YF;->c:Ljava/lang/Runnable;

    const v2, -0x74cfb395

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1355984
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8YF;->d:Z

    .line 1355985
    iget-object v0, p0, LX/8YF;->b:Landroid/os/Handler;

    iget-object v1, p0, LX/8YF;->c:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1355986
    return-void
.end method
