.class public LX/8Gc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0xi;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final b:LX/0wW;

.field public c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0wd;",
            ">;"
        }
    .end annotation
.end field

.field public d:F

.field public e:F

.field public f:F

.field public g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0wW;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1319828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1319829
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/8Gc;->c:LX/0am;

    .line 1319830
    const/high16 v0, 0x42a00000    # 80.0f

    iput v0, p0, LX/8Gc;->f:F

    .line 1319831
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/8Gc;->g:Ljava/lang/ref/WeakReference;

    .line 1319832
    new-instance v0, LX/8Gb;

    invoke-direct {v0, p0}, LX/8Gb;-><init>(LX/8Gc;)V

    iput-object v0, p0, LX/8Gc;->a:LX/0xi;

    .line 1319833
    iput-object p1, p0, LX/8Gc;->b:LX/0wW;

    .line 1319834
    return-void
.end method

.method public static b(LX/0QB;)LX/8Gc;
    .locals 2

    .prologue
    .line 1319826
    new-instance v1, LX/8Gc;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-direct {v1, v0}, LX/8Gc;-><init>(LX/0wW;)V

    .line 1319827
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1319835
    iget-object v0, p0, LX/8Gc;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1319836
    iget-object v0, p0, LX/8Gc;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wd;

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1319837
    iget-object v1, p0, LX/8Gc;->a:LX/0xi;

    iget-object v0, p0, LX/8Gc;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wd;

    invoke-interface {v1, v0}, LX/0xi;->b(LX/0wd;)V

    .line 1319838
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 6

    .prologue
    .line 1319821
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/8Gc;->g:Ljava/lang/ref/WeakReference;

    .line 1319822
    int-to-float v0, p2

    iput v0, p0, LX/8Gc;->d:F

    .line 1319823
    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    iput v0, p0, LX/8Gc;->e:F

    .line 1319824
    iget-object v0, p0, LX/8Gc;->b:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    iget v1, p0, LX/8Gc;->f:F

    float-to-double v2, v1

    const-wide/high16 v4, 0x402e000000000000L    # 15.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    iget-object v1, p0, LX/8Gc;->a:LX/0xi;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/8Gc;->c:LX/0am;

    .line 1319825
    return-void
.end method
