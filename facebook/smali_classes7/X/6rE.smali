.class public LX/6rE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ev;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ev",
        "<",
        "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6rL;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6qx;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/6qh;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/6rL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6qx;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1151892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1151893
    iput-object p1, p0, LX/6rE;->a:LX/0Ot;

    .line 1151894
    iput-object p2, p0, LX/6rE;->b:LX/0Ot;

    .line 1151895
    return-void
.end method

.method private a(LX/6re;)LX/6Ev;
    .locals 3

    .prologue
    .line 1151896
    sget-object v0, LX/6rD;->a:[I

    invoke-virtual {p1}, LX/6re;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1151897
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1151898
    :pswitch_0
    iget-object v0, p0, LX/6rE;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ev;

    .line 1151899
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/6rE;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ev;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1151900
    iput-object p1, p0, LX/6rE;->c:LX/6qh;

    .line 1151901
    return-void
.end method

.method public final a(Landroid/os/Bundle;Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 1

    .prologue
    .line 1151902
    check-cast p2, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 1151903
    invoke-virtual {p2}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->d:LX/6re;

    invoke-direct {p0, v0}, LX/6rE;->a(LX/6re;)LX/6Ev;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/6Ev;->a(Landroid/os/Bundle;Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151904
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 2

    .prologue
    .line 1151905
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 1151906
    iget-object v0, p0, LX/6rE;->c:LX/6qh;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151907
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->d:LX/6re;

    invoke-direct {p0, v0}, LX/6rE;->a(LX/6re;)LX/6Ev;

    move-result-object v0

    iget-object v1, p0, LX/6rE;->c:LX/6qh;

    invoke-interface {v0, v1}, LX/6Ev;->a(LX/6qh;)V

    .line 1151908
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->d:LX/6re;

    invoke-direct {p0, v0}, LX/6rE;->a(LX/6re;)LX/6Ev;

    move-result-object v0

    invoke-interface {v0, p1}, LX/6Ev;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151909
    return-void
.end method

.method public final b(Landroid/os/Bundle;Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 1

    .prologue
    .line 1151910
    check-cast p2, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 1151911
    invoke-virtual {p2}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->d:LX/6re;

    invoke-direct {p0, v0}, LX/6rE;->a(LX/6re;)LX/6Ev;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/6Ev;->b(Landroid/os/Bundle;Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151912
    return-void
.end method
