.class public LX/7Eb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1184922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184923
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 1184924
    check-cast p1, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;

    .line 1184925
    iget-object v0, p1, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->d:LX/0P1;

    move-object v0, v0

    .line 1184926
    new-instance v3, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 1184927
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1184928
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v2, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 1184929
    :cond_0
    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1184930
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 1184931
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "session_blob"

    .line 1184932
    iget-object v3, p1, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1184933
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184934
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "impression_event"

    .line 1184935
    iget-object v3, p1, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1184936
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184937
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184938
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "context"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184939
    new-instance v0, LX/14N;

    const-string v1, "postResponse"

    const-string v2, "POST"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1184940
    iget-object v4, p1, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1184941
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/impressions"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1184942
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1184943
    const/4 v0, 0x0

    return-object v0
.end method
