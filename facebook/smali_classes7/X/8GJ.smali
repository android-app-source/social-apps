.class public LX/8GJ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/graphics/Rect;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1318924
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/8GJ;->a:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1318922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318923
    return-void
.end method

.method private static a(Landroid/graphics/Canvas;LX/5iG;III)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1318909
    iget-object v0, p1, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1318910
    if-nez v0, :cond_0

    .line 1318911
    :goto_0
    return-void

    .line 1318912
    :cond_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1318913
    iget-object v0, p1, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1318914
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1318915
    sget-object v1, LX/8GJ;->a:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v2, p2, p3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1318916
    sget-object v1, LX/8GJ;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1318917
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    .line 1318918
    sget-object v1, LX/8GJ;->a:Landroid/graphics/Rect;

    invoke-virtual {v1, p4, v2, p2, p3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1318919
    sget-object v1, LX/8GJ;->a:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 1318920
    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1318921
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Canvas;LX/5iG;LX/5iG;LX/5jK;III)V
    .locals 1

    .prologue
    .line 1318903
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1318904
    invoke-virtual {p3}, LX/5jK;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1318905
    invoke-static {p0, p2, p4, p5, p6}, LX/8GJ;->a(Landroid/graphics/Canvas;LX/5iG;III)V

    .line 1318906
    :cond_0
    :goto_0
    return-void

    .line 1318907
    :cond_1
    invoke-virtual {p3}, LX/5jK;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1318908
    invoke-static {p0, p1, p4, p5, p6}, LX/8GJ;->b(Landroid/graphics/Canvas;LX/5iG;III)V

    goto :goto_0
.end method

.method private static a(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1318899
    if-lez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1318900
    sub-int v3, p4, p3

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, LX/8GJ;->a(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;IIF)V

    .line 1318901
    return-void

    :cond_0
    move v0, v4

    .line 1318902
    goto :goto_0
.end method

.method private static a(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;IIF)V
    .locals 10

    .prologue
    .line 1318891
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1318892
    invoke-virtual {p1}, LX/5iG;->d()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v9, :cond_1

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 1318893
    invoke-virtual {p1, v2}, LX/5iG;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;)LX/1aX;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1318894
    invoke-virtual {p1, v2}, LX/5iG;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;)LX/1aX;

    move-result-object v0

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1318895
    if-eqz v1, :cond_0

    move-object v0, p0

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p2

    .line 1318896
    invoke-static/range {v0 .. v6}, LX/8GJ;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Lcom/facebook/photos/creativeediting/model/StickerParams;IIFLandroid/graphics/RectF;)V

    .line 1318897
    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 1318898
    :cond_1
    return-void
.end method

.method public static a(Landroid/graphics/Canvas;LX/5jK;Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;LX/5iG;LX/5iG;LX/5iG;Landroid/graphics/RectF;IIIZ)V
    .locals 1

    .prologue
    .line 1318795
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1318796
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->setSwipeableItem(LX/5iG;)V

    .line 1318797
    invoke-virtual {p1}, LX/5jK;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1318798
    invoke-static {p0, p5, p7, p8, p9}, LX/8GJ;->a(Landroid/graphics/Canvas;LX/5iG;III)V

    .line 1318799
    if-eqz p10, :cond_1

    .line 1318800
    invoke-static {p0, p4, p6, p7, p9}, LX/8GJ;->a(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    .line 1318801
    :cond_0
    :goto_0
    return-void

    .line 1318802
    :cond_1
    invoke-static {p0, p4, p6, p8, p9}, LX/8GJ;->b(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    goto :goto_0

    .line 1318803
    :cond_2
    invoke-virtual {p1}, LX/5jK;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1318804
    invoke-static {p0, p3, p7, p8, p9}, LX/8GJ;->b(Landroid/graphics/Canvas;LX/5iG;III)V

    .line 1318805
    if-eqz p10, :cond_3

    .line 1318806
    invoke-static {p0, p4, p6, p7, p9}, LX/8GJ;->c(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    goto :goto_0

    .line 1318807
    :cond_3
    invoke-static {p0, p4, p6, p8, p9}, LX/8GJ;->d(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Canvas;LX/5jK;Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;LX/5iG;LX/5iG;LX/5iG;Landroid/graphics/RectF;IIZ)V
    .locals 1

    .prologue
    .line 1318871
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1318872
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1318873
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->setSwipeableItem(LX/5iG;)V

    .line 1318874
    invoke-virtual {p1}, LX/5jK;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p9, :cond_1

    .line 1318875
    invoke-static {p0, p5, p6, p7, p8}, LX/8GJ;->c(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    .line 1318876
    invoke-virtual {p4}, LX/5iG;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1318877
    invoke-static {p0, p4, p6, p7, p8}, LX/8GJ;->a(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    .line 1318878
    :cond_0
    :goto_0
    return-void

    .line 1318879
    :cond_1
    invoke-virtual {p1}, LX/5jK;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1318880
    invoke-static {p0, p5, p6, p7, p8}, LX/8GJ;->d(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    .line 1318881
    invoke-virtual {p4}, LX/5iG;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1318882
    invoke-static {p0, p4, p6, p7, p8}, LX/8GJ;->b(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    goto :goto_0

    .line 1318883
    :cond_2
    invoke-virtual {p1}, LX/5jK;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p9, :cond_3

    .line 1318884
    invoke-static {p0, p3, p6, p7, p8}, LX/8GJ;->a(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    .line 1318885
    invoke-virtual {p4}, LX/5iG;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1318886
    invoke-static {p0, p4, p6, p7, p8}, LX/8GJ;->c(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    goto :goto_0

    .line 1318887
    :cond_3
    invoke-virtual {p1}, LX/5jK;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1318888
    invoke-static {p0, p3, p6, p7, p8}, LX/8GJ;->b(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    .line 1318889
    invoke-virtual {p4}, LX/5iG;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1318890
    invoke-static {p0, p4, p6, p7, p8}, LX/8GJ;->d(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Canvas;LX/5jK;Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;LX/5iG;LX/5iG;Landroid/graphics/RectF;IIIZ)V
    .locals 1

    .prologue
    .line 1318858
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1318859
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->setSwipeableItem(LX/5iG;)V

    .line 1318860
    invoke-virtual {p1}, LX/5jK;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1318861
    invoke-static {p0, p4, p6, p7, p8}, LX/8GJ;->a(Landroid/graphics/Canvas;LX/5iG;III)V

    .line 1318862
    if-eqz p9, :cond_1

    .line 1318863
    invoke-static {p0, p4, p5, p6, p8}, LX/8GJ;->c(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    .line 1318864
    :cond_0
    :goto_0
    return-void

    .line 1318865
    :cond_1
    invoke-static {p0, p4, p5, p7, p8}, LX/8GJ;->d(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    goto :goto_0

    .line 1318866
    :cond_2
    invoke-virtual {p1}, LX/5jK;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1318867
    invoke-static {p0, p3, p6, p7, p8}, LX/8GJ;->b(Landroid/graphics/Canvas;LX/5iG;III)V

    .line 1318868
    if-eqz p9, :cond_3

    .line 1318869
    invoke-static {p0, p3, p5, p6, p8}, LX/8GJ;->a(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    goto :goto_0

    .line 1318870
    :cond_3
    invoke-static {p0, p3, p5, p7, p8}, LX/8GJ;->b(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V

    goto :goto_0
.end method

.method private static a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Lcom/facebook/photos/creativeediting/model/StickerParams;IIFLandroid/graphics/RectF;)V
    .locals 5

    .prologue
    .line 1318845
    invoke-virtual {p6}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/model/StickerParams;->m()F

    move-result v1

    mul-float/2addr v0, v1

    iget v1, p6, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    .line 1318846
    invoke-virtual {p6}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/model/StickerParams;->n()F

    move-result v2

    mul-float/2addr v1, v2

    iget v2, p6, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v2

    .line 1318847
    invoke-virtual {p6}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/model/StickerParams;->e()F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v2, v0

    .line 1318848
    invoke-virtual {p6}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/model/StickerParams;->f()F

    move-result v4

    mul-float/2addr v3, v4

    add-float/2addr v3, v1

    .line 1318849
    sget-object v4, LX/8GJ;->a:Landroid/graphics/Rect;

    float-to-int v0, v0

    float-to-int v1, v1

    float-to-int v2, v2

    float-to-int v3, v3

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1318850
    sget-object v0, LX/8GJ;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1318851
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, p5

    float-to-int v0, v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1318852
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 1318853
    int-to-float v1, p3

    int-to-float v2, p4

    invoke-virtual {p0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1318854
    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/model/StickerParams;->c()F

    move-result v1

    sget-object v2, LX/8GJ;->a:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    sget-object v3, LX/8GJ;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1318855
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1318856
    invoke-virtual {p0, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1318857
    return-void
.end method

.method public static a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Lcom/facebook/photos/creativeediting/model/StickerParams;Landroid/graphics/RectF;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1318843
    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, v3

    move-object v6, p3

    invoke-static/range {v0 .. v6}, LX/8GJ;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Lcom/facebook/photos/creativeediting/model/StickerParams;IIFLandroid/graphics/RectF;)V

    .line 1318844
    return-void
.end method

.method private static b(Landroid/graphics/Canvas;LX/5iG;III)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1318830
    iget-object v0, p1, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1318831
    if-nez v0, :cond_0

    .line 1318832
    :goto_0
    return-void

    .line 1318833
    :cond_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1318834
    iget-object v0, p1, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1318835
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1318836
    sget-object v1, LX/8GJ;->a:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v2, p2, p3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1318837
    sget-object v1, LX/8GJ;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1318838
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    .line 1318839
    sget-object v1, LX/8GJ;->a:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v2, p4, p3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1318840
    sget-object v1, LX/8GJ;->a:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 1318841
    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1318842
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method private static b(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1318826
    if-lez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1318827
    sub-int v0, p4, p3

    int-to-float v0, v0

    const v1, 0x3e4ccccd    # 0.2f

    mul-float/2addr v0, v1

    float-to-int v4, v0

    int-to-float v0, p4

    int-to-float v1, p3

    div-float v5, v0, v1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, LX/8GJ;->a(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;IIF)V

    .line 1318828
    return-void

    :cond_0
    move v0, v3

    .line 1318829
    goto :goto_0
.end method

.method private static b(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;IIF)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1318816
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1318817
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1318818
    invoke-virtual {p1}, LX/5iG;->d()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v7, v1

    :goto_1
    if-ge v7, v9, :cond_2

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 1318819
    invoke-virtual {p1, v2}, LX/5iG;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;)LX/1aX;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1318820
    invoke-virtual {p1, v2}, LX/5iG;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;)LX/1aX;

    move-result-object v0

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1318821
    if-eqz v1, :cond_0

    move-object v0, p0

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p2

    .line 1318822
    invoke-static/range {v0 .. v6}, LX/8GJ;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Lcom/facebook/photos/creativeediting/model/StickerParams;IIFLandroid/graphics/RectF;)V

    .line 1318823
    :cond_0
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 1318824
    goto :goto_0

    .line 1318825
    :cond_2
    return-void
.end method

.method private static c(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1318812
    if-lez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1318813
    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p4

    invoke-static/range {v0 .. v5}, LX/8GJ;->b(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;IIF)V

    .line 1318814
    return-void

    :cond_0
    move v0, v4

    .line 1318815
    goto :goto_0
.end method

.method private static d(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;II)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1318808
    if-lez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1318809
    int-to-float v0, p4

    const v1, 0x3e4ccccd    # 0.2f

    mul-float/2addr v0, v1

    float-to-int v4, v0

    sub-int v0, p3, p4

    int-to-float v0, v0

    int-to-float v1, p3

    div-float v5, v0, v1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, LX/8GJ;->b(Landroid/graphics/Canvas;LX/5iG;Landroid/graphics/RectF;IIF)V

    .line 1318810
    return-void

    :cond_0
    move v0, v3

    .line 1318811
    goto :goto_0
.end method
