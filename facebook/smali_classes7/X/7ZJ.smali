.class public LX/7ZJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/gms/cast/TextTrackStyle;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/google/android/gms/cast/TextTrackStyle;
    .locals 17

    invoke-static/range {p0 .. p0}, LX/2xb;->b(Landroid/os/Parcel;)I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v15

    if-ge v15, v1, :cond_0

    invoke-static/range {p0 .. p0}, LX/2xb;->a(Landroid/os/Parcel;)I

    move-result v15

    invoke-static {v15}, LX/2xb;->a(I)I

    move-result v16

    packed-switch v16, :pswitch_data_0

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2xb;->a(Landroid/os/Parcel;I)V

    goto :goto_0

    :pswitch_0
    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v2

    goto :goto_0

    :pswitch_1
    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2xb;->j(Landroid/os/Parcel;I)F

    move-result v3

    goto :goto_0

    :pswitch_2
    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v4

    goto :goto_0

    :pswitch_3
    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v5

    goto :goto_0

    :pswitch_4
    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v6

    goto :goto_0

    :pswitch_5
    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v7

    goto :goto_0

    :pswitch_6
    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v8

    goto :goto_0

    :pswitch_7
    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v9

    goto :goto_0

    :pswitch_8
    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v10

    goto :goto_0

    :pswitch_9
    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2xb;->n(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    :pswitch_a
    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v12

    goto :goto_0

    :pswitch_b
    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v13

    goto :goto_0

    :pswitch_c
    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2xb;->n(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v14

    goto :goto_0

    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v15

    if-eq v15, v1, :cond_1

    new-instance v2, LX/4sr;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x25

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Overread allowed size end="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v2, v1, v0}, LX/4sr;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v2

    :cond_1
    new-instance v1, Lcom/google/android/gms/cast/TextTrackStyle;

    invoke-direct/range {v1 .. v14}, Lcom/google/android/gms/cast/TextTrackStyle;-><init>(IFIIIIIIILjava/lang/String;IILjava/lang/String;)V

    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, LX/7ZJ;->a(Landroid/os/Parcel;)Lcom/google/android/gms/cast/TextTrackStyle;

    move-result-object v0

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/cast/TextTrackStyle;

    return-object v0
.end method
