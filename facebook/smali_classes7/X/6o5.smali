.class public final enum LX/6o5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6o5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6o5;

.field public static final enum AVAILABLE:LX/6o5;

.field public static final enum KEY_PAIR_INVALIDATED:LX/6o5;

.field public static final enum LOCK_SCREEN_NOT_SETUP:LX/6o5;

.field public static final enum NO_ENROLLED_FINGERPRINTS:LX/6o5;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1147878
    new-instance v0, LX/6o5;

    const-string v1, "LOCK_SCREEN_NOT_SETUP"

    invoke-direct {v0, v1, v2}, LX/6o5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6o5;->LOCK_SCREEN_NOT_SETUP:LX/6o5;

    .line 1147879
    new-instance v0, LX/6o5;

    const-string v1, "NO_ENROLLED_FINGERPRINTS"

    invoke-direct {v0, v1, v3}, LX/6o5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6o5;->NO_ENROLLED_FINGERPRINTS:LX/6o5;

    .line 1147880
    new-instance v0, LX/6o5;

    const-string v1, "KEY_PAIR_INVALIDATED"

    invoke-direct {v0, v1, v4}, LX/6o5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6o5;->KEY_PAIR_INVALIDATED:LX/6o5;

    .line 1147881
    new-instance v0, LX/6o5;

    const-string v1, "AVAILABLE"

    invoke-direct {v0, v1, v5}, LX/6o5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6o5;->AVAILABLE:LX/6o5;

    .line 1147882
    const/4 v0, 0x4

    new-array v0, v0, [LX/6o5;

    sget-object v1, LX/6o5;->LOCK_SCREEN_NOT_SETUP:LX/6o5;

    aput-object v1, v0, v2

    sget-object v1, LX/6o5;->NO_ENROLLED_FINGERPRINTS:LX/6o5;

    aput-object v1, v0, v3

    sget-object v1, LX/6o5;->KEY_PAIR_INVALIDATED:LX/6o5;

    aput-object v1, v0, v4

    sget-object v1, LX/6o5;->AVAILABLE:LX/6o5;

    aput-object v1, v0, v5

    sput-object v0, LX/6o5;->$VALUES:[LX/6o5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1147883
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6o5;
    .locals 1

    .prologue
    .line 1147877
    const-class v0, LX/6o5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6o5;

    return-object v0
.end method

.method public static values()[LX/6o5;
    .locals 1

    .prologue
    .line 1147876
    sget-object v0, LX/6o5;->$VALUES:[LX/6o5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6o5;

    return-object v0
.end method
