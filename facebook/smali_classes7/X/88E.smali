.class public final LX/88E;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1301674
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1301675
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1301676
    :goto_0
    return v1

    .line 1301677
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1301678
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1301679
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1301680
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1301681
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1301682
    const-string v3, "cover_photo"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1301683
    const/4 v2, 0x0

    .line 1301684
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1301685
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1301686
    :goto_2
    move v0, v2

    .line 1301687
    goto :goto_1

    .line 1301688
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1301689
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1301690
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 1301691
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1301692
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 1301693
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1301694
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1301695
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_5

    if-eqz v3, :cond_5

    .line 1301696
    const-string v4, "photo"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1301697
    const/4 v3, 0x0

    .line 1301698
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_b

    .line 1301699
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1301700
    :goto_4
    move v0, v3

    .line 1301701
    goto :goto_3

    .line 1301702
    :cond_6
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1301703
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1301704
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3

    .line 1301705
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1301706
    :cond_9
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_a

    .line 1301707
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1301708
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1301709
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_9

    if-eqz v4, :cond_9

    .line 1301710
    const-string v5, "preview_payload"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1301711
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 1301712
    :cond_a
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1301713
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1301714
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_4

    :cond_b
    move v0, v3

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1301715
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1301716
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1301717
    if-eqz v0, :cond_2

    .line 1301718
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1301719
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1301720
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1301721
    if-eqz v1, :cond_1

    .line 1301722
    const-string p1, "photo"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1301723
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1301724
    const/4 p1, 0x0

    invoke-virtual {p0, v1, p1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p1

    .line 1301725
    if-eqz p1, :cond_0

    .line 1301726
    const-string v0, "preview_payload"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1301727
    invoke-virtual {p2, p1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1301728
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1301729
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1301730
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1301731
    return-void
.end method
