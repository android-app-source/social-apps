.class public final enum LX/7wp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7wp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7wp;

.field public static final enum BOUGHT:LX/7wp;

.field public static final enum BUYING:LX/7wp;

.field public static final enum CHECKOUT:LX/7wp;

.field public static final enum ERROR:LX/7wp;

.field public static final enum FETCH:LX/7wp;

.field public static final enum SELECT:LX/7wp;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1276287
    new-instance v0, LX/7wp;

    const-string v1, "FETCH"

    invoke-direct {v0, v1, v3}, LX/7wp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wp;->FETCH:LX/7wp;

    .line 1276288
    new-instance v0, LX/7wp;

    const-string v1, "SELECT"

    invoke-direct {v0, v1, v4}, LX/7wp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wp;->SELECT:LX/7wp;

    .line 1276289
    new-instance v0, LX/7wp;

    const-string v1, "CHECKOUT"

    invoke-direct {v0, v1, v5}, LX/7wp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wp;->CHECKOUT:LX/7wp;

    .line 1276290
    new-instance v0, LX/7wp;

    const-string v1, "BUYING"

    invoke-direct {v0, v1, v6}, LX/7wp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wp;->BUYING:LX/7wp;

    .line 1276291
    new-instance v0, LX/7wp;

    const-string v1, "BOUGHT"

    invoke-direct {v0, v1, v7}, LX/7wp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wp;->BOUGHT:LX/7wp;

    .line 1276292
    new-instance v0, LX/7wp;

    const-string v1, "ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7wp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wp;->ERROR:LX/7wp;

    .line 1276293
    const/4 v0, 0x6

    new-array v0, v0, [LX/7wp;

    sget-object v1, LX/7wp;->FETCH:LX/7wp;

    aput-object v1, v0, v3

    sget-object v1, LX/7wp;->SELECT:LX/7wp;

    aput-object v1, v0, v4

    sget-object v1, LX/7wp;->CHECKOUT:LX/7wp;

    aput-object v1, v0, v5

    sget-object v1, LX/7wp;->BUYING:LX/7wp;

    aput-object v1, v0, v6

    sget-object v1, LX/7wp;->BOUGHT:LX/7wp;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7wp;->ERROR:LX/7wp;

    aput-object v2, v0, v1

    sput-object v0, LX/7wp;->$VALUES:[LX/7wp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1276294
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7wp;
    .locals 1

    .prologue
    .line 1276286
    const-class v0, LX/7wp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7wp;

    return-object v0
.end method

.method public static values()[LX/7wp;
    .locals 1

    .prologue
    .line 1276285
    sget-object v0, LX/7wp;->$VALUES:[LX/7wp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7wp;

    return-object v0
.end method
