.class public LX/7CV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Re;

.field private final b:LX/7CS;

.field private final c:LX/2Rd;


# direct methods
.method public constructor <init>(LX/2Re;LX/7CS;LX/2Rd;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1180981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1180982
    iput-object p1, p0, LX/7CV;->a:LX/2Re;

    .line 1180983
    iput-object p2, p0, LX/7CV;->b:LX/7CS;

    .line 1180984
    iput-object p3, p0, LX/7CV;->c:LX/2Rd;

    .line 1180985
    return-void
.end method

.method public static b(LX/0QB;)LX/7CV;
    .locals 4

    .prologue
    .line 1180986
    new-instance v3, LX/7CV;

    invoke-static {p0}, LX/2Re;->a(LX/0QB;)LX/2Re;

    move-result-object v0

    check-cast v0, LX/2Re;

    .line 1180987
    new-instance v1, LX/7CS;

    invoke-direct {v1}, LX/7CS;-><init>()V

    .line 1180988
    move-object v1, v1

    .line 1180989
    move-object v1, v1

    .line 1180990
    check-cast v1, LX/7CS;

    invoke-static {p0}, LX/2Rd;->b(LX/0QB;)LX/2Rd;

    move-result-object v2

    check-cast v2, LX/2Rd;

    invoke-direct {v3, v0, v1, v2}, LX/7CV;-><init>(LX/2Re;LX/7CS;LX/2Rd;)V

    .line 1180991
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1180992
    iget-object v0, p0, LX/7CV;->a:LX/2Re;

    invoke-virtual {v0, p1}, LX/2Re;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 1180993
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1180994
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1180995
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x3

    if-lt v5, v6, :cond_0

    .line 1180996
    iget-object v5, p0, LX/7CV;->b:LX/7CS;

    invoke-virtual {v5, v0}, LX/7CS;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1180997
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1180998
    :cond_0
    iget-object v5, p0, LX/7CV;->c:LX/2Rd;

    invoke-virtual {v5, v0}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1180999
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
