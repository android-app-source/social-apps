.class public LX/7D4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1182159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(F)F
    .locals 4

    .prologue
    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    .line 1182160
    float-to-double v0, p0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    float-to-double v0, p0

    sub-double v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    :goto_0
    double-to-float v0, v0

    return v0

    :cond_0
    float-to-double v0, p0

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    neg-double v0, v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)LX/5PR;
    .locals 23
    .param p0    # Lcom/facebook/bitmaps/SphericalPhotoMetadata;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1182161
    const/16 v2, 0x10

    new-array v2, v2, [F

    .line 1182162
    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1182163
    const/4 v6, 0x0

    .line 1182164
    const/high16 v5, 0x3f800000    # 1.0f

    .line 1182165
    const/4 v4, 0x0

    .line 1182166
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1182167
    if-eqz p0, :cond_4

    .line 1182168
    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->j()D

    move-result-wide v4

    double-to-float v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/high16 v7, -0x40800000    # -1.0f

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 1182169
    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->i()D

    move-result-wide v4

    double-to-float v4, v4

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 1182170
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->f()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->d()I

    move-result v4

    int-to-float v4, v4

    div-float v6, v3, v4

    .line 1182171
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->h()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->d()I

    move-result v4

    int-to-float v4, v4

    div-float v5, v3, v4

    .line 1182172
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->e()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->c()I

    move-result v4

    int-to-float v4, v4

    div-float v4, v3, v4

    .line 1182173
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->g()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->c()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v3, v7

    move v9, v3

    move v10, v4

    move v11, v5

    move v12, v6

    .line 1182174
    :goto_0
    const/16 v3, 0xb43

    new-array v0, v3, [F

    move-object/from16 v17, v0

    .line 1182175
    const/16 v3, 0x782

    new-array v0, v3, [F

    move-object/from16 v18, v0

    .line 1182176
    const/4 v3, 0x4

    new-array v7, v3, [F

    .line 1182177
    const/4 v3, 0x4

    new-array v3, v3, [F

    .line 1182178
    const/4 v6, 0x0

    .line 1182179
    const/4 v5, 0x0

    .line 1182180
    const/4 v4, 0x0

    move/from16 v16, v4

    :goto_1
    const/16 v4, 0x1e

    move/from16 v0, v16

    if-gt v0, v4, :cond_1

    .line 1182181
    move/from16 v0, v16

    int-to-float v4, v0

    const/high16 v8, 0x41f00000    # 30.0f

    div-float/2addr v4, v8

    mul-float/2addr v4, v11

    add-float/2addr v4, v12

    .line 1182182
    const v8, 0x40490fdb    # (float)Math.PI

    mul-float v19, v4, v8

    .line 1182183
    const/4 v4, 0x0

    move v13, v4

    move v14, v5

    move v15, v6

    :goto_2
    const/16 v4, 0x1e

    if-gt v13, v4, :cond_0

    .line 1182184
    int-to-float v4, v13

    const/high16 v5, 0x41f00000    # 30.0f

    div-float/2addr v4, v5

    mul-float/2addr v4, v9

    add-float/2addr v4, v10

    .line 1182185
    const v5, 0x40c90fdb

    mul-float/2addr v4, v5

    .line 1182186
    float-to-double v0, v4

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->sin(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v5, v0

    .line 1182187
    float-to-double v0, v4

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v4, v0

    .line 1182188
    neg-float v5, v5

    .line 1182189
    invoke-static/range {v19 .. v19}, LX/7D4;->a(F)F

    move-result v6

    .line 1182190
    int-to-float v8, v13

    const/high16 v20, 0x41f00000    # 30.0f

    div-float v20, v8, v20

    .line 1182191
    move/from16 v0, v16

    int-to-float v8, v0

    const/high16 v21, 0x41f00000    # 30.0f

    div-float v21, v8, v21

    .line 1182192
    const/4 v8, 0x0

    aput v5, v7, v8

    .line 1182193
    const/4 v5, 0x1

    aput v6, v7, v5

    .line 1182194
    const/4 v5, 0x2

    aput v4, v7, v5

    .line 1182195
    const/4 v4, 0x3

    const/4 v5, 0x0

    aput v5, v7, v4

    .line 1182196
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v5, v2

    invoke-static/range {v3 .. v8}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 1182197
    const/4 v4, 0x0

    aget v4, v3, v4

    .line 1182198
    const/4 v5, 0x1

    aget v5, v3, v5

    .line 1182199
    const/4 v6, 0x2

    aget v8, v3, v6

    .line 1182200
    add-int/lit8 v22, v15, 0x1

    aput v20, v18, v15

    .line 1182201
    add-int/lit8 v6, v22, 0x1

    aput v21, v18, v22

    .line 1182202
    add-int/lit8 v15, v14, 0x1

    const/high16 v20, 0x40000000    # 2.0f

    mul-float v4, v4, v20

    aput v4, v17, v14

    .line 1182203
    add-int/lit8 v4, v15, 0x1

    const/high16 v14, 0x40000000    # 2.0f

    mul-float/2addr v5, v14

    aput v5, v17, v15

    .line 1182204
    add-int/lit8 v5, v4, 0x1

    const/high16 v14, 0x40000000    # 2.0f

    mul-float/2addr v8, v14

    aput v8, v17, v4

    .line 1182205
    add-int/lit8 v4, v13, 0x1

    move v13, v4

    move v14, v5

    move v15, v6

    goto :goto_2

    .line 1182206
    :cond_0
    add-int/lit8 v4, v16, 0x1

    move/from16 v16, v4

    move v5, v14

    move v6, v15

    goto/16 :goto_1

    .line 1182207
    :cond_1
    const/16 v2, 0x1518

    new-array v5, v2, [S

    .line 1182208
    const/4 v3, 0x0

    .line 1182209
    const/4 v2, 0x0

    move v4, v2

    :goto_3
    const/16 v2, 0x1e

    if-ge v4, v2, :cond_3

    .line 1182210
    const/4 v2, 0x0

    :goto_4
    const/16 v6, 0x1e

    if-ge v2, v6, :cond_2

    .line 1182211
    mul-int/lit8 v6, v4, 0x1f

    add-int/2addr v6, v2

    int-to-short v6, v6

    .line 1182212
    add-int/lit8 v7, v6, 0x1e

    add-int/lit8 v7, v7, 0x1

    int-to-short v7, v7

    .line 1182213
    add-int/lit8 v8, v3, 0x1

    aput-short v6, v5, v3

    .line 1182214
    add-int/lit8 v3, v8, 0x1

    add-int/lit8 v9, v6, 0x1

    int-to-short v9, v9

    aput-short v9, v5, v8

    .line 1182215
    add-int/lit8 v8, v3, 0x1

    aput-short v7, v5, v3

    .line 1182216
    add-int/lit8 v3, v8, 0x1

    aput-short v7, v5, v8

    .line 1182217
    add-int/lit8 v8, v3, 0x1

    add-int/lit8 v6, v6, 0x1

    int-to-short v6, v6

    aput-short v6, v5, v3

    .line 1182218
    add-int/lit8 v3, v8, 0x1

    add-int/lit8 v6, v7, 0x1

    int-to-short v6, v6

    aput-short v6, v5, v8

    .line 1182219
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1182220
    :cond_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 1182221
    :cond_3
    new-instance v2, LX/5PQ;

    const/16 v3, 0xb43

    invoke-direct {v2, v3}, LX/5PQ;-><init>(I)V

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, LX/5PQ;->a(I)LX/5PQ;

    move-result-object v2

    const-string v3, "aPosition"

    new-instance v4, LX/5Pg;

    const/4 v6, 0x3

    move-object/from16 v0, v17

    invoke-direct {v4, v0, v6}, LX/5Pg;-><init>([FI)V

    invoke-virtual {v2, v3, v4}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v2

    new-instance v3, LX/5PY;

    invoke-direct {v3, v5}, LX/5PY;-><init>([S)V

    invoke-virtual {v2, v3}, LX/5PQ;->a(LX/5PY;)LX/5PQ;

    move-result-object v2

    const-string v3, "aTextureCoord"

    new-instance v4, LX/5Pg;

    const/4 v5, 0x2

    move-object/from16 v0, v18

    invoke-direct {v4, v0, v5}, LX/5Pg;-><init>([FI)V

    invoke-virtual {v2, v3, v4}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v2

    invoke-virtual {v2}, LX/5PQ;->a()LX/5PR;

    move-result-object v2

    return-object v2

    :cond_4
    move v9, v3

    move v10, v4

    move v11, v5

    move v12, v6

    goto/16 :goto_0
.end method
