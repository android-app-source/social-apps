.class public LX/7SU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/61B;


# instance fields
.field private final a:Lcom/facebook/common/callercontext/CallerContext;

.field private final b:LX/BVF;

.field private final c:LX/7SW;

.field public d:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/5Pb;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/5PR;

.field public final f:LX/7SZ;

.field public final g:LX/7SS;

.field public final h:LX/7SS;

.field public final i:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Landroid/net/Uri;LX/7SR;LX/7SR;Lcom/facebook/common/callercontext/CallerContext;LX/BVF;LX/7SW;LX/7ST;LX/7Sa;)V
    .locals 4
    .param p1    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/7SR;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/7SR;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/BVF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 1208861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1208862
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/7SU;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1208863
    iput-object p4, p0, LX/7SU;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1208864
    iput-object p5, p0, LX/7SU;->b:LX/BVF;

    .line 1208865
    iput-object p6, p0, LX/7SU;->c:LX/7SW;

    .line 1208866
    iget-object v0, p0, LX/7SU;->b:LX/BVF;

    invoke-virtual {p8, p1, p4, v0}, LX/7Sa;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;LX/BVF;)LX/7SZ;

    move-result-object v0

    iput-object v0, p0, LX/7SU;->f:LX/7SZ;

    .line 1208867
    if-eqz p2, :cond_1

    iget-object v0, p0, LX/7SU;->a:Lcom/facebook/common/callercontext/CallerContext;

    const/4 v2, 0x1

    invoke-virtual {p7, p2, v0, v2}, LX/7ST;->a(LX/7SR;Lcom/facebook/common/callercontext/CallerContext;Z)LX/7SS;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/7SU;->g:LX/7SS;

    .line 1208868
    if-eqz p3, :cond_0

    iget-object v0, p0, LX/7SU;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p7, p3, v0, v3}, LX/7ST;->a(LX/7SR;Lcom/facebook/common/callercontext/CallerContext;Z)LX/7SS;

    move-result-object v1

    :cond_0
    iput-object v1, p0, LX/7SU;->h:LX/7SS;

    .line 1208869
    return-void

    :cond_1
    move-object v0, v1

    .line 1208870
    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 1208871
    iget-object v0, p0, LX/7SU;->b:LX/BVF;

    .line 1208872
    invoke-static {v0}, LX/BVF;->g(LX/BVF;)Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;

    move-result-object p0

    invoke-virtual {p0, p1, p2}, Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;->setPreviewViewSize(II)V

    .line 1208873
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 0

    .prologue
    .line 1208874
    return-void
.end method

.method public final a([F[F[FJ)V
    .locals 3

    .prologue
    .line 1208875
    iget-object v0, p0, LX/7SU;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1208876
    iget-object v0, p0, LX/7SU;->f:LX/7SZ;

    .line 1208877
    iget-object v1, v0, LX/7SZ;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;

    invoke-direct {v2, v0}, Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;-><init>(LX/7SZ;)V

    const p1, 0x2acf6a54

    invoke-static {v1, v2, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1208878
    iget-object v0, p0, LX/7SU;->g:LX/7SS;

    if-eqz v0, :cond_0

    .line 1208879
    iget-object v0, p0, LX/7SU;->g:LX/7SS;

    invoke-virtual {v0}, LX/7SS;->a()V

    .line 1208880
    :cond_0
    iget-object v0, p0, LX/7SU;->h:LX/7SS;

    if-eqz v0, :cond_1

    .line 1208881
    iget-object v0, p0, LX/7SU;->h:LX/7SS;

    invoke-virtual {v0}, LX/7SS;->a()V

    .line 1208882
    :cond_1
    iget-object v0, p0, LX/7SU;->c:LX/7SW;

    invoke-virtual {v0}, LX/7SW;->a()LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/7SU;->d:LX/1FJ;

    .line 1208883
    :cond_2
    :goto_0
    return-void

    .line 1208884
    :cond_3
    iget-object v0, p0, LX/7SU;->b:LX/BVF;

    invoke-virtual {v0}, LX/BVF;->c()I

    move-result v1

    .line 1208885
    if-eqz v1, :cond_2

    .line 1208886
    iget-object v0, p0, LX/7SU;->e:LX/5PR;

    if-nez v0, :cond_5

    .line 1208887
    iget-object v0, p0, LX/7SU;->b:LX/BVF;

    .line 1208888
    iget v2, v0, LX/BVF;->a:I

    move v2, v2

    .line 1208889
    invoke-virtual {v0}, LX/BVF;->b()LX/0P1;

    move-result-object p1

    .line 1208890
    new-instance p2, LX/5PQ;

    mul-int/lit8 p3, v2, 0x4

    invoke-direct {p2, p3}, LX/5PQ;-><init>(I)V

    const/4 p3, 0x4

    .line 1208891
    iput p3, p2, LX/5PQ;->a:I

    .line 1208892
    move-object p2, p2

    .line 1208893
    new-instance p3, LX/5PZ;

    .line 1208894
    invoke-static {v0}, LX/BVF;->g(LX/BVF;)Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;

    move-result-object p4

    invoke-virtual {p4}, Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;->indexArray()Ljava/nio/ByteBuffer;

    move-result-object p4

    move-object p4, p4

    .line 1208895
    mul-int/lit8 v2, v2, 0x6

    invoke-direct {p3, p4, v2}, LX/5PZ;-><init>(Ljava/nio/Buffer;I)V

    invoke-virtual {p2, p3}, LX/5PQ;->a(LX/5PY;)LX/5PQ;

    move-result-object p2

    .line 1208896
    invoke-virtual {p1}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1208897
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5Pg;

    invoke-virtual {p2, p1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    goto :goto_1

    .line 1208898
    :cond_4
    invoke-virtual {p2}, LX/5PQ;->a()LX/5PR;

    move-result-object v2

    move-object v0, v2

    .line 1208899
    iput-object v0, p0, LX/7SU;->e:LX/5PR;

    .line 1208900
    :cond_5
    iget-object v0, p0, LX/7SU;->e:LX/5PR;

    iget-object v0, v0, LX/5PR;->b:LX/5PY;

    check-cast v0, LX/5PZ;

    mul-int/lit8 v1, v1, 0x6

    iput v1, v0, LX/5PZ;->d:I

    .line 1208901
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 1208902
    const-string v0, "GL_BLEND"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1208903
    const/16 v0, 0x302

    const/16 v1, 0x303

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 1208904
    const-string v0, "blendFunc"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1208905
    iget-object v0, p0, LX/7SU;->d:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Pb;

    invoke-virtual {v0}, LX/5Pb;->a()LX/5Pa;

    move-result-object v0

    .line 1208906
    const-string v1, "uTimestamp"

    iget-object v2, p0, LX/7SU;->b:LX/BVF;

    .line 1208907
    invoke-static {v2}, LX/BVF;->g(LX/BVF;)Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;->timestamp()I

    move-result p1

    move v2, p1

    .line 1208908
    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;

    .line 1208909
    iget-object v1, p0, LX/7SU;->f:LX/7SZ;

    invoke-virtual {v1, v0}, LX/7SZ;->a(LX/5Pa;)V

    .line 1208910
    iget-object v1, p0, LX/7SU;->g:LX/7SS;

    if-eqz v1, :cond_6

    .line 1208911
    iget-object v1, p0, LX/7SU;->g:LX/7SS;

    invoke-virtual {v1, v0}, LX/7SS;->a(LX/5Pa;)V

    .line 1208912
    :cond_6
    iget-object v1, p0, LX/7SU;->h:LX/7SS;

    if-eqz v1, :cond_7

    .line 1208913
    iget-object v1, p0, LX/7SU;->h:LX/7SS;

    invoke-virtual {v1, v0}, LX/7SS;->a(LX/5Pa;)V

    .line 1208914
    :cond_7
    iget-object v1, p0, LX/7SU;->e:LX/5PR;

    invoke-virtual {v0, v1}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1208915
    iget-object v0, p0, LX/7SU;->f:LX/7SZ;

    invoke-virtual {v0}, LX/7SZ;->b()V

    .line 1208916
    iget-object v0, p0, LX/7SU;->g:LX/7SS;

    if-eqz v0, :cond_0

    .line 1208917
    iget-object v0, p0, LX/7SU;->g:LX/7SS;

    invoke-virtual {v0}, LX/7SS;->b()V

    .line 1208918
    :cond_0
    iget-object v0, p0, LX/7SU;->h:LX/7SS;

    if-eqz v0, :cond_1

    .line 1208919
    iget-object v0, p0, LX/7SU;->h:LX/7SS;

    invoke-virtual {v0}, LX/7SS;->b()V

    .line 1208920
    :cond_1
    iget-object v0, p0, LX/7SU;->d:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1208921
    const/4 v0, 0x0

    iput-object v0, p0, LX/7SU;->d:LX/1FJ;

    .line 1208922
    iget-object v0, p0, LX/7SU;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1208923
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1208924
    const/4 v0, 0x1

    return v0
.end method
