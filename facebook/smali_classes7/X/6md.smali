.class public LX/6md;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;


# instance fields
.field public final errStr:Ljava/lang/String;

.field public final errno:Ljava/lang/Integer;

.field public final fbTraceMeta:Ljava/lang/String;

.field public final isRetryable:Ljava/lang/Boolean;

.field public final isSICheckInMqttFailed:Ljava/lang/Boolean;

.field public final offlineThreadingId:Ljava/lang/Long;

.field public final sendSucceeded:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0xb

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 1145837
    new-instance v0, LX/1sv;

    const-string v1, "SendMessageResponse"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6md;->b:LX/1sv;

    .line 1145838
    new-instance v0, LX/1sw;

    const-string v1, "offlineThreadingId"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6md;->c:LX/1sw;

    .line 1145839
    new-instance v0, LX/1sw;

    const-string v1, "sendSucceeded"

    invoke-direct {v0, v1, v4, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6md;->d:LX/1sw;

    .line 1145840
    new-instance v0, LX/1sw;

    const-string v1, "errno"

    const/16 v2, 0x8

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6md;->e:LX/1sw;

    .line 1145841
    new-instance v0, LX/1sw;

    const-string v1, "errStr"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6md;->f:LX/1sw;

    .line 1145842
    new-instance v0, LX/1sw;

    const-string v1, "isRetryable"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6md;->g:LX/1sw;

    .line 1145843
    new-instance v0, LX/1sw;

    const-string v1, "fbTraceMeta"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6md;->h:LX/1sw;

    .line 1145844
    new-instance v0, LX/1sw;

    const-string v1, "isSICheckInMqttFailed"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6md;->i:LX/1sw;

    .line 1145845
    sput-boolean v5, LX/6md;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1146012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146013
    iput-object p1, p0, LX/6md;->offlineThreadingId:Ljava/lang/Long;

    .line 1146014
    iput-object p2, p0, LX/6md;->sendSucceeded:Ljava/lang/Boolean;

    .line 1146015
    iput-object p3, p0, LX/6md;->errno:Ljava/lang/Integer;

    .line 1146016
    iput-object p4, p0, LX/6md;->errStr:Ljava/lang/String;

    .line 1146017
    iput-object p5, p0, LX/6md;->isRetryable:Ljava/lang/Boolean;

    .line 1146018
    iput-object p6, p0, LX/6md;->fbTraceMeta:Ljava/lang/String;

    .line 1146019
    iput-object p7, p0, LX/6md;->isSICheckInMqttFailed:Ljava/lang/Boolean;

    .line 1146020
    return-void
.end method

.method public static b(LX/1su;)LX/6md;
    .locals 12

    .prologue
    const/16 v11, 0xb

    const/4 v10, 0x2

    const/4 v7, 0x0

    .line 1145983
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v6, v7

    move-object v5, v7

    move-object v4, v7

    move-object v3, v7

    move-object v2, v7

    move-object v1, v7

    .line 1145984
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1145985
    iget-byte v8, v0, LX/1sw;->b:B

    if-eqz v8, :cond_7

    .line 1145986
    iget-short v8, v0, LX/1sw;->c:S

    packed-switch v8, :pswitch_data_0

    .line 1145987
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1145988
    :pswitch_0
    iget-byte v8, v0, LX/1sw;->b:B

    const/16 v9, 0xa

    if-ne v8, v9, :cond_0

    .line 1145989
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 1145990
    :cond_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1145991
    :pswitch_1
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_1

    .line 1145992
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 1145993
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1145994
    :pswitch_2
    iget-byte v8, v0, LX/1sw;->b:B

    const/16 v9, 0x8

    if-ne v8, v9, :cond_2

    .line 1145995
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    .line 1145996
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1145997
    :pswitch_3
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v11, :cond_3

    .line 1145998
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 1145999
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1146000
    :pswitch_4
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_4

    .line 1146001
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_0

    .line 1146002
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1146003
    :pswitch_5
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v11, :cond_5

    .line 1146004
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 1146005
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1146006
    :pswitch_6
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_6

    .line 1146007
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    goto/16 :goto_0

    .line 1146008
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1146009
    :cond_7
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1146010
    new-instance v0, LX/6md;

    invoke-direct/range {v0 .. v7}, LX/6md;-><init>(Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1146011
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1145907
    if-eqz p2, :cond_c

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1145908
    :goto_0
    if-eqz p2, :cond_d

    const-string v0, "\n"

    move-object v3, v0

    .line 1145909
    :goto_1
    if-eqz p2, :cond_e

    const-string v0, " "

    .line 1145910
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "SendMessageResponse"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1145911
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145912
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145913
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145914
    const/4 v1, 0x1

    .line 1145915
    iget-object v6, p0, LX/6md;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v6, :cond_0

    .line 1145916
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145917
    const-string v1, "offlineThreadingId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145918
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145919
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145920
    iget-object v1, p0, LX/6md;->offlineThreadingId:Ljava/lang/Long;

    if-nez v1, :cond_f

    .line 1145921
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1145922
    :cond_0
    iget-object v6, p0, LX/6md;->sendSucceeded:Ljava/lang/Boolean;

    if-eqz v6, :cond_2

    .line 1145923
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145924
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145925
    const-string v1, "sendSucceeded"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145926
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145927
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145928
    iget-object v1, p0, LX/6md;->sendSucceeded:Ljava/lang/Boolean;

    if-nez v1, :cond_10

    .line 1145929
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 1145930
    :cond_2
    iget-object v6, p0, LX/6md;->errno:Ljava/lang/Integer;

    if-eqz v6, :cond_4

    .line 1145931
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145932
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145933
    const-string v1, "errno"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145934
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145935
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145936
    iget-object v1, p0, LX/6md;->errno:Ljava/lang/Integer;

    if-nez v1, :cond_11

    .line 1145937
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v1, v2

    .line 1145938
    :cond_4
    iget-object v6, p0, LX/6md;->errStr:Ljava/lang/String;

    if-eqz v6, :cond_6

    .line 1145939
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145940
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145941
    const-string v1, "errStr"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145942
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145943
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145944
    iget-object v1, p0, LX/6md;->errStr:Ljava/lang/String;

    if-nez v1, :cond_12

    .line 1145945
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    move v1, v2

    .line 1145946
    :cond_6
    iget-object v6, p0, LX/6md;->isRetryable:Ljava/lang/Boolean;

    if-eqz v6, :cond_8

    .line 1145947
    if-nez v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145948
    :cond_7
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145949
    const-string v1, "isRetryable"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145950
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145951
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145952
    iget-object v1, p0, LX/6md;->isRetryable:Ljava/lang/Boolean;

    if-nez v1, :cond_13

    .line 1145953
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_7
    move v1, v2

    .line 1145954
    :cond_8
    iget-object v6, p0, LX/6md;->fbTraceMeta:Ljava/lang/String;

    if-eqz v6, :cond_16

    .line 1145955
    if-nez v1, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145956
    :cond_9
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145957
    const-string v1, "fbTraceMeta"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145958
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145959
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145960
    iget-object v1, p0, LX/6md;->fbTraceMeta:Ljava/lang/String;

    if-nez v1, :cond_14

    .line 1145961
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145962
    :goto_8
    iget-object v1, p0, LX/6md;->isSICheckInMqttFailed:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 1145963
    if-nez v2, :cond_a

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145964
    :cond_a
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145965
    const-string v1, "isSICheckInMqttFailed"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145966
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145967
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145968
    iget-object v0, p0, LX/6md;->isSICheckInMqttFailed:Ljava/lang/Boolean;

    if-nez v0, :cond_15

    .line 1145969
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145970
    :cond_b
    :goto_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145971
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145972
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1145973
    :cond_c
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1145974
    :cond_d
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1145975
    :cond_e
    const-string v0, ""

    goto/16 :goto_2

    .line 1145976
    :cond_f
    iget-object v1, p0, LX/6md;->offlineThreadingId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1145977
    :cond_10
    iget-object v1, p0, LX/6md;->sendSucceeded:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1145978
    :cond_11
    iget-object v1, p0, LX/6md;->errno:Ljava/lang/Integer;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1145979
    :cond_12
    iget-object v1, p0, LX/6md;->errStr:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1145980
    :cond_13
    iget-object v1, p0, LX/6md;->isRetryable:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1145981
    :cond_14
    iget-object v1, p0, LX/6md;->fbTraceMeta:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1145982
    :cond_15
    iget-object v0, p0, LX/6md;->isSICheckInMqttFailed:Ljava/lang/Boolean;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    :cond_16
    move v2, v1

    goto/16 :goto_8
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1146021
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1146022
    iget-object v0, p0, LX/6md;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1146023
    iget-object v0, p0, LX/6md;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1146024
    sget-object v0, LX/6md;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146025
    iget-object v0, p0, LX/6md;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1146026
    :cond_0
    iget-object v0, p0, LX/6md;->sendSucceeded:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1146027
    iget-object v0, p0, LX/6md;->sendSucceeded:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1146028
    sget-object v0, LX/6md;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146029
    iget-object v0, p0, LX/6md;->sendSucceeded:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1146030
    :cond_1
    iget-object v0, p0, LX/6md;->errno:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1146031
    iget-object v0, p0, LX/6md;->errno:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1146032
    sget-object v0, LX/6md;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146033
    iget-object v0, p0, LX/6md;->errno:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1146034
    :cond_2
    iget-object v0, p0, LX/6md;->errStr:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1146035
    iget-object v0, p0, LX/6md;->errStr:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1146036
    sget-object v0, LX/6md;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146037
    iget-object v0, p0, LX/6md;->errStr:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1146038
    :cond_3
    iget-object v0, p0, LX/6md;->isRetryable:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1146039
    iget-object v0, p0, LX/6md;->isRetryable:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1146040
    sget-object v0, LX/6md;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146041
    iget-object v0, p0, LX/6md;->isRetryable:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1146042
    :cond_4
    iget-object v0, p0, LX/6md;->fbTraceMeta:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1146043
    iget-object v0, p0, LX/6md;->fbTraceMeta:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1146044
    sget-object v0, LX/6md;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146045
    iget-object v0, p0, LX/6md;->fbTraceMeta:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1146046
    :cond_5
    iget-object v0, p0, LX/6md;->isSICheckInMqttFailed:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 1146047
    iget-object v0, p0, LX/6md;->isSICheckInMqttFailed:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 1146048
    sget-object v0, LX/6md;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146049
    iget-object v0, p0, LX/6md;->isSICheckInMqttFailed:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1146050
    :cond_6
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1146051
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1146052
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1145850
    if-nez p1, :cond_1

    .line 1145851
    :cond_0
    :goto_0
    return v0

    .line 1145852
    :cond_1
    instance-of v1, p1, LX/6md;

    if-eqz v1, :cond_0

    .line 1145853
    check-cast p1, LX/6md;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1145854
    if-nez p1, :cond_3

    .line 1145855
    :cond_2
    :goto_1
    move v0, v2

    .line 1145856
    goto :goto_0

    .line 1145857
    :cond_3
    iget-object v0, p0, LX/6md;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1145858
    :goto_2
    iget-object v3, p1, LX/6md;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1145859
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1145860
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145861
    iget-object v0, p0, LX/6md;->offlineThreadingId:Ljava/lang/Long;

    iget-object v3, p1, LX/6md;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145862
    :cond_5
    iget-object v0, p0, LX/6md;->sendSucceeded:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1145863
    :goto_4
    iget-object v3, p1, LX/6md;->sendSucceeded:Ljava/lang/Boolean;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1145864
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1145865
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145866
    iget-object v0, p0, LX/6md;->sendSucceeded:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6md;->sendSucceeded:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145867
    :cond_7
    iget-object v0, p0, LX/6md;->errno:Ljava/lang/Integer;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1145868
    :goto_6
    iget-object v3, p1, LX/6md;->errno:Ljava/lang/Integer;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1145869
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1145870
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145871
    iget-object v0, p0, LX/6md;->errno:Ljava/lang/Integer;

    iget-object v3, p1, LX/6md;->errno:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145872
    :cond_9
    iget-object v0, p0, LX/6md;->errStr:Ljava/lang/String;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1145873
    :goto_8
    iget-object v3, p1, LX/6md;->errStr:Ljava/lang/String;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1145874
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1145875
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145876
    iget-object v0, p0, LX/6md;->errStr:Ljava/lang/String;

    iget-object v3, p1, LX/6md;->errStr:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145877
    :cond_b
    iget-object v0, p0, LX/6md;->isRetryable:Ljava/lang/Boolean;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1145878
    :goto_a
    iget-object v3, p1, LX/6md;->isRetryable:Ljava/lang/Boolean;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1145879
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1145880
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145881
    iget-object v0, p0, LX/6md;->isRetryable:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6md;->isRetryable:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145882
    :cond_d
    iget-object v0, p0, LX/6md;->fbTraceMeta:Ljava/lang/String;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1145883
    :goto_c
    iget-object v3, p1, LX/6md;->fbTraceMeta:Ljava/lang/String;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1145884
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1145885
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145886
    iget-object v0, p0, LX/6md;->fbTraceMeta:Ljava/lang/String;

    iget-object v3, p1, LX/6md;->fbTraceMeta:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145887
    :cond_f
    iget-object v0, p0, LX/6md;->isSICheckInMqttFailed:Ljava/lang/Boolean;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1145888
    :goto_e
    iget-object v3, p1, LX/6md;->isSICheckInMqttFailed:Ljava/lang/Boolean;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1145889
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1145890
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145891
    iget-object v0, p0, LX/6md;->isSICheckInMqttFailed:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6md;->isSICheckInMqttFailed:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_11
    move v2, v1

    .line 1145892
    goto/16 :goto_1

    :cond_12
    move v0, v2

    .line 1145893
    goto/16 :goto_2

    :cond_13
    move v3, v2

    .line 1145894
    goto/16 :goto_3

    :cond_14
    move v0, v2

    .line 1145895
    goto/16 :goto_4

    :cond_15
    move v3, v2

    .line 1145896
    goto/16 :goto_5

    :cond_16
    move v0, v2

    .line 1145897
    goto/16 :goto_6

    :cond_17
    move v3, v2

    .line 1145898
    goto/16 :goto_7

    :cond_18
    move v0, v2

    .line 1145899
    goto/16 :goto_8

    :cond_19
    move v3, v2

    .line 1145900
    goto :goto_9

    :cond_1a
    move v0, v2

    .line 1145901
    goto :goto_a

    :cond_1b
    move v3, v2

    .line 1145902
    goto :goto_b

    :cond_1c
    move v0, v2

    .line 1145903
    goto :goto_c

    :cond_1d
    move v3, v2

    .line 1145904
    goto :goto_d

    :cond_1e
    move v0, v2

    .line 1145905
    goto :goto_e

    :cond_1f
    move v3, v2

    .line 1145906
    goto :goto_f
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1145849
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1145846
    sget-boolean v0, LX/6md;->a:Z

    .line 1145847
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6md;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1145848
    return-object v0
.end method
