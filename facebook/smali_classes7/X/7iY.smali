.class public LX/7iY;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field public a:F

.field public b:Landroid/view/View;


# direct methods
.method public constructor <init>(ILandroid/view/View;)V
    .locals 2

    .prologue
    .line 1227689
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1227690
    iput-object p2, p0, LX/7iY;->b:Landroid/view/View;

    .line 1227691
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, LX/7iY;->setDuration(J)V

    .line 1227692
    new-instance v0, LX/7iX;

    invoke-direct {v0, p0}, LX/7iX;-><init>(LX/7iY;)V

    invoke-virtual {p0, v0}, LX/7iY;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1227693
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1227684
    cmpl-float v0, p1, v2

    if-nez v0, :cond_0

    .line 1227685
    iget-object v0, p0, LX/7iY;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1227686
    :goto_0
    iget-object v0, p0, LX/7iY;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1227687
    return-void

    .line 1227688
    :cond_0
    iget-object v0, p0, LX/7iY;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, LX/7iY;->a:F

    sub-float/2addr v2, p1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method public final willChangeBounds()Z
    .locals 1

    .prologue
    .line 1227683
    const/4 v0, 0x1

    return v0
.end method
