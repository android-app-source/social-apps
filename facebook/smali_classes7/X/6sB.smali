.class public LX/6sB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6rs;


# direct methods
.method public constructor <init>(LX/6rs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1152960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152961
    iput-object p1, p0, LX/6sB;->a:LX/6rs;

    .line 1152962
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1152963
    const-string v0, "identifier"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6rc;->forValue(Ljava/lang/String;)LX/6rc;

    move-result-object v0

    .line 1152964
    sget-object v1, LX/6rc;->OPTIONS:LX/6rc;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152965
    const-string v0, "collected_data_key"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152966
    const-string v0, "title"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152967
    const-string v0, "actionable_title"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152968
    iget-object v0, p0, LX/6sB;->a:LX/6rs;

    invoke-virtual {v0, p1}, LX/6rs;->m(Ljava/lang/String;)LX/6rr;

    move-result-object v0

    const-string v1, "options"

    invoke-static {p2, v1}, LX/16N;->d(LX/0lF;Ljava/lang/String;)LX/162;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Px;

    .line 1152969
    const-string v0, "collected_data_key"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "actionable_title"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "option_list_title"

    invoke-virtual {p2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "pre_selected_option_ids"

    invoke-static {p2, v4}, LX/16N;->b(LX/0lF;Ljava/lang/String;)LX/0Px;

    move-result-object v4

    invoke-static/range {v0 .. v5}, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;)LX/6rY;

    move-result-object v0

    const-string v1, "allows_multiple_selection"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    .line 1152970
    iput-boolean v1, v0, LX/6rY;->g:Z

    .line 1152971
    move-object v0, v0

    .line 1152972
    const-string v1, "should_add_to_order_summary"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    .line 1152973
    iput-boolean v1, v0, LX/6rY;->h:Z

    .line 1152974
    move-object v0, v0

    .line 1152975
    const-string v1, "optional"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    .line 1152976
    iput-boolean v1, v0, LX/6rY;->j:Z

    .line 1152977
    move-object v1, v0

    .line 1152978
    const-string v0, "custom_option"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1152979
    iget-object v0, p0, LX/6sB;->a:LX/6rs;

    .line 1152980
    iget-object v2, v0, LX/6rs;->t:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6rr;

    move-object v0, v2

    .line 1152981
    const-string v2, "custom_option"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-interface {v0, p1, v2}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;

    .line 1152982
    iput-object v0, v1, LX/6rY;->i:Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;

    .line 1152983
    :cond_0
    invoke-virtual {v1}, LX/6rY;->a()Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    move-result-object v0

    return-object v0

    .line 1152984
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
