.class public LX/7GP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final g:Ljava/lang/Object;


# instance fields
.field private final b:LX/0SG;

.field private final c:LX/7GL;

.field private final d:LX/7Gg;

.field private final e:LX/7G1;

.field private final f:LX/7G3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1189094
    const-class v0, LX/7GP;

    sput-object v0, LX/7GP;->a:Ljava/lang/Class;

    .line 1189095
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/7GP;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/7GL;LX/7Gg;LX/7G1;LX/7G3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1189096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189097
    iput-object p1, p0, LX/7GP;->b:LX/0SG;

    .line 1189098
    iput-object p2, p0, LX/7GP;->c:LX/7GL;

    .line 1189099
    iput-object p3, p0, LX/7GP;->d:LX/7Gg;

    .line 1189100
    iput-object p4, p0, LX/7GP;->e:LX/7G1;

    .line 1189101
    iput-object p5, p0, LX/7GP;->f:LX/7G3;

    .line 1189102
    return-void
.end method

.method private a(LX/7GT;Landroid/os/Parcelable;Ljava/util/List;LX/7Fx;)LX/7GN;
    .locals 11
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PREFETCH_DATA_TYPE::",
            "Landroid/os/Parcelable;",
            "DE",
            "LTA_WRAPPER_TYPE:Ljava/lang/Object;",
            ">(",
            "LX/7GT;",
            "TPREFETCH_DATA_TYPE;",
            "Ljava/util/List",
            "<",
            "LX/7GJ",
            "<TDE",
            "LTA_WRAPPER_TYPE;",
            ">;>;",
            "LX/7Fx",
            "<TPREFETCH_DATA_TYPE;TDE",
            "LTA_WRAPPER_TYPE;",
            ">;)",
            "LX/7GN;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    .line 1189103
    invoke-interface {p4}, LX/7Fx;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v10, :cond_0

    .line 1189104
    :try_start_0
    invoke-interface {p4, p2, p3}, LX/7Fx;->a(Ljava/lang/Object;Ljava/util/List;)LX/0P1;

    move-result-object v1

    .line 1189105
    new-instance v0, LX/7GN;

    .line 1189106
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1189107
    invoke-direct {v0, v1, v2}, LX/7GN;-><init>(LX/0P1;LX/0Px;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1189108
    :goto_0
    return-object v0

    .line 1189109
    :catch_0
    move-exception v0

    .line 1189110
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "db_handle_batch_fail"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1189111
    const-string v2, "error"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1189112
    iget-object v0, p0, LX/7GP;->e:LX/7G1;

    invoke-virtual {v0, v1, p1}, LX/7G1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7GT;)V

    .line 1189113
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1189114
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1189115
    invoke-interface {p3}, Ljava/util/List;->size()I

    .line 1189116
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7GJ;

    .line 1189117
    invoke-interface {p4}, LX/7Fx;->a()J

    move-result-wide v4

    .line 1189118
    iget-wide v6, v0, LX/7GJ;->b:J

    invoke-static {v6, v7, v4, v5}, LX/7Gg;->a(JJ)LX/7Gf;

    move-result-object v6

    .line 1189119
    sget-object v7, LX/7GM;->a:[I

    invoke-virtual {v6}, LX/7Gf;->ordinal()I

    move-result v6

    aget v6, v7, v6

    packed-switch v6, :pswitch_data_0

    goto :goto_1

    .line 1189120
    :pswitch_0
    invoke-interface {p4, p2, v0}, LX/7Fx;->a(Ljava/lang/Object;LX/7GJ;)Landroid/os/Bundle;

    move-result-object v4

    .line 1189121
    if-eqz v4, :cond_2

    .line 1189122
    iget-wide v6, v0, LX/7GJ;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1189123
    const-string v0, "updatedPrefetchData"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 1189124
    const-string v5, "updatedPrefetchData"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1189125
    if-eqz v0, :cond_1

    move-object p2, v0

    .line 1189126
    goto :goto_1

    .line 1189127
    :cond_2
    sget-object v4, LX/7GP;->a:Ljava/lang/Class;

    const-string v5, "dbResult is null for seqId %d , delta: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, v0, LX/7GJ;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    iget-object v0, v0, LX/7GJ;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v10

    invoke-static {v4, v5, v6}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1189128
    :pswitch_1
    new-instance v1, LX/7GZ;

    iget-wide v2, v0, LX/7GJ;->b:J

    invoke-direct {v1, v2, v3, v4, v5}, LX/7GZ;-><init>(JJ)V

    throw v1

    .line 1189129
    :pswitch_2
    iget-wide v4, v0, LX/7GJ;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1189130
    :cond_3
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    .line 1189131
    invoke-virtual {v1}, LX/0P1;->size()I

    invoke-interface {p4}, LX/7Fx;->a()J

    .line 1189132
    new-instance v0, LX/7GN;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7GN;-><init>(LX/0P1;LX/0Px;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/7GP;
    .locals 13

    .prologue
    .line 1189133
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1189134
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1189135
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1189136
    if-nez v1, :cond_0

    .line 1189137
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1189138
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1189139
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1189140
    sget-object v1, LX/7GP;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1189141
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1189142
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1189143
    :cond_1
    if-nez v1, :cond_4

    .line 1189144
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1189145
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1189146
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1189147
    new-instance v7, LX/7GP;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v0}, LX/7GL;->a(LX/0QB;)LX/7GL;

    move-result-object v9

    check-cast v9, LX/7GL;

    invoke-static {v0}, LX/7Gg;->a(LX/0QB;)LX/7Gg;

    move-result-object v10

    check-cast v10, LX/7Gg;

    invoke-static {v0}, LX/7G1;->a(LX/0QB;)LX/7G1;

    move-result-object v11

    check-cast v11, LX/7G1;

    invoke-static {v0}, LX/7G3;->a(LX/0QB;)LX/7G3;

    move-result-object v12

    check-cast v12, LX/7G3;

    invoke-direct/range {v7 .. v12}, LX/7GP;-><init>(LX/0SG;LX/7GL;LX/7Gg;LX/7G1;LX/7G3;)V

    .line 1189148
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1189149
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1189150
    if-nez v1, :cond_2

    .line 1189151
    sget-object v0, LX/7GP;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7GP;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1189152
    :goto_1
    if-eqz v0, :cond_3

    .line 1189153
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1189154
    :goto_3
    check-cast v0, LX/7GP;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1189155
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1189156
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1189157
    :catchall_1
    move-exception v0

    .line 1189158
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1189159
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1189160
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1189161
    :cond_2
    :try_start_8
    sget-object v0, LX/7GP;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7GP;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(LX/7GN;Ljava/util/List;LX/7GI;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PREFETCH_DATA_TYPE:",
            "Ljava/lang/Object;",
            "DE",
            "LTA_WRAPPER_TYPE:Ljava/lang/Object;",
            ">(",
            "LX/7GN;",
            "Ljava/util/List",
            "<",
            "LX/7GJ",
            "<TDE",
            "LTA_WRAPPER_TYPE;",
            ">;>;",
            "LX/7GI",
            "<TPREFETCH_DATA_TYPE;TDE",
            "LTA_WRAPPER_TYPE;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1189162
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7GJ;

    .line 1189163
    iget-object v1, p0, LX/7GN;->b:LX/0Px;

    iget-wide v4, v0, LX/7GJ;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1189164
    :cond_0
    return-void

    .line 1189165
    :cond_1
    iget-object v1, p0, LX/7GN;->a:LX/0P1;

    iget-wide v4, v0, LX/7GJ;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 1189166
    iget-object v3, v0, LX/7GJ;->a:Ljava/lang/Object;

    invoke-interface {p2, v3}, LX/7GI;->a(Ljava/lang/Object;)LX/7GG;

    move-result-object v3

    .line 1189167
    invoke-interface {v3, v1, v0}, LX/7GG;->a(Landroid/os/Bundle;LX/7GJ;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7GT;Ljava/util/List;JLX/7GK;LX/7GH;LX/7Fx;LX/7GI;Lcom/facebook/fbtrace/FbTraceNode;LX/7GO;)LX/7GN;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PREFETCH_DATA_TYPE::",
            "Landroid/os/Parcelable;",
            "DE",
            "LTA_WRAPPER_TYPE:Ljava/lang/Object;",
            ">(",
            "LX/7GT;",
            "Ljava/util/List",
            "<TDE",
            "LTA_WRAPPER_TYPE;",
            ">;J",
            "LX/7GK",
            "<TDE",
            "LTA_WRAPPER_TYPE;",
            ">;",
            "LX/7GH",
            "<TPREFETCH_DATA_TYPE;TDE",
            "LTA_WRAPPER_TYPE;",
            ">;",
            "LX/7Fx",
            "<TPREFETCH_DATA_TYPE;TDE",
            "LTA_WRAPPER_TYPE;",
            ">;",
            "LX/7GI",
            "<TPREFETCH_DATA_TYPE;TDE",
            "LTA_WRAPPER_TYPE;",
            ">;",
            "Lcom/facebook/fbtrace/FbTraceNode;",
            "LX/7GO;",
            ")",
            "LX/7GN;"
        }
    .end annotation

    .prologue
    .line 1189168
    iget-object v2, p0, LX/7GP;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v4

    .line 1189169
    move-object/from16 v0, p5

    move-object/from16 v1, p9

    invoke-static {p2, p3, p4, v0, v1}, LX/7GL;->a(Ljava/util/List;JLX/7GK;Lcom/facebook/fbtrace/FbTraceNode;)LX/0Px;

    move-result-object v3

    .line 1189170
    move-object/from16 v0, p6

    invoke-interface {v0, v3}, LX/7GH;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Parcelable;

    .line 1189171
    move-object/from16 v0, p7

    invoke-direct {p0, p1, v2, v3, v0}, LX/7GP;->a(LX/7GT;Landroid/os/Parcelable;Ljava/util/List;LX/7Fx;)LX/7GN;

    move-result-object v8

    .line 1189172
    :try_start_0
    move-object/from16 v0, p8

    invoke-static {v8, v3, v0}, LX/7GP;->a(LX/7GN;Ljava/util/List;LX/7GI;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1189173
    invoke-interface/range {p10 .. p10}, LX/7GO;->b()V

    .line 1189174
    iget-object v2, p0, LX/7GP;->e:LX/7G1;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    const/4 v7, 0x1

    move-object v3, p1

    invoke-virtual/range {v2 .. v7}, LX/7G1;->a(LX/7GT;JIZ)V

    move-object v2, v8

    .line 1189175
    :goto_0
    return-object v2

    .line 1189176
    :catch_0
    move-exception v9

    .line 1189177
    iget-object v2, p0, LX/7GP;->e:LX/7G1;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    const/4 v7, 0x0

    move-object v3, p1

    invoke-virtual/range {v2 .. v7}, LX/7G1;->a(LX/7GT;JIZ)V

    .line 1189178
    iget-object v2, p0, LX/7GP;->f:LX/7G3;

    invoke-virtual {v2, p1, v9}, LX/7G3;->b(LX/7GT;Ljava/lang/Exception;)V

    .line 1189179
    invoke-interface/range {p10 .. p10}, LX/7GO;->a()V

    move-object v2, v8

    .line 1189180
    goto :goto_0
.end method
