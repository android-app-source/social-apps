.class public LX/6wn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/6wp;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6wp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1158020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158021
    iput-object p1, p0, LX/6wn;->a:Landroid/content/Context;

    .line 1158022
    iput-object p2, p0, LX/6wn;->b:LX/6wp;

    .line 1158023
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1158019
    iget-object v0, p0, LX/6wn;->b:LX/6wp;

    invoke-virtual {v0}, LX/6wp;->a()I

    move-result v0

    return v0
.end method

.method public final a(LX/6z8;)Z
    .locals 2

    .prologue
    .line 1158025
    sget-object v0, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-interface {p1}, LX/6z8;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method public final b(LX/6z8;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1158024
    iget-object v0, p0, LX/6wn;->a:Landroid/content/Context;

    const v1, 0x7f081e2b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
