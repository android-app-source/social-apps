.class public final LX/7yV;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/base/photos/LocalPhoto;

.field public final synthetic b:LX/7ye;

.field public final synthetic c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;


# direct methods
.method public constructor <init>(Lcom/facebook/facerec/manager/FaceBoxPrioritizer;Lcom/facebook/photos/base/photos/LocalPhoto;LX/7ye;)V
    .locals 0

    .prologue
    .line 1279403
    iput-object p1, p0, LX/7yV;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iput-object p2, p0, LX/7yV;->a:Lcom/facebook/photos/base/photos/LocalPhoto;

    iput-object p3, p0, LX/7yV;->b:LX/7ye;

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1279404
    if-nez p1, :cond_0

    .line 1279405
    :goto_0
    return-void

    .line 1279406
    :cond_0
    iget-object v0, p0, LX/7yV;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v0, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->j:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1279407
    iget-object v0, p0, LX/7yV;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v0, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yc;

    const/4 v1, 0x0

    .line 1279408
    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, LX/7yc;->a(Landroid/graphics/Bitmap;IZ)Ljava/util/List;

    move-result-object v2

    move-object v0, v2

    .line 1279409
    iget-object v1, p0, LX/7yV;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v2, p0, LX/7yV;->b:LX/7ye;

    .line 1279410
    iget-object v3, v1, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->k:Ljava/util/concurrent/Executor;

    new-instance p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;

    invoke-direct {p0, v1, v2, v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;-><init>(Lcom/facebook/facerec/manager/FaceBoxPrioritizer;LX/7ye;Ljava/util/List;)V

    const p1, -0x248732ff

    invoke-static {v3, p0, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1279411
    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1279412
    iget-object v0, p0, LX/7yV;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v0, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->j:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1279413
    iget-object v0, p0, LX/7yV;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->b:Z

    .line 1279414
    iget-object v0, p0, LX/7yV;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v1, p0, LX/7yV;->b:LX/7ye;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a(LX/7ye;Z)V

    .line 1279415
    return-void
.end method
