.class public final LX/7us;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1273178
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1273179
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1273180
    :goto_0
    return v1

    .line 1273181
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1273182
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 1273183
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1273184
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1273185
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1273186
    const-string v7, "event_declines"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1273187
    invoke-static {p0, p1}, LX/7uo;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1273188
    :cond_2
    const-string v7, "event_invitees"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1273189
    invoke-static {p0, p1}, LX/7up;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1273190
    :cond_3
    const-string v7, "event_maybes"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1273191
    invoke-static {p0, p1}, LX/7uq;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1273192
    :cond_4
    const-string v7, "event_members"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1273193
    invoke-static {p0, p1}, LX/7ur;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1273194
    :cond_5
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1273195
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1273196
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1273197
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1273198
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1273199
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1273200
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1273201
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1273202
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1273203
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1273204
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1273205
    if-eqz v0, :cond_0

    .line 1273206
    const-string v1, "event_declines"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1273207
    invoke-static {p0, v0, p2}, LX/7uo;->a(LX/15i;ILX/0nX;)V

    .line 1273208
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1273209
    if-eqz v0, :cond_1

    .line 1273210
    const-string v1, "event_invitees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1273211
    invoke-static {p0, v0, p2}, LX/7up;->a(LX/15i;ILX/0nX;)V

    .line 1273212
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1273213
    if-eqz v0, :cond_2

    .line 1273214
    const-string v1, "event_maybes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1273215
    invoke-static {p0, v0, p2}, LX/7uq;->a(LX/15i;ILX/0nX;)V

    .line 1273216
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1273217
    if-eqz v0, :cond_3

    .line 1273218
    const-string v1, "event_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1273219
    invoke-static {p0, v0, p2}, LX/7ur;->a(LX/15i;ILX/0nX;)V

    .line 1273220
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1273221
    if-eqz v0, :cond_4

    .line 1273222
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1273223
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1273224
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1273225
    return-void
.end method
