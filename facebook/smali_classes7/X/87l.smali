.class public final LX/87l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:I

.field public final synthetic c:LX/87n;

.field public final synthetic d:LX/87o;


# direct methods
.method public constructor <init>(LX/87o;ZILX/87n;)V
    .locals 0

    .prologue
    .line 1300580
    iput-object p1, p0, LX/87l;->d:LX/87o;

    iput-boolean p2, p0, LX/87l;->a:Z

    iput p3, p0, LX/87l;->b:I

    iput-object p4, p0, LX/87l;->c:LX/87n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1300581
    iget-object v0, p0, LX/87l;->d:LX/87o;

    iget-object v0, v0, LX/87o;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/goodfriends/data/FriendStateFetcher$2$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/goodfriends/data/FriendStateFetcher$2$2;-><init>(LX/87l;Ljava/lang/Throwable;)V

    const v2, 0x1d7d6ab9

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1300582
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1300583
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1300584
    if-nez p1, :cond_0

    .line 1300585
    :goto_0
    return-void

    .line 1300586
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1300587
    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel;

    iget-boolean v1, p0, LX/87l;->a:Z

    iget v2, p0, LX/87l;->b:I

    invoke-static {v0, v1, v2}, LX/87o;->a(Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel;ZI)LX/0Px;

    move-result-object v1

    .line 1300588
    invoke-static {v1}, LX/87o;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    .line 1300589
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1300590
    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel;

    .line 1300591
    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel;->a()Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel;->a()LX/0Px;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel;

    invoke-virtual {v3}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel;->k()Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$MembersModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$MembersModel;->a()I

    move-result v3

    move v0, v3

    .line 1300592
    iget-object v3, p0, LX/87l;->d:LX/87o;

    iget-object v3, v3, LX/87o;->c:Ljava/util/concurrent/Executor;

    new-instance v4, Lcom/facebook/goodfriends/data/FriendStateFetcher$2$1;

    invoke-direct {v4, p0, v1, v0, v2}, Lcom/facebook/goodfriends/data/FriendStateFetcher$2$1;-><init>(LX/87l;LX/0Px;ILX/0Px;)V

    const v0, 0x54dabdf0

    invoke-static {v3, v4, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
