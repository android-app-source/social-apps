.class public LX/8cH;
.super LX/2TZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2TZ",
        "<",
        "LX/8cK;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 1374236
    invoke-direct {p0, p1}, LX/2TZ;-><init>(Landroid/database/Cursor;)V

    .line 1374237
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1374238
    const-wide/16 v6, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1374239
    invoke-static {}, LX/8cK;->q()LX/8cJ;

    move-result-object v0

    sget-object v3, LX/7Br;->b:LX/0U1;

    .line 1374240
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374241
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374242
    iput-object v3, v0, LX/8cJ;->c:Ljava/lang/String;

    .line 1374243
    move-object v0, v0

    .line 1374244
    sget-object v3, LX/7Br;->c:LX/0U1;

    .line 1374245
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374246
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374247
    iput-object v3, v0, LX/8cJ;->d:Ljava/lang/String;

    .line 1374248
    move-object v0, v0

    .line 1374249
    sget-object v3, LX/7Br;->d:LX/0U1;

    .line 1374250
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374251
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374252
    iput-object v3, v0, LX/8cJ;->e:Ljava/lang/String;

    .line 1374253
    move-object v0, v0

    .line 1374254
    sget-object v3, LX/7Br;->e:LX/0U1;

    .line 1374255
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374256
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374257
    iput-object v3, v0, LX/8cJ;->f:Ljava/lang/String;

    .line 1374258
    move-object v0, v0

    .line 1374259
    sget-object v3, LX/7Br;->f:LX/0U1;

    .line 1374260
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374261
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374262
    iput-object v3, v0, LX/8cJ;->g:Ljava/lang/String;

    .line 1374263
    move-object v0, v0

    .line 1374264
    sget-object v3, LX/7Br;->g:LX/0U1;

    .line 1374265
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374266
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374267
    iput-object v3, v0, LX/8cJ;->h:Ljava/lang/String;

    .line 1374268
    move-object v0, v0

    .line 1374269
    sget-object v3, LX/7Br;->h:LX/0U1;

    .line 1374270
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374271
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374272
    iput-object v3, v0, LX/8cJ;->i:Ljava/lang/String;

    .line 1374273
    move-object v0, v0

    .line 1374274
    sget-object v3, LX/7Br;->i:LX/0U1;

    .line 1374275
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374276
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    .line 1374277
    iput-object v3, v0, LX/8cJ;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1374278
    move-object v3, v0

    .line 1374279
    sget-object v0, LX/7Br;->j:LX/0U1;

    .line 1374280
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 1374281
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v0, v1

    .line 1374282
    :goto_0
    iput-boolean v0, v3, LX/8cJ;->l:Z

    .line 1374283
    move-object v0, v3

    .line 1374284
    sget-object v3, LX/7Br;->k:LX/0U1;

    .line 1374285
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374286
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    .line 1374287
    iput-object v3, v0, LX/8cJ;->m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1374288
    move-object v0, v0

    .line 1374289
    sget-object v3, LX/7Br;->l:LX/0U1;

    .line 1374290
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374291
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    .line 1374292
    iput-wide v4, v0, LX/8cJ;->j:D

    .line 1374293
    move-object v3, v0

    .line 1374294
    sget-object v0, LX/7Br;->m:LX/0U1;

    .line 1374295
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 1374296
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    move v0, v1

    .line 1374297
    :goto_1
    iput-boolean v0, v3, LX/8cJ;->a:Z

    .line 1374298
    move-object v0, v3

    .line 1374299
    sget-object v3, LX/7Br;->n:LX/0U1;

    .line 1374300
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374301
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v3

    .line 1374302
    iput-object v3, v0, LX/8cJ;->b:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1374303
    move-object v0, v0

    .line 1374304
    iput-boolean v1, v0, LX/8cJ;->n:Z

    .line 1374305
    move-object v0, v0

    .line 1374306
    sget-object v3, LX/7Br;->o:LX/0U1;

    .line 1374307
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374308
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374309
    iput-object v3, v0, LX/8cJ;->o:Ljava/lang/String;

    .line 1374310
    move-object v0, v0

    .line 1374311
    sget-object v3, LX/7Br;->p:LX/0U1;

    .line 1374312
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374313
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    .line 1374314
    :goto_2
    iput-boolean v1, v0, LX/8cJ;->p:Z

    .line 1374315
    move-object v0, v0

    .line 1374316
    invoke-virtual {v0}, LX/8cJ;->q()LX/8cK;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method
