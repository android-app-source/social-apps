.class public LX/7IE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public f:I

.field public g:I

.field public h:Ljava/lang/String;

.field public i:LX/03z;

.field public j:Ljava/lang/String;


# direct methods
.method public constructor <init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 1191531
    const-string v6, ""

    const-string v7, ""

    sget-object v9, LX/03z;->UNKNOWN:LX/03z;

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v9}, LX/7IE;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/03z;)V

    .line 1191532
    return-void
.end method

.method public constructor <init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/03z;)V
    .locals 0

    .prologue
    .line 1191533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1191534
    iput p1, p0, LX/7IE;->a:I

    .line 1191535
    iput p2, p0, LX/7IE;->f:I

    .line 1191536
    iput p3, p0, LX/7IE;->g:I

    .line 1191537
    iput-object p4, p0, LX/7IE;->b:Ljava/lang/String;

    .line 1191538
    iput-object p5, p0, LX/7IE;->c:Ljava/lang/String;

    .line 1191539
    iput-object p6, p0, LX/7IE;->d:Ljava/lang/String;

    .line 1191540
    iput-object p7, p0, LX/7IE;->h:Ljava/lang/String;

    .line 1191541
    iput-object p8, p0, LX/7IE;->e:Ljava/lang/String;

    .line 1191542
    iput-object p9, p0, LX/7IE;->i:LX/03z;

    .line 1191543
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1191544
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1191545
    iget-object v1, p0, LX/7IE;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1191546
    iget-object v1, p0, LX/7IE;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1191547
    :cond_0
    iget-object v1, p0, LX/7IE;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1191548
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/7IE;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1191549
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 1191550
    iput p1, p0, LX/7IE;->f:I

    .line 1191551
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1191552
    iput-object p1, p0, LX/7IE;->h:Ljava/lang/String;

    .line 1191553
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 1191554
    iput p1, p0, LX/7IE;->g:I

    .line 1191555
    return-void
.end method
