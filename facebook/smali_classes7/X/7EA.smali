.class public final LX/7EA;
.super LX/7Cy;
.source ""

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field public final l:Ljava/lang/String;

.field public m:Landroid/graphics/SurfaceTexture;

.field public final synthetic n:LX/7EB;


# direct methods
.method public constructor <init>(LX/7EB;Landroid/view/TextureView$SurfaceTextureListener;)V
    .locals 1

    .prologue
    .line 1184368
    iput-object p1, p0, LX/7EA;->n:LX/7EB;

    .line 1184369
    invoke-direct {p0, p1, p2}, LX/7Cy;-><init>(LX/2qW;Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 1184370
    const-class v0, LX/7EA;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7EA;->l:Ljava/lang/String;

    .line 1184371
    return-void
.end method


# virtual methods
.method public final b()Lcom/facebook/spherical/GlMediaRenderThread;
    .locals 13

    .prologue
    .line 1184372
    new-instance v0, Lcom/facebook/spherical/video/GlVideoRenderThread;

    iget-object v1, p0, LX/7EA;->n:LX/7EB;

    invoke-virtual {v1}, LX/7EB;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/7Cy;->d:Landroid/graphics/SurfaceTexture;

    iget-object v3, p0, LX/7Cy;->e:Ljava/lang/Runnable;

    iget-object v4, p0, LX/7Cy;->f:Ljava/lang/Runnable;

    iget v5, p0, LX/7Cy;->i:I

    iget v6, p0, LX/7Cy;->j:I

    new-instance v7, LX/7ED;

    iget-object v8, p0, LX/7EA;->n:LX/7EB;

    invoke-virtual {v8}, LX/7EB;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-direct {v7, v8}, LX/7ED;-><init>(Landroid/content/res/Resources;)V

    new-instance v8, LX/7E9;

    invoke-direct {v8, p0}, LX/7E9;-><init>(LX/7EA;)V

    iget-object v9, p0, LX/7EA;->n:LX/7EB;

    iget-object v9, v9, LX/7EB;->c:LX/0Ot;

    iget-object v10, p0, LX/7EA;->n:LX/7EB;

    iget-object v10, v10, LX/7EB;->e:LX/0SG;

    iget-object v11, p0, LX/7EA;->n:LX/7EB;

    iget-object v11, v11, LX/7EB;->f:LX/0wW;

    iget-object v12, p0, LX/7EA;->n:LX/7EB;

    iget-object v12, v12, LX/7EB;->g:LX/7Cz;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/spherical/video/GlVideoRenderThread;-><init>(Landroid/content/Context;Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;Ljava/lang/Runnable;IILX/7D0;LX/7E9;LX/0Ot;LX/0SG;LX/0wW;LX/7Cz;)V

    return-object v0
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 2

    .prologue
    .line 1184373
    iget-object v0, p0, LX/7EA;->m:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 1184374
    iget-object v0, p0, LX/7Cy;->b:Landroid/view/TextureView$SurfaceTextureListener;

    iget-object v1, p0, LX/7EA;->m:Landroid/graphics/SurfaceTexture;

    invoke-interface {v0, v1}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z

    .line 1184375
    const/4 v0, 0x0

    iput-object v0, p0, LX/7EA;->m:Landroid/graphics/SurfaceTexture;

    .line 1184376
    :cond_0
    invoke-super {p0, p1}, LX/7Cy;->onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z

    move-result v0

    return v0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    .prologue
    .line 1184377
    invoke-super {p0, p1, p2, p3}, LX/7Cy;->onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V

    .line 1184378
    iget-object v0, p0, LX/7Cy;->b:Landroid/view/TextureView$SurfaceTextureListener;

    iget-object v1, p0, LX/7EA;->m:Landroid/graphics/SurfaceTexture;

    invoke-interface {v0, v1, p2, p3}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V

    .line 1184379
    return-void
.end method
