.class public LX/7Px;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/7Po;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/7Po;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1203501
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method private a()LX/7Po;
    .locals 24

    .prologue
    .line 1203500
    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v3

    check-cast v3, LX/0kb;

    const/16 v4, 0x12f4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0Vw;->a(LX/0QB;)LX/0Vw;

    move-result-object v5

    check-cast v5, LX/0Vw;

    invoke-static/range {p0 .. p0}, LX/1Lk;->a(LX/0QB;)LX/1Ln;

    move-result-object v6

    check-cast v6, LX/1Ln;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v11

    check-cast v11, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static/range {p0 .. p0}, LX/7PM;->a(LX/0QB;)LX/7PM;

    move-result-object v12

    check-cast v12, LX/7PM;

    invoke-static/range {p0 .. p0}, LX/19X;->a(LX/0QB;)LX/19Z;

    move-result-object v13

    check-cast v13, LX/19Z;

    invoke-static/range {p0 .. p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v14

    check-cast v14, LX/0oz;

    invoke-static/range {p0 .. p0}, LX/1Ls;->a(LX/0QB;)LX/1Lt;

    move-result-object v15

    check-cast v15, LX/1Lt;

    const/16 v16, 0x135d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x12e4

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0wq;->a(LX/0QB;)LX/0wq;

    move-result-object v18

    check-cast v18, LX/0wq;

    invoke-static/range {p0 .. p0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v19

    check-cast v19, LX/0V8;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v20

    check-cast v20, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v21

    check-cast v21, LX/0WJ;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v22

    check-cast v22, LX/0lC;

    invoke-static/range {p0 .. p0}, LX/1Lu;->a(LX/0QB;)LX/1Lu;

    move-result-object v23

    check-cast v23, LX/1Lu;

    invoke-static/range {v2 .. v23}, LX/19Y;->a(LX/0So;LX/0kb;LX/0Ot;LX/0Vw;LX/1Ln;LX/0Sh;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;LX/0ad;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/7PM;LX/19Z;LX/0oz;LX/1Lt;LX/0Ot;LX/0Or;LX/0wq;LX/0V8;LX/0Zb;LX/0WJ;LX/0lC;LX/1Lu;)LX/7Po;

    move-result-object v2

    return-object v2
.end method

.method public static a(LX/0QB;)LX/7Po;
    .locals 3

    .prologue
    .line 1203502
    sget-object v0, LX/7Px;->a:LX/7Po;

    if-nez v0, :cond_1

    .line 1203503
    const-class v1, LX/7Px;

    monitor-enter v1

    .line 1203504
    :try_start_0
    sget-object v0, LX/7Px;->a:LX/7Po;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1203505
    if-eqz v2, :cond_0

    .line 1203506
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/7Px;->b(LX/0QB;)LX/7Po;

    move-result-object v0

    sput-object v0, LX/7Px;->a:LX/7Po;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1203507
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1203508
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1203509
    :cond_1
    sget-object v0, LX/7Px;->a:LX/7Po;

    return-object v0

    .line 1203510
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1203511
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/7Po;
    .locals 24

    .prologue
    .line 1203499
    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v3

    check-cast v3, LX/0kb;

    const/16 v4, 0x12f4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0Vw;->a(LX/0QB;)LX/0Vw;

    move-result-object v5

    check-cast v5, LX/0Vw;

    invoke-static/range {p0 .. p0}, LX/1Lk;->a(LX/0QB;)LX/1Ln;

    move-result-object v6

    check-cast v6, LX/1Ln;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v11

    check-cast v11, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static/range {p0 .. p0}, LX/7PM;->a(LX/0QB;)LX/7PM;

    move-result-object v12

    check-cast v12, LX/7PM;

    invoke-static/range {p0 .. p0}, LX/19X;->a(LX/0QB;)LX/19Z;

    move-result-object v13

    check-cast v13, LX/19Z;

    invoke-static/range {p0 .. p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v14

    check-cast v14, LX/0oz;

    invoke-static/range {p0 .. p0}, LX/1Ls;->a(LX/0QB;)LX/1Lt;

    move-result-object v15

    check-cast v15, LX/1Lt;

    const/16 v16, 0x135d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x12e4

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0wq;->a(LX/0QB;)LX/0wq;

    move-result-object v18

    check-cast v18, LX/0wq;

    invoke-static/range {p0 .. p0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v19

    check-cast v19, LX/0V8;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v20

    check-cast v20, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v21

    check-cast v21, LX/0WJ;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v22

    check-cast v22, LX/0lC;

    invoke-static/range {p0 .. p0}, LX/1Lu;->a(LX/0QB;)LX/1Lu;

    move-result-object v23

    check-cast v23, LX/1Lu;

    invoke-static/range {v2 .. v23}, LX/19Y;->a(LX/0So;LX/0kb;LX/0Ot;LX/0Vw;LX/1Ln;LX/0Sh;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;LX/0ad;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/7PM;LX/19Z;LX/0oz;LX/1Lt;LX/0Ot;LX/0Or;LX/0wq;LX/0V8;LX/0Zb;LX/0WJ;LX/0lC;LX/1Lu;)LX/7Po;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1203498
    invoke-direct {p0}, LX/7Px;->a()LX/7Po;

    move-result-object v0

    return-object v0
.end method
