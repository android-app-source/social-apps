.class public LX/7EB;
.super LX/2qW;
.source ""


# instance fields
.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/7Cz;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1184391
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/7EB;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1184392
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 1184386
    invoke-direct {p0, p1, p2, p3}, LX/2qW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1184387
    const-class v0, LX/7EB;

    invoke-static {v0, p0}, LX/7EB;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1184388
    new-instance v0, LX/7Cz;

    invoke-direct {v0}, LX/7Cz;-><init>()V

    iput-object v0, p0, LX/7EB;->g:LX/7Cz;

    .line 1184389
    iget-object v0, p0, LX/7EB;->d:LX/0W3;

    sget-wide v2, LX/0X5;->e:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    iput-boolean v0, p0, LX/7EB;->h:Z

    .line 1184390
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, LX/7EB;

    const/16 v1, 0x259

    invoke-static {v3, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v3}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v1

    check-cast v1, LX/0W3;

    invoke-static {v3}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {v3}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v3

    check-cast v3, LX/0wW;

    iput-object p0, p1, LX/7EB;->c:LX/0Ot;

    iput-object v1, p1, LX/7EB;->d:LX/0W3;

    iput-object v2, p1, LX/7EB;->e:LX/0SG;

    iput-object v3, p1, LX/7EB;->f:LX/0wW;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/TextureView$SurfaceTextureListener;)LX/7Cy;
    .locals 1

    .prologue
    .line 1184385
    new-instance v0, LX/7EA;

    invoke-direct {v0, p0, p1}, LX/7EA;-><init>(LX/7EB;Landroid/view/TextureView$SurfaceTextureListener;)V

    return-object v0
.end method

.method public getRenderThreadParams()LX/7Cz;
    .locals 1

    .prologue
    .line 1184384
    iget-object v0, p0, LX/7EB;->g:LX/7Cz;

    return-object v0
.end method

.method public setShouldUseFullScreenControl(Z)V
    .locals 2

    .prologue
    .line 1184380
    invoke-super {p0, p1}, LX/2qW;->setShouldUseFullScreenControl(Z)V

    .line 1184381
    iget-object v1, p0, LX/7EB;->g:LX/7Cz;

    iget-boolean v0, p0, LX/2qW;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/7EB;->h:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/7Cl;->VIDEO_NEW_FULL_SCREEN:LX/7Cl;

    :goto_0
    iput-object v0, v1, LX/7Cz;->c:LX/7Cl;

    .line 1184382
    return-void

    .line 1184383
    :cond_0
    sget-object v0, LX/7Cl;->VIDEO:LX/7Cl;

    goto :goto_0
.end method
