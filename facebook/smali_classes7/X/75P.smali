.class public final LX/75P;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private final c:LX/6Z0;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Set;LX/6Z0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "LX/6Z0;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1169361
    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 1169362
    iput-object p1, p0, LX/75P;->b:Landroid/content/Context;

    .line 1169363
    iput-object p2, p0, LX/75P;->d:Ljava/lang/String;

    .line 1169364
    iput-object p3, p0, LX/75P;->a:Ljava/util/Set;

    .line 1169365
    iput-object p4, p0, LX/75P;->c:LX/6Z0;

    .line 1169366
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1169367
    const/4 v1, 0x1

    .line 1169368
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "?"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move v0, v1

    .line 1169369
    :goto_0
    iget-object v3, p0, LX/75P;->a:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 1169370
    const-string v3, ",?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1169371
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1169372
    :cond_0
    const-string v0, "%s = ? AND %s IN (%s)"

    sget-object v3, LX/6Yy;->k:LX/0U1;

    .line 1169373
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1169374
    sget-object v4, LX/6Yy;->a:LX/0U1;

    .line 1169375
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1169376
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v3, v4, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1169377
    iget-object v0, p0, LX/75P;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    .line 1169378
    const/4 v0, 0x0

    iget-object v4, p0, LX/75P;->d:Ljava/lang/String;

    aput-object v4, v3, v0

    .line 1169379
    iget-object v0, p0, LX/75P;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 1169380
    add-int/lit8 v0, v1, 0x1

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    move v1, v0

    .line 1169381
    goto :goto_1

    .line 1169382
    :cond_1
    iget-object v0, p0, LX/75P;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, LX/75P;->c:LX/6Z0;

    iget-object v1, v1, LX/6Z0;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1169383
    const/4 v0, 0x0

    return-object v0
.end method
