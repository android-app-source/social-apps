.class public enum LX/6pF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6pF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6pF;

.field public static final enum CHANGE:LX/6pF;

.field public static final enum CREATE:LX/6pF;

.field public static final enum CREATE_OR_UPDATE_PROTECTION_STATUS:LX/6pF;

.field public static final enum CREATE_OR_VERIFY:LX/6pF;

.field public static final enum DELETE:LX/6pF;

.field public static final enum DELETE_WITH_PASSWORD:LX/6pF;

.field public static final enum RESET:LX/6pF;

.field public static final enum UPDATE_PROTECTION_STATUS:LX/6pF;

.field public static final enum VERIFY:LX/6pF;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1149217
    new-instance v0, LX/6pF;

    const-string v1, "CREATE"

    invoke-direct {v0, v1, v3}, LX/6pF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6pF;->CREATE:LX/6pF;

    .line 1149218
    new-instance v0, LX/6pF;

    const-string v1, "VERIFY"

    invoke-direct {v0, v1, v4}, LX/6pF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6pF;->VERIFY:LX/6pF;

    .line 1149219
    new-instance v0, LX/6pF;

    const-string v1, "CHANGE"

    invoke-direct {v0, v1, v5}, LX/6pF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6pF;->CHANGE:LX/6pF;

    .line 1149220
    new-instance v0, LX/6pF;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v6}, LX/6pF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6pF;->DELETE:LX/6pF;

    .line 1149221
    new-instance v0, LX/6pF;

    const-string v1, "DELETE_WITH_PASSWORD"

    invoke-direct {v0, v1, v7}, LX/6pF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6pF;->DELETE_WITH_PASSWORD:LX/6pF;

    .line 1149222
    new-instance v0, LX/6pF;

    const-string v1, "RESET"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6pF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6pF;->RESET:LX/6pF;

    .line 1149223
    new-instance v0, LX/6pF;

    const-string v1, "UPDATE_PROTECTION_STATUS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/6pF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6pF;->UPDATE_PROTECTION_STATUS:LX/6pF;

    .line 1149224
    new-instance v0, LX/6pG;

    const-string v1, "CREATE_OR_VERIFY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/6pG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6pF;->CREATE_OR_VERIFY:LX/6pF;

    .line 1149225
    new-instance v0, LX/6pH;

    const-string v1, "CREATE_OR_UPDATE_PROTECTION_STATUS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/6pH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6pF;->CREATE_OR_UPDATE_PROTECTION_STATUS:LX/6pF;

    .line 1149226
    const/16 v0, 0x9

    new-array v0, v0, [LX/6pF;

    sget-object v1, LX/6pF;->CREATE:LX/6pF;

    aput-object v1, v0, v3

    sget-object v1, LX/6pF;->VERIFY:LX/6pF;

    aput-object v1, v0, v4

    sget-object v1, LX/6pF;->CHANGE:LX/6pF;

    aput-object v1, v0, v5

    sget-object v1, LX/6pF;->DELETE:LX/6pF;

    aput-object v1, v0, v6

    sget-object v1, LX/6pF;->DELETE_WITH_PASSWORD:LX/6pF;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6pF;->RESET:LX/6pF;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6pF;->UPDATE_PROTECTION_STATUS:LX/6pF;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6pF;->CREATE_OR_VERIFY:LX/6pF;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6pF;->CREATE_OR_UPDATE_PROTECTION_STATUS:LX/6pF;

    aput-object v2, v0, v1

    sput-object v0, LX/6pF;->$VALUES:[LX/6pF;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1149227
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6pF;
    .locals 1

    .prologue
    .line 1149228
    const-class v0, LX/6pF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6pF;

    return-object v0
.end method

.method public static values()[LX/6pF;
    .locals 1

    .prologue
    .line 1149229
    sget-object v0, LX/6pF;->$VALUES:[LX/6pF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6pF;

    return-object v0
.end method


# virtual methods
.method public getAction(Z)LX/6pF;
    .locals 0

    .prologue
    .line 1149230
    return-object p0
.end method
