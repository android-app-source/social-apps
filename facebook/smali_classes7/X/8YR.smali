.class public LX/8YR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/8YR;


# instance fields
.field public final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1356190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1356191
    iput-object p1, p0, LX/8YR;->a:LX/0ad;

    .line 1356192
    return-void
.end method

.method public static a(LX/0QB;)LX/8YR;
    .locals 4

    .prologue
    .line 1356193
    sget-object v0, LX/8YR;->b:LX/8YR;

    if-nez v0, :cond_1

    .line 1356194
    const-class v1, LX/8YR;

    monitor-enter v1

    .line 1356195
    :try_start_0
    sget-object v0, LX/8YR;->b:LX/8YR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1356196
    if-eqz v2, :cond_0

    .line 1356197
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1356198
    new-instance p0, LX/8YR;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/8YR;-><init>(LX/0ad;)V

    .line 1356199
    move-object v0, p0

    .line 1356200
    sput-object v0, LX/8YR;->b:LX/8YR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1356201
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1356202
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1356203
    :cond_1
    sget-object v0, LX/8YR;->b:LX/8YR;

    return-object v0

    .line 1356204
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1356205
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
