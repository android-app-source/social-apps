.class public final LX/8My;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Mx;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Lcom/facebook/photos/upload/operation/UploadOperation;

.field public final synthetic c:Lcom/facebook/photos/upload/protocol/PhotoPublisher;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/protocol/PhotoPublisher;Ljava/util/List;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 0

    .prologue
    .line 1335511
    iput-object p1, p0, LX/8My;->c:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    iput-object p2, p0, LX/8My;->a:Ljava/util/List;

    iput-object p3, p0, LX/8My;->b:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/399;
    .locals 8

    .prologue
    .line 1335512
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1335513
    iget-object v0, p0, LX/8My;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1335514
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1335515
    :cond_0
    new-instance v0, LX/4Ht;

    invoke-direct {v0}, LX/4Ht;-><init>()V

    iget-object v2, p0, LX/8My;->b:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1335516
    iget-wide v6, v2, Lcom/facebook/photos/upload/operation/UploadOperation;->j:J

    move-wide v2, v6

    .line 1335517
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 1335518
    const-string v3, "actor_id"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335519
    move-object v0, v0

    .line 1335520
    iget-object v2, p0, LX/8My;->b:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1335521
    iget-wide v6, v2, Lcom/facebook/photos/upload/operation/UploadOperation;->j:J

    move-wide v2, v6

    .line 1335522
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 1335523
    const-string v3, "page_id"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335524
    move-object v0, v0

    .line 1335525
    const-string v2, "photo_ids"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1335526
    move-object v0, v0

    .line 1335527
    new-instance v1, LX/8Az;

    invoke-direct {v1}, LX/8Az;-><init>()V

    move-object v1, v1

    .line 1335528
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/8Az;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    return-object v0
.end method
