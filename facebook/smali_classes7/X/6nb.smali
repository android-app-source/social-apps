.class public final LX/6nb;
.super LX/1SG;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/1SG",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1147464
    invoke-direct {p0}, LX/1SG;-><init>()V

    .line 1147465
    iput-object p1, p0, LX/6nb;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1147466
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1147467
    invoke-virtual {p0}, LX/17D;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1147468
    :cond_0
    return-void

    .line 1147469
    :cond_1
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 1147470
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot block UI thread when waiting for service call."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final synthetic a()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 1147471
    invoke-virtual {p0}, LX/6nb;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1147472
    iget-object v0, p0, LX/6nb;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1147473
    invoke-virtual {p0}, LX/6nb;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 1147474
    invoke-direct {p0}, LX/6nb;->c()V

    .line 1147475
    invoke-super {p0}, LX/1SG;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 1147476
    invoke-direct {p0}, LX/6nb;->c()V

    .line 1147477
    invoke-super {p0, p1, p2, p3}, LX/1SG;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
