.class public final enum LX/7gc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gc;

.field public static final enum SHARESHEET:LX/7gc;


# instance fields
.field private final mProductName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1224604
    new-instance v0, LX/7gc;

    const-string v1, "SHARESHEET"

    const-string v2, "sharesheet"

    invoke-direct {v0, v1, v3, v2}, LX/7gc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gc;->SHARESHEET:LX/7gc;

    .line 1224605
    const/4 v0, 0x1

    new-array v0, v0, [LX/7gc;

    sget-object v1, LX/7gc;->SHARESHEET:LX/7gc;

    aput-object v1, v0, v3

    sput-object v0, LX/7gc;->$VALUES:[LX/7gc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1224606
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1224607
    iput-object p3, p0, LX/7gc;->mProductName:Ljava/lang/String;

    .line 1224608
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gc;
    .locals 1

    .prologue
    .line 1224609
    const-class v0, LX/7gc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gc;

    return-object v0
.end method

.method public static values()[LX/7gc;
    .locals 1

    .prologue
    .line 1224610
    sget-object v0, LX/7gc;->$VALUES:[LX/7gc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gc;

    return-object v0
.end method


# virtual methods
.method public final getProductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224611
    iget-object v0, p0, LX/7gc;->mProductName:Ljava/lang/String;

    return-object v0
.end method
