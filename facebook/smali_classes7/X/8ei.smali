.class public final LX/8ei;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1383446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v10, 0x0

    const/4 v2, 0x0

    .line 1383447
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1383448
    iget-object v1, p0, LX/8ei;->a:LX/0Px;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1383449
    iget-object v3, p0, LX/8ei;->b:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1383450
    iget-object v5, p0, LX/8ei;->c:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1383451
    iget-object v6, p0, LX/8ei;->d:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1383452
    iget-object v7, p0, LX/8ei;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1383453
    iget-object v8, p0, LX/8ei;->f:Ljava/lang/String;

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1383454
    const/4 v9, 0x6

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1383455
    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 1383456
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1383457
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1383458
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1383459
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1383460
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1383461
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1383462
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1383463
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1383464
    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1383465
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1383466
    new-instance v1, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    invoke-direct {v1, v0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;-><init>(LX/15i;)V

    .line 1383467
    return-object v1
.end method
