.class public final enum LX/7gS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gS;

.field public static final enum BADGING_NUMBER:LX/7gS;

.field public static final enum CURRENT_PRIVACY:LX/7gS;

.field public static final enum DEVICE_MANUFACTURER:LX/7gS;

.field public static final enum DEVICE_MODEL:LX/7gS;

.field public static final enum DIRECT_RECIPIENTS:LX/7gS;

.field public static final enum DIRECT_RECIPIENTS_RANKINGS:LX/7gS;

.field public static final enum DIRECT_RECIPIENTS_SIZE:LX/7gS;

.field public static final enum DIRECT_RECIPIENTS_SOURCE:LX/7gS;

.field public static final enum DIRECT_SESSION_ID:LX/7gS;

.field public static final enum GESTURE:LX/7gS;

.field public static final enum HAS_NEW_CONTENT:LX/7gS;

.field public static final enum INSPIRATION_GROUP_SESSION:LX/7gS;

.field public static final enum IS_REPLAY:LX/7gS;

.field public static final enum IS_REPLY:LX/7gS;

.field public static final enum MEDIA_CONTENT_ID:LX/7gS;

.field public static final enum MEDIA_TYPE:LX/7gS;

.field public static final enum NUMBER_OF_ROWS:LX/7gS;

.field public static final enum NUMBER_OF_UNSEEN_ROWS:LX/7gS;

.field public static final enum PREVIOUS_PRIVACY:LX/7gS;

.field public static final enum PROMPT_ID:LX/7gS;

.field public static final enum RANKED_AUDIENCE_TYPE:LX/7gS;

.field public static final enum REASON:LX/7gS;

.field public static final enum REPLY_COUNT:LX/7gS;

.field public static final enum ROW_INDEX:LX/7gS;

.field public static final enum SEARCH_AUDIENCE_TYPE:LX/7gS;

.field public static final enum SESSION_TIME:LX/7gS;

.field public static final enum SHARE_SHEET_ID:LX/7gS;

.field public static final enum SOURCE:LX/7gS;

.field public static final enum TARGET_ID:LX/7gS;

.field public static final enum THREAD_COUNT:LX/7gS;

.field public static final enum THREAD_ID:LX/7gS;

.field public static final enum THREAD_INDEX:LX/7gS;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1224327
    new-instance v0, LX/7gS;

    const-string v1, "SHARE_SHEET_ID"

    const-string v2, "share_sheet_id"

    invoke-direct {v0, v1, v4, v2}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->SHARE_SHEET_ID:LX/7gS;

    .line 1224328
    new-instance v0, LX/7gS;

    const-string v1, "SEARCH_AUDIENCE_TYPE"

    const-string v2, "search"

    invoke-direct {v0, v1, v5, v2}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->SEARCH_AUDIENCE_TYPE:LX/7gS;

    .line 1224329
    new-instance v0, LX/7gS;

    const-string v1, "RANKED_AUDIENCE_TYPE"

    const-string v2, "recommendation"

    invoke-direct {v0, v1, v6, v2}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->RANKED_AUDIENCE_TYPE:LX/7gS;

    .line 1224330
    new-instance v0, LX/7gS;

    const-string v1, "INSPIRATION_GROUP_SESSION"

    const-string v2, "inspiration_group_session"

    invoke-direct {v0, v1, v7, v2}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->INSPIRATION_GROUP_SESSION:LX/7gS;

    .line 1224331
    new-instance v0, LX/7gS;

    const-string v1, "PROMPT_ID"

    const-string v2, "prompt_id"

    invoke-direct {v0, v1, v8, v2}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->PROMPT_ID:LX/7gS;

    .line 1224332
    new-instance v0, LX/7gS;

    const-string v1, "MEDIA_TYPE"

    const/4 v2, 0x5

    const-string v3, "media_type"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->MEDIA_TYPE:LX/7gS;

    .line 1224333
    new-instance v0, LX/7gS;

    const-string v1, "MEDIA_CONTENT_ID"

    const/4 v2, 0x6

    const-string v3, "media_content_id"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->MEDIA_CONTENT_ID:LX/7gS;

    .line 1224334
    new-instance v0, LX/7gS;

    const-string v1, "DEVICE_MODEL"

    const/4 v2, 0x7

    const-string v3, "device_model"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->DEVICE_MODEL:LX/7gS;

    .line 1224335
    new-instance v0, LX/7gS;

    const-string v1, "DEVICE_MANUFACTURER"

    const/16 v2, 0x8

    const-string v3, "device_manufacturer"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->DEVICE_MANUFACTURER:LX/7gS;

    .line 1224336
    new-instance v0, LX/7gS;

    const-string v1, "DIRECT_RECIPIENTS"

    const/16 v2, 0x9

    const-string v3, "direct_recipients"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->DIRECT_RECIPIENTS:LX/7gS;

    .line 1224337
    new-instance v0, LX/7gS;

    const-string v1, "DIRECT_RECIPIENTS_SIZE"

    const/16 v2, 0xa

    const-string v3, "direct_recipients_size"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->DIRECT_RECIPIENTS_SIZE:LX/7gS;

    .line 1224338
    new-instance v0, LX/7gS;

    const-string v1, "DIRECT_RECIPIENTS_RANKINGS"

    const/16 v2, 0xb

    const-string v3, "direct_recipients_rankings"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->DIRECT_RECIPIENTS_RANKINGS:LX/7gS;

    .line 1224339
    new-instance v0, LX/7gS;

    const-string v1, "DIRECT_RECIPIENTS_SOURCE"

    const/16 v2, 0xc

    const-string v3, "direct_recipients_source"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->DIRECT_RECIPIENTS_SOURCE:LX/7gS;

    .line 1224340
    new-instance v0, LX/7gS;

    const-string v1, "PREVIOUS_PRIVACY"

    const/16 v2, 0xd

    const-string v3, "previous_privacy"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->PREVIOUS_PRIVACY:LX/7gS;

    .line 1224341
    new-instance v0, LX/7gS;

    const-string v1, "CURRENT_PRIVACY"

    const/16 v2, 0xe

    const-string v3, "current_privacy"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->CURRENT_PRIVACY:LX/7gS;

    .line 1224342
    new-instance v0, LX/7gS;

    const-string v1, "DIRECT_SESSION_ID"

    const/16 v2, 0xf

    const-string v3, "direct_session_id"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->DIRECT_SESSION_ID:LX/7gS;

    .line 1224343
    new-instance v0, LX/7gS;

    const-string v1, "NUMBER_OF_ROWS"

    const/16 v2, 0x10

    const-string v3, "number_rows"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->NUMBER_OF_ROWS:LX/7gS;

    .line 1224344
    new-instance v0, LX/7gS;

    const-string v1, "NUMBER_OF_UNSEEN_ROWS"

    const/16 v2, 0x11

    const-string v3, "number_new_rows"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->NUMBER_OF_UNSEEN_ROWS:LX/7gS;

    .line 1224345
    new-instance v0, LX/7gS;

    const-string v1, "BADGING_NUMBER"

    const/16 v2, 0x12

    const-string v3, "number_in_red_badging"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->BADGING_NUMBER:LX/7gS;

    .line 1224346
    new-instance v0, LX/7gS;

    const-string v1, "GESTURE"

    const/16 v2, 0x13

    const-string v3, "gesture"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->GESTURE:LX/7gS;

    .line 1224347
    new-instance v0, LX/7gS;

    const-string v1, "SOURCE"

    const/16 v2, 0x14

    const-string v3, "source"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->SOURCE:LX/7gS;

    .line 1224348
    new-instance v0, LX/7gS;

    const-string v1, "TARGET_ID"

    const/16 v2, 0x15

    const-string v3, "target_id"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->TARGET_ID:LX/7gS;

    .line 1224349
    new-instance v0, LX/7gS;

    const-string v1, "ROW_INDEX"

    const/16 v2, 0x16

    const-string v3, "row_index"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->ROW_INDEX:LX/7gS;

    .line 1224350
    new-instance v0, LX/7gS;

    const-string v1, "IS_REPLAY"

    const/16 v2, 0x17

    const-string v3, "is_replay"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->IS_REPLAY:LX/7gS;

    .line 1224351
    new-instance v0, LX/7gS;

    const-string v1, "SESSION_TIME"

    const/16 v2, 0x18

    const-string v3, "session_time"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->SESSION_TIME:LX/7gS;

    .line 1224352
    new-instance v0, LX/7gS;

    const-string v1, "REASON"

    const/16 v2, 0x19

    const-string v3, "reason"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->REASON:LX/7gS;

    .line 1224353
    new-instance v0, LX/7gS;

    const-string v1, "THREAD_ID"

    const/16 v2, 0x1a

    const-string v3, "thread_id"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->THREAD_ID:LX/7gS;

    .line 1224354
    new-instance v0, LX/7gS;

    const-string v1, "THREAD_INDEX"

    const/16 v2, 0x1b

    const-string v3, "thread_index"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->THREAD_INDEX:LX/7gS;

    .line 1224355
    new-instance v0, LX/7gS;

    const-string v1, "REPLY_COUNT"

    const/16 v2, 0x1c

    const-string v3, "reply_count"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->REPLY_COUNT:LX/7gS;

    .line 1224356
    new-instance v0, LX/7gS;

    const-string v1, "THREAD_COUNT"

    const/16 v2, 0x1d

    const-string v3, "thread_count"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->THREAD_COUNT:LX/7gS;

    .line 1224357
    new-instance v0, LX/7gS;

    const-string v1, "IS_REPLY"

    const/16 v2, 0x1e

    const-string v3, "is_replay"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->IS_REPLY:LX/7gS;

    .line 1224358
    new-instance v0, LX/7gS;

    const-string v1, "HAS_NEW_CONTENT"

    const/16 v2, 0x1f

    const-string v3, "has_new_content"

    invoke-direct {v0, v1, v2, v3}, LX/7gS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gS;->HAS_NEW_CONTENT:LX/7gS;

    .line 1224359
    const/16 v0, 0x20

    new-array v0, v0, [LX/7gS;

    sget-object v1, LX/7gS;->SHARE_SHEET_ID:LX/7gS;

    aput-object v1, v0, v4

    sget-object v1, LX/7gS;->SEARCH_AUDIENCE_TYPE:LX/7gS;

    aput-object v1, v0, v5

    sget-object v1, LX/7gS;->RANKED_AUDIENCE_TYPE:LX/7gS;

    aput-object v1, v0, v6

    sget-object v1, LX/7gS;->INSPIRATION_GROUP_SESSION:LX/7gS;

    aput-object v1, v0, v7

    sget-object v1, LX/7gS;->PROMPT_ID:LX/7gS;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7gS;->MEDIA_TYPE:LX/7gS;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7gS;->MEDIA_CONTENT_ID:LX/7gS;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7gS;->DEVICE_MODEL:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7gS;->DEVICE_MANUFACTURER:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7gS;->DIRECT_RECIPIENTS:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7gS;->DIRECT_RECIPIENTS_SIZE:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7gS;->DIRECT_RECIPIENTS_RANKINGS:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7gS;->DIRECT_RECIPIENTS_SOURCE:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/7gS;->PREVIOUS_PRIVACY:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/7gS;->CURRENT_PRIVACY:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/7gS;->DIRECT_SESSION_ID:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/7gS;->NUMBER_OF_ROWS:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/7gS;->NUMBER_OF_UNSEEN_ROWS:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/7gS;->BADGING_NUMBER:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/7gS;->GESTURE:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/7gS;->SOURCE:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/7gS;->TARGET_ID:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/7gS;->ROW_INDEX:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/7gS;->IS_REPLAY:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/7gS;->SESSION_TIME:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/7gS;->REASON:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/7gS;->THREAD_ID:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/7gS;->THREAD_INDEX:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/7gS;->REPLY_COUNT:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/7gS;->THREAD_COUNT:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/7gS;->IS_REPLY:LX/7gS;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/7gS;->HAS_NEW_CONTENT:LX/7gS;

    aput-object v2, v0, v1

    sput-object v0, LX/7gS;->$VALUES:[LX/7gS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1224324
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1224325
    iput-object p3, p0, LX/7gS;->mName:Ljava/lang/String;

    .line 1224326
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gS;
    .locals 1

    .prologue
    .line 1224323
    const-class v0, LX/7gS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gS;

    return-object v0
.end method

.method public static values()[LX/7gS;
    .locals 1

    .prologue
    .line 1224321
    sget-object v0, LX/7gS;->$VALUES:[LX/7gS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gS;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224322
    iget-object v0, p0, LX/7gS;->mName:Ljava/lang/String;

    return-object v0
.end method
