.class public final LX/8Zw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 1365999
    const/16 v16, 0x0

    .line 1366000
    const/4 v15, 0x0

    .line 1366001
    const/4 v14, 0x0

    .line 1366002
    const/4 v13, 0x0

    .line 1366003
    const/4 v12, 0x0

    .line 1366004
    const/4 v9, 0x0

    .line 1366005
    const-wide/16 v10, 0x0

    .line 1366006
    const/4 v8, 0x0

    .line 1366007
    const/4 v7, 0x0

    .line 1366008
    const/4 v6, 0x0

    .line 1366009
    const-wide/16 v4, 0x0

    .line 1366010
    const/4 v3, 0x0

    .line 1366011
    const/4 v2, 0x0

    .line 1366012
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_f

    .line 1366013
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1366014
    const/4 v2, 0x0

    .line 1366015
    :goto_0
    return v2

    .line 1366016
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_c

    .line 1366017
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1366018
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1366019
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1366020
    const-string v6, "border"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1366021
    invoke-static/range {p0 .. p1}, LX/8Zv;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 1366022
    :cond_1
    const-string v6, "capitalization"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1366023
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 1366024
    :cond_2
    const-string v6, "color"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1366025
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 1366026
    :cond_3
    const-string v6, "display"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1366027
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 1366028
    :cond_4
    const-string v6, "font"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1366029
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 1366030
    :cond_5
    const-string v6, "inner_padding_spacing"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1366031
    invoke-static/range {p0 .. p1}, LX/8ao;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 1366032
    :cond_6
    const-string v6, "line_height_scale"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1366033
    const/4 v2, 0x1

    .line 1366034
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 1366035
    :cond_7
    const-string v6, "text_alignment"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1366036
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 1366037
    :cond_8
    const-string v6, "text_background_color"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1366038
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1366039
    :cond_9
    const-string v6, "text_margin_spacing"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1366040
    invoke-static/range {p0 .. p1}, LX/8ao;->a(LX/15w;LX/186;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 1366041
    :cond_a
    const-string v6, "text_size_scale"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1366042
    const/4 v2, 0x1

    .line 1366043
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v10, v6

    goto/16 :goto_1

    .line 1366044
    :cond_b
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1366045
    :cond_c
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1366046
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1366047
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1366048
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1366049
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1366050
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1366051
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1366052
    if-eqz v3, :cond_d

    .line 1366053
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1366054
    :cond_d
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1366055
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1366056
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1366057
    if-eqz v8, :cond_e

    .line 1366058
    const/16 v3, 0xa

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v10

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1366059
    :cond_e
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_f
    move/from16 v17, v14

    move/from16 v18, v15

    move/from16 v19, v16

    move v14, v9

    move v15, v12

    move/from16 v16, v13

    move v12, v7

    move v13, v8

    move v9, v6

    move v8, v2

    move-wide/from16 v20, v4

    move-wide v4, v10

    move-wide/from16 v10, v20

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/4 v6, 0x7

    const/4 v3, 0x3

    const/4 v2, 0x1

    const-wide/16 v4, 0x0

    .line 1366060
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1366061
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366062
    if-eqz v0, :cond_0

    .line 1366063
    const-string v1, "border"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366064
    invoke-static {p0, v0, p2, p3}, LX/8Zv;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366065
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1366066
    if-eqz v0, :cond_1

    .line 1366067
    const-string v0, "capitalization"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366068
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366069
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1366070
    if-eqz v0, :cond_2

    .line 1366071
    const-string v1, "color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366072
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366073
    :cond_2
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1366074
    if-eqz v0, :cond_3

    .line 1366075
    const-string v0, "display"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366076
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366077
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1366078
    if-eqz v0, :cond_4

    .line 1366079
    const-string v1, "font"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366080
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366081
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366082
    if-eqz v0, :cond_5

    .line 1366083
    const-string v1, "inner_padding_spacing"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366084
    invoke-static {p0, v0, p2, p3}, LX/8ao;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366085
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1366086
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_6

    .line 1366087
    const-string v2, "line_height_scale"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366088
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1366089
    :cond_6
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1366090
    if-eqz v0, :cond_7

    .line 1366091
    const-string v0, "text_alignment"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366092
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366093
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1366094
    if-eqz v0, :cond_8

    .line 1366095
    const-string v1, "text_background_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366096
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1366097
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1366098
    if-eqz v0, :cond_9

    .line 1366099
    const-string v1, "text_margin_spacing"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366100
    invoke-static {p0, v0, p2, p3}, LX/8ao;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1366101
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1366102
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_a

    .line 1366103
    const-string v2, "text_size_scale"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1366104
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1366105
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1366106
    return-void
.end method
