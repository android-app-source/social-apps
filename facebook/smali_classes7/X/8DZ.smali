.class public LX/8DZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field public final c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field private g:Ljava/lang/String;

.field private h:Z

.field private final i:Z

.field private final j:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZZ)V
    .locals 0

    .prologue
    .line 1312767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1312768
    iput-object p1, p0, LX/8DZ;->a:Ljava/lang/String;

    .line 1312769
    iput-object p2, p0, LX/8DZ;->b:Ljava/lang/String;

    .line 1312770
    iput-boolean p3, p0, LX/8DZ;->c:Z

    .line 1312771
    iput-boolean p4, p0, LX/8DZ;->d:Z

    .line 1312772
    iput-boolean p5, p0, LX/8DZ;->e:Z

    .line 1312773
    iput-boolean p6, p0, LX/8DZ;->f:Z

    .line 1312774
    iput-object p7, p0, LX/8DZ;->g:Ljava/lang/String;

    .line 1312775
    iput-boolean p8, p0, LX/8DZ;->h:Z

    .line 1312776
    iput-boolean p9, p0, LX/8DZ;->i:Z

    .line 1312777
    iput-boolean p10, p0, LX/8DZ;->j:Z

    .line 1312778
    return-void
.end method

.method public static a(Landroid/content/ContentResolver;Landroid/net/Uri;)LX/8DZ;
    .locals 14

    .prologue
    .line 1312807
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1312808
    if-nez v11, :cond_0

    .line 1312809
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to fetch settings: null cursor."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1312810
    :cond_0
    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1312811
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to fetch settings: empty cursor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1312812
    :catchall_0
    move-exception v0

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1312813
    :cond_1
    :try_start_1
    sget-object v0, LX/8Da;->a:Ljava/lang/String;

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1312814
    const-string v1, "signature"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 1312815
    const-string v1, "is_managed"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 1312816
    sget-object v1, LX/8Da;->b:Ljava/lang/String;

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 1312817
    sget-object v1, LX/8Da;->c:Ljava/lang/String;

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 1312818
    sget-object v1, LX/8Da;->d:Ljava/lang/String;

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 1312819
    sget-object v1, LX/8Da;->e:Ljava/lang/String;

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 1312820
    sget-object v1, LX/8Da;->f:Ljava/lang/String;

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 1312821
    sget-object v1, LX/8Da;->g:Ljava/lang/String;

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 1312822
    sget-object v1, LX/8Da;->h:Ljava/lang/String;

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 1312823
    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1312824
    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1312825
    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_5

    const/4 v3, 0x1

    .line 1312826
    :goto_0
    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v4, 0x1

    .line 1312827
    :goto_1
    invoke-interface {v11, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_7

    const/4 v5, 0x1

    .line 1312828
    :goto_2
    invoke-interface {v11, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_8

    const/4 v6, 0x1

    .line 1312829
    :goto_3
    invoke-interface {v11, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1312830
    const/4 v8, 0x0

    .line 1312831
    if-ltz v9, :cond_2

    .line 1312832
    invoke-interface {v11, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_4
    move v8, v0

    .line 1312833
    :cond_2
    const/4 v9, 0x0

    .line 1312834
    if-ltz v12, :cond_3

    .line 1312835
    invoke-interface {v11, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_5
    move v9, v0

    .line 1312836
    :cond_3
    const/4 v10, 0x0

    .line 1312837
    if-ltz v12, :cond_4

    .line 1312838
    invoke-interface {v11, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_6
    move v10, v0

    .line 1312839
    :cond_4
    new-instance v0, LX/8DZ;

    invoke-direct/range {v0 .. v10}, LX/8DZ;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1312840
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    return-object v0

    .line 1312841
    :cond_5
    const/4 v3, 0x0

    goto :goto_0

    .line 1312842
    :cond_6
    const/4 v4, 0x0

    goto :goto_1

    .line 1312843
    :cond_7
    const/4 v5, 0x0

    goto :goto_2

    .line 1312844
    :cond_8
    const/4 v6, 0x0

    goto :goto_3

    .line 1312845
    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    .line 1312846
    :cond_a
    const/4 v0, 0x0

    goto :goto_5

    .line 1312847
    :cond_b
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public static a(Landroid/content/Context;)LX/8DZ;
    .locals 2

    .prologue
    .line 1312848
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 1312849
    invoke-static {v1}, LX/29J;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-static {v0, p0}, LX/8DZ;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)LX/8DZ;

    move-result-object p0

    move-object v0, p0

    .line 1312850
    return-object v0
.end method

.method public static a(Landroid/content/ContentResolver;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1312795
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1312796
    sget-object v4, LX/8Da;->f:Ljava/lang/String;

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1312797
    invoke-static {p1}, LX/29J;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1312798
    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v3, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 1312799
    if-eq v0, v1, :cond_1

    .line 1312800
    new-instance v1, LX/8DY;

    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Expected 1 row changed, actually "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/8DY;-><init>(ILjava/lang/String;)V

    throw v1

    :cond_0
    move v0, v2

    .line 1312801
    goto :goto_0

    .line 1312802
    :catch_0
    move-exception v0

    .line 1312803
    new-instance v2, LX/8DY;

    const-string v3, "Could not resolve content uri for firstparty settings"

    invoke-direct {v2, v1, v3, v0}, LX/8DY;-><init>(ILjava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 1312804
    :catch_1
    move-exception v0

    .line 1312805
    new-instance v1, LX/8DY;

    const-string v3, "Unexpected failure."

    invoke-direct {v1, v2, v3, v0}, LX/8DY;-><init>(ILjava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1312806
    :cond_1
    return-void
.end method

.method public static b(LX/8DZ;Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1312779
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1312780
    sget-object v4, LX/8Da;->b:Ljava/lang/String;

    iget-boolean v0, p0, LX/8DZ;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1312781
    sget-object v4, LX/8Da;->c:Ljava/lang/String;

    iget-boolean v0, p0, LX/8DZ;->e:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1312782
    sget-object v4, LX/8Da;->d:Ljava/lang/String;

    iget-boolean v0, p0, LX/8DZ;->f:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1312783
    iget-object v0, p0, LX/8DZ;->g:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 1312784
    sget-object v0, LX/8Da;->e:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1312785
    :goto_3
    sget-object v0, LX/8Da;->f:Ljava/lang/String;

    iget-boolean v4, p0, LX/8DZ;->h:Z

    if-eqz v4, :cond_4

    :goto_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1312786
    invoke-virtual {p1, p2, v3, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1312787
    if-gez v0, :cond_5

    .line 1312788
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to update settings"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 1312789
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1312790
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1312791
    goto :goto_2

    .line 1312792
    :cond_3
    sget-object v0, LX/8Da;->e:Ljava/lang/String;

    iget-object v4, p0, LX/8DZ;->g:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    move v1, v2

    .line 1312793
    goto :goto_4

    .line 1312794
    :cond_5
    return-void
.end method
