.class public LX/6kR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final messageMetadata:LX/6kn;

.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1136789
    new-instance v0, LX/1sv;

    const-string v1, "DeltaThreadName"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kR;->b:LX/1sv;

    .line 1136790
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kR;->c:LX/1sw;

    .line 1136791
    new-instance v0, LX/1sw;

    const-string v1, "name"

    const/16 v2, 0xb

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kR;->d:LX/1sw;

    .line 1136792
    sput-boolean v4, LX/6kR;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6kn;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1136785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1136786
    iput-object p1, p0, LX/6kR;->messageMetadata:LX/6kn;

    .line 1136787
    iput-object p2, p0, LX/6kR;->name:Ljava/lang/String;

    .line 1136788
    return-void
.end method

.method public static a(LX/6kR;)V
    .locals 4

    .prologue
    .line 1136782
    iget-object v0, p0, LX/6kR;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1136783
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kR;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1136784
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1136793
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1136794
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1136795
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1136796
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaThreadName"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1136797
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136798
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136799
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136800
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136801
    const-string v4, "messageMetadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136802
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136803
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136804
    iget-object v4, p0, LX/6kR;->messageMetadata:LX/6kn;

    if-nez v4, :cond_4

    .line 1136805
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136806
    :goto_3
    iget-object v4, p0, LX/6kR;->name:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1136807
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136808
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136809
    const-string v4, "name"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136810
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136811
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136812
    iget-object v0, p0, LX/6kR;->name:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 1136813
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136814
    :cond_0
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136815
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136816
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1136817
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1136818
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1136819
    :cond_3
    const-string v0, ""

    goto/16 :goto_2

    .line 1136820
    :cond_4
    iget-object v4, p0, LX/6kR;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1136821
    :cond_5
    iget-object v0, p0, LX/6kR;->name:Ljava/lang/String;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1136770
    invoke-static {p0}, LX/6kR;->a(LX/6kR;)V

    .line 1136771
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1136772
    iget-object v0, p0, LX/6kR;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1136773
    sget-object v0, LX/6kR;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136774
    iget-object v0, p0, LX/6kR;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1136775
    :cond_0
    iget-object v0, p0, LX/6kR;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1136776
    iget-object v0, p0, LX/6kR;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1136777
    sget-object v0, LX/6kR;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136778
    iget-object v0, p0, LX/6kR;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1136779
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1136780
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1136781
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1136748
    if-nez p1, :cond_1

    .line 1136749
    :cond_0
    :goto_0
    return v0

    .line 1136750
    :cond_1
    instance-of v1, p1, LX/6kR;

    if-eqz v1, :cond_0

    .line 1136751
    check-cast p1, LX/6kR;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1136752
    if-nez p1, :cond_3

    .line 1136753
    :cond_2
    :goto_1
    move v0, v2

    .line 1136754
    goto :goto_0

    .line 1136755
    :cond_3
    iget-object v0, p0, LX/6kR;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1136756
    :goto_2
    iget-object v3, p1, LX/6kR;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1136757
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1136758
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136759
    iget-object v0, p0, LX/6kR;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6kR;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1136760
    :cond_5
    iget-object v0, p0, LX/6kR;->name:Ljava/lang/String;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1136761
    :goto_4
    iget-object v3, p1, LX/6kR;->name:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1136762
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1136763
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136764
    iget-object v0, p0, LX/6kR;->name:Ljava/lang/String;

    iget-object v3, p1, LX/6kR;->name:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1136765
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1136766
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1136767
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1136768
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1136769
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1136747
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1136744
    sget-boolean v0, LX/6kR;->a:Z

    .line 1136745
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kR;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1136746
    return-object v0
.end method
