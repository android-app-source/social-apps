.class public final enum LX/7VB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7VB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7VB;

.field public static final enum DONT_HANDLE:LX/7VB;

.field public static final enum HANDLE_AFTER_REWRITE:LX/7VB;

.field public static final enum HANDLE_BEHIND_DIALOG:LX/7VB;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1214055
    new-instance v0, LX/7VB;

    const-string v1, "HANDLE_AFTER_REWRITE"

    invoke-direct {v0, v1, v2}, LX/7VB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7VB;->HANDLE_AFTER_REWRITE:LX/7VB;

    .line 1214056
    new-instance v0, LX/7VB;

    const-string v1, "HANDLE_BEHIND_DIALOG"

    invoke-direct {v0, v1, v3}, LX/7VB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7VB;->HANDLE_BEHIND_DIALOG:LX/7VB;

    .line 1214057
    new-instance v0, LX/7VB;

    const-string v1, "DONT_HANDLE"

    invoke-direct {v0, v1, v4}, LX/7VB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7VB;->DONT_HANDLE:LX/7VB;

    .line 1214058
    const/4 v0, 0x3

    new-array v0, v0, [LX/7VB;

    sget-object v1, LX/7VB;->HANDLE_AFTER_REWRITE:LX/7VB;

    aput-object v1, v0, v2

    sget-object v1, LX/7VB;->HANDLE_BEHIND_DIALOG:LX/7VB;

    aput-object v1, v0, v3

    sget-object v1, LX/7VB;->DONT_HANDLE:LX/7VB;

    aput-object v1, v0, v4

    sput-object v0, LX/7VB;->$VALUES:[LX/7VB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1214059
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7VB;
    .locals 1

    .prologue
    .line 1214054
    const-class v0, LX/7VB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7VB;

    return-object v0
.end method

.method public static values()[LX/7VB;
    .locals 1

    .prologue
    .line 1214053
    sget-object v0, LX/7VB;->$VALUES:[LX/7VB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7VB;

    return-object v0
.end method
