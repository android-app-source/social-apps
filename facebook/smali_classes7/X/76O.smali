.class public final LX/76O;
.super LX/76J;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/76J",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final b:LX/6cX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/6cX",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Xl;LX/0So;LX/1fU;LX/6cX;LX/2gV;LX/03V;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Xl;",
            "LX/0So;",
            "LX/1fU;",
            "LX/6cX",
            "<TT;>;",
            "Lcom/facebook/push/mqtt/service/MqttPushServiceClient;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1171001
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, LX/76J;-><init>(Ljava/lang/String;LX/0Xl;LX/0So;LX/1fU;LX/2gV;LX/03V;)V

    .line 1171002
    iput-object p5, p0, LX/76O;->b:LX/6cX;

    .line 1171003
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;[B)V
    .locals 1

    .prologue
    .line 1171006
    iget-object v0, p0, LX/76O;->b:LX/6cX;

    invoke-interface {v0, p2}, LX/6cX;->a([B)V

    .line 1171007
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1171005
    iget-object v0, p0, LX/76O;->b:LX/6cX;

    invoke-interface {v0}, LX/6cX;->a()Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 1171004
    iget-object v0, p0, LX/76O;->b:LX/6cX;

    invoke-interface {v0}, LX/6cX;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
