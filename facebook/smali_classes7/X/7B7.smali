.class public final LX/7B7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1177907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1177908
    const/4 v0, 0x3

    new-array v0, v0, [Z

    .line 1177909
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1177910
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 1177911
    new-instance v2, LX/7B8;

    invoke-direct {v2}, LX/7B8;-><init>()V

    const/4 v3, 0x0

    aget-boolean v3, v0, v3

    .line 1177912
    iput-boolean v3, v2, LX/7B8;->a:Z

    .line 1177913
    move-object v2, v2

    .line 1177914
    const/4 v3, 0x1

    aget-boolean v3, v0, v3

    .line 1177915
    iput-boolean v3, v2, LX/7B8;->b:Z

    .line 1177916
    move-object v2, v2

    .line 1177917
    const/4 v3, 0x2

    aget-boolean v0, v0, v3

    .line 1177918
    iput-boolean v0, v2, LX/7B8;->c:Z

    .line 1177919
    move-object v0, v2

    .line 1177920
    iput-object v1, v0, LX/7B8;->d:Ljava/lang/String;

    .line 1177921
    move-object v0, v0

    .line 1177922
    invoke-virtual {v0}, LX/7B8;->a()Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;

    move-result-object v0

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1177923
    new-array v0, p1, [Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;

    return-object v0
.end method
