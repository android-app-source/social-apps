.class public final LX/87r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9As;

.field public final synthetic b:LX/1WV;


# direct methods
.method public constructor <init>(LX/1WV;LX/9As;)V
    .locals 0

    .prologue
    .line 1300718
    iput-object p1, p0, LX/87r;->b:LX/1WV;

    iput-object p2, p0, LX/87r;->a:LX/9As;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1300665
    sget-object v1, LX/1WU;->a:Ljava/lang/String;

    const-string p0, "Failed to retrieve reactors\' data"

    invoke-static {v1, p0, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1300666
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1300667
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 1300668
    if-nez p1, :cond_0

    .line 1300669
    :goto_0
    return-void

    .line 1300670
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1300671
    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;

    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->j()Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;->a()LX/0Px;

    move-result-object v3

    .line 1300672
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1300673
    if-eqz v3, :cond_1

    .line 1300674
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;

    .line 1300675
    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->j()Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    invoke-virtual {v7, v6, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    .line 1300676
    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v7, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v7, v0, v2}, LX/15i;->j(II)I

    move-result v0

    .line 1300677
    new-instance v7, LX/87x;

    invoke-direct {v7, v0, v6}, LX/87x;-><init>(ILjava/lang/String;)V

    invoke-virtual {v4, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1300678
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1300679
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1300680
    iget-object v1, p0, LX/87r;->a:LX/9As;

    .line 1300681
    iget-object v2, v1, LX/9As;->a:Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;

    const/4 v5, 0x0

    const/16 v7, 0x8

    .line 1300682
    iget-object v3, v2, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->i:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v3}, Lcom/facebook/widget/CustomLinearLayout;->removeAllViewsInLayout()V

    .line 1300683
    iget v3, v2, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->c:I

    if-le v3, v7, :cond_5

    .line 1300684
    iget v3, v2, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->c:I

    add-int/lit8 v3, v3, -0x8

    iput v3, v2, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->d:I

    .line 1300685
    const/4 v3, 0x1

    move v4, v3

    .line 1300686
    :goto_2
    iget v3, v2, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->c:I

    if-gt v3, v7, :cond_2

    iget v3, v2, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->c:I

    move v6, v3

    .line 1300687
    :goto_3
    if-ge v5, v6, :cond_3

    .line 1300688
    new-instance v8, Lcom/facebook/goodfriends/ui/GoodFriendsReactorView;

    iget-object v3, v2, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->e:Landroid/content/Context;

    invoke-direct {v8, v3}, Lcom/facebook/goodfriends/ui/GoodFriendsReactorView;-><init>(Landroid/content/Context;)V

    .line 1300689
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/87x;

    .line 1300690
    iget-object p0, v3, LX/87x;->b:Ljava/lang/String;

    move-object v3, p0

    .line 1300691
    iget-object p0, v8, Lcom/facebook/goodfriends/ui/GoodFriendsReactorView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    sget-object v1, Lcom/facebook/goodfriends/ui/GoodFriendsReactorView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1300692
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/87x;

    .line 1300693
    iget p0, v3, LX/87x;->a:I

    move v3, p0

    .line 1300694
    const/4 p0, 0x0

    .line 1300695
    packed-switch v3, :pswitch_data_0

    .line 1300696
    :goto_4
    :pswitch_0
    move v3, p0

    .line 1300697
    iget-object p0, v8, Lcom/facebook/goodfriends/ui/GoodFriendsReactorView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 1300698
    iget-object v3, v2, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->i:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v3, v8}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    .line 1300699
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_2
    move v6, v7

    .line 1300700
    goto :goto_3

    .line 1300701
    :cond_3
    if-eqz v4, :cond_4

    .line 1300702
    iget-object v4, v2, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/87x;

    .line 1300703
    iget-object v5, v3, LX/87x;->b:Ljava/lang/String;

    move-object v3, v5

    .line 1300704
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v5, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v3, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1300705
    iget-object v3, v2, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->g:Lcom/facebook/resources/ui/FbTextView;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v2, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->d:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1300706
    :goto_5
    goto/16 :goto_0

    .line 1300707
    :cond_4
    iget-object v3, v2, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1300708
    iget-object v3, v2, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_5

    :cond_5
    move v4, v5

    goto :goto_2

    .line 1300709
    :pswitch_1
    const p0, 0x7f0215f2

    goto :goto_4

    .line 1300710
    :pswitch_2
    const p0, 0x7f0215f4

    goto :goto_4

    .line 1300711
    :pswitch_3
    const p0, 0x7f0215fe

    goto :goto_4

    .line 1300712
    :pswitch_4
    const p0, 0x7f0215ee

    goto :goto_4

    .line 1300713
    :pswitch_5
    const p0, 0x7f021600

    goto :goto_4

    .line 1300714
    :pswitch_6
    const p0, 0x7f0215f9

    goto :goto_4

    .line 1300715
    :pswitch_7
    const p0, 0x7f0215e5

    goto :goto_4

    .line 1300716
    :pswitch_8
    const p0, 0x7f0215e7

    goto :goto_4

    .line 1300717
    :pswitch_9
    const p0, 0x7f0215fc

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
