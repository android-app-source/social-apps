.class public final enum LX/8Hl;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Hl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Hl;

.field public static final enum COLLAPSE:LX/8Hl;

.field public static final enum EXPAND:LX/8Hl;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1321526
    new-instance v0, LX/8Hl;

    const-string v1, "EXPAND"

    invoke-direct {v0, v1, v2}, LX/8Hl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Hl;->EXPAND:LX/8Hl;

    .line 1321527
    new-instance v0, LX/8Hl;

    const-string v1, "COLLAPSE"

    invoke-direct {v0, v1, v3}, LX/8Hl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Hl;->COLLAPSE:LX/8Hl;

    .line 1321528
    const/4 v0, 0x2

    new-array v0, v0, [LX/8Hl;

    sget-object v1, LX/8Hl;->EXPAND:LX/8Hl;

    aput-object v1, v0, v2

    sget-object v1, LX/8Hl;->COLLAPSE:LX/8Hl;

    aput-object v1, v0, v3

    sput-object v0, LX/8Hl;->$VALUES:[LX/8Hl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1321529
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Hl;
    .locals 1

    .prologue
    .line 1321530
    const-class v0, LX/8Hl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Hl;

    return-object v0
.end method

.method public static values()[LX/8Hl;
    .locals 1

    .prologue
    .line 1321531
    sget-object v0, LX/8Hl;->$VALUES:[LX/8Hl;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Hl;

    return-object v0
.end method
