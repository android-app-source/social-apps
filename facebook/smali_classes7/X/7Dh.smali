.class public LX/7Dh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final d:I

.field private static volatile g:LX/7Dh;


# instance fields
.field public final e:LX/0ad;

.field public final f:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1183508
    const/16 v0, 0x4c7

    sput v0, LX/7Dh;->a:I

    .line 1183509
    const/16 v0, 0x4c8

    sput v0, LX/7Dh;->b:I

    .line 1183510
    const/16 v0, 0x52b

    sput v0, LX/7Dh;->c:I

    .line 1183511
    const/16 v0, 0x4c9

    sput v0, LX/7Dh;->d:I

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1183512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1183513
    iput-object p1, p0, LX/7Dh;->e:LX/0ad;

    .line 1183514
    iput-object p2, p0, LX/7Dh;->f:LX/0Uh;

    .line 1183515
    return-void
.end method

.method public static a(LX/0QB;)LX/7Dh;
    .locals 5

    .prologue
    .line 1183516
    sget-object v0, LX/7Dh;->g:LX/7Dh;

    if-nez v0, :cond_1

    .line 1183517
    const-class v1, LX/7Dh;

    monitor-enter v1

    .line 1183518
    :try_start_0
    sget-object v0, LX/7Dh;->g:LX/7Dh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1183519
    if-eqz v2, :cond_0

    .line 1183520
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1183521
    new-instance p0, LX/7Dh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/7Dh;-><init>(LX/0ad;LX/0Uh;)V

    .line 1183522
    move-object v0, p0

    .line 1183523
    sput-object v0, LX/7Dh;->g:LX/7Dh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1183524
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1183525
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1183526
    :cond_1
    sget-object v0, LX/7Dh;->g:LX/7Dh;

    return-object v0

    .line 1183527
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1183528
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 1183529
    iget-object v0, p0, LX/7Dh;->f:LX/0Uh;

    sget v1, LX/7Dh;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 1183530
    iget-object v0, p0, LX/7Dh;->f:LX/0Uh;

    sget v1, LX/7Dh;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1183531
    iget-object v1, p0, LX/7Dh;->f:LX/0Uh;

    sget v2, LX/7Dh;->c:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/7Dh;->e:LX/0ad;

    sget-short v2, LX/7Dg;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1183532
    iget-object v1, p0, LX/7Dh;->f:LX/0Uh;

    sget v2, LX/7Dh;->d:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/7Dh;->e:LX/0ad;

    sget-short v2, LX/7Dg;->c:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method
