.class public LX/8NH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1336503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1336504
    iput-object p1, p0, LX/8NH;->a:LX/0SG;

    .line 1336505
    return-void
.end method

.method public static a(LX/0QB;)LX/8NH;
    .locals 1

    .prologue
    .line 1336506
    invoke-static {p0}, LX/8NH;->b(LX/0QB;)LX/8NH;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/io/File;
    .locals 2

    .prologue
    .line 1336507
    invoke-virtual {p0}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->b()Ljava/lang/String;

    move-result-object v0

    .line 1336508
    invoke-static {p0}, LX/8NH;->c(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1336509
    invoke-virtual {p0}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->d()Ljava/lang/String;

    move-result-object v0

    .line 1336510
    :cond_0
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1336511
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "UploadPhotoMethod: file not specified"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1336512
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method private b(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)LX/14N;
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 1336513
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "published"

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v3

    .line 1336514
    iget-boolean v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->k:Z

    move v0, v0

    .line 1336515
    if-eqz v0, :cond_0

    .line 1336516
    iget-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->j:Ljava/lang/String;

    move-object v0, v0

    .line 1336517
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1336518
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "caption"

    invoke-direct {v1, v4, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336519
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->f()Ljava/lang/String;

    move-result-object v0

    .line 1336520
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1336521
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "profile_id"

    invoke-direct {v1, v4, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336522
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->h()Ljava/lang/String;

    move-result-object v0

    .line 1336523
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1336524
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "place"

    invoke-direct {v1, v4, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336525
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1336526
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "allow_spherical_photo"

    const-string v4, "true"

    invoke-direct {v0, v1, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336527
    :cond_3
    iget-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->h:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-object v0, v0

    .line 1336528
    if-eqz v0, :cond_4

    .line 1336529
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "spherical_metadata"

    invoke-virtual {v0}, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->serializeToJson()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336530
    :cond_4
    iget-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1336531
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1336532
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "text_only_place"

    invoke-direct {v1, v4, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336533
    :cond_5
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "checkin_entry_point"

    .line 1336534
    iget-boolean v4, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->f:Z

    move v4, v4

    .line 1336535
    invoke-static {v4}, LX/5RB;->a(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336536
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "is_explicit_location"

    .line 1336537
    iget-boolean v4, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->x:Z

    move v4, v4

    .line 1336538
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336539
    iget-boolean v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->u:Z

    move v0, v0

    .line 1336540
    if-eqz v0, :cond_15

    .line 1336541
    iget-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->m:LX/0Px;

    move-object v4, v0

    .line 1336542
    iget-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->l:LX/0Px;

    move-object v5, v0

    .line 1336543
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 1336544
    if-eqz v4, :cond_6

    .line 1336545
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v7

    move v1, v2

    :goto_0
    if-ge v1, v7, :cond_6

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1336546
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1336547
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1336548
    :cond_6
    if-eqz v5, :cond_7

    .line 1336549
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_7

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1336550
    new-instance v7, LX/75C;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, LX/75C;-><init>(J)V

    .line 1336551
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1336552
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1336553
    :cond_7
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1336554
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "tags"

    invoke-static {v6}, LX/759;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336555
    :cond_8
    :goto_2
    iget-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->n:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-object v0, v0

    .line 1336556
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1336557
    iget-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->o:Ljava/lang/String;

    move-object v0, v0

    .line 1336558
    if-eqz v0, :cond_9

    .line 1336559
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "referenced_sticker_id"

    .line 1336560
    iget-object v4, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->o:Ljava/lang/String;

    move-object v4, v4

    .line 1336561
    invoke-direct {v0, v1, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336562
    :cond_9
    iget-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-object v0, v0

    .line 1336563
    iget-object v1, v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->e:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1336564
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "privacy"

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->e:Ljava/lang/String;

    invoke-direct {v1, v4, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336565
    :cond_a
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "audience_exp"

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336566
    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->H()J

    move-result-wide v0

    .line 1336567
    cmp-long v4, v0, v12

    if-eqz v4, :cond_b

    .line 1336568
    iget-object v4, p0, LX/8NH;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 1336569
    sub-long v0, v4, v0

    invoke-static {v0, v1, v12, v13}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1336570
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "time_since_original_post"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336571
    :cond_b
    iget-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p:Ljava/lang/String;

    move-object v0, v0

    .line 1336572
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 1336573
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "qn"

    invoke-direct {v1, v4, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336574
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "composer_session_id"

    invoke-direct {v1, v4, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336575
    :cond_c
    iget-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->q:Ljava/lang/String;

    move-object v0, v0

    .line 1336576
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 1336577
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "idempotence_token"

    invoke-direct {v1, v4, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336578
    :cond_d
    iget v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->r:I

    move v0, v0

    .line 1336579
    if-eqz v0, :cond_e

    .line 1336580
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "orientation"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336581
    :cond_e
    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p()Z

    move-result v1

    .line 1336582
    invoke-static {p1}, LX/8NH;->c(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v4

    .line 1336583
    const/4 v0, 0x0

    .line 1336584
    if-eqz v1, :cond_16

    .line 1336585
    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->q()Ljava/lang/String;

    move-result-object v1

    .line 1336586
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "vault_image_id"

    invoke-direct {v0, v5, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336587
    new-instance v0, LX/4cQ;

    const-string v5, "vault_image_id"

    new-instance v6, LX/4cY;

    sget-object v7, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v6, v1, v7}, LX/4cY;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v5, v6}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    move-object v1, v0

    .line 1336588
    :goto_3
    if-eqz v4, :cond_19

    .line 1336589
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "is_full_res"

    const-string v6, "true"

    invoke-direct {v0, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336590
    :cond_f
    :goto_4
    const-string v0, "me/photos"

    .line 1336591
    if-eqz v4, :cond_1a

    .line 1336592
    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->u()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 1336593
    :cond_10
    :goto_5
    iget-object v2, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->v:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object v2, v2

    .line 1336594
    if-eqz v2, :cond_11

    .line 1336595
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "proxied_app_id"

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336596
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "proxied_app_name"

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336597
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "android_key_hash"

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336598
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "user_selected_tags"

    .line 1336599
    iget-boolean v6, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->w:Z

    move v6, v6

    .line 1336600
    invoke-static {v6}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336601
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "user_selected_place"

    .line 1336602
    iget-boolean v6, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->x:Z

    move v6, v6

    .line 1336603
    invoke-static {v6}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336604
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "attribution_app_id"

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336605
    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_11

    .line 1336606
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "attribution_app_metadata"

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v5, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336607
    :cond_11
    iget-object v2, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Q:Ljava/lang/String;

    move-object v2, v2

    .line 1336608
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_12

    .line 1336609
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "source_type"

    invoke-direct {v4, v5, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336610
    :cond_12
    iget-boolean v2, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Y:Z

    move v2, v2

    .line 1336611
    if-eqz v2, :cond_13

    .line 1336612
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "post_surfaces_blacklist"

    sget-object v5, LX/7mB;->a:LX/0Px;

    invoke-static {v5}, LX/16N;->b(Ljava/util/List;)LX/162;

    move-result-object v5

    invoke-virtual {v5}, LX/162;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336613
    :cond_13
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v4, "upload-photo"

    .line 1336614
    iput-object v4, v2, LX/14O;->b:Ljava/lang/String;

    .line 1336615
    move-object v2, v2

    .line 1336616
    const-string v4, "POST"

    .line 1336617
    iput-object v4, v2, LX/14O;->c:Ljava/lang/String;

    .line 1336618
    move-object v2, v2

    .line 1336619
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 1336620
    move-object v0, v2

    .line 1336621
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1336622
    iput-object v2, v0, LX/14O;->k:LX/14S;

    .line 1336623
    move-object v0, v0

    .line 1336624
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1336625
    iput-object v2, v0, LX/14O;->g:Ljava/util/List;

    .line 1336626
    move-object v0, v0

    .line 1336627
    if-eqz v1, :cond_14

    .line 1336628
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1336629
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 1336630
    :cond_14
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1336631
    :cond_15
    iget-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->m:LX/0Px;

    move-object v0, v0

    .line 1336632
    if-eqz v0, :cond_8

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    .line 1336633
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "tags"

    invoke-static {v0}, LX/759;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_2

    .line 1336634
    :cond_16
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->X:Ljava/lang/String;

    move-object v1, v1

    .line 1336635
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 1336636
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "sync_object_uuid"

    .line 1336637
    iget-object v5, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->X:Ljava/lang/String;

    move-object v5, v5

    .line 1336638
    invoke-direct {v0, v1, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336639
    new-instance v0, LX/4cQ;

    const-string v1, "sync_object_uuid"

    new-instance v5, LX/4cY;

    .line 1336640
    iget-object v6, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->X:Ljava/lang/String;

    move-object v6, v6

    .line 1336641
    sget-object v7, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v5, v6, v7}, LX/4cY;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v1, v5}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    move-object v1, v0

    goto/16 :goto_3

    .line 1336642
    :cond_17
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->K:Ljava/lang/String;

    move-object v1, v1

    .line 1336643
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_18

    .line 1336644
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "fbuploader_source"

    .line 1336645
    iget-object v6, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->K:Ljava/lang/String;

    move-object v6, v6

    .line 1336646
    invoke-direct {v1, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-object v1, v0

    goto/16 :goto_3

    .line 1336647
    :cond_18
    invoke-static {p1}, LX/8NH;->a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/io/File;

    move-result-object v0

    .line 1336648
    new-instance v1, LX/4ct;

    const-string v5, "image/jpeg"

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v0, v5, v6}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 1336649
    new-instance v0, LX/4cQ;

    const-string v5, "source"

    invoke-direct {v0, v5, v1}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    move-object v1, v0

    goto/16 :goto_3

    .line 1336650
    :cond_19
    iget-boolean v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->B:Z

    move v0, v0

    .line 1336651
    if-eqz v0, :cond_f

    .line 1336652
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "full_res_is_coming_later"

    const-string v6, "true"

    invoke-direct {v0, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_4

    .line 1336653
    :cond_1a
    iget-object v4, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v4, v4

    .line 1336654
    if-eqz v4, :cond_10

    .line 1336655
    iget-boolean v5, v4, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v5, v5

    .line 1336656
    if-eqz v5, :cond_10

    .line 1336657
    const-string v0, "%s/photos"

    new-array v5, v10, [Ljava/lang/Object;

    .line 1336658
    iget-object v6, v4, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v4, v6

    .line 1336659
    aput-object v4, v5, v2

    invoke-static {v0, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1336660
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "temporary"

    invoke-static {v10}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_5
.end method

.method public static b(LX/0QB;)LX/8NH;
    .locals 2

    .prologue
    .line 1336661
    new-instance v1, LX/8NH;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-direct {v1, v0}, LX/8NH;-><init>(LX/0SG;)V

    .line 1336662
    return-object v1
.end method

.method private static c(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z
    .locals 6

    .prologue
    .line 1336663
    iget-wide v4, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    move-wide v0, v4

    .line 1336664
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)LX/14N;
    .locals 1

    .prologue
    .line 1336665
    check-cast p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-direct {p0, p1}, LX/8NH;->b(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1336666
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1336667
    invoke-static {v0}, LX/16N;->c(LX/0lF;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
