.class public LX/75F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/75F;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/ipc/media/MediaIdKey;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/75H;


# direct methods
.method public constructor <init>(LX/75H;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1169130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169131
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/75F;->a:Ljava/util/Map;

    .line 1169132
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/75F;->b:Ljava/util/Set;

    .line 1169133
    iput-object p1, p0, LX/75F;->c:LX/75H;

    .line 1169134
    return-void
.end method

.method public static a(LX/0QB;)LX/75F;
    .locals 4

    .prologue
    .line 1169114
    sget-object v0, LX/75F;->d:LX/75F;

    if-nez v0, :cond_1

    .line 1169115
    const-class v1, LX/75F;

    monitor-enter v1

    .line 1169116
    :try_start_0
    sget-object v0, LX/75F;->d:LX/75F;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1169117
    if-eqz v2, :cond_0

    .line 1169118
    :try_start_1
    new-instance p0, LX/75F;

    .line 1169119
    new-instance v3, LX/75H;

    invoke-direct {v3}, LX/75H;-><init>()V

    .line 1169120
    move-object v3, v3

    .line 1169121
    move-object v3, v3

    .line 1169122
    check-cast v3, LX/75H;

    invoke-direct {p0, v3}, LX/75F;-><init>(LX/75H;)V

    .line 1169123
    move-object v0, p0

    .line 1169124
    sput-object v0, LX/75F;->d:LX/75F;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1169125
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1169126
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1169127
    :cond_1
    sget-object v0, LX/75F;->d:LX/75F;

    return-object v0

    .line 1169128
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1169129
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/media/MediaIdKey;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ipc/media/MediaIdKey;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1169110
    iget-object v0, p0, LX/75F;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1169111
    if-nez v0, :cond_0

    .line 1169112
    const/4 v0, 0x0

    .line 1169113
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/74x;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74x;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1169109
    iget-object v0, p0, LX/75F;->a:Ljava/util/Map;

    invoke-virtual {p1}, LX/74x;->a()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final a(LX/74x;Ljava/util/List;)V
    .locals 10
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74x;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1169074
    instance-of v0, p1, Lcom/facebook/photos/base/photos/LocalPhoto;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    move-object v0, p1

    .line 1169075
    check-cast v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1169076
    iget v1, v0, Lcom/facebook/photos/base/photos/LocalPhoto;->e:I

    move v0, v1

    .line 1169077
    const/high16 v2, 0x3f000000    # 0.5f

    .line 1169078
    if-eqz p2, :cond_0

    if-nez v0, :cond_2

    .line 1169079
    :cond_0
    :goto_0
    move-object p2, p2

    .line 1169080
    :cond_1
    iget-object v0, p0, LX/75F;->a:Ljava/util/Map;

    invoke-virtual {p1}, LX/74x;->a()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1169081
    return-void

    .line 1169082
    :cond_2
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 1169083
    neg-int v1, v0

    int-to-float v1, v1

    invoke-virtual {v3, v1, v2, v2}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1169084
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1169085
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1169086
    new-instance v5, Landroid/graphics/RectF;

    invoke-virtual {v1}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1169087
    invoke-virtual {v3, v5}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1169088
    new-instance v6, Lcom/facebook/photos/base/tagging/FaceBox;

    invoke-virtual {v1}, Lcom/facebook/photos/base/tagging/FaceBox;->n()Ljava/util/List;

    move-result-object v7

    .line 1169089
    iget-boolean v8, v1, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    move v8, v8

    .line 1169090
    const/4 v9, 0x0

    invoke-direct {v6, v5, v7, v8, v9}, Lcom/facebook/photos/base/tagging/FaceBox;-><init>(Landroid/graphics/RectF;Ljava/util/List;ZZ)V

    .line 1169091
    iget-object v5, v1, Lcom/facebook/photos/base/tagging/FaceBox;->i:[B

    move-object v1, v5

    .line 1169092
    iput-object v1, v6, Lcom/facebook/photos/base/tagging/FaceBox;->i:[B

    .line 1169093
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object p2, v2

    .line 1169094
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1169135
    iget-object v0, p0, LX/75F;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1169136
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1169105
    iget-object v0, p0, LX/75F;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1169106
    iget-object v0, p0, LX/75F;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1169107
    invoke-virtual {p0}, LX/75F;->clearUserData()V

    .line 1169108
    :cond_0
    return-void
.end method

.method public final b(LX/74x;)Z
    .locals 1

    .prologue
    .line 1169104
    invoke-virtual {p0, p1}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/74x;)Z
    .locals 1

    .prologue
    .line 1169103
    invoke-virtual {p0, p1}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 1169101
    iget-object v0, p0, LX/75F;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1169102
    return-void
.end method

.method public final d(LX/74x;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1169095
    invoke-virtual {p0, p1}, LX/75F;->b(LX/74x;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1169096
    :goto_0
    return v0

    .line 1169097
    :cond_0
    invoke-virtual {p0, p1}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1169098
    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->o()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1169099
    goto :goto_0

    .line 1169100
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
