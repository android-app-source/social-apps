.class public LX/6uc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/6uW;",
            "LX/6F1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/6F1;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1156055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156056
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1156057
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6F1;

    .line 1156058
    iget-object v3, v0, LX/6F1;->a:LX/6uW;

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1156059
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/6uc;->a:LX/0P1;

    .line 1156060
    return-void
.end method

.method public static a(LX/0QB;)LX/6uc;
    .locals 6

    .prologue
    .line 1156042
    const-class v1, LX/6uc;

    monitor-enter v1

    .line 1156043
    :try_start_0
    sget-object v0, LX/6uc;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1156044
    sput-object v2, LX/6uc;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1156045
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1156046
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1156047
    new-instance v3, LX/6uc;

    .line 1156048
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/6uY;

    invoke-direct {p0, v0}, LX/6uY;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 1156049
    invoke-direct {v3, v4}, LX/6uc;-><init>(Ljava/util/Set;)V

    .line 1156050
    move-object v0, v3

    .line 1156051
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1156052
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6uc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1156053
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1156054
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(LX/6uW;)LX/6uV;
    .locals 1

    .prologue
    .line 1156039
    iget-object v0, p0, LX/6uc;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1156040
    :goto_0
    iget-object v0, p0, LX/6uc;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6F1;

    iget-object v0, v0, LX/6F1;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6uV;

    return-object v0

    .line 1156041
    :cond_0
    sget-object p1, LX/6uW;->SIMPLE:LX/6uW;

    goto :goto_0
.end method

.method public final d(LX/6uW;)LX/6ud;
    .locals 1

    .prologue
    .line 1156033
    iget-object v0, p0, LX/6uc;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1156034
    :goto_0
    iget-object v0, p0, LX/6uc;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6F1;

    iget-object v0, v0, LX/6F1;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ud;

    return-object v0

    .line 1156035
    :cond_0
    sget-object p1, LX/6uW;->SIMPLE:LX/6uW;

    goto :goto_0
.end method

.method public final e(LX/6uW;)LX/6F4;
    .locals 1

    .prologue
    .line 1156036
    iget-object v0, p0, LX/6uc;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1156037
    :goto_0
    iget-object v0, p0, LX/6uc;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6F1;

    iget-object v0, v0, LX/6F1;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6F4;

    return-object v0

    .line 1156038
    :cond_0
    sget-object p1, LX/6uW;->SIMPLE:LX/6uW;

    goto :goto_0
.end method
