.class public final LX/6yA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6vE;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V
    .locals 0

    .prologue
    .line 1159563
    iput-object p1, p0, LX/6yA;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/6z8;
    .locals 2

    .prologue
    .line 1159560
    iget-object v0, p0, LX/6yA;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    .line 1159561
    new-instance v1, LX/6zF;

    iget-object p0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {p0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, LX/6zF;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 1159562
    return-object v0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1159564
    if-eqz p1, :cond_0

    .line 1159565
    iget-object v0, p0, LX/6yA;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    const-string v1, "payflows_field_focus"

    invoke-static {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->b$redex0(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;Ljava/lang/String;)V

    .line 1159566
    :cond_0
    return-void
.end method
