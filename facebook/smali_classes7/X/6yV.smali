.class public final LX/6yV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/6yP;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/6yP;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1160069
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160070
    iput-object p1, p0, LX/6yV;->a:LX/0QB;

    .line 1160071
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1160072
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/6yV;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1160073
    packed-switch p2, :pswitch_data_0

    .line 1160074
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1160075
    :pswitch_0
    new-instance v2, LX/IjD;

    const/16 v3, 0x285d

    invoke-static {p1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2859

    invoke-static {p1, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x285a

    invoke-static {p1, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x285e

    invoke-static {p1, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x285b

    invoke-static {p1, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, LX/IjD;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1160076
    move-object v0, v2

    .line 1160077
    :goto_0
    return-object v0

    .line 1160078
    :pswitch_1
    invoke-static {p1}, LX/IjU;->b(LX/0QB;)LX/IjU;

    move-result-object v0

    goto :goto_0

    .line 1160079
    :pswitch_2
    new-instance v2, LX/Ijc;

    const/16 v3, 0x2d7e

    invoke-static {p1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2d79

    invoke-static {p1, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2864

    invoke-static {p1, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2d7f

    invoke-static {p1, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2865

    invoke-static {p1, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, LX/Ijc;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1160080
    move-object v0, v2

    .line 1160081
    goto :goto_0

    .line 1160082
    :pswitch_3
    new-instance v2, LX/6yf;

    const/16 v3, 0x2d7e

    invoke-static {p1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2d79

    invoke-static {p1, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2d7a

    invoke-static {p1, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2d7f

    invoke-static {p1, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2d7c

    invoke-static {p1, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, LX/6yf;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1160083
    move-object v0, v2

    .line 1160084
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1160085
    const/4 v0, 0x4

    return v0
.end method
