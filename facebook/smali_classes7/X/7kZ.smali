.class public final LX/7kZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/7ja;


# direct methods
.method public constructor <init>(LX/7ja;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1232224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232225
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1232226
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ID can\'t be blank! \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1232227
    :cond_0
    iput-object p2, p0, LX/7kZ;->a:Ljava/lang/String;

    .line 1232228
    iput-object p1, p0, LX/7kZ;->b:LX/7ja;

    .line 1232229
    return-void
.end method
