.class public LX/7Pv;
.super Landroid/preference/PreferenceCategory;
.source ""


# instance fields
.field public a:LX/1Ln;
    .annotation build Lcom/facebook/video/server/VideoCache;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1203477
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1203478
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/7Pv;

    invoke-static {v0}, LX/1Lk;->a(LX/0QB;)LX/1Ln;

    move-result-object v0

    check-cast v0, LX/1Ln;

    iput-object v0, p0, LX/7Pv;->a:LX/1Ln;

    .line 1203479
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 3

    .prologue
    .line 1203480
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 1203481
    invoke-virtual {p0}, LX/7Pv;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1203482
    const-string v1, "Video Server"

    invoke-virtual {p0, v1}, LX/7Pv;->setTitle(Ljava/lang/CharSequence;)V

    .line 1203483
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1203484
    const-string v2, "Clear Video Cache"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1203485
    new-instance v2, LX/7Pu;

    invoke-direct {v2, p0, v0}, LX/7Pu;-><init>(LX/7Pv;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1203486
    invoke-virtual {p0, v1}, LX/7Pv;->addPreference(Landroid/preference/Preference;)Z

    .line 1203487
    return-void
.end method
