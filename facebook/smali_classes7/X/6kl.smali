.class public LX/6kl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;


# instance fields
.field public final canStopSendingLocation:Ljava/lang/Boolean;

.field public final coordinate:LX/6km;

.field public final expirationTime:Ljava/lang/Long;

.field public final id:Ljava/lang/Long;

.field public final locationTitle:Ljava/lang/String;

.field public final messageId:Ljava/lang/String;

.field public final offlineThreadingId:Ljava/lang/String;

.field public final senderId:Ljava/lang/Long;

.field public final shouldShowEta:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/16 v6, 0xb

    const/16 v5, 0xa

    const/4 v4, 0x2

    .line 1140817
    new-instance v0, LX/1sv;

    const-string v1, "MessageLiveLocation"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kl;->b:LX/1sv;

    .line 1140818
    new-instance v0, LX/1sw;

    const-string v1, "id"

    invoke-direct {v0, v1, v5, v7}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kl;->c:LX/1sw;

    .line 1140819
    new-instance v0, LX/1sw;

    const-string v1, "senderId"

    invoke-direct {v0, v1, v5, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kl;->d:LX/1sw;

    .line 1140820
    new-instance v0, LX/1sw;

    const-string v1, "coordinate"

    const/16 v2, 0xc

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kl;->e:LX/1sw;

    .line 1140821
    new-instance v0, LX/1sw;

    const-string v1, "expirationTime"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kl;->f:LX/1sw;

    .line 1140822
    new-instance v0, LX/1sw;

    const-string v1, "canStopSendingLocation"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kl;->g:LX/1sw;

    .line 1140823
    new-instance v0, LX/1sw;

    const-string v1, "shouldShowEta"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kl;->h:LX/1sw;

    .line 1140824
    new-instance v0, LX/1sw;

    const-string v1, "offlineThreadingId"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kl;->i:LX/1sw;

    .line 1140825
    new-instance v0, LX/1sw;

    const-string v1, "messageId"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kl;->j:LX/1sw;

    .line 1140826
    new-instance v0, LX/1sw;

    const-string v1, "locationTitle"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kl;->k:LX/1sw;

    .line 1140827
    sput-boolean v7, LX/6kl;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Long;Ljava/lang/Long;LX/6km;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1140806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1140807
    iput-object p1, p0, LX/6kl;->id:Ljava/lang/Long;

    .line 1140808
    iput-object p2, p0, LX/6kl;->senderId:Ljava/lang/Long;

    .line 1140809
    iput-object p3, p0, LX/6kl;->coordinate:LX/6km;

    .line 1140810
    iput-object p4, p0, LX/6kl;->expirationTime:Ljava/lang/Long;

    .line 1140811
    iput-object p5, p0, LX/6kl;->canStopSendingLocation:Ljava/lang/Boolean;

    .line 1140812
    iput-object p6, p0, LX/6kl;->shouldShowEta:Ljava/lang/Boolean;

    .line 1140813
    iput-object p7, p0, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    .line 1140814
    iput-object p8, p0, LX/6kl;->messageId:Ljava/lang/String;

    .line 1140815
    iput-object p9, p0, LX/6kl;->locationTitle:Ljava/lang/String;

    .line 1140816
    return-void
.end method

.method private static a(LX/6kl;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1140793
    iget-object v0, p0, LX/6kl;->id:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 1140794
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'id\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1140795
    :cond_0
    iget-object v0, p0, LX/6kl;->senderId:Ljava/lang/Long;

    if-nez v0, :cond_1

    .line 1140796
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'senderId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1140797
    :cond_1
    iget-object v0, p0, LX/6kl;->expirationTime:Ljava/lang/Long;

    if-nez v0, :cond_2

    .line 1140798
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'expirationTime\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1140799
    :cond_2
    iget-object v0, p0, LX/6kl;->canStopSendingLocation:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    .line 1140800
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'canStopSendingLocation\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1140801
    :cond_3
    iget-object v0, p0, LX/6kl;->shouldShowEta:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    .line 1140802
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'shouldShowEta\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1140803
    :cond_4
    iget-object v0, p0, LX/6kl;->messageId:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 1140804
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'messageId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1140805
    :cond_5
    return-void
.end method

.method public static b(LX/1su;)LX/6kl;
    .locals 15

    .prologue
    const/4 v14, 0x2

    const/16 v13, 0xb

    const/16 v12, 0xa

    const/4 v9, 0x0

    .line 1140757
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v8, v9

    move-object v7, v9

    move-object v6, v9

    move-object v5, v9

    move-object v4, v9

    move-object v3, v9

    move-object v2, v9

    move-object v1, v9

    .line 1140758
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1140759
    iget-byte v10, v0, LX/1sw;->b:B

    if-eqz v10, :cond_9

    .line 1140760
    iget-short v10, v0, LX/1sw;->c:S

    packed-switch v10, :pswitch_data_0

    .line 1140761
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1140762
    :pswitch_0
    iget-byte v10, v0, LX/1sw;->b:B

    if-ne v10, v12, :cond_0

    .line 1140763
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 1140764
    :cond_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1140765
    :pswitch_1
    iget-byte v10, v0, LX/1sw;->b:B

    if-ne v10, v12, :cond_1

    .line 1140766
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0

    .line 1140767
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1140768
    :pswitch_2
    iget-byte v10, v0, LX/1sw;->b:B

    const/16 v11, 0xc

    if-ne v10, v11, :cond_2

    .line 1140769
    invoke-static {p0}, LX/6km;->b(LX/1su;)LX/6km;

    move-result-object v3

    goto :goto_0

    .line 1140770
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1140771
    :pswitch_3
    iget-byte v10, v0, LX/1sw;->b:B

    if-ne v10, v12, :cond_3

    .line 1140772
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_0

    .line 1140773
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1140774
    :pswitch_4
    iget-byte v10, v0, LX/1sw;->b:B

    if-ne v10, v14, :cond_4

    .line 1140775
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_0

    .line 1140776
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1140777
    :pswitch_5
    iget-byte v10, v0, LX/1sw;->b:B

    if-ne v10, v14, :cond_5

    .line 1140778
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto :goto_0

    .line 1140779
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1140780
    :pswitch_6
    iget-byte v10, v0, LX/1sw;->b:B

    if-ne v10, v13, :cond_6

    .line 1140781
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1140782
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1140783
    :pswitch_7
    iget-byte v10, v0, LX/1sw;->b:B

    if-ne v10, v13, :cond_7

    .line 1140784
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 1140785
    :cond_7
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1140786
    :pswitch_8
    iget-byte v10, v0, LX/1sw;->b:B

    if-ne v10, v13, :cond_8

    .line 1140787
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0

    .line 1140788
    :cond_8
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1140789
    :cond_9
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1140790
    new-instance v0, LX/6kl;

    invoke-direct/range {v0 .. v9}, LX/6kl;-><init>(Ljava/lang/Long;Ljava/lang/Long;LX/6km;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1140791
    invoke-static {v0}, LX/6kl;->a(LX/6kl;)V

    .line 1140792
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1140828
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1140829
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v1, v0

    .line 1140830
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1140831
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MessageLiveLocation"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1140832
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140833
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140834
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140835
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140836
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140837
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140838
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140839
    iget-object v4, p0, LX/6kl;->id:Ljava/lang/Long;

    if-nez v4, :cond_6

    .line 1140840
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140841
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140842
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140843
    const-string v4, "senderId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140844
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140845
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140846
    iget-object v4, p0, LX/6kl;->senderId:Ljava/lang/Long;

    if-nez v4, :cond_7

    .line 1140847
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140848
    :goto_4
    iget-object v4, p0, LX/6kl;->coordinate:LX/6km;

    if-eqz v4, :cond_0

    .line 1140849
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140850
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140851
    const-string v4, "coordinate"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140852
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140853
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140854
    iget-object v4, p0, LX/6kl;->coordinate:LX/6km;

    if-nez v4, :cond_8

    .line 1140855
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140856
    :cond_0
    :goto_5
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140857
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140858
    const-string v4, "expirationTime"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140859
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140860
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140861
    iget-object v4, p0, LX/6kl;->expirationTime:Ljava/lang/Long;

    if-nez v4, :cond_9

    .line 1140862
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140863
    :goto_6
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140864
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140865
    const-string v4, "canStopSendingLocation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140866
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140867
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140868
    iget-object v4, p0, LX/6kl;->canStopSendingLocation:Ljava/lang/Boolean;

    if-nez v4, :cond_a

    .line 1140869
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140870
    :goto_7
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140871
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140872
    const-string v4, "shouldShowEta"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140873
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140874
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140875
    iget-object v4, p0, LX/6kl;->shouldShowEta:Ljava/lang/Boolean;

    if-nez v4, :cond_b

    .line 1140876
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140877
    :goto_8
    iget-object v4, p0, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 1140878
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140879
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140880
    const-string v4, "offlineThreadingId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140881
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140882
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140883
    iget-object v4, p0, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    if-nez v4, :cond_c

    .line 1140884
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140885
    :cond_1
    :goto_9
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140886
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140887
    const-string v4, "messageId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140888
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140889
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140890
    iget-object v4, p0, LX/6kl;->messageId:Ljava/lang/String;

    if-nez v4, :cond_d

    .line 1140891
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140892
    :goto_a
    iget-object v4, p0, LX/6kl;->locationTitle:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 1140893
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140894
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140895
    const-string v4, "locationTitle"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140896
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140897
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140898
    iget-object v0, p0, LX/6kl;->locationTitle:Ljava/lang/String;

    if-nez v0, :cond_e

    .line 1140899
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140900
    :cond_2
    :goto_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140901
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140902
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1140903
    :cond_3
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1140904
    :cond_4
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1140905
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1140906
    :cond_6
    iget-object v4, p0, LX/6kl;->id:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1140907
    :cond_7
    iget-object v4, p0, LX/6kl;->senderId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1140908
    :cond_8
    iget-object v4, p0, LX/6kl;->coordinate:LX/6km;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1140909
    :cond_9
    iget-object v4, p0, LX/6kl;->expirationTime:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1140910
    :cond_a
    iget-object v4, p0, LX/6kl;->canStopSendingLocation:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1140911
    :cond_b
    iget-object v4, p0, LX/6kl;->shouldShowEta:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1140912
    :cond_c
    iget-object v4, p0, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 1140913
    :cond_d
    iget-object v4, p0, LX/6kl;->messageId:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 1140914
    :cond_e
    iget-object v0, p0, LX/6kl;->locationTitle:Ljava/lang/String;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1140722
    invoke-static {p0}, LX/6kl;->a(LX/6kl;)V

    .line 1140723
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1140724
    iget-object v0, p0, LX/6kl;->id:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1140725
    sget-object v0, LX/6kl;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140726
    iget-object v0, p0, LX/6kl;->id:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1140727
    :cond_0
    iget-object v0, p0, LX/6kl;->senderId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1140728
    sget-object v0, LX/6kl;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140729
    iget-object v0, p0, LX/6kl;->senderId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1140730
    :cond_1
    iget-object v0, p0, LX/6kl;->coordinate:LX/6km;

    if-eqz v0, :cond_2

    .line 1140731
    iget-object v0, p0, LX/6kl;->coordinate:LX/6km;

    if-eqz v0, :cond_2

    .line 1140732
    sget-object v0, LX/6kl;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140733
    iget-object v0, p0, LX/6kl;->coordinate:LX/6km;

    invoke-virtual {v0, p1}, LX/6km;->a(LX/1su;)V

    .line 1140734
    :cond_2
    iget-object v0, p0, LX/6kl;->expirationTime:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1140735
    sget-object v0, LX/6kl;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140736
    iget-object v0, p0, LX/6kl;->expirationTime:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1140737
    :cond_3
    iget-object v0, p0, LX/6kl;->canStopSendingLocation:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1140738
    sget-object v0, LX/6kl;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140739
    iget-object v0, p0, LX/6kl;->canStopSendingLocation:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1140740
    :cond_4
    iget-object v0, p0, LX/6kl;->shouldShowEta:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1140741
    sget-object v0, LX/6kl;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140742
    iget-object v0, p0, LX/6kl;->shouldShowEta:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1140743
    :cond_5
    iget-object v0, p0, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1140744
    iget-object v0, p0, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1140745
    sget-object v0, LX/6kl;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140746
    iget-object v0, p0, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1140747
    :cond_6
    iget-object v0, p0, LX/6kl;->messageId:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1140748
    sget-object v0, LX/6kl;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140749
    iget-object v0, p0, LX/6kl;->messageId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1140750
    :cond_7
    iget-object v0, p0, LX/6kl;->locationTitle:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1140751
    iget-object v0, p0, LX/6kl;->locationTitle:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1140752
    sget-object v0, LX/6kl;->k:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140753
    iget-object v0, p0, LX/6kl;->locationTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1140754
    :cond_8
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1140755
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1140756
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1140651
    if-nez p1, :cond_1

    .line 1140652
    :cond_0
    :goto_0
    return v0

    .line 1140653
    :cond_1
    instance-of v1, p1, LX/6kl;

    if-eqz v1, :cond_0

    .line 1140654
    check-cast p1, LX/6kl;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1140655
    if-nez p1, :cond_3

    .line 1140656
    :cond_2
    :goto_1
    move v0, v2

    .line 1140657
    goto :goto_0

    .line 1140658
    :cond_3
    iget-object v0, p0, LX/6kl;->id:Ljava/lang/Long;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1140659
    :goto_2
    iget-object v3, p1, LX/6kl;->id:Ljava/lang/Long;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1140660
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1140661
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1140662
    iget-object v0, p0, LX/6kl;->id:Ljava/lang/Long;

    iget-object v3, p1, LX/6kl;->id:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1140663
    :cond_5
    iget-object v0, p0, LX/6kl;->senderId:Ljava/lang/Long;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1140664
    :goto_4
    iget-object v3, p1, LX/6kl;->senderId:Ljava/lang/Long;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1140665
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1140666
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1140667
    iget-object v0, p0, LX/6kl;->senderId:Ljava/lang/Long;

    iget-object v3, p1, LX/6kl;->senderId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1140668
    :cond_7
    iget-object v0, p0, LX/6kl;->coordinate:LX/6km;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1140669
    :goto_6
    iget-object v3, p1, LX/6kl;->coordinate:LX/6km;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1140670
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1140671
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1140672
    iget-object v0, p0, LX/6kl;->coordinate:LX/6km;

    iget-object v3, p1, LX/6kl;->coordinate:LX/6km;

    invoke-virtual {v0, v3}, LX/6km;->a(LX/6km;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1140673
    :cond_9
    iget-object v0, p0, LX/6kl;->expirationTime:Ljava/lang/Long;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1140674
    :goto_8
    iget-object v3, p1, LX/6kl;->expirationTime:Ljava/lang/Long;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1140675
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1140676
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1140677
    iget-object v0, p0, LX/6kl;->expirationTime:Ljava/lang/Long;

    iget-object v3, p1, LX/6kl;->expirationTime:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1140678
    :cond_b
    iget-object v0, p0, LX/6kl;->canStopSendingLocation:Ljava/lang/Boolean;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1140679
    :goto_a
    iget-object v3, p1, LX/6kl;->canStopSendingLocation:Ljava/lang/Boolean;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1140680
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1140681
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1140682
    iget-object v0, p0, LX/6kl;->canStopSendingLocation:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6kl;->canStopSendingLocation:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1140683
    :cond_d
    iget-object v0, p0, LX/6kl;->shouldShowEta:Ljava/lang/Boolean;

    if-eqz v0, :cond_20

    move v0, v1

    .line 1140684
    :goto_c
    iget-object v3, p1, LX/6kl;->shouldShowEta:Ljava/lang/Boolean;

    if-eqz v3, :cond_21

    move v3, v1

    .line 1140685
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1140686
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1140687
    iget-object v0, p0, LX/6kl;->shouldShowEta:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6kl;->shouldShowEta:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1140688
    :cond_f
    iget-object v0, p0, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    if-eqz v0, :cond_22

    move v0, v1

    .line 1140689
    :goto_e
    iget-object v3, p1, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    if-eqz v3, :cond_23

    move v3, v1

    .line 1140690
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1140691
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1140692
    iget-object v0, p0, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    iget-object v3, p1, LX/6kl;->offlineThreadingId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1140693
    :cond_11
    iget-object v0, p0, LX/6kl;->messageId:Ljava/lang/String;

    if-eqz v0, :cond_24

    move v0, v1

    .line 1140694
    :goto_10
    iget-object v3, p1, LX/6kl;->messageId:Ljava/lang/String;

    if-eqz v3, :cond_25

    move v3, v1

    .line 1140695
    :goto_11
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 1140696
    :cond_12
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1140697
    iget-object v0, p0, LX/6kl;->messageId:Ljava/lang/String;

    iget-object v3, p1, LX/6kl;->messageId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1140698
    :cond_13
    iget-object v0, p0, LX/6kl;->locationTitle:Ljava/lang/String;

    if-eqz v0, :cond_26

    move v0, v1

    .line 1140699
    :goto_12
    iget-object v3, p1, LX/6kl;->locationTitle:Ljava/lang/String;

    if-eqz v3, :cond_27

    move v3, v1

    .line 1140700
    :goto_13
    if-nez v0, :cond_14

    if-eqz v3, :cond_15

    .line 1140701
    :cond_14
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1140702
    iget-object v0, p0, LX/6kl;->locationTitle:Ljava/lang/String;

    iget-object v3, p1, LX/6kl;->locationTitle:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_15
    move v2, v1

    .line 1140703
    goto/16 :goto_1

    :cond_16
    move v0, v2

    .line 1140704
    goto/16 :goto_2

    :cond_17
    move v3, v2

    .line 1140705
    goto/16 :goto_3

    :cond_18
    move v0, v2

    .line 1140706
    goto/16 :goto_4

    :cond_19
    move v3, v2

    .line 1140707
    goto/16 :goto_5

    :cond_1a
    move v0, v2

    .line 1140708
    goto/16 :goto_6

    :cond_1b
    move v3, v2

    .line 1140709
    goto/16 :goto_7

    :cond_1c
    move v0, v2

    .line 1140710
    goto/16 :goto_8

    :cond_1d
    move v3, v2

    .line 1140711
    goto/16 :goto_9

    :cond_1e
    move v0, v2

    .line 1140712
    goto/16 :goto_a

    :cond_1f
    move v3, v2

    .line 1140713
    goto/16 :goto_b

    :cond_20
    move v0, v2

    .line 1140714
    goto/16 :goto_c

    :cond_21
    move v3, v2

    .line 1140715
    goto/16 :goto_d

    :cond_22
    move v0, v2

    .line 1140716
    goto :goto_e

    :cond_23
    move v3, v2

    .line 1140717
    goto :goto_f

    :cond_24
    move v0, v2

    .line 1140718
    goto :goto_10

    :cond_25
    move v3, v2

    .line 1140719
    goto :goto_11

    :cond_26
    move v0, v2

    .line 1140720
    goto :goto_12

    :cond_27
    move v3, v2

    .line 1140721
    goto :goto_13
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1140650
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1140647
    sget-boolean v0, LX/6kl;->a:Z

    .line 1140648
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kl;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1140649
    return-object v0
.end method
