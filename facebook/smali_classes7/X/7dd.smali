.class public interface abstract LX/7dd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a(Lcom/google/android/gms/nearby/internal/connection/dev/GetLocalDeviceIdParams;)Ljava/lang/String;
.end method

.method public abstract a(Lcom/google/android/gms/nearby/internal/connection/dev/GetLocalEndpointIdParams;)Ljava/lang/String;
.end method

.method public abstract a(J)V
.end method

.method public abstract a(LX/7db;Ljava/lang/String;J)V
.end method

.method public abstract a(LX/7db;Ljava/lang/String;JJ)V
.end method

.method public abstract a(LX/7db;Ljava/lang/String;Lcom/google/android/gms/nearby/connection/dev/AppMetadata;JJ)V
.end method

.method public abstract a(LX/7db;Ljava/lang/String;Ljava/lang/String;[BJ)V
.end method

.method public abstract a(LX/7db;Ljava/lang/String;[BJ)V
.end method

.method public abstract a(Lcom/google/android/gms/nearby/internal/connection/dev/AcceptConnectionRequestParams;)V
.end method

.method public abstract a(Lcom/google/android/gms/nearby/internal/connection/dev/ClientDisconnectingParams;)V
.end method

.method public abstract a(Lcom/google/android/gms/nearby/internal/connection/dev/DisconnectFromEndpointParams;)V
.end method

.method public abstract a(Lcom/google/android/gms/nearby/internal/connection/dev/RejectConnectionRequestParams;)V
.end method

.method public abstract a(Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;)V
.end method

.method public abstract a(Lcom/google/android/gms/nearby/internal/connection/dev/SendPayloadParams;)V
.end method

.method public abstract a(Lcom/google/android/gms/nearby/internal/connection/dev/StartAdvertisingParams;)V
.end method

.method public abstract a(Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;)V
.end method

.method public abstract a(Lcom/google/android/gms/nearby/internal/connection/dev/StopAdvertisingParams;)V
.end method

.method public abstract a(Lcom/google/android/gms/nearby/internal/connection/dev/StopAllEndpointsParams;)V
.end method

.method public abstract a(Lcom/google/android/gms/nearby/internal/connection/dev/StopDiscoveryParams;)V
.end method

.method public abstract a(Ljava/lang/String;J)V
.end method

.method public abstract a([Ljava/lang/String;[BJ)V
.end method

.method public abstract b(J)V
.end method

.method public abstract b(Ljava/lang/String;J)V
.end method

.method public abstract b([Ljava/lang/String;[BJ)V
.end method

.method public abstract c(J)V
.end method

.method public abstract d(J)Ljava/lang/String;
.end method
