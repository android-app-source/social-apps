.class public final enum LX/89y;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/89y;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/89y;

.field public static final enum HEADER:LX/89y;

.field public static final enum REACTION_CARD:LX/89y;

.field public static final enum TAB_DEEPLINK:LX/89y;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1305528
    new-instance v0, LX/89y;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v2}, LX/89y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89y;->HEADER:LX/89y;

    .line 1305529
    new-instance v0, LX/89y;

    const-string v1, "REACTION_CARD"

    invoke-direct {v0, v1, v3}, LX/89y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89y;->REACTION_CARD:LX/89y;

    .line 1305530
    new-instance v0, LX/89y;

    const-string v1, "TAB_DEEPLINK"

    invoke-direct {v0, v1, v4}, LX/89y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89y;->TAB_DEEPLINK:LX/89y;

    .line 1305531
    const/4 v0, 0x3

    new-array v0, v0, [LX/89y;

    sget-object v1, LX/89y;->HEADER:LX/89y;

    aput-object v1, v0, v2

    sget-object v1, LX/89y;->REACTION_CARD:LX/89y;

    aput-object v1, v0, v3

    sget-object v1, LX/89y;->TAB_DEEPLINK:LX/89y;

    aput-object v1, v0, v4

    sput-object v0, LX/89y;->$VALUES:[LX/89y;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1305532
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/89y;
    .locals 1

    .prologue
    .line 1305533
    const-class v0, LX/89y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/89y;

    return-object v0
.end method

.method public static values()[LX/89y;
    .locals 1

    .prologue
    .line 1305534
    sget-object v0, LX/89y;->$VALUES:[LX/89y;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/89y;

    return-object v0
.end method
