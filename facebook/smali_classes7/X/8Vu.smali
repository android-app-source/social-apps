.class public final LX/8Vu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/8Vb;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/8Vv;


# direct methods
.method public constructor <init>(LX/8Vv;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1353495
    iput-object p1, p0, LX/8Vu;->b:LX/8Vv;

    iput-object p2, p0, LX/8Vu;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 6

    .prologue
    .line 1353496
    check-cast p1, LX/8Vb;

    check-cast p2, LX/8Vb;

    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 1353497
    iget-object v2, p0, LX/8Vu;->a:Ljava/lang/String;

    iget-object v3, p1, LX/8Vb;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1353498
    :cond_0
    :goto_0
    return v0

    .line 1353499
    :cond_1
    iget-object v2, p0, LX/8Vu;->a:Ljava/lang/String;

    iget-object v3, p2, LX/8Vb;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 1353500
    goto :goto_0

    .line 1353501
    :cond_2
    iget-wide v2, p1, LX/8Vb;->l:J

    iget-wide v4, p2, LX/8Vb;->l:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    move v0, v1

    .line 1353502
    goto :goto_0
.end method
