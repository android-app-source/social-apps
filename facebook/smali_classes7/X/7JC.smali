.class public final LX/7JC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/27U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/27U",
        "<",
        "Lcom/google/android/gms/common/api/Status;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7JI;


# direct methods
.method public constructor <init>(LX/7JI;)V
    .locals 0

    .prologue
    .line 1193244
    iput-object p1, p0, LX/7JC;->a:LX/7JI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2NW;)V
    .locals 4

    .prologue
    .line 1193245
    check-cast p1, Lcom/google/android/gms/common/api/Status;

    .line 1193246
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1193247
    iget-object v0, p0, LX/7JC;->a:LX/7JI;

    iget-object v0, v0, LX/7JI;->a:LX/38g;

    iget-object v0, v0, LX/38g;->d:LX/37f;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    iget v2, v1, Lcom/google/android/gms/common/api/Status;->i:I

    move v1, v2

    .line 1193248
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FbAppPlayer.startPlaying(): "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    iget-object p1, v3, Lcom/google/android/gms/common/api/Status;->j:Ljava/lang/String;

    move-object v3, p1

    .line 1193249
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/37f;->a(ILjava/lang/String;)V

    .line 1193250
    iget-object v0, p0, LX/7JC;->a:LX/7JI;

    iget-object v0, v0, LX/7JI;->a:LX/38g;

    iget-object v0, v0, LX/38g;->h:LX/38i;

    invoke-virtual {v0}, LX/38i;->c()V

    .line 1193251
    :cond_0
    return-void
.end method
