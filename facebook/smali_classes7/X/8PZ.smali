.class public final LX/8PZ;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1341881
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 1341882
    return-void
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/03R;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/privacy/gating/IsDefaultPostPrivacyEnabled;
    .end annotation

    .prologue
    .line 1341883
    sget-object v0, LX/2bs;->m:LX/0Tn;

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 1341884
    if-eqz v0, :cond_0

    .line 1341885
    sget-object v0, LX/03R;->YES:LX/03R;

    .line 1341886
    :goto_0
    return-object v0

    .line 1341887
    :cond_0
    sget-object v0, LX/2bs;->n:LX/0Tn;

    invoke-interface {p0, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/3nM;LX/3nP;LX/2c3;LX/8SI;)LX/1qM;
    .locals 3
    .annotation runtime Lcom/facebook/inject/ContextScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/privacy/service/PrivacyDataQueue;
    .end annotation

    .prologue
    .line 1341888
    new-instance v0, LX/2m1;

    new-instance v1, LX/2m1;

    new-instance v2, LX/2m1;

    invoke-direct {v2, p1, p3}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    invoke-direct {v1, p2, v2}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    invoke-direct {v0, p0, v1}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 1341889
    return-void
.end method
