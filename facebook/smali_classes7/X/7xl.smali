.class public final LX/7xl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;)V
    .locals 0

    .prologue
    .line 1277848
    iput-object p1, p0, LX/7xl;->a:Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x2

    const v0, -0x6ab2680f

    invoke-static {v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1277836
    iget-object v1, p0, LX/7xl;->a:Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->r:Lcom/facebook/resources/ui/FbRadioButton;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    .line 1277837
    iget-object v1, p0, LX/7xl;->a:Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->u:LX/7xg;

    if-eqz v1, :cond_0

    .line 1277838
    iget-object v1, p0, LX/7xl;->a:Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->u:LX/7xg;

    iget-object v2, p0, LX/7xl;->a:Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;

    iget v2, v2, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->v:I

    .line 1277839
    iget-object v4, v1, LX/7xg;->a:LX/7xj;

    iget-object p0, v1, LX/7xg;->a:LX/7xj;

    iget p0, p0, LX/7xj;->h:I

    invoke-virtual {v4, p0}, LX/1OM;->i_(I)V

    .line 1277840
    iget-object v4, v1, LX/7xg;->a:LX/7xj;

    iget-object p0, v1, LX/7xg;->a:LX/7xj;

    iget p0, p0, LX/7xj;->d:I

    .line 1277841
    iput p0, v4, LX/7xj;->c:I

    .line 1277842
    iget-object v4, v1, LX/7xg;->a:LX/7xj;

    .line 1277843
    iput v2, v4, LX/7xj;->d:I

    .line 1277844
    iget-object v4, v1, LX/7xg;->a:LX/7xj;

    iget-object p0, v1, LX/7xg;->a:LX/7xj;

    iget p0, p0, LX/7xj;->g:I

    iget-object p1, v1, LX/7xg;->a:LX/7xj;

    iget p1, p1, LX/7xj;->c:I

    add-int/2addr p0, p1

    invoke-virtual {v4, p0}, LX/1OM;->i_(I)V

    .line 1277845
    iget-object v4, v1, LX/7xg;->a:LX/7xj;

    iget-object p0, v1, LX/7xg;->a:LX/7xj;

    iget p0, p0, LX/7xj;->g:I

    iget-object p1, v1, LX/7xg;->a:LX/7xj;

    iget p1, p1, LX/7xj;->d:I

    add-int/2addr p0, p1

    invoke-virtual {v4, p0}, LX/1OM;->i_(I)V

    .line 1277846
    iget-object v4, v1, LX/7xg;->a:LX/7xj;

    invoke-static {v4}, LX/7xj;->g(LX/7xj;)V

    .line 1277847
    :cond_0
    const v1, -0x4218f9bd

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
