.class public final LX/6v0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/payments/consent/model/ConsentExtraData;

.field public final b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

.field public c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/consent/model/ConsentExtraData;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)V
    .locals 2

    .prologue
    .line 1156364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156365
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v0

    sget-object v1, LX/73i;->PAYMENTS_WHITE:LX/73i;

    .line 1156366
    iput-object v1, v0, LX/6wu;->b:LX/73i;

    .line 1156367
    move-object v0, v0

    .line 1156368
    sget-object v1, LX/6ws;->MODAL_BOTTOM:LX/6ws;

    .line 1156369
    iput-object v1, v0, LX/6wu;->a:LX/6ws;

    .line 1156370
    move-object v0, v0

    .line 1156371
    const/4 v1, 0x1

    .line 1156372
    iput-boolean v1, v0, LX/6wu;->d:Z

    .line 1156373
    move-object v0, v0

    .line 1156374
    invoke-virtual {v0}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v0

    iput-object v0, p0, LX/6v0;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1156375
    iput-object p1, p0, LX/6v0;->a:Lcom/facebook/payments/consent/model/ConsentExtraData;

    .line 1156376
    iput-object p2, p0, LX/6v0;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1156377
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;
    .locals 2

    .prologue
    .line 1156378
    new-instance v0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    invoke-direct {v0, p0}, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;-><init>(LX/6v0;)V

    return-object v0
.end method
