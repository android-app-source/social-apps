.class public final enum LX/7zq;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/1gt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7zq;",
        ">;",
        "LX/1gt;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7zq;

.field public static final enum STATUS_STUCK:LX/7zq;


# instance fields
.field private mShouldLogClientEvent:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1281424
    new-instance v0, LX/7zq;

    const-string v1, "STATUS_STUCK"

    invoke-direct {v0, v1, v2, v3}, LX/7zq;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/7zq;->STATUS_STUCK:LX/7zq;

    .line 1281425
    new-array v0, v3, [LX/7zq;

    sget-object v1, LX/7zq;->STATUS_STUCK:LX/7zq;

    aput-object v1, v0, v2

    sput-object v0, LX/7zq;->$VALUES:[LX/7zq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1281426
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7zq;-><init>(Ljava/lang/String;IZ)V

    .line 1281427
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 1281428
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1281429
    iput-boolean p3, p0, LX/7zq;->mShouldLogClientEvent:Z

    .line 1281430
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7zq;
    .locals 1

    .prologue
    .line 1281431
    const-class v0, LX/7zq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7zq;

    return-object v0
.end method

.method public static values()[LX/7zq;
    .locals 1

    .prologue
    .line 1281432
    sget-object v0, LX/7zq;->$VALUES:[LX/7zq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7zq;

    return-object v0
.end method


# virtual methods
.method public final getClientEventName()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1281433
    iget-boolean v0, p0, LX/7zq;->mShouldLogClientEvent:Z

    if-eqz v0, :cond_0

    .line 1281434
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "nf_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/7zq;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1281435
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1281436
    invoke-virtual {p0}, LX/7zq;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
