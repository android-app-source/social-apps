.class public final LX/8Sv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/QuicksilverFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V
    .locals 0

    .prologue
    .line 1347550
    iput-object p1, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/quicksilver/QuicksilverFragment;B)V
    .locals 0

    .prologue
    .line 1347549
    invoke-direct {p0, p1}, LX/8Sv;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1347533
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    .line 1347534
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    sget-object v2, LX/8TE;->FUNNEL_ACTION_TAG_SCORE:LX/8TE;

    iget-object v2, v2, LX/8TE;->value:Ljava/lang/String;

    iget-object v3, v0, LX/8TD;->c:LX/8TS;

    .line 1347535
    iget v4, v3, LX/8TS;->n:I

    move v3, v4

    .line 1347536
    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v1

    .line 1347537
    sget-object v2, LX/8TE;->FUNNEL_GAME_PLAY_END:LX/8TE;

    invoke-static {v0, v2, v1}, LX/8TD;->a(LX/8TD;LX/8TE;LX/1rQ;)V

    .line 1347538
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    sget-object v1, LX/8TV;->END_SCREEN:LX/8TV;

    invoke-virtual {v0, v1}, LX/8TS;->a(LX/8TV;)V

    .line 1347539
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->v:LX/8Ul;

    .line 1347540
    iget-object v1, v0, LX/8Ul;->b:LX/8TS;

    .line 1347541
    iget-object v2, v1, LX/8TS;->j:LX/8TP;

    move-object v1, v2

    .line 1347542
    iget-object v2, v0, LX/8Ul;->b:LX/8TS;

    .line 1347543
    iget v3, v2, LX/8TS;->n:I

    move v2, v3

    .line 1347544
    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/8Ul;->a(LX/8Ul;LX/8TP;IZ)V

    .line 1347545
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-static {v0}, Lcom/facebook/quicksilver/QuicksilverFragment;->s(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    .line 1347546
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->invalidateOptionsMenu()V

    .line 1347547
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$1;

    invoke-direct {v1, p0}, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$1;-><init>(LX/8Sv;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1347548
    return-void
.end method

.method public final a(I)V
    .locals 6

    .prologue
    .line 1347526
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    invoke-virtual {v0, p1}, LX/8TS;->a(I)V

    .line 1347527
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    int-to-long v2, p1

    int-to-long v4, p1

    invoke-virtual {v0, v2, v3, v4, v5}, LX/8TS;->a(JJ)V

    .line 1347528
    sget-object v0, LX/8TV;->END_SCREEN:LX/8TV;

    iget-object v1, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v1, v1, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    .line 1347529
    iget-object v2, v1, LX/8TS;->m:LX/8TV;

    move-object v1, v2

    .line 1347530
    invoke-virtual {v0, v1}, LX/8TV;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1347531
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v1, LX/8TE;->ON_SCORE_CALLED_AFTER_GAME_END:LX/8TE;

    const-string v2, "Score isn\'t recorded to display in leaderboard"

    invoke-virtual {v0, v1, v2}, LX/8TD;->a(LX/8TE;Ljava/lang/String;)V

    .line 1347532
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 1347524
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$5;

    invoke-direct {v1, p0, p1}, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$5;-><init>(LX/8Sv;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1347525
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1347551
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$6;

    invoke-direct {v1, p0, p1}, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$6;-><init>(LX/8Sv;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1347552
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1347510
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$9;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$9;-><init>(LX/8Sv;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1347511
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1347508
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;-><init>(LX/8Sv;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1347509
    return-void
.end method

.method public final a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 1347522
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$8;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$8;-><init>(LX/8Sv;Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1347523
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1347512
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v1, LX/8TE;->FUNNEL_GAME_READY:LX/8TE;

    invoke-virtual {v0, v1}, LX/8TD;->b(LX/8TE;)V

    .line 1347513
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    const/4 v1, 0x1

    .line 1347514
    iput-boolean v1, v0, LX/8TS;->i:Z

    .line 1347515
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$4;

    invoke-direct {v1, p0}, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$4;-><init>(LX/8Sv;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1347516
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 1347517
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v1, LX/8TE;->FUNNEL_LOADING_STARTED:LX/8TE;

    invoke-virtual {v0, v1}, LX/8TD;->b(LX/8TE;)V

    .line 1347518
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$2;-><init>(LX/8Sv;I)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1347519
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 1347520
    iget-object v0, p0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$3;-><init>(LX/8Sv;I)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1347521
    return-void
.end method
