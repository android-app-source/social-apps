.class public LX/7yl;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field public f:I

.field public g:LX/7yk;

.field public h:LX/7yn;

.field private i:LX/7ym;

.field private j:LX/7yo;

.field public k:Landroid/graphics/Paint;

.field public final l:Landroid/os/Handler;

.field public final m:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1280251
    const-class v0, LX/7yl;

    sput-object v0, LX/7yl;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/7ym;LX/7yk;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1280252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280253
    iput-object v0, p0, LX/7yl;->g:LX/7yk;

    .line 1280254
    iput-object v0, p0, LX/7yl;->h:LX/7yn;

    .line 1280255
    iput-object v0, p0, LX/7yl;->i:LX/7ym;

    .line 1280256
    iput-object v0, p0, LX/7yl;->j:LX/7yo;

    .line 1280257
    iput-object v0, p0, LX/7yl;->k:Landroid/graphics/Paint;

    .line 1280258
    new-instance v0, Lcom/facebook/fbui/tinyclicks/MasterTouchDelegate$1;

    invoke-direct {v0, p0}, Lcom/facebook/fbui/tinyclicks/MasterTouchDelegate$1;-><init>(LX/7yl;)V

    iput-object v0, p0, LX/7yl;->m:Ljava/lang/Runnable;

    .line 1280259
    iput-object p2, p0, LX/7yl;->i:LX/7ym;

    .line 1280260
    iput-object p3, p0, LX/7yl;->g:LX/7yk;

    .line 1280261
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 1280262
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, LX/7yl;->b:I

    .line 1280263
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    iput v0, p0, LX/7yl;->e:I

    .line 1280264
    iget v0, p0, LX/7yl;->b:I

    mul-int/lit8 v0, v0, -0x2

    iput v0, p0, LX/7yl;->f:I

    .line 1280265
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/7yl;->l:Landroid/os/Handler;

    .line 1280266
    return-void
.end method

.method private a(II)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, -0x1

    .line 1280272
    iget v1, p0, LX/7yl;->c:I

    if-ltz v1, :cond_0

    iget v1, p0, LX/7yl;->d:I

    if-gez v1, :cond_1

    .line 1280273
    :cond_0
    :goto_0
    return v0

    .line 1280274
    :cond_1
    iget v1, p0, LX/7yl;->c:I

    sub-int v1, p1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 1280275
    iget v2, p0, LX/7yl;->d:I

    sub-int v2, p2, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 1280276
    iget v3, p0, LX/7yl;->b:I

    if-gt v1, v3, :cond_2

    iget v1, p0, LX/7yl;->b:I

    if-le v2, v1, :cond_3

    .line 1280277
    :cond_2
    iput v4, p0, LX/7yl;->c:I

    .line 1280278
    iput v4, p0, LX/7yl;->d:I

    goto :goto_0

    .line 1280279
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/7yl;
    .locals 5

    .prologue
    .line 1280267
    new-instance v3, LX/7yl;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/7ym;->a(LX/0QB;)LX/7ym;

    move-result-object v1

    check-cast v1, LX/7ym;

    .line 1280268
    new-instance v4, LX/7yk;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {v4, v2}, LX/7yk;-><init>(Landroid/content/Context;)V

    .line 1280269
    move-object v2, v4

    .line 1280270
    check-cast v2, LX/7yk;

    invoke-direct {v3, v0, v1, v2}, LX/7yl;-><init>(Landroid/content/Context;LX/7ym;LX/7yk;)V

    .line 1280271
    return-object v3
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 1280245
    iget-object v0, p0, LX/7yl;->i:LX/7ym;

    .line 1280246
    iget-boolean p0, v0, LX/7ym;->d:Z

    move v0, p0

    .line 1280247
    return v0
.end method

.method private e()V
    .locals 5

    .prologue
    .line 1280248
    iget-object v0, p0, LX/7yl;->l:Landroid/os/Handler;

    iget-object v1, p0, LX/7yl;->m:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1280249
    iget-object v0, p0, LX/7yl;->l:Landroid/os/Handler;

    iget-object v1, p0, LX/7yl;->m:Ljava/lang/Runnable;

    iget v2, p0, LX/7yl;->e:I

    int-to-long v2, v2

    const v4, -0x3e52f5a

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1280250
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1280240
    iget-object v0, p0, LX/7yl;->i:LX/7ym;

    iget-object v1, p0, LX/7yl;->j:LX/7yo;

    .line 1280241
    iget-object p0, v0, LX/7ym;->b:Ljava/util/Set;

    invoke-interface {p0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1280242
    iget-boolean p0, v0, LX/7ym;->d:Z

    if-nez p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    invoke-interface {v1, p0}, LX/7yo;->setWillNotDraw(Z)V

    .line 1280243
    return-void

    .line 1280244
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final a(LX/7yo;)V
    .locals 2

    .prologue
    .line 1280237
    instance-of v0, p1, Landroid/view/ViewGroup;

    const-string v1, "MasterTouchDelegateLayout MUST be a ViewGroup"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1280238
    iput-object p1, p0, LX/7yl;->j:LX/7yo;

    .line 1280239
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1280228
    invoke-direct {p0}, LX/7yl;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/7yl;->h:LX/7yn;

    if-eqz v0, :cond_1

    .line 1280229
    iget-object v0, p0, LX/7yl;->h:LX/7yn;

    .line 1280230
    iget-object v1, v0, LX/7yn;->d:Landroid/graphics/Rect;

    move-object v0, v1

    .line 1280231
    iget-object v1, p0, LX/7yl;->k:Landroid/graphics/Paint;

    if-nez v1, :cond_0

    .line 1280232
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, LX/7yl;->k:Landroid/graphics/Paint;

    .line 1280233
    iget-object v1, p0, LX/7yl;->k:Landroid/graphics/Paint;

    const-string v2, "#80FF0000"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1280234
    :cond_0
    iget-object v1, p0, LX/7yl;->k:Landroid/graphics/Paint;

    move-object v1, v1

    .line 1280235
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1280236
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1280189
    iget-object v0, p0, LX/7yl;->i:LX/7ym;

    .line 1280190
    iget-boolean v3, v0, LX/7ym;->c:Z

    move v0, v3

    .line 1280191
    if-nez v0, :cond_1

    .line 1280192
    :cond_0
    :goto_0
    return v2

    .line 1280193
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 1280194
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 1280195
    iget-object v3, p0, LX/7yl;->h:LX/7yn;

    .line 1280196
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    .line 1280197
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_2
    move-object v0, v3

    .line 1280198
    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    .line 1280199
    if-eqz v1, :cond_7

    .line 1280200
    float-to-int v1, v4

    .line 1280201
    invoke-static {v0, v1}, LX/7yn;->c(LX/7yn;I)I

    move-result v3

    iget-object v7, v0, LX/7yn;->c:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v7

    move v1, v3

    .line 1280202
    int-to-float v1, v1

    float-to-int v3, v5

    .line 1280203
    invoke-static {v0, v3}, LX/7yn;->d(LX/7yn;I)I

    move-result v7

    iget-object v8, v0, LX/7yn;->c:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v8

    move v3, v7

    .line 1280204
    int-to-float v3, v3

    invoke-virtual {p1, v1, v3}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1280205
    :goto_2
    iget-object v1, v0, LX/7yn;->a:Landroid/view/View;

    move-object v0, v1

    .line 1280206
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1280207
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1280208
    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1280209
    :cond_4
    invoke-direct {p0}, LX/7yl;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1280210
    iget-object v0, p0, LX/7yl;->j:LX/7yo;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    goto :goto_0

    .line 1280211
    :pswitch_0
    float-to-int v0, v4

    iput v0, p0, LX/7yl;->c:I

    .line 1280212
    float-to-int v0, v5

    iput v0, p0, LX/7yl;->d:I

    .line 1280213
    iget-object v3, p0, LX/7yl;->g:LX/7yk;

    iget-object v0, p0, LX/7yl;->j:LX/7yo;

    check-cast v0, Landroid/view/ViewGroup;

    iget v7, p0, LX/7yl;->c:I

    iget v8, p0, LX/7yl;->d:I

    invoke-virtual {v3, v0, v7, v8}, LX/7yk;->a(Landroid/view/ViewGroup;II)LX/7yn;

    move-result-object v0

    iput-object v0, p0, LX/7yl;->h:LX/7yn;

    .line 1280214
    if-eqz v0, :cond_3

    .line 1280215
    iget-object v3, v0, LX/7yn;->b:Landroid/view/ViewGroup;

    move-object v3, v3

    .line 1280216
    if-eqz v3, :cond_3

    .line 1280217
    invoke-direct {p0}, LX/7yl;->e()V

    goto :goto_1

    .line 1280218
    :pswitch_1
    if-eqz v3, :cond_8

    .line 1280219
    float-to-int v0, v4

    float-to-int v7, v5

    invoke-direct {p0, v0, v7}, LX/7yl;->a(II)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    move v1, v0

    .line 1280220
    :goto_4
    iput-object v9, p0, LX/7yl;->h:LX/7yn;

    move v2, v0

    move-object v0, v3

    .line 1280221
    goto :goto_1

    :cond_5
    move v0, v2

    .line 1280222
    goto :goto_3

    .line 1280223
    :pswitch_2
    if-eqz v3, :cond_2

    .line 1280224
    float-to-int v0, v4

    float-to-int v7, v5

    invoke-direct {p0, v0, v7}, LX/7yl;->a(II)Z

    move-result v0

    if-nez v0, :cond_6

    :goto_5
    move-object v0, v3

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_5

    .line 1280225
    :cond_7
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1280226
    iget v1, p0, LX/7yl;->f:I

    int-to-float v1, v1

    iget v3, p0, LX/7yl;->f:I

    int-to-float v3, v3

    invoke-virtual {p1, v1, v3}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1280227
    iput-object v9, p0, LX/7yl;->h:LX/7yn;

    goto :goto_2

    :cond_8
    move v0, v2

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1280185
    const/4 v0, 0x0

    iput-object v0, p0, LX/7yl;->h:LX/7yn;

    .line 1280186
    iget-object v0, p0, LX/7yl;->i:LX/7ym;

    iget-object v1, p0, LX/7yl;->j:LX/7yo;

    .line 1280187
    iget-object p0, v0, LX/7ym;->b:Ljava/util/Set;

    invoke-interface {p0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1280188
    return-void
.end method
