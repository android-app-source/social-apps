.class public LX/6z5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wl;


# instance fields
.field private a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1160771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160772
    iput-object p1, p0, LX/6z5;->a:Landroid/content/res/Resources;

    .line 1160773
    return-void
.end method

.method public static b(LX/0QB;)LX/6z5;
    .locals 2

    .prologue
    .line 1160774
    new-instance v1, LX/6z5;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/6z5;-><init>(Landroid/content/res/Resources;)V

    .line 1160775
    return-object v1
.end method


# virtual methods
.method public final a(LX/6z8;)Z
    .locals 2

    .prologue
    .line 1160776
    invoke-interface {p1}, LX/6z8;->a()Ljava/lang/String;

    move-result-object v0

    .line 1160777
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1160778
    const/4 v0, 0x0

    .line 1160779
    :goto_0
    return v0

    :cond_0
    const-string v1, "\\d{5}"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(LX/6z8;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1160780
    iget-object v0, p0, LX/6z5;->a:Landroid/content/res/Resources;

    const v1, 0x7f081e73

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
