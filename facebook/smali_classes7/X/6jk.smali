.class public LX/6jk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;


# instance fields
.field public final attachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6jZ;",
            ">;"
        }
    .end annotation
.end field

.field public final body:Ljava/lang/String;

.field public final messageMetadatas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6kn;",
            ">;"
        }
    .end annotation
.end field

.field public final stickerId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xf

    const/4 v4, 0x1

    .line 1132386
    new-instance v0, LX/1sv;

    const-string v1, "DeltaBroadcastMessage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jk;->b:LX/1sv;

    .line 1132387
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadatas"

    invoke-direct {v0, v1, v5, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jk;->c:LX/1sw;

    .line 1132388
    new-instance v0, LX/1sw;

    const-string v1, "body"

    const/16 v2, 0xb

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jk;->d:LX/1sw;

    .line 1132389
    new-instance v0, LX/1sw;

    const-string v1, "stickerId"

    const/16 v2, 0xa

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jk;->e:LX/1sw;

    .line 1132390
    new-instance v0, LX/1sw;

    const-string v1, "attachments"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jk;->f:LX/1sw;

    .line 1132391
    sput-boolean v4, LX/6jk;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6kn;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "LX/6jZ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1132392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1132393
    iput-object p1, p0, LX/6jk;->messageMetadatas:Ljava/util/List;

    .line 1132394
    iput-object p2, p0, LX/6jk;->body:Ljava/lang/String;

    .line 1132395
    iput-object p3, p0, LX/6jk;->stickerId:Ljava/lang/Long;

    .line 1132396
    iput-object p4, p0, LX/6jk;->attachments:Ljava/util/List;

    .line 1132397
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1132398
    if-eqz p2, :cond_6

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1132399
    :goto_0
    if-eqz p2, :cond_7

    const-string v0, "\n"

    move-object v3, v0

    .line 1132400
    :goto_1
    if-eqz p2, :cond_8

    const-string v0, " "

    .line 1132401
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "DeltaBroadcastMessage"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1132402
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132403
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132404
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132405
    const/4 v1, 0x1

    .line 1132406
    iget-object v6, p0, LX/6jk;->messageMetadatas:Ljava/util/List;

    if-eqz v6, :cond_0

    .line 1132407
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132408
    const-string v1, "messageMetadatas"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132409
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132410
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132411
    iget-object v1, p0, LX/6jk;->messageMetadatas:Ljava/util/List;

    if-nez v1, :cond_9

    .line 1132412
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1132413
    :cond_0
    iget-object v6, p0, LX/6jk;->body:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 1132414
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132415
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132416
    const-string v1, "body"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132417
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132418
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132419
    iget-object v1, p0, LX/6jk;->body:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 1132420
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 1132421
    :cond_2
    iget-object v6, p0, LX/6jk;->stickerId:Ljava/lang/Long;

    if-eqz v6, :cond_d

    .line 1132422
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132423
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132424
    const-string v1, "stickerId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132425
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132426
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132427
    iget-object v1, p0, LX/6jk;->stickerId:Ljava/lang/Long;

    if-nez v1, :cond_b

    .line 1132428
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132429
    :goto_5
    iget-object v1, p0, LX/6jk;->attachments:Ljava/util/List;

    if-eqz v1, :cond_5

    .line 1132430
    if-nez v2, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132431
    :cond_4
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132432
    const-string v1, "attachments"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132433
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132434
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132435
    iget-object v0, p0, LX/6jk;->attachments:Ljava/util/List;

    if-nez v0, :cond_c

    .line 1132436
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132437
    :cond_5
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132438
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132439
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1132440
    :cond_6
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1132441
    :cond_7
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1132442
    :cond_8
    const-string v0, ""

    goto/16 :goto_2

    .line 1132443
    :cond_9
    iget-object v1, p0, LX/6jk;->messageMetadatas:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1132444
    :cond_a
    iget-object v1, p0, LX/6jk;->body:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1132445
    :cond_b
    iget-object v1, p0, LX/6jk;->stickerId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1132446
    :cond_c
    iget-object v0, p0, LX/6jk;->attachments:Ljava/util/List;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    :cond_d
    move v2, v1

    goto/16 :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    const/16 v2, 0xc

    .line 1132447
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1132448
    iget-object v0, p0, LX/6jk;->messageMetadatas:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1132449
    iget-object v0, p0, LX/6jk;->messageMetadatas:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1132450
    sget-object v0, LX/6jk;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132451
    new-instance v0, LX/1u3;

    iget-object v1, p0, LX/6jk;->messageMetadatas:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v2, v1}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1132452
    iget-object v0, p0, LX/6jk;->messageMetadatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kn;

    .line 1132453
    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    goto :goto_0

    .line 1132454
    :cond_0
    iget-object v0, p0, LX/6jk;->body:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1132455
    iget-object v0, p0, LX/6jk;->body:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1132456
    sget-object v0, LX/6jk;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132457
    iget-object v0, p0, LX/6jk;->body:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1132458
    :cond_1
    iget-object v0, p0, LX/6jk;->stickerId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1132459
    iget-object v0, p0, LX/6jk;->stickerId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1132460
    sget-object v0, LX/6jk;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132461
    iget-object v0, p0, LX/6jk;->stickerId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1132462
    :cond_2
    iget-object v0, p0, LX/6jk;->attachments:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 1132463
    iget-object v0, p0, LX/6jk;->attachments:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 1132464
    sget-object v0, LX/6jk;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132465
    new-instance v0, LX/1u3;

    iget-object v1, p0, LX/6jk;->attachments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v2, v1}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1132466
    iget-object v0, p0, LX/6jk;->attachments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6jZ;

    .line 1132467
    invoke-virtual {v0, p1}, LX/6jZ;->a(LX/1su;)V

    goto :goto_1

    .line 1132468
    :cond_3
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1132469
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1132470
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1132471
    if-nez p1, :cond_1

    .line 1132472
    :cond_0
    :goto_0
    return v0

    .line 1132473
    :cond_1
    instance-of v1, p1, LX/6jk;

    if-eqz v1, :cond_0

    .line 1132474
    check-cast p1, LX/6jk;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1132475
    if-nez p1, :cond_3

    .line 1132476
    :cond_2
    :goto_1
    move v0, v2

    .line 1132477
    goto :goto_0

    .line 1132478
    :cond_3
    iget-object v0, p0, LX/6jk;->messageMetadatas:Ljava/util/List;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1132479
    :goto_2
    iget-object v3, p1, LX/6jk;->messageMetadatas:Ljava/util/List;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1132480
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1132481
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132482
    iget-object v0, p0, LX/6jk;->messageMetadatas:Ljava/util/List;

    iget-object v3, p1, LX/6jk;->messageMetadatas:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132483
    :cond_5
    iget-object v0, p0, LX/6jk;->body:Ljava/lang/String;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1132484
    :goto_4
    iget-object v3, p1, LX/6jk;->body:Ljava/lang/String;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1132485
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1132486
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132487
    iget-object v0, p0, LX/6jk;->body:Ljava/lang/String;

    iget-object v3, p1, LX/6jk;->body:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132488
    :cond_7
    iget-object v0, p0, LX/6jk;->stickerId:Ljava/lang/Long;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1132489
    :goto_6
    iget-object v3, p1, LX/6jk;->stickerId:Ljava/lang/Long;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1132490
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1132491
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132492
    iget-object v0, p0, LX/6jk;->stickerId:Ljava/lang/Long;

    iget-object v3, p1, LX/6jk;->stickerId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132493
    :cond_9
    iget-object v0, p0, LX/6jk;->attachments:Ljava/util/List;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1132494
    :goto_8
    iget-object v3, p1, LX/6jk;->attachments:Ljava/util/List;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1132495
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1132496
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132497
    iget-object v0, p0, LX/6jk;->attachments:Ljava/util/List;

    iget-object v3, p1, LX/6jk;->attachments:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_b
    move v2, v1

    .line 1132498
    goto :goto_1

    :cond_c
    move v0, v2

    .line 1132499
    goto :goto_2

    :cond_d
    move v3, v2

    .line 1132500
    goto :goto_3

    :cond_e
    move v0, v2

    .line 1132501
    goto :goto_4

    :cond_f
    move v3, v2

    .line 1132502
    goto :goto_5

    :cond_10
    move v0, v2

    .line 1132503
    goto :goto_6

    :cond_11
    move v3, v2

    .line 1132504
    goto :goto_7

    :cond_12
    move v0, v2

    .line 1132505
    goto :goto_8

    :cond_13
    move v3, v2

    .line 1132506
    goto :goto_9
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1132507
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1132508
    sget-boolean v0, LX/6jk;->a:Z

    .line 1132509
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jk;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1132510
    return-object v0
.end method
