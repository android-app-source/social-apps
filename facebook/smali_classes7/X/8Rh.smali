.class public final LX/8Rh;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/model/SelectablePrivacyData;

.field public final synthetic b:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 0

    .prologue
    .line 1344774
    iput-object p1, p0, LX/8Rh;->b:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iput-object p2, p0, LX/8Rh;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method private a()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1344775
    iget-object v0, p0, LX/8Rh;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8Rh;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0}, Lcom/facebook/privacy/model/SelectablePrivacyData;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1344776
    invoke-direct {p0}, LX/8Rh;->a()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
