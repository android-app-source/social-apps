.class public abstract LX/7cR;
.super Landroid/os/Binder;
.source ""

# interfaces
.implements LX/7cP;


# direct methods
.method public static a(Landroid/os/IBinder;)LX/7cP;
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.maps.model.internal.IIndoorBuildingDelegate"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, LX/7cP;

    if-eqz v1, :cond_1

    check-cast v0, LX/7cP;

    goto :goto_0

    :cond_1
    new-instance v0, LX/7cQ;

    invoke-direct {v0, p0}, LX/7cQ;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method
