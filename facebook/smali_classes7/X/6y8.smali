.class public LX/6y8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:I

.field public final d:I

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Lcom/facebook/common/locale/Country;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/6y9;)V
    .locals 2

    .prologue
    .line 1159532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159533
    iget-object v0, p1, LX/6y9;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1159534
    iput-object v0, p0, LX/6y8;->a:Ljava/lang/String;

    .line 1159535
    iget-object v0, p1, LX/6y9;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-object v0, v0

    .line 1159536
    if-nez v0, :cond_0

    iget-object v0, p0, LX/6y8;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6y8;->a:Ljava/lang/String;

    invoke-static {v0}, LX/6yU;->a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/6y8;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    .line 1159537
    iget-object v0, p1, LX/6y9;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1159538
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1159539
    iget v0, p1, LX/6y9;->d:I

    move v0, v0

    .line 1159540
    iput v0, p0, LX/6y8;->c:I

    .line 1159541
    iget v0, p1, LX/6y9;->e:I

    move v0, v0

    .line 1159542
    iput v0, p0, LX/6y8;->d:I

    .line 1159543
    :goto_1
    iget-object v0, p1, LX/6y9;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1159544
    iput-object v0, p0, LX/6y8;->e:Ljava/lang/String;

    .line 1159545
    iget-object v0, p1, LX/6y9;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1159546
    iput-object v0, p0, LX/6y8;->f:Ljava/lang/String;

    .line 1159547
    iget-object v0, p1, LX/6y9;->h:Lcom/facebook/common/locale/Country;

    move-object v0, v0

    .line 1159548
    iput-object v0, p0, LX/6y8;->g:Lcom/facebook/common/locale/Country;

    .line 1159549
    return-void

    .line 1159550
    :cond_0
    iget-object v0, p1, LX/6y9;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-object v0, v0

    .line 1159551
    goto :goto_0

    .line 1159552
    :cond_1
    iget-object v0, p1, LX/6y9;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1159553
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1159554
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, LX/6y8;->c:I

    .line 1159555
    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6y8;->d:I

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZLcom/facebook/common/locale/Country;)Lcom/facebook/payments/paymentmethods/model/CreditCard;
    .locals 15

    .prologue
    .line 1159556
    iget-object v1, p0, LX/6y8;->a:Ljava/lang/String;

    invoke-static {v1}, LX/6yU;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1159557
    new-instance v1, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    iget v3, p0, LX/6y8;->c:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, LX/6y8;->d:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-static {v2, v5}, LX/0YN;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x6

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, LX/6y8;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    iget-object v2, p0, LX/6y8;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    invoke-virtual {v2}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getHumanReadableName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    iget-object v2, p0, LX/6y8;->f:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v13, 0x1

    :goto_0
    new-instance v14, Lcom/facebook/payments/paymentmethods/model/BillingAddress;

    iget-object v2, p0, LX/6y8;->f:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-direct {v14, v2, v0}, Lcom/facebook/payments/paymentmethods/model/BillingAddress;-><init>(Ljava/lang/String;Lcom/facebook/common/locale/Country;)V

    move-object/from16 v2, p1

    move/from16 v12, p2

    invoke-direct/range {v1 .. v14}, Lcom/facebook/payments/paymentmethods/model/CreditCard;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Ljava/lang/String;Ljava/lang/String;ZZZZLcom/facebook/payments/paymentmethods/model/BillingAddress;)V

    return-object v1

    :cond_0
    const/4 v13, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1159529
    iget-object v0, p0, LX/6y8;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    if-nez v0, :cond_0

    .line 1159530
    const/4 v0, 0x0

    .line 1159531
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6y8;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getHumanReadableName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
