.class public final LX/8J6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 48

    .prologue
    .line 1327542
    const/16 v44, 0x0

    .line 1327543
    const/16 v43, 0x0

    .line 1327544
    const/16 v42, 0x0

    .line 1327545
    const/16 v41, 0x0

    .line 1327546
    const/16 v40, 0x0

    .line 1327547
    const/16 v39, 0x0

    .line 1327548
    const/16 v38, 0x0

    .line 1327549
    const/16 v37, 0x0

    .line 1327550
    const/16 v36, 0x0

    .line 1327551
    const/16 v35, 0x0

    .line 1327552
    const/16 v34, 0x0

    .line 1327553
    const/16 v33, 0x0

    .line 1327554
    const/16 v32, 0x0

    .line 1327555
    const/16 v31, 0x0

    .line 1327556
    const/16 v30, 0x0

    .line 1327557
    const/16 v29, 0x0

    .line 1327558
    const/16 v28, 0x0

    .line 1327559
    const/16 v27, 0x0

    .line 1327560
    const/16 v26, 0x0

    .line 1327561
    const/16 v25, 0x0

    .line 1327562
    const/16 v24, 0x0

    .line 1327563
    const/16 v23, 0x0

    .line 1327564
    const/16 v22, 0x0

    .line 1327565
    const/16 v21, 0x0

    .line 1327566
    const/16 v20, 0x0

    .line 1327567
    const/16 v19, 0x0

    .line 1327568
    const/16 v18, 0x0

    .line 1327569
    const/16 v17, 0x0

    .line 1327570
    const/16 v16, 0x0

    .line 1327571
    const/4 v15, 0x0

    .line 1327572
    const/4 v14, 0x0

    .line 1327573
    const/4 v13, 0x0

    .line 1327574
    const/4 v12, 0x0

    .line 1327575
    const/4 v11, 0x0

    .line 1327576
    const/4 v10, 0x0

    .line 1327577
    const/4 v9, 0x0

    .line 1327578
    const/4 v8, 0x0

    .line 1327579
    const/4 v7, 0x0

    .line 1327580
    const/4 v6, 0x0

    .line 1327581
    const/4 v5, 0x0

    .line 1327582
    const/4 v4, 0x0

    .line 1327583
    const/4 v3, 0x0

    .line 1327584
    const/4 v2, 0x0

    .line 1327585
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v45

    sget-object v46, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    if-eq v0, v1, :cond_1

    .line 1327586
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1327587
    const/4 v2, 0x0

    .line 1327588
    :goto_0
    return v2

    .line 1327589
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1327590
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v45

    sget-object v46, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    if-eq v0, v1, :cond_1f

    .line 1327591
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v45

    .line 1327592
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1327593
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v46

    sget-object v47, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v46

    move-object/from16 v1, v47

    if-eq v0, v1, :cond_1

    if-eqz v45, :cond_1

    .line 1327594
    const-string v46, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_2

    .line 1327595
    const/4 v14, 0x1

    .line 1327596
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v44

    goto :goto_1

    .line 1327597
    :cond_2
    const-string v46, "can_see_voice_switcher"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_3

    .line 1327598
    const/4 v13, 0x1

    .line 1327599
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v43

    goto :goto_1

    .line 1327600
    :cond_3
    const-string v46, "can_viewer_comment"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_4

    .line 1327601
    const/4 v12, 0x1

    .line 1327602
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v42

    goto :goto_1

    .line 1327603
    :cond_4
    const-string v46, "can_viewer_comment_with_photo"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_5

    .line 1327604
    const/4 v11, 0x1

    .line 1327605
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v41

    goto :goto_1

    .line 1327606
    :cond_5
    const-string v46, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_6

    .line 1327607
    const/4 v10, 0x1

    .line 1327608
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v40

    goto :goto_1

    .line 1327609
    :cond_6
    const-string v46, "can_viewer_comment_with_video"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_7

    .line 1327610
    const/4 v9, 0x1

    .line 1327611
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 1327612
    :cond_7
    const-string v46, "can_viewer_like"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_8

    .line 1327613
    const/4 v8, 0x1

    .line 1327614
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto/16 :goto_1

    .line 1327615
    :cond_8
    const-string v46, "can_viewer_react"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_9

    .line 1327616
    const/4 v7, 0x1

    .line 1327617
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto/16 :goto_1

    .line 1327618
    :cond_9
    const-string v46, "can_viewer_subscribe"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_a

    .line 1327619
    const/4 v6, 0x1

    .line 1327620
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto/16 :goto_1

    .line 1327621
    :cond_a
    const-string v46, "comments_mirroring_domain"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_b

    .line 1327622
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    goto/16 :goto_1

    .line 1327623
    :cond_b
    const-string v46, "default_comment_ordering"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_c

    .line 1327624
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v34

    goto/16 :goto_1

    .line 1327625
    :cond_c
    const-string v46, "does_viewer_like"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_d

    .line 1327626
    const/4 v5, 0x1

    .line 1327627
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto/16 :goto_1

    .line 1327628
    :cond_d
    const-string v46, "id"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_e

    .line 1327629
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    goto/16 :goto_1

    .line 1327630
    :cond_e
    const-string v46, "important_reactors"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_f

    .line 1327631
    invoke-static/range {p0 .. p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v31

    goto/16 :goto_1

    .line 1327632
    :cond_f
    const-string v46, "is_viewer_subscribed"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_10

    .line 1327633
    const/4 v4, 0x1

    .line 1327634
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v30

    goto/16 :goto_1

    .line 1327635
    :cond_10
    const-string v46, "legacy_api_post_id"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_11

    .line 1327636
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    goto/16 :goto_1

    .line 1327637
    :cond_11
    const-string v46, "likers"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_12

    .line 1327638
    invoke-static/range {p0 .. p1}, LX/5Ac;->a(LX/15w;LX/186;)I

    move-result v28

    goto/16 :goto_1

    .line 1327639
    :cond_12
    const-string v46, "reactors"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_13

    .line 1327640
    invoke-static/range {p0 .. p1}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v27

    goto/16 :goto_1

    .line 1327641
    :cond_13
    const-string v46, "real_time_activity_info"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_14

    .line 1327642
    invoke-static/range {p0 .. p1}, LX/5Ab;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 1327643
    :cond_14
    const-string v46, "remixable_photo_uri"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_15

    .line 1327644
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto/16 :goto_1

    .line 1327645
    :cond_15
    const-string v46, "reshares"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_16

    .line 1327646
    invoke-static/range {p0 .. p1}, LX/5Ad;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 1327647
    :cond_16
    const-string v46, "should_use_likers_sentence"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_17

    .line 1327648
    const/4 v3, 0x1

    .line 1327649
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto/16 :goto_1

    .line 1327650
    :cond_17
    const-string v46, "supported_reactions"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_18

    .line 1327651
    invoke-static/range {p0 .. p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 1327652
    :cond_18
    const-string v46, "top_level_comments"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_19

    .line 1327653
    invoke-static/range {p0 .. p1}, LX/5Ae;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 1327654
    :cond_19
    const-string v46, "top_reactions"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1a

    .line 1327655
    invoke-static/range {p0 .. p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 1327656
    :cond_1a
    const-string v46, "viewer_acts_as_page"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1b

    .line 1327657
    invoke-static/range {p0 .. p1}, LX/5AX;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1327658
    :cond_1b
    const-string v46, "viewer_acts_as_person"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1c

    .line 1327659
    invoke-static/range {p0 .. p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 1327660
    :cond_1c
    const-string v46, "viewer_does_not_like_sentence"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1d

    .line 1327661
    invoke-static/range {p0 .. p1}, LX/8J4;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 1327662
    :cond_1d
    const-string v46, "viewer_feedback_reaction_key"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1e

    .line 1327663
    const/4 v2, 0x1

    .line 1327664
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    goto/16 :goto_1

    .line 1327665
    :cond_1e
    const-string v46, "viewer_likes_sentence"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_0

    .line 1327666
    invoke-static/range {p0 .. p1}, LX/8J5;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1327667
    :cond_1f
    const/16 v45, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1327668
    if-eqz v14, :cond_20

    .line 1327669
    const/4 v14, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v14, v1}, LX/186;->a(IZ)V

    .line 1327670
    :cond_20
    if-eqz v13, :cond_21

    .line 1327671
    const/4 v13, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 1327672
    :cond_21
    if-eqz v12, :cond_22

    .line 1327673
    const/4 v12, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 1327674
    :cond_22
    if-eqz v11, :cond_23

    .line 1327675
    const/4 v11, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 1327676
    :cond_23
    if-eqz v10, :cond_24

    .line 1327677
    const/4 v10, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 1327678
    :cond_24
    if-eqz v9, :cond_25

    .line 1327679
    const/4 v9, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 1327680
    :cond_25
    if-eqz v8, :cond_26

    .line 1327681
    const/4 v8, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 1327682
    :cond_26
    if-eqz v7, :cond_27

    .line 1327683
    const/4 v7, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1327684
    :cond_27
    if-eqz v6, :cond_28

    .line 1327685
    const/16 v6, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1327686
    :cond_28
    const/16 v6, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1327687
    const/16 v6, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1327688
    if-eqz v5, :cond_29

    .line 1327689
    const/16 v5, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1327690
    :cond_29
    const/16 v5, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1327691
    const/16 v5, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1327692
    if-eqz v4, :cond_2a

    .line 1327693
    const/16 v4, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1327694
    :cond_2a
    const/16 v4, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1327695
    const/16 v4, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1327696
    const/16 v4, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1327697
    const/16 v4, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1327698
    const/16 v4, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1327699
    const/16 v4, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1327700
    if-eqz v3, :cond_2b

    .line 1327701
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1327702
    :cond_2b
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1327703
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1327704
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1327705
    const/16 v3, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1327706
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1327707
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1327708
    if-eqz v2, :cond_2c

    .line 1327709
    const/16 v2, 0x1c

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1327710
    :cond_2c
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1327711
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1327712
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1327713
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 1327714
    if-eqz v0, :cond_0

    .line 1327715
    const-string v1, "can_page_viewer_invite_post_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327716
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1327717
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1327718
    if-eqz v0, :cond_1

    .line 1327719
    const-string v1, "can_see_voice_switcher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327720
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1327721
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1327722
    if-eqz v0, :cond_2

    .line 1327723
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327724
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1327725
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1327726
    if-eqz v0, :cond_3

    .line 1327727
    const-string v1, "can_viewer_comment_with_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327728
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1327729
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1327730
    if-eqz v0, :cond_4

    .line 1327731
    const-string v1, "can_viewer_comment_with_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327732
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1327733
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1327734
    if-eqz v0, :cond_5

    .line 1327735
    const-string v1, "can_viewer_comment_with_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327736
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1327737
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1327738
    if-eqz v0, :cond_6

    .line 1327739
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327740
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1327741
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1327742
    if-eqz v0, :cond_7

    .line 1327743
    const-string v1, "can_viewer_react"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327744
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1327745
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1327746
    if-eqz v0, :cond_8

    .line 1327747
    const-string v1, "can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327748
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1327749
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1327750
    if-eqz v0, :cond_9

    .line 1327751
    const-string v1, "comments_mirroring_domain"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327752
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1327753
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1327754
    if-eqz v0, :cond_a

    .line 1327755
    const-string v1, "default_comment_ordering"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327756
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1327757
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1327758
    if-eqz v0, :cond_b

    .line 1327759
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327760
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1327761
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1327762
    if-eqz v0, :cond_c

    .line 1327763
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327764
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1327765
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327766
    if-eqz v0, :cond_d

    .line 1327767
    const-string v1, "important_reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327768
    invoke-static {p0, v0, p2, p3}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1327769
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1327770
    if-eqz v0, :cond_e

    .line 1327771
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327772
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1327773
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1327774
    if-eqz v0, :cond_f

    .line 1327775
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327776
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1327777
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327778
    if-eqz v0, :cond_10

    .line 1327779
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327780
    invoke-static {p0, v0, p2}, LX/5Ac;->a(LX/15i;ILX/0nX;)V

    .line 1327781
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327782
    if-eqz v0, :cond_11

    .line 1327783
    const-string v1, "reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327784
    invoke-static {p0, v0, p2}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 1327785
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327786
    if-eqz v0, :cond_12

    .line 1327787
    const-string v1, "real_time_activity_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327788
    invoke-static {p0, v0, p2, p3}, LX/5Ab;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1327789
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1327790
    if-eqz v0, :cond_13

    .line 1327791
    const-string v1, "remixable_photo_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327792
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1327793
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327794
    if-eqz v0, :cond_14

    .line 1327795
    const-string v1, "reshares"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327796
    invoke-static {p0, v0, p2}, LX/5Ad;->a(LX/15i;ILX/0nX;)V

    .line 1327797
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1327798
    if-eqz v0, :cond_15

    .line 1327799
    const-string v1, "should_use_likers_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327800
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1327801
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327802
    if-eqz v0, :cond_16

    .line 1327803
    const-string v1, "supported_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327804
    invoke-static {p0, v0, p2, p3}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1327805
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327806
    if-eqz v0, :cond_17

    .line 1327807
    const-string v1, "top_level_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327808
    invoke-static {p0, v0, p2}, LX/5Ae;->a(LX/15i;ILX/0nX;)V

    .line 1327809
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327810
    if-eqz v0, :cond_18

    .line 1327811
    const-string v1, "top_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327812
    invoke-static {p0, v0, p2, p3}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1327813
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327814
    if-eqz v0, :cond_19

    .line 1327815
    const-string v1, "viewer_acts_as_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327816
    invoke-static {p0, v0, p2, p3}, LX/5AX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1327817
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327818
    if-eqz v0, :cond_1a

    .line 1327819
    const-string v1, "viewer_acts_as_person"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327820
    invoke-static {p0, v0, p2}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 1327821
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327822
    if-eqz v0, :cond_1b

    .line 1327823
    const-string v1, "viewer_does_not_like_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327824
    invoke-static {p0, v0, p2}, LX/8J4;->a(LX/15i;ILX/0nX;)V

    .line 1327825
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1327826
    if-eqz v0, :cond_1c

    .line 1327827
    const-string v1, "viewer_feedback_reaction_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327828
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1327829
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327830
    if-eqz v0, :cond_1d

    .line 1327831
    const-string v1, "viewer_likes_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327832
    invoke-static {p0, v0, p2}, LX/8J5;->a(LX/15i;ILX/0nX;)V

    .line 1327833
    :cond_1d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1327834
    return-void
.end method
