.class public final enum LX/7IM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7IM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7IM;

.field public static final enum LIVE_POLLER_BATCH_FAIL:LX/7IM;

.field public static final enum LIVE_POLLER_BATCH_START:LX/7IM;

.field public static final enum LIVE_POLLER_FAIL:LX/7IM;

.field public static final enum LIVE_POLLER_START:LX/7IM;

.field public static final enum LIVE_POLLER_SUCCEED:LX/7IM;

.field public static final enum LIVE_SUBSCRIPTION_QUERY_FAIL:LX/7IM;

.field public static final enum LIVE_SUBSCRIPTION_QUERY_START:LX/7IM;

.field public static final enum LIVE_SUBSCRIPTION_UPDATE:LX/7IM;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1191625
    new-instance v0, LX/7IM;

    const-string v1, "LIVE_POLLER_START"

    invoke-direct {v0, v1, v3}, LX/7IM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IM;->LIVE_POLLER_START:LX/7IM;

    .line 1191626
    new-instance v0, LX/7IM;

    const-string v1, "LIVE_POLLER_SUCCEED"

    invoke-direct {v0, v1, v4}, LX/7IM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IM;->LIVE_POLLER_SUCCEED:LX/7IM;

    .line 1191627
    new-instance v0, LX/7IM;

    const-string v1, "LIVE_POLLER_FAIL"

    invoke-direct {v0, v1, v5}, LX/7IM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IM;->LIVE_POLLER_FAIL:LX/7IM;

    .line 1191628
    new-instance v0, LX/7IM;

    const-string v1, "LIVE_POLLER_BATCH_START"

    invoke-direct {v0, v1, v6}, LX/7IM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IM;->LIVE_POLLER_BATCH_START:LX/7IM;

    .line 1191629
    new-instance v0, LX/7IM;

    const-string v1, "LIVE_POLLER_BATCH_FAIL"

    invoke-direct {v0, v1, v7}, LX/7IM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IM;->LIVE_POLLER_BATCH_FAIL:LX/7IM;

    .line 1191630
    new-instance v0, LX/7IM;

    const-string v1, "LIVE_SUBSCRIPTION_UPDATE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7IM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IM;->LIVE_SUBSCRIPTION_UPDATE:LX/7IM;

    .line 1191631
    new-instance v0, LX/7IM;

    const-string v1, "LIVE_SUBSCRIPTION_QUERY_START"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/7IM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IM;->LIVE_SUBSCRIPTION_QUERY_START:LX/7IM;

    .line 1191632
    new-instance v0, LX/7IM;

    const-string v1, "LIVE_SUBSCRIPTION_QUERY_FAIL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/7IM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IM;->LIVE_SUBSCRIPTION_QUERY_FAIL:LX/7IM;

    .line 1191633
    const/16 v0, 0x8

    new-array v0, v0, [LX/7IM;

    sget-object v1, LX/7IM;->LIVE_POLLER_START:LX/7IM;

    aput-object v1, v0, v3

    sget-object v1, LX/7IM;->LIVE_POLLER_SUCCEED:LX/7IM;

    aput-object v1, v0, v4

    sget-object v1, LX/7IM;->LIVE_POLLER_FAIL:LX/7IM;

    aput-object v1, v0, v5

    sget-object v1, LX/7IM;->LIVE_POLLER_BATCH_START:LX/7IM;

    aput-object v1, v0, v6

    sget-object v1, LX/7IM;->LIVE_POLLER_BATCH_FAIL:LX/7IM;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7IM;->LIVE_SUBSCRIPTION_UPDATE:LX/7IM;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7IM;->LIVE_SUBSCRIPTION_QUERY_START:LX/7IM;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7IM;->LIVE_SUBSCRIPTION_QUERY_FAIL:LX/7IM;

    aput-object v2, v0, v1

    sput-object v0, LX/7IM;->$VALUES:[LX/7IM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1191634
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7IM;
    .locals 1

    .prologue
    .line 1191635
    const-class v0, LX/7IM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7IM;

    return-object v0
.end method

.method public static values()[LX/7IM;
    .locals 1

    .prologue
    .line 1191636
    sget-object v0, LX/7IM;->$VALUES:[LX/7IM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7IM;

    return-object v0
.end method
