.class public final LX/87p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DIB;

.field public final synthetic b:LX/87q;


# direct methods
.method public constructor <init>(LX/87q;LX/DIB;)V
    .locals 0

    .prologue
    .line 1300648
    iput-object p1, p0, LX/87p;->b:LX/87q;

    iput-object p2, p0, LX/87p;->a:LX/DIB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1300656
    iget-object v0, p0, LX/87p;->a:LX/DIB;

    .line 1300657
    iget-object v1, v0, LX/DIB;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v1, v1, Lcom/facebook/goodfriends/audience/AudienceFragment;->o:LX/4BY;

    invoke-virtual {v1}, LX/4BY;->hide()V

    .line 1300658
    sget-object v1, Lcom/facebook/goodfriends/audience/AudienceFragment;->a:Ljava/lang/String;

    const-string p0, "Error while mutation"

    invoke-static {v1, p0, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1300659
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1300649
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1300650
    if-nez p1, :cond_0

    .line 1300651
    :goto_0
    return-void

    .line 1300652
    :cond_0
    iget-object v0, p0, LX/87p;->a:LX/DIB;

    .line 1300653
    iget-object v1, v0, LX/DIB;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v1, v1, Lcom/facebook/goodfriends/audience/AudienceFragment;->o:LX/4BY;

    invoke-virtual {v1}, LX/4BY;->hide()V

    .line 1300654
    iget-object v1, v0, LX/DIB;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    sget-object p0, LX/9J1;->PICKER_CLOSE:LX/9J1;

    const/4 p1, -0x1

    invoke-static {v1, p0, p1}, Lcom/facebook/goodfriends/audience/AudienceFragment;->a$redex0(Lcom/facebook/goodfriends/audience/AudienceFragment;LX/9J1;I)V

    .line 1300655
    goto :goto_0
.end method
