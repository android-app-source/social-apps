.class public LX/6jp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final messageMetadata:LX/6kn;

.field public final mutation:LX/6kb;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xc

    const/4 v3, 0x1

    .line 1133092
    new-instance v0, LX/1sv;

    const-string v1, "DeltaGenericMapMutation"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jp;->b:LX/1sv;

    .line 1133093
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jp;->c:LX/1sw;

    .line 1133094
    new-instance v0, LX/1sw;

    const-string v1, "mutation"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jp;->d:LX/1sw;

    .line 1133095
    sput-boolean v3, LX/6jp;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6kn;LX/6kb;)V
    .locals 0

    .prologue
    .line 1133088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1133089
    iput-object p1, p0, LX/6jp;->messageMetadata:LX/6kn;

    .line 1133090
    iput-object p2, p0, LX/6jp;->mutation:LX/6kb;

    .line 1133091
    return-void
.end method

.method public static a(LX/6jp;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1133055
    iget-object v0, p0, LX/6jp;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1133056
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jp;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1133057
    :cond_0
    iget-object v0, p0, LX/6jp;->mutation:LX/6kb;

    if-nez v0, :cond_1

    .line 1133058
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'mutation\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jp;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1133059
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1133060
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1133061
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1133062
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1133063
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaGenericMapMutation"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1133064
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133065
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133066
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133067
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133068
    const-string v4, "messageMetadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133069
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133070
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133071
    iget-object v4, p0, LX/6jp;->messageMetadata:LX/6kn;

    if-nez v4, :cond_3

    .line 1133072
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133073
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133074
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133075
    const-string v4, "mutation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133076
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133077
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133078
    iget-object v0, p0, LX/6jp;->mutation:LX/6kb;

    if-nez v0, :cond_4

    .line 1133079
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133080
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133081
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133082
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1133083
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1133084
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1133085
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1133086
    :cond_3
    iget-object v4, p0, LX/6jp;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1133087
    :cond_4
    iget-object v0, p0, LX/6jp;->mutation:LX/6kb;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1133096
    invoke-static {p0}, LX/6jp;->a(LX/6jp;)V

    .line 1133097
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1133098
    iget-object v0, p0, LX/6jp;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1133099
    sget-object v0, LX/6jp;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133100
    iget-object v0, p0, LX/6jp;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1133101
    :cond_0
    iget-object v0, p0, LX/6jp;->mutation:LX/6kb;

    if-eqz v0, :cond_1

    .line 1133102
    sget-object v0, LX/6jp;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133103
    iget-object v0, p0, LX/6jp;->mutation:LX/6kb;

    invoke-virtual {v0, p1}, LX/6kT;->a(LX/1su;)V

    .line 1133104
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1133105
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1133106
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1133029
    if-nez p1, :cond_1

    .line 1133030
    :cond_0
    :goto_0
    return v0

    .line 1133031
    :cond_1
    instance-of v1, p1, LX/6jp;

    if-eqz v1, :cond_0

    .line 1133032
    check-cast p1, LX/6jp;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1133033
    if-nez p1, :cond_3

    .line 1133034
    :cond_2
    :goto_1
    move v0, v2

    .line 1133035
    goto :goto_0

    .line 1133036
    :cond_3
    iget-object v0, p0, LX/6jp;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1133037
    :goto_2
    iget-object v3, p1, LX/6jp;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1133038
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1133039
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133040
    iget-object v0, p0, LX/6jp;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6jp;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133041
    :cond_5
    iget-object v0, p0, LX/6jp;->mutation:LX/6kb;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1133042
    :goto_4
    iget-object v3, p1, LX/6jp;->mutation:LX/6kb;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1133043
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1133044
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133045
    iget-object v0, p0, LX/6jp;->mutation:LX/6kb;

    iget-object v3, p1, LX/6jp;->mutation:LX/6kb;

    invoke-virtual {v0, v3}, LX/6kb;->a(LX/6kb;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1133046
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1133047
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1133048
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1133049
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1133050
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1133051
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1133052
    sget-boolean v0, LX/6jp;->a:Z

    .line 1133053
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jp;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1133054
    return-object v0
.end method
