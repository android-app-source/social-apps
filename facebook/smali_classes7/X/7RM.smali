.class public final LX/7RM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 33

    .prologue
    .line 1206297
    const/16 v29, 0x0

    .line 1206298
    const/16 v28, 0x0

    .line 1206299
    const/16 v27, 0x0

    .line 1206300
    const/16 v26, 0x0

    .line 1206301
    const/16 v25, 0x0

    .line 1206302
    const/16 v24, 0x0

    .line 1206303
    const/16 v23, 0x0

    .line 1206304
    const/16 v22, 0x0

    .line 1206305
    const/16 v21, 0x0

    .line 1206306
    const/16 v20, 0x0

    .line 1206307
    const/16 v19, 0x0

    .line 1206308
    const/16 v18, 0x0

    .line 1206309
    const/16 v17, 0x0

    .line 1206310
    const/16 v16, 0x0

    .line 1206311
    const/4 v15, 0x0

    .line 1206312
    const/4 v14, 0x0

    .line 1206313
    const/4 v13, 0x0

    .line 1206314
    const/4 v12, 0x0

    .line 1206315
    const/4 v11, 0x0

    .line 1206316
    const/4 v10, 0x0

    .line 1206317
    const/4 v9, 0x0

    .line 1206318
    const/4 v8, 0x0

    .line 1206319
    const/4 v7, 0x0

    .line 1206320
    const/4 v6, 0x0

    .line 1206321
    const/4 v5, 0x0

    .line 1206322
    const/4 v4, 0x0

    .line 1206323
    const/4 v3, 0x0

    .line 1206324
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_1

    .line 1206325
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1206326
    const/4 v3, 0x0

    .line 1206327
    :goto_0
    return v3

    .line 1206328
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1206329
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_13

    .line 1206330
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v30

    .line 1206331
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1206332
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v31

    sget-object v32, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    if-eq v0, v1, :cond_1

    if-eqz v30, :cond_1

    .line 1206333
    const-string v31, "__type__"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-nez v31, :cond_2

    const-string v31, "__typename"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_3

    .line 1206334
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v29

    goto :goto_1

    .line 1206335
    :cond_3
    const-string v31, "atom_size"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_4

    .line 1206336
    const/4 v12, 0x1

    .line 1206337
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v28

    goto :goto_1

    .line 1206338
    :cond_4
    const-string v31, "bitrate"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_5

    .line 1206339
    const/4 v11, 0x1

    .line 1206340
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v27

    goto :goto_1

    .line 1206341
    :cond_5
    const-string v31, "broadcast_status"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_6

    .line 1206342
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v26

    goto :goto_1

    .line 1206343
    :cond_6
    const-string v31, "hdAtomSize"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_7

    .line 1206344
    const/4 v10, 0x1

    .line 1206345
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v25

    goto :goto_1

    .line 1206346
    :cond_7
    const-string v31, "hdBitrate"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_8

    .line 1206347
    const/4 v9, 0x1

    .line 1206348
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v24

    goto/16 :goto_1

    .line 1206349
    :cond_8
    const-string v31, "height"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_9

    .line 1206350
    const/4 v8, 0x1

    .line 1206351
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v23

    goto/16 :goto_1

    .line 1206352
    :cond_9
    const-string v31, "id"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_a

    .line 1206353
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto/16 :goto_1

    .line 1206354
    :cond_a
    const-string v31, "is_live_streaming"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_b

    .line 1206355
    const/4 v7, 0x1

    .line 1206356
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto/16 :goto_1

    .line 1206357
    :cond_b
    const-string v31, "live_viewer_count"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_c

    .line 1206358
    const/4 v6, 0x1

    .line 1206359
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v20

    goto/16 :goto_1

    .line 1206360
    :cond_c
    const-string v31, "live_viewer_count_read_only"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_d

    .line 1206361
    const/4 v5, 0x1

    .line 1206362
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v19

    goto/16 :goto_1

    .line 1206363
    :cond_d
    const-string v31, "playableUrlHdString"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_e

    .line 1206364
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto/16 :goto_1

    .line 1206365
    :cond_e
    const-string v31, "playable_duration_in_ms"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_f

    .line 1206366
    const/4 v4, 0x1

    .line 1206367
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v17

    goto/16 :goto_1

    .line 1206368
    :cond_f
    const-string v31, "playable_url"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_10

    .line 1206369
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 1206370
    :cond_10
    const-string v31, "playlist"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_11

    .line 1206371
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 1206372
    :cond_11
    const-string v31, "preferredPlayableUrlString"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_12

    .line 1206373
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 1206374
    :cond_12
    const-string v31, "width"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_0

    .line 1206375
    const/4 v3, 0x1

    .line 1206376
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto/16 :goto_1

    .line 1206377
    :cond_13
    const/16 v30, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1206378
    const/16 v30, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1206379
    if-eqz v12, :cond_14

    .line 1206380
    const/4 v12, 0x1

    const/16 v29, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-virtual {v0, v12, v1, v2}, LX/186;->a(III)V

    .line 1206381
    :cond_14
    if-eqz v11, :cond_15

    .line 1206382
    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v11, v1, v12}, LX/186;->a(III)V

    .line 1206383
    :cond_15
    const/4 v11, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v11, v1}, LX/186;->b(II)V

    .line 1206384
    if-eqz v10, :cond_16

    .line 1206385
    const/4 v10, 0x4

    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v10, v1, v11}, LX/186;->a(III)V

    .line 1206386
    :cond_16
    if-eqz v9, :cond_17

    .line 1206387
    const/4 v9, 0x5

    const/4 v10, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v9, v1, v10}, LX/186;->a(III)V

    .line 1206388
    :cond_17
    if-eqz v8, :cond_18

    .line 1206389
    const/4 v8, 0x6

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v8, v1, v9}, LX/186;->a(III)V

    .line 1206390
    :cond_18
    const/4 v8, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1206391
    if-eqz v7, :cond_19

    .line 1206392
    const/16 v7, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1206393
    :cond_19
    if-eqz v6, :cond_1a

    .line 1206394
    const/16 v6, 0x9

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v6, v1, v7}, LX/186;->a(III)V

    .line 1206395
    :cond_1a
    if-eqz v5, :cond_1b

    .line 1206396
    const/16 v5, 0xa

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v5, v1, v6}, LX/186;->a(III)V

    .line 1206397
    :cond_1b
    const/16 v5, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1206398
    if-eqz v4, :cond_1c

    .line 1206399
    const/16 v4, 0xc

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 1206400
    :cond_1c
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1206401
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 1206402
    const/16 v4, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 1206403
    if-eqz v3, :cond_1d

    .line 1206404
    const/16 v3, 0x10

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13, v4}, LX/186;->a(III)V

    .line 1206405
    :cond_1d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 1206406
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1206407
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1206408
    if-eqz v0, :cond_0

    .line 1206409
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206410
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1206411
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1206412
    if-eqz v0, :cond_1

    .line 1206413
    const-string v1, "atom_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206414
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1206415
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1206416
    if-eqz v0, :cond_2

    .line 1206417
    const-string v1, "bitrate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206418
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1206419
    :cond_2
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1206420
    if-eqz v0, :cond_3

    .line 1206421
    const-string v0, "broadcast_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206422
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1206423
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1206424
    if-eqz v0, :cond_4

    .line 1206425
    const-string v1, "hdAtomSize"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206426
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1206427
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1206428
    if-eqz v0, :cond_5

    .line 1206429
    const-string v1, "hdBitrate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206430
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1206431
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1206432
    if-eqz v0, :cond_6

    .line 1206433
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206434
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1206435
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1206436
    if-eqz v0, :cond_7

    .line 1206437
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206438
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1206439
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1206440
    if-eqz v0, :cond_8

    .line 1206441
    const-string v1, "is_live_streaming"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206442
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1206443
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1206444
    if-eqz v0, :cond_9

    .line 1206445
    const-string v1, "live_viewer_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206446
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1206447
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1206448
    if-eqz v0, :cond_a

    .line 1206449
    const-string v1, "live_viewer_count_read_only"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206450
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1206451
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1206452
    if-eqz v0, :cond_b

    .line 1206453
    const-string v1, "playableUrlHdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206454
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1206455
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1206456
    if-eqz v0, :cond_c

    .line 1206457
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206458
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1206459
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1206460
    if-eqz v0, :cond_d

    .line 1206461
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206462
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1206463
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1206464
    if-eqz v0, :cond_e

    .line 1206465
    const-string v1, "playlist"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206466
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1206467
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1206468
    if-eqz v0, :cond_f

    .line 1206469
    const-string v1, "preferredPlayableUrlString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206470
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1206471
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1206472
    if-eqz v0, :cond_10

    .line 1206473
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206474
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1206475
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1206476
    return-void
.end method
