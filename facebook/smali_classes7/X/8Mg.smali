.class public LX/8Mg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1Kf;

.field private final b:LX/1nC;


# direct methods
.method public constructor <init>(LX/1Kf;LX/1nC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1334972
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1334973
    iput-object p1, p0, LX/8Mg;->a:LX/1Kf;

    .line 1334974
    iput-object p2, p0, LX/8Mg;->b:LX/1nC;

    .line 1334975
    return-void
.end method

.method private a(LX/7mj;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1334978
    iget-object v0, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v0

    .line 1334979
    invoke-static {v1}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v1}, LX/17E;->h(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1334980
    :cond_0
    sget-object v0, LX/21D;->COMPOST:LX/21D;

    const-string v2, "composerConfigurationFromDraft"

    invoke-static {v0, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 1334981
    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowDynamicTextStyle(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1334982
    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowRichTextStyle(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1334983
    :goto_0
    iget-object v2, p0, LX/8Mg;->b:LX/1nC;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v0, v3}, LX/1nC;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;Z)V

    .line 1334984
    iget-object v1, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1334985
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1334986
    sget-object v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1334987
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    return-object v0

    .line 1334988
    :cond_2
    invoke-static {v1}, LX/17E;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v1}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1334989
    :cond_3
    sget-object v0, LX/21D;->COMPOST:LX/21D;

    const-string v2, "composerPhotoConfigurationFromDraft"

    invoke-static {v0, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    goto :goto_0

    .line 1334990
    :cond_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "We only support status, photo, and multi-photo posts for now..."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(LX/7mj;Landroid/support/v4/app/Fragment;I)V
    .locals 3

    .prologue
    .line 1334976
    iget-object v0, p0, LX/8Mg;->a:LX/1Kf;

    invoke-virtual {p1}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, LX/8Mg;->a(LX/7mj;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-interface {v0, v1, v2, p3, p2}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 1334977
    return-void
.end method
