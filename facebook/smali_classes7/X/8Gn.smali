.class public LX/8Gn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/8Gn;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final c:J


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Long;LX/0SG;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/photos/data/cache/PhotoSetCacheSize;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/photos/data/cache/PhotoSetCacheMaxAge;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1319975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1319976
    new-instance v0, LX/0aq;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/8Gn;->b:LX/0aq;

    .line 1319977
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/8Gn;->c:J

    .line 1319978
    iput-object p3, p0, LX/8Gn;->a:LX/0SG;

    .line 1319979
    return-void
.end method

.method public static a(LX/0QB;)LX/8Gn;
    .locals 6

    .prologue
    .line 1319980
    sget-object v0, LX/8Gn;->d:LX/8Gn;

    if-nez v0, :cond_1

    .line 1319981
    const-class v1, LX/8Gn;

    monitor-enter v1

    .line 1319982
    :try_start_0
    sget-object v0, LX/8Gn;->d:LX/8Gn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1319983
    if-eqz v2, :cond_0

    .line 1319984
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1319985
    new-instance p0, LX/8Gn;

    .line 1319986
    invoke-static {}, LX/8Gj;->a()Ljava/lang/Integer;

    move-result-object v3

    move-object v3, v3

    .line 1319987
    check-cast v3, Ljava/lang/Integer;

    .line 1319988
    invoke-static {}, LX/8Gj;->b()Ljava/lang/Long;

    move-result-object v4

    move-object v4, v4

    .line 1319989
    check-cast v4, Ljava/lang/Long;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {p0, v3, v4, v5}, LX/8Gn;-><init>(Ljava/lang/Integer;Ljava/lang/Long;LX/0SG;)V

    .line 1319990
    move-object v0, p0

    .line 1319991
    sput-object v0, LX/8Gn;->d:LX/8Gn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1319992
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1319993
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1319994
    :cond_1
    sget-object v0, LX/8Gn;->d:LX/8Gn;

    return-object v0

    .line 1319995
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1319996
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized clearUserData()V
    .locals 1

    .prologue
    .line 1319997
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8Gn;->b:LX/0aq;

    invoke-virtual {v0}, LX/0aq;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1319998
    monitor-exit p0

    return-void

    .line 1319999
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
