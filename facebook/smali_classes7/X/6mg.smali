.class public LX/6mg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final recipient:Ljava/lang/Long;

.field public final sender:Ljava/lang/Long;

.field public final state:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v3, 0xa

    const/4 v4, 0x1

    .line 1146245
    new-instance v0, LX/1sv;

    const-string v1, "ThreadPresenceFromClientThrift"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mg;->b:LX/1sv;

    .line 1146246
    new-instance v0, LX/1sw;

    const-string v1, "recipient"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mg;->c:LX/1sw;

    .line 1146247
    new-instance v0, LX/1sw;

    const-string v1, "sender"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mg;->d:LX/1sw;

    .line 1146248
    new-instance v0, LX/1sw;

    const-string v1, "state"

    const/16 v2, 0x8

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mg;->e:LX/1sw;

    .line 1146249
    sput-boolean v4, LX/6mg;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1146250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146251
    iput-object p1, p0, LX/6mg;->recipient:Ljava/lang/Long;

    .line 1146252
    iput-object p2, p0, LX/6mg;->sender:Ljava/lang/Long;

    .line 1146253
    iput-object p3, p0, LX/6mg;->state:Ljava/lang/Integer;

    .line 1146254
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1146255
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1146256
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1146257
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1146258
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ThreadPresenceFromClientThrift"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1146259
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146260
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146261
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146262
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146263
    const-string v4, "recipient"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146264
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146265
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146266
    iget-object v4, p0, LX/6mg;->recipient:Ljava/lang/Long;

    if-nez v4, :cond_3

    .line 1146267
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146268
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146269
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146270
    const-string v4, "sender"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146271
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146272
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146273
    iget-object v4, p0, LX/6mg;->sender:Ljava/lang/Long;

    if-nez v4, :cond_4

    .line 1146274
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146275
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146276
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146277
    const-string v4, "state"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146278
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146279
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146280
    iget-object v0, p0, LX/6mg;->state:Ljava/lang/Integer;

    if-nez v0, :cond_5

    .line 1146281
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146282
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146283
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146284
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1146285
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1146286
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1146287
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1146288
    :cond_3
    iget-object v4, p0, LX/6mg;->recipient:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1146289
    :cond_4
    iget-object v4, p0, LX/6mg;->sender:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1146290
    :cond_5
    iget-object v0, p0, LX/6mg;->state:Ljava/lang/Integer;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1146291
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1146292
    iget-object v0, p0, LX/6mg;->recipient:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1146293
    sget-object v0, LX/6mg;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146294
    iget-object v0, p0, LX/6mg;->recipient:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1146295
    :cond_0
    iget-object v0, p0, LX/6mg;->sender:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1146296
    sget-object v0, LX/6mg;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146297
    iget-object v0, p0, LX/6mg;->sender:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1146298
    :cond_1
    iget-object v0, p0, LX/6mg;->state:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1146299
    sget-object v0, LX/6mg;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146300
    iget-object v0, p0, LX/6mg;->state:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1146301
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1146302
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1146303
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1146304
    if-nez p1, :cond_1

    .line 1146305
    :cond_0
    :goto_0
    return v0

    .line 1146306
    :cond_1
    instance-of v1, p1, LX/6mg;

    if-eqz v1, :cond_0

    .line 1146307
    check-cast p1, LX/6mg;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1146308
    if-nez p1, :cond_3

    .line 1146309
    :cond_2
    :goto_1
    move v0, v2

    .line 1146310
    goto :goto_0

    .line 1146311
    :cond_3
    iget-object v0, p0, LX/6mg;->recipient:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1146312
    :goto_2
    iget-object v3, p1, LX/6mg;->recipient:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1146313
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1146314
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146315
    iget-object v0, p0, LX/6mg;->recipient:Ljava/lang/Long;

    iget-object v3, p1, LX/6mg;->recipient:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1146316
    :cond_5
    iget-object v0, p0, LX/6mg;->sender:Ljava/lang/Long;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1146317
    :goto_4
    iget-object v3, p1, LX/6mg;->sender:Ljava/lang/Long;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1146318
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1146319
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146320
    iget-object v0, p0, LX/6mg;->sender:Ljava/lang/Long;

    iget-object v3, p1, LX/6mg;->sender:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1146321
    :cond_7
    iget-object v0, p0, LX/6mg;->state:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1146322
    :goto_6
    iget-object v3, p1, LX/6mg;->state:Ljava/lang/Integer;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1146323
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1146324
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146325
    iget-object v0, p0, LX/6mg;->state:Ljava/lang/Integer;

    iget-object v3, p1, LX/6mg;->state:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 1146326
    goto :goto_1

    :cond_a
    move v0, v2

    .line 1146327
    goto :goto_2

    :cond_b
    move v3, v2

    .line 1146328
    goto :goto_3

    :cond_c
    move v0, v2

    .line 1146329
    goto :goto_4

    :cond_d
    move v3, v2

    .line 1146330
    goto :goto_5

    :cond_e
    move v0, v2

    .line 1146331
    goto :goto_6

    :cond_f
    move v3, v2

    .line 1146332
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1146333
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1146334
    sget-boolean v0, LX/6mg;->a:Z

    .line 1146335
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mg;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1146336
    return-object v0
.end method
