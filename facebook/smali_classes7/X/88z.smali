.class public LX/88z;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/88z;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1303097
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1303098
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1303099
    const-string v1, "feed_type_name"

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->d:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 1303100
    iget-object v3, v2, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    move-object v2, v3

    .line 1303101
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1303102
    const-string v1, "hashtag/{%s}?name={%s}&id={%s}"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "hashtag_feed_hashtag"

    const-string v3, "hashtag_feed_title"

    const-string v4, "hashtag_feed_id"

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 1303103
    const-string v1, "hashtag/{%s}?name={%s}"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "hashtag_feed_hashtag"

    const-string v3, "hashtag_feed_title"

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->NATIVE_NEWS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 1303104
    const-string v1, "hashtag/{%s}"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "hashtag_feed_hashtag"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->NATIVE_NEWS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 1303105
    return-void
.end method

.method public static a(LX/0QB;)LX/88z;
    .locals 3

    .prologue
    .line 1303106
    sget-object v0, LX/88z;->a:LX/88z;

    if-nez v0, :cond_1

    .line 1303107
    const-class v1, LX/88z;

    monitor-enter v1

    .line 1303108
    :try_start_0
    sget-object v0, LX/88z;->a:LX/88z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1303109
    if-eqz v2, :cond_0

    .line 1303110
    :try_start_1
    new-instance v0, LX/88z;

    invoke-direct {v0}, LX/88z;-><init>()V

    .line 1303111
    move-object v0, v0

    .line 1303112
    sput-object v0, LX/88z;->a:LX/88z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1303113
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1303114
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1303115
    :cond_1
    sget-object v0, LX/88z;->a:LX/88z;

    return-object v0

    .line 1303116
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1303117
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
