.class public LX/6jK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static volatile f:LX/6jK;


# instance fields
.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/3MK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1130524
    const-class v0, LX/6jK;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6jK;->a:Ljava/lang/String;

    .line 1130525
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LX/3MP;->a:LX/0U1;

    .line 1130526
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1130527
    aput-object v2, v0, v1

    sput-object v0, LX/6jK;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1130528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/6jK;
    .locals 4

    .prologue
    .line 1130529
    sget-object v0, LX/6jK;->f:LX/6jK;

    if-nez v0, :cond_1

    .line 1130530
    const-class v1, LX/6jK;

    monitor-enter v1

    .line 1130531
    :try_start_0
    sget-object v0, LX/6jK;->f:LX/6jK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1130532
    if-eqz v2, :cond_0

    .line 1130533
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1130534
    new-instance p0, LX/6jK;

    invoke-direct {p0}, LX/6jK;-><init>()V

    .line 1130535
    invoke-static {v0}, LX/3MK;->a(LX/0QB;)LX/3MK;

    move-result-object v3

    check-cast v3, LX/3MK;

    .line 1130536
    iput-object v3, p0, LX/6jK;->e:LX/3MK;

    .line 1130537
    move-object v0, p0

    .line 1130538
    sput-object v0, LX/6jK;->f:LX/6jK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1130539
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1130540
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1130541
    :cond_1
    sget-object v0, LX/6jK;->f:LX/6jK;

    return-object v0

    .line 1130542
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1130543
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/6jK;LX/0ux;)Ljava/util/Set;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ux;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 1130544
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 1130545
    :try_start_0
    iget-object v0, p0, LX/6jK;->e:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "sms_business_address_list"

    sget-object v2, LX/6jK;->b:[Ljava/lang/String;

    invoke-virtual {p1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1130546
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1130547
    sget-object v0, LX/3MP;->a:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 1130548
    invoke-interface {v9, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 1130549
    :catch_0
    move-exception v0

    .line 1130550
    :goto_1
    :try_start_2
    sget-object v2, LX/6jK;->a:Ljava/lang/String;

    const-string v3, "Error getting sms business address info"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1130551
    if-eqz v1, :cond_0

    .line 1130552
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1130553
    :cond_0
    :goto_2
    return-object v9

    .line 1130554
    :cond_1
    if-eqz v1, :cond_0

    .line 1130555
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 1130556
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_3
    if-eqz v1, :cond_2

    .line 1130557
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 1130558
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 1130559
    :catch_1
    move-exception v0

    move-object v1, v8

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1130560
    iget-object v0, p0, LX/6jK;->c:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 1130561
    sget-object v0, LX/3MP;->b:LX/0U1;

    .line 1130562
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1130563
    const-string v1, "0"

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 1130564
    invoke-static {p0, v0}, LX/6jK;->a(LX/6jK;LX/0ux;)Ljava/util/Set;

    move-result-object v0

    .line 1130565
    iput-object v0, p0, LX/6jK;->c:Ljava/util/Set;

    .line 1130566
    :cond_0
    iget-object v0, p0, LX/6jK;->c:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1130567
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1130568
    iget-object v0, p0, LX/6jK;->e:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1130569
    const v0, -0x70103cce

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1130570
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1130571
    sget-object v4, LX/3MP;->a:LX/0U1;

    .line 1130572
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1130573
    invoke-virtual {v1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1130574
    const-string v0, "sms_business_address_list"

    const/4 v4, 0x0

    const/4 v5, 0x4

    const v6, -0x9c69ca6

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {v2, v0, v4, v1, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v0, 0x1b3167f

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1130575
    :catchall_0
    move-exception v0

    const v1, 0x70f09177

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 1130576
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1130577
    iget-object v0, p0, LX/6jK;->c:Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 1130578
    iget-object v0, p0, LX/6jK;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1130579
    :cond_1
    const v0, -0x1c584e39

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1130580
    return-void
.end method
