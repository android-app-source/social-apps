.class public final enum LX/89l;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/89l;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/89l;

.field public static final enum LIKERS_FOR_FEEDBACK_ID:LX/89l;

.field public static final enum PROFILES:LX/89l;

.field public static final enum PROFILES_BY_IDS:LX/89l;

.field public static final enum SEEN_BY_FOR_FEEDBACK_ID:LX/89l;

.field public static final enum UNKNOWN:LX/89l;

.field public static final enum VOTERS_FOR_POLL_OPTION_ID:LX/89l;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1305254
    new-instance v0, LX/89l;

    const-string v1, "PROFILES"

    invoke-direct {v0, v1, v3}, LX/89l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89l;->PROFILES:LX/89l;

    .line 1305255
    new-instance v0, LX/89l;

    const-string v1, "PROFILES_BY_IDS"

    invoke-direct {v0, v1, v4}, LX/89l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89l;->PROFILES_BY_IDS:LX/89l;

    .line 1305256
    new-instance v0, LX/89l;

    const-string v1, "LIKERS_FOR_FEEDBACK_ID"

    invoke-direct {v0, v1, v5}, LX/89l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89l;->LIKERS_FOR_FEEDBACK_ID:LX/89l;

    .line 1305257
    new-instance v0, LX/89l;

    const-string v1, "SEEN_BY_FOR_FEEDBACK_ID"

    invoke-direct {v0, v1, v6}, LX/89l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89l;->SEEN_BY_FOR_FEEDBACK_ID:LX/89l;

    .line 1305258
    new-instance v0, LX/89l;

    const-string v1, "VOTERS_FOR_POLL_OPTION_ID"

    invoke-direct {v0, v1, v7}, LX/89l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89l;->VOTERS_FOR_POLL_OPTION_ID:LX/89l;

    .line 1305259
    new-instance v0, LX/89l;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/89l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89l;->UNKNOWN:LX/89l;

    .line 1305260
    const/4 v0, 0x6

    new-array v0, v0, [LX/89l;

    sget-object v1, LX/89l;->PROFILES:LX/89l;

    aput-object v1, v0, v3

    sget-object v1, LX/89l;->PROFILES_BY_IDS:LX/89l;

    aput-object v1, v0, v4

    sget-object v1, LX/89l;->LIKERS_FOR_FEEDBACK_ID:LX/89l;

    aput-object v1, v0, v5

    sget-object v1, LX/89l;->SEEN_BY_FOR_FEEDBACK_ID:LX/89l;

    aput-object v1, v0, v6

    sget-object v1, LX/89l;->VOTERS_FOR_POLL_OPTION_ID:LX/89l;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/89l;->UNKNOWN:LX/89l;

    aput-object v2, v0, v1

    sput-object v0, LX/89l;->$VALUES:[LX/89l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1305261
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/89l;
    .locals 1

    .prologue
    .line 1305262
    const-class v0, LX/89l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/89l;

    return-object v0
.end method

.method public static values()[LX/89l;
    .locals 1

    .prologue
    .line 1305263
    sget-object v0, LX/89l;->$VALUES:[LX/89l;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/89l;

    return-object v0
.end method
