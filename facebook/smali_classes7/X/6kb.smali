.class public LX/6kb;
.super LX/6kT;
.source ""


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1139850
    sput-boolean v3, LX/6kb;->a:Z

    .line 1139851
    new-instance v0, LX/1sv;

    const-string v1, "GenericMapMutation"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kb;->b:LX/1sv;

    .line 1139852
    new-instance v0, LX/1sw;

    const-string v1, "keyMutations"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kb;->c:LX/1sw;

    .line 1139853
    new-instance v0, LX/1sw;

    const-string v1, "mapOverwrite"

    const/16 v2, 0xc

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kb;->d:LX/1sw;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1139992
    invoke-direct {p0}, LX/6kT;-><init>()V

    .line 1139993
    return-void
.end method

.method public static b(LX/1su;)LX/6kb;
    .locals 3

    .prologue
    .line 1139979
    new-instance v0, LX/6kb;

    invoke-direct {v0}, LX/6kb;-><init>()V

    .line 1139980
    new-instance v0, LX/6kb;

    invoke-direct {v0}, LX/6kb;-><init>()V

    .line 1139981
    const/4 v1, 0x0

    iput v1, v0, LX/6kb;->setField_:I

    .line 1139982
    const/4 v1, 0x0

    iput-object v1, v0, LX/6kb;->value_:Ljava/lang/Object;

    .line 1139983
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    .line 1139984
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v1

    .line 1139985
    invoke-virtual {v0, p0, v1}, LX/6kb;->a(LX/1su;LX/1sw;)Ljava/lang/Object;

    move-result-object v2

    iput-object v2, v0, LX/6kb;->value_:Ljava/lang/Object;

    .line 1139986
    iget-object v2, v0, LX/6kT;->value_:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 1139987
    iget-short v1, v1, LX/1sw;->c:S

    iput v1, v0, LX/6kb;->setField_:I

    .line 1139988
    :cond_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    .line 1139989
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1139990
    move-object v0, v0

    .line 1139991
    return-object v0
.end method

.method private c()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/6ka;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1139972
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 1139973
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1139974
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1139975
    check-cast v0, Ljava/util/List;

    return-object v0

    .line 1139976
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'keyMutations\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1139977
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 1139978
    invoke-virtual {p0, v2}, LX/6kb;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private d()LX/6kZ;
    .locals 3

    .prologue
    .line 1139965
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 1139966
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1139967
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1139968
    check-cast v0, LX/6kZ;

    return-object v0

    .line 1139969
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'mapOverwrite\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1139970
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 1139971
    invoke-virtual {p0, v2}, LX/6kb;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(LX/1su;LX/1sw;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1139919
    iget-short v2, p2, LX/1sw;->c:S

    packed-switch v2, :pswitch_data_0

    .line 1139920
    iget-byte v0, p2, LX/1sw;->b:B

    invoke-static {p1, v0}, LX/3ae;->a(LX/1su;B)V

    move-object v0, v1

    .line 1139921
    :goto_0
    return-object v0

    .line 1139922
    :pswitch_0
    iget-byte v2, p2, LX/1sw;->b:B

    sget-object v3, LX/6kb;->c:LX/1sw;

    iget-byte v3, v3, LX/1sw;->b:B

    if-ne v2, v3, :cond_7

    .line 1139923
    invoke-virtual {p1}, LX/1su;->h()LX/1u3;

    move-result-object v2

    .line 1139924
    new-instance v1, Ljava/util/ArrayList;

    iget v3, v2, LX/1u3;->b:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1139925
    :goto_1
    iget v3, v2, LX/1u3;->b:I

    if-gez v3, :cond_5

    invoke-static {}, LX/1su;->t()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1139926
    :cond_0
    const/4 v3, 0x0

    .line 1139927
    invoke-virtual {p1}, LX/1su;->r()LX/1sv;

    move-object v4, v3

    .line 1139928
    :goto_2
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    move-result-object v5

    .line 1139929
    iget-byte p0, v5, LX/1sw;->b:B

    if-eqz p0, :cond_4

    .line 1139930
    iget-short p0, v5, LX/1sw;->c:S

    packed-switch p0, :pswitch_data_1

    .line 1139931
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {p1, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 1139932
    :pswitch_1
    iget-byte p0, v5, LX/1sw;->b:B

    const/16 p2, 0xb

    if-ne p0, p2, :cond_1

    .line 1139933
    invoke-virtual {p1}, LX/1su;->p()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 1139934
    :cond_1
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {p1, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 1139935
    :pswitch_2
    iget-byte p0, v5, LX/1sw;->b:B

    const/16 p2, 0xc

    if-ne p0, p2, :cond_3

    .line 1139936
    new-instance v3, LX/6kc;

    invoke-direct {v3}, LX/6kc;-><init>()V

    .line 1139937
    new-instance v3, LX/6kc;

    invoke-direct {v3}, LX/6kc;-><init>()V

    .line 1139938
    const/4 v5, 0x0

    iput v5, v3, LX/6kc;->setField_:I

    .line 1139939
    const/4 v5, 0x0

    iput-object v5, v3, LX/6kc;->value_:Ljava/lang/Object;

    .line 1139940
    invoke-virtual {p1}, LX/1su;->r()LX/1sv;

    .line 1139941
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    move-result-object v5

    .line 1139942
    invoke-virtual {v3, p1, v5}, LX/6kc;->a(LX/1su;LX/1sw;)Ljava/lang/Object;

    move-result-object p0

    iput-object p0, v3, LX/6kc;->value_:Ljava/lang/Object;

    .line 1139943
    iget-object p0, v3, LX/6kT;->value_:Ljava/lang/Object;

    if-eqz p0, :cond_2

    .line 1139944
    iget-short v5, v5, LX/1sw;->c:S

    iput v5, v3, LX/6kc;->setField_:I

    .line 1139945
    :cond_2
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    .line 1139946
    invoke-virtual {p1}, LX/1su;->e()V

    .line 1139947
    move-object v3, v3

    .line 1139948
    move-object v3, v3

    .line 1139949
    goto :goto_2

    .line 1139950
    :cond_3
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {p1, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 1139951
    :cond_4
    invoke-virtual {p1}, LX/1su;->e()V

    .line 1139952
    new-instance v5, LX/6ka;

    invoke-direct {v5, v4, v3}, LX/6ka;-><init>(Ljava/lang/String;LX/6kc;)V

    .line 1139953
    invoke-static {v5}, LX/6ka;->a(LX/6ka;)V

    .line 1139954
    move-object v3, v5

    .line 1139955
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1139956
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1139957
    :cond_5
    iget v3, v2, LX/1u3;->b:I

    if-lt v0, v3, :cond_0

    :cond_6
    move-object v0, v1

    .line 1139958
    goto/16 :goto_0

    .line 1139959
    :cond_7
    iget-byte v0, p2, LX/1sw;->b:B

    invoke-static {p1, v0}, LX/3ae;->a(LX/1su;B)V

    move-object v0, v1

    .line 1139960
    goto/16 :goto_0

    .line 1139961
    :pswitch_3
    iget-byte v0, p2, LX/1sw;->b:B

    sget-object v2, LX/6kb;->d:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v0, v2, :cond_8

    .line 1139962
    invoke-static {p1}, LX/6kZ;->b(LX/1su;)LX/6kZ;

    move-result-object v0

    goto/16 :goto_0

    .line 1139963
    :cond_8
    iget-byte v0, p2, LX/1sw;->b:B

    invoke-static {p1, v0}, LX/3ae;->a(LX/1su;B)V

    move-object v0, v1

    .line 1139964
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 1139886
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1139887
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1139888
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1139889
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "GenericMapMutation"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1139890
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139891
    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139892
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139893
    iget v5, p0, LX/6kT;->setField_:I

    move v5, v5

    .line 1139894
    if-ne v5, v1, :cond_0

    .line 1139895
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139896
    const-string v1, "keyMutations"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139897
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139898
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139899
    invoke-direct {p0}, LX/6kb;->c()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_6

    .line 1139900
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139901
    :goto_3
    const/4 v1, 0x0

    .line 1139902
    :cond_0
    iget v5, p0, LX/6kT;->setField_:I

    move v5, v5

    .line 1139903
    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    .line 1139904
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139905
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139906
    const-string v1, "mapOverwrite"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139907
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139908
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139909
    invoke-direct {p0}, LX/6kb;->d()LX/6kZ;

    move-result-object v0

    if-nez v0, :cond_7

    .line 1139910
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139911
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139912
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139913
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1139914
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1139915
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1139916
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1139917
    :cond_6
    invoke-direct {p0}, LX/6kb;->c()Ljava/util/List;

    move-result-object v1

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1139918
    :cond_7
    invoke-direct {p0}, LX/6kb;->d()LX/6kZ;

    move-result-object v0

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;S)V
    .locals 4

    .prologue
    .line 1139875
    packed-switch p2, :pswitch_data_0

    .line 1139876
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot write union with unknown field "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1139877
    :pswitch_0
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1139878
    check-cast v0, Ljava/util/List;

    .line 1139879
    new-instance v1, LX/1u3;

    const/16 v2, 0xc

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v1, v2, v3}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v1}, LX/1su;->a(LX/1u3;)V

    .line 1139880
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ka;

    .line 1139881
    invoke-virtual {v0, p1}, LX/6ka;->a(LX/1su;)V

    goto :goto_0

    .line 1139882
    :pswitch_1
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1139883
    check-cast v0, LX/6kZ;

    .line 1139884
    invoke-virtual {v0, p1}, LX/6kZ;->a(LX/1su;)V

    .line 1139885
    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/6kb;)Z
    .locals 2

    .prologue
    .line 1139865
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 1139866
    iget v1, p1, LX/6kT;->setField_:I

    move v1, v1

    .line 1139867
    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_1

    .line 1139868
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1139869
    check-cast v0, [B

    check-cast v0, [B

    .line 1139870
    iget-object v1, p1, LX/6kT;->value_:Ljava/lang/Object;

    move-object v1, v1

    .line 1139871
    check-cast v1, [B

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 1139872
    :cond_1
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1139873
    iget-object v1, p1, LX/6kT;->value_:Ljava/lang/Object;

    move-object v1, v1

    .line 1139874
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)LX/1sw;
    .locals 3

    .prologue
    .line 1139861
    packed-switch p1, :pswitch_data_0

    .line 1139862
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown field id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1139863
    :pswitch_0
    sget-object v0, LX/6kb;->c:LX/1sw;

    .line 1139864
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, LX/6kb;->d:LX/1sw;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1139858
    instance-of v0, p1, LX/6kb;

    if-eqz v0, :cond_0

    .line 1139859
    check-cast p1, LX/6kb;

    invoke-virtual {p0, p1}, LX/6kb;->a(LX/6kb;)Z

    move-result v0

    .line 1139860
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1139857
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1139854
    sget-boolean v0, LX/6kb;->a:Z

    .line 1139855
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kb;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1139856
    return-object v0
.end method
