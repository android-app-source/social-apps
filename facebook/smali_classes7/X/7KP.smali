.class public final LX/7KP;
.super LX/041;
.source ""


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;",
            ">;"
        }
    .end annotation
.end field

.field private volatile c:Ljava/util/concurrent/ExecutorService;

.field private d:Z


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1195816
    invoke-direct {p0}, LX/041;-><init>()V

    .line 1195817
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/7KP;->a:Ljava/lang/Object;

    .line 1195818
    iput-object v1, p0, LX/7KP;->b:Ljava/lang/ref/WeakReference;

    .line 1195819
    iput-object v1, p0, LX/7KP;->c:Ljava/util/concurrent/ExecutorService;

    .line 1195820
    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V
    .locals 3

    .prologue
    .line 1195866
    invoke-direct {p0}, LX/041;-><init>()V

    .line 1195867
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/7KP;->a:Ljava/lang/Object;

    .line 1195868
    iget-object v0, p1, LX/2q6;->r:LX/0ad;

    sget-short v1, LX/0ws;->eE:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/7KP;->d:Z

    .line 1195869
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/7KP;->b:Ljava/lang/ref/WeakReference;

    .line 1195870
    const/4 v0, 0x0

    iput-object v0, p0, LX/7KP;->c:Ljava/util/concurrent/ExecutorService;

    .line 1195871
    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 1195872
    iget-boolean v0, p0, LX/7KP;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7KP;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v0, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bC:LX/2q5;

    if-eqz v0, :cond_0

    .line 1195873
    iget-object v0, p0, LX/7KP;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v0, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bC:LX/2q5;

    invoke-virtual {v0, p1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195874
    :goto_0
    return-void

    .line 1195875
    :cond_0
    invoke-direct {p0}, LX/7KP;->d()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    const v1, -0xb8feabf

    invoke-static {v0, p1, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public static b(LX/7KP;IIF)V
    .locals 2

    .prologue
    .line 1195876
    invoke-direct {p0}, LX/7KP;->e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-result-object v0

    .line 1195877
    const/4 v1, 0x0

    .line 1195878
    invoke-static {v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Landroid/net/Uri;)Z

    move-result p0

    move v1, p0

    .line 1195879
    if-eqz v1, :cond_0

    iget v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bl:I

    rem-int/lit16 v1, v1, 0xb4

    if-eqz v1, :cond_0

    .line 1195880
    invoke-virtual {v0, p2, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(II)V

    .line 1195881
    :goto_0
    return-void

    .line 1195882
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(II)V

    goto :goto_0
.end method

.method public static b(LX/7KP;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/RendererContext;)V
    .locals 4

    .prologue
    .line 1195883
    :try_start_0
    invoke-direct {p0}, LX/7KP;->e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-result-object v0

    const/4 p0, 0x0

    .line 1195884
    const-string v1, "Renderers built"

    new-array v2, p0, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195885
    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aj:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    if-eq p1, v1, :cond_0

    .line 1195886
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->af:Landroid/view/Surface;

    .line 1195887
    :cond_0
    iput-object p1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aj:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    .line 1195888
    iput-object p2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->al:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    .line 1195889
    iget v1, p3, Lcom/facebook/exoplayer/ipc/RendererContext;->b:I

    iput v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ax:I

    .line 1195890
    iget v1, p3, Lcom/facebook/exoplayer/ipc/RendererContext;->c:I

    iput v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->X:I

    .line 1195891
    iget-object v1, p3, Lcom/facebook/exoplayer/ipc/RendererContext;->a:Ljava/lang/String;

    invoke-static {v1}, LX/09L;->valueOf(Ljava/lang/String;)LX/09L;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->P:LX/09L;

    .line 1195892
    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Z:LX/2px;

    iget-object v2, v0, LX/2q6;->P:LX/09L;

    iget-object v2, v2, LX/09L;->value:Ljava/lang/String;

    .line 1195893
    iput-object v2, v1, LX/2px;->k:Ljava/lang/String;

    .line 1195894
    const/4 v1, 0x3

    iput v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->am:I

    .line 1195895
    iget-object v1, p3, Lcom/facebook/exoplayer/ipc/RendererContext;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1195896
    iget-object v1, p3, Lcom/facebook/exoplayer/ipc/RendererContext;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ay:Ljava/lang/String;

    .line 1195897
    iget-object v1, v0, LX/2q6;->F:LX/7IE;

    if-eqz v1, :cond_1

    .line 1195898
    iget-object v1, v0, LX/2q6;->F:LX/7IE;

    iget-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ay:Ljava/lang/String;

    .line 1195899
    iput-object v2, v1, LX/7IE;->j:Ljava/lang/String;

    .line 1195900
    :cond_1
    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v1, v1, LX/0wq;->f:Z

    if-eqz v1, :cond_3

    .line 1195901
    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    if-eqz v1, :cond_2

    .line 1195902
    const-string v1, "mVideoSurfaceTarget.isSurfaceAllocated()? %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v3}, LX/2p8;->d()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, p0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195903
    :cond_2
    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v1}, LX/2p8;->d()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1195904
    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    .line 1195905
    iget-object v2, v1, LX/2p8;->a:Landroid/view/Surface;

    move-object v1, v2

    .line 1195906
    invoke-static {v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/view/Surface;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1195907
    :cond_3
    :goto_0
    return-void

    .line 1195908
    :catch_0
    goto :goto_0

    .line 1195909
    :cond_4
    const-string v1, "no surface to be used yet"

    new-array v2, p0, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b(LX/7KP;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZII)V
    .locals 11

    .prologue
    .line 1195910
    :try_start_0
    invoke-direct {p0}, LX/7KP;->e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-result-object v0

    const/4 v10, 0x5

    const/4 v9, 0x3

    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1195911
    const-string v1, "PlayerStateChanged: %s"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {p3}, LX/7K1;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195912
    if-eqz p1, :cond_1

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->hashCode()I

    move-result v1

    iget-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-virtual {v2}, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->hashCode()I

    move-result v2

    if-eq v1, v2, :cond_1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1195913
    :cond_0
    :goto_0
    return-void

    .line 1195914
    :catch_0
    goto :goto_0

    .line 1195915
    :cond_1
    iget v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->an:I

    if-eq p3, v1, :cond_9

    .line 1195916
    iget v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->an:I

    .line 1195917
    iput p3, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->an:I

    .line 1195918
    if-ne p3, v8, :cond_2

    .line 1195919
    iput v6, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aR:I

    .line 1195920
    if-eqz p2, :cond_2

    .line 1195921
    const/4 v2, -0x1

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 1195922
    :cond_2
    invoke-static {v0, p3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->d(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;I)V

    .line 1195923
    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz p2, :cond_3

    if-ne p3, v9, :cond_3

    .line 1195924
    const-string v2, "onStartBuffering"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195925
    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Z(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    .line 1195926
    if-ne v1, v8, :cond_3

    .line 1195927
    iget-object v2, v0, LX/2q6;->O:LX/09M;

    invoke-virtual {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, LX/09M;->a(J)V

    .line 1195928
    :cond_3
    if-ne v1, v9, :cond_5

    if-ne p3, v8, :cond_5

    .line 1195929
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bs:Z

    .line 1195930
    iget-object v1, v0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2pf;

    .line 1195931
    iget-object v3, v0, LX/2q6;->l:LX/0Sh;

    new-instance v4, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$17;

    invoke-direct {v4, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$17;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2pf;)V

    invoke-virtual {v3, v4}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 1195932
    :cond_4
    iget-object v1, v0, LX/2q6;->O:LX/09M;

    invoke-virtual {v1}, LX/09M;->c()V

    .line 1195933
    iget-object v1, v0, LX/2q6;->O:LX/09M;

    invoke-virtual {v1}, LX/09M;->f()LX/09O;

    move-result-object v1

    .line 1195934
    iget v2, v1, LX/09O;->b:I

    move v2, v2

    .line 1195935
    if-lez v2, :cond_7

    .line 1195936
    const-string v2, "onStopBuffering, stall total= %.2f s, count = %d, max = %.2f s, avg = %.2f s, last = %.2f s"

    new-array v3, v10, [Ljava/lang/Object;

    .line 1195937
    iget v4, v1, LX/09O;->c:F

    move v4, v4

    .line 1195938
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    .line 1195939
    iget v4, v1, LX/09O;->b:I

    move v4, v4

    .line 1195940
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    const/4 v4, 0x2

    .line 1195941
    iget-object v5, v1, LX/09O;->f:LX/09N;

    move-object v5, v5

    .line 1195942
    invoke-virtual {v5}, LX/09N;->b()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1}, LX/09O;->j()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v9

    .line 1195943
    iget-object v4, v1, LX/09O;->e:LX/09N;

    move-object v1, v4

    .line 1195944
    invoke-virtual {v1}, LX/09N;->b()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v3, v8

    invoke-static {v0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195945
    :cond_5
    :goto_2
    if-ne p3, v10, :cond_8

    .line 1195946
    const-string v1, "Playback complete, sid=%s"

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    invoke-static {v3}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195947
    invoke-virtual {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->E()V

    .line 1195948
    :cond_6
    :goto_3
    iget-object v1, v0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2pf;

    .line 1195949
    invoke-interface {v1, p4}, LX/2pf;->b(I)V

    goto :goto_4

    .line 1195950
    :cond_7
    const-string v1, "onStopBuffering, no stall is recorded"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 1195951
    :cond_8
    if-ne p3, v7, :cond_6

    iget-object v1, v0, LX/2q6;->D:LX/2qD;

    sget-object v2, LX/2qD;->STATE_ERROR:LX/2qD;

    if-eq v1, v2, :cond_6

    .line 1195952
    sget-object v1, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {v0, v1}, LX/2q6;->a(LX/2qD;)V

    goto :goto_3

    .line 1195953
    :cond_9
    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v1, v1, LX/0wq;->O:Z

    if-eqz v1, :cond_6

    .line 1195954
    invoke-static {v0, p3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->d(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;I)V

    goto :goto_3
.end method

.method private d()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .prologue
    .line 1195955
    iget-object v0, p0, LX/7KP;->c:Ljava/util/concurrent/ExecutorService;

    .line 1195956
    if-eqz v0, :cond_0

    .line 1195957
    :goto_0
    return-object v0

    .line 1195958
    :cond_0
    iget-object v1, p0, LX/7KP;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1195959
    :try_start_0
    iget-object v0, p0, LX/7KP;->c:Ljava/util/concurrent/ExecutorService;

    .line 1195960
    if-eqz v0, :cond_1

    .line 1195961
    monitor-exit v1

    goto :goto_0

    .line 1195962
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1195963
    :cond_1
    :try_start_1
    new-instance v0, LX/7KO;

    invoke-direct {v0, p0}, LX/7KO;-><init>(LX/7KP;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 1195964
    iput-object v0, p0, LX/7KP;->c:Ljava/util/concurrent/ExecutorService;

    .line 1195965
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;
    .locals 2

    .prologue
    .line 1195966
    iget-object v0, p0, LX/7KP;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    .line 1195967
    if-nez v0, :cond_0

    .line 1195968
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ExoVideoPlayerClient Weak Ref reclaimed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1195969
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1195970
    invoke-direct {p0}, LX/7KP;->e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-result-object v0

    .line 1195971
    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->U(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1195972
    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$8;

    invoke-direct {v1, p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$8;-><init>(LX/7KP;Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    invoke-direct {p0, v1}, LX/7KP;->a(Ljava/lang/Runnable;)V

    .line 1195973
    :goto_0
    return-void

    .line 1195974
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->F()V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1195860
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServiceListener: onSpatialAudioBufferUnderrun count="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1195861
    invoke-direct {p0}, LX/7KP;->e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-result-object v0

    .line 1195862
    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->U(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1195863
    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$7;

    invoke-direct {v1, p0, v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$7;-><init>(LX/7KP;Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;I)V

    invoke-direct {p0, v1}, LX/7KP;->a(Ljava/lang/Runnable;)V

    .line 1195864
    :goto_0
    return-void

    .line 1195865
    :cond_0
    invoke-virtual {v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(I)V

    goto :goto_0
.end method

.method public final a(IIF)V
    .locals 1

    .prologue
    .line 1195975
    invoke-direct {p0}, LX/7KP;->e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->U(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1195976
    new-instance v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$3;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$3;-><init>(LX/7KP;IIF)V

    invoke-direct {p0, v0}, LX/7KP;->a(Ljava/lang/Runnable;)V

    .line 1195977
    :goto_0
    return-void

    .line 1195978
    :cond_0
    invoke-static {p0, p1, p2, p3}, LX/7KP;->b(LX/7KP;IIF)V

    goto :goto_0
.end method

.method public final a(IJ)V
    .locals 6

    .prologue
    .line 1195854
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServiceListener: onDroppedFrames count="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " elapsed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1195855
    invoke-direct {p0}, LX/7KP;->e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-result-object v2

    .line 1195856
    invoke-static {v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->U(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1195857
    new-instance v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$6;

    move-object v1, p0

    move v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$6;-><init>(LX/7KP;Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;IJ)V

    invoke-direct {p0, v0}, LX/7KP;->a(Ljava/lang/Runnable;)V

    .line 1195858
    :goto_0
    return-void

    .line 1195859
    :cond_0
    invoke-virtual {v2, p1, p2, p3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(IJ)V

    goto :goto_0
.end method

.method public final a(ILcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V
    .locals 0

    .prologue
    .line 1195853
    return-void
.end method

.method public final a(ILcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;IJJ)V
    .locals 10

    .prologue
    .line 1195847
    invoke-static {}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->I()Ljava/lang/String;

    .line 1195848
    invoke-direct {p0}, LX/7KP;->e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-result-object v2

    .line 1195849
    invoke-static {v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->U(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1195850
    new-instance v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$4;

    move-object v1, p0

    move v3, p1

    move-object v4, p2

    move v5, p3

    move-wide v6, p4

    move-wide/from16 v8, p6

    invoke-direct/range {v0 .. v9}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$4;-><init>(LX/7KP;Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;ILcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;IJJ)V

    invoke-direct {p0, v0}, LX/7KP;->a(Ljava/lang/Runnable;)V

    .line 1195851
    :goto_0
    return-void

    :cond_0
    move v3, p1

    move-object v4, p2

    move v5, p3

    move-wide v6, p4

    move-wide/from16 v8, p6

    .line 1195852
    invoke-virtual/range {v2 .. v9}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(ILcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;IJJ)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/RendererContext;)V
    .locals 1

    .prologue
    .line 1195843
    invoke-direct {p0}, LX/7KP;->e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->U(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1195844
    new-instance v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$2;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$2;-><init>(LX/7KP;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/RendererContext;)V

    invoke-direct {p0, v0}, LX/7KP;->a(Ljava/lang/Runnable;)V

    .line 1195845
    :goto_0
    return-void

    .line 1195846
    :cond_0
    invoke-static {p0, p1, p2, p3}, LX/7KP;->b(LX/7KP;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/RendererContext;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZII)V
    .locals 6

    .prologue
    .line 1195839
    invoke-direct {p0}, LX/7KP;->e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->U(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1195840
    new-instance v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$1;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$1;-><init>(LX/7KP;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZII)V

    invoke-direct {p0, v0}, LX/7KP;->a(Ljava/lang/Runnable;)V

    .line 1195841
    :goto_0
    return-void

    .line 1195842
    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, LX/7KP;->b(LX/7KP;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZII)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1195838
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 8

    .prologue
    .line 1195833
    invoke-direct {p0}, LX/7KP;->e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-result-object v2

    .line 1195834
    invoke-static {v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->U(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1195835
    new-instance v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$5;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-wide v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$5;-><init>(LX/7KP;Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    invoke-direct {p0, v0}, LX/7KP;->a(Ljava/lang/Runnable;)V

    .line 1195836
    :goto_0
    return-void

    :cond_0
    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-wide v6, p4

    .line 1195837
    invoke-virtual/range {v2 .. v7}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;J[Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;",
            ">;J[",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1195831
    invoke-direct {p0}, LX/7KP;->e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-result-object v0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/util/List;J[Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;)V

    .line 1195832
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1195826
    invoke-direct {p0}, LX/7KP;->e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-result-object v0

    .line 1195827
    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->U(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1195828
    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$9;

    invoke-direct {v1, p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$9;-><init>(LX/7KP;Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    invoke-direct {p0, v1}, LX/7KP;->a(Ljava/lang/Runnable;)V

    .line 1195829
    :goto_0
    return-void

    .line 1195830
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->G()V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1195821
    invoke-direct {p0}, LX/7KP;->e()Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-result-object v0

    .line 1195822
    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->U(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1195823
    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$10;

    invoke-direct {v1, p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$ServiceListenerStub$10;-><init>(LX/7KP;Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    invoke-direct {p0, v1}, LX/7KP;->a(Ljava/lang/Runnable;)V

    .line 1195824
    :goto_0
    return-void

    .line 1195825
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->H()V

    goto :goto_0
.end method
