.class public final LX/6zk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6zj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6zj",
        "<",
        "Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;

.field public final synthetic b:I

.field public final synthetic c:Landroid/content/Intent;

.field public final synthetic d:LX/6zl;


# direct methods
.method public constructor <init>(LX/6zl;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;ILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 1161406
    iput-object p1, p0, LX/6zk;->d:LX/6zl;

    iput-object p2, p0, LX/6zk;->a:Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;

    iput p3, p0, LX/6zk;->b:I

    iput-object p4, p0, LX/6zk;->c:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/CoreClientData;)V
    .locals 4

    .prologue
    .line 1161407
    check-cast p1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;

    .line 1161408
    iget-object v1, p0, LX/6zk;->d:LX/6zl;

    iget-object v0, p0, LX/6zk;->a:Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;

    invoke-virtual {v0}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    iget v2, p0, LX/6zk;->b:I

    iget-object v3, p0, LX/6zk;->c:Landroid/content/Intent;

    invoke-static {v1, p1, v0, v2, v3}, LX/6zl;->a$redex0(LX/6zl;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;ILandroid/content/Intent;)V

    .line 1161409
    return-void
.end method
