.class public LX/6td;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rl",
            "<",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/6r9;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1155135
    new-instance v0, LX/6tb;

    invoke-direct {v0}, LX/6tb;-><init>()V

    sput-object v0, LX/6td;->a:LX/0Rl;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/6r9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1155131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1155132
    iput-object p1, p0, LX/6td;->b:Landroid/content/Context;

    .line 1155133
    iput-object p2, p0, LX/6td;->c:LX/6r9;

    .line 1155134
    return-void
.end method

.method public static a(LX/0Rf;)I
    .locals 4
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "LX/6vb;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1155123
    invoke-virtual {p0}, LX/0Rf;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1155124
    invoke-static {p0}, LX/0Ph;->d(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6vb;

    .line 1155125
    sget-object v1, LX/6tc;->a:[I

    invoke-virtual {v0}, LX/6vb;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1155126
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1155127
    :pswitch_0
    const v0, 0x7f081e28

    .line 1155128
    :goto_0
    return v0

    .line 1155129
    :pswitch_1
    const v0, 0x7f081e29

    goto :goto_0

    .line 1155130
    :cond_0
    const v0, 0x7f081e3e

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/0Rf;Ljava/util/List;)I
    .locals 4
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "LX/6vb;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;>;)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1155109
    invoke-virtual {p0}, LX/0Rf;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 1155110
    invoke-static {p0}, LX/0Ph;->d(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6vb;

    .line 1155111
    sget-object v1, LX/6tc;->a:[I

    invoke-virtual {v0}, LX/6vb;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1155112
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1155113
    :pswitch_0
    const v0, 0x7f081e3b

    .line 1155114
    :goto_0
    return v0

    .line 1155115
    :pswitch_1
    const v0, 0x7f081e3c

    goto :goto_0

    .line 1155116
    :cond_0
    invoke-virtual {p0}, LX/0Rf;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 1155117
    invoke-static {p1}, LX/0Ph;->d(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v0}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->d()LX/6vb;

    move-result-object v0

    .line 1155118
    sget-object v1, LX/6tc;->a:[I

    invoke-virtual {v0}, LX/6vb;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 1155119
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1155120
    :pswitch_2
    const v0, 0x7f081e3c

    goto :goto_0

    .line 1155121
    :pswitch_3
    const v0, 0x7f081e3b

    goto :goto_0

    .line 1155122
    :cond_1
    const v0, 0x7f081e3d

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
