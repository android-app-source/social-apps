.class public final LX/857;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1292220
    const-class v1, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;

    const v0, 0x540fd999

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "PaginatedPeopleYouMayKnowQuery"

    const-string v6, "94cbfccd6935af4b863005852f01cabf"

    const-string v7, "node"

    const-string v8, "10155140977261729"

    const-string v9, "10155259089641729"

    .line 1292221
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1292222
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1292223
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1292212
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1292213
    sparse-switch v0, :sswitch_data_0

    .line 1292214
    :goto_0
    return-object p1

    .line 1292215
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1292216
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1292217
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1292218
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1292219
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x529c6759 -> :sswitch_4
        -0x295975c2 -> :sswitch_1
        0x21beac6a -> :sswitch_0
        0x73a026b5 -> :sswitch_2
        0x7e07ec78 -> :sswitch_3
    .end sparse-switch
.end method
