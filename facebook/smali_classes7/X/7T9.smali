.class public LX/7T9;
.super Ljava/lang/Exception;
.source ""


# instance fields
.field private mCodecInitError:Z

.field private mVideoResizeStatus:LX/7TE;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1209908
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 1209909
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7T9;->mCodecInitError:Z

    .line 1209910
    const/4 v0, 0x0

    iput-object v0, p0, LX/7T9;->mVideoResizeStatus:LX/7TE;

    .line 1209911
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1209906
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/7T9;-><init>(Ljava/lang/String;ZLX/7TE;)V

    .line 1209907
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/7TE;)V
    .locals 1

    .prologue
    .line 1209904
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, LX/7T9;-><init>(Ljava/lang/String;ZLX/7TE;)V

    .line 1209905
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1209902
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, LX/7T9;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZLX/7TE;)V

    .line 1209903
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;ZLX/7TE;)V
    .locals 1

    .prologue
    .line 1209912
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1209913
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7T9;->mCodecInitError:Z

    .line 1209914
    const/4 v0, 0x0

    iput-object v0, p0, LX/7T9;->mVideoResizeStatus:LX/7TE;

    .line 1209915
    iput-boolean p3, p0, LX/7T9;->mCodecInitError:Z

    .line 1209916
    iput-object p4, p0, LX/7T9;->mVideoResizeStatus:LX/7TE;

    .line 1209917
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ZLX/7TE;)V
    .locals 1

    .prologue
    .line 1209896
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 1209897
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7T9;->mCodecInitError:Z

    .line 1209898
    const/4 v0, 0x0

    iput-object v0, p0, LX/7T9;->mVideoResizeStatus:LX/7TE;

    .line 1209899
    iput-boolean p2, p0, LX/7T9;->mCodecInitError:Z

    .line 1209900
    iput-object p3, p0, LX/7T9;->mVideoResizeStatus:LX/7TE;

    .line 1209901
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1209892
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 1209893
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7T9;->mCodecInitError:Z

    .line 1209894
    const/4 v0, 0x0

    iput-object v0, p0, LX/7T9;->mVideoResizeStatus:LX/7TE;

    .line 1209895
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1209891
    iget-boolean v0, p0, LX/7T9;->mCodecInitError:Z

    return v0
.end method

.method public final b()LX/7TE;
    .locals 1

    .prologue
    .line 1209890
    iget-object v0, p0, LX/7T9;->mVideoResizeStatus:LX/7TE;

    return-object v0
.end method
