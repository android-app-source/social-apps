.class public LX/8GX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/8JD;

.field private final b:LX/75Q;

.field public final c:LX/75F;


# direct methods
.method public constructor <init>(LX/75Q;LX/75F;LX/8JD;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1319699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1319700
    iput-object p1, p0, LX/8GX;->b:LX/75Q;

    .line 1319701
    iput-object p2, p0, LX/8GX;->c:LX/75F;

    .line 1319702
    iput-object p3, p0, LX/8GX;->a:LX/8JD;

    .line 1319703
    return-void
.end method

.method private static a(LX/8GX;Lcom/facebook/photos/base/media/PhotoItem;)I
    .locals 5

    .prologue
    .line 1319671
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319672
    iget-object v1, p0, LX/8GX;->b:LX/75Q;

    .line 1319673
    iget-object v0, p1, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v0

    .line 1319674
    check-cast v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    invoke-virtual {v1, v0}, LX/75Q;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1319675
    iget-object v1, p0, LX/8GX;->b:LX/75Q;

    .line 1319676
    new-instance v2, LX/75N;

    iget-object v3, v1, LX/75Q;->e:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/facebook/photos/base/media/PhotoItem;->w()Ljava/lang/String;

    move-result-object v4

    iget-object p0, v1, LX/75Q;->c:LX/6Z0;

    invoke-direct {v2, v3, v4, p0}, LX/75N;-><init>(Landroid/content/Context;Ljava/lang/String;LX/6Z0;)V

    .line 1319677
    iget-object v3, v1, LX/75Q;->e:Landroid/content/Context;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v4}, LX/3nE;->a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;

    .line 1319678
    invoke-virtual {v1, p1}, LX/75Q;->b(Lcom/facebook/photos/base/media/PhotoItem;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1319679
    return v0
.end method

.method public static b(LX/0QB;)LX/8GX;
    .locals 8

    .prologue
    .line 1319680
    new-instance v3, LX/8GX;

    invoke-static {p0}, LX/75Q;->a(LX/0QB;)LX/75Q;

    move-result-object v0

    check-cast v0, LX/75Q;

    invoke-static {p0}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v1

    check-cast v1, LX/75F;

    .line 1319681
    new-instance v7, LX/8JD;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    const-class v4, Landroid/content/Context;

    const-class v5, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v4, v5}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v5

    check-cast v5, LX/1HI;

    invoke-static {p0}, LX/8I3;->b(LX/0QB;)LX/8I2;

    move-result-object v6

    check-cast v6, LX/8I2;

    invoke-direct {v7, v2, v4, v5, v6}, LX/8JD;-><init>(LX/03V;Landroid/content/Context;LX/1HI;LX/8I2;)V

    .line 1319682
    move-object v2, v7

    .line 1319683
    check-cast v2, LX/8JD;

    invoke-direct {v3, v0, v1, v2}, LX/8GX;-><init>(LX/75Q;LX/75F;LX/8JD;)V

    .line 1319684
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)I
    .locals 5

    .prologue
    .line 1319685
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319686
    iget-object v0, p0, LX/8GX;->a:LX/8JD;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 1319687
    const/4 v2, 0x0

    .line 1319688
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, v1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 1319689
    const-string v4, "Orientation"

    const/4 p0, 0x1

    invoke-virtual {v3, v4, p0}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v3

    const/4 v4, 0x0

    .line 1319690
    packed-switch v3, :pswitch_data_0

    .line 1319691
    :goto_0
    :pswitch_0
    move v2, v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1319692
    :goto_1
    move v0, v2

    .line 1319693
    return v0

    .line 1319694
    :catch_0
    move-exception v3

    .line 1319695
    iget-object v4, v0, LX/8JD;->a:LX/03V;

    const-string p0, "RotationManager"

    const-string p1, "Error checking exif"

    invoke-virtual {v4, p0, p1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1319696
    :pswitch_1
    const/16 v4, 0x5a

    goto :goto_0

    .line 1319697
    :pswitch_2
    const/16 v4, 0xb4

    goto :goto_0

    .line 1319698
    :pswitch_3
    const/16 v4, 0x10e

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/facebook/photos/base/media/PhotoItem;I)I
    .locals 9

    .prologue
    .line 1319615
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319616
    if-ltz p2, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1319617
    iget-object v0, p0, LX/8GX;->a:LX/8JD;

    .line 1319618
    const/16 v7, 0x10e

    const/16 v6, 0x5a

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1319619
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/media/data/MediaData;->k()LX/4gP;

    move-result-object v3

    .line 1319620
    iput p2, v3, LX/4gP;->e:I

    .line 1319621
    move-object v5, v3

    .line 1319622
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    .line 1319623
    iget v4, v3, Lcom/facebook/ipc/media/data/MediaData;->mOrientation:I

    move v3, v4

    .line 1319624
    if-eq v3, v6, :cond_0

    if-ne v3, v7, :cond_5

    :cond_0
    move v4, v1

    :goto_1
    if-eq p2, v6, :cond_1

    if-ne p2, v7, :cond_6

    :cond_1
    move v3, v1

    :goto_2
    if-eq v4, v3, :cond_7

    .line 1319625
    :goto_3
    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1319626
    iget v2, v1, Lcom/facebook/ipc/media/data/MediaData;->mAspectRatio:F

    move v1, v2

    .line 1319627
    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 1319628
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v2

    .line 1319629
    iget v3, v2, Lcom/facebook/ipc/media/data/MediaData;->mAspectRatio:F

    move v2, v3

    .line 1319630
    div-float/2addr v1, v2

    .line 1319631
    iput v1, v5, LX/4gP;->h:F

    .line 1319632
    :cond_2
    iget-object v1, p1, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/data/LocalMediaData;->e()LX/4gN;

    move-result-object v1

    invoke-virtual {v5}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v1

    invoke-virtual {v1}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v1

    iput-object v1, p1, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1319633
    iget-object v1, p1, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1319634
    iput p2, v1, Lcom/facebook/photos/base/photos/LocalPhoto;->e:I

    .line 1319635
    iget-object v1, v0, LX/8JD;->d:LX/8I2;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v3

    .line 1319636
    iget-object v2, v1, LX/8I2;->d:Ljava/util/Map;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319637
    iget-object v1, v0, LX/8JD;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1319638
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1319639
    const-string v3, "orientation"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1319640
    :try_start_0
    const-string v3, "%s = ?"

    const-string v4, "_id"

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1319641
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 1319642
    sget-object v5, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v5, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1319643
    if-lez v2, :cond_3

    .line 1319644
    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1319645
    :cond_3
    :goto_4
    invoke-static {p0, p1}, LX/8GX;->a(LX/8GX;Lcom/facebook/photos/base/media/PhotoItem;)I

    move-result v0

    .line 1319646
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319647
    iget-object v1, p0, LX/8GX;->c:LX/75F;

    .line 1319648
    iget-object v2, p1, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v2, v2

    .line 1319649
    check-cast v2, LX/74x;

    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/75F;->a(LX/74x;Ljava/util/List;)V

    .line 1319650
    return v0

    .line 1319651
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1319652
    :catch_0
    move-exception v1

    .line 1319653
    iget-object v2, v0, LX/8JD;->a:LX/03V;

    const-string v3, "RotationManager"

    const-string v4, "Error writing content resolver"

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_5
    move v4, v2

    .line 1319654
    goto/16 :goto_1

    :cond_6
    move v3, v2

    goto/16 :goto_2

    :cond_7
    move v1, v2

    goto/16 :goto_3
.end method

.method public final a(Landroid/net/Uri;I)V
    .locals 5

    .prologue
    .line 1319655
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319656
    iget-object v0, p0, LX/8GX;->a:LX/8JD;

    .line 1319657
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 1319658
    :try_start_0
    new-instance v2, Landroid/media/ExifInterface;

    invoke-direct {v2, v1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 1319659
    const-string v3, "Orientation"

    const/4 v4, 0x1

    .line 1319660
    sparse-switch p2, :sswitch_data_0

    .line 1319661
    :goto_0
    :sswitch_0
    move v4, v4

    .line 1319662
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1319663
    invoke-virtual {v2}, Landroid/media/ExifInterface;->saveAttributes()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1319664
    :goto_1
    iget-object v1, v0, LX/8JD;->c:LX/1HI;

    invoke-virtual {v1, p1}, LX/1HI;->a(Landroid/net/Uri;)V

    .line 1319665
    return-void

    .line 1319666
    :catch_0
    move-exception v2

    .line 1319667
    iget-object v3, v0, LX/8JD;->a:LX/03V;

    const-string v4, "RotationManager"

    const-string p0, "Error writing exif"

    invoke-virtual {v3, v4, p0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1319668
    :sswitch_1
    const/4 v4, 0x6

    goto :goto_0

    .line 1319669
    :sswitch_2
    const/4 v4, 0x3

    goto :goto_0

    .line 1319670
    :sswitch_3
    const/16 v4, 0x8

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method
