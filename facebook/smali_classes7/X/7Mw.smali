.class public final LX/7Mw;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7My;


# direct methods
.method public constructor <init>(LX/7My;)V
    .locals 0

    .prologue
    .line 1199504
    iput-object p1, p0, LX/7Mw;->a:LX/7My;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1199503
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1199493
    check-cast p1, LX/2ou;

    .line 1199494
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PAUSED:LX/2qV;

    if-ne v0, v1, :cond_1

    .line 1199495
    iget-object v0, p0, LX/7Mw;->a:LX/7My;

    iget-object v0, v0, LX/7My;->c:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->i()V

    .line 1199496
    :cond_0
    :goto_0
    return-void

    .line 1199497
    :cond_1
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-ne v0, v1, :cond_2

    .line 1199498
    iget-object v0, p0, LX/7Mw;->a:LX/7My;

    iget-object v0, v0, LX/7My;->c:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->j()V

    goto :goto_0

    .line 1199499
    :cond_2
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_3

    .line 1199500
    iget-object v0, p0, LX/7Mw;->a:LX/7My;

    invoke-static {v0}, LX/7My;->u(LX/7My;)V

    goto :goto_0

    .line 1199501
    :cond_3
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 1199502
    iget-object v0, p0, LX/7Mw;->a:LX/7My;

    iget-object v0, v0, LX/7My;->c:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->i()V

    goto :goto_0
.end method
