.class public final enum LX/7Fz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Fz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Fz;

.field public static final enum DELTA_FORCED_FETCH_NO_ARGS:LX/7Fz;

.field public static final enum ERROR_CODE_FROM_SERVER:LX/7Fz;

.field public static final enum GATEKEEPER_CHANGED:LX/7Fz;

.field public static final enum MISSED_DELTA:LX/7Fz;

.field public static final enum NONE:LX/7Fz;

.field public static final enum NO_EXISTING_SEQUENCE_ID:LX/7Fz;

.field public static final enum NO_EXISTING_SYNC_TOKEN:LX/7Fz;

.field public static final enum RECOVERY_FROM_UNCAUGHT_EXCEPTION:LX/7Fz;

.field public static final enum UNCAUGHT_EXCEPTION:LX/7Fz;

.field public static final enum USER_REQUESTED:LX/7Fz;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1188587
    new-instance v0, LX/7Fz;

    const-string v1, "NO_EXISTING_SYNC_TOKEN"

    invoke-direct {v0, v1, v3}, LX/7Fz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Fz;->NO_EXISTING_SYNC_TOKEN:LX/7Fz;

    .line 1188588
    new-instance v0, LX/7Fz;

    const-string v1, "NO_EXISTING_SEQUENCE_ID"

    invoke-direct {v0, v1, v4}, LX/7Fz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Fz;->NO_EXISTING_SEQUENCE_ID:LX/7Fz;

    .line 1188589
    new-instance v0, LX/7Fz;

    const-string v1, "DELTA_FORCED_FETCH_NO_ARGS"

    invoke-direct {v0, v1, v5}, LX/7Fz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Fz;->DELTA_FORCED_FETCH_NO_ARGS:LX/7Fz;

    .line 1188590
    new-instance v0, LX/7Fz;

    const-string v1, "UNCAUGHT_EXCEPTION"

    invoke-direct {v0, v1, v6}, LX/7Fz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Fz;->UNCAUGHT_EXCEPTION:LX/7Fz;

    .line 1188591
    new-instance v0, LX/7Fz;

    const-string v1, "RECOVERY_FROM_UNCAUGHT_EXCEPTION"

    invoke-direct {v0, v1, v7}, LX/7Fz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Fz;->RECOVERY_FROM_UNCAUGHT_EXCEPTION:LX/7Fz;

    .line 1188592
    new-instance v0, LX/7Fz;

    const-string v1, "MISSED_DELTA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7Fz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Fz;->MISSED_DELTA:LX/7Fz;

    .line 1188593
    new-instance v0, LX/7Fz;

    const-string v1, "ERROR_CODE_FROM_SERVER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/7Fz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Fz;->ERROR_CODE_FROM_SERVER:LX/7Fz;

    .line 1188594
    new-instance v0, LX/7Fz;

    const-string v1, "USER_REQUESTED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/7Fz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Fz;->USER_REQUESTED:LX/7Fz;

    .line 1188595
    new-instance v0, LX/7Fz;

    const-string v1, "GATEKEEPER_CHANGED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/7Fz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Fz;->GATEKEEPER_CHANGED:LX/7Fz;

    .line 1188596
    new-instance v0, LX/7Fz;

    const-string v1, "NONE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/7Fz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Fz;->NONE:LX/7Fz;

    .line 1188597
    const/16 v0, 0xa

    new-array v0, v0, [LX/7Fz;

    sget-object v1, LX/7Fz;->NO_EXISTING_SYNC_TOKEN:LX/7Fz;

    aput-object v1, v0, v3

    sget-object v1, LX/7Fz;->NO_EXISTING_SEQUENCE_ID:LX/7Fz;

    aput-object v1, v0, v4

    sget-object v1, LX/7Fz;->DELTA_FORCED_FETCH_NO_ARGS:LX/7Fz;

    aput-object v1, v0, v5

    sget-object v1, LX/7Fz;->UNCAUGHT_EXCEPTION:LX/7Fz;

    aput-object v1, v0, v6

    sget-object v1, LX/7Fz;->RECOVERY_FROM_UNCAUGHT_EXCEPTION:LX/7Fz;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7Fz;->MISSED_DELTA:LX/7Fz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7Fz;->ERROR_CODE_FROM_SERVER:LX/7Fz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7Fz;->USER_REQUESTED:LX/7Fz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7Fz;->GATEKEEPER_CHANGED:LX/7Fz;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7Fz;->NONE:LX/7Fz;

    aput-object v2, v0, v1

    sput-object v0, LX/7Fz;->$VALUES:[LX/7Fz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1188586
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Fz;
    .locals 1

    .prologue
    .line 1188585
    const-class v0, LX/7Fz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Fz;

    return-object v0
.end method

.method public static values()[LX/7Fz;
    .locals 1

    .prologue
    .line 1188584
    sget-object v0, LX/7Fz;->$VALUES:[LX/7Fz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Fz;

    return-object v0
.end method
