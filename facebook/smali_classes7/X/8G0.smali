.class public final LX/8G0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/creativeediting/model/FetchFramePacksGraphQLModels$FetchFramePacksQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8G2;


# direct methods
.method public constructor <init>(LX/8G2;)V
    .locals 0

    .prologue
    .line 1318414
    iput-object p1, p0, LX/8G0;->a:LX/8G2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1318415
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 1318416
    if-eqz p1, :cond_0

    .line 1318417
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1318418
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1318419
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1318420
    :goto_1
    return-object v0

    .line 1318421
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1318422
    check-cast v0, Lcom/facebook/photos/creativeediting/model/FetchFramePacksGraphQLModels$FetchFramePacksQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FetchFramePacksGraphQLModels$FetchFramePacksQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1318423
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_0

    .line 1318424
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1318425
    check-cast v0, Lcom/facebook/photos/creativeediting/model/FetchFramePacksGraphQLModels$FetchFramePacksQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FetchFramePacksGraphQLModels$FetchFramePacksQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v2, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-virtual {v1, v0, v3, v2}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 1318426
    if-eqz v0, :cond_5

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 1318427
    :goto_2
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1318428
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_3
    if-ge v2, v6, :cond_7

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 1318429
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->bd_()J

    move-result-wide v8

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->b()J

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, LX/8GN;->a(JJ)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1318430
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1318431
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1318432
    :cond_5
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1318433
    move-object v1, v0

    goto :goto_2

    .line 1318434
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v4, v3

    :goto_4
    if-ge v4, v8, :cond_4

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1318435
    iget-object v9, p0, LX/8G0;->a:LX/8G2;

    iget-object v9, v9, LX/8G2;->d:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    invoke-virtual {v9, v0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)V

    .line 1318436
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    .line 1318437
    :cond_7
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_1
.end method
