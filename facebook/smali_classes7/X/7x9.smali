.class public LX/7x9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field public final b:LX/1nG;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/1nG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1277050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1277051
    iput-object p1, p0, LX/7x9;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1277052
    iput-object p2, p0, LX/7x9;->b:LX/1nG;

    .line 1277053
    return-void
.end method

.method public static b(LX/0QB;)LX/7x9;
    .locals 3

    .prologue
    .line 1277054
    new-instance v2, LX/7x9;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v1

    check-cast v1, LX/1nG;

    invoke-direct {v2, v0, v1}, LX/7x9;-><init>(Lcom/facebook/content/SecureContextHelper;LX/1nG;)V

    .line 1277055
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)V
    .locals 2

    .prologue
    .line 1277056
    const v0, 0x7f0d0da0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1277057
    if-nez v0, :cond_0

    .line 1277058
    :goto_0
    return-void

    .line 1277059
    :cond_0
    new-instance v1, LX/7x8;

    invoke-direct {v1, p0, p1}, LX/7x8;-><init>(LX/7x9;Landroid/view/View;)V

    invoke-virtual {v0, p2, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(LX/3Ab;LX/7wC;)V

    goto :goto_0
.end method
