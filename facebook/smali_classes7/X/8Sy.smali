.class public LX/8Sy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/8TY;


# direct methods
.method public constructor <init>(LX/8TY;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1347984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1347985
    iput-object p1, p0, LX/8Sy;->a:LX/8TY;

    .line 1347986
    return-void
.end method

.method public static a(LX/0QB;)LX/8Sy;
    .locals 4

    .prologue
    .line 1347987
    const-class v1, LX/8Sy;

    monitor-enter v1

    .line 1347988
    :try_start_0
    sget-object v0, LX/8Sy;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1347989
    sput-object v2, LX/8Sy;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1347990
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1347991
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1347992
    new-instance p0, LX/8Sy;

    invoke-static {v0}, LX/8TY;->a(LX/0QB;)LX/8TY;

    move-result-object v3

    check-cast v3, LX/8TY;

    invoke-direct {p0, v3}, LX/8Sy;-><init>(LX/8TY;)V

    .line 1347993
    move-object v0, p0

    .line 1347994
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1347995
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8Sy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1347996
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1347997
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
