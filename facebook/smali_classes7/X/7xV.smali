.class public LX/7xV;
.super LX/1OM;
.source ""

# interfaces
.implements LX/7xU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/62U;",
        ">;",
        "LX/7xU;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/7we;


# direct methods
.method public constructor <init>(LX/7we;)V
    .locals 0

    .prologue
    .line 1277416
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1277417
    iput-object p1, p0, LX/7xV;->b:LX/7we;

    .line 1277418
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 1277419
    sget-object v0, LX/7xS;->a:[I

    invoke-static {}, LX/7xT;->values()[LX/7xT;

    move-result-object v1

    aget-object v1, v1, p2

    invoke-virtual {v1}, LX/7xT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1277420
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1277421
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030516

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1277422
    new-instance v1, LX/7xY;

    invoke-direct {v1, v0}, LX/7xY;-><init>(Landroid/view/View;)V

    move-object v0, v1

    .line 1277423
    :goto_0
    return-object v0

    .line 1277424
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030517

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1277425
    new-instance v1, LX/7xb;

    invoke-direct {v1, v0}, LX/7xb;-><init>(Landroid/view/View;)V

    move-object v0, v1

    .line 1277426
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 1277427
    check-cast p1, LX/62U;

    .line 1277428
    sget-object v0, LX/7xS;->a:[I

    invoke-static {}, LX/7xT;->values()[LX/7xT;

    move-result-object v1

    .line 1277429
    iget v2, p1, LX/1a1;->e:I

    move v2, v2

    .line 1277430
    aget-object v1, v1, v2

    invoke-virtual {v1}, LX/7xT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1277431
    :goto_0
    return-void

    .line 1277432
    :pswitch_0
    const/4 v0, 0x1

    .line 1277433
    check-cast p1, LX/7xY;

    .line 1277434
    iget-object v1, p0, LX/7xV;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    move v1, v0

    .line 1277435
    :goto_1
    iget-object v0, p0, LX/7xV;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    iget-object v2, p0, LX/7xV;->b:LX/7we;

    invoke-virtual {p1, p2, v0, v1, v2}, LX/7xY;->a(ILcom/facebook/events/tickets/modal/model/EventTicketTierModel;ZLX/7we;)V

    .line 1277436
    goto :goto_0

    .line 1277437
    :pswitch_1
    check-cast p1, LX/7xb;

    iget-object v0, p0, LX/7xV;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    invoke-virtual {p1, v0}, LX/7xb;->a(Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;)V

    goto :goto_0

    .line 1277438
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V
    .locals 1

    .prologue
    .line 1277439
    iget-object v0, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    iput-object v0, p0, LX/7xV;->a:LX/0Px;

    .line 1277440
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    .line 1277441
    iget-object v0, p0, LX/7xV;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    .line 1277442
    iget-object v1, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->d:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;->POST_SALE:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    if-eq v1, v2, :cond_0

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->d:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;->SOLD_OUT:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    if-ne v0, v1, :cond_1

    .line 1277443
    :cond_0
    sget-object v0, LX/7xT;->NOT_AVAILABLE_ITEM:LX/7xT;

    invoke-virtual {v0}, LX/7xT;->ordinal()I

    move-result v0

    .line 1277444
    :goto_0
    return v0

    :cond_1
    sget-object v0, LX/7xT;->ITEM:LX/7xT;

    invoke-virtual {v0}, LX/7xT;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1277445
    iget-object v0, p0, LX/7xV;->a:LX/0Px;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/7xV;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
