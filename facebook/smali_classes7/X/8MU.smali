.class public final LX/8MU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/7ml;

.field public final synthetic b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)V
    .locals 0

    .prologue
    .line 1334454
    iput-object p1, p0, LX/8MU;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iput-object p2, p0, LX/8MU;->a:LX/7ml;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 12

    .prologue
    .line 1334455
    iget-object v0, p0, LX/8MU;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v1, p0, LX/8MU;->a:LX/7ml;

    .line 1334456
    iget-object v3, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->n:LX/8Mb;

    .line 1334457
    iget-object v4, v1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v4, v4

    .line 1334458
    sget-object v5, LX/5Ro;->COMPOST:LX/5Ro;

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, LX/8Mb;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/5Ro;Z)V

    .line 1334459
    iget-boolean v3, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->C:Z

    if-eqz v3, :cond_0

    .line 1334460
    iget-object v7, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    iget-object v8, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->D:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1334461
    iget-object v7, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->B:Landroid/os/Handler;

    iget-object v8, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->O:Ljava/lang/Runnable;

    invoke-static {v7, v8}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1334462
    new-instance v7, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder$19;

    invoke-direct {v7, v0, v1}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder$19;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)V

    iput-object v7, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->O:Ljava/lang/Runnable;

    .line 1334463
    iget-object v7, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->B:Landroid/os/Handler;

    iget-object v8, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->O:Ljava/lang/Runnable;

    const-wide/16 v9, 0xbb8

    const v11, -0x562d808b

    invoke-static {v7, v8, v9, v10, v11}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1334464
    :cond_0
    iget-object v0, p0, LX/8MU;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    iget-object v1, p0, LX/8MU;->a:LX/7ml;

    invoke-virtual {v1}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "view_holder"

    invoke-virtual {v0, v1, v2}, LX/1RW;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1334465
    const/4 v0, 0x1

    return v0
.end method
