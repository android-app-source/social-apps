.class public final LX/7YO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;

.field public final synthetic b:LX/7YP;


# direct methods
.method public constructor <init>(LX/7YP;Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;)V
    .locals 0

    .prologue
    .line 1219742
    iput-object p1, p0, LX/7YO;->b:LX/7YP;

    iput-object p2, p0, LX/7YO;->a:Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4

    .prologue
    .line 1219743
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1219744
    const-string v1, "zeroBuyPromoParams"

    iget-object v2, p0, LX/7YO;->a:Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1219745
    iget-object v1, p0, LX/7YO;->b:LX/7YP;

    iget-object v1, v1, LX/7YP;->a:LX/0aG;

    const-string v2, "zero_buy_promo"

    const v3, -0x7e9f7e01

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 1219746
    new-instance v2, LX/7YN;

    invoke-direct {v2, p0}, LX/7YN;-><init>(LX/7YO;)V

    iget-object v0, p0, LX/7YO;->b:LX/7YP;

    iget-object v0, v0, LX/7YP;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
