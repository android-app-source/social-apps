.class public LX/8BN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/8BN;


# instance fields
.field public a:Ljava/util/Map;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/media/upload/common/UploadSession;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0TD;

.field private final c:LX/8BU;

.field private final d:LX/6b8;


# direct methods
.method public constructor <init>(LX/0TD;LX/8BU;LX/6b8;)V
    .locals 1
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1309110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309111
    iput-object p1, p0, LX/8BN;->b:LX/0TD;

    .line 1309112
    iput-object p2, p0, LX/8BN;->c:LX/8BU;

    .line 1309113
    iput-object p3, p0, LX/8BN;->d:LX/6b8;

    .line 1309114
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8BN;->a:Ljava/util/Map;

    .line 1309115
    return-void
.end method

.method public static a(LX/0QB;)LX/8BN;
    .locals 6

    .prologue
    .line 1309116
    sget-object v0, LX/8BN;->e:LX/8BN;

    if-nez v0, :cond_1

    .line 1309117
    const-class v1, LX/8BN;

    monitor-enter v1

    .line 1309118
    :try_start_0
    sget-object v0, LX/8BN;->e:LX/8BN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1309119
    if-eqz v2, :cond_0

    .line 1309120
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1309121
    new-instance p0, LX/8BN;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    const-class v4, LX/8BU;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/8BU;

    const-class v5, LX/6b8;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/6b8;

    invoke-direct {p0, v3, v4, v5}, LX/8BN;-><init>(LX/0TD;LX/8BU;LX/6b8;)V

    .line 1309122
    move-object v0, p0

    .line 1309123
    sput-object v0, LX/8BN;->e:LX/8BN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1309124
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1309125
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1309126
    :cond_1
    sget-object v0, LX/8BN;->e:LX/8BN;

    return-object v0

    .line 1309127
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1309128
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/8BN;Lcom/facebook/ipc/media/MediaItem;Lcom/facebook/media/upload/MediaUploadParameters;LX/6b7;Z)LX/8BM;
    .locals 10

    .prologue
    .line 1309129
    invoke-static {p3}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v0

    .line 1309130
    sget-object v1, LX/74R;->MEDIA_UPLOAD_START:LX/74R;

    invoke-static {p3, v1, v0}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1309131
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    .line 1309132
    sget-object v1, LX/4gF;->VIDEO:LX/4gF;

    if-ne v0, v1, :cond_1

    .line 1309133
    iget-object v0, p2, Lcom/facebook/media/upload/MediaUploadParameters;->a:Ljava/lang/String;

    move-object v1, v0

    .line 1309134
    iget-object v0, p0, LX/8BN;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8BT;

    .line 1309135
    if-eqz v0, :cond_3

    .line 1309136
    iget-object v2, v0, LX/8BT;->i:Lcom/facebook/photos/base/media/VideoItem;

    move-object v2, v2

    .line 1309137
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v3

    if-ne v2, v3, :cond_3

    .line 1309138
    instance-of v2, v0, LX/8BT;

    const-string v3, "Sessions do not match types"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1309139
    check-cast v0, LX/8BT;

    .line 1309140
    :goto_0
    move-object v0, v0

    .line 1309141
    if-nez v0, :cond_0

    .line 1309142
    iget-object v0, p0, LX/8BN;->c:LX/8BU;

    .line 1309143
    new-instance v3, LX/8BT;

    const-class v4, LX/8Bg;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/8Bg;

    const-class v5, LX/8Bc;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/8Bc;

    const-class v6, LX/8BX;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/8BX;

    .line 1309144
    new-instance v8, LX/8BO;

    invoke-static {v0}, LX/7zS;->a(LX/0QB;)LX/7zS;

    move-result-object v7

    check-cast v7, LX/7zS;

    invoke-direct {v8, v7}, LX/8BO;-><init>(LX/7zS;)V

    .line 1309145
    move-object v7, v8

    .line 1309146
    check-cast v7, LX/8BO;

    invoke-static {v0}, LX/6b9;->a(LX/0QB;)LX/6b9;

    move-result-object v8

    check-cast v8, LX/6b9;

    move-object v9, p3

    invoke-direct/range {v3 .. v9}, LX/8BT;-><init>(LX/8Bg;LX/8Bc;LX/8BX;LX/8BO;LX/6b9;LX/6b7;)V

    .line 1309147
    move-object v0, v3

    .line 1309148
    iget-object v2, p0, LX/8BN;->a:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309149
    :cond_0
    invoke-virtual {v0, p1, p2, p4}, LX/8BT;->a(Lcom/facebook/ipc/media/MediaItem;Lcom/facebook/media/upload/MediaUploadParameters;Z)LX/8BM;

    move-result-object v0

    .line 1309150
    iget-object v2, p0, LX/8BN;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309151
    iget-object v1, v0, LX/8BM;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1309152
    invoke-static {p3}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v2

    .line 1309153
    const-string v3, "fbid"

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309154
    sget-object v3, LX/74R;->MEDIA_UPLOAD_SUCCESS:LX/74R;

    invoke-static {p3, v3, v2}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1309155
    return-object v0

    .line 1309156
    :cond_1
    sget-object v1, LX/4gF;->PHOTO:LX/4gF;

    if-ne v0, v1, :cond_2

    .line 1309157
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Photo uploads are currently unsupported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1309158
    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unsupported media type passed into uploader"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
