.class public final enum LX/7TP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7TP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7TP;

.field public static final enum AudioOutputRouteBluetooth:LX/7TP;

.field public static final enum AudioOutputRouteEarpiece:LX/7TP;

.field public static final enum AudioOutputRouteHeadset:LX/7TP;

.field public static final enum AudioOutputRouteSpeakerphone:LX/7TP;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1210600
    new-instance v0, LX/7TP;

    const-string v1, "AudioOutputRouteBluetooth"

    invoke-direct {v0, v1, v2}, LX/7TP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TP;->AudioOutputRouteBluetooth:LX/7TP;

    .line 1210601
    new-instance v0, LX/7TP;

    const-string v1, "AudioOutputRouteHeadset"

    invoke-direct {v0, v1, v3}, LX/7TP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TP;->AudioOutputRouteHeadset:LX/7TP;

    .line 1210602
    new-instance v0, LX/7TP;

    const-string v1, "AudioOutputRouteEarpiece"

    invoke-direct {v0, v1, v4}, LX/7TP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TP;->AudioOutputRouteEarpiece:LX/7TP;

    .line 1210603
    new-instance v0, LX/7TP;

    const-string v1, "AudioOutputRouteSpeakerphone"

    invoke-direct {v0, v1, v5}, LX/7TP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7TP;->AudioOutputRouteSpeakerphone:LX/7TP;

    .line 1210604
    const/4 v0, 0x4

    new-array v0, v0, [LX/7TP;

    sget-object v1, LX/7TP;->AudioOutputRouteBluetooth:LX/7TP;

    aput-object v1, v0, v2

    sget-object v1, LX/7TP;->AudioOutputRouteHeadset:LX/7TP;

    aput-object v1, v0, v3

    sget-object v1, LX/7TP;->AudioOutputRouteEarpiece:LX/7TP;

    aput-object v1, v0, v4

    sget-object v1, LX/7TP;->AudioOutputRouteSpeakerphone:LX/7TP;

    aput-object v1, v0, v5

    sput-object v0, LX/7TP;->$VALUES:[LX/7TP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1210605
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7TP;
    .locals 1

    .prologue
    .line 1210606
    const-class v0, LX/7TP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7TP;

    return-object v0
.end method

.method public static values()[LX/7TP;
    .locals 1

    .prologue
    .line 1210607
    sget-object v0, LX/7TP;->$VALUES:[LX/7TP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7TP;

    return-object v0
.end method
