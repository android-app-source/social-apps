.class public LX/6qB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/auth/pin/model/FetchPageInfoParams;",
        "Lcom/facebook/payments/auth/pin/model/PageInfo;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/6qB;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1150113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150114
    return-void
.end method

.method public static a(LX/0QB;)LX/6qB;
    .locals 3

    .prologue
    .line 1150115
    sget-object v0, LX/6qB;->a:LX/6qB;

    if-nez v0, :cond_1

    .line 1150116
    const-class v1, LX/6qB;

    monitor-enter v1

    .line 1150117
    :try_start_0
    sget-object v0, LX/6qB;->a:LX/6qB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1150118
    if-eqz v2, :cond_0

    .line 1150119
    :try_start_1
    new-instance v0, LX/6qB;

    invoke-direct {v0}, LX/6qB;-><init>()V

    .line 1150120
    move-object v0, v0

    .line 1150121
    sput-object v0, LX/6qB;->a:LX/6qB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1150122
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1150123
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1150124
    :cond_1
    sget-object v0, LX/6qB;->a:LX/6qB;

    return-object v0

    .line 1150125
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1150126
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 1150127
    check-cast p1, Lcom/facebook/payments/auth/pin/model/FetchPageInfoParams;

    .line 1150128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1150129
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "q"

    const-string v3, "page(%s) { name }"

    .line 1150130
    iget-object v4, p1, Lcom/facebook/payments/auth/pin/model/FetchPageInfoParams;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1150131
    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150132
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "fetch_page_info"

    .line 1150133
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1150134
    move-object v1, v1

    .line 1150135
    const-string v2, "GET"

    .line 1150136
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1150137
    move-object v1, v1

    .line 1150138
    const-string v2, "graphql"

    .line 1150139
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1150140
    move-object v1, v1

    .line 1150141
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1150142
    move-object v0, v1

    .line 1150143
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1150144
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1150145
    move-object v0, v0

    .line 1150146
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1150147
    check-cast p1, Lcom/facebook/payments/auth/pin/model/FetchPageInfoParams;

    .line 1150148
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1150149
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1150150
    iget-object v1, p1, Lcom/facebook/payments/auth/pin/model/FetchPageInfoParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1150151
    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1150152
    const-string v1, "name"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 1150153
    if-eqz v1, :cond_0

    new-instance v0, Lcom/facebook/payments/auth/pin/model/PageInfo;

    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/payments/auth/pin/model/PageInfo;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
