.class public final LX/75m;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/75n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/75n;)V
    .locals 1

    .prologue
    .line 1169947
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1169948
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/75m;->a:Ljava/lang/ref/WeakReference;

    .line 1169949
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 1169950
    iget-object v0, p0, LX/75m;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/75n;

    .line 1169951
    if-eqz v0, :cond_0

    .line 1169952
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 1169953
    const-string v2, "access_token"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1169954
    if-nez v2, :cond_1

    .line 1169955
    const-string v1, "access_token parameter not found."

    invoke-static {p1, v1}, LX/75n;->a(Landroid/os/Message;Ljava/lang/String;)V

    .line 1169956
    :cond_0
    :goto_0
    return-void

    .line 1169957
    :cond_1
    iget-object v1, v0, LX/75n;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1169958
    if-eqz v1, :cond_2

    .line 1169959
    iget-object v3, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1169960
    if-nez v3, :cond_3

    .line 1169961
    :cond_2
    const-string v1, "no logged in user"

    invoke-static {p1, v1}, LX/75n;->a(Landroid/os/Message;Ljava/lang/String;)V

    goto :goto_0

    .line 1169962
    :cond_3
    new-instance v3, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Params;

    .line 1169963
    iget-object p0, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v1, p0

    .line 1169964
    invoke-direct {v3, v2, v1}, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169965
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1169966
    const-string v2, "access_token"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1169967
    iget-object v2, v0, LX/75n;->c:LX/0aG;

    const-string v3, "platform_extend_access_token"

    const p0, 0x68be56b2

    invoke-static {v2, v3, v1, p0}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v1

    .line 1169968
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 1169969
    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 1169970
    new-instance v3, LX/75l;

    invoke-direct {v3, v0, v2}, LX/75l;-><init>(LX/75n;Landroid/os/Message;)V

    iget-object v2, v0, LX/75n;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v3, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
