.class public LX/8NQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/8LS;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/8LS;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Landroid/os/Bundle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final G:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final H:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final I:Z

.field public final J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final K:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

.field public final L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final M:Z

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:Ljava/lang/String;

.field public final g:Z

.field public final h:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

.field public final i:J

.field public final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Lcom/facebook/ipc/composer/model/MinutiaeTag;

.field public final l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Ljava/lang/String;

.field public final n:J

.field public final o:Z

.field public final p:J

.field public final q:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

.field public final r:Lcom/facebook/share/model/ComposerAppAttribution;

.field public final s:Z

.field public final t:Z

.field public final u:Ljava/lang/String;

.field public final v:Ljava/lang/String;

.field public final w:Z

.field public final x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final z:J
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    .line 1337115
    sget-object v0, LX/8LS;->PROFILE_VIDEO:LX/8LS;

    const-string v1, "profile_video"

    sget-object v2, LX/8LS;->LIVE_VIDEO:LX/8LS;

    const-string v3, "live_video"

    sget-object v4, LX/8LS;->MOMENTS_VIDEO:LX/8LS;

    const-string v5, "moments_video"

    sget-object v6, LX/8LS;->PROFILE_INTRO_CARD_VIDEO:LX/8LS;

    const-string v7, "profile_intro_card"

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/8NQ;->a:Ljava/util/Map;

    .line 1337116
    sget-object v0, LX/8LS;->COMMENT_VIDEO:LX/8LS;

    const-string v1, "VIDEO_COMMENT"

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    sput-object v0, LX/8NQ;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;JZLjava/lang/String;JLX/0Px;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Ljava/lang/String;JLcom/facebook/share/model/ComposerAppAttribution;ZZLX/0am;LX/0am;Lcom/facebook/ipc/composer/model/ProductItemAttachment;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p12    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p21    # Lcom/facebook/ipc/composer/model/ProductItemAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p33    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p35    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p36    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p40    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;",
            "JZ",
            "Ljava/lang/String;",
            "J",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/ipc/composer/model/MinutiaeTag;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/facebook/share/model/ComposerAppAttribution;",
            "ZZ",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/ipc/composer/model/ProductItemAttachment;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/productionprompts/logging/PromptAnalytics;",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1337117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1337118
    invoke-static {p10}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337119
    iput-object p1, p0, LX/8NQ;->c:Ljava/lang/String;

    .line 1337120
    iput-object p2, p0, LX/8NQ;->d:Ljava/lang/String;

    .line 1337121
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    iput-object v2, p0, LX/8NQ;->h:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1337122
    iput-wide p4, p0, LX/8NQ;->e:J

    .line 1337123
    iput-object p7, p0, LX/8NQ;->f:Ljava/lang/String;

    .line 1337124
    iput-boolean p6, p0, LX/8NQ;->g:Z

    .line 1337125
    iput-wide p8, p0, LX/8NQ;->i:J

    .line 1337126
    iput-object p10, p0, LX/8NQ;->j:LX/0Px;

    .line 1337127
    iput-object p11, p0, LX/8NQ;->k:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1337128
    move-object/from16 v0, p12

    iput-object v0, p0, LX/8NQ;->l:Ljava/lang/String;

    .line 1337129
    move-object/from16 v0, p13

    iput-object v0, p0, LX/8NQ;->m:Ljava/lang/String;

    .line 1337130
    move-wide/from16 v0, p14

    iput-wide v0, p0, LX/8NQ;->n:J

    .line 1337131
    move-object/from16 v0, p16

    iput-object v0, p0, LX/8NQ;->r:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1337132
    move/from16 v0, p17

    iput-boolean v0, p0, LX/8NQ;->s:Z

    .line 1337133
    move/from16 v0, p18

    iput-boolean v0, p0, LX/8NQ;->t:Z

    .line 1337134
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object/from16 v0, p19

    invoke-virtual {v0, v2}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, p0, LX/8NQ;->o:Z

    .line 1337135
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p20

    invoke-virtual {v0, v2}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, LX/8NQ;->p:J

    .line 1337136
    move-object/from16 v0, p21

    iput-object v0, p0, LX/8NQ;->q:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1337137
    move-object/from16 v0, p22

    iput-object v0, p0, LX/8NQ;->u:Ljava/lang/String;

    .line 1337138
    move-object/from16 v0, p23

    iput-object v0, p0, LX/8NQ;->v:Ljava/lang/String;

    .line 1337139
    move/from16 v0, p24

    iput-boolean v0, p0, LX/8NQ;->w:Z

    .line 1337140
    move-object/from16 v0, p25

    iput-object v0, p0, LX/8NQ;->x:Ljava/lang/String;

    .line 1337141
    move-object/from16 v0, p26

    iput-object v0, p0, LX/8NQ;->y:Ljava/lang/String;

    .line 1337142
    move-wide/from16 v0, p27

    iput-wide v0, p0, LX/8NQ;->z:J

    .line 1337143
    move-object/from16 v0, p29

    iput-object v0, p0, LX/8NQ;->A:Landroid/os/Bundle;

    .line 1337144
    move-object/from16 v0, p30

    iput-object v0, p0, LX/8NQ;->E:Ljava/lang/String;

    .line 1337145
    move-object/from16 v0, p31

    iput-object v0, p0, LX/8NQ;->F:Ljava/lang/String;

    .line 1337146
    move-object/from16 v0, p32

    iput-object v0, p0, LX/8NQ;->G:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1337147
    move-object/from16 v0, p33

    iput-object v0, p0, LX/8NQ;->H:LX/0Px;

    .line 1337148
    move/from16 v0, p34

    iput-boolean v0, p0, LX/8NQ;->I:Z

    .line 1337149
    move-object/from16 v0, p35

    iput-object v0, p0, LX/8NQ;->B:Ljava/lang/String;

    .line 1337150
    move-object/from16 v0, p36

    iput-object v0, p0, LX/8NQ;->C:Ljava/lang/String;

    .line 1337151
    move-object/from16 v0, p37

    iput-object v0, p0, LX/8NQ;->J:Ljava/lang/String;

    .line 1337152
    move-object/from16 v0, p38

    iput-object v0, p0, LX/8NQ;->K:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 1337153
    move-object/from16 v0, p39

    iput-object v0, p0, LX/8NQ;->D:Ljava/lang/String;

    .line 1337154
    move-object/from16 v0, p40

    iput-object v0, p0, LX/8NQ;->L:Ljava/lang/String;

    .line 1337155
    move/from16 v0, p41

    iput-boolean v0, p0, LX/8NQ;->M:Z

    .line 1337156
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)LX/8NQ;
    .locals 44

    .prologue
    .line 1337157
    new-instance v2, LX/8NQ;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->B()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->D()Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->E()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->H()Z

    move-result v8

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->F()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->C()J

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->M()LX/0Px;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ah()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ai()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aj()J

    move-result-wide v16

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->X()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v18

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->Y()Z

    move-result v19

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->Z()Z

    move-result v20

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->an()LX/5Rn;

    move-result-object v3

    sget-object v21, LX/5Rn;->NORMAL:LX/5Rn;

    move-object/from16 v0, v21

    if-ne v3, v0, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v21

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ao()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v22

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->w()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v23

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aA()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->Q()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->R()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v26

    sget-object v3, LX/8NQ;->a:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->O()LX/8LS;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aB()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aC()J

    move-result-wide v29

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->A()Landroid/os/Bundle;

    move-result-object v31

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aJ()Ljava/lang/String;

    move-result-object v32

    sget-object v3, LX/8NQ;->b:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->O()LX/8LS;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aK()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v34

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aL()LX/0Px;

    move-result-object v35

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aM()Z

    move-result v36

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aD()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aE()Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aO()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aN()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v40

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->G()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aR()Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aS()Z

    move-result v43

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v43}, LX/8NQ;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;JZLjava/lang/String;JLX/0Px;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Ljava/lang/String;JLcom/facebook/share/model/ComposerAppAttribution;ZZLX/0am;LX/0am;Lcom/facebook/ipc/composer/model/ProductItemAttachment;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v2

    :cond_0
    const/4 v3, 0x0

    goto/16 :goto_0
.end method
