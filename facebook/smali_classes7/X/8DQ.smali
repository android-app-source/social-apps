.class public LX/8DQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0SG;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/2Dl;

.field public final d:Ljava/lang/String;

.field public final e:LX/0Tn;

.field public final f:I

.field public final g:I

.field public final h:Ljava/util/Date;

.field public final i:Ljava/util/Date;

.field public final j:LX/8DP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Dl;LX/27f;)V
    .locals 10
    .param p4    # LX/27f;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1312671
    iget-object v4, p4, LX/27f;->name:Ljava/lang/String;

    iget v5, p4, LX/27f;->groupSize:I

    iget v6, p4, LX/27f;->groupCount:I

    iget-object v7, p4, LX/27f;->startDate:Ljava/util/Date;

    iget-object v8, p4, LX/27f;->endDate:Ljava/util/Date;

    iget-object v9, p4, LX/27f;->mConditionalFilter:LX/8DP;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v9}, LX/8DQ;-><init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Dl;Ljava/lang/String;IILjava/util/Date;Ljava/util/Date;LX/8DP;)V

    .line 1312672
    return-void
.end method

.method private constructor <init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Dl;Ljava/lang/String;IILjava/util/Date;Ljava/util/Date;LX/8DP;)V
    .locals 1
    .param p9    # LX/8DP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1312673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1312674
    iput-object p1, p0, LX/8DQ;->a:LX/0SG;

    .line 1312675
    iput-object p2, p0, LX/8DQ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1312676
    iput-object p3, p0, LX/8DQ;->c:LX/2Dl;

    .line 1312677
    iput-object p4, p0, LX/8DQ;->d:Ljava/lang/String;

    .line 1312678
    sget-object v0, LX/8DR;->b:LX/0Tn;

    invoke-virtual {v0, p4}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    move-object v0, v0

    .line 1312679
    iput-object v0, p0, LX/8DQ;->e:LX/0Tn;

    .line 1312680
    iput p5, p0, LX/8DQ;->f:I

    .line 1312681
    iput p6, p0, LX/8DQ;->g:I

    .line 1312682
    iput-object p7, p0, LX/8DQ;->h:Ljava/util/Date;

    .line 1312683
    iput-object p8, p0, LX/8DQ;->i:Ljava/util/Date;

    .line 1312684
    iput-object p9, p0, LX/8DQ;->j:LX/8DP;

    .line 1312685
    return-void
.end method
