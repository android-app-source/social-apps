.class public final LX/6r9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/6qw;",
            "LX/6Dy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/6Dy;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1151776
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1151777
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1151778
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    .line 1151779
    iget-object v3, v0, LX/6Dy;->a:LX/6qw;

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1151780
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/6r9;->a:LX/0P1;

    .line 1151781
    return-void
.end method

.method public static a(LX/0QB;)LX/6r9;
    .locals 6

    .prologue
    .line 1151763
    const-class v1, LX/6r9;

    monitor-enter v1

    .line 1151764
    :try_start_0
    sget-object v0, LX/6r9;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1151765
    sput-object v2, LX/6r9;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1151766
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151767
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1151768
    new-instance v3, LX/6r9;

    .line 1151769
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/6qy;

    invoke-direct {p0, v0}, LX/6qy;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 1151770
    invoke-direct {v3, v4}, LX/6r9;-><init>(Ljava/util/Set;)V

    .line 1151771
    move-object v0, v3

    .line 1151772
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1151773
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6r9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1151774
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1151775
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/6qw;)LX/6qa;
    .locals 2

    .prologue
    .line 1151745
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151746
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6qa;

    .line 1151747
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    sget-object v1, LX/6qw;->SIMPLE:LX/6qw;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6qa;

    goto :goto_0
.end method

.method public final b(LX/6qw;)LX/6qd;
    .locals 2

    .prologue
    .line 1151760
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151761
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6qd;

    .line 1151762
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    sget-object v1, LX/6qw;->SIMPLE:LX/6qw;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6qd;

    goto :goto_0
.end method

.method public final d(LX/6qw;)LX/6Dk;
    .locals 2

    .prologue
    .line 1151757
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151758
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dk;

    .line 1151759
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    sget-object v1, LX/6qw;->SIMPLE:LX/6qw;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dk;

    goto :goto_0
.end method

.method public final e(LX/6qw;)LX/6E0;
    .locals 2

    .prologue
    .line 1151754
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151755
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6E0;

    .line 1151756
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    sget-object v1, LX/6qw;->SIMPLE:LX/6qw;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6E0;

    goto :goto_0
.end method

.method public final f(LX/6qw;)LX/6Ds;
    .locals 2

    .prologue
    .line 1151751
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151752
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ds;

    .line 1151753
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    sget-object v1, LX/6qw;->SIMPLE:LX/6qw;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ds;

    goto :goto_0
.end method

.method public final g(LX/6qw;)LX/6Du;
    .locals 2

    .prologue
    .line 1151748
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151749
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Du;

    .line 1151750
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    sget-object v1, LX/6qw;->SIMPLE:LX/6qw;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Du;

    goto :goto_0
.end method

.method public final h(LX/6qw;)LX/6Dw;
    .locals 2

    .prologue
    .line 1151742
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151743
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dw;

    .line 1151744
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    sget-object v1, LX/6qw;->SIMPLE:LX/6qw;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dw;

    goto :goto_0
.end method

.method public final i(LX/6qw;)LX/6tv;
    .locals 2

    .prologue
    .line 1151739
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151740
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6tv;

    .line 1151741
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6r9;->a:LX/0P1;

    sget-object v1, LX/6qw;->SIMPLE:LX/6qw;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Dy;

    iget-object v0, v0, LX/6Dy;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6tv;

    goto :goto_0
.end method
