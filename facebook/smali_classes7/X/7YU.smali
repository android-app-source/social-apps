.class public abstract LX/7YU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1219984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1219985
    iput-object p1, p0, LX/7YU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1219986
    const-string v0, "campaign_id_key"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->b:Ljava/lang/String;

    .line 1219987
    const-string v0, "type_key"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->c:Ljava/lang/String;

    .line 1219988
    const-string v0, "ttl_key"

    invoke-direct {p0, v0, v2}, LX/7YU;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/7YU;->d:I

    .line 1219989
    const-string v0, "delay_interval_key"

    invoke-direct {p0, v0, v2}, LX/7YU;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/7YU;->e:I

    .line 1219990
    const-string v0, "title_key"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->f:Ljava/lang/String;

    .line 1219991
    const-string v0, "description_text_key"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->g:Ljava/lang/String;

    .line 1219992
    const-string v0, "terms_and_conditions_text_key"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->h:Ljava/lang/String;

    .line 1219993
    const-string v0, "clickable_link_text_key"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->i:Ljava/lang/String;

    .line 1219994
    const-string v0, "clickable_link_url_key"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->j:Ljava/lang/String;

    .line 1219995
    const-string v0, "primary_button_text_key"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->k:Ljava/lang/String;

    .line 1219996
    const-string v0, "primary_button_intent_key"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->l:Ljava/lang/String;

    .line 1219997
    const-string v0, "secondary_button_text_key"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->m:Ljava/lang/String;

    .line 1219998
    const-string v0, "secondary_button_intent_key"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->n:Ljava/lang/String;

    .line 1219999
    const-string v0, "back_button_behavior"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->o:Ljava/lang/String;

    .line 1220000
    const-string v0, "campaign_token_to_refresh_type_key"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->p:Ljava/lang/String;

    .line 1220001
    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V
    .locals 1

    .prologue
    .line 1220002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1220003
    iput-object p1, p0, LX/7YU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1220004
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->b:Ljava/lang/String;

    .line 1220005
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->y()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->c:Ljava/lang/String;

    .line 1220006
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->N()I

    move-result v0

    iput v0, p0, LX/7YU;->d:I

    .line 1220007
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->t()I

    move-result v0

    iput v0, p0, LX/7YU;->e:I

    .line 1220008
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->M()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->f:Ljava/lang/String;

    .line 1220009
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->g:Ljava/lang/String;

    .line 1220010
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->L()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->h:Ljava/lang/String;

    .line 1220011
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->i:Ljava/lang/String;

    .line 1220012
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->j:Ljava/lang/String;

    .line 1220013
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->C()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->k:Ljava/lang/String;

    .line 1220014
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->A()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->l:Ljava/lang/String;

    .line 1220015
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->H()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->m:Ljava/lang/String;

    .line 1220016
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->E()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->n:Ljava/lang/String;

    .line 1220017
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->o:Ljava/lang/String;

    .line 1220018
    invoke-virtual {p2}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YU;->p:Ljava/lang/String;

    .line 1220019
    return-void
.end method

.method private a(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 1219979
    iget-object v1, p0, LX/7YU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 1219983
    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1220020
    iget-object v1, p0, LX/7YU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0hN;)V
    .locals 3

    .prologue
    .line 1219981
    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v1, "campaign_id_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v1, p0, LX/7YU;->b:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "type_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YU;->c:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "ttl_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget v2, p0, LX/7YU;->d:I

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "delay_interval_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget v2, p0, LX/7YU;->e:I

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "title_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YU;->f:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "description_text_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YU;->g:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "terms_and_conditions_text_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YU;->h:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "clickable_link_text_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YU;->i:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "clickable_link_url_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YU;->j:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "primary_button_text_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YU;->k:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "primary_button_intent_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YU;->l:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "secondary_button_text_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YU;->m:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "secondary_button_intent_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YU;->n:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "back_button_behavior"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YU;->o:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    const-string v2, "campaign_token_to_refresh_type_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YU;->p:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1219982
    return-void
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 1219980
    iget-object v1, p0, LX/7YU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {p0}, LX/7YU;->b()LX/0Tn;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public abstract b()LX/0Tn;
.end method
