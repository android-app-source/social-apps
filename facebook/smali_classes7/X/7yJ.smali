.class public LX/7yJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/7yJ;


# instance fields
.field public a:LX/0Zb;

.field private b:Lcom/facebook/performancelogger/PerformanceLogger;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1278921
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1278922
    const/4 v0, 0x0

    iput-object v0, p0, LX/7yJ;->c:Ljava/lang/String;

    .line 1278923
    iput-object p1, p0, LX/7yJ;->a:LX/0Zb;

    .line 1278924
    iput-object p2, p0, LX/7yJ;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 1278925
    return-void
.end method

.method public static a(LX/0QB;)LX/7yJ;
    .locals 5

    .prologue
    .line 1278926
    sget-object v0, LX/7yJ;->d:LX/7yJ;

    if-nez v0, :cond_1

    .line 1278927
    const-class v1, LX/7yJ;

    monitor-enter v1

    .line 1278928
    :try_start_0
    sget-object v0, LX/7yJ;->d:LX/7yJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1278929
    if-eqz v2, :cond_0

    .line 1278930
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1278931
    new-instance p0, LX/7yJ;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v4

    check-cast v4, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {p0, v3, v4}, LX/7yJ;-><init>(LX/0Zb;Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 1278932
    move-object v0, p0

    .line 1278933
    sput-object v0, LX/7yJ;->d:LX/7yJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1278934
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1278935
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1278936
    :cond_1
    sget-object v0, LX/7yJ;->d:LX/7yJ;

    return-object v0

    .line 1278937
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1278938
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1278939
    const-string v0, "TRACKER_FAILURE"

    .line 1278940
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1278941
    const-string v2, "composer"

    .line 1278942
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1278943
    iget-object v2, p0, LX/7yJ;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1278944
    iget-object v2, p0, LX/7yJ;->c:Ljava/lang/String;

    .line 1278945
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1278946
    :cond_0
    move-object v0, v1

    .line 1278947
    const-string v1, "REASON"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1278948
    iget-object v1, p0, LX/7yJ;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1278949
    return-void
.end method
