.class public LX/8Px;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:Lcom/facebook/privacy/PrivacyOperationsClient;

.field private final c:LX/0pZ;

.field public final d:LX/03V;

.field private final e:LX/0kb;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0SG;

.field public i:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public j:LX/1K2;

.field public k:Z

.field public final l:Landroid/content/DialogInterface$OnCancelListener;

.field public final m:Landroid/content/DialogInterface$OnClickListener;

.field public final n:Landroid/content/DialogInterface$OnClickListener;

.field private final o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0pZ;LX/03V;LX/0kb;LX/0Or;LX/0Or;LX/0SG;)V
    .locals 1
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/privacy/gating/IsDefaultPostPrivacyEnabled;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/privacy/gating/IsPostStickyPrivacyUpsellEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            "LX/0pZ;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0kb;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1342274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1342275
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8Px;->k:Z

    .line 1342276
    new-instance v0, LX/8Pt;

    invoke-direct {v0, p0}, LX/8Pt;-><init>(LX/8Px;)V

    iput-object v0, p0, LX/8Px;->l:Landroid/content/DialogInterface$OnCancelListener;

    .line 1342277
    new-instance v0, LX/8Pu;

    invoke-direct {v0, p0}, LX/8Pu;-><init>(LX/8Px;)V

    iput-object v0, p0, LX/8Px;->m:Landroid/content/DialogInterface$OnClickListener;

    .line 1342278
    new-instance v0, LX/8Pv;

    invoke-direct {v0, p0}, LX/8Pv;-><init>(LX/8Px;)V

    iput-object v0, p0, LX/8Px;->n:Landroid/content/DialogInterface$OnClickListener;

    .line 1342279
    const-string v0, "post_privacy_upsell_dialog_controller"

    iput-object v0, p0, LX/8Px;->o:Ljava/lang/String;

    .line 1342280
    iput-object p1, p0, LX/8Px;->a:Landroid/content/res/Resources;

    .line 1342281
    iput-object p2, p0, LX/8Px;->b:Lcom/facebook/privacy/PrivacyOperationsClient;

    .line 1342282
    iput-object p3, p0, LX/8Px;->c:LX/0pZ;

    .line 1342283
    iput-object p4, p0, LX/8Px;->d:LX/03V;

    .line 1342284
    iput-object p5, p0, LX/8Px;->e:LX/0kb;

    .line 1342285
    iput-object p6, p0, LX/8Px;->f:LX/0Or;

    .line 1342286
    iput-object p7, p0, LX/8Px;->g:LX/0Or;

    .line 1342287
    iput-object p8, p0, LX/8Px;->h:LX/0SG;

    .line 1342288
    return-void
.end method

.method public static a$redex0(LX/8Px;LX/5nr;)V
    .locals 12

    .prologue
    .line 1342289
    iget-object v1, p0, LX/8Px;->b:Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v0, p0, LX/8Px;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v0, p0, LX/8Px;->i:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/8Px;->i:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    .line 1342290
    :goto_0
    sget-object v3, LX/8Pw;->a:[I

    iget-object v4, p0, LX/8Px;->j:LX/1K2;

    invoke-virtual {v4}, LX/1K2;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1342291
    iget-object v3, p0, LX/8Px;->d:LX/03V;

    const-string v4, "post_privacy_upsell_dialog_controller"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unable to convert surface to report param: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, LX/8Px;->j:LX/1K2;

    invoke-virtual {v6}, LX/1K2;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1342292
    const/4 v3, 0x0

    :goto_1
    move-object v3, v3

    .line 1342293
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1342294
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 1342295
    const-string v11, "params"

    new-instance v4, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;

    const/4 v7, 0x0

    move-object v5, p1

    move-object v6, v2

    move-object v8, v0

    move-object v9, v3

    invoke-direct/range {v4 .. v9}, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;-><init>(LX/5nr;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;LX/5ns;)V

    invoke-virtual {v10, v11, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1342296
    iget-object v4, v1, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v5, "report_sticky_upsell_action"

    sget-object v7, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v8, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v9, 0x12a2e657

    move-object v6, v10

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    .line 1342297
    invoke-static {v1, v4}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1342298
    sget-object v0, LX/5nr;->CLOSED:LX/5nr;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/5nr;->DECLINED:LX/5nr;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/5nr;->DISMISSED:LX/5nr;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/5nr;->ACCEPTED:LX/5nr;

    if-ne p1, v0, :cond_1

    .line 1342299
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8Px;->k:Z

    .line 1342300
    :cond_1
    return-void

    .line 1342301
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1342302
    :pswitch_0
    sget-object v3, LX/5ns;->NEWSFEED:LX/5ns;

    goto :goto_1

    .line 1342303
    :pswitch_1
    sget-object v3, LX/5ns;->TIMELINE:LX/5ns;

    goto :goto_1

    .line 1342304
    :pswitch_2
    sget-object v3, LX/5ns;->PERMALINK:LX/5ns;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/1K2;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1342305
    iput-object p4, p0, LX/8Px;->j:LX/1K2;

    .line 1342306
    if-nez p3, :cond_1

    .line 1342307
    iget-object v0, p0, LX/8Px;->d:LX/03V;

    const-string v1, "post_privacy_upsell_dialog_controller"

    const-string v2, "null suggested privacy passed to Post Privacy Upsell."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1342308
    :cond_0
    :goto_0
    return-void

    .line 1342309
    :cond_1
    if-nez p2, :cond_2

    .line 1342310
    iget-object v0, p0, LX/8Px;->d:LX/03V;

    const-string v1, "post_privacy_upsell_dialog_controller"

    const-string v2, "null parentView passed in, not showing dialog."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1342311
    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1342312
    iget-object v0, p0, LX/8Px;->d:LX/03V;

    const-string v1, "post_privacy_upsell_dialog_controller"

    const-string v2, "privacy option passed to upsell is missing name"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1342313
    :cond_3
    iget-object v0, p0, LX/8Px;->e:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8Px;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8Px;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1342314
    iput-object p3, p0, LX/8Px;->i:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1342315
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8Px;->k:Z

    .line 1342316
    new-instance v0, LX/0ju;

    invoke-direct {v0, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1342317
    new-instance v1, LX/47x;

    iget-object v2, p0, LX/8Px;->a:Landroid/content/res/Resources;

    invoke-direct {v1, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    const v2, 0x7f081318

    invoke-virtual {v1, v2}, LX/47x;->a(I)LX/47x;

    move-result-object v1

    const-string v2, "%1$s"

    iget-object p2, p0, LX/8Px;->i:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object p2

    new-instance p3, Landroid/text/style/StyleSpan;

    const/4 p1, 0x1

    invoke-direct {p3, p1}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 p1, 0x21

    invoke-virtual {v1, v2, p2, p3, p1}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v1

    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    move-object v1, v1

    .line 1342318
    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    .line 1342319
    new-instance v1, LX/47x;

    iget-object v2, p0, LX/8Px;->a:Landroid/content/res/Resources;

    invoke-direct {v1, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    const v2, 0x7f081319

    invoke-virtual {v1, v2}, LX/47x;->a(I)LX/47x;

    move-result-object v1

    const-string v2, "%1$s"

    iget-object p2, p0, LX/8Px;->i:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object p2

    new-instance p3, Landroid/text/style/StyleSpan;

    const/4 p1, 0x1

    invoke-direct {p3, p1}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 p1, 0x21

    invoke-virtual {v1, v2, p2, p3, p1}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v1

    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    move-object v1, v1

    .line 1342320
    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, LX/8Px;->a:Landroid/content/res/Resources;

    const v2, 0x7f081317

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/8Px;->n:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, LX/8Px;->a:Landroid/content/res/Resources;

    const v2, 0x7f08131a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/8Px;->m:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, LX/8Px;->l:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1342321
    sget-object v0, LX/5nr;->EXPOSED:LX/5nr;

    invoke-static {p0, v0}, LX/8Px;->a$redex0(LX/8Px;LX/5nr;)V

    .line 1342322
    goto/16 :goto_0
.end method
