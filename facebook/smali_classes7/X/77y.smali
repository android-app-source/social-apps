.class public final enum LX/77y;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/77y;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/77y;

.field public static final enum PRIMARY:LX/77y;

.field public static final enum SECONDARY:LX/77y;

.field public static final enum XOUT:LX/77y;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1172291
    new-instance v0, LX/77y;

    const-string v1, "PRIMARY"

    invoke-direct {v0, v1, v2}, LX/77y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77y;->PRIMARY:LX/77y;

    .line 1172292
    new-instance v0, LX/77y;

    const-string v1, "SECONDARY"

    invoke-direct {v0, v1, v3}, LX/77y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77y;->SECONDARY:LX/77y;

    .line 1172293
    new-instance v0, LX/77y;

    const-string v1, "XOUT"

    invoke-direct {v0, v1, v4}, LX/77y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77y;->XOUT:LX/77y;

    .line 1172294
    const/4 v0, 0x3

    new-array v0, v0, [LX/77y;

    sget-object v1, LX/77y;->PRIMARY:LX/77y;

    aput-object v1, v0, v2

    sget-object v1, LX/77y;->SECONDARY:LX/77y;

    aput-object v1, v0, v3

    sget-object v1, LX/77y;->XOUT:LX/77y;

    aput-object v1, v0, v4

    sput-object v0, LX/77y;->$VALUES:[LX/77y;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1172295
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/77y;
    .locals 1

    .prologue
    .line 1172296
    const-class v0, LX/77y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/77y;

    return-object v0
.end method

.method public static values()[LX/77y;
    .locals 1

    .prologue
    .line 1172297
    sget-object v0, LX/77y;->$VALUES:[LX/77y;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/77y;

    return-object v0
.end method
