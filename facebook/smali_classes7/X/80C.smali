.class public final LX/80C;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1282193
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 1282194
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1282195
    :goto_0
    return v1

    .line 1282196
    :cond_0
    const-string v7, "remaining_count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1282197
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 1282198
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 1282199
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1282200
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1282201
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1282202
    const-string v7, "nodes"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1282203
    invoke-static {p0, p1}, LX/2aD;->b(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1282204
    :cond_2
    const-string v7, "page_info"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1282205
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1282206
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v8, :cond_c

    .line 1282207
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1282208
    :goto_2
    move v4, v6

    .line 1282209
    goto :goto_1

    .line 1282210
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1282211
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1282212
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1282213
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1282214
    if-eqz v0, :cond_5

    .line 1282215
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 1282216
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1

    .line 1282217
    :cond_7
    const-string v11, "has_next_page"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1282218
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v8, v4

    move v4, v7

    .line 1282219
    :cond_8
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_a

    .line 1282220
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1282221
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1282222
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_8

    if-eqz v10, :cond_8

    .line 1282223
    const-string v11, "end_cursor"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1282224
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_3

    .line 1282225
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1282226
    :cond_a
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1282227
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 1282228
    if-eqz v4, :cond_b

    .line 1282229
    invoke-virtual {p1, v7, v8}, LX/186;->a(IZ)V

    .line 1282230
    :cond_b
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_2

    :cond_c
    move v4, v6

    move v8, v6

    move v9, v6

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1282231
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1282232
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1282233
    if-eqz v0, :cond_0

    .line 1282234
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1282235
    invoke-static {p0, v0, p2, p3}, LX/2aD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1282236
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1282237
    if-eqz v0, :cond_3

    .line 1282238
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1282239
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1282240
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1282241
    if-eqz v1, :cond_1

    .line 1282242
    const-string p3, "end_cursor"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1282243
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1282244
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1282245
    if-eqz v1, :cond_2

    .line 1282246
    const-string p3, "has_next_page"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1282247
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1282248
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1282249
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1282250
    if-eqz v0, :cond_4

    .line 1282251
    const-string v1, "remaining_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1282252
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1282253
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1282254
    return-void
.end method
