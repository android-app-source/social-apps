.class public LX/7Sz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/5Pc;

.field public final c:I

.field public final d:I

.field public final e:LX/60w;

.field public f:LX/7T2;

.field public g:Landroid/graphics/SurfaceTexture;

.field public h:LX/7T1;

.field public i:Landroid/view/Surface;

.field public j:Ljavax/microedition/khronos/egl/EGL10;

.field public k:Ljavax/microedition/khronos/egl/EGLDisplay;

.field public l:Ljavax/microedition/khronos/egl/EGLContext;

.field public m:Ljavax/microedition/khronos/egl/EGLSurface;

.field public n:Ljava/nio/ByteBuffer;

.field public o:J

.field public p:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1209427
    const-class v0, LX/7Sz;

    sput-object v0, LX/7Sz;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/5Pc;LX/7Sx;LX/60w;)V
    .locals 2

    .prologue
    .line 1209428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1209429
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    iput-object v0, p0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 1209430
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    iput-object v0, p0, LX/7Sz;->l:Ljavax/microedition/khronos/egl/EGLContext;

    .line 1209431
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    iput-object v0, p0, LX/7Sz;->m:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 1209432
    iget v0, p2, LX/7Sx;->d:I

    if-lez v0, :cond_0

    iget v0, p2, LX/7Sx;->e:I

    if-gtz v0, :cond_1

    .line 1209433
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1209434
    :cond_1
    iput-object p1, p0, LX/7Sz;->b:LX/5Pc;

    .line 1209435
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v0, p0, LX/7Sz;->j:Ljavax/microedition/khronos/egl/EGL10;

    .line 1209436
    iget v0, p2, LX/7Sx;->d:I

    iput v0, p0, LX/7Sz;->c:I

    .line 1209437
    iget v0, p2, LX/7Sx;->e:I

    iput v0, p0, LX/7Sz;->d:I

    .line 1209438
    iput-object p3, p0, LX/7Sz;->e:LX/60w;

    .line 1209439
    invoke-direct {p0}, LX/7Sz;->d()V

    .line 1209440
    invoke-direct {p0}, LX/7Sz;->e()V

    .line 1209441
    new-instance v0, LX/7T2;

    iget-object v1, p0, LX/7Sz;->b:LX/5Pc;

    iget-object p1, p0, LX/7Sz;->e:LX/60w;

    invoke-direct {v0, v1, p2, p1}, LX/7T2;-><init>(LX/5Pc;LX/7Sx;LX/60w;)V

    iput-object v0, p0, LX/7Sz;->f:LX/7T2;

    .line 1209442
    iget-object v0, p0, LX/7Sz;->f:LX/7T2;

    invoke-virtual {v0}, LX/7T2;->b()V

    .line 1209443
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1209444
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "textureID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/7Sz;->f:LX/7T2;

    invoke-virtual {v1}, LX/7T2;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1209445
    :cond_2
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, LX/7Sz;->f:LX/7T2;

    invoke-virtual {v1}, LX/7T2;->a()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, LX/7Sz;->g:Landroid/graphics/SurfaceTexture;

    .line 1209446
    new-instance v0, LX/7T1;

    iget-object v1, p0, LX/7Sz;->g:Landroid/graphics/SurfaceTexture;

    iget-object p1, p0, LX/7Sz;->f:LX/7T2;

    const/16 p3, 0x1388

    invoke-direct {v0, v1, p1, p3}, LX/7T1;-><init>(Landroid/graphics/SurfaceTexture;LX/7T2;I)V

    iput-object v0, p0, LX/7Sz;->h:LX/7T1;

    .line 1209447
    iget-object v0, p0, LX/7Sz;->g:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, LX/7Sz;->h:LX/7T1;

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 1209448
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, LX/7Sz;->g:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, LX/7Sz;->i:Landroid/view/Surface;

    .line 1209449
    iget v0, p0, LX/7Sz;->c:I

    iget v1, p0, LX/7Sz;->d:I

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/7Sz;->n:Ljava/nio/ByteBuffer;

    .line 1209450
    iget-object v0, p0, LX/7Sz;->n:Ljava/nio/ByteBuffer;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1209451
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1209452
    iget-object v0, p0, LX/7Sz;->j:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    const/16 v1, 0x3000

    if-eq v0, v1, :cond_0

    .line 1209453
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": EGL error: 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1209454
    :cond_0
    return-void
.end method

.method private d()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v4, 0x1

    .line 1209455
    iget-object v0, p0, LX/7Sz;->j:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 1209456
    iget-object v0, p0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-ne v0, v1, :cond_0

    .line 1209457
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to get EGL14 display"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209458
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1209459
    iget-object v1, p0, LX/7Sz;->j:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v1, v2, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1209460
    iput-object v8, p0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 1209461
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to initialize EGL14"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209462
    :cond_1
    const/16 v0, 0xd

    new-array v2, v0, [I

    fill-array-data v2, :array_0

    .line 1209463
    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 1209464
    new-array v5, v4, [I

    .line 1209465
    iget-object v0, p0, LX/7Sz;->j:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1209466
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to find RGB888+recordable ES2 EGL config"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209467
    :cond_2
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    .line 1209468
    iget-object v1, p0, LX/7Sz;->j:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    aget-object v5, v3, v7

    sget-object v6, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v1, v2, v5, v6, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    iput-object v0, p0, LX/7Sz;->l:Ljavax/microedition/khronos/egl/EGLContext;

    .line 1209469
    const-string v0, "eglCreateContext"

    invoke-direct {p0, v0}, LX/7Sz;->a(Ljava/lang/String;)V

    .line 1209470
    iget-object v0, p0, LX/7Sz;->l:Ljavax/microedition/khronos/egl/EGLContext;

    if-nez v0, :cond_3

    .line 1209471
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "null context"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209472
    :cond_3
    new-array v0, v4, [I

    .line 1209473
    invoke-static {v4, v0, v7}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 1209474
    new-instance v1, Landroid/graphics/SurfaceTexture;

    aget v0, v0, v7

    invoke-direct {v1, v0}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    .line 1209475
    iget v0, p0, LX/7Sz;->c:I

    iget v2, p0, LX/7Sz;->d:I

    invoke-virtual {v1, v0, v2}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 1209476
    iget-object v0, p0, LX/7Sz;->j:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    aget-object v3, v3, v7

    invoke-interface {v0, v2, v3, v1, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, LX/7Sz;->m:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 1209477
    const-string v0, "eglCreateWindowSurface"

    invoke-direct {p0, v0}, LX/7Sz;->a(Ljava/lang/String;)V

    .line 1209478
    iget-object v0, p0, LX/7Sz;->m:Ljavax/microedition/khronos/egl/EGLSurface;

    if-nez v0, :cond_4

    .line 1209479
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "surface was null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209480
    :cond_4
    return-void

    nop

    :array_0
    .array-data 4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3040
        0x4
        0x3033
        0x4
        0x3038
    .end array-data

    .line 1209481
    :array_1
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method private e()V
    .locals 5

    .prologue
    .line 1209482
    iget-object v0, p0, LX/7Sz;->j:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/7Sz;->k:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, LX/7Sz;->m:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v3, p0, LX/7Sz;->m:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v4, p0, LX/7Sz;->l:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1209483
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglMakeCurrent failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209484
    :cond_0
    return-void
.end method
