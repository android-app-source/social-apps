.class public final LX/7Mi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V
    .locals 0

    .prologue
    .line 1198963
    iput-object p1, p0, LX/7Mi;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1198956
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1198957
    check-cast v0, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;

    .line 1198958
    if-nez v0, :cond_0

    .line 1198959
    sget-object v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->b:Ljava/lang/Class;

    const-string v1, "%s.setCoverImage(): Unexpected null value returned for blurred image"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1198960
    :goto_0
    return-void

    .line 1198961
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1198962
    iget-object v1, p0, LX/7Mi;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->o:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1198953
    sget-object v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->b:Ljava/lang/Class;

    const-string v1, "%s.setCoverImage(): Blurred image failure: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1198954
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1198955
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/7Mi;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
