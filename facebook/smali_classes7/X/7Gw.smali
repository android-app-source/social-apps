.class public LX/7Gw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1190092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.text.util.Linkify.addLinks"
        }
    .end annotation

    .prologue
    .line 1190066
    :try_start_0
    invoke-static {p0, p1, p2, p3, p4}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1190067
    :goto_0
    return-void

    .line 1190068
    :catch_0
    move-exception v0

    .line 1190069
    const-string v1, "SafeLinkifier"

    const-string v2, "unable to linkify: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/text/Spannable;I)Z
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-com.facebook.text.FbLinkify.addLinks"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1190088
    :try_start_0
    invoke-static {p0, p1}, LX/7Gu;->a(Landroid/text/Spannable;I)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1190089
    :goto_0
    return v0

    .line 1190090
    :catch_0
    move-exception v1

    .line 1190091
    const-string v2, "SafeLinkifier"

    const-string v3, "unable to linkify: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v2, v1, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/widget/TextView;I)Z
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-com.facebook.text.FbLinkify.addLinks"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1190070
    :try_start_0
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1190071
    if-nez p1, :cond_0

    move v1, v2

    .line 1190072
    :goto_0
    move v0, v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1190073
    :goto_1
    return v0

    .line 1190074
    :catch_0
    move-exception v1

    .line 1190075
    const-string v2, "SafeLinkifier"

    const-string v3, "unable to linkify: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v2, v1, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1190076
    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 1190077
    instance-of v4, v1, Landroid/text/Spannable;

    if-eqz v4, :cond_2

    .line 1190078
    check-cast v1, Landroid/text/Spannable;

    invoke-static {v1, p1}, LX/7Gu;->a(Landroid/text/Spannable;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1190079
    invoke-static {p0}, LX/7Gu;->a(Landroid/widget/TextView;)V

    move v1, v3

    .line 1190080
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1190081
    goto :goto_0

    .line 1190082
    :cond_2
    invoke-static {v1}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    .line 1190083
    invoke-static {v1, p1}, LX/7Gu;->a(Landroid/text/Spannable;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1190084
    invoke-static {p0}, LX/7Gu;->a(Landroid/widget/TextView;)V

    .line 1190085
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v1, v3

    .line 1190086
    goto :goto_0

    :cond_3
    move v1, v2

    .line 1190087
    goto :goto_0
.end method
