.class public final LX/71d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/selector/model/OptionSelectorRow;

.field public final synthetic b:LX/71f;


# direct methods
.method public constructor <init>(LX/71f;Lcom/facebook/payments/selector/model/OptionSelectorRow;)V
    .locals 0

    .prologue
    .line 1163527
    iput-object p1, p0, LX/71d;->b:LX/71f;

    iput-object p2, p0, LX/71d;->a:Lcom/facebook/payments/selector/model/OptionSelectorRow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x2f009964

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1163516
    iget-object v2, p0, LX/71d;->b:LX/71f;

    iget-object v2, v2, LX/71f;->b:LX/71a;

    iget-object v3, p0, LX/71d;->a:Lcom/facebook/payments/selector/model/OptionSelectorRow;

    iget-object v4, p0, LX/71d;->a:Lcom/facebook/payments/selector/model/OptionSelectorRow;

    iget-object v5, p0, LX/71d;->a:Lcom/facebook/payments/selector/model/OptionSelectorRow;

    iget-boolean v5, v5, Lcom/facebook/payments/selector/model/OptionSelectorRow;->e:Z

    if-nez v5, :cond_2

    .line 1163517
    :goto_0
    new-instance v6, Lcom/facebook/payments/selector/model/OptionSelectorRow;

    iget-object v7, v4, Lcom/facebook/payments/selector/model/OptionSelectorRow;->a:Ljava/lang/String;

    iget-object v8, v4, Lcom/facebook/payments/selector/model/OptionSelectorRow;->b:Ljava/lang/String;

    iget-object v9, v4, Lcom/facebook/payments/selector/model/OptionSelectorRow;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-boolean v10, v4, Lcom/facebook/payments/selector/model/OptionSelectorRow;->d:Z

    move v11, v0

    invoke-direct/range {v6 .. v11}, Lcom/facebook/payments/selector/model/OptionSelectorRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;ZZ)V

    move-object v0, v6

    .line 1163518
    iget-object v4, v2, LX/71a;->a:Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;

    .line 1163519
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1163520
    iget-object v5, v4, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v8, v5, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->b:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v5, 0x0

    move v6, v5

    :goto_1
    if-ge v6, v9, :cond_1

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/payments/selector/model/SelectorRow;

    .line 1163521
    invoke-virtual {v5, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v5, v0

    :cond_0
    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1163522
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 1163523
    :cond_1
    iget-object v5, v4, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->a(LX/0Px;)Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    move-result-object v5

    iput-object v5, v4, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    .line 1163524
    invoke-static {v4}, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->h(Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;)V

    .line 1163525
    const v0, 0x5fdc8448

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1163526
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
