.class public final enum LX/7IC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7IC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7IC;

.field public static final enum BUFFERING:LX/7IC;

.field public static final enum ERROR:LX/7IC;

.field public static final enum PAUSING:LX/7IC;

.field public static final enum PLAYING:LX/7IC;

.field public static final enum PREPARING:LX/7IC;

.field public static final enum READY:LX/7IC;

.field public static final enum SEEKING:LX/7IC;

.field public static final enum UNPREPARED:LX/7IC;

.field public static final enum UNPREPARING:LX/7IC;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1191518
    new-instance v0, LX/7IC;

    const-string v1, "UNPREPARED"

    invoke-direct {v0, v1, v3}, LX/7IC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IC;->UNPREPARED:LX/7IC;

    .line 1191519
    new-instance v0, LX/7IC;

    const-string v1, "PREPARING"

    invoke-direct {v0, v1, v4}, LX/7IC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IC;->PREPARING:LX/7IC;

    .line 1191520
    new-instance v0, LX/7IC;

    const-string v1, "UNPREPARING"

    invoke-direct {v0, v1, v5}, LX/7IC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IC;->UNPREPARING:LX/7IC;

    .line 1191521
    new-instance v0, LX/7IC;

    const-string v1, "READY"

    invoke-direct {v0, v1, v6}, LX/7IC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IC;->READY:LX/7IC;

    .line 1191522
    new-instance v0, LX/7IC;

    const-string v1, "SEEKING"

    invoke-direct {v0, v1, v7}, LX/7IC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IC;->SEEKING:LX/7IC;

    .line 1191523
    new-instance v0, LX/7IC;

    const-string v1, "PLAYING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7IC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IC;->PLAYING:LX/7IC;

    .line 1191524
    new-instance v0, LX/7IC;

    const-string v1, "BUFFERING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/7IC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IC;->BUFFERING:LX/7IC;

    .line 1191525
    new-instance v0, LX/7IC;

    const-string v1, "PAUSING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/7IC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IC;->PAUSING:LX/7IC;

    .line 1191526
    new-instance v0, LX/7IC;

    const-string v1, "ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/7IC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7IC;->ERROR:LX/7IC;

    .line 1191527
    const/16 v0, 0x9

    new-array v0, v0, [LX/7IC;

    sget-object v1, LX/7IC;->UNPREPARED:LX/7IC;

    aput-object v1, v0, v3

    sget-object v1, LX/7IC;->PREPARING:LX/7IC;

    aput-object v1, v0, v4

    sget-object v1, LX/7IC;->UNPREPARING:LX/7IC;

    aput-object v1, v0, v5

    sget-object v1, LX/7IC;->READY:LX/7IC;

    aput-object v1, v0, v6

    sget-object v1, LX/7IC;->SEEKING:LX/7IC;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7IC;->PLAYING:LX/7IC;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7IC;->BUFFERING:LX/7IC;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7IC;->PAUSING:LX/7IC;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7IC;->ERROR:LX/7IC;

    aput-object v2, v0, v1

    sput-object v0, LX/7IC;->$VALUES:[LX/7IC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1191528
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7IC;
    .locals 1

    .prologue
    .line 1191517
    const-class v0, LX/7IC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7IC;

    return-object v0
.end method

.method public static values()[LX/7IC;
    .locals 1

    .prologue
    .line 1191516
    sget-object v0, LX/7IC;->$VALUES:[LX/7IC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7IC;

    return-object v0
.end method
