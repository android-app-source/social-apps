.class public final LX/7so;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1265879
    const/4 v12, 0x0

    .line 1265880
    const/4 v11, 0x0

    .line 1265881
    const/4 v10, 0x0

    .line 1265882
    const/4 v9, 0x0

    .line 1265883
    const/4 v8, 0x0

    .line 1265884
    const/4 v7, 0x0

    .line 1265885
    const/4 v6, 0x0

    .line 1265886
    const/4 v5, 0x0

    .line 1265887
    const/4 v4, 0x0

    .line 1265888
    const/4 v3, 0x0

    .line 1265889
    const/4 v2, 0x0

    .line 1265890
    const/4 v1, 0x0

    .line 1265891
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1265892
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1265893
    const/4 v1, 0x0

    .line 1265894
    :goto_0
    return v1

    .line 1265895
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1265896
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_a

    .line 1265897
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1265898
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1265899
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1265900
    const-string v14, "event_buy_ticket_display_url"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1265901
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 1265902
    :cond_2
    const-string v14, "event_ticket_provider"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1265903
    invoke-static/range {p0 .. p1}, LX/7sf;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1265904
    :cond_3
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1265905
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1265906
    :cond_4
    const-string v14, "is_official"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1265907
    const/4 v3, 0x1

    .line 1265908
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 1265909
    :cond_5
    const-string v14, "supports_multi_tier_purchase"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1265910
    const/4 v2, 0x1

    .line 1265911
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 1265912
    :cond_6
    const-string v14, "ticket_seat_selection_note"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1265913
    invoke-static/range {p0 .. p1}, LX/7sg;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1265914
    :cond_7
    const-string v14, "ticket_settings"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1265915
    invoke-static/range {p0 .. p1}, LX/7sj;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1265916
    :cond_8
    const-string v14, "ticket_tiers"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1265917
    invoke-static/range {p0 .. p1}, LX/7sn;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1265918
    :cond_9
    const-string v14, "total_purchased_tickets"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1265919
    const/4 v1, 0x1

    .line 1265920
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v4

    goto/16 :goto_1

    .line 1265921
    :cond_a
    const/16 v13, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1265922
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1265923
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1265924
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1265925
    if-eqz v3, :cond_b

    .line 1265926
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 1265927
    :cond_b
    if-eqz v2, :cond_c

    .line 1265928
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 1265929
    :cond_c
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1265930
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1265931
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1265932
    if-eqz v1, :cond_d

    .line 1265933
    const/16 v1, 0x8

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4, v2}, LX/186;->a(III)V

    .line 1265934
    :cond_d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
