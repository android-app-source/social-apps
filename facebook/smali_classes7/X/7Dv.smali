.class public LX/7Dv;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1184068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184069
    return-void
.end method

.method public static a(LX/7Dw;)Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;
    .locals 10

    .prologue
    const/high16 v9, 0x43b40000    # 360.0f

    const/high16 v8, 0x43340000    # 180.0f

    const/high16 v7, 0x40000000    # 2.0f

    .line 1184070
    invoke-interface {p0}, LX/7Dw;->e()LX/7Dy;

    move-result-object v0

    .line 1184071
    invoke-interface {v0}, LX/7Dy;->a()I

    move-result v1

    int-to-float v1, v1

    .line 1184072
    invoke-interface {v0}, LX/7Dy;->b()I

    move-result v2

    int-to-float v2, v2

    .line 1184073
    invoke-interface {v0}, LX/7Dy;->c()I

    move-result v3

    int-to-float v3, v3

    .line 1184074
    invoke-interface {v0}, LX/7Dy;->d()I

    move-result v4

    int-to-float v4, v4

    .line 1184075
    invoke-interface {v0}, LX/7Dy;->e()I

    move-result v5

    int-to-float v5, v5

    .line 1184076
    invoke-interface {v0}, LX/7Dy;->f()I

    move-result v0

    int-to-float v0, v0

    .line 1184077
    div-float/2addr v3, v1

    mul-float/2addr v3, v9

    .line 1184078
    div-float v6, v1, v7

    sub-float v5, v6, v5

    mul-float/2addr v5, v9

    div-float v1, v5, v1

    .line 1184079
    sub-float/2addr v3, v1

    .line 1184080
    div-float/2addr v4, v2

    mul-float/2addr v4, v8

    .line 1184081
    div-float v5, v2, v7

    sub-float v0, v5, v0

    mul-float/2addr v0, v8

    div-float/2addr v0, v2

    .line 1184082
    sub-float v2, v4, v0

    .line 1184083
    new-instance v4, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    invoke-direct {v4, v1, v3, v0, v2}, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;-><init>(FFFF)V

    return-object v4
.end method
