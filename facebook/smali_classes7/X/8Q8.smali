.class public final LX/8Q8;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "LX/8QW;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V
    .locals 0

    .prologue
    .line 1342513
    iput-object p1, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1342514
    iget-object v0, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->r:Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    .line 1342515
    iget-object v0, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v1, v1, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->r:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 1342516
    :cond_0
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1342517
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 1342518
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 1342519
    iget-object v0, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->c:LX/03V;

    const-string v1, "edit_story_privacy_fragment_option_fetch_error"

    const-string v2, "Privacy options fetch failure"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1342520
    :cond_0
    iget-object v0, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->v:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1342521
    invoke-direct {p0}, LX/8Q8;->a()V

    .line 1342522
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1342523
    check-cast p1, LX/8QW;

    .line 1342524
    iget-object v0, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1342525
    iget-object v0, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    .line 1342526
    iput-object p1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->m:LX/8QW;

    .line 1342527
    iget-object v0, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->m:LX/8QW;

    if-nez v0, :cond_0

    .line 1342528
    iget-object v0, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->c:LX/03V;

    const-string v1, "edit_story_privacy_fragment_no_selectable_privacy"

    const-string v2, "fetch successful but get a null SelectablePrivacyData"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1342529
    iget-object v0, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->v:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1342530
    :goto_0
    return-void

    .line 1342531
    :cond_0
    iget-object v0, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-nez v0, :cond_2

    .line 1342532
    iget-object v0, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v1, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v1, v1, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->m:LX/8QW;

    .line 1342533
    iget-object v2, v1, LX/8QW;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v1, v2

    .line 1342534
    iput-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->o:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1342535
    iget-object v0, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v1, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v1, v1, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->o:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1342536
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1342537
    iput-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1342538
    :goto_1
    iget-object v0, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    .line 1342539
    invoke-static {v0}, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->l(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1342540
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    if-nez v1, :cond_1

    .line 1342541
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const v2, 0x7f0d0d2f

    invoke-virtual {v1, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iput-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    .line 1342542
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    if-nez v1, :cond_1

    .line 1342543
    new-instance v1, Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-direct {v1}, Lcom/facebook/privacy/selector/AudiencePickerFragment;-><init>()V

    iput-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    .line 1342544
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1342545
    const v2, 0x7f0d0d2f

    iget-object p1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-virtual {v1, v2, p1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1342546
    invoke-virtual {v1}, LX/0hH;->c()I

    .line 1342547
    :cond_1
    new-instance v1, LX/8QF;

    invoke-direct {v1}, LX/8QF;-><init>()V

    iget-object v2, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->x:LX/8Q5;

    invoke-interface {v2}, LX/8Q5;->a()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v2

    .line 1342548
    iput-object v2, v1, LX/8QF;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1342549
    move-object v1, v1

    .line 1342550
    iget-object v2, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->x:LX/8Q5;

    invoke-interface {v2}, LX/8Q5;->b()Z

    move-result v2

    .line 1342551
    iput-boolean v2, v1, LX/8QF;->b:Z

    .line 1342552
    move-object v1, v1

    .line 1342553
    invoke-virtual {v1}, LX/8QF;->a()Lcom/facebook/privacy/model/AudiencePickerInput;

    move-result-object v1

    .line 1342554
    iget-object v2, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-virtual {v2, v1}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a(Lcom/facebook/privacy/model/AudiencePickerInput;)V

    .line 1342555
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->w:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1342556
    :goto_2
    invoke-direct {p0}, LX/8Q8;->a()V

    goto :goto_0

    .line 1342557
    :cond_2
    iget-object v0, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    new-instance v1, LX/8QV;

    iget-object v2, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v2, v2, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->m:LX/8QW;

    .line 1342558
    iget-object p1, v2, LX/8QW;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v2, p1

    .line 1342559
    invoke-direct {v1, v2}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    iget-object v2, p0, LX/8Q8;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v2, v2, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v1, v2}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v1

    invoke-virtual {v1}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    .line 1342560
    iput-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->o:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1342561
    goto :goto_1

    .line 1342562
    :cond_3
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->q:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    if-nez v1, :cond_4

    .line 1342563
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const v2, 0x7f0d0d2f

    invoke-virtual {v1, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iput-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->q:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    .line 1342564
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->q:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    if-nez v1, :cond_4

    .line 1342565
    new-instance v1, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-direct {v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;-><init>()V

    iput-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->q:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    .line 1342566
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1342567
    const v2, 0x7f0d0d2f

    iget-object p1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->q:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-virtual {v1, v2, p1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1342568
    invoke-virtual {v1}, LX/0hH;->c()I

    .line 1342569
    :cond_4
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->q:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v2, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->x:LX/8Q5;

    invoke-virtual {v1, v2}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(LX/8Q5;)V

    .line 1342570
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->w:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1342571
    goto :goto_2
.end method
