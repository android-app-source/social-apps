.class public final LX/845;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/0TF;

.field public final synthetic c:LX/2dl;


# direct methods
.method public constructor <init>(LX/2dl;Ljava/lang/String;LX/0TF;)V
    .locals 0

    .prologue
    .line 1290785
    iput-object p1, p0, LX/845;->c:LX/2dl;

    iput-object p2, p0, LX/845;->a:Ljava/lang/String;

    iput-object p3, p0, LX/845;->b:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1290786
    iget-object v0, p0, LX/845;->b:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1290787
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1290788
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1290789
    iget-object v0, p0, LX/845;->c:LX/2dl;

    iget-object v0, v0, LX/2dl;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2io;

    iget-object v1, p0, LX/845;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/2io;->a(Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1290790
    iget-object v0, p0, LX/845;->b:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1290791
    return-void
.end method
