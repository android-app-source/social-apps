.class public final LX/8PL;
.super LX/4hh;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:LX/0m9;

.field public f:Z

.field public final g:LX/0lC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1341554
    const-class v0, LX/8PL;

    sput-object v0, LX/8PL;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0lC;)V
    .locals 0

    .prologue
    .line 1341555
    invoke-direct {p0}, LX/4hh;-><init>()V

    .line 1341556
    iput-object p1, p0, LX/8PL;->g:LX/0lC;

    .line 1341557
    return-void
.end method

.method private i()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341558
    new-instance v0, LX/8PG;

    invoke-direct {v0, p0}, LX/8PG;-><init>(LX/8PL;)V

    return-object v0
.end method

.method private j()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341559
    new-instance v0, LX/8PH;

    invoke-direct {v0, p0}, LX/8PH;-><init>(LX/8PL;)V

    return-object v0
.end method

.method private l()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341560
    new-instance v0, LX/8PJ;

    invoke-direct {v0, p0}, LX/8PJ;-><init>(LX/8PL;)V

    return-object v0
.end method

.method private m()LX/4hg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4hg",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1341561
    new-instance v0, LX/8PK;

    invoke-direct {v0, p0}, LX/8PK;-><init>(LX/8PL;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 1341562
    const-string v2, "com.facebook.platform.extra.ACTION_TYPE"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PL;->i()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "com.facebook.platform.extra.PREVIEW_PROPERTY_NAME"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PL;->j()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "com.facebook.platform.extra.ACTION"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PL;->l()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v6, "com.facebook.platform.extra.DATA_FAILURES_FATAL"

    const-class v8, Ljava/lang/Boolean;

    invoke-direct {p0}, LX/8PL;->m()LX/4hg;

    move-result-object v9

    move-object v4, p0

    move-object v5, p1

    invoke-virtual/range {v4 .. v9}, LX/4hh;->b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v7

    .line 1341563
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8PL;->e:LX/0m9;

    if-eqz v0, :cond_1

    :goto_1
    return v7

    :cond_0
    move v0, v3

    .line 1341564
    goto :goto_0

    :cond_1
    move v7, v3

    .line 1341565
    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;)Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 1341566
    const-string v2, "action_type"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PL;->i()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "preview_property_name"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PL;->j()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "action"

    const-class v4, Ljava/lang/String;

    invoke-direct {p0}, LX/8PL;->l()LX/4hg;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v6, "HASHTAG"

    const-class v8, Ljava/lang/String;

    .line 1341567
    new-instance v0, LX/8PI;

    invoke-direct {v0, p0}, LX/8PI;-><init>(LX/8PL;)V

    move-object v9, v0

    .line 1341568
    move-object v4, p0

    move-object v5, p1

    invoke-virtual/range {v4 .. v9}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v6, "DATA_FAILURES_FATAL"

    const-class v8, Ljava/lang/Boolean;

    invoke-direct {p0}, LX/8PL;->m()LX/4hg;

    move-result-object v9

    move-object v4, p0

    move-object v5, p1

    invoke-virtual/range {v4 .. v9}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v7

    .line 1341569
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8PL;->e:LX/0m9;

    if-eqz v0, :cond_1

    :goto_1
    return v7

    :cond_0
    move v0, v3

    .line 1341570
    goto :goto_0

    :cond_1
    move v7, v3

    .line 1341571
    goto :goto_1
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1341572
    iget-boolean v0, p0, LX/8PL;->f:Z

    return v0
.end method
