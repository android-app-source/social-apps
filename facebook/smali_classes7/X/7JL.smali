.class public final LX/7JL;
.super LX/38i;
.source ""


# instance fields
.field public final synthetic a:LX/37Y;


# direct methods
.method public constructor <init>(LX/37Y;)V
    .locals 0

    .prologue
    .line 1193455
    iput-object p1, p0, LX/7JL;->a:LX/37Y;

    invoke-direct {p0}, LX/38i;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1193453
    iget-object v0, p0, LX/7JL;->a:LX/37Y;

    invoke-static {v0}, LX/37Y;->F(LX/37Y;)V

    .line 1193454
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1193448
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1193449
    if-nez v0, :cond_0

    .line 1193450
    iget-object v0, p0, LX/7JL;->a:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->r()V

    .line 1193451
    :cond_0
    iget-object v0, p0, LX/7JL;->a:LX/37Y;

    invoke-static {v0}, LX/37Y;->F(LX/37Y;)V

    .line 1193452
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1193436
    iget-object v0, p0, LX/7JL;->a:LX/37Y;

    .line 1193437
    iget-object v1, v0, LX/37Y;->s:LX/7JN;

    if-eqz v1, :cond_0

    .line 1193438
    const/4 v2, 0x0

    .line 1193439
    iget-object v1, v0, LX/37Y;->s:LX/7JN;

    iput-object v1, v0, LX/37Y;->r:LX/7JN;

    .line 1193440
    iget-object v1, v0, LX/37Y;->v:Lcom/facebook/video/engine/VideoPlayerParams;

    iput-object v1, v0, LX/37Y;->u:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1193441
    iput-object v2, v0, LX/37Y;->s:LX/7JN;

    .line 1193442
    iput-object v2, v0, LX/37Y;->v:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1193443
    iget-object v1, v0, LX/37Y;->c:LX/37f;

    iget-object v2, v0, LX/37Y;->k:LX/37Z;

    invoke-virtual {v2}, LX/37Z;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/37f;->a(Ljava/lang/String;)V

    .line 1193444
    :cond_0
    iget-object v1, v0, LX/37Y;->r:LX/7JN;

    invoke-static {v0, v1}, LX/37Y;->a(LX/37Y;LX/7JN;)V

    .line 1193445
    iget-object v1, v0, LX/37Y;->f:LX/38o;

    iget-object v2, v0, LX/37Y;->r:LX/7JN;

    invoke-virtual {v0}, LX/37Y;->n()I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/38o;->a(LX/7JN;I)V

    .line 1193446
    iget-object v0, p0, LX/7JL;->a:LX/37Y;

    invoke-static {v0}, LX/37Y;->F(LX/37Y;)V

    .line 1193447
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 1193456
    iget-object v0, p0, LX/7JL;->a:LX/37Y;

    .line 1193457
    invoke-static {v0}, LX/37Y;->B(LX/37Y;)V

    .line 1193458
    iget-object v1, v0, LX/37Y;->s:LX/7JN;

    if-eqz v1, :cond_0

    .line 1193459
    iget-object v1, v0, LX/37Y;->s:LX/7JN;

    .line 1193460
    iget v2, v1, LX/7JN;->q:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, LX/7JN;->q:I

    .line 1193461
    iget v2, v1, LX/7JN;->q:I

    iget-object v3, v1, LX/7JN;->n:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 1193462
    const/4 v2, 0x1

    .line 1193463
    :goto_0
    move v1, v2

    .line 1193464
    if-eqz v1, :cond_1

    .line 1193465
    invoke-virtual {v0}, LX/37Y;->r()V

    .line 1193466
    :cond_0
    :goto_1
    iget-object v0, p0, LX/7JL;->a:LX/37Y;

    invoke-static {v0}, LX/37Y;->F(LX/37Y;)V

    .line 1193467
    return-void

    .line 1193468
    :cond_1
    const/4 v1, 0x0

    .line 1193469
    iput-object v1, v0, LX/37Y;->s:LX/7JN;

    .line 1193470
    iput-object v1, v0, LX/37Y;->v:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1193471
    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1193434
    iget-object v0, p0, LX/7JL;->a:LX/37Y;

    invoke-static {v0}, LX/37Y;->F(LX/37Y;)V

    .line 1193435
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1193430
    iget-object v0, p0, LX/7JL;->a:LX/37Y;

    .line 1193431
    iget-object v1, v0, LX/37Y;->i:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Iw;

    .line 1193432
    invoke-interface {v1}, LX/7Iw;->dL_()V

    goto :goto_0

    .line 1193433
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1193428
    iget-object v0, p0, LX/7JL;->a:LX/37Y;

    invoke-static {v0}, LX/37Y;->F(LX/37Y;)V

    .line 1193429
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1193426
    iget-object v0, p0, LX/7JL;->a:LX/37Y;

    invoke-static {v0}, LX/37Y;->F(LX/37Y;)V

    .line 1193427
    return-void
.end method
