.class public LX/8HC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/data/method/EditPhotoLocationParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320617
    const-class v0, LX/8HC;

    sput-object v0, LX/8HC;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320615
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320616
    return-void
.end method

.method public static a(LX/0QB;)LX/8HC;
    .locals 1

    .prologue
    .line 1320621
    new-instance v0, LX/8HC;

    invoke-direct {v0}, LX/8HC;-><init>()V

    .line 1320622
    move-object v0, v0

    .line 1320623
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1320624
    check-cast p1, Lcom/facebook/photos/data/method/EditPhotoLocationParams;

    .line 1320625
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1320626
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_explicit_location"

    const-string v3, "true"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320627
    iget-object v1, p1, Lcom/facebook/photos/data/method/EditPhotoLocationParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1320628
    const-string v2, "-1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1320629
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "place"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320630
    :cond_0
    iget-object v1, p1, Lcom/facebook/photos/data/method/EditPhotoLocationParams;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1320631
    if-eqz v1, :cond_1

    .line 1320632
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "text_only_place"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320633
    :cond_1
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    sget-object v2, LX/8HC;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1320634
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1320635
    move-object v1, v1

    .line 1320636
    const-string v2, "POST"

    .line 1320637
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1320638
    move-object v1, v1

    .line 1320639
    iget-object v2, p1, Lcom/facebook/photos/data/method/EditPhotoLocationParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1320640
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1320641
    move-object v1, v1

    .line 1320642
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1320643
    move-object v0, v1

    .line 1320644
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1320645
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1320646
    move-object v0, v0

    .line 1320647
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1320618
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1320619
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1320620
    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
