.class public LX/6lY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6lX;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1142748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142749
    iput-object p1, p0, LX/6lY;->a:Landroid/content/Context;

    .line 1142750
    return-void
.end method


# virtual methods
.method public final a(LX/6lg;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1142751
    invoke-virtual {p1}, LX/6lg;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1142752
    iget-object v0, p0, LX/6lY;->a:Landroid/content/Context;

    const v1, 0x7f0819f0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1142753
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6lY;->a:Landroid/content/Context;

    const v1, 0x7f0819f1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, LX/6lg;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
