.class public LX/6lK;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private final a:LX/6lM;

.field public b:I


# direct methods
.method public constructor <init>(LX/6lM;)V
    .locals 0

    .prologue
    .line 1142621
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1142622
    iput-object p1, p0, LX/6lK;->a:LX/6lM;

    .line 1142623
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    .line 1142624
    invoke-virtual {p0}, LX/6lK;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1142625
    iget-object v1, p0, LX/6lK;->a:LX/6lM;

    iget v2, p0, LX/6lK;->b:I

    iget v3, v0, Landroid/graphics/Rect;->left:I

    iget v0, v0, Landroid/graphics/Rect;->top:I

    const/4 p0, 0x1

    const/4 v11, 0x0

    .line 1142626
    iget v4, v1, LX/6lM;->d:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    .line 1142627
    int-to-float v5, v3

    add-float/2addr v5, v4

    .line 1142628
    int-to-float v6, v0

    add-float/2addr v6, v4

    .line 1142629
    iget-object v7, v1, LX/6lM;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v4, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1142630
    iget v4, v1, LX/6lM;->e:I

    if-gtz v4, :cond_0

    .line 1142631
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1142632
    iget-object v7, v1, LX/6lM;->a:Landroid/content/res/Resources;

    const v8, 0x7f080510

    new-array v9, p0, [Ljava/lang/Object;

    const v10, 0x75bcd15

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 1142633
    iget-object v8, v1, LX/6lM;->c:Landroid/text/TextPaint;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v8, v7, v11, v9, v4}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1142634
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    iput v4, v1, LX/6lM;->e:I

    .line 1142635
    :cond_0
    iget-object v4, v1, LX/6lM;->a:Landroid/content/res/Resources;

    const v7, 0x7f080510

    new-array v8, p0, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {v4, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1142636
    iget v7, v1, LX/6lM;->e:I

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    add-float/2addr v6, v7

    iget-object v7, v1, LX/6lM;->c:Landroid/text/TextPaint;

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1142637
    return-void
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 1142638
    iget-object v0, p0, LX/6lK;->a:LX/6lM;

    .line 1142639
    iget p0, v0, LX/6lM;->d:I

    move v0, p0

    .line 1142640
    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 1142641
    iget-object v0, p0, LX/6lK;->a:LX/6lM;

    .line 1142642
    iget p0, v0, LX/6lM;->d:I

    move v0, p0

    .line 1142643
    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1142644
    iget-object v0, p0, LX/6lK;->a:LX/6lM;

    .line 1142645
    iget-object p0, v0, LX/6lM;->c:Landroid/text/TextPaint;

    invoke-virtual {p0}, Landroid/text/TextPaint;->getAlpha()I

    move-result p0

    sparse-switch p0, :sswitch_data_0

    .line 1142646
    const/4 p0, -0x3

    :goto_0
    move v0, p0

    .line 1142647
    return v0

    .line 1142648
    :sswitch_0
    const/4 p0, -0x1

    goto :goto_0

    .line 1142649
    :sswitch_1
    const/4 p0, -0x2

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0xff -> :sswitch_0
    .end sparse-switch
.end method

.method public final setAlpha(I)V
    .locals 2

    .prologue
    .line 1142650
    iget-object v0, p0, LX/6lK;->a:LX/6lM;

    .line 1142651
    iget-object v1, v0, LX/6lM;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1142652
    iget-object v1, v0, LX/6lM;->c:Landroid/text/TextPaint;

    invoke-virtual {v1, p1}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 1142653
    invoke-virtual {p0}, LX/6lK;->invalidateSelf()V

    .line 1142654
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 2

    .prologue
    .line 1142655
    iget-object v0, p0, LX/6lK;->a:LX/6lM;

    .line 1142656
    iget-object v1, v0, LX/6lM;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1142657
    iget-object v1, v0, LX/6lM;->c:Landroid/text/TextPaint;

    invoke-virtual {v1, p1}, Landroid/text/TextPaint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1142658
    invoke-virtual {p0}, LX/6lK;->invalidateSelf()V

    .line 1142659
    return-void
.end method
