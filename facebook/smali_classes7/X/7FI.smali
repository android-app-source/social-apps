.class public final enum LX/7FI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7FI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7FI;

.field public static final enum ACTION:LX/7FI;

.field public static final enum CANCEL_FREEFORM:LX/7FI;

.field public static final enum CLICK_CLOSE_BUTTON:LX/7FI;

.field public static final enum CLICK_CROSS_OUT:LX/7FI;

.field public static final enum DISMISS_SURVEY:LX/7FI;

.field public static final enum DISMISS_TOAST:LX/7FI;


# instance fields
.field private final mImpressionExtra:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1186091
    new-instance v0, LX/7FI;

    const-string v1, "ACTION"

    const-string v2, "action"

    invoke-direct {v0, v1, v4, v2}, LX/7FI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7FI;->ACTION:LX/7FI;

    .line 1186092
    new-instance v0, LX/7FI;

    const-string v1, "DISMISS_SURVEY"

    const-string v2, "dismiss_survey"

    invoke-direct {v0, v1, v5, v2}, LX/7FI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7FI;->DISMISS_SURVEY:LX/7FI;

    .line 1186093
    new-instance v0, LX/7FI;

    const-string v1, "DISMISS_TOAST"

    const-string v2, "dismiss_toast"

    invoke-direct {v0, v1, v6, v2}, LX/7FI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7FI;->DISMISS_TOAST:LX/7FI;

    .line 1186094
    new-instance v0, LX/7FI;

    const-string v1, "CLICK_CROSS_OUT"

    const-string v2, "click_cross_out"

    invoke-direct {v0, v1, v7, v2}, LX/7FI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7FI;->CLICK_CROSS_OUT:LX/7FI;

    .line 1186095
    new-instance v0, LX/7FI;

    const-string v1, "CLICK_CLOSE_BUTTON"

    const-string v2, "click_close_button"

    invoke-direct {v0, v1, v8, v2}, LX/7FI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7FI;->CLICK_CLOSE_BUTTON:LX/7FI;

    .line 1186096
    new-instance v0, LX/7FI;

    const-string v1, "CANCEL_FREEFORM"

    const/4 v2, 0x5

    const-string v3, "cancel_freeform"

    invoke-direct {v0, v1, v2, v3}, LX/7FI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7FI;->CANCEL_FREEFORM:LX/7FI;

    .line 1186097
    const/4 v0, 0x6

    new-array v0, v0, [LX/7FI;

    sget-object v1, LX/7FI;->ACTION:LX/7FI;

    aput-object v1, v0, v4

    sget-object v1, LX/7FI;->DISMISS_SURVEY:LX/7FI;

    aput-object v1, v0, v5

    sget-object v1, LX/7FI;->DISMISS_TOAST:LX/7FI;

    aput-object v1, v0, v6

    sget-object v1, LX/7FI;->CLICK_CROSS_OUT:LX/7FI;

    aput-object v1, v0, v7

    sget-object v1, LX/7FI;->CLICK_CLOSE_BUTTON:LX/7FI;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7FI;->CANCEL_FREEFORM:LX/7FI;

    aput-object v2, v0, v1

    sput-object v0, LX/7FI;->$VALUES:[LX/7FI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1186098
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1186099
    iput-object p3, p0, LX/7FI;->mImpressionExtra:Ljava/lang/String;

    .line 1186100
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7FI;
    .locals 1

    .prologue
    .line 1186101
    const-class v0, LX/7FI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7FI;

    return-object v0
.end method

.method public static values()[LX/7FI;
    .locals 1

    .prologue
    .line 1186102
    sget-object v0, LX/7FI;->$VALUES:[LX/7FI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7FI;

    return-object v0
.end method


# virtual methods
.method public final getImpressionExtra()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1186103
    iget-object v0, p0, LX/7FI;->mImpressionExtra:Ljava/lang/String;

    return-object v0
.end method
