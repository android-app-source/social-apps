.class public LX/7vF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/events/model/Event;

.field public final b:Lcom/facebook/events/model/Event;

.field private final c:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;


# direct methods
.method private constructor <init>(Lcom/facebook/events/model/Event;Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)V
    .locals 2

    .prologue
    .line 1274327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274328
    iget-object v0, p1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1274329
    iget-object v1, p2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1274330
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1274331
    iput-object p1, p0, LX/7vF;->a:Lcom/facebook/events/model/Event;

    .line 1274332
    iput-object p2, p0, LX/7vF;->b:Lcom/facebook/events/model/Event;

    .line 1274333
    iput-object p3, p0, LX/7vF;->c:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1274334
    return-void
.end method

.method public static a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)LX/7vF;
    .locals 3

    .prologue
    .line 1274335
    new-instance v0, LX/7vC;

    invoke-direct {v0, p0}, LX/7vC;-><init>(Lcom/facebook/events/model/Event;)V

    .line 1274336
    iput-object p1, v0, LX/7vC;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1274337
    move-object v0, v0

    .line 1274338
    const/4 v1, 0x0

    .line 1274339
    iput-boolean v1, v0, LX/7vC;->H:Z

    .line 1274340
    move-object v0, v0

    .line 1274341
    invoke-virtual {v0}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 1274342
    new-instance v1, LX/7vF;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->RSVP:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-direct {v1, p0, v0, v2}, LX/7vF;-><init>(Lcom/facebook/events/model/Event;Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)V

    return-object v1
.end method

.method public static a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/7vF;
    .locals 3

    .prologue
    .line 1274249
    new-instance v0, LX/7vC;

    invoke-direct {v0, p0}, LX/7vC;-><init>(Lcom/facebook/events/model/Event;)V

    .line 1274250
    iput-object p1, v0, LX/7vC;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1274251
    move-object v0, v0

    .line 1274252
    invoke-static {p1}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    .line 1274253
    iput-object v1, v0, LX/7vC;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1274254
    move-object v0, v0

    .line 1274255
    const/4 v1, 0x0

    .line 1274256
    iput-boolean v1, v0, LX/7vC;->H:Z

    .line 1274257
    move-object v0, v0

    .line 1274258
    invoke-virtual {v0}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 1274259
    new-instance v1, LX/7vF;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-direct {v1, p0, v0, v2}, LX/7vF;-><init>(Lcom/facebook/events/model/Event;Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)V

    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;
    .locals 6

    .prologue
    .line 1274264
    new-instance v2, LX/7p3;

    invoke-direct {v2}, LX/7p3;-><init>()V

    .line 1274265
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->t()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->a:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 1274266
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    .line 1274267
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->v()Z

    move-result v3

    iput-boolean v3, v2, LX/7p3;->c:Z

    .line 1274268
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->w()Z

    move-result v3

    iput-boolean v3, v2, LX/7p3;->d:Z

    .line 1274269
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->k()Z

    move-result v3

    iput-boolean v3, v2, LX/7p3;->e:Z

    .line 1274270
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->x()Z

    move-result v3

    iput-boolean v3, v2, LX/7p3;->f:Z

    .line 1274271
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->g:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1274272
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1274273
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ac()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    .line 1274274
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->z()J

    move-result-wide v4

    iput-wide v4, v2, LX/7p3;->j:J

    .line 1274275
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->b()J

    move-result-wide v4

    iput-wide v4, v2, LX/7p3;->k:J

    .line 1274276
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ad()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    .line 1274277
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ae()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1274278
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->B()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->n:Ljava/lang/String;

    .line 1274279
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->af()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    .line 1274280
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    .line 1274281
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1274282
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ai()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->r:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    .line 1274283
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->s:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1274284
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    .line 1274285
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    .line 1274286
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1274287
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->J()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->w:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1274288
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->x:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 1274289
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->L()Lcom/facebook/graphql/enums/GraphQLEventType;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->y:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 1274290
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    .line 1274291
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->N()Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->A:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 1274292
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->an()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->B:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 1274293
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->C:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1274294
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 1274295
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->E:Ljava/lang/String;

    .line 1274296
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->eQ_()Z

    move-result v3

    iput-boolean v3, v2, LX/7p3;->F:Z

    .line 1274297
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->R()Z

    move-result v3

    iput-boolean v3, v2, LX/7p3;->G:Z

    .line 1274298
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->n()Z

    move-result v3

    iput-boolean v3, v2, LX/7p3;->H:Z

    .line 1274299
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->S()Z

    move-result v3

    iput-boolean v3, v2, LX/7p3;->I:Z

    .line 1274300
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->J:Ljava/lang/String;

    .line 1274301
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->K:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    .line 1274302
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->U()Z

    move-result v3

    iput-boolean v3, v2, LX/7p3;->L:Z

    .line 1274303
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ar()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->M:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    .line 1274304
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->o()J

    move-result-wide v4

    iput-wide v4, v2, LX/7p3;->N:J

    .line 1274305
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->j()J

    move-result-wide v4

    iput-wide v4, v2, LX/7p3;->O:J

    .line 1274306
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->as()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->P:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 1274307
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->p()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->Q:Ljava/lang/String;

    .line 1274308
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->X()I

    move-result v3

    iput v3, v2, LX/7p3;->R:I

    .line 1274309
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->S:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1274310
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->r()Z

    move-result v3

    iput-boolean v3, v2, LX/7p3;->T:Z

    .line 1274311
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Y()LX/0Px;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->U:LX/0Px;

    .line 1274312
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Z()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->V:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1274313
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v3

    iput-object v3, v2, LX/7p3;->W:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1274314
    move-object v0, v2

    .line 1274315
    iget-object v1, p0, LX/7vF;->b:Lcom/facebook/events/model/Event;

    .line 1274316
    iget-boolean v2, v1, Lcom/facebook/events/model/Event;->H:Z

    move v1, v2

    .line 1274317
    iput-boolean v1, v0, LX/7p3;->T:Z

    .line 1274318
    move-object v0, v0

    .line 1274319
    iget-object v1, p0, LX/7vF;->b:Lcom/facebook/events/model/Event;

    invoke-virtual {v1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    .line 1274320
    iput-object v1, v0, LX/7p3;->S:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1274321
    move-object v0, v0

    .line 1274322
    iget-object v1, p0, LX/7vF;->b:Lcom/facebook/events/model/Event;

    .line 1274323
    iget-object v2, v1, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v1, v2

    .line 1274324
    iput-object v1, v0, LX/7p3;->W:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1274325
    move-object v0, v0

    .line 1274326
    invoke-virtual {v0}, LX/7p3;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1274261
    iget-object v0, p0, LX/7vF;->a:Lcom/facebook/events/model/Event;

    .line 1274262
    iget-object p0, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1274263
    return-object v0
.end method

.method public final c()LX/7vF;
    .locals 4

    .prologue
    .line 1274260
    new-instance v0, LX/7vF;

    iget-object v1, p0, LX/7vF;->b:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/7vF;->a:Lcom/facebook/events/model/Event;

    iget-object v3, p0, LX/7vF;->c:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-direct {v0, v1, v2, v3}, LX/7vF;-><init>(Lcom/facebook/events/model/Event;Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)V

    return-object v0
.end method
