.class public abstract LX/7mV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/compost/story/CompostStory;",
        ">",
        "Ljava/lang/Object;",
        "LX/0c5;"
    }
.end annotation


# instance fields
.field public final a:LX/0SG;

.field public final b:Ljava/lang/Object;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field public d:LX/7mc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7mc",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 1

    .prologue
    .line 1237119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1237120
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/7mV;->b:Ljava/lang/Object;

    .line 1237121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7mV;->c:Ljava/util/List;

    .line 1237122
    iput-object p1, p0, LX/7mV;->a:LX/0SG;

    .line 1237123
    const/4 v0, 0x0

    iput-object v0, p0, LX/7mV;->d:LX/7mc;

    .line 1237124
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1237116
    iget-object v1, p0, LX/7mV;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 1237117
    :try_start_0
    iget-object v0, p0, LX/7mV;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1237118
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 1237104
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1237105
    iget-object v1, p0, LX/7mV;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 1237106
    :try_start_0
    iget-object v3, p0, LX/7mV;->c:Ljava/util/List;

    new-instance v4, LX/7mb;

    invoke-direct {v4, p0}, LX/7mb;-><init>(LX/7mV;)V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1237107
    const/4 v3, 0x0

    move v4, v3

    :goto_0
    iget-object v3, p0, LX/7mV;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_0

    .line 1237108
    iget-object v3, p0, LX/7mV;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v5

    iget-object v3, p0, LX/7mV;->c:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7mi;

    invoke-virtual {v3}, LX/7mi;->a()J

    move-result-wide v7

    sub-long/2addr v5, v7

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    .line 1237109
    invoke-virtual {p0}, LX/7mV;->b()J

    move-result-wide v7

    cmp-long v3, v5, v7

    if-lez v3, :cond_1

    .line 1237110
    iget-object v3, p0, LX/7mV;->c:Ljava/util/List;

    iget-object v5, p0, LX/7mV;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v3, v4, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1237111
    :cond_0
    iget-object v2, p0, LX/7mV;->c:Ljava/util/List;

    invoke-virtual {v0, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1237112
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1237113
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1237114
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1237115
    :cond_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0
.end method

.method public a(LX/7mi;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1237066
    iget-object v2, p0, LX/7mV;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 1237067
    :try_start_0
    invoke-virtual {p1}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/7mV;->e(Ljava/lang/String;)I

    move-result v0

    .line 1237068
    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    .line 1237069
    iget-object v0, p0, LX/7mV;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 1237070
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1237071
    iget-object v2, p0, LX/7mV;->d:LX/7mc;

    if-eqz v2, :cond_0

    .line 1237072
    if-eqz v0, :cond_2

    .line 1237073
    iget-object v0, p0, LX/7mV;->d:LX/7mc;

    invoke-interface {v0, p1}, LX/7mc;->a(Ljava/lang/Object;)V

    .line 1237074
    :cond_0
    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1237075
    :cond_1
    :try_start_1
    iget-object v3, p0, LX/7mV;->c:Ljava/util/List;

    invoke-interface {v3, v0, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1237076
    const/4 v0, 0x0

    goto :goto_0

    .line 1237077
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1237078
    :cond_2
    iget-object v0, p0, LX/7mV;->d:LX/7mc;

    invoke-interface {v0}, LX/7mc;->a()V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1237094
    const/4 v0, 0x0

    .line 1237095
    iget-object v1, p0, LX/7mV;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 1237096
    :try_start_0
    invoke-virtual {p0, p1}, LX/7mV;->e(Ljava/lang/String;)I

    move-result v2

    .line 1237097
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 1237098
    iget-object v0, p0, LX/7mV;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7mi;

    .line 1237099
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1237100
    if-eqz v0, :cond_1

    iget-object v1, p0, LX/7mV;->d:LX/7mc;

    if-eqz v1, :cond_1

    .line 1237101
    iget-object v1, p0, LX/7mV;->d:LX/7mc;

    invoke-interface {v1, v0}, LX/7mc;->b(Ljava/lang/Object;)V

    .line 1237102
    :cond_1
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1237103
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public abstract b()J
.end method

.method public c(Ljava/lang/String;)LX/7mi;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1237089
    if-nez p1, :cond_0

    move-object v0, v1

    .line 1237090
    :goto_0
    return-object v0

    .line 1237091
    :cond_0
    iget-object v0, p0, LX/7mV;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7mi;

    .line 1237092
    invoke-virtual {v0}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 1237093
    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 1237087
    invoke-direct {p0}, LX/7mV;->e()V

    .line 1237088
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1237085
    const/4 v0, 0x0

    iput-object v0, p0, LX/7mV;->d:LX/7mc;

    .line 1237086
    return-void
.end method

.method public final d(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1237084
    invoke-virtual {p0, p1}, LX/7mV;->e(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 1237079
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/7mV;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1237080
    iget-object v0, p0, LX/7mV;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7mi;

    invoke-virtual {v0}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1237081
    :goto_1
    return v1

    .line 1237082
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1237083
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method
