.class public LX/8OS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8OM;


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field private final a:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

.field private final b:LX/8Om;

.field private final c:LX/7mC;

.field private final d:LX/7m9;

.field private final e:LX/11H;

.field private final f:LX/0cW;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8Ne;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/8OL;

.field private final j:LX/0SG;

.field private final k:LX/03V;

.field private final l:LX/8LX;

.field private final m:LX/8LT;

.field private final n:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

.field private final o:LX/8N9;

.field private final p:Ljava/lang/String;

.field private q:Ljava/util/concurrent/Semaphore;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1339457
    const-class v0, LX/8OS;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/8OS;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;LX/8Om;LX/7mC;LX/7m9;LX/11H;LX/0cW;LX/0Or;LX/8OL;LX/0SG;LX/03V;LX/8LX;LX/8LT;Lcom/facebook/photos/upload/protocol/PhotoPublisher;LX/8N9;Ljava/lang/String;)V
    .locals 0
    .param p15    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;",
            "LX/8Om;",
            "LX/7mC;",
            "LX/7m9;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/0cW;",
            "LX/0Or",
            "<",
            "LX/8Ne;",
            ">;",
            "LX/8OL;",
            "LX/0SG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/8LX;",
            "LX/8LT;",
            "Lcom/facebook/photos/upload/protocol/PhotoPublisher;",
            "LX/8N9;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1339440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1339441
    iput-object p1, p0, LX/8OS;->a:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    .line 1339442
    iput-object p2, p0, LX/8OS;->b:LX/8Om;

    .line 1339443
    iput-object p3, p0, LX/8OS;->c:LX/7mC;

    .line 1339444
    iput-object p4, p0, LX/8OS;->d:LX/7m9;

    .line 1339445
    iput-object p5, p0, LX/8OS;->e:LX/11H;

    .line 1339446
    iput-object p6, p0, LX/8OS;->f:LX/0cW;

    .line 1339447
    iput-object p7, p0, LX/8OS;->h:LX/0Or;

    .line 1339448
    iput-object p8, p0, LX/8OS;->i:LX/8OL;

    .line 1339449
    iput-object p9, p0, LX/8OS;->j:LX/0SG;

    .line 1339450
    iput-object p10, p0, LX/8OS;->k:LX/03V;

    .line 1339451
    iput-object p11, p0, LX/8OS;->l:LX/8LX;

    .line 1339452
    iput-object p12, p0, LX/8OS;->m:LX/8LT;

    .line 1339453
    iput-object p13, p0, LX/8OS;->n:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    .line 1339454
    iput-object p14, p0, LX/8OS;->o:LX/8N9;

    .line 1339455
    iput-object p15, p0, LX/8OS;->p:Ljava/lang/String;

    .line 1339456
    return-void
.end method

.method private static a(LX/8OS;Lcom/facebook/photos/upload/operation/UploadOperation;LX/0Px;LX/0Px;LX/0Px;Landroid/os/Bundle;LX/8Ne;Z)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/Bundle;",
            "LX/8Ne;",
            "Z)",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    .line 1339419
    const/4 v0, 0x0

    .line 1339420
    iget-object v1, p0, LX/8OS;->l:LX/8LX;

    invoke-virtual {v1, p1}, LX/8LX;->b(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/5M9;

    move-result-object v1

    .line 1339421
    iput-object p2, v1, LX/5M9;->Z:LX/0Px;

    .line 1339422
    move-object v2, v1

    .line 1339423
    iput-object p3, v2, LX/5M9;->Y:LX/0Px;

    .line 1339424
    move-object v2, v2

    .line 1339425
    iput-object p4, v2, LX/5M9;->aa:LX/0Px;

    .line 1339426
    move-object v2, v2

    .line 1339427
    iget-wide v6, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->u:J

    move-wide v4, v6

    .line 1339428
    iput-wide v4, v2, LX/5M9;->f:J

    .line 1339429
    invoke-virtual {v1}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v2

    .line 1339430
    :cond_0
    :try_start_0
    invoke-static {p0, p1, v2, p5, p7}, LX/8OS;->a(LX/8OS;Lcom/facebook/photos/upload/operation/UploadOperation;Lcom/facebook/composer/publish/common/PublishPostParams;Landroid/os/Bundle;Z)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 1339431
    invoke-interface {p6}, LX/8Ne;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1339432
    return-object v1

    .line 1339433
    :catch_0
    move-exception v1

    .line 1339434
    invoke-interface {p6, v1}, LX/8Ne;->a(Ljava/lang/Exception;)V

    .line 1339435
    iget-object v3, p0, LX/8OS;->i:LX/8OL;

    .line 1339436
    iget-boolean v4, v3, LX/8OL;->d:Z

    move v3, v4

    .line 1339437
    if-nez v3, :cond_1

    add-int/lit8 v0, v0, 0x1

    invoke-interface {p6}, LX/8Ne;->b()I

    move-result v3

    if-le v0, v3, :cond_0

    .line 1339438
    :cond_1
    iget-object v2, p0, LX/8OS;->i:LX/8OL;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Publish cancelled at attempt #"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1339439
    throw v1
.end method

.method private static a(LX/8OS;Lcom/facebook/photos/upload/operation/UploadOperation;LX/0Px;LX/0Px;Landroid/os/Bundle;LX/8Ne;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/Bundle;",
            "LX/8Ne;",
            ")",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1339407
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->al:Lcom/facebook/composer/publish/common/EditPostParams;

    move-object v1, v1

    .line 1339408
    invoke-static {v1}, Lcom/facebook/composer/publish/common/EditPostParams;->a(Lcom/facebook/composer/publish/common/EditPostParams;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setMediaFbIds(LX/0Px;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setMediaCaptions(LX/0Px;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->a()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v2

    .line 1339409
    :cond_0
    :try_start_0
    iget-object v1, p0, LX/8OS;->e:LX/11H;

    iget-object v3, p0, LX/8OS;->d:LX/7m9;

    invoke-virtual {v1, v3, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339410
    invoke-interface {p5}, LX/8Ne;->a()V

    .line 1339411
    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/EditPostParams;->getStoryId()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/util/Pair;

    const/4 v4, 0x0

    const-string v5, "fbids"

    invoke-static {v5, p4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;[Landroid/util/Pair;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1339412
    :catch_0
    move-exception v1

    .line 1339413
    invoke-interface {p5, v1}, LX/8Ne;->a(Ljava/lang/Exception;)V

    .line 1339414
    iget-object v3, p0, LX/8OS;->i:LX/8OL;

    .line 1339415
    iget-boolean v4, v3, LX/8OL;->d:Z

    move v3, v4

    .line 1339416
    if-nez v3, :cond_1

    add-int/lit8 v0, v0, 0x1

    invoke-interface {p5}, LX/8Ne;->b()I

    move-result v3

    if-le v0, v3, :cond_0

    .line 1339417
    :cond_1
    iget-object v2, p0, LX/8OS;->i:LX/8OL;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Publish cancelled at attempt #"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1339418
    throw v1
.end method

.method private static a(LX/8OS;Lcom/facebook/photos/upload/operation/UploadOperation;Lcom/facebook/composer/publish/common/PublishPostParams;Landroid/os/Bundle;Z)Lcom/facebook/fbservice/service/OperationResult;
    .locals 12

    .prologue
    .line 1339380
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->O()LX/8LS;

    move-result-object v0

    sget-object v1, LX/8LS;->TARGET:LX/8LS;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->D()Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->D()Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    move v1, v0

    .line 1339381
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->U()Lcom/facebook/audience/model/UploadShot;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    move v9, v0

    .line 1339382
    :goto_1
    const/4 v5, 0x0

    .line 1339383
    if-eqz v1, :cond_1

    .line 1339384
    iget-object v0, p0, LX/8OS;->e:LX/11H;

    iget-object v2, p0, LX/8OS;->c:LX/7mC;

    invoke-virtual {v0, v2, p2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v5, v0

    .line 1339385
    :cond_1
    const/4 v0, 0x0

    .line 1339386
    if-eqz v9, :cond_8

    .line 1339387
    iget-object v0, p2, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaFbIds:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339388
    iget-object v0, p2, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaFbIds:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1339389
    iget-object v2, p0, LX/8OS;->o:LX/8N9;

    iget-object v0, p2, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaFbIds:LX/0Px;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, LX/8N9;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/Long;)LX/2Vj;

    move-result-object v0

    .line 1339390
    iget-object v2, p0, LX/8OS;->e:LX/11H;

    invoke-virtual {v0}, LX/2Vj;->a()LX/0e6;

    move-result-object v3

    invoke-virtual {v0}, LX/2Vj;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/upload/protocol/ShotCreateResult;

    move-object v8, v0

    .line 1339391
    :goto_3
    if-eqz v1, :cond_6

    if-eqz p4, :cond_6

    .line 1339392
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1339393
    iget-object v2, p2, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaFbIds:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_5

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1339394
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1339395
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1339396
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 1339397
    :cond_3
    const/4 v0, 0x0

    move v9, v0

    goto :goto_1

    .line 1339398
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 1339399
    :cond_5
    iget-object v0, p0, LX/8OS;->l:LX/8LX;

    iget-object v1, p0, LX/8OS;->j:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, LX/8LX;->a(Lcom/facebook/photos/upload/operation/UploadOperation;J)LX/73w;

    move-result-object v2

    .line 1339400
    iget-object v0, p0, LX/8OS;->n:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    const/4 v7, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;ZZLjava/lang/String;LX/0Px;I)LX/8Mw;

    .line 1339401
    :cond_6
    const/4 v0, 0x0

    .line 1339402
    if-eqz v9, :cond_7

    .line 1339403
    invoke-virtual {v8}, Lcom/facebook/audience/upload/protocol/ShotCreateResult;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1339404
    :cond_7
    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    .line 1339405
    const-string v2, "shot_fbid"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1339406
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/util/Pair;

    const/4 v2, 0x0

    const-string v3, "fbids"

    invoke-static {v3, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "shot_fbid"

    invoke-static {v3, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v5, v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;[Landroid/util/Pair;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    :cond_8
    move-object v8, v0

    goto :goto_3
.end method

.method public static b(LX/0QB;)LX/8OS;
    .locals 17

    .prologue
    .line 1339378
    new-instance v1, LX/8OS;

    invoke-static/range {p0 .. p0}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->a(LX/0QB;)Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    invoke-static/range {p0 .. p0}, LX/8Om;->a(LX/0QB;)LX/8Om;

    move-result-object v3

    check-cast v3, LX/8Om;

    invoke-static/range {p0 .. p0}, LX/7mC;->a(LX/0QB;)LX/7mC;

    move-result-object v4

    check-cast v4, LX/7mC;

    invoke-static/range {p0 .. p0}, LX/7m9;->a(LX/0QB;)LX/7m9;

    move-result-object v5

    check-cast v5, LX/7m9;

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v6

    check-cast v6, LX/11H;

    invoke-static/range {p0 .. p0}, LX/0cW;->a(LX/0QB;)LX/0cW;

    move-result-object v7

    check-cast v7, LX/0cW;

    const/16 v8, 0x2f01

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/8OL;->a(LX/0QB;)LX/8OL;

    move-result-object v9

    check-cast v9, LX/8OL;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static/range {p0 .. p0}, LX/8LX;->a(LX/0QB;)LX/8LX;

    move-result-object v12

    check-cast v12, LX/8LX;

    invoke-static/range {p0 .. p0}, LX/8LT;->a(LX/0QB;)LX/8LT;

    move-result-object v13

    check-cast v13, LX/8LT;

    invoke-static/range {p0 .. p0}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(LX/0QB;)Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move-result-object v14

    check-cast v14, Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    invoke-static/range {p0 .. p0}, LX/8N9;->a(LX/0QB;)LX/8N9;

    move-result-object v15

    check-cast v15, LX/8N9;

    invoke-static/range {p0 .. p0}, LX/0dG;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    invoke-direct/range {v1 .. v16}, LX/8OS;-><init>(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;LX/8Om;LX/7mC;LX/7m9;LX/11H;LX/0cW;LX/0Or;LX/8OL;LX/0SG;LX/03V;LX/8LX;LX/8LT;Lcom/facebook/photos/upload/protocol/PhotoPublisher;LX/8N9;Ljava/lang/String;)V

    .line 1339379
    return-object v1
.end method

.method private static b(Lcom/facebook/photos/upload/operation/UploadOperation;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1339458
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v3, v0

    .line 1339459
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1339460
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v5, LX/4gF;->PHOTO:LX/4gF;

    invoke-virtual {v0, v5}, LX/4gF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1339461
    const/4 v0, 0x1

    .line 1339462
    :goto_1
    return v0

    .line 1339463
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1339464
    goto :goto_1
.end method

.method private c(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 20

    .prologue
    .line 1339286
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8OS;->h:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, LX/8Ne;

    .line 1339287
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->i()Z

    move-result v2

    invoke-interface {v13, v2}, LX/8Ne;->a(Z)V

    .line 1339288
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8OS;->q:Ljava/util/concurrent/Semaphore;

    invoke-interface {v13, v2}, LX/8Ne;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1339289
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->v()Lcom/facebook/photos/upload/operation/UploadRecords;

    move-result-object v11

    .line 1339290
    if-eqz v11, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Lcom/facebook/photos/upload/operation/UploadRecords;->a(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadRecord;

    move-result-object v2

    .line 1339291
    :goto_0
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v12

    .line 1339292
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/facebook/photos/upload/operation/UploadRecord;->multimediaInfo:Lcom/facebook/photos/upload/operation/MultimediaInfo;

    if-eqz v3, :cond_0

    .line 1339293
    iget-object v2, v2, Lcom/facebook/photos/upload/operation/UploadRecord;->multimediaInfo:Lcom/facebook/photos/upload/operation/MultimediaInfo;

    iget-object v2, v2, Lcom/facebook/photos/upload/operation/MultimediaInfo;->videoPathToWaterfallId:LX/0P1;

    invoke-virtual {v12, v2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 1339294
    :cond_0
    invoke-static/range {p1 .. p1}, LX/8OS;->b(Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1339295
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8OS;->a:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    .line 1339296
    invoke-virtual {v3}, Lcom/facebook/fbservice/service/OperationResult;->success()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1339297
    const-string v2, "fbids"

    invoke-virtual {v3, v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 1339298
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->J()LX/8LR;

    move-result-object v4

    sget-object v5, LX/8LR;->SLIDESHOW:LX/8LR;

    if-ne v4, v5, :cond_3

    move-object v2, v3

    .line 1339299
    :goto_1
    return-object v2

    .line 1339300
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1339301
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/fbservice/service/OperationResult;->getErrorCode()LX/1nY;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    goto :goto_1

    :cond_3
    move-object v9, v2

    .line 1339302
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->y()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v14

    .line 1339303
    const/4 v2, 0x0

    move v10, v2

    :goto_3
    if-ge v10, v14, :cond_b

    .line 1339304
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->y()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/media/MediaItem;

    .line 1339305
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v3

    sget-object v4, LX/4gF;->VIDEO:LX/4gF;

    invoke-virtual {v3, v4}, LX/4gF;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1339306
    if-eqz v11, :cond_6

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Lcom/facebook/photos/upload/operation/UploadRecords;->a(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadRecord;

    move-result-object v3

    .line 1339307
    :goto_4
    if-eqz v3, :cond_7

    iget-wide v4, v3, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_7

    .line 1339308
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    iget-wide v4, v3, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    invoke-virtual {v9, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1339309
    :cond_4
    :goto_5
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_3

    .line 1339310
    :cond_5
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    move-object v9, v2

    goto :goto_2

    .line 1339311
    :cond_6
    const/4 v3, 0x0

    goto :goto_4

    .line 1339312
    :cond_7
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->q()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1339313
    new-instance v4, LX/8LQ;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, LX/8LQ;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1339314
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1339315
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1339316
    :goto_6
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/8LQ;->a(LX/0Px;)LX/8LQ;

    .line 1339317
    invoke-virtual {v4, v3}, LX/8LQ;->a(Ljava/lang/String;)LX/8LQ;

    .line 1339318
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/8LQ;->b(Ljava/lang/String;)LX/8LQ;

    .line 1339319
    invoke-virtual {v9}, Landroid/os/Bundle;->size()I

    move-result v3

    invoke-virtual {v4, v3}, LX/8LQ;->a(I)LX/8LQ;

    .line 1339320
    invoke-virtual {v4, v14}, LX/8LQ;->b(I)LX/8LQ;

    .line 1339321
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->z()LX/0Px;

    move-result-object v3

    .line 1339322
    if-eqz v3, :cond_8

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_8

    .line 1339323
    invoke-virtual {v3, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    .line 1339324
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8OS;->m:LX/8LT;

    invoke-virtual {v5, v2, v3}, LX/8LT;->a(Lcom/facebook/ipc/media/MediaItem;Landroid/os/Bundle;)I

    move-result v5

    invoke-virtual {v4, v5}, LX/8LQ;->c(I)LX/8LQ;

    .line 1339325
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/8LQ;->b(LX/0Px;)LX/8LQ;

    .line 1339326
    :cond_8
    sget-object v3, LX/8LS;->VIDEO:LX/8LS;

    invoke-virtual {v4, v3}, LX/8LQ;->a(LX/8LS;)LX/8LQ;

    .line 1339327
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8OS;->b:LX/8Om;

    invoke-virtual {v3}, LX/8Om;->a()V

    .line 1339328
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8OS;->b:LX/8Om;

    invoke-virtual {v4}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/8Om;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    .line 1339329
    invoke-virtual {v3}, Lcom/facebook/fbservice/service/OperationResult;->success()Z

    move-result v4

    if-nez v4, :cond_a

    .line 1339330
    invoke-virtual {v3}, Lcom/facebook/fbservice/service/OperationResult;->getErrorCode()LX/1nY;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    goto/16 :goto_1

    .line 1339331
    :cond_9
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1339332
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339333
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8OS;->f:LX/0cW;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/facebook/photos/upload/operation/UploadRecord;

    new-instance v8, Lcom/facebook/photos/upload/operation/MultimediaInfo;

    invoke-static {v12}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v15

    invoke-direct {v8, v15}, Lcom/facebook/photos/upload/operation/MultimediaInfo;-><init>(LX/0P1;)V

    invoke-direct {v7, v8}, Lcom/facebook/photos/upload/operation/UploadRecord;-><init>(Lcom/facebook/photos/upload/operation/MultimediaInfo;)V

    invoke-virtual {v5, v6, v7}, LX/0cW;->a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadRecord;)Z

    goto/16 :goto_6

    .line 1339334
    :cond_a
    invoke-virtual {v3}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1339335
    move-object/from16 v0, p0

    iget-object v15, v0, LX/8OS;->f:LX/0cW;

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v16

    new-instance v3, Lcom/facebook/photos/upload/operation/UploadRecord;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/8OS;->j:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/facebook/photos/upload/operation/UploadRecord;-><init>(JJZ)V

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v3}, LX/0cW;->a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadRecord;)Z

    .line 1339336
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_5

    .line 1339337
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->J()LX/8LR;

    move-result-object v2

    sget-object v3, LX/8LR;->EDIT_MULTIMEDIA:LX/8LR;

    if-ne v2, v3, :cond_c

    const/4 v2, 0x1

    move v4, v2

    .line 1339338
    :goto_7
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->z()LX/0Px;

    move-result-object v7

    .line 1339339
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1339340
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 1339341
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 1339342
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 1339343
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 1339344
    if-eqz v4, :cond_d

    .line 1339345
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->T()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v3

    .line 1339346
    if-eqz v3, :cond_d

    .line 1339347
    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/EditPostParams;->getMediaFbIds()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    .line 1339348
    const/4 v2, 0x0

    :goto_8
    if-ge v2, v5, :cond_d

    .line 1339349
    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/EditPostParams;->getMediaFbIds()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v8, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1339350
    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/EditPostParams;->getMediaCaptions()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v10, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1339351
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 1339352
    :cond_c
    const/4 v2, 0x0

    move v4, v2

    goto :goto_7

    .line 1339353
    :cond_d
    const/4 v3, 0x0

    .line 1339354
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->y()LX/0Px;

    move-result-object v15

    invoke-virtual {v15}, LX/0Px;->size()I

    move-result v16

    const/4 v2, 0x0

    move v5, v2

    move v6, v3

    :goto_9
    move/from16 v0, v16

    if-ge v5, v0, :cond_12

    invoke-virtual {v15, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/media/MediaItem;

    .line 1339355
    const/4 v3, 0x0

    .line 1339356
    if-eqz v7, :cond_e

    .line 1339357
    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    .line 1339358
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1339359
    const-string v17, "caption"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1339360
    :cond_e
    if-nez v3, :cond_f

    const-string v3, ""

    .line 1339361
    :cond_f
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_10

    .line 1339362
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1339363
    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1339364
    :goto_a
    add-int/lit8 v3, v6, 0x1

    .line 1339365
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v6, v3

    goto :goto_9

    .line 1339366
    :cond_10
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->q()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_11

    .line 1339367
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1339368
    invoke-interface {v14, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 1339369
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8OS;->k:LX/03V;

    sget-object v3, LX/8OS;->g:Ljava/lang/String;

    const-string v17, "Couldn\'t find fbid for media"

    move-object/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    .line 1339370
    :cond_12
    invoke-virtual {v10, v14}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2, v12}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1339371
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8OS;->i:LX/8OL;

    const-string v3, "before multimedia publish"

    invoke-virtual {v2, v3}, LX/8OL;->b(Ljava/lang/String;)V

    .line 1339372
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1339373
    invoke-virtual {v9}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1339374
    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v2, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b

    .line 1339375
    :cond_13
    if-eqz v4, :cond_14

    .line 1339376
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object v7, v13

    invoke-static/range {v2 .. v7}, LX/8OS;->a(LX/8OS;Lcom/facebook/photos/upload/operation/UploadOperation;LX/0Px;LX/0Px;Landroid/os/Bundle;LX/8Ne;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    goto/16 :goto_1

    .line 1339377
    :cond_14
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v10

    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v11

    invoke-static/range {p0 .. p1}, LX/8OS;->d(LX/8OS;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v14

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object v12, v6

    invoke-static/range {v7 .. v14}, LX/8OS;->a(LX/8OS;Lcom/facebook/photos/upload/operation/UploadOperation;LX/0Px;LX/0Px;LX/0Px;Landroid/os/Bundle;LX/8Ne;Z)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    goto/16 :goto_1
.end method

.method private static d(LX/8OS;Lcom/facebook/photos/upload/operation/UploadOperation;)Z
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1339280
    iget-wide v6, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v2, v6

    .line 1339281
    iget-object v1, p0, LX/8OS;->p:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1339282
    iget-wide v6, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->u:J

    move-wide v2, v6

    .line 1339283
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v1

    if-le v1, v0, :cond_0

    .line 1339284
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    move-object v1, v1

    .line 1339285
    sget-object v2, LX/21D;->TIMELINE:LX/21D;

    invoke-virtual {v2}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v2

    if-eq v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1339272
    :try_start_0
    iget-object v0, p0, LX/8OS;->f:LX/0cW;

    .line 1339273
    const-string v1, "multimedia_upload_in_progress_waterfallid"

    invoke-static {v0, p1, v1}, LX/0cW;->b(LX/0cW;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    .line 1339274
    invoke-static {v0, p1}, LX/0cW;->i(LX/0cW;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1339275
    invoke-direct {p0, p1}, LX/8OS;->c(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1339276
    iget-object v1, p0, LX/8OS;->f:LX/0cW;

    invoke-virtual {v1, p1}, LX/0cW;->c(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1339277
    iput-object v2, p0, LX/8OS;->q:Ljava/util/concurrent/Semaphore;

    return-object v0

    .line 1339278
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/8OS;->f:LX/0cW;

    invoke-virtual {v1, p1}, LX/0cW;->c(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1339279
    iput-object v2, p0, LX/8OS;->q:Ljava/util/concurrent/Semaphore;

    throw v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1339267
    iget-object v0, p0, LX/8OS;->a:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    invoke-virtual {v0}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->a()V

    .line 1339268
    iget-object v0, p0, LX/8OS;->b:LX/8Om;

    invoke-virtual {v0}, LX/8Om;->a()V

    .line 1339269
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, LX/8OS;->q:Ljava/util/concurrent/Semaphore;

    .line 1339270
    iget-object v0, p0, LX/8OS;->i:LX/8OL;

    invoke-virtual {v0}, LX/8OL;->a()V

    .line 1339271
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 1339261
    iget-object v0, p0, LX/8OS;->q:Ljava/util/concurrent/Semaphore;

    if-eqz v0, :cond_0

    .line 1339262
    iget-object v0, p0, LX/8OS;->q:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1339263
    :cond_0
    iget-object v0, p0, LX/8OS;->a:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    invoke-virtual {v0}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->b()Z

    move-result v0

    .line 1339264
    iget-object v1, p0, LX/8OS;->b:LX/8Om;

    invoke-virtual {v1}, LX/8Om;->b()Z

    move-result v1

    .line 1339265
    iget-object v2, p0, LX/8OS;->i:LX/8OL;

    invoke-virtual {v2}, LX/8OL;->c()Z

    move-result v2

    .line 1339266
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
