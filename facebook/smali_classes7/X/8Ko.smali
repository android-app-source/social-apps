.class public LX/8Ko;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1330617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1330618
    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330677
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 1330678
    if-eqz v0, :cond_0

    const v0, 0x7f08201c

    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f08201b

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1330676
    const v0, 0x7f0218e8

    return v0
.end method

.method public a(Lcom/facebook/photos/upload/operation/UploadOperation;)I
    .locals 1

    .prologue
    .line 1330670
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->N:Ljava/lang/String;

    move-object v0, v0

    .line 1330671
    if-nez v0, :cond_0

    .line 1330672
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 1330673
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0

    .line 1330674
    :cond_0
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->N:Ljava/lang/String;

    move-object v0, v0

    .line 1330675
    goto :goto_0
.end method

.method public a(Landroid/content/Context;II)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1330669
    if-nez p2, :cond_0

    const v0, 0x7f08201d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f08201e

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330668
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, LX/8Ko;->a(Landroid/content/Context;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/Boolean;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1330653
    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1330654
    const v0, 0x7f082029

    .line 1330655
    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1330656
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->ae()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1330657
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1330658
    const v0, 0x7f08202f

    goto :goto_0

    .line 1330659
    :cond_1
    const v0, 0x7f08202e

    goto :goto_0

    .line 1330660
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1330661
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1330662
    const v0, 0x7f08202d

    goto :goto_0

    .line 1330663
    :cond_3
    const v0, 0x7f08202c

    goto :goto_0

    .line 1330664
    :cond_4
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1330665
    sget-object v1, LX/8LS;->PHOTO_REVIEW:LX/8LS;

    if-ne v0, v1, :cond_5

    .line 1330666
    const v0, 0x7f082028

    goto :goto_0

    .line 1330667
    :cond_5
    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    const v0, 0x7f08202a

    goto :goto_0

    :cond_6
    const v0, 0x7f08202b

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1330652
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 1330651
    const v0, 0x1080078

    return v0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330650
    const v0, 0x7f082034

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/Context;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1330623
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1330624
    sget-object v1, LX/8LS;->VIDEO:LX/8LS;

    if-eq v0, v1, :cond_0

    .line 1330625
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1330626
    sget-object v1, LX/8LS;->PROFILE_PIC:LX/8LS;

    if-eq v0, v1, :cond_0

    .line 1330627
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1330628
    sget-object v1, LX/8LS;->COVER_PHOTO:LX/8LS;

    if-eq v0, v1, :cond_0

    .line 1330629
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1330630
    sget-object v1, LX/8LS;->PLACE_PHOTO:LX/8LS;

    if-eq v0, v1, :cond_0

    .line 1330631
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1330632
    sget-object v1, LX/8LS;->MENU_PHOTO:LX/8LS;

    if-eq v0, v1, :cond_0

    .line 1330633
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1330634
    sget-object v1, LX/8LS;->PROFILE_VIDEO:LX/8LS;

    if-eq v0, v1, :cond_0

    .line 1330635
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1330636
    sget-object v1, LX/8LS;->PRODUCT_IMAGE:LX/8LS;

    if-eq v0, v1, :cond_0

    .line 1330637
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1330638
    sget-object v1, LX/8LS;->LIVE_VIDEO:LX/8LS;

    if-eq v0, v1, :cond_0

    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1330639
    iget-boolean v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->n:Z

    move v0, v0

    .line 1330640
    if-eqz v0, :cond_1

    .line 1330641
    :cond_0
    const v0, 0x7f082021

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1330642
    :goto_0
    return-object v0

    .line 1330643
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->ad()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1330644
    const v0, 0x7f082022

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1330645
    :cond_2
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1330646
    sget-object v1, LX/8LS;->PHOTO_REVIEW:LX/8LS;

    if-ne v0, v1, :cond_3

    .line 1330647
    const v0, 0x7f082025

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1330648
    :cond_3
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 1330649
    if-eqz v0, :cond_4

    const v0, 0x7f082024

    :goto_1
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    const v0, 0x7f082023

    goto :goto_1
.end method

.method public c()I
    .locals 1

    .prologue
    .line 1330622
    const v0, 0x7f0218e3

    return v0
.end method

.method public c(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330621
    const v0, 0x7f08201d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330620
    const v0, 0x7f082032

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330619
    const v0, 0x7f082031

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
