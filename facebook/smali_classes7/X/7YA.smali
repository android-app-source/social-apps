.class public LX/7YA;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/7YA;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1219027
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1219028
    const-string v0, "zero_upsell"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 1219029
    return-void
.end method

.method public static a(LX/0QB;)LX/7YA;
    .locals 3

    .prologue
    .line 1219030
    sget-object v0, LX/7YA;->a:LX/7YA;

    if-nez v0, :cond_1

    .line 1219031
    const-class v1, LX/7YA;

    monitor-enter v1

    .line 1219032
    :try_start_0
    sget-object v0, LX/7YA;->a:LX/7YA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1219033
    if-eqz v2, :cond_0

    .line 1219034
    :try_start_1
    new-instance v0, LX/7YA;

    invoke-direct {v0}, LX/7YA;-><init>()V

    .line 1219035
    move-object v0, v0

    .line 1219036
    sput-object v0, LX/7YA;->a:LX/7YA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1219037
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1219038
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1219039
    :cond_1
    sget-object v0, LX/7YA;->a:LX/7YA;

    return-object v0

    .line 1219040
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1219041
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
