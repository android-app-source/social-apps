.class public interface abstract LX/6nB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/inject/DeclareMultiBindings;
.end annotation


# virtual methods
.method public abstract getOmnistoreStartupComponents()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2cF;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getOmnistoreStoredProcedureComponents()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/omnistore/module/OmnistoreStoredProcedureComponent;",
            ">;"
        }
    .end annotation
.end method
