.class public final LX/7Ci;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final a:[I

.field public final synthetic b:Lcom/facebook/spherical/GlMediaRenderThread;


# direct methods
.method public constructor <init>(Lcom/facebook/spherical/GlMediaRenderThread;Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 1181525
    iput-object p1, p0, LX/7Ci;->b:Lcom/facebook/spherical/GlMediaRenderThread;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1181526
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, LX/7Ci;->a:[I

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 1181505
    iget-object v0, p0, LX/7Ci;->b:Lcom/facebook/spherical/GlMediaRenderThread;

    invoke-virtual {v0}, Lcom/facebook/spherical/GlMediaRenderThread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1181506
    iget-object v0, p0, LX/7Ci;->b:Lcom/facebook/spherical/GlMediaRenderThread;

    invoke-static {v0}, Lcom/facebook/spherical/GlMediaRenderThread;->p(Lcom/facebook/spherical/GlMediaRenderThread;)V

    .line 1181507
    :cond_0
    :goto_0
    return-void

    .line 1181508
    :cond_1
    iget-object v0, p0, LX/7Ci;->b:Lcom/facebook/spherical/GlMediaRenderThread;

    invoke-virtual {v0, p1}, Lcom/facebook/spherical/GlMediaRenderThread;->a(Landroid/os/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1181509
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1181510
    :pswitch_0
    iget-object v0, p0, LX/7Ci;->b:Lcom/facebook/spherical/GlMediaRenderThread;

    iget-object v1, p0, LX/7Ci;->a:[I

    .line 1181511
    const/4 v2, 0x0

    iget p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->x:I

    aput p0, v1, v2

    .line 1181512
    const/4 v2, 0x1

    iget p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    aput p0, v1, v2

    .line 1181513
    iget-object v2, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v2, v2, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1181514
    iget-object v2, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->n:F

    .line 1181515
    iput p0, v2, LX/7Ce;->q:F

    .line 1181516
    iget-object v2, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->l:F

    iget p1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->m:F

    invoke-virtual {v2, p0, p1}, LX/7Ce;->e(FF)V

    .line 1181517
    iget-object v2, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v2, v2, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1181518
    invoke-virtual {v0, v1}, Lcom/facebook/spherical/GlMediaRenderThread;->c([I)V

    .line 1181519
    goto :goto_0

    .line 1181520
    :pswitch_1
    iget-object v0, p0, LX/7Ci;->b:Lcom/facebook/spherical/GlMediaRenderThread;

    iget-object v1, p0, LX/7Ci;->a:[I

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/GlMediaRenderThread;->b([I)V

    goto :goto_0

    .line 1181521
    :pswitch_2
    iget-object v0, p0, LX/7Ci;->b:Lcom/facebook/spherical/GlMediaRenderThread;

    invoke-static {v0}, Lcom/facebook/spherical/GlMediaRenderThread;->p(Lcom/facebook/spherical/GlMediaRenderThread;)V

    goto :goto_0

    .line 1181522
    :pswitch_3
    iget-object v0, p0, LX/7Ci;->b:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 1181523
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    invoke-interface {v1}, LX/7D0;->c()V

    .line 1181524
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
