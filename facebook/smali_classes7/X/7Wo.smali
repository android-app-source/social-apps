.class public LX/7Wo;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1216790
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1216791
    iput-object p1, p0, LX/7Wo;->a:Landroid/content/Context;

    .line 1216792
    iput-object p2, p0, LX/7Wo;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1216793
    new-instance v0, LX/7Wn;

    invoke-direct {v0, p0}, LX/7Wn;-><init>(LX/7Wo;)V

    invoke-virtual {p0, v0}, LX/7Wo;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1216794
    const v0, 0x7f080e9a

    invoke-virtual {p0, v0}, LX/7Wo;->setTitle(I)V

    .line 1216795
    return-void
.end method

.method public static b(LX/0QB;)LX/7Wo;
    .locals 3

    .prologue
    .line 1216796
    new-instance v2, LX/7Wo;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v2, v0, v1}, LX/7Wo;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 1216797
    return-object v2
.end method
