.class public final LX/7kW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

.field public final synthetic b:LX/7kY;


# direct methods
.method public constructor <init>(LX/7kY;Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;)V
    .locals 0

    .prologue
    .line 1232084
    iput-object p1, p0, LX/7kW;->b:LX/7kY;

    iput-object p2, p0, LX/7kW;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1232062
    iget-object v0, p0, LX/7kW;->b:LX/7kY;

    iget-object v0, v0, LX/7kY;->c:LX/7ka;

    iget-object v1, p0, LX/7kW;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 1232063
    iget-object v2, v1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->m:Ljava/lang/String;

    move-object v1, v2

    .line 1232064
    invoke-virtual {v0, v1}, LX/7ka;->a(Ljava/lang/String;)V

    .line 1232065
    iget-object v0, p0, LX/7kW;->b:LX/7kY;

    iget-object v0, v0, LX/7kY;->b:LX/7jB;

    sget-object v1, LX/7jD;->CREATE:LX/7jD;

    iget-object v2, p0, LX/7kW;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 1232066
    iget-object p0, v2, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->m:Ljava/lang/String;

    move-object v2, p0

    .line 1232067
    invoke-static {v1, v2}, LX/7jK;->a(LX/7jD;Ljava/lang/String;)LX/7jK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1232068
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1232069
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1232070
    iget-object v0, p0, LX/7kW;->b:LX/7kY;

    iget-object v0, v0, LX/7kY;->c:LX/7ka;

    iget-object v1, p0, LX/7kW;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 1232071
    iget-object v2, v1, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->m:Ljava/lang/String;

    move-object v1, v2

    .line 1232072
    invoke-virtual {v0, v1}, LX/7ka;->a(Ljava/lang/String;)V

    .line 1232073
    iget-object v0, p0, LX/7kW;->b:LX/7kY;

    invoke-static {v0}, LX/7kY;->a$redex0(LX/7kY;)V

    .line 1232074
    if-eqz p1, :cond_0

    .line 1232075
    iget-object v0, p0, LX/7kW;->b:LX/7kY;

    iget-object v1, v0, LX/7kY;->b:LX/7jB;

    sget-object v2, LX/7jD;->CREATE:LX/7jD;

    .line 1232076
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1232077
    check-cast v0, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;->a()Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    move-result-object v0

    iget-object v3, p0, LX/7kW;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 1232078
    iget-object p0, v3, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->m:Ljava/lang/String;

    move-object v3, p0

    .line 1232079
    invoke-static {v2, v0, v3}, LX/7jK;->a(LX/7jD;Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;Ljava/lang/String;)LX/7jK;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b7;)V

    .line 1232080
    :goto_0
    return-void

    .line 1232081
    :cond_0
    iget-object v0, p0, LX/7kW;->b:LX/7kY;

    iget-object v0, v0, LX/7kY;->b:LX/7jB;

    sget-object v1, LX/7jD;->CREATE:LX/7jD;

    iget-object v2, p0, LX/7kW;->a:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 1232082
    iget-object v3, v2, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->m:Ljava/lang/String;

    move-object v2, v3

    .line 1232083
    invoke-static {v1, v2}, LX/7jK;->a(LX/7jD;Ljava/lang/String;)LX/7jK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method
