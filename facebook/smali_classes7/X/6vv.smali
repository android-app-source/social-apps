.class public abstract LX/6vv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<SIMP",
        "LE_PICKER_RUN_TIME_DATA:Lcom/facebook/payments/picker/model/SimplePickerRunTimeData",
        "<TPICKER_SCREEN_CONFIG;TFETCHER_PARAMS;TCORE_C",
        "LIENT_DATA;",
        "TSECTION_TYPE;>;PICKER_SCREEN_CONFIG::",
        "Lcom/facebook/payments/picker/model/PickerScreenConfig;",
        "FETCHER_PARAMS::",
        "Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;",
        "CORE_C",
        "LIENT_DATA::Lcom/facebook/payments/picker/model/CoreClientData;",
        "SECTION_TYPE::",
        "LX/6vZ;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/payments/picker/PickerRunTimeDataMutator",
        "<TSIMP",
        "LE_PICKER_RUN_TIME_DATA;",
        "TPICKER_SCREEN_CONFIG;TFETCHER_PARAMS;TCORE_C",
        "LIENT_DATA;",
        "TSECTION_TYPE;>;"
    }
.end annotation


# instance fields
.field public a:LX/70m;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1157300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0P1;LX/6vZ;Ljava/lang/String;)LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<TSECTION_TYPE;",
            "Ljava/lang/String;",
            ">;TSECTION_TYPE;",
            "Ljava/lang/String;",
            ")",
            "LX/0P1",
            "<TSECTION_TYPE;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1157301
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1157302
    invoke-interface {v0, p0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1157303
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1157304
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a(Lcom/facebook/payments/picker/model/PickerScreenConfig;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPICKER_SCREEN_CONFIG;)TPICKER_RUN_TIME_DATA;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPICKER_SCREEN_CONFIG;TFETCHER_PARAMS;TCORE_C",
            "LIENT_DATA;",
            "LX/0P1",
            "<TSECTION_TYPE;",
            "Ljava/lang/String;",
            ">;)TPICKER_RUN_TIME_DATA;"
        }
    .end annotation
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;Lcom/facebook/payments/picker/model/CoreClientData;)V
    .locals 4

    .prologue
    .line 1157305
    check-cast p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;

    .line 1157306
    iget-object v0, p0, LX/6vv;->a:LX/70m;

    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v1

    .line 1157307
    iget-object v2, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    move-object v2, v2

    .line 1157308
    iget-object v3, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->d:LX/0P1;

    move-object v3, v3

    .line 1157309
    invoke-virtual {p0, v1, v2, p2, v3}, LX/6vv;->a(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)Lcom/facebook/payments/picker/model/PickerRunTimeData;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/70m;->b(Lcom/facebook/payments/picker/model/PickerRunTimeData;)V

    .line 1157310
    return-void
.end method

.method public final a(Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TSIMP",
            "LE_PICKER_RUN_TIME_DATA;",
            "TFETCHER_PARAMS;)V"
        }
    .end annotation

    .prologue
    .line 1157311
    iget-object v0, p0, LX/6vv;->a:LX/70m;

    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v1

    .line 1157312
    iget-object v2, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v2, v2

    .line 1157313
    iget-object v3, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->d:LX/0P1;

    move-object v3, v3

    .line 1157314
    invoke-virtual {p0, v1, p2, v2, v3}, LX/6vv;->a(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)Lcom/facebook/payments/picker/model/PickerRunTimeData;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/70m;->a(Lcom/facebook/payments/picker/model/PickerRunTimeData;)V

    .line 1157315
    return-void
.end method

.method public final a(Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;LX/6vZ;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TSIMP",
            "LE_PICKER_RUN_TIME_DATA;",
            "TFETCHER_PARAMS;TSECTION_TYPE;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1157316
    iget-object v0, p0, LX/6vv;->a:LX/70m;

    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v1

    .line 1157317
    iget-object v2, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v2, v2

    .line 1157318
    iget-object v3, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->d:LX/0P1;

    move-object v3, v3

    .line 1157319
    invoke-static {v3, p3, p4}, LX/6vv;->a(LX/0P1;LX/6vZ;Ljava/lang/String;)LX/0P1;

    move-result-object v3

    invoke-virtual {p0, v1, p2, v2, v3}, LX/6vv;->a(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)Lcom/facebook/payments/picker/model/PickerRunTimeData;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/70m;->a(Lcom/facebook/payments/picker/model/PickerRunTimeData;)V

    .line 1157320
    return-void
.end method
