.class public final enum LX/89U;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/89U;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/89U;

.field public static final enum PORTRAIT_4_3:LX/89U;

.field public static final enum SQUARE:LX/89U;


# instance fields
.field private final value:F


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1304970
    new-instance v0, LX/89U;

    const-string v1, "SQUARE"

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v3, v2}, LX/89U;-><init>(Ljava/lang/String;IF)V

    sput-object v0, LX/89U;->SQUARE:LX/89U;

    .line 1304971
    new-instance v0, LX/89U;

    const-string v1, "PORTRAIT_4_3"

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-direct {v0, v1, v4, v2}, LX/89U;-><init>(Ljava/lang/String;IF)V

    sput-object v0, LX/89U;->PORTRAIT_4_3:LX/89U;

    .line 1304972
    const/4 v0, 0x2

    new-array v0, v0, [LX/89U;

    sget-object v1, LX/89U;->SQUARE:LX/89U;

    aput-object v1, v0, v3

    sget-object v1, LX/89U;->PORTRAIT_4_3:LX/89U;

    aput-object v1, v0, v4

    sput-object v0, LX/89U;->$VALUES:[LX/89U;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IF)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)V"
        }
    .end annotation

    .prologue
    .line 1304973
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1304974
    iput p3, p0, LX/89U;->value:F

    .line 1304975
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/89U;
    .locals 1

    .prologue
    .line 1304976
    const-class v0, LX/89U;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/89U;

    return-object v0
.end method

.method public static values()[LX/89U;
    .locals 1

    .prologue
    .line 1304977
    sget-object v0, LX/89U;->$VALUES:[LX/89U;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/89U;

    return-object v0
.end method


# virtual methods
.method public final getValue()F
    .locals 1

    .prologue
    .line 1304978
    iget v0, p0, LX/89U;->value:F

    return v0
.end method
