.class public final LX/8Fc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8E8;


# instance fields
.field public final synthetic a:LX/8Fd;

.field public final synthetic b:LX/8Fe;


# direct methods
.method public constructor <init>(LX/8Fe;LX/8Fd;)V
    .locals 0

    .prologue
    .line 1318077
    iput-object p1, p0, LX/8Fc;->b:LX/8Fe;

    iput-object p2, p0, LX/8Fc;->a:LX/8Fd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1318076
    return-void
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 2
    .param p1    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1318068
    iget-object v0, p0, LX/8Fc;->b:LX/8Fe;

    iget-object v1, p0, LX/8Fc;->a:LX/8Fd;

    invoke-static {v0, p1, v1}, LX/8Fe;->a$redex0(LX/8Fe;Lcom/facebook/auth/viewercontext/ViewerContext;LX/8Fd;)V

    .line 1318069
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1318072
    iget-object v0, p0, LX/8Fc;->b:LX/8Fe;

    iget-object v0, v0, LX/8Fe;->e:LX/03V;

    const-class v1, LX/8Fe;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onViewerContextFetchFailed()"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1318073
    iget-object v0, p0, LX/8Fc;->a:LX/8Fd;

    if-eqz v0, :cond_0

    .line 1318074
    iget-object v0, p0, LX/8Fc;->a:LX/8Fd;

    invoke-interface {v0}, LX/8Fd;->a()V

    .line 1318075
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 2

    .prologue
    .line 1318070
    iget-object v0, p0, LX/8Fc;->b:LX/8Fe;

    iget-object v1, p0, LX/8Fc;->a:LX/8Fd;

    invoke-static {v0, p1, v1}, LX/8Fe;->a$redex0(LX/8Fe;Lcom/facebook/auth/viewercontext/ViewerContext;LX/8Fd;)V

    .line 1318071
    return-void
.end method
