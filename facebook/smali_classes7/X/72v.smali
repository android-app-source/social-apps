.class public final LX/72v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wC;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:LX/712;


# direct methods
.method public constructor <init>(LX/712;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1165013
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165014
    iput-object p1, p0, LX/72v;->a:LX/712;

    .line 1165015
    return-void
.end method


# virtual methods
.method public final a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1165016
    sget-object v0, LX/72u;->a:[I

    invoke-interface {p2}, LX/6vm;->a()LX/71I;

    move-result-object v1

    invoke-virtual {v1}, LX/71I;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1165017
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal row type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, LX/6vm;->a()LX/71I;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1165018
    :pswitch_0
    check-cast p2, LX/72s;

    .line 1165019
    if-nez p3, :cond_0

    new-instance p3, LX/72t;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/72t;-><init>(Landroid/content/Context;)V

    .line 1165020
    :goto_0
    invoke-virtual {p3, p1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1165021
    iput-object p2, p3, LX/72t;->d:LX/72s;

    .line 1165022
    iget-object p1, p3, LX/72t;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v0, p3, LX/72t;->d:LX/72s;

    iget-boolean v0, v0, LX/72s;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1165023
    iget-object v0, p3, LX/72t;->b:Landroid/widget/TextView;

    iget-object p1, p3, LX/72t;->d:LX/72s;

    iget-object p1, p1, LX/72s;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1165024
    move-object v0, p3

    .line 1165025
    :goto_2
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/72v;->a:LX/712;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/712;->a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_2

    .line 1165026
    :cond_0
    check-cast p3, LX/72t;

    goto :goto_0

    .line 1165027
    :cond_1
    const/16 v0, 0x8

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
