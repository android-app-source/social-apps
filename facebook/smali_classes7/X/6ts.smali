.class public final LX/6ts;
.super LX/6nc;
.source ""


# instance fields
.field public final synthetic a:LX/6tv;


# direct methods
.method public constructor <init>(LX/6tv;)V
    .locals 0

    .prologue
    .line 1155259
    iput-object p1, p0, LX/6ts;->a:LX/6tv;

    invoke-direct {p0}, LX/6nc;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/6qH;)V
    .locals 3

    .prologue
    .line 1155249
    sget-object v0, LX/6tu;->a:[I

    .line 1155250
    iget-object v1, p1, LX/6qH;->a:LX/6qJ;

    move-object v1, v1

    .line 1155251
    invoke-virtual {v1}, LX/6qJ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1155252
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected authResult "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1155253
    :pswitch_0
    iget-object v0, p0, LX/6ts;->a:LX/6tv;

    iget-object v0, v0, LX/6tv;->e:LX/6qd;

    iget-object v1, p0, LX/6ts;->a:LX/6tv;

    iget-object v1, v1, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-virtual {p1}, LX/6qH;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)V

    .line 1155254
    iget-object v0, p0, LX/6ts;->a:LX/6tv;

    invoke-virtual {v0}, LX/6tv;->c()V

    .line 1155255
    :goto_0
    return-void

    .line 1155256
    :pswitch_1
    iget-object v0, p0, LX/6ts;->a:LX/6tv;

    iget-object v0, v0, LX/6tv;->e:LX/6qd;

    iget-object v1, p0, LX/6ts;->a:LX/6tv;

    iget-object v1, v1, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-virtual {p1}, LX/6qH;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/6qd;->b(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)V

    .line 1155257
    iget-object v0, p0, LX/6ts;->a:LX/6tv;

    invoke-virtual {v0}, LX/6tv;->c()V

    goto :goto_0

    .line 1155258
    :pswitch_2
    iget-object v0, p0, LX/6ts;->a:LX/6tv;

    invoke-static {v0}, LX/6tv;->j(LX/6tv;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1155247
    iget-object v0, p0, LX/6ts;->a:LX/6tv;

    invoke-virtual {v0}, LX/6tv;->c()V

    .line 1155248
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1155245
    iget-object v0, p0, LX/6ts;->a:LX/6tv;

    invoke-virtual {v0}, LX/6tv;->b()V

    .line 1155246
    return-void
.end method
