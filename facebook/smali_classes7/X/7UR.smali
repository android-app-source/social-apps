.class public LX/7UR;
.super LX/7UQ;
.source ""


# instance fields
.field public a:Landroid/view/ScaleGestureDetector;

.field public b:Landroid/view/GestureDetector;

.field public c:I

.field public d:F

.field public e:F

.field public f:I

.field public g:Landroid/view/GestureDetector$OnGestureListener;

.field public h:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

.field public i:Z

.field public j:Z

.field public k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1212963
    invoke-direct {p0, p1, p2}, LX/7UQ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1212964
    iput-boolean v0, p0, LX/7UR;->i:Z

    .line 1212965
    iput-boolean v0, p0, LX/7UR;->j:Z

    .line 1212966
    iput-boolean v0, p0, LX/7UR;->k:Z

    .line 1212967
    return-void
.end method


# virtual methods
.method public a(FF)F
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1212956
    iget v0, p0, LX/7UR;->f:I

    if-ne v0, v1, :cond_1

    .line 1212957
    iget v0, p0, LX/7UR;->e:F

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    add-float/2addr v0, p1

    cmpg-float v0, v0, p2

    if-gtz v0, :cond_0

    .line 1212958
    iget v0, p0, LX/7UR;->e:F

    add-float p2, p1, v0

    .line 1212959
    :goto_0
    return p2

    .line 1212960
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, LX/7UR;->f:I

    goto :goto_0

    .line 1212961
    :cond_1
    iput v1, p0, LX/7UR;->f:I

    .line 1212962
    const/high16 p2, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1212947
    invoke-super {p0}, LX/7UQ;->a()V

    .line 1212948
    invoke-virtual {p0}, LX/7UR;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, LX/7UR;->c:I

    .line 1212949
    invoke-virtual {p0}, LX/7UR;->getGestureListener()Landroid/view/GestureDetector$OnGestureListener;

    move-result-object v0

    iput-object v0, p0, LX/7UR;->g:Landroid/view/GestureDetector$OnGestureListener;

    .line 1212950
    invoke-virtual {p0}, LX/7UR;->getScaleListener()Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    move-result-object v0

    iput-object v0, p0, LX/7UR;->h:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    .line 1212951
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, LX/7UR;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/7UR;->h:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, LX/7UR;->a:Landroid/view/ScaleGestureDetector;

    .line 1212952
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, LX/7UR;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/7UR;->g:Landroid/view/GestureDetector$OnGestureListener;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v0, p0, LX/7UR;->b:Landroid/view/GestureDetector;

    .line 1212953
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/7UR;->d:F

    .line 1212954
    iput v4, p0, LX/7UR;->f:I

    .line 1212955
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 1212917
    invoke-super {p0, p1}, LX/7UQ;->a(F)V

    .line 1212918
    iget-object v0, p0, LX/7UR;->a:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    iput p1, p0, LX/7UR;->d:F

    .line 1212919
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1212942
    invoke-super {p0, p1}, LX/7UQ;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1212943
    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 1212944
    iget-object v1, p0, LX/7UQ;->o:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1212945
    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p0, LX/7UR;->d:F

    .line 1212946
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;ZLandroid/graphics/Matrix;F)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/Matrix;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1212939
    invoke-super {p0, p1, p2, p3, p4}, LX/7UQ;->a(Landroid/graphics/drawable/Drawable;ZLandroid/graphics/Matrix;F)V

    .line 1212940
    invoke-virtual {p0}, LX/7UQ;->getMaxZoom()F

    move-result v0

    const/high16 v1, 0x40400000    # 3.0f

    div-float/2addr v0, v1

    iput v0, p0, LX/7UR;->e:F

    .line 1212941
    return-void
.end method

.method public a(I)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1212928
    invoke-virtual {p0}, LX/7UQ;->getBitmapRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 1212929
    iget-object v3, p0, LX/7UQ;->J:Landroid/graphics/RectF;

    invoke-virtual {p0, v2, v3}, LX/7UQ;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 1212930
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1212931
    invoke-virtual {p0, v3}, LX/7UR;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1212932
    iget v4, v2, Landroid/graphics/RectF;->right:F

    iget v5, v3, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_2

    .line 1212933
    if-gez p1, :cond_2

    .line 1212934
    iget v2, v2, Landroid/graphics/RectF;->right:F

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 1212935
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1212936
    goto :goto_0

    .line 1212937
    :cond_2
    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, LX/7UQ;->J:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v2, v2

    .line 1212938
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v2, v4

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public b(F)V
    .locals 2

    .prologue
    .line 1212923
    invoke-super {p0, p1}, LX/7UQ;->b(F)V

    .line 1212924
    iget-object v0, p0, LX/7UR;->a:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    iput p1, p0, LX/7UR;->d:F

    .line 1212925
    :cond_0
    invoke-virtual {p0}, LX/7UQ;->getMinZoom()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 1212926
    invoke-virtual {p0}, LX/7UQ;->getMinZoom()F

    move-result v0

    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {p0, v0, v1}, LX/7UQ;->b(FF)V

    .line 1212927
    :cond_1
    return-void
.end method

.method public final canScrollVertically(I)Z
    .locals 2

    .prologue
    .line 1212920
    iget v0, p0, LX/7UR;->d:F

    invoke-virtual {p0}, LX/7UQ;->getMinZoom()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 1212921
    const/4 v0, 0x0

    .line 1212922
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getGestureListener()Landroid/view/GestureDetector$OnGestureListener;
    .locals 1

    .prologue
    .line 1212916
    new-instance v0, LX/7UO;

    invoke-direct {v0, p0}, LX/7UO;-><init>(LX/7UR;)V

    return-object v0
.end method

.method public getScaleListener()Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
    .locals 1

    .prologue
    .line 1212915
    new-instance v0, LX/7UP;

    invoke-direct {v0, p0}, LX/7UP;-><init>(LX/7UR;)V

    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x2

    const v1, -0x79e8bd1d

    invoke-static {v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1212908
    iget-object v1, p0, LX/7UR;->a:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1212909
    iget-object v1, p0, LX/7UR;->a:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/7UR;->b:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1212910
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 1212911
    and-int/lit16 v1, v1, 0xff

    packed-switch v1, :pswitch_data_0

    .line 1212912
    :cond_1
    :goto_0
    const v1, -0x3566820b    # -5029626.5f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return v3

    .line 1212913
    :pswitch_0
    invoke-virtual {p0}, LX/7UQ;->getScale()F

    move-result v1

    invoke-virtual {p0}, LX/7UQ;->getMinZoom()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 1212914
    invoke-virtual {p0}, LX/7UQ;->getMinZoom()F

    move-result v1

    const/high16 v2, 0x42480000    # 50.0f

    invoke-virtual {p0, v1, v2}, LX/7UQ;->b(FF)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
