.class public final LX/7h1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/audience/model/Reply;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1225200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 8

    .prologue
    .line 1225201
    check-cast p1, Lcom/facebook/audience/model/Reply;

    check-cast p2, Lcom/facebook/audience/model/Reply;

    const-wide/16 v4, 0x0

    .line 1225202
    iget-wide v6, p1, Lcom/facebook/audience/model/Reply;->i:J

    move-wide v0, v6

    .line 1225203
    iget-wide v6, p2, Lcom/facebook/audience/model/Reply;->i:J

    move-wide v2, v6

    .line 1225204
    sub-long/2addr v0, v2

    .line 1225205
    cmp-long v2, v0, v4

    if-gez v2, :cond_0

    .line 1225206
    const/4 v0, -0x1

    .line 1225207
    :goto_0
    return v0

    .line 1225208
    :cond_0
    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 1225209
    const/4 v0, 0x1

    goto :goto_0

    .line 1225210
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
