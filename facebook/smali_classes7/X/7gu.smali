.class public final LX/7gu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:LX/7gw;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1225074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225075
    return-void
.end method

.method public constructor <init>(Lcom/facebook/audience/model/ReplyThreadConfig;)V
    .locals 1

    .prologue
    .line 1225076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225077
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1225078
    instance-of v0, p1, Lcom/facebook/audience/model/ReplyThreadConfig;

    if-eqz v0, :cond_0

    .line 1225079
    check-cast p1, Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 1225080
    iget-boolean v0, p1, Lcom/facebook/audience/model/ReplyThreadConfig;->a:Z

    iput-boolean v0, p0, LX/7gu;->a:Z

    .line 1225081
    iget-boolean v0, p1, Lcom/facebook/audience/model/ReplyThreadConfig;->b:Z

    iput-boolean v0, p0, LX/7gu;->b:Z

    .line 1225082
    iget-boolean v0, p1, Lcom/facebook/audience/model/ReplyThreadConfig;->c:Z

    iput-boolean v0, p0, LX/7gu;->c:Z

    .line 1225083
    iget-object v0, p1, Lcom/facebook/audience/model/ReplyThreadConfig;->d:LX/7gw;

    iput-object v0, p0, LX/7gu;->d:LX/7gw;

    .line 1225084
    :goto_0
    return-void

    .line 1225085
    :cond_0
    iget-boolean v0, p1, Lcom/facebook/audience/model/ReplyThreadConfig;->a:Z

    move v0, v0

    .line 1225086
    iput-boolean v0, p0, LX/7gu;->a:Z

    .line 1225087
    iget-boolean v0, p1, Lcom/facebook/audience/model/ReplyThreadConfig;->b:Z

    move v0, v0

    .line 1225088
    iput-boolean v0, p0, LX/7gu;->b:Z

    .line 1225089
    iget-boolean v0, p1, Lcom/facebook/audience/model/ReplyThreadConfig;->c:Z

    move v0, v0

    .line 1225090
    iput-boolean v0, p0, LX/7gu;->c:Z

    .line 1225091
    iget-object v0, p1, Lcom/facebook/audience/model/ReplyThreadConfig;->d:LX/7gw;

    move-object v0, v0

    .line 1225092
    iput-object v0, p0, LX/7gu;->d:LX/7gw;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/audience/model/ReplyThreadConfig;
    .locals 2

    .prologue
    .line 1225093
    new-instance v0, Lcom/facebook/audience/model/ReplyThreadConfig;

    invoke-direct {v0, p0}, Lcom/facebook/audience/model/ReplyThreadConfig;-><init>(LX/7gu;)V

    return-object v0
.end method
