.class public LX/8RL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8RK;


# instance fields
.field public a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/3fr;

.field public final c:LX/0Sh;

.field public final d:LX/2RQ;

.field private final e:LX/3Oq;


# direct methods
.method public constructor <init>(LX/3fr;LX/0Sh;LX/3Oq;LX/2RQ;)V
    .locals 1
    .param p3    # LX/3Oq;
        .annotation runtime Lcom/facebook/contacts/module/ContactLinkQueryType;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1344609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1344610
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/8RL;->a:Ljava/util/Set;

    .line 1344611
    iput-object p1, p0, LX/8RL;->b:LX/3fr;

    .line 1344612
    iput-object p2, p0, LX/8RL;->c:LX/0Sh;

    .line 1344613
    iput-object p3, p0, LX/8RL;->e:LX/3Oq;

    .line 1344614
    iput-object p4, p0, LX/8RL;->d:LX/2RQ;

    .line 1344615
    return-void
.end method

.method public static a(LX/0QB;)LX/8RL;
    .locals 1

    .prologue
    .line 1344616
    invoke-static {p0}, LX/8RL;->b(LX/0QB;)LX/8RL;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8RL;
    .locals 5

    .prologue
    .line 1344617
    new-instance v4, LX/8RL;

    invoke-static {p0}, LX/3fr;->a(LX/0QB;)LX/3fr;

    move-result-object v0

    check-cast v0, LX/3fr;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/6NS;->b(LX/0QB;)LX/3Oq;

    move-result-object v2

    check-cast v2, LX/3Oq;

    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v3

    check-cast v3, LX/2RQ;

    invoke-direct {v4, v0, v1, v2, v3}, LX/8RL;-><init>(LX/3fr;LX/0Sh;LX/3Oq;LX/2RQ;)V

    .line 1344618
    return-object v4
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1344619
    invoke-virtual {p0, p1}, LX/8RL;->b(Ljava/lang/String;)V

    .line 1344620
    return-void
.end method

.method public a(LX/8QL;)Z
    .locals 2

    .prologue
    .line 1344621
    instance-of v0, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    if-eqz v0, :cond_0

    .line 1344622
    iget-object v0, p0, LX/8RL;->a:Ljava/util/Set;

    check-cast p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1344623
    iget-object v1, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 1344624
    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 1344625
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1344626
    iget-object v0, p0, LX/8RL;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1344627
    iget-object v0, p0, LX/8RL;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1344628
    iget-object v0, p0, LX/8RL;->b:LX/3fr;

    iget-object v1, p0, LX/8RL;->d:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    .line 1344629
    iput-object p1, v1, LX/2RR;->e:Ljava/lang/String;

    .line 1344630
    move-object v1, v1

    .line 1344631
    iget-object v2, p0, LX/8RL;->e:LX/3Oq;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1344632
    iput-object v2, v1, LX/2RR;->c:Ljava/util/Collection;

    .line 1344633
    move-object v1, v1

    .line 1344634
    sget-object v2, LX/2RS;->NAME:LX/2RS;

    .line 1344635
    iput-object v2, v1, LX/2RR;->n:LX/2RS;

    .line 1344636
    move-object v1, v1

    .line 1344637
    invoke-virtual {v0, v1}, LX/3fr;->a(LX/2RR;)LX/6N1;

    move-result-object v1

    .line 1344638
    :goto_0
    :try_start_0
    invoke-interface {v1}, LX/6N1;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1344639
    iget-object v2, p0, LX/8RL;->a:Ljava/util/Set;

    invoke-interface {v1}, LX/6N1;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1344640
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/6N1;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, LX/6N1;->close()V

    .line 1344641
    return-void
.end method
