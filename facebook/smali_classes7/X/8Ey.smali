.class public final LX/8Ey;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1316416
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 1316417
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1316418
    :goto_0
    return v1

    .line 1316419
    :cond_0
    const-string v6, "can_viewer_promote_cta"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1316420
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    .line 1316421
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 1316422
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1316423
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1316424
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1316425
    const-string v6, "boosted_cta_promotions"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1316426
    const/4 v5, 0x0

    .line 1316427
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_a

    .line 1316428
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1316429
    :goto_2
    move v4, v5

    .line 1316430
    goto :goto_1

    .line 1316431
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1316432
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1316433
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1316434
    if-eqz v0, :cond_4

    .line 1316435
    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1316436
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1

    .line 1316437
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1316438
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_9

    .line 1316439
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1316440
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1316441
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_7

    if-eqz v6, :cond_7

    .line 1316442
    const-string v7, "nodes"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1316443
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1316444
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_8

    .line 1316445
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_8

    .line 1316446
    const/4 v7, 0x0

    .line 1316447
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v8, :cond_e

    .line 1316448
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1316449
    :goto_5
    move v6, v7

    .line 1316450
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1316451
    :cond_8
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1316452
    goto :goto_3

    .line 1316453
    :cond_9
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1316454
    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 1316455
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_2

    :cond_a
    move v4, v5

    goto :goto_3

    .line 1316456
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1316457
    :cond_c
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_d

    .line 1316458
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1316459
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1316460
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_c

    if-eqz v8, :cond_c

    .line 1316461
    const-string v9, "boosting_status"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1316462
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto :goto_6

    .line 1316463
    :cond_d
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1316464
    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 1316465
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_5

    :cond_e
    move v6, v7

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1316466
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1316467
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1316468
    if-eqz v0, :cond_3

    .line 1316469
    const-string v1, "boosted_cta_promotions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1316470
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1316471
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1316472
    if-eqz v1, :cond_2

    .line 1316473
    const-string v2, "nodes"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1316474
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1316475
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 1316476
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    const/4 p3, 0x0

    .line 1316477
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1316478
    invoke-virtual {p0, v3, p3}, LX/15i;->g(II)I

    move-result v0

    .line 1316479
    if-eqz v0, :cond_0

    .line 1316480
    const-string v0, "boosting_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1316481
    invoke-virtual {p0, v3, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1316482
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1316483
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1316484
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1316485
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1316486
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1316487
    if-eqz v0, :cond_4

    .line 1316488
    const-string v1, "can_viewer_promote_cta"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1316489
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1316490
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1316491
    return-void
.end method
