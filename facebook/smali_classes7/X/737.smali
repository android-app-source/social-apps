.class public LX/737;
.super LX/0QF;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0QF",
        "<",
        "Ljava/lang/String;",
        "LX/0Px",
        "<",
        "Lcom/facebook/payments/shipping/model/MailingAddress;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Sh;

.field private final b:LX/0QJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QJ",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0TD;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1165276
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/737;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/739;LX/0Sh;LX/0TD;LX/0Or;)V
    .locals 4
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/739;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0TD;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1165306
    invoke-direct {p0}, LX/0QF;-><init>()V

    .line 1165307
    iput-object p2, p0, LX/737;->a:LX/0Sh;

    .line 1165308
    iput-object p3, p0, LX/737;->c:LX/0TD;

    .line 1165309
    iput-object p4, p0, LX/737;->d:LX/0Or;

    .line 1165310
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0xa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    new-instance v1, LX/735;

    invoke-direct {v1, p0, p1}, LX/735;-><init>(LX/737;LX/739;)V

    invoke-virtual {v0, v1}, LX/0QN;->a(LX/0QM;)LX/0QJ;

    move-result-object v0

    iput-object v0, p0, LX/737;->b:LX/0QJ;

    .line 1165311
    return-void
.end method

.method public static a(LX/0QB;)LX/737;
    .locals 10

    .prologue
    .line 1165277
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1165278
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1165279
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1165280
    if-nez v1, :cond_0

    .line 1165281
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1165282
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1165283
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1165284
    sget-object v1, LX/737;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1165285
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1165286
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1165287
    :cond_1
    if-nez v1, :cond_4

    .line 1165288
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1165289
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1165290
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1165291
    new-instance v9, LX/737;

    invoke-static {v0}, LX/739;->b(LX/0QB;)LX/739;

    move-result-object v1

    check-cast v1, LX/739;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, LX/0TD;

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v9, v1, v7, v8, p0}, LX/737;-><init>(LX/739;LX/0Sh;LX/0TD;LX/0Or;)V

    .line 1165292
    move-object v1, v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1165293
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1165294
    if-nez v1, :cond_2

    .line 1165295
    sget-object v0, LX/737;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/737;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1165296
    :goto_1
    if-eqz v0, :cond_3

    .line 1165297
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1165298
    :goto_3
    check-cast v0, LX/737;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1165299
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1165300
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1165301
    :catchall_1
    move-exception v0

    .line 1165302
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1165303
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1165304
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1165305
    :cond_2
    :try_start_8
    sget-object v0, LX/737;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/737;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1165274
    iget-object v0, p0, LX/737;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1165275
    invoke-super {p0, p1}, LX/0QF;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1165312
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, LX/737;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()LX/0QI;
    .locals 1

    .prologue
    .line 1165273
    invoke-virtual {p0}, LX/737;->f()LX/0QJ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1165272
    invoke-virtual {p0}, LX/737;->f()LX/0QJ;

    move-result-object v0

    return-object v0
.end method

.method public final f()LX/0QJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QJ",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1165261
    iget-object v0, p0, LX/737;->b:LX/0QJ;

    return-object v0
.end method

.method public final g()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1165268
    iget-object v0, p0, LX/737;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1165269
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1165270
    invoke-virtual {p0, v0}, LX/0QG;->b(Ljava/lang/Object;)V

    .line 1165271
    invoke-virtual {p0}, LX/737;->h()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1165262
    iget-object v0, p0, LX/737;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1165263
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1165264
    invoke-virtual {p0, v1}, LX/0QG;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1165265
    if-eqz v0, :cond_0

    .line 1165266
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1165267
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/737;->c:LX/0TD;

    new-instance v2, LX/736;

    invoke-direct {v2, p0, v1}, LX/736;-><init>(LX/737;Ljava/lang/String;)V

    invoke-interface {v0, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
