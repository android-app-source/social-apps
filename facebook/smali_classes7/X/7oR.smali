.class public final LX/7oR;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1241022
    const-class v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;

    const v0, -0x6aef8844

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchSuggestionsForForCutType"

    const-string v6, "073ef95fc0a4b1a80f49d28495fe607e"

    const-string v7, "me"

    const-string v8, "10155207561586729"

    const-string v9, "10155259088381729"

    .line 1241023
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1241024
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1241025
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1241026
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1241027
    sparse-switch v0, :sswitch_data_0

    .line 1241028
    :goto_0
    return-object p1

    .line 1241029
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1241030
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1241031
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1241032
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1241033
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1241034
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1241035
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1241036
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1241037
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1241038
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1241039
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1241040
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1241041
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x41a91745 -> :sswitch_9
        -0x380757d0 -> :sswitch_8
        -0x4a314c6 -> :sswitch_4
        0x180aba4 -> :sswitch_b
        0x24b0df17 -> :sswitch_3
        0x291d8de0 -> :sswitch_c
        0x3052e0ff -> :sswitch_0
        0x4b46b5f1 -> :sswitch_1
        0x4c6d50cb -> :sswitch_5
        0x5f424068 -> :sswitch_a
        0x61bc9553 -> :sswitch_6
        0x6d2645b9 -> :sswitch_2
        0x73a026b5 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1241042
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1241043
    :goto_1
    return v0

    .line 1241044
    :pswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1241045
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
