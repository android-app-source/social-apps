.class public LX/8RF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8RE;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 1344330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1344331
    iput-boolean p1, p0, LX/8RF;->a:Z

    .line 1344332
    return-void
.end method


# virtual methods
.method public a()LX/03R;
    .locals 1

    .prologue
    .line 1344417
    sget-object v0, LX/03R;->UNSET:LX/03R;

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1344416
    iget-boolean v0, p0, LX/8RF;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->a()Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;LX/621;)V
    .locals 1

    .prologue
    .line 1344411
    check-cast p1, LX/8vJ;

    .line 1344412
    iget-object v0, p1, LX/8vJ;->b:Landroid/widget/TextView;

    invoke-interface {p2}, LX/621;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1344413
    invoke-interface {p2}, LX/621;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, LX/8vJ;->setVisibility(I)V

    .line 1344414
    return-void

    .line 1344415
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public a(Landroid/view/View;LX/8QL;Z)V
    .locals 5

    .prologue
    .line 1344372
    check-cast p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;

    const/4 v0, 0x1

    const/16 p0, 0x8

    const/4 v1, 0x0

    .line 1344373
    invoke-virtual {p2}, LX/8QK;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1344374
    iget-object v2, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->h:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1344375
    invoke-virtual {p1, v1}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->setEnabled(Z)V

    .line 1344376
    :goto_0
    iget-object v2, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->b:Landroid/widget/TextView;

    invoke-virtual {p2}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1344377
    invoke-virtual {p2}, LX/8QL;->j()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1344378
    iget-object v2, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->e:Landroid/widget/ToggleButton;

    if-nez p3, :cond_0

    invoke-virtual {p2}, LX/8QK;->a()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_0
    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 1344379
    :goto_2
    goto :goto_3

    .line 1344380
    :goto_3
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->g:Landroid/widget/ImageView;

    new-instance v2, LX/8vL;

    invoke-direct {v2, p1, p2}, LX/8vL;-><init>(Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;LX/8QL;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1344381
    invoke-virtual {p2}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1344382
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1344383
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1344384
    :goto_4
    invoke-virtual {p2}, LX/8QL;->e()I

    move-result v0

    if-lez v0, :cond_6

    .line 1344385
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p2}, LX/8QL;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1344386
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1344387
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1344388
    :goto_5
    invoke-virtual {p2}, LX/8QL;->f()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1344389
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p2}, LX/8QL;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1344390
    :cond_1
    iget-object v0, p2, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-ne v0, v1, :cond_a

    .line 1344391
    const-string v0, "TAG_EXPANSION_VIEW"

    invoke-virtual {p1, v0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->setTag(Ljava/lang/Object;)V

    .line 1344392
    :goto_6
    return-void

    .line 1344393
    :cond_2
    iget-object v2, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->h:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1344394
    invoke-virtual {p1, v0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 1344395
    goto :goto_1

    .line 1344396
    :cond_4
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setVisibility(I)V

    goto :goto_2

    .line 1344397
    :cond_5
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1344398
    goto :goto_4

    .line 1344399
    :cond_6
    invoke-virtual {p2}, LX/8QL;->g()I

    move-result v0

    if-gtz v0, :cond_7

    invoke-virtual {p2}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    .line 1344400
    :cond_7
    invoke-virtual {p2}, LX/8QL;->g()I

    move-result v0

    if-lez v0, :cond_8

    .line 1344401
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p2}, LX/8QL;->g()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1344402
    :goto_7
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1344403
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_5

    .line 1344404
    :cond_8
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->c:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1344405
    const/4 v4, 0x0

    move-object v4, v4

    .line 1344406
    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_7

    .line 1344407
    :cond_9
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p2}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1344408
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1344409
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto/16 :goto_5

    .line 1344410
    :cond_a
    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->setTag(Ljava/lang/Object;)V

    goto :goto_6
.end method

.method public final a(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1344368
    instance-of v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;

    if-nez v0, :cond_0

    instance-of v0, p1, LX/8vJ;

    if-eqz v0, :cond_1

    .line 1344369
    :cond_0
    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "Cannot convert an item row to a view more row"

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1344370
    :cond_1
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1344371
    return-void
.end method

.method public b(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1344367
    new-instance v0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public b(Landroid/view/View;LX/8QL;Z)V
    .locals 5

    .prologue
    .line 1344342
    check-cast p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;

    const/16 p0, 0x8

    const/4 v1, 0x0

    .line 1344343
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->b:Landroid/widget/TextView;

    invoke-virtual {p2}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1344344
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->c:Landroid/widget/TextView;

    invoke-virtual {p2}, LX/8QL;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1344345
    invoke-virtual {p2}, LX/8QL;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1344346
    iget-object v2, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->e:Landroid/widget/ToggleButton;

    if-nez p3, :cond_0

    invoke-virtual {p2}, LX/8QK;->a()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 1344347
    :goto_1
    invoke-virtual {p2}, LX/8QL;->e()I

    move-result v0

    if-lez v0, :cond_4

    .line 1344348
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p2}, LX/8QL;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1344349
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1344350
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1344351
    :goto_2
    invoke-virtual {p2}, LX/8QL;->f()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1344352
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p2}, LX/8QL;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1344353
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 1344354
    goto :goto_0

    .line 1344355
    :cond_3
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setVisibility(I)V

    goto :goto_1

    .line 1344356
    :cond_4
    invoke-virtual {p2}, LX/8QL;->g()I

    move-result v0

    if-gtz v0, :cond_5

    invoke-virtual {p2}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    .line 1344357
    :cond_5
    invoke-virtual {p2}, LX/8QL;->g()I

    move-result v0

    if-lez v0, :cond_6

    .line 1344358
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p2}, LX/8QL;->g()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1344359
    :goto_3
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1344360
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_2

    .line 1344361
    :cond_6
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->d:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1344362
    const/4 v4, 0x0

    move-object v4, v4

    .line 1344363
    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 1344364
    :cond_7
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p2}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1344365
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1344366
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_2
.end method

.method public c(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1344340
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1344341
    iget-boolean v1, p0, LX/8RF;->a:Z

    if-eqz v1, :cond_0

    const v1, 0x7f031532

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v1, 0x7f031531

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public d(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1344335
    iget-boolean v0, p0, LX/8RF;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, LX/8vJ;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8vJ;-><init>(Landroid/content/Context;)V

    .line 1344336
    const v1, 0x7f0d07fd

    invoke-virtual {v0, v1}, LX/8vJ;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1344337
    const v1, 0x7f0d0c4d

    invoke-virtual {v0, v1}, LX/8vJ;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1344338
    move-object v0, v0

    .line 1344339
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/8vJ;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8vJ;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final e(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1344333
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1344334
    const v1, 0x7f031530

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
