.class public final LX/7Fk;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1188091
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1188092
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1188093
    :goto_0
    return v1

    .line 1188094
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1188095
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1188096
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1188097
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1188098
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1188099
    const-string v4, "buckets"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1188100
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1188101
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 1188102
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 1188103
    invoke-static {p0, p1}, LX/7Fj;->b(LX/15w;LX/186;)I

    move-result v3

    .line 1188104
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1188105
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1188106
    goto :goto_1

    .line 1188107
    :cond_3
    const-string v4, "control_node"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1188108
    invoke-static {p0, p1}, LX/7Fh;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1188109
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1188110
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1188111
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1188112
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1188076
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1188077
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1188078
    if-eqz v0, :cond_1

    .line 1188079
    const-string v1, "buckets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188080
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1188081
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1188082
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/7Fj;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1188083
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1188084
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1188085
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1188086
    if-eqz v0, :cond_2

    .line 1188087
    const-string v1, "control_node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188088
    invoke-static {p0, v0, p2, p3}, LX/7Fh;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1188089
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1188090
    return-void
.end method
