.class public LX/6kC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final leftParticipantFbId:Ljava/lang/Long;

.field public final messageMetadata:LX/6kn;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1135321
    new-instance v0, LX/1sv;

    const-string v1, "DeltaParticipantLeftGroupThread"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kC;->b:LX/1sv;

    .line 1135322
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kC;->c:LX/1sw;

    .line 1135323
    new-instance v0, LX/1sw;

    const-string v1, "leftParticipantFbId"

    const/16 v2, 0xa

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kC;->d:LX/1sw;

    .line 1135324
    sput-boolean v4, LX/6kC;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6kn;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1135317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1135318
    iput-object p1, p0, LX/6kC;->messageMetadata:LX/6kn;

    .line 1135319
    iput-object p2, p0, LX/6kC;->leftParticipantFbId:Ljava/lang/Long;

    .line 1135320
    return-void
.end method

.method public static a(LX/6kC;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1135312
    iget-object v0, p0, LX/6kC;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1135313
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kC;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1135314
    :cond_0
    iget-object v0, p0, LX/6kC;->leftParticipantFbId:Ljava/lang/Long;

    if-nez v0, :cond_1

    .line 1135315
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'leftParticipantFbId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kC;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1135316
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1135325
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1135326
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1135327
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1135328
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaParticipantLeftGroupThread"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1135329
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135330
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135331
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135332
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135333
    const-string v4, "messageMetadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135334
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135335
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135336
    iget-object v4, p0, LX/6kC;->messageMetadata:LX/6kn;

    if-nez v4, :cond_3

    .line 1135337
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135338
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135339
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135340
    const-string v4, "leftParticipantFbId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135341
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135342
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135343
    iget-object v0, p0, LX/6kC;->leftParticipantFbId:Ljava/lang/Long;

    if-nez v0, :cond_4

    .line 1135344
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135345
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135346
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135347
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1135348
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1135349
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1135350
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1135351
    :cond_3
    iget-object v4, p0, LX/6kC;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1135352
    :cond_4
    iget-object v0, p0, LX/6kC;->leftParticipantFbId:Ljava/lang/Long;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1135301
    invoke-static {p0}, LX/6kC;->a(LX/6kC;)V

    .line 1135302
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1135303
    iget-object v0, p0, LX/6kC;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1135304
    sget-object v0, LX/6kC;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135305
    iget-object v0, p0, LX/6kC;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1135306
    :cond_0
    iget-object v0, p0, LX/6kC;->leftParticipantFbId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1135307
    sget-object v0, LX/6kC;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135308
    iget-object v0, p0, LX/6kC;->leftParticipantFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1135309
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1135310
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1135311
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1135279
    if-nez p1, :cond_1

    .line 1135280
    :cond_0
    :goto_0
    return v0

    .line 1135281
    :cond_1
    instance-of v1, p1, LX/6kC;

    if-eqz v1, :cond_0

    .line 1135282
    check-cast p1, LX/6kC;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1135283
    if-nez p1, :cond_3

    .line 1135284
    :cond_2
    :goto_1
    move v0, v2

    .line 1135285
    goto :goto_0

    .line 1135286
    :cond_3
    iget-object v0, p0, LX/6kC;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1135287
    :goto_2
    iget-object v3, p1, LX/6kC;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1135288
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1135289
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135290
    iget-object v0, p0, LX/6kC;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6kC;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1135291
    :cond_5
    iget-object v0, p0, LX/6kC;->leftParticipantFbId:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1135292
    :goto_4
    iget-object v3, p1, LX/6kC;->leftParticipantFbId:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1135293
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1135294
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135295
    iget-object v0, p0, LX/6kC;->leftParticipantFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/6kC;->leftParticipantFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1135296
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1135297
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1135298
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1135299
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1135300
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1135278
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1135275
    sget-boolean v0, LX/6kC;->a:Z

    .line 1135276
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kC;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1135277
    return-object v0
.end method
