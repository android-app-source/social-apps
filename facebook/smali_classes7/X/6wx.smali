.class public final LX/6wx;
.super LX/6oO;
.source ""


# instance fields
.field public final synthetic a:LX/6wz;


# direct methods
.method public constructor <init>(LX/6wz;)V
    .locals 0

    .prologue
    .line 1158212
    iput-object p1, p0, LX/6wx;->a:LX/6wz;

    invoke-direct {p0}, LX/6oO;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 9

    .prologue
    .line 1158213
    iget-object v0, p0, LX/6wx;->a:LX/6wz;

    invoke-virtual {v0}, LX/6wz;->b()Z

    move-result v0

    .line 1158214
    iget-object v1, p0, LX/6wx;->a:LX/6wz;

    iget-object v1, v1, LX/6wz;->e:LX/6x9;

    invoke-virtual {v1, v0}, LX/6x9;->a(Z)V

    .line 1158215
    iget-object v1, p0, LX/6wx;->a:LX/6wz;

    iget-object v1, v1, LX/6wz;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1, v0}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 1158216
    iget-object v1, p0, LX/6wx;->a:LX/6wz;

    iget-object v1, v1, LX/6wz;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v2, p0, LX/6wx;->a:LX/6wz;

    const/4 p1, 0x1

    const/4 p0, 0x0

    .line 1158217
    if-nez v0, :cond_0

    iget-object v3, v2, LX/6wz;->h:Lcom/facebook/payments/currency/CurrencyAmount;

    if-nez v3, :cond_1

    iget-object v3, v2, LX/6wz;->g:Lcom/facebook/payments/currency/CurrencyAmount;

    if-nez v3, :cond_1

    .line 1158218
    :cond_0
    const/4 v3, 0x0

    .line 1158219
    :goto_0
    move-object v0, v3

    .line 1158220
    invoke-virtual {v1, v0}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 1158221
    return-void

    .line 1158222
    :cond_1
    iget-object v3, v2, LX/6wz;->h:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v3, :cond_2

    iget-object v3, v2, LX/6wz;->g:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v3, :cond_2

    .line 1158223
    iget-object v3, v2, LX/6wz;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081e46

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v2, LX/6wz;->b:LX/5fv;

    iget-object v7, v2, LX/6wz;->h:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v8, v2, LX/6wz;->j:LX/5fu;

    invoke-virtual {v6, v7, v8}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, p0

    iget-object v6, v2, LX/6wz;->b:LX/5fv;

    iget-object v7, v2, LX/6wz;->g:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v8, v2, LX/6wz;->j:LX/5fu;

    invoke-virtual {v6, v7, v8}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, p1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 1158224
    :cond_2
    iget-object v3, v2, LX/6wz;->h:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v3, :cond_3

    .line 1158225
    iget-object v3, v2, LX/6wz;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081e47

    new-array v5, p1, [Ljava/lang/Object;

    iget-object v6, v2, LX/6wz;->b:LX/5fv;

    iget-object v7, v2, LX/6wz;->h:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v8, v2, LX/6wz;->j:LX/5fu;

    invoke-virtual {v6, v7, v8}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, p0

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 1158226
    :cond_3
    iget-object v3, v2, LX/6wz;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081e48

    new-array v5, p1, [Ljava/lang/Object;

    iget-object v6, v2, LX/6wz;->b:LX/5fv;

    iget-object v7, v2, LX/6wz;->g:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v8, v2, LX/6wz;->j:LX/5fu;

    invoke-virtual {v6, v7, v8}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, p0

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method
