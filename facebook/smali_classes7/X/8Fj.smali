.class public LX/8Fj;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1318167
    const/16 v0, 0x8

    new-array v0, v0, [F

    sput-object v0, LX/8Fj;->a:[F

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1318168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318169
    return-void
.end method

.method public static a(F)F
    .locals 1

    .prologue
    .line 1318170
    const/high16 v0, -0x40800000    # -1.0f

    invoke-static {p0, v0}, LX/8Fj;->a(FF)F

    move-result v0

    return v0
.end method

.method public static a(FF)F
    .locals 1

    .prologue
    .line 1318171
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    mul-float/2addr v0, p0

    add-float/2addr v0, p1

    return v0
.end method
