.class public final LX/7Lc;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/player/VideoController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/VideoController;)V
    .locals 1

    .prologue
    .line 1198197
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1198198
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/7Lc;->a:Ljava/lang/ref/WeakReference;

    .line 1198199
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 1198200
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1198201
    :cond_0
    :goto_0
    return-void

    .line 1198202
    :pswitch_0
    iget-object v0, p0, LX/7Lc;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/VideoController;

    .line 1198203
    if-eqz v0, :cond_0

    .line 1198204
    invoke-virtual {v0}, Lcom/facebook/video/player/VideoController;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
