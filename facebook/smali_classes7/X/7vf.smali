.class public final LX/7vf;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderStatusSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7vr;

.field public final synthetic b:LX/7vi;


# direct methods
.method public constructor <init>(LX/7vi;LX/7vr;)V
    .locals 0

    .prologue
    .line 1274962
    iput-object p1, p0, LX/7vf;->b:LX/7vi;

    iput-object p2, p0, LX/7vf;->a:LX/7vr;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1274963
    iget-object v0, p0, LX/7vf;->b:LX/7vi;

    iget-object v0, v0, LX/7vi;->e:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/events/tickets/EventBuyTicketsPollingGraphQLMutator$3$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/events/tickets/EventBuyTicketsPollingGraphQLMutator$3$2;-><init>(LX/7vf;Ljava/lang/Throwable;)V

    const v2, -0x6e78558f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1274964
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1274965
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderStatusSubscriptionModel;

    .line 1274966
    iget-object v0, p0, LX/7vf;->b:LX/7vi;

    iget-object v0, v0, LX/7vi;->e:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/events/tickets/EventBuyTicketsPollingGraphQLMutator$3$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/events/tickets/EventBuyTicketsPollingGraphQLMutator$3$1;-><init>(LX/7vf;Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderStatusSubscriptionModel;)V

    const v2, -0x4d2a3f66

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1274967
    return-void
.end method
