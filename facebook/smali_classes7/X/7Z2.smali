.class public final LX/7Z2;
.super LX/7Z1;
.source ""


# instance fields
.field public final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/7Z3;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(LX/7Z3;)V
    .locals 2

    invoke-direct {p0}, LX/7Z1;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/7Z2;->a:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p1, LX/2wI;->j:Landroid/os/Looper;

    move-object v1, v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/7Z2;->b:Landroid/os/Handler;

    return-void
.end method

.method private static a(LX/7Z3;JI)V
    .locals 3

    iget-object v1, p0, LX/7Z3;->w:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/7Z3;->w:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wh;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v1, p3}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, LX/2wh;->a(Ljava/lang/Object;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a(LX/7Z3;I)Z
    .locals 3

    sget-object v1, LX/7Z3;->A:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/7Z3;->y:LX/2wh;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7Z3;->y:LX/2wh;

    new-instance v2, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v2, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v2}, LX/2wh;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, LX/7Z3;->y:LX/2wh;

    const/4 v0, 0x1

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    monitor-exit v1

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()LX/7Z3;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, LX/7Z2;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Z3;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/7Z3;->z(LX/7Z3;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 6

    invoke-virtual {p0}, LX/7Z2;->a()LX/7Z3;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, LX/7Z3;->d:LX/7Z9;

    const-string v2, "ICastDeviceControllerListener.onDisconnected: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, LX/7Z9;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/2wI;->b(I)V

    goto :goto_0
.end method

.method public final a(J)V
    .locals 3

    iget-object v0, p0, LX/7Z2;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Z3;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, LX/7Z2;->a(LX/7Z3;JI)V

    goto :goto_0
.end method

.method public final a(JI)V
    .locals 1

    iget-object v0, p0, LX/7Z2;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Z3;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v0, p1, p2, p3}, LX/7Z2;->a(LX/7Z3;JI)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9

    iget-object v0, p0, LX/7Z2;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/7Z3;

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, v6, LX/7Z3;->e:Lcom/google/android/gms/cast/ApplicationMetadata;

    iget-object v0, p1, Lcom/google/android/gms/cast/ApplicationMetadata;->a:Ljava/lang/String;

    move-object v0, v0

    iput-object v0, v6, LX/7Z3;->t:Ljava/lang/String;

    iput-object p3, v6, LX/7Z3;->u:Ljava/lang/String;

    iput-object p2, v6, LX/7Z3;->k:Ljava/lang/String;

    sget-object v7, LX/7Z3;->z:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-object v0, v6, LX/7Z3;->x:LX/2wh;

    if-eqz v0, :cond_1

    iget-object v8, v6, LX/7Z3;->x:LX/2wh;

    new-instance v0, LX/7Yz;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/7Yz;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v8, v0}, LX/2wh;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, v6, LX/7Z3;->x:LX/2wh;

    :cond_1
    monitor-exit v7

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/cast/internal/ApplicationStatus;)V
    .locals 4

    iget-object v0, p0, LX/7Z2;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Z3;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, LX/7Z3;->d:LX/7Z9;

    const-string v2, "onApplicationStatusChanged"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, LX/7Z9;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, LX/7Z2;->b:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/cast/internal/zze$zzb$3;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/gms/cast/internal/zze$zzb$3;-><init>(LX/7Z2;LX/7Z3;Lcom/google/android/gms/cast/internal/ApplicationStatus;)V

    const v0, 0x5dc1ec10

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/cast/internal/DeviceStatus;)V
    .locals 4

    iget-object v0, p0, LX/7Z2;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Z3;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, LX/7Z3;->d:LX/7Z9;

    const-string v2, "onDeviceStatusChanged"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, LX/7Z9;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, LX/7Z2;->b:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/cast/internal/zze$zzb$2;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/gms/cast/internal/zze$zzb$2;-><init>(LX/7Z2;LX/7Z3;Lcom/google/android/gms/cast/internal/DeviceStatus;)V

    const v0, -0x790844f

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, LX/7Z2;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Z3;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, LX/7Z3;->d:LX/7Z9;

    const-string v2, "Receive (type=text, ns=%s) %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, LX/7Z9;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, LX/7Z2;->b:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/cast/internal/zze$zzb$4;

    invoke-direct {v2, p0, v0, p1, p2}, Lcom/google/android/gms/cast/internal/zze$zzb$4;-><init>(LX/7Z2;LX/7Z3;Ljava/lang/String;Ljava/lang/String;)V

    const v0, -0x1cb41ede

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 5

    iget-object v0, p0, LX/7Z2;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Z3;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, LX/7Z3;->d:LX/7Z9;

    const-string v1, "IGNORING: Receive (type=binary, ns=%s) <%d bytes>"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    array-length v4, p2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, LX/7Z9;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 5

    iget-object v0, p0, LX/7Z2;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Z3;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, LX/7Z3;->z:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, LX/7Z3;->x:LX/2wh;

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/7Z3;->x:LX/2wh;

    new-instance v3, LX/7Yz;

    new-instance v4, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v4, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-direct {v3, v4}, LX/7Yz;-><init>(Lcom/google/android/gms/common/api/Status;)V

    invoke-interface {v2, v3}, LX/2wh;->a(Ljava/lang/Object;)V

    const/4 v2, 0x0

    iput-object v2, v0, LX/7Z3;->x:LX/2wh;

    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 3

    sget-object v0, LX/7Z3;->d:LX/7Z9;

    const-string v1, "Deprecated callback: \"onStatusreceived\""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/7Z9;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final c(I)V
    .locals 1

    iget-object v0, p0, LX/7Z2;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Z3;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v0, p1}, LX/7Z2;->a(LX/7Z3;I)Z

    goto :goto_0
.end method

.method public final d(I)V
    .locals 1

    iget-object v0, p0, LX/7Z2;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Z3;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v0, p1}, LX/7Z2;->a(LX/7Z3;I)Z

    goto :goto_0
.end method

.method public final e(I)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, LX/7Z2;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Z3;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v1, v0, LX/7Z3;->t:Ljava/lang/String;

    iput-object v1, v0, LX/7Z3;->u:Ljava/lang/String;

    invoke-static {v0, p1}, LX/7Z2;->a(LX/7Z3;I)Z

    iget-object v1, v0, LX/7Z3;->g:LX/38Y;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/7Z2;->b:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/cast/internal/zze$zzb$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/gms/cast/internal/zze$zzb$1;-><init>(LX/7Z2;LX/7Z3;I)V

    const v0, 0x108f6d8c

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
