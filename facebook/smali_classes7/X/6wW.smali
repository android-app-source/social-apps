.class public LX/6wW;
.super LX/6sV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sV",
        "<",
        "Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;",
        "Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/0Or;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/common/PaymentNetworkOperationHelper;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1157699
    const-class v0, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;

    invoke-direct {p0, p1, v0}, LX/6sV;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 1157700
    iput-object p2, p0, LX/6wW;->c:LX/0Or;

    .line 1157701
    return-void
.end method

.method public static b(LX/0QB;)LX/6wW;
    .locals 3

    .prologue
    .line 1157702
    new-instance v1, LX/6wW;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    const/16 v2, 0x12cb

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/6wW;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/0Or;)V

    .line 1157703
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1157704
    check-cast p1, Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;

    .line 1157705
    iget-object v0, p0, LX/6wW;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1157706
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1157707
    iget-object v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    if-eqz v0, :cond_0

    .line 1157708
    iget-object v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    check-cast v0, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;

    .line 1157709
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "user_input_email"

    iget-object v0, v0, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1157710
    :cond_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "default"

    iget-boolean v0, p1, Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "1"

    :goto_0
    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1157711
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v2, "add_email_contact_info"

    .line 1157712
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 1157713
    move-object v0, v0

    .line 1157714
    const-string v2, "POST"

    .line 1157715
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 1157716
    move-object v2, v0

    .line 1157717
    const-string v3, "%d/payment_account_emails"

    iget-object v0, p0, LX/6wW;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1157718
    iget-object v4, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v4

    .line 1157719
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1157720
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 1157721
    move-object v0, v2

    .line 1157722
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1157723
    move-object v0, v0

    .line 1157724
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 1157725
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1157726
    move-object v0, v0

    .line 1157727
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1157728
    :cond_1
    const-string v0, "0"

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157729
    const-string v0, "add_email_contact_info"

    return-object v0
.end method
