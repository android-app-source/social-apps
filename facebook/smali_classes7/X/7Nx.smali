.class public final LX/7Nx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V
    .locals 0

    .prologue
    .line 1200808
    iput-object p1, p0, LX/7Nx;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2

    .prologue
    .line 1200804
    if-nez p3, :cond_0

    .line 1200805
    :goto_0
    return-void

    .line 1200806
    :cond_0
    iget-object v0, p0, LX/7Nx;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->H:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getMax()I

    move-result v0

    .line 1200807
    iget-object v1, p0, LX/7Nx;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    iget v1, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->L:I

    mul-int/2addr v1, p2

    div-int v0, v1, v0

    iput v0, p0, LX/7Nx;->b:I

    goto :goto_0
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 1200793
    iget-object v0, p0, LX/7Nx;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    const/4 v1, 0x1

    .line 1200794
    iput-boolean v1, v0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->O:Z

    .line 1200795
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 5

    .prologue
    .line 1200796
    iget-object v0, p0, LX/7Nx;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->r:LX/7J3;

    const-string v1, "seek"

    iget v2, p0, LX/7Nx;->b:I

    invoke-virtual {v0, v1, v2}, LX/7J3;->b(Ljava/lang/String;I)V

    .line 1200797
    iget-object v0, p0, LX/7Nx;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    iget v1, p0, LX/7Nx;->b:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/37Y;->a(J)V

    .line 1200798
    iget-object v0, p0, LX/7Nx;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    iget v1, p0, LX/7Nx;->b:I

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->b(I)V

    .line 1200799
    :try_start_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 1200800
    new-instance v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment$SeekBarListener$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment$SeekBarListener$1;-><init>(LX/7Nx;)V

    const-wide/16 v2, 0x1f4

    const v4, 0x1bd51034

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1200801
    :goto_0
    return-void

    .line 1200802
    :catch_0
    move-exception v0

    .line 1200803
    iget-object v1, p0, LX/7Nx;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->v:LX/37e;

    sget-object v2, LX/7JJ;->VideoCastControllerFragment_OnStopTrackingTouch:LX/7JJ;

    invoke-virtual {v1, v2, v0}, LX/37e;->a(LX/7JJ;Ljava/lang/Exception;)V

    goto :goto_0
.end method
