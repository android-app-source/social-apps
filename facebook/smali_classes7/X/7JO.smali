.class public LX/7JO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1193531
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/2pa;LX/04D;)LX/7JN;
    .locals 7

    .prologue
    .line 1193541
    invoke-static {p0}, LX/7JO;->a(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1193542
    const/4 v1, 0x0

    .line 1193543
    if-nez v0, :cond_4

    .line 1193544
    :cond_0
    :goto_0
    move-object v1, v1

    .line 1193545
    const/4 v3, 0x0

    .line 1193546
    if-nez v0, :cond_5

    .line 1193547
    :cond_1
    :goto_1
    move-object v0, v3

    .line 1193548
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 1193549
    :cond_2
    invoke-static {}, LX/7JM;->newBuilder()LX/7JM;

    move-result-object v2

    .line 1193550
    const/4 v4, 0x0

    .line 1193551
    iget-object v3, p0, LX/2pa;->b:LX/0P1;

    const-string v5, "BlurredCoverImageParamsKey"

    invoke-virtual {v3, v5}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1193552
    iget-object v3, p0, LX/2pa;->b:LX/0P1;

    const-string v5, "BlurredCoverImageParamsKey"

    invoke-virtual {v3, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 1193553
    instance-of v5, v3, LX/1bf;

    if-eqz v5, :cond_a

    .line 1193554
    check-cast v3, LX/1bf;

    .line 1193555
    :goto_2
    move-object v3, v3

    .line 1193556
    iput-object v3, v2, LX/7JM;->l:LX/1bf;

    .line 1193557
    move-object v2, v2

    .line 1193558
    const/4 v4, 0x0

    .line 1193559
    iget-object v3, p0, LX/2pa;->b:LX/0P1;

    const-string v5, "CoverImageParamsKey"

    invoke-virtual {v3, v5}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1193560
    iget-object v3, p0, LX/2pa;->b:LX/0P1;

    const-string v5, "CoverImageParamsKey"

    invoke-virtual {v3, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 1193561
    instance-of v5, v3, LX/1bf;

    if-eqz v5, :cond_b

    .line 1193562
    check-cast v3, LX/1bf;

    .line 1193563
    :goto_3
    move-object v3, v3

    .line 1193564
    iput-object v3, v2, LX/7JM;->k:LX/1bf;

    .line 1193565
    move-object v2, v2

    .line 1193566
    iget-object v3, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1193567
    iput v3, v2, LX/7JM;->f:I

    .line 1193568
    move-object v2, v2

    .line 1193569
    iget-object v3, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->g:Z

    .line 1193570
    iput-boolean v3, v2, LX/7JM;->i:Z

    .line 1193571
    move-object v2, v2

    .line 1193572
    invoke-virtual {p0}, LX/2pa;->a()Z

    move-result v3

    .line 1193573
    iput-boolean v3, v2, LX/7JM;->h:Z

    .line 1193574
    move-object v2, v2

    .line 1193575
    iget-object v3, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    .line 1193576
    iput-boolean v3, v2, LX/7JM;->b:Z

    .line 1193577
    move-object v2, v2

    .line 1193578
    iput-object p1, v2, LX/7JM;->m:LX/04D;

    .line 1193579
    move-object v2, v2

    .line 1193580
    const/4 v3, -0x1

    .line 1193581
    iput v3, v2, LX/7JM;->g:I

    .line 1193582
    move-object v2, v2

    .line 1193583
    iget-object v3, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    .line 1193584
    iput-object v3, v2, LX/7JM;->e:LX/162;

    .line 1193585
    move-object v2, v2

    .line 1193586
    if-eqz v1, :cond_3

    .line 1193587
    iput-object v1, v2, LX/7JM;->d:Ljava/lang/String;

    .line 1193588
    :cond_3
    move-object v1, v2

    .line 1193589
    iget-object v2, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    .line 1193590
    iput-object v2, v1, LX/7JM;->n:LX/0Px;

    .line 1193591
    move-object v1, v1

    .line 1193592
    iget-object v2, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/7JM;->b(Ljava/lang/String;)LX/7JM;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/7JM;->a(Ljava/lang/String;)LX/7JM;

    move-result-object v0

    iget-object v1, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->y:Z

    .line 1193593
    iput-boolean v1, v0, LX/7JM;->o:Z

    .line 1193594
    move-object v0, v0

    .line 1193595
    invoke-virtual {v0}, LX/7JM;->a()LX/7JN;

    move-result-object v0

    return-object v0

    .line 1193596
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 1193597
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-static {v3}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-static {v1}, LX/1xl;->a(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1193598
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v2

    .line 1193599
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    move-object v3, v2

    .line 1193600
    goto/16 :goto_1

    .line 1193601
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 1193602
    const-class v4, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v4}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v4

    const-string v5, "text"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v4

    invoke-virtual {v4}, LX/237;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v2, v4

    .line 1193603
    :goto_4
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_8

    move-object v3, v2

    .line 1193604
    goto/16 :goto_1

    :cond_7
    move-object v2, v3

    .line 1193605
    goto :goto_4

    .line 1193606
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 1193607
    :goto_5
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    move-object v3, v2

    .line 1193608
    goto/16 :goto_1

    :cond_9
    move-object v2, v3

    .line 1193609
    goto :goto_5

    :cond_a
    move-object v3, v4

    goto/16 :goto_2

    :cond_b
    move-object v3, v4

    goto/16 :goto_3
.end method

.method private static a(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1193532
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1193533
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1193534
    instance-of v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1193535
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1193536
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 1193537
    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1193538
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1193539
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1193540
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
