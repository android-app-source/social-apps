.class public LX/74Z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I


# direct methods
.method public constructor <init>(LX/74Y;)V
    .locals 1

    .prologue
    .line 1168125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1168126
    iget-object v0, p1, LX/74Y;->a:Ljava/lang/String;

    iput-object v0, p0, LX/74Z;->a:Ljava/lang/String;

    .line 1168127
    iget v0, p1, LX/74Y;->b:I

    iput v0, p0, LX/74Z;->b:I

    .line 1168128
    iget v0, p1, LX/74Y;->c:I

    iput v0, p0, LX/74Z;->c:I

    .line 1168129
    iget v0, p1, LX/74Y;->d:I

    iput v0, p0, LX/74Z;->d:I

    .line 1168130
    iget v0, p1, LX/74Y;->e:I

    iput v0, p0, LX/74Z;->e:I

    .line 1168131
    iget v0, p1, LX/74Y;->f:I

    iput v0, p0, LX/74Z;->f:I

    .line 1168132
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1168133
    iget-object v0, p0, LX/74Z;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1168134
    const-string v0, "target_type"

    iget-object v1, p0, LX/74Z;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1168135
    :cond_0
    const-string v0, "batch_size"

    iget v1, p0, LX/74Z;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1168136
    const-string v0, "media_attachment_count"

    iget v1, p0, LX/74Z;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1168137
    const-string v0, "xy_tag_count"

    iget v1, p0, LX/74Z;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1168138
    const-string v0, "with_tag_count"

    iget v1, p0, LX/74Z;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1168139
    const-string v0, "auto_retry_count"

    iget v1, p0, LX/74Z;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1168140
    return-object p1
.end method
