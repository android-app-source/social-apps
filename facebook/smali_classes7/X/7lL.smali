.class public LX/7lL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1234736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/7lL;
    .locals 1

    .prologue
    .line 1234737
    new-instance v0, LX/7lL;

    invoke-direct {v0}, LX/7lL;-><init>()V

    .line 1234738
    move-object v0, v0

    .line 1234739
    return-object v0
.end method

.method private static a(LX/0Px;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1234740
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x2c

    invoke-static {v1}, LX/0PO;->on(C)LX/0PO;

    move-result-object v1

    invoke-virtual {v1}, LX/0PO;->skipNulls()LX/0PO;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;
    .locals 2

    .prologue
    .line 1234741
    const/16 v0, 0x22

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\":\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1234742
    check-cast p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    .line 1234743
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 1234744
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "icon"

    iget-object v2, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->iconId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1234745
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "title"

    iget-object v2, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->description:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1234746
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "surface"

    iget-object v2, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->surface:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1234747
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "privacy"

    iget-object v2, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->privacy:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1234748
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "life_event_type"

    iget-object v2, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->lifeEventType:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1234749
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->story:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1234750
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "description"

    iget-object v2, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->story:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1234751
    :cond_0
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->startDate:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1234752
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "start_date"

    iget-object v2, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->startDate:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1234753
    :cond_1
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->place:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1234754
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "place"

    iget-object v2, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->place:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1234755
    :cond_2
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->tags:LX/0Px;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->tags:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1234756
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "tags"

    iget-object v2, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->tags:LX/0Px;

    invoke-static {v2}, LX/7lL;->a(LX/0Px;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1234757
    :cond_3
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->photoFbids:LX/0Px;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->photoFbids:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1234758
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "photos"

    iget-object v2, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->photoFbids:LX/0Px;

    invoke-static {v2}, LX/7lL;->a(LX/0Px;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1234759
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "{"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1234760
    iget-boolean v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->shouldUpdateRelationshipStatus:Z

    if-eqz v0, :cond_5

    .line 1234761
    const-string v2, "update_relationship_status"

    iget-boolean v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->shouldUpdateRelationshipStatus:Z

    if-eqz v0, :cond_d

    const-string v0, "1"

    :goto_0
    invoke-static {v1, v2, v0}, LX/7lL;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1234762
    :cond_5
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->endDate:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1234763
    const-string v0, "end_date"

    iget-object v2, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->endDate:Ljava/lang/String;

    .line 1234764
    const/16 v3, 0x22

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x2c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1234765
    :cond_6
    iget-boolean v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->isGraduated:Z

    if-eqz v0, :cond_7

    .line 1234766
    const-string v2, "graduated"

    iget-boolean v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->isGraduated:Z

    if-eqz v0, :cond_e

    const-string v0, "1"

    :goto_1
    invoke-static {v1, v2, v0}, LX/7lL;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1234767
    :cond_7
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->schoolType:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1234768
    const-string v0, "school_type"

    iget-object v2, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->schoolType:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, LX/7lL;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1234769
    :cond_8
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->schoolHubId:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1234770
    const-string v0, "school_hub_id"

    iget-object v2, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->schoolHubId:Ljava/lang/String;

    invoke-static {v1, v0, v2}, LX/7lL;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1234771
    :cond_9
    iget-boolean v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->isCurrent:Z

    if-eqz v0, :cond_a

    .line 1234772
    const-string v2, "current"

    iget-boolean v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->isCurrent:Z

    if-eqz v0, :cond_f

    const-string v0, "1"

    :goto_2
    invoke-static {v1, v2, v0}, LX/7lL;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1234773
    :cond_a
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->employerHubId:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 1234774
    const-string v0, "employer_hub_id"

    iget-object v2, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->employerHubId:Ljava/lang/String;

    invoke-static {v1, v0, v2}, LX/7lL;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1234775
    :cond_b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v2, 0x1

    if-gt v0, v2, :cond_10

    .line 1234776
    const-string v0, ""

    .line 1234777
    :goto_3
    move-object v0, v0

    .line 1234778
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 1234779
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "extras"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1234780
    :cond_c
    const-string v0, "%s/milestones"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->userId:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1234781
    new-instance v0, LX/14N;

    const-string v1, "graphObjectPosts"

    const-string v2, "POST"

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 1234782
    :cond_d
    const-string v0, "0"

    goto/16 :goto_0

    .line 1234783
    :cond_e
    const-string v0, "0"

    goto :goto_1

    .line 1234784
    :cond_f
    const-string v0, "0"

    goto :goto_2

    .line 1234785
    :cond_10
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const-string v3, "}"

    invoke-virtual {v1, v0, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1234786
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1234787
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
