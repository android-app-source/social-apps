.class public final LX/7zO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic b:LX/7z4;

.field public final synthetic c:LX/7zR;


# direct methods
.method public constructor <init>(LX/7zR;Lcom/google/common/util/concurrent/ListenableFuture;LX/7z4;)V
    .locals 0

    .prologue
    .line 1280920
    iput-object p1, p0, LX/7zO;->c:LX/7zR;

    iput-object p2, p0, LX/7zO;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p3, p0, LX/7zO;->b:LX/7z4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1280921
    iget-object v0, p0, LX/7zO;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1280922
    iget-object v0, p0, LX/7zO;->b:LX/7z4;

    invoke-virtual {v0}, LX/7z4;->a()V

    .line 1280923
    :goto_0
    return-void

    .line 1280924
    :cond_0
    check-cast p1, Ljava/lang/Exception;

    .line 1280925
    instance-of v0, p1, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 1280926
    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v2, 0x190

    if-eq v0, v2, :cond_1

    move v0, v1

    .line 1280927
    :goto_1
    iget-object v1, p0, LX/7zO;->b:LX/7z4;

    invoke-virtual {v1, p1, v0}, LX/7z4;->a(Ljava/lang/Exception;Z)V

    goto :goto_0

    .line 1280928
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1280929
    check-cast p1, Ljava/lang/String;

    .line 1280930
    iget-object v0, p0, LX/7zO;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1280931
    iget-object v0, p0, LX/7zO;->b:LX/7z4;

    invoke-virtual {v0}, LX/7z4;->a()V

    .line 1280932
    :goto_0
    return-void

    .line 1280933
    :cond_0
    iget-object v0, p0, LX/7zO;->b:LX/7z4;

    invoke-virtual {v0, p1}, LX/7z4;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
