.class public final enum LX/74S;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/74S;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/74S;

.field public static final enum ALBUM_PERMALINK:LX/74S;

.field public static final enum CAMPAIGN_SUGGESTED_PHOTOS:LX/74S;

.field public static final enum EVENT_COVER_PHOTO:LX/74S;

.field public static final enum FACEWEB:LX/74S;

.field public static final enum FOOD_PHOTOS:LX/74S;

.field public static final enum FULLSCREEN_GALLERY:LX/74S;

.field public static final enum FUNDRAISER_COVER_PHOTO:LX/74S;

.field public static final enum GROUPS_COVER_PHOTO:LX/74S;

.field public static final enum GROUPS_INFO_PAGE_PHOTO_ITEM:LX/74S;

.field public static final enum INTENT:LX/74S;

.field public static final enum NEARBYPLACES:LX/74S;

.field public static final enum NEWSFEED:LX/74S;

.field public static final enum OTHER:LX/74S;

.field public static final enum PAGE_COVER_PHOTO:LX/74S;

.field public static final enum PAGE_GRID_PHOTO_CARD:LX/74S;

.field public static final enum PAGE_PHOTOS_TAB:LX/74S;

.field public static final enum PAGE_PHOTO_MENUS:LX/74S;

.field public static final enum PAGE_PROFILE_PHOTO:LX/74S;

.field public static final enum PERSON_CARD_CONTEXT_ITEM:LX/74S;

.field public static final enum PHOTOS_BY_CATEGORY:LX/74S;

.field public static final enum PHOTOS_FEED:LX/74S;

.field public static final enum PHOTO_COMMENT:LX/74S;

.field public static final enum REACTION_FEED_STORY_PHOTO_ALBUM:LX/74S;

.field public static final enum REACTION_PHOTO_ITEM:LX/74S;

.field public static final enum REACTION_SHOW_MORE_PHOTOS:LX/74S;

.field public static final enum SEARCH_EYEWITNESS_MODULE:LX/74S;

.field public static final enum SEARCH_PHOTOS_GRID_MODULE:LX/74S;

.field public static final enum SEARCH_PHOTO_RESULTS_PAGE:LX/74S;

.field public static final enum SEARCH_PROFILE_SNAPSHOTS_MODULE:LX/74S;

.field public static final enum SEARCH_TOP_PHOTOS_MODULE:LX/74S;

.field public static final enum SNOWFLAKE:LX/74S;

.field public static final enum SOUVENIRS:LX/74S;

.field public static final enum TIMELINE_CONTEXT_ITEM:LX/74S;

.field public static final enum TIMELINE_COVER_PHOTO:LX/74S;

.field public static final enum TIMELINE_INTRO_CARD_FAV_PHOTO:LX/74S;

.field public static final enum TIMELINE_PANDORA_PHOTOS_OF_TARGET_AND_MUTUAL_FRIENDS_SMALL_THUMBNAIL:LX/74S;

.field public static final enum TIMELINE_PANDORA_PHOTOS_OF_USER_HIGHLIGHT_ALL_PHOTOS:LX/74S;

.field public static final enum TIMELINE_PANDORA_PHOTOS_OF_USER_SMALL_THUMBNAIL_ALL_PHOTOS:LX/74S;

.field public static final enum TIMELINE_PANDORA_PHOTOS_OF_USER_SMALL_THUMBNAIL_RELATIONSHIP_CARD:LX/74S;

.field public static final enum TIMELINE_PHOTOS_ABOUT_TAB:LX/74S;

.field public static final enum TIMELINE_PHOTOS_OF_USER:LX/74S;

.field public static final enum TIMELINE_PHOTOS_SYNCED:LX/74S;

.field public static final enum TIMELINE_PHOTO_ALBUMS:LX/74S;

.field public static final enum TIMELINE_PROFILE_PHOTO:LX/74S;

.field public static final enum TIMELINE_WALL:LX/74S;

.field public static final enum UNKNOWN:LX/74S;

.field public static final enum YOUR_PHOTOS:LX/74S;


# instance fields
.field public final referrer:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1168033
    new-instance v0, LX/74S;

    const-string v1, "UNKNOWN"

    sget-object v2, LX/74O;->OTHER:LX/74O;

    invoke-direct {v0, v1, v4, v2}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->UNKNOWN:LX/74S;

    .line 1168034
    new-instance v0, LX/74S;

    const-string v1, "OTHER"

    sget-object v2, LX/74O;->OTHER:LX/74O;

    invoke-direct {v0, v1, v5, v2}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->OTHER:LX/74S;

    .line 1168035
    new-instance v0, LX/74S;

    const-string v1, "SEARCH_EYEWITNESS_MODULE"

    sget-object v2, LX/74O;->SEARCH_EYEWITNESS_MODULE:LX/74O;

    invoke-direct {v0, v1, v6, v2}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->SEARCH_EYEWITNESS_MODULE:LX/74S;

    .line 1168036
    new-instance v0, LX/74S;

    const-string v1, "SEARCH_TOP_PHOTOS_MODULE"

    sget-object v2, LX/74O;->SEARCH_TOP_PHOTOS_MODULE:LX/74O;

    invoke-direct {v0, v1, v7, v2}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->SEARCH_TOP_PHOTOS_MODULE:LX/74S;

    .line 1168037
    new-instance v0, LX/74S;

    const-string v1, "SEARCH_PHOTOS_GRID_MODULE"

    sget-object v2, LX/74O;->SEARCH_PHOTOS_GRID_MODULE:LX/74O;

    invoke-direct {v0, v1, v8, v2}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->SEARCH_PHOTOS_GRID_MODULE:LX/74S;

    .line 1168038
    new-instance v0, LX/74S;

    const-string v1, "SEARCH_PHOTO_RESULTS_PAGE"

    const/4 v2, 0x5

    sget-object v3, LX/74O;->SEARCH_PHOTO_RESULTS_PAGE:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->SEARCH_PHOTO_RESULTS_PAGE:LX/74S;

    .line 1168039
    new-instance v0, LX/74S;

    const-string v1, "SEARCH_PROFILE_SNAPSHOTS_MODULE"

    const/4 v2, 0x6

    sget-object v3, LX/74O;->SEARCH_PROFILE_SNAPSHOT_MODULE:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->SEARCH_PROFILE_SNAPSHOTS_MODULE:LX/74S;

    .line 1168040
    new-instance v0, LX/74S;

    const-string v1, "TIMELINE_WALL"

    const/4 v2, 0x7

    sget-object v3, LX/74O;->TIMELINE:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->TIMELINE_WALL:LX/74S;

    .line 1168041
    new-instance v0, LX/74S;

    const-string v1, "TIMELINE_PHOTOS_ABOUT_TAB"

    const/16 v2, 0x8

    sget-object v3, LX/74O;->TIMELINE_PHOTO_WIDGET:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->TIMELINE_PHOTOS_ABOUT_TAB:LX/74S;

    .line 1168042
    new-instance v0, LX/74S;

    const-string v1, "TIMELINE_PHOTOS_OF_USER"

    const/16 v2, 0x9

    sget-object v3, LX/74O;->PHOTOS_OF_:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->TIMELINE_PHOTOS_OF_USER:LX/74S;

    .line 1168043
    new-instance v0, LX/74S;

    const-string v1, "TIMELINE_PANDORA_PHOTOS_OF_USER_HIGHLIGHT_ALL_PHOTOS"

    const/16 v2, 0xa

    sget-object v3, LX/74O;->PANDORA_BENNY_PHOTOS_OF_LARGE_THUMBNAIL_ALL_PHOTOS:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->TIMELINE_PANDORA_PHOTOS_OF_USER_HIGHLIGHT_ALL_PHOTOS:LX/74S;

    .line 1168044
    new-instance v0, LX/74S;

    const-string v1, "TIMELINE_PANDORA_PHOTOS_OF_USER_SMALL_THUMBNAIL_RELATIONSHIP_CARD"

    const/16 v2, 0xb

    sget-object v3, LX/74O;->PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_SPECIAL_CARD:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->TIMELINE_PANDORA_PHOTOS_OF_USER_SMALL_THUMBNAIL_RELATIONSHIP_CARD:LX/74S;

    .line 1168045
    new-instance v0, LX/74S;

    const-string v1, "TIMELINE_PANDORA_PHOTOS_OF_USER_SMALL_THUMBNAIL_ALL_PHOTOS"

    const/16 v2, 0xc

    sget-object v3, LX/74O;->PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_ALL_PHOTOS:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->TIMELINE_PANDORA_PHOTOS_OF_USER_SMALL_THUMBNAIL_ALL_PHOTOS:LX/74S;

    .line 1168046
    new-instance v0, LX/74S;

    const-string v1, "TIMELINE_PANDORA_PHOTOS_OF_TARGET_AND_MUTUAL_FRIENDS_SMALL_THUMBNAIL"

    const/16 v2, 0xd

    sget-object v3, LX/74O;->PANDORA_BENNY_PHOTOS_OF_SMALL_THUMBNAIL_TARGET_AND_MUTUAL_FRIENDS:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->TIMELINE_PANDORA_PHOTOS_OF_TARGET_AND_MUTUAL_FRIENDS_SMALL_THUMBNAIL:LX/74S;

    .line 1168047
    new-instance v0, LX/74S;

    const-string v1, "TIMELINE_PHOTO_ALBUMS"

    const/16 v2, 0xe

    sget-object v3, LX/74O;->ALBUM:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->TIMELINE_PHOTO_ALBUMS:LX/74S;

    .line 1168048
    new-instance v0, LX/74S;

    const-string v1, "TIMELINE_COVER_PHOTO"

    const/16 v2, 0xf

    sget-object v3, LX/74O;->TIMELINE_COVER_PHOTO:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->TIMELINE_COVER_PHOTO:LX/74S;

    .line 1168049
    new-instance v0, LX/74S;

    const-string v1, "TIMELINE_PROFILE_PHOTO"

    const/16 v2, 0x10

    sget-object v3, LX/74O;->TIMELINE_PROFILE_PHOTO:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->TIMELINE_PROFILE_PHOTO:LX/74S;

    .line 1168050
    new-instance v0, LX/74S;

    const-string v1, "TIMELINE_INTRO_CARD_FAV_PHOTO"

    const/16 v2, 0x11

    sget-object v3, LX/74O;->TIMELINE_INTRO_CARD_FAV_PHOTO:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->TIMELINE_INTRO_CARD_FAV_PHOTO:LX/74S;

    .line 1168051
    new-instance v0, LX/74S;

    const-string v1, "PAGE_COVER_PHOTO"

    const/16 v2, 0x12

    sget-object v3, LX/74O;->PAGE_COVER_PHOTO:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->PAGE_COVER_PHOTO:LX/74S;

    .line 1168052
    new-instance v0, LX/74S;

    const-string v1, "PAGE_PROFILE_PHOTO"

    const/16 v2, 0x13

    sget-object v3, LX/74O;->PAGE_PROFILE_PHOTO:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->PAGE_PROFILE_PHOTO:LX/74S;

    .line 1168053
    new-instance v0, LX/74S;

    const-string v1, "NEWSFEED"

    const/16 v2, 0x14

    sget-object v3, LX/74O;->FEED:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->NEWSFEED:LX/74S;

    .line 1168054
    new-instance v0, LX/74S;

    const-string v1, "FULLSCREEN_GALLERY"

    const/16 v2, 0x15

    sget-object v3, LX/74O;->FULL_SCREEN_GALLERY:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->FULLSCREEN_GALLERY:LX/74S;

    .line 1168055
    new-instance v0, LX/74S;

    const-string v1, "SNOWFLAKE"

    const/16 v2, 0x16

    sget-object v3, LX/74O;->SNOWFLAKE:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->SNOWFLAKE:LX/74S;

    .line 1168056
    new-instance v0, LX/74S;

    const-string v1, "PHOTOS_FEED"

    const/16 v2, 0x17

    sget-object v3, LX/74O;->PHOTOS_FEED:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->PHOTOS_FEED:LX/74S;

    .line 1168057
    new-instance v0, LX/74S;

    const-string v1, "PHOTO_COMMENT"

    const/16 v2, 0x18

    sget-object v3, LX/74O;->PHOTO_COMMENT:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->PHOTO_COMMENT:LX/74S;

    .line 1168058
    new-instance v0, LX/74S;

    const-string v1, "TIMELINE_PHOTOS_SYNCED"

    const/16 v2, 0x19

    sget-object v3, LX/74O;->SYNC:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->TIMELINE_PHOTOS_SYNCED:LX/74S;

    .line 1168059
    new-instance v0, LX/74S;

    const-string v1, "EVENT_COVER_PHOTO"

    const/16 v2, 0x1a

    sget-object v3, LX/74O;->PHOTO_COMMENT:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->EVENT_COVER_PHOTO:LX/74S;

    .line 1168060
    new-instance v0, LX/74S;

    const-string v1, "TIMELINE_CONTEXT_ITEM"

    const/16 v2, 0x1b

    sget-object v3, LX/74O;->OTHER:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->TIMELINE_CONTEXT_ITEM:LX/74S;

    .line 1168061
    new-instance v0, LX/74S;

    const-string v1, "PERSON_CARD_CONTEXT_ITEM"

    const/16 v2, 0x1c

    sget-object v3, LX/74O;->OTHER:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->PERSON_CARD_CONTEXT_ITEM:LX/74S;

    .line 1168062
    new-instance v0, LX/74S;

    const-string v1, "GROUPS_INFO_PAGE_PHOTO_ITEM"

    const/16 v2, 0x1d

    sget-object v3, LX/74O;->GROUPS_INFO_PAGE_PHOTO:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->GROUPS_INFO_PAGE_PHOTO_ITEM:LX/74S;

    .line 1168063
    new-instance v0, LX/74S;

    const-string v1, "GROUPS_COVER_PHOTO"

    const/16 v2, 0x1e

    sget-object v3, LX/74O;->GROUPS_COVER_PHOTO:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->GROUPS_COVER_PHOTO:LX/74S;

    .line 1168064
    new-instance v0, LX/74S;

    const-string v1, "REACTION_FEED_STORY_PHOTO_ALBUM"

    const/16 v2, 0x1f

    sget-object v3, LX/74O;->OTHER:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->REACTION_FEED_STORY_PHOTO_ALBUM:LX/74S;

    .line 1168065
    new-instance v0, LX/74S;

    const-string v1, "REACTION_PHOTO_ITEM"

    const/16 v2, 0x20

    sget-object v3, LX/74O;->OTHER:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->REACTION_PHOTO_ITEM:LX/74S;

    .line 1168066
    new-instance v0, LX/74S;

    const-string v1, "REACTION_SHOW_MORE_PHOTOS"

    const/16 v2, 0x21

    sget-object v3, LX/74O;->OTHER:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->REACTION_SHOW_MORE_PHOTOS:LX/74S;

    .line 1168067
    new-instance v0, LX/74S;

    const-string v1, "ALBUM_PERMALINK"

    const/16 v2, 0x22

    sget-object v3, LX/74O;->ALBUM_PERMALINK:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->ALBUM_PERMALINK:LX/74S;

    .line 1168068
    new-instance v0, LX/74S;

    const-string v1, "YOUR_PHOTOS"

    const/16 v2, 0x23

    sget-object v3, LX/74O;->YOUR_PHOTOS:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->YOUR_PHOTOS:LX/74S;

    .line 1168069
    new-instance v0, LX/74S;

    const-string v1, "FACEWEB"

    const/16 v2, 0x24

    sget-object v3, LX/74O;->FACEWEB:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->FACEWEB:LX/74S;

    .line 1168070
    new-instance v0, LX/74S;

    const-string v1, "PHOTOS_BY_CATEGORY"

    const/16 v2, 0x25

    sget-object v3, LX/74O;->PHOTOS_BY_CATEGORY:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->PHOTOS_BY_CATEGORY:LX/74S;

    .line 1168071
    new-instance v0, LX/74S;

    const-string v1, "PAGE_GRID_PHOTO_CARD"

    const/16 v2, 0x26

    sget-object v3, LX/74O;->PAGE_GRID_PHOTO_CARD:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->PAGE_GRID_PHOTO_CARD:LX/74S;

    .line 1168072
    new-instance v0, LX/74S;

    const-string v1, "PAGE_PHOTO_MENUS"

    const/16 v2, 0x27

    sget-object v3, LX/74O;->PAGE_PHOTO_MENUS:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->PAGE_PHOTO_MENUS:LX/74S;

    .line 1168073
    new-instance v0, LX/74S;

    const-string v1, "PAGE_PHOTOS_TAB"

    const/16 v2, 0x28

    sget-object v3, LX/74O;->PAGE_PHOTOS_TAB:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->PAGE_PHOTOS_TAB:LX/74S;

    .line 1168074
    new-instance v0, LX/74S;

    const-string v1, "FOOD_PHOTOS"

    const/16 v2, 0x29

    sget-object v3, LX/74O;->FOOD_PHOTOS:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->FOOD_PHOTOS:LX/74S;

    .line 1168075
    new-instance v0, LX/74S;

    const-string v1, "SOUVENIRS"

    const/16 v2, 0x2a

    sget-object v3, LX/74O;->SOUVENIRS:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->SOUVENIRS:LX/74S;

    .line 1168076
    new-instance v0, LX/74S;

    const-string v1, "CAMPAIGN_SUGGESTED_PHOTOS"

    const/16 v2, 0x2b

    sget-object v3, LX/74O;->PROMOTION_CAMPAIGN_PHOTOS:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->CAMPAIGN_SUGGESTED_PHOTOS:LX/74S;

    .line 1168077
    new-instance v0, LX/74S;

    const-string v1, "INTENT"

    const/16 v2, 0x2c

    sget-object v3, LX/74O;->INTENT:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->INTENT:LX/74S;

    .line 1168078
    new-instance v0, LX/74S;

    const-string v1, "NEARBYPLACES"

    const/16 v2, 0x2d

    sget-object v3, LX/74O;->NEARBYPLACES:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->NEARBYPLACES:LX/74S;

    .line 1168079
    new-instance v0, LX/74S;

    const-string v1, "FUNDRAISER_COVER_PHOTO"

    const/16 v2, 0x2e

    sget-object v3, LX/74O;->FUNDRAISER_COVER_PHOTO:LX/74O;

    invoke-direct {v0, v1, v2, v3}, LX/74S;-><init>(Ljava/lang/String;ILX/74O;)V

    sput-object v0, LX/74S;->FUNDRAISER_COVER_PHOTO:LX/74S;

    .line 1168080
    const/16 v0, 0x2f

    new-array v0, v0, [LX/74S;

    sget-object v1, LX/74S;->UNKNOWN:LX/74S;

    aput-object v1, v0, v4

    sget-object v1, LX/74S;->OTHER:LX/74S;

    aput-object v1, v0, v5

    sget-object v1, LX/74S;->SEARCH_EYEWITNESS_MODULE:LX/74S;

    aput-object v1, v0, v6

    sget-object v1, LX/74S;->SEARCH_TOP_PHOTOS_MODULE:LX/74S;

    aput-object v1, v0, v7

    sget-object v1, LX/74S;->SEARCH_PHOTOS_GRID_MODULE:LX/74S;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/74S;->SEARCH_PHOTO_RESULTS_PAGE:LX/74S;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/74S;->SEARCH_PROFILE_SNAPSHOTS_MODULE:LX/74S;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/74S;->TIMELINE_WALL:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/74S;->TIMELINE_PHOTOS_ABOUT_TAB:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/74S;->TIMELINE_PHOTOS_OF_USER:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/74S;->TIMELINE_PANDORA_PHOTOS_OF_USER_HIGHLIGHT_ALL_PHOTOS:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/74S;->TIMELINE_PANDORA_PHOTOS_OF_USER_SMALL_THUMBNAIL_RELATIONSHIP_CARD:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/74S;->TIMELINE_PANDORA_PHOTOS_OF_USER_SMALL_THUMBNAIL_ALL_PHOTOS:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/74S;->TIMELINE_PANDORA_PHOTOS_OF_TARGET_AND_MUTUAL_FRIENDS_SMALL_THUMBNAIL:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/74S;->TIMELINE_PHOTO_ALBUMS:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/74S;->TIMELINE_COVER_PHOTO:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/74S;->TIMELINE_PROFILE_PHOTO:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/74S;->TIMELINE_INTRO_CARD_FAV_PHOTO:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/74S;->PAGE_COVER_PHOTO:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/74S;->PAGE_PROFILE_PHOTO:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/74S;->NEWSFEED:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/74S;->FULLSCREEN_GALLERY:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/74S;->SNOWFLAKE:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/74S;->PHOTOS_FEED:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/74S;->PHOTO_COMMENT:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/74S;->TIMELINE_PHOTOS_SYNCED:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/74S;->EVENT_COVER_PHOTO:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/74S;->TIMELINE_CONTEXT_ITEM:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/74S;->PERSON_CARD_CONTEXT_ITEM:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/74S;->GROUPS_INFO_PAGE_PHOTO_ITEM:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/74S;->GROUPS_COVER_PHOTO:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/74S;->REACTION_FEED_STORY_PHOTO_ALBUM:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/74S;->REACTION_PHOTO_ITEM:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/74S;->REACTION_SHOW_MORE_PHOTOS:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/74S;->ALBUM_PERMALINK:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/74S;->YOUR_PHOTOS:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/74S;->FACEWEB:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/74S;->PHOTOS_BY_CATEGORY:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/74S;->PAGE_GRID_PHOTO_CARD:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/74S;->PAGE_PHOTO_MENUS:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/74S;->PAGE_PHOTOS_TAB:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/74S;->FOOD_PHOTOS:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/74S;->SOUVENIRS:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/74S;->CAMPAIGN_SUGGESTED_PHOTOS:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/74S;->INTENT:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/74S;->NEARBYPLACES:LX/74S;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, LX/74S;->FUNDRAISER_COVER_PHOTO:LX/74S;

    aput-object v2, v0, v1

    sput-object v0, LX/74S;->$VALUES:[LX/74S;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/74O;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74O;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1168030
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1168031
    iget-object v0, p3, LX/74O;->value:Ljava/lang/String;

    iput-object v0, p0, LX/74S;->referrer:Ljava/lang/String;

    .line 1168032
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/74S;
    .locals 1

    .prologue
    .line 1168081
    const-class v0, LX/74S;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/74S;

    return-object v0
.end method

.method public static values()[LX/74S;
    .locals 1

    .prologue
    .line 1168029
    sget-object v0, LX/74S;->$VALUES:[LX/74S;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/74S;

    return-object v0
.end method
