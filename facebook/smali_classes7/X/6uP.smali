.class public final LX/6uP;
.super LX/6qh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/confirmation/ConfirmationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/confirmation/ConfirmationFragment;)V
    .locals 0

    .prologue
    .line 1155809
    iput-object p1, p0, LX/6uP;->a:Lcom/facebook/payments/confirmation/ConfirmationFragment;

    invoke-direct {p0}, LX/6qh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/73T;)V
    .locals 4

    .prologue
    .line 1155789
    iget-object v0, p0, LX/6uP;->a:Lcom/facebook/payments/confirmation/ConfirmationFragment;

    .line 1155790
    sget-object v1, LX/6uS;->a:[I

    .line 1155791
    iget-object v2, p1, LX/73T;->a:LX/73S;

    move-object v2, v2

    .line 1155792
    invoke-virtual {v2}, LX/73S;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1155793
    :goto_0
    return-void

    .line 1155794
    :pswitch_0
    const-string v1, "extra_reset_data"

    invoke-virtual {p1, v1}, LX/73T;->a(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/confirmation/ConfirmationData;

    iput-object v1, v0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->f:Lcom/facebook/payments/confirmation/ConfirmationData;

    .line 1155795
    invoke-static {v0}, Lcom/facebook/payments/confirmation/ConfirmationFragment;->k(Lcom/facebook/payments/confirmation/ConfirmationFragment;)V

    goto :goto_0

    .line 1155796
    :pswitch_1
    const-string v1, "extra_user_action"

    invoke-virtual {p1, v1}, LX/73T;->b(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LX/6uT;

    .line 1155797
    iget-object v2, v0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->g:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result p0

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, p0, :cond_0

    iget-object v2, v0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->g:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6uG;

    .line 1155798
    invoke-interface {v2}, LX/6uG;->d()LX/6uT;

    move-result-object p1

    if-ne p1, v1, :cond_1

    .line 1155799
    check-cast v2, LX/6uH;

    .line 1155800
    iget-object v3, v0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->j:LX/6F4;

    iget-object p0, v0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->f:Lcom/facebook/payments/confirmation/ConfirmationData;

    invoke-interface {v3, p0, v2}, LX/6F4;->onClick(Lcom/facebook/payments/confirmation/ConfirmationData;LX/6uH;)V

    .line 1155801
    :cond_0
    goto :goto_0

    .line 1155802
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1155803
    iget-object v0, p0, LX/6uP;->a:Lcom/facebook/payments/confirmation/ConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/6uP;->a:Lcom/facebook/payments/confirmation/ConfirmationFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1155804
    return-void
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 1155805
    iget-object v0, p0, LX/6uP;->a:Lcom/facebook/payments/confirmation/ConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/6uP;->a:Lcom/facebook/payments/confirmation/ConfirmationFragment;

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1155806
    return-void
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1155807
    iget-object v0, p0, LX/6uP;->a:Lcom/facebook/payments/confirmation/ConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/6uP;->a:Lcom/facebook/payments/confirmation/ConfirmationFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1155808
    return-void
.end method
