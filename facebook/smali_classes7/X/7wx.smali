.class public final enum LX/7wx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7wx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7wx;

.field public static final enum BOOLEAN:LX/7wx;

.field public static final enum COMPOUND_MAP:LX/7wx;

.field public static final enum STRING:LX/7wx;

.field public static final enum STRING_SET:LX/7wx;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1276802
    new-instance v0, LX/7wx;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v2}, LX/7wx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wx;->STRING:LX/7wx;

    .line 1276803
    new-instance v0, LX/7wx;

    const-string v1, "BOOLEAN"

    invoke-direct {v0, v1, v3}, LX/7wx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wx;->BOOLEAN:LX/7wx;

    .line 1276804
    new-instance v0, LX/7wx;

    const-string v1, "STRING_SET"

    invoke-direct {v0, v1, v4}, LX/7wx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wx;->STRING_SET:LX/7wx;

    .line 1276805
    new-instance v0, LX/7wx;

    const-string v1, "COMPOUND_MAP"

    invoke-direct {v0, v1, v5}, LX/7wx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7wx;->COMPOUND_MAP:LX/7wx;

    .line 1276806
    const/4 v0, 0x4

    new-array v0, v0, [LX/7wx;

    sget-object v1, LX/7wx;->STRING:LX/7wx;

    aput-object v1, v0, v2

    sget-object v1, LX/7wx;->BOOLEAN:LX/7wx;

    aput-object v1, v0, v3

    sget-object v1, LX/7wx;->STRING_SET:LX/7wx;

    aput-object v1, v0, v4

    sget-object v1, LX/7wx;->COMPOUND_MAP:LX/7wx;

    aput-object v1, v0, v5

    sput-object v0, LX/7wx;->$VALUES:[LX/7wx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1276807
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7wx;
    .locals 1

    .prologue
    .line 1276801
    const-class v0, LX/7wx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7wx;

    return-object v0
.end method

.method public static values()[LX/7wx;
    .locals 1

    .prologue
    .line 1276800
    sget-object v0, LX/7wx;->$VALUES:[LX/7wx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7wx;

    return-object v0
.end method
