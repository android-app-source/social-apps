.class public final LX/8U8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1349970
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 1349971
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1349972
    :goto_0
    return v1

    .line 1349973
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_8

    .line 1349974
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1349975
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1349976
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 1349977
    const-string v10, "has_existing_game"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1349978
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v8, v0

    move v0, v2

    goto :goto_1

    .line 1349979
    :cond_1
    const-string v10, "line_one"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1349980
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1349981
    :cond_2
    const-string v10, "line_two"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1349982
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1349983
    :cond_3
    const-string v10, "participants"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1349984
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1349985
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_4

    .line 1349986
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_4

    .line 1349987
    invoke-static {p0, p1}, LX/8U7;->b(LX/15w;LX/186;)I

    move-result v9

    .line 1349988
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1349989
    :cond_4
    invoke-static {v5, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 1349990
    goto :goto_1

    .line 1349991
    :cond_5
    const-string v10, "thread_key"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1349992
    const/4 v9, 0x0

    .line 1349993
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v10, :cond_e

    .line 1349994
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1349995
    :goto_3
    move v4, v9

    .line 1349996
    goto/16 :goto_1

    .line 1349997
    :cond_6
    const-string v10, "thread_name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1349998
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1349999
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1350000
    :cond_8
    const/4 v9, 0x6

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1350001
    if-eqz v0, :cond_9

    .line 1350002
    invoke-virtual {p1, v1, v8}, LX/186;->a(IZ)V

    .line 1350003
    :cond_9
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 1350004
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1350005
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1350006
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1350007
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1350008
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1

    .line 1350009
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1350010
    :cond_c
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_d

    .line 1350011
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1350012
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1350013
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_c

    if-eqz v10, :cond_c

    .line 1350014
    const-string v11, "thread_fbid"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 1350015
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_4

    .line 1350016
    :cond_d
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1350017
    invoke-virtual {p1, v9, v4}, LX/186;->b(II)V

    .line 1350018
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_3

    :cond_e
    move v4, v9

    goto :goto_4
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1350019
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1350020
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1350021
    if-eqz v0, :cond_0

    .line 1350022
    const-string v1, "has_existing_game"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1350023
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1350024
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1350025
    if-eqz v0, :cond_1

    .line 1350026
    const-string v1, "line_one"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1350027
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1350028
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1350029
    if-eqz v0, :cond_2

    .line 1350030
    const-string v1, "line_two"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1350031
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1350032
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1350033
    if-eqz v0, :cond_4

    .line 1350034
    const-string v1, "participants"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1350035
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1350036
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 1350037
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/8U7;->a(LX/15i;ILX/0nX;)V

    .line 1350038
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1350039
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1350040
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1350041
    if-eqz v0, :cond_6

    .line 1350042
    const-string v1, "thread_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1350043
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1350044
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1350045
    if-eqz v1, :cond_5

    .line 1350046
    const-string v2, "thread_fbid"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1350047
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1350048
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1350049
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1350050
    if-eqz v0, :cond_7

    .line 1350051
    const-string v1, "thread_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1350052
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1350053
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1350054
    return-void
.end method
