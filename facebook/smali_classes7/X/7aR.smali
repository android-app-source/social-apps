.class public final LX/7aR;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()LX/7aQ;
    .locals 2

    :try_start_0
    new-instance v0, LX/7aQ;

    invoke-static {}, LX/7aR;->c()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;->a()LX/1ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7aQ;-><init>(LX/1ot;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(F)LX/7aQ;
    .locals 2

    :try_start_0
    new-instance v0, LX/7aQ;

    invoke-static {}, LX/7aR;->c()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;->a(F)LX/1ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7aQ;-><init>(LX/1ot;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(FF)LX/7aQ;
    .locals 2

    :try_start_0
    new-instance v0, LX/7aQ;

    invoke-static {}, LX/7aR;->c()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;

    move-result-object v1

    invoke-interface {v1, p0, p1}, Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;->a(FF)LX/1ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7aQ;-><init>(LX/1ot;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(FLandroid/graphics/Point;)LX/7aQ;
    .locals 4

    :try_start_0
    new-instance v0, LX/7aQ;

    invoke-static {}, LX/7aR;->c()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;

    move-result-object v1

    iget v2, p1, Landroid/graphics/Point;->x:I

    iget v3, p1, Landroid/graphics/Point;->y:I

    invoke-interface {v1, p0, v2, v3}, Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;->a(FII)LX/1ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7aQ;-><init>(LX/1ot;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(Lcom/google/android/gms/maps/model/CameraPosition;)LX/7aQ;
    .locals 2

    :try_start_0
    new-instance v0, LX/7aQ;

    invoke-static {}, LX/7aR;->c()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;->a(Lcom/google/android/gms/maps/model/CameraPosition;)LX/1ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7aQ;-><init>(LX/1ot;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLng;)LX/7aQ;
    .locals 2

    :try_start_0
    new-instance v0, LX/7aQ;

    invoke-static {}, LX/7aR;->c()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;->a(Lcom/google/android/gms/maps/model/LatLng;)LX/1ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7aQ;-><init>(LX/1ot;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLng;F)LX/7aQ;
    .locals 2

    :try_start_0
    new-instance v0, LX/7aQ;

    invoke-static {}, LX/7aR;->c()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;

    move-result-object v1

    invoke-interface {v1, p0, p1}, Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;->a(Lcom/google/android/gms/maps/model/LatLng;F)LX/1ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7aQ;-><init>(LX/1ot;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLngBounds;I)LX/7aQ;
    .locals 2

    :try_start_0
    new-instance v0, LX/7aQ;

    invoke-static {}, LX/7aR;->c()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;

    move-result-object v1

    invoke-interface {v1, p0, p1}, Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;->a(Lcom/google/android/gms/maps/model/LatLngBounds;I)LX/1ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7aQ;-><init>(LX/1ot;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLngBounds;III)LX/7aQ;
    .locals 2

    :try_start_0
    new-instance v0, LX/7aQ;

    invoke-static {}, LX/7aR;->c()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;

    move-result-object v1

    invoke-interface {v1, p0, p1, p2, p3}, Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;->a(Lcom/google/android/gms/maps/model/LatLngBounds;III)LX/1ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7aQ;-><init>(LX/1ot;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static b()LX/7aQ;
    .locals 2

    :try_start_0
    new-instance v0, LX/7aQ;

    invoke-static {}, LX/7aR;->c()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;->b()LX/1ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7aQ;-><init>(LX/1ot;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static b(F)LX/7aQ;
    .locals 2

    :try_start_0
    new-instance v0, LX/7aQ;

    invoke-static {}, LX/7aR;->c()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;->b(F)LX/1ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7aQ;-><init>(LX/1ot;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method private static c()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;
    .locals 2

    sget-object v0, LX/7aR;->a:Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;

    const-string v1, "CameraUpdateFactory is not initialized"

    invoke-static {v0, v1}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;

    return-object v0
.end method
