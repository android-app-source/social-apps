.class public LX/82T;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static volatile c:LX/82T;


# instance fields
.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1288239
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "seefirst/nux_seen_times"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/82T;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1288241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1288242
    iput-object p1, p0, LX/82T;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1288243
    return-void
.end method

.method public static a(LX/0QB;)LX/82T;
    .locals 4

    .prologue
    .line 1288244
    sget-object v0, LX/82T;->c:LX/82T;

    if-nez v0, :cond_1

    .line 1288245
    const-class v1, LX/82T;

    monitor-enter v1

    .line 1288246
    :try_start_0
    sget-object v0, LX/82T;->c:LX/82T;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1288247
    if-eqz v2, :cond_0

    .line 1288248
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1288249
    new-instance p0, LX/82T;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/82T;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1288250
    move-object v0, p0

    .line 1288251
    sput-object v0, LX/82T;->c:LX/82T;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1288252
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1288253
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1288254
    :cond_1
    sget-object v0, LX/82T;->c:LX/82T;

    return-object v0

    .line 1288255
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1288256
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1288240
    iget-object v1, p0, LX/82T;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/82T;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
