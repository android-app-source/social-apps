.class public final LX/6tI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Dp;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1154134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1154135
    return-void
.end method

.method public static a(LX/0QB;)LX/6tI;
    .locals 1

    .prologue
    .line 1154136
    new-instance v0, LX/6tI;

    invoke-direct {v0}, LX/6tI;-><init>()V

    .line 1154137
    move-object v0, v0

    .line 1154138
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;LX/6so;)LX/6E8;
    .locals 3

    .prologue
    .line 1154139
    sget-object v0, LX/6tH;->a:[I

    invoke-virtual {p2}, LX/6so;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1154140
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled checkoutRowType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1154141
    :pswitch_0
    new-instance v0, LX/6tB;

    .line 1154142
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03162c

    const/4 p0, 0x0

    invoke-virtual {v1, v2, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;

    move-object v1, v1

    .line 1154143
    invoke-direct {v0, v1}, LX/6tB;-><init>(Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;)V

    move-object v0, v0

    .line 1154144
    :goto_0
    return-object v0

    .line 1154145
    :pswitch_1
    new-instance v0, LX/6tD;

    .line 1154146
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03162d

    const/4 p0, 0x0

    invoke-virtual {v1, v2, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;

    move-object v1, v1

    .line 1154147
    invoke-direct {v0, v1}, LX/6tD;-><init>(Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;)V

    move-object v0, v0

    .line 1154148
    goto :goto_0

    .line 1154149
    :pswitch_2
    new-instance v0, LX/73W;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/73W;-><init>(Landroid/content/Context;)V

    .line 1154150
    new-instance v1, LX/73X;

    invoke-direct {v1, v0}, LX/73X;-><init>(LX/73W;)V

    move-object v0, v1

    .line 1154151
    goto :goto_0

    .line 1154152
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03028d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/ImageDetailView;

    .line 1154153
    new-instance v1, LX/6sr;

    invoke-direct {v1, v0}, LX/6sr;-><init>(Lcom/facebook/payments/ui/ImageDetailView;)V

    move-object v0, v1

    .line 1154154
    goto :goto_0

    .line 1154155
    :pswitch_4
    new-instance v0, LX/6ss;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6ss;-><init>(Landroid/content/Context;)V

    .line 1154156
    new-instance v1, LX/6st;

    invoke-direct {v1, v0}, LX/6st;-><init>(LX/6ss;)V

    move-object v0, v1

    .line 1154157
    goto :goto_0

    .line 1154158
    :pswitch_5
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03162e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    .line 1154159
    new-instance v1, LX/6sy;

    invoke-direct {v1, v0}, LX/6sy;-><init>(Lcom/facebook/payments/ui/PrimaryCtaButtonView;)V

    move-object v0, v1

    .line 1154160
    goto :goto_0

    .line 1154161
    :pswitch_6
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031660

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PriceTableView;

    .line 1154162
    new-instance v1, LX/6t9;

    invoke-direct {v1, v0}, LX/6t9;-><init>(Lcom/facebook/payments/ui/PriceTableView;)V

    move-object v0, v1

    .line 1154163
    goto/16 :goto_0

    .line 1154164
    :pswitch_7
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03162f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/MediaGridTextLayout;

    .line 1154165
    new-instance v1, LX/6tF;

    invoke-direct {v1, v0}, LX/6tF;-><init>(Lcom/facebook/payments/ui/MediaGridTextLayout;)V

    move-object v0, v1

    .line 1154166
    goto/16 :goto_0

    .line 1154167
    :pswitch_8
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031630

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesCheckoutView;

    .line 1154168
    new-instance v1, LX/6tP;

    invoke-direct {v1, v0}, LX/6tP;-><init>(Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesCheckoutView;)V

    move-object v0, v1

    .line 1154169
    goto/16 :goto_0

    .line 1154170
    :pswitch_9
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03100f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;

    .line 1154171
    new-instance v1, LX/6t6;

    invoke-direct {v1, v0}, LX/6t6;-><init>(Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;)V

    move-object v0, v1

    .line 1154172
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
