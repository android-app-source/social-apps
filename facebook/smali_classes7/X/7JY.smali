.class public final LX/7JY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/List",
        "<",
        "LX/7Jh;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:LX/19w;


# direct methods
.method public constructor <init>(LX/19w;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1193911
    iput-object p1, p0, LX/7JY;->b:LX/19w;

    iput-object p2, p0, LX/7JY;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1193912
    iget-object v0, p0, LX/7JY;->b:LX/19w;

    invoke-static {v0}, LX/19w;->s(LX/19w;)V

    .line 1193913
    iget-object v0, p0, LX/7JY;->b:LX/19w;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1193914
    const v0, 0x39362a80

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1193915
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1193916
    :try_start_0
    iget-object v0, p0, LX/7JY;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1193917
    iget-object v4, p0, LX/7JY;->b:LX/19w;

    invoke-virtual {v4}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, v0}, LX/1A1;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)LX/7Jh;

    move-result-object v0

    .line 1193918
    if-eqz v0, :cond_0

    .line 1193919
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1193920
    :catch_0
    move-exception v0

    .line 1193921
    :try_start_1
    sget-object v2, LX/19w;->a:Ljava/lang/String;

    const-string v3, "Exception"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1193922
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1193923
    :catchall_0
    move-exception v0

    const v2, -0x4c341bea

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 1193924
    :cond_1
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1193925
    const v0, 0x9ba1580

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1193926
    return-object v2
.end method
