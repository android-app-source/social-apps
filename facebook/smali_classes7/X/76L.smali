.class public final enum LX/76L;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/76L;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/76L;

.field public static final enum MQTT_DID_NOT_RECEIVE_RESPONSE:LX/76L;

.field public static final enum MQTT_EXCEPTION:LX/76L;

.field public static final enum MQTT_FAILED_TO_CONNECT:LX/76L;

.field public static final enum MQTT_PUBLISH_FAILED:LX/76L;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1170971
    new-instance v0, LX/76L;

    const-string v1, "MQTT_FAILED_TO_CONNECT"

    invoke-direct {v0, v1, v2}, LX/76L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/76L;->MQTT_FAILED_TO_CONNECT:LX/76L;

    .line 1170972
    new-instance v0, LX/76L;

    const-string v1, "MQTT_PUBLISH_FAILED"

    invoke-direct {v0, v1, v3}, LX/76L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/76L;->MQTT_PUBLISH_FAILED:LX/76L;

    .line 1170973
    new-instance v0, LX/76L;

    const-string v1, "MQTT_DID_NOT_RECEIVE_RESPONSE"

    invoke-direct {v0, v1, v4}, LX/76L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/76L;->MQTT_DID_NOT_RECEIVE_RESPONSE:LX/76L;

    .line 1170974
    new-instance v0, LX/76L;

    const-string v1, "MQTT_EXCEPTION"

    invoke-direct {v0, v1, v5}, LX/76L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/76L;->MQTT_EXCEPTION:LX/76L;

    .line 1170975
    const/4 v0, 0x4

    new-array v0, v0, [LX/76L;

    sget-object v1, LX/76L;->MQTT_FAILED_TO_CONNECT:LX/76L;

    aput-object v1, v0, v2

    sget-object v1, LX/76L;->MQTT_PUBLISH_FAILED:LX/76L;

    aput-object v1, v0, v3

    sget-object v1, LX/76L;->MQTT_DID_NOT_RECEIVE_RESPONSE:LX/76L;

    aput-object v1, v0, v4

    sget-object v1, LX/76L;->MQTT_EXCEPTION:LX/76L;

    aput-object v1, v0, v5

    sput-object v0, LX/76L;->$VALUES:[LX/76L;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1170968
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/76L;
    .locals 1

    .prologue
    .line 1170969
    const-class v0, LX/76L;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/76L;

    return-object v0
.end method

.method public static values()[LX/76L;
    .locals 1

    .prologue
    .line 1170970
    sget-object v0, LX/76L;->$VALUES:[LX/76L;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/76L;

    return-object v0
.end method
