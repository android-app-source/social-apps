.class public final enum LX/72g;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/72g;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/72g;

.field public static final enum MESSENGER_COMMERCE:LX/72g;

.field public static final enum SIMPLE:LX/72g;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1164819
    new-instance v0, LX/72g;

    const-string v1, "MESSENGER_COMMERCE"

    invoke-direct {v0, v1, v2}, LX/72g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/72g;->MESSENGER_COMMERCE:LX/72g;

    .line 1164820
    new-instance v0, LX/72g;

    const-string v1, "SIMPLE"

    invoke-direct {v0, v1, v3}, LX/72g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/72g;->SIMPLE:LX/72g;

    .line 1164821
    const/4 v0, 0x2

    new-array v0, v0, [LX/72g;

    sget-object v1, LX/72g;->MESSENGER_COMMERCE:LX/72g;

    aput-object v1, v0, v2

    sget-object v1, LX/72g;->SIMPLE:LX/72g;

    aput-object v1, v0, v3

    sput-object v0, LX/72g;->$VALUES:[LX/72g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1164822
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/72g;
    .locals 1

    .prologue
    .line 1164823
    const-class v0, LX/72g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/72g;

    return-object v0
.end method

.method public static values()[LX/72g;
    .locals 1

    .prologue
    .line 1164824
    sget-object v0, LX/72g;->$VALUES:[LX/72g;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/72g;

    return-object v0
.end method
