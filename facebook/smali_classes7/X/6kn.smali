.class public LX/6kn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;

.field private static final l:LX/1sw;


# instance fields
.field public final actorFbId:Ljava/lang/Long;

.field public final adminText:Ljava/lang/String;

.field public final messageId:Ljava/lang/String;

.field public final offlineThreadingId:Ljava/lang/Long;

.field public final shouldBuzzDevice:Ljava/lang/Boolean;

.field public final skipBumpThread:Ljava/lang/Boolean;

.field public final tags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final threadKey:LX/6l9;

.field public final threadReadStateEffect:Ljava/lang/Integer;

.field public final timestamp:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0xb

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/16 v3, 0xa

    .line 1141418
    new-instance v0, LX/1sv;

    const-string v1, "MessageMetadata"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kn;->b:LX/1sv;

    .line 1141419
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kn;->c:LX/1sw;

    .line 1141420
    new-instance v0, LX/1sw;

    const-string v1, "messageId"

    invoke-direct {v0, v1, v7, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kn;->d:LX/1sw;

    .line 1141421
    new-instance v0, LX/1sw;

    const-string v1, "offlineThreadingId"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kn;->e:LX/1sw;

    .line 1141422
    new-instance v0, LX/1sw;

    const-string v1, "actorFbId"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kn;->f:LX/1sw;

    .line 1141423
    new-instance v0, LX/1sw;

    const-string v1, "timestamp"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kn;->g:LX/1sw;

    .line 1141424
    new-instance v0, LX/1sw;

    const-string v1, "shouldBuzzDevice"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kn;->h:LX/1sw;

    .line 1141425
    new-instance v0, LX/1sw;

    const-string v1, "adminText"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v7, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kn;->i:LX/1sw;

    .line 1141426
    new-instance v0, LX/1sw;

    const-string v1, "tags"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kn;->j:LX/1sw;

    .line 1141427
    new-instance v0, LX/1sw;

    const-string v1, "threadReadStateEffect"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kn;->k:LX/1sw;

    .line 1141428
    new-instance v0, LX/1sw;

    const-string v1, "skipBumpThread"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kn;->l:LX/1sw;

    .line 1141429
    sput-boolean v5, LX/6kn;->a:Z

    return-void
.end method

.method private constructor <init>(LX/6l9;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6l9;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1141157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1141158
    iput-object p1, p0, LX/6kn;->threadKey:LX/6l9;

    .line 1141159
    iput-object p2, p0, LX/6kn;->messageId:Ljava/lang/String;

    .line 1141160
    iput-object p3, p0, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    .line 1141161
    iput-object p4, p0, LX/6kn;->actorFbId:Ljava/lang/Long;

    .line 1141162
    iput-object p5, p0, LX/6kn;->timestamp:Ljava/lang/Long;

    .line 1141163
    iput-object p6, p0, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    .line 1141164
    iput-object p7, p0, LX/6kn;->adminText:Ljava/lang/String;

    .line 1141165
    iput-object p8, p0, LX/6kn;->tags:Ljava/util/List;

    .line 1141166
    iput-object p9, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    .line 1141167
    iput-object p10, p0, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    .line 1141168
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1141415
    iget-object v0, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, LX/6l0;->a:LX/1sn;

    iget-object v1, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1141416
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'threadReadStateEffect\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1141417
    :cond_0
    return-void
.end method

.method public static b(LX/1su;)LX/6kn;
    .locals 14

    .prologue
    .line 1141359
    const/4 v1, 0x0

    .line 1141360
    const/4 v2, 0x0

    .line 1141361
    const/4 v3, 0x0

    .line 1141362
    const/4 v4, 0x0

    .line 1141363
    const/4 v5, 0x0

    .line 1141364
    const/4 v6, 0x0

    .line 1141365
    const/4 v7, 0x0

    .line 1141366
    const/4 v8, 0x0

    .line 1141367
    const/4 v9, 0x0

    .line 1141368
    const/4 v10, 0x0

    .line 1141369
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    .line 1141370
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1141371
    iget-byte v11, v0, LX/1sw;->b:B

    if-eqz v11, :cond_c

    .line 1141372
    iget-short v11, v0, LX/1sw;->c:S

    packed-switch v11, :pswitch_data_0

    .line 1141373
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141374
    :pswitch_0
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xc

    if-ne v11, v12, :cond_1

    .line 1141375
    invoke-static {p0}, LX/6l9;->b(LX/1su;)LX/6l9;

    move-result-object v1

    goto :goto_0

    .line 1141376
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141377
    :pswitch_1
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xb

    if-ne v11, v12, :cond_2

    .line 1141378
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1141379
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141380
    :pswitch_2
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xa

    if-ne v11, v12, :cond_3

    .line 1141381
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    .line 1141382
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141383
    :pswitch_3
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xa

    if-ne v11, v12, :cond_4

    .line 1141384
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_0

    .line 1141385
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141386
    :pswitch_4
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xa

    if-ne v11, v12, :cond_5

    .line 1141387
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0

    .line 1141388
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141389
    :pswitch_5
    iget-byte v11, v0, LX/1sw;->b:B

    const/4 v12, 0x2

    if-ne v11, v12, :cond_6

    .line 1141390
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto/16 :goto_0

    .line 1141391
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141392
    :pswitch_6
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xb

    if-ne v11, v12, :cond_7

    .line 1141393
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1141394
    :cond_7
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141395
    :pswitch_7
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xf

    if-ne v11, v12, :cond_9

    .line 1141396
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v11

    .line 1141397
    new-instance v8, Ljava/util/ArrayList;

    const/4 v0, 0x0

    iget v12, v11, LX/1u3;->b:I

    invoke-static {v0, v12}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1141398
    const/4 v0, 0x0

    .line 1141399
    :goto_1
    iget v12, v11, LX/1u3;->b:I

    if-gez v12, :cond_8

    invoke-static {}, LX/1su;->t()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1141400
    :goto_2
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v12

    .line 1141401
    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1141402
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1141403
    :cond_8
    iget v12, v11, LX/1u3;->b:I

    if-ge v0, v12, :cond_0

    goto :goto_2

    .line 1141404
    :cond_9
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141405
    :pswitch_8
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0x8

    if-ne v11, v12, :cond_a

    .line 1141406
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    goto/16 :goto_0

    .line 1141407
    :cond_a
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141408
    :pswitch_9
    iget-byte v11, v0, LX/1sw;->b:B

    const/4 v12, 0x2

    if-ne v11, v12, :cond_b

    .line 1141409
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto/16 :goto_0

    .line 1141410
    :cond_b
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141411
    :cond_c
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1141412
    new-instance v0, LX/6kn;

    invoke-direct/range {v0 .. v10}, LX/6kn;-><init>(LX/6l9;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    .line 1141413
    invoke-direct {v0}, LX/6kn;->a()V

    .line 1141414
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1141250
    if-eqz p2, :cond_13

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1141251
    :goto_0
    if-eqz p2, :cond_14

    const-string v0, "\n"

    move-object v3, v0

    .line 1141252
    :goto_1
    if-eqz p2, :cond_15

    const-string v0, " "

    move-object v1, v0

    .line 1141253
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "MessageMetadata"

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1141254
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141255
    const-string v0, "("

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141256
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141257
    const/4 v0, 0x1

    .line 1141258
    iget-object v6, p0, LX/6kn;->threadKey:LX/6l9;

    if-eqz v6, :cond_0

    .line 1141259
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141260
    const-string v0, "threadKey"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141261
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141262
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141263
    iget-object v0, p0, LX/6kn;->threadKey:LX/6l9;

    if-nez v0, :cond_16

    .line 1141264
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v0, v2

    .line 1141265
    :cond_0
    iget-object v6, p0, LX/6kn;->messageId:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 1141266
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141267
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141268
    const-string v0, "messageId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141269
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141270
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141271
    iget-object v0, p0, LX/6kn;->messageId:Ljava/lang/String;

    if-nez v0, :cond_17

    .line 1141272
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v0, v2

    .line 1141273
    :cond_2
    iget-object v6, p0, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v6, :cond_4

    .line 1141274
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141275
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141276
    const-string v0, "offlineThreadingId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141277
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141278
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141279
    iget-object v0, p0, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    if-nez v0, :cond_18

    .line 1141280
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v0, v2

    .line 1141281
    :cond_4
    iget-object v6, p0, LX/6kn;->actorFbId:Ljava/lang/Long;

    if-eqz v6, :cond_6

    .line 1141282
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141283
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141284
    const-string v0, "actorFbId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141285
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141286
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141287
    iget-object v0, p0, LX/6kn;->actorFbId:Ljava/lang/Long;

    if-nez v0, :cond_19

    .line 1141288
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    move v0, v2

    .line 1141289
    :cond_6
    iget-object v6, p0, LX/6kn;->timestamp:Ljava/lang/Long;

    if-eqz v6, :cond_8

    .line 1141290
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141291
    :cond_7
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141292
    const-string v0, "timestamp"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141293
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141294
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141295
    iget-object v0, p0, LX/6kn;->timestamp:Ljava/lang/Long;

    if-nez v0, :cond_1a

    .line 1141296
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_7
    move v0, v2

    .line 1141297
    :cond_8
    iget-object v6, p0, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    if-eqz v6, :cond_a

    .line 1141298
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141299
    :cond_9
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141300
    const-string v0, "shouldBuzzDevice"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141301
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141302
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141303
    iget-object v0, p0, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    if-nez v0, :cond_1b

    .line 1141304
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_8
    move v0, v2

    .line 1141305
    :cond_a
    iget-object v6, p0, LX/6kn;->adminText:Ljava/lang/String;

    if-eqz v6, :cond_c

    .line 1141306
    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141307
    :cond_b
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141308
    const-string v0, "adminText"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141309
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141310
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141311
    iget-object v0, p0, LX/6kn;->adminText:Ljava/lang/String;

    if-nez v0, :cond_1c

    .line 1141312
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_9
    move v0, v2

    .line 1141313
    :cond_c
    iget-object v6, p0, LX/6kn;->tags:Ljava/util/List;

    if-eqz v6, :cond_e

    .line 1141314
    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141315
    :cond_d
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141316
    const-string v0, "tags"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141317
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141318
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141319
    iget-object v0, p0, LX/6kn;->tags:Ljava/util/List;

    if-nez v0, :cond_1d

    .line 1141320
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_a
    move v0, v2

    .line 1141321
    :cond_e
    iget-object v6, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    if-eqz v6, :cond_21

    .line 1141322
    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141323
    :cond_f
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141324
    const-string v0, "threadReadStateEffect"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141325
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141326
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141327
    iget-object v0, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    if-nez v0, :cond_1e

    .line 1141328
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141329
    :cond_10
    :goto_b
    iget-object v0, p0, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    .line 1141330
    if-nez v2, :cond_11

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141331
    :cond_11
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141332
    const-string v0, "skipBumpThread"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141333
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141334
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141335
    iget-object v0, p0, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    if-nez v0, :cond_20

    .line 1141336
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141337
    :cond_12
    :goto_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141338
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141339
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1141340
    :cond_13
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1141341
    :cond_14
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1141342
    :cond_15
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 1141343
    :cond_16
    iget-object v0, p0, LX/6kn;->threadKey:LX/6l9;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1141344
    :cond_17
    iget-object v0, p0, LX/6kn;->messageId:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1141345
    :cond_18
    iget-object v0, p0, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1141346
    :cond_19
    iget-object v0, p0, LX/6kn;->actorFbId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1141347
    :cond_1a
    iget-object v0, p0, LX/6kn;->timestamp:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1141348
    :cond_1b
    iget-object v0, p0, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1141349
    :cond_1c
    iget-object v0, p0, LX/6kn;->adminText:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 1141350
    :cond_1d
    iget-object v0, p0, LX/6kn;->tags:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 1141351
    :cond_1e
    sget-object v0, LX/6l0;->b:Ljava/util/Map;

    iget-object v6, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1141352
    if-eqz v0, :cond_1f

    .line 1141353
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141354
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141355
    :cond_1f
    iget-object v6, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1141356
    if-eqz v0, :cond_10

    .line 1141357
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    .line 1141358
    :cond_20
    iget-object v0, p0, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    :cond_21
    move v2, v0

    goto/16 :goto_b
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1141430
    invoke-direct {p0}, LX/6kn;->a()V

    .line 1141431
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1141432
    iget-object v0, p0, LX/6kn;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1141433
    iget-object v0, p0, LX/6kn;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1141434
    sget-object v0, LX/6kn;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141435
    iget-object v0, p0, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1141436
    :cond_0
    iget-object v0, p0, LX/6kn;->messageId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1141437
    iget-object v0, p0, LX/6kn;->messageId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1141438
    sget-object v0, LX/6kn;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141439
    iget-object v0, p0, LX/6kn;->messageId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1141440
    :cond_1
    iget-object v0, p0, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1141441
    iget-object v0, p0, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1141442
    sget-object v0, LX/6kn;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141443
    iget-object v0, p0, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1141444
    :cond_2
    iget-object v0, p0, LX/6kn;->actorFbId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1141445
    iget-object v0, p0, LX/6kn;->actorFbId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1141446
    sget-object v0, LX/6kn;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141447
    iget-object v0, p0, LX/6kn;->actorFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1141448
    :cond_3
    iget-object v0, p0, LX/6kn;->timestamp:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1141449
    iget-object v0, p0, LX/6kn;->timestamp:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1141450
    sget-object v0, LX/6kn;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141451
    iget-object v0, p0, LX/6kn;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1141452
    :cond_4
    iget-object v0, p0, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1141453
    iget-object v0, p0, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1141454
    sget-object v0, LX/6kn;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141455
    iget-object v0, p0, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1141456
    :cond_5
    iget-object v0, p0, LX/6kn;->adminText:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1141457
    iget-object v0, p0, LX/6kn;->adminText:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1141458
    sget-object v0, LX/6kn;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141459
    iget-object v0, p0, LX/6kn;->adminText:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1141460
    :cond_6
    iget-object v0, p0, LX/6kn;->tags:Ljava/util/List;

    if-eqz v0, :cond_7

    .line 1141461
    iget-object v0, p0, LX/6kn;->tags:Ljava/util/List;

    if-eqz v0, :cond_7

    .line 1141462
    sget-object v0, LX/6kn;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141463
    new-instance v0, LX/1u3;

    const/16 v1, 0xb

    iget-object v2, p0, LX/6kn;->tags:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1141464
    iget-object v0, p0, LX/6kn;->tags:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1141465
    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1141466
    :cond_7
    iget-object v0, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 1141467
    iget-object v0, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 1141468
    sget-object v0, LX/6kn;->k:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141469
    iget-object v0, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1141470
    :cond_8
    iget-object v0, p0, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 1141471
    iget-object v0, p0, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 1141472
    sget-object v0, LX/6kn;->l:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141473
    iget-object v0, p0, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1141474
    :cond_9
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1141475
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1141476
    return-void
.end method

.method public final a(LX/6kn;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1141177
    if-nez p1, :cond_1

    .line 1141178
    :cond_0
    :goto_0
    return v2

    .line 1141179
    :cond_1
    iget-object v0, p0, LX/6kn;->threadKey:LX/6l9;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1141180
    :goto_1
    iget-object v3, p1, LX/6kn;->threadKey:LX/6l9;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1141181
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1141182
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141183
    iget-object v0, p0, LX/6kn;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6kn;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141184
    :cond_3
    iget-object v0, p0, LX/6kn;->messageId:Ljava/lang/String;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1141185
    :goto_3
    iget-object v3, p1, LX/6kn;->messageId:Ljava/lang/String;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1141186
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1141187
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141188
    iget-object v0, p0, LX/6kn;->messageId:Ljava/lang/String;

    iget-object v3, p1, LX/6kn;->messageId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141189
    :cond_5
    iget-object v0, p0, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1141190
    :goto_5
    iget-object v3, p1, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1141191
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1141192
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141193
    iget-object v0, p0, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    iget-object v3, p1, LX/6kn;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141194
    :cond_7
    iget-object v0, p0, LX/6kn;->actorFbId:Ljava/lang/Long;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1141195
    :goto_7
    iget-object v3, p1, LX/6kn;->actorFbId:Ljava/lang/Long;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1141196
    :goto_8
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1141197
    :cond_8
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141198
    iget-object v0, p0, LX/6kn;->actorFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/6kn;->actorFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141199
    :cond_9
    iget-object v0, p0, LX/6kn;->timestamp:Ljava/lang/Long;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1141200
    :goto_9
    iget-object v3, p1, LX/6kn;->timestamp:Ljava/lang/Long;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1141201
    :goto_a
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1141202
    :cond_a
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141203
    iget-object v0, p0, LX/6kn;->timestamp:Ljava/lang/Long;

    iget-object v3, p1, LX/6kn;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141204
    :cond_b
    iget-object v0, p0, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    if-eqz v0, :cond_20

    move v0, v1

    .line 1141205
    :goto_b
    iget-object v3, p1, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    if-eqz v3, :cond_21

    move v3, v1

    .line 1141206
    :goto_c
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1141207
    :cond_c
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141208
    iget-object v0, p0, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6kn;->shouldBuzzDevice:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141209
    :cond_d
    iget-object v0, p0, LX/6kn;->adminText:Ljava/lang/String;

    if-eqz v0, :cond_22

    move v0, v1

    .line 1141210
    :goto_d
    iget-object v3, p1, LX/6kn;->adminText:Ljava/lang/String;

    if-eqz v3, :cond_23

    move v3, v1

    .line 1141211
    :goto_e
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1141212
    :cond_e
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141213
    iget-object v0, p0, LX/6kn;->adminText:Ljava/lang/String;

    iget-object v3, p1, LX/6kn;->adminText:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141214
    :cond_f
    iget-object v0, p0, LX/6kn;->tags:Ljava/util/List;

    if-eqz v0, :cond_24

    move v0, v1

    .line 1141215
    :goto_f
    iget-object v3, p1, LX/6kn;->tags:Ljava/util/List;

    if-eqz v3, :cond_25

    move v3, v1

    .line 1141216
    :goto_10
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1141217
    :cond_10
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141218
    iget-object v0, p0, LX/6kn;->tags:Ljava/util/List;

    iget-object v3, p1, LX/6kn;->tags:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141219
    :cond_11
    iget-object v0, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    if-eqz v0, :cond_26

    move v0, v1

    .line 1141220
    :goto_11
    iget-object v3, p1, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    if-eqz v3, :cond_27

    move v3, v1

    .line 1141221
    :goto_12
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 1141222
    :cond_12
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141223
    iget-object v0, p0, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    iget-object v3, p1, LX/6kn;->threadReadStateEffect:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141224
    :cond_13
    iget-object v0, p0, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    if-eqz v0, :cond_28

    move v0, v1

    .line 1141225
    :goto_13
    iget-object v3, p1, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    if-eqz v3, :cond_29

    move v3, v1

    .line 1141226
    :goto_14
    if-nez v0, :cond_14

    if-eqz v3, :cond_15

    .line 1141227
    :cond_14
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141228
    iget-object v0, p0, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6kn;->skipBumpThread:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_15
    move v2, v1

    .line 1141229
    goto/16 :goto_0

    :cond_16
    move v0, v2

    .line 1141230
    goto/16 :goto_1

    :cond_17
    move v3, v2

    .line 1141231
    goto/16 :goto_2

    :cond_18
    move v0, v2

    .line 1141232
    goto/16 :goto_3

    :cond_19
    move v3, v2

    .line 1141233
    goto/16 :goto_4

    :cond_1a
    move v0, v2

    .line 1141234
    goto/16 :goto_5

    :cond_1b
    move v3, v2

    .line 1141235
    goto/16 :goto_6

    :cond_1c
    move v0, v2

    .line 1141236
    goto/16 :goto_7

    :cond_1d
    move v3, v2

    .line 1141237
    goto/16 :goto_8

    :cond_1e
    move v0, v2

    .line 1141238
    goto/16 :goto_9

    :cond_1f
    move v3, v2

    .line 1141239
    goto/16 :goto_a

    :cond_20
    move v0, v2

    .line 1141240
    goto/16 :goto_b

    :cond_21
    move v3, v2

    .line 1141241
    goto/16 :goto_c

    :cond_22
    move v0, v2

    .line 1141242
    goto/16 :goto_d

    :cond_23
    move v3, v2

    .line 1141243
    goto/16 :goto_e

    :cond_24
    move v0, v2

    .line 1141244
    goto :goto_f

    :cond_25
    move v3, v2

    .line 1141245
    goto :goto_10

    :cond_26
    move v0, v2

    .line 1141246
    goto :goto_11

    :cond_27
    move v3, v2

    .line 1141247
    goto :goto_12

    :cond_28
    move v0, v2

    .line 1141248
    goto :goto_13

    :cond_29
    move v3, v2

    .line 1141249
    goto :goto_14
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1141173
    if-nez p1, :cond_1

    .line 1141174
    :cond_0
    :goto_0
    return v0

    .line 1141175
    :cond_1
    instance-of v1, p1, LX/6kn;

    if-eqz v1, :cond_0

    .line 1141176
    check-cast p1, LX/6kn;

    invoke-virtual {p0, p1}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1141172
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1141169
    sget-boolean v0, LX/6kn;->a:Z

    .line 1141170
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kn;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1141171
    return-object v0
.end method
