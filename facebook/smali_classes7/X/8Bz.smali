.class public LX/8Bz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/8Bz;


# instance fields
.field private final a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final c:LX/0SF;


# direct methods
.method public constructor <init>(LX/0SF;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1309998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309999
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/8Bz;->a:Ljava/util/LinkedList;

    .line 1310000
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/8Bz;->b:Ljava/util/LinkedList;

    .line 1310001
    iput-object p1, p0, LX/8Bz;->c:LX/0SF;

    .line 1310002
    return-void
.end method

.method public static a(LX/0QB;)LX/8Bz;
    .locals 4

    .prologue
    .line 1310013
    sget-object v0, LX/8Bz;->d:LX/8Bz;

    if-nez v0, :cond_1

    .line 1310014
    const-class v1, LX/8Bz;

    monitor-enter v1

    .line 1310015
    :try_start_0
    sget-object v0, LX/8Bz;->d:LX/8Bz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1310016
    if-eqz v2, :cond_0

    .line 1310017
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1310018
    new-instance p0, LX/8Bz;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SF;

    invoke-direct {p0, v3}, LX/8Bz;-><init>(LX/0SF;)V

    .line 1310019
    move-object v0, p0

    .line 1310020
    sput-object v0, LX/8Bz;->d:LX/8Bz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1310021
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1310022
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1310023
    :cond_1
    sget-object v0, LX/8Bz;->d:LX/8Bz;

    return-object v0

    .line 1310024
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1310025
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1310026
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 1310027
    iget-object v1, p0, LX/8Bz;->c:LX/0SF;

    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 1310028
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/8Bx;LX/8By;)V
    .locals 3

    .prologue
    .line 1310008
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8Bz;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 1310009
    iget-object v0, p0, LX/8Bz;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 1310010
    :cond_0
    iget-object v0, p0, LX/8Bz;->a:Ljava/util/LinkedList;

    const-string v1, "%s: %s %s"

    invoke-direct {p0}, LX/8Bz;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1310011
    monitor-exit p0

    return-void

    .line 1310012
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1310003
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8Bz;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 1310004
    iget-object v0, p0, LX/8Bz;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 1310005
    :cond_0
    iget-object v0, p0, LX/8Bz;->b:Ljava/util/LinkedList;

    const-string v1, "%s: %s %s"

    invoke-direct {p0}, LX/8Bz;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1310006
    monitor-exit p0

    return-void

    .line 1310007
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
