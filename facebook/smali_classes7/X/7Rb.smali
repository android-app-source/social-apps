.class public final enum LX/7Rb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Rb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Rb;

.field public static final enum NORMAL:LX/7Rb;

.field public static final enum SHOULD_STOP_STREAMING:LX/7Rb;

.field public static final enum WEAK:LX/7Rb;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1207141
    new-instance v0, LX/7Rb;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, LX/7Rb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Rb;->NORMAL:LX/7Rb;

    .line 1207142
    new-instance v0, LX/7Rb;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v3}, LX/7Rb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Rb;->WEAK:LX/7Rb;

    .line 1207143
    new-instance v0, LX/7Rb;

    const-string v1, "SHOULD_STOP_STREAMING"

    invoke-direct {v0, v1, v4}, LX/7Rb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Rb;->SHOULD_STOP_STREAMING:LX/7Rb;

    .line 1207144
    const/4 v0, 0x3

    new-array v0, v0, [LX/7Rb;

    sget-object v1, LX/7Rb;->NORMAL:LX/7Rb;

    aput-object v1, v0, v2

    sget-object v1, LX/7Rb;->WEAK:LX/7Rb;

    aput-object v1, v0, v3

    sget-object v1, LX/7Rb;->SHOULD_STOP_STREAMING:LX/7Rb;

    aput-object v1, v0, v4

    sput-object v0, LX/7Rb;->$VALUES:[LX/7Rb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1207135
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromInteger(I)LX/7Rb;
    .locals 1

    .prologue
    .line 1207136
    packed-switch p0, :pswitch_data_0

    .line 1207137
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1207138
    :pswitch_0
    sget-object v0, LX/7Rb;->NORMAL:LX/7Rb;

    goto :goto_0

    .line 1207139
    :pswitch_1
    sget-object v0, LX/7Rb;->WEAK:LX/7Rb;

    goto :goto_0

    .line 1207140
    :pswitch_2
    sget-object v0, LX/7Rb;->SHOULD_STOP_STREAMING:LX/7Rb;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Rb;
    .locals 1

    .prologue
    .line 1207134
    const-class v0, LX/7Rb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Rb;

    return-object v0
.end method

.method public static values()[LX/7Rb;
    .locals 1

    .prologue
    .line 1207133
    sget-object v0, LX/7Rb;->$VALUES:[LX/7Rb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Rb;

    return-object v0
.end method
