.class public interface abstract LX/6qd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DATA::",
        "Lcom/facebook/payments/checkout/model/CheckoutData;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a(LX/6qc;)V
.end method

.method public abstract a(LX/6qh;)V
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;I)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Rf;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "LX/0Rf",
            "<",
            "LX/6rp;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6tr;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "LX/6tr;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/73T;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "LX/73T;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;Landroid/os/Parcelable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Lcom/facebook/payments/checkout/CheckoutCommonParams;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Lcom/facebook/payments/contactinfo/model/NameContactInfo;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/model/PaymentsPin;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Lcom/facebook/payments/model/PaymentsPin;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethod;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/shipping/model/MailingAddress;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/shipping/model/ShippingOption;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Ljava/lang/Integer;",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;LX/0Px;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;Z)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/payments/checkout/model/CheckoutData;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;)Z"
        }
    .end annotation
.end method

.method public abstract b(Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;)V"
        }
    .end annotation
.end method

.method public abstract b(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract b(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract c(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract c(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method
