.class public final enum LX/748;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/748;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/748;

.field public static final enum PROFILE_PHOTO_PROMPT_CANCELED:LX/748;

.field public static final enum PROFILE_PHOTO_PROMPT_CLICKED:LX/748;

.field public static final enum PROFILE_PHOTO_PROMPT_DISMISSED:LX/748;

.field public static final enum PROFILE_PHOTO_PROMPT_DISPLAYED:LX/748;

.field public static final enum PROFILE_PHOTO_PROMPT_UPLOADED:LX/748;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1167513
    new-instance v0, LX/748;

    const-string v1, "PROFILE_PHOTO_PROMPT_CLICKED"

    const-string v2, "profile_photo_prompt_clicked"

    invoke-direct {v0, v1, v3, v2}, LX/748;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/748;->PROFILE_PHOTO_PROMPT_CLICKED:LX/748;

    .line 1167514
    new-instance v0, LX/748;

    const-string v1, "PROFILE_PHOTO_PROMPT_DISMISSED"

    const-string v2, "profile_photo_prompt_dismissed"

    invoke-direct {v0, v1, v4, v2}, LX/748;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/748;->PROFILE_PHOTO_PROMPT_DISMISSED:LX/748;

    .line 1167515
    new-instance v0, LX/748;

    const-string v1, "PROFILE_PHOTO_PROMPT_DISPLAYED"

    const-string v2, "profile_photo_prompt_displayed"

    invoke-direct {v0, v1, v5, v2}, LX/748;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/748;->PROFILE_PHOTO_PROMPT_DISPLAYED:LX/748;

    .line 1167516
    new-instance v0, LX/748;

    const-string v1, "PROFILE_PHOTO_PROMPT_UPLOADED"

    const-string v2, "profile_photo_prompt_uploaded"

    invoke-direct {v0, v1, v6, v2}, LX/748;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/748;->PROFILE_PHOTO_PROMPT_UPLOADED:LX/748;

    .line 1167517
    new-instance v0, LX/748;

    const-string v1, "PROFILE_PHOTO_PROMPT_CANCELED"

    const-string v2, "profile_photo_prompt_canceled"

    invoke-direct {v0, v1, v7, v2}, LX/748;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/748;->PROFILE_PHOTO_PROMPT_CANCELED:LX/748;

    .line 1167518
    const/4 v0, 0x5

    new-array v0, v0, [LX/748;

    sget-object v1, LX/748;->PROFILE_PHOTO_PROMPT_CLICKED:LX/748;

    aput-object v1, v0, v3

    sget-object v1, LX/748;->PROFILE_PHOTO_PROMPT_DISMISSED:LX/748;

    aput-object v1, v0, v4

    sget-object v1, LX/748;->PROFILE_PHOTO_PROMPT_DISPLAYED:LX/748;

    aput-object v1, v0, v5

    sget-object v1, LX/748;->PROFILE_PHOTO_PROMPT_UPLOADED:LX/748;

    aput-object v1, v0, v6

    sget-object v1, LX/748;->PROFILE_PHOTO_PROMPT_CANCELED:LX/748;

    aput-object v1, v0, v7

    sput-object v0, LX/748;->$VALUES:[LX/748;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1167519
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1167520
    iput-object p3, p0, LX/748;->name:Ljava/lang/String;

    .line 1167521
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/748;
    .locals 1

    .prologue
    .line 1167522
    const-class v0, LX/748;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/748;

    return-object v0
.end method

.method public static values()[LX/748;
    .locals 1

    .prologue
    .line 1167523
    sget-object v0, LX/748;->$VALUES:[LX/748;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/748;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1167524
    iget-object v0, p0, LX/748;->name:Ljava/lang/String;

    return-object v0
.end method
