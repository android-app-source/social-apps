.class public LX/7h0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/7gz;

.field public final b:LX/7gy;

.field public final c:J

.field public final d:Lcom/facebook/audience/model/AudienceControlData;

.field public final e:Landroid/net/Uri;

.field public final f:Landroid/net/Uri;

.field public final g:Landroid/net/Uri;

.field public final h:Z

.field public final i:Ljava/lang/String;

.field public final j:I

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:J

.field public n:LX/7gy;


# direct methods
.method private constructor <init>(LX/7gz;LX/7gy;JLcom/facebook/audience/model/AudienceControlData;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;ZLjava/lang/String;ILjava/lang/String;ZJLX/7gy;)V
    .locals 1

    .prologue
    .line 1225184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225185
    iput-object p1, p0, LX/7h0;->a:LX/7gz;

    .line 1225186
    iput-object p2, p0, LX/7h0;->b:LX/7gy;

    .line 1225187
    iput-wide p3, p0, LX/7h0;->c:J

    .line 1225188
    iput-object p5, p0, LX/7h0;->d:Lcom/facebook/audience/model/AudienceControlData;

    .line 1225189
    iput-object p6, p0, LX/7h0;->e:Landroid/net/Uri;

    .line 1225190
    iput-object p7, p0, LX/7h0;->f:Landroid/net/Uri;

    .line 1225191
    iput-object p8, p0, LX/7h0;->g:Landroid/net/Uri;

    .line 1225192
    iput-boolean p9, p0, LX/7h0;->h:Z

    .line 1225193
    iput-object p10, p0, LX/7h0;->i:Ljava/lang/String;

    .line 1225194
    iput p11, p0, LX/7h0;->j:I

    .line 1225195
    iput-object p12, p0, LX/7h0;->k:Ljava/lang/String;

    .line 1225196
    iput-boolean p13, p0, LX/7h0;->l:Z

    .line 1225197
    iput-wide p14, p0, LX/7h0;->m:J

    .line 1225198
    move-object/from16 v0, p16

    iput-object v0, p0, LX/7h0;->n:LX/7gy;

    .line 1225199
    return-void
.end method

.method public synthetic constructor <init>(LX/7gz;LX/7gy;JLcom/facebook/audience/model/AudienceControlData;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;ZLjava/lang/String;ILjava/lang/String;ZJLX/7gy;B)V
    .locals 0

    .prologue
    .line 1225183
    invoke-direct/range {p0 .. p16}, LX/7h0;-><init>(LX/7gz;LX/7gy;JLcom/facebook/audience/model/AudienceControlData;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;ZLjava/lang/String;ILjava/lang/String;ZJLX/7gy;)V

    return-void
.end method
