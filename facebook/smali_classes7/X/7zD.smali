.class public final LX/7zD;
.super LX/7z4;
.source ""


# instance fields
.field public final synthetic a:LX/7z8;


# direct methods
.method public constructor <init>(LX/7z8;)V
    .locals 0

    .prologue
    .line 1280855
    iput-object p1, p0, LX/7zD;->a:LX/7z8;

    invoke-direct {p0}, LX/7z4;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1280853
    iget-object v0, p0, LX/7zD;->a:LX/7z8;

    invoke-static {v0}, LX/7z8;->g(LX/7z8;)V

    .line 1280854
    return-void
.end method

.method public final a(Ljava/lang/Exception;Z)V
    .locals 2

    .prologue
    .line 1280838
    iget-object v0, p0, LX/7zD;->a:LX/7z8;

    iget-object v0, v0, LX/7z8;->a:LX/7yw;

    .line 1280839
    iget-object v1, v0, LX/7yw;->f:LX/7yz;

    move-object v0, v1

    .line 1280840
    invoke-virtual {v0, p1}, LX/7yz;->a(Ljava/lang/Exception;)V

    .line 1280841
    iget-object v0, p0, LX/7zD;->a:LX/7z8;

    const-string v1, "Failed GET request for fetching offset"

    invoke-static {v0, v1, p1, p2}, LX/7z8;->a$redex0(LX/7z8;Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 1280842
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1280843
    iget-object v0, p0, LX/7zD;->a:LX/7z8;

    .line 1280844
    :try_start_0
    invoke-static {p1}, LX/7z8;->b(Ljava/lang/String;)LX/7zG;

    move-result-object v1

    .line 1280845
    iget-boolean v2, v1, LX/7zG;->b:Z

    if-eqz v2, :cond_0

    .line 1280846
    iget-object v2, v0, LX/7z8;->d:LX/7yy;

    .line 1280847
    iget-wide v5, v2, LX/7yy;->d:J

    move-wide v3, v5

    .line 1280848
    iput-wide v3, v1, LX/7zG;->a:J

    .line 1280849
    :cond_0
    iget-wide v3, v1, LX/7zG;->a:J

    iget-boolean v1, v1, LX/7zG;->b:Z

    invoke-virtual {v0, v3, v4, v1}, LX/7z8;->a(JZ)V
    :try_end_0
    .catch LX/7z7; {:try_start_0 .. :try_end_0} :catch_0

    .line 1280850
    :goto_0
    return-void

    .line 1280851
    :catch_0
    move-exception v1

    .line 1280852
    const-string v2, "Failed to parse offset from GET request response"

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3}, LX/7z8;->a$redex0(LX/7z8;Ljava/lang/String;Ljava/lang/Exception;Z)V

    goto :goto_0
.end method
