.class public LX/71v;
.super LX/6E7;
.source ""

# interfaces
.implements LX/6vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E7;",
        "LX/6vq",
        "<",
        "LX/72c;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/6xb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field public c:Lcom/facebook/fbui/glyph/GlyphView;

.field private d:LX/72c;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1163765
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1163766
    const-class v0, LX/71v;

    invoke-static {v0, p0}, LX/71v;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1163767
    const v0, 0x7f031320

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1163768
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/71v;->setOrientation(I)V

    .line 1163769
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/71v;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a00d5

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1163770
    invoke-virtual {p0}, LX/71v;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b13b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1163771
    invoke-virtual {p0, v0, v0, v0, v0}, LX/71v;->setPadding(IIII)V

    .line 1163772
    const v0, 0x7f0d2c6a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, LX/71v;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 1163773
    const v0, 0x7f0d0b20

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/71v;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1163774
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/71v;

    invoke-static {p0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object p0

    check-cast p0, LX/6xb;

    iput-object p0, p1, LX/71v;->a:LX/6xb;

    return-void
.end method


# virtual methods
.method public final a(LX/72c;)V
    .locals 5

    .prologue
    const/16 v0, 0x8

    .line 1163786
    iput-object p1, p0, LX/71v;->d:LX/72c;

    .line 1163787
    iget-object v1, p0, LX/71v;->d:LX/72c;

    iget-object v1, v1, LX/72c;->e:Ljava/lang/String;

    .line 1163788
    iget-object v2, p0, LX/71v;->d:LX/72c;

    iget-object v2, v2, LX/72c;->d:Ljava/lang/String;

    .line 1163789
    sget-object v3, LX/71u;->a:[I

    iget-object v4, p0, LX/71v;->d:LX/72c;

    iget-object v4, v4, LX/72c;->a:LX/72f;

    invoke-virtual {v4}, LX/72f;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1163790
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/71v;->d:LX/72c;

    iget-object v2, v2, LX/72c;->a:LX/72f;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1163791
    :pswitch_0
    iget-object v3, p0, LX/71v;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v4, p0, LX/71v;->d:LX/72c;

    iget-boolean v4, v4, LX/72c;->f:Z

    if-eqz v4, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1163792
    iget-object v0, p0, LX/71v;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 1163793
    iget-object v0, p0, LX/71v;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1163794
    :goto_0
    return-void

    .line 1163795
    :pswitch_1
    iget-object v3, p0, LX/71v;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1163796
    iget-object v0, p0, LX/71v;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->a()V

    .line 1163797
    iget-object v0, p0, LX/71v;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1163798
    iget-object v0, p0, LX/71v;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onClick()V
    .locals 3

    .prologue
    .line 1163775
    iget-object v0, p0, LX/71v;->a:LX/6xb;

    iget-object v1, p0, LX/71v;->d:LX/72c;

    iget-object v1, v1, LX/72c;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v2, p0, LX/71v;->d:LX/72c;

    iget-object v2, v2, LX/72c;->g:Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/6xb;->c(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;)V

    .line 1163776
    sget-object v0, LX/71u;->a:[I

    iget-object v1, p0, LX/71v;->d:LX/72c;

    iget-object v1, v1, LX/72c;->a:LX/72f;

    invoke-virtual {v1}, LX/72f;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1163777
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/71v;->d:LX/72c;

    iget-object v2, v2, LX/72c;->a:LX/72f;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1163778
    :pswitch_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1163779
    const-string v1, "extra_user_action"

    iget-object v2, p0, LX/71v;->d:LX/72c;

    iget-object v2, v2, LX/72c;->g:Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163780
    const-string v1, "extra_section_type"

    sget-object v2, LX/729;->SHIPPING_ADDRESSES:LX/729;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1163781
    new-instance v1, LX/73T;

    sget-object v2, LX/73S;->USER_ACTION:LX/73S;

    invoke-direct {v1, v2, v0}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    .line 1163782
    invoke-virtual {p0, v1}, LX/6E7;->a(LX/73T;)V

    .line 1163783
    :goto_0
    return-void

    .line 1163784
    :pswitch_1
    iget-object v0, p0, LX/71v;->d:LX/72c;

    iget-object v0, v0, LX/72c;->b:Landroid/content/Intent;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1163785
    iget-object v0, p0, LX/71v;->d:LX/72c;

    iget-object v0, v0, LX/72c;->b:Landroid/content/Intent;

    iget-object v1, p0, LX/71v;->d:LX/72c;

    iget v1, v1, LX/72c;->c:I

    invoke-virtual {p0, v0, v1}, LX/6E7;->a(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
