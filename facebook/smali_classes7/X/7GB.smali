.class public LX/7GB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/7GB;


# instance fields
.field private final a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1188890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1188891
    iput-object p1, p0, LX/7GB;->a:LX/0SG;

    .line 1188892
    return-void
.end method

.method public static a(LX/0QB;)LX/7GB;
    .locals 4

    .prologue
    .line 1188893
    sget-object v0, LX/7GB;->b:LX/7GB;

    if-nez v0, :cond_1

    .line 1188894
    const-class v1, LX/7GB;

    monitor-enter v1

    .line 1188895
    :try_start_0
    sget-object v0, LX/7GB;->b:LX/7GB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1188896
    if-eqz v2, :cond_0

    .line 1188897
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1188898
    new-instance p0, LX/7GB;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {p0, v3}, LX/7GB;-><init>(LX/0SG;)V

    .line 1188899
    move-object v0, p0

    .line 1188900
    sput-object v0, LX/7GB;->b:LX/7GB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1188901
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1188902
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1188903
    :cond_1
    sget-object v0, LX/7GB;->b:LX/7GB;

    return-object v0

    .line 1188904
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1188905
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/7Fx;Lcom/facebook/sync/analytics/FullRefreshReason;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Fx",
            "<**>;",
            "Lcom/facebook/sync/analytics/FullRefreshReason;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1188906
    const/4 v0, 0x1

    invoke-interface {p0, v0, p1}, LX/7Fx;->a(ZLcom/facebook/sync/analytics/FullRefreshReason;)V

    .line 1188907
    return-void
.end method


# virtual methods
.method public final b(LX/7Fx;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Fx",
            "<**>;)Z"
        }
    .end annotation

    .prologue
    .line 1188908
    invoke-interface {p1}, LX/7Fx;->d()J

    move-result-wide v0

    .line 1188909
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    const-wide/32 v2, 0x493e0

    add-long/2addr v0, v2

    iget-object v2, p0, LX/7GB;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/7Fx;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Fx",
            "<**>;)J"
        }
    .end annotation

    .prologue
    const-wide/32 v4, 0x493e0

    .line 1188910
    invoke-interface {p1}, LX/7Fx;->d()J

    move-result-wide v0

    .line 1188911
    iget-object v2, p0, LX/7GB;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 1188912
    cmp-long v2, v0, v4

    if-lez v2, :cond_0

    .line 1188913
    const-wide/16 v0, 0x0

    .line 1188914
    :goto_0
    return-wide v0

    :cond_0
    sub-long v0, v4, v0

    const-wide/16 v2, 0xa

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public final d(LX/7Fx;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Fx",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 1188915
    iget-object v0, p0, LX/7GB;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, LX/7Fx;->b(J)V

    .line 1188916
    const/4 v0, 0x0

    sget-object v1, Lcom/facebook/sync/analytics/FullRefreshReason;->g:Lcom/facebook/sync/analytics/FullRefreshReason;

    invoke-interface {p1, v0, v1}, LX/7Fx;->a(ZLcom/facebook/sync/analytics/FullRefreshReason;)V

    .line 1188917
    return-void
.end method
