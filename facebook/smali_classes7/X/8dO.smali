.class public final LX/8dO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1375299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/8dO;
    .locals 2

    .prologue
    .line 1375300
    new-instance v0, LX/8dO;

    invoke-direct {v0}, LX/8dO;-><init>()V

    .line 1375301
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->t()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    move-result-object v1

    iput-object v1, v0, LX/8dO;->a:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    .line 1375302
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->fi_()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/8dO;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1375303
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dO;->c:Ljava/lang/String;

    .line 1375304
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->k()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    iput-object v1, v0, LX/8dO;->d:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1375305
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->u()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    move-result-object v1

    iput-object v1, v0, LX/8dO;->e:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    .line 1375306
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->v()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    iput-object v1, v0, LX/8dO;->f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 1375307
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->n()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/8dO;->g:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1375308
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->w()Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;

    move-result-object v1

    iput-object v1, v0, LX/8dO;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;

    .line 1375309
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->p()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/8dO;->i:LX/0Px;

    .line 1375310
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    iput-object v1, v0, LX/8dO;->j:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1375311
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->x()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object v1

    iput-object v1, v0, LX/8dO;->k:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    .line 1375312
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->y()Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;

    move-result-object v1

    iput-object v1, v0, LX/8dO;->l:Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;

    .line 1375313
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;
    .locals 14

    .prologue
    .line 1375314
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1375315
    iget-object v1, p0, LX/8dO;->a:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1375316
    iget-object v2, p0, LX/8dO;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1375317
    iget-object v3, p0, LX/8dO;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1375318
    iget-object v4, p0, LX/8dO;->d:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1375319
    iget-object v5, p0, LX/8dO;->e:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1375320
    iget-object v6, p0, LX/8dO;->f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1375321
    iget-object v7, p0, LX/8dO;->g:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1375322
    iget-object v8, p0, LX/8dO;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1375323
    iget-object v9, p0, LX/8dO;->i:LX/0Px;

    invoke-virtual {v0, v9}, LX/186;->c(Ljava/util/List;)I

    move-result v9

    .line 1375324
    iget-object v10, p0, LX/8dO;->j:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 1375325
    iget-object v11, p0, LX/8dO;->k:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1375326
    iget-object v12, p0, LX/8dO;->l:Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1375327
    const/16 v13, 0xc

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1375328
    const/4 v13, 0x0

    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 1375329
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1375330
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1375331
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1375332
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1375333
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1375334
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1375335
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1375336
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1375337
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1375338
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1375339
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 1375340
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1375341
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1375342
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1375343
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1375344
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1375345
    new-instance v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    invoke-direct {v1, v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;-><init>(LX/15i;)V

    .line 1375346
    iget-object v0, p0, LX/8dO;->b:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 1375347
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->fi_()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iget-object v2, p0, LX/8dO;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->L_()LX/0x2;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/model/GraphQLStory;->a(LX/0x2;)V

    .line 1375348
    :cond_0
    iget-object v0, p0, LX/8dO;->g:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 1375349
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->n()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iget-object v2, p0, LX/8dO;->g:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->L_()LX/0x2;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/model/GraphQLStory;->a(LX/0x2;)V

    .line 1375350
    :cond_1
    return-object v1
.end method
