.class public final LX/8JJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

.field private b:F


# direct methods
.method public constructor <init>(Lcom/facebook/photos/tagging/shared/FaceBoxesView;)V
    .locals 1

    .prologue
    .line 1328161
    iput-object p1, p0, LX/8JJ;->a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1328162
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, LX/8JJ;->b:F

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/photos/tagging/shared/FaceBoxesView;B)V
    .locals 0

    .prologue
    .line 1328174
    invoke-direct {p0, p1}, LX/8JJ;-><init>(Lcom/facebook/photos/tagging/shared/FaceBoxesView;)V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6

    .prologue
    .line 1328163
    iget v0, p0, LX/8JJ;->b:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 1328164
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, LX/8JJ;->b:F

    .line 1328165
    iget-object v0, p0, LX/8JJ;->a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-static {v0}, LX/0vv;->d(Landroid/view/View;)V

    .line 1328166
    :goto_0
    return-void

    .line 1328167
    :cond_0
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1328168
    iget-object v1, p0, LX/8JJ;->a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v1}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    .line 1328169
    const v2, 0x3e99999a    # 0.3f

    mul-float/2addr v2, v1

    .line 1328170
    iget v3, p0, LX/8JJ;->b:F

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    mul-float/2addr v3, v1

    .line 1328171
    iget v4, p0, LX/8JJ;->b:F

    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    mul-float/2addr v1, v4

    add-float/2addr v1, v2

    .line 1328172
    iget-object v2, p0, LX/8JJ;->a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    float-to-int v3, v3

    const/4 v4, 0x0

    float-to-int v1, v1

    iget-object v5, p0, LX/8JJ;->a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v5}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->getHeight()I

    move-result v5

    invoke-static {v2, v3, v4, v1, v5}, LX/0vv;->a(Landroid/view/View;IIII)V

    .line 1328173
    iput v0, p0, LX/8JJ;->b:F

    goto :goto_0
.end method
