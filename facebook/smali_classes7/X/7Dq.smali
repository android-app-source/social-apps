.class public final LX/7Dq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Ia;


# instance fields
.field public final synthetic a:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;)V
    .locals 0

    .prologue
    .line 1183752
    iput-object p1, p0, LX/7Dq;->a:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;B)V
    .locals 0

    .prologue
    .line 1183751
    invoke-direct {p0, p1}, LX/7Dq;-><init>(Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;)V

    return-void
.end method


# virtual methods
.method public onClick()V
    .locals 6

    .prologue
    const/16 v4, 0x3e8

    .line 1183743
    iget-object v0, p0, LX/7Dq;->a:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iget-object v0, v0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->a:LX/1xG;

    iget-object v1, p0, LX/7Dq;->a:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iget-object v1, v1, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->i:Ljava/lang/String;

    iget-object v2, p0, LX/7Dq;->a:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iget-object v2, v2, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->h:LX/7Dj;

    .line 1183744
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "spherical_photo_tap_heading_indicator"

    invoke-direct {v3, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1183745
    invoke-static {v0, v3, v1, v2}, LX/1xG;->a(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 1183746
    iget-object v0, p0, LX/7Dq;->a:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iget-object v0, v0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->b:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    if-eqz v0, :cond_0

    .line 1183747
    iget-object v0, p0, LX/7Dq;->a:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iget-object v0, v0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->b:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->a()V

    .line 1183748
    :cond_0
    iget-object v0, p0, LX/7Dq;->a:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iget-object v0, v0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->d:LX/3IP;

    iget-object v1, p0, LX/7Dq;->a:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iget-object v1, v1, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->c:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    invoke-virtual {v1}, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->e()F

    move-result v1

    neg-float v1, v1

    iget-object v2, p0, LX/7Dq;->a:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iget-object v2, v2, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->c:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    invoke-virtual {v2}, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->c()F

    move-result v2

    iget-object v3, p0, LX/7Dq;->a:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    invoke-virtual {v0, v1, v2, v4, v3}, LX/3IP;->a(FFILX/3II;)V

    .line 1183749
    iget-object v0, p0, LX/7Dq;->a:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iget-object v0, v0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->d:LX/3IP;

    iget-object v1, p0, LX/7Dq;->a:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iget-object v1, v1, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->c:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    invoke-virtual {v1}, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->b()F

    move-result v1

    iget-object v2, p0, LX/7Dq;->a:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    invoke-virtual {v0, v1, v4, v2}, LX/3IP;->a(FILX/3II;)V

    .line 1183750
    return-void
.end method
