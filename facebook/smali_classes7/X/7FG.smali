.class public LX/7FG;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/03V;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/1Uj;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/structuredsurvey/StructuredSurveyController;

.field public g:LX/7FH;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1186071
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NaRF:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, LX/7FG;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7FG;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/1Uj;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/text/CustomFontUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1186072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1186073
    iput-object p1, p0, LX/7FG;->b:LX/03V;

    .line 1186074
    iput-object p2, p0, LX/7FG;->e:LX/0Ot;

    .line 1186075
    iput-object p3, p0, LX/7FG;->c:Ljava/util/concurrent/ExecutorService;

    .line 1186076
    iput-object p4, p0, LX/7FG;->d:LX/1Uj;

    .line 1186077
    return-void
.end method

.method public static a(Landroid/text/SpannableStringBuilder;Landroid/text/style/CharacterStyle;Ljava/lang/String;LX/1yL;)V
    .locals 4

    .prologue
    .line 1186078
    :try_start_0
    invoke-static {p2, p3}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v0

    .line 1186079
    invoke-static {p1}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v1

    .line 1186080
    iget v2, v0, LX/1yN;->a:I

    move v2, v2

    .line 1186081
    invoke-virtual {v0}, LX/1yN;->c()I

    move-result v0

    const/16 v3, 0x11

    invoke-virtual {p0, v1, v2, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 1186082
    :goto_0
    return-void

    .line 1186083
    :catch_0
    move-exception v0

    .line 1186084
    const-string v1, "SurveyNotificationHelper"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/7FG;LX/15i;I)Landroid/text/Spannable;
    .locals 10

    .prologue
    .line 1186041
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1186042
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1186043
    iget-object v2, p0, LX/7FG;->d:LX/1Uj;

    invoke-virtual {v2}, LX/1Uj;->a()Landroid/text/style/MetricAffectingSpan;

    move-result-object v2

    move-object v2, v2

    .line 1186044
    iget-object v3, p0, LX/7FG;->d:LX/1Uj;

    invoke-virtual {v3}, LX/1Uj;->b()Landroid/text/style/MetricAffectingSpan;

    move-result-object v3

    move-object v3, v3

    .line 1186045
    invoke-static {v3}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v3

    .line 1186046
    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v5, 0x21

    invoke-virtual {v1, v3, v4, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1186047
    const/4 p0, 0x2

    const v9, -0x5f41ddb5

    const v5, -0x63ee10a9

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1186048
    invoke-static {p1, p2, v3, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_3

    invoke-static {p1, p2, v3, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 1186049
    invoke-static {p1, p2, v3, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 1186050
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1186051
    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v5

    .line 1186052
    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 1186053
    new-instance v7, LX/1yL;

    invoke-virtual {v6, v5, v3}, LX/15i;->j(II)I

    move-result v8

    invoke-virtual {v6, v5, v4}, LX/15i;->j(II)I

    move-result v5

    invoke-direct {v7, v8, v5}, LX/1yL;-><init>(II)V

    .line 1186054
    invoke-virtual {p1, p2, p0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v5, v7}, LX/7FG;->a(Landroid/text/SpannableStringBuilder;Landroid/text/style/CharacterStyle;Ljava/lang/String;LX/1yL;)V

    goto :goto_4

    .line 1186055
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v4

    goto :goto_2

    :cond_3
    move v0, v4

    goto :goto_2

    .line 1186056
    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 1186057
    :cond_5
    invoke-static {p1, p2, v4, v9}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_5
    if-eqz v0, :cond_9

    .line 1186058
    invoke-static {p1, p2, v4, v9}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 1186059
    if-eqz v0, :cond_7

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_6
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v3

    :goto_7
    if-eqz v0, :cond_b

    .line 1186060
    invoke-static {p1, p2, v4, v9}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 1186061
    if-eqz v0, :cond_a

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_8
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :goto_9
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1186062
    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v5

    .line 1186063
    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 1186064
    new-instance v7, LX/1yL;

    invoke-virtual {v6, v5, v3}, LX/15i;->j(II)I

    move-result v8

    invoke-virtual {v6, v5, v4}, LX/15i;->j(II)I

    move-result v5

    invoke-direct {v7, v8, v5}, LX/1yL;-><init>(II)V

    .line 1186065
    invoke-virtual {p1, p2, p0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v5, v7}, LX/7FG;->a(Landroid/text/SpannableStringBuilder;Landroid/text/style/CharacterStyle;Ljava/lang/String;LX/1yL;)V

    goto :goto_9

    .line 1186066
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_5

    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_6

    :cond_8
    move v0, v4

    goto :goto_7

    :cond_9
    move v0, v4

    goto :goto_7

    .line 1186067
    :cond_a
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_8

    .line 1186068
    :cond_b
    return-object v1
.end method

.method public static e(LX/7FG;)V
    .locals 1

    .prologue
    .line 1186069
    const/4 v0, 0x0

    iput-object v0, p0, LX/7FG;->g:LX/7FH;

    .line 1186070
    return-void
.end method
