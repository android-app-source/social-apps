.class public LX/7Qo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Qf;


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field private final d:LX/7Qn;


# direct methods
.method public constructor <init>(LX/04D;LX/0JP;LX/0J9;JILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1204802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204803
    new-instance v0, LX/7Qn;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-direct/range {v0 .. v5}, LX/7Qn;-><init>(LX/04D;LX/0JP;LX/0J9;J)V

    iput-object v0, p0, LX/7Qo;->d:LX/7Qn;

    .line 1204804
    iput p6, p0, LX/7Qo;->a:I

    .line 1204805
    iput-object p7, p0, LX/7Qo;->b:Ljava/lang/String;

    .line 1204806
    iput-object p8, p0, LX/7Qo;->c:Ljava/lang/String;

    .line 1204807
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 1204808
    iget-object v0, p0, LX/7Qo;->d:LX/7Qn;

    invoke-virtual {v0, p1}, LX/7Qn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1204809
    sget-object v0, LX/0JS;->NUMBER_OF_SECTION:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget v1, p0, LX/7Qo;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204810
    sget-object v0, LX/0JS;->REACTION_UNIT_TYPE:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget-object v1, p0, LX/7Qo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204811
    sget-object v0, LX/0JS;->REACTION_COMPONENT_TRACKING_FIELD:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget-object v1, p0, LX/7Qo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204812
    return-void
.end method
