.class public abstract LX/7cG;
.super Landroid/os/Binder;
.source ""

# interfaces
.implements Lcom/google/android/gms/maps/model/internal/IPolylineDelegate;


# direct methods
.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/maps/model/internal/IPolylineDelegate;
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.maps.model.internal.IPolylineDelegate"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/maps/model/internal/IPolylineDelegate;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/maps/model/internal/IPolylineDelegate;

    goto :goto_0

    :cond_1
    new-instance v0, LX/7cF;

    invoke-direct {v0, p0}, LX/7cF;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method
