.class public LX/6w4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6w3;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6w3",
        "<",
        "Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6vw;


# direct methods
.method public constructor <init>(LX/6vw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1157405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157406
    iput-object p1, p0, LX/6w4;->a:LX/6vw;

    .line 1157407
    return-void
.end method


# virtual methods
.method public final a(LX/6qh;LX/70k;)V
    .locals 0

    .prologue
    .line 1157408
    return-void
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1157409
    check-cast p1, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;

    const/4 v0, -0x1

    .line 1157410
    packed-switch p2, :pswitch_data_0

    .line 1157411
    :cond_0
    :goto_0
    return-void

    .line 1157412
    :pswitch_0
    if-ne p3, v0, :cond_0

    if-eqz p4, :cond_0

    .line 1157413
    :pswitch_1
    if-ne p3, v0, :cond_0

    .line 1157414
    if-eqz p4, :cond_1

    .line 1157415
    const-string v0, "contact_info"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1157416
    iget-object v1, p0, LX/6w4;->a:LX/6vw;

    new-instance v2, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;-><init>(Z)V

    invoke-interface {v0}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->d()LX/6vb;

    move-result-object v3

    invoke-virtual {v3}, LX/6vb;->getSectionType()LX/6va;

    move-result-object v3

    invoke-interface {v0}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v2, v3, v0}, LX/6vv;->a(Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;LX/6vZ;Ljava/lang/String;)V

    goto :goto_0

    .line 1157417
    :cond_1
    iget-object v0, p0, LX/6w4;->a:LX/6vw;

    .line 1157418
    iget-object v1, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    move-object v1, v1

    .line 1157419
    invoke-virtual {v0, p1, v1}, LX/6vv;->a(Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
