.class public LX/73e;
.super LX/6E7;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/text/BetterTextView;

.field public b:Lcom/facebook/fig/button/FigToggleButton;

.field public c:Lcom/facebook/fig/button/FigToggleButton;

.field public d:I

.field public e:I

.field public f:I

.field public g:LX/6x1;

.field public h:Ljava/text/NumberFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1166148
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1166149
    const p1, 0x7f0310bb

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1166150
    const p1, 0x7f0d27c6

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, LX/73e;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1166151
    const p1, 0x7f0d27c5

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fig/button/FigToggleButton;

    iput-object p1, p0, LX/73e;->b:Lcom/facebook/fig/button/FigToggleButton;

    .line 1166152
    const p1, 0x7f0d27c7

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fig/button/FigToggleButton;

    iput-object p1, p0, LX/73e;->c:Lcom/facebook/fig/button/FigToggleButton;

    .line 1166153
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object p1

    iput-object p1, p0, LX/73e;->h:Ljava/text/NumberFormat;

    .line 1166154
    return-void
.end method

.method public static b(LX/73e;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1166155
    iget-object v0, p0, LX/73e;->a:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p0, LX/73e;->h:Ljava/text/NumberFormat;

    iget v4, p0, LX/73e;->e:I

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1166156
    iget-object v3, p0, LX/73e;->b:Lcom/facebook/fig/button/FigToggleButton;

    iget v0, p0, LX/73e;->e:I

    iget v4, p0, LX/73e;->d:I

    if-le v0, v4, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/fig/button/FigToggleButton;->setChecked(Z)V

    .line 1166157
    iget-object v0, p0, LX/73e;->c:Lcom/facebook/fig/button/FigToggleButton;

    iget v3, p0, LX/73e;->e:I

    iget v4, p0, LX/73e;->f:I

    if-ge v3, v4, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigToggleButton;->setChecked(Z)V

    .line 1166158
    iget-object v0, p0, LX/73e;->g:LX/6x1;

    if-eqz v0, :cond_0

    .line 1166159
    iget-object v0, p0, LX/73e;->g:LX/6x1;

    iget v1, p0, LX/73e;->e:I

    .line 1166160
    iget-object v2, v0, LX/6x1;->a:LX/6x2;

    iget-object v2, v2, LX/6x2;->c:Landroid/content/Intent;

    const-string v3, "extra_quantity"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1166161
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 1166162
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1166163
    goto :goto_1
.end method
