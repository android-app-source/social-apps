.class public final enum LX/7gd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gd;

.field public static final enum CACHE:LX/7gd;

.field public static final enum DEFAULT:LX/7gd;

.field public static final enum SERVER:LX/7gd;

.field public static final enum STORY_TRAY:LX/7gd;

.field public static final enum VIEWER:LX/7gd;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1224617
    new-instance v0, LX/7gd;

    const-string v1, "SERVER"

    const-string v2, "server"

    invoke-direct {v0, v1, v3, v2}, LX/7gd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gd;->SERVER:LX/7gd;

    .line 1224618
    new-instance v0, LX/7gd;

    const-string v1, "CACHE"

    const-string v2, "cache"

    invoke-direct {v0, v1, v4, v2}, LX/7gd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gd;->CACHE:LX/7gd;

    .line 1224619
    new-instance v0, LX/7gd;

    const-string v1, "DEFAULT"

    const-string v2, "default"

    invoke-direct {v0, v1, v5, v2}, LX/7gd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gd;->DEFAULT:LX/7gd;

    .line 1224620
    new-instance v0, LX/7gd;

    const-string v1, "VIEWER"

    const-string v2, "viewer"

    invoke-direct {v0, v1, v6, v2}, LX/7gd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gd;->VIEWER:LX/7gd;

    .line 1224621
    new-instance v0, LX/7gd;

    const-string v1, "STORY_TRAY"

    const-string v2, "story_tray"

    invoke-direct {v0, v1, v7, v2}, LX/7gd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gd;->STORY_TRAY:LX/7gd;

    .line 1224622
    const/4 v0, 0x5

    new-array v0, v0, [LX/7gd;

    sget-object v1, LX/7gd;->SERVER:LX/7gd;

    aput-object v1, v0, v3

    sget-object v1, LX/7gd;->CACHE:LX/7gd;

    aput-object v1, v0, v4

    sget-object v1, LX/7gd;->DEFAULT:LX/7gd;

    aput-object v1, v0, v5

    sget-object v1, LX/7gd;->VIEWER:LX/7gd;

    aput-object v1, v0, v6

    sget-object v1, LX/7gd;->STORY_TRAY:LX/7gd;

    aput-object v1, v0, v7

    sput-object v0, LX/7gd;->$VALUES:[LX/7gd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1224613
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1224614
    iput-object p3, p0, LX/7gd;->mName:Ljava/lang/String;

    .line 1224615
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gd;
    .locals 1

    .prologue
    .line 1224623
    const-class v0, LX/7gd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gd;

    return-object v0
.end method

.method public static values()[LX/7gd;
    .locals 1

    .prologue
    .line 1224616
    sget-object v0, LX/7gd;->$VALUES:[LX/7gd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gd;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224612
    iget-object v0, p0, LX/7gd;->mName:Ljava/lang/String;

    return-object v0
.end method
