.class public LX/8Wr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/8Wi;

.field private final b:LX/8Te;

.field private final c:LX/8TS;

.field public d:LX/8Wj;


# direct methods
.method public constructor <init>(LX/8Wi;LX/8Te;LX/8TS;Landroid/support/v7/widget/RecyclerView;)V
    .locals 2
    .param p4    # Landroid/support/v7/widget/RecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1354431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1354432
    iput-object p1, p0, LX/8Wr;->a:LX/8Wi;

    .line 1354433
    iput-object p2, p0, LX/8Wr;->b:LX/8Te;

    .line 1354434
    iput-object p3, p0, LX/8Wr;->c:LX/8TS;

    .line 1354435
    iget-object v0, p0, LX/8Wr;->a:LX/8Wi;

    invoke-virtual {p4, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1354436
    iget-object v0, p0, LX/8Wr;->a:LX/8Wi;

    new-instance v1, LX/8Wp;

    invoke-direct {v1, p0}, LX/8Wp;-><init>(LX/8Wr;)V

    .line 1354437
    iput-object v1, v0, LX/8Wi;->c:LX/8Wp;

    .line 1354438
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1354439
    iget-object v0, p0, LX/8Wr;->c:LX/8TS;

    .line 1354440
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1354441
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8Wr;->c:LX/8TS;

    invoke-virtual {v0}, LX/8TS;->c()LX/8Vb;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1354442
    iget-object v0, p0, LX/8Wr;->b:LX/8Te;

    iget-object v1, p0, LX/8Wr;->c:LX/8TS;

    .line 1354443
    iget-object v2, v1, LX/8TS;->e:LX/8TO;

    move-object v1, v2

    .line 1354444
    iget-object v2, v1, LX/8TO;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1354445
    iget-object v2, p0, LX/8Wr;->c:LX/8TS;

    invoke-virtual {v2}, LX/8TS;->c()LX/8Vb;

    move-result-object v2

    iget-object v2, v2, LX/8Vb;->a:Ljava/lang/String;

    iget-object v3, p0, LX/8Wr;->c:LX/8TS;

    .line 1354446
    iget-object v4, v3, LX/8TS;->j:LX/8TP;

    move-object v3, v4

    .line 1354447
    new-instance v4, LX/8Wq;

    invoke-direct {v4, p0}, LX/8Wq;-><init>(LX/8Wr;)V

    invoke-virtual {v0, v1, v2, v3, v4}, LX/8Te;->a(Ljava/lang/String;Ljava/lang/String;LX/8TP;LX/8Td;)V

    .line 1354448
    :cond_0
    return-void
.end method
