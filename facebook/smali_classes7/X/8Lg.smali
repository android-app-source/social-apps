.class public final LX/8Lg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/107;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/upload/progresspage/CompostFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/progresspage/CompostFragment;)V
    .locals 0

    .prologue
    .line 1333538
    iput-object p1, p0, LX/8Lg;->a:Lcom/facebook/photos/upload/progresspage/CompostFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1333539
    iget-object v0, p0, LX/8Lg;->a:Lcom/facebook/photos/upload/progresspage/CompostFragment;

    .line 1333540
    iget-object v1, v0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->b:LX/1RW;

    .line 1333541
    iget-object v2, v1, LX/1RW;->a:LX/0Zb;

    const-string p0, "open_simple_picker"

    invoke-static {v1, p0}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v2, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1333542
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Landroid/app/Activity;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 1333543
    sget-object v2, LX/21D;->COMPOST:LX/21D;

    const-string p0, "launchComposerFromCompost"

    invoke-static {v2, p0}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    .line 1333544
    new-instance p0, LX/8AA;

    sget-object p1, LX/8AB;->COMPOST:LX/8AB;

    invoke-direct {p0, p1}, LX/8AA;-><init>(LX/8AB;)V

    .line 1333545
    iput-object v2, p0, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1333546
    move-object v2, p0

    .line 1333547
    invoke-static {v1, v2}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v1

    .line 1333548
    iget-object v2, v0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->h:Lcom/facebook/content/SecureContextHelper;

    const/4 p0, 0x1

    invoke-interface {v2, v1, p0, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1333549
    return-void
.end method
