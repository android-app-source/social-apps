.class public final LX/7sN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 1264743
    const-wide/16 v14, 0x0

    .line 1264744
    const/4 v13, 0x0

    .line 1264745
    const/4 v12, 0x0

    .line 1264746
    const/4 v11, 0x0

    .line 1264747
    const/4 v10, 0x0

    .line 1264748
    const/4 v9, 0x0

    .line 1264749
    const/4 v8, 0x0

    .line 1264750
    const-wide/16 v6, 0x0

    .line 1264751
    const/4 v5, 0x0

    .line 1264752
    const/4 v4, 0x0

    .line 1264753
    const/4 v3, 0x0

    .line 1264754
    const/4 v2, 0x0

    .line 1264755
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_e

    .line 1264756
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1264757
    const/4 v2, 0x0

    .line 1264758
    :goto_0
    return v2

    .line 1264759
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_a

    .line 1264760
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1264761
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1264762
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1264763
    const-string v6, "end_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1264764
    const/4 v2, 0x1

    .line 1264765
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1264766
    :cond_1
    const-string v6, "eventProfilePicture"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1264767
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 1264768
    :cond_2
    const-string v6, "event_place"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1264769
    invoke-static/range {p0 .. p1}, LX/7rw;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 1264770
    :cond_3
    const-string v6, "event_ticket_provider"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1264771
    invoke-static/range {p0 .. p1}, LX/7sM;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 1264772
    :cond_4
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1264773
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 1264774
    :cond_5
    const-string v6, "is_all_day"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1264775
    const/4 v2, 0x1

    .line 1264776
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move v14, v6

    goto :goto_1

    .line 1264777
    :cond_6
    const-string v6, "name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1264778
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 1264779
    :cond_7
    const-string v6, "start_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1264780
    const/4 v2, 0x1

    .line 1264781
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v8, v2

    move-wide v12, v6

    goto/16 :goto_1

    .line 1264782
    :cond_8
    const-string v6, "timezone"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1264783
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 1264784
    :cond_9
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1264785
    :cond_a
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1264786
    if-eqz v3, :cond_b

    .line 1264787
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1264788
    :cond_b
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1264789
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1264790
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1264791
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1264792
    if-eqz v9, :cond_c

    .line 1264793
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->a(IZ)V

    .line 1264794
    :cond_c
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1264795
    if-eqz v8, :cond_d

    .line 1264796
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1264797
    :cond_d
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1264798
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move/from16 v16, v11

    move/from16 v17, v12

    move/from16 v18, v13

    move-wide v12, v6

    move v11, v8

    move v8, v2

    move/from16 v19, v9

    move v9, v3

    move v3, v4

    move/from16 v20, v5

    move-wide v4, v14

    move/from16 v14, v19

    move v15, v10

    move/from16 v10, v20

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1264799
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1264800
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1264801
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 1264802
    const-string v2, "end_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264803
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1264804
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1264805
    if-eqz v0, :cond_1

    .line 1264806
    const-string v1, "eventProfilePicture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264807
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1264808
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1264809
    if-eqz v0, :cond_2

    .line 1264810
    const-string v1, "event_place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264811
    invoke-static {p0, v0, p2, p3}, LX/7rw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1264812
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1264813
    if-eqz v0, :cond_3

    .line 1264814
    const-string v1, "event_ticket_provider"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264815
    invoke-static {p0, v0, p2, p3}, LX/7sM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1264816
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1264817
    if-eqz v0, :cond_4

    .line 1264818
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264819
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1264820
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1264821
    if-eqz v0, :cond_5

    .line 1264822
    const-string v1, "is_all_day"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264823
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1264824
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1264825
    if-eqz v0, :cond_6

    .line 1264826
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264827
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1264828
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1264829
    cmp-long v2, v0, v4

    if-eqz v2, :cond_7

    .line 1264830
    const-string v2, "start_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264831
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1264832
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1264833
    if-eqz v0, :cond_8

    .line 1264834
    const-string v1, "timezone"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1264835
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1264836
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1264837
    return-void
.end method
