.class public final enum LX/7mH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7mH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7mH;

.field public static final enum ENABLE_POST_AFTER_AUTOTAGGING:LX/7mH;

.field public static final enum TIMEOUT_AUTOTAGGING:LX/7mH;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1236779
    new-instance v0, LX/7mH;

    const-string v1, "TIMEOUT_AUTOTAGGING"

    invoke-direct {v0, v1, v2}, LX/7mH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7mH;->TIMEOUT_AUTOTAGGING:LX/7mH;

    .line 1236780
    new-instance v0, LX/7mH;

    const-string v1, "ENABLE_POST_AFTER_AUTOTAGGING"

    invoke-direct {v0, v1, v3}, LX/7mH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7mH;->ENABLE_POST_AFTER_AUTOTAGGING:LX/7mH;

    .line 1236781
    const/4 v0, 0x2

    new-array v0, v0, [LX/7mH;

    sget-object v1, LX/7mH;->TIMEOUT_AUTOTAGGING:LX/7mH;

    aput-object v1, v0, v2

    sget-object v1, LX/7mH;->ENABLE_POST_AFTER_AUTOTAGGING:LX/7mH;

    aput-object v1, v0, v3

    sput-object v0, LX/7mH;->$VALUES:[LX/7mH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1236782
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7mH;
    .locals 1

    .prologue
    .line 1236783
    const-class v0, LX/7mH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7mH;

    return-object v0
.end method

.method public static values()[LX/7mH;
    .locals 1

    .prologue
    .line 1236784
    sget-object v0, LX/7mH;->$VALUES:[LX/7mH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7mH;

    return-object v0
.end method
