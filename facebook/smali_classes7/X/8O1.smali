.class public final LX/8O1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/photos/upload/operation/UploadOperation;

.field public b:LX/8Ob;

.field public c:I

.field public d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

.field public e:LX/0cW;

.field public f:LX/8OL;

.field public g:LX/8On;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ob;ILX/0cW;LX/8OL;LX/8On;)V
    .locals 2

    .prologue
    .line 1338229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1338230
    iput-object p1, p0, LX/8O1;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1338231
    iput-object p2, p0, LX/8O1;->b:LX/8Ob;

    .line 1338232
    iput p3, p0, LX/8O1;->c:I

    .line 1338233
    iput-object p4, p0, LX/8O1;->e:LX/0cW;

    .line 1338234
    iput-object p5, p0, LX/8O1;->f:LX/8OL;

    .line 1338235
    if-ltz p3, :cond_0

    iget-object v0, p2, LX/8Ob;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p3, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1338236
    iget-object v0, p2, LX/8Ob;->B:Ljava/util/List;

    iget v1, p0, LX/8O1;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iput-object v0, p0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    .line 1338237
    iput-object p6, p0, LX/8O1;->g:LX/8On;

    .line 1338238
    return-void

    .line 1338239
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
