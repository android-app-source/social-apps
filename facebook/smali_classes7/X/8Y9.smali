.class public final enum LX/8Y9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Y9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Y9;

.field public static final enum NEWSFEED:LX/8Y9;

.field public static final enum SEARCH_NULL_STATE:LX/8Y9;


# instance fields
.field private final mMarauderValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1355924
    new-instance v0, LX/8Y9;

    const-string v1, "NEWSFEED"

    const-string v2, "newsfeed"

    invoke-direct {v0, v1, v3, v2}, LX/8Y9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Y9;->NEWSFEED:LX/8Y9;

    .line 1355925
    new-instance v0, LX/8Y9;

    const-string v1, "SEARCH_NULL_STATE"

    const-string v2, "search_null"

    invoke-direct {v0, v1, v4, v2}, LX/8Y9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Y9;->SEARCH_NULL_STATE:LX/8Y9;

    .line 1355926
    const/4 v0, 0x2

    new-array v0, v0, [LX/8Y9;

    sget-object v1, LX/8Y9;->NEWSFEED:LX/8Y9;

    aput-object v1, v0, v3

    sget-object v1, LX/8Y9;->SEARCH_NULL_STATE:LX/8Y9;

    aput-object v1, v0, v4

    sput-object v0, LX/8Y9;->$VALUES:[LX/8Y9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1355927
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1355928
    iput-object p3, p0, LX/8Y9;->mMarauderValue:Ljava/lang/String;

    .line 1355929
    return-void
.end method

.method public static getMarauderValue(LX/8Y9;)Ljava/lang/String;
    .locals 1
    .param p0    # LX/8Y9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1355930
    if-nez p0, :cond_0

    .line 1355931
    const/4 v0, 0x0

    .line 1355932
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/8Y9;->getMarauderValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Y9;
    .locals 1

    .prologue
    .line 1355933
    const-class v0, LX/8Y9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Y9;

    return-object v0
.end method

.method public static values()[LX/8Y9;
    .locals 1

    .prologue
    .line 1355934
    sget-object v0, LX/8Y9;->$VALUES:[LX/8Y9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Y9;

    return-object v0
.end method


# virtual methods
.method public final getMarauderValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1355935
    iget-object v0, p0, LX/8Y9;->mMarauderValue:Ljava/lang/String;

    return-object v0
.end method
