.class public LX/7E5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/spherical/model/KeyframeParams;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/spherical/model/SphericalVideoParams;)V
    .locals 6

    .prologue
    .line 1184237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7E5;->a:Ljava/util/List;

    .line 1184239
    invoke-static {p1}, LX/7E5;->a(Lcom/facebook/spherical/model/SphericalVideoParams;)Z

    move-result v0

    iput-boolean v0, p0, LX/7E5;->b:Z

    .line 1184240
    iget-boolean v0, p0, LX/7E5;->b:Z

    if-eqz v0, :cond_0

    .line 1184241
    if-eqz p1, :cond_0

    .line 1184242
    iget-object v1, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->i:Lcom/facebook/spherical/model/GuidedTourParams;

    move-object v1, v1

    .line 1184243
    if-eqz v1, :cond_0

    .line 1184244
    iget-object v1, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->i:Lcom/facebook/spherical/model/GuidedTourParams;

    move-object v1, v1

    .line 1184245
    iget-object v1, v1, Lcom/facebook/spherical/model/GuidedTourParams;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1184246
    :cond_0
    :goto_0
    return-void

    .line 1184247
    :cond_1
    new-instance v1, LX/7DB;

    invoke-direct {v1}, LX/7DB;-><init>()V

    const-wide/16 v3, 0x0

    .line 1184248
    iput-wide v3, v1, LX/7DB;->a:J

    .line 1184249
    move-object v1, v1

    .line 1184250
    invoke-virtual {p1}, Lcom/facebook/spherical/model/SphericalVideoParams;->e()F

    move-result v2

    float-to-int v2, v2

    .line 1184251
    iput v2, v1, LX/7DB;->b:I

    .line 1184252
    move-object v1, v1

    .line 1184253
    invoke-virtual {p1}, Lcom/facebook/spherical/model/SphericalVideoParams;->c()F

    move-result v2

    float-to-int v2, v2

    .line 1184254
    iput v2, v1, LX/7DB;->c:I

    .line 1184255
    move-object v1, v1

    .line 1184256
    invoke-virtual {v1}, LX/7DB;->a()Lcom/facebook/spherical/model/KeyframeParams;

    move-result-object v1

    .line 1184257
    iget-object v2, p0, LX/7E5;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184258
    iget-object v1, p0, LX/7E5;->a:Ljava/util/List;

    .line 1184259
    iget-object v2, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->i:Lcom/facebook/spherical/model/GuidedTourParams;

    move-object v2, v2

    .line 1184260
    iget-object v2, v2, Lcom/facebook/spherical/model/GuidedTourParams;->a:LX/0Px;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public static a(Lcom/facebook/spherical/model/SphericalVideoParams;)Z
    .locals 1

    .prologue
    .line 1184266
    if-eqz p0, :cond_0

    .line 1184267
    iget-object v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->i:Lcom/facebook/spherical/model/GuidedTourParams;

    move-object v0, v0

    .line 1184268
    if-eqz v0, :cond_0

    .line 1184269
    iget-object v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->i:Lcom/facebook/spherical/model/GuidedTourParams;

    move-object v0, v0

    .line 1184270
    iget-object v0, v0, Lcom/facebook/spherical/model/GuidedTourParams;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lcom/facebook/spherical/model/KeyframeParams;
    .locals 1

    .prologue
    .line 1184271
    iget-object v0, p0, LX/7E5;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/model/KeyframeParams;

    return-object v0
.end method

.method public final b(I)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1184261
    move v1, v0

    move v2, v0

    .line 1184262
    :goto_0
    iget-object v0, p0, LX/7E5;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1184263
    int-to-long v4, p1

    iget-object v0, p0, LX/7E5;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/model/KeyframeParams;

    iget-wide v6, v0, Lcom/facebook/spherical/model/KeyframeParams;->a:J

    const-wide/16 v8, 0x7d0

    sub-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    .line 1184264
    add-int/lit8 v0, v1, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1184265
    :cond_0
    return v2
.end method
