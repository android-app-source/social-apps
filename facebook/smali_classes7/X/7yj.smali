.class public LX/7yj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/7yd;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7yd;)V
    .locals 1

    .prologue
    .line 1279992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279993
    iput-object p1, p0, LX/7yj;->a:LX/7yd;

    .line 1279994
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/7yj;->b:Ljava/util/Map;

    .line 1279995
    return-void
.end method


# virtual methods
.method public final a(LX/1pN;LX/03V;)LX/7yj;
    .locals 12

    .prologue
    .line 1279996
    invoke-virtual {p1}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1279997
    if-nez v0, :cond_1

    .line 1279998
    const-string v0, "FaceRecResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ResponseNode was null: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1279999
    iget-object v2, p1, LX/1pN;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 1280000
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1280001
    :cond_0
    return-object p0

    .line 1280002
    :cond_1
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 1280003
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v3

    .line 1280004
    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v4

    .line 1280005
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1280006
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1280007
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1280008
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1280009
    const-string v5, "error"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 1280010
    if-eqz v5, :cond_2

    .line 1280011
    const-string v0, "FaceRecResponse crop error"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "type"

    invoke-virtual {v5, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ": "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "message"

    invoke-virtual {v5, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1280012
    :cond_2
    const-string v5, "tags"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1280013
    if-nez v0, :cond_3

    .line 1280014
    const-string v0, "FaceRecResponse crop error"

    const-string v1, "No error and no suggestions"

    invoke-virtual {p2, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1280015
    :cond_3
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_4

    .line 1280016
    const-string v1, "FaceRecResponse crop error"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Got "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " faceboxes for a crop"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1280017
    :cond_4
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    const-string v5, "suggestions"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1280018
    if-nez v0, :cond_5

    .line 1280019
    const-string v0, "FaceRecResponse crop error"

    const-string v1, "No suggestions included for crop"

    invoke-virtual {p2, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1280020
    :cond_5
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    .line 1280021
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 1280022
    invoke-virtual {v0}, LX/0lF;->G()Ljava/util/Iterator;

    move-result-object v7

    .line 1280023
    :cond_6
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1280024
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1280025
    const-string v8, "id"

    invoke-virtual {v0, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    .line 1280026
    if-eqz v8, :cond_7

    invoke-virtual {v8}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 1280027
    :cond_7
    const-string v0, "FaceRecResponse crop error"

    const-string v8, "No id included in the suggestion"

    invoke-virtual {p2, v0, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1280028
    :cond_8
    invoke-virtual {v8}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1280029
    const-string v9, "score"

    invoke-virtual {v0, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1280030
    if-eqz v0, :cond_6

    .line 1280031
    invoke-virtual {v8}, LX/0lF;->D()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v0}, LX/0lF;->y()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-interface {v5, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1280032
    :cond_9
    invoke-interface {v3, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280033
    invoke-interface {v2, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1280034
    :cond_a
    iget-object v0, p0, LX/7yj;->a:LX/7yd;

    invoke-virtual {v0, v3}, LX/7yd;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    .line 1280035
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1280036
    iget-object v1, p0, LX/7yj;->b:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v1, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280037
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "Suggestions for crop: "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1280038
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 1280039
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1280040
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1280041
    iget-object v7, v0, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v7, v7

    .line 1280042
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Score: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/tagging/model/TaggingProfile;->b()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method
