.class public LX/7H2;
.super LX/1su;
.source ""


# static fields
.field private static final f:LX/1sv;


# instance fields
.field public a:Z

.field public b:Z

.field public c:I

.field public d:Z

.field private g:[B

.field private h:[B

.field private i:[B

.field private j:[B

.field private k:[B

.field private l:[B

.field private m:[B

.field private n:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1190367
    new-instance v0, LX/1sv;

    invoke-direct {v0}, LX/1sv;-><init>()V

    sput-object v0, LX/7H2;->f:LX/1sv;

    return-void
.end method

.method public constructor <init>(LX/1ss;ZZ)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1190311
    invoke-direct {p0, p1}, LX/1su;-><init>(LX/1ss;)V

    .line 1190312
    iput-boolean v0, p0, LX/7H2;->a:Z

    .line 1190313
    iput-boolean v1, p0, LX/7H2;->b:Z

    .line 1190314
    iput-boolean v0, p0, LX/7H2;->d:Z

    .line 1190315
    new-array v0, v1, [B

    iput-object v0, p0, LX/7H2;->g:[B

    .line 1190316
    new-array v0, v2, [B

    iput-object v0, p0, LX/7H2;->h:[B

    .line 1190317
    new-array v0, v3, [B

    iput-object v0, p0, LX/7H2;->i:[B

    .line 1190318
    new-array v0, v4, [B

    iput-object v0, p0, LX/7H2;->j:[B

    .line 1190319
    new-array v0, v1, [B

    iput-object v0, p0, LX/7H2;->k:[B

    .line 1190320
    new-array v0, v2, [B

    iput-object v0, p0, LX/7H2;->l:[B

    .line 1190321
    new-array v0, v3, [B

    iput-object v0, p0, LX/7H2;->m:[B

    .line 1190322
    new-array v0, v4, [B

    iput-object v0, p0, LX/7H2;->n:[B

    .line 1190323
    iput-boolean p2, p0, LX/7H2;->a:Z

    .line 1190324
    iput-boolean p3, p0, LX/7H2;->b:Z

    .line 1190325
    return-void
.end method

.method private a([BII)I
    .locals 1

    .prologue
    .line 1190326
    invoke-static {p0, p3}, LX/7H2;->d(LX/7H2;I)V

    .line 1190327
    iget-object v0, p0, LX/1su;->e:LX/1ss;

    invoke-virtual {v0, p1, p2, p3}, LX/1ss;->c([BII)I

    move-result v0

    return v0
.end method

.method private c(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1190328
    :try_start_0
    invoke-static {p0, p1}, LX/7H2;->d(LX/7H2;I)V

    .line 1190329
    new-array v0, p1, [B

    .line 1190330
    iget-object v1, p0, LX/1su;->e:LX/1ss;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, p1}, LX/1ss;->c([BII)I

    .line 1190331
    new-instance v1, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 1190332
    :catch_0
    new-instance v0, LX/7H0;

    const-string v1, "JVM DOES NOT SUPPORT UTF-8"

    invoke-direct {v0, v1}, LX/7H0;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static d(LX/7H2;I)V
    .locals 3

    .prologue
    .line 1190333
    if-gez p1, :cond_0

    .line 1190334
    new-instance v0, LX/7H0;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Negative length: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H0;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1190335
    :cond_0
    iget-boolean v0, p0, LX/7H2;->d:Z

    if-eqz v0, :cond_1

    .line 1190336
    iget v0, p0, LX/7H2;->c:I

    sub-int/2addr v0, p1

    iput v0, p0, LX/7H2;->c:I

    .line 1190337
    iget v0, p0, LX/7H2;->c:I

    if-gez v0, :cond_1

    .line 1190338
    new-instance v0, LX/7H0;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Message length exceeded: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H0;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1190339
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1190340
    return-void
.end method

.method public final a(B)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1190341
    iget-object v0, p0, LX/7H2;->g:[B

    aput-byte p1, v0, v3

    .line 1190342
    iget-object v0, p0, LX/1su;->e:LX/1ss;

    iget-object v1, p0, LX/7H2;->g:[B

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v3, v2}, LX/1ss;->b([BII)V

    .line 1190343
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1190344
    iget-object v0, p0, LX/7H2;->i:[B

    shr-int/lit8 v1, p1, 0x18

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v0, v3

    .line 1190345
    iget-object v0, p0, LX/7H2;->i:[B

    const/4 v1, 0x1

    shr-int/lit8 v2, p1, 0x10

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 1190346
    iget-object v0, p0, LX/7H2;->i:[B

    const/4 v1, 0x2

    shr-int/lit8 v2, p1, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 1190347
    iget-object v0, p0, LX/7H2;->i:[B

    const/4 v1, 0x3

    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 1190348
    iget-object v0, p0, LX/1su;->e:LX/1ss;

    iget-object v1, p0, LX/7H2;->i:[B

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v3, v2}, LX/1ss;->b([BII)V

    .line 1190349
    return-void
.end method

.method public final a(J)V
    .locals 9

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    const-wide/16 v4, 0xff

    .line 1190353
    iget-object v0, p0, LX/7H2;->j:[B

    const/16 v1, 0x38

    shr-long v2, p1, v1

    and-long/2addr v2, v4

    long-to-int v1, v2

    int-to-byte v1, v1

    aput-byte v1, v0, v6

    .line 1190354
    iget-object v0, p0, LX/7H2;->j:[B

    const/4 v1, 0x1

    const/16 v2, 0x30

    shr-long v2, p1, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 1190355
    iget-object v0, p0, LX/7H2;->j:[B

    const/4 v1, 0x2

    const/16 v2, 0x28

    shr-long v2, p1, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 1190356
    iget-object v0, p0, LX/7H2;->j:[B

    const/4 v1, 0x3

    const/16 v2, 0x20

    shr-long v2, p1, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 1190357
    iget-object v0, p0, LX/7H2;->j:[B

    const/4 v1, 0x4

    const/16 v2, 0x18

    shr-long v2, p1, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 1190358
    iget-object v0, p0, LX/7H2;->j:[B

    const/4 v1, 0x5

    const/16 v2, 0x10

    shr-long v2, p1, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 1190359
    iget-object v0, p0, LX/7H2;->j:[B

    const/4 v1, 0x6

    shr-long v2, p1, v7

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 1190360
    iget-object v0, p0, LX/7H2;->j:[B

    const/4 v1, 0x7

    and-long v2, v4, p1

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 1190361
    iget-object v0, p0, LX/1su;->e:LX/1ss;

    iget-object v1, p0, LX/7H2;->j:[B

    invoke-virtual {v0, v1, v6, v7}, LX/1ss;->b([BII)V

    .line 1190362
    return-void
.end method

.method public final a(LX/1sw;)V
    .locals 1

    .prologue
    .line 1190350
    iget-byte v0, p1, LX/1sw;->b:B

    invoke-virtual {p0, v0}, LX/1su;->a(B)V

    .line 1190351
    iget-short v0, p1, LX/1sw;->c:S

    invoke-virtual {p0, v0}, LX/1su;->a(S)V

    .line 1190352
    return-void
.end method

.method public final a(LX/1u3;)V
    .locals 1

    .prologue
    .line 1190377
    iget-byte v0, p1, LX/1u3;->a:B

    invoke-virtual {p0, v0}, LX/1su;->a(B)V

    .line 1190378
    iget v0, p1, LX/1u3;->b:I

    invoke-virtual {p0, v0}, LX/1su;->a(I)V

    .line 1190379
    return-void
.end method

.method public final a(LX/7H3;)V
    .locals 1

    .prologue
    .line 1190368
    iget-byte v0, p1, LX/7H3;->a:B

    invoke-virtual {p0, v0}, LX/1su;->a(B)V

    .line 1190369
    iget-byte v0, p1, LX/7H3;->b:B

    invoke-virtual {p0, v0}, LX/1su;->a(B)V

    .line 1190370
    iget v0, p1, LX/7H3;->c:I

    invoke-virtual {p0, v0}, LX/1su;->a(I)V

    .line 1190371
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1190372
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 1190373
    array-length v1, v0

    invoke-virtual {p0, v1}, LX/1su;->a(I)V

    .line 1190374
    iget-object v1, p0, LX/1su;->e:LX/1ss;

    const/4 v2, 0x0

    array-length v3, v0

    invoke-virtual {v1, v0, v2, v3}, LX/1ss;->b([BII)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1190375
    return-void

    .line 1190376
    :catch_0
    new-instance v0, LX/7H0;

    const-string v1, "JVM DOES NOT SUPPORT UTF-8"

    invoke-direct {v0, v1}, LX/7H0;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(S)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1190363
    iget-object v0, p0, LX/7H2;->h:[B

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v0, v3

    .line 1190364
    iget-object v0, p0, LX/7H2;->h:[B

    const/4 v1, 0x1

    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 1190365
    iget-object v0, p0, LX/1su;->e:LX/1ss;

    iget-object v1, p0, LX/7H2;->h:[B

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v3, v2}, LX/1ss;->b([BII)V

    .line 1190366
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1190305
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, LX/1su;->a(B)V

    .line 1190306
    return-void

    .line 1190307
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a([B)V
    .locals 3

    .prologue
    .line 1190308
    array-length v0, p1

    invoke-virtual {p0, v0}, LX/1su;->a(I)V

    .line 1190309
    iget-object v0, p0, LX/1su;->e:LX/1ss;

    const/4 v1, 0x0

    array-length v2, p1

    invoke-virtual {v0, p1, v1, v2}, LX/1ss;->b([BII)V

    .line 1190310
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1190247
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1190248
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1su;->a(B)V

    .line 1190249
    return-void
.end method

.method public final d()LX/1sv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1sv;"
        }
    .end annotation

    .prologue
    .line 1190250
    sget-object v0, LX/7H2;->f:LX/1sv;

    return-object v0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1190304
    return-void
.end method

.method public final f()LX/1sw;
    .locals 4

    .prologue
    .line 1190251
    invoke-virtual {p0}, LX/1su;->k()B

    move-result v1

    .line 1190252
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 1190253
    :goto_0
    new-instance v2, LX/1sw;

    const-string v3, ""

    invoke-direct {v2, v3, v1, v0}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    return-object v2

    .line 1190254
    :cond_0
    invoke-virtual {p0}, LX/1su;->l()S

    move-result v0

    goto :goto_0
.end method

.method public final g()LX/7H3;
    .locals 4

    .prologue
    .line 1190255
    new-instance v0, LX/7H3;

    invoke-virtual {p0}, LX/1su;->k()B

    move-result v1

    invoke-virtual {p0}, LX/1su;->k()B

    move-result v2

    invoke-virtual {p0}, LX/1su;->m()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/7H3;-><init>(BBI)V

    return-object v0
.end method

.method public final h()LX/1u3;
    .locals 3

    .prologue
    .line 1190256
    new-instance v0, LX/1u3;

    invoke-virtual {p0}, LX/1su;->k()B

    move-result v1

    invoke-virtual {p0}, LX/1su;->m()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    return-object v0
.end method

.method public final i()LX/7H5;
    .locals 3

    .prologue
    .line 1190257
    new-instance v0, LX/7H5;

    invoke-virtual {p0}, LX/1su;->k()B

    move-result v1

    invoke-virtual {p0}, LX/1su;->m()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/7H5;-><init>(BI)V

    return-object v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1190258
    invoke-virtual {p0}, LX/1su;->k()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()B
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1190259
    const/4 v0, -0x1

    move v0, v0

    .line 1190260
    if-lez v0, :cond_0

    .line 1190261
    const/4 v0, 0x0

    move-object v0, v0

    .line 1190262
    const/4 v1, 0x0

    move v1, v1

    .line 1190263
    aget-byte v0, v0, v1

    .line 1190264
    :goto_0
    return v0

    .line 1190265
    :cond_0
    iget-object v0, p0, LX/7H2;->k:[B

    const/4 v1, 0x1

    invoke-direct {p0, v0, v2, v1}, LX/7H2;->a([BII)I

    .line 1190266
    iget-object v0, p0, LX/7H2;->k:[B

    aget-byte v0, v0, v2

    goto :goto_0
.end method

.method public final l()S
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 1190267
    iget-object v1, p0, LX/7H2;->l:[B

    .line 1190268
    const/4 v2, -0x1

    move v2, v2

    .line 1190269
    if-lt v2, v3, :cond_0

    .line 1190270
    const/4 v0, 0x0

    move-object v1, v0

    .line 1190271
    const/4 v0, 0x0

    move v0, v0

    .line 1190272
    :goto_0
    aget-byte v2, v1, v0

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    add-int/lit8 v0, v0, 0x1

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v2

    int-to-short v0, v0

    return v0

    .line 1190273
    :cond_0
    iget-object v2, p0, LX/7H2;->l:[B

    invoke-direct {p0, v2, v0, v3}, LX/7H2;->a([BII)I

    goto :goto_0
.end method

.method public final m()I
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v0, 0x0

    .line 1190274
    iget-object v1, p0, LX/7H2;->m:[B

    .line 1190275
    const/4 v2, -0x1

    move v2, v2

    .line 1190276
    if-lt v2, v3, :cond_0

    .line 1190277
    const/4 v0, 0x0

    move-object v1, v0

    .line 1190278
    const/4 v0, 0x0

    move v0, v0

    .line 1190279
    :goto_0
    aget-byte v2, v1, v0

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    add-int/lit8 v3, v0, 0x1

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    add-int/lit8 v3, v0, 0x2

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x3

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v2

    return v0

    .line 1190280
    :cond_0
    iget-object v2, p0, LX/7H2;->m:[B

    invoke-direct {p0, v2, v0, v3}, LX/7H2;->a([BII)I

    goto :goto_0
.end method

.method public final n()J
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/16 v7, 0x8

    .line 1190281
    iget-object v1, p0, LX/7H2;->n:[B

    .line 1190282
    const/4 v2, -0x1

    move v2, v2

    .line 1190283
    if-lt v2, v7, :cond_0

    .line 1190284
    const/4 v0, 0x0

    move-object v1, v0

    .line 1190285
    const/4 v0, 0x0

    move v0, v0

    .line 1190286
    :goto_0
    aget-byte v2, v1, v0

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x38

    shl-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    const/16 v6, 0x30

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x2

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    const/16 v6, 0x28

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x3

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    const/16 v6, 0x20

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x4

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    const/16 v6, 0x18

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x5

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    const/16 v6, 0x10

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x6

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    shl-long/2addr v4, v7

    or-long/2addr v2, v4

    add-int/lit8 v0, v0, 0x7

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    int-to-long v0, v0

    or-long/2addr v0, v2

    return-wide v0

    .line 1190287
    :cond_0
    iget-object v2, p0, LX/7H2;->n:[B

    invoke-direct {p0, v2, v0, v7}, LX/7H2;->a([BII)I

    goto :goto_0
.end method

.method public final o()D
    .locals 2

    .prologue
    .line 1190288
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public final p()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1190289
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v1

    .line 1190290
    const/4 v0, -0x1

    move v0, v0

    .line 1190291
    if-lt v0, v1, :cond_0

    .line 1190292
    :try_start_0
    new-instance v0, Ljava/lang/String;

    .line 1190293
    const/4 v2, 0x0

    move-object v2, v2

    .line 1190294
    const/4 v3, 0x0

    move v3, v3

    .line 1190295
    const-string v4, "UTF-8"

    invoke-direct {v0, v2, v3, v1, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1190296
    :goto_0
    return-object v0

    .line 1190297
    :catch_0
    new-instance v0, LX/7H0;

    const-string v1, "JVM DOES NOT SUPPORT UTF-8"

    invoke-direct {v0, v1}, LX/7H0;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1190298
    :cond_0
    invoke-direct {p0, v1}, LX/7H2;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final q()[B
    .locals 4

    .prologue
    .line 1190299
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    .line 1190300
    invoke-static {p0, v0}, LX/7H2;->d(LX/7H2;I)V

    .line 1190301
    new-array v1, v0, [B

    .line 1190302
    iget-object v2, p0, LX/1su;->e:LX/1ss;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, v0}, LX/1ss;->c([BII)I

    .line 1190303
    return-object v1
.end method
