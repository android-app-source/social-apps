.class public final LX/7ND;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/OverflowMenuPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/OverflowMenuPlugin;)V
    .locals 0

    .prologue
    .line 1200012
    iput-object p1, p0, LX/7ND;->a:Lcom/facebook/video/player/plugins/OverflowMenuPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/player/plugins/OverflowMenuPlugin;B)V
    .locals 0

    .prologue
    .line 1200015
    invoke-direct {p0, p1}, LX/7ND;-><init>(Lcom/facebook/video/player/plugins/OverflowMenuPlugin;)V

    return-void
.end method

.method private a(LX/2ou;)V
    .locals 2

    .prologue
    .line 1200016
    iget-object v0, p0, LX/7ND;->a:Lcom/facebook/video/player/plugins/OverflowMenuPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7ND;->a:Lcom/facebook/video/player/plugins/OverflowMenuPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, LX/2ou;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1200017
    :cond_0
    :goto_0
    return-void

    .line 1200018
    :cond_1
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 1200019
    iget-object v0, p0, LX/7ND;->a:Lcom/facebook/video/player/plugins/OverflowMenuPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1200014
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 1200013
    check-cast p1, LX/2ou;

    invoke-direct {p0, p1}, LX/7ND;->a(LX/2ou;)V

    return-void
.end method
