.class public final LX/8YC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# instance fields
.field public final synthetic a:LX/8YE;


# direct methods
.method public constructor <init>(LX/8YE;)V
    .locals 0

    .prologue
    .line 1355944
    iput-object p1, p0, LX/8YC;->a:LX/8YE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final doFrame(J)V
    .locals 7

    .prologue
    .line 1355945
    iget-object v0, p0, LX/8YC;->a:LX/8YE;

    iget-boolean v0, v0, LX/8YE;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8YC;->a:LX/8YE;

    iget-object v0, v0, LX/8YD;->a:LX/8YH;

    if-nez v0, :cond_1

    .line 1355946
    :cond_0
    :goto_0
    return-void

    .line 1355947
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1355948
    iget-object v2, p0, LX/8YC;->a:LX/8YE;

    iget-object v2, v2, LX/8YD;->a:LX/8YH;

    iget-object v3, p0, LX/8YC;->a:LX/8YE;

    iget-wide v4, v3, LX/8YE;->e:J

    sub-long v4, v0, v4

    long-to-double v4, v4

    invoke-virtual {v2, v4, v5}, LX/8YH;->a(D)V

    .line 1355949
    iget-object v2, p0, LX/8YC;->a:LX/8YE;

    .line 1355950
    iput-wide v0, v2, LX/8YE;->e:J

    .line 1355951
    iget-object v0, p0, LX/8YC;->a:LX/8YE;

    iget-object v0, v0, LX/8YE;->b:Landroid/view/Choreographer;

    iget-object v1, p0, LX/8YC;->a:LX/8YE;

    iget-object v1, v1, LX/8YE;->c:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    goto :goto_0
.end method
