.class public LX/7zR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0TD;

.field public final b:LX/11H;

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/7z3;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0TD;LX/11H;)V
    .locals 1

    .prologue
    .line 1280949
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280950
    iput-object p1, p0, LX/7zR;->a:LX/0TD;

    .line 1280951
    iput-object p2, p0, LX/7zR;->b:LX/11H;

    .line 1280952
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7zR;->c:Ljava/util/Map;

    .line 1280953
    return-void
.end method


# virtual methods
.method public final a(LX/7z5;Ljava/util/Map;Ljava/net/URI;LX/7z6;LX/7z4;)LX/7z3;
    .locals 10
    .param p4    # LX/7z6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7z5;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/net/URI;",
            "LX/7z6;",
            "LX/7z4;",
            ")",
            "LX/7z3;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 1280954
    new-instance v6, LX/7z3;

    invoke-direct {v6}, LX/7z3;-><init>()V

    .line 1280955
    invoke-virtual {p3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1280956
    const-string v2, "facebook.com/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 1280957
    if-gez v2, :cond_2

    .line 1280958
    const-string v1, ""

    .line 1280959
    :goto_0
    move-object v1, v1

    .line 1280960
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1280961
    new-instance v1, LX/7zB;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid or non-Facebook URI: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (Non-Retriable)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/7zB;-><init>(Ljava/lang/String;)V

    invoke-virtual {p5, v1, v4}, LX/7z4;->a(Ljava/lang/Exception;Z)V

    .line 1280962
    :goto_1
    return-object v0

    .line 1280963
    :cond_0
    sget-object v2, LX/7zP;->a:[I

    invoke-virtual {p1}, LX/7z5;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1280964
    :goto_2
    iget-object v1, p0, LX/7zR;->a:LX/0TD;

    invoke-interface {v1, v0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1280965
    iget-object v1, p0, LX/7zR;->c:Ljava/util/Map;

    invoke-interface {v1, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280966
    new-instance v1, LX/7zO;

    invoke-direct {v1, p0, v0, p5}, LX/7zO;-><init>(LX/7zR;Lcom/google/common/util/concurrent/ListenableFuture;LX/7z4;)V

    iget-object v2, p0, LX/7zR;->a:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    move-object v0, v6

    .line 1280967
    goto :goto_1

    .line 1280968
    :pswitch_0
    new-instance v2, LX/7zT;

    invoke-direct {v2}, LX/7zT;-><init>()V

    .line 1280969
    new-instance v3, LX/7zU;

    invoke-direct {v3, v1, p2}, LX/7zU;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    .line 1280970
    new-instance v1, LX/14U;

    invoke-direct {v1}, LX/14U;-><init>()V

    .line 1280971
    new-instance v0, LX/7zM;

    invoke-direct {v0, p0, v2, v3, v1}, LX/7zM;-><init>(LX/7zR;LX/0e6;LX/7zU;LX/14U;)V

    goto :goto_2

    .line 1280972
    :pswitch_1
    if-nez p4, :cond_1

    .line 1280973
    new-instance v1, LX/7zB;

    const-string v2, "No POST body"

    invoke-direct {v1, v2}, LX/7zB;-><init>(Ljava/lang/String;)V

    invoke-virtual {p5, v1, v4}, LX/7z4;->a(Ljava/lang/Exception;Z)V

    goto :goto_1

    .line 1280974
    :cond_1
    new-instance v7, LX/7zV;

    invoke-direct {v7}, LX/7zV;-><init>()V

    .line 1280975
    new-instance v0, LX/7zW;

    iget-object v2, p4, LX/7z6;->a:LX/7yy;

    .line 1280976
    iget-object v3, v2, LX/7yy;->a:Ljava/io/File;

    move-object v2, v3

    .line 1280977
    iget-object v3, p4, LX/7z6;->a:LX/7yy;

    invoke-virtual {v3}, LX/7yy;->b()J

    move-result-wide v4

    iget-wide v8, p4, LX/7z6;->b:J

    add-long/2addr v4, v8

    long-to-int v3, v4

    iget-object v4, p4, LX/7z6;->a:LX/7yy;

    invoke-virtual {v4}, LX/7yy;->c()J

    move-result-wide v4

    iget-wide v8, p4, LX/7z6;->b:J

    sub-long/2addr v4, v8

    long-to-int v4, v4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/7zW;-><init>(Ljava/lang/String;Ljava/io/File;IILjava/util/Map;)V

    .line 1280978
    new-instance v2, LX/14U;

    invoke-direct {v2}, LX/14U;-><init>()V

    .line 1280979
    new-instance v1, LX/7zQ;

    iget-object v3, p4, LX/7z6;->a:LX/7yy;

    invoke-virtual {v3}, LX/7yy;->b()J

    move-result-wide v4

    invoke-direct {v1, v4, v5, p5}, LX/7zQ;-><init>(JLX/7z4;)V

    .line 1280980
    iput-object v1, v2, LX/14U;->a:LX/4ck;

    .line 1280981
    new-instance v1, LX/7zN;

    invoke-direct {v1, p0, v7, v0, v2}, LX/7zN;-><init>(LX/7zR;LX/0e6;LX/7zW;LX/14U;)V

    move-object v0, v1

    goto :goto_2

    :cond_2
    add-int/lit8 v2, v2, 0xd

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
