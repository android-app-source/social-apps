.class public final LX/89G;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/ipc/composer/model/ComposerReshareContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z


# direct methods
.method private constructor <init>(Lcom/facebook/graphql/model/GraphQLEntity;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLEntity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1304421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304422
    iput-object p1, p0, LX/89G;->a:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1304423
    iput-object p2, p0, LX/89G;->b:Ljava/lang/String;

    .line 1304424
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)V
    .locals 1

    .prologue
    .line 1304425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304426
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, LX/89G;->a:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1304427
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    iput-object v0, p0, LX/89G;->b:Ljava/lang/String;

    .line 1304428
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1304429
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareTracking:Ljava/lang/String;

    iput-object v0, p0, LX/89G;->d:Ljava/lang/String;

    .line 1304430
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->quoteText:Ljava/lang/String;

    iput-object v0, p0, LX/89G;->e:Ljava/lang/String;

    .line 1304431
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    iput-object v0, p0, LX/89G;->f:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    .line 1304432
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->isMemeShare:Z

    iput-boolean v0, p0, LX/89G;->g:Z

    .line 1304433
    return-void
.end method

.method public static a()LX/89G;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1304434
    new-instance v0, LX/89G;

    invoke-direct {v0, v1, v1}, LX/89G;-><init>(Lcom/facebook/graphql/model/GraphQLEntity;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;
    .locals 2

    .prologue
    .line 1304435
    new-instance v0, LX/89G;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LX/89G;-><init>(Lcom/facebook/graphql/model/GraphQLEntity;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)LX/89G;
    .locals 2

    .prologue
    .line 1304436
    new-instance v0, LX/89G;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, LX/89G;-><init>(Lcom/facebook/graphql/model/GraphQLEntity;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .locals 2

    .prologue
    .line 1304437
    new-instance v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/intent/ComposerShareParams;-><init>(LX/89G;)V

    return-object v0
.end method
