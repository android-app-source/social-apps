.class public abstract LX/8D6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0lC;

.field private final b:LX/03V;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/0iA;

.field public final e:LX/0SG;

.field private f:LX/0i1;

.field private g:LX/8D7;


# direct methods
.method public constructor <init>(LX/0lC;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0iA;LX/0SG;)V
    .locals 0

    .prologue
    .line 1312282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1312283
    iput-object p1, p0, LX/8D6;->a:LX/0lC;

    .line 1312284
    iput-object p2, p0, LX/8D6;->b:LX/03V;

    .line 1312285
    iput-object p3, p0, LX/8D6;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1312286
    iput-object p4, p0, LX/8D6;->d:LX/0iA;

    .line 1312287
    iput-object p5, p0, LX/8D6;->e:LX/0SG;

    .line 1312288
    return-void
.end method

.method public static a(LX/8D6;Lcom/facebook/nux/NuxHistory;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1312289
    :try_start_0
    iget-object v0, p0, LX/8D6;->a:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1312290
    :goto_0
    return-object v0

    .line 1312291
    :catch_0
    move-exception v0

    .line 1312292
    iget-object v1, p0, LX/8D6;->b:LX/03V;

    const-string v2, "nux_history_encode_fail"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1312293
    const-string v0, ""

    goto :goto_0
.end method

.method public static d(LX/8D6;)LX/8D7;
    .locals 1

    .prologue
    .line 1312294
    iget-object v0, p0, LX/8D6;->g:LX/8D7;

    if-nez v0, :cond_0

    .line 1312295
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, LX/8D7;->forNuxDelegate(Ljava/lang/Class;)LX/8D7;

    move-result-object v0

    iput-object v0, p0, LX/8D6;->g:LX/8D7;

    .line 1312296
    :cond_0
    iget-object v0, p0, LX/8D6;->g:LX/8D7;

    return-object v0
.end method

.method public static e(LX/8D6;)LX/0i1;
    .locals 2

    .prologue
    .line 1312297
    iget-object v0, p0, LX/8D6;->f:LX/0i1;

    if-nez v0, :cond_0

    .line 1312298
    iget-object v0, p0, LX/8D6;->d:LX/0iA;

    invoke-static {p0}, LX/8D6;->d(LX/8D6;)LX/8D7;

    move-result-object v1

    iget-object v1, v1, LX/8D7;->interstitialId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0iA;->a(Ljava/lang/String;)LX/0i1;

    move-result-object v0

    iput-object v0, p0, LX/8D6;->f:LX/0i1;

    .line 1312299
    :cond_0
    iget-object v0, p0, LX/8D6;->f:LX/0i1;

    return-object v0
.end method

.method public static f(LX/8D6;)Lcom/facebook/nux/NuxHistory;
    .locals 3

    .prologue
    .line 1312300
    iget-object v0, p0, LX/8D6;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/8D6;->d(LX/8D6;)LX/8D7;

    move-result-object v1

    iget-object v1, v1, LX/8D7;->prefKey:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1312301
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1312302
    new-instance v0, Lcom/facebook/nux/NuxHistory;

    invoke-direct {v0}, Lcom/facebook/nux/NuxHistory;-><init>()V

    .line 1312303
    :goto_0
    return-object v0

    .line 1312304
    :cond_0
    :try_start_0
    iget-object v1, p0, LX/8D6;->a:LX/0lC;

    const-class v2, Lcom/facebook/nux/NuxHistory;

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nux/NuxHistory;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1312305
    :catch_0
    move-exception v0

    .line 1312306
    iget-object v1, p0, LX/8D6;->b:LX/03V;

    const-string v2, "nux_history_decode_fail"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1312307
    new-instance v0, Lcom/facebook/nux/NuxHistory;

    invoke-direct {v0}, Lcom/facebook/nux/NuxHistory;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/nux/NuxHistory;->a(Z)Lcom/facebook/nux/NuxHistory;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 1312308
    invoke-static {p0}, LX/8D6;->f(LX/8D6;)Lcom/facebook/nux/NuxHistory;

    move-result-object v0

    .line 1312309
    invoke-virtual {v0}, Lcom/facebook/nux/NuxHistory;->a()V

    .line 1312310
    iget-object v1, p0, LX/8D6;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/nux/NuxHistory;->a(J)Lcom/facebook/nux/NuxHistory;

    .line 1312311
    iget-object v1, p0, LX/8D6;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-static {p0}, LX/8D6;->d(LX/8D6;)LX/8D7;

    move-result-object v2

    iget-object v2, v2, LX/8D7;->prefKey:LX/0Tn;

    invoke-static {p0, v0}, LX/8D6;->a(LX/8D6;Lcom/facebook/nux/NuxHistory;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1312312
    iget-object v0, p0, LX/8D6;->d:LX/0iA;

    invoke-static {p0}, LX/8D6;->e(LX/8D6;)LX/0i1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0iA;->a(LX/0i1;)V

    .line 1312313
    iget-object v0, p0, LX/8D6;->d:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    invoke-static {p0}, LX/8D6;->d(LX/8D6;)LX/8D7;

    move-result-object v1

    iget-object v1, v1, LX/8D7;->interstitialId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1312314
    return-void
.end method

.method public a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    .locals 2

    .prologue
    .line 1312315
    invoke-static {p0}, LX/8D6;->e(LX/8D6;)LX/0i1;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8D6;->d:LX/0iA;

    invoke-static {p0}, LX/8D6;->e(LX/8D6;)LX/0i1;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/0iA;->a(LX/0i1;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1312316
    invoke-static {p0}, LX/8D6;->f(LX/8D6;)Lcom/facebook/nux/NuxHistory;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/nux/NuxHistory;->a(Z)Lcom/facebook/nux/NuxHistory;

    move-result-object v0

    .line 1312317
    iget-object v1, p0, LX/8D6;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-static {p0}, LX/8D6;->d(LX/8D6;)LX/8D7;

    move-result-object v2

    iget-object v2, v2, LX/8D7;->prefKey:LX/0Tn;

    invoke-static {p0, v0}, LX/8D6;->a(LX/8D6;Lcom/facebook/nux/NuxHistory;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1312318
    iget-object v0, p0, LX/8D6;->d:LX/0iA;

    invoke-static {p0}, LX/8D6;->e(LX/8D6;)LX/0i1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0iA;->a(LX/0i1;)V

    .line 1312319
    iget-object v0, p0, LX/8D6;->d:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    invoke-static {p0}, LX/8D6;->d(LX/8D6;)LX/8D7;

    move-result-object v1

    iget-object v1, v1, LX/8D7;->interstitialId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->d(Ljava/lang/String;)V

    .line 1312320
    return-void
.end method
