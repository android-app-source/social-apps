.class public LX/6kt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;


# instance fields
.field public final firstName:Ljava/lang/String;

.field public final fullName:Ljava/lang/String;

.field public final isMessengerUser:Ljava/lang/Boolean;

.field public final profPicURIMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final userFbId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xb

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 1141723
    new-instance v0, LX/1sv;

    const-string v1, "ParticipantInfo"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kt;->b:LX/1sv;

    .line 1141724
    new-instance v0, LX/1sw;

    const-string v1, "userFbId"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kt;->c:LX/1sw;

    .line 1141725
    new-instance v0, LX/1sw;

    const-string v1, "firstName"

    invoke-direct {v0, v1, v5, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kt;->d:LX/1sw;

    .line 1141726
    new-instance v0, LX/1sw;

    const-string v1, "fullName"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kt;->e:LX/1sw;

    .line 1141727
    new-instance v0, LX/1sw;

    const-string v1, "isMessengerUser"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kt;->f:LX/1sw;

    .line 1141728
    new-instance v0, LX/1sw;

    const-string v1, "profPicURIMap"

    const/16 v2, 0xd

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kt;->g:LX/1sw;

    .line 1141729
    sput-boolean v4, LX/6kt;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1141730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1141731
    iput-object p1, p0, LX/6kt;->userFbId:Ljava/lang/Long;

    .line 1141732
    iput-object p2, p0, LX/6kt;->firstName:Ljava/lang/String;

    .line 1141733
    iput-object p3, p0, LX/6kt;->fullName:Ljava/lang/String;

    .line 1141734
    iput-object p4, p0, LX/6kt;->isMessengerUser:Ljava/lang/Boolean;

    .line 1141735
    iput-object p5, p0, LX/6kt;->profPicURIMap:Ljava/util/Map;

    .line 1141736
    return-void
.end method

.method public static b(LX/1su;)LX/6kt;
    .locals 11

    .prologue
    const/16 v10, 0xb

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1141737
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v4, v5

    move-object v3, v5

    move-object v2, v5

    move-object v1, v5

    .line 1141738
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1141739
    iget-byte v7, v0, LX/1sw;->b:B

    if-eqz v7, :cond_7

    .line 1141740
    iget-short v7, v0, LX/1sw;->c:S

    packed-switch v7, :pswitch_data_0

    .line 1141741
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141742
    :pswitch_0
    iget-byte v7, v0, LX/1sw;->b:B

    const/16 v8, 0xa

    if-ne v7, v8, :cond_1

    .line 1141743
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 1141744
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141745
    :pswitch_1
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v10, :cond_2

    .line 1141746
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1141747
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141748
    :pswitch_2
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v10, :cond_3

    .line 1141749
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 1141750
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141751
    :pswitch_3
    iget-byte v7, v0, LX/1sw;->b:B

    const/4 v8, 0x2

    if-ne v7, v8, :cond_4

    .line 1141752
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_0

    .line 1141753
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141754
    :pswitch_4
    iget-byte v7, v0, LX/1sw;->b:B

    const/16 v8, 0xd

    if-ne v7, v8, :cond_6

    .line 1141755
    invoke-virtual {p0}, LX/1su;->g()LX/7H3;

    move-result-object v7

    .line 1141756
    new-instance v5, Ljava/util/HashMap;

    iget v0, v7, LX/7H3;->c:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/HashMap;-><init>(I)V

    move v0, v6

    .line 1141757
    :goto_1
    iget v8, v7, LX/7H3;->c:I

    if-gez v8, :cond_5

    invoke-static {}, LX/1su;->s()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1141758
    :goto_2
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 1141759
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v9

    .line 1141760
    invoke-interface {v5, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1141761
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1141762
    :cond_5
    iget v8, v7, LX/7H3;->c:I

    if-ge v0, v8, :cond_0

    goto :goto_2

    .line 1141763
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141764
    :cond_7
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1141765
    new-instance v0, LX/6kt;

    invoke-direct/range {v0 .. v5}, LX/6kt;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/Map;)V

    .line 1141766
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1141592
    if-eqz p2, :cond_8

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1141593
    :goto_0
    if-eqz p2, :cond_9

    const-string v0, "\n"

    move-object v3, v0

    .line 1141594
    :goto_1
    if-eqz p2, :cond_a

    const-string v0, " "

    .line 1141595
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "ParticipantInfo"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1141596
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141597
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141598
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141599
    const/4 v1, 0x1

    .line 1141600
    iget-object v6, p0, LX/6kt;->userFbId:Ljava/lang/Long;

    if-eqz v6, :cond_0

    .line 1141601
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141602
    const-string v1, "userFbId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141603
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141604
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141605
    iget-object v1, p0, LX/6kt;->userFbId:Ljava/lang/Long;

    if-nez v1, :cond_b

    .line 1141606
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1141607
    :cond_0
    iget-object v6, p0, LX/6kt;->firstName:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 1141608
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141609
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141610
    const-string v1, "firstName"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141611
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141612
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141613
    iget-object v1, p0, LX/6kt;->firstName:Ljava/lang/String;

    if-nez v1, :cond_c

    .line 1141614
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 1141615
    :cond_2
    iget-object v6, p0, LX/6kt;->fullName:Ljava/lang/String;

    if-eqz v6, :cond_4

    .line 1141616
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141617
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141618
    const-string v1, "fullName"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141619
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141620
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141621
    iget-object v1, p0, LX/6kt;->fullName:Ljava/lang/String;

    if-nez v1, :cond_d

    .line 1141622
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v1, v2

    .line 1141623
    :cond_4
    iget-object v6, p0, LX/6kt;->isMessengerUser:Ljava/lang/Boolean;

    if-eqz v6, :cond_10

    .line 1141624
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141625
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141626
    const-string v1, "isMessengerUser"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141627
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141628
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141629
    iget-object v1, p0, LX/6kt;->isMessengerUser:Ljava/lang/Boolean;

    if-nez v1, :cond_e

    .line 1141630
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141631
    :goto_6
    iget-object v1, p0, LX/6kt;->profPicURIMap:Ljava/util/Map;

    if-eqz v1, :cond_7

    .line 1141632
    if-nez v2, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141633
    :cond_6
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141634
    const-string v1, "profPicURIMap"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141635
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141636
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141637
    iget-object v0, p0, LX/6kt;->profPicURIMap:Ljava/util/Map;

    if-nez v0, :cond_f

    .line 1141638
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141639
    :cond_7
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141640
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141641
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1141642
    :cond_8
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1141643
    :cond_9
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1141644
    :cond_a
    const-string v0, ""

    goto/16 :goto_2

    .line 1141645
    :cond_b
    iget-object v1, p0, LX/6kt;->userFbId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1141646
    :cond_c
    iget-object v1, p0, LX/6kt;->firstName:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1141647
    :cond_d
    iget-object v1, p0, LX/6kt;->fullName:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1141648
    :cond_e
    iget-object v1, p0, LX/6kt;->isMessengerUser:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1141649
    :cond_f
    iget-object v0, p0, LX/6kt;->profPicURIMap:Ljava/util/Map;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    :cond_10
    move v2, v1

    goto/16 :goto_6
.end method

.method public final a(LX/1su;)V
    .locals 4

    .prologue
    .line 1141696
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1141697
    iget-object v0, p0, LX/6kt;->userFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1141698
    iget-object v0, p0, LX/6kt;->userFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1141699
    sget-object v0, LX/6kt;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141700
    iget-object v0, p0, LX/6kt;->userFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1141701
    :cond_0
    iget-object v0, p0, LX/6kt;->firstName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1141702
    iget-object v0, p0, LX/6kt;->firstName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1141703
    sget-object v0, LX/6kt;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141704
    iget-object v0, p0, LX/6kt;->firstName:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1141705
    :cond_1
    iget-object v0, p0, LX/6kt;->fullName:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1141706
    iget-object v0, p0, LX/6kt;->fullName:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1141707
    sget-object v0, LX/6kt;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141708
    iget-object v0, p0, LX/6kt;->fullName:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1141709
    :cond_2
    iget-object v0, p0, LX/6kt;->isMessengerUser:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1141710
    iget-object v0, p0, LX/6kt;->isMessengerUser:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1141711
    sget-object v0, LX/6kt;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141712
    iget-object v0, p0, LX/6kt;->isMessengerUser:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1141713
    :cond_3
    iget-object v0, p0, LX/6kt;->profPicURIMap:Ljava/util/Map;

    if-eqz v0, :cond_4

    .line 1141714
    iget-object v0, p0, LX/6kt;->profPicURIMap:Ljava/util/Map;

    if-eqz v0, :cond_4

    .line 1141715
    sget-object v0, LX/6kt;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1141716
    new-instance v0, LX/7H3;

    const/16 v1, 0x8

    const/16 v2, 0xb

    iget-object v3, p0, LX/6kt;->profPicURIMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 1141717
    iget-object v0, p0, LX/6kt;->profPicURIMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1141718
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, LX/1su;->a(I)V

    .line 1141719
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1141720
    :cond_4
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1141721
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1141722
    return-void
.end method

.method public final a(LX/6kt;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1141658
    if-nez p1, :cond_1

    .line 1141659
    :cond_0
    :goto_0
    return v2

    .line 1141660
    :cond_1
    iget-object v0, p0, LX/6kt;->userFbId:Ljava/lang/Long;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1141661
    :goto_1
    iget-object v3, p1, LX/6kt;->userFbId:Ljava/lang/Long;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1141662
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1141663
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141664
    iget-object v0, p0, LX/6kt;->userFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/6kt;->userFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141665
    :cond_3
    iget-object v0, p0, LX/6kt;->firstName:Ljava/lang/String;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1141666
    :goto_3
    iget-object v3, p1, LX/6kt;->firstName:Ljava/lang/String;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1141667
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1141668
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141669
    iget-object v0, p0, LX/6kt;->firstName:Ljava/lang/String;

    iget-object v3, p1, LX/6kt;->firstName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141670
    :cond_5
    iget-object v0, p0, LX/6kt;->fullName:Ljava/lang/String;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1141671
    :goto_5
    iget-object v3, p1, LX/6kt;->fullName:Ljava/lang/String;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1141672
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1141673
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141674
    iget-object v0, p0, LX/6kt;->fullName:Ljava/lang/String;

    iget-object v3, p1, LX/6kt;->fullName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141675
    :cond_7
    iget-object v0, p0, LX/6kt;->isMessengerUser:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1141676
    :goto_7
    iget-object v3, p1, LX/6kt;->isMessengerUser:Ljava/lang/Boolean;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1141677
    :goto_8
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1141678
    :cond_8
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141679
    iget-object v0, p0, LX/6kt;->isMessengerUser:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6kt;->isMessengerUser:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141680
    :cond_9
    iget-object v0, p0, LX/6kt;->profPicURIMap:Ljava/util/Map;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1141681
    :goto_9
    iget-object v3, p1, LX/6kt;->profPicURIMap:Ljava/util/Map;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1141682
    :goto_a
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1141683
    :cond_a
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1141684
    iget-object v0, p0, LX/6kt;->profPicURIMap:Ljava/util/Map;

    iget-object v3, p1, LX/6kt;->profPicURIMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_b
    move v2, v1

    .line 1141685
    goto/16 :goto_0

    :cond_c
    move v0, v2

    .line 1141686
    goto/16 :goto_1

    :cond_d
    move v3, v2

    .line 1141687
    goto/16 :goto_2

    :cond_e
    move v0, v2

    .line 1141688
    goto :goto_3

    :cond_f
    move v3, v2

    .line 1141689
    goto :goto_4

    :cond_10
    move v0, v2

    .line 1141690
    goto :goto_5

    :cond_11
    move v3, v2

    .line 1141691
    goto :goto_6

    :cond_12
    move v0, v2

    .line 1141692
    goto :goto_7

    :cond_13
    move v3, v2

    .line 1141693
    goto :goto_8

    :cond_14
    move v0, v2

    .line 1141694
    goto :goto_9

    :cond_15
    move v3, v2

    .line 1141695
    goto :goto_a
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1141654
    if-nez p1, :cond_1

    .line 1141655
    :cond_0
    :goto_0
    return v0

    .line 1141656
    :cond_1
    instance-of v1, p1, LX/6kt;

    if-eqz v1, :cond_0

    .line 1141657
    check-cast p1, LX/6kt;

    invoke-virtual {p0, p1}, LX/6kt;->a(LX/6kt;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1141653
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1141650
    sget-boolean v0, LX/6kt;->a:Z

    .line 1141651
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kt;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1141652
    return-object v0
.end method
