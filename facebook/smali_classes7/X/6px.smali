.class public LX/6px;
.super LX/6pY;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6px;


# instance fields
.field public final a:LX/6p6;


# direct methods
.method public constructor <init>(LX/6p6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1149748
    invoke-direct {p0}, LX/6pY;-><init>()V

    .line 1149749
    iput-object p1, p0, LX/6px;->a:LX/6p6;

    .line 1149750
    return-void
.end method

.method public static a(LX/0QB;)LX/6px;
    .locals 4

    .prologue
    .line 1149751
    sget-object v0, LX/6px;->b:LX/6px;

    if-nez v0, :cond_1

    .line 1149752
    const-class v1, LX/6px;

    monitor-enter v1

    .line 1149753
    :try_start_0
    sget-object v0, LX/6px;->b:LX/6px;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1149754
    if-eqz v2, :cond_0

    .line 1149755
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1149756
    new-instance p0, LX/6px;

    invoke-static {v0}, LX/6p6;->a(LX/0QB;)LX/6p6;

    move-result-object v3

    check-cast v3, LX/6p6;

    invoke-direct {p0, v3}, LX/6px;-><init>(LX/6p6;)V

    .line 1149757
    move-object v0, p0

    .line 1149758
    sput-object v0, LX/6px;->b:LX/6px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1149759
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1149760
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1149761
    :cond_1
    sget-object v0, LX/6px;->b:LX/6px;

    return-object v0

    .line 1149762
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1149763
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/6pM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1149764
    sget-object v0, LX/6pM;->VERIFY:LX/6pM;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;LX/6pM;)LX/6od;
    .locals 1

    .prologue
    .line 1149765
    new-instance v0, LX/6pv;

    invoke-direct {v0, p0, p1, p1, p2}, LX/6pv;-><init>(LX/6px;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;)V

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1149766
    iget-object v0, p0, LX/6px;->a:LX/6p6;

    invoke-virtual {v0}, LX/6p6;->a()V

    .line 1149767
    return-void
.end method
