.class public final LX/7IP;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideosByVideoChannelQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1191804
    const-class v1, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideosByVideoChannelQueryModel;

    const v0, 0x73bafe97

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "FetchVideosByVideoChannelQuery"

    const-string v6, "7fda9ed7894d578873e34f5a03f6da79"

    const-string v7, "node"

    const-string v8, "10155261854681729"

    const/4 v9, 0x0

    .line 1191805
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1191806
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1191807
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1191767
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1191768
    sparse-switch v0, :sswitch_data_0

    .line 1191769
    :goto_0
    return-object p1

    .line 1191770
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1191771
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1191772
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1191773
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1191774
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1191775
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1191776
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1191777
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1191778
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1191779
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1191780
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1191781
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1191782
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1191783
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1191784
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1191785
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1191786
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1191787
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1191788
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1191789
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1191790
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1191791
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 1191792
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 1191793
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 1191794
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 1191795
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 1191796
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    .line 1191797
    :sswitch_1b
    const-string p1, "27"

    goto :goto_0

    .line 1191798
    :sswitch_1c
    const-string p1, "28"

    goto :goto_0

    .line 1191799
    :sswitch_1d
    const-string p1, "29"

    goto :goto_0

    .line 1191800
    :sswitch_1e
    const-string p1, "30"

    goto :goto_0

    .line 1191801
    :sswitch_1f
    const-string p1, "31"

    goto :goto_0

    .line 1191802
    :sswitch_20
    const-string p1, "32"

    goto :goto_0

    .line 1191803
    :sswitch_21
    const-string p1, "33"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_1b
        -0x680de62a -> :sswitch_c
        -0x6326fdb3 -> :sswitch_b
        -0x626f1062 -> :sswitch_0
        -0x57984ae8 -> :sswitch_17
        -0x5709d77d -> :sswitch_1f
        -0x51869875 -> :sswitch_3
        -0x513764de -> :sswitch_1c
        -0x4496acc9 -> :sswitch_d
        -0x41a91745 -> :sswitch_15
        -0x41143822 -> :sswitch_4
        -0x3c54de38 -> :sswitch_11
        -0x3b85b241 -> :sswitch_1e
        -0x2fab0379 -> :sswitch_1d
        -0x25a646c8 -> :sswitch_6
        -0x22c73366 -> :sswitch_14
        -0x2177e47b -> :sswitch_7
        -0x1d6ce0bf -> :sswitch_13
        -0x1b87b280 -> :sswitch_a
        -0x15db59af -> :sswitch_21
        -0x12efdeb3 -> :sswitch_e
        -0x3e446ed -> :sswitch_9
        -0x12603b3 -> :sswitch_19
        0x58705dc -> :sswitch_2
        0xa1fa812 -> :sswitch_1
        0x214100e0 -> :sswitch_f
        0x2292beef -> :sswitch_1a
        0x26d0c0ff -> :sswitch_18
        0x291d8de0 -> :sswitch_16
        0x43ee5105 -> :sswitch_10
        0x4c5e681b -> :sswitch_20
        0x5e7957c4 -> :sswitch_5
        0x6df81024 -> :sswitch_8
        0x73a026b5 -> :sswitch_12
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1191751
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1191752
    :goto_1
    return v0

    .line 1191753
    :sswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "4"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "6"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "16"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "17"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "19"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v2, "25"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    :sswitch_8
    const-string v2, "28"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    :sswitch_9
    const-string v2, "33"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x9

    goto :goto_0

    :sswitch_a
    const-string v2, "29"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xa

    goto :goto_0

    :sswitch_b
    const-string v2, "30"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xb

    goto :goto_0

    :sswitch_c
    const-string v2, "23"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xc

    goto/16 :goto_0

    .line 1191754
    :pswitch_0
    const-string v0, "video_channel"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1191755
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1191756
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1191757
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1191758
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1191759
    :pswitch_5
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1191760
    :pswitch_6
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1191761
    :pswitch_7
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1191762
    :pswitch_8
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1191763
    :pswitch_9
    const-string v0, "mobile"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1191764
    :pswitch_a
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1191765
    :pswitch_b
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1191766
    :pswitch_c
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x34 -> :sswitch_1
        0x35 -> :sswitch_2
        0x36 -> :sswitch_3
        0x625 -> :sswitch_4
        0x626 -> :sswitch_5
        0x628 -> :sswitch_6
        0x641 -> :sswitch_c
        0x643 -> :sswitch_7
        0x646 -> :sswitch_8
        0x647 -> :sswitch_a
        0x65d -> :sswitch_b
        0x660 -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method
