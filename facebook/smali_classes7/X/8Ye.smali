.class public final LX/8Ye;
.super Ljava/util/HashMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "LX/8Yf;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 1356452
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 1356453
    const-string v0, "HelveticaNeue"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif"

    invoke-direct {v1, v2, v3}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356454
    const-string v0, "HelveticaNeue-Bold"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif"

    invoke-direct {v1, v2, v5}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356455
    const-string v0, "HelveticaNeue-BoldItalic"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif"

    invoke-direct {v1, v2, v6}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356456
    const-string v0, "HelveticaNeue-CondensedBlack"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif-condensed"

    invoke-direct {v1, v2, v5}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356457
    const-string v0, "HelveticaNeue-CondensedBold"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif-condensed"

    invoke-direct {v1, v2, v5}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356458
    const-string v0, "HelveticaNeue-Italic"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif"

    invoke-direct {v1, v2, v4}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356459
    const-string v0, "HelveticaNeue-Light"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif-light"

    invoke-direct {v1, v2, v3}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356460
    const-string v0, "HelveticaNeue-LightItalic"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif-light"

    invoke-direct {v1, v2, v4}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356461
    const-string v0, "HelveticaNeue-Medium"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif"

    invoke-direct {v1, v2, v5}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356462
    const-string v0, "HelveticaNeue-MediumItalic"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif"

    invoke-direct {v1, v2, v6}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356463
    const-string v0, "HelveticaNeue-UltraLight"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif-thin"

    invoke-direct {v1, v2, v3}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356464
    const-string v0, "HelveticaNeue-UltraLightItalic"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif-thin"

    invoke-direct {v1, v2, v4}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356465
    const-string v0, "HelveticaNeue-Thin"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif-thin"

    invoke-direct {v1, v2, v3}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356466
    const-string v0, "HelveticaNeue-ThinItalic"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif-thin"

    invoke-direct {v1, v2, v4}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356467
    const-string v0, "Helvetica"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif"

    invoke-direct {v1, v2, v3}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356468
    const-string v0, "Helvetica-Bold"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif"

    invoke-direct {v1, v2, v5}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356469
    const-string v0, "Helvetica-BoldOblique"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif"

    invoke-direct {v1, v2, v6}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356470
    const-string v0, "Helvetica-Light"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif-light"

    invoke-direct {v1, v2, v3}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356471
    const-string v0, "Helvetica-LightOblique"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif-light"

    invoke-direct {v1, v2, v4}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356472
    const-string v0, "Helvetica-Oblique"

    new-instance v1, LX/8Yf;

    const-string v2, "sans-serif"

    invoke-direct {v1, v2, v4}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356473
    const-string v0, "Georgia"

    new-instance v1, LX/8Yf;

    const-string v2, "serif"

    invoke-direct {v1, v2, v3}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356474
    const-string v0, "Georgia-Bold"

    new-instance v1, LX/8Yf;

    const-string v2, "serif"

    invoke-direct {v1, v2, v5}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356475
    const-string v0, "Georgia-BoldItalic"

    new-instance v1, LX/8Yf;

    const-string v2, "serif"

    invoke-direct {v1, v2, v6}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356476
    const-string v0, "Georgia-Italic"

    new-instance v1, LX/8Yf;

    const-string v2, "serif"

    invoke-direct {v1, v2, v4}, LX/8Yf;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, LX/8Ye;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356477
    return-void
.end method
