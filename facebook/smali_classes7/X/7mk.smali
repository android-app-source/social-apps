.class public final enum LX/7mk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7mk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7mk;

.field public static final enum MMP:LX/7mk;

.field public static final enum OTHER:LX/7mk;

.field public static final enum PHOTO:LX/7mk;

.field public static final enum TEXT:LX/7mk;

.field public static final enum VIDEO:LX/7mk;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1237427
    new-instance v0, LX/7mk;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v2}, LX/7mk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7mk;->TEXT:LX/7mk;

    new-instance v0, LX/7mk;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v3}, LX/7mk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7mk;->PHOTO:LX/7mk;

    new-instance v0, LX/7mk;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v4}, LX/7mk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7mk;->VIDEO:LX/7mk;

    new-instance v0, LX/7mk;

    const-string v1, "MMP"

    invoke-direct {v0, v1, v5}, LX/7mk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7mk;->MMP:LX/7mk;

    new-instance v0, LX/7mk;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v6}, LX/7mk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7mk;->OTHER:LX/7mk;

    .line 1237428
    const/4 v0, 0x5

    new-array v0, v0, [LX/7mk;

    sget-object v1, LX/7mk;->TEXT:LX/7mk;

    aput-object v1, v0, v2

    sget-object v1, LX/7mk;->PHOTO:LX/7mk;

    aput-object v1, v0, v3

    sget-object v1, LX/7mk;->VIDEO:LX/7mk;

    aput-object v1, v0, v4

    sget-object v1, LX/7mk;->MMP:LX/7mk;

    aput-object v1, v0, v5

    sget-object v1, LX/7mk;->OTHER:LX/7mk;

    aput-object v1, v0, v6

    sput-object v0, LX/7mk;->$VALUES:[LX/7mk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1237429
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7mk;
    .locals 1

    .prologue
    .line 1237430
    const-class v0, LX/7mk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7mk;

    return-object v0
.end method

.method public static values()[LX/7mk;
    .locals 1

    .prologue
    .line 1237431
    sget-object v0, LX/7mk;->$VALUES:[LX/7mk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7mk;

    return-object v0
.end method
