.class public LX/86n;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/8GT;

.field private final c:LX/1FZ;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8GT;LX/1FZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1297732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1297733
    iput-object p1, p0, LX/86n;->a:Landroid/content/Context;

    .line 1297734
    iput-object p2, p0, LX/86n;->b:LX/8GT;

    .line 1297735
    iput-object p3, p0, LX/86n;->c:LX/1FZ;

    .line 1297736
    return-void
.end method

.method public static b(LX/0QB;)LX/86n;
    .locals 4

    .prologue
    .line 1297737
    new-instance v3, LX/86n;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/8GT;->a(LX/0QB;)LX/8GT;

    move-result-object v1

    check-cast v1, LX/8GT;

    invoke-static {p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v2

    check-cast v2, LX/1FZ;

    invoke-direct {v3, v0, v1, v2}, LX/86n;-><init>(Landroid/content/Context;LX/8GT;LX/1FZ;)V

    .line 1297738
    return-object v3
.end method


# virtual methods
.method public final a(ZLcom/facebook/friendsharing/inspiration/model/InspirationTextParams;Landroid/content/res/Resources;)LX/1FJ;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;",
            "Landroid/content/res/Resources;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1297739
    iget-object v0, p0, LX/86n;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTypeface()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 1297740
    new-instance v2, Landroid/text/TextPaint;

    const/4 v1, 0x3

    invoke-direct {v2, v1}, Landroid/text/TextPaint;-><init>(I)V

    .line 1297741
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextColor()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 1297742
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextSize()F

    move-result v1

    invoke-virtual/range {p3 .. p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->scaledDensity:F

    mul-float/2addr v1, v3

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getSizeMultiplier()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1297743
    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1297744
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getShadowRadius()F

    move-result v1

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getShadowDX()F

    move-result v3

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getShadowDY()F

    move-result v4

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getShadowColor()I

    move-result v5

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 1297745
    new-instance v13, Landroid/text/TextPaint;

    const/4 v1, 0x3

    invoke-direct {v13, v1}, Landroid/text/TextPaint;-><init>(I)V

    .line 1297746
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getBorderColor()I

    move-result v1

    invoke-virtual {v13, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 1297747
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getBorderAlpha()I

    move-result v1

    invoke-virtual {v13, v1}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 1297748
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextSize()F

    move-result v1

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getSizeMultiplier()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v13, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1297749
    invoke-virtual {v13, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1297750
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v13, v0}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1297751
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getBorderWidth()F

    move-result v0

    invoke-virtual {v13, v0}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 1297752
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextHeight()I

    move-result v0

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getSizeMultiplier()I

    move-result v1

    mul-int v12, v0, v1

    .line 1297753
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextWidth()I

    move-result v0

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getSizeMultiplier()I

    move-result v1

    mul-int v3, v0, v1

    .line 1297754
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getText()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 1297755
    new-instance v4, Landroid/text/StaticLayout;

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getText()Ljava/lang/String;

    move-result-object v5

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, v13

    move v7, v3

    invoke-direct/range {v4 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 1297756
    iget-object v5, p0, LX/86n;->c:LX/1FZ;

    if-eqz p1, :cond_1

    add-int v1, v3, v12

    move v2, v1

    :goto_0
    if-eqz p1, :cond_2

    add-int v1, v12, v3

    :goto_1
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v5, v2, v1, v6}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v2

    .line 1297757
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 1297758
    new-instance v5, Landroid/graphics/Canvas;

    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-direct {v5, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1297759
    if-eqz p1, :cond_0

    .line 1297760
    int-to-float v1, v12

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v1, v6

    int-to-float v6, v3

    const/high16 v7, 0x3f000000    # 0.5f

    mul-float/2addr v6, v7

    invoke-virtual {v5, v1, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1297761
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getRotation()F

    move-result v1

    int-to-float v3, v3

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v3, v6

    int-to-float v6, v12

    const/high16 v7, 0x3f000000    # 0.5f

    mul-float/2addr v6, v7

    invoke-virtual {v5, v1, v3, v6}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1297762
    :cond_0
    invoke-virtual {v0, v5}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1297763
    invoke-virtual {v4, v5}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1297764
    return-object v2

    :cond_1
    move v2, v3

    .line 1297765
    goto :goto_0

    :cond_2
    move v1, v12

    goto :goto_1
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;Landroid/content/res/Resources;)Lcom/facebook/photos/creativeediting/model/TextParams;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1297766
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1297767
    :cond_0
    :goto_0
    return-object v0

    .line 1297768
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/86n;->b:LX/8GT;

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getSessionId()Ljava/lang/String;

    move-result-object v2

    const-string v3, "png"

    invoke-virtual {v1, v2, v3}, LX/8GT;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1297769
    if-eqz v1, :cond_0

    .line 1297770
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1297771
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1, p2}, LX/86n;->a(ZLcom/facebook/friendsharing/inspiration/model/InspirationTextParams;Landroid/content/res/Resources;)LX/1FJ;

    move-result-object v3

    .line 1297772
    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {v0, v4, v5, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1297773
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 1297774
    invoke-static {v3}, LX/1FJ;->c(LX/1FJ;)V

    .line 1297775
    new-instance v0, LX/5jN;

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LX/5jN;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getLeftPercentage()F

    move-result v1

    .line 1297776
    iput v1, v0, LX/5jN;->c:F

    .line 1297777
    move-object v0, v0

    .line 1297778
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTopPercentage()F

    move-result v1

    .line 1297779
    iput v1, v0, LX/5jN;->d:F

    .line 1297780
    move-object v0, v0

    .line 1297781
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getWidthPercentage()F

    move-result v1

    .line 1297782
    iput v1, v0, LX/5jN;->e:F

    .line 1297783
    move-object v0, v0

    .line 1297784
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getHeightPercentage()F

    move-result v1

    .line 1297785
    iput v1, v0, LX/5jN;->f:F

    .line 1297786
    move-object v0, v0

    .line 1297787
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextColor()I

    move-result v1

    .line 1297788
    iput v1, v0, LX/5jN;->h:I

    .line 1297789
    move-object v0, v0

    .line 1297790
    const/4 v1, 0x0

    .line 1297791
    iput v1, v0, LX/5jN;->g:F

    .line 1297792
    move-object v0, v0

    .line 1297793
    const/4 v1, 0x0

    .line 1297794
    iput-object v1, v0, LX/5jN;->i:Ljava/lang/String;

    .line 1297795
    move-object v0, v0

    .line 1297796
    invoke-virtual {v0}, LX/5jN;->a()Lcom/facebook/photos/creativeediting/model/TextParams;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1297797
    :catch_0
    move-exception v0

    .line 1297798
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method
