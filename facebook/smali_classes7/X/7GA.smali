.class public LX/7GA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/7GA;


# instance fields
.field private final a:LX/7GF;

.field private final b:LX/0Uh;


# direct methods
.method public constructor <init>(LX/7GF;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1188886
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1188887
    iput-object p1, p0, LX/7GA;->a:LX/7GF;

    .line 1188888
    iput-object p2, p0, LX/7GA;->b:LX/0Uh;

    .line 1188889
    return-void
.end method

.method public static a(LX/0QB;)LX/7GA;
    .locals 5

    .prologue
    .line 1188858
    sget-object v0, LX/7GA;->c:LX/7GA;

    if-nez v0, :cond_1

    .line 1188859
    const-class v1, LX/7GA;

    monitor-enter v1

    .line 1188860
    :try_start_0
    sget-object v0, LX/7GA;->c:LX/7GA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1188861
    if-eqz v2, :cond_0

    .line 1188862
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1188863
    new-instance p0, LX/7GA;

    invoke-static {v0}, LX/7GF;->a(LX/0QB;)LX/7GF;

    move-result-object v3

    check-cast v3, LX/7GF;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/7GA;-><init>(LX/7GF;LX/0Uh;)V

    .line 1188864
    move-object v0, p0

    .line 1188865
    sput-object v0, LX/7GA;->c:LX/7GA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1188866
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1188867
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1188868
    :cond_1
    sget-object v0, LX/7GA;->c:LX/7GA;

    return-object v0

    .line 1188869
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1188870
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0m9;
    .locals 8

    .prologue
    .line 1188871
    iget-object v0, p0, LX/7GA;->a:LX/7GF;

    .line 1188872
    iget-object v1, v0, LX/7GF;->b:LX/6hN;

    invoke-virtual {v1}, LX/6hN;->d()I

    move-result v1

    .line 1188873
    iget-object v2, v0, LX/7GF;->b:LX/6hN;

    invoke-virtual {v2}, LX/6hN;->e()I

    move-result v2

    .line 1188874
    iget-object v3, v0, LX/7GF;->b:LX/6hN;

    invoke-virtual {v3}, LX/6hN;->c()I

    move-result v3

    .line 1188875
    iget-object v4, v0, LX/7GF;->b:LX/6hN;

    invoke-virtual {v4}, LX/6hN;->a()I

    move-result v4

    .line 1188876
    iget-object v5, v0, LX/7GF;->b:LX/6hN;

    invoke-virtual {v5}, LX/6hN;->b()I

    move-result v5

    .line 1188877
    iget-object v6, v0, LX/7GF;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v6

    sget-object v7, LX/5dQ;->FULL_SCREEN:LX/5dQ;

    iget-object v7, v7, LX/5dQ;->persistentIndex:Ljava/lang/Integer;

    invoke-static {v7}, LX/7GE;->a(Ljava/lang/Integer;)LX/0Tn;

    move-result-object v7

    invoke-interface {v6, v7, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    sget-object v6, LX/5dQ;->FULL_SCREEN:LX/5dQ;

    iget-object v6, v6, LX/5dQ;->persistentIndex:Ljava/lang/Integer;

    invoke-static {v6}, LX/7GE;->b(Ljava/lang/Integer;)LX/0Tn;

    move-result-object v6

    invoke-interface {v1, v6, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    sget-object v2, LX/5dQ;->LARGE_PREVIEW:LX/5dQ;

    iget-object v2, v2, LX/5dQ;->persistentIndex:Ljava/lang/Integer;

    invoke-static {v2}, LX/7GE;->a(Ljava/lang/Integer;)LX/0Tn;

    move-result-object v2

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    sget-object v2, LX/5dQ;->LARGE_PREVIEW:LX/5dQ;

    iget-object v2, v2, LX/5dQ;->persistentIndex:Ljava/lang/Integer;

    invoke-static {v2}, LX/7GE;->b(Ljava/lang/Integer;)LX/0Tn;

    move-result-object v2

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    sget-object v2, LX/5dQ;->MEDIUM_PREVIEW:LX/5dQ;

    iget-object v2, v2, LX/5dQ;->persistentIndex:Ljava/lang/Integer;

    invoke-static {v2}, LX/7GE;->a(Ljava/lang/Integer;)LX/0Tn;

    move-result-object v2

    invoke-interface {v1, v2, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    sget-object v2, LX/5dQ;->MEDIUM_PREVIEW:LX/5dQ;

    iget-object v2, v2, LX/5dQ;->persistentIndex:Ljava/lang/Integer;

    invoke-static {v2}, LX/7GE;->b(Ljava/lang/Integer;)LX/0Tn;

    move-result-object v2

    invoke-interface {v1, v2, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    sget-object v2, LX/5dQ;->SMALL_PREVIEW:LX/5dQ;

    iget-object v2, v2, LX/5dQ;->persistentIndex:Ljava/lang/Integer;

    invoke-static {v2}, LX/7GE;->a(Ljava/lang/Integer;)LX/0Tn;

    move-result-object v2

    invoke-interface {v1, v2, v5}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    sget-object v2, LX/5dQ;->SMALL_PREVIEW:LX/5dQ;

    iget-object v2, v2, LX/5dQ;->persistentIndex:Ljava/lang/Integer;

    invoke-static {v2}, LX/7GE;->b(Ljava/lang/Integer;)LX/0Tn;

    move-result-object v2

    invoke-interface {v1, v2, v5}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1188878
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 1188879
    const-string v1, "image_sizes"

    iget-object v2, p0, LX/7GA;->a:LX/7GF;

    invoke-virtual {v2}, LX/7GF;->b()LX/0m9;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1188880
    const-string v1, "animated_image_format"

    const-string v2, "WEBP,GIF"

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1188881
    iget-object v1, p0, LX/7GA;->b:LX/0Uh;

    const/16 v2, 0x1e4

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1188882
    const-string v1, "animated_image_sizes"

    iget-object v2, p0, LX/7GA;->a:LX/7GF;

    invoke-virtual {v2}, LX/7GF;->b()LX/0m9;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1188883
    :cond_0
    iget-object v1, p0, LX/7GA;->b:LX/0Uh;

    const/16 v2, 0x183

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_1

    .line 1188884
    const-string v1, "mini_preview"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1188885
    :cond_1
    return-object v0
.end method
