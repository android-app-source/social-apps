.class public final LX/8El;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1315719
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 1315720
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1315721
    :goto_0
    return v1

    .line 1315722
    :cond_0
    const-string v7, "is_active"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1315723
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    .line 1315724
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 1315725
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1315726
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1315727
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1315728
    const-string v7, "action_type"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1315729
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_1

    .line 1315730
    :cond_2
    const-string v7, "fields"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1315731
    invoke-static {p0, p1}, LX/8Ek;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1315732
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1315733
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1315734
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1315735
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1315736
    if-eqz v0, :cond_5

    .line 1315737
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 1315738
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1315739
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1315740
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1315741
    if-eqz v0, :cond_0

    .line 1315742
    const-string v0, "action_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1315743
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1315744
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1315745
    if-eqz v0, :cond_1

    .line 1315746
    const-string v1, "fields"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1315747
    invoke-static {p0, v0, p2, p3}, LX/8Ek;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1315748
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1315749
    if-eqz v0, :cond_2

    .line 1315750
    const-string v1, "is_active"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1315751
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1315752
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1315753
    return-void
.end method
