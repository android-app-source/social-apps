.class public LX/86j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/86d;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1297682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1297683
    return-void
.end method

.method public static a(LX/0QB;)LX/86j;
    .locals 3

    .prologue
    .line 1297684
    const-class v1, LX/86j;

    monitor-enter v1

    .line 1297685
    :try_start_0
    sget-object v0, LX/86j;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1297686
    sput-object v2, LX/86j;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1297687
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1297688
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1297689
    new-instance v0, LX/86j;

    invoke-direct {v0}, LX/86j;-><init>()V

    .line 1297690
    move-object v0, v0

    .line 1297691
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1297692
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/86j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1297693
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1297694
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;ZZ)Z
    .locals 1

    .prologue
    .line 1297695
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getParticleEffect()Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
