.class public LX/7yy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/io/File;

.field private final b:Ljava/lang/String;

.field private final c:J

.field public final d:J

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/7yx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/File;JJLjava/lang/String;)V
    .locals 6

    .prologue
    .line 1280523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280524
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7yy;->g:Ljava/util/Map;

    .line 1280525
    iput-object p1, p0, LX/7yy;->a:Ljava/io/File;

    .line 1280526
    iget-object v0, p0, LX/7yy;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7yy;->b:Ljava/lang/String;

    .line 1280527
    iput-wide p2, p0, LX/7yy;->c:J

    .line 1280528
    iput-wide p4, p0, LX/7yy;->d:J

    .line 1280529
    iput-object p6, p0, LX/7yy;->e:Ljava/lang/String;

    .line 1280530
    iget-object v0, p0, LX/7yy;->b:Ljava/lang/String;

    iget-wide v1, p0, LX/7yy;->d:J

    iget-object v3, p0, LX/7yy;->e:Ljava/lang/String;

    iget-wide v4, p0, LX/7yy;->c:J

    invoke-static/range {v0 .. v5}, LX/7yy;->a(Ljava/lang/String;JLjava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7yy;->f:Ljava/lang/String;

    .line 1280531
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1280514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280515
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7yy;->g:Ljava/util/Map;

    .line 1280516
    iput-object p1, p0, LX/7yy;->a:Ljava/io/File;

    .line 1280517
    iget-object v0, p0, LX/7yy;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7yy;->b:Ljava/lang/String;

    .line 1280518
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/7yy;->c:J

    .line 1280519
    iget-object v0, p0, LX/7yy;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, LX/7yy;->d:J

    .line 1280520
    iput-object p2, p0, LX/7yy;->e:Ljava/lang/String;

    .line 1280521
    iget-object v0, p0, LX/7yy;->b:Ljava/lang/String;

    iget-wide v1, p0, LX/7yy;->d:J

    iget-object v3, p0, LX/7yy;->e:Ljava/lang/String;

    iget-wide v4, p0, LX/7yy;->c:J

    invoke-static/range {v0 .. v5}, LX/7yy;->a(Ljava/lang/String;JLjava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7yy;->f:Ljava/lang/String;

    .line 1280522
    return-void
.end method

.method private static a(Ljava/lang/String;JLjava/lang/String;J)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1280504
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1280505
    const-string v2, "MD5"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 1280506
    const-string v3, "UTF-8"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v3

    .line 1280507
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 1280508
    const/4 v2, 0x0

    :goto_0
    array-length v5, v3

    if-ge v2, v5, :cond_0

    .line 1280509
    aget-byte v5, v3, v2

    and-int/lit16 v5, v5, 0xff

    or-int/lit16 v5, v5, 0x100

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    const/4 p0, 0x1

    const/4 p3, 0x3

    invoke-virtual {v5, p0, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1280510
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1280511
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1280512
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1280513
    :goto_1
    return-object v0

    :catch_0
    const-string v0, ""

    goto :goto_1
.end method

.method private static a(Ljava/io/InputStream;Ljava/lang/String;IJJ)[B
    .locals 11

    .prologue
    .line 1280470
    const/4 v0, 0x0

    .line 1280471
    :try_start_0
    invoke-static {p1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 1280472
    new-array v3, p2, [B

    .line 1280473
    invoke-virtual {p0, p3, p4}, Ljava/io/InputStream;->skip(J)J

    .line 1280474
    const/4 v1, 0x0

    :goto_0
    int-to-long v4, v1

    cmp-long v4, v4, p5

    if-gez v4, :cond_1

    .line 1280475
    const/4 v4, 0x0

    int-to-long v6, p2

    int-to-long v8, v1

    sub-long v8, p5, v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    long-to-int v5, v6

    invoke-virtual {p0, v3, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    .line 1280476
    if-gtz v4, :cond_0

    .line 1280477
    const/4 v0, 0x0

    .line 1280478
    :goto_1
    return-object v0

    .line 1280479
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/security/MessageDigest;->update([BII)V

    .line 1280480
    add-int/2addr v1, v4

    goto :goto_0

    .line 1280481
    :cond_1
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    :catch_0
    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)LX/7yx;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 1280485
    iget-object v0, p0, LX/7yy;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1280486
    iget-object v0, p0, LX/7yy;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yx;

    .line 1280487
    :cond_0
    :goto_0
    return-object v0

    .line 1280488
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v0, p0, LX/7yy;->a:Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1280489
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    .line 1280490
    iget-wide v4, p0, LX/7yy;->c:J

    iget-wide v6, p0, LX/7yy;->d:J

    move-object v2, p1

    move v3, p2

    invoke-static/range {v1 .. v7}, LX/7yy;->a(Ljava/io/InputStream;Ljava/lang/String;IJJ)[B

    move-result-object v2

    .line 1280491
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v10

    .line 1280492
    if-eqz v2, :cond_3

    .line 1280493
    new-instance v0, LX/7yx;

    invoke-direct {v0, v2, v4, v5}, LX/7yx;-><init>([BJ)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1280494
    :try_start_2
    iget-object v2, p0, LX/7yy;->g:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1280495
    :goto_1
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 1280496
    :catch_0
    goto :goto_0

    .line 1280497
    :catch_1
    move-object v0, v8

    move-object v1, v8

    :goto_2
    if-eqz v1, :cond_0

    .line 1280498
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1280499
    :catch_2
    goto :goto_0

    .line 1280500
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_3
    if-eqz v1, :cond_2

    .line 1280501
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1280502
    :cond_2
    :goto_4
    throw v0

    :catch_3
    goto :goto_4

    .line 1280503
    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_4
    move-object v0, v8

    goto :goto_2

    :catch_5
    goto :goto_2

    :cond_3
    move-object v0, v8

    goto :goto_1
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1280484
    iget-wide v0, p0, LX/7yy;->c:J

    return-wide v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 1280483
    iget-wide v0, p0, LX/7yy;->d:J

    return-wide v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1280482
    iget-object v0, p0, LX/7yy;->f:Ljava/lang/String;

    return-object v0
.end method
