.class public final LX/7Xp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/7Xr;


# direct methods
.method public constructor <init>(LX/7Xr;)V
    .locals 0

    .prologue
    .line 1218733
    iput-object p1, p0, LX/7Xp;->a:LX/7Xr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x45cc5a9d

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1218734
    iget-object v1, p0, LX/7Xp;->a:LX/7Xr;

    iget-object v1, v1, LX/7Xr;->a:LX/11x;

    if-eqz v1, :cond_0

    .line 1218735
    iget-object v1, p0, LX/7Xp;->a:LX/7Xr;

    iget-object v1, v1, LX/7Xr;->a:LX/11x;

    .line 1218736
    iget-object v3, v1, LX/11x;->a:LX/11v;

    .line 1218737
    invoke-static {v3}, LX/11v;->f(LX/11v;)V

    .line 1218738
    iget-object p0, v3, LX/11v;->k:LX/0Zb;

    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "dismiss"

    invoke-direct {p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v1, v3, LX/11v;->m:Ljava/lang/String;

    .line 1218739
    iput-object v1, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1218740
    move-object p1, p1

    .line 1218741
    const-string v1, "zero_indicator"

    .line 1218742
    iput-object v1, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1218743
    move-object p1, p1

    .line 1218744
    const-string v1, "zero_indicator_close"

    .line 1218745
    iput-object v1, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 1218746
    move-object p1, p1

    .line 1218747
    invoke-interface {p0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1218748
    iget-object p0, v3, LX/11v;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p0

    sget-object p1, LX/0yY;->ZERO_INDICATOR:LX/0yY;

    invoke-static {p1}, LX/0df;->a(LX/0yY;)LX/0Tn;

    move-result-object p1

    const/4 v1, 0x0

    invoke-interface {p0, p1, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object p0

    invoke-interface {p0}, LX/0hN;->commit()V

    .line 1218749
    :cond_0
    const v1, -0x62b1306f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
