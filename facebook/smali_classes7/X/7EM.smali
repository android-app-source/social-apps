.class public final LX/7EM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/structuredsurvey/StructuredSurveyController;

.field public final synthetic b:Ljava/lang/Runnable;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:LX/1x7;


# direct methods
.method public constructor <init>(LX/1x7;Lcom/facebook/structuredsurvey/StructuredSurveyController;Ljava/lang/Runnable;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1184656
    iput-object p1, p0, LX/7EM;->d:LX/1x7;

    iput-object p2, p0, LX/7EM;->a:Lcom/facebook/structuredsurvey/StructuredSurveyController;

    iput-object p3, p0, LX/7EM;->b:Ljava/lang/Runnable;

    iput-object p4, p0, LX/7EM;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1184632
    iget-object v0, p0, LX/7EM;->d:LX/1x7;

    iget-object v1, p0, LX/7EM;->d:LX/1x7;

    iget-object v1, v1, LX/1x7;->o:Landroid/content/res/Resources;

    const v2, 0x7f081a8c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/7EM;->c:Landroid/content/Context;

    invoke-static {v0, v1, v2}, LX/1x7;->a$redex0(LX/1x7;Ljava/lang/String;Landroid/content/Context;)V

    .line 1184633
    iget-object v0, p0, LX/7EM;->d:LX/1x7;

    iget-object v0, v0, LX/1x7;->i:LX/03V;

    sget-object v1, LX/1x7;->b:Ljava/lang/String;

    const-string v2, "NaRF:Survey GraphQL Fetch Failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1184634
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1184635
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1184636
    if-nez p1, :cond_0

    .line 1184637
    :goto_0
    return-void

    .line 1184638
    :cond_0
    :try_start_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1184639
    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFragmentModel;

    .line 1184640
    iget-object v1, p0, LX/7EM;->a:Lcom/facebook/structuredsurvey/StructuredSurveyController;

    iget-object v2, p0, LX/7EM;->b:Ljava/lang/Runnable;

    .line 1184641
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFragmentModel;->j()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->t:Ljava/lang/String;

    .line 1184642
    new-instance v3, LX/7ER;

    invoke-direct {v3}, LX/7ER;-><init>()V

    iput-object v3, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->q:LX/7ER;

    .line 1184643
    new-instance v3, LX/31O;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFragmentModel;->k()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;

    move-result-object v4

    iget-object p1, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->q:LX/7ER;

    invoke-direct {v3, v4, p1}, LX/31O;-><init>(Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;LX/7ER;)V

    iput-object v3, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->o:LX/31O;

    .line 1184644
    new-instance v3, LX/1ww;

    invoke-direct {v3}, LX/1ww;-><init>()V

    invoke-virtual {v3}, LX/1ww;->k()LX/1x4;

    move-result-object v3

    iput-object v3, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->r:LX/1x4;

    .line 1184645
    new-instance v3, LX/7EJ;

    iget-object v4, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->D:Landroid/content/res/Resources;

    iget-object p1, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->r:LX/1x4;

    invoke-direct {v3, v4, p1}, LX/7EJ;-><init>(Landroid/content/res/Resources;LX/1x4;)V

    iput-object v3, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    .line 1184646
    iget-object v3, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    iget-object v4, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->D:Landroid/content/res/Resources;

    const p1, 0x7f081a88

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/7EJ;->a(Ljava/lang/String;)V

    .line 1184647
    iget-object v3, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    iget-object v4, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->D:Landroid/content/res/Resources;

    const p1, 0x7f081a82

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/7EJ;->b(Ljava/lang/String;)V

    .line 1184648
    iget-object v3, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    iget-object v4, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->D:Landroid/content/res/Resources;

    const p1, 0x7f081a83

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/7EJ;->c(Ljava/lang/String;)V

    .line 1184649
    iget-object v3, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    iget-object v4, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->o:LX/31O;

    invoke-virtual {v4}, LX/31O;->b()I

    move-result v4

    .line 1184650
    iput v4, v3, LX/7EJ;->d:I

    .line 1184651
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1184652
    goto :goto_0

    .line 1184653
    :catch_0
    move-exception v0

    .line 1184654
    iget-object v1, p0, LX/7EM;->d:LX/1x7;

    iget-object v2, p0, LX/7EM;->d:LX/1x7;

    iget-object v2, v2, LX/1x7;->o:Landroid/content/res/Resources;

    const v3, 0x7f081a89

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/7EM;->c:Landroid/content/Context;

    invoke-static {v1, v2, v3}, LX/1x7;->a$redex0(LX/1x7;Ljava/lang/String;Landroid/content/Context;)V

    .line 1184655
    iget-object v1, p0, LX/7EM;->d:LX/1x7;

    iget-object v1, v1, LX/1x7;->i:LX/03V;

    sget-object v2, LX/1x7;->b:Ljava/lang/String;

    const-string v3, "NaRF:Survey Model Init Failed"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method
