.class public LX/8NX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/8NY;",
        "LX/8NZ;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1337384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/8NY;)LX/14N;
    .locals 14

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x1

    .line 1337385
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1337386
    iget-object v1, p0, LX/8NY;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1337387
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1337388
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "composer_session_id"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337389
    :cond_0
    iget-object v2, p0, LX/8NY;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1337390
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1337391
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "original_file_hash"

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337392
    :cond_1
    iget-wide v12, p0, LX/8NY;->a:J

    move-wide v2, v12

    .line 1337393
    cmp-long v4, v2, v10

    if-lez v4, :cond_2

    .line 1337394
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "file_size"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v5, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337395
    :cond_2
    iget-object v2, p0, LX/8NY;->e:Ljava/lang/String;

    move-object v2, v2

    .line 1337396
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1337397
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "container_type"

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337398
    :cond_3
    iget-object v2, p0, LX/8NY;->k:LX/8Oa;

    move-object v2, v2

    .line 1337399
    if-eqz v2, :cond_4

    .line 1337400
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "partition_start_offset"

    iget-wide v6, v2, LX/8Oa;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337401
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "partition_end_offset"

    iget-wide v6, v2, LX/8Oa;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337402
    :cond_4
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "published"

    .line 1337403
    iget-boolean v4, p0, LX/8NY;->i:Z

    move v4, v4

    .line 1337404
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337405
    iget-wide v12, p0, LX/8NY;->j:J

    move-wide v2, v12

    .line 1337406
    cmp-long v2, v2, v10

    if-lez v2, :cond_5

    .line 1337407
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "scheduled_publish_time"

    .line 1337408
    iget-wide v12, p0, LX/8NY;->j:J

    move-wide v4, v12

    .line 1337409
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337410
    :cond_5
    iget-object v2, p0, LX/8NY;->f:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object v2, v2

    .line 1337411
    if-eqz v2, :cond_6

    .line 1337412
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "proxied_app_id"

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337413
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "proxied_app_name"

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337414
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "android_key_hash"

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337415
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "user_selected_tags"

    .line 1337416
    iget-boolean v4, p0, LX/8NY;->g:Z

    move v4, v4

    .line 1337417
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337418
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "user_selected_place"

    .line 1337419
    iget-boolean v4, p0, LX/8NY;->h:Z

    move v4, v4

    .line 1337420
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337421
    :cond_6
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "spherical"

    .line 1337422
    iget-boolean v4, p0, LX/8NY;->l:Z

    move v4, v4

    .line 1337423
    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337424
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "v2.3/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1337425
    iget-wide v12, p0, LX/8NY;->b:J

    move-wide v4, v12

    .line 1337426
    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/videos"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1337427
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "upload_phase"

    const-string v5, "start"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337428
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v3

    const-string v4, "upload-video-chunk-start"

    .line 1337429
    iput-object v4, v3, LX/14O;->b:Ljava/lang/String;

    .line 1337430
    move-object v3, v3

    .line 1337431
    const-string v4, "POST"

    .line 1337432
    iput-object v4, v3, LX/14O;->c:Ljava/lang/String;

    .line 1337433
    move-object v3, v3

    .line 1337434
    iput-object v2, v3, LX/14O;->d:Ljava/lang/String;

    .line 1337435
    move-object v2, v3

    .line 1337436
    sget-object v3, LX/14S;->JSON:LX/14S;

    .line 1337437
    iput-object v3, v2, LX/14O;->k:LX/14S;

    .line 1337438
    move-object v2, v2

    .line 1337439
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1337440
    iput-object v0, v2, LX/14O;->g:Ljava/util/List;

    .line 1337441
    move-object v0, v2

    .line 1337442
    iput-boolean v8, v0, LX/14O;->n:Z

    .line 1337443
    move-object v0, v0

    .line 1337444
    iput-boolean v8, v0, LX/14O;->p:Z

    .line 1337445
    move-object v0, v0

    .line 1337446
    iput-object v1, v0, LX/14O;->A:Ljava/lang/String;

    .line 1337447
    move-object v0, v0

    .line 1337448
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/8NX;
    .locals 1

    .prologue
    .line 1337449
    new-instance v0, LX/8NX;

    invoke-direct {v0}, LX/8NX;-><init>()V

    .line 1337450
    move-object v0, v0

    .line 1337451
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)LX/14N;
    .locals 1

    .prologue
    .line 1337452
    check-cast p1, LX/8NY;

    invoke-static {p1}, LX/8NX;->a(LX/8NY;)LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1337453
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1337454
    const/4 v8, 0x0

    .line 1337455
    const-string v1, "skip_upload"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1337456
    const-string v1, "skip_upload"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->F()Z

    move-result v8

    .line 1337457
    :cond_0
    new-instance v1, LX/8NZ;

    const-string v2, "upload_session_id"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v2

    const-string v3, "video_id"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-virtual {v3}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v3

    const-string v4, "start_offset"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->D()J

    move-result-wide v4

    const-string v6, "end_offset"

    invoke-virtual {v0, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->D()J

    move-result-wide v6

    invoke-direct/range {v1 .. v8}, LX/8NZ;-><init>(Ljava/lang/String;Ljava/lang/String;JJZ)V

    return-object v1
.end method
