.class public final LX/7fy;
.super LX/1Ah;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Ah",
        "<",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/apptab/ui/DownloadableImageTabView;


# direct methods
.method public constructor <init>(Lcom/facebook/apptab/ui/DownloadableImageTabView;)V
    .locals 0

    .prologue
    .line 1223759
    iput-object p1, p0, LX/7fy;->a:Lcom/facebook/apptab/ui/DownloadableImageTabView;

    invoke-direct {p0}, LX/1Ah;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1223760
    iget-object v0, p0, LX/7fy;->a:Lcom/facebook/apptab/ui/DownloadableImageTabView;

    iget-object v0, v0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7fy;->a:Lcom/facebook/apptab/ui/DownloadableImageTabView;

    iget-object v0, v0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    .line 1223761
    iget-object v1, v0, LX/1aX;->f:LX/1aZ;

    move-object v0, v1

    .line 1223762
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7fy;->a:Lcom/facebook/apptab/ui/DownloadableImageTabView;

    iget-object v0, v0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1223763
    iget-object v0, p0, LX/7fy;->a:Lcom/facebook/apptab/ui/DownloadableImageTabView;

    iget-object v1, p0, LX/7fy;->a:Lcom/facebook/apptab/ui/DownloadableImageTabView;

    iget-object v1, v1, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/apptab/ui/DownloadableImageTabView;->setGlyphImageWithoutChangeColorSize(Landroid/graphics/drawable/Drawable;)V

    .line 1223764
    :cond_0
    return-void
.end method
