.class public final LX/7LG;
.super LX/1cC;
.source ""


# instance fields
.field public a:Z

.field public final synthetic b:Lcom/facebook/video/player/FullScreenVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V
    .locals 1

    .prologue
    .line 1196923
    iput-object p1, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-direct {p0}, LX/1cC;-><init>()V

    .line 1196924
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7LG;->a:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/player/FullScreenVideoPlayer;B)V
    .locals 0

    .prologue
    .line 1196922
    invoke-direct {p0, p1}, LX/7LG;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1196896
    invoke-virtual {p0}, LX/7LG;->b()V

    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1196906
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7LG;->a:Z

    .line 1196907
    iget-object v0, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getTopLevelDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1196908
    sget-object v1, LX/7L4;->a:[I

    iget-object v2, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v2, v2, Lcom/facebook/video/player/FullScreenVideoPlayer;->aH:LX/7LF;

    invoke-virtual {v2}, LX/7LF;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1196909
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Unknown action"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1196910
    :pswitch_0
    iget-object v1, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->ay:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1}, LX/0SQ;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1196911
    iget-object v1, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->h()V

    .line 1196912
    iget-object v1, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-boolean v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->w:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->az:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1196913
    iget-object v1, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->i()V

    .line 1196914
    iget-object v1, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-static {v1, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->d(Lcom/facebook/video/player/FullScreenVideoPlayer;Landroid/graphics/drawable/Drawable;)V

    .line 1196915
    :cond_0
    :goto_0
    :pswitch_1
    iget-object v0, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    sget-object v1, LX/7LF;->NOP:LX/7LF;

    .line 1196916
    iput-object v1, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aH:LX/7LF;

    .line 1196917
    return-void

    .line 1196918
    :cond_1
    iget-object v1, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1196919
    :pswitch_2
    iget-object v1, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->h()V

    .line 1196920
    iget-object v1, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-static {v1, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->c(Lcom/facebook/video/player/FullScreenVideoPlayer;Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1196921
    iget-object v1, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->K:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-static {v1, v0}, LX/1r0;->a(Landroid/widget/RelativeLayout$LayoutParams;Landroid/graphics/Rect;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1196897
    sget-object v0, LX/7L4;->a:[I

    iget-object v1, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->aH:LX/7LF;

    invoke-virtual {v1}, LX/7LF;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1196898
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Unknown action"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1196899
    :pswitch_0
    iget-object v0, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ay:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0}, LX/0SQ;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1196900
    iget-object v0, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    .line 1196901
    iput-object v2, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->az:Landroid/view/View;

    .line 1196902
    iget-object v0, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1196903
    :cond_0
    :pswitch_1
    iget-object v0, p0, LX/7LG;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    sget-object v1, LX/7LF;->NOP:LX/7LF;

    .line 1196904
    iput-object v1, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aH:LX/7LF;

    .line 1196905
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
