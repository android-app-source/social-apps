.class public final LX/6gU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1125386
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1125387
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125388
    :goto_0
    return v1

    .line 1125389
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125390
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1125391
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1125392
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1125393
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1125394
    const-string v5, "section_id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1125395
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1125396
    :cond_2
    const-string v5, "section_title"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1125397
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1125398
    :cond_3
    const-string v5, "section_units"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1125399
    const/4 v4, 0x0

    .line 1125400
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_c

    .line 1125401
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125402
    :goto_2
    move v0, v4

    .line 1125403
    goto :goto_1

    .line 1125404
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1125405
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1125406
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1125407
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1125408
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 1125409
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125410
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_b

    .line 1125411
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1125412
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1125413
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_7

    if-eqz v7, :cond_7

    .line 1125414
    const-string v8, "edges"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1125415
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1125416
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_8

    .line 1125417
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_8

    .line 1125418
    const/4 v8, 0x0

    .line 1125419
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_10

    .line 1125420
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125421
    :goto_5
    move v7, v8

    .line 1125422
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1125423
    :cond_8
    invoke-static {v6, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 1125424
    goto :goto_3

    .line 1125425
    :cond_9
    const-string v8, "order_token"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1125426
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 1125427
    :cond_a
    const-string v8, "page_info"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1125428
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1125429
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v9, :cond_16

    .line 1125430
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125431
    :goto_6
    move v0, v7

    .line 1125432
    goto :goto_3

    .line 1125433
    :cond_b
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1125434
    invoke-virtual {p1, v4, v6}, LX/186;->b(II)V

    .line 1125435
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v5}, LX/186;->b(II)V

    .line 1125436
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1125437
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_2

    :cond_c
    move v0, v4

    move v5, v4

    move v6, v4

    goto/16 :goto_3

    .line 1125438
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125439
    :cond_e
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_f

    .line 1125440
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1125441
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1125442
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_e

    if-eqz v9, :cond_e

    .line 1125443
    const-string v10, "node"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 1125444
    invoke-static {p0, p1}, LX/6gT;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_7

    .line 1125445
    :cond_f
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1125446
    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1125447
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_5

    :cond_10
    move v7, v8

    goto :goto_7

    .line 1125448
    :cond_11
    const-string v12, "has_next_page"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_13

    .line 1125449
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v9, v0

    move v0, v8

    .line 1125450
    :cond_12
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_14

    .line 1125451
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1125452
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1125453
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_12

    if-eqz v11, :cond_12

    .line 1125454
    const-string v12, "end_cursor"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_11

    .line 1125455
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_8

    .line 1125456
    :cond_13
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_8

    .line 1125457
    :cond_14
    const/4 v11, 0x2

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1125458
    invoke-virtual {p1, v7, v10}, LX/186;->b(II)V

    .line 1125459
    if-eqz v0, :cond_15

    .line 1125460
    invoke-virtual {p1, v8, v9}, LX/186;->a(IZ)V

    .line 1125461
    :cond_15
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_6

    :cond_16
    move v0, v7

    move v9, v7

    move v10, v7

    goto :goto_8
.end method
