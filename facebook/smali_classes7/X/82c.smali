.class public LX/82c;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# static fields
.field public static final a:Landroid/graphics/Rect;


# instance fields
.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1288360
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/82c;->a:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1288361
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/82c;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 1288362
    return-void
.end method

.method public constructor <init>([Landroid/graphics/drawable/Drawable;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1288315
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1288316
    iput-boolean v1, p0, LX/82c;->d:Z

    .line 1288317
    if-eqz p1, :cond_1

    array-length v0, p1

    .line 1288318
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, LX/82c;->b:Ljava/util/List;

    .line 1288319
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, LX/82c;->c:Ljava/util/List;

    .line 1288320
    if-eqz p1, :cond_2

    .line 1288321
    array-length v2, p1

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_2

    .line 1288322
    aget-object v1, p1, v0

    const/4 v3, 0x0

    .line 1288323
    if-nez v3, :cond_0

    .line 1288324
    sget-object v3, LX/82c;->a:Landroid/graphics/Rect;

    .line 1288325
    :cond_0
    iget-object v4, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1288326
    iget-object v4, p0, LX/82c;->c:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1288327
    invoke-virtual {v1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1288328
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1288329
    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    .line 1288330
    :cond_2
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1288374
    const/4 v0, 0x0

    iget-object v1, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1288375
    iget-object v0, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1288376
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1288377
    :cond_0
    return-void
.end method

.method public final getOpacity()I
    .locals 4

    .prologue
    .line 1288363
    iget-boolean v0, p0, LX/82c;->d:Z

    if-eqz v0, :cond_0

    .line 1288364
    iget v0, p0, LX/82c;->e:I

    .line 1288365
    :goto_0
    return v0

    .line 1288366
    :cond_0
    const/4 v1, -0x2

    .line 1288367
    const/4 v0, 0x0

    iget-object v2, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    .line 1288368
    iget-object v0, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    invoke-static {v2, v0}, Landroid/graphics/drawable/Drawable;->resolveOpacity(II)I

    move-result v2

    .line 1288369
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1288370
    :cond_1
    iput v2, p0, LX/82c;->e:I

    .line 1288371
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/82c;->d:Z

    .line 1288372
    iget v0, p0, LX/82c;->e:I

    goto :goto_0
.end method

.method public final getPadding(Landroid/graphics/Rect;)Z
    .locals 2

    .prologue
    .line 1288373
    iget-object v0, p0, LX/82c;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1288351
    invoke-virtual {p0}, LX/82c;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v0

    .line 1288352
    if-eqz v0, :cond_0

    .line 1288353
    invoke-interface {v0, p0}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1288354
    :cond_0
    return-void
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 8

    .prologue
    .line 1288355
    const/4 v0, 0x0

    iget-object v1, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 1288356
    iget-object v0, p0, LX/82c;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 1288357
    iget-object v1, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    iget v4, p1, Landroid/graphics/Rect;->left:I

    iget v5, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v5

    iget v5, p1, Landroid/graphics/Rect;->top:I

    iget v6, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v6

    iget v6, p1, Landroid/graphics/Rect;->right:I

    iget v7, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v7

    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v7, v0

    invoke-virtual {v1, v4, v5, v6, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1288358
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1288359
    :cond_0
    return-void
.end method

.method public final onLevelChange(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1288347
    iget-object v1, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 1288348
    iget-object v0, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    move-result v0

    or-int/2addr v2, v0

    .line 1288349
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1288350
    :cond_0
    return v2
.end method

.method public final onStateChange([I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1288343
    iget-object v1, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 1288344
    iget-object v0, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v0

    or-int/2addr v2, v0

    .line 1288345
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1288346
    :cond_0
    return v2
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 1288339
    invoke-virtual {p0}, LX/82c;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v0

    .line 1288340
    if-eqz v0, :cond_0

    .line 1288341
    invoke-interface {v0, p0, p2, p3, p4}, Landroid/graphics/drawable/Drawable$Callback;->scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V

    .line 1288342
    :cond_0
    return-void
.end method

.method public final setAlpha(I)V
    .locals 3

    .prologue
    .line 1288335
    const/4 v0, 0x0

    iget-object v1, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1288336
    iget-object v0, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1288337
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1288338
    :cond_0
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 3

    .prologue
    .line 1288331
    const/4 v0, 0x0

    iget-object v1, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1288332
    iget-object v0, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1288333
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1288334
    :cond_0
    return-void
.end method

.method public final setDither(Z)V
    .locals 3

    .prologue
    .line 1288311
    const/4 v0, 0x0

    iget-object v1, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1288312
    iget-object v0, p0, LX/82c;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 1288313
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1288314
    :cond_0
    return-void
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1288307
    invoke-virtual {p0}, LX/82c;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v0

    .line 1288308
    if-eqz v0, :cond_0

    .line 1288309
    invoke-interface {v0, p0, p2}, Landroid/graphics/drawable/Drawable$Callback;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V

    .line 1288310
    :cond_0
    return-void
.end method
