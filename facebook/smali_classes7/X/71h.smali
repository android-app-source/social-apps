.class public final LX/71h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/payments/selector/model/DividerSelectorRow;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1163596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1163597
    new-instance v0, Lcom/facebook/payments/selector/model/DividerSelectorRow;

    invoke-direct {v0}, Lcom/facebook/payments/selector/model/DividerSelectorRow;-><init>()V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1163598
    new-array v0, p1, [Lcom/facebook/payments/selector/model/DividerSelectorRow;

    return-object v0
.end method
