.class public final LX/7rq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1263574
    const/4 v11, 0x0

    .line 1263575
    const/4 v10, 0x0

    .line 1263576
    const/4 v9, 0x0

    .line 1263577
    const/4 v8, 0x0

    .line 1263578
    const-wide/16 v6, 0x0

    .line 1263579
    const/4 v5, 0x0

    .line 1263580
    const/4 v4, 0x0

    .line 1263581
    const/4 v3, 0x0

    .line 1263582
    const/4 v2, 0x0

    .line 1263583
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_c

    .line 1263584
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1263585
    const/4 v2, 0x0

    .line 1263586
    :goto_0
    return v2

    .line 1263587
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v13, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v13, :cond_a

    .line 1263588
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1263589
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1263590
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v13, v14, :cond_0

    if-eqz v3, :cond_0

    .line 1263591
    const-string v13, "__type__"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_1

    const-string v13, "__typename"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 1263592
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 1263593
    :cond_2
    const-string v13, "action_name"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 1263594
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v11, v3

    goto :goto_1

    .line 1263595
    :cond_3
    const-string v13, "activity_id"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 1263596
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 1263597
    :cond_4
    const-string v13, "actors"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 1263598
    invoke-static/range {p0 .. p1}, LX/7rn;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 1263599
    :cond_5
    const-string v13, "create_time"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 1263600
    const/4 v2, 0x1

    .line 1263601
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 1263602
    :cond_6
    const-string v13, "notif_type"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 1263603
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 1263604
    :cond_7
    const-string v13, "target_story"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 1263605
    invoke-static/range {p0 .. p1}, LX/7ro;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 1263606
    :cond_8
    const-string v13, "title"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1263607
    invoke-static/range {p0 .. p1}, LX/7rp;->a(LX/15w;LX/186;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 1263608
    :cond_9
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1263609
    :cond_a
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1263610
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1263611
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1263612
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1263613
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1263614
    if-eqz v2, :cond_b

    .line 1263615
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1263616
    :cond_b
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1263617
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1263618
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1263619
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v12, v11

    move v11, v10

    move v10, v5

    move-wide v15, v6

    move v6, v8

    move v7, v9

    move v9, v4

    move v8, v3

    move-wide v4, v15

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1263620
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1263621
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1263622
    if-eqz v0, :cond_0

    .line 1263623
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263624
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1263625
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1263626
    if-eqz v0, :cond_1

    .line 1263627
    const-string v1, "action_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263628
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1263629
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1263630
    if-eqz v0, :cond_2

    .line 1263631
    const-string v1, "activity_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263632
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1263633
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1263634
    if-eqz v0, :cond_4

    .line 1263635
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263636
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1263637
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 1263638
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/7rn;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1263639
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1263640
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1263641
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1263642
    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 1263643
    const-string v2, "create_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263644
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1263645
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1263646
    if-eqz v0, :cond_6

    .line 1263647
    const-string v1, "notif_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263648
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1263649
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1263650
    if-eqz v0, :cond_7

    .line 1263651
    const-string v1, "target_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263652
    invoke-static {p0, v0, p2}, LX/7ro;->a(LX/15i;ILX/0nX;)V

    .line 1263653
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1263654
    if-eqz v0, :cond_d

    .line 1263655
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263656
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1263657
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1263658
    if-eqz v1, :cond_b

    .line 1263659
    const-string v2, "ranges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263660
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1263661
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_a

    .line 1263662
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    const/4 p3, 0x0

    .line 1263663
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1263664
    invoke-virtual {p0, v3, p3, p3}, LX/15i;->a(III)I

    move-result v4

    .line 1263665
    if-eqz v4, :cond_8

    .line 1263666
    const-string p1, "length"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263667
    invoke-virtual {p2, v4}, LX/0nX;->b(I)V

    .line 1263668
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4, p3}, LX/15i;->a(III)I

    move-result v4

    .line 1263669
    if-eqz v4, :cond_9

    .line 1263670
    const-string p1, "offset"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263671
    invoke-virtual {p2, v4}, LX/0nX;->b(I)V

    .line 1263672
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1263673
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1263674
    :cond_a
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1263675
    :cond_b
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1263676
    if-eqz v1, :cond_c

    .line 1263677
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1263678
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1263679
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1263680
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1263681
    return-void
.end method
