.class public final LX/7PH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Di;


# instance fields
.field public final synthetic a:LX/7PI;

.field public final b:LX/3Di;

.field private final c:J

.field public d:J

.field public e:Ljava/io/OutputStream;

.field public f:LX/1iJ;

.field private g:J

.field private h:Lcom/facebook/video/server/ThrottlingAsyncWriter$ThrottledHandler$ThrottledRunnable;


# direct methods
.method public constructor <init>(LX/7PI;JJLX/3Di;)V
    .locals 0

    .prologue
    .line 1202704
    iput-object p1, p0, LX/7PH;->a:LX/7PI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202705
    iput-object p6, p0, LX/7PH;->b:LX/3Di;

    .line 1202706
    iput-wide p2, p0, LX/7PH;->c:J

    .line 1202707
    iput-wide p2, p0, LX/7PH;->g:J

    .line 1202708
    iput-wide p4, p0, LX/7PH;->d:J

    .line 1202709
    return-void
.end method

.method public static a$redex0(LX/7PH;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x14

    const-wide/16 v2, -0x2

    .line 1202725
    iget-object v0, p0, LX/7PH;->a:LX/7PI;

    iget-object v0, v0, LX/7PI;->c:LX/7Pw;

    .line 1202726
    iget-object v1, v0, LX/7Pw;->c:LX/2WF;

    move-object v0, v1

    .line 1202727
    if-nez v0, :cond_0

    move-wide v0, v2

    .line 1202728
    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v0, v4

    if-nez v4, :cond_2

    .line 1202729
    new-instance v0, LX/7Og;

    const-string v1, "Throttling stopped"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/7Og;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1202730
    iget-object v1, p0, LX/7PH;->e:Ljava/io/OutputStream;

    if-nez v1, :cond_1

    .line 1202731
    iget-object v1, p0, LX/7PH;->b:LX/3Di;

    invoke-interface {v1, v0}, LX/3Di;->a(Ljava/io/IOException;)V

    .line 1202732
    :goto_1
    return-void

    .line 1202733
    :cond_0
    iget-object v0, p0, LX/7PH;->a:LX/7PI;

    iget-object v0, v0, LX/7PI;->b:LX/7PJ;

    iget-object v1, p0, LX/7PH;->a:LX/7PI;

    iget-object v1, v1, LX/7PI;->c:LX/7Pw;

    iget-wide v4, p0, LX/7PH;->g:J

    invoke-interface {v0, v1, v4, v5}, LX/7PJ;->a(LX/7Pw;J)J

    move-result-wide v0

    goto :goto_0

    .line 1202734
    :cond_1
    iget-object v1, p0, LX/7PH;->b:LX/3Di;

    iget-object v2, p0, LX/7PH;->e:Ljava/io/OutputStream;

    invoke-interface {v1, v2, v0}, LX/3Di;->a(Ljava/io/OutputStream;Ljava/io/IOException;)V

    goto :goto_1

    .line 1202735
    :cond_2
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_5

    .line 1202736
    iget-object v0, p0, LX/7PH;->a:LX/7PI;

    iget-boolean v0, v0, LX/7PI;->e:Z

    if-eqz v0, :cond_4

    .line 1202737
    iget-object v0, p0, LX/7PH;->h:Lcom/facebook/video/server/ThrottlingAsyncWriter$ThrottledHandler$ThrottledRunnable;

    if-nez v0, :cond_3

    .line 1202738
    new-instance v0, Lcom/facebook/video/server/ThrottlingAsyncWriter$ThrottledHandler$ThrottledRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/video/server/ThrottlingAsyncWriter$ThrottledHandler$ThrottledRunnable;-><init>(LX/7PH;)V

    iput-object v0, p0, LX/7PH;->h:Lcom/facebook/video/server/ThrottlingAsyncWriter$ThrottledHandler$ThrottledRunnable;

    .line 1202739
    :cond_3
    iget-object v0, p0, LX/7PH;->a:LX/7PI;

    iget-object v0, v0, LX/7PI;->d:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/7PH;->h:Lcom/facebook/video/server/ThrottlingAsyncWriter$ThrottledHandler$ThrottledRunnable;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v6, v7, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_1

    .line 1202740
    :cond_4
    iget-object v0, p0, LX/7PH;->a:LX/7PI;

    iget-object v0, v0, LX/7PI;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/video/server/ThrottlingAsyncWriter$ThrottledHandler$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/server/ThrottlingAsyncWriter$ThrottledHandler$1;-><init>(LX/7PH;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v6, v7, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_1

    .line 1202741
    :cond_5
    cmp-long v2, v0, v2

    if-nez v2, :cond_6

    iget-wide v0, p0, LX/7PH;->d:J

    .line 1202742
    :goto_2
    iget-object v2, p0, LX/7PH;->a:LX/7PI;

    iget-wide v4, p0, LX/7PH;->g:J

    invoke-static {v2, v4, v5, v0, v1}, LX/7PI;->a$redex0(LX/7PI;JJ)J

    move-result-wide v4

    .line 1202743
    iget-object v0, p0, LX/7PH;->a:LX/7PI;

    iget-object v1, v0, LX/7PI;->a:LX/3DW;

    iget-wide v2, p0, LX/7PH;->g:J

    move-object v6, p0

    invoke-interface/range {v1 .. v6}, LX/3DW;->a(JJLX/3Di;)V

    goto :goto_1

    .line 1202744
    :cond_6
    iget-wide v2, p0, LX/7PH;->g:J

    add-long/2addr v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/3Dd;)Ljava/io/OutputStream;
    .locals 5

    .prologue
    .line 1202745
    iget-object v0, p0, LX/7PH;->f:LX/1iJ;

    if-nez v0, :cond_0

    .line 1202746
    const-wide/16 v3, 0x0

    .line 1202747
    iget-object v1, p0, LX/7PH;->b:LX/3Di;

    invoke-interface {v1, p1}, LX/3Di;->a(LX/3Dd;)Ljava/io/OutputStream;

    move-result-object v1

    iput-object v1, p0, LX/7PH;->e:Ljava/io/OutputStream;

    .line 1202748
    iget-object v1, p0, LX/7PH;->e:Ljava/io/OutputStream;

    if-eqz v1, :cond_0

    .line 1202749
    new-instance v1, LX/1iJ;

    iget-object v2, p0, LX/7PH;->e:Ljava/io/OutputStream;

    invoke-direct {v1, v2}, LX/1iJ;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, p0, LX/7PH;->f:LX/1iJ;

    .line 1202750
    iget-wide v1, p0, LX/7PH;->d:J

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    iget-wide v1, p1, LX/3Dd;->a:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 1202751
    iget-wide v1, p1, LX/3Dd;->a:J

    iput-wide v1, p0, LX/7PH;->d:J

    .line 1202752
    :cond_0
    iget-object v0, p0, LX/7PH;->f:LX/1iJ;

    return-object v0
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 1202721
    iget-object v0, p0, LX/7PH;->f:LX/1iJ;

    if-nez v0, :cond_0

    .line 1202722
    iget-object v0, p0, LX/7PH;->b:LX/3Di;

    invoke-interface {v0, p1}, LX/3Di;->a(Ljava/io/IOException;)V

    .line 1202723
    :goto_0
    return-void

    .line 1202724
    :cond_0
    iget-object v0, p0, LX/7PH;->b:LX/3Di;

    iget-object v1, p0, LX/7PH;->e:Ljava/io/OutputStream;

    invoke-interface {v0, v1, p1}, LX/3Di;->a(Ljava/io/OutputStream;Ljava/io/IOException;)V

    goto :goto_0
.end method

.method public final a(Ljava/io/OutputStream;Ljava/io/IOException;)V
    .locals 6

    .prologue
    .line 1202710
    if-eqz p2, :cond_0

    .line 1202711
    iget-object v0, p0, LX/7PH;->b:LX/3Di;

    iget-object v1, p0, LX/7PH;->e:Ljava/io/OutputStream;

    invoke-interface {v0, v1, p2}, LX/3Di;->a(Ljava/io/OutputStream;Ljava/io/IOException;)V

    .line 1202712
    :goto_0
    return-void

    .line 1202713
    :cond_0
    iget-wide v0, p0, LX/7PH;->c:J

    iget-object v2, p0, LX/7PH;->f:LX/1iJ;

    .line 1202714
    iget-wide v4, v2, LX/1iJ;->a:J

    move-wide v2, v4

    .line 1202715
    add-long/2addr v0, v2

    .line 1202716
    iget-wide v2, p0, LX/7PH;->g:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_2

    .line 1202717
    iput-wide v0, p0, LX/7PH;->g:J

    .line 1202718
    iget-wide v0, p0, LX/7PH;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-wide v0, p0, LX/7PH;->g:J

    iget-wide v2, p0, LX/7PH;->d:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 1202719
    :cond_1
    invoke-static {p0}, LX/7PH;->a$redex0(LX/7PH;)V

    goto :goto_0

    .line 1202720
    :cond_2
    iget-object v0, p0, LX/7PH;->b:LX/3Di;

    iget-object v1, p0, LX/7PH;->e:Ljava/io/OutputStream;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/3Di;->a(Ljava/io/OutputStream;Ljava/io/IOException;)V

    goto :goto_0
.end method
