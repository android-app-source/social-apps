.class public final LX/7Nt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V
    .locals 0

    .prologue
    .line 1200766
    iput-object p1, p0, LX/7Nt;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x53006cb9

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1200767
    iget-object v1, p0, LX/7Nt;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    .line 1200768
    invoke-static {v1}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->y(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1200769
    :goto_0
    const v1, 0x690e2e24

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1200770
    :cond_0
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->r:LX/7J3;

    const-string v4, "select_cast_device"

    invoke-virtual {v3, v4}, LX/7J3;->a(Ljava/lang/String;)LX/7J2;

    move-result-object v5

    .line 1200771
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v3}, LX/37Y;->b()Ljava/util/List;

    move-result-object v6

    .line 1200772
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    new-array p0, v3, [Ljava/lang/String;

    .line 1200773
    const/4 v3, 0x0

    move v4, v3

    :goto_1
    array-length v3, p0

    if-ge v4, v3, :cond_1

    .line 1200774
    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/384;

    .line 1200775
    iget-object p1, v3, LX/384;->d:Ljava/lang/String;

    move-object v3, p1

    .line 1200776
    aput-object v3, p0, v4

    .line 1200777
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 1200778
    :cond_1
    new-instance v3, LX/0ju;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1200779
    const v4, 0x7f081a69

    invoke-virtual {v3, v4}, LX/0ju;->a(I)LX/0ju;

    move-result-object v3

    new-instance v4, LX/7Nv;

    invoke-direct {v4, v1, v5, v6}, LX/7Nv;-><init>(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;LX/7J2;Ljava/util/List;)V

    invoke-virtual {v3, p0, v4}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    new-instance v4, LX/7Nu;

    invoke-direct {v4, v1, v5}, LX/7Nu;-><init>(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;LX/7J2;)V

    invoke-virtual {v3, v4}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->a()LX/2EJ;

    move-result-object v3

    invoke-virtual {v3}, LX/2EJ;->show()V

    goto :goto_0
.end method
