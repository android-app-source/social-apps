.class public LX/8I2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static volatile i:LX/8I2;


# instance fields
.field private c:Landroid/content/ContentResolver;

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/8I4;

.field private f:LX/1FJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/io/Closeable;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1322057
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "media_type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    sput-object v0, LX/8I2;->a:[Ljava/lang/String;

    .line 1322058
    const-class v0, LX/8I2;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/8I2;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/8I4;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "LX/8I4;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1322049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1322050
    const/4 v0, 0x0

    iput-object v0, p0, LX/8I2;->f:LX/1FJ;

    .line 1322051
    new-instance v0, LX/8I1;

    invoke-direct {v0, p0}, LX/8I1;-><init>(LX/8I2;)V

    iput-object v0, p0, LX/8I2;->h:Ljava/io/Closeable;

    .line 1322052
    iput-object p1, p0, LX/8I2;->c:Landroid/content/ContentResolver;

    .line 1322053
    iput-object p2, p0, LX/8I2;->e:LX/8I4;

    .line 1322054
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/8I2;->d:Ljava/util/Map;

    .line 1322055
    iput-object p3, p0, LX/8I2;->g:LX/0Ot;

    .line 1322056
    return-void
.end method

.method public static a(LX/0QB;)LX/8I2;
    .locals 6

    .prologue
    .line 1322036
    sget-object v0, LX/8I2;->i:LX/8I2;

    if-nez v0, :cond_1

    .line 1322037
    const-class v1, LX/8I2;

    monitor-enter v1

    .line 1322038
    :try_start_0
    sget-object v0, LX/8I2;->i:LX/8I2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1322039
    if-eqz v2, :cond_0

    .line 1322040
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1322041
    new-instance v5, LX/8I2;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/8I4;->b(LX/0QB;)LX/8I4;

    move-result-object v4

    check-cast v4, LX/8I4;

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/8I2;-><init>(Landroid/content/ContentResolver;LX/8I4;LX/0Ot;)V

    .line 1322042
    move-object v0, v5

    .line 1322043
    sput-object v0, LX/8I2;->i:LX/8I2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1322044
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1322045
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1322046
    :cond_1
    sget-object v0, LX/8I2;->i:LX/8I2;

    return-object v0

    .line 1322047
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1322048
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FJ",
            "<",
            "Ljava/io/Closeable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1322023
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8I2;->f:LX/1FJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8I2;->f:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1322024
    iget-object v0, p0, LX/8I2;->f:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->b()LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1322025
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1322026
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/8I2;->h:Ljava/io/Closeable;

    invoke-static {v0}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/8I2;->f:LX/1FJ;

    .line 1322027
    iget-object v0, p0, LX/8I2;->f:LX/1FJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1322028
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/4gI;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1322059
    const/4 v0, 0x0

    const-string v1, "_id"

    invoke-virtual {p0, p1, p2, v0, v1}, LX/8I2;->a(LX/4gI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4gI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1322029
    invoke-static {p1}, LX/4gB;->a(LX/4gI;)Ljava/lang/String;

    move-result-object v3

    .line 1322030
    if-eqz p2, :cond_0

    .line 1322031
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND %s = \'%s\'"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "bucket_display_name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1322032
    :cond_0
    if-eqz p3, :cond_1

    .line 1322033
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1322034
    :cond_1
    iget-object v0, p0, LX/8I2;->c:Landroid/content/ContentResolver;

    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/8I2;->a:[Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " DESC"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1322035
    return-object v0
.end method

.method public final declared-synchronized a(J)Lcom/facebook/ipc/media/MediaItem;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1322022
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8I2;->d:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(JLandroid/database/Cursor;II)Lcom/facebook/ipc/media/MediaItem;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1322001
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8I2;->d:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1322002
    if-eqz v0, :cond_0

    .line 1322003
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1322004
    :cond_0
    :try_start_1
    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1322005
    const/4 v0, 0x0

    goto :goto_0

    .line 1322006
    :cond_1
    invoke-interface {p3, p4}, Landroid/database/Cursor;->moveToPosition(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1322007
    const/4 v0, 0x1

    :try_start_2
    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_3

    move v0, v1

    .line 1322008
    :goto_1
    add-int v1, p4, p5

    :try_start_3
    invoke-interface {p3, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1322009
    invoke-interface {p3}, Landroid/database/Cursor;->moveToLast()Z

    .line 1322010
    :cond_2
    const/4 v1, 0x0

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1322011
    if-eqz v0, :cond_4

    .line 1322012
    iget-object v1, p0, LX/8I2;->e:LX/8I4;

    iget-object v6, p0, LX/8I2;->d:Ljava/util/Map;

    move-wide v2, p1

    .line 1322013
    const-string v0, "_id"

    invoke-static {v0, v2, v3, v4, v5}, LX/8I4;->a(Ljava/lang/String;JJ)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v6}, LX/8I4;->b(LX/8I4;Ljava/lang/String;Ljava/util/Map;)V

    .line 1322014
    :goto_2
    iget-object v0, p0, LX/8I2;->d:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1322015
    goto :goto_1

    .line 1322016
    :catch_0
    move-exception v1

    .line 1322017
    iget-object v0, p0, LX/8I2;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v3, LX/8I2;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_1

    .line 1322018
    :cond_4
    iget-object v1, p0, LX/8I2;->e:LX/8I4;

    iget-object v6, p0, LX/8I2;->d:Ljava/util/Map;

    move-wide v2, p1

    .line 1322019
    const-string v0, "_id"

    invoke-static {v0, v2, v3, v4, v5}, LX/8I4;->a(Ljava/lang/String;JJ)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v6}, LX/8I4;->a(LX/8I4;Ljava/lang/String;Ljava/util/Map;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1322020
    goto :goto_2

    .line 1322021
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/database/Cursor;I)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1321978
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8I2;->e:LX/8I4;

    iget-object v1, p0, LX/8I2;->d:Ljava/util/Map;

    const/4 v3, 0x0

    .line 1321979
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1321980
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 1321981
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, p2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1321982
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, p2}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v3

    .line 1321983
    :cond_0
    :goto_0
    if-ge v2, p2, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1321984
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 1321985
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1321986
    add-int/lit8 v2, v2, 0x1

    .line 1321987
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v1, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 1321988
    const/4 v10, 0x1

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/4 v11, 0x3

    if-ne v10, v11, :cond_1

    .line 1321989
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1321990
    :cond_1
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1321991
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1321992
    const-string v2, "_id"

    invoke-static {v2, v4}, LX/8I4;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, LX/8I4;->a(LX/8I4;Ljava/lang/String;Ljava/util/Map;)V

    .line 1321993
    :cond_3
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1321994
    const-string v2, "_id"

    invoke-static {v2, v5}, LX/8I4;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, LX/8I4;->b(LX/8I4;Ljava/lang/String;Ljava/util/Map;)V

    .line 1321995
    :cond_4
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1321996
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1321997
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1321998
    :cond_5
    const/4 v2, 0x0

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1321999
    :cond_6
    move-object v0, v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1322000
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
