.class public final LX/6zo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6zn;


# instance fields
.field public final synthetic a:LX/700;

.field public final synthetic b:LX/6qh;

.field public final synthetic c:LX/6zq;


# direct methods
.method public constructor <init>(LX/6zq;LX/700;LX/6qh;)V
    .locals 0

    .prologue
    .line 1161471
    iput-object p1, p0, LX/6zo;->c:LX/6zq;

    iput-object p2, p0, LX/6zo;->a:LX/700;

    iput-object p3, p0, LX/6zo;->b:LX/6qh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/locale/Country;)V
    .locals 4

    .prologue
    .line 1161472
    iget-object v0, p0, LX/6zo;->a:LX/700;

    iget-object v1, p0, LX/6zo;->b:LX/6qh;

    .line 1161473
    invoke-static {}, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;->newBuilder()LX/707;

    move-result-object v2

    iget-object v3, v0, LX/700;->c:Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;

    invoke-virtual {v2, v3}, LX/707;->a(Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;)LX/707;

    move-result-object v2

    const/4 v3, 0x1

    .line 1161474
    iput-boolean v3, v2, LX/707;->a:Z

    .line 1161475
    move-object v2, v2

    .line 1161476
    iput-object p1, v2, LX/707;->d:Lcom/facebook/common/locale/Country;

    .line 1161477
    move-object v2, v2

    .line 1161478
    invoke-virtual {v2}, LX/707;->e()Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;

    move-result-object v2

    .line 1161479
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1161480
    const-string p0, "extra_reset_data"

    invoke-virtual {v3, p0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1161481
    new-instance v2, LX/73T;

    sget-object p0, LX/73S;->RESET:LX/73S;

    invoke-direct {v2, p0, v3}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, LX/6qh;->a(LX/73T;)V

    .line 1161482
    return-void
.end method
