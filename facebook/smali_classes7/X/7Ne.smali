.class public LX/7Ne;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field public static b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7D1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1200558
    const-class v0, LX/7Ne;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Ne;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1200556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1200557
    return-void
.end method

.method public static a(LX/7D1;LX/2pa;)Landroid/content/Intent;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1200534
    iget-object v7, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1200535
    iget-object v0, v7, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/facebook/video/engine/VideoDataSource;

    .line 1200536
    iget-object v0, v3, Lcom/facebook/video/engine/VideoDataSource;->d:Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v0, v3, Lcom/facebook/video/engine/VideoDataSource;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1200537
    :goto_0
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_3

    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    iget-object v0, v0, Lcom/facebook/spherical/model/SphericalVideoParams;->a:LX/19o;

    .line 1200538
    :goto_1
    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v1}, Lcom/facebook/video/engine/VideoPlayerParams;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    move-object v5, v0

    .line 1200539
    :goto_2
    iget-object v0, v3, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    if-eqz v0, :cond_5

    iget-object v0, v3, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    :goto_3
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1200540
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1200541
    const/4 v8, 0x1

    .line 1200542
    const-string v0, "remote-uri="

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1200543
    array-length v4, v0

    if-le v4, v8, :cond_0

    .line 1200544
    aget-object v0, v0, v8

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1200545
    :cond_0
    move-object v1, v1

    .line 1200546
    :cond_1
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_6

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v4, "Video360CastTitle"

    invoke-virtual {v0, v4}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1200547
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v4, "Video360CastTitle"

    invoke-virtual {v0, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    .line 1200548
    :goto_4
    sget-object v0, LX/7Nc;->a:[I

    invoke-virtual {p0}, LX/7D1;->ordinal()I

    move-result v8

    aget v0, v0, v8

    packed-switch v0, :pswitch_data_0

    .line 1200549
    :goto_5
    return-object v6

    .line 1200550
    :cond_2
    const-string v2, ""

    goto :goto_0

    :cond_3
    move-object v0, v6

    .line 1200551
    goto :goto_1

    .line 1200552
    :cond_4
    sget-object v5, LX/19o;->CUBEMAP:LX/19o;

    goto :goto_2

    .line 1200553
    :cond_5
    iget-object v0, v3, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    goto :goto_3

    .line 1200554
    :cond_6
    const-string v4, ""

    goto :goto_4

    .line 1200555
    :pswitch_0
    iget-object v0, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoDataSource;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, LX/7Ne;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/19o;)Landroid/content/Intent;

    move-result-object v6

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/19o;)Landroid/content/Intent;
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    .line 1200513
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 1200514
    sget-object v0, LX/7Ne;->a:Ljava/lang/String;

    const-string v1, "should not run on UI thread"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1200515
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.oculus.cinema.action.CAST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "video/vr"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1200516
    new-instance v0, LX/7Nd;

    invoke-direct {v0, p1}, LX/7Nd;-><init>(Ljava/lang/String;)V

    invoke-static {v0, p5}, LX/7Nd;->a$redex0(LX/7Nd;LX/19o;)LX/7Nd;

    move-result-object v0

    const-string v2, "remote"

    invoke-static {v0, v2}, LX/7Nd;->a$redex0(LX/7Nd;Ljava/lang/String;)LX/7Nd;

    move-result-object v0

    .line 1200517
    iget-object v2, v0, LX/7Nd;->b:Ljava/util/Map;

    const-string v3, "title"

    invoke-interface {v2, v3, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1200518
    move-object v2, v0

    .line 1200519
    sget-object v0, LX/19o;->CUBEMAP:LX/19o;

    if-ne p5, v0, :cond_1

    .line 1200520
    const-string v0, "EXTRA_VR_VIDEO_ID"

    invoke-virtual {v1, v0, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1200521
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1200522
    :try_start_0
    new-instance v0, LX/7QV;

    invoke-direct {v0, p3}, LX/7QV;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/7QV;->a()Ljava/util/LinkedHashSet;

    move-result-object v0

    .line 1200523
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1200524
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    int-to-long v4, v3

    const/4 v3, 0x1

    invoke-static {v4, v5, v3}, LX/7Ne;->a(JZ)Ljava/lang/String;

    .line 1200525
    invoke-static {p3}, LX/7Ne;->b(Ljava/lang/String;)[B

    move-result-object v3

    .line 1200526
    array-length v4, v3

    int-to-long v4, v4

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, LX/7Ne;->a(JZ)Ljava/lang/String;

    .line 1200527
    const-string v4, "EXTRA_VR_DASH_MANIFEST"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1200528
    const-string v3, "dash"

    invoke-static {v2, v3}, LX/7Nd;->a$redex0(LX/7Nd;Ljava/lang/String;)LX/7Nd;

    move-result-object v3

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19o;

    invoke-static {v3, v0}, LX/7Nd;->a$redex0(LX/7Nd;LX/19o;)LX/7Nd;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1200529
    :cond_2
    :goto_0
    invoke-virtual {v2}, LX/7Nd;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1200530
    invoke-static {v0}, Landroid/webkit/URLUtil;->isNetworkUrl(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1200531
    sget-object v2, LX/7Ne;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    const-string v4, "playableUri is not a network Url"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v2, v3, v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1200532
    :cond_3
    const-string v2, "EXTRA_VR_VIDEO_URL"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1200533
    return-object v1

    :catch_0
    goto :goto_0
.end method

.method private static a(JZ)Ljava/lang/String;
    .locals 12

    .prologue
    .line 1200505
    if-eqz p2, :cond_0

    const/16 v0, 0x3e8

    move v1, v0

    .line 1200506
    :goto_0
    int-to-long v2, v1

    cmp-long v0, p0, v2

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " B"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1200507
    :goto_1
    return-object v0

    .line 1200508
    :cond_0
    const/16 v0, 0x400

    move v1, v0

    goto :goto_0

    .line 1200509
    :cond_1
    long-to-double v2, p0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    int-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-int v2, v2

    .line 1200510
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p2, :cond_2

    const-string v0, "kMGTPE"

    :goto_2
    add-int/lit8 v4, v2, -0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p2, :cond_3

    const-string v0, ""

    :goto_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1200511
    const-string v3, "%.1f %sB"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    long-to-double v6, p0

    int-to-double v8, v1

    int-to-double v10, v2

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1200512
    :cond_2
    const-string v0, "KMGTPE"

    goto :goto_2

    :cond_3
    const-string v0, "i"

    goto :goto_3
.end method

.method private static b(Ljava/lang/String;)[B
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1200497
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1200498
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1200499
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1200500
    new-instance v1, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v1, v0}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1200501
    const-string v2, "UTF-8"

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    .line 1200502
    invoke-virtual {v1}, Ljava/util/zip/GZIPOutputStream;->close()V

    .line 1200503
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    .line 1200504
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
