.class public LX/6q6;
.super LX/6q2;
.source ""


# instance fields
.field private final a:LX/18V;

.field private final b:LX/6qE;

.field private final c:LX/6qC;

.field private final d:LX/6qF;

.field private final e:LX/6q9;

.field private final f:LX/6q7;

.field private final g:LX/6qD;

.field private final h:LX/6qB;

.field private final i:LX/6q8;

.field private final j:LX/6qG;

.field private final k:LX/6qA;


# direct methods
.method public constructor <init>(LX/18V;LX/6qE;LX/6qC;LX/6qF;LX/6q9;LX/6q7;LX/6qD;LX/6qB;LX/6q8;LX/6qG;LX/6qA;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1149927
    const-string v0, "PaymentPinWebServiceHandler"

    invoke-direct {p0, v0}, LX/6q2;-><init>(Ljava/lang/String;)V

    .line 1149928
    iput-object p1, p0, LX/6q6;->a:LX/18V;

    .line 1149929
    iput-object p2, p0, LX/6q6;->b:LX/6qE;

    .line 1149930
    iput-object p3, p0, LX/6q6;->c:LX/6qC;

    .line 1149931
    iput-object p4, p0, LX/6q6;->d:LX/6qF;

    .line 1149932
    iput-object p5, p0, LX/6q6;->e:LX/6q9;

    .line 1149933
    iput-object p6, p0, LX/6q6;->f:LX/6q7;

    .line 1149934
    iput-object p7, p0, LX/6q6;->g:LX/6qD;

    .line 1149935
    iput-object p8, p0, LX/6q6;->h:LX/6qB;

    .line 1149936
    iput-object p9, p0, LX/6q6;->i:LX/6q8;

    .line 1149937
    iput-object p10, p0, LX/6q6;->j:LX/6qG;

    .line 1149938
    iput-object p11, p0, LX/6q6;->k:LX/6qA;

    .line 1149939
    return-void
.end method

.method public static b(LX/0QB;)LX/6q6;
    .locals 14

    .prologue
    .line 1149974
    new-instance v0, LX/6q6;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v1

    check-cast v1, LX/18V;

    .line 1149975
    new-instance v3, LX/6qE;

    invoke-static {p0}, LX/3Ed;->a(LX/0QB;)LX/3Ed;

    move-result-object v2

    check-cast v2, LX/3Ed;

    invoke-direct {v3, v2}, LX/6qE;-><init>(LX/3Ed;)V

    .line 1149976
    move-object v2, v3

    .line 1149977
    check-cast v2, LX/6qE;

    invoke-static {p0}, LX/6qC;->a(LX/0QB;)LX/6qC;

    move-result-object v3

    check-cast v3, LX/6qC;

    .line 1149978
    new-instance v4, LX/6qF;

    invoke-direct {v4}, LX/6qF;-><init>()V

    .line 1149979
    move-object v4, v4

    .line 1149980
    move-object v4, v4

    .line 1149981
    check-cast v4, LX/6qF;

    .line 1149982
    new-instance v5, LX/6q9;

    invoke-direct {v5}, LX/6q9;-><init>()V

    .line 1149983
    move-object v5, v5

    .line 1149984
    move-object v5, v5

    .line 1149985
    check-cast v5, LX/6q9;

    .line 1149986
    new-instance v6, LX/6q7;

    invoke-direct {v6}, LX/6q7;-><init>()V

    .line 1149987
    move-object v6, v6

    .line 1149988
    move-object v6, v6

    .line 1149989
    check-cast v6, LX/6q7;

    invoke-static {p0}, LX/6qD;->a(LX/0QB;)LX/6qD;

    move-result-object v7

    check-cast v7, LX/6qD;

    invoke-static {p0}, LX/6qB;->a(LX/0QB;)LX/6qB;

    move-result-object v8

    check-cast v8, LX/6qB;

    .line 1149990
    new-instance v10, LX/6q8;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v9

    check-cast v9, LX/0dC;

    const/16 v11, 0x15e7

    invoke-static {p0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-direct {v10, v9, v11}, LX/6q8;-><init>(LX/0dC;LX/0Or;)V

    .line 1149991
    move-object v9, v10

    .line 1149992
    check-cast v9, LX/6q8;

    .line 1149993
    new-instance v11, LX/6qG;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v10

    check-cast v10, LX/0dC;

    const/16 v12, 0x15e7

    invoke-static {p0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-direct {v11, v10, v12}, LX/6qG;-><init>(LX/0dC;LX/0Or;)V

    .line 1149994
    move-object v10, v11

    .line 1149995
    check-cast v10, LX/6qG;

    .line 1149996
    new-instance v12, LX/6qA;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v11

    check-cast v11, LX/0dC;

    const/16 v13, 0x15e7

    invoke-static {p0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-direct {v12, v11, v13}, LX/6qA;-><init>(LX/0dC;LX/0Or;)V

    .line 1149997
    move-object v11, v12

    .line 1149998
    check-cast v11, LX/6qA;

    invoke-direct/range {v0 .. v11}, LX/6q6;-><init>(LX/18V;LX/6qE;LX/6qC;LX/6qF;LX/6q9;LX/6q7;LX/6qD;LX/6qB;LX/6q8;LX/6qG;LX/6qA;)V

    .line 1149999
    return-object v0
.end method


# virtual methods
.method public final a(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1149970
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1149971
    sget-object v1, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;

    .line 1149972
    iget-object v1, p0, LX/6q6;->a:LX/18V;

    iget-object v2, p0, LX/6q6;->b:LX/6qE;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1149973
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1149968
    iget-object v0, p0, LX/6q6;->a:LX/18V;

    iget-object v1, p0, LX/6q6;->c:LX/6qC;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1149969
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1149963
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1149964
    sget-object v1, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;

    .line 1149965
    iget-object v1, p0, LX/6q6;->a:LX/18V;

    iget-object v2, p0, LX/6q6;->d:LX/6qF;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1149966
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1149967
    return-object v0
.end method

.method public final d(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1149958
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1149959
    sget-object v1, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;

    .line 1149960
    iget-object v1, p0, LX/6q6;->a:LX/18V;

    iget-object v2, p0, LX/6q6;->e:LX/6q9;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1149961
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1149962
    return-object v0
.end method

.method public final e(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1150000
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1150001
    sget-object v1, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;

    .line 1150002
    iget-object v1, p0, LX/6q6;->a:LX/18V;

    iget-object v2, p0, LX/6q6;->f:LX/6q7;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1150003
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final f(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1149956
    iget-object v0, p0, LX/6q6;->a:LX/18V;

    iget-object v1, p0, LX/6q6;->g:LX/6qD;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;

    .line 1149957
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final g(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1149952
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1149953
    sget-object v1, Lcom/facebook/payments/auth/pin/model/FetchPageInfoParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/FetchPageInfoParams;

    .line 1149954
    iget-object v1, p0, LX/6q6;->a:LX/18V;

    iget-object v2, p0, LX/6q6;->h:LX/6qB;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PageInfo;

    .line 1149955
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final h(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1149948
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1149949
    const-string v1, "createFingerprintNonceParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/params/CreateFingerprintNonceParams;

    .line 1149950
    iget-object v1, p0, LX/6q6;->a:LX/18V;

    iget-object v2, p0, LX/6q6;->i:LX/6q8;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1149951
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final i(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1149943
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1149944
    const-string v1, "verifyFingerprintNonceParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/params/VerifyFingerprintNonceParams;

    .line 1149945
    iget-object v1, p0, LX/6q6;->a:LX/18V;

    iget-object v2, p0, LX/6q6;->j:LX/6qG;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1149946
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1149947
    return-object v0
.end method

.method public final j(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1149940
    iget-object v0, p0, LX/6q6;->a:LX/18V;

    iget-object v1, p0, LX/6q6;->k:LX/6qA;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1149941
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1149942
    return-object v0
.end method
