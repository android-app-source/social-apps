.class public final LX/84F;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/84G;


# direct methods
.method public constructor <init>(LX/84G;)V
    .locals 0

    .prologue
    .line 1290868
    iput-object p1, p0, LX/84F;->a:LX/84G;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 1290871
    iget-object v0, p0, LX/84F;->a:LX/84G;

    iget-object v0, v0, LX/84G;->b:LX/3UJ;

    iget-object v0, v0, LX/3UJ;->b:LX/2do;

    new-instance v1, LX/2f2;

    iget-object v2, p0, LX/84F;->a:LX/84G;

    iget-wide v2, v2, LX/84G;->a:J

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1290872
    iget-object v0, p0, LX/84F;->a:LX/84G;

    iget-object v0, v0, LX/84G;->b:LX/3UJ;

    iget-object v0, v0, LX/3UJ;->c:LX/2hZ;

    invoke-virtual {v0, p1}, LX/2hZ;->a(Ljava/lang/Throwable;)V

    .line 1290873
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1290869
    iget-object v0, p0, LX/84F;->a:LX/84G;

    iget-object v0, v0, LX/84G;->b:LX/3UJ;

    iget-object v0, v0, LX/3UJ;->b:LX/2do;

    new-instance v1, LX/2f2;

    iget-object v2, p0, LX/84F;->a:LX/84G;

    iget-wide v2, v2, LX/84G;->a:J

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1290870
    return-void
.end method
