.class public final enum LX/86t;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/86t;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/86t;

.field public static final enum DOODLE:LX/86t;

.field public static final enum LOCATION_NUX:LX/86t;

.field public static final enum NUX:LX/86t;

.field public static final enum PREVIEW:LX/86t;

.field public static final enum TRAY:LX/86t;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1297992
    new-instance v0, LX/86t;

    const-string v1, "TRAY"

    invoke-direct {v0, v1, v2}, LX/86t;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86t;->TRAY:LX/86t;

    .line 1297993
    new-instance v0, LX/86t;

    const-string v1, "PREVIEW"

    invoke-direct {v0, v1, v3}, LX/86t;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86t;->PREVIEW:LX/86t;

    .line 1297994
    new-instance v0, LX/86t;

    const-string v1, "DOODLE"

    invoke-direct {v0, v1, v4}, LX/86t;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86t;->DOODLE:LX/86t;

    .line 1297995
    new-instance v0, LX/86t;

    const-string v1, "NUX"

    invoke-direct {v0, v1, v5}, LX/86t;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86t;->NUX:LX/86t;

    .line 1297996
    new-instance v0, LX/86t;

    const-string v1, "LOCATION_NUX"

    invoke-direct {v0, v1, v6}, LX/86t;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86t;->LOCATION_NUX:LX/86t;

    .line 1297997
    const/4 v0, 0x5

    new-array v0, v0, [LX/86t;

    sget-object v1, LX/86t;->TRAY:LX/86t;

    aput-object v1, v0, v2

    sget-object v1, LX/86t;->PREVIEW:LX/86t;

    aput-object v1, v0, v3

    sget-object v1, LX/86t;->DOODLE:LX/86t;

    aput-object v1, v0, v4

    sget-object v1, LX/86t;->NUX:LX/86t;

    aput-object v1, v0, v5

    sget-object v1, LX/86t;->LOCATION_NUX:LX/86t;

    aput-object v1, v0, v6

    sput-object v0, LX/86t;->$VALUES:[LX/86t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1297991
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/86t;
    .locals 1

    .prologue
    .line 1297989
    const-class v0, LX/86t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/86t;

    return-object v0
.end method

.method public static values()[LX/86t;
    .locals 1

    .prologue
    .line 1297990
    sget-object v0, LX/86t;->$VALUES:[LX/86t;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/86t;

    return-object v0
.end method
