.class public LX/7K4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/7K4;

.field public static final b:LX/7K4;


# instance fields
.field public final c:I

.field public final d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1194454
    new-instance v0, LX/7K4;

    invoke-direct {v0}, LX/7K4;-><init>()V

    sput-object v0, LX/7K4;->a:LX/7K4;

    .line 1194455
    new-instance v0, LX/7K4;

    invoke-direct {v0, v1, v1}, LX/7K4;-><init>(II)V

    sput-object v0, LX/7K4;->b:LX/7K4;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1194450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194451
    iput v0, p0, LX/7K4;->c:I

    .line 1194452
    iput v0, p0, LX/7K4;->d:I

    .line 1194453
    return-void
.end method

.method public constructor <init>(II)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 1194443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194444
    if-gez p1, :cond_0

    if-ne p1, v3, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1194445
    if-gez p2, :cond_1

    if-ne p2, v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1194446
    iput p1, p0, LX/7K4;->c:I

    .line 1194447
    iput p2, p0, LX/7K4;->d:I

    .line 1194448
    return-void

    :cond_3
    move v0, v1

    .line 1194449
    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 1194442
    iget v0, p0, LX/7K4;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1194441
    iget v0, p0, LX/7K4;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1194437
    instance-of v1, p1, LX/7K4;

    if-eqz v1, :cond_0

    .line 1194438
    check-cast p1, LX/7K4;

    .line 1194439
    iget v1, p1, LX/7K4;->c:I

    iget v2, p0, LX/7K4;->c:I

    if-ne v1, v2, :cond_0

    iget v1, p1, LX/7K4;->d:I

    iget v2, p0, LX/7K4;->d:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1194440
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1194436
    iget v0, p0, LX/7K4;->c:I

    iget v1, p0, LX/7K4;->d:I

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1194435
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PlayPosition{startFromPosition="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/7K4;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastStartPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/7K4;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
