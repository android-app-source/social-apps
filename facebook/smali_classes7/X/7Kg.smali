.class public final LX/7Kg;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:LX/7Ki;

.field public final synthetic b:LX/1ca;

.field public final synthetic c:LX/7Kh;


# direct methods
.method public constructor <init>(LX/7Kh;LX/7Ki;LX/1ca;)V
    .locals 0

    .prologue
    .line 1196169
    iput-object p1, p0, LX/7Kg;->c:LX/7Kh;

    iput-object p2, p0, LX/7Kg;->a:LX/7Ki;

    iput-object p3, p0, LX/7Kg;->b:LX/1ca;

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1196170
    iget-object v0, p0, LX/7Kg;->b:LX/1ca;

    invoke-interface {v0}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object v0

    .line 1196171
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v0, :cond_0

    const-string v0, "Null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1196172
    sget-object v1, LX/7Kh;->a:Ljava/lang/Class;

    const-string v2, "Failed to load image for casting notification: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1196173
    return-void

    .line 1196174
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1196175
    if-nez p1, :cond_1

    .line 1196176
    const-string v0, "null"

    invoke-direct {p0, v0}, LX/7Kg;->a(Ljava/lang/String;)V

    .line 1196177
    :cond_0
    :goto_0
    iget-object v0, p0, LX/7Kg;->a:LX/7Ki;

    invoke-virtual {v0, p1}, LX/7Ki;->a(Landroid/graphics/Bitmap;)V

    .line 1196178
    return-void

    .line 1196179
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1196180
    const-string v0, "Recycled"

    invoke-direct {p0, v0}, LX/7Kg;->a(Ljava/lang/String;)V

    .line 1196181
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 2

    .prologue
    .line 1196182
    const-string v0, "onFailureImpl"

    invoke-direct {p0, v0}, LX/7Kg;->a(Ljava/lang/String;)V

    .line 1196183
    iget-object v0, p0, LX/7Kg;->a:LX/7Ki;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/7Ki;->a(Landroid/graphics/Bitmap;)V

    .line 1196184
    return-void
.end method
