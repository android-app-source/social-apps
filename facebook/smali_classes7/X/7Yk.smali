.class public final LX/7Yk;
.super LX/7Yf;
.source ""


# instance fields
.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/7Ym;


# direct methods
.method public constructor <init>(LX/7Ym;LX/2wX;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, LX/7Yk;->e:LX/7Ym;

    iput-object p3, p0, LX/7Yk;->d:Ljava/lang/String;

    invoke-direct {p0, p2}, LX/7Yf;-><init>(LX/2wX;)V

    return-void
.end method


# virtual methods
.method public final a(LX/7Z3;)V
    .locals 3

    const/16 v1, 0x7d1

    iget-object v0, p0, LX/7Yk;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "IllegalArgument: sessionId cannot be null or empty"

    new-instance v2, Lcom/google/android/gms/common/api/Status;

    const/4 p1, 0x0

    invoke-direct {v2, v1, v0, p1}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v2}, LX/2wf;->a(Lcom/google/android/gms/common/api/Status;)LX/2NW;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/2wf;->a(LX/2NW;)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, LX/7Yk;->d:Ljava/lang/String;

    invoke-static {p1, p0}, LX/7Z3;->b(LX/7Z3;LX/2wh;)V

    invoke-static {p1}, LX/7Z3;->A(LX/7Z3;)LX/7Z6;

    move-result-object v2

    invoke-interface {v2, v0}, LX/7Z6;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-virtual {p0, v1}, LX/7Ye;->a(I)V

    goto :goto_0
.end method

.method public final synthetic b(LX/2wK;)V
    .locals 0

    check-cast p1, LX/7Z3;

    invoke-virtual {p0, p1}, LX/7Yk;->a(LX/7Z3;)V

    return-void
.end method
