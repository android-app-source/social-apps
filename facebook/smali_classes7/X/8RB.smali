.class public final LX/8RB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V
    .locals 0

    .prologue
    .line 1344306
    iput-object p1, p0, LX/8RB;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1344307
    iget-object v0, p0, LX/8RB;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->f:LX/8tB;

    invoke-virtual {v0}, LX/3Tf;->a()LX/333;

    move-result-object v0

    iget-object v1, p0, LX/8RB;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v0, v1}, LX/333;->a(Ljava/lang/CharSequence;)V

    .line 1344308
    iget-object v0, p0, LX/8RB;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8RB;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, LX/8RB;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 1344309
    :goto_0
    return-void

    .line 1344310
    :cond_0
    iget-object v0, p0, LX/8RB;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    iput-object v0, p0, LX/8RB;->b:Ljava/util/List;

    .line 1344311
    iget-object v0, p0, LX/8RB;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->f:LX/8tB;

    iget-object v1, p0, LX/8RB;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    .line 1344312
    iput-object v1, v0, LX/8tB;->i:Ljava/util/List;

    .line 1344313
    iget-object v0, p0, LX/8RB;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->l(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1344314
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1344315
    return-void
.end method
