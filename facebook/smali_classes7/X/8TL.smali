.class public final LX/8TL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/8TQ;

.field public final synthetic c:LX/8TM;


# direct methods
.method public constructor <init>(LX/8TM;Ljava/lang/String;LX/8TQ;)V
    .locals 0

    .prologue
    .line 1348478
    iput-object p1, p0, LX/8TL;->c:LX/8TM;

    iput-object p2, p0, LX/8TL;->a:Ljava/lang/String;

    iput-object p3, p0, LX/8TL;->b:LX/8TQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;)V
    .locals 17

    .prologue
    .line 1348479
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_1

    .line 1348480
    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Null Instant game info received"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/8TL;->a(Ljava/lang/Throwable;)V

    .line 1348481
    :cond_0
    :goto_0
    return-void

    .line 1348482
    :cond_1
    new-instance v1, LX/8TN;

    invoke-direct {v1}, LX/8TN;-><init>()V

    .line 1348483
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;->a()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 1348484
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;->a()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 1348485
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;->a()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 1348486
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;->a()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 1348487
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;->a()LX/1vs;

    move-result-object v10

    iget-object v11, v10, LX/1vs;->a:LX/15i;

    iget v10, v10, LX/1vs;->b:I

    .line 1348488
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;->a()LX/1vs;

    move-result-object v12

    iget-object v13, v12, LX/1vs;->a:LX/15i;

    iget v12, v12, LX/1vs;->b:I

    .line 1348489
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;->a()LX/1vs;

    move-result-object v14

    iget-object v15, v14, LX/1vs;->a:LX/15i;

    iget v14, v14, LX/1vs;->b:I

    .line 1348490
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8TL;->a:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, LX/8TN;->a(Ljava/lang/String;)LX/8TN;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;->j()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, LX/8TN;->c(Ljava/lang/String;)LX/8TN;

    move-result-object v1

    const/16 v16, 0x6

    move/from16 v0, v16

    invoke-virtual {v3, v2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8TN;->b(Ljava/lang/String;)LX/8TN;

    move-result-object v2

    const/4 v1, 0x2

    const-class v3, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    sget-object v16, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    move-object/from16 v0, v16

    invoke-virtual {v5, v4, v1, v3, v0}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/8TN;->d(Ljava/lang/String;)LX/8TN;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v7, v6, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8TN;->e(Ljava/lang/String;)LX/8TN;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v9, v8, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8TN;->f(Ljava/lang/String;)LX/8TN;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v11, v10, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8TN;->g(Ljava/lang/String;)LX/8TN;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v13, v12, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8TN;->h(Ljava/lang/String;)LX/8TN;

    move-result-object v2

    const/4 v1, 0x3

    const-class v3, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    invoke-virtual {v15, v14, v1, v3, v4}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    invoke-virtual {v2, v1}, LX/8TN;->a(Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;)LX/8TN;

    move-result-object v1

    invoke-virtual {v1}, LX/8TN;->a()LX/8TO;

    move-result-object v1

    .line 1348491
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8TL;->b:LX/8TQ;

    if-eqz v2, :cond_0

    .line 1348492
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8TL;->b:LX/8TQ;

    invoke-virtual {v2, v1}, LX/8TQ;->a(LX/8TO;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1348493
    iget-object v0, p0, LX/8TL;->b:LX/8TQ;

    if-eqz v0, :cond_0

    .line 1348494
    iget-object v0, p0, LX/8TL;->b:LX/8TQ;

    .line 1348495
    iget-object p0, v0, LX/8TQ;->a:LX/8TS;

    iget-object p0, p0, LX/8TS;->p:LX/2th;

    if-eqz p0, :cond_0

    .line 1348496
    iget-object p0, v0, LX/8TQ;->a:LX/8TS;

    iget-object p0, p0, LX/8TS;->p:LX/2th;

    invoke-virtual {p0, p1}, LX/2th;->a(Ljava/lang/Throwable;)V

    .line 1348497
    :cond_0
    return-void
.end method
