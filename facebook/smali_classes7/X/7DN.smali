.class public LX/7DN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7D0;


# static fields
.field private static a:LX/0SG;


# instance fields
.field private final b:LX/5Pc;

.field private c:LX/5Pb;

.field private d:LX/5PR;

.field private e:LX/7DD;

.field private f:I

.field private g:I

.field private h:I

.field private i:J

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0SG;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1182842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182843
    new-instance v0, LX/7DD;

    invoke-direct {v0}, LX/7DD;-><init>()V

    iput-object v0, p0, LX/7DN;->e:LX/7DD;

    .line 1182844
    iput v1, p0, LX/7DN;->f:I

    .line 1182845
    iput v1, p0, LX/7DN;->g:I

    .line 1182846
    iput v1, p0, LX/7DN;->h:I

    .line 1182847
    new-instance v0, LX/5Pd;

    invoke-direct {v0, p1}, LX/5Pd;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, LX/7DN;->b:LX/5Pc;

    .line 1182848
    invoke-static {}, LX/7D3;->newBuilder()LX/7D2;

    move-result-object v0

    invoke-virtual {v0}, LX/7D2;->a()LX/5PR;

    move-result-object v0

    iput-object v0, p0, LX/7DN;->d:LX/5PR;

    .line 1182849
    sput-object p2, LX/7DN;->a:LX/0SG;

    .line 1182850
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1182841
    iget v0, p0, LX/7DN;->f:I

    return v0
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 1182840
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 1182791
    return-void
.end method

.method public final a(LX/19o;)V
    .locals 0

    .prologue
    .line 1182839
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 14

    .prologue
    const/4 v5, 0x1

    const v4, 0x46180400    # 9729.0f

    const/4 v2, 0x0

    const v3, 0x8513

    const/4 v1, 0x0

    .line 1182810
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1182811
    :cond_0
    :goto_0
    return-void

    .line 1182812
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v2, v2, v2, v0}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 1182813
    iget v0, p0, LX/7DN;->f:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 1182814
    new-array v0, v5, [I

    .line 1182815
    iget v2, p0, LX/7DN;->f:I

    aput v2, v0, v1

    .line 1182816
    invoke-static {v5, v0, v1}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 1182817
    aget v0, v0, v1

    iput v0, p0, LX/7DN;->f:I

    .line 1182818
    :cond_2
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 1182819
    iget v0, p0, LX/7DN;->f:I

    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1182820
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "glBindTexture GL_TEXTURE_CUBE_MAP textureId: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/7DN;->f:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1182821
    const/16 v0, 0x2801

    invoke-static {v3, v0, v4}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1182822
    const/16 v0, 0x2800

    invoke-static {v3, v0, v4}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1182823
    const/16 v0, 0x2802

    const v2, 0x812f

    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 1182824
    const/16 v0, 0x2803

    const v2, 0x812f

    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 1182825
    const-string v0, "glTexParameter"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1182826
    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 1182827
    const/16 v0, 0x404

    invoke-static {v0}, Landroid/opengl/GLES20;->glCullFace(I)V

    .line 1182828
    const-string v0, "glCullFace"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1182829
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v10

    .line 1182830
    iget-object v0, p0, LX/7DN;->e:LX/7DD;

    iput v10, v0, LX/7DD;->d:I

    .line 1182831
    iget-object v0, p0, LX/7DN;->e:LX/7DD;

    div-int/lit8 v2, v10, 0x4

    iput v2, v0, LX/7DD;->f:I

    .line 1182832
    div-int/lit8 v0, v10, 0x18

    int-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-int v3, v2

    move v9, v1

    .line 1182833
    :goto_1
    const/4 v0, 0x6

    if-ge v9, v0, :cond_3

    .line 1182834
    const v0, 0x8515

    add-int/2addr v0, v9

    const/16 v2, 0x1908

    const/16 v6, 0x1908

    const/16 v7, 0x1401

    div-int/lit8 v4, v10, 0x6

    mul-int/2addr v4, v9

    int-to-long v4, v4

    div-int/lit8 v8, v10, 0x6

    int-to-long v12, v8

    invoke-static {p1, v4, v5, v12, v13}, Lcom/facebook/imagepipeline/nativecode/Bitmaps;->a(Landroid/graphics/Bitmap;JJ)Ljava/nio/ByteBuffer;

    move-result-object v8

    move v4, v3

    move v5, v1

    invoke-static/range {v0 .. v8}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 1182835
    invoke-static {}, Landroid/opengl/GLES20;->glFinish()V

    .line 1182836
    invoke-static {p1}, Lcom/facebook/imagepipeline/nativecode/Bitmaps;->b(Landroid/graphics/Bitmap;)V

    .line 1182837
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    .line 1182838
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "glTexImage2D textureId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/7DN;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)V
    .locals 0

    .prologue
    .line 1182851
    return-void
.end method

.method public final a(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;)V
    .locals 0

    .prologue
    .line 1182809
    return-void
.end method

.method public final a([F[F[FII)V
    .locals 9

    .prologue
    const v8, 0x8513

    const/4 v1, 0x0

    .line 1182794
    iget v0, p0, LX/7DN;->f:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 1182795
    :goto_0
    return-void

    .line 1182796
    :cond_0
    iget v0, p0, LX/7DN;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7DN;->j:I

    .line 1182797
    sget-object v0, LX/7DN;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 1182798
    iget v0, p0, LX/7DN;->g:I

    if-ne p4, v0, :cond_1

    iget v0, p0, LX/7DN;->h:I

    if-eq p5, v0, :cond_2

    .line 1182799
    :cond_1
    invoke-static {v1, v1, p4, p5}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1182800
    iput p4, p0, LX/7DN;->g:I

    .line 1182801
    iput p5, p0, LX/7DN;->h:I

    .line 1182802
    :cond_2
    const/16 v0, 0x10

    new-array v0, v0, [F

    move-object v2, p2

    move v3, v1

    move-object v4, p1

    move v5, v1

    .line 1182803
    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1182804
    const/16 v2, 0x4000

    invoke-static {v2}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1182805
    iget v2, p0, LX/7DN;->f:I

    invoke-static {v8, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1182806
    iget-object v2, p0, LX/7DN;->c:LX/5Pb;

    invoke-virtual {v2}, LX/5Pb;->a()LX/5Pa;

    move-result-object v2

    const-string v3, "sTexture"

    iget v4, p0, LX/7DN;->f:I

    invoke-virtual {v2, v3, v1, v8, v4}, LX/5Pa;->a(Ljava/lang/String;III)LX/5Pa;

    move-result-object v1

    const-string v2, "uMVPMatrix"

    invoke-virtual {v1, v2, v0}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    iget-object v1, p0, LX/7DN;->d:LX/5PR;

    invoke-virtual {v0, v1}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    .line 1182807
    iget-wide v0, p0, LX/7DN;->i:J

    sget-object v2, LX/7DN;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v2, v6

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/7DN;->i:J

    .line 1182808
    iget-object v0, p0, LX/7DN;->e:LX/7DD;

    iget-wide v2, p0, LX/7DN;->i:J

    long-to-float v1, v2

    iget v2, p0, LX/7DN;->j:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, v0, LX/7DD;->a:F

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1182792
    iget-object v0, p0, LX/7DN;->b:LX/5Pc;

    const v1, 0x7f07004d

    const v2, 0x7f07004c

    invoke-interface {v0, v1, v2}, LX/5Pc;->a(II)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, LX/7DN;->c:LX/5Pb;

    .line 1182793
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1182785
    iget v0, p0, LX/7DN;->f:I

    if-eq v0, v2, :cond_0

    .line 1182786
    new-array v0, v4, [I

    .line 1182787
    iget v1, p0, LX/7DN;->f:I

    aput v1, v0, v3

    .line 1182788
    invoke-static {v4, v0, v3}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 1182789
    iput v2, p0, LX/7DN;->f:I

    .line 1182790
    :cond_0
    return-void
.end method

.method public final d()LX/7DC;
    .locals 1

    .prologue
    .line 1182784
    sget-object v0, LX/7DC;->CUBESTRIP:LX/7DC;

    return-object v0
.end method

.method public final e()LX/7DD;
    .locals 2

    .prologue
    .line 1182781
    iget-object v0, p0, LX/7DN;->e:LX/7DD;

    iget v1, p0, LX/7DN;->h:I

    iput v1, v0, LX/7DD;->g:I

    .line 1182782
    iget-object v0, p0, LX/7DN;->e:LX/7DD;

    iget v1, p0, LX/7DN;->g:I

    iput v1, v0, LX/7DD;->h:I

    .line 1182783
    iget-object v0, p0, LX/7DN;->e:LX/7DD;

    return-object v0
.end method
