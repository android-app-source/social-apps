.class public final enum LX/6xN;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6LU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6xN;",
        ">;",
        "LX/6LU",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6xN;

.field public static final enum NOTE:LX/6xN;

.field public static final enum PRICE:LX/6xN;

.field public static final enum SUBTITLE:LX/6xN;

.field public static final enum TITLE:LX/6xN;

.field public static final enum UNKNOWN:LX/6xN;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1158759
    new-instance v0, LX/6xN;

    const-string v1, "TITLE"

    invoke-direct {v0, v1, v2}, LX/6xN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xN;->TITLE:LX/6xN;

    .line 1158760
    new-instance v0, LX/6xN;

    const-string v1, "SUBTITLE"

    invoke-direct {v0, v1, v3}, LX/6xN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xN;->SUBTITLE:LX/6xN;

    .line 1158761
    new-instance v0, LX/6xN;

    const-string v1, "PRICE"

    invoke-direct {v0, v1, v4}, LX/6xN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xN;->PRICE:LX/6xN;

    .line 1158762
    new-instance v0, LX/6xN;

    const-string v1, "NOTE"

    invoke-direct {v0, v1, v5}, LX/6xN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xN;->NOTE:LX/6xN;

    .line 1158763
    new-instance v0, LX/6xN;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, LX/6xN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xN;->UNKNOWN:LX/6xN;

    .line 1158764
    const/4 v0, 0x5

    new-array v0, v0, [LX/6xN;

    sget-object v1, LX/6xN;->TITLE:LX/6xN;

    aput-object v1, v0, v2

    sget-object v1, LX/6xN;->SUBTITLE:LX/6xN;

    aput-object v1, v0, v3

    sget-object v1, LX/6xN;->PRICE:LX/6xN;

    aput-object v1, v0, v4

    sget-object v1, LX/6xN;->NOTE:LX/6xN;

    aput-object v1, v0, v5

    sget-object v1, LX/6xN;->UNKNOWN:LX/6xN;

    aput-object v1, v0, v6

    sput-object v0, LX/6xN;->$VALUES:[LX/6xN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1158758
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forValue(Ljava/lang/String;)LX/6xN;
    .locals 2

    .prologue
    .line 1158757
    invoke-static {}, LX/6xN;->values()[LX/6xN;

    move-result-object v0

    invoke-static {v0, p0}, LX/6LV;->a([LX/6LU;Ljava/lang/Object;)LX/6LU;

    move-result-object v0

    sget-object v1, LX/6xN;->UNKNOWN:LX/6xN;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6xN;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6xN;
    .locals 1

    .prologue
    .line 1158756
    const-class v0, LX/6xN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xN;

    return-object v0
.end method

.method public static values()[LX/6xN;
    .locals 1

    .prologue
    .line 1158755
    sget-object v0, LX/6xN;->$VALUES:[LX/6xN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6xN;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1158754
    invoke-virtual {p0}, LX/6xN;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1158753
    invoke-virtual {p0}, LX/6xN;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1158752
    invoke-virtual {p0}, LX/6xN;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
