.class public LX/6si;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1153669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethod;",
            ")",
            "LX/0Px",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1153670
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 1153671
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->CREDENTIAL_ID:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1153672
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    const-string v2, "type"

    invoke-interface {p0}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->b()LX/6zU;

    move-result-object v3

    invoke-virtual {v3}, LX/6zU;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v1

    .line 1153673
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v3, LX/6xk;->NMOR_PAYMENT_METHOD:LX/6xk;

    invoke-virtual {v3}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1153674
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
