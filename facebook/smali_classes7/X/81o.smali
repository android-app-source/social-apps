.class public final LX/81o;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/feed/protocol/QuestionAddPollOptionModels$QuestionAddResponseMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1286613
    const-class v1, Lcom/facebook/feed/protocol/QuestionAddPollOptionModels$QuestionAddResponseMutationModel;

    const v0, -0x331f50a9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "QuestionAddResponseMutation"

    const-string v6, "97f340797bcde5a8a25399433cfa9ead"

    const-string v7, "question_add_option"

    const-string v8, "0"

    const-string v9, "10155069968771729"

    const/4 v10, 0x0

    .line 1286614
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1286615
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1286616
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1286617
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1286618
    sparse-switch v0, :sswitch_data_0

    .line 1286619
    :goto_0
    return-object p1

    .line 1286620
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1286621
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1286622
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1286623
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3619b3d2 -> :sswitch_1
        -0x736e985 -> :sswitch_3
        0x5fb57ca -> :sswitch_0
        0x410878b1 -> :sswitch_2
    .end sparse-switch
.end method
