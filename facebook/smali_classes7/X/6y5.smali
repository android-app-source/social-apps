.class public final LX/6y5;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;)V
    .locals 0

    .prologue
    .line 1159355
    iput-object p1, p0, LX/6y5;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 4

    .prologue
    .line 1159356
    iget-object v0, p0, LX/6y5;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->r:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    .line 1159357
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v1}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v1

    move v0, v1

    .line 1159358
    if-eqz v0, :cond_0

    .line 1159359
    :goto_0
    return-void

    .line 1159360
    :cond_0
    iget-object v0, p0, LX/6y5;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->c:LX/6yZ;

    iget-object v1, p0, LX/6y5;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v0, v1}, LX/6yZ;->b(LX/6yO;)LX/6xu;

    move-result-object v0

    .line 1159361
    iget-object v1, p0, LX/6y5;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->a:LX/0Zb;

    iget-object v2, p0, LX/6y5;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    iget-object v3, p0, LX/6y5;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0, v3}, LX/6xu;->e(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1159362
    iget-object v0, p0, LX/6y5;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->b()Z

    goto :goto_0
.end method
