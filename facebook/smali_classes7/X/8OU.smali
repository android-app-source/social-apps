.class public LX/8OU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/8Ni;

.field private final c:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0ad;LX/8Ni;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1339483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1339484
    iput-object p1, p0, LX/8OU;->a:LX/0ad;

    .line 1339485
    iput-object p2, p0, LX/8OU;->b:LX/8Ni;

    .line 1339486
    iput-object p3, p0, LX/8OU;->c:LX/0Zb;

    .line 1339487
    return-void
.end method

.method private static a(LX/8OU;LX/434;Z)I
    .locals 4

    .prologue
    .line 1339488
    iget-object v0, p0, LX/8OU;->b:LX/8Ni;

    iget v1, p1, LX/434;->b:I

    iget v2, p1, LX/434;->a:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, p2}, LX/8Ni;->a(IIZZ)LX/43G;

    move-result-object v0

    .line 1339489
    iget v1, v0, LX/43G;->c:I

    move v0, v1

    .line 1339490
    return v0
.end method

.method private static a(LX/8OU;II)LX/434;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 1339491
    iget-object v0, p0, LX/8OU;->b:LX/8Ni;

    invoke-virtual {v0, p1, p2, v1, v1}, LX/8Ni;->a(IIZZ)LX/43G;

    move-result-object v0

    .line 1339492
    iget v1, v0, LX/43G;->a:I

    move v1, v1

    .line 1339493
    int-to-float v1, v1

    int-to-float v2, p1

    div-float/2addr v1, v2

    invoke-static {v3, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 1339494
    iget v2, v0, LX/43G;->b:I

    move v0, v2

    .line 1339495
    int-to-float v0, v0

    int-to-float v2, p2

    div-float/2addr v0, v2

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1339496
    int-to-float v1, p1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    .line 1339497
    int-to-float v2, p2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 1339498
    new-instance v2, LX/434;

    invoke-direct {v2, v1, v0}, LX/434;-><init>(II)V

    return-object v2
.end method

.method public static a(LX/0QB;)LX/8OU;
    .locals 1

    .prologue
    .line 1339499
    invoke-static {p0}, LX/8OU;->b(LX/0QB;)LX/8OU;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/8OU;LX/434;Z)J
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1339500
    iget-object v0, p0, LX/8OU;->a:LX/0ad;

    sget v1, LX/8Jz;->E:F

    invoke-interface {v0, v1, v7}, LX/0ad;->a(FF)F

    move-result v0

    .line 1339501
    iget-object v1, p0, LX/8OU;->a:LX/0ad;

    sget v2, LX/8Jz;->G:F

    invoke-interface {v1, v2, v7}, LX/0ad;->a(FF)F

    move-result v1

    .line 1339502
    iget-object v2, p0, LX/8OU;->a:LX/0ad;

    sget v3, LX/8Jz;->A:F

    invoke-interface {v2, v3, v7}, LX/0ad;->a(FF)F

    move-result v2

    .line 1339503
    iget-object v3, p0, LX/8OU;->a:LX/0ad;

    sget v4, LX/8Jz;->B:F

    invoke-interface {v3, v4, v7}, LX/0ad;->a(FF)F

    move-result v3

    .line 1339504
    iget-object v4, p0, LX/8OU;->a:LX/0ad;

    sget v5, LX/8Jz;->F:F

    invoke-interface {v4, v5, v7}, LX/0ad;->a(FF)F

    move-result v4

    .line 1339505
    iget-object v5, p0, LX/8OU;->a:LX/0ad;

    sget v6, LX/8Jz;->z:F

    invoke-interface {v5, v6, v7}, LX/0ad;->a(FF)F

    move-result v5

    .line 1339506
    iget v6, p1, LX/434;->b:I

    iget v7, p1, LX/434;->a:I

    mul-int/2addr v6, v7

    int-to-float v6, v6

    .line 1339507
    invoke-static {p0, p1, p2}, LX/8OU;->a(LX/8OU;LX/434;Z)I

    move-result v7

    int-to-float v7, v7

    .line 1339508
    mul-float/2addr v1, v6

    mul-float/2addr v1, v6

    mul-float/2addr v0, v6

    add-float/2addr v0, v1

    mul-float v1, v3, v7

    mul-float/2addr v1, v7

    add-float/2addr v0, v1

    mul-float v1, v2, v7

    add-float/2addr v0, v1

    mul-float v1, v4, v6

    mul-float/2addr v1, v7

    add-float/2addr v0, v1

    add-float/2addr v0, v5

    float-to-long v0, v0

    return-wide v0
.end method

.method public static b(LX/0QB;)LX/8OU;
    .locals 4

    .prologue
    .line 1339509
    new-instance v3, LX/8OU;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/8Ni;->b(LX/0QB;)LX/8Ni;

    move-result-object v1

    check-cast v1, LX/8Ni;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-direct {v3, v0, v1, v2}, LX/8OU;-><init>(LX/0ad;LX/8Ni;LX/0Zb;)V

    .line 1339510
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/util/List;)LX/0am;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1339511
    const-wide/16 v4, 0x0

    .line 1339512
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    move v2, v0

    .line 1339513
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1339514
    instance-of v1, v0, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1339515
    iget-boolean v6, v1, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    move v1, v6

    .line 1339516
    if-eqz v1, :cond_2

    .line 1339517
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 1339518
    :goto_2
    return-object v0

    .line 1339519
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 1339520
    check-cast v1, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1339521
    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v6

    .line 1339522
    iget v7, v6, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v6, v7

    .line 1339523
    if-lez v6, :cond_3

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v6

    .line 1339524
    iget v7, v6, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v6, v7

    .line 1339525
    if-gtz v6, :cond_4

    .line 1339526
    :cond_3
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_2

    .line 1339527
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v6

    .line 1339528
    iget v7, v6, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v6, v7

    .line 1339529
    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1339530
    iget v7, v1, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v1, v7

    .line 1339531
    invoke-static {p0, v6, v1}, LX/8OU;->a(LX/8OU;II)LX/434;

    move-result-object v1

    .line 1339532
    invoke-static {p0, v1, v2}, LX/8OU;->b(LX/8OU;LX/434;Z)J

    move-result-wide v6

    .line 1339533
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-gtz v8, :cond_5

    .line 1339534
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_2

    .line 1339535
    :cond_5
    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v10

    .line 1339536
    iget v11, v10, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v10, v11

    .line 1339537
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1339538
    iget v10, v0, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v0, v10

    .line 1339539
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v8, v9

    const/4 v0, 0x2

    iget v9, v1, LX/434;->b:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v0

    const/4 v0, 0x3

    iget v1, v1, LX/434;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v8, v0

    const/4 v0, 0x4

    const-wide/16 v10, 0x400

    div-long v10, v6, v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v8, v0

    .line 1339540
    add-long v0, v4, v6

    move-wide v4, v0

    .line 1339541
    goto/16 :goto_1

    .line 1339542
    :cond_6
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-eqz v0, :cond_7

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto/16 :goto_2

    :cond_7
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto/16 :goto_2
.end method

.method public final a(LX/434;LX/434;JLX/74b;Z)V
    .locals 9

    .prologue
    .line 1339543
    iget-object v0, p0, LX/8OU;->a:LX/0ad;

    sget-short v1, LX/8Jz;->y:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1339544
    :cond_0
    :goto_0
    return-void

    .line 1339545
    :cond_1
    iget v0, p1, LX/434;->b:I

    iget v1, p1, LX/434;->a:I

    invoke-static {p0, v0, v1}, LX/8OU;->a(LX/8OU;II)LX/434;

    move-result-object v0

    .line 1339546
    invoke-static {p0, v0, p6}, LX/8OU;->b(LX/8OU;LX/434;Z)J

    move-result-wide v2

    .line 1339547
    invoke-static {p0, p1, p6}, LX/8OU;->a(LX/8OU;LX/434;Z)I

    move-result v1

    .line 1339548
    iget-object v4, p0, LX/8OU;->c:LX/0Zb;

    const-string v5, "fb4a_upload_size_estimation"

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 1339549
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1339550
    invoke-virtual {p5}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/util/Map;)LX/0oG;

    .line 1339551
    const-string v5, "source_width"

    iget v6, p1, LX/434;->b:I

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1339552
    const-string v5, "source_height"

    iget v6, p1, LX/434;->a:I

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1339553
    const-string v5, "estimated_target_width"

    iget v6, v0, LX/434;->b:I

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1339554
    const-string v5, "estimated_target_height"

    iget v6, v0, LX/434;->a:I

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1339555
    const-string v5, "real_target_width"

    iget v6, p2, LX/434;->b:I

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1339556
    const-string v5, "real_target_height"

    iget v6, p2, LX/434;->a:I

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1339557
    const-string v5, "estimated_size"

    invoke-virtual {v4, v5, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 1339558
    const-string v5, "real_size"

    invoke-virtual {v4, v5, p3, p4}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 1339559
    const-string v5, "quality"

    invoke-virtual {v4, v5, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1339560
    const/16 v5, 0x9

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p1, LX/434;->b:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget v7, p1, LX/434;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget v7, v0, LX/434;->b:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget v0, v0, LX/434;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x4

    iget v6, p2, LX/434;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x5

    iget v6, p2, LX/434;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x6

    const-wide/16 v6, 0x400

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v0

    const/4 v0, 0x7

    const-wide/16 v2, 0x400

    div-long v2, p3, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v0

    const/16 v0, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    .line 1339561
    invoke-virtual {v4}, LX/0oG;->d()V

    goto/16 :goto_0
.end method
