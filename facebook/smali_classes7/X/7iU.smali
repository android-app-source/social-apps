.class public LX/7iU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/7iU;


# instance fields
.field public final a:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1227608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1227609
    iput-object p1, p0, LX/7iU;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1227610
    return-void
.end method

.method public static a(LX/0QB;)LX/7iU;
    .locals 4

    .prologue
    .line 1227611
    sget-object v0, LX/7iU;->b:LX/7iU;

    if-nez v0, :cond_1

    .line 1227612
    const-class v1, LX/7iU;

    monitor-enter v1

    .line 1227613
    :try_start_0
    sget-object v0, LX/7iU;->b:LX/7iU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1227614
    if-eqz v2, :cond_0

    .line 1227615
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1227616
    new-instance p0, LX/7iU;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, v3}, LX/7iU;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 1227617
    move-object v0, p0

    .line 1227618
    sput-object v0, LX/7iU;->b:LX/7iU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1227619
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1227620
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1227621
    :cond_1
    sget-object v0, LX/7iU;->b:LX/7iU;

    return-object v0

    .line 1227622
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1227623
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
