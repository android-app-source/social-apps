.class public LX/7yd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# instance fields
.field public final a:LX/0Sh;

.field public final b:LX/0WJ;

.field public final c:LX/3LP;

.field public final d:LX/3iT;

.field public final e:LX/03V;

.field public f:LX/2RQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public volatile g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field

.field public volatile h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Sh;LX/0WJ;LX/3LP;LX/3iT;LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1279896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279897
    iput-object p1, p0, LX/7yd;->a:LX/0Sh;

    .line 1279898
    iput-object p2, p0, LX/7yd;->b:LX/0WJ;

    .line 1279899
    iput-object p3, p0, LX/7yd;->c:LX/3LP;

    .line 1279900
    iput-object p4, p0, LX/7yd;->d:LX/3iT;

    .line 1279901
    iput-object p5, p0, LX/7yd;->e:LX/03V;

    .line 1279902
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1279903
    iput-object v0, p0, LX/7yd;->g:LX/0Px;

    .line 1279904
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/7yd;->h:Ljava/util/Map;

    .line 1279905
    return-void
.end method

.method public static b(LX/0QB;)LX/7yd;
    .locals 6

    .prologue
    .line 1279892
    new-instance v0, LX/7yd;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v2

    check-cast v2, LX/0WJ;

    invoke-static {p0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v3

    check-cast v3, LX/3LP;

    invoke-static {p0}, LX/3iT;->b(LX/0QB;)LX/3iT;

    move-result-object v4

    check-cast v4, LX/3iT;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct/range {v0 .. v5}, LX/7yd;-><init>(LX/0Sh;LX/0WJ;LX/3LP;LX/3iT;LX/03V;)V

    .line 1279893
    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v1

    check-cast v1, LX/2RQ;

    .line 1279894
    iput-object v1, v0, LX/7yd;->f:LX/2RQ;

    .line 1279895
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/Map;)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1279860
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1279861
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1279862
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 1279863
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1279864
    iget-object v7, p0, LX/7yd;->h:Ljava/util/Map;

    invoke-interface {v7, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1279865
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1279866
    sget-object v2, LX/2RU;->USER:LX/2RU;

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1279867
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1279868
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 1279869
    iget-object v2, p0, LX/7yd;->c:LX/3LP;

    iget-object v4, p0, LX/7yd;->f:LX/2RQ;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/2RQ;->b(Ljava/util/Collection;)LX/2RR;

    move-result-object v3

    sget-object v4, LX/3Oq;->USERS:LX/0Px;

    .line 1279870
    iput-object v4, v3, LX/2RR;->c:Ljava/util/Collection;

    .line 1279871
    move-object v3, v3

    .line 1279872
    invoke-virtual {v2, v3}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v8

    .line 1279873
    :cond_3
    :goto_1
    :try_start_0
    invoke-interface {v8}, LX/3On;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1279874
    invoke-interface {v8}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/facebook/user/model/User;

    move-object v6, v0

    .line 1279875
    if-eqz v6, :cond_3

    .line 1279876
    iget-object v0, v6, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v9, v0

    .line 1279877
    iget-object v2, p0, LX/7yd;->d:LX/3iT;

    .line 1279878
    iget-object v0, v6, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v3, v0

    .line 1279879
    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v6}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v6

    sget-object v7, LX/7Gr;->USER:LX/7Gr;

    invoke-virtual/range {v2 .. v7}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v2

    .line 1279880
    iget-object v3, p0, LX/7yd;->h:Ljava/util/Map;

    invoke-interface {v3, v9, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1279881
    :catchall_0
    move-exception v2

    invoke-interface {v8}, LX/3On;->close()V

    throw v2

    :cond_4
    invoke-interface {v8}, LX/3On;->close()V

    .line 1279882
    :cond_5
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    .line 1279883
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1279884
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v7

    .line 1279885
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1279886
    iget-object v4, p0, LX/7yd;->h:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1279887
    if-eqz v4, :cond_6

    .line 1279888
    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1279889
    :cond_6
    const-string v4, "LocalSuggestionsStore"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Could not find local metadata for friend with FBID = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1279890
    :cond_7
    invoke-interface {v5, v2, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1279891
    :cond_8
    return-object v5
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 1279856
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1279857
    iput-object v0, p0, LX/7yd;->g:LX/0Px;

    .line 1279858
    iget-object v0, p0, LX/7yd;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1279859
    return-void
.end method
