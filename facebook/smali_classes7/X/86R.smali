.class public final LX/86R;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1297162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;)Lcom/facebook/graphql/model/GraphQLNode;
    .locals 12
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGraphQLNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1297163
    if-nez p0, :cond_0

    .line 1297164
    const/4 v0, 0x0

    .line 1297165
    :goto_0
    return-object v0

    .line 1297166
    :cond_0
    new-instance v0, LX/4XR;

    invoke-direct {v0}, LX/4XR;-><init>()V

    .line 1297167
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 1297168
    iput-object v1, v0, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1297169
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->c()Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;

    move-result-object v1

    .line 1297170
    if-nez v1, :cond_1

    .line 1297171
    const/4 v2, 0x0

    .line 1297172
    :goto_1
    move-object v1, v2

    .line 1297173
    iput-object v1, v0, LX/4XR;->E:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    .line 1297174
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->b()I

    move-result v1

    .line 1297175
    iput v1, v0, LX/4XR;->ff:I

    .line 1297176
    invoke-virtual {v0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    goto :goto_0

    .line 1297177
    :cond_1
    new-instance v3, LX/4Xw;

    invoke-direct {v3}, LX/4Xw;-><init>()V

    .line 1297178
    invoke-virtual {v1}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;->b()LX/2uF;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1297179
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1297180
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v1}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;->b()LX/2uF;

    move-result-object v5

    invoke-virtual {v5}, LX/39O;->c()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 1297181
    invoke-virtual {v1}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;->b()LX/2uF;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v7, v5, LX/1vs;->b:I

    .line 1297182
    if-nez v7, :cond_4

    .line 1297183
    const/4 v5, 0x0

    .line 1297184
    :goto_3
    move-object v5, v5

    .line 1297185
    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1297186
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1297187
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1297188
    iput-object v2, v3, LX/4Xw;->b:LX/0Px;

    .line 1297189
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;->a()LX/0us;

    move-result-object v2

    .line 1297190
    if-nez v2, :cond_6

    .line 1297191
    const/4 v4, 0x0

    .line 1297192
    :goto_4
    move-object v2, v4

    .line 1297193
    iput-object v2, v3, LX/4Xw;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1297194
    invoke-virtual {v3}, LX/4Xw;->a()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v2

    goto :goto_1

    .line 1297195
    :cond_4
    new-instance v5, LX/4Xx;

    invoke-direct {v5}, LX/4Xx;-><init>()V

    .line 1297196
    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    .line 1297197
    iput-object v8, v5, LX/4Xx;->b:Ljava/lang/String;

    .line 1297198
    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    .line 1297199
    iput-object v8, v5, LX/4Xx;->c:Ljava/lang/String;

    .line 1297200
    const/4 v8, 0x2

    invoke-virtual {v6, v7, v8}, LX/15i;->g(II)I

    move-result v8

    .line 1297201
    if-nez v8, :cond_5

    .line 1297202
    const/4 v9, 0x0

    .line 1297203
    :goto_5
    move-object v8, v9

    .line 1297204
    iput-object v8, v5, LX/4Xx;->d:Lcom/facebook/graphql/model/GraphQLContactPoint;

    .line 1297205
    const/4 v8, 0x3

    invoke-virtual {v6, v7, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    .line 1297206
    iput-object v8, v5, LX/4Xx;->e:Ljava/lang/String;

    .line 1297207
    new-instance v8, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    invoke-direct {v8, v5}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;-><init>(LX/4Xx;)V

    .line 1297208
    move-object v5, v8

    .line 1297209
    goto :goto_3

    .line 1297210
    :cond_5
    new-instance v10, LX/4W0;

    invoke-direct {v10}, LX/4W0;-><init>()V

    .line 1297211
    const/4 v9, 0x0

    const-class v11, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v6, v8, v9, v11}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v9

    check-cast v9, Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1297212
    iput-object v9, v10, LX/4W0;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1297213
    new-instance v9, Lcom/facebook/graphql/model/GraphQLContactPoint;

    invoke-direct {v9, v10}, Lcom/facebook/graphql/model/GraphQLContactPoint;-><init>(LX/4W0;)V

    .line 1297214
    move-object v9, v9

    .line 1297215
    goto :goto_5

    .line 1297216
    :cond_6
    new-instance v4, LX/17L;

    invoke-direct {v4}, LX/17L;-><init>()V

    .line 1297217
    invoke-interface {v2}, LX/0us;->a()Ljava/lang/String;

    move-result-object v5

    .line 1297218
    iput-object v5, v4, LX/17L;->c:Ljava/lang/String;

    .line 1297219
    invoke-interface {v2}, LX/0us;->b()Z

    move-result v5

    .line 1297220
    iput-boolean v5, v4, LX/17L;->d:Z

    .line 1297221
    invoke-interface {v2}, LX/0us;->c()Z

    move-result v5

    .line 1297222
    iput-boolean v5, v4, LX/17L;->e:Z

    .line 1297223
    invoke-interface {v2}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v5

    .line 1297224
    iput-object v5, v4, LX/17L;->f:Ljava/lang/String;

    .line 1297225
    invoke-virtual {v4}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    goto :goto_4
.end method
