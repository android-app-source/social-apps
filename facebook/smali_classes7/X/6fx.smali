.class public final enum LX/6fx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6fx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6fx;

.field public static final enum EMOJI:LX/6fx;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum EMOJI_V2:LX/6fx;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LOCATION_IMAGE:LX/6fx;

.field public static final enum PHOTO:LX/6fx;

.field public static final enum SPONSORED_MESSAGE_IMAGE:LX/6fx;

.field public static final enum STICKER:LX/6fx;

.field public static final enum VIDEO:LX/6fx;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1120772
    new-instance v0, LX/6fx;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v3}, LX/6fx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fx;->PHOTO:LX/6fx;

    .line 1120773
    new-instance v0, LX/6fx;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v4}, LX/6fx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fx;->VIDEO:LX/6fx;

    .line 1120774
    new-instance v0, LX/6fx;

    const-string v1, "STICKER"

    invoke-direct {v0, v1, v5}, LX/6fx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fx;->STICKER:LX/6fx;

    .line 1120775
    new-instance v0, LX/6fx;

    const-string v1, "EMOJI"

    invoke-direct {v0, v1, v6}, LX/6fx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fx;->EMOJI:LX/6fx;

    .line 1120776
    new-instance v0, LX/6fx;

    const-string v1, "EMOJI_V2"

    invoke-direct {v0, v1, v7}, LX/6fx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fx;->EMOJI_V2:LX/6fx;

    .line 1120777
    new-instance v0, LX/6fx;

    const-string v1, "LOCATION_IMAGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6fx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fx;->LOCATION_IMAGE:LX/6fx;

    .line 1120778
    new-instance v0, LX/6fx;

    const-string v1, "SPONSORED_MESSAGE_IMAGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/6fx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fx;->SPONSORED_MESSAGE_IMAGE:LX/6fx;

    .line 1120779
    const/4 v0, 0x7

    new-array v0, v0, [LX/6fx;

    sget-object v1, LX/6fx;->PHOTO:LX/6fx;

    aput-object v1, v0, v3

    sget-object v1, LX/6fx;->VIDEO:LX/6fx;

    aput-object v1, v0, v4

    sget-object v1, LX/6fx;->STICKER:LX/6fx;

    aput-object v1, v0, v5

    sget-object v1, LX/6fx;->EMOJI:LX/6fx;

    aput-object v1, v0, v6

    sget-object v1, LX/6fx;->EMOJI_V2:LX/6fx;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6fx;->LOCATION_IMAGE:LX/6fx;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6fx;->SPONSORED_MESSAGE_IMAGE:LX/6fx;

    aput-object v2, v0, v1

    sput-object v0, LX/6fx;->$VALUES:[LX/6fx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1120780
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6fx;
    .locals 1

    .prologue
    .line 1120781
    const-class v0, LX/6fx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6fx;

    return-object v0
.end method

.method public static values()[LX/6fx;
    .locals 1

    .prologue
    .line 1120771
    sget-object v0, LX/6fx;->$VALUES:[LX/6fx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6fx;

    return-object v0
.end method
