.class public final enum LX/6tr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6tr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6tr;

.field public static final enum CHECK_AUTHENTICATION:LX/6tr;

.field public static final enum CONFIRM_CSC:LX/6tr;

.field public static final enum FINISH:LX/6tr;

.field public static final enum PREPARE_CHECKOUT:LX/6tr;

.field public static final enum PROCESSING_CHECK_AUTHENTICATION:LX/6tr;

.field public static final enum PROCESSING_CONFIRM_CSC:LX/6tr;

.field public static final enum PROCESSING_FINISH:LX/6tr;

.field public static final enum PROCESSING_SEND_PAYMENT:LX/6tr;

.field public static final enum PROCESSING_VERIFY_PAYMENT_METHOD:LX/6tr;

.field public static final enum SEND_PAYMENT:LX/6tr;

.field public static final enum VERIFY_PAYMENT_METHOD:LX/6tr;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1155230
    new-instance v0, LX/6tr;

    const-string v1, "PREPARE_CHECKOUT"

    invoke-direct {v0, v1, v3}, LX/6tr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6tr;->PREPARE_CHECKOUT:LX/6tr;

    .line 1155231
    new-instance v0, LX/6tr;

    const-string v1, "VERIFY_PAYMENT_METHOD"

    invoke-direct {v0, v1, v4}, LX/6tr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6tr;->VERIFY_PAYMENT_METHOD:LX/6tr;

    .line 1155232
    new-instance v0, LX/6tr;

    const-string v1, "PROCESSING_VERIFY_PAYMENT_METHOD"

    invoke-direct {v0, v1, v5}, LX/6tr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6tr;->PROCESSING_VERIFY_PAYMENT_METHOD:LX/6tr;

    .line 1155233
    new-instance v0, LX/6tr;

    const-string v1, "CONFIRM_CSC"

    invoke-direct {v0, v1, v6}, LX/6tr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6tr;->CONFIRM_CSC:LX/6tr;

    .line 1155234
    new-instance v0, LX/6tr;

    const-string v1, "PROCESSING_CONFIRM_CSC"

    invoke-direct {v0, v1, v7}, LX/6tr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6tr;->PROCESSING_CONFIRM_CSC:LX/6tr;

    .line 1155235
    new-instance v0, LX/6tr;

    const-string v1, "CHECK_AUTHENTICATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6tr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6tr;->CHECK_AUTHENTICATION:LX/6tr;

    .line 1155236
    new-instance v0, LX/6tr;

    const-string v1, "PROCESSING_CHECK_AUTHENTICATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/6tr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6tr;->PROCESSING_CHECK_AUTHENTICATION:LX/6tr;

    .line 1155237
    new-instance v0, LX/6tr;

    const-string v1, "SEND_PAYMENT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/6tr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6tr;->SEND_PAYMENT:LX/6tr;

    .line 1155238
    new-instance v0, LX/6tr;

    const-string v1, "PROCESSING_SEND_PAYMENT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/6tr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6tr;->PROCESSING_SEND_PAYMENT:LX/6tr;

    .line 1155239
    new-instance v0, LX/6tr;

    const-string v1, "FINISH"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/6tr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6tr;->FINISH:LX/6tr;

    .line 1155240
    new-instance v0, LX/6tr;

    const-string v1, "PROCESSING_FINISH"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/6tr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6tr;->PROCESSING_FINISH:LX/6tr;

    .line 1155241
    const/16 v0, 0xb

    new-array v0, v0, [LX/6tr;

    sget-object v1, LX/6tr;->PREPARE_CHECKOUT:LX/6tr;

    aput-object v1, v0, v3

    sget-object v1, LX/6tr;->VERIFY_PAYMENT_METHOD:LX/6tr;

    aput-object v1, v0, v4

    sget-object v1, LX/6tr;->PROCESSING_VERIFY_PAYMENT_METHOD:LX/6tr;

    aput-object v1, v0, v5

    sget-object v1, LX/6tr;->CONFIRM_CSC:LX/6tr;

    aput-object v1, v0, v6

    sget-object v1, LX/6tr;->PROCESSING_CONFIRM_CSC:LX/6tr;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6tr;->CHECK_AUTHENTICATION:LX/6tr;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6tr;->PROCESSING_CHECK_AUTHENTICATION:LX/6tr;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6tr;->SEND_PAYMENT:LX/6tr;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6tr;->PROCESSING_SEND_PAYMENT:LX/6tr;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6tr;->FINISH:LX/6tr;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6tr;->PROCESSING_FINISH:LX/6tr;

    aput-object v2, v0, v1

    sput-object v0, LX/6tr;->$VALUES:[LX/6tr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1155242
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6tr;
    .locals 1

    .prologue
    .line 1155243
    const-class v0, LX/6tr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6tr;

    return-object v0
.end method

.method public static values()[LX/6tr;
    .locals 1

    .prologue
    .line 1155244
    sget-object v0, LX/6tr;->$VALUES:[LX/6tr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6tr;

    return-object v0
.end method
