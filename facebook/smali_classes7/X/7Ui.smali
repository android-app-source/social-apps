.class public final LX/7Ui;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/soundwave/SoundWaveView;

.field private b:F


# direct methods
.method public constructor <init>(Lcom/facebook/widget/soundwave/SoundWaveView;F)V
    .locals 1

    .prologue
    .line 1213288
    iput-object p1, p0, LX/7Ui;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1213289
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, LX/7Ui;->b:F

    .line 1213290
    iput p2, p0, LX/7Ui;->b:F

    .line 1213291
    return-void
.end method


# virtual methods
.method public final getInterpolation(F)F
    .locals 4

    .prologue
    .line 1213292
    iget v0, p0, LX/7Ui;->b:F

    iget v1, p0, LX/7Ui;->b:F

    div-float v1, p1, v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v0, v1

    .line 1213293
    return v0
.end method
