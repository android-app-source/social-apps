.class public final LX/8MV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/7ml;

.field public final synthetic c:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;ZLX/7ml;)V
    .locals 0

    .prologue
    .line 1334466
    iput-object p1, p0, LX/8MV;->c:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iput-boolean p2, p0, LX/8MV;->a:Z

    iput-object p3, p0, LX/8MV;->b:LX/7ml;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 1334467
    iget-boolean v0, p0, LX/8MV;->a:Z

    if-nez v0, :cond_0

    .line 1334468
    iget-object v0, p0, LX/8MV;->c:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v1, p0, LX/8MV;->b:LX/7ml;

    .line 1334469
    new-instance v2, LX/0ju;

    iget-object v3, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f0810a8

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    const v3, 0x7f081065

    new-instance p1, LX/8MD;

    invoke-direct {p1, v0}, LX/8MD;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;)V

    invoke-virtual {v2, v3, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    const v3, 0x7f08104c

    new-instance p1, LX/8MC;

    invoke-direct {p1, v0, v1}, LX/8MC;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)V

    invoke-virtual {v2, v3, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    .line 1334470
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v2

    .line 1334471
    new-instance v3, LX/8ME;

    invoke-direct {v3, v0}, LX/8ME;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;)V

    invoke-static {v0, v2, v3}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/2EJ;Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1334472
    invoke-virtual {v2}, LX/2EJ;->show()V

    .line 1334473
    :goto_0
    iget-object v0, p0, LX/8MV;->c:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    sget-object v1, LX/8K6;->PENDING_SECTION:LX/8K6;

    iget-object v1, v1, LX/8K6;->analyticsName:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1RW;->b(Ljava/lang/String;)V

    .line 1334474
    const/4 v0, 0x1

    return v0

    .line 1334475
    :cond_0
    iget-object v0, p0, LX/8MV;->c:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v1, p0, LX/8MV;->b:LX/7ml;

    .line 1334476
    new-instance v2, LX/0ju;

    iget-object v3, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f082068

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    const v3, 0x7f082067

    new-instance p1, LX/8MG;

    invoke-direct {p1, v0}, LX/8MG;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;)V

    invoke-virtual {v2, v3, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    const v3, 0x7f08104c

    new-instance p1, LX/8MF;

    invoke-direct {p1, v0, v1}, LX/8MF;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)V

    invoke-virtual {v2, v3, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    .line 1334477
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v2

    .line 1334478
    new-instance v3, LX/8MH;

    invoke-direct {v3, v0}, LX/8MH;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;)V

    invoke-static {v0, v2, v3}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/2EJ;Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1334479
    invoke-virtual {v2}, LX/2EJ;->show()V

    .line 1334480
    goto :goto_0
.end method
