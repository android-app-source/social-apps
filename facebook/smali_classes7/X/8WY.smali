.class public final LX/8WY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/8WZ;


# direct methods
.method public constructor <init>(LX/8WZ;)V
    .locals 0

    .prologue
    .line 1354206
    iput-object p1, p0, LX/8WY;->a:LX/8WZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x2ef5c6fd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1354207
    iget-object v0, p0, LX/8WY;->a:LX/8WZ;

    iget-object v0, v0, LX/8WZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    iget-object v1, p0, LX/8WY;->a:LX/8WZ;

    iget-object v1, v1, LX/8WZ;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iget-object v2, p0, LX/8WY;->a:LX/8WZ;

    iget-object v2, v2, LX/8WZ;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8TS;

    .line 1354208
    iget-object p1, v2, LX/8TS;->e:LX/8TO;

    move-object v2, p1

    .line 1354209
    iget-object p1, v2, LX/8TO;->b:Ljava/lang/String;

    move-object v2, p1

    .line 1354210
    const-string v5, "98.0"

    invoke-static {v0, v5}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1354211
    sget-object v5, LX/8Tp;->MESSENGER_UNSUPPORTED:LX/8Tp;

    .line 1354212
    :goto_0
    move-object v1, v5

    .line 1354213
    iget-object v0, p0, LX/8WY;->a:LX/8WZ;

    iget-object v0, v0, LX/8WZ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8T9;

    invoke-virtual {v0, v1}, LX/8T9;->a(LX/8Tp;)V

    .line 1354214
    const v0, 0x148757b2

    invoke-static {v4, v4, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1354215
    :cond_0
    instance-of v5, v1, Lcom/facebook/quicksilver/QuicksilverActivity;

    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 1354216
    new-instance v6, Lcom/facebook/quicksilver/common/sharing/GameChallengeCreationExtras;

    move-object v5, v1

    check-cast v5, Lcom/facebook/quicksilver/QuicksilverActivity;

    invoke-virtual {v5}, Lcom/facebook/quicksilver/QuicksilverActivity;->m()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v2, v5}, Lcom/facebook/quicksilver/common/sharing/GameChallengeCreationExtras;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1354217
    iget-object v5, v0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-static {v6}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(Lcom/facebook/quicksilver/common/sharing/GameShareExtras;)Landroid/content/Intent;

    move-result-object v6

    const/16 p1, 0x22b7

    invoke-interface {v5, v6, p1, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1354218
    sget-object v5, LX/8Tp;->ACTIVITY_STARTED:LX/8Tp;

    goto :goto_0
.end method
