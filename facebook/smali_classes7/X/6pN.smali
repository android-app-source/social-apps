.class public abstract LX/6pN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6od;


# instance fields
.field private final a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V
    .locals 1

    .prologue
    .line 1149405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1149406
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iput-object v0, p0, LX/6pN;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    .line 1149407
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1149408
    iget-object v0, p0, LX/6pN;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    .line 1149409
    iget-object v1, v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1149410
    iget-object v2, v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->a:LX/6pF;

    move-object v1, v2

    .line 1149411
    sget-object v2, LX/6pF;->DELETE:LX/6pF;

    if-ne v1, v2, :cond_0

    .line 1149412
    sget-object v1, LX/6pF;->DELETE_WITH_PASSWORD:LX/6pF;

    invoke-static {v0, v1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;LX/6pF;)Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    move-result-object v1

    .line 1149413
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->a(Landroid/content/Context;Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;)Landroid/content/Intent;

    move-result-object v1

    .line 1149414
    iget-object v2, v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {v2, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1149415
    :goto_0
    return-void

    .line 1149416
    :cond_0
    invoke-static {v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->t(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    goto :goto_0
.end method
