.class public LX/8bW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/1M7;

.field private final b:LX/1HI;

.field private final c:LX/1Lg;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/36s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1HI;LX/1Lg;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1372118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1372119
    iput-object p1, p0, LX/8bW;->b:LX/1HI;

    .line 1372120
    iput-object p2, p0, LX/8bW;->c:LX/1Lg;

    .line 1372121
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8bW;->d:Ljava/util/Map;

    .line 1372122
    return-void
.end method

.method public static b(LX/0QB;)LX/8bW;
    .locals 3

    .prologue
    .line 1372111
    new-instance v2, LX/8bW;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v0

    check-cast v0, LX/1HI;

    invoke-static {p0}, LX/1Lf;->a(LX/0QB;)LX/1Lg;

    move-result-object v1

    check-cast v1, LX/1Lg;

    invoke-direct {v2, v0, v1}, LX/8bW;-><init>(LX/1HI;LX/1Lg;)V

    .line 1372112
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)LX/1ca;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1372123
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1372124
    const/4 v0, 0x0

    .line 1372125
    :goto_0
    return-object v0

    .line 1372126
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    sget-object v1, LX/1bc;->MEDIUM:LX/1bc;

    .line 1372127
    iput-object v1, v0, LX/1bX;->i:LX/1bc;

    .line 1372128
    move-object v0, v0

    .line 1372129
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1372130
    iget-object v1, p0, LX/8bW;->b:LX/1HI;

    invoke-virtual {v1, v0, p2}, LX/1HI;->e(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 1372131
    new-instance v1, LX/8bV;

    invoke-direct {v1}, LX/8bV;-><init>()V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1372113
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1372114
    :cond_0
    :goto_0
    return-void

    .line 1372115
    :cond_1
    iget-object v0, p0, LX/8bW;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1372116
    iget-object v0, p0, LX/8bW;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/36s;

    .line 1372117
    iget-object v1, p0, LX/8bW;->a:LX/1M7;

    const/4 v2, 0x1

    new-array v2, v2, [LX/36s;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, LX/1M7;->c([LX/36s;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1372104
    iget-object v0, p0, LX/8bW;->c:LX/1Lg;

    sget-object v1, LX/1Li;->INSTANT_ARTICLE:LX/1Li;

    invoke-virtual {v0, v1}, LX/1Lg;->a(LX/1Li;)LX/1M7;

    move-result-object v0

    iput-object v0, p0, LX/8bW;->a:LX/1M7;

    .line 1372105
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1372106
    new-instance v0, LX/36s;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1, p2}, LX/36s;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 1372107
    iget-object v1, p0, LX/8bW;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1372108
    iget-object v1, p0, LX/8bW;->a:LX/1M7;

    new-array v2, v4, [LX/36s;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, LX/1M7;->a([LX/36s;)V

    .line 1372109
    :cond_0
    iget-object v0, p0, LX/8bW;->a:LX/1M7;

    invoke-interface {v0, v4}, LX/1M7;->a(Z)V

    .line 1372110
    return-void
.end method
