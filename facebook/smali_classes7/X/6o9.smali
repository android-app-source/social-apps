.class public LX/6o9;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Tn;


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1147911
    sget-object v0, LX/6oM;->a:LX/0Tn;

    const-string v1, "fingerprint_authentication_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6o9;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1147912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1147913
    iput-object p1, p0, LX/6o9;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1147914
    return-void
.end method

.method public static b(LX/0QB;)LX/6o9;
    .locals 2

    .prologue
    .line 1147915
    new-instance v1, LX/6o9;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v0}, LX/6o9;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1147916
    return-object v1
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 1147917
    iget-object v0, p0, LX/6o9;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/6o9;->a:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1147918
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 1147919
    iget-object v0, p0, LX/6o9;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/6o9;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method
