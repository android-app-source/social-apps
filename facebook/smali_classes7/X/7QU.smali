.class public LX/7QU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1204387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/view/ViewGroup;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1204378
    if-eqz p1, :cond_1

    .line 1204379
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 1204380
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 1204381
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1204382
    if-eq v2, p0, :cond_0

    .line 1204383
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2}, LX/0vv;->e(Landroid/view/View;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1204384
    const/4 v3, 0x4

    invoke-static {v2, v3}, LX/0vv;->d(Landroid/view/View;I)V

    .line 1204385
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1204386
    :cond_1
    return-void
.end method

.method public static a(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1204388
    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1204389
    :cond_0
    :goto_0
    return v1

    .line 1204390
    :cond_1
    invoke-static {}, LX/3sp;->b()LX/3sp;

    move-result-object v0

    .line 1204391
    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/3sp;)V

    .line 1204392
    invoke-virtual {v0}, LX/3sp;->g()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1204393
    invoke-virtual {v0}, LX/3sp;->p()V

    move v1, v2

    .line 1204394
    goto :goto_0

    .line 1204395
    :cond_2
    invoke-virtual {v0}, LX/3sp;->p()V

    .line 1204396
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1204397
    check-cast p0, Landroid/view/ViewGroup;

    move v0, v1

    .line 1204398
    :goto_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 1204399
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, LX/7QU;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v1, v2

    .line 1204400
    goto :goto_0

    .line 1204401
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1204376
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 1204377
    return-void
.end method

.method public static b(Landroid/view/View;Landroid/view/ViewGroup;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/view/ViewGroup;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1204364
    if-eqz p1, :cond_2

    .line 1204365
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 1204366
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 1204367
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1204368
    if-eq v2, p0, :cond_0

    .line 1204369
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1204370
    invoke-static {v2}, LX/0vv;->e(Landroid/view/View;)I

    move-result v3

    .line 1204371
    :goto_1
    move v3, v3

    .line 1204372
    invoke-static {v2, v3}, LX/0vv;->d(Landroid/view/View;I)V

    .line 1204373
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1204374
    :cond_1
    invoke-interface {p2}, Ljava/util/Map;->clear()V

    .line 1204375
    :cond_2
    return-void

    :cond_3
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    goto :goto_1
.end method
