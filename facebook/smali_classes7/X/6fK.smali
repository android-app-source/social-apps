.class public final enum LX/6fK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6fK;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6fK;

.field public static final enum FETCH_FQL:LX/6fK;

.field public static final enum LOADED_BY_THREADVIEW:LX/6fK;

.field public static final enum MERCURY_ACTION_DELIVERY:LX/6fK;

.field public static final enum PUSH_C2DM_DELIVERY:LX/6fK;

.field public static final enum PUSH_CALL_LOG_MESSAGE:LX/6fK;

.field public static final enum PUSH_META_DATA_ACTION_MESSAGE:LX/6fK;

.field public static final enum PUSH_MQTT_MESSAGE:LX/6fK;

.field public static final enum PUSH_MQTT_PAGE_MESSAGE:LX/6fK;

.field public static final enum SYNC_PROTOCOL_ADMIN_TEXT_MESSAGE_DELTA:LX/6fK;

.field public static final enum SYNC_PROTOCOL_BROADACST_DELTA:LX/6fK;

.field public static final enum SYNC_PROTOCOL_NEW_MESSAGE_DELTA:LX/6fK;

.field public static final enum SYNC_PROTOCOL_PARTICIPANTS_ADDED_DELTA:LX/6fK;

.field public static final enum SYNC_PROTOCOL_PARTICIPANT_LEFT_GROUP_DELTA:LX/6fK;

.field public static final enum SYNC_PROTOCOL_RTC_EVENT_LOG_DELTA:LX/6fK;

.field public static final enum SYNC_PROTOCOL_SENT_MESSAGE_DELTA:LX/6fK;

.field public static final enum SYNC_PROTOCOL_THREAD_IMAGE_DELTA:LX/6fK;

.field public static final enum SYNC_PROTOCOL_THREAD_NAME_DELTA:LX/6fK;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1119759
    new-instance v0, LX/6fK;

    const-string v1, "FETCH_FQL"

    invoke-direct {v0, v1, v3}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->FETCH_FQL:LX/6fK;

    .line 1119760
    new-instance v0, LX/6fK;

    const-string v1, "MERCURY_ACTION_DELIVERY"

    invoke-direct {v0, v1, v4}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->MERCURY_ACTION_DELIVERY:LX/6fK;

    .line 1119761
    new-instance v0, LX/6fK;

    const-string v1, "PUSH_C2DM_DELIVERY"

    invoke-direct {v0, v1, v5}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->PUSH_C2DM_DELIVERY:LX/6fK;

    .line 1119762
    new-instance v0, LX/6fK;

    const-string v1, "PUSH_CALL_LOG_MESSAGE"

    invoke-direct {v0, v1, v6}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->PUSH_CALL_LOG_MESSAGE:LX/6fK;

    .line 1119763
    new-instance v0, LX/6fK;

    const-string v1, "PUSH_META_DATA_ACTION_MESSAGE"

    invoke-direct {v0, v1, v7}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->PUSH_META_DATA_ACTION_MESSAGE:LX/6fK;

    .line 1119764
    new-instance v0, LX/6fK;

    const-string v1, "PUSH_MQTT_MESSAGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->PUSH_MQTT_MESSAGE:LX/6fK;

    .line 1119765
    new-instance v0, LX/6fK;

    const-string v1, "PUSH_MQTT_PAGE_MESSAGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->PUSH_MQTT_PAGE_MESSAGE:LX/6fK;

    .line 1119766
    new-instance v0, LX/6fK;

    const-string v1, "LOADED_BY_THREADVIEW"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->LOADED_BY_THREADVIEW:LX/6fK;

    .line 1119767
    new-instance v0, LX/6fK;

    const-string v1, "SYNC_PROTOCOL_ADMIN_TEXT_MESSAGE_DELTA"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->SYNC_PROTOCOL_ADMIN_TEXT_MESSAGE_DELTA:LX/6fK;

    .line 1119768
    new-instance v0, LX/6fK;

    const-string v1, "SYNC_PROTOCOL_BROADACST_DELTA"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->SYNC_PROTOCOL_BROADACST_DELTA:LX/6fK;

    .line 1119769
    new-instance v0, LX/6fK;

    const-string v1, "SYNC_PROTOCOL_NEW_MESSAGE_DELTA"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->SYNC_PROTOCOL_NEW_MESSAGE_DELTA:LX/6fK;

    .line 1119770
    new-instance v0, LX/6fK;

    const-string v1, "SYNC_PROTOCOL_PARTICIPANTS_ADDED_DELTA"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->SYNC_PROTOCOL_PARTICIPANTS_ADDED_DELTA:LX/6fK;

    .line 1119771
    new-instance v0, LX/6fK;

    const-string v1, "SYNC_PROTOCOL_PARTICIPANT_LEFT_GROUP_DELTA"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->SYNC_PROTOCOL_PARTICIPANT_LEFT_GROUP_DELTA:LX/6fK;

    .line 1119772
    new-instance v0, LX/6fK;

    const-string v1, "SYNC_PROTOCOL_RTC_EVENT_LOG_DELTA"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->SYNC_PROTOCOL_RTC_EVENT_LOG_DELTA:LX/6fK;

    .line 1119773
    new-instance v0, LX/6fK;

    const-string v1, "SYNC_PROTOCOL_SENT_MESSAGE_DELTA"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->SYNC_PROTOCOL_SENT_MESSAGE_DELTA:LX/6fK;

    .line 1119774
    new-instance v0, LX/6fK;

    const-string v1, "SYNC_PROTOCOL_THREAD_IMAGE_DELTA"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->SYNC_PROTOCOL_THREAD_IMAGE_DELTA:LX/6fK;

    .line 1119775
    new-instance v0, LX/6fK;

    const-string v1, "SYNC_PROTOCOL_THREAD_NAME_DELTA"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/6fK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fK;->SYNC_PROTOCOL_THREAD_NAME_DELTA:LX/6fK;

    .line 1119776
    const/16 v0, 0x11

    new-array v0, v0, [LX/6fK;

    sget-object v1, LX/6fK;->FETCH_FQL:LX/6fK;

    aput-object v1, v0, v3

    sget-object v1, LX/6fK;->MERCURY_ACTION_DELIVERY:LX/6fK;

    aput-object v1, v0, v4

    sget-object v1, LX/6fK;->PUSH_C2DM_DELIVERY:LX/6fK;

    aput-object v1, v0, v5

    sget-object v1, LX/6fK;->PUSH_CALL_LOG_MESSAGE:LX/6fK;

    aput-object v1, v0, v6

    sget-object v1, LX/6fK;->PUSH_META_DATA_ACTION_MESSAGE:LX/6fK;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6fK;->PUSH_MQTT_MESSAGE:LX/6fK;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6fK;->PUSH_MQTT_PAGE_MESSAGE:LX/6fK;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6fK;->LOADED_BY_THREADVIEW:LX/6fK;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6fK;->SYNC_PROTOCOL_ADMIN_TEXT_MESSAGE_DELTA:LX/6fK;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6fK;->SYNC_PROTOCOL_BROADACST_DELTA:LX/6fK;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6fK;->SYNC_PROTOCOL_NEW_MESSAGE_DELTA:LX/6fK;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/6fK;->SYNC_PROTOCOL_PARTICIPANTS_ADDED_DELTA:LX/6fK;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/6fK;->SYNC_PROTOCOL_PARTICIPANT_LEFT_GROUP_DELTA:LX/6fK;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/6fK;->SYNC_PROTOCOL_RTC_EVENT_LOG_DELTA:LX/6fK;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/6fK;->SYNC_PROTOCOL_SENT_MESSAGE_DELTA:LX/6fK;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/6fK;->SYNC_PROTOCOL_THREAD_IMAGE_DELTA:LX/6fK;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/6fK;->SYNC_PROTOCOL_THREAD_NAME_DELTA:LX/6fK;

    aput-object v2, v0, v1

    sput-object v0, LX/6fK;->$VALUES:[LX/6fK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1119758
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6fK;
    .locals 1

    .prologue
    .line 1119757
    const-class v0, LX/6fK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6fK;

    return-object v0
.end method

.method public static values()[LX/6fK;
    .locals 1

    .prologue
    .line 1119756
    sget-object v0, LX/6fK;->$VALUES:[LX/6fK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6fK;

    return-object v0
.end method
