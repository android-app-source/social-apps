.class public final enum LX/7LF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7LF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7LF;

.field public static final enum ENTER_ANIMATE:LX/7LF;

.field public static final enum NOP:LX/7LF;

.field public static final enum POSITION:LX/7LF;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1196889
    new-instance v0, LX/7LF;

    const-string v1, "ENTER_ANIMATE"

    invoke-direct {v0, v1, v2}, LX/7LF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7LF;->ENTER_ANIMATE:LX/7LF;

    .line 1196890
    new-instance v0, LX/7LF;

    const-string v1, "POSITION"

    invoke-direct {v0, v1, v3}, LX/7LF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7LF;->POSITION:LX/7LF;

    .line 1196891
    new-instance v0, LX/7LF;

    const-string v1, "NOP"

    invoke-direct {v0, v1, v4}, LX/7LF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7LF;->NOP:LX/7LF;

    .line 1196892
    const/4 v0, 0x3

    new-array v0, v0, [LX/7LF;

    sget-object v1, LX/7LF;->ENTER_ANIMATE:LX/7LF;

    aput-object v1, v0, v2

    sget-object v1, LX/7LF;->POSITION:LX/7LF;

    aput-object v1, v0, v3

    sget-object v1, LX/7LF;->NOP:LX/7LF;

    aput-object v1, v0, v4

    sput-object v0, LX/7LF;->$VALUES:[LX/7LF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1196893
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7LF;
    .locals 1

    .prologue
    .line 1196894
    const-class v0, LX/7LF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7LF;

    return-object v0
.end method

.method public static values()[LX/7LF;
    .locals 1

    .prologue
    .line 1196895
    sget-object v0, LX/7LF;->$VALUES:[LX/7LF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7LF;

    return-object v0
.end method
