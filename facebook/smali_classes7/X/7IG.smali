.class public abstract LX/7IG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/7IJ;

.field public b:I

.field public c:I

.field public d:I

.field private final e:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public constructor <init>(Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 2
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, -0x1

    .line 1191563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1191564
    iput-object p1, p0, LX/7IG;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1191565
    sget-object v0, LX/7IJ;->START:LX/7IJ;

    iput-object v0, p0, LX/7IG;->a:LX/7IJ;

    .line 1191566
    iput v1, p0, LX/7IG;->b:I

    .line 1191567
    iput v1, p0, LX/7IG;->c:I

    .line 1191568
    iput v1, p0, LX/7IG;->d:I

    .line 1191569
    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract b()I
.end method

.method public abstract c()Z
.end method

.method public final declared-synchronized d()LX/7IJ;
    .locals 1

    .prologue
    .line 1191562
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7IG;->a:LX/7IJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1191561
    iget-object v0, p0, LX/7IG;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method
