.class public final LX/7Qd;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7Qe;


# direct methods
.method public constructor <init>(LX/7Qe;)V
    .locals 0

    .prologue
    .line 1204691
    iput-object p1, p0, LX/7Qd;->a:LX/7Qe;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1204692
    iget-object v0, p0, LX/7Qd;->a:LX/7Qe;

    iget-object v0, v0, LX/7Qe;->e:LX/0Zc;

    const-string v1, ""

    sget-object v2, LX/7IM;->LIVE_SUBSCRIPTION_QUERY_FAIL:LX/7IM;

    const-string v3, "Live subscription query failed"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0Zc;->a(Ljava/lang/String;LX/7IM;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1204693
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1204694
    check-cast p1, Ljava/util/Map;

    .line 1204695
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/7Qd;->a:LX/7Qe;

    iget-object v0, v0, LX/7Qe;->g:LX/7QY;

    if-eqz v0, :cond_1

    .line 1204696
    iget-object v0, p0, LX/7Qd;->a:LX/7Qe;

    iget-object v0, v0, LX/7Qe;->g:LX/7QY;

    .line 1204697
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1204698
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1204699
    iget-object v4, v0, LX/7QY;->a:LX/7Qb;

    iget-object v4, v4, LX/7Qb;->h:LX/7QZ;

    invoke-virtual {v4, v2}, LX/7QZ;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1204700
    iget-object p0, v0, LX/7QY;->a:LX/7Qb;

    iget-object p0, p0, LX/7Qb;->h:LX/7QZ;

    invoke-virtual {p0, v2}, LX/7QZ;->a(Ljava/lang/String;)V

    .line 1204701
    if-eqz v4, :cond_0

    .line 1204702
    iget-object v2, v0, LX/7QY;->a:LX/7Qb;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;

    invoke-static {v2, v4, v1}, LX/7Qb;->a$redex0(LX/7Qb;Ljava/lang/String;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V

    goto :goto_0

    .line 1204703
    :cond_1
    return-void
.end method
