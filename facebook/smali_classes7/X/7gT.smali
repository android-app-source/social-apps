.class public LX/7gT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static volatile n:LX/7gT;


# instance fields
.field private final c:LX/0Zb;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:Z

.field public m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1224360
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sput-object v0, LX/7gT;->a:Ljava/lang/String;

    .line 1224361
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sput-object v0, LX/7gT;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1224362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224363
    iput-boolean v0, p0, LX/7gT;->k:Z

    .line 1224364
    iput-boolean v0, p0, LX/7gT;->l:Z

    .line 1224365
    iput-boolean v0, p0, LX/7gT;->m:Z

    .line 1224366
    iput-object p1, p0, LX/7gT;->c:LX/0Zb;

    .line 1224367
    return-void
.end method

.method public static a(LX/0QB;)LX/7gT;
    .locals 4

    .prologue
    .line 1224368
    sget-object v0, LX/7gT;->n:LX/7gT;

    if-nez v0, :cond_1

    .line 1224369
    const-class v1, LX/7gT;

    monitor-enter v1

    .line 1224370
    :try_start_0
    sget-object v0, LX/7gT;->n:LX/7gT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1224371
    if-eqz v2, :cond_0

    .line 1224372
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1224373
    new-instance p0, LX/7gT;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/7gT;-><init>(LX/0Zb;)V

    .line 1224374
    move-object v0, p0

    .line 1224375
    sput-object v0, LX/7gT;->n:LX/7gT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1224376
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1224377
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1224378
    :cond_1
    sget-object v0, LX/7gT;->n:LX/7gT;

    return-object v0

    .line 1224379
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1224380
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/7gR;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7gR;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1224381
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1224382
    :goto_0
    return-void

    .line 1224383
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1224384
    invoke-static {p0, v0}, LX/7gT;->a(LX/7gT;Landroid/os/Bundle;)V

    .line 1224385
    sget-object v1, LX/7gS;->DIRECT_RECIPIENTS:LX/7gS;

    invoke-virtual {v1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1224386
    if-eqz p3, :cond_1

    .line 1224387
    sget-object v1, LX/7gS;->DIRECT_RECIPIENTS_RANKINGS:LX/7gS;

    invoke-virtual {v1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1224388
    :cond_1
    sget-object v1, LX/7gS;->DIRECT_RECIPIENTS_SIZE:LX/7gS;

    invoke-virtual {v1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1224389
    sget-object v1, LX/7gS;->DIRECT_RECIPIENTS_SOURCE:LX/7gS;

    invoke-virtual {v1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224390
    invoke-static {p0, p1, v0}, LX/7gT;->a(LX/7gT;LX/7gR;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public static a(LX/7gT;LX/7gR;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # LX/7gR;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1224391
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p1}, LX/7gR;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1224392
    invoke-virtual {p1}, LX/7gR;->getModuleName()Ljava/lang/String;

    move-result-object v0

    .line 1224393
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1224394
    iget-object v0, p0, LX/7gT;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1224395
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1224396
    if-eqz p2, :cond_0

    .line 1224397
    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1224398
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 1224399
    :cond_0
    iget-object v0, p0, LX/7gT;->c:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1224400
    return-void
.end method

.method public static a(LX/7gT;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1224401
    sget-object v0, LX/7gS;->SHARE_SHEET_ID:LX/7gS;

    invoke-virtual {v0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1224402
    iget-object v1, p0, LX/7gT;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1224403
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224404
    sget-object v0, LX/7gS;->INSPIRATION_GROUP_SESSION:LX/7gS;

    invoke-virtual {v0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/7gT;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224405
    sget-object v0, LX/7gS;->PROMPT_ID:LX/7gS;

    invoke-virtual {v0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/7gT;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224406
    sget-object v0, LX/7gS;->MEDIA_TYPE:LX/7gS;

    invoke-virtual {v0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/7gT;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224407
    sget-object v0, LX/7gS;->MEDIA_CONTENT_ID:LX/7gS;

    invoke-virtual {v0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/7gT;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224408
    sget-object v0, LX/7gS;->DEVICE_MODEL:LX/7gS;

    invoke-virtual {v0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/7gT;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224409
    sget-object v0, LX/7gS;->DEVICE_MANUFACTURER:LX/7gS;

    invoke-virtual {v0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/7gT;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224410
    return-void
.end method


# virtual methods
.method public final a(LX/7gR;)V
    .locals 3

    .prologue
    .line 1224411
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1224412
    invoke-static {p0, v0}, LX/7gT;->a(LX/7gT;Landroid/os/Bundle;)V

    .line 1224413
    sget-object v1, LX/7gR;->INCLUDE_NEWS_FEED:LX/7gR;

    if-eq p1, v1, :cond_0

    sget-object v1, LX/7gR;->ENTER_SHARE_SHEET:LX/7gR;

    if-eq p1, v1, :cond_0

    sget-object v1, LX/7gR;->SEND_NEWS_FEED:LX/7gR;

    if-ne p1, v1, :cond_1

    .line 1224414
    :cond_0
    sget-object v1, LX/7gS;->CURRENT_PRIVACY:LX/7gS;

    invoke-virtual {v1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/7gT;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224415
    :cond_1
    invoke-static {p0, p1, v0}, LX/7gT;->a(LX/7gT;LX/7gR;Landroid/os/Bundle;)V

    .line 1224416
    return-void
.end method

.method public final a(LX/7gR;LX/0Px;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7gR;",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1224417
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1224418
    :cond_0
    :goto_0
    return-void

    .line 1224419
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1224420
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1224421
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1224422
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_3

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData;

    .line 1224423
    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getAudienceType()LX/7gf;

    move-result-object v6

    sget-object v7, LX/7gf;->RANKED_FRIENDS:LX/7gf;

    if-ne v6, v7, :cond_2

    .line 1224424
    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1224425
    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getRanking()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1224426
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1224427
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1224428
    :cond_3
    sget-object v0, LX/7gS;->RANKED_AUDIENCE_TYPE:LX/7gS;

    invoke-virtual {v0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v2, v3, v0}, LX/7gT;->a(LX/7gR;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 1224429
    const/4 v0, 0x0

    sget-object v1, LX/7gS;->SEARCH_AUDIENCE_TYPE:LX/7gS;

    invoke-virtual {v1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v4, v0, v1}, LX/7gT;->a(LX/7gR;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_0
.end method
