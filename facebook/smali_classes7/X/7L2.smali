.class public final LX/7L2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/FullScreenVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V
    .locals 0

    .prologue
    .line 1196742
    iput-object p1, p0, LX/7L2;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3

    .prologue
    .line 1196743
    iget-object v0, p0, LX/7L2;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-boolean v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->w:Z

    if-nez v0, :cond_0

    .line 1196744
    iget-object v0, p0, LX/7L2;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->e()V

    .line 1196745
    iget-object v0, p0, LX/7L2;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->f:Lcom/facebook/video/player/VideoSpecText;

    iget-object v1, p0, LX/7L2;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v1}, LX/7Kd;->getMetadata()LX/7IE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/VideoSpecText;->a(LX/7IE;)V

    .line 1196746
    :goto_0
    return-void

    .line 1196747
    :cond_0
    iget-object v0, p0, LX/7L2;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->g:LX/0Sh;

    iget-object v1, p0, LX/7L2;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->ay:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v2, LX/7Kt;

    invoke-direct {v2, p0, p1}, LX/7Kt;-><init>(LX/7L2;Landroid/media/MediaPlayer;)V

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method
