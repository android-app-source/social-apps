.class public LX/8KA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/8KA;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Or;LX/0ad;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/upload/quality/PhotosHighDefUploadSettingValue;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1329918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1329919
    iput-object p1, p0, LX/8KA;->a:LX/0Or;

    .line 1329920
    iput-object p2, p0, LX/8KA;->b:LX/0ad;

    .line 1329921
    return-void
.end method

.method public static a(LX/0QB;)LX/8KA;
    .locals 5

    .prologue
    .line 1329922
    sget-object v0, LX/8KA;->c:LX/8KA;

    if-nez v0, :cond_1

    .line 1329923
    const-class v1, LX/8KA;

    monitor-enter v1

    .line 1329924
    :try_start_0
    sget-object v0, LX/8KA;->c:LX/8KA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1329925
    if-eqz v2, :cond_0

    .line 1329926
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1329927
    new-instance v4, LX/8KA;

    const/16 v3, 0x1554

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, p0, v3}, LX/8KA;-><init>(LX/0Or;LX/0ad;)V

    .line 1329928
    move-object v0, v4

    .line 1329929
    sput-object v0, LX/8KA;->c:LX/8KA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1329930
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1329931
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1329932
    :cond_1
    sget-object v0, LX/8KA;->c:LX/8KA;

    return-object v0

    .line 1329933
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1329934
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 1329917
    iget-object v0, p0, LX/8KA;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8KA;->b:LX/0ad;

    sget v1, LX/8Jz;->n:I

    const/16 v2, 0x5a

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/8KA;->b:LX/0ad;

    sget v1, LX/8Jz;->r:I

    const/16 v2, 0x47

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 1329916
    iget-object v0, p0, LX/8KA;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8KA;->b:LX/0ad;

    sget v1, LX/8Jz;->o:I

    const/16 v2, 0x5a

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/8KA;->b:LX/0ad;

    sget v1, LX/8Jz;->s:I

    const/16 v2, 0x47

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0
.end method
