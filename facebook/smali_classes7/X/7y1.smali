.class public LX/7y1;
.super LX/62U;
.source ""


# instance fields
.field public final m:Lcom/facebook/widget/text/BetterTextView;

.field public final n:Landroid/content/res/Resources;

.field public o:LX/7xH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1278702
    invoke-direct {p0, p1}, LX/62U;-><init>(Landroid/view/View;)V

    .line 1278703
    const-class v0, LX/7y1;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, LX/7y1;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    move-object v0, p1

    .line 1278704
    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/7y1;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 1278705
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/7y1;->n:Landroid/content/res/Resources;

    .line 1278706
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/7y1;

    invoke-static {p0}, LX/7xH;->a(LX/0QB;)LX/7xH;

    move-result-object p0

    check-cast p0, LX/7xH;

    iput-object p0, p1, LX/7y1;->o:LX/7xH;

    return-void
.end method
