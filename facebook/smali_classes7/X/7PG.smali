.class public final LX/7PG;
.super Ljava/io/OutputStream;
.source ""


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/io/OutputStream;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 1202665
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 1202666
    invoke-static {p1}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/7PG;->a:LX/0Px;

    .line 1202667
    return-void
.end method

.method private declared-synchronized a()V
    .locals 1

    .prologue
    .line 1202668
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/7PG;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1202669
    monitor-exit p0

    return-void

    .line 1202670
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1202671
    iget-object v0, p0, LX/7PG;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/7PG;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 1202672
    :try_start_0
    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1202673
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1202674
    :catch_0
    move-exception v0

    .line 1202675
    invoke-static {p0}, LX/7PG;->b(LX/7PG;)V

    .line 1202676
    throw v0

    .line 1202677
    :cond_0
    return-void
.end method

.method private a([BII)V
    .locals 3

    .prologue
    .line 1202678
    iget-object v0, p0, LX/7PG;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/7PG;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 1202679
    :try_start_0
    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1202680
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1202681
    :catch_0
    move-exception v0

    .line 1202682
    invoke-static {p0}, LX/7PG;->b(LX/7PG;)V

    .line 1202683
    throw v0

    .line 1202684
    :cond_0
    return-void
.end method

.method private static b(LX/7PG;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1202685
    iget-object v0, p0, LX/7PG;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/7PG;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 1202686
    invoke-static {v0, v2}, LX/1md;->a(Ljava/io/Closeable;Z)V

    .line 1202687
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1202688
    :cond_0
    invoke-direct {p0}, LX/7PG;->a()V

    .line 1202689
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 1202690
    :try_start_0
    invoke-super {p0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1202691
    invoke-static {p0}, LX/7PG;->b(LX/7PG;)V

    .line 1202692
    return-void

    .line 1202693
    :catchall_0
    move-exception v0

    invoke-static {p0}, LX/7PG;->b(LX/7PG;)V

    throw v0
.end method

.method public final write(I)V
    .locals 0

    .prologue
    .line 1202694
    invoke-direct {p0, p1}, LX/7PG;->a(I)V

    .line 1202695
    return-void
.end method

.method public final write([BII)V
    .locals 0

    .prologue
    .line 1202696
    invoke-direct {p0, p1, p2, p3}, LX/7PG;->a([BII)V

    .line 1202697
    return-void
.end method
