.class public final LX/6w0;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<+",
        "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Pz;

.field public final synthetic b:LX/6zj;

.field public final synthetic c:Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;

.field public final synthetic d:LX/6w2;


# direct methods
.method public constructor <init>(LX/6w2;LX/0Pz;LX/6zj;Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;)V
    .locals 0

    .prologue
    .line 1157366
    iput-object p1, p0, LX/6w0;->d:LX/6w2;

    iput-object p2, p0, LX/6w0;->a:LX/0Pz;

    iput-object p3, p0, LX/6w0;->b:LX/6zj;

    iput-object p4, p0, LX/6w0;->c:Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1157367
    iget-object v0, p0, LX/6w0;->d:LX/6w2;

    iget-object v0, v0, LX/6w2;->a:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1157368
    iget-object v0, p0, LX/6w0;->d:LX/6w2;

    iget-object v0, v0, LX/6w2;->c:LX/70k;

    new-instance v1, LX/6vz;

    invoke-direct {v1, p0}, LX/6vz;-><init>(LX/6w0;)V

    invoke-virtual {v0, v1}, LX/70k;->a(LX/1DI;)V

    .line 1157369
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1157370
    check-cast p1, LX/0Px;

    .line 1157371
    iget-object v0, p0, LX/6w0;->a:LX/0Pz;

    invoke-virtual {v0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1157372
    iget-object v0, p0, LX/6w0;->d:LX/6w2;

    iget-object v0, v0, LX/6w2;->a:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1157373
    iget-object v0, p0, LX/6w0;->d:LX/6w2;

    iget-object v0, v0, LX/6w2;->c:LX/70k;

    invoke-virtual {v0}, LX/70k;->b()V

    .line 1157374
    iget-object v0, p0, LX/6w0;->b:LX/6zj;

    .line 1157375
    new-instance v1, LX/6vt;

    invoke-direct {v1}, LX/6vt;-><init>()V

    move-object v1, v1

    .line 1157376
    iget-object v2, p0, LX/6w0;->a:LX/0Pz;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1157377
    iput-object v2, v1, LX/6vt;->a:LX/0Px;

    .line 1157378
    move-object v1, v1

    .line 1157379
    new-instance v2, Lcom/facebook/payments/contactinfo/picker/ContactInfoCoreClientData;

    invoke-direct {v2, v1}, Lcom/facebook/payments/contactinfo/picker/ContactInfoCoreClientData;-><init>(LX/6vt;)V

    move-object v1, v2

    .line 1157380
    invoke-interface {v0, v1}, LX/6zj;->a(Lcom/facebook/payments/picker/model/CoreClientData;)V

    .line 1157381
    :cond_0
    return-void
.end method
