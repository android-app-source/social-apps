.class public final LX/6m4;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/6lz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/6mF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/6lv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/6m5;

.field private f:I

.field private g:Landroid/graphics/Rect;

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:LX/6m3;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:LX/6m6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1143278
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1143279
    invoke-direct {p0}, LX/6m4;->a()V

    .line 1143280
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1143275
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1143276
    invoke-direct {p0}, LX/6m4;->a()V

    .line 1143277
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1143272
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1143273
    invoke-direct {p0}, LX/6m4;->a()V

    .line 1143274
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1143263
    const-class v0, LX/6m4;

    invoke-static {v0, p0}, LX/6m4;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1143264
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/6m4;->g:Landroid/graphics/Rect;

    .line 1143265
    invoke-virtual {p0}, LX/6m4;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x40800000    # 4.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/6m4;->f:I

    .line 1143266
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_0

    .line 1143267
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/6m4;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1143268
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/6m4;->setClipChildren(Z)V

    .line 1143269
    invoke-direct {p0}, LX/6m4;->b()V

    .line 1143270
    invoke-direct {p0}, LX/6m4;->e()V

    .line 1143271
    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 1143252
    iget-object v0, p0, LX/6m4;->e:LX/6m5;

    invoke-virtual {v0}, LX/6m5;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1143253
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v2

    sub-int v2, p1, v1

    .line 1143254
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    div-int/2addr v1, v2

    .line 1143255
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    rem-int/2addr v3, v2

    if-lez v3, :cond_0

    .line 1143256
    add-int/lit8 v1, v1, 0x1

    .line 1143257
    :cond_0
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v0, v2

    sub-int v3, p1, v0

    .line 1143258
    div-int v0, v3, v2

    .line 1143259
    rem-int v2, v3, v2

    if-lez v2, :cond_1

    .line 1143260
    add-int/lit8 v0, v0, 0x1

    .line 1143261
    :cond_1
    iget-object v2, p0, LX/6m4;->e:LX/6m5;

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 1143262
    return-void
.end method

.method private static a(LX/6m4;LX/6lz;LX/0Zb;LX/6mF;LX/6lv;)V
    .locals 0

    .prologue
    .line 1143251
    iput-object p1, p0, LX/6m4;->a:LX/6lz;

    iput-object p2, p0, LX/6m4;->b:LX/0Zb;

    iput-object p3, p0, LX/6m4;->c:LX/6mF;

    iput-object p4, p0, LX/6m4;->d:LX/6lv;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/6m4;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, LX/6m4;

    new-instance p1, LX/6lz;

    const-class v0, Landroid/content/Context;

    invoke-interface {v3, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v3}, LX/0ja;->a(LX/0QB;)LX/0ja;

    move-result-object v1

    check-cast v1, LX/0ja;

    invoke-static {v3}, LX/6lj;->a(LX/0QB;)LX/6lj;

    move-result-object v2

    check-cast v2, LX/6lj;

    invoke-direct {p1, v0, v1, v2}, LX/6lz;-><init>(Landroid/content/Context;LX/0ja;LX/6lj;)V

    move-object v0, p1

    check-cast v0, LX/6lz;

    invoke-static {v3}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {v3}, LX/6mF;->b(LX/0QB;)LX/6mF;

    move-result-object v2

    check-cast v2, LX/6mF;

    invoke-static {v3}, LX/6lv;->a(LX/0QB;)LX/6lv;

    move-result-object v3

    check-cast v3, LX/6lv;

    invoke-static {p0, v0, v1, v2, v3}, LX/6m4;->a(LX/6m4;LX/6lz;LX/0Zb;LX/6mF;LX/6lv;)V

    return-void
.end method

.method public static a$redex0(LX/6m4;Ljava/lang/String;ILcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;)V
    .locals 4

    .prologue
    .line 1143241
    const/4 v0, 0x0

    .line 1143242
    invoke-virtual {p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->dj_()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->dj_()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1143243
    invoke-virtual {p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->dj_()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 1143244
    :cond_0
    iget-object v1, p0, LX/6m4;->k:LX/6m6;

    if-eqz v1, :cond_1

    invoke-virtual {p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->dj_()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1143245
    invoke-virtual {p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->dj_()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    .line 1143246
    :cond_1
    iget-object v1, p0, LX/6m4;->b:LX/0Zb;

    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "messenger_hscroll_impression"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "messenger_hscroll"

    .line 1143247
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1143248
    move-object v2, v2

    .line 1143249
    const-string v3, "xma_id"

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "page_position"

    invoke-virtual {v2, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "attachment_target_id"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1143250
    return-void
.end method

.method private b()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1143207
    new-instance v0, LX/6m5;

    invoke-virtual {p0}, LX/6m4;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6m5;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/6m4;->e:LX/6m5;

    .line 1143208
    iget-object v0, p0, LX/6m4;->e:LX/6m5;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, LX/6m5;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1143209
    iget-object v0, p0, LX/6m4;->e:LX/6m5;

    const/4 v1, 0x1

    invoke-virtual {v0, v4, v1}, Lcom/facebook/widget/CustomViewPager;->b(IZ)V

    .line 1143210
    iget-object v0, p0, LX/6m4;->e:LX/6m5;

    invoke-virtual {v0, v4}, LX/6m5;->setClipChildren(Z)V

    .line 1143211
    iget-object v0, p0, LX/6m4;->e:LX/6m5;

    iget v1, p0, LX/6m4;->f:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 1143212
    iget-object v0, p0, LX/6m4;->e:LX/6m5;

    invoke-virtual {p0, v0}, LX/6m4;->addView(Landroid/view/View;)V

    .line 1143213
    iget-object v0, p0, LX/6m4;->e:LX/6m5;

    new-instance v1, LX/6m0;

    invoke-direct {v1, p0}, LX/6m0;-><init>(LX/6m4;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1143214
    return-void
.end method

.method private e()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1143238
    iget-object v0, p0, LX/6m4;->c:LX/6mF;

    new-instance v1, LX/6m2;

    invoke-direct {v1, p0}, LX/6m2;-><init>(LX/6m4;)V

    .line 1143239
    iput-object v1, v0, LX/6mF;->b:LX/6m1;

    .line 1143240
    return-void
.end method

.method private getViewPagerVisibleRect()Landroid/graphics/Rect;
    .locals 7

    .prologue
    .line 1143231
    iget-object v0, p0, LX/6m4;->e:LX/6m5;

    invoke-virtual {v0}, LX/6m5;->getX()F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, LX/6m4;->e:LX/6m5;

    invoke-virtual {v1}, LX/6m5;->getScrollX()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1143232
    iget-object v1, p0, LX/6m4;->e:LX/6m5;

    invoke-virtual {v1}, LX/6m5;->getY()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, LX/6m4;->e:LX/6m5;

    invoke-virtual {v2}, LX/6m5;->getScrollY()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1143233
    iget-object v2, p0, LX/6m4;->a:LX/6lz;

    invoke-virtual {v2}, LX/0gG;->b()I

    move-result v2

    .line 1143234
    iget-object v3, p0, LX/6m4;->g:Landroid/graphics/Rect;

    iget-object v4, p0, LX/6m4;->e:LX/6m5;

    invoke-virtual {v4}, LX/6m5;->getWidth()I

    move-result v4

    mul-int/2addr v4, v2

    add-int/2addr v4, v0

    iget-object v5, p0, LX/6m4;->e:LX/6m5;

    .line 1143235
    iget v6, v5, Landroid/support/v4/view/ViewPager;->q:I

    move v5, v6

    .line 1143236
    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v2, v5

    add-int/2addr v2, v4

    iget-object v4, p0, LX/6m4;->e:LX/6m5;

    invoke-virtual {v4}, LX/6m5;->getHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v3, v0, v1, v2, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1143237
    iget-object v0, p0, LX/6m4;->g:Landroid/graphics/Rect;

    return-object v0
.end method


# virtual methods
.method public final getAdapter()LX/6lz;
    .locals 1

    .prologue
    .line 1143230
    iget-object v0, p0, LX/6m4;->a:LX/6lz;

    return-object v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1143229
    iget-object v0, p0, LX/6m4;->c:LX/6mF;

    invoke-virtual {v0, p1}, LX/6mF;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 1143226
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-direct {p0, v0}, LX/6m4;->a(I)V

    .line 1143227
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1143228
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0xb45db1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1143217
    iget-object v0, p0, LX/6m4;->c:LX/6mF;

    invoke-virtual {v0, p1}, LX/6mF;->b(Landroid/view/MotionEvent;)V

    .line 1143218
    invoke-direct {p0}, LX/6m4;->getViewPagerVisibleRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 1143219
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1143220
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x3204c04a

    invoke-static {v4, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1143221
    :goto_0
    return v0

    .line 1143222
    :cond_0
    iget-object v0, p0, LX/6m4;->e:LX/6m5;

    invoke-virtual {v0}, LX/6m5;->getX()F

    move-result v0

    neg-float v0, v0

    iget-object v2, p0, LX/6m4;->e:LX/6m5;

    invoke-virtual {v2}, LX/6m5;->getY()F

    move-result v2

    neg-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 1143223
    iget-object v0, p0, LX/6m4;->e:LX/6m5;

    invoke-virtual {v0, p1}, LX/6m5;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1143224
    iget-object v2, p0, LX/6m4;->e:LX/6m5;

    invoke-virtual {v2}, LX/6m5;->getX()F

    move-result v2

    iget-object v3, p0, LX/6m4;->e:LX/6m5;

    invoke-virtual {v3}, LX/6m5;->getY()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 1143225
    const v2, 0x6491f7f1

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public final setOnPageScrolledListener(LX/6m3;)V
    .locals 0

    .prologue
    .line 1143215
    iput-object p1, p0, LX/6m4;->j:LX/6m3;

    .line 1143216
    return-void
.end method
