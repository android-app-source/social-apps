.class public interface abstract LX/6qb;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(LX/0Px;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Landroid/os/Parcelable;)V
.end method

.method public abstract a(Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V
.end method

.method public abstract a(Lcom/facebook/payments/model/PaymentsPin;)V
.end method

.method public abstract a(Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)V
.end method

.method public abstract b(LX/0Px;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract c(LX/0Px;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;)V"
        }
    .end annotation
.end method
