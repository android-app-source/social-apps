.class public LX/7Gg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/7Gg;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1189680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189681
    return-void
.end method

.method public static a(JJ)LX/7Gf;
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 1189682
    add-long v0, p2, v2

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 1189683
    sget-object v0, LX/7Gf;->EXPECTED:LX/7Gf;

    .line 1189684
    :goto_0
    return-object v0

    .line 1189685
    :cond_0
    add-long v0, p2, v2

    cmp-long v0, p0, v0

    if-gez v0, :cond_1

    .line 1189686
    sget-object v0, LX/7Gf;->ALREADY_PROCESSED:LX/7Gf;

    goto :goto_0

    .line 1189687
    :cond_1
    sget-object v0, LX/7Gf;->AHEAD:LX/7Gf;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/7Gg;
    .locals 3

    .prologue
    .line 1189688
    sget-object v0, LX/7Gg;->a:LX/7Gg;

    if-nez v0, :cond_1

    .line 1189689
    const-class v1, LX/7Gg;

    monitor-enter v1

    .line 1189690
    :try_start_0
    sget-object v0, LX/7Gg;->a:LX/7Gg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1189691
    if-eqz v2, :cond_0

    .line 1189692
    :try_start_1
    new-instance v0, LX/7Gg;

    invoke-direct {v0}, LX/7Gg;-><init>()V

    .line 1189693
    move-object v0, v0

    .line 1189694
    sput-object v0, LX/7Gg;->a:LX/7Gg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1189695
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1189696
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1189697
    :cond_1
    sget-object v0, LX/7Gg;->a:LX/7Gg;

    return-object v0

    .line 1189698
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1189699
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
