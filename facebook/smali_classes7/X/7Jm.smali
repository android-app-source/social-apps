.class public abstract LX/7Jm;
.super Ljava/io/OutputStream;
.source ""


# instance fields
.field private final a:Ljava/io/RandomAccessFile;

.field private final b:I

.field private c:I

.field public d:Z


# direct methods
.method public constructor <init>(Ljava/io/RandomAccessFile;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1194088
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 1194089
    iput-object p1, p0, LX/7Jm;->a:Ljava/io/RandomAccessFile;

    .line 1194090
    iput p2, p0, LX/7Jm;->b:I

    .line 1194091
    iput v0, p0, LX/7Jm;->c:I

    .line 1194092
    iput-boolean v0, p0, LX/7Jm;->d:Z

    .line 1194093
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final write(I)V
    .locals 1

    .prologue
    .line 1194094
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public write([BII)V
    .locals 2

    .prologue
    .line 1194095
    iget-object v0, p0, LX/7Jm;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 1194096
    iget v0, p0, LX/7Jm;->c:I

    add-int/2addr v0, p3

    iput v0, p0, LX/7Jm;->c:I

    .line 1194097
    iget v0, p0, LX/7Jm;->c:I

    iget v1, p0, LX/7Jm;->b:I

    if-le v0, v1, :cond_0

    iget-boolean v0, p0, LX/7Jm;->d:Z

    if-nez v0, :cond_0

    .line 1194098
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7Jm;->d:Z

    .line 1194099
    invoke-virtual {p0}, LX/7Jm;->a()V

    .line 1194100
    :cond_0
    return-void
.end method
