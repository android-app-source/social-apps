.class public final LX/72U;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/72g;",
            "LX/72Q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/72Q;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1164623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164624
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1164625
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/72Q;

    .line 1164626
    iget-object v3, v0, LX/72Q;->a:LX/72g;

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1164627
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/72U;->a:LX/0P1;

    .line 1164628
    return-void
.end method

.method public static a(LX/0QB;)LX/72U;
    .locals 6

    .prologue
    .line 1164629
    const-class v1, LX/72U;

    monitor-enter v1

    .line 1164630
    :try_start_0
    sget-object v0, LX/72U;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1164631
    sput-object v2, LX/72U;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1164632
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1164633
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1164634
    new-instance v3, LX/72U;

    .line 1164635
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/72A;

    invoke-direct {p0, v0}, LX/72A;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 1164636
    invoke-direct {v3, v4}, LX/72U;-><init>(Ljava/util/Set;)V

    .line 1164637
    move-object v0, v3

    .line 1164638
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1164639
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/72U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1164640
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1164641
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/72g;)LX/72T;
    .locals 2

    .prologue
    .line 1164642
    iget-object v0, p0, LX/72U;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1164643
    iget-object v0, p0, LX/72U;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/72Q;

    iget-object v0, v0, LX/72Q;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/72T;

    .line 1164644
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/72U;->a:LX/0P1;

    sget-object v1, LX/72g;->SIMPLE:LX/72g;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/72Q;

    iget-object v0, v0, LX/72Q;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/72T;

    goto :goto_0
.end method
