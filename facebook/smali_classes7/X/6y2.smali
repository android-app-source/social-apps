.class public final LX/6y2;
.super LX/6qh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;)V
    .locals 0

    .prologue
    .line 1159342
    iput-object p1, p0, LX/6y2;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    invoke-direct {p0}, LX/6qh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/73T;)V
    .locals 3

    .prologue
    .line 1159312
    iget-object v0, p0, LX/6y2;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    const/4 p0, -0x1

    .line 1159313
    sget-object v1, LX/6y7;->a:[I

    .line 1159314
    iget-object v2, p1, LX/73T;->a:LX/73S;

    move-object v2, v2

    .line 1159315
    invoke-virtual {v2}, LX/73S;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1159316
    :cond_0
    :goto_0
    return-void

    .line 1159317
    :pswitch_0
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    .line 1159318
    if-eqz v2, :cond_0

    .line 1159319
    const-string v1, "extra_activity_result_data"

    invoke-virtual {p1, v1}, LX/73T;->a(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 1159320
    if-eqz v1, :cond_1

    .line 1159321
    invoke-virtual {v2, p0, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1159322
    :goto_1
    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 1159323
    :cond_1
    invoke-virtual {v2, p0}, Landroid/app/Activity;->setResult(I)V

    goto :goto_1

    .line 1159324
    :pswitch_1
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->r:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    .line 1159325
    iget-object v2, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v2}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1159326
    :goto_2
    goto :goto_0

    .line 1159327
    :pswitch_2
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    if-eqz v1, :cond_0

    .line 1159328
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->e()V

    goto :goto_0

    .line 1159329
    :cond_2
    iget-object v2, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->h:LX/6yL;

    iget-object p0, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v2, p0, p1}, LX/6yL;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/73T;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1159330
    iget-object v2, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->g:LX/6y1;

    invoke-virtual {v2}, LX/6y1;->a()V

    .line 1159331
    iget-object v2, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance p0, LX/6yM;

    invoke-direct {p0, v1}, LX/6yM;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;)V

    iget-object v0, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->b:Ljava/util/concurrent/Executor;

    invoke-static {v2, p0, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V
    .locals 2

    .prologue
    .line 1159343
    iget-object v0, p0, LX/6y2;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "payments_component_dialog_fragment"

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1159344
    return-void
.end method

.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;Z)V
    .locals 3

    .prologue
    .line 1159334
    iget-object v0, p0, LX/6y2;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    .line 1159335
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 1159336
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1159337
    :cond_0
    iput-object p1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1159338
    if-nez p2, :cond_1

    .line 1159339
    :goto_0
    return-void

    .line 1159340
    :cond_1
    invoke-static {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p(Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;)V

    .line 1159341
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/6y6;

    invoke-direct {v2, v0}, LX/6y6;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;)V

    iget-object p0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->e:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1159332
    iget-object v0, p0, LX/6y2;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/6y2;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1159333
    return-void
.end method
