.class public LX/7g4;
.super LX/7g2;
.source ""

# interfaces
.implements LX/7g3;


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/7g1;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/content/Context;

.field private f:LX/0gc;

.field private g:I

.field private h:LX/7g3;

.field private i:LX/7g1;

.field private j:Z

.field private final k:LX/7fz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1223914
    const-class v0, LX/7g4;

    sput-object v0, LX/7g4;->c:Ljava/lang/Class;

    return-void
.end method

.method private a(Ljava/lang/String;LX/0hH;)LX/0hH;
    .locals 4

    .prologue
    .line 1223884
    const/4 v1, 0x0

    .line 1223885
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, LX/7g4;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1223886
    iget-object v0, p0, LX/7g4;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7g1;

    .line 1223887
    iget-object v3, v0, LX/7g1;->a:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1223888
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 1223889
    :cond_0
    if-nez v1, :cond_1

    .line 1223890
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No tab known for tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1223891
    :cond_1
    iget-object v0, p0, LX/7g4;->i:LX/7g1;

    if-eq v0, v1, :cond_5

    .line 1223892
    if-nez p2, :cond_2

    .line 1223893
    iget-object v0, p0, LX/7g4;->f:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object p2

    .line 1223894
    :cond_2
    iget-object v0, p0, LX/7g4;->i:LX/7g1;

    if-eqz v0, :cond_3

    .line 1223895
    iget-object v0, p0, LX/7g4;->i:LX/7g1;

    iget-object v0, v0, LX/7g1;->d:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_3

    .line 1223896
    iget-object v0, p0, LX/7g4;->k:LX/7fz;

    sget-object v2, LX/7fz;->HIDE_ON_SWITCH:LX/7fz;

    if-ne v0, v2, :cond_6

    .line 1223897
    iget-object v0, p0, LX/7g4;->i:LX/7g1;

    iget-object v0, v0, LX/7g1;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {p2, v0}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1223898
    :cond_3
    :goto_2
    if-eqz v1, :cond_4

    .line 1223899
    iget-object v0, v1, LX/7g1;->d:Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_7

    .line 1223900
    iget-object v0, p0, LX/7g4;->e:Landroid/content/Context;

    iget-object v2, v1, LX/7g1;->b:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, LX/7g1;->c:Landroid/os/Bundle;

    invoke-static {v0, v2, v3}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1223901
    iput-object v0, v1, LX/7g1;->d:Landroid/support/v4/app/Fragment;

    .line 1223902
    iget v0, p0, LX/7g4;->g:I

    iget-object v2, v1, LX/7g1;->d:Landroid/support/v4/app/Fragment;

    iget-object v3, v1, LX/7g1;->a:Ljava/lang/String;

    invoke-virtual {p2, v0, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 1223903
    :cond_4
    :goto_3
    iput-object v1, p0, LX/7g4;->i:LX/7g1;

    .line 1223904
    :cond_5
    return-object p2

    .line 1223905
    :cond_6
    iget-object v0, p0, LX/7g4;->i:LX/7g1;

    iget-object v0, v0, LX/7g1;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {p2, v0}, LX/0hH;->d(Landroid/support/v4/app/Fragment;)LX/0hH;

    goto :goto_2

    .line 1223906
    :cond_7
    iget-object v0, v1, LX/7g1;->d:Landroid/support/v4/app/Fragment;

    .line 1223907
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mDetached:Z

    move v0, v2

    .line 1223908
    if-eqz v0, :cond_8

    .line 1223909
    iget-object v0, v1, LX/7g1;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {p2, v0}, LX/0hH;->e(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1223910
    iget-object v0, p0, LX/7g4;->k:LX/7fz;

    sget-object v2, LX/7fz;->HIDE_ON_SWITCH:LX/7fz;

    if-ne v0, v2, :cond_4

    .line 1223911
    sget-object v0, LX/7g4;->c:Ljava/lang/Class;

    const-string v2, "Fragment was detached during HIDE_ON_SWITCH strategy"

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_3

    .line 1223912
    :cond_8
    iget-object v0, p0, LX/7g4;->k:LX/7fz;

    sget-object v2, LX/7fz;->HIDE_ON_SWITCH:LX/7fz;

    if-ne v0, v2, :cond_4

    .line 1223913
    iget-object v0, v1, LX/7g1;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {p2, v0}, LX/0hH;->c(Landroid/support/v4/app/Fragment;)LX/0hH;

    goto :goto_3

    :cond_9
    move-object v0, v1

    goto/16 :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1223960
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call setup() that takes a Context and FragmentManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1223953
    iget-boolean v0, p0, LX/7g4;->j:Z

    if-eqz v0, :cond_0

    .line 1223954
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7g4;->a(Ljava/lang/String;LX/0hH;)LX/0hH;

    move-result-object v0

    .line 1223955
    if-eqz v0, :cond_0

    .line 1223956
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1223957
    :cond_0
    iget-object v0, p0, LX/7g4;->h:LX/7g3;

    if-eqz v0, :cond_1

    .line 1223958
    iget-object v0, p0, LX/7g4;->h:LX/7g3;

    invoke-interface {v0, p1}, LX/7g3;->a(Ljava/lang/String;)V

    .line 1223959
    :cond_1
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0xe5673b8

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1223931
    invoke-super {p0}, LX/7g2;->onAttachedToWindow()V

    .line 1223932
    invoke-virtual {p0}, LX/7g2;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v4

    .line 1223933
    const/4 v1, 0x0

    .line 1223934
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, LX/7g4;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1223935
    iget-object v0, p0, LX/7g4;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7g1;

    .line 1223936
    iget-object v5, p0, LX/7g4;->f:LX/0gc;

    iget-object v6, v0, LX/7g1;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    .line 1223937
    iput-object v5, v0, LX/7g1;->d:Landroid/support/v4/app/Fragment;

    .line 1223938
    iget-object v5, v0, LX/7g1;->d:Landroid/support/v4/app/Fragment;

    if-eqz v5, :cond_0

    iget-object v5, v0, LX/7g1;->d:Landroid/support/v4/app/Fragment;

    .line 1223939
    iget-boolean v6, v5, Landroid/support/v4/app/Fragment;->mDetached:Z

    move v5, v6

    .line 1223940
    if-nez v5, :cond_0

    .line 1223941
    iget-object v5, v0, LX/7g1;->a:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1223942
    iput-object v0, p0, LX/7g4;->i:LX/7g1;

    .line 1223943
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1223944
    :cond_1
    if-nez v1, :cond_2

    .line 1223945
    iget-object v1, p0, LX/7g4;->f:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1223946
    :cond_2
    iget-object v0, v0, LX/7g1;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v0}, LX/0hH;->d(Landroid/support/v4/app/Fragment;)LX/0hH;

    goto :goto_1

    .line 1223947
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7g4;->j:Z

    .line 1223948
    invoke-direct {p0, v4, v1}, LX/7g4;->a(Ljava/lang/String;LX/0hH;)LX/0hH;

    move-result-object v0

    .line 1223949
    if-eqz v0, :cond_4

    .line 1223950
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1223951
    iget-object v0, p0, LX/7g4;->f:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1223952
    :cond_4
    const v0, -0x35ee5960    # -2386344.0f

    invoke-static {v0, v3}, LX/02F;->g(II)V

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0xdf5e8af

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1223961
    invoke-super {p0}, LX/7g2;->onDetachedFromWindow()V

    .line 1223962
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/7g4;->j:Z

    .line 1223963
    const/16 v1, 0x2d

    const v2, 0x58ede9c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    .line 1223921
    check-cast p1, Lcom/facebook/apptab/ui/SimpleCustomFragmentTabHost$SavedState;

    .line 1223922
    invoke-virtual {p1}, Lcom/facebook/apptab/ui/SimpleCustomFragmentTabHost$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, LX/7g2;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1223923
    iget-object v0, p1, Lcom/facebook/apptab/ui/SimpleCustomFragmentTabHost$SavedState;->a:Ljava/lang/String;

    .line 1223924
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    iget-object v1, p0, LX/7g2;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 1223925
    iget-object v1, p0, LX/7g2;->e:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7g8;

    .line 1223926
    iget-object p1, v1, LX/7g8;->a:Ljava/lang/String;

    move-object v1, p1

    .line 1223927
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1223928
    invoke-virtual {p0, v2}, LX/7g2;->setCurrentTab(I)V

    .line 1223929
    :cond_0
    return-void

    .line 1223930
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1223917
    invoke-super {p0}, LX/7g2;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1223918
    new-instance v1, Lcom/facebook/apptab/ui/SimpleCustomFragmentTabHost$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/apptab/ui/SimpleCustomFragmentTabHost$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1223919
    invoke-virtual {p0}, LX/7g2;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/facebook/apptab/ui/SimpleCustomFragmentTabHost$SavedState;->a:Ljava/lang/String;

    .line 1223920
    return-object v1
.end method

.method public setOnTabChangedListener(LX/7g3;)V
    .locals 0

    .prologue
    .line 1223915
    iput-object p1, p0, LX/7g4;->h:LX/7g3;

    .line 1223916
    return-void
.end method
