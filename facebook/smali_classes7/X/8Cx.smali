.class public LX/8Cx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field private b:Landroid/view/View;

.field private c:Landroid/support/v4/app/Fragment;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;II)V
    .locals 1

    .prologue
    .line 1312096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1312097
    const/4 v0, 0x0

    iput-object v0, p0, LX/8Cx;->b:Landroid/view/View;

    .line 1312098
    iput-object p1, p0, LX/8Cx;->c:Landroid/support/v4/app/Fragment;

    .line 1312099
    iput p2, p0, LX/8Cx;->d:I

    .line 1312100
    iput p3, p0, LX/8Cx;->a:I

    .line 1312101
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 2

    .prologue
    .line 1312102
    iget-object v0, p0, LX/8Cx;->b:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1312103
    iget-object v0, p0, LX/8Cx;->c:Landroid/support/v4/app/Fragment;

    .line 1312104
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 1312105
    iget v1, p0, LX/8Cx;->d:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/8Cx;->b:Landroid/view/View;

    .line 1312106
    :cond_0
    iget-object v0, p0, LX/8Cx;->b:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewStub;

    if-eqz v0, :cond_1

    .line 1312107
    iget-object v0, p0, LX/8Cx;->b:Landroid/view/View;

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/8Cx;->b:Landroid/view/View;

    .line 1312108
    :cond_1
    iget-object v0, p0, LX/8Cx;->b:Landroid/view/View;

    return-object v0
.end method
