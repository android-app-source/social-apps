.class public LX/7zk;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:LX/7zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7zi",
            "<+",
            "Lcom/facebook/components/ComponentView;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/3nq;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/7zi;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/7zi",
            "<+",
            "Lcom/facebook/components/ComponentView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1281305
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1281306
    iput-object p2, p0, LX/7zk;->a:LX/7zi;

    .line 1281307
    new-instance p1, LX/7zj;

    invoke-direct {p1, p0}, LX/7zj;-><init>(LX/7zk;)V

    invoke-virtual {p0, p1}, LX/7zk;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1281308
    return-void
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 1281309
    iget-object v0, p0, LX/7zk;->b:LX/3nq;

    if-eqz v0, :cond_0

    .line 1281310
    iget-object v0, p0, LX/7zk;->b:LX/3nq;

    invoke-virtual {v0}, LX/3nq;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1281311
    invoke-virtual {p0}, LX/7zk;->getPaddingBottom()I

    move-result v1

    sub-int v1, p2, v1

    invoke-virtual {p0}, LX/7zk;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1281312
    invoke-virtual {p0}, LX/7zk;->getPaddingLeft()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {p0}, LX/7zk;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1281313
    iget-object v1, p0, LX/7zk;->b:LX/3nq;

    invoke-virtual {v1, v0}, LX/3nq;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1281314
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/3nq;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1281315
    iput-object p1, p0, LX/7zk;->b:LX/3nq;

    .line 1281316
    invoke-virtual {p0}, LX/7zk;->getWidth()I

    move-result v0

    invoke-virtual {p0}, LX/7zk;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, LX/7zk;->a(II)V

    .line 1281317
    return-void
.end method

.method public getAssignedFloatingWrapper()LX/3nq;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1281318
    iget-object v0, p0, LX/7zk;->b:LX/3nq;

    return-object v0
.end method

.method public getComponentViewSupplier()LX/7zi;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/7zi",
            "<+",
            "Lcom/facebook/components/ComponentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1281319
    iget-object v0, p0, LX/7zk;->a:LX/7zi;

    return-object v0
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x541ed55c    # -1.5999047E-12f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1281320
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 1281321
    invoke-direct {p0, p1, p2}, LX/7zk;->a(II)V

    .line 1281322
    const/16 v1, 0x2d

    const v2, -0x4caa1889

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
