.class public final LX/7ip;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1228706
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 1228707
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1228708
    :goto_0
    return v1

    .line 1228709
    :cond_0
    const-string v10, "is_user_subscribed_to_merchant"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1228710
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v7, v4

    move v4, v2

    .line 1228711
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_5

    .line 1228712
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1228713
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1228714
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 1228715
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1228716
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1228717
    :cond_2
    const-string v10, "payment_provider"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1228718
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v2

    goto :goto_1

    .line 1228719
    :cond_3
    const-string v10, "show_edit_interface"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1228720
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1228721
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1228722
    :cond_5
    const/4 v9, 0x4

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1228723
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1228724
    if-eqz v4, :cond_6

    .line 1228725
    invoke-virtual {p1, v2, v7}, LX/186;->a(IZ)V

    .line 1228726
    :cond_6
    if-eqz v3, :cond_7

    .line 1228727
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v6, v1}, LX/186;->a(III)V

    .line 1228728
    :cond_7
    if-eqz v0, :cond_8

    .line 1228729
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1228730
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1228731
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1228732
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1228733
    if-eqz v0, :cond_0

    .line 1228734
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228735
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1228736
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1228737
    if-eqz v0, :cond_1

    .line 1228738
    const-string v1, "is_user_subscribed_to_merchant"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228739
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1228740
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1228741
    if-eqz v0, :cond_2

    .line 1228742
    const-string v1, "payment_provider"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228743
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1228744
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1228745
    if-eqz v0, :cond_3

    .line 1228746
    const-string v1, "show_edit_interface"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228747
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1228748
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1228749
    return-void
.end method
