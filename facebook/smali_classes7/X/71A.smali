.class public LX/71A;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

.field public b:LX/71C;

.field public c:LX/6xg;

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

.field public f:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

.field public g:Lcom/facebook/payments/picker/model/ProductParcelableConfig;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1163127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163128
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->newBuilder()LX/71E;

    move-result-object v0

    invoke-virtual {v0}, LX/71E;->c()Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    move-result-object v0

    iput-object v0, p0, LX/71A;->e:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    .line 1163129
    new-instance v0, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;-><init>(Z)V

    iput-object v0, p0, LX/71A;->f:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    return-void
.end method


# virtual methods
.method public final a(LX/6xg;)LX/71A;
    .locals 0

    .prologue
    .line 1163130
    iput-object p1, p0, LX/71A;->c:LX/6xg;

    .line 1163131
    return-object p0
.end method

.method public final a(LX/71C;)LX/71A;
    .locals 0

    .prologue
    .line 1163132
    iput-object p1, p0, LX/71A;->b:LX/71C;

    .line 1163133
    return-object p0
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;)LX/71A;
    .locals 0

    .prologue
    .line 1163134
    iput-object p1, p0, LX/71A;->a:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 1163135
    return-object p0
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;)LX/71A;
    .locals 0

    .prologue
    .line 1163136
    iput-object p1, p0, LX/71A;->f:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    .line 1163137
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/71A;
    .locals 0

    .prologue
    .line 1163138
    iput-object p1, p0, LX/71A;->d:Ljava/lang/String;

    .line 1163139
    return-object p0
.end method

.method public final h()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;
    .locals 1

    .prologue
    .line 1163140
    new-instance v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    invoke-direct {v0, p0}, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;-><init>(LX/71A;)V

    return-object v0
.end method
