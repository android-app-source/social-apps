.class public final LX/6gd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1125831
    const/4 v13, 0x0

    .line 1125832
    const/4 v12, 0x0

    .line 1125833
    const/4 v11, 0x0

    .line 1125834
    const/4 v10, 0x0

    .line 1125835
    const/4 v9, 0x0

    .line 1125836
    const/4 v8, 0x0

    .line 1125837
    const/4 v7, 0x0

    .line 1125838
    const/4 v6, 0x0

    .line 1125839
    const/4 v5, 0x0

    .line 1125840
    const/4 v4, 0x0

    .line 1125841
    const/4 v3, 0x0

    .line 1125842
    const/4 v2, 0x0

    .line 1125843
    const/4 v1, 0x0

    .line 1125844
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 1125845
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1125846
    const/4 v1, 0x0

    .line 1125847
    :goto_0
    return v1

    .line 1125848
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1125849
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_9

    .line 1125850
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 1125851
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1125852
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 1125853
    const-string v15, "animation_type"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 1125854
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    goto :goto_1

    .line 1125855
    :cond_2
    const-string v15, "asset_image"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1125856
    invoke-static/range {p0 .. p1}, LX/6gc;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1125857
    :cond_3
    const-string v15, "col_of_sprites"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1125858
    const/4 v5, 0x1

    .line 1125859
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    goto :goto_1

    .line 1125860
    :cond_4
    const-string v15, "frame_rate"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1125861
    const/4 v4, 0x1

    .line 1125862
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    goto :goto_1

    .line 1125863
    :cond_5
    const-string v15, "id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1125864
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1125865
    :cond_6
    const-string v15, "num_of_sprites"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 1125866
    const/4 v3, 0x1

    .line 1125867
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    goto :goto_1

    .line 1125868
    :cond_7
    const-string v15, "play_count"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 1125869
    const/4 v2, 0x1

    .line 1125870
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    goto/16 :goto_1

    .line 1125871
    :cond_8
    const-string v15, "row_of_sprites"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1125872
    const/4 v1, 0x1

    .line 1125873
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    goto/16 :goto_1

    .line 1125874
    :cond_9
    const/16 v14, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 1125875
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1125876
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1125877
    if-eqz v5, :cond_a

    .line 1125878
    const/4 v5, 0x2

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11, v12}, LX/186;->a(III)V

    .line 1125879
    :cond_a
    if-eqz v4, :cond_b

    .line 1125880
    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10, v5}, LX/186;->a(III)V

    .line 1125881
    :cond_b
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 1125882
    if-eqz v3, :cond_c

    .line 1125883
    const/4 v3, 0x5

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8, v4}, LX/186;->a(III)V

    .line 1125884
    :cond_c
    if-eqz v2, :cond_d

    .line 1125885
    const/4 v2, 0x6

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v3}, LX/186;->a(III)V

    .line 1125886
    :cond_d
    if-eqz v1, :cond_e

    .line 1125887
    const/4 v1, 0x7

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6, v2}, LX/186;->a(III)V

    .line 1125888
    :cond_e
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1125889
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1125890
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1125891
    if-eqz v0, :cond_0

    .line 1125892
    const-string v0, "animation_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125893
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1125894
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125895
    if-eqz v0, :cond_1

    .line 1125896
    const-string v1, "asset_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125897
    invoke-static {p0, v0, p2}, LX/6gc;->a(LX/15i;ILX/0nX;)V

    .line 1125898
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1125899
    if-eqz v0, :cond_2

    .line 1125900
    const-string v1, "col_of_sprites"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125901
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1125902
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1125903
    if-eqz v0, :cond_3

    .line 1125904
    const-string v1, "frame_rate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125905
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1125906
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1125907
    if-eqz v0, :cond_4

    .line 1125908
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125909
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1125910
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1125911
    if-eqz v0, :cond_5

    .line 1125912
    const-string v1, "num_of_sprites"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125913
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1125914
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1125915
    if-eqz v0, :cond_6

    .line 1125916
    const-string v1, "play_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125917
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1125918
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1125919
    if-eqz v0, :cond_7

    .line 1125920
    const-string v1, "row_of_sprites"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125921
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1125922
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1125923
    return-void
.end method
