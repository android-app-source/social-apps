.class public LX/7TM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7TL;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private final a:LX/616;

.field private final b:LX/5Pc;

.field private c:LX/617;

.field private d:LX/617;

.field private e:LX/7T0;

.field private f:LX/7Sx;

.field private g:Z


# direct methods
.method public constructor <init>(LX/616;LX/5Pc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1210420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1210421
    iput-object p1, p0, LX/7TM;->a:LX/616;

    .line 1210422
    iput-object p2, p0, LX/7TM;->b:LX/5Pc;

    .line 1210423
    return-void
.end method


# virtual methods
.method public final a(J)LX/614;
    .locals 1

    .prologue
    .line 1210419
    iget-object v0, p0, LX/7TM;->d:LX/617;

    invoke-virtual {v0, p1, p2}, LX/617;->a(J)LX/614;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/614;)V
    .locals 1

    .prologue
    .line 1210417
    iget-object v0, p0, LX/7TM;->d:LX/617;

    invoke-virtual {v0, p1}, LX/617;->a(LX/614;)V

    .line 1210418
    return-void
.end method

.method public final a(LX/7Sx;)V
    .locals 5

    .prologue
    .line 1210388
    new-instance v0, LX/618;

    sget-object v1, LX/612;->CODEC_VIDEO_H264:LX/612;

    iget v2, p1, LX/7Sx;->d:I

    iget v3, p1, LX/7Sx;->e:I

    const v4, 0x7f000789

    invoke-direct {v0, v1, v2, v3, v4}, LX/618;-><init>(LX/612;III)V

    iget v1, p1, LX/7Sx;->j:I

    .line 1210389
    iput v1, v0, LX/618;->e:I

    .line 1210390
    move-object v0, v0

    .line 1210391
    iget v1, p1, LX/7Sx;->o:I

    .line 1210392
    iput v1, v0, LX/618;->g:I

    .line 1210393
    move-object v0, v0

    .line 1210394
    iget v1, p1, LX/7Sx;->n:I

    .line 1210395
    iput v1, v0, LX/618;->f:I

    .line 1210396
    move-object v0, v0

    .line 1210397
    iget-object v1, p1, LX/7Sx;->p:LX/7Sy;

    if-eqz v1, :cond_0

    .line 1210398
    iget-object v1, p1, LX/7Sx;->p:LX/7Sy;

    iget v1, v1, LX/7Sy;->a:I

    iget-object v2, p1, LX/7Sx;->p:LX/7Sy;

    iget v2, v2, LX/7Sy;->b:I

    .line 1210399
    iput v1, v0, LX/618;->i:I

    .line 1210400
    iput v2, v0, LX/618;->j:I

    .line 1210401
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/618;->h:Z

    .line 1210402
    move-object v0, v0

    .line 1210403
    :cond_0
    iget v1, p1, LX/7Sx;->r:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    .line 1210404
    iget v1, p1, LX/7Sx;->r:I

    .line 1210405
    iput v1, v0, LX/618;->k:I

    .line 1210406
    move-object v0, v0

    .line 1210407
    :cond_1
    invoke-virtual {v0}, LX/618;->a()Landroid/media/MediaFormat;

    move-result-object v0

    .line 1210408
    sget-object v2, LX/612;->CODEC_VIDEO_H264:LX/612;

    iget-object v2, v2, LX/612;->value:Ljava/lang/String;

    sget-object v3, LX/610;->SURFACE:LX/610;

    invoke-static {v2, v0, v3}, LX/616;->a(Ljava/lang/String;Landroid/media/MediaFormat;LX/610;)LX/617;

    move-result-object v0

    iput-object v0, p0, LX/7TM;->c:LX/617;

    .line 1210409
    iget-object v0, p0, LX/7TM;->c:LX/617;

    invoke-virtual {v0}, LX/617;->a()V

    .line 1210410
    new-instance v0, LX/7T0;

    iget-object v1, p0, LX/7TM;->b:LX/5Pc;

    iget-object v2, p0, LX/7TM;->c:LX/617;

    .line 1210411
    iget-object v3, v2, LX/617;->a:LX/611;

    sget-object v4, LX/611;->ENCODER:LX/611;

    if-ne v3, v4, :cond_2

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1210412
    iget-object v3, v2, LX/617;->c:Landroid/view/Surface;

    move-object v2, v3

    .line 1210413
    invoke-direct {v0, v1, v2, p1}, LX/7T0;-><init>(LX/5Pc;Landroid/view/Surface;LX/7Sx;)V

    iput-object v0, p0, LX/7TM;->e:LX/7T0;

    .line 1210414
    iput-object p1, p0, LX/7TM;->f:LX/7Sx;

    .line 1210415
    return-void

    .line 1210416
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/media/MediaFormat;)V
    .locals 3

    .prologue
    .line 1210383
    const-string v1, "mime"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/7TM;->e:LX/7T0;

    .line 1210384
    iget-object v0, v2, LX/7T0;->e:Landroid/view/Surface;

    move-object v2, v0

    .line 1210385
    invoke-static {v1, p1, v2}, LX/616;->a(Ljava/lang/String;Landroid/media/MediaFormat;Landroid/view/Surface;)LX/617;

    move-result-object v0

    iput-object v0, p0, LX/7TM;->d:LX/617;

    .line 1210386
    iget-object v0, p0, LX/7TM;->d:LX/617;

    invoke-virtual {v0}, LX/617;->a()V

    .line 1210387
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1210382
    iget-boolean v0, p0, LX/7TM;->g:Z

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1210380
    invoke-virtual {p0}, LX/7TM;->c()V

    .line 1210381
    return-void
.end method

.method public final b(J)V
    .locals 7

    .prologue
    .line 1210330
    iget-object v0, p0, LX/7TM;->d:LX/617;

    invoke-virtual {v0, p1, p2}, LX/617;->b(J)LX/614;

    move-result-object v0

    .line 1210331
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/614;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1210332
    invoke-virtual {v0}, LX/614;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v1

    .line 1210333
    iget-object v2, p0, LX/7TM;->d:LX/617;

    invoke-virtual {v2, v0}, LX/617;->b(LX/614;)V

    .line 1210334
    iget v0, v1, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 1210335
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7TM;->g:Z

    .line 1210336
    iget-object v0, p0, LX/7TM;->c:LX/617;

    .line 1210337
    iget-object v1, v0, LX/617;->a:LX/611;

    sget-object v2, LX/611;->ENCODER:LX/611;

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1210338
    iget-object v1, v0, LX/617;->b:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->signalEndOfInputStream()V

    .line 1210339
    :cond_0
    :goto_1
    return-void

    .line 1210340
    :cond_1
    iget-object v0, p0, LX/7TM;->e:LX/7T0;

    .line 1210341
    iget-object v2, v0, LX/7T0;->j:LX/7T1;

    invoke-virtual {v2}, LX/7T1;->a()V

    .line 1210342
    iget-object v0, p0, LX/7TM;->e:LX/7T0;

    .line 1210343
    iget-object v2, v0, LX/7T0;->j:LX/7T1;

    invoke-virtual {v2}, LX/7T1;->b()V

    .line 1210344
    if-eqz v1, :cond_2

    .line 1210345
    iget-object v0, p0, LX/7TM;->e:LX/7T0;

    iget-wide v2, v1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 1210346
    iget-object v1, v0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    iget-object v4, v0, LX/7T0;->h:Landroid/opengl/EGLSurface;

    invoke-static {v1, v4, v2, v3}, Landroid/opengl/EGLExt;->eglPresentationTimeANDROID(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;J)Z

    .line 1210347
    :cond_2
    iget-object v0, p0, LX/7TM;->e:LX/7T0;

    .line 1210348
    iget-object v1, v0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    iget-object v2, v0, LX/7T0;->h:Landroid/opengl/EGLSurface;

    invoke-static {v1, v2}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 1210349
    goto :goto_1

    .line 1210350
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b(LX/614;)V
    .locals 1

    .prologue
    .line 1210378
    iget-object v0, p0, LX/7TM;->c:LX/617;

    invoke-virtual {v0, p1}, LX/617;->b(LX/614;)V

    .line 1210379
    return-void
.end method

.method public final c(J)LX/614;
    .locals 1

    .prologue
    .line 1210377
    iget-object v0, p0, LX/7TM;->c:LX/617;

    invoke-virtual {v0, p1, p2}, LX/617;->b(J)LX/614;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1210355
    iget-object v0, p0, LX/7TM;->d:LX/617;

    if-eqz v0, :cond_0

    .line 1210356
    iget-object v0, p0, LX/7TM;->d:LX/617;

    invoke-virtual {v0}, LX/617;->b()V

    .line 1210357
    iput-object v1, p0, LX/7TM;->d:LX/617;

    .line 1210358
    :cond_0
    iget-object v0, p0, LX/7TM;->c:LX/617;

    if-eqz v0, :cond_1

    .line 1210359
    iget-object v0, p0, LX/7TM;->c:LX/617;

    invoke-virtual {v0}, LX/617;->b()V

    .line 1210360
    iput-object v1, p0, LX/7TM;->c:LX/617;

    .line 1210361
    :cond_1
    iget-object v0, p0, LX/7TM;->e:LX/7T0;

    if-eqz v0, :cond_3

    .line 1210362
    iget-object v0, p0, LX/7TM;->e:LX/7T0;

    const/4 v6, 0x0

    .line 1210363
    invoke-static {}, Landroid/opengl/EGL14;->eglGetCurrentContext()Landroid/opengl/EGLContext;

    move-result-object v2

    iget-object v3, v0, LX/7T0;->g:Landroid/opengl/EGLContext;

    invoke-virtual {v2, v3}, Landroid/opengl/EGLContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1210364
    iget-object v2, v0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v4, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v5, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v2, v3, v4, v5}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 1210365
    :cond_2
    iget-object v2, v0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    iget-object v3, v0, LX/7T0;->h:Landroid/opengl/EGLSurface;

    invoke-static {v2, v3}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 1210366
    iget-object v2, v0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    iget-object v3, v0, LX/7T0;->g:Landroid/opengl/EGLContext;

    invoke-static {v2, v3}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    .line 1210367
    iget-object v2, v0, LX/7T0;->e:Landroid/view/Surface;

    invoke-virtual {v2}, Landroid/view/Surface;->release()V

    .line 1210368
    iput-object v6, v0, LX/7T0;->f:Landroid/opengl/EGLDisplay;

    .line 1210369
    iput-object v6, v0, LX/7T0;->g:Landroid/opengl/EGLContext;

    .line 1210370
    iput-object v6, v0, LX/7T0;->h:Landroid/opengl/EGLSurface;

    .line 1210371
    iput-object v6, v0, LX/7T0;->i:LX/7T2;

    .line 1210372
    iput-object v6, v0, LX/7T0;->e:Landroid/view/Surface;

    .line 1210373
    iput-object v6, v0, LX/7T0;->c:Landroid/graphics/SurfaceTexture;

    .line 1210374
    iput-object v6, v0, LX/7T0;->j:LX/7T1;

    .line 1210375
    iput-object v1, p0, LX/7TM;->e:LX/7T0;

    .line 1210376
    :cond_3
    return-void
.end method

.method public final d()Landroid/media/MediaFormat;
    .locals 1

    .prologue
    .line 1210352
    iget-object v0, p0, LX/7TM;->c:LX/617;

    .line 1210353
    iget-object p0, v0, LX/617;->e:Landroid/media/MediaFormat;

    move-object v0, p0

    .line 1210354
    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1210351
    iget-object v0, p0, LX/7TM;->f:LX/7Sx;

    invoke-virtual {v0}, LX/7Sx;->a()I

    move-result v0

    return v0
.end method
