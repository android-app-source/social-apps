.class public final LX/8GK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8GH;


# instance fields
.field public final synthetic a:LX/8GL;


# direct methods
.method public constructor <init>(LX/8GL;)V
    .locals 0

    .prologue
    .line 1318933
    iput-object p1, p0, LX/8GK;->a:LX/8GL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1318932
    return-void
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;ZI)V
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1318931
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1318930
    return-void
.end method

.method public final b(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 4
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1318925
    iget-object v0, p0, LX/8GK;->a:LX/8GL;

    iget-object v0, v0, LX/8GL;->d:LX/0Px;

    const-string v1, "Call refreshCirclePageIndicator before registering the SwipeEventListener."

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1318926
    iget-object v0, p0, LX/8GK;->a:LX/8GL;

    iget-object v0, v0, LX/8GL;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1318927
    iget-object v1, p0, LX/8GK;->a:LX/8GL;

    iget-object v1, v1, LX/8GL;->c:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    iget-object v2, p0, LX/8GK;->a:LX/8GL;

    iget v2, v2, LX/8GL;->a:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a(IIZ)V

    .line 1318928
    return-void
.end method

.method public final c(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1318929
    return-void
.end method
