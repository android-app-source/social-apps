.class public final LX/7hx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/1Bx;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Z

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/1Be;


# direct methods
.method public constructor <init>(LX/1Be;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1226773
    iput-object p1, p0, LX/7hx;->d:LX/1Be;

    iput-object p2, p0, LX/7hx;->a:Ljava/lang/String;

    iput-boolean p3, p0, LX/7hx;->b:Z

    iput-object p4, p0, LX/7hx;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1226774
    const/4 v7, 0x0

    .line 1226775
    const/4 v0, 0x0

    .line 1226776
    iget-object v3, p0, LX/7hx;->a:Ljava/lang/String;

    move-object v6, v7

    move v8, v0

    .line 1226777
    :goto_0
    const/16 v0, 0x1e

    if-gt v8, v0, :cond_0

    if-eqz v3, :cond_0

    :try_start_0
    iget-object v0, p0, LX/7hx;->d:LX/1Be;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, LX/1Be;->a$redex0(LX/1Be;Landroid/net/Uri;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-nez v0, :cond_2

    .line 1226778
    :cond_0
    if-eqz v6, :cond_1

    .line 1226779
    iget-object v0, p0, LX/7hx;->d:LX/1Be;

    iget-object v0, v0, LX/1Be;->y:LX/1Bp;

    invoke-virtual {v0, v6}, LX/1Bp;->b(LX/3nA;)V

    :cond_1
    :goto_1
    return-object v7

    .line 1226780
    :cond_2
    :try_start_1
    iget-boolean v0, p0, LX/7hx;->b:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/7hx;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1226781
    if-eqz v6, :cond_1

    .line 1226782
    iget-object v0, p0, LX/7hx;->d:LX/1Be;

    iget-object v0, v0, LX/1Be;->y:LX/1Bp;

    invoke-virtual {v0, v6}, LX/1Bp;->b(LX/3nA;)V

    goto :goto_1

    .line 1226783
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/7hx;->d:LX/1Be;

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-static {v0, v3, v1, v2}, LX/1Be;->a$redex0(LX/1Be;Ljava/lang/String;ZZ)Ljava/util/Map;

    move-result-object v5

    .line 1226784
    iget-object v0, p0, LX/7hx;->d:LX/1Be;

    iget-object v0, v0, LX/1Be;->y:LX/1Bp;

    const/4 v1, 0x0

    iget-object v2, p0, LX/7hx;->a:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual/range {v0 .. v5}, LX/1Bp;->a(LX/1Bt;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;)LX/3nA;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    .line 1226785
    :try_start_3
    iget-object v0, p0, LX/7hx;->d:LX/1Be;

    iget-object v0, v0, LX/1Be;->y:LX/1Bp;

    invoke-virtual {v0, v1}, LX/1Bp;->a(LX/3nA;)LX/1Bx;

    move-result-object v2

    .line 1226786
    if-eqz v2, :cond_4

    iget-object v0, v2, LX/1Bx;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1226787
    add-int/lit8 v0, v8, 0x1

    .line 1226788
    iget-object v2, v2, LX/1Bx;->d:Ljava/lang/String;

    invoke-static {v3, v2}, LX/1Be;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    move-object v6, v1

    move v8, v0

    goto :goto_0

    .line 1226789
    :cond_4
    if-eqz v1, :cond_1

    .line 1226790
    iget-object v0, p0, LX/7hx;->d:LX/1Be;

    iget-object v0, v0, LX/1Be;->y:LX/1Bp;

    invoke-virtual {v0, v1}, LX/1Bp;->b(LX/3nA;)V

    goto :goto_1

    .line 1226791
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_5

    .line 1226792
    iget-object v2, p0, LX/7hx;->d:LX/1Be;

    iget-object v2, v2, LX/1Be;->y:LX/1Bp;

    invoke-virtual {v2, v1}, LX/1Bp;->b(LX/3nA;)V

    :cond_5
    throw v0

    .line 1226793
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method
