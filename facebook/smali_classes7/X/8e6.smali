.class public final LX/8e6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v7, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 1380131
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1380132
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1380133
    :goto_0
    return v1

    .line 1380134
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v12, :cond_4

    .line 1380135
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1380136
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1380137
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v9, :cond_0

    .line 1380138
    const-string v12, "latitude"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1380139
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v7

    goto :goto_1

    .line 1380140
    :cond_1
    const-string v12, "longitude"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1380141
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v10

    move v6, v7

    goto :goto_1

    .line 1380142
    :cond_2
    const-string v12, "timezone"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1380143
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1380144
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1380145
    :cond_4
    const/4 v9, 0x3

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1380146
    if-eqz v0, :cond_5

    move-object v0, p1

    .line 1380147
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1380148
    :cond_5
    if-eqz v6, :cond_6

    move-object v0, p1

    move v1, v7

    move-wide v2, v10

    .line 1380149
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1380150
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1380151
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v6, v1

    move v0, v1

    move v8, v1

    move-wide v10, v4

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1380152
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1380153
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1380154
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 1380155
    const-string v2, "latitude"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1380156
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1380157
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1380158
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 1380159
    const-string v2, "longitude"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1380160
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1380161
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1380162
    if-eqz v0, :cond_2

    .line 1380163
    const-string v1, "timezone"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1380164
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1380165
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1380166
    return-void
.end method
