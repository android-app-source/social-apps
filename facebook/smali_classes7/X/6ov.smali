.class public final LX/6ov;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QR",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

.field public final synthetic c:LX/6p6;


# direct methods
.method public constructor <init>(LX/6p6;Ljava/lang/String;Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;)V
    .locals 0

    .prologue
    .line 1148699
    iput-object p1, p0, LX/6ov;->c:LX/6p6;

    iput-object p2, p0, LX/6ov;->a:Ljava/lang/String;

    iput-object p3, p0, LX/6ov;->b:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1148700
    iget-object v0, p0, LX/6ov;->c:LX/6p6;

    iget-object v0, v0, LX/6p6;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1148701
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1148702
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1148703
    iget-object v0, p0, LX/6ov;->c:LX/6p6;

    iget-object v0, v0, LX/6p6;->i:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    iget-object v1, p0, LX/6ov;->a:Ljava/lang/String;

    iget-object v4, p0, LX/6ov;->b:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    if-nez v4, :cond_0

    sget-object v4, LX/03R;->UNSET:LX/03R;

    :goto_0
    iget-object v5, p0, LX/6ov;->b:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    if-nez v5, :cond_1

    const/4 v5, 0x0

    .line 1148704
    :goto_1
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1148705
    sget-object v7, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->a:Ljava/lang/String;

    .line 1148706
    new-instance v8, LX/6ot;

    invoke-direct {v8}, LX/6ot;-><init>()V

    move-object v8, v8

    .line 1148707
    iput-object v1, v8, LX/6ot;->a:Ljava/lang/String;

    .line 1148708
    move-object v8, v8

    .line 1148709
    iput-wide v2, v8, LX/6ot;->b:J

    .line 1148710
    move-object v8, v8

    .line 1148711
    iput-object v4, v8, LX/6ot;->c:LX/03R;

    .line 1148712
    move-object v8, v8

    .line 1148713
    iput-object v5, v8, LX/6ot;->d:Ljava/util/Map;

    .line 1148714
    move-object v8, v8

    .line 1148715
    new-instance v9, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;

    invoke-direct {v9, v8}, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;-><init>(LX/6ot;)V

    move-object v8, v9

    .line 1148716
    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1148717
    const-string v7, "set_payment_pin"

    invoke-static {v0, v6, v7}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 1148718
    invoke-static {v0, v6}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v0, v6

    .line 1148719
    return-object v0

    :cond_0
    iget-object v4, p0, LX/6ov;->b:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    .line 1148720
    iget-object v5, v4, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;->b:LX/03R;

    move-object v4, v5

    .line 1148721
    goto :goto_0

    :cond_1
    iget-object v5, p0, LX/6ov;->b:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    invoke-virtual {v5}, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;->b()LX/0P1;

    move-result-object v5

    goto :goto_1
.end method
