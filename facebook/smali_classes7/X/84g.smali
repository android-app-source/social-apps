.class public LX/84g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/84g;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1291089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1291090
    return-void
.end method

.method public static a(LX/0QB;)LX/84g;
    .locals 3

    .prologue
    .line 1291077
    sget-object v0, LX/84g;->a:LX/84g;

    if-nez v0, :cond_1

    .line 1291078
    const-class v1, LX/84g;

    monitor-enter v1

    .line 1291079
    :try_start_0
    sget-object v0, LX/84g;->a:LX/84g;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1291080
    if-eqz v2, :cond_0

    .line 1291081
    :try_start_1
    new-instance v0, LX/84g;

    invoke-direct {v0}, LX/84g;-><init>()V

    .line 1291082
    move-object v0, v0

    .line 1291083
    sput-object v0, LX/84g;->a:LX/84g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1291084
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1291085
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1291086
    :cond_1
    sget-object v0, LX/84g;->a:LX/84g;

    return-object v0

    .line 1291087
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1291088
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1291071
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1291072
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "seen"

    const-string v2, "true"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1291073
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1291074
    new-instance v0, LX/14N;

    const-string v1, "markFriendRequestsSeen"

    const-string v2, "POST"

    const-string v3, "me/friendrequests"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1291075
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1291076
    const/4 v0, 0x0

    return-object v0
.end method
