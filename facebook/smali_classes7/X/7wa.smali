.class public final LX/7wa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;)V
    .locals 0

    .prologue
    .line 1275865
    iput-object p1, p0, LX/7wa;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 6

    .prologue
    .line 1275866
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 1275867
    iget-object v0, p0, LX/7wa;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1275868
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 1275869
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1275870
    :goto_0
    iget-object v0, p0, LX/7wa;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1275871
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 1275872
    iget-object v1, p0, LX/7wa;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    invoke-static {v1}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->l(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 1275873
    iget-object v0, p0, LX/7wa;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    const/4 v5, 0x0

    .line 1275874
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v3, v2

    .line 1275875
    if-nez v3, :cond_2

    .line 1275876
    const/4 v2, 0x0

    .line 1275877
    :goto_1
    move-object v0, v2

    .line 1275878
    if-eqz v0, :cond_0

    .line 1275879
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1275880
    :cond_0
    return-void

    .line 1275881
    :cond_1
    iget-object v0, p0, LX/7wa;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1275882
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 1275883
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 1275884
    :cond_2
    const/4 v2, 0x2

    new-array v2, v2, [I

    invoke-static {v0}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->l(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;)I

    move-result v4

    aput v4, v2, v5

    const/4 v4, 0x1

    aput v5, v2, v4

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 1275885
    new-instance v4, LX/7wZ;

    invoke-direct {v4, v0, v3}, LX/7wZ;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;Landroid/view/View;)V

    invoke-virtual {v2, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1275886
    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    goto :goto_1
.end method
