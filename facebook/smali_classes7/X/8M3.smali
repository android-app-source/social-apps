.class public final LX/8M3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/7mj;

.field public final synthetic b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7mj;)V
    .locals 0

    .prologue
    .line 1334266
    iput-object p1, p0, LX/8M3;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iput-object p2, p0, LX/8M3;->a:LX/7mj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 1334267
    iget-object v0, p0, LX/8M3;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->K:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1334268
    iget-object v0, p0, LX/8M3;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v1, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    const-string v2, "menu"

    iget-object v0, p0, LX/8M3;->a:LX/7mj;

    invoke-virtual {v0}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/8M3;->a:LX/7mj;

    invoke-virtual {v0}, LX/7mi;->k()I

    move-result v4

    iget-object v0, p0, LX/8M3;->a:LX/7mj;

    invoke-virtual {v0}, LX/7mi;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8M3;->a:LX/7mj;

    invoke-virtual {v0}, LX/7mi;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v0

    :goto_0
    invoke-virtual {v1, v2, v3, v4, v0}, LX/1RW;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 1334269
    iget-object v0, p0, LX/8M3;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->K:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Lo;

    iget-object v1, p0, LX/8M3;->a:LX/7mj;

    invoke-virtual {v0, v1}, LX/8Lo;->a(LX/7mj;)V

    .line 1334270
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 1334271
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
