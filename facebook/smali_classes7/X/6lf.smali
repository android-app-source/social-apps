.class public abstract LX/6lf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "LX/6le;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/messaging/xma/StyleRenderer;",
        "Lcom/facebook/messaging/xma/SubattachmentStyleRenderer;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<TVH;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "TVH;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1142853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142854
    invoke-static {}, LX/0UK;->a()Ljava/util/ArrayDeque;

    move-result-object v0

    iput-object v0, p0, LX/6lf;->a:Ljava/util/Deque;

    .line 1142855
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/6lf;->b:Ljava/util/Map;

    .line 1142856
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1142849
    iget-object v0, p0, LX/6lf;->a:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, LX/6lf;->b(Landroid/view/ViewGroup;)LX/6le;

    move-result-object v0

    .line 1142850
    :goto_0
    iget-object v1, p0, LX/6lf;->b:Ljava/util/Map;

    iget-object v2, v0, LX/6le;->a:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1142851
    iget-object v0, v0, LX/6le;->a:Landroid/view/View;

    return-object v0

    .line 1142852
    :cond_0
    iget-object v0, p0, LX/6lf;->a:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6le;

    goto :goto_0
.end method

.method public a(LX/6le;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesInterfaces$XMAAttachmentStoryFields$Subattachments;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1142848
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1142845
    iget-object v0, p0, LX/6lf;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "Tried to return a view that was not lent out by this StyleRenderer"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6le;

    .line 1142846
    iget-object v1, p0, LX/6lf;->a:Ljava/util/Deque;

    invoke-interface {v1, v0}, Ljava/util/Deque;->push(Ljava/lang/Object;)V

    .line 1142847
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;)V
    .locals 1

    .prologue
    .line 1142841
    iget-object v0, p0, LX/6lf;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6le;

    .line 1142842
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1142843
    invoke-virtual {p0, v0, p2}, LX/6lf;->a(LX/6le;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;)V

    .line 1142844
    return-void
.end method

.method public abstract b(Landroid/view/ViewGroup;)LX/6le;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")TVH;"
        }
    .end annotation
.end method
