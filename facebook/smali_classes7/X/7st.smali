.class public final LX/7st;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1266097
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 1266098
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1266099
    :goto_0
    return v1

    .line 1266100
    :cond_0
    const-string v11, "is_official"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1266101
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v8, v3

    move v3, v2

    .line 1266102
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_7

    .line 1266103
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1266104
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1266105
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1266106
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1266107
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1266108
    :cond_2
    const-string v11, "max_ticket_price"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1266109
    invoke-static {p0, p1}, LX/7sp;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1266110
    :cond_3
    const-string v11, "min_ticket_price"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1266111
    invoke-static {p0, p1}, LX/7sq;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1266112
    :cond_4
    const-string v11, "ticket_tiers"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1266113
    invoke-static {p0, p1}, LX/7ss;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1266114
    :cond_5
    const-string v11, "total_purchased_tickets"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1266115
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 1266116
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1266117
    :cond_7
    const/4 v10, 0x6

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1266118
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1266119
    if-eqz v3, :cond_8

    .line 1266120
    invoke-virtual {p1, v2, v8}, LX/186;->a(IZ)V

    .line 1266121
    :cond_8
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 1266122
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 1266123
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 1266124
    if-eqz v0, :cond_9

    .line 1266125
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 1266126
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1266127
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1266128
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266129
    if-eqz v0, :cond_0

    .line 1266130
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266131
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266132
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1266133
    if-eqz v0, :cond_1

    .line 1266134
    const-string v1, "is_official"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266135
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1266136
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1266137
    if-eqz v0, :cond_2

    .line 1266138
    const-string v1, "max_ticket_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266139
    invoke-static {p0, v0, p2}, LX/7sp;->a(LX/15i;ILX/0nX;)V

    .line 1266140
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1266141
    if-eqz v0, :cond_3

    .line 1266142
    const-string v1, "min_ticket_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266143
    invoke-static {p0, v0, p2}, LX/7sq;->a(LX/15i;ILX/0nX;)V

    .line 1266144
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1266145
    if-eqz v0, :cond_4

    .line 1266146
    const-string v1, "ticket_tiers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266147
    invoke-static {p0, v0, p2, p3}, LX/7ss;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1266148
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1266149
    if-eqz v0, :cond_5

    .line 1266150
    const-string v1, "total_purchased_tickets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266151
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1266152
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1266153
    return-void
.end method
