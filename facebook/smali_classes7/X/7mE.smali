.class public LX/7mE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;",
        "Lcom/facebook/composer/publish/protocol/SimplePhotoUploadResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1236741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236742
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 12

    .prologue
    .line 1236685
    check-cast p1, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1236686
    iget-object v0, p1, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->c:Ljava/lang/String;

    move-object v3, v0

    .line 1236687
    iget-wide v10, p1, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->b:J

    move-wide v4, v10

    .line 1236688
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 1236689
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1236690
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1236691
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1236692
    iget-object v5, p1, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1236693
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1236694
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "composer_session_id"

    invoke-direct {v6, v7, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236695
    :cond_0
    iget-object v5, p1, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->d:Ljava/lang/String;

    move-object v5, v5

    .line 1236696
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1236697
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "caption"

    invoke-direct {v6, v7, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236698
    :cond_1
    iget-object v5, p1, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->f:LX/5Rn;

    move-object v5, v5

    .line 1236699
    sget-object v6, LX/5Rn;->NORMAL:LX/5Rn;

    if-eq v5, v6, :cond_2

    .line 1236700
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "published"

    sget-object v7, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236701
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "unpublished_content_type"

    .line 1236702
    iget-object v7, p1, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->f:LX/5Rn;

    move-object v7, v7

    .line 1236703
    invoke-virtual {v7}, LX/5Rn;->getContentType()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236704
    :cond_2
    iget-wide v10, p1, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->e:J

    move-wide v6, v10

    .line 1236705
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_3

    .line 1236706
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "scheduled_publish_time"

    .line 1236707
    iget-wide v10, p1, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->e:J

    move-wide v8, v10

    .line 1236708
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236709
    :cond_3
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1236710
    new-instance v3, LX/4ct;

    const-string v6, "image/jpeg"

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v5, v6, v7}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 1236711
    new-instance v5, LX/4cQ;

    const-string v6, "source"

    invoke-direct {v5, v6, v3}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 1236712
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v3

    const-string v6, "graphObjectPhoto"

    .line 1236713
    iput-object v6, v3, LX/14O;->b:Ljava/lang/String;

    .line 1236714
    move-object v3, v3

    .line 1236715
    const-string v6, "POST"

    .line 1236716
    iput-object v6, v3, LX/14O;->c:Ljava/lang/String;

    .line 1236717
    move-object v3, v3

    .line 1236718
    const-string v6, "/%d/photos"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-static {v6, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1236719
    iput-object v1, v3, LX/14O;->d:Ljava/lang/String;

    .line 1236720
    move-object v1, v3

    .line 1236721
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1236722
    move-object v0, v1

    .line 1236723
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1236724
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1236725
    move-object v0, v0

    .line 1236726
    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1236727
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 1236728
    move-object v0, v0

    .line 1236729
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :cond_4
    move v0, v2

    .line 1236730
    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 1236731
    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1236732
    const/4 v0, 0x0

    .line 1236733
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v1

    .line 1236734
    const-string v2, "post_id"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 1236735
    const-string v3, "id"

    invoke-virtual {v1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 1236736
    if-eqz v2, :cond_1

    .line 1236737
    invoke-virtual {v2}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    .line 1236738
    :goto_0
    if-eqz v3, :cond_0

    .line 1236739
    invoke-virtual {v3}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 1236740
    :cond_0
    new-instance v2, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadResult;

    invoke-direct {v2, v1, v0}, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method
