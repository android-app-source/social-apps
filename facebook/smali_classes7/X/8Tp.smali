.class public final enum LX/8Tp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Tp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Tp;

.field public static final enum ACTIVITY_RETURN_UNSUCCESSFUL:LX/8Tp;

.field public static final enum ACTIVITY_STARTED:LX/8Tp;

.field public static final enum CHALLENGE_CREATED:LX/8Tp;

.field public static final enum FACEBOOK_UNSUPPORTED:LX/8Tp;

.field public static final enum FB_SHARING_SUCCESSFUL:LX/8Tp;

.field public static final enum GAME_INFO_FETCH_FAILURE:LX/8Tp;

.field public static final enum MESSENGER_UNSUPPORTED:LX/8Tp;

.field public static final enum MN_SHARING_STARTED:LX/8Tp;

.field public static final enum MN_SHARING_SUCCESSFUL:LX/8Tp;

.field public static final enum SHORTCUT_CREATED:LX/8Tp;

.field public static final enum SOMETHING_WENT_WRONG:LX/8Tp;

.field public static final enum UNHANDLED_EVENT:LX/8Tp;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1349167
    new-instance v0, LX/8Tp;

    const-string v1, "ACTIVITY_STARTED"

    invoke-direct {v0, v1, v3}, LX/8Tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Tp;->ACTIVITY_STARTED:LX/8Tp;

    .line 1349168
    new-instance v0, LX/8Tp;

    const-string v1, "ACTIVITY_RETURN_UNSUCCESSFUL"

    invoke-direct {v0, v1, v4}, LX/8Tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Tp;->ACTIVITY_RETURN_UNSUCCESSFUL:LX/8Tp;

    .line 1349169
    new-instance v0, LX/8Tp;

    const-string v1, "MESSENGER_UNSUPPORTED"

    invoke-direct {v0, v1, v5}, LX/8Tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Tp;->MESSENGER_UNSUPPORTED:LX/8Tp;

    .line 1349170
    new-instance v0, LX/8Tp;

    const-string v1, "FACEBOOK_UNSUPPORTED"

    invoke-direct {v0, v1, v6}, LX/8Tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Tp;->FACEBOOK_UNSUPPORTED:LX/8Tp;

    .line 1349171
    new-instance v0, LX/8Tp;

    const-string v1, "SHORTCUT_CREATED"

    invoke-direct {v0, v1, v7}, LX/8Tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Tp;->SHORTCUT_CREATED:LX/8Tp;

    .line 1349172
    new-instance v0, LX/8Tp;

    const-string v1, "UNHANDLED_EVENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8Tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Tp;->UNHANDLED_EVENT:LX/8Tp;

    .line 1349173
    new-instance v0, LX/8Tp;

    const-string v1, "MN_SHARING_STARTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8Tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Tp;->MN_SHARING_STARTED:LX/8Tp;

    .line 1349174
    new-instance v0, LX/8Tp;

    const-string v1, "MN_SHARING_SUCCESSFUL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/8Tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Tp;->MN_SHARING_SUCCESSFUL:LX/8Tp;

    .line 1349175
    new-instance v0, LX/8Tp;

    const-string v1, "FB_SHARING_SUCCESSFUL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/8Tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Tp;->FB_SHARING_SUCCESSFUL:LX/8Tp;

    .line 1349176
    new-instance v0, LX/8Tp;

    const-string v1, "CHALLENGE_CREATED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/8Tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Tp;->CHALLENGE_CREATED:LX/8Tp;

    .line 1349177
    new-instance v0, LX/8Tp;

    const-string v1, "SOMETHING_WENT_WRONG"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/8Tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Tp;->SOMETHING_WENT_WRONG:LX/8Tp;

    .line 1349178
    new-instance v0, LX/8Tp;

    const-string v1, "GAME_INFO_FETCH_FAILURE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/8Tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Tp;->GAME_INFO_FETCH_FAILURE:LX/8Tp;

    .line 1349179
    const/16 v0, 0xc

    new-array v0, v0, [LX/8Tp;

    sget-object v1, LX/8Tp;->ACTIVITY_STARTED:LX/8Tp;

    aput-object v1, v0, v3

    sget-object v1, LX/8Tp;->ACTIVITY_RETURN_UNSUCCESSFUL:LX/8Tp;

    aput-object v1, v0, v4

    sget-object v1, LX/8Tp;->MESSENGER_UNSUPPORTED:LX/8Tp;

    aput-object v1, v0, v5

    sget-object v1, LX/8Tp;->FACEBOOK_UNSUPPORTED:LX/8Tp;

    aput-object v1, v0, v6

    sget-object v1, LX/8Tp;->SHORTCUT_CREATED:LX/8Tp;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8Tp;->UNHANDLED_EVENT:LX/8Tp;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8Tp;->MN_SHARING_STARTED:LX/8Tp;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8Tp;->MN_SHARING_SUCCESSFUL:LX/8Tp;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8Tp;->FB_SHARING_SUCCESSFUL:LX/8Tp;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8Tp;->CHALLENGE_CREATED:LX/8Tp;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8Tp;->SOMETHING_WENT_WRONG:LX/8Tp;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8Tp;->GAME_INFO_FETCH_FAILURE:LX/8Tp;

    aput-object v2, v0, v1

    sput-object v0, LX/8Tp;->$VALUES:[LX/8Tp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1349180
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Tp;
    .locals 1

    .prologue
    .line 1349181
    const-class v0, LX/8Tp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Tp;

    return-object v0
.end method

.method public static values()[LX/8Tp;
    .locals 1

    .prologue
    .line 1349182
    sget-object v0, LX/8Tp;->$VALUES:[LX/8Tp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Tp;

    return-object v0
.end method
