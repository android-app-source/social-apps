.class public final LX/7K0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Jy;


# instance fields
.field public final synthetic a:LX/7K1;


# direct methods
.method public constructor <init>(LX/7K1;)V
    .locals 0

    .prologue
    .line 1194368
    iput-object p1, p0, LX/7K0;->a:LX/7K1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IIIF)V
    .locals 1

    .prologue
    .line 1194360
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    if-eqz v0, :cond_0

    .line 1194361
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(IIIF)V

    .line 1194362
    :goto_0
    return-void

    .line 1194363
    :cond_0
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    .line 1194364
    iput p1, v0, LX/7K1;->i:I

    .line 1194365
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    .line 1194366
    iput p2, v0, LX/7K1;->j:I

    .line 1194367
    goto :goto_0
.end method

.method public final a(IJ)V
    .locals 2

    .prologue
    .line 1194357
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    if-eqz v0, :cond_0

    .line 1194358
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(IJ)V

    .line 1194359
    :cond_0
    return-void
.end method

.method public final a(LX/0L3;)V
    .locals 1

    .prologue
    .line 1194353
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    if-eqz v0, :cond_0

    .line 1194354
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/0L3;)V

    .line 1194355
    :goto_0
    return-void

    .line 1194356
    :cond_0
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    invoke-static {v0, p1}, LX/7K1;->a$redex0(LX/7K1;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(LX/0L4;)V
    .locals 2

    .prologue
    .line 1194342
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    if-eqz v0, :cond_1

    .line 1194343
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/0L4;)V

    .line 1194344
    :cond_0
    :goto_0
    return-void

    .line 1194345
    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/0L4;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1194346
    iget-object v0, p1, LX/0L4;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0Al;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1194347
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v1, p1, LX/0L4;->b:Ljava/lang/String;

    .line 1194348
    iput-object v1, v0, LX/7K1;->g:Ljava/lang/String;

    .line 1194349
    :cond_2
    iget-object v0, p1, LX/0L4;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0Al;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1194350
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v1, p1, LX/0L4;->b:Ljava/lang/String;

    .line 1194351
    iput-object v1, v0, LX/7K1;->h:Ljava/lang/String;

    .line 1194352
    goto :goto_0
.end method

.method public final a(LX/0LK;)V
    .locals 1

    .prologue
    .line 1194338
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    if-eqz v0, :cond_0

    .line 1194339
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/0LK;)V

    .line 1194340
    :goto_0
    return-void

    .line 1194341
    :cond_0
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    invoke-static {v0, p1}, LX/7K1;->a$redex0(LX/7K1;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(LX/0LM;)V
    .locals 1

    .prologue
    .line 1194335
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    if-eqz v0, :cond_0

    .line 1194336
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/0LM;)V

    .line 1194337
    :cond_0
    return-void
.end method

.method public final a(Landroid/media/MediaCodec$CryptoException;)V
    .locals 1

    .prologue
    .line 1194326
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    if-eqz v0, :cond_0

    .line 1194327
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Landroid/media/MediaCodec$CryptoException;)V

    .line 1194328
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 1194332
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    if-eqz v0, :cond_0

    .line 1194333
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Landroid/view/Surface;)V

    .line 1194334
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;JJ)V
    .locals 6

    .prologue
    .line 1194329
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    if-eqz v0, :cond_0

    .line 1194330
    iget-object v0, p0, LX/7K0;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;JJ)V

    .line 1194331
    :cond_0
    return-void
.end method
