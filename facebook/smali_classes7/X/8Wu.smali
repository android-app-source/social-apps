.class public final LX/8Wu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/8Wx;

.field public final synthetic b:I

.field public final synthetic c:LX/8Wv;


# direct methods
.method public constructor <init>(LX/8Wv;LX/8Wx;I)V
    .locals 0

    .prologue
    .line 1354467
    iput-object p1, p0, LX/8Wu;->c:LX/8Wv;

    iput-object p2, p0, LX/8Wu;->a:LX/8Wx;

    iput p3, p0, LX/8Wu;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0xfda7d2f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1354458
    iget-object v1, p0, LX/8Wu;->c:LX/8Wv;

    iget-object v1, v1, LX/8Wv;->l:Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;

    iget-object v1, v1, Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;->c:LX/8Wy;

    if-eqz v1, :cond_0

    .line 1354459
    iget-object v1, p0, LX/8Wu;->c:LX/8Wv;

    iget-object v1, v1, LX/8Wv;->l:Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;

    iget-object v1, v1, Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;->c:LX/8Wy;

    iget-object v2, p0, LX/8Wu;->a:LX/8Wx;

    .line 1354460
    iget-object v3, v2, LX/8Wx;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1354461
    iget v3, p0, LX/8Wu;->b:I

    .line 1354462
    iget-object p0, v1, LX/8Wy;->a:LX/8X0;

    iget-object p0, p0, LX/8X0;->d:LX/8TS;

    invoke-virtual {p0, v2}, LX/8TS;->b(Ljava/lang/String;)V

    .line 1354463
    iget-object p0, v1, LX/8Wy;->a:LX/8X0;

    iget-object p0, p0, LX/8X0;->c:LX/8TD;

    .line 1354464
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object p1

    sget-object v1, LX/8TE;->FUNNEL_ACTION_TAG_INDEX:LX/8TE;

    iget-object v1, v1, LX/8TE;->value:Ljava/lang/String;

    invoke-virtual {p1, v1, v3}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object p1

    sget-object v1, LX/8TE;->FUNNEL_ACTION_TAG_GAME_ID:LX/8TE;

    iget-object v1, v1, LX/8TE;->value:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object p1

    .line 1354465
    sget-object v1, LX/8TE;->FUNNEL_GAME_SWITCH:LX/8TE;

    invoke-static {p0, v1, p1}, LX/8TD;->a(LX/8TD;LX/8TE;LX/1rQ;)V

    .line 1354466
    :cond_0
    const v1, 0x193c30

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
