.class public LX/7kp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/7ko;

.field private static volatile c:LX/7kp;


# instance fields
.field public final b:LX/11i;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1232475
    new-instance v0, LX/7ko;

    invoke-direct {v0}, LX/7ko;-><init>()V

    sput-object v0, LX/7kp;->a:LX/7ko;

    return-void
.end method

.method public constructor <init>(LX/11i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1232444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232445
    iput-object p1, p0, LX/7kp;->b:LX/11i;

    .line 1232446
    return-void
.end method

.method public static a(LX/0QB;)LX/7kp;
    .locals 4

    .prologue
    .line 1232458
    sget-object v0, LX/7kp;->c:LX/7kp;

    if-nez v0, :cond_1

    .line 1232459
    const-class v1, LX/7kp;

    monitor-enter v1

    .line 1232460
    :try_start_0
    sget-object v0, LX/7kp;->c:LX/7kp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1232461
    if-eqz v2, :cond_0

    .line 1232462
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1232463
    new-instance p0, LX/7kp;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-direct {p0, v3}, LX/7kp;-><init>(LX/11i;)V

    .line 1232464
    move-object v0, p0

    .line 1232465
    sput-object v0, LX/7kp;->c:LX/7kp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1232466
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1232467
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1232468
    :cond_1
    sget-object v0, LX/7kp;->c:LX/7kp;

    return-object v0

    .line 1232469
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1232470
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/7kp;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1232471
    iget-object v0, p0, LX/7kp;->b:LX/11i;

    sget-object v1, LX/7kp;->a:LX/7ko;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1232472
    if-eqz v0, :cond_0

    .line 1232473
    const v1, 0x4f6e4ef1

    invoke-static {v0, p1, v1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1232474
    :cond_0
    return-void
.end method

.method public static c(LX/7kp;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1232454
    iget-object v0, p0, LX/7kp;->b:LX/11i;

    sget-object v1, LX/7kp;->a:LX/7ko;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1232455
    if-eqz v0, :cond_0

    .line 1232456
    const v1, 0x25ffdee6

    invoke-static {v0, p1, v1}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1232457
    :cond_0
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 3

    .prologue
    .line 1232449
    const-string v0, "WithTagPressed"

    .line 1232450
    iget-object v1, p0, LX/7kp;->b:LX/11i;

    sget-object v2, LX/7kp;->a:LX/7ko;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 1232451
    if-eqz v1, :cond_0

    .line 1232452
    const v2, 0xd3110bd

    invoke-static {v1, v0, v2}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1232453
    :cond_0
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 1232447
    iget-object v0, p0, LX/7kp;->b:LX/11i;

    sget-object v1, LX/7kp;->a:LX/7ko;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V

    .line 1232448
    return-void
.end method
