.class public LX/7Dz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Dy;


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;)V
    .locals 0

    .prologue
    .line 1184127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184128
    iput-object p1, p0, LX/7Dz;->a:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    .line 1184129
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1184126
    iget-object v0, p0, LX/7Dz;->a:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->n()I

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1184125
    iget-object v0, p0, LX/7Dz;->a:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->m()I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1184124
    iget-object v0, p0, LX/7Dz;->a:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->j()I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1184123
    iget-object v0, p0, LX/7Dz;->a:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->a()I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1184130
    iget-object v0, p0, LX/7Dz;->a:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->k()I

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1184122
    iget-object v0, p0, LX/7Dz;->a:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->l()I

    move-result v0

    return v0
.end method

.method public final g()D
    .locals 2

    .prologue
    .line 1184121
    iget-object v0, p0, LX/7Dz;->a:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->o()D

    move-result-wide v0

    return-wide v0
.end method

.method public final h()D
    .locals 2

    .prologue
    .line 1184120
    iget-object v0, p0, LX/7Dz;->a:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->p()D

    move-result-wide v0

    return-wide v0
.end method

.method public final i()D
    .locals 2

    .prologue
    .line 1184119
    iget-object v0, p0, LX/7Dz;->a:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->q()D

    move-result-wide v0

    return-wide v0
.end method

.method public final j()D
    .locals 2

    .prologue
    .line 1184118
    iget-object v0, p0, LX/7Dz;->a:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->r()D

    move-result-wide v0

    return-wide v0
.end method
