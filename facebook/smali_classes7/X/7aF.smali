.class public final LX/7aF;
.super LX/7aA;
.source ""


# instance fields
.field private a:LX/2wh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2wh",
            "<",
            "Lcom/google/android/gms/location/LocationSettingsResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2wh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wh",
            "<",
            "Lcom/google/android/gms/location/LocationSettingsResult;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, LX/7aA;-><init>()V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "listener can\'t be null."

    invoke-static {v0, v1}, LX/1ol;->b(ZLjava/lang/Object;)V

    iput-object p1, p0, LX/7aF;->a:LX/2wh;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/location/LocationSettingsResult;)V
    .locals 1

    iget-object v0, p0, LX/7aF;->a:LX/2wh;

    invoke-interface {v0, p1}, LX/2wh;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, LX/7aF;->a:LX/2wh;

    return-void
.end method
