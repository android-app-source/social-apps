.class public final LX/7lr;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/publish/common/EditPostParams;

.field public final synthetic b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Lcom/facebook/composer/publish/common/EditPostParams;)V
    .locals 0

    .prologue
    .line 1235777
    iput-object p1, p0, LX/7lr;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iput-object p2, p0, LX/7lr;->a:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 1235778
    iget-object v0, p0, LX/7lr;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    iget-object v1, p0, LX/7lr;->a:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/EditPostParams;->getComposerSessionId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2rt;->STATUS:LX/2rt;

    const-string v3, "{}"

    const-string v4, "publish_edit_post"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/0gd;->a(Ljava/lang/String;LX/2rt;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1235779
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 1235780
    iget-object v0, p0, LX/7lr;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->f:LX/1CW;

    invoke-virtual {v0, p1, v1, v1}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v5

    .line 1235781
    iget-object v0, p0, LX/7lr;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->j:LX/4nT;

    invoke-virtual {v0, v5}, LX/4nT;->a(Ljava/lang/String;)V

    .line 1235782
    iget-object v0, p0, LX/7lr;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    iget-object v1, p0, LX/7lr;->a:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/EditPostParams;->getComposerSessionId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2rt;->STATUS:LX/2rt;

    const-string v3, "{}"

    const-string v4, "publish_edit_post"

    const/4 v7, 0x0

    move-object v6, p1

    invoke-virtual/range {v0 .. v7}, LX/0gd;->a(Ljava/lang/String;LX/2rt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/fbservice/service/ServiceException;I)V

    .line 1235783
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1235784
    invoke-direct {p0}, LX/7lr;->a()V

    return-void
.end method
