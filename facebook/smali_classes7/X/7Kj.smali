.class public final LX/7Kj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/7Kk;

.field private final b:Landroid/app/PendingIntent;

.field private final c:Landroid/app/PendingIntent;

.field public d:Landroid/widget/RemoteViews;

.field public e:Landroid/widget/RemoteViews;


# direct methods
.method public constructor <init>(LX/7Kk;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1196229
    iput-object p1, p0, LX/7Kj;->a:LX/7Kk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1196230
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p1, LX/7Kk;->f:Landroid/content/Context;

    const-class v2, Lcom/facebook/video/chromecast/notification/CastNotificationActionService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1196231
    const-string v1, "com.facebook.video.chromecast.CAST_PLAY_PAUSE_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1196232
    iget-object v1, p1, LX/7Kk;->f:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, LX/7Kj;->b:Landroid/app/PendingIntent;

    .line 1196233
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p1, LX/7Kk;->f:Landroid/content/Context;

    const-class v2, Lcom/facebook/video/chromecast/notification/CastNotificationActionService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1196234
    const-string v1, "com.facebook.video.chromecast.CAST_DISCONNECT_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1196235
    iget-object v1, p1, LX/7Kk;->f:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, LX/7Kj;->c:Landroid/app/PendingIntent;

    .line 1196236
    return-void
.end method

.method public static a(LX/7Kj;Landroid/widget/RemoteViews;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1196237
    const v0, 0x7f0d08e9

    iget-object v1, p0, LX/7Kj;->b:Landroid/app/PendingIntent;

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 1196238
    const v0, 0x7f0d08ea

    iget-object v1, p0, LX/7Kj;->c:Landroid/app/PendingIntent;

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 1196239
    const v0, 0x7f0d08e9

    invoke-direct {p0}, LX/7Kj;->e()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 1196240
    const v0, 0x7f0d08e7

    iget-object v1, p0, LX/7Kj;->a:LX/7Kk;

    iget-object v1, v1, LX/7Kk;->e:LX/7Kh;

    .line 1196241
    invoke-static {v1}, LX/7Kh;->i(LX/7Kh;)LX/7JN;

    move-result-object v2

    .line 1196242
    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object v1, v2

    .line 1196243
    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1196244
    const v0, 0x7f0d08e8

    iget-object v1, p0, LX/7Kj;->a:LX/7Kk;

    iget-object v1, v1, LX/7Kk;->e:LX/7Kh;

    .line 1196245
    iget-object v2, v1, LX/7Kh;->d:LX/37Y;

    invoke-virtual {v2}, LX/37Y;->f()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1196246
    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1196247
    const v0, 0x7f0d08ec

    iget-object v1, p0, LX/7Kj;->a:LX/7Kk;

    iget-object v1, v1, LX/7Kk;->e:LX/7Kh;

    .line 1196248
    invoke-static {v1}, LX/7Kh;->i(LX/7Kh;)LX/7JN;

    move-result-object v2

    .line 1196249
    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object v1, v2

    .line 1196250
    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1196251
    const v0, 0x7f0d08e9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1196252
    return-void

    .line 1196253
    :cond_0
    iget-object v1, v2, LX/7JN;->c:Ljava/lang/String;

    move-object v2, v1

    .line 1196254
    goto :goto_0

    .line 1196255
    :cond_1
    iget-object v1, v2, LX/7JN;->b:Ljava/lang/String;

    move-object v2, v1

    .line 1196256
    goto :goto_1
.end method

.method private e()I
    .locals 1

    .prologue
    .line 1196257
    iget-object v0, p0, LX/7Kj;->a:LX/7Kk;

    iget-object v0, v0, LX/7Kk;->e:LX/7Kh;

    .line 1196258
    iget-object p0, v0, LX/7Kh;->d:LX/37Y;

    invoke-virtual {p0}, LX/37Y;->s()LX/38j;

    move-result-object p0

    invoke-virtual {p0}, LX/38j;->a()Z

    move-result p0

    move v0, p0

    .line 1196259
    if-eqz v0, :cond_0

    const v0, 0x7f02094e

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f02096b

    goto :goto_0
.end method


# virtual methods
.method public final d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1196260
    iget-object v0, p0, LX/7Kj;->d:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_0

    .line 1196261
    iget-object v0, p0, LX/7Kj;->d:Landroid/widget/RemoteViews;

    const v1, 0x7f0d08e9

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1196262
    iput-object v3, p0, LX/7Kj;->d:Landroid/widget/RemoteViews;

    .line 1196263
    :cond_0
    iget-object v0, p0, LX/7Kj;->e:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_1

    .line 1196264
    iget-object v0, p0, LX/7Kj;->e:Landroid/widget/RemoteViews;

    const v1, 0x7f0d08e9

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1196265
    iput-object v3, p0, LX/7Kj;->e:Landroid/widget/RemoteViews;

    .line 1196266
    :cond_1
    return-void
.end method
