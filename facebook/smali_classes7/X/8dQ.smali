.class public final LX/8dQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:D

.field public C:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Z

.field public G:I

.field public H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:I

.field public L:I

.field public M:I

.field public N:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Z

.field public P:Z

.field public Q:Z

.field public R:Z

.field public S:Z

.field public T:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:I

.field public W:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$LocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:D

.field public aB:D

.field public aC:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aD:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:I

.field public aF:J

.field public aG:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:Z

.field public aM:Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aN:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aO:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aR:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aS:I

.field public aa:I

.field public ab:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:D

.field public af:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:I

.field public ao:I

.field public ap:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aq:D

.field public ar:D

.field public as:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public av:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aw:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ax:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ay:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public az:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SourceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$AllShareStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:J

.field public o:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:J

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Z

.field public s:Z

.field public t:J

.field public u:I

.field public v:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Z

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1375429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/8dQ;
    .locals 4

    .prologue
    .line 1375430
    new-instance v0, LX/8dQ;

    invoke-direct {v0}, LX/8dQ;-><init>()V

    .line 1375431
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1375432
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->A()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->b:Ljava/lang/String;

    .line 1375433
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aZ()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->c:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    .line 1375434
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ba()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$AllShareStoriesModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->d:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$AllShareStoriesModel;

    .line 1375435
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->c()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->e:LX/0Px;

    .line 1375436
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bb()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->f:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    .line 1375437
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->T()Z

    move-result v1

    iput-boolean v1, v0, LX/8dQ;->g:Z

    .line 1375438
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->U()Z

    move-result v1

    iput-boolean v1, v0, LX/8dQ;->h:Z

    .line 1375439
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->u()Z

    move-result v1

    iput-boolean v1, v0, LX/8dQ;->i:Z

    .line 1375440
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->v()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->j:LX/0Px;

    .line 1375441
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->k:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1375442
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->dV_()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->l:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1375443
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bc()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->m:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 1375444
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->V()J

    move-result-wide v2

    iput-wide v2, v0, LX/8dQ;->n:J

    .line 1375445
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->o:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1375446
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->W()J

    move-result-wide v2

    iput-wide v2, v0, LX/8dQ;->p:J

    .line 1375447
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->X()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->q:LX/0Px;

    .line 1375448
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->w()Z

    move-result v1

    iput-boolean v1, v0, LX/8dQ;->r:Z

    .line 1375449
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->Y()Z

    move-result v1

    iput-boolean v1, v0, LX/8dQ;->s:Z

    .line 1375450
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->Z()J

    move-result-wide v2

    iput-wide v2, v0, LX/8dQ;->t:J

    .line 1375451
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aa()I

    move-result v1

    iput v1, v0, LX/8dQ;->u:I

    .line 1375452
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bd()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->v:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    .line 1375453
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->be()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->w:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    .line 1375454
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ac()Z

    move-result v1

    iput-boolean v1, v0, LX/8dQ;->x:Z

    .line 1375455
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->O()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->y:Ljava/lang/String;

    .line 1375456
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bf()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->z:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    .line 1375457
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ae()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->A:Ljava/lang/String;

    .line 1375458
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->af()D

    move-result-wide v2

    iput-wide v2, v0, LX/8dQ;->B:D

    .line 1375459
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->C:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1375460
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bg()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->D:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    .line 1375461
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bh()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->E:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 1375462
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ah()Z

    move-result v1

    iput-boolean v1, v0, LX/8dQ;->F:Z

    .line 1375463
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ai()I

    move-result v1

    iput v1, v0, LX/8dQ;->G:I

    .line 1375464
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->dW_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->H:Ljava/lang/String;

    .line 1375465
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bi()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ImageModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->I:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ImageModel;

    .line 1375466
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bj()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1375467
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ak()I

    move-result v1

    iput v1, v0, LX/8dQ;->K:I

    .line 1375468
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->al()I

    move-result v1

    iput v1, v0, LX/8dQ;->L:I

    .line 1375469
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->am()I

    move-result v1

    iput v1, v0, LX/8dQ;->M:I

    .line 1375470
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bk()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->N:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    .line 1375471
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ao()Z

    move-result v1

    iput-boolean v1, v0, LX/8dQ;->O:Z

    .line 1375472
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ap()Z

    move-result v1

    iput-boolean v1, v0, LX/8dQ;->P:Z

    .line 1375473
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aq()Z

    move-result v1

    iput-boolean v1, v0, LX/8dQ;->Q:Z

    .line 1375474
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ar()Z

    move-result v1

    iput-boolean v1, v0, LX/8dQ;->R:Z

    .line 1375475
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->o()Z

    move-result v1

    iput-boolean v1, v0, LX/8dQ;->S:Z

    .line 1375476
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bl()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->T:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    .line 1375477
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bm()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->U:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;

    .line 1375478
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->au()I

    move-result v1

    iput v1, v0, LX/8dQ;->V:I

    .line 1375479
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bn()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$LocationModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->W:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$LocationModel;

    .line 1375480
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bo()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->X:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1375481
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bp()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->Y:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    .line 1375482
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ax()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->Z:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1375483
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ay()I

    move-result v1

    iput v1, v0, LX/8dQ;->aa:I

    .line 1375484
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->ab:Ljava/lang/String;

    .line 1375485
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bq()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->ac:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    .line 1375486
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->ad:Ljava/lang/String;

    .line 1375487
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aA()D

    move-result-wide v2

    iput-wide v2, v0, LX/8dQ;->ae:D

    .line 1375488
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->br()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->af:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    .line 1375489
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bs()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->ag:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    .line 1375490
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bt()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->ah:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    .line 1375491
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bu()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->ai:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    .line 1375492
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bv()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aj:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    .line 1375493
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bw()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->ak:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 1375494
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bx()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->al:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    .line 1375495
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->H()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->am:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 1375496
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aE()I

    move-result v1

    iput v1, v0, LX/8dQ;->an:I

    .line 1375497
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aF()I

    move-result v1

    iput v1, v0, LX/8dQ;->ao:I

    .line 1375498
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->I()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->ap:Ljava/lang/String;

    .line 1375499
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aG()D

    move-result-wide v2

    iput-wide v2, v0, LX/8dQ;->aq:D

    .line 1375500
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aH()D

    move-result-wide v2

    iput-wide v2, v0, LX/8dQ;->ar:D

    .line 1375501
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->by()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->as:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1375502
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aI()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->at:Ljava/lang/String;

    .line 1375503
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bz()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->au:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    .line 1375504
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bA()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->av:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;

    .line 1375505
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bB()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aw:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    .line 1375506
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->J()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->ax:LX/0Px;

    .line 1375507
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bC()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->ay:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    .line 1375508
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bD()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SourceModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->az:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SourceModel;

    .line 1375509
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aN()D

    move-result-wide v2

    iput-wide v2, v0, LX/8dQ;->aA:D

    .line 1375510
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aO()D

    move-result-wide v2

    iput-wide v2, v0, LX/8dQ;->aB:D

    .line 1375511
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aP()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aC:Ljava/lang/String;

    .line 1375512
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aQ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aD:Ljava/lang/String;

    .line 1375513
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aR()I

    move-result v1

    iput v1, v0, LX/8dQ;->aE:I

    .line 1375514
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aS()J

    move-result-wide v2

    iput-wide v2, v0, LX/8dQ;->aF:J

    .line 1375515
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bE()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aG:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;

    .line 1375516
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aH:Ljava/lang/String;

    .line 1375517
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bF()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$TitleModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aI:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$TitleModel;

    .line 1375518
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aV()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aJ:Ljava/lang/String;

    .line 1375519
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->z()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aK:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1375520
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aW()Z

    move-result v1

    iput-boolean v1, v0, LX/8dQ;->aL:Z

    .line 1375521
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bG()Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aM:Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;

    .line 1375522
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aN:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1375523
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aO:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1375524
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->K()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aP:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1375525
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aQ:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1375526
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bH()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v1

    iput-object v1, v0, LX/8dQ;->aR:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    .line 1375527
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aY()I

    move-result v1

    iput v1, v0, LX/8dQ;->aS:I

    .line 1375528
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;
    .locals 67

    .prologue
    .line 1375529
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1375530
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8dQ;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1375531
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8dQ;->b:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1375532
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8dQ;->c:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1375533
    move-object/from16 v0, p0

    iget-object v6, v0, LX/8dQ;->d:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$AllShareStoriesModel;

    invoke-static {v2, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1375534
    move-object/from16 v0, p0

    iget-object v7, v0, LX/8dQ;->e:LX/0Px;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/util/List;)I

    move-result v7

    .line 1375535
    move-object/from16 v0, p0

    iget-object v8, v0, LX/8dQ;->f:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1375536
    move-object/from16 v0, p0

    iget-object v9, v0, LX/8dQ;->j:LX/0Px;

    invoke-virtual {v2, v9}, LX/186;->b(Ljava/util/List;)I

    move-result v9

    .line 1375537
    move-object/from16 v0, p0

    iget-object v10, v0, LX/8dQ;->k:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v2, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 1375538
    move-object/from16 v0, p0

    iget-object v11, v0, LX/8dQ;->l:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-virtual {v2, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 1375539
    move-object/from16 v0, p0

    iget-object v12, v0, LX/8dQ;->m:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    invoke-static {v2, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1375540
    move-object/from16 v0, p0

    iget-object v13, v0, LX/8dQ;->o:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1375541
    move-object/from16 v0, p0

    iget-object v14, v0, LX/8dQ;->q:LX/0Px;

    invoke-virtual {v2, v14}, LX/186;->c(Ljava/util/List;)I

    move-result v14

    .line 1375542
    move-object/from16 v0, p0

    iget-object v15, v0, LX/8dQ;->v:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    invoke-static {v2, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1375543
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->w:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1375544
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->y:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 1375545
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->z:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1375546
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->A:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 1375547
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->C:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v20

    .line 1375548
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->D:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1375549
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->E:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1375550
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->H:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 1375551
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->I:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ImageModel;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 1375552
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 1375553
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->N:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1375554
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->T:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 1375555
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->U:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 1375556
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->W:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$LocationModel;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 1375557
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->X:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 1375558
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->Y:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 1375559
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->Z:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v32

    .line 1375560
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->ab:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    .line 1375561
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->ac:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 1375562
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->ad:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    .line 1375563
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->af:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 1375564
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->ag:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 1375565
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->ah:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 1375566
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->ai:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 1375567
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aj:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 1375568
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->ak:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 1375569
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->al:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 1375570
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->am:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v43

    .line 1375571
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->ap:Ljava/lang/String;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v44

    .line 1375572
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->as:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 1375573
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->at:Ljava/lang/String;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v46

    .line 1375574
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->au:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-object/from16 v47, v0

    move-object/from16 v0, v47

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 1375575
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->av:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v48

    .line 1375576
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aw:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-object/from16 v49, v0

    move-object/from16 v0, v49

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v49

    .line 1375577
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->ax:LX/0Px;

    move-object/from16 v50, v0

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v50

    .line 1375578
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->ay:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-object/from16 v51, v0

    move-object/from16 v0, v51

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 1375579
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->az:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SourceModel;

    move-object/from16 v52, v0

    move-object/from16 v0, v52

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 1375580
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aC:Ljava/lang/String;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v53

    .line 1375581
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aD:Ljava/lang/String;

    move-object/from16 v54, v0

    move-object/from16 v0, v54

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v54

    .line 1375582
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aG:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v55

    .line 1375583
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aH:Ljava/lang/String;

    move-object/from16 v56, v0

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v56

    .line 1375584
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aI:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$TitleModel;

    move-object/from16 v57, v0

    move-object/from16 v0, v57

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v57

    .line 1375585
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aJ:Ljava/lang/String;

    move-object/from16 v58, v0

    move-object/from16 v0, v58

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v58

    .line 1375586
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aK:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object/from16 v59, v0

    move-object/from16 v0, v59

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v59

    .line 1375587
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aM:Lcom/facebook/search/results/protocol/video/SearchResultsWebVideoModels$SearchResultsWebVideoModel$VideoShareModel;

    move-object/from16 v60, v0

    move-object/from16 v0, v60

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v60

    .line 1375588
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aN:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-object/from16 v61, v0

    move-object/from16 v0, v61

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v61

    .line 1375589
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aO:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-object/from16 v62, v0

    move-object/from16 v0, v62

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v62

    .line 1375590
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aP:Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-object/from16 v63, v0

    move-object/from16 v0, v63

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v63

    .line 1375591
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aQ:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object/from16 v64, v0

    move-object/from16 v0, v64

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v64

    .line 1375592
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8dQ;->aR:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-object/from16 v65, v0

    move-object/from16 v0, v65

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v65

    .line 1375593
    const/16 v66, 0x61

    move/from16 v0, v66

    invoke-virtual {v2, v0}, LX/186;->c(I)V

    .line 1375594
    const/16 v66, 0x0

    move/from16 v0, v66

    invoke-virtual {v2, v0, v3}, LX/186;->b(II)V

    .line 1375595
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v4}, LX/186;->b(II)V

    .line 1375596
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1375597
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v6}, LX/186;->b(II)V

    .line 1375598
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1375599
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1375600
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dQ;->g:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1375601
    const/4 v3, 0x7

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dQ;->h:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1375602
    const/16 v3, 0x8

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dQ;->i:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1375603
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1375604
    const/16 v3, 0xa

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1375605
    const/16 v3, 0xb

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1375606
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 1375607
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dQ;->n:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1375608
    const/16 v3, 0xe

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 1375609
    const/16 v3, 0xf

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dQ;->p:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1375610
    const/16 v3, 0x10

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 1375611
    const/16 v3, 0x11

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dQ;->r:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1375612
    const/16 v3, 0x12

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dQ;->s:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1375613
    const/16 v3, 0x13

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dQ;->t:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1375614
    const/16 v3, 0x14

    move-object/from16 v0, p0

    iget v4, v0, LX/8dQ;->u:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1375615
    const/16 v3, 0x15

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 1375616
    const/16 v3, 0x16

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375617
    const/16 v3, 0x17

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dQ;->x:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1375618
    const/16 v3, 0x18

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375619
    const/16 v3, 0x19

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375620
    const/16 v3, 0x1a

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375621
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dQ;->B:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1375622
    const/16 v3, 0x1c

    move/from16 v0, v20

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375623
    const/16 v3, 0x1d

    move/from16 v0, v21

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375624
    const/16 v3, 0x1e

    move/from16 v0, v22

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375625
    const/16 v3, 0x1f

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dQ;->F:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1375626
    const/16 v3, 0x20

    move-object/from16 v0, p0

    iget v4, v0, LX/8dQ;->G:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1375627
    const/16 v3, 0x21

    move/from16 v0, v23

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375628
    const/16 v3, 0x22

    move/from16 v0, v24

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375629
    const/16 v3, 0x23

    move/from16 v0, v25

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375630
    const/16 v3, 0x24

    move-object/from16 v0, p0

    iget v4, v0, LX/8dQ;->K:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1375631
    const/16 v3, 0x25

    move-object/from16 v0, p0

    iget v4, v0, LX/8dQ;->L:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1375632
    const/16 v3, 0x26

    move-object/from16 v0, p0

    iget v4, v0, LX/8dQ;->M:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1375633
    const/16 v3, 0x27

    move/from16 v0, v26

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375634
    const/16 v3, 0x28

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dQ;->O:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1375635
    const/16 v3, 0x29

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dQ;->P:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1375636
    const/16 v3, 0x2a

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dQ;->Q:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1375637
    const/16 v3, 0x2b

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dQ;->R:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1375638
    const/16 v3, 0x2c

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dQ;->S:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1375639
    const/16 v3, 0x2d

    move/from16 v0, v27

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375640
    const/16 v3, 0x2e

    move/from16 v0, v28

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375641
    const/16 v3, 0x2f

    move-object/from16 v0, p0

    iget v4, v0, LX/8dQ;->V:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1375642
    const/16 v3, 0x30

    move/from16 v0, v29

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375643
    const/16 v3, 0x31

    move/from16 v0, v30

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375644
    const/16 v3, 0x32

    move/from16 v0, v31

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375645
    const/16 v3, 0x33

    move/from16 v0, v32

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375646
    const/16 v3, 0x34

    move-object/from16 v0, p0

    iget v4, v0, LX/8dQ;->aa:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1375647
    const/16 v3, 0x35

    move/from16 v0, v33

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375648
    const/16 v3, 0x36

    move/from16 v0, v34

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375649
    const/16 v3, 0x37

    move/from16 v0, v35

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375650
    const/16 v3, 0x38

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dQ;->ae:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1375651
    const/16 v3, 0x39

    move/from16 v0, v36

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375652
    const/16 v3, 0x3a

    move/from16 v0, v37

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375653
    const/16 v3, 0x3b

    move/from16 v0, v38

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375654
    const/16 v3, 0x3c

    move/from16 v0, v39

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375655
    const/16 v3, 0x3d

    move/from16 v0, v40

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375656
    const/16 v3, 0x3e

    move/from16 v0, v41

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375657
    const/16 v3, 0x3f

    move/from16 v0, v42

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375658
    const/16 v3, 0x40

    move/from16 v0, v43

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375659
    const/16 v3, 0x41

    move-object/from16 v0, p0

    iget v4, v0, LX/8dQ;->an:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1375660
    const/16 v3, 0x42

    move-object/from16 v0, p0

    iget v4, v0, LX/8dQ;->ao:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1375661
    const/16 v3, 0x43

    move/from16 v0, v44

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375662
    const/16 v3, 0x44

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dQ;->aq:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1375663
    const/16 v3, 0x45

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dQ;->ar:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1375664
    const/16 v3, 0x46

    move/from16 v0, v45

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375665
    const/16 v3, 0x47

    move/from16 v0, v46

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375666
    const/16 v3, 0x48

    move/from16 v0, v47

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375667
    const/16 v3, 0x49

    move/from16 v0, v48

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375668
    const/16 v3, 0x4a

    move/from16 v0, v49

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375669
    const/16 v3, 0x4b

    move/from16 v0, v50

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375670
    const/16 v3, 0x4c

    move/from16 v0, v51

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375671
    const/16 v3, 0x4d

    move/from16 v0, v52

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375672
    const/16 v3, 0x4e

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dQ;->aA:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1375673
    const/16 v3, 0x4f

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dQ;->aB:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1375674
    const/16 v3, 0x50

    move/from16 v0, v53

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375675
    const/16 v3, 0x51

    move/from16 v0, v54

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375676
    const/16 v3, 0x52

    move-object/from16 v0, p0

    iget v4, v0, LX/8dQ;->aE:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1375677
    const/16 v3, 0x53

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8dQ;->aF:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1375678
    const/16 v3, 0x54

    move/from16 v0, v55

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375679
    const/16 v3, 0x55

    move/from16 v0, v56

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375680
    const/16 v3, 0x56

    move/from16 v0, v57

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375681
    const/16 v3, 0x57

    move/from16 v0, v58

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375682
    const/16 v3, 0x58

    move/from16 v0, v59

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375683
    const/16 v3, 0x59

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8dQ;->aL:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1375684
    const/16 v3, 0x5a

    move/from16 v0, v60

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375685
    const/16 v3, 0x5b

    move/from16 v0, v61

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375686
    const/16 v3, 0x5c

    move/from16 v0, v62

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375687
    const/16 v3, 0x5d

    move/from16 v0, v63

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375688
    const/16 v3, 0x5e

    move/from16 v0, v64

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375689
    const/16 v3, 0x5f

    move/from16 v0, v65

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1375690
    const/16 v3, 0x60

    move-object/from16 v0, p0

    iget v4, v0, LX/8dQ;->aS:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1375691
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1375692
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1375693
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1375694
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1375695
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1375696
    new-instance v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;-><init>(LX/15i;)V

    .line 1375697
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8dQ;->o:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    .line 1375698
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8dQ;->o:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->L_()LX/0x2;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/graphql/model/GraphQLStory;->a(LX/0x2;)V

    .line 1375699
    :cond_0
    return-object v3
.end method
