.class public LX/8cB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/8bj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1374027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1374028
    return-void
.end method


# virtual methods
.method public final a(LX/15i;I)LX/7C2;
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "from"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 1374029
    if-nez p2, :cond_0

    .line 1374030
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    const-string v2, "null edge for keyword suggestions"

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1374031
    :cond_0
    invoke-virtual {p1, p2, v3}, LX/15i;->g(II)I

    move-result v1

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1374032
    if-nez v1, :cond_1

    .line 1374033
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    const-string v2, "Missing node for bootstrap suggestion!"

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1374034
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1374035
    :cond_1
    new-instance v0, LX/7C1;

    invoke-direct {v0}, LX/7C1;-><init>()V

    const/4 v2, 0x5

    invoke-virtual {p1, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 1374036
    iput-object v2, v0, LX/7C1;->a:Ljava/lang/String;

    .line 1374037
    move-object v0, v0

    .line 1374038
    const/4 v2, 0x4

    invoke-virtual {p1, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 1374039
    iput-object v2, v0, LX/7C1;->c:Ljava/lang/String;

    .line 1374040
    move-object v2, v0

    .line 1374041
    invoke-virtual {p1, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "keyword"

    .line 1374042
    :goto_0
    iput-object v0, v2, LX/7C1;->b:Ljava/lang/String;

    .line 1374043
    move-object v0, v2

    .line 1374044
    iget-object v2, p0, LX/8cB;->a:LX/8bj;

    .line 1374045
    const/4 v4, 0x5

    invoke-virtual {p1, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    .line 1374046
    iget-object p0, v2, LX/8bj;->c:LX/7CU;

    invoke-virtual {p0, v4}, LX/7CU;->b(Ljava/lang/String;)LX/0Px;

    move-result-object v4

    move-object v2, v4

    .line 1374047
    if-nez v2, :cond_2

    .line 1374048
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1374049
    :cond_2
    iput-object v2, v0, LX/7C1;->f:LX/0Px;

    .line 1374050
    move-object v0, v0

    .line 1374051
    const/4 v2, 0x6

    invoke-virtual {p1, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 1374052
    iput-object v2, v0, LX/7C1;->d:Ljava/lang/String;

    .line 1374053
    move-object v0, v0

    .line 1374054
    invoke-virtual {p1, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 1374055
    iput-object v2, v0, LX/7C1;->e:Ljava/lang/String;

    .line 1374056
    move-object v0, v0

    .line 1374057
    const/4 v2, 0x2

    invoke-virtual {p1, v1, v2}, LX/15i;->l(II)D

    move-result-wide v2

    .line 1374058
    iput-wide v2, v0, LX/7C1;->g:D

    .line 1374059
    move-object v0, v0

    .line 1374060
    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 1374061
    iput-object v1, v0, LX/7C1;->h:Ljava/lang/String;

    .line 1374062
    move-object v0, v0

    .line 1374063
    new-instance v1, LX/7C2;

    invoke-direct {v1, v0}, LX/7C2;-><init>(LX/7C1;)V

    .line 1374064
    invoke-static {v1}, LX/7C2;->i(LX/7C2;)V

    .line 1374065
    move-object v0, v1

    .line 1374066
    return-object v0

    .line 1374067
    :cond_3
    invoke-virtual {p1, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
