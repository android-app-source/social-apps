.class public LX/7mB;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1236590
    const-string v0, "2"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/7mB;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1236588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0Px;LX/0Px;LX/0Px;)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1236591
    new-instance v6, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1236592
    invoke-static {p0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v3

    .line 1236593
    :goto_0
    invoke-static {p2}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v4, v3

    .line 1236594
    :goto_1
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    .line 1236595
    :goto_2
    add-int v1, v2, v4

    if-ne v1, v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    move v5, v3

    .line 1236596
    :goto_4
    if-ge v5, v4, :cond_4

    .line 1236597
    new-instance v7, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 1236598
    invoke-virtual {p2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1236599
    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1236600
    const-string v8, "sync_object_uuid"

    invoke-virtual {v7, v8, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236601
    const-string v0, "message"

    invoke-virtual {v7, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236602
    const-string v0, "notify_when_processed"

    invoke-virtual {v7, v0, v3}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1236603
    invoke-virtual {v6, v7}, LX/162;->a(LX/0lF;)LX/162;

    .line 1236604
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_4

    .line 1236605
    :cond_0
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    move v2, v0

    goto :goto_0

    .line 1236606
    :cond_1
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    move v4, v0

    goto :goto_1

    .line 1236607
    :cond_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    goto :goto_2

    :cond_3
    move v0, v3

    .line 1236608
    goto :goto_3

    .line 1236609
    :cond_4
    :goto_5
    if-ge v3, v2, :cond_5

    .line 1236610
    new-instance v5, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 1236611
    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1236612
    add-int v1, v3, v4

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1236613
    const-string v7, "media_fbid"

    invoke-virtual {v5, v7, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236614
    const-string v0, "message"

    invoke-virtual {v5, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236615
    invoke-virtual {v6, v5}, LX/162;->a(LX/0lF;)LX/162;

    .line 1236616
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1236617
    :cond_5
    invoke-virtual {v6}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1236589
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x2c

    invoke-static {v1}, LX/0PO;->on(C)LX/0PO;

    move-result-object v1

    invoke-virtual {v1}, LX/0PO;->skipNulls()LX/0PO;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/composer/publish/common/PublishPostParams;LX/0SG;LX/0lB;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/publish/common/PublishPostParams;",
            "LX/0SG;",
            "LX/0lB;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 1236433
    iget-wide v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_37

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1236434
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1236435
    iget-wide v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    iget-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->userId:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1236436
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1236437
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "privacy"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236438
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionLoggingData:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    if-eqz v1, :cond_1

    .line 1236439
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "composer_session_events_log"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionLoggingData:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    invoke-virtual {p2, v3}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236440
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->link:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1236441
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "link"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->link:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236442
    :cond_2
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->quote:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1236443
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "quote"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->quote:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236444
    :cond_3
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->title:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_38

    .line 1236445
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "title"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->title:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236446
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->souvenir:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1236447
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "message"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236448
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeTag:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1236449
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "place"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeTag:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236450
    :cond_5
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->nectarModule:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1236451
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "nectar_module"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->nectarModule:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236452
    :cond_6
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->taggedIds:LX/0Px;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->taggedIds:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 1236453
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tags"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->taggedIds:LX/0Px;

    invoke-static {v3}, LX/7mB;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236454
    :cond_7
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->publishMode:LX/5Rn;

    sget-object v2, LX/5Rn;->NORMAL:LX/5Rn;

    if-eq v1, v2, :cond_8

    .line 1236455
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "published"

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236456
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "unpublished_content_type"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->publishMode:LX/5Rn;

    invoke-virtual {v3}, LX/5Rn;->getContentType()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236457
    :cond_8
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1236458
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "composer_session_id"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236459
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "qn"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236460
    :cond_9
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->idempotenceToken:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1236461
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "idempotence_token"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->idempotenceToken:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236462
    :cond_a
    iget-wide v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->schedulePublishTime:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_b

    .line 1236463
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "scheduled_publish_time"

    iget-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->schedulePublishTime:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236464
    :cond_b
    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isTagsUserSelected:Z

    if-eqz v1, :cond_c

    .line 1236465
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "user_selected_tags"

    iget-boolean v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isTagsUserSelected:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236466
    :cond_c
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ref:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 1236467
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "ref"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ref:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236468
    :cond_d
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppId:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 1236469
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "proxied_app_id"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppId:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236470
    :cond_e
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppName:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 1236471
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "proxied_app_name"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppName:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236472
    :cond_f
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->androidKeyHash:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 1236473
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "android_key_hash"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->androidKeyHash:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236474
    :cond_10
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    if-eqz v1, :cond_16

    .line 1236475
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iget-object v1, v1, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogActionTypeId:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 1236476
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "og_action_type_id"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iget-object v3, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogActionTypeId:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236477
    :cond_11
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iget-object v1, v1, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogObjectId:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 1236478
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "og_object_id"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iget-object v3, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogObjectId:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236479
    :cond_12
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iget-object v1, v1, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogPhrase:Ljava/lang/String;

    if-eqz v1, :cond_13

    .line 1236480
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "og_phrase"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iget-object v3, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogPhrase:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236481
    :cond_13
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iget-object v1, v1, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogIconId:Ljava/lang/String;

    if-eqz v1, :cond_14

    .line 1236482
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "og_icon_id"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iget-object v3, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogIconId:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236483
    :cond_14
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iget-object v1, v1, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogSuggestionMechanism:Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 1236484
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "og_suggestion_mechanism"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iget-object v3, v3, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogSuggestionMechanism:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236485
    :cond_15
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iget-boolean v1, v1, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogHideAttachment:Z

    if-eqz v1, :cond_16

    .line 1236486
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "og_hide_object_attachment"

    const-string v3, "true"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236487
    :cond_16
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "audience_exp"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236488
    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isExplicitLocation:Z

    if-eqz v1, :cond_17

    .line 1236489
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_explicit_location"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236490
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "user_selected_place"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236491
    :cond_17
    iget-wide v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->originalPostTime:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_18

    .line 1236492
    invoke-interface {p1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1236493
    iget-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->originalPostTime:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 1236494
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "time_since_original_post"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236495
    :cond_18
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "attach_place_suggestion"

    iget-boolean v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->b:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236496
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->c:Lcom/facebook/ipc/composer/model/ComposerLocation;

    if-eqz v1, :cond_19

    .line 1236497
    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-virtual {v1}, LX/0mC;->c()LX/0m9;

    move-result-object v1

    .line 1236498
    const-string v2, "latitude"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->c:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iget-wide v4, v3, Lcom/facebook/ipc/composer/model/ComposerLocation;->latitude:D

    invoke-virtual {v1, v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 1236499
    const-string v2, "longitude"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->c:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iget-wide v4, v3, Lcom/facebook/ipc/composer/model/ComposerLocation;->longitude:D

    invoke-virtual {v1, v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 1236500
    const-string v2, "accuracy"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->c:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iget v3, v3, Lcom/facebook/ipc/composer/model/ComposerLocation;->accuracy:F

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;F)LX/0m9;

    .line 1236501
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "viewer_coordinates"

    invoke-virtual {v1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236502
    :cond_19
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    if-eqz v1, :cond_1a

    .line 1236503
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-virtual {p2, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1236504
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "product_item"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236505
    :cond_1a
    iget-wide v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mMarketplaceId:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_1b

    .line 1236506
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "marketplace_id"

    iget-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mMarketplaceId:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236507
    :cond_1b
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236508
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->connectionClass:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1c

    .line 1236509
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "connection_class"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->connectionClass:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236510
    :cond_1c
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->textOnlyPlace:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 1236511
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "text_only_place"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->textOnlyPlace:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236512
    :cond_1d
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->sponsorId:Ljava/lang/String;

    if-eqz v1, :cond_1e

    .line 1236513
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "sponsor_id"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->sponsorId:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236514
    :cond_1e
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->sourceType:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1f

    .line 1236515
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "source_type"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->sourceType:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236516
    :cond_1f
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->throwbackCardJson:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_20

    .line 1236517
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "throwback_card"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->throwbackCardJson:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236518
    :cond_20
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->stickerId:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_21

    .line 1236519
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "referenced_sticker_id"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->stickerId:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236520
    :cond_21
    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeAttachmentRemoved:Z

    if-eqz v1, :cond_22

    .line 1236521
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "place_attachment_setting"

    const-string v3, "1"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236522
    :cond_22
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "checkin_entry_point"

    iget-boolean v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isCheckin:Z

    invoke-static {v3}, LX/5RB;->a(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236523
    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isBackoutDraft:Z

    if-eqz v1, :cond_23

    .line 1236524
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_backout_draft"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236525
    :cond_23
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->f:LX/0Px;

    if-eqz v1, :cond_39

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->f:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_39

    .line 1236526
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "attached_media"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->f:LX/0Px;

    const/4 p2, 0x1

    .line 1236527
    new-instance v6, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v4}, LX/162;-><init>(LX/0mC;)V

    .line 1236528
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    if-ge v5, v7, :cond_2d

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/publish/common/MediaAttachment;

    .line 1236529
    new-instance v8, LX/0m9;

    sget-object v9, LX/0mC;->a:LX/0mC;

    invoke-direct {v8, v9}, LX/0m9;-><init>(LX/0mC;)V

    .line 1236530
    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment;->getMediaFbid()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_24

    .line 1236531
    const-string v9, "media_fbid"

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment;->getMediaFbid()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v8, v9, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236532
    :cond_24
    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment;->getSyncObjectUuid()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_25

    .line 1236533
    const-string v9, "sync_object_uuid"

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment;->getSyncObjectUuid()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v8, v9, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236534
    :cond_25
    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_26

    .line 1236535
    const-string v9, "message"

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v8, v9, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236536
    :cond_26
    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment;->getStickers()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_27

    .line 1236537
    const-string v9, "stickers"

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment;->getStickers()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v8, v9, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236538
    :cond_27
    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment;->getTextOverlay()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_28

    .line 1236539
    const-string v9, "text_overlay"

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment;->getTextOverlay()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v8, v9, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236540
    :cond_28
    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment;->getHasCrop()Z

    move-result v9

    if-eqz v9, :cond_29

    .line 1236541
    const-string v9, "has_crop"

    invoke-virtual {v8, v9, p2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1236542
    :cond_29
    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment;->getHasFilter()Z

    move-result v9

    if-eqz v9, :cond_2a

    .line 1236543
    const-string v9, "has_filter"

    invoke-virtual {v8, v9, p2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1236544
    :cond_2a
    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment;->getHasRotation()Z

    move-result v9

    if-eqz v9, :cond_2b

    .line 1236545
    const-string v9, "has_rotation"

    invoke-virtual {v8, v9, p2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1236546
    :cond_2b
    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment;->getNotifyWhenProcessed()Z

    move-result v4

    if-eqz v4, :cond_2c

    .line 1236547
    const-string v4, "notify_when_processed"

    invoke-virtual {v8, v4, p2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1236548
    :cond_2c
    invoke-virtual {v6, v8}, LX/162;->a(LX/0lF;)LX/162;

    .line 1236549
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_2

    .line 1236550
    :cond_2d
    invoke-virtual {v6}, LX/162;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 1236551
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236552
    :cond_2e
    :goto_3
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->souvenir:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2f

    .line 1236553
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "souvenir"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->souvenir:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236554
    :cond_2f
    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->warnAcknowledged:Z

    if-eqz v1, :cond_30

    .line 1236555
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "warn_ack"

    iget-boolean v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->warnAcknowledged:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236556
    :cond_30
    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->canHandleSentryWarning:Z

    if-eqz v1, :cond_31

    .line 1236557
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "warn_can_handle"

    iget-boolean v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->canHandleSentryWarning:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236558
    :cond_31
    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isFeedOnlyPost:Z

    if-eqz v1, :cond_32

    .line 1236559
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "post_surfaces_blacklist"

    sget-object v3, LX/7mB;->a:LX/0Px;

    invoke-static {v3}, LX/16N;->b(Ljava/util/List;)LX/162;

    move-result-object v3

    invoke-virtual {v3}, LX/162;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236560
    :cond_32
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    if-eqz v1, :cond_33

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    sget-object v2, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {v1, v2}, LX/87Y;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result v1

    if-nez v1, :cond_33

    .line 1236561
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getPresetId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3b

    .line 1236562
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "text_format_preset_id"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getPresetId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236563
    :cond_33
    :goto_4
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaType:Ljava/lang/String;

    if-eqz v1, :cond_34

    .line 1236564
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "cta_type"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaType:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236565
    :cond_34
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaLink:Ljava/lang/String;

    if-eqz v1, :cond_35

    .line 1236566
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "cta_link"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaLink:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236567
    :cond_35
    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isPlacelistPost:Z

    if-eqz v1, :cond_36

    .line 1236568
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 1236569
    const-string v2, "is_place_list_post"

    iget-boolean v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isPlacelistPost:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236570
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "place_list_data"

    invoke-virtual {v1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1236571
    :cond_36
    return-object v0

    .line 1236572
    :cond_37
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1236573
    :cond_38
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1236574
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "message"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1236575
    :cond_39
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaFbIds:LX/0Px;

    if-nez v1, :cond_3a

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->syncObjectUUIDs:LX/0Px;

    if-eqz v1, :cond_2e

    .line 1236576
    :cond_3a
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "attached_media"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaFbIds:LX/0Px;

    iget-object v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaCaptions:LX/0Px;

    iget-object v5, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->syncObjectUUIDs:LX/0Px;

    invoke-static {v3, v4, v5}, LX/7mB;->a(LX/0Px;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 1236577
    :cond_3b
    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-virtual {v1}, LX/0mC;->c()LX/0m9;

    move-result-object v1

    .line 1236578
    const-string v2, "color"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getColor()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, LX/5MA;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236579
    const-string v2, "background_color"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, LX/5MA;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236580
    const-string v2, "text_align"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getTextAlign()LX/5RS;

    move-result-object v3

    invoke-virtual {v3}, LX/5RS;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236581
    const-string v2, "font_weight"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v3

    invoke-virtual {v3}, LX/5RY;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236582
    iget-object v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3c

    .line 1236583
    const-string v2, "background_image_name"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236584
    :cond_3c
    iget-object v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3d

    .line 1236585
    const-string v2, "background_gradient_color"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236586
    const-string v2, "background_gradient_direction"

    iget-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientDirection()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1236587
    :cond_3d
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "text_format_metadata"

    invoke-virtual {v1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4
.end method
