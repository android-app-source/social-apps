.class public final LX/8Ri;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 1344777
    iput-object p1, p0, LX/8Ri;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 5

    .prologue
    .line 1344778
    iget-object v0, p0, LX/8Ri;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->d:LX/8RJ;

    .line 1344779
    iget-boolean v1, v0, LX/8RJ;->e:Z

    if-eqz v1, :cond_0

    .line 1344780
    iget-object v1, v0, LX/8RJ;->b:LX/11i;

    sget-object v2, LX/8RJ;->a:LX/8RH;

    invoke-interface {v1, v2}, LX/11i;->b(LX/0Pq;)V

    .line 1344781
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/8RJ;->e:Z

    .line 1344782
    :goto_0
    iget-object v0, p0, LX/8Ri;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1344783
    const/4 v0, 0x1

    return v0

    .line 1344784
    :cond_0
    iget-object v1, v0, LX/8RJ;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-class v2, LX/8RJ;

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Trying to terminate a perf sequence without starting it. This is likely caused by calling AudienceTypeaheadFragment without calling AudienceSelectorPerformanceLogger.onOpenAudienceSelector() to track perf."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
