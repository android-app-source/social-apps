.class public LX/8Jy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Jv;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/8Jw;

.field private c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1329690
    const-class v0, LX/8Jy;

    sput-object v0, LX/8Jy;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1329691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1329692
    const/4 v0, 0x0

    iput-object v0, p0, LX/8Jy;->b:LX/8Jw;

    .line 1329693
    const/4 v0, 0x0

    iput v0, p0, LX/8Jy;->c:I

    .line 1329694
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 1329695
    iget-object v0, p0, LX/8Jy;->b:LX/8Jw;

    if-eqz v0, :cond_1

    .line 1329696
    iget-object v0, p0, LX/8Jy;->b:LX/8Jw;

    .line 1329697
    iget-object v1, v0, LX/8Jw;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v3

    .line 1329698
    invoke-static {v0}, LX/8Jw;->d(LX/8Jw;)Ljava/io/File;

    move-result-object v2

    .line 1329699
    if-eqz v2, :cond_0

    .line 1329700
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1329701
    invoke-virtual {v2, v3, v4}, Ljava/io/File;->setLastModified(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1329702
    sget-object v1, LX/8Jw;->b:Ljava/lang/Class;

    const-string v5, "setLastModified failed"

    invoke-static {v1, v5}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1329703
    :cond_0
    :goto_0
    iget-object v1, v0, LX/8Jw;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v2, v0, LX/8Jw;->a:LX/0Tn;

    invoke-interface {v1, v2, v3, v4}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1329704
    :cond_1
    return-void

    .line 1329705
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1329706
    sget-object v1, LX/8Jw;->b:Ljava/lang/Class;

    const-string v5, "createNewFile failed"

    invoke-static {v1, v5}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1329707
    :catch_0
    move-exception v1

    .line 1329708
    sget-object v5, LX/8Jw;->b:Ljava/lang/Class;

    const-string v6, "Couldn\'t set file date %s, %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v8

    const/4 v2, 0x1

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v2

    invoke-static {v5, v6, v7}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1329709
    iget v0, p0, LX/8Jy;->c:I

    add-int/2addr v0, p1

    iput v0, p0, LX/8Jy;->c:I

    .line 1329710
    return-void
.end method

.method public final a(LX/8Jw;)V
    .locals 0

    .prologue
    .line 1329711
    iput-object p1, p0, LX/8Jy;->b:LX/8Jw;

    .line 1329712
    return-void
.end method

.method public final a(IILcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z
    .locals 1

    .prologue
    .line 1329713
    invoke-virtual {p3}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->ac()Z

    move-result v0

    if-nez v0, :cond_1

    if-ne p1, p2, :cond_1

    iget v0, p0, LX/8Jy;->c:I

    if-nez v0, :cond_1

    iget-object v0, p0, LX/8Jy;->b:LX/8Jw;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8Jy;->b:LX/8Jw;

    .line 1329714
    invoke-static {v0}, LX/8Jw;->e(LX/8Jw;)Z

    move-result p0

    move v0, p0

    .line 1329715
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
