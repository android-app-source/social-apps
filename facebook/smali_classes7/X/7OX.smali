.class public final LX/7OX;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1201806
    const-class v1, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;

    const v0, 0x3ad3ef7b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x3

    const-string v5, "VideoCreationStoryFromVideoQuery"

    const-string v6, "578f3a2bee690995b189caae064f373f"

    const-string v7, "videos"

    const-string v8, "10155261855281729"

    const/4 v9, 0x0

    .line 1201807
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1201808
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1201809
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1201810
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1201811
    sparse-switch v0, :sswitch_data_0

    .line 1201812
    :goto_0
    return-object p1

    .line 1201813
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1201814
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1201815
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1201816
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1201817
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1201818
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1201819
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1201820
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1201821
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1201822
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1201823
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1201824
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1201825
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1201826
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1201827
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1201828
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1201829
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1201830
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1201831
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1201832
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1201833
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_10
        -0x680de62a -> :sswitch_5
        -0x6326fdb3 -> :sswitch_8
        -0x57984ae8 -> :sswitch_12
        -0x513764de -> :sswitch_f
        -0x4496acc9 -> :sswitch_4
        -0x41a91745 -> :sswitch_c
        -0x41143822 -> :sswitch_1
        -0x3c54de38 -> :sswitch_a
        -0x3b85b241 -> :sswitch_13
        -0x25a646c8 -> :sswitch_2
        -0x1b87b280 -> :sswitch_9
        -0x15db59af -> :sswitch_11
        -0x12efdeb3 -> :sswitch_7
        -0x3e446ed -> :sswitch_3
        -0x12603b3 -> :sswitch_e
        0xa1fa812 -> :sswitch_0
        0x214100e0 -> :sswitch_6
        0x291d8de0 -> :sswitch_d
        0x44a0c75f -> :sswitch_14
        0x73a026b5 -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1201834
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1201835
    :goto_1
    return v0

    .line 1201836
    :sswitch_0
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "10"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "14"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "15"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "17"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "19"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v2, "18"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    .line 1201837
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1201838
    :pswitch_1
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1201839
    :pswitch_2
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1201840
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1201841
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1201842
    :pswitch_5
    const-string v0, "mobile"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 1201843
    :pswitch_6
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1201844
    :pswitch_7
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x32 -> :sswitch_1
        0x61f -> :sswitch_2
        0x623 -> :sswitch_3
        0x624 -> :sswitch_4
        0x626 -> :sswitch_5
        0x627 -> :sswitch_7
        0x628 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 1201845
    new-instance v0, Lcom/facebook/video/protocol/VideoQuery$VideoCreationStoryFromVideoQueryString$1;

    const-class v1, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/video/protocol/VideoQuery$VideoCreationStoryFromVideoQueryString$1;-><init>(LX/7OX;Ljava/lang/Class;)V

    return-object v0
.end method
