.class public LX/8cA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/8bj;


# direct methods
.method public constructor <init>(LX/8bj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1373955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1373956
    iput-object p1, p0, LX/8cA;->a:LX/8bj;

    .line 1373957
    return-void
.end method


# virtual methods
.method public final a(LX/8bp;)LX/7C0;
    .locals 13
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "from"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1373958
    invoke-interface {p1}, LX/8bp;->d()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    move-result-object v0

    .line 1373959
    if-nez v0, :cond_0

    .line 1373960
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    const-string v2, "Missing searchable for bootstrap suggestion!"

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1373961
    :cond_0
    new-instance v1, LX/7Bz;

    invoke-direct {v1}, LX/7Bz;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 1373962
    iput-object v2, v1, LX/7Bz;->a:Ljava/lang/String;

    .line 1373963
    move-object v1, v1

    .line 1373964
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->m()Ljava/lang/String;

    move-result-object v2

    .line 1373965
    iput-object v2, v1, LX/7Bz;->b:Ljava/lang/String;

    .line 1373966
    move-object v1, v1

    .line 1373967
    invoke-interface {p1}, LX/8bo;->b()Ljava/lang/String;

    move-result-object v2

    .line 1373968
    iput-object v2, v1, LX/7Bz;->c:Ljava/lang/String;

    .line 1373969
    move-object v1, v1

    .line 1373970
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 1373971
    iput-object v2, v1, LX/7Bz;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1373972
    move-object v1, v1

    .line 1373973
    invoke-interface {p1}, LX/8bo;->a()Ljava/lang/String;

    move-result-object v2

    .line 1373974
    iput-object v2, v1, LX/7Bz;->g:Ljava/lang/String;

    .line 1373975
    move-object v1, v1

    .line 1373976
    invoke-interface {p1}, LX/8bo;->c()Ljava/lang/String;

    move-result-object v2

    .line 1373977
    iput-object v2, v1, LX/7Bz;->f:Ljava/lang/String;

    .line 1373978
    move-object v1, v1

    .line 1373979
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->dZ_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    .line 1373980
    iput-object v2, v1, LX/7Bz;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1373981
    move-object v1, v1

    .line 1373982
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->d()Z

    move-result v2

    .line 1373983
    iput-boolean v2, v1, LX/7Bz;->k:Z

    .line 1373984
    move-object v1, v1

    .line 1373985
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->q()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    .line 1373986
    iput-object v2, v1, LX/7Bz;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1373987
    move-object v1, v1

    .line 1373988
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->l()Z

    move-result v2

    .line 1373989
    iput-boolean v2, v1, LX/7Bz;->h:Z

    .line 1373990
    move-object v1, v1

    .line 1373991
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->p()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v2

    .line 1373992
    iput-object v2, v1, LX/7Bz;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1373993
    move-object v1, v1

    .line 1373994
    iget-object v2, p0, LX/8cA;->a:LX/8bj;

    invoke-virtual {v2, v0}, LX/8bj;->a(Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/7Bz;->a(LX/0Px;)LX/7Bz;

    move-result-object v1

    invoke-interface {p1}, LX/8bp;->e()LX/2uF;

    move-result-object v2

    .line 1373995
    invoke-virtual {v2}, LX/39O;->a()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1373996
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    .line 1373997
    :cond_1
    move-wide v2, v4

    .line 1373998
    iput-wide v2, v1, LX/7Bz;->n:D

    .line 1373999
    move-object v1, v1

    .line 1374000
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->k()Z

    move-result v2

    .line 1374001
    iput-boolean v2, v1, LX/7Bz;->p:Z

    .line 1374002
    move-object v1, v1

    .line 1374003
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x25d6af

    if-ne v2, v3, :cond_4

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->e()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1374004
    const v2, 0x499e8e7

    .line 1374005
    new-instance v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v3, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1374006
    iput-object v3, v1, LX/7Bz;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1374007
    :goto_0
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x41e065f

    if-ne v2, v3, :cond_5

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->ea_()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->ea_()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;->a()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 1374008
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->ea_()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;->a()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1374009
    iput-object v2, v1, LX/7Bz;->e:Landroid/net/Uri;

    .line 1374010
    :cond_2
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1374011
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->c()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1374012
    iput-object v0, v1, LX/7Bz;->o:Ljava/lang/String;

    .line 1374013
    :cond_3
    invoke-virtual {v1}, LX/7Bz;->q()LX/7C0;

    move-result-object v0

    .line 1374014
    return-object v0

    .line 1374015
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 1374016
    iput-object v2, v1, LX/7Bz;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1374017
    goto :goto_0

    .line 1374018
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->o()LX/1Fb;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1374019
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->o()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1374020
    iput-object v2, v1, LX/7Bz;->e:Landroid/net/Uri;

    .line 1374021
    goto :goto_1

    .line 1374022
    :cond_6
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 1374023
    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v8

    :cond_7
    :goto_2
    invoke-interface {v8}, LX/2sN;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v8}, LX/2sN;->b()LX/1vs;

    move-result-object v6

    iget-object v9, v6, LX/1vs;->a:LX/15i;

    iget v10, v6, LX/1vs;->b:I

    .line 1374024
    const/4 v6, 0x0

    invoke-virtual {v9, v10, v6}, LX/15i;->l(II)D

    move-result-wide v6

    .line 1374025
    cmpg-double v11, v6, v4

    if-gez v11, :cond_7

    sget-object v11, LX/8bi;->a:LX/0Rf;

    const/4 v12, 0x1

    invoke-virtual {v9, v10, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v11, v9}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    move-wide v4, v6

    .line 1374026
    goto :goto_2
.end method
