.class public LX/83U;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1290502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/4CI;I)Landroid/animation/ValueAnimator;
    .locals 13

    .prologue
    .line 1290503
    const/4 v12, 0x1

    const/4 v9, 0x0

    .line 1290504
    iget-object v6, p0, LX/4CI;->c:LX/4C5;

    if-nez v6, :cond_2

    const/4 v6, 0x0

    :goto_0
    move v6, v6

    .line 1290505
    new-instance v7, Landroid/animation/ValueAnimator;

    invoke-direct {v7}, Landroid/animation/ValueAnimator;-><init>()V

    .line 1290506
    const/4 v8, 0x2

    new-array v8, v8, [I

    aput v9, v8, v9

    invoke-virtual {p0}, LX/4CI;->b()J

    move-result-wide v10

    long-to-int v9, v10

    aput v9, v8, v12

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 1290507
    invoke-virtual {p0}, LX/4CI;->b()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1290508
    if-eqz v6, :cond_1

    :goto_1
    invoke-virtual {v7, v6}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 1290509
    invoke-virtual {v7, v12}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 1290510
    const/4 v6, 0x0

    invoke-virtual {v7, v6}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1290511
    new-instance v6, LX/83T;

    invoke-direct {v6, p0}, LX/83T;-><init>(LX/4CI;)V

    move-object v6, v6

    .line 1290512
    invoke-virtual {v7, v6}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1290513
    move-object v0, v7

    .line 1290514
    if-nez v0, :cond_0

    .line 1290515
    const/4 v0, 0x0

    .line 1290516
    :goto_2
    return-object v0

    .line 1290517
    :cond_0
    int-to-long v2, p1

    invoke-virtual {p0}, LX/4CI;->b()J

    move-result-wide v4

    div-long/2addr v2, v4

    const-wide/16 v4, 0x1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    long-to-int v1, v2

    .line 1290518
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    goto :goto_2

    .line 1290519
    :cond_1
    const/4 v6, -0x1

    goto :goto_1

    :cond_2
    iget-object v6, p0, LX/4CI;->c:LX/4C5;

    invoke-interface {v6}, LX/4C4;->e()I

    move-result v6

    goto :goto_0
.end method
