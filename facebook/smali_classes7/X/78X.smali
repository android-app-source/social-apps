.class public final LX/78X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;)V
    .locals 0

    .prologue
    .line 1172957
    iput-object p1, p0, LX/78X;->a:Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x385b0106

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1172958
    iget-object v1, p0, LX/78X;->a:Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;

    .line 1172959
    iget-object v3, v1, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->u:LX/7F5;

    if-eqz v3, :cond_0

    .line 1172960
    iget-object v3, v1, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->u:LX/7F5;

    iget-object p1, v1, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->q:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {p1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, LX/7F5;->a(Ljava/lang/String;)V

    .line 1172961
    iget-object v3, v1, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->r:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v3}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->h()V

    .line 1172962
    :cond_0
    iget-object v1, p0, LX/78X;->a:Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;

    .line 1172963
    iget-object v3, v1, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->r:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v3}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->i()V

    .line 1172964
    iget-object v3, v1, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->r:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    sget-object p1, LX/7FJ;->COMPLETE:LX/7FJ;

    invoke-virtual {v3, p1}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(LX/7FJ;)V

    .line 1172965
    iget-object v1, p0, LX/78X;->a:Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;

    invoke-static {v1}, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->o(Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;)V

    .line 1172966
    iget-object v1, p0, LX/78X;->a:Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;

    const/4 v6, 0x0

    .line 1172967
    new-instance v3, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1172968
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f081a7e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1172969
    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 1172970
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0052

    invoke-static {v4, v5}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1172971
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a010c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1172972
    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1172973
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0062

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v6, v6, v6, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1172974
    new-instance v4, LX/0ju;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v3}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->a()LX/2EJ;

    move-result-object v3

    .line 1172975
    invoke-virtual {v3}, LX/2EJ;->show()V

    .line 1172976
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment$4;

    invoke-direct {v5, v1, v3}, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment$4;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;LX/2EJ;)V

    const-wide/16 v7, 0x7d0

    const v3, 0x7014eb8e

    invoke-static {v4, v5, v7, v8, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1172977
    const v1, -0x767b9663

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
