.class public final enum LX/7B4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7B4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7B4;

.field public static final enum AWARENESS:LX/7B4;

.field public static final enum GROUP_COMMERCE:LX/7B4;

.field public static final enum GROUP_COMMUNITY:LX/7B4;

.field public static final enum MARKETPLACE:LX/7B4;

.field public static final enum PLACE:LX/7B4;

.field public static final enum SCOPED_TAB:LX/7B4;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1177739
    new-instance v0, LX/7B4;

    const-string v1, "GROUP_COMMERCE"

    invoke-direct {v0, v1, v3}, LX/7B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7B4;->GROUP_COMMERCE:LX/7B4;

    .line 1177740
    new-instance v0, LX/7B4;

    const-string v1, "SCOPED_TAB"

    invoke-direct {v0, v1, v4}, LX/7B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7B4;->SCOPED_TAB:LX/7B4;

    .line 1177741
    new-instance v0, LX/7B4;

    const-string v1, "PLACE"

    invoke-direct {v0, v1, v5}, LX/7B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7B4;->PLACE:LX/7B4;

    .line 1177742
    new-instance v0, LX/7B4;

    const-string v1, "AWARENESS"

    invoke-direct {v0, v1, v6}, LX/7B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7B4;->AWARENESS:LX/7B4;

    .line 1177743
    new-instance v0, LX/7B4;

    const-string v1, "MARKETPLACE"

    invoke-direct {v0, v1, v7}, LX/7B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7B4;->MARKETPLACE:LX/7B4;

    .line 1177744
    new-instance v0, LX/7B4;

    const-string v1, "GROUP_COMMUNITY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7B4;->GROUP_COMMUNITY:LX/7B4;

    .line 1177745
    const/4 v0, 0x6

    new-array v0, v0, [LX/7B4;

    sget-object v1, LX/7B4;->GROUP_COMMERCE:LX/7B4;

    aput-object v1, v0, v3

    sget-object v1, LX/7B4;->SCOPED_TAB:LX/7B4;

    aput-object v1, v0, v4

    sget-object v1, LX/7B4;->PLACE:LX/7B4;

    aput-object v1, v0, v5

    sget-object v1, LX/7B4;->AWARENESS:LX/7B4;

    aput-object v1, v0, v6

    sget-object v1, LX/7B4;->MARKETPLACE:LX/7B4;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7B4;->GROUP_COMMUNITY:LX/7B4;

    aput-object v2, v0, v1

    sput-object v0, LX/7B4;->$VALUES:[LX/7B4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1177738
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7B4;
    .locals 1

    .prologue
    .line 1177737
    const-class v0, LX/7B4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7B4;

    return-object v0
.end method

.method public static values()[LX/7B4;
    .locals 1

    .prologue
    .line 1177736
    sget-object v0, LX/7B4;->$VALUES:[LX/7B4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7B4;

    return-object v0
.end method
