.class public final LX/7RC;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1205598
    const-class v1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;

    const v0, -0x698332dc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "LiveVideoBroadcastStatusUpdateQuery"

    const-string v6, "356b75b61126a618c428dcc2926df87c"

    const-string v7, "nodes"

    const-string v8, "10155210646116729"

    const/4 v9, 0x0

    .line 1205599
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1205600
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1205601
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1205602
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1205603
    sparse-switch v0, :sswitch_data_0

    .line 1205604
    :goto_0
    return-object p1

    .line 1205605
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1205606
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1205607
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1205608
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1205609
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1205610
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x25a646c8 -> :sswitch_1
        -0x2177e47b -> :sswitch_3
        -0x203dd256 -> :sswitch_0
        0x9813228 -> :sswitch_2
        0x2292beef -> :sswitch_5
        0x26d0c0ff -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1205611
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1205612
    :goto_1
    return v0

    .line 1205613
    :pswitch_0
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_1
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 1205614
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1205615
    :pswitch_3
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 1205616
    new-instance v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptions$LiveVideoBroadcastStatusUpdateQueryString$1;

    const-class v1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptions$LiveVideoBroadcastStatusUpdateQueryString$1;-><init>(LX/7RC;Ljava/lang/Class;)V

    return-object v0
.end method
