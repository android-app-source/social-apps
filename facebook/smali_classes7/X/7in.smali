.class public final LX/7in;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1228541
    const/4 v12, 0x0

    .line 1228542
    const/4 v11, 0x0

    .line 1228543
    const/4 v10, 0x0

    .line 1228544
    const/4 v9, 0x0

    .line 1228545
    const/4 v8, 0x0

    .line 1228546
    const/4 v7, 0x0

    .line 1228547
    const/4 v6, 0x0

    .line 1228548
    const/4 v5, 0x0

    .line 1228549
    const/4 v4, 0x0

    .line 1228550
    const/4 v3, 0x0

    .line 1228551
    const/4 v2, 0x0

    .line 1228552
    const/4 v1, 0x0

    .line 1228553
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1228554
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1228555
    const/4 v1, 0x0

    .line 1228556
    :goto_0
    return v1

    .line 1228557
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1228558
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_c

    .line 1228559
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1228560
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1228561
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1228562
    const-string v14, "commerce_product_visibility"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1228563
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    goto :goto_1

    .line 1228564
    :cond_2
    const-string v14, "current_product_price"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1228565
    invoke-static/range {p0 .. p1}, LX/7ir;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1228566
    :cond_3
    const-string v14, "description"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1228567
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1228568
    :cond_4
    const-string v14, "external_url"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1228569
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1228570
    :cond_5
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1228571
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1228572
    :cond_6
    const-string v14, "images"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1228573
    invoke-static/range {p0 .. p1}, LX/7ik;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1228574
    :cond_7
    const-string v14, "is_on_sale"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1228575
    const/4 v1, 0x1

    .line 1228576
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    goto/16 :goto_1

    .line 1228577
    :cond_8
    const-string v14, "name"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1228578
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 1228579
    :cond_9
    const-string v14, "productCatalogImage"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 1228580
    invoke-static/range {p0 .. p1}, LX/7il;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1228581
    :cond_a
    const-string v14, "productImageLarge"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 1228582
    invoke-static/range {p0 .. p1}, LX/7im;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1228583
    :cond_b
    const-string v14, "product_item_price"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1228584
    invoke-static/range {p0 .. p1}, LX/7ir;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1228585
    :cond_c
    const/16 v13, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1228586
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1228587
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1228588
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1228589
    const/4 v10, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1228590
    const/4 v9, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1228591
    const/4 v8, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1228592
    if-eqz v1, :cond_d

    .line 1228593
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->a(IZ)V

    .line 1228594
    :cond_d
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1228595
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1228596
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1228597
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1228598
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1228599
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1228600
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1228601
    if-eqz v0, :cond_0

    .line 1228602
    const-string v0, "commerce_product_visibility"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228603
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1228604
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1228605
    if-eqz v0, :cond_1

    .line 1228606
    const-string v1, "current_product_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228607
    invoke-static {p0, v0, p2}, LX/7ir;->a(LX/15i;ILX/0nX;)V

    .line 1228608
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1228609
    if-eqz v0, :cond_2

    .line 1228610
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228611
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1228612
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1228613
    if-eqz v0, :cond_3

    .line 1228614
    const-string v1, "external_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228615
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1228616
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1228617
    if-eqz v0, :cond_4

    .line 1228618
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228619
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1228620
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1228621
    if-eqz v0, :cond_6

    .line 1228622
    const-string v1, "images"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228623
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1228624
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 1228625
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/7ik;->a(LX/15i;ILX/0nX;)V

    .line 1228626
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1228627
    :cond_5
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1228628
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1228629
    if-eqz v0, :cond_7

    .line 1228630
    const-string v1, "is_on_sale"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228631
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1228632
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1228633
    if-eqz v0, :cond_8

    .line 1228634
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228635
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1228636
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1228637
    if-eqz v0, :cond_9

    .line 1228638
    const-string v1, "productCatalogImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228639
    invoke-static {p0, v0, p2}, LX/7il;->a(LX/15i;ILX/0nX;)V

    .line 1228640
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1228641
    if-eqz v0, :cond_a

    .line 1228642
    const-string v1, "productImageLarge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228643
    invoke-static {p0, v0, p2}, LX/7im;->a(LX/15i;ILX/0nX;)V

    .line 1228644
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1228645
    if-eqz v0, :cond_b

    .line 1228646
    const-string v1, "product_item_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1228647
    invoke-static {p0, v0, p2}, LX/7ir;->a(LX/15i;ILX/0nX;)V

    .line 1228648
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1228649
    return-void
.end method
