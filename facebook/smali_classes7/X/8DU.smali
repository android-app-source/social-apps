.class public LX/8DU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/8DU;


# instance fields
.field public final a:LX/2L1;

.field public final b:LX/0Sh;


# direct methods
.method public constructor <init>(LX/2L1;LX/0Sh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1312695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1312696
    iput-object p1, p0, LX/8DU;->a:LX/2L1;

    .line 1312697
    iput-object p2, p0, LX/8DU;->b:LX/0Sh;

    .line 1312698
    return-void
.end method

.method public static a(LX/0QB;)LX/8DU;
    .locals 5

    .prologue
    .line 1312699
    sget-object v0, LX/8DU;->c:LX/8DU;

    if-nez v0, :cond_1

    .line 1312700
    const-class v1, LX/8DU;

    monitor-enter v1

    .line 1312701
    :try_start_0
    sget-object v0, LX/8DU;->c:LX/8DU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1312702
    if-eqz v2, :cond_0

    .line 1312703
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1312704
    new-instance p0, LX/8DU;

    invoke-static {v0}, LX/2L1;->a(LX/0QB;)LX/2L1;

    move-result-object v3

    check-cast v3, LX/2L1;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-direct {p0, v3, v4}, LX/8DU;-><init>(LX/2L1;LX/0Sh;)V

    .line 1312705
    move-object v0, p0

    .line 1312706
    sput-object v0, LX/8DU;->c:LX/8DU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1312707
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1312708
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1312709
    :cond_1
    sget-object v0, LX/8DU;->c:LX/8DU;

    return-object v0

    .line 1312710
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1312711
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
