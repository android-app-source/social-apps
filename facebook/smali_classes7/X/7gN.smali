.class public LX/7gN;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/3iB;
.implements LX/3FT;
.implements LX/1a7;


# instance fields
.field private a:Lcom/facebook/video/player/RichVideoPlayer;

.field private final b:Landroid/widget/RelativeLayout$LayoutParams;

.field private c:Landroid/view/ViewGroup$LayoutParams;

.field private d:Z


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 1224184
    invoke-virtual {p0}, LX/7gN;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 1224185
    :goto_0
    return-void

    .line 1224186
    :cond_0
    int-to-float v0, p1

    const v1, 0x3fe38e39

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 1224187
    iget-object v1, p0, LX/7gN;->c:Landroid/view/ViewGroup$LayoutParams;

    if-nez v1, :cond_1

    .line 1224188
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, p1, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iput-object v1, p0, LX/7gN;->c:Landroid/view/ViewGroup$LayoutParams;

    .line 1224189
    :cond_1
    iget-object v1, p0, LX/7gN;->c:Landroid/view/ViewGroup$LayoutParams;

    iput p1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1224190
    iget-object v1, p0, LX/7gN;->c:Landroid/view/ViewGroup$LayoutParams;

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1224191
    invoke-virtual {p0}, LX/7gN;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    iget-object v1, p0, LX/7gN;->c:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 2

    .prologue
    .line 1224180
    iput-object p1, p0, LX/7gN;->a:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1224181
    const/4 v0, 0x0

    iget-object v1, p0, LX/7gN;->c:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->attachRecyclableViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1224182
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7gN;->d:Z

    .line 1224183
    return-void
.end method

.method public final c()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 2

    .prologue
    .line 1224176
    invoke-virtual {p0}, LX/7gN;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->detachRecyclableViewFromParent(Landroid/view/View;)V

    .line 1224177
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7gN;->d:Z

    .line 1224178
    invoke-virtual {p0}, LX/7gN;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    iget-object v1, p0, LX/7gN;->b:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1224179
    iget-object v0, p0, LX/7gN;->a:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public final cr_()Z
    .locals 1

    .prologue
    .line 1224175
    const/4 v0, 0x1

    return v0
.end method

.method public final d()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 1224174
    iget-object v0, p0, LX/7gN;->a:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public getAdditionalPlugins()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1224192
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCoverController()LX/1aZ;
    .locals 1

    .prologue
    .line 1224173
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 1224172
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    return-object v0
.end method

.method public getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 1224171
    iget-object v0, p0, LX/7gN;->a:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public setBackgroundResource(I)V
    .locals 0

    .prologue
    .line 1224170
    return-void
.end method

.method public setCoverController(LX/1aZ;)V
    .locals 0

    .prologue
    .line 1224169
    return-void
.end method
