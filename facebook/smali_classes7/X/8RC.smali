.class public final LX/8RC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V
    .locals 0

    .prologue
    .line 1344316
    iput-object p1, p0, LX/8RC;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1344317
    iget-object v0, p0, LX/8RC;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->f:LX/8tB;

    invoke-virtual {v0, p3}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1344318
    if-nez v0, :cond_0

    .line 1344319
    :goto_0
    return-void

    .line 1344320
    :cond_0
    iget-object v1, p0, LX/8RC;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1344321
    iget-object v1, p0, LX/8RC;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1344322
    :goto_1
    iget-object v0, p0, LX/8RC;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->l(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V

    goto :goto_0

    .line 1344323
    :cond_1
    iget-object v1, p0, LX/8RC;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
