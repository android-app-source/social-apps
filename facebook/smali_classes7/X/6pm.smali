.class public LX/6pm;
.super LX/6pY;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6pm;


# instance fields
.field public final a:LX/6p6;


# direct methods
.method public constructor <init>(LX/6p6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1149617
    invoke-direct {p0}, LX/6pY;-><init>()V

    .line 1149618
    iput-object p1, p0, LX/6pm;->a:LX/6p6;

    .line 1149619
    return-void
.end method

.method public static a(LX/0QB;)LX/6pm;
    .locals 4

    .prologue
    .line 1149620
    sget-object v0, LX/6pm;->b:LX/6pm;

    if-nez v0, :cond_1

    .line 1149621
    const-class v1, LX/6pm;

    monitor-enter v1

    .line 1149622
    :try_start_0
    sget-object v0, LX/6pm;->b:LX/6pm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1149623
    if-eqz v2, :cond_0

    .line 1149624
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1149625
    new-instance p0, LX/6pm;

    invoke-static {v0}, LX/6p6;->a(LX/0QB;)LX/6p6;

    move-result-object v3

    check-cast v3, LX/6p6;

    invoke-direct {p0, v3}, LX/6pm;-><init>(LX/6p6;)V

    .line 1149626
    move-object v0, p0

    .line 1149627
    sput-object v0, LX/6pm;->b:LX/6pm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1149628
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1149629
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1149630
    :cond_1
    sget-object v0, LX/6pm;->b:LX/6pm;

    return-object v0

    .line 1149631
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1149632
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/6pM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1149633
    sget-object v0, LX/6pM;->DELETE_WITH_PASSWORD:LX/6pM;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/ResetPinFragment;)LX/6oW;
    .locals 1

    .prologue
    .line 1149634
    new-instance v0, LX/6pj;

    invoke-direct {v0, p0, p1, p2}, LX/6pj;-><init>(LX/6pm;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/ResetPinFragment;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)LX/6oc;
    .locals 1

    .prologue
    .line 1149635
    new-instance v0, LX/6pl;

    invoke-direct {v0, p0, p1}, LX/6pl;-><init>(LX/6pm;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1149636
    iget-object v0, p0, LX/6pm;->a:LX/6p6;

    invoke-virtual {v0}, LX/6p6;->a()V

    .line 1149637
    return-void
.end method
