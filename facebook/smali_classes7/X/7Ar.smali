.class public final LX/7Ar;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1177578
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1177579
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1177580
    :goto_0
    return v1

    .line 1177581
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1177582
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1177583
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1177584
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1177585
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1177586
    const-string v3, "savable"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1177587
    invoke-static {p0, p1}, LX/7Aq;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1177588
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1177589
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1177590
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1177591
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1177592
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1177593
    if-eqz v0, :cond_0

    .line 1177594
    const-string v1, "savable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1177595
    invoke-static {p0, v0, p2}, LX/7Aq;->a(LX/15i;ILX/0nX;)V

    .line 1177596
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1177597
    return-void
.end method
