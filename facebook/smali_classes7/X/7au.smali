.class public LX/7au;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field private final a:LX/7at;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, LX/7at;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, LX/7at;-><init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V

    iput-object v0, p0, LX/7au;->a:LX/7at;

    invoke-direct {p0}, LX/7au;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, LX/7at;

    invoke-direct {v0, p0, p1, p2}, LX/7at;-><init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V

    iput-object v0, p0, LX/7au;->a:LX/7at;

    invoke-direct {p0}, LX/7au;->a()V

    return-void
.end method

.method private a()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/7au;->setClickable(Z)V

    return-void
.end method


# virtual methods
.method public final a(LX/6an;)V
    .locals 1

    const-string v0, "getMapAsync() must be called on the main thread"

    invoke-static {v0}, LX/1ol;->b(Ljava/lang/String;)V

    iget-object v0, p0, LX/7au;->a:LX/7at;

    iget-object p0, v0, LX/4th;->a:LX/4td;

    move-object p0, p0

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/4th;->a:LX/4td;

    move-object p0, p0

    check-cast p0, LX/7as;

    invoke-virtual {p0, p1}, LX/7as;->a(LX/6an;)V

    :goto_0
    return-void

    :cond_0
    iget-object p0, v0, LX/7at;->e:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7

    iget-object v0, p0, LX/7au;->a:LX/7at;

    new-instance v1, LX/4te;

    invoke-direct {v1, v0, p1}, LX/4te;-><init>(LX/4th;Landroid/os/Bundle;)V

    invoke-static {v0, p1, v1}, LX/4th;->a(LX/4th;Landroid/os/Bundle;LX/3K8;)V

    iget-object v0, p0, LX/7au;->a:LX/7at;

    iget-object v1, v0, LX/4th;->a:LX/4td;

    move-object v0, v1

    if-nez v0, :cond_0

    const/4 p1, -0x2

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/1oW;->a(Landroid/content/Context;)I

    move-result v1

    invoke-static {v0}, LX/1oW;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/2Gy;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1}, LX/2Gy;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v5, p1, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    new-instance v5, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v6, p1, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    if-eqz v3, :cond_0

    new-instance v2, Landroid/widget/Button;

    invoke-direct {v2, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v5, p1, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v3, LX/4tf;

    invoke-direct {v3, v0, v1}, LX/4tf;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, LX/7au;->a:LX/7at;

    const/4 v1, 0x0

    new-instance p0, LX/4tg;

    invoke-direct {p0, v0}, LX/4tg;-><init>(LX/4th;)V

    invoke-static {v0, v1, p0}, LX/4th;->a(LX/4th;Landroid/os/Bundle;LX/3K8;)V

    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, LX/7au;->a:LX/7at;

    iget-object p0, v0, LX/4th;->a:LX/4td;

    if-eqz p0, :cond_1

    iget-object p0, v0, LX/4th;->a:LX/4td;

    invoke-interface {p0, p1}, LX/4td;->b(Landroid/os/Bundle;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object p0, v0, LX/4th;->b:Landroid/os/Bundle;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/4th;->b:Landroid/os/Bundle;

    invoke-virtual {p1, p0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, LX/7au;->a:LX/7at;

    iget-object p0, v0, LX/4th;->a:LX/4td;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/4th;->a:LX/4td;

    invoke-interface {p0}, LX/4td;->b()V

    :goto_0
    return-void

    :cond_0
    const/4 p0, 0x5

    invoke-static {v0, p0}, LX/4th;->a(LX/4th;I)V

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, LX/7au;->a:LX/7at;

    iget-object p0, v0, LX/4th;->a:LX/4td;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/4th;->a:LX/4td;

    invoke-interface {p0}, LX/4td;->c()V

    :goto_0
    return-void

    :cond_0
    const/4 p0, 0x1

    invoke-static {v0, p0}, LX/4th;->a(LX/4th;I)V

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, LX/7au;->a:LX/7at;

    iget-object p0, v0, LX/4th;->a:LX/4td;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/4th;->a:LX/4td;

    invoke-interface {p0}, LX/4td;->d()V

    :cond_0
    return-void
.end method
