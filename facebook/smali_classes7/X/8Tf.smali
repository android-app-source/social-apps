.class public final LX/8Tf;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8WG;

.field public final synthetic b:LX/8Th;


# direct methods
.method public constructor <init>(LX/8Th;LX/8WG;)V
    .locals 0

    .prologue
    .line 1348888
    iput-object p1, p0, LX/8Tf;->b:LX/8Th;

    iput-object p2, p0, LX/8Tf;->a:LX/8WG;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1348889
    iget-object v0, p0, LX/8Tf;->b:LX/8Th;

    iget-object v0, v0, LX/8Th;->d:LX/8TD;

    sget-object v1, LX/8TE;->CURRENT_MATCH:LX/8TE;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 1348890
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 1348891
    check-cast p1, Ljava/util/List;

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 1348892
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1348893
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-object v2, v3

    move v4, v5

    move-object v6, v3

    move-object v7, v0

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1348894
    if-eqz v0, :cond_0

    .line 1348895
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1348896
    instance-of v9, v1, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    if-eqz v9, :cond_1

    .line 1348897
    iget-object v0, p0, LX/8Tf;->b:LX/8Th;

    iget-object v0, v0, LX/8Th;->d:LX/8TD;

    sget-object v4, LX/8TE;->CURRENT_MATCH:LX/8TE;

    invoke-virtual {v0, v4, v5, v3}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    move-object v0, v1

    .line 1348898
    check-cast v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    .line 1348899
    iget-object v1, p0, LX/8Tf;->b:LX/8Th;

    .line 1348900
    if-nez v0, :cond_4

    .line 1348901
    iget-object v4, v1, LX/8Th;->d:LX/8TD;

    sget-object v6, LX/8TE;->NO_THREAD_PARTICIPANT:LX/8TE;

    const-string v7, "participants list returns empty"

    invoke-virtual {v4, v6, v7}, LX/8TD;->a(LX/8TE;Ljava/lang/String;)V

    .line 1348902
    const/4 v4, 0x0

    .line 1348903
    :goto_1
    move-object v4, v4

    .line 1348904
    iget-object v1, p0, LX/8Tf;->b:LX/8Th;

    .line 1348905
    if-nez v0, :cond_5

    .line 1348906
    const/4 v6, 0x0

    .line 1348907
    :goto_2
    move-object v1, v6

    .line 1348908
    iget-object v6, p0, LX/8Tf;->b:LX/8Th;

    .line 1348909
    if-nez v0, :cond_6

    .line 1348910
    const/4 v7, 0x1

    .line 1348911
    :goto_3
    move v0, v7

    .line 1348912
    move-object v6, v1

    move-object v7, v4

    move v4, v0

    .line 1348913
    goto :goto_0

    :cond_1
    instance-of v9, v1, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;

    if-eqz v9, :cond_3

    .line 1348914
    check-cast v1, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;

    .line 1348915
    if-eqz v0, :cond_3

    .line 1348916
    invoke-virtual {v1}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    :goto_4
    move-object v2, v0

    .line 1348917
    goto :goto_0

    .line 1348918
    :cond_2
    iget-object v0, p0, LX/8Tf;->a:LX/8WG;

    invoke-virtual {v0, v7, v6, v4, v2}, LX/8WG;->a(Ljava/util/List;Ljava/lang/String;ZLjava/lang/String;)V

    .line 1348919
    return-void

    :cond_3
    move-object v0, v2

    goto :goto_4

    :cond_4
    iget-object v4, v1, LX/8Th;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/8Tn;

    invoke-virtual {v4, v0}, LX/8Tn;->a(Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;)Ljava/util/List;

    move-result-object v4

    goto :goto_1

    :cond_5
    iget-object v6, v1, LX/8Th;->g:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v0}, LX/8Tn;->b(Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :cond_6
    iget-object v7, v6, LX/8Th;->g:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 v7, 0x1

    .line 1348920
    if-nez v0, :cond_8

    .line 1348921
    :cond_7
    :goto_5
    move v7, v7

    .line 1348922
    goto :goto_3

    :cond_8
    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;->a()Z

    move-result v6

    if-eqz v6, :cond_7

    const/4 v7, 0x0

    goto :goto_5
.end method
