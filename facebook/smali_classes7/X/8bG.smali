.class public LX/8bG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/11M;

.field private b:LX/0Sh;

.field public c:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field public final d:LX/03V;


# direct methods
.method public constructor <init>(LX/11M;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0Sh;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1371897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1371898
    iput-object p1, p0, LX/8bG;->a:LX/11M;

    .line 1371899
    iput-object p2, p0, LX/8bG;->c:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 1371900
    iput-object p3, p0, LX/8bG;->b:LX/0Sh;

    .line 1371901
    iput-object p4, p0, LX/8bG;->d:LX/03V;

    .line 1371902
    return-void
.end method

.method public static b(LX/0QB;)LX/8bG;
    .locals 5

    .prologue
    .line 1371895
    new-instance v4, LX/8bG;

    invoke-static {p0}, LX/11M;->a(LX/0QB;)LX/11M;

    move-result-object v0

    check-cast v0, LX/11M;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v1

    check-cast v1, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {v4, v0, v1, v2, v3}, LX/8bG;-><init>(LX/11M;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0Sh;LX/03V;)V

    .line 1371896
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1371893
    iget-object v0, p0, LX/8bG;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/richdocument/utils/ActionUtils$GetRequestAsyncTask;

    new-instance v2, LX/8bE;

    invoke-direct {v2, p0, p1}, LX/8bE;-><init>(LX/8bG;Ljava/lang/String;)V

    invoke-direct {v1, p0, p1, v2}, Lcom/facebook/richdocument/utils/ActionUtils$GetRequestAsyncTask;-><init>(LX/8bG;Ljava/lang/String;LX/0TF;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    .line 1371894
    return-void
.end method
