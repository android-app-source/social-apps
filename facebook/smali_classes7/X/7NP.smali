.class public final LX/7NP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/7Mz;


# direct methods
.method public constructor <init>(LX/7Mz;)V
    .locals 0

    .prologue
    .line 1200261
    iput-object p1, p0, LX/7NP;->a:LX/7Mz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x2

    const v2, 0x39220103

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 1200262
    iget-object v0, p0, LX/7NP;->a:LX/7Mz;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1200263
    iget-object v0, p0, LX/7NP;->a:LX/7Mz;

    iget-boolean v0, v0, LX/7Mz;->z:Z

    if-eqz v0, :cond_2

    sget-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    .line 1200264
    :goto_0
    iget-object v2, p0, LX/7NP;->a:LX/7Mz;

    iget-object v2, v2, LX/2oy;->i:LX/2oj;

    new-instance v3, LX/2qd;

    sget-object v4, LX/04g;->BY_USER:LX/04g;

    invoke-direct {v3, v4, v0}, LX/2qd;-><init>(LX/04g;LX/2oi;)V

    invoke-virtual {v2, v3}, LX/2oj;->a(LX/2ol;)V

    .line 1200265
    iget-object v0, p0, LX/7NP;->a:LX/7Mz;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->f()LX/2oi;

    move-result-object v0

    sget-object v2, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    if-ne v0, v2, :cond_3

    move v7, v1

    .line 1200266
    :goto_1
    iget-object v0, p0, LX/7NP;->a:LX/7Mz;

    iget-boolean v0, v0, LX/7Mz;->z:Z

    if-eq v0, v7, :cond_1

    .line 1200267
    iget-object v0, p0, LX/7NP;->a:LX/7Mz;

    iget-object v0, v0, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7NP;->a:LX/7Mz;

    iget-object v0, v0, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1200268
    if-eqz v7, :cond_4

    .line 1200269
    iget-object v0, p0, LX/7NP;->a:LX/7Mz;

    iget-object v0, v0, LX/7Mr;->f:LX/1C2;

    iget-object v1, p0, LX/7NP;->a:LX/7Mz;

    iget-object v1, v1, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, p0, LX/7NP;->a:LX/7Mz;

    iget-object v2, v2, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v3, p0, LX/7NP;->a:LX/7Mz;

    iget-object v3, v3, LX/2oy;->j:LX/2pb;

    invoke-virtual {v3}, LX/2pb;->s()LX/04D;

    move-result-object v3

    iget-object v4, p0, LX/7NP;->a:LX/7Mz;

    iget-object v4, v4, LX/2oy;->j:LX/2pb;

    .line 1200270
    iget-object v5, v4, LX/2pb;->D:LX/04G;

    move-object v4, v5

    .line 1200271
    iget-object v5, p0, LX/7NP;->a:LX/7Mz;

    iget-object v5, v5, LX/2oy;->j:LX/2pb;

    invoke-virtual {v5}, LX/2pb;->h()I

    move-result v5

    iget-object v6, p0, LX/7NP;->a:LX/7Mz;

    iget-object v6, v6, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v6, v6, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    invoke-virtual/range {v0 .. v6}, LX/1C2;->a(Ljava/lang/String;LX/0lF;LX/04D;LX/04G;IZ)LX/1C2;

    .line 1200272
    :cond_0
    :goto_2
    iget-object v0, p0, LX/7NP;->a:LX/7Mz;

    .line 1200273
    iput-boolean v7, v0, LX/7Mz;->z:Z

    .line 1200274
    iget-object v0, p0, LX/7NP;->a:LX/7Mz;

    iget-object v1, v0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, LX/7NP;->a:LX/7Mz;

    iget-boolean v0, v0, LX/7Mz;->z:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/7NP;->a:LX/7Mz;

    iget v0, v0, LX/7Mz;->x:I

    :goto_3
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1200275
    :cond_1
    const v0, -0x391c7c30

    invoke-static {v0, v8}, LX/02F;->a(II)V

    return-void

    .line 1200276
    :cond_2
    sget-object v0, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    goto/16 :goto_0

    .line 1200277
    :cond_3
    const/4 v0, 0x0

    move v7, v0

    goto :goto_1

    .line 1200278
    :cond_4
    iget-object v0, p0, LX/7NP;->a:LX/7Mz;

    iget-object v0, v0, LX/7Mr;->f:LX/1C2;

    iget-object v1, p0, LX/7NP;->a:LX/7Mz;

    iget-object v1, v1, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, p0, LX/7NP;->a:LX/7Mz;

    iget-object v2, v2, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v3, p0, LX/7NP;->a:LX/7Mz;

    iget-object v3, v3, LX/2oy;->j:LX/2pb;

    invoke-virtual {v3}, LX/2pb;->s()LX/04D;

    move-result-object v3

    iget-object v4, p0, LX/7NP;->a:LX/7Mz;

    iget-object v4, v4, LX/2oy;->j:LX/2pb;

    .line 1200279
    iget-object v5, v4, LX/2pb;->D:LX/04G;

    move-object v4, v5

    .line 1200280
    iget-object v5, p0, LX/7NP;->a:LX/7Mz;

    iget-object v5, v5, LX/2oy;->j:LX/2pb;

    invoke-virtual {v5}, LX/2pb;->h()I

    move-result v5

    iget-object v6, p0, LX/7NP;->a:LX/7Mz;

    iget-object v6, v6, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v6, v6, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    invoke-virtual/range {v0 .. v6}, LX/1C2;->b(Ljava/lang/String;LX/0lF;LX/04D;LX/04G;IZ)LX/1C2;

    goto :goto_2

    .line 1200281
    :cond_5
    iget-object v0, p0, LX/7NP;->a:LX/7Mz;

    iget v0, v0, LX/7Mz;->y:I

    goto :goto_3
.end method
