.class public final enum LX/74l;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/74l;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/74l;

.field public static final enum EXTERNAL:LX/74l;

.field public static final enum INTERNAL:LX/74l;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1168392
    new-instance v0, LX/74l;

    const-string v1, "EXTERNAL"

    invoke-direct {v0, v1, v2}, LX/74l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74l;->EXTERNAL:LX/74l;

    .line 1168393
    new-instance v0, LX/74l;

    const-string v1, "INTERNAL"

    invoke-direct {v0, v1, v3}, LX/74l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74l;->INTERNAL:LX/74l;

    .line 1168394
    const/4 v0, 0x2

    new-array v0, v0, [LX/74l;

    sget-object v1, LX/74l;->EXTERNAL:LX/74l;

    aput-object v1, v0, v2

    sget-object v1, LX/74l;->INTERNAL:LX/74l;

    aput-object v1, v0, v3

    sput-object v0, LX/74l;->$VALUES:[LX/74l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1168395
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/74l;
    .locals 1

    .prologue
    .line 1168396
    const-class v0, LX/74l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/74l;

    return-object v0
.end method

.method public static values()[LX/74l;
    .locals 1

    .prologue
    .line 1168397
    sget-object v0, LX/74l;->$VALUES:[LX/74l;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/74l;

    return-object v0
.end method
