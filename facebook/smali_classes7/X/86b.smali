.class public LX/86b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONTACT::",
        "LX/2lr;",
        "USER::",
        "LX/2lr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<TCONTACT;>;"
        }
    .end annotation
.end field

.field public final b:I

.field public final c:I

.field public d:I

.field public final e:I


# direct methods
.method public constructor <init>(III)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1297584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1297585
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1297586
    if-ltz p2, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1297587
    const/4 v0, 0x2

    if-lt p3, v0, :cond_2

    :goto_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1297588
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/86b;->a:Ljava/util/LinkedList;

    .line 1297589
    int-to-double v0, p1

    int-to-double v2, p3

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, LX/86b;->e:I

    .line 1297590
    iput p2, p0, LX/86b;->b:I

    .line 1297591
    iput p3, p0, LX/86b;->c:I

    .line 1297592
    return-void

    :cond_0
    move v0, v2

    .line 1297593
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1297594
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1297595
    goto :goto_2
.end method
