.class public LX/7YV;
.super LX/7YU;
.source ""


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:LX/0yV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1220021
    const-class v0, LX/7YV;

    sput-object v0, LX/7YV;->b:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yV;)V
    .locals 0

    .prologue
    .line 1220022
    invoke-direct {p0, p1}, LX/7YU;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1220023
    iput-object p2, p0, LX/7YV;->m:LX/0yV;

    .line 1220024
    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yV;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V
    .locals 1

    .prologue
    .line 1220025
    invoke-direct {p0, p1, p3}, LX/7YU;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V

    .line 1220026
    iput-object p2, p0, LX/7YV;->m:LX/0yV;

    .line 1220027
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->K()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YV;->c:Ljava/lang/String;

    .line 1220028
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YV;->d:Ljava/lang/String;

    .line 1220029
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->J()Z

    move-result v0

    iput-boolean v0, p0, LX/7YV;->e:Z

    .line 1220030
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YV;->f:Ljava/lang/String;

    .line 1220031
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->v()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1220032
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1220033
    :goto_0
    iput-object v0, p0, LX/7YV;->g:LX/0Px;

    .line 1220034
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YV;->h:Ljava/lang/String;

    .line 1220035
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->z()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YV;->i:Ljava/lang/String;

    .line 1220036
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->G()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YV;->j:Ljava/lang/String;

    .line 1220037
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->D()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YV;->k:Ljava/lang/String;

    .line 1220038
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->F()Z

    move-result v0

    iput-boolean v0, p0, LX/7YV;->l:Z

    .line 1220039
    return-void

    .line 1220040
    :cond_0
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->v()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yV;)LX/7YV;
    .locals 2

    .prologue
    .line 1220041
    new-instance v0, LX/7YV;

    invoke-direct {v0, p0, p1}, LX/7YV;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yV;)V

    const/4 p1, 0x0

    .line 1220042
    const-string v1, "subtitle_key"

    const-string p0, ""

    invoke-virtual {v0, v1, p0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7YV;->c:Ljava/lang/String;

    .line 1220043
    const-string v1, "image_url_key"

    const-string p0, ""

    invoke-virtual {v0, v1, p0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7YV;->d:Ljava/lang/String;

    .line 1220044
    const-string v1, "should_use_default_image_key"

    invoke-virtual {v0, v1, p1}, LX/7YU;->a(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, LX/7YV;->e:Z

    .line 1220045
    const-string v1, "facepile_text_key"

    const-string p0, ""

    invoke-virtual {v0, v1, p0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7YV;->f:Ljava/lang/String;

    .line 1220046
    const-string v1, "primary_button_step_key"

    const-string p0, ""

    invoke-virtual {v0, v1, p0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7YV;->h:Ljava/lang/String;

    .line 1220047
    const-string v1, "primary_button_action_key"

    const-string p0, ""

    invoke-virtual {v0, v1, p0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7YV;->i:Ljava/lang/String;

    .line 1220048
    const-string v1, "secondary_button_step_key"

    const-string p0, ""

    invoke-virtual {v0, v1, p0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7YV;->j:Ljava/lang/String;

    .line 1220049
    const-string v1, "secondary_button_action_key"

    const-string p0, ""

    invoke-virtual {v0, v1, p0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7YV;->k:Ljava/lang/String;

    .line 1220050
    const-string v1, "secondary_button_override_back_only_key"

    invoke-virtual {v0, v1, p1}, LX/7YU;->a(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, LX/7YV;->l:Z

    .line 1220051
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1220052
    iput-object v1, v0, LX/7YV;->g:LX/0Px;

    .line 1220053
    const-string v1, "facepile_profile_picture_urls_key"

    const-string p0, ""

    invoke-virtual {v0, v1, p0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1220054
    :try_start_0
    iget-object p0, v0, LX/7YV;->m:LX/0yV;

    invoke-virtual {p0, v1}, LX/0yV;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/7YV;->g:LX/0Px;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1220055
    :goto_0
    move-object v0, v0

    .line 1220056
    return-object v0

    .line 1220057
    :catch_0
    move-exception v1

    .line 1220058
    sget-object p0, LX/7YV;->b:Ljava/lang/Class;

    const-string p1, "Failed to read zero optin facepile URLs from shared prefs"

    invoke-static {p0, p1, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 2

    .prologue
    .line 1220059
    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0df;->x:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1220060
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1220061
    iget-object v0, p0, LX/7YU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 1220062
    invoke-super {p0, v1}, LX/7YU;->a(LX/0hN;)V

    .line 1220063
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "subtitle_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YV;->c:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v3, "image_url_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v3, p0, LX/7YV;->d:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v3, "should_use_default_image_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-boolean v3, p0, LX/7YV;->e:Z

    invoke-interface {v2, v0, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v3, "facepile_text_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v3, p0, LX/7YV;->f:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v3, "primary_button_step_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v3, p0, LX/7YV;->h:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v3, "primary_button_action_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v3, p0, LX/7YV;->i:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v3, "secondary_button_step_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v3, p0, LX/7YV;->j:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v3, "secondary_button_action_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v3, p0, LX/7YV;->k:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v3, "secondary_button_override_back_only_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-boolean v3, p0, LX/7YV;->l:Z

    invoke-interface {v2, v0, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 1220064
    :try_start_0
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "facepile_profile_picture_urls_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YV;->m:LX/0yV;

    iget-object v3, p0, LX/7YV;->g:LX/0Px;

    invoke-virtual {v2, v3}, LX/0yV;->a(LX/0Px;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1220065
    :goto_0
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1220066
    return-void

    .line 1220067
    :catch_0
    move-exception v0

    .line 1220068
    sget-object v2, LX/7YV;->b:Ljava/lang/Class;

    const-string v3, "Failed to write zero optin facepile URLs to shared prefs"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final b()LX/0Tn;
    .locals 1

    .prologue
    .line 1220069
    sget-object v0, LX/0df;->x:LX/0Tn;

    return-object v0
.end method
