.class public final enum LX/79H;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/79H;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/79H;

.field public static final enum WebRTCExpressionDoodle:LX/79H;

.field public static final enum WebRTCExpressionFilter:LX/79H;

.field public static final enum WebRTCExpressionMask:LX/79H;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1174021
    new-instance v0, LX/79H;

    const-string v1, "WebRTCExpressionMask"

    invoke-direct {v0, v1, v2}, LX/79H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79H;->WebRTCExpressionMask:LX/79H;

    .line 1174022
    new-instance v0, LX/79H;

    const-string v1, "WebRTCExpressionFilter"

    invoke-direct {v0, v1, v3}, LX/79H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79H;->WebRTCExpressionFilter:LX/79H;

    .line 1174023
    new-instance v0, LX/79H;

    const-string v1, "WebRTCExpressionDoodle"

    invoke-direct {v0, v1, v4}, LX/79H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79H;->WebRTCExpressionDoodle:LX/79H;

    .line 1174024
    const/4 v0, 0x3

    new-array v0, v0, [LX/79H;

    sget-object v1, LX/79H;->WebRTCExpressionMask:LX/79H;

    aput-object v1, v0, v2

    sget-object v1, LX/79H;->WebRTCExpressionFilter:LX/79H;

    aput-object v1, v0, v3

    sget-object v1, LX/79H;->WebRTCExpressionDoodle:LX/79H;

    aput-object v1, v0, v4

    sput-object v0, LX/79H;->$VALUES:[LX/79H;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1174025
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/79H;
    .locals 1

    .prologue
    .line 1174026
    const-class v0, LX/79H;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/79H;

    return-object v0
.end method

.method public static values()[LX/79H;
    .locals 1

    .prologue
    .line 1174027
    sget-object v0, LX/79H;->$VALUES:[LX/79H;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/79H;

    return-object v0
.end method
