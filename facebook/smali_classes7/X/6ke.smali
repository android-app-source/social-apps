.class public LX/6ke;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;

.field private static final l:LX/1sw;

.field private static final m:LX/1sw;


# instance fields
.field public final animatedImageURIMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final animatedImageURIMapFormat:Ljava/lang/String;

.field public final height:Ljava/lang/Integer;

.field public final imageSource:Ljava/lang/Integer;

.field public final imageURIMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final imageURIMapFormat:Ljava/lang/String;

.field public final miniPreview:[B

.field public final rawImageURI:Ljava/lang/String;

.field public final rawImageURIFormat:Ljava/lang/String;

.field public final renderAsSticker:Ljava/lang/Boolean;

.field public final width:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0xd

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/16 v4, 0x8

    const/16 v3, 0xb

    .line 1140598
    new-instance v0, LX/1sv;

    const-string v1, "ImageMetadata"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6ke;->b:LX/1sv;

    .line 1140599
    new-instance v0, LX/1sw;

    const-string v1, "width"

    invoke-direct {v0, v1, v4, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ke;->c:LX/1sw;

    .line 1140600
    new-instance v0, LX/1sw;

    const-string v1, "height"

    invoke-direct {v0, v1, v4, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ke;->d:LX/1sw;

    .line 1140601
    new-instance v0, LX/1sw;

    const-string v1, "imageURIMap"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v7, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ke;->e:LX/1sw;

    .line 1140602
    new-instance v0, LX/1sw;

    const-string v1, "imageSource"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ke;->f:LX/1sw;

    .line 1140603
    new-instance v0, LX/1sw;

    const-string v1, "rawImageURI"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ke;->g:LX/1sw;

    .line 1140604
    new-instance v0, LX/1sw;

    const-string v1, "rawImageURIFormat"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ke;->h:LX/1sw;

    .line 1140605
    new-instance v0, LX/1sw;

    const-string v1, "animatedImageURIMap"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v7, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ke;->i:LX/1sw;

    .line 1140606
    new-instance v0, LX/1sw;

    const-string v1, "imageURIMapFormat"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ke;->j:LX/1sw;

    .line 1140607
    new-instance v0, LX/1sw;

    const-string v1, "animatedImageURIMapFormat"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ke;->k:LX/1sw;

    .line 1140608
    new-instance v0, LX/1sw;

    const-string v1, "renderAsSticker"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ke;->l:LX/1sw;

    .line 1140609
    new-instance v0, LX/1sw;

    const-string v1, "miniPreview"

    invoke-direct {v0, v1, v3, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ke;->m:LX/1sw;

    .line 1140610
    sput-boolean v5, LX/6ke;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/Map;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;[B)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "[B)V"
        }
    .end annotation

    .prologue
    .line 1140585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1140586
    iput-object p1, p0, LX/6ke;->width:Ljava/lang/Integer;

    .line 1140587
    iput-object p2, p0, LX/6ke;->height:Ljava/lang/Integer;

    .line 1140588
    iput-object p3, p0, LX/6ke;->imageURIMap:Ljava/util/Map;

    .line 1140589
    iput-object p4, p0, LX/6ke;->imageSource:Ljava/lang/Integer;

    .line 1140590
    iput-object p5, p0, LX/6ke;->rawImageURI:Ljava/lang/String;

    .line 1140591
    iput-object p6, p0, LX/6ke;->rawImageURIFormat:Ljava/lang/String;

    .line 1140592
    iput-object p7, p0, LX/6ke;->animatedImageURIMap:Ljava/util/Map;

    .line 1140593
    iput-object p8, p0, LX/6ke;->imageURIMapFormat:Ljava/lang/String;

    .line 1140594
    iput-object p9, p0, LX/6ke;->animatedImageURIMapFormat:Ljava/lang/String;

    .line 1140595
    iput-object p10, p0, LX/6ke;->renderAsSticker:Ljava/lang/Boolean;

    .line 1140596
    iput-object p11, p0, LX/6ke;->miniPreview:[B

    .line 1140597
    return-void
.end method

.method private static a(LX/6ke;)V
    .locals 3

    .prologue
    .line 1140247
    iget-object v0, p0, LX/6ke;->imageSource:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, LX/6kg;->a:LX/1sn;

    iget-object v1, p0, LX/6ke;->imageSource:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1140248
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'imageSource\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6ke;->imageSource:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1140249
    :cond_0
    return-void
.end method

.method public static b(LX/1su;)LX/6ke;
    .locals 15

    .prologue
    .line 1140516
    const/4 v1, 0x0

    .line 1140517
    const/4 v2, 0x0

    .line 1140518
    const/4 v3, 0x0

    .line 1140519
    const/4 v4, 0x0

    .line 1140520
    const/4 v5, 0x0

    .line 1140521
    const/4 v6, 0x0

    .line 1140522
    const/4 v7, 0x0

    .line 1140523
    const/4 v8, 0x0

    .line 1140524
    const/4 v9, 0x0

    .line 1140525
    const/4 v10, 0x0

    .line 1140526
    const/4 v11, 0x0

    .line 1140527
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    .line 1140528
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1140529
    iget-byte v12, v0, LX/1sw;->b:B

    if-eqz v12, :cond_e

    .line 1140530
    iget-short v12, v0, LX/1sw;->c:S

    packed-switch v12, :pswitch_data_0

    .line 1140531
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1140532
    :pswitch_0
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0x8

    if-ne v12, v13, :cond_1

    .line 1140533
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 1140534
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1140535
    :pswitch_1
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0x8

    if-ne v12, v13, :cond_2

    .line 1140536
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    .line 1140537
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1140538
    :pswitch_2
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xd

    if-ne v12, v13, :cond_4

    .line 1140539
    invoke-virtual {p0}, LX/1su;->g()LX/7H3;

    move-result-object v12

    .line 1140540
    new-instance v3, Ljava/util/HashMap;

    const/4 v0, 0x0

    iget v13, v12, LX/7H3;->c:I

    mul-int/lit8 v13, v13, 0x2

    invoke-static {v0, v13}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 1140541
    const/4 v0, 0x0

    .line 1140542
    :goto_1
    iget v13, v12, LX/7H3;->c:I

    if-gez v13, :cond_3

    invoke-static {}, LX/1su;->s()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1140543
    :goto_2
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 1140544
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v14

    .line 1140545
    invoke-interface {v3, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1140546
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1140547
    :cond_3
    iget v13, v12, LX/7H3;->c:I

    if-ge v0, v13, :cond_0

    goto :goto_2

    .line 1140548
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1140549
    :pswitch_3
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0x8

    if-ne v12, v13, :cond_5

    .line 1140550
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/16 :goto_0

    .line 1140551
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1140552
    :pswitch_4
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xb

    if-ne v12, v13, :cond_6

    .line 1140553
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 1140554
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1140555
    :pswitch_5
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xb

    if-ne v12, v13, :cond_7

    .line 1140556
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 1140557
    :cond_7
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1140558
    :pswitch_6
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xd

    if-ne v12, v13, :cond_9

    .line 1140559
    invoke-virtual {p0}, LX/1su;->g()LX/7H3;

    move-result-object v12

    .line 1140560
    new-instance v7, Ljava/util/HashMap;

    const/4 v0, 0x0

    iget v13, v12, LX/7H3;->c:I

    mul-int/lit8 v13, v13, 0x2

    invoke-static {v0, v13}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v7, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 1140561
    const/4 v0, 0x0

    .line 1140562
    :goto_3
    iget v13, v12, LX/7H3;->c:I

    if-gez v13, :cond_8

    invoke-static {}, LX/1su;->s()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1140563
    :goto_4
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 1140564
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v14

    .line 1140565
    invoke-interface {v7, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1140566
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1140567
    :cond_8
    iget v13, v12, LX/7H3;->c:I

    if-ge v0, v13, :cond_0

    goto :goto_4

    .line 1140568
    :cond_9
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1140569
    :pswitch_7
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xb

    if-ne v12, v13, :cond_a

    .line 1140570
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 1140571
    :cond_a
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1140572
    :pswitch_8
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xb

    if-ne v12, v13, :cond_b

    .line 1140573
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0

    .line 1140574
    :cond_b
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1140575
    :pswitch_9
    iget-byte v12, v0, LX/1sw;->b:B

    const/4 v13, 0x2

    if-ne v12, v13, :cond_c

    .line 1140576
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto/16 :goto_0

    .line 1140577
    :cond_c
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1140578
    :pswitch_a
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xb

    if-ne v12, v13, :cond_d

    .line 1140579
    invoke-virtual {p0}, LX/1su;->q()[B

    move-result-object v11

    goto/16 :goto_0

    .line 1140580
    :cond_d
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1140581
    :cond_e
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1140582
    new-instance v0, LX/6ke;

    invoke-direct/range {v0 .. v11}, LX/6ke;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/Map;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;[B)V

    .line 1140583
    invoke-static {v0}, LX/6ke;->a(LX/6ke;)V

    .line 1140584
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x80

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1140393
    if-eqz p2, :cond_15

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    .line 1140394
    :goto_0
    if-eqz p2, :cond_16

    const-string v0, "\n"

    move-object v4, v0

    .line 1140395
    :goto_1
    if-eqz p2, :cond_17

    const-string v0, " "

    move-object v1, v0

    .line 1140396
    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v0, "ImageMetadata"

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1140397
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140398
    const-string v0, "("

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140399
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140400
    iget-object v0, p0, LX/6ke;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_27

    .line 1140401
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140402
    const-string v0, "width"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140403
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140404
    const-string v0, ":"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140405
    iget-object v0, p0, LX/6ke;->width:Ljava/lang/Integer;

    if-nez v0, :cond_18

    .line 1140406
    const-string v0, "null"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v0, v2

    .line 1140407
    :goto_4
    iget-object v7, p0, LX/6ke;->height:Ljava/lang/Integer;

    if-eqz v7, :cond_1

    .line 1140408
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140409
    :cond_0
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140410
    const-string v0, "height"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140411
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140412
    const-string v0, ":"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140413
    iget-object v0, p0, LX/6ke;->height:Ljava/lang/Integer;

    if-nez v0, :cond_19

    .line 1140414
    const-string v0, "null"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v0, v2

    .line 1140415
    :cond_1
    iget-object v7, p0, LX/6ke;->imageURIMap:Ljava/util/Map;

    if-eqz v7, :cond_3

    .line 1140416
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140417
    :cond_2
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140418
    const-string v0, "imageURIMap"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140419
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140420
    const-string v0, ":"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140421
    iget-object v0, p0, LX/6ke;->imageURIMap:Ljava/util/Map;

    if-nez v0, :cond_1a

    .line 1140422
    const-string v0, "null"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    move v0, v2

    .line 1140423
    :cond_3
    iget-object v7, p0, LX/6ke;->imageSource:Ljava/lang/Integer;

    if-eqz v7, :cond_6

    .line 1140424
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140425
    :cond_4
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140426
    const-string v0, "imageSource"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140427
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140428
    const-string v0, ":"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140429
    iget-object v0, p0, LX/6ke;->imageSource:Ljava/lang/Integer;

    if-nez v0, :cond_1b

    .line 1140430
    const-string v0, "null"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    :goto_7
    move v0, v2

    .line 1140431
    :cond_6
    iget-object v7, p0, LX/6ke;->rawImageURI:Ljava/lang/String;

    if-eqz v7, :cond_8

    .line 1140432
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140433
    :cond_7
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140434
    const-string v0, "rawImageURI"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140435
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140436
    const-string v0, ":"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140437
    iget-object v0, p0, LX/6ke;->rawImageURI:Ljava/lang/String;

    if-nez v0, :cond_1d

    .line 1140438
    const-string v0, "null"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_8
    move v0, v2

    .line 1140439
    :cond_8
    iget-object v7, p0, LX/6ke;->rawImageURIFormat:Ljava/lang/String;

    if-eqz v7, :cond_a

    .line 1140440
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140441
    :cond_9
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140442
    const-string v0, "rawImageURIFormat"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140443
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140444
    const-string v0, ":"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140445
    iget-object v0, p0, LX/6ke;->rawImageURIFormat:Ljava/lang/String;

    if-nez v0, :cond_1e

    .line 1140446
    const-string v0, "null"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_9
    move v0, v2

    .line 1140447
    :cond_a
    iget-object v7, p0, LX/6ke;->animatedImageURIMap:Ljava/util/Map;

    if-eqz v7, :cond_c

    .line 1140448
    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140449
    :cond_b
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140450
    const-string v0, "animatedImageURIMap"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140451
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140452
    const-string v0, ":"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140453
    iget-object v0, p0, LX/6ke;->animatedImageURIMap:Ljava/util/Map;

    if-nez v0, :cond_1f

    .line 1140454
    const-string v0, "null"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_a
    move v0, v2

    .line 1140455
    :cond_c
    iget-object v7, p0, LX/6ke;->imageURIMapFormat:Ljava/lang/String;

    if-eqz v7, :cond_e

    .line 1140456
    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140457
    :cond_d
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140458
    const-string v0, "imageURIMapFormat"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140459
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140460
    const-string v0, ":"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140461
    iget-object v0, p0, LX/6ke;->imageURIMapFormat:Ljava/lang/String;

    if-nez v0, :cond_20

    .line 1140462
    const-string v0, "null"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_b
    move v0, v2

    .line 1140463
    :cond_e
    iget-object v7, p0, LX/6ke;->animatedImageURIMapFormat:Ljava/lang/String;

    if-eqz v7, :cond_10

    .line 1140464
    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140465
    :cond_f
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140466
    const-string v0, "animatedImageURIMapFormat"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140467
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140468
    const-string v0, ":"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140469
    iget-object v0, p0, LX/6ke;->animatedImageURIMapFormat:Ljava/lang/String;

    if-nez v0, :cond_21

    .line 1140470
    const-string v0, "null"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_c
    move v0, v2

    .line 1140471
    :cond_10
    iget-object v7, p0, LX/6ke;->renderAsSticker:Ljava/lang/Boolean;

    if-eqz v7, :cond_12

    .line 1140472
    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140473
    :cond_11
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140474
    const-string v0, "renderAsSticker"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140475
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140476
    const-string v0, ":"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140477
    iget-object v0, p0, LX/6ke;->renderAsSticker:Ljava/lang/Boolean;

    if-nez v0, :cond_22

    .line 1140478
    const-string v0, "null"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_d
    move v0, v2

    .line 1140479
    :cond_12
    iget-object v7, p0, LX/6ke;->miniPreview:[B

    if-eqz v7, :cond_14

    .line 1140480
    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140481
    :cond_13
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140482
    const-string v0, "miniPreview"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140483
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140484
    const-string v0, ":"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140485
    iget-object v0, p0, LX/6ke;->miniPreview:[B

    if-nez v0, :cond_23

    .line 1140486
    const-string v0, "null"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140487
    :cond_14
    :goto_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140488
    const-string v0, ")"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140489
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1140490
    :cond_15
    const-string v0, ""

    move-object v5, v0

    goto/16 :goto_0

    .line 1140491
    :cond_16
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_1

    .line 1140492
    :cond_17
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 1140493
    :cond_18
    iget-object v0, p0, LX/6ke;->width:Ljava/lang/Integer;

    add-int/lit8 v7, p1, 0x1

    invoke-static {v0, v7, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1140494
    :cond_19
    iget-object v0, p0, LX/6ke;->height:Ljava/lang/Integer;

    add-int/lit8 v7, p1, 0x1

    invoke-static {v0, v7, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1140495
    :cond_1a
    iget-object v0, p0, LX/6ke;->imageURIMap:Ljava/util/Map;

    add-int/lit8 v7, p1, 0x1

    invoke-static {v0, v7, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1140496
    :cond_1b
    sget-object v0, LX/6kg;->b:Ljava/util/Map;

    iget-object v7, p0, LX/6ke;->imageSource:Ljava/lang/Integer;

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1140497
    if-eqz v0, :cond_1c

    .line 1140498
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140499
    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140500
    :cond_1c
    iget-object v7, p0, LX/6ke;->imageSource:Ljava/lang/Integer;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1140501
    if-eqz v0, :cond_5

    .line 1140502
    const-string v0, ")"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1140503
    :cond_1d
    iget-object v0, p0, LX/6ke;->rawImageURI:Ljava/lang/String;

    add-int/lit8 v7, p1, 0x1

    invoke-static {v0, v7, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1140504
    :cond_1e
    iget-object v0, p0, LX/6ke;->rawImageURIFormat:Ljava/lang/String;

    add-int/lit8 v7, p1, 0x1

    invoke-static {v0, v7, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 1140505
    :cond_1f
    iget-object v0, p0, LX/6ke;->animatedImageURIMap:Ljava/util/Map;

    add-int/lit8 v7, p1, 0x1

    invoke-static {v0, v7, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 1140506
    :cond_20
    iget-object v0, p0, LX/6ke;->imageURIMapFormat:Ljava/lang/String;

    add-int/lit8 v7, p1, 0x1

    invoke-static {v0, v7, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    .line 1140507
    :cond_21
    iget-object v0, p0, LX/6ke;->animatedImageURIMapFormat:Ljava/lang/String;

    add-int/lit8 v7, p1, 0x1

    invoke-static {v0, v7, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    .line 1140508
    :cond_22
    iget-object v0, p0, LX/6ke;->renderAsSticker:Ljava/lang/Boolean;

    add-int/lit8 v7, p1, 0x1

    invoke-static {v0, v7, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_d

    .line 1140509
    :cond_23
    iget-object v0, p0, LX/6ke;->miniPreview:[B

    array-length v0, v0

    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1140510
    :goto_f
    if-ge v2, v1, :cond_26

    .line 1140511
    if-eqz v2, :cond_24

    const-string v0, " "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140512
    :cond_24
    iget-object v0, p0, LX/6ke;->miniPreview:[B

    aget-byte v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_25

    iget-object v0, p0, LX/6ke;->miniPreview:[B

    aget-byte v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, LX/6ke;->miniPreview:[B

    aget-byte v7, v7, v2

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    :goto_10
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1140513
    add-int/lit8 v2, v2, 0x1

    goto :goto_f

    .line 1140514
    :cond_25
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "0"

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, LX/6ke;->miniPreview:[B

    aget-byte v7, v7, v2

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_10

    .line 1140515
    :cond_26
    iget-object v0, p0, LX/6ke;->miniPreview:[B

    array-length v0, v0

    if-le v0, v8, :cond_14

    const-string v0, " ..."

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_e

    :cond_27
    move v0, v3

    goto/16 :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 5

    .prologue
    const/16 v4, 0xb

    const/16 v3, 0x8

    .line 1140338
    invoke-static {p0}, LX/6ke;->a(LX/6ke;)V

    .line 1140339
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1140340
    iget-object v0, p0, LX/6ke;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1140341
    iget-object v0, p0, LX/6ke;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1140342
    sget-object v0, LX/6ke;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140343
    iget-object v0, p0, LX/6ke;->width:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1140344
    :cond_0
    iget-object v0, p0, LX/6ke;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1140345
    iget-object v0, p0, LX/6ke;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1140346
    sget-object v0, LX/6ke;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140347
    iget-object v0, p0, LX/6ke;->height:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1140348
    :cond_1
    iget-object v0, p0, LX/6ke;->imageURIMap:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 1140349
    iget-object v0, p0, LX/6ke;->imageURIMap:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 1140350
    sget-object v0, LX/6ke;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140351
    new-instance v0, LX/7H3;

    iget-object v1, p0, LX/6ke;->imageURIMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v3, v4, v1}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 1140352
    iget-object v0, p0, LX/6ke;->imageURIMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1140353
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, LX/1su;->a(I)V

    .line 1140354
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1140355
    :cond_2
    iget-object v0, p0, LX/6ke;->imageSource:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1140356
    iget-object v0, p0, LX/6ke;->imageSource:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1140357
    sget-object v0, LX/6ke;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140358
    iget-object v0, p0, LX/6ke;->imageSource:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1140359
    :cond_3
    iget-object v0, p0, LX/6ke;->rawImageURI:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1140360
    iget-object v0, p0, LX/6ke;->rawImageURI:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1140361
    sget-object v0, LX/6ke;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140362
    iget-object v0, p0, LX/6ke;->rawImageURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1140363
    :cond_4
    iget-object v0, p0, LX/6ke;->rawImageURIFormat:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1140364
    iget-object v0, p0, LX/6ke;->rawImageURIFormat:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1140365
    sget-object v0, LX/6ke;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140366
    iget-object v0, p0, LX/6ke;->rawImageURIFormat:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1140367
    :cond_5
    iget-object v0, p0, LX/6ke;->animatedImageURIMap:Ljava/util/Map;

    if-eqz v0, :cond_6

    .line 1140368
    iget-object v0, p0, LX/6ke;->animatedImageURIMap:Ljava/util/Map;

    if-eqz v0, :cond_6

    .line 1140369
    sget-object v0, LX/6ke;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140370
    new-instance v0, LX/7H3;

    iget-object v1, p0, LX/6ke;->animatedImageURIMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v3, v4, v1}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 1140371
    iget-object v0, p0, LX/6ke;->animatedImageURIMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1140372
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, LX/1su;->a(I)V

    .line 1140373
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 1140374
    :cond_6
    iget-object v0, p0, LX/6ke;->imageURIMapFormat:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1140375
    iget-object v0, p0, LX/6ke;->imageURIMapFormat:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1140376
    sget-object v0, LX/6ke;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140377
    iget-object v0, p0, LX/6ke;->imageURIMapFormat:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1140378
    :cond_7
    iget-object v0, p0, LX/6ke;->animatedImageURIMapFormat:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1140379
    iget-object v0, p0, LX/6ke;->animatedImageURIMapFormat:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1140380
    sget-object v0, LX/6ke;->k:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140381
    iget-object v0, p0, LX/6ke;->animatedImageURIMapFormat:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1140382
    :cond_8
    iget-object v0, p0, LX/6ke;->renderAsSticker:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 1140383
    iget-object v0, p0, LX/6ke;->renderAsSticker:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 1140384
    sget-object v0, LX/6ke;->l:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140385
    iget-object v0, p0, LX/6ke;->renderAsSticker:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1140386
    :cond_9
    iget-object v0, p0, LX/6ke;->miniPreview:[B

    if-eqz v0, :cond_a

    .line 1140387
    iget-object v0, p0, LX/6ke;->miniPreview:[B

    if-eqz v0, :cond_a

    .line 1140388
    sget-object v0, LX/6ke;->m:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140389
    iget-object v0, p0, LX/6ke;->miniPreview:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 1140390
    :cond_a
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1140391
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1140392
    return-void
.end method

.method public final a(LX/6ke;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1140258
    if-nez p1, :cond_1

    .line 1140259
    :cond_0
    :goto_0
    return v2

    .line 1140260
    :cond_1
    iget-object v0, p0, LX/6ke;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1140261
    :goto_1
    iget-object v3, p1, LX/6ke;->width:Ljava/lang/Integer;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1140262
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1140263
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140264
    iget-object v0, p0, LX/6ke;->width:Ljava/lang/Integer;

    iget-object v3, p1, LX/6ke;->width:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140265
    :cond_3
    iget-object v0, p0, LX/6ke;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1140266
    :goto_3
    iget-object v3, p1, LX/6ke;->height:Ljava/lang/Integer;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1140267
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1140268
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140269
    iget-object v0, p0, LX/6ke;->height:Ljava/lang/Integer;

    iget-object v3, p1, LX/6ke;->height:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140270
    :cond_5
    iget-object v0, p0, LX/6ke;->imageURIMap:Ljava/util/Map;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1140271
    :goto_5
    iget-object v3, p1, LX/6ke;->imageURIMap:Ljava/util/Map;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1140272
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1140273
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140274
    iget-object v0, p0, LX/6ke;->imageURIMap:Ljava/util/Map;

    iget-object v3, p1, LX/6ke;->imageURIMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140275
    :cond_7
    iget-object v0, p0, LX/6ke;->imageSource:Ljava/lang/Integer;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1140276
    :goto_7
    iget-object v3, p1, LX/6ke;->imageSource:Ljava/lang/Integer;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1140277
    :goto_8
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1140278
    :cond_8
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140279
    iget-object v0, p0, LX/6ke;->imageSource:Ljava/lang/Integer;

    iget-object v3, p1, LX/6ke;->imageSource:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140280
    :cond_9
    iget-object v0, p0, LX/6ke;->rawImageURI:Ljava/lang/String;

    if-eqz v0, :cond_20

    move v0, v1

    .line 1140281
    :goto_9
    iget-object v3, p1, LX/6ke;->rawImageURI:Ljava/lang/String;

    if-eqz v3, :cond_21

    move v3, v1

    .line 1140282
    :goto_a
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1140283
    :cond_a
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140284
    iget-object v0, p0, LX/6ke;->rawImageURI:Ljava/lang/String;

    iget-object v3, p1, LX/6ke;->rawImageURI:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140285
    :cond_b
    iget-object v0, p0, LX/6ke;->rawImageURIFormat:Ljava/lang/String;

    if-eqz v0, :cond_22

    move v0, v1

    .line 1140286
    :goto_b
    iget-object v3, p1, LX/6ke;->rawImageURIFormat:Ljava/lang/String;

    if-eqz v3, :cond_23

    move v3, v1

    .line 1140287
    :goto_c
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1140288
    :cond_c
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140289
    iget-object v0, p0, LX/6ke;->rawImageURIFormat:Ljava/lang/String;

    iget-object v3, p1, LX/6ke;->rawImageURIFormat:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140290
    :cond_d
    iget-object v0, p0, LX/6ke;->animatedImageURIMap:Ljava/util/Map;

    if-eqz v0, :cond_24

    move v0, v1

    .line 1140291
    :goto_d
    iget-object v3, p1, LX/6ke;->animatedImageURIMap:Ljava/util/Map;

    if-eqz v3, :cond_25

    move v3, v1

    .line 1140292
    :goto_e
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1140293
    :cond_e
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140294
    iget-object v0, p0, LX/6ke;->animatedImageURIMap:Ljava/util/Map;

    iget-object v3, p1, LX/6ke;->animatedImageURIMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140295
    :cond_f
    iget-object v0, p0, LX/6ke;->imageURIMapFormat:Ljava/lang/String;

    if-eqz v0, :cond_26

    move v0, v1

    .line 1140296
    :goto_f
    iget-object v3, p1, LX/6ke;->imageURIMapFormat:Ljava/lang/String;

    if-eqz v3, :cond_27

    move v3, v1

    .line 1140297
    :goto_10
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1140298
    :cond_10
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140299
    iget-object v0, p0, LX/6ke;->imageURIMapFormat:Ljava/lang/String;

    iget-object v3, p1, LX/6ke;->imageURIMapFormat:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140300
    :cond_11
    iget-object v0, p0, LX/6ke;->animatedImageURIMapFormat:Ljava/lang/String;

    if-eqz v0, :cond_28

    move v0, v1

    .line 1140301
    :goto_11
    iget-object v3, p1, LX/6ke;->animatedImageURIMapFormat:Ljava/lang/String;

    if-eqz v3, :cond_29

    move v3, v1

    .line 1140302
    :goto_12
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 1140303
    :cond_12
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140304
    iget-object v0, p0, LX/6ke;->animatedImageURIMapFormat:Ljava/lang/String;

    iget-object v3, p1, LX/6ke;->animatedImageURIMapFormat:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140305
    :cond_13
    iget-object v0, p0, LX/6ke;->renderAsSticker:Ljava/lang/Boolean;

    if-eqz v0, :cond_2a

    move v0, v1

    .line 1140306
    :goto_13
    iget-object v3, p1, LX/6ke;->renderAsSticker:Ljava/lang/Boolean;

    if-eqz v3, :cond_2b

    move v3, v1

    .line 1140307
    :goto_14
    if-nez v0, :cond_14

    if-eqz v3, :cond_15

    .line 1140308
    :cond_14
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140309
    iget-object v0, p0, LX/6ke;->renderAsSticker:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6ke;->renderAsSticker:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140310
    :cond_15
    iget-object v0, p0, LX/6ke;->miniPreview:[B

    if-eqz v0, :cond_2c

    move v0, v1

    .line 1140311
    :goto_15
    iget-object v3, p1, LX/6ke;->miniPreview:[B

    if-eqz v3, :cond_2d

    move v3, v1

    .line 1140312
    :goto_16
    if-nez v0, :cond_16

    if-eqz v3, :cond_17

    .line 1140313
    :cond_16
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140314
    iget-object v0, p0, LX/6ke;->miniPreview:[B

    iget-object v3, p1, LX/6ke;->miniPreview:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_17
    move v2, v1

    .line 1140315
    goto/16 :goto_0

    :cond_18
    move v0, v2

    .line 1140316
    goto/16 :goto_1

    :cond_19
    move v3, v2

    .line 1140317
    goto/16 :goto_2

    :cond_1a
    move v0, v2

    .line 1140318
    goto/16 :goto_3

    :cond_1b
    move v3, v2

    .line 1140319
    goto/16 :goto_4

    :cond_1c
    move v0, v2

    .line 1140320
    goto/16 :goto_5

    :cond_1d
    move v3, v2

    .line 1140321
    goto/16 :goto_6

    :cond_1e
    move v0, v2

    .line 1140322
    goto/16 :goto_7

    :cond_1f
    move v3, v2

    .line 1140323
    goto/16 :goto_8

    :cond_20
    move v0, v2

    .line 1140324
    goto/16 :goto_9

    :cond_21
    move v3, v2

    .line 1140325
    goto/16 :goto_a

    :cond_22
    move v0, v2

    .line 1140326
    goto/16 :goto_b

    :cond_23
    move v3, v2

    .line 1140327
    goto/16 :goto_c

    :cond_24
    move v0, v2

    .line 1140328
    goto/16 :goto_d

    :cond_25
    move v3, v2

    .line 1140329
    goto/16 :goto_e

    :cond_26
    move v0, v2

    .line 1140330
    goto/16 :goto_f

    :cond_27
    move v3, v2

    .line 1140331
    goto/16 :goto_10

    :cond_28
    move v0, v2

    .line 1140332
    goto/16 :goto_11

    :cond_29
    move v3, v2

    .line 1140333
    goto/16 :goto_12

    :cond_2a
    move v0, v2

    .line 1140334
    goto :goto_13

    :cond_2b
    move v3, v2

    .line 1140335
    goto :goto_14

    :cond_2c
    move v0, v2

    .line 1140336
    goto :goto_15

    :cond_2d
    move v3, v2

    .line 1140337
    goto :goto_16
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1140254
    if-nez p1, :cond_1

    .line 1140255
    :cond_0
    :goto_0
    return v0

    .line 1140256
    :cond_1
    instance-of v1, p1, LX/6ke;

    if-eqz v1, :cond_0

    .line 1140257
    check-cast p1, LX/6ke;

    invoke-virtual {p0, p1}, LX/6ke;->a(LX/6ke;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1140253
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1140250
    sget-boolean v0, LX/6ke;->a:Z

    .line 1140251
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6ke;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1140252
    return-object v0
.end method
