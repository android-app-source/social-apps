.class public final LX/8Vy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Tj;


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/GamesTopScoresFragment;)V
    .locals 0

    .prologue
    .line 1353613
    iput-object p1, p0, LX/8Vy;->a:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1353614
    iget-object v0, p0, LX/8Vy;->a:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->c:LX/8TS;

    .line 1353615
    iput-object p3, v0, LX/8TS;->k:Ljava/lang/String;

    .line 1353616
    iget-object v0, p0, LX/8Vy;->a:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->a:LX/8WB;

    .line 1353617
    if-eqz p2, :cond_1

    .line 1353618
    invoke-static {p2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p3

    iput-object p3, v0, LX/8WB;->d:LX/0Px;

    .line 1353619
    :goto_0
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1353620
    iget-object v0, p0, LX/8Vy;->a:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->a:LX/8WB;

    .line 1353621
    if-eqz p1, :cond_2

    .line 1353622
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p2

    iput-object p2, v0, LX/8WB;->c:LX/0Px;

    .line 1353623
    :goto_1
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1353624
    iget-object v0, p0, LX/8Vy;->a:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    iget-boolean v0, v0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->f:Z

    if-eqz v0, :cond_0

    .line 1353625
    iget-object v0, p0, LX/8Vy;->a:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->a:LX/8WB;

    invoke-virtual {v0}, LX/8WB;->d()V

    .line 1353626
    :cond_0
    return-void

    .line 1353627
    :cond_1
    const/4 p3, 0x0

    iput-object p3, v0, LX/8WB;->d:LX/0Px;

    goto :goto_0

    .line 1353628
    :cond_2
    const/4 p2, 0x0

    iput-object p2, v0, LX/8WB;->c:LX/0Px;

    goto :goto_1
.end method
