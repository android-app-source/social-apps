.class public abstract LX/6nV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/content/ServiceConnection;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6nY;

.field public final b:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/6nY;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1147339
    iput-object p1, p0, LX/6nV;->a:LX/6nY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1147340
    iput-object p2, p0, LX/6nV;->b:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1147341
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/os/IBinder;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            ")TT;"
        }
    .end annotation
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5

    .prologue
    .line 1147342
    iget-object v0, p0, LX/6nV;->a:LX/6nY;

    iget-object v0, v0, LX/6nY;->b:Landroid/content/ComponentName;

    invoke-virtual {v0, p1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1147343
    const/4 v0, 0x0

    const-string v1, "%s service component mismatch. Expected: %s, was: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/6nV;->a:LX/6nY;

    iget-object v4, v4, LX/6nY;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/6nV;->a:LX/6nY;

    iget-object v4, v4, LX/6nY;->b:Landroid/content/ComponentName;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1147344
    iget-object v1, p0, LX/6nV;->b:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1147345
    iget-object v0, p0, LX/6nV;->a:LX/6nY;

    iget-object v0, v0, LX/6nY;->c:Landroid/content/Context;

    const v1, 0x6c95b1de

    invoke-static {v0, p0, v1}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V

    .line 1147346
    :goto_0
    return-void

    .line 1147347
    :cond_0
    iget-object v0, p0, LX/6nV;->a:LX/6nY;

    iget-object v0, v0, LX/6nY;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/oxygen/preloads/sdk/serviceproxy/ServiceProxy$ProxyConnection$1;

    invoke-direct {v1, p0, p2}, Lcom/facebook/oxygen/preloads/sdk/serviceproxy/ServiceProxy$ProxyConnection$1;-><init>(LX/6nV;Landroid/os/IBinder;)V

    const v2, -0x1c63764a

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 1147348
    return-void
.end method
