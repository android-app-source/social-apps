.class public final LX/6nX;
.super LX/6nV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6nV",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:J

.field public final synthetic c:Landroid/os/Bundle;

.field public final synthetic d:LX/6nZ;


# direct methods
.method public constructor <init>(LX/6nZ;Lcom/google/common/util/concurrent/SettableFuture;JLandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1147354
    iput-object p1, p0, LX/6nX;->d:LX/6nZ;

    iput-wide p3, p0, LX/6nX;->b:J

    iput-object p5, p0, LX/6nX;->c:Landroid/os/Bundle;

    invoke-direct {p0, p1, p2}, LX/6nV;-><init>(LX/6nY;Lcom/google/common/util/concurrent/SettableFuture;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1147355
    invoke-static {p1}, LX/6nZ;->b(Landroid/os/IBinder;)LX/6nK;

    move-result-object v0

    .line 1147356
    iget-wide v2, p0, LX/6nX;->b:J

    iget-object v1, p0, LX/6nX;->c:Landroid/os/Bundle;

    invoke-interface {v0, v2, v3, v1}, LX/6nK;->a(JLandroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 1147357
    const-string v1, "result"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
