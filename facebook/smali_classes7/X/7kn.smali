.class public LX/7kn;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/7km;


# instance fields
.field public final b:LX/11i;

.field public final c:LX/0So;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1232430
    new-instance v0, LX/7km;

    invoke-direct {v0}, LX/7km;-><init>()V

    sput-object v0, LX/7kn;->a:LX/7km;

    return-void
.end method

.method public constructor <init>(LX/11i;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1232431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232432
    iput-object p1, p0, LX/7kn;->b:LX/11i;

    .line 1232433
    iput-object p2, p0, LX/7kn;->c:LX/0So;

    .line 1232434
    return-void
.end method

.method public static a(LX/0QB;)LX/7kn;
    .locals 1

    .prologue
    .line 1232435
    invoke-static {p0}, LX/7kn;->b(LX/0QB;)LX/7kn;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/7kn;
    .locals 3

    .prologue
    .line 1232436
    new-instance v2, LX/7kn;

    invoke-static {p0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v0

    check-cast v0, LX/11i;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-direct {v2, v0, v1}, LX/7kn;-><init>(LX/11i;LX/0So;)V

    .line 1232437
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;JIIIZ)V
    .locals 12

    .prologue
    .line 1232438
    iget-object v0, p0, LX/7kn;->b:LX/11i;

    sget-object v1, LX/7kn;->a:LX/7km;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1232439
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1232440
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/11o;

    const-string v9, "loadPhoto"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    const-string v0, "index"

    invoke-static/range {p4 .. p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "bm_width"

    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "bm_height"

    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "is_video"

    invoke-static/range {p7 .. p7}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v3

    iget-object v0, p0, LX/7kn;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x125d6d85

    move-object v0, v8

    move-object v1, v9

    move-object v2, v10

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 1232441
    :cond_0
    return-void
.end method
