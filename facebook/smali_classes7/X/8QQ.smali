.class public LX/8QQ;
.super LX/8QL;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8QL",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1343032
    sget-object v0, LX/8vA;->LOADING:LX/8vA;

    invoke-direct {p0, v0}, LX/8QL;-><init>(LX/8vA;)V

    .line 1343033
    const v0, 0x7f081303

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8QQ;->e:Ljava/lang/String;

    .line 1343034
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1343041
    iget-object v0, p0, LX/8QQ;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1343043
    iget-object v0, p0, LX/8QQ;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1343042
    const/4 v0, -0x1

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1343039
    const/4 v0, -0x1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1343040
    instance-of v0, p1, LX/8QL;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8QL;->a:LX/8vA;

    check-cast p1, LX/8QL;

    iget-object v1, p1, LX/8QL;->a:LX/8vA;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1343038
    const/4 v0, -0x1

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1343037
    const/4 v0, -0x1

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1343036
    const/4 v0, 0x0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1343035
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    sget-object v2, LX/8vA;->LOADING:LX/8vA;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
