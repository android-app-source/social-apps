.class public abstract LX/6kT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;


# instance fields
.field public setField_:I

.field public value_:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1136933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1136934
    const/4 v0, 0x0

    iput v0, p0, LX/6kT;->setField_:I

    .line 1136935
    const/4 v0, 0x0

    iput-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    .line 1136936
    return-void
.end method


# virtual methods
.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1136922
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 1136923
    if-eqz v0, :cond_0

    .line 1136924
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1136925
    if-nez v0, :cond_1

    .line 1136926
    :cond_0
    new-instance v0, LX/7H4;

    const-string v1, "Cannot write a TUnion with no set value!"

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1136927
    :cond_1
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1136928
    iget v0, p0, LX/6kT;->setField_:I

    invoke-virtual {p0, v0}, LX/6kT;->b(I)LX/1sw;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136929
    iget v0, p0, LX/6kT;->setField_:I

    int-to-short v0, v0

    invoke-virtual {p0, p1, v0}, LX/6kT;->a(LX/1su;S)V

    .line 1136930
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1136931
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1136932
    return-void
.end method

.method public abstract a(LX/1su;S)V
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 1136902
    iget v0, p0, LX/6kT;->setField_:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b(I)LX/1sw;
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 1136903
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 1136904
    instance-of v1, v0, [B

    if-eqz v1, :cond_4

    .line 1136905
    check-cast v0, [B

    check-cast v0, [B

    const/16 v7, 0x80

    .line 1136906
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1136907
    array-length v1, v0

    invoke-static {v1, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 1136908
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    .line 1136909
    if-eqz v2, :cond_0

    .line 1136910
    const-string v1, " "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136911
    :cond_0
    aget-byte v1, v0, v2

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 1136912
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_1

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136913
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1136914
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "0"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1136915
    :cond_2
    array-length v1, v0

    if-le v1, v7, :cond_3

    .line 1136916
    const-string v1, " ..."

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136917
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1136918
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1136919
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 1136920
    invoke-virtual {p0, v2}, LX/6kT;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1136921
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
