.class public LX/7WR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0

    .prologue
    .line 1216388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1216389
    iput-object p1, p0, LX/7WR;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1216390
    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1216403
    return-void
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1216402
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1216391
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1216392
    if-eqz p1, :cond_0

    .line 1216393
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1216394
    if-eqz v0, :cond_0

    .line 1216395
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1216396
    check-cast v0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;->a()Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1216397
    :cond_0
    :goto_0
    return-void

    .line 1216398
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1216399
    check-cast v0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;->a()Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    move-result-object v0

    .line 1216400
    iget-object v1, p0, LX/7WR;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0df;->N:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->p()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0df;->O:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0df;->P:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->t()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0df;->Q:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->s()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0df;->R:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0df;->S:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0df;->T:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0df;->U:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0df;->V:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0df;->W:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1216401
    invoke-virtual {p0, p1}, LX/7WR;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_0
.end method
