.class public final LX/8Ua;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1350923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 1350924
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1350925
    iget-object v1, p0, LX/8Ua;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1350926
    iget-object v3, p0, LX/8Ua;->b:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1350927
    iget-object v5, p0, LX/8Ua;->c:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1350928
    iget-object v6, p0, LX/8Ua;->d:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1350929
    const/4 v7, 0x4

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1350930
    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1350931
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1350932
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1350933
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1350934
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1350935
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1350936
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1350937
    invoke-virtual {v1, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1350938
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1350939
    new-instance v1, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;

    invoke-direct {v1, v0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;-><init>(LX/15i;)V

    .line 1350940
    return-object v1
.end method
