.class public LX/7U4;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/1HI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1211317
    const-class v0, LX/7U4;

    sput-object v0, LX/7U4;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1HI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1211318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1211319
    iput-object p1, p0, LX/7U4;->b:LX/1HI;

    .line 1211320
    return-void
.end method

.method public static a$redex0(LX/7U4;Ljava/util/Iterator;LX/4e3;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "LX/1bf;",
            ">;",
            "LX/4e3",
            "<",
            "LX/1ln;",
            ">;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1211321
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1211322
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1bf;

    .line 1211323
    iget-object v0, p0, LX/7U4;->b:LX/1HI;

    invoke-virtual {v0, v2, p3}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v6

    .line 1211324
    new-instance v0, LX/7U3;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/7U3;-><init>(LX/7U4;LX/1bf;LX/4e3;Ljava/util/Iterator;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1211325
    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-interface {v6, v0, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1211326
    return-void
.end method
