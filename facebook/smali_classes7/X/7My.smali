.class public LX/7My;
.super LX/2oy;
.source ""


# instance fields
.field private a:I

.field public b:Z

.field public c:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

.field public d:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1199517
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7My;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1199518
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1199563
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7My;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199564
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1199552
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199553
    const/4 v0, 0x0

    iput v0, p0, LX/7My;->a:I

    .line 1199554
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7My;->b:Z

    .line 1199555
    const v0, 0x7f030738

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1199556
    const v0, 0x7f0d134b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    iput-object v0, p0, LX/7My;->d:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    .line 1199557
    new-instance v0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-direct {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;-><init>()V

    iput-object v0, p0, LX/7My;->c:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    .line 1199558
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p1, LX/7Mx;

    invoke-direct {p1, p0}, LX/7Mx;-><init>(LX/7My;)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199559
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p1, LX/7Mu;

    invoke-direct {p1, p0}, LX/7Mu;-><init>(LX/7My;)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199560
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p1, LX/7Mv;

    invoke-direct {p1, p0}, LX/7Mv;-><init>(LX/7My;)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199561
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p1, LX/7Mw;

    invoke-direct {p1, p0}, LX/7Mw;-><init>(LX/7My;)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199562
    return-void
.end method

.method public static c(LX/7My;I)V
    .locals 2

    .prologue
    .line 1199541
    iget-boolean v0, p0, LX/7My;->b:Z

    if-nez v0, :cond_1

    .line 1199542
    const/16 v0, 0xc

    iput v0, p0, LX/7My;->a:I

    .line 1199543
    :goto_0
    iget-object v0, p0, LX/7My;->d:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    if-eqz v0, :cond_0

    .line 1199544
    iget-object v0, p0, LX/7My;->d:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    iget v1, p0, LX/7My;->a:I

    .line 1199545
    iput v1, v0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->j:I

    .line 1199546
    :cond_0
    return-void

    .line 1199547
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 1199548
    const/16 v0, 0x78

    iput v0, p0, LX/7My;->a:I

    goto :goto_0

    .line 1199549
    :cond_2
    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    .line 1199550
    const/16 v0, 0xf0

    iput v0, p0, LX/7My;->a:I

    goto :goto_0

    .line 1199551
    :cond_3
    invoke-static {p0}, LX/7My;->j(LX/7My;)V

    goto :goto_0
.end method

.method public static h(LX/7My;)V
    .locals 10

    .prologue
    const-wide/16 v2, 0x12c

    .line 1199565
    iget-object v0, p0, LX/7My;->c:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    iget-object v1, p0, LX/7My;->d:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    const-wide/16 v6, 0x7d0

    const-wide/16 v8, 0x0

    move-wide v4, v2

    invoke-virtual/range {v0 .. v9}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->a(Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;JJJJ)V

    .line 1199566
    iget-object v0, p0, LX/7My;->d:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->setVisibility(I)V

    .line 1199567
    iget-object v0, p0, LX/7My;->d:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    iget v1, p0, LX/7My;->a:I

    .line 1199568
    iput v1, v0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->j:I

    .line 1199569
    return-void
.end method

.method public static j(LX/7My;)V
    .locals 2

    .prologue
    .line 1199531
    invoke-virtual {p0}, LX/7My;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1199532
    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_1

    .line 1199533
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-static {p0, v0}, LX/7My;->c(LX/7My;I)V

    .line 1199534
    :goto_0
    iget-object v0, p0, LX/7My;->d:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    if-eqz v0, :cond_0

    .line 1199535
    iget-object v0, p0, LX/7My;->d:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    iget v1, p0, LX/7My;->a:I

    .line 1199536
    iput v1, v0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->j:I

    .line 1199537
    :cond_0
    return-void

    .line 1199538
    :cond_1
    iget-boolean v0, p0, LX/7My;->b:Z

    if-eqz v0, :cond_2

    .line 1199539
    const/16 v0, 0x78

    iput v0, p0, LX/7My;->a:I

    goto :goto_0

    .line 1199540
    :cond_2
    const/16 v0, 0xc

    iput v0, p0, LX/7My;->a:I

    goto :goto_0
.end method

.method public static u(LX/7My;)V
    .locals 1

    .prologue
    .line 1199529
    iget-object v0, p0, LX/7My;->c:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->b()V

    .line 1199530
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 1

    .prologue
    .line 1199522
    invoke-super {p0, p1, p2}, LX/2oy;->a(LX/2pa;Z)V

    .line 1199523
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7My;->b:Z

    .line 1199524
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/7My;->d:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    if-eqz v0, :cond_0

    .line 1199525
    invoke-static {p0}, LX/7My;->j(LX/7My;)V

    .line 1199526
    invoke-static {p0}, LX/7My;->h(LX/7My;)V

    .line 1199527
    iget-object v0, p0, LX/7My;->c:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->h()V

    .line 1199528
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1199519
    invoke-super {p0}, LX/2oy;->d()V

    .line 1199520
    invoke-static {p0}, LX/7My;->u(LX/7My;)V

    .line 1199521
    return-void
.end method
