.class public final LX/6zl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6w3;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6w3",
        "<",
        "Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/03V;

.field public final b:LX/6zd;

.field public final c:LX/6zi;

.field private d:LX/6qh;


# direct methods
.method public constructor <init>(LX/03V;LX/6zd;LX/6zi;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1161464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161465
    iput-object p1, p0, LX/6zl;->a:LX/03V;

    .line 1161466
    iput-object p2, p0, LX/6zl;->b:LX/6zd;

    .line 1161467
    iput-object p3, p0, LX/6zl;->c:LX/6zi;

    .line 1161468
    return-void
.end method

.method public static a(LX/6zl;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V
    .locals 4

    .prologue
    .line 1161458
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1161459
    const-string v1, "selected_payment_method"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1161460
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1161461
    const-string v2, "extra_activity_result_data"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1161462
    iget-object v0, p0, LX/6zl;->d:LX/6qh;

    new-instance v2, LX/73T;

    sget-object v3, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v2, v3, v1}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {v0, v2}, LX/6qh;->a(LX/73T;)V

    .line 1161463
    return-void
.end method

.method public static a(LX/6zl;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;)V
    .locals 4

    .prologue
    .line 1161454
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1161455
    const-string v1, "extra_reset_data"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1161456
    iget-object v1, p0, LX/6zl;->d:LX/6qh;

    new-instance v2, LX/73T;

    sget-object v3, LX/73S;->RESET:LX/73S;

    invoke-direct {v2, v3, v0}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, LX/6qh;->a(LX/73T;)V

    .line 1161457
    return-void
.end method

.method public static a$redex0(LX/6zl;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;ILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 1161439
    iget-object v0, p1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1161440
    packed-switch p3, :pswitch_data_0

    .line 1161441
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown request code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1161442
    :pswitch_0
    const-string v1, "encoded_credential_id"

    invoke-virtual {p4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1161443
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1161444
    :cond_0
    iget-object v2, p0, LX/6zl;->a:LX/03V;

    const-class v3, LX/6zl;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Added credential %s was not returned by server as an available payment method"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object v1, v5, p3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1161445
    invoke-static {p0, p1}, LX/6zl;->a(LX/6zl;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;)V

    .line 1161446
    :goto_0
    return-void

    .line 1161447
    :pswitch_1
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    move-object v1, v1

    .line 1161448
    invoke-static {v1}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v1

    const-class v2, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    invoke-virtual {v1, v2}, LX/0wv;->a(Ljava/lang/Class;)LX/0wv;

    move-result-object v1

    invoke-virtual {v1}, LX/0wv;->a()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    .line 1161449
    if-nez v1, :cond_2

    .line 1161450
    invoke-static {p0, p1}, LX/6zl;->a(LX/6zl;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;)V

    .line 1161451
    :goto_1
    goto :goto_0

    .line 1161452
    :cond_1
    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    move-result-object v1

    invoke-static {p0, v1}, LX/6zl;->a(LX/6zl;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V

    goto :goto_0

    .line 1161453
    :cond_2
    invoke-static {p0, v1}, LX/6zl;->a(LX/6zl;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/6qh;LX/70k;)V
    .locals 1

    .prologue
    .line 1161410
    iput-object p1, p0, LX/6zl;->d:LX/6qh;

    .line 1161411
    iget-object v0, p0, LX/6zl;->c:LX/6zi;

    invoke-virtual {v0, p2}, LX/6zi;->a(LX/70k;)V

    .line 1161412
    return-void
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1161413
    check-cast p1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;

    const/4 v0, -0x1

    .line 1161414
    packed-switch p2, :pswitch_data_0

    .line 1161415
    :cond_0
    :goto_0
    return-void

    .line 1161416
    :pswitch_0
    if-ne p3, v0, :cond_0

    .line 1161417
    invoke-static {}, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;->newBuilder()LX/707;

    move-result-object v1

    .line 1161418
    iget-object v0, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    move-object v0, v0

    .line 1161419
    check-cast v0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;

    invoke-virtual {v1, v0}, LX/707;->a(Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;)LX/707;

    move-result-object v0

    const/4 v1, 0x1

    .line 1161420
    iput-boolean v1, v0, LX/707;->a:Z

    .line 1161421
    move-object v0, v0

    .line 1161422
    invoke-virtual {v0}, LX/707;->e()Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;

    move-result-object v0

    .line 1161423
    iget-object v1, p0, LX/6zl;->b:LX/6zd;

    sget-object p2, LX/708;->SELECT_PAYMENT_METHOD:LX/708;

    const-string p3, "encoded_credential_id"

    invoke-virtual {p4, p3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v1, p1, v0, p2, p3}, LX/6vv;->a(Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;LX/6vZ;Ljava/lang/String;)V

    .line 1161424
    goto :goto_0

    .line 1161425
    :pswitch_1
    if-ne p3, v0, :cond_0

    .line 1161426
    :pswitch_2
    invoke-static {}, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;->newBuilder()LX/707;

    move-result-object v1

    .line 1161427
    iget-object v0, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    move-object v0, v0

    .line 1161428
    check-cast v0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;

    invoke-virtual {v1, v0}, LX/707;->a(Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;)LX/707;

    move-result-object v0

    const/4 v1, 0x1

    .line 1161429
    iput-boolean v1, v0, LX/707;->a:Z

    .line 1161430
    move-object v0, v0

    .line 1161431
    invoke-virtual {v0}, LX/707;->e()Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;

    move-result-object v2

    .line 1161432
    new-instance v3, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;

    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    .line 1161433
    iget-object v1, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v1, v1

    .line 1161434
    check-cast v1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;

    .line 1161435
    iget-object p3, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->d:LX/0P1;

    move-object p3, p3

    .line 1161436
    invoke-direct {v3, v0, v2, v1, p3}, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;-><init>(Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;LX/0P1;)V

    .line 1161437
    iget-object v0, p0, LX/6zl;->c:LX/6zi;

    new-instance v1, LX/6zk;

    invoke-direct {v1, p0, p1, p2, p4}, LX/6zk;-><init>(LX/6zl;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;ILandroid/content/Intent;)V

    invoke-virtual {v0, v1, v3}, LX/6zi;->a(LX/6zj;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;)V

    .line 1161438
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
