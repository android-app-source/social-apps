.class public LX/8Or;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/analytics/logger/HoneyClientEvent;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:I

.field public j:Z

.field private k:I

.field private l:Z

.field public m:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1341179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341180
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1341181
    iput-object p2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1341182
    move-object v0, v0

    .line 1341183
    iput-object v0, p0, LX/8Or;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1341184
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)LX/8Or;
    .locals 1

    .prologue
    .line 1341171
    if-nez p1, :cond_0

    .line 1341172
    :goto_0
    return-object p0

    .line 1341173
    :cond_0
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_1

    .line 1341174
    check-cast p1, Lcom/facebook/fbservice/service/ServiceException;

    .line 1341175
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 1341176
    invoke-virtual {v0}, LX/1nY;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8Or;->d:Ljava/lang/String;

    .line 1341177
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/ServiceException;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8Or;->e:Ljava/lang/String;

    goto :goto_0

    .line 1341178
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8Or;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a()Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 1341145
    iget-object v0, p0, LX/8Or;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8Or;->h:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1341146
    :cond_0
    const/4 v0, 0x0

    .line 1341147
    :goto_0
    return-object v0

    .line 1341148
    :cond_1
    iget-object v0, p0, LX/8Or;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "app_id"

    iget-object v2, p0, LX/8Or;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1341149
    iget-object v0, p0, LX/8Or;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "type"

    iget-object v2, p0, LX/8Or;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1341150
    iget-object v0, p0, LX/8Or;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1341151
    iget-object v0, p0, LX/8Or;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "action_id"

    iget-object v2, p0, LX/8Or;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1341152
    :cond_2
    iget v0, p0, LX/8Or;->i:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 1341153
    iget-object v0, p0, LX/8Or;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "num_photos"

    iget v2, p0, LX/8Or;->i:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1341154
    :cond_3
    iget-boolean v0, p0, LX/8Or;->j:Z

    if-eqz v0, :cond_4

    .line 1341155
    iget-object v0, p0, LX/8Or;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "has_video"

    iget-boolean v2, p0, LX/8Or;->j:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1341156
    :cond_4
    iget-object v0, p0, LX/8Or;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1341157
    iget-object v0, p0, LX/8Or;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "error_code"

    iget-object v2, p0, LX/8Or;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1341158
    :cond_5
    iget-object v0, p0, LX/8Or;->e:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1341159
    iget-object v0, p0, LX/8Or;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "error"

    iget-object v2, p0, LX/8Or;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1341160
    :cond_6
    iget-object v0, p0, LX/8Or;->f:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1341161
    iget-object v0, p0, LX/8Or;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "error_response"

    iget-object v2, p0, LX/8Or;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1341162
    :cond_7
    iget-object v0, p0, LX/8Or;->g:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1341163
    iget-object v0, p0, LX/8Or;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "method"

    iget-object v2, p0, LX/8Or;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1341164
    :cond_8
    iget v0, p0, LX/8Or;->k:I

    if-eqz v0, :cond_9

    .line 1341165
    iget-object v0, p0, LX/8Or;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "num_recipients"

    iget v2, p0, LX/8Or;->k:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1341166
    :cond_9
    iget-boolean v0, p0, LX/8Or;->l:Z

    if-eqz v0, :cond_a

    .line 1341167
    iget-object v0, p0, LX/8Or;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "has_message"

    iget-boolean v2, p0, LX/8Or;->l:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1341168
    :cond_a
    iget-boolean v0, p0, LX/8Or;->m:Z

    if-eqz v0, :cond_b

    .line 1341169
    iget-object v0, p0, LX/8Or;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "is_native_intent"

    iget-boolean v2, p0, LX/8Or;->m:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1341170
    :cond_b
    iget-object v0, p0, LX/8Or;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto/16 :goto_0
.end method
