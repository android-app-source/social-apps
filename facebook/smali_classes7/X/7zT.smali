.class public LX/7zT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/7zU;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1280999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 1281000
    check-cast p1, LX/7zU;

    .line 1281001
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "Resumable-Upload-Get"

    .line 1281002
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1281003
    move-object v0, v0

    .line 1281004
    const-string v1, "GET"

    .line 1281005
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1281006
    move-object v0, v0

    .line 1281007
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1281008
    iget-object v1, p1, LX/7zU;->b:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 1281009
    iget-object v1, p1, LX/7zU;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1281010
    new-instance p0, Lorg/apache/http/message/BasicNameValuePair;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v2, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1281011
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 1281012
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1281013
    move-object v0, v0

    .line 1281014
    const/4 v1, 0x1

    .line 1281015
    iput-boolean v1, v0, LX/14O;->o:Z

    .line 1281016
    move-object v0, v0

    .line 1281017
    iget-object v1, p1, LX/7zU;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1281018
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 1281019
    move-object v0, v0

    .line 1281020
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 1281021
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1281022
    move-object v0, v0

    .line 1281023
    sget-object v1, LX/14P;->RETRY_SAFE:LX/14P;

    invoke-virtual {v0, v1}, LX/14O;->a(LX/14P;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1281024
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1281025
    invoke-virtual {p2}, LX/1pN;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
