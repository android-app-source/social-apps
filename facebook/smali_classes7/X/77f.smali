.class public LX/77f;
.super LX/2g7;
.source ""


# instance fields
.field private final a:LX/13N;

.field private final b:LX/0SG;


# direct methods
.method public constructor <init>(LX/13N;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1172008
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1172009
    iput-object p1, p0, LX/77f;->a:LX/13N;

    .line 1172010
    iput-object p2, p0, LX/77f;->b:LX/0SG;

    .line 1172011
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 6

    .prologue
    .line 1172012
    iget-object v0, p0, LX/77f;->a:LX/13N;

    sget-object v1, LX/2fy;->DISMISSAL:LX/2fy;

    invoke-virtual {v0, p1, v1}, LX/13N;->d(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)J

    move-result-wide v0

    .line 1172013
    iget-object v2, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 1172014
    iget-object v4, p0, LX/77f;->b:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    add-long/2addr v0, v2

    cmp-long v0, v4, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
