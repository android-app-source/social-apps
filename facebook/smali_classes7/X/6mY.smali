.class public LX/6mY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;


# instance fields
.field public final detailedClientPresence:Ljava/lang/Short;

.field public final lastActiveTimeSec:Ljava/lang/Long;

.field public final state:Ljava/lang/Integer;

.field public final uid:Ljava/lang/Long;

.field public final voipCapabilities:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0xa

    .line 1144953
    new-instance v0, LX/1sv;

    const-string v1, "PresenceUpdate"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mY;->b:LX/1sv;

    .line 1144954
    new-instance v0, LX/1sw;

    const-string v1, "uid"

    invoke-direct {v0, v1, v4, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mY;->c:LX/1sw;

    .line 1144955
    new-instance v0, LX/1sw;

    const-string v1, "state"

    const/16 v2, 0x8

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mY;->d:LX/1sw;

    .line 1144956
    new-instance v0, LX/1sw;

    const-string v1, "lastActiveTimeSec"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mY;->e:LX/1sw;

    .line 1144957
    new-instance v0, LX/1sw;

    const-string v1, "detailedClientPresence"

    const/4 v2, 0x6

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mY;->f:LX/1sw;

    .line 1144958
    new-instance v0, LX/1sw;

    const-string v1, "voipCapabilities"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mY;->g:LX/1sw;

    .line 1144959
    sput-boolean v5, LX/6mY;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Short;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1144960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1144961
    iput-object p1, p0, LX/6mY;->uid:Ljava/lang/Long;

    .line 1144962
    iput-object p2, p0, LX/6mY;->state:Ljava/lang/Integer;

    .line 1144963
    iput-object p3, p0, LX/6mY;->lastActiveTimeSec:Ljava/lang/Long;

    .line 1144964
    iput-object p4, p0, LX/6mY;->detailedClientPresence:Ljava/lang/Short;

    .line 1144965
    iput-object p5, p0, LX/6mY;->voipCapabilities:Ljava/lang/Long;

    .line 1144966
    return-void
.end method

.method public static a(LX/6mY;)V
    .locals 3

    .prologue
    .line 1144967
    iget-object v0, p0, LX/6mY;->state:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, LX/6mX;->a:LX/1sn;

    iget-object v1, p0, LX/6mY;->state:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1144968
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'state\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6mY;->state:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1144969
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1144970
    if-eqz p2, :cond_9

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1144971
    :goto_0
    if-eqz p2, :cond_a

    const-string v0, "\n"

    move-object v3, v0

    .line 1144972
    :goto_1
    if-eqz p2, :cond_b

    const-string v0, " "

    move-object v1, v0

    .line 1144973
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "PresenceUpdate"

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1144974
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144975
    const-string v0, "("

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144976
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144977
    const/4 v0, 0x1

    .line 1144978
    iget-object v6, p0, LX/6mY;->uid:Ljava/lang/Long;

    if-eqz v6, :cond_0

    .line 1144979
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144980
    const-string v0, "uid"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144981
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144982
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144983
    iget-object v0, p0, LX/6mY;->uid:Ljava/lang/Long;

    if-nez v0, :cond_c

    .line 1144984
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v0, v2

    .line 1144985
    :cond_0
    iget-object v6, p0, LX/6mY;->state:Ljava/lang/Integer;

    if-eqz v6, :cond_3

    .line 1144986
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144987
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144988
    const-string v0, "state"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144989
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144990
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144991
    iget-object v0, p0, LX/6mY;->state:Ljava/lang/Integer;

    if-nez v0, :cond_d

    .line 1144992
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    :goto_4
    move v0, v2

    .line 1144993
    :cond_3
    iget-object v6, p0, LX/6mY;->lastActiveTimeSec:Ljava/lang/Long;

    if-eqz v6, :cond_5

    .line 1144994
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144995
    :cond_4
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144996
    const-string v0, "lastActiveTimeSec"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144997
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144998
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144999
    iget-object v0, p0, LX/6mY;->lastActiveTimeSec:Ljava/lang/Long;

    if-nez v0, :cond_f

    .line 1145000
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v0, v2

    .line 1145001
    :cond_5
    iget-object v6, p0, LX/6mY;->detailedClientPresence:Ljava/lang/Short;

    if-eqz v6, :cond_12

    .line 1145002
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145003
    :cond_6
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145004
    const-string v0, "detailedClientPresence"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145005
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145006
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145007
    iget-object v0, p0, LX/6mY;->detailedClientPresence:Ljava/lang/Short;

    if-nez v0, :cond_10

    .line 1145008
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145009
    :goto_6
    iget-object v0, p0, LX/6mY;->voipCapabilities:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 1145010
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145011
    :cond_7
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145012
    const-string v0, "voipCapabilities"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145013
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145014
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145015
    iget-object v0, p0, LX/6mY;->voipCapabilities:Ljava/lang/Long;

    if-nez v0, :cond_11

    .line 1145016
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145017
    :cond_8
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145018
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145019
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1145020
    :cond_9
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1145021
    :cond_a
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1145022
    :cond_b
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 1145023
    :cond_c
    iget-object v0, p0, LX/6mY;->uid:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1145024
    :cond_d
    sget-object v0, LX/6mX;->b:Ljava/util/Map;

    iget-object v6, p0, LX/6mY;->state:Ljava/lang/Integer;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1145025
    if-eqz v0, :cond_e

    .line 1145026
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145027
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145028
    :cond_e
    iget-object v6, p0, LX/6mY;->state:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1145029
    if-eqz v0, :cond_2

    .line 1145030
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1145031
    :cond_f
    iget-object v0, p0, LX/6mY;->lastActiveTimeSec:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1145032
    :cond_10
    iget-object v0, p0, LX/6mY;->detailedClientPresence:Ljava/lang/Short;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1145033
    :cond_11
    iget-object v0, p0, LX/6mY;->voipCapabilities:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    :cond_12
    move v2, v0

    goto/16 :goto_6
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1145034
    invoke-static {p0}, LX/6mY;->a(LX/6mY;)V

    .line 1145035
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1145036
    iget-object v0, p0, LX/6mY;->uid:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1145037
    iget-object v0, p0, LX/6mY;->uid:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1145038
    sget-object v0, LX/6mY;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145039
    iget-object v0, p0, LX/6mY;->uid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1145040
    :cond_0
    iget-object v0, p0, LX/6mY;->state:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1145041
    iget-object v0, p0, LX/6mY;->state:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1145042
    sget-object v0, LX/6mY;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145043
    iget-object v0, p0, LX/6mY;->state:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1145044
    :cond_1
    iget-object v0, p0, LX/6mY;->lastActiveTimeSec:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1145045
    iget-object v0, p0, LX/6mY;->lastActiveTimeSec:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1145046
    sget-object v0, LX/6mY;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145047
    iget-object v0, p0, LX/6mY;->lastActiveTimeSec:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1145048
    :cond_2
    iget-object v0, p0, LX/6mY;->detailedClientPresence:Ljava/lang/Short;

    if-eqz v0, :cond_3

    .line 1145049
    iget-object v0, p0, LX/6mY;->detailedClientPresence:Ljava/lang/Short;

    if-eqz v0, :cond_3

    .line 1145050
    sget-object v0, LX/6mY;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145051
    iget-object v0, p0, LX/6mY;->detailedClientPresence:Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(S)V

    .line 1145052
    :cond_3
    iget-object v0, p0, LX/6mY;->voipCapabilities:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1145053
    iget-object v0, p0, LX/6mY;->voipCapabilities:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1145054
    sget-object v0, LX/6mY;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145055
    iget-object v0, p0, LX/6mY;->voipCapabilities:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1145056
    :cond_4
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1145057
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1145058
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1145059
    if-nez p1, :cond_1

    .line 1145060
    :cond_0
    :goto_0
    return v0

    .line 1145061
    :cond_1
    instance-of v1, p1, LX/6mY;

    if-eqz v1, :cond_0

    .line 1145062
    check-cast p1, LX/6mY;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1145063
    if-nez p1, :cond_3

    .line 1145064
    :cond_2
    :goto_1
    move v0, v2

    .line 1145065
    goto :goto_0

    .line 1145066
    :cond_3
    iget-object v0, p0, LX/6mY;->uid:Ljava/lang/Long;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1145067
    :goto_2
    iget-object v3, p1, LX/6mY;->uid:Ljava/lang/Long;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1145068
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1145069
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145070
    iget-object v0, p0, LX/6mY;->uid:Ljava/lang/Long;

    iget-object v3, p1, LX/6mY;->uid:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145071
    :cond_5
    iget-object v0, p0, LX/6mY;->state:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1145072
    :goto_4
    iget-object v3, p1, LX/6mY;->state:Ljava/lang/Integer;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1145073
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1145074
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145075
    iget-object v0, p0, LX/6mY;->state:Ljava/lang/Integer;

    iget-object v3, p1, LX/6mY;->state:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145076
    :cond_7
    iget-object v0, p0, LX/6mY;->lastActiveTimeSec:Ljava/lang/Long;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1145077
    :goto_6
    iget-object v3, p1, LX/6mY;->lastActiveTimeSec:Ljava/lang/Long;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1145078
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1145079
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145080
    iget-object v0, p0, LX/6mY;->lastActiveTimeSec:Ljava/lang/Long;

    iget-object v3, p1, LX/6mY;->lastActiveTimeSec:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145081
    :cond_9
    iget-object v0, p0, LX/6mY;->detailedClientPresence:Ljava/lang/Short;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1145082
    :goto_8
    iget-object v3, p1, LX/6mY;->detailedClientPresence:Ljava/lang/Short;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1145083
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1145084
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145085
    iget-object v0, p0, LX/6mY;->detailedClientPresence:Ljava/lang/Short;

    iget-object v3, p1, LX/6mY;->detailedClientPresence:Ljava/lang/Short;

    invoke-virtual {v0, v3}, Ljava/lang/Short;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145086
    :cond_b
    iget-object v0, p0, LX/6mY;->voipCapabilities:Ljava/lang/Long;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1145087
    :goto_a
    iget-object v3, p1, LX/6mY;->voipCapabilities:Ljava/lang/Long;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1145088
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1145089
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145090
    iget-object v0, p0, LX/6mY;->voipCapabilities:Ljava/lang/Long;

    iget-object v3, p1, LX/6mY;->voipCapabilities:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_d
    move v2, v1

    .line 1145091
    goto/16 :goto_1

    :cond_e
    move v0, v2

    .line 1145092
    goto/16 :goto_2

    :cond_f
    move v3, v2

    .line 1145093
    goto/16 :goto_3

    :cond_10
    move v0, v2

    .line 1145094
    goto :goto_4

    :cond_11
    move v3, v2

    .line 1145095
    goto :goto_5

    :cond_12
    move v0, v2

    .line 1145096
    goto :goto_6

    :cond_13
    move v3, v2

    .line 1145097
    goto :goto_7

    :cond_14
    move v0, v2

    .line 1145098
    goto :goto_8

    :cond_15
    move v3, v2

    .line 1145099
    goto :goto_9

    :cond_16
    move v0, v2

    .line 1145100
    goto :goto_a

    :cond_17
    move v3, v2

    .line 1145101
    goto :goto_b
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1145102
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1145103
    sget-boolean v0, LX/6mY;->a:Z

    .line 1145104
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mY;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1145105
    return-object v0
.end method
