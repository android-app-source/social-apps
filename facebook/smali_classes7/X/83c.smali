.class public final LX/83c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2hs;


# direct methods
.method public constructor <init>(LX/2hs;)V
    .locals 0

    .prologue
    .line 1290565
    iput-object p1, p0, LX/83c;->a:LX/2hs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1290566
    iget-object v0, p0, LX/83c;->a:LX/2hs;

    .line 1290567
    iget-boolean v1, v0, LX/2hs;->g:Z

    if-eqz v1, :cond_0

    .line 1290568
    iget-object v1, v0, LX/2hs;->f:LX/2dl;

    sget-object p0, LX/2hs;->c:LX/0Rf;

    invoke-virtual {v1, p0}, LX/2dl;->a(Ljava/util/Collection;)V

    .line 1290569
    const/4 v1, 0x0

    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1290570
    :goto_0
    move-object v0, v1

    .line 1290571
    return-object v0

    :cond_0
    iget-object v1, v0, LX/2hs;->e:LX/1mR;

    sget-object p0, LX/2hs;->c:LX/0Rf;

    invoke-virtual {v1, p0}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0
.end method
