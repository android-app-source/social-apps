.class public final LX/7Jx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Jp;


# instance fields
.field public final synthetic a:LX/7K1;


# direct methods
.method public constructor <init>(LX/7K1;)V
    .locals 0

    .prologue
    .line 1194286
    iput-object p1, p0, LX/7Jx;->a:LX/7K1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Jw;LX/0GT;ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1194287
    iget-object v0, p0, LX/7Jx;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    if-eqz v0, :cond_0

    .line 1194288
    iget-object v0, p0, LX/7Jx;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/0Jw;LX/0GT;ILjava/lang/String;)V

    .line 1194289
    :goto_0
    return-void

    .line 1194290
    :cond_0
    iget-object v0, p0, LX/7Jx;->a:LX/7K1;

    if-nez p1, :cond_1

    new-instance p1, LX/0Ku;

    invoke-direct {p1}, LX/0Ku;-><init>()V

    .line 1194291
    :cond_1
    iput-object p1, v0, LX/7K1;->o:LX/0GT;

    .line 1194292
    iget-object v0, p0, LX/7Jx;->a:LX/7K1;

    if-nez p2, :cond_2

    new-instance p2, LX/0Ku;

    invoke-direct {p2}, LX/0Ku;-><init>()V

    .line 1194293
    :cond_2
    iput-object p2, v0, LX/7K1;->p:LX/0GT;

    .line 1194294
    iget-object v0, p0, LX/7Jx;->a:LX/7K1;

    .line 1194295
    iput p3, v0, LX/7K1;->q:I

    .line 1194296
    iget-object v0, p0, LX/7Jx;->a:LX/7K1;

    .line 1194297
    iput-object p4, v0, LX/7K1;->r:Ljava/lang/String;

    .line 1194298
    iget-object v0, p0, LX/7Jx;->a:LX/7K1;

    const/4 v1, 0x3

    .line 1194299
    iput v1, v0, LX/7K1;->e:I

    .line 1194300
    iget-object v0, p0, LX/7Jx;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->m:LX/0Kx;

    invoke-interface {v0, v3}, LX/0Kx;->a(Z)V

    .line 1194301
    iget-object v0, p0, LX/7Jx;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->m:LX/0Kx;

    const/4 v1, 0x2

    new-array v1, v1, [LX/0GT;

    iget-object v2, p0, LX/7Jx;->a:LX/7K1;

    iget-object v2, v2, LX/7K1;->o:LX/0GT;

    aput-object v2, v1, v3

    const/4 v2, 0x1

    iget-object v3, p0, LX/7Jx;->a:LX/7K1;

    iget-object v3, v3, LX/7K1;->p:LX/0GT;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/0Kx;->a([LX/0GT;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 1194302
    iget-object v0, p0, LX/7Jx;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    if-eqz v0, :cond_0

    .line 1194303
    iget-object v0, p0, LX/7Jx;->a:LX/7K1;

    iget-object v0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/Exception;)V

    .line 1194304
    :goto_0
    return-void

    .line 1194305
    :cond_0
    iget-object v0, p0, LX/7Jx;->a:LX/7K1;

    invoke-static {v0, p1}, LX/7K1;->a$redex0(LX/7K1;Ljava/lang/Exception;)V

    goto :goto_0
.end method
