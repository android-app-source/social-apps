.class public LX/8Oq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/8Oq;


# instance fields
.field private a:Landroid/os/PowerManager$WakeLock;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1341122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341123
    const v0, 0x1b7740

    iput v0, p0, LX/8Oq;->b:I

    .line 1341124
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 1341125
    const/4 v1, 0x1

    const-string v2, "MediaUploader"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, LX/8Oq;->a:Landroid/os/PowerManager$WakeLock;

    .line 1341126
    iget-object v0, p0, LX/8Oq;->a:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 1341127
    return-void
.end method

.method public static a(LX/0QB;)LX/8Oq;
    .locals 4

    .prologue
    .line 1341128
    sget-object v0, LX/8Oq;->c:LX/8Oq;

    if-nez v0, :cond_1

    .line 1341129
    const-class v1, LX/8Oq;

    monitor-enter v1

    .line 1341130
    :try_start_0
    sget-object v0, LX/8Oq;->c:LX/8Oq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1341131
    if-eqz v2, :cond_0

    .line 1341132
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1341133
    new-instance p0, LX/8Oq;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/8Oq;-><init>(Landroid/content/Context;)V

    .line 1341134
    move-object v0, p0

    .line 1341135
    sput-object v0, LX/8Oq;->c:LX/8Oq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1341136
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1341137
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1341138
    :cond_1
    sget-object v0, LX/8Oq;->c:LX/8Oq;

    return-object v0

    .line 1341139
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1341140
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1341141
    iget-object v0, p0, LX/8Oq;->a:Landroid/os/PowerManager$WakeLock;

    iget v1, p0, LX/8Oq;->b:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 1341142
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1341143
    iget-object v0, p0, LX/8Oq;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1341144
    return-void
.end method
