.class public final LX/7UW;
.super LX/7UO;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;)V
    .locals 0

    .prologue
    .line 1212998
    iput-object p1, p0, LX/7UW;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-direct {p0, p1}, LX/7UO;-><init>(LX/7UR;)V

    return-void
.end method


# virtual methods
.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 1212999
    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {v1, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1213000
    iget-object v0, p0, LX/7UW;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget-object v0, v0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7US;

    .line 1213001
    iget-object v3, p0, LX/7UW;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-static {v3, v1}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->a(Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    invoke-interface {v0}, LX/7US;->a()V

    goto :goto_0

    .line 1213002
    :cond_0
    invoke-super {p0, p1}, LX/7UO;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    .line 1213003
    iget-object v0, p0, LX/7UW;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget-object v0, v0, LX/7UR;->a:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1213004
    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {v1, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1213005
    iget-object v0, p0, LX/7UW;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget-object v0, v0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7US;

    .line 1213006
    iget-object v3, p0, LX/7UW;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-static {v3, v1}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->a(Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-interface {v0, v1, v3}, LX/7US;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    goto :goto_0

    .line 1213007
    :cond_0
    invoke-super {p0, p1}, LX/7UO;->onLongPress(Landroid/view/MotionEvent;)V

    .line 1213008
    return-void
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 1213009
    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {v1, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1213010
    iget-object v0, p0, LX/7UW;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget-object v0, v0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7US;

    .line 1213011
    iget-object v3, p0, LX/7UW;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-static {v3, v1}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->a(Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-interface {v0, v1, v3}, LX/7US;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    goto :goto_0

    .line 1213012
    :cond_0
    invoke-super {p0, p1}, LX/7UO;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1213013
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1213014
    iget-object v1, p0, LX/7UW;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget-object v1, v1, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 1213015
    iget-object v2, p0, LX/7UW;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-static {v2, v0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->a(Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    goto :goto_0

    .line 1213016
    :cond_0
    invoke-super {p0, p1}, LX/7UO;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
