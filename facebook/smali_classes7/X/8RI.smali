.class public final enum LX/8RI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8RI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8RI;

.field public static final enum ALBUM_CREATOR_FRAGMENT:LX/8RI;

.field public static final enum CHANNEL_CREATOR_ACTIVITY:LX/8RI;

.field public static final enum COMPOSER_AUDIENCE_FRAGMENT:LX/8RI;

.field public static final enum COMPOSER_FRAGMENT:LX/8RI;

.field public static final enum EDIT_STORY_PRIVACY_FRAGMENT:LX/8RI;

.field public static final enum FACECAST_ACTIVITY:LX/8RI;

.field public static final enum NOW_AUDIENCE_ACTIVITY:LX/8RI;

.field public static final enum PAYMENTS_CHECKOUT_ACTIVITY:LX/8RI;

.field public static final enum PRIVACY_CHECKUP_STEP_FRAGMENT:LX/8RI;

.field public static final enum PRIVACY_SAMPLE_APP:LX/8RI;

.field public static final enum SHARESHEET_FRAGMENT:LX/8RI;


# instance fields
.field private final caller:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1344568
    new-instance v0, LX/8RI;

    const-string v1, "ALBUM_CREATOR_FRAGMENT"

    const-string v2, "com.facebook.photos.albumcreator.AlbumCreatorFragment"

    invoke-direct {v0, v1, v4, v2}, LX/8RI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8RI;->ALBUM_CREATOR_FRAGMENT:LX/8RI;

    .line 1344569
    new-instance v0, LX/8RI;

    const-string v1, "CHANNEL_CREATOR_ACTIVITY"

    const-string v2, "com.facebook.channels.activity.ChannelCreatorActivity"

    invoke-direct {v0, v1, v5, v2}, LX/8RI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8RI;->CHANNEL_CREATOR_ACTIVITY:LX/8RI;

    .line 1344570
    new-instance v0, LX/8RI;

    const-string v1, "COMPOSER_AUDIENCE_FRAGMENT"

    const-string v2, "com.facebook.composer.privacy.common.ComposerAudienceFragment"

    invoke-direct {v0, v1, v6, v2}, LX/8RI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8RI;->COMPOSER_AUDIENCE_FRAGMENT:LX/8RI;

    .line 1344571
    new-instance v0, LX/8RI;

    const-string v1, "COMPOSER_FRAGMENT"

    const-string v2, "com.facebook.composer.activity.ComposerFragment"

    invoke-direct {v0, v1, v7, v2}, LX/8RI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8RI;->COMPOSER_FRAGMENT:LX/8RI;

    .line 1344572
    new-instance v0, LX/8RI;

    const-string v1, "EDIT_STORY_PRIVACY_FRAGMENT"

    const-string v2, "com.facebook.privacy.edit.EditStoryPrivacyFragment"

    invoke-direct {v0, v1, v8, v2}, LX/8RI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8RI;->EDIT_STORY_PRIVACY_FRAGMENT:LX/8RI;

    .line 1344573
    new-instance v0, LX/8RI;

    const-string v1, "FACECAST_ACTIVITY"

    const/4 v2, 0x5

    const-string v3, "com.facebook.facecast.FacecastActivity"

    invoke-direct {v0, v1, v2, v3}, LX/8RI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8RI;->FACECAST_ACTIVITY:LX/8RI;

    .line 1344574
    new-instance v0, LX/8RI;

    const-string v1, "PAYMENTS_CHECKOUT_ACTIVITY"

    const/4 v2, 0x6

    const-string v3, "com.facebook.payments.checkout.CheckoutActivity"

    invoke-direct {v0, v1, v2, v3}, LX/8RI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8RI;->PAYMENTS_CHECKOUT_ACTIVITY:LX/8RI;

    .line 1344575
    new-instance v0, LX/8RI;

    const-string v1, "PRIVACY_SAMPLE_APP"

    const/4 v2, 0x7

    const-string v3, "com.facebook.samples.privacy.MainFragment"

    invoke-direct {v0, v1, v2, v3}, LX/8RI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8RI;->PRIVACY_SAMPLE_APP:LX/8RI;

    .line 1344576
    new-instance v0, LX/8RI;

    const-string v1, "NOW_AUDIENCE_ACTIVITY"

    const/16 v2, 0x8

    const-string v3, "com.facebook.now.NowSettingsHostActivity"

    invoke-direct {v0, v1, v2, v3}, LX/8RI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8RI;->NOW_AUDIENCE_ACTIVITY:LX/8RI;

    .line 1344577
    new-instance v0, LX/8RI;

    const-string v1, "PRIVACY_CHECKUP_STEP_FRAGMENT"

    const/16 v2, 0x9

    const-string v3, "com.facebook.privacy.checkup.PrivacyCheckupStepFragment"

    invoke-direct {v0, v1, v2, v3}, LX/8RI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8RI;->PRIVACY_CHECKUP_STEP_FRAGMENT:LX/8RI;

    .line 1344578
    new-instance v0, LX/8RI;

    const-string v1, "SHARESHEET_FRAGMENT"

    const/16 v2, 0xa

    const-string v3, "com.facebook.snacks.sharesheet.app.SharesheetFragment"

    invoke-direct {v0, v1, v2, v3}, LX/8RI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8RI;->SHARESHEET_FRAGMENT:LX/8RI;

    .line 1344579
    const/16 v0, 0xb

    new-array v0, v0, [LX/8RI;

    sget-object v1, LX/8RI;->ALBUM_CREATOR_FRAGMENT:LX/8RI;

    aput-object v1, v0, v4

    sget-object v1, LX/8RI;->CHANNEL_CREATOR_ACTIVITY:LX/8RI;

    aput-object v1, v0, v5

    sget-object v1, LX/8RI;->COMPOSER_AUDIENCE_FRAGMENT:LX/8RI;

    aput-object v1, v0, v6

    sget-object v1, LX/8RI;->COMPOSER_FRAGMENT:LX/8RI;

    aput-object v1, v0, v7

    sget-object v1, LX/8RI;->EDIT_STORY_PRIVACY_FRAGMENT:LX/8RI;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/8RI;->FACECAST_ACTIVITY:LX/8RI;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8RI;->PAYMENTS_CHECKOUT_ACTIVITY:LX/8RI;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8RI;->PRIVACY_SAMPLE_APP:LX/8RI;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8RI;->NOW_AUDIENCE_ACTIVITY:LX/8RI;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8RI;->PRIVACY_CHECKUP_STEP_FRAGMENT:LX/8RI;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8RI;->SHARESHEET_FRAGMENT:LX/8RI;

    aput-object v2, v0, v1

    sput-object v0, LX/8RI;->$VALUES:[LX/8RI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1344580
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1344581
    iput-object p3, p0, LX/8RI;->caller:Ljava/lang/String;

    .line 1344582
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8RI;
    .locals 1

    .prologue
    .line 1344583
    const-class v0, LX/8RI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8RI;

    return-object v0
.end method

.method public static values()[LX/8RI;
    .locals 1

    .prologue
    .line 1344584
    sget-object v0, LX/8RI;->$VALUES:[LX/8RI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8RI;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1344585
    iget-object v0, p0, LX/8RI;->caller:Ljava/lang/String;

    return-object v0
.end method
