.class public final enum LX/7G7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7G7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7G7;

.field public static final enum ENSURE:LX/7G7;

.field public static final enum REFRESH_CONNECTION:LX/7G7;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1188777
    new-instance v0, LX/7G7;

    const-string v1, "ENSURE"

    invoke-direct {v0, v1, v2}, LX/7G7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7G7;->ENSURE:LX/7G7;

    .line 1188778
    new-instance v0, LX/7G7;

    const-string v1, "REFRESH_CONNECTION"

    invoke-direct {v0, v1, v3}, LX/7G7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7G7;->REFRESH_CONNECTION:LX/7G7;

    .line 1188779
    const/4 v0, 0x2

    new-array v0, v0, [LX/7G7;

    sget-object v1, LX/7G7;->ENSURE:LX/7G7;

    aput-object v1, v0, v2

    sget-object v1, LX/7G7;->REFRESH_CONNECTION:LX/7G7;

    aput-object v1, v0, v3

    sput-object v0, LX/7G7;->$VALUES:[LX/7G7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1188776
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7G7;
    .locals 1

    .prologue
    .line 1188775
    const-class v0, LX/7G7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7G7;

    return-object v0
.end method

.method public static values()[LX/7G7;
    .locals 1

    .prologue
    .line 1188774
    sget-object v0, LX/7G7;->$VALUES:[LX/7G7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7G7;

    return-object v0
.end method
