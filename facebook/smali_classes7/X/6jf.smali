.class public LX/6jf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final messageMetadata:LX/6kn;

.field public final removedAdminFbIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1131846
    new-instance v0, LX/1sv;

    const-string v1, "DeltaAdminRemovedFromGroupThread"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jf;->b:LX/1sv;

    .line 1131847
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jf;->c:LX/1sw;

    .line 1131848
    new-instance v0, LX/1sw;

    const-string v1, "removedAdminFbIds"

    const/16 v2, 0xf

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jf;->d:LX/1sw;

    .line 1131849
    sput-boolean v4, LX/6jf;->a:Z

    return-void
.end method

.method private constructor <init>(LX/6kn;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kn;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1131850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1131851
    iput-object p1, p0, LX/6jf;->messageMetadata:LX/6kn;

    .line 1131852
    iput-object p2, p0, LX/6jf;->removedAdminFbIds:Ljava/util/List;

    .line 1131853
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1131780
    iget-object v0, p0, LX/6jf;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1131781
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jf;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1131782
    :cond_0
    iget-object v0, p0, LX/6jf;->removedAdminFbIds:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1131783
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'removedAdminFbIds\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jf;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1131784
    :cond_1
    return-void
.end method

.method public static b(LX/1su;)LX/6jf;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1131824
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v3, v0

    .line 1131825
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 1131826
    iget-byte v4, v2, LX/1sw;->b:B

    if-eqz v4, :cond_5

    .line 1131827
    iget-short v4, v2, LX/1sw;->c:S

    packed-switch v4, :pswitch_data_0

    .line 1131828
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1131829
    :pswitch_0
    iget-byte v4, v2, LX/1sw;->b:B

    const/16 v5, 0xc

    if-ne v4, v5, :cond_0

    .line 1131830
    invoke-static {p0}, LX/6kn;->b(LX/1su;)LX/6kn;

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    .line 1131831
    :cond_0
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1131832
    :pswitch_1
    iget-byte v4, v2, LX/1sw;->b:B

    const/16 v5, 0xf

    if-ne v4, v5, :cond_4

    .line 1131833
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v4

    .line 1131834
    new-instance v2, Ljava/util/ArrayList;

    iget v0, v4, LX/1u3;->b:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 1131835
    :goto_1
    iget v5, v4, LX/1u3;->b:I

    if-gez v5, :cond_2

    invoke-static {}, LX/1su;->t()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1131836
    :cond_1
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 1131837
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1131838
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1131839
    :cond_2
    iget v5, v4, LX/1u3;->b:I

    if-lt v0, v5, :cond_1

    :cond_3
    move-object v0, v2

    .line 1131840
    goto :goto_0

    .line 1131841
    :cond_4
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1131842
    :cond_5
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1131843
    new-instance v1, LX/6jf;

    invoke-direct {v1, v3, v0}, LX/6jf;-><init>(LX/6kn;Ljava/util/List;)V

    .line 1131844
    invoke-direct {v1}, LX/6jf;->a()V

    .line 1131845
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1131854
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1131855
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1131856
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1131857
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaAdminRemovedFromGroupThread"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1131858
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131859
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131860
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131861
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131862
    const-string v4, "messageMetadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131863
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131864
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131865
    iget-object v4, p0, LX/6jf;->messageMetadata:LX/6kn;

    if-nez v4, :cond_3

    .line 1131866
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131867
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131868
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131869
    const-string v4, "removedAdminFbIds"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131870
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131871
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131872
    iget-object v0, p0, LX/6jf;->removedAdminFbIds:Ljava/util/List;

    if-nez v0, :cond_4

    .line 1131873
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131874
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131875
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131876
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1131877
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1131878
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1131879
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1131880
    :cond_3
    iget-object v4, p0, LX/6jf;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1131881
    :cond_4
    iget-object v0, p0, LX/6jf;->removedAdminFbIds:Ljava/util/List;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 4

    .prologue
    .line 1131811
    invoke-direct {p0}, LX/6jf;->a()V

    .line 1131812
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1131813
    iget-object v0, p0, LX/6jf;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1131814
    sget-object v0, LX/6jf;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131815
    iget-object v0, p0, LX/6jf;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1131816
    :cond_0
    iget-object v0, p0, LX/6jf;->removedAdminFbIds:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1131817
    sget-object v0, LX/6jf;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131818
    new-instance v0, LX/1u3;

    const/16 v1, 0xa

    iget-object v2, p0, LX/6jf;->removedAdminFbIds:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1131819
    iget-object v0, p0, LX/6jf;->removedAdminFbIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1131820
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, LX/1su;->a(J)V

    goto :goto_0

    .line 1131821
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1131822
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1131823
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1131789
    if-nez p1, :cond_1

    .line 1131790
    :cond_0
    :goto_0
    return v0

    .line 1131791
    :cond_1
    instance-of v1, p1, LX/6jf;

    if-eqz v1, :cond_0

    .line 1131792
    check-cast p1, LX/6jf;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1131793
    if-nez p1, :cond_3

    .line 1131794
    :cond_2
    :goto_1
    move v0, v2

    .line 1131795
    goto :goto_0

    .line 1131796
    :cond_3
    iget-object v0, p0, LX/6jf;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1131797
    :goto_2
    iget-object v3, p1, LX/6jf;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1131798
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1131799
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1131800
    iget-object v0, p0, LX/6jf;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6jf;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1131801
    :cond_5
    iget-object v0, p0, LX/6jf;->removedAdminFbIds:Ljava/util/List;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1131802
    :goto_4
    iget-object v3, p1, LX/6jf;->removedAdminFbIds:Ljava/util/List;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1131803
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1131804
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1131805
    iget-object v0, p0, LX/6jf;->removedAdminFbIds:Ljava/util/List;

    iget-object v3, p1, LX/6jf;->removedAdminFbIds:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1131806
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1131807
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1131808
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1131809
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1131810
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1131788
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1131785
    sget-boolean v0, LX/6jf;->a:Z

    .line 1131786
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jf;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1131787
    return-object v0
.end method
