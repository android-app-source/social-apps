.class public final LX/7oL;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1240919
    const-class v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    const v0, 0x63b4a2c4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchEventPermalinkQuery"

    const-string v6, "2da8d0bfef180d8a5e978f056aadc758"

    const-string v7, "event"

    const-string v8, "10155207561636729"

    const-string v9, "10155259087416729"

    .line 1240920
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1240921
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1240922
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1240923
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1240924
    sparse-switch v0, :sswitch_data_0

    .line 1240925
    :goto_0
    return-object p1

    .line 1240926
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1240927
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1240928
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1240929
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1240930
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1240931
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1240932
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1240933
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1240934
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1240935
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1240936
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1240937
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1240938
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1240939
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7d1a28b1 -> :sswitch_9
        -0x51484e72 -> :sswitch_3
        -0x41a91745 -> :sswitch_8
        -0x336f0c67 -> :sswitch_d
        0x180aba4 -> :sswitch_b
        0x1093c0e0 -> :sswitch_4
        0x291d8de0 -> :sswitch_c
        0x3052e0ff -> :sswitch_0
        0x4b46b5f1 -> :sswitch_1
        0x4c6d50cb -> :sswitch_5
        0x5f424068 -> :sswitch_a
        0x61bc9553 -> :sswitch_6
        0x6d2645b9 -> :sswitch_2
        0x73a026b5 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1240940
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1240941
    :goto_1
    return v0

    .line 1240942
    :sswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "9"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 1240943
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1240944
    :pswitch_1
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x39 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
