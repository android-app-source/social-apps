.class public LX/8Fe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/0Sh;

.field private final b:LX/0SI;

.field private final c:LX/3iH;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/03V;

.field public f:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(LX/0Sh;LX/0SI;LX/3iH;Ljava/util/concurrent/ExecutorService;LX/03V;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1318078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318079
    iput-object p1, p0, LX/8Fe;->a:LX/0Sh;

    .line 1318080
    iput-object p2, p0, LX/8Fe;->b:LX/0SI;

    .line 1318081
    iput-object p3, p0, LX/8Fe;->c:LX/3iH;

    .line 1318082
    iput-object p4, p0, LX/8Fe;->d:Ljava/util/concurrent/ExecutorService;

    .line 1318083
    iput-object p5, p0, LX/8Fe;->e:LX/03V;

    .line 1318084
    return-void
.end method

.method public static a(LX/0QB;)LX/8Fe;
    .locals 9

    .prologue
    .line 1318123
    const-class v1, LX/8Fe;

    monitor-enter v1

    .line 1318124
    :try_start_0
    sget-object v0, LX/8Fe;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1318125
    sput-object v2, LX/8Fe;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1318126
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1318127
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1318128
    new-instance v3, LX/8Fe;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v5

    check-cast v5, LX/0SI;

    invoke-static {v0}, LX/3iH;->b(LX/0QB;)LX/3iH;

    move-result-object v6

    check-cast v6, LX/3iH;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v3 .. v8}, LX/8Fe;-><init>(LX/0Sh;LX/0SI;LX/3iH;Ljava/util/concurrent/ExecutorService;LX/03V;)V

    .line 1318129
    move-object v0, v3

    .line 1318130
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1318131
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8Fe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1318132
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1318133
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/8Fe;Lcom/facebook/auth/viewercontext/ViewerContext;LX/8Fd;)V
    .locals 3
    .param p0    # LX/8Fe;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1318105
    iget-object v0, p0, LX/8Fe;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1318106
    iget-boolean v0, p0, LX/8Fe;->h:Z

    if-nez v0, :cond_1

    .line 1318107
    :cond_0
    :goto_0
    return-void

    .line 1318108
    :cond_1
    if-eqz p1, :cond_2

    .line 1318109
    iget-boolean v0, p1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v0

    .line 1318110
    if-nez v0, :cond_2

    .line 1318111
    iget-object v0, p0, LX/8Fe;->e:LX/03V;

    const-class v1, LX/8Fe;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Non-page ViewerContext fetched"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1318112
    if-eqz p2, :cond_0

    .line 1318113
    invoke-interface {p2}, LX/8Fd;->a()V

    goto :goto_0

    .line 1318114
    :cond_2
    if-eqz p1, :cond_3

    .line 1318115
    iget-object v0, p1, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1318116
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1318117
    const/4 p1, 0x0

    .line 1318118
    :cond_3
    iput-object p1, p0, LX/8Fe;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1318119
    iget-boolean v0, p0, LX/8Fe;->g:Z

    if-eqz v0, :cond_4

    .line 1318120
    iget-object v0, p0, LX/8Fe;->b:LX/0SI;

    invoke-interface {v0, p1}, LX/0SI;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 1318121
    :cond_4
    if-eqz p2, :cond_0

    .line 1318122
    invoke-interface {p2, p1}, LX/8Fd;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1318103
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/8Fe;->a(Z)V

    .line 1318104
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1318101
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/8Fe;->a(Ljava/lang/String;LX/8Fd;)V

    .line 1318102
    return-void
.end method

.method public final a(Ljava/lang/String;LX/8Fd;)V
    .locals 3
    .param p2    # LX/8Fd;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1318095
    iget-object v0, p0, LX/8Fe;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1318096
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1318097
    iget-boolean v0, p0, LX/8Fe;->h:Z

    if-nez v0, :cond_1

    .line 1318098
    :goto_1
    return-void

    .line 1318099
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1318100
    :cond_1
    iget-object v0, p0, LX/8Fe;->c:LX/3iH;

    new-instance v1, LX/8Fc;

    invoke-direct {v1, p0, p2}, LX/8Fc;-><init>(LX/8Fe;LX/8Fd;)V

    iget-object v2, p0, LX/8Fe;->d:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v0, p1, v1, v2}, LX/3iH;->a(Ljava/lang/String;LX/8E8;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1318091
    iget-object v0, p0, LX/8Fe;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1318092
    iput-boolean p1, p0, LX/8Fe;->g:Z

    .line 1318093
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8Fe;->h:Z

    .line 1318094
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1318085
    iget-object v0, p0, LX/8Fe;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1318086
    iput-boolean v1, p0, LX/8Fe;->g:Z

    .line 1318087
    iput-boolean v1, p0, LX/8Fe;->h:Z

    .line 1318088
    iput-object v2, p0, LX/8Fe;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1318089
    iget-object v0, p0, LX/8Fe;->b:LX/0SI;

    invoke-interface {v0, v2}, LX/0SI;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 1318090
    return-void
.end method
