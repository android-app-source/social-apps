.class public LX/6sr;
.super LX/6E8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E8",
        "<",
        "Lcom/facebook/payments/ui/ImageDetailView;",
        "LX/6sq;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/ui/ImageDetailView;)V
    .locals 0

    .prologue
    .line 1153818
    invoke-direct {p0, p1}, LX/6E8;-><init>(LX/6E6;)V

    .line 1153819
    return-void
.end method


# virtual methods
.method public final a(LX/6E2;)V
    .locals 2

    .prologue
    .line 1153820
    check-cast p1, LX/6sq;

    .line 1153821
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/payments/ui/ImageDetailView;

    .line 1153822
    iget-object v1, p1, LX/6sq;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/ImageDetailView;->setImageUrl(Landroid/net/Uri;)V

    .line 1153823
    iget-object v1, p1, LX/6sq;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/ImageDetailView;->setTitle(Ljava/lang/CharSequence;)V

    .line 1153824
    iget-object v1, p1, LX/6sq;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/ImageDetailView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 1153825
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1153826
    return-void
.end method
