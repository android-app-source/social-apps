.class public LX/6lT;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1142742
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 1142743
    return-void
.end method

.method public static a(LX/01T;LX/0Or;LX/0Or;)Ljava/lang/Boolean;
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/video/config/SendVideoV7Gk;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/video/config/SendVideoV5Gk;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/IsVideoSendingEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/01T;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    .line 1142728
    sget-object v0, LX/01T;->MESSENGER:LX/01T;

    if-eq p0, v0, :cond_0

    .line 1142729
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1142730
    :goto_0
    return-object v0

    .line 1142731
    :cond_0
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 1142732
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1142733
    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 1142734
    :cond_1
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public static b(LX/01T;LX/0Or;LX/0Or;)Ljava/lang/Boolean;
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/video/config/TranscodeVideoV7Gk;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/video/config/SendVideoV5Gk;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/media/upload/config/IsVideoTranscodingEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/01T;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    .line 1142735
    sget-object v0, LX/01T;->MESSENGER:LX/01T;

    if-eq p0, v0, :cond_0

    .line 1142736
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1142737
    :goto_0
    return-object v0

    .line 1142738
    :cond_0
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 1142739
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1142740
    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 1142741
    :cond_1
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 1142727
    return-void
.end method
