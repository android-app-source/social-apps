.class public final enum LX/89b;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/89b;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/89b;

.field public static final enum IMAGE:LX/89b;

.field public static final enum VIDEO:LX/89b;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1305088
    new-instance v0, LX/89b;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v2}, LX/89b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89b;->IMAGE:LX/89b;

    new-instance v0, LX/89b;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/89b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89b;->VIDEO:LX/89b;

    .line 1305089
    const/4 v0, 0x2

    new-array v0, v0, [LX/89b;

    sget-object v1, LX/89b;->IMAGE:LX/89b;

    aput-object v1, v0, v2

    sget-object v1, LX/89b;->VIDEO:LX/89b;

    aput-object v1, v0, v3

    sput-object v0, LX/89b;->$VALUES:[LX/89b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1305091
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/89b;
    .locals 1

    .prologue
    .line 1305092
    const-class v0, LX/89b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/89b;

    return-object v0
.end method

.method public static values()[LX/89b;
    .locals 1

    .prologue
    .line 1305090
    sget-object v0, LX/89b;->$VALUES:[LX/89b;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/89b;

    return-object v0
.end method
