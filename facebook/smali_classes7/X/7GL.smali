.class public LX/7GL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/7GL;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1189066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189067
    return-void
.end method

.method public static a(Ljava/util/List;JLX/7GK;Lcom/facebook/fbtrace/FbTraceNode;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<DW:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TDW;>;J",
            "LX/7GK",
            "<TDW;>;",
            "Lcom/facebook/fbtrace/FbTraceNode;",
            ")",
            "LX/0Px",
            "<",
            "LX/7GJ",
            "<TDW;>;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1189068
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    move v1, v0

    .line 1189069
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 1189070
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 1189071
    int-to-long v4, v0

    add-long/2addr v4, p1

    int-to-long v6, v1

    add-long/2addr v4, v6

    .line 1189072
    invoke-interface {p3, v3}, LX/7GK;->a(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {p3, v3}, LX/7GK;->b(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {p3, v3}, LX/7GK;->b(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v7, 0x2

    if-lt v6, v7, :cond_0

    .line 1189073
    invoke-interface {p3, v3}, LX/7GK;->b(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    add-int/2addr v1, v6

    .line 1189074
    :cond_0
    new-instance v6, LX/7GJ;

    invoke-direct {v6, v3, v4, v5, p4}, LX/7GJ;-><init>(Ljava/lang/Object;JLcom/facebook/fbtrace/FbTraceNode;)V

    invoke-virtual {v2, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1189075
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1189076
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/7GL;
    .locals 3

    .prologue
    .line 1189077
    sget-object v0, LX/7GL;->a:LX/7GL;

    if-nez v0, :cond_1

    .line 1189078
    const-class v1, LX/7GL;

    monitor-enter v1

    .line 1189079
    :try_start_0
    sget-object v0, LX/7GL;->a:LX/7GL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1189080
    if-eqz v2, :cond_0

    .line 1189081
    :try_start_1
    new-instance v0, LX/7GL;

    invoke-direct {v0}, LX/7GL;-><init>()V

    .line 1189082
    move-object v0, v0

    .line 1189083
    sput-object v0, LX/7GL;->a:LX/7GL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1189084
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1189085
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1189086
    :cond_1
    sget-object v0, LX/7GL;->a:LX/7GL;

    return-object v0

    .line 1189087
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1189088
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
