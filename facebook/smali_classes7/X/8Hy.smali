.class public final LX/8Hy;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "Ljava/util/List",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

.field private final b:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:D

.field private final d:I

.field private final e:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:D

.field private final g:I


# direct methods
.method public constructor <init>(Lcom/facebook/photos/imageprocessing/ImageDupeDetector;Landroid/net/Uri;DILandroid/net/Uri;DI)V
    .locals 1
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1321861
    iput-object p1, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    invoke-direct {p0}, LX/1ci;-><init>()V

    .line 1321862
    iput-object p2, p0, LX/8Hy;->b:Landroid/net/Uri;

    .line 1321863
    iput-wide p3, p0, LX/8Hy;->c:D

    .line 1321864
    iput p5, p0, LX/8Hy;->d:I

    .line 1321865
    iput-object p6, p0, LX/8Hy;->e:Landroid/net/Uri;

    .line 1321866
    iput-wide p7, p0, LX/8Hy;->f:D

    .line 1321867
    iput p9, p0, LX/8Hy;->g:I

    .line 1321868
    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "Ljava/util/List",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    .line 1321869
    const/4 v1, 0x0

    .line 1321870
    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, LX/1ca;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1321871
    :cond_0
    iget-object v0, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    invoke-static {v0}, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->b(Lcom/facebook/photos/imageprocessing/ImageDupeDetector;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1321872
    iget-object v0, p0, LX/8Hy;->b:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 1321873
    iget-object v0, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    iget-object v0, v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->d:LX/1HI;

    iget-object v1, p0, LX/8Hy;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1HI;->a(Landroid/net/Uri;)V

    .line 1321874
    :cond_1
    iget-object v0, p0, LX/8Hy;->e:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 1321875
    iget-object v0, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    iget-object v0, v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->d:LX/1HI;

    iget-object v1, p0, LX/8Hy;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1HI;->a(Landroid/net/Uri;)V

    .line 1321876
    :cond_2
    :goto_0
    return-void

    .line 1321877
    :cond_3
    :try_start_1
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1321878
    if-eqz v0, :cond_4

    :try_start_2
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1321879
    :cond_4
    iget-object v1, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    invoke-static {v1}, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->b(Lcom/facebook/photos/imageprocessing/ImageDupeDetector;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1321880
    if-eqz v0, :cond_5

    .line 1321881
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1321882
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_1

    .line 1321883
    :cond_5
    iget-object v0, p0, LX/8Hy;->b:Landroid/net/Uri;

    if-eqz v0, :cond_6

    .line 1321884
    iget-object v0, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    iget-object v0, v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->d:LX/1HI;

    iget-object v1, p0, LX/8Hy;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1HI;->a(Landroid/net/Uri;)V

    .line 1321885
    :cond_6
    iget-object v0, p0, LX/8Hy;->e:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 1321886
    iget-object v0, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    iget-object v0, v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->d:LX/1HI;

    iget-object v1, p0, LX/8Hy;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1HI;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 1321887
    :cond_7
    :try_start_3
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1321888
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1FJ;

    .line 1321889
    if-eqz v1, :cond_8

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, LX/1lm;

    if-eqz v3, :cond_8

    .line 1321890
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1lm;

    invoke-virtual {v1}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 1321891
    :catchall_0
    move-exception v1

    :goto_3
    if-eqz v0, :cond_f

    .line 1321892
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1321893
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_4

    .line 1321894
    :cond_9
    :try_start_4
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    if-eq v1, v4, :cond_c

    .line 1321895
    iget-object v1, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    invoke-static {v1}, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->b(Lcom/facebook/photos/imageprocessing/ImageDupeDetector;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1321896
    if-eqz v0, :cond_a

    .line 1321897
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1321898
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_5

    .line 1321899
    :cond_a
    iget-object v0, p0, LX/8Hy;->b:Landroid/net/Uri;

    if-eqz v0, :cond_b

    .line 1321900
    iget-object v0, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    iget-object v0, v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->d:LX/1HI;

    iget-object v1, p0, LX/8Hy;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1HI;->a(Landroid/net/Uri;)V

    .line 1321901
    :cond_b
    iget-object v0, p0, LX/8Hy;->e:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 1321902
    iget-object v0, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    iget-object v0, v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->d:LX/1HI;

    iget-object v1, p0, LX/8Hy;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1HI;->a(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 1321903
    :cond_c
    :try_start_5
    iget-object v10, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    iget-wide v2, p0, LX/8Hy;->c:D

    iget v4, p0, LX/8Hy;->d:I

    const/4 v5, 0x1

    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    iget-wide v6, p0, LX/8Hy;->f:D

    iget v8, p0, LX/8Hy;->g:I

    invoke-static/range {v1 .. v8}, Lcom/facebook/photos/imageprocessing/ImageDupeDetector$SimilarityScore;->a(Landroid/graphics/Bitmap;DILandroid/graphics/Bitmap;DI)F

    move-result v1

    .line 1321904
    iput v1, v10, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->h:F

    .line 1321905
    const/4 v1, 0x5

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v3, 0x2

    const/4 v1, 0x1

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v3, 0x3

    const/4 v1, 0x1

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v1, 0x4

    iget-object v3, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    iget v3, v3, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->h:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v1

    .line 1321906
    iget-object v1, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    iget-object v2, v1, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->g:Ljava/lang/Object;

    monitor-enter v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1321907
    :try_start_6
    iget-object v1, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    iget-object v1, v1, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->g:Ljava/lang/Object;

    const v3, 0x2feb437f

    invoke-static {v1, v3}, LX/02L;->b(Ljava/lang/Object;I)V

    .line 1321908
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1321909
    if-eqz v0, :cond_d

    .line 1321910
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1321911
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_6

    .line 1321912
    :catchall_1
    move-exception v1

    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1321913
    :cond_d
    iget-object v0, p0, LX/8Hy;->b:Landroid/net/Uri;

    if-eqz v0, :cond_e

    .line 1321914
    iget-object v0, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    iget-object v0, v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->d:LX/1HI;

    iget-object v1, p0, LX/8Hy;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1HI;->a(Landroid/net/Uri;)V

    .line 1321915
    :cond_e
    iget-object v0, p0, LX/8Hy;->e:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 1321916
    iget-object v0, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    iget-object v0, v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->d:LX/1HI;

    iget-object v1, p0, LX/8Hy;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1HI;->a(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 1321917
    :cond_f
    iget-object v0, p0, LX/8Hy;->b:Landroid/net/Uri;

    if-eqz v0, :cond_10

    .line 1321918
    iget-object v0, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    iget-object v0, v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->d:LX/1HI;

    iget-object v2, p0, LX/8Hy;->b:Landroid/net/Uri;

    invoke-virtual {v0, v2}, LX/1HI;->a(Landroid/net/Uri;)V

    .line 1321919
    :cond_10
    iget-object v0, p0, LX/8Hy;->e:Landroid/net/Uri;

    if-eqz v0, :cond_11

    .line 1321920
    iget-object v0, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    iget-object v0, v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->d:LX/1HI;

    iget-object v2, p0, LX/8Hy;->e:Landroid/net/Uri;

    invoke-virtual {v0, v2}, LX/1HI;->a(Landroid/net/Uri;)V

    :cond_11
    throw v1

    .line 1321921
    :catchall_2
    move-exception v0

    move-object v11, v0

    move-object v0, v1

    move-object v1, v11

    goto/16 :goto_3
.end method

.method public final f(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "Ljava/util/List",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1321922
    :try_start_0
    iget-object v0, p0, LX/8Hy;->a:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    invoke-static {v0}, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->b(Lcom/facebook/photos/imageprocessing/ImageDupeDetector;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1321923
    invoke-interface {p1}, LX/1ca;->g()Z

    .line 1321924
    return-void

    .line 1321925
    :catchall_0
    move-exception v0

    invoke-interface {p1}, LX/1ca;->g()Z

    throw v0
.end method
