.class public final enum LX/6iH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6iH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6iH;

.field public static final enum NONE:LX/6iH;

.field public static final enum ROOM:LX/6iH;

.field public static final enum STANDARD_GROUP:LX/6iH;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1129117
    new-instance v0, LX/6iH;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/6iH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6iH;->NONE:LX/6iH;

    .line 1129118
    new-instance v0, LX/6iH;

    const-string v1, "STANDARD_GROUP"

    invoke-direct {v0, v1, v3}, LX/6iH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6iH;->STANDARD_GROUP:LX/6iH;

    .line 1129119
    new-instance v0, LX/6iH;

    const-string v1, "ROOM"

    invoke-direct {v0, v1, v4}, LX/6iH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6iH;->ROOM:LX/6iH;

    .line 1129120
    const/4 v0, 0x3

    new-array v0, v0, [LX/6iH;

    sget-object v1, LX/6iH;->NONE:LX/6iH;

    aput-object v1, v0, v2

    sget-object v1, LX/6iH;->STANDARD_GROUP:LX/6iH;

    aput-object v1, v0, v3

    sget-object v1, LX/6iH;->ROOM:LX/6iH;

    aput-object v1, v0, v4

    sput-object v0, LX/6iH;->$VALUES:[LX/6iH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1129123
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6iH;
    .locals 1

    .prologue
    .line 1129122
    const-class v0, LX/6iH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6iH;

    return-object v0
.end method

.method public static values()[LX/6iH;
    .locals 1

    .prologue
    .line 1129121
    sget-object v0, LX/6iH;->$VALUES:[LX/6iH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6iH;

    return-object v0
.end method
