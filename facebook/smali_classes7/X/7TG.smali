.class public LX/7TG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/7TG;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7TC;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Sj;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Sj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/7TC;",
            ">;",
            "LX/0Sj;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1210269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1210270
    iput-object p1, p0, LX/7TG;->a:LX/0Or;

    .line 1210271
    iput-object p2, p0, LX/7TG;->b:LX/0Sj;

    .line 1210272
    return-void
.end method

.method public static a(LX/0QB;)LX/7TG;
    .locals 5

    .prologue
    .line 1210251
    sget-object v0, LX/7TG;->c:LX/7TG;

    if-nez v0, :cond_1

    .line 1210252
    const-class v1, LX/7TG;

    monitor-enter v1

    .line 1210253
    :try_start_0
    sget-object v0, LX/7TG;->c:LX/7TG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1210254
    if-eqz v2, :cond_0

    .line 1210255
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1210256
    new-instance v4, LX/7TG;

    const/16 v3, 0x3879

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v3

    check-cast v3, LX/0Sj;

    invoke-direct {v4, p0, v3}, LX/7TG;-><init>(LX/0Or;LX/0Sj;)V

    .line 1210257
    move-object v0, v4

    .line 1210258
    sput-object v0, LX/7TG;->c:LX/7TG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1210259
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1210260
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1210261
    :cond_1
    sget-object v0, LX/7TG;->c:LX/7TG;

    return-object v0

    .line 1210262
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1210263
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/7TH;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7TH;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7TD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1210264
    new-instance v0, LX/7TF;

    invoke-direct {v0, p0}, LX/7TF;-><init>(LX/7TG;)V

    .line 1210265
    new-instance v1, Lcom/facebook/videocodec/resizer/VideoResizer$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/facebook/videocodec/resizer/VideoResizer$1;-><init>(LX/7TG;LX/7TH;LX/7TF;)V

    const-string v2, "Video Resizer"

    const v3, 0x441bcc1

    invoke-static {v1, v2, v3}, LX/00l;->a(Ljava/lang/Runnable;Ljava/lang/String;I)Ljava/lang/Thread;

    move-result-object v1

    .line 1210266
    iput-object v1, v0, LX/7TF;->b:Ljava/lang/Thread;

    .line 1210267
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1210268
    return-object v0
.end method
