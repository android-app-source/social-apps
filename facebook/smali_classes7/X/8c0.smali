.class public final LX/8c0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1373543
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 1373544
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1373545
    :goto_0
    return v1

    .line 1373546
    :cond_0
    const-string v8, "is_delta_update"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1373547
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v5, v3

    move v3, v2

    .line 1373548
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 1373549
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1373550
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1373551
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1373552
    const-string v8, "edges"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1373553
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1373554
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_2

    .line 1373555
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_2

    .line 1373556
    invoke-static {p0, p1}, LX/8bz;->b(LX/15w;LX/186;)I

    move-result v7

    .line 1373557
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1373558
    :cond_2
    invoke-static {v6, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 1373559
    goto :goto_1

    .line 1373560
    :cond_3
    const-string v8, "timestamp"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1373561
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 1373562
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1373563
    :cond_5
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1373564
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1373565
    if-eqz v3, :cond_6

    .line 1373566
    invoke-virtual {p1, v2, v5}, LX/186;->a(IZ)V

    .line 1373567
    :cond_6
    if-eqz v0, :cond_7

    .line 1373568
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 1373569
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1373570
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1373571
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1373572
    if-eqz v0, :cond_1

    .line 1373573
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373574
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1373575
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1373576
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/8bz;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1373577
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1373578
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1373579
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1373580
    if-eqz v0, :cond_2

    .line 1373581
    const-string v1, "is_delta_update"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373582
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1373583
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1373584
    if-eqz v0, :cond_3

    .line 1373585
    const-string v1, "timestamp"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373586
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1373587
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1373588
    return-void
.end method
