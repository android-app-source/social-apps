.class public LX/8K3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0kb;

.field private b:Ljava/util/Timer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/8K1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/8K2;


# direct methods
.method public constructor <init>(LX/0kb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1329791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1329792
    iput-object p1, p0, LX/8K3;->a:LX/0kb;

    .line 1329793
    const/4 v0, 0x0

    iput-object v0, p0, LX/8K3;->c:LX/8K1;

    .line 1329794
    invoke-virtual {p0}, LX/8K3;->a()V

    .line 1329795
    return-void
.end method

.method public static b(LX/0QB;)LX/8K3;
    .locals 2

    .prologue
    .line 1329806
    new-instance v1, LX/8K3;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-direct {v1, v0}, LX/8K3;-><init>(LX/0kb;)V

    .line 1329807
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1329808
    iget-object v0, p0, LX/8K3;->a:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1329809
    sget-object v0, LX/8K2;->CONNECTED:LX/8K2;

    iput-object v0, p0, LX/8K3;->d:LX/8K2;

    .line 1329810
    :goto_0
    return-void

    .line 1329811
    :cond_0
    sget-object v0, LX/8K2;->NO_INTERNET:LX/8K2;

    iput-object v0, p0, LX/8K3;->d:LX/8K2;

    goto :goto_0
.end method

.method public final a(LX/8K1;)V
    .locals 6

    .prologue
    const-wide/16 v2, 0x1388

    .line 1329801
    iput-object p1, p0, LX/8K3;->c:LX/8K1;

    .line 1329802
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    .line 1329803
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, LX/8K3;->b:Ljava/util/Timer;

    .line 1329804
    iget-object v0, p0, LX/8K3;->b:Ljava/util/Timer;

    new-instance v1, Lcom/facebook/photos/upload/compost/CompostNetworkMonitor$1;

    invoke-direct {v1, p0, v4}, Lcom/facebook/photos/upload/compost/CompostNetworkMonitor$1;-><init>(LX/8K3;Landroid/os/Handler;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 1329805
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1329796
    iget-object v0, p0, LX/8K3;->b:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1329797
    iget-object v0, p0, LX/8K3;->b:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1329798
    :cond_0
    iput-object v1, p0, LX/8K3;->b:Ljava/util/Timer;

    .line 1329799
    iput-object v1, p0, LX/8K3;->c:LX/8K1;

    .line 1329800
    return-void
.end method
