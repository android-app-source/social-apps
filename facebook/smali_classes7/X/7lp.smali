.class public final LX/7lp;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/publish/common/PostParamsWrapper;

.field public final synthetic b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Lcom/facebook/composer/publish/common/PostParamsWrapper;)V
    .locals 0

    .prologue
    .line 1235724
    iput-object p1, p0, LX/7lp;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iput-object p2, p0, LX/7lp;->a:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 9

    .prologue
    .line 1235714
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1235715
    iget-object v0, p0, LX/7lp;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7md;

    iget-object v1, p0, LX/7lp;->a:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v1

    .line 1235716
    iget-object v2, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v2, v2

    .line 1235717
    invoke-virtual {v0, v1, v2, v4}, LX/7md;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1235718
    iget-object v0, p0, LX/7lp;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    iget-object v1, p0, LX/7lp;->a:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->b(Ljava/lang/String;)V

    .line 1235719
    iget-object v0, p0, LX/7lp;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v1, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c:LX/7m8;

    sget-object v2, LX/7m7;->SUCCESS:LX/7m7;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/7lp;->a:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, LX/7lp;->a:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->c()J

    move-result-wide v6

    new-instance v0, LX/2rc;

    invoke-direct {v0}, LX/2rc;-><init>()V

    const/4 v8, 0x0

    .line 1235720
    iput-boolean v8, v0, LX/2rc;->a:Z

    .line 1235721
    move-object v0, v0

    .line 1235722
    invoke-virtual {v0}, LX/2rc;->a()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v8

    invoke-virtual/range {v1 .. v8}, LX/7m8;->a(LX/7m7;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;JLcom/facebook/composer/publish/common/ErrorDetails;)V

    .line 1235723
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 1235710
    iget-object v0, p0, LX/7lp;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7md;

    iget-object v1, p0, LX/7lp;->a:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7md;->b(Ljava/lang/String;)V

    .line 1235711
    iget-object v0, p0, LX/7lp;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v1, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c:LX/7m8;

    sget-object v2, LX/7m7;->EXCEPTION:LX/7m7;

    iget-object v0, p0, LX/7lp;->a:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, LX/7lp;->a:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->c()J

    move-result-wide v6

    iget-object v0, p0, LX/7lp;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v4, p0, LX/7lp;->a:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, p1, v4}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Lcom/facebook/fbservice/service/ServiceException;Ljava/lang/String;)Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v8

    move-object v4, v3

    invoke-virtual/range {v1 .. v8}, LX/7m8;->a(LX/7m7;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;JLcom/facebook/composer/publish/common/ErrorDetails;)V

    .line 1235712
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1235713
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/7lp;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
