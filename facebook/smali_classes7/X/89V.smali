.class public final LX/89V;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:LX/4gI;

.field public g:I

.field public h:LX/0Px;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public j:I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

.field public l:LX/89Z;

.field private m:LX/89U;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1304979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304980
    const/4 v0, 0x0

    iput v0, p0, LX/89V;->g:I

    .line 1304981
    sget-object v0, LX/89Z;->UNKNOWN:LX/89Z;

    iput-object v0, p0, LX/89V;->l:LX/89Z;

    .line 1304982
    sget-object v0, LX/89U;->SQUARE:LX/89U;

    iput-object v0, p0, LX/89V;->m:LX/89U;

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;
    .locals 15

    .prologue
    .line 1304983
    new-instance v0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    iget-boolean v1, p0, LX/89V;->a:Z

    iget-boolean v2, p0, LX/89V;->b:Z

    iget-boolean v3, p0, LX/89V;->c:Z

    iget-boolean v4, p0, LX/89V;->d:Z

    iget-boolean v5, p0, LX/89V;->e:Z

    iget-object v6, p0, LX/89V;->f:LX/4gI;

    iget v7, p0, LX/89V;->g:I

    iget-object v8, p0, LX/89V;->h:LX/0Px;

    iget-object v9, p0, LX/89V;->i:Ljava/lang/String;

    iget v10, p0, LX/89V;->j:I

    iget-object v11, p0, LX/89V;->k:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iget-object v12, p0, LX/89V;->l:LX/89Z;

    iget-object v13, p0, LX/89V;->m:LX/89U;

    const/4 v14, 0x0

    invoke-direct/range {v0 .. v14}, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;-><init>(ZZZZZLX/4gI;ILX/0Px;Ljava/lang/String;ILcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/89Z;LX/89U;B)V

    return-object v0
.end method
