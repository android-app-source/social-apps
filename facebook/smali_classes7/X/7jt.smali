.class public final LX/7jt;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/commerce/publishing/graphql/CommerceStoreDeleteMutationModels$CommerceStoreDeleteMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1230572
    const-class v1, Lcom/facebook/commerce/publishing/graphql/CommerceStoreDeleteMutationModels$CommerceStoreDeleteMutationModel;

    const v0, 0x1483aa47

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "CommerceStoreDeleteMutation"

    const-string v6, "d79a46d1b4a592bce6ff5c5b46186cf1"

    const-string v7, "commerce_merchant_deactivate"

    const-string v8, "0"

    const-string v9, "10155069963201729"

    const/4 v10, 0x0

    .line 1230573
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1230574
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1230575
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1230568
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1230569
    packed-switch v0, :pswitch_data_0

    .line 1230570
    :goto_0
    return-object p1

    .line 1230571
    :pswitch_0
    const-string p1, "0"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5fb57ca
        :pswitch_0
    .end packed-switch
.end method
