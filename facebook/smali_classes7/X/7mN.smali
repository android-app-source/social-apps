.class public LX/7mN;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/7mN;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7mO;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1RW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1236812
    const-string v0, "compost_draft"

    invoke-static {p1}, LX/7mN;->a(LX/0Uh;)I

    move-result v1

    new-instance v2, LX/7mM;

    invoke-direct {v2}, LX/7mM;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 1236813
    return-void
.end method

.method public static a(LX/0Uh;)I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1236814
    const/16 v1, 0x42

    invoke-virtual {p0, v1, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    :cond_0
    return v0
.end method

.method public static a(LX/0QB;)LX/7mN;
    .locals 6

    .prologue
    .line 1236815
    sget-object v0, LX/7mN;->d:LX/7mN;

    if-nez v0, :cond_1

    .line 1236816
    const-class v1, LX/7mN;

    monitor-enter v1

    .line 1236817
    :try_start_0
    sget-object v0, LX/7mN;->d:LX/7mN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1236818
    if-eqz v2, :cond_0

    .line 1236819
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1236820
    new-instance v4, LX/7mN;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {v4, v3}, LX/7mN;-><init>(LX/0Uh;)V

    .line 1236821
    const/16 v3, 0x19eb

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v5, 0x259

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0xf36

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1236822
    iput-object v3, v4, LX/7mN;->a:LX/0Ot;

    iput-object v5, v4, LX/7mN;->b:LX/0Ot;

    iput-object p0, v4, LX/7mN;->c:LX/0Ot;

    .line 1236823
    move-object v0, v4

    .line 1236824
    sput-object v0, LX/7mN;->d:LX/7mN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1236825
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1236826
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1236827
    :cond_1
    sget-object v0, LX/7mN;->d:LX/7mN;

    return-object v0

    .line 1236828
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1236829
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 1236830
    iget-object v0, p0, LX/7mN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7mO;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3, p1, v6}, LX/7mO;->a(JLandroid/database/sqlite/SQLiteDatabase;I)LX/0Px;

    move-result-object v3

    .line 1236831
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 1236832
    const-string v0, "draft_story"

    invoke-virtual {p1, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1236833
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7mj;

    .line 1236834
    iget-object v1, p0, LX/7mN;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7mO;

    invoke-virtual {v1, p1, v0}, LX/7mO;->a(Landroid/database/sqlite/SQLiteDatabase;LX/7mj;)V

    .line 1236835
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1236836
    :cond_0
    iget-object v0, p0, LX/7mN;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RW;

    .line 1236837
    iget-object v1, v0, LX/1RW;->a:LX/0Zb;

    const-string v2, "draft_db_do_upgrade"

    invoke-static {v0, v2}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "current_version"

    invoke-virtual {v2, v3, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "story_count"

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1236838
    return v4
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 7

    .prologue
    .line 1236839
    const/4 v4, 0x0

    .line 1236840
    iget-object v0, p0, LX/7mN;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RW;

    .line 1236841
    iget-object v1, v0, LX/1RW;->a:LX/0Zb;

    const-string v2, "draft_db_upgrade_start"

    invoke-static {v0, v2}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "current_version"

    invoke-virtual {v2, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "to_version"

    invoke-virtual {v2, v3, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1236842
    if-le p2, p3, :cond_1

    .line 1236843
    :try_start_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot upgrade to a lower version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1236844
    :catch_0
    move-exception v0

    move-object v1, v0

    move v2, p2

    .line 1236845
    :goto_0
    :try_start_1
    const-string v0, "draft_story"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v3, 0x5b35f214

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x2f5ac730

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1236846
    iget-object v0, p0, LX/7mN;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "compost_draft_table_upgrade"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error while trying tp upgrade DB: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1236847
    iget-object v0, p0, LX/7mN;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RW;

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    move v1, p2

    move v3, p3

    .line 1236848
    iget-object v6, v0, LX/1RW;->a:LX/0Zb;

    const-string p0, "draft_db_upgrade_failed"

    invoke-static {v0, p0}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "start_version"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "current_version"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "to_version"

    invoke-virtual {p0, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "story_count"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "details"

    invoke-virtual {p0, p1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v6, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1236849
    :goto_1
    return-void

    .line 1236850
    :pswitch_0
    :try_start_2
    invoke-direct {p0, p1}, LX/7mN;->d(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v4

    .line 1236851
    add-int/lit8 v2, v2, 0x1

    .line 1236852
    :goto_2
    if-ge v2, p3, :cond_0

    .line 1236853
    packed-switch v2, :pswitch_data_0

    .line 1236854
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unable to upgrade to version "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " from version "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1236855
    :catch_1
    move-exception v0

    move-object v1, v0

    goto/16 :goto_0

    .line 1236856
    :cond_0
    iget-object v0, p0, LX/7mN;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RW;

    .line 1236857
    iget-object v1, v0, LX/1RW;->a:LX/0Zb;

    const-string v2, "draft_db_upgrade_finish"

    invoke-static {v0, v2}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "current_version"

    invoke-virtual {v2, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "to_version"

    invoke-virtual {v2, v3, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "story_count"

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1236858
    goto :goto_1

    .line 1236859
    :catchall_0
    move-exception v0

    .line 1236860
    throw v0

    :cond_1
    move v2, p2

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
