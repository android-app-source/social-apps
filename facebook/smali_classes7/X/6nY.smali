.class public abstract LX/6nY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/content/ComponentName;

.field public final c:Landroid/content/Context;

.field public final d:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/ComponentName;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 1147358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1147359
    iput-object p2, p0, LX/6nY;->a:Ljava/lang/String;

    .line 1147360
    iput-object p3, p0, LX/6nY;->b:Landroid/content/ComponentName;

    .line 1147361
    iput-object p1, p0, LX/6nY;->c:Landroid/content/Context;

    .line 1147362
    iput-object p4, p0, LX/6nY;->d:Ljava/util/concurrent/ExecutorService;

    .line 1147363
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1147364
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1147365
    iget-object v1, p0, LX/6nY;->b:Landroid/content/ComponentName;

    move-object v1, v1

    .line 1147366
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1147367
    return-object v0
.end method

.method public final a(Lcom/google/common/util/concurrent/SettableFuture;Landroid/content/ServiceConnection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<*>;",
            "Landroid/content/ServiceConnection;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1147368
    invoke-virtual {p0}, LX/6nY;->a()Landroid/content/Intent;

    move-result-object v0

    .line 1147369
    :try_start_0
    iget-object v1, p0, LX/6nY;->c:Landroid/content/Context;

    .line 1147370
    const/4 v2, 0x1

    move v2, v2

    .line 1147371
    const v3, -0x145961e

    invoke-static {v1, v0, p2, v2, v3}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1147372
    new-instance v1, Landroid/content/pm/PackageManager$NameNotFoundException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to bind to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/6nY;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "service: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/content/pm/PackageManager$NameNotFoundException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1147373
    :cond_0
    :goto_0
    return-void

    .line 1147374
    :catch_0
    move-exception v0

    .line 1147375
    invoke-virtual {p1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
