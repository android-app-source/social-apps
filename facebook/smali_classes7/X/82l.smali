.class public final enum LX/82l;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/82l;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/82l;

.field public static final enum SWIPE:LX/82l;

.field public static final enum TAP:LX/82l;


# instance fields
.field private final mAction:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1288445
    new-instance v0, LX/82l;

    const-string v1, "SWIPE"

    const-string v2, "navigate_page_swipe"

    invoke-direct {v0, v1, v3, v2}, LX/82l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/82l;->SWIPE:LX/82l;

    .line 1288446
    new-instance v0, LX/82l;

    const-string v1, "TAP"

    const-string v2, "navigate_tab_click"

    invoke-direct {v0, v1, v4, v2}, LX/82l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/82l;->TAP:LX/82l;

    .line 1288447
    const/4 v0, 0x2

    new-array v0, v0, [LX/82l;

    sget-object v1, LX/82l;->SWIPE:LX/82l;

    aput-object v1, v0, v3

    sget-object v1, LX/82l;->TAP:LX/82l;

    aput-object v1, v0, v4

    sput-object v0, LX/82l;->$VALUES:[LX/82l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1288450
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1288451
    iput-object p3, p0, LX/82l;->mAction:Ljava/lang/String;

    .line 1288452
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/82l;
    .locals 1

    .prologue
    .line 1288453
    const-class v0, LX/82l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/82l;

    return-object v0
.end method

.method public static values()[LX/82l;
    .locals 1

    .prologue
    .line 1288449
    sget-object v0, LX/82l;->$VALUES:[LX/82l;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/82l;

    return-object v0
.end method


# virtual methods
.method public final getAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1288448
    iget-object v0, p0, LX/82l;->mAction:Ljava/lang/String;

    return-object v0
.end method
