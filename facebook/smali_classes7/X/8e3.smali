.class public final LX/8e3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1380022
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1380023
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1380024
    :goto_0
    return v1

    .line 1380025
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1380026
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1380027
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1380028
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1380029
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1380030
    const-string v4, "article_canonical_url"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1380031
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1380032
    :cond_2
    const-string v4, "document_owner"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1380033
    invoke-static {p0, p1}, LX/8e2;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1380034
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1380035
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1380036
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1380037
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1380038
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1380039
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1380040
    if-eqz v0, :cond_0

    .line 1380041
    const-string v1, "article_canonical_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1380042
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1380043
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1380044
    if-eqz v0, :cond_1

    .line 1380045
    const-string v1, "document_owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1380046
    invoke-static {p0, v0, p2}, LX/8e2;->a(LX/15i;ILX/0nX;)V

    .line 1380047
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1380048
    return-void
.end method
