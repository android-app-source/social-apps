.class public final enum LX/8By;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8By;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8By;

.field public static final enum AIRPLANE_MODE:LX/8By;

.field public static final enum CAPTIVE_PORTAL:LX/8By;

.field public static final enum CONNECTED:LX/8By;

.field public static final enum CONNECTING:LX/8By;

.field public static final enum HIDDEN:LX/8By;

.field public static final enum NO_INTERNET:LX/8By;

.field public static final enum WAITING_TO_CONNECT:LX/8By;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1309987
    new-instance v0, LX/8By;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v3}, LX/8By;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8By;->HIDDEN:LX/8By;

    .line 1309988
    new-instance v0, LX/8By;

    const-string v1, "NO_INTERNET"

    invoke-direct {v0, v1, v4}, LX/8By;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8By;->NO_INTERNET:LX/8By;

    .line 1309989
    new-instance v0, LX/8By;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v5}, LX/8By;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8By;->CONNECTED:LX/8By;

    .line 1309990
    new-instance v0, LX/8By;

    const-string v1, "WAITING_TO_CONNECT"

    invoke-direct {v0, v1, v6}, LX/8By;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8By;->WAITING_TO_CONNECT:LX/8By;

    .line 1309991
    new-instance v0, LX/8By;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v7}, LX/8By;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8By;->CONNECTING:LX/8By;

    .line 1309992
    new-instance v0, LX/8By;

    const-string v1, "AIRPLANE_MODE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8By;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8By;->AIRPLANE_MODE:LX/8By;

    .line 1309993
    new-instance v0, LX/8By;

    const-string v1, "CAPTIVE_PORTAL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8By;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8By;->CAPTIVE_PORTAL:LX/8By;

    .line 1309994
    const/4 v0, 0x7

    new-array v0, v0, [LX/8By;

    sget-object v1, LX/8By;->HIDDEN:LX/8By;

    aput-object v1, v0, v3

    sget-object v1, LX/8By;->NO_INTERNET:LX/8By;

    aput-object v1, v0, v4

    sget-object v1, LX/8By;->CONNECTED:LX/8By;

    aput-object v1, v0, v5

    sget-object v1, LX/8By;->WAITING_TO_CONNECT:LX/8By;

    aput-object v1, v0, v6

    sget-object v1, LX/8By;->CONNECTING:LX/8By;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8By;->AIRPLANE_MODE:LX/8By;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8By;->CAPTIVE_PORTAL:LX/8By;

    aput-object v2, v0, v1

    sput-object v0, LX/8By;->$VALUES:[LX/8By;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1309995
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8By;
    .locals 1

    .prologue
    .line 1309996
    const-class v0, LX/8By;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8By;

    return-object v0
.end method

.method public static values()[LX/8By;
    .locals 1

    .prologue
    .line 1309997
    sget-object v0, LX/8By;->$VALUES:[LX/8By;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8By;

    return-object v0
.end method
