.class public LX/7ky;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1233045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1233046
    return-void
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1233047
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1233048
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1233049
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1233050
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1233051
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Px;LX/0Px;LX/7l0;J)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;",
            "LX/7l0;",
            "J)",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1233052
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1233053
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1233054
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1233055
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v6

    cmp-long v6, p3, v6

    if-eqz v6, :cond_0

    .line 1233056
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1233057
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1233058
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1233059
    :cond_1
    invoke-static {p2, p1}, LX/7ky;->a(LX/7l0;LX/0Px;)LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_3

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1233060
    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/Tag;->h()J

    move-result-wide v8

    cmp-long v1, p3, v8

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/Tag;->h()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1233061
    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/Tag;->h()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v7

    .line 1233062
    iget-object v1, v0, Lcom/facebook/photos/base/tagging/Tag;->b:Lcom/facebook/user/model/Name;

    move-object v1, v1

    .line 1233063
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/Name;

    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v1

    .line 1233064
    iput-object v1, v7, LX/5Rc;->b:Ljava/lang/String;

    .line 1233065
    move-object v1, v7

    .line 1233066
    iget-object v7, v0, Lcom/facebook/photos/base/tagging/Tag;->i:Ljava/lang/String;

    move-object v7, v7

    .line 1233067
    iput-object v7, v1, LX/5Rc;->c:Ljava/lang/String;

    .line 1233068
    move-object v1, v1

    .line 1233069
    invoke-virtual {v1}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1233070
    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/Tag;->h()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1233071
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1233072
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Px;LX/75F;J)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;",
            "LX/75F;",
            "J)",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1233073
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 1233074
    invoke-static {p0}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_3

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1233075
    instance-of v1, v0, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v1, :cond_2

    .line 1233076
    new-instance v1, Lcom/facebook/ipc/media/MediaIdKey;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v7

    .line 1233077
    iget-object v8, v0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v0, v8

    .line 1233078
    invoke-virtual {v0}, Lcom/facebook/ipc/media/data/LocalMediaData;->d()J

    move-result-wide v8

    invoke-direct {v1, v7, v8, v9}, Lcom/facebook/ipc/media/MediaIdKey;-><init>(Ljava/lang/String;J)V

    invoke-virtual {p1, v1}, LX/75F;->a(Lcom/facebook/ipc/media/MediaIdKey;)LX/0Px;

    move-result-object v7

    .line 1233079
    if-eqz v7, :cond_2

    .line 1233080
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v1, v2

    :goto_1
    if-ge v1, v8, :cond_2

    invoke-virtual {v7, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1233081
    const/4 v9, 0x1

    .line 1233082
    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->o()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->n()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-ne v10, v9, :cond_4

    :goto_2
    move v9, v9

    .line 1233083
    if-eqz v9, :cond_1

    .line 1233084
    iget-boolean v9, v0, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    move v9, v9

    .line 1233085
    if-nez v9, :cond_1

    .line 1233086
    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1233087
    invoke-virtual {v0}, Lcom/facebook/tagging/model/TaggingProfile;->b()J

    move-result-wide v10

    cmp-long v10, v10, p2

    if-eqz v10, :cond_0

    .line 1233088
    invoke-virtual {v0}, Lcom/facebook/tagging/model/TaggingProfile;->b()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1233089
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1233090
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1233091
    :cond_3
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0

    :cond_4
    const/4 v9, 0x0

    goto :goto_2
.end method

.method public static a(LX/7l0;LX/0Px;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7l0;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1233092
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1233093
    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, LX/7ky;->a(LX/7l0;LX/0Px;LX/0Rf;Z)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/7l0;LX/0Px;LX/0Rf;Z)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7l0;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;Z)",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1233094
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1233095
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1233096
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    .line 1233097
    instance-of v6, v1, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v6, :cond_1

    move-object v0, v1

    .line 1233098
    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1233099
    :cond_0
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1233100
    :cond_1
    instance-of v1, v1, Lcom/facebook/photos/base/media/VideoItem;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1233101
    invoke-static {v0}, LX/7ky;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/0Px;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 1233102
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1233103
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 1233104
    invoke-virtual {p0, v4}, LX/7l0;->a(Ljava/util/List;)LX/0Px;

    move-result-object v4

    .line 1233105
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_2
    if-ge v1, v6, :cond_5

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1233106
    if-eqz p3, :cond_3

    .line 1233107
    iget-wide v10, v0, Lcom/facebook/photos/base/tagging/Tag;->c:J

    move-wide v8, v10

    .line 1233108
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1233109
    :cond_3
    iget-wide v10, v0, Lcom/facebook/photos/base/tagging/Tag;->c:J

    move-wide v8, v10

    .line 1233110
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p2, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1233111
    iget-wide v10, v0, Lcom/facebook/photos/base/tagging/Tag;->c:J

    move-wide v8, v10

    .line 1233112
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1233113
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1233114
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1233115
    :cond_5
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1233116
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1233117
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getFrames()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 1233118
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    .line 1233119
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->getMediaData()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1233120
    if-eqz v0, :cond_0

    .line 1233121
    new-instance v5, LX/74k;

    invoke-direct {v5}, LX/74k;-><init>()V

    new-instance v6, LX/4gN;

    invoke-direct {v6}, LX/4gN;-><init>()V

    invoke-virtual {v6, v0}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v0

    invoke-virtual {v0}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    .line 1233122
    iput-object v0, v5, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1233123
    move-object v0, v5

    .line 1233124
    invoke-virtual {v0}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v0

    .line 1233125
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1233126
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1233127
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(JLX/7l0;LX/0Px;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Px;)LX/0Rf;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/7l0;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1233128
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1233129
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {p3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1233130
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1233131
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1233132
    :cond_0
    const v0, 0x285feb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v4

    .line 1233133
    invoke-static {p4, v4}, LX/8oj;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Rf;)LX/0Px;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1233134
    invoke-virtual {p5}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_1

    invoke-virtual {p5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1233135
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0, v4}, LX/8oj;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Rf;)LX/0Px;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1233136
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1233137
    :cond_1
    invoke-static {p2, p5}, LX/7ky;->a(LX/7l0;LX/0Px;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    :goto_2
    if-ge v1, v4, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1233138
    iget-wide v8, v0, Lcom/facebook/photos/base/tagging/Tag;->c:J

    move-wide v6, v8

    .line 1233139
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1233140
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1233141
    :cond_2
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1233142
    invoke-static {v3}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
