.class public final LX/6yg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

.field public final synthetic b:LX/6yh;


# direct methods
.method public constructor <init>(LX/6yh;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)V
    .locals 0

    .prologue
    .line 1160309
    iput-object p1, p0, LX/6yg;->b:LX/6yh;

    iput-object p2, p0, LX/6yg;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x32c3b907

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1160310
    iget-object v1, p0, LX/6yg;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    iget-object v2, p0, LX/6yg;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    const v3, 0x7f081e7f

    invoke-static {v1, v2, v3}, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;I)Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;

    move-result-object v1

    .line 1160311
    iget-object v2, p0, LX/6yg;->b:LX/6yh;

    iget-object v2, v2, LX/6yh;->a:LX/6qh;

    invoke-virtual {v1, v2}, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;->a(LX/6qh;)V

    .line 1160312
    iget-object v2, p0, LX/6yg;->b:LX/6yh;

    iget-object v2, v2, LX/6yh;->a:LX/6qh;

    invoke-virtual {v2, v1}, LX/6qh;->a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V

    .line 1160313
    const v1, 0x43bd2eeb

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
