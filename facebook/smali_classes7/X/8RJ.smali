.class public LX/8RJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/8RH;

.field private static volatile f:LX/8RJ;


# instance fields
.field public final b:LX/11i;

.field private final c:Landroid/content/Context;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1344586
    new-instance v0, LX/8RH;

    invoke-direct {v0}, LX/8RH;-><init>()V

    sput-object v0, LX/8RJ;->a:LX/8RH;

    return-void
.end method

.method public constructor <init>(LX/11i;Landroid/content/Context;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11i;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1344587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1344588
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8RJ;->e:Z

    .line 1344589
    iput-object p1, p0, LX/8RJ;->b:LX/11i;

    .line 1344590
    iput-object p2, p0, LX/8RJ;->c:Landroid/content/Context;

    .line 1344591
    iput-object p3, p0, LX/8RJ;->d:LX/0Ot;

    .line 1344592
    return-void
.end method

.method public static a(LX/0QB;)LX/8RJ;
    .locals 6

    .prologue
    .line 1344593
    sget-object v0, LX/8RJ;->f:LX/8RJ;

    if-nez v0, :cond_1

    .line 1344594
    const-class v1, LX/8RJ;

    monitor-enter v1

    .line 1344595
    :try_start_0
    sget-object v0, LX/8RJ;->f:LX/8RJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1344596
    if-eqz v2, :cond_0

    .line 1344597
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1344598
    new-instance v5, LX/8RJ;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/8RJ;-><init>(LX/11i;Landroid/content/Context;LX/0Ot;)V

    .line 1344599
    move-object v0, v5

    .line 1344600
    sput-object v0, LX/8RJ;->f:LX/8RJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1344601
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1344602
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1344603
    :cond_1
    sget-object v0, LX/8RJ;->f:LX/8RJ;

    return-object v0

    .line 1344604
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1344605
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/8RI;)V
    .locals 6

    .prologue
    .line 1344606
    iget-object v0, p0, LX/8RJ;->b:LX/11i;

    sget-object v1, LX/8RJ;->a:LX/8RH;

    const-string v2, "year_class"

    iget-object v3, p0, LX/8RJ;->c:Landroid/content/Context;

    invoke-static {v3}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "caller"

    invoke-virtual {p1}, LX/8RI;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/11i;->a(LX/0Pq;LX/0P1;)LX/11o;

    .line 1344607
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8RJ;->e:Z

    .line 1344608
    return-void
.end method
