.class public LX/6rx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6rs;


# direct methods
.method public constructor <init>(LX/6rs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1152806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152807
    iput-object p1, p0, LX/6rx;->a:LX/6rs;

    .line 1152808
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1152809
    new-instance v0, LX/6rQ;

    invoke-direct {v0}, LX/6rQ;-><init>()V

    move-object v1, v0

    .line 1152810
    iget-object v0, p0, LX/6rx;->a:LX/6rs;

    .line 1152811
    iget-object v2, v0, LX/6rs;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6rr;

    move-object v0, v2

    .line 1152812
    const-string v2, "items"

    invoke-static {p2, v2}, LX/16N;->d(LX/0lF;Ljava/lang/String;)LX/162;

    move-result-object v2

    invoke-interface {v0, p1, v2}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1152813
    iput-object v0, v1, LX/6rQ;->b:LX/0Px;

    .line 1152814
    iget-object v0, p0, LX/6rx;->a:LX/6rs;

    invoke-virtual {v0, p1}, LX/6rs;->i(Ljava/lang/String;)LX/6rr;

    move-result-object v0

    const-string v2, "price_list"

    invoke-static {p2, v2}, LX/16N;->d(LX/0lF;Ljava/lang/String;)LX/162;

    move-result-object v2

    invoke-interface {v0, p1, v2}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1152815
    iput-object v0, v1, LX/6rQ;->c:LX/0Px;

    .line 1152816
    iget-object v0, p0, LX/6rx;->a:LX/6rs;

    .line 1152817
    const/4 v2, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v2, :pswitch_data_1

    .line 1152818
    iget-object v2, v0, LX/6rs;->p:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6rr;

    :goto_1
    move-object v0, v2

    .line 1152819
    const-string v2, "purchase_info"

    invoke-static {p2, v2}, LX/16N;->d(LX/0lF;Ljava/lang/String;)LX/162;

    move-result-object v2

    invoke-interface {v0, p1, v2}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1152820
    iput-object v0, v1, LX/6rQ;->d:LX/0Px;

    .line 1152821
    const-string v0, "pay_action_content"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1152822
    iget-object v0, p0, LX/6rx;->a:LX/6rs;

    .line 1152823
    iget-object v2, v0, LX/6rs;->o:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6rr;

    move-object v0, v2

    .line 1152824
    const-string v2, "pay_action_content"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-interface {v0, p1, v2}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;

    .line 1152825
    iput-object v0, v1, LX/6rQ;->e:Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;

    .line 1152826
    :cond_1
    invoke-virtual {v1}, LX/6rQ;->a()Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    move-result-object v0

    return-object v0

    .line 1152827
    :pswitch_0
    const-string v3, "1.1"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    .line 1152828
    :pswitch_1
    iget-object v2, v0, LX/6rs;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6rr;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0xbdb4
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
