.class public final enum LX/6fk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6fk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6fk;

.field public static final enum Enabled:LX/6fk;

.field public static final enum PermanentlyDisabled:LX/6fk;

.field public static final enum TemporarilyMuted:LX/6fk;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1120387
    new-instance v0, LX/6fk;

    const-string v1, "Enabled"

    invoke-direct {v0, v1, v2}, LX/6fk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fk;->Enabled:LX/6fk;

    .line 1120388
    new-instance v0, LX/6fk;

    const-string v1, "PermanentlyDisabled"

    invoke-direct {v0, v1, v3}, LX/6fk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fk;->PermanentlyDisabled:LX/6fk;

    .line 1120389
    new-instance v0, LX/6fk;

    const-string v1, "TemporarilyMuted"

    invoke-direct {v0, v1, v4}, LX/6fk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6fk;->TemporarilyMuted:LX/6fk;

    .line 1120390
    const/4 v0, 0x3

    new-array v0, v0, [LX/6fk;

    sget-object v1, LX/6fk;->Enabled:LX/6fk;

    aput-object v1, v0, v2

    sget-object v1, LX/6fk;->PermanentlyDisabled:LX/6fk;

    aput-object v1, v0, v3

    sget-object v1, LX/6fk;->TemporarilyMuted:LX/6fk;

    aput-object v1, v0, v4

    sput-object v0, LX/6fk;->$VALUES:[LX/6fk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1120391
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6fk;
    .locals 1

    .prologue
    .line 1120392
    const-class v0, LX/6fk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6fk;

    return-object v0
.end method

.method public static values()[LX/6fk;
    .locals 1

    .prologue
    .line 1120393
    sget-object v0, LX/6fk;->$VALUES:[LX/6fk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6fk;

    return-object v0
.end method
