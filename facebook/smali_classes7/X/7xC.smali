.class public LX/7xC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/7xC;


# instance fields
.field public a:Landroid/content/Context;

.field private b:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1277147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1277148
    iput-object p1, p0, LX/7xC;->a:Landroid/content/Context;

    .line 1277149
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    iput-object v0, p0, LX/7xC;->b:Ljava/util/Locale;

    .line 1277150
    return-void
.end method

.method public static a(LX/0QB;)LX/7xC;
    .locals 5

    .prologue
    .line 1277134
    sget-object v0, LX/7xC;->c:LX/7xC;

    if-nez v0, :cond_1

    .line 1277135
    const-class v1, LX/7xC;

    monitor-enter v1

    .line 1277136
    :try_start_0
    sget-object v0, LX/7xC;->c:LX/7xC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1277137
    if-eqz v2, :cond_0

    .line 1277138
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1277139
    new-instance v4, LX/7xC;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 p0, 0x1617

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/7xC;-><init>(Landroid/content/Context;LX/0Or;)V

    .line 1277140
    move-object v0, v4

    .line 1277141
    sput-object v0, LX/7xC;->c:LX/7xC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1277142
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1277143
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1277144
    :cond_1
    sget-object v0, LX/7xC;->c:LX/7xC;

    return-object v0

    .line 1277145
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1277146
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/7xB;)Ljava/text/SimpleDateFormat;
    .locals 3

    .prologue
    .line 1277125
    sget-object v0, LX/7xA;->a:[I

    invoke-virtual {p1}, LX/7xB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1277126
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal relative time state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1277127
    :pswitch_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "h aa"

    iget-object v2, p0, LX/7xC;->b:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1277128
    :goto_0
    return-object v0

    .line 1277129
    :pswitch_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "h:mm aa"

    iget-object v2, p0, LX/7xC;->b:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    .line 1277130
    :pswitch_2
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "kk:mm"

    iget-object v2, p0, LX/7xC;->b:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    .line 1277131
    :pswitch_3
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE"

    iget-object v2, p0, LX/7xC;->b:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    .line 1277132
    :pswitch_4
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMM d"

    iget-object v2, p0, LX/7xC;->b:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    .line 1277133
    :pswitch_5
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMM d, yyyy"

    iget-object v2, p0, LX/7xC;->b:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1277079
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1277080
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Event GraphQL model doesn\'t contain ticket tier info."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1277081
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 1277082
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 1277083
    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static c(J)Z
    .locals 6

    .prologue
    const/4 v4, 0x6

    const/4 v0, 0x1

    .line 1277119
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 1277120
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 1277121
    invoke-virtual {v2, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1277122
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 1277123
    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 1277124
    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v2, v1, :cond_0

    sub-int v1, v3, v4

    const/4 v2, 0x7

    if-gt v1, v2, :cond_0

    sub-int v1, v3, v4

    if-ltz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1277113
    invoke-direct {p0, p1, p2}, LX/7xC;->e(J)LX/7xB;

    move-result-object v0

    .line 1277114
    sget-object v1, LX/7xB;->TOMORROW:LX/7xB;

    if-ne v0, v1, :cond_0

    .line 1277115
    iget-object v0, p0, LX/7xC;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081f92

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1277116
    :goto_0
    return-object v0

    .line 1277117
    :cond_0
    invoke-direct {p0, v0}, LX/7xC;->a(LX/7xB;)Ljava/text/SimpleDateFormat;

    move-result-object v0

    .line 1277118
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private e(J)LX/7xB;
    .locals 5

    .prologue
    .line 1277084
    invoke-static {p1, p2}, LX/7xC;->h(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277085
    iget-object v0, p0, LX/7xC;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1277086
    sget-object v0, LX/7xB;->DAY_TWENTY_FOUR:LX/7xB;

    .line 1277087
    :goto_0
    move-object v0, v0

    .line 1277088
    :goto_1
    return-object v0

    .line 1277089
    :cond_0
    const/4 p0, 0x5

    const/4 v0, 0x1

    .line 1277090
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 1277091
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 1277092
    invoke-virtual {v2, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1277093
    invoke-virtual {v1, p0, v0}, Ljava/util/Calendar;->add(II)V

    .line 1277094
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_7

    invoke-virtual {v1, p0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v2, p0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_7

    :goto_2
    move v0, v0

    .line 1277095
    if-eqz v0, :cond_1

    .line 1277096
    sget-object v0, LX/7xB;->TOMORROW:LX/7xB;

    goto :goto_1

    .line 1277097
    :cond_1
    invoke-static {p1, p2}, LX/7xC;->c(J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1277098
    sget-object v0, LX/7xB;->WEEK_DAY:LX/7xB;

    goto :goto_1

    .line 1277099
    :cond_2
    const/4 v0, 0x1

    .line 1277100
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 1277101
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 1277102
    invoke-virtual {v2, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1277103
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_8

    :goto_3
    move v0, v0

    .line 1277104
    if-eqz v0, :cond_3

    .line 1277105
    sget-object v0, LX/7xB;->DATE:LX/7xB;

    goto :goto_1

    .line 1277106
    :cond_3
    sget-object v0, LX/7xB;->DATE_YEAR:LX/7xB;

    goto :goto_1

    .line 1277107
    :cond_4
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1277108
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1277109
    const/16 p0, 0xc

    invoke-virtual {v0, p0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 1277110
    if-nez v0, :cond_5

    .line 1277111
    sget-object v0, LX/7xB;->DAY:LX/7xB;

    goto :goto_0

    .line 1277112
    :cond_5
    sget-object v0, LX/7xB;->DAY_ALT:LX/7xB;

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_4

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private static h(J)Z
    .locals 2

    .prologue
    .line 1277078
    invoke-static {p0, p1}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(J)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1277075
    invoke-static {p1, p2}, LX/7xC;->h(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277076
    iget-object v0, p0, LX/7xC;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081f8f

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, LX/7xC;->d(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1277077
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/7xC;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081f8e

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, LX/7xC;->d(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(J)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1277072
    invoke-static {p1, p2}, LX/7xC;->h(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277073
    iget-object v0, p0, LX/7xC;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081f91

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, LX/7xC;->d(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1277074
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/7xC;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081f90

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, LX/7xC;->d(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
