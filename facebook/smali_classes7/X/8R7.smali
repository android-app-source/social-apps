.class public final LX/8R7;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V
    .locals 0

    .prologue
    .line 1344279
    iput-object p1, p0, LX/8R7;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1344280
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1344281
    check-cast p1, Ljava/util/List;

    .line 1344282
    iget-object v0, p0, LX/8R7;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    .line 1344283
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1344284
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 1344285
    new-instance v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-static {v2}, LX/8uo;->a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/User;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/User;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1344286
    :cond_0
    move-object v2, v3

    .line 1344287
    move-object v1, v2

    .line 1344288
    iput-object v1, v0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->n:Ljava/util/ArrayList;

    .line 1344289
    iget-object v0, p0, LX/8R7;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->f:LX/8tB;

    iget-object v1, p0, LX/8R7;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v2, p0, LX/8R7;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v2, v2, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->n:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->b$redex0(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8tB;->a(Ljava/util/List;)V

    .line 1344290
    iget-object v0, p0, LX/8R7;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    iget-object v1, p0, LX/8R7;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->f:LX/8tB;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/8tB;->g(I)LX/621;

    move-result-object v1

    invoke-interface {v1}, LX/621;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1344291
    iget-object v0, p0, LX/8R7;->a:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->k(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V

    .line 1344292
    return-void
.end method
