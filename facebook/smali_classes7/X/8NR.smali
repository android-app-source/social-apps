.class public LX/8NR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/8NS;",
        "LX/8NT;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1337292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/8NR;
    .locals 1

    .prologue
    .line 1337158
    new-instance v0, LX/8NR;

    invoke-direct {v0}, LX/8NR;-><init>()V

    .line 1337159
    move-object v0, v0

    .line 1337160
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 14

    .prologue
    .line 1337163
    check-cast p1, LX/8NS;

    const/4 v11, 0x1

    .line 1337164
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1337165
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "start_offset"

    .line 1337166
    iget-wide v12, p1, LX/8NS;->c:J

    move-wide v2, v12

    .line 1337167
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337168
    iget-object v0, p1, LX/8NS;->a:Ljava/lang/String;

    move-object v9, v0

    .line 1337169
    invoke-static {v9}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1337170
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "composer_session_id"

    invoke-direct {v0, v1, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337171
    :cond_0
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "target"

    .line 1337172
    iget-wide v12, p1, LX/8NS;->e:J

    move-wide v2, v12

    .line 1337173
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337174
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "upload_speed"

    .line 1337175
    iget v2, p1, LX/8NS;->h:F

    move v2, v2

    .line 1337176
    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337177
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "v2.3/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1337178
    iget-wide v12, p1, LX/8NS;->e:J

    move-wide v2, v12

    .line 1337179
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/videos"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1337180
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "upload_phase"

    const-string v2, "transfer"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337181
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "upload_session_id"

    .line 1337182
    iget-wide v12, p1, LX/8NS;->b:J

    move-wide v2, v12

    .line 1337183
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337184
    iget-object v0, p1, LX/8NS;->m:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1337185
    if-eqz v0, :cond_2

    .line 1337186
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "fbuploader_video_file_chunk"

    .line 1337187
    iget-object v2, p1, LX/8NS;->m:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1337188
    const-string v2, ""

    .line 1337189
    :goto_1
    move-object v2, v2

    .line 1337190
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337191
    iget-boolean v0, p1, LX/8NS;->i:Z

    move v0, v0

    .line 1337192
    if-eqz v0, :cond_1

    .line 1337193
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "partition_start_offset"

    .line 1337194
    iget-wide v12, p1, LX/8NS;->j:J

    move-wide v2, v12

    .line 1337195
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337196
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "partition_end_offset"

    .line 1337197
    iget-wide v12, p1, LX/8NS;->k:J

    move-wide v2, v12

    .line 1337198
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337199
    :cond_1
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "upload-video-chunk-receive"

    .line 1337200
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1337201
    move-object v0, v0

    .line 1337202
    const-string v1, "POST"

    .line 1337203
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1337204
    move-object v0, v0

    .line 1337205
    iput-object v10, v0, LX/14O;->d:Ljava/lang/String;

    .line 1337206
    move-object v0, v0

    .line 1337207
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1337208
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1337209
    move-object v0, v0

    .line 1337210
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1337211
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1337212
    move-object v0, v0

    .line 1337213
    iput-boolean v11, v0, LX/14O;->n:Z

    .line 1337214
    move-object v0, v0

    .line 1337215
    iput-boolean v11, v0, LX/14O;->p:Z

    .line 1337216
    move-object v0, v0

    .line 1337217
    iput-object v9, v0, LX/14O;->A:Ljava/lang/String;

    .line 1337218
    move-object v0, v0

    .line 1337219
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    .line 1337220
    :goto_2
    return-object v0

    .line 1337221
    :cond_2
    iget-object v0, p1, LX/8NS;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1337222
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1337223
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 1337224
    :cond_3
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1337225
    iget-object v0, p1, LX/8NS;->l:LX/8Oa;

    move-object v4, v0

    .line 1337226
    if-eqz v4, :cond_4

    .line 1337227
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "partition_start_offset"

    iget-wide v6, v4, LX/8Oa;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337228
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "partition_end_offset"

    iget-wide v6, v4, LX/8Oa;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337229
    new-instance v0, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 1337230
    const-string v2, "segment_type"

    iget-object v3, v4, LX/8Oa;->c:LX/8OZ;

    invoke-virtual {v3}, LX/8OZ;->getValue()I

    move-result v3

    int-to-long v6, v3

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1337231
    const-string v2, "segment_start_offset"

    iget-wide v6, v4, LX/8Oa;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1337232
    const-string v2, "segment_end_offset"

    iget-wide v6, v4, LX/8Oa;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1337233
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "metadata"

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337234
    new-instance v0, LX/4cu;

    .line 1337235
    iget-object v2, p1, LX/8NS;->g:Ljava/lang/String;

    move-object v2, v2

    .line 1337236
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1337237
    iget-wide v12, p1, LX/8NS;->c:J

    move-wide v6, v12

    .line 1337238
    iget-wide v4, v4, LX/8Oa;->e:J

    sub-long v4, v6, v4

    .line 1337239
    iget-wide v12, p1, LX/8NS;->d:J

    move-wide v6, v12

    .line 1337240
    invoke-direct/range {v0 .. v7}, LX/4cu;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 1337241
    :goto_3
    new-instance v1, LX/4cQ;

    const-string v2, "video_file_chunk"

    invoke-direct {v1, v2, v0}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 1337242
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v2, "upload-video-chunk-receive"

    .line 1337243
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 1337244
    move-object v0, v0

    .line 1337245
    const-string v2, "POST"

    .line 1337246
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 1337247
    move-object v0, v0

    .line 1337248
    iput-object v10, v0, LX/14O;->d:Ljava/lang/String;

    .line 1337249
    move-object v0, v0

    .line 1337250
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1337251
    iput-object v2, v0, LX/14O;->k:LX/14S;

    .line 1337252
    move-object v0, v0

    .line 1337253
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1337254
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 1337255
    move-object v0, v0

    .line 1337256
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1337257
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1337258
    move-object v0, v0

    .line 1337259
    iput-boolean v11, v0, LX/14O;->n:Z

    .line 1337260
    move-object v0, v0

    .line 1337261
    iput-boolean v11, v0, LX/14O;->p:Z

    .line 1337262
    move-object v0, v0

    .line 1337263
    iput-object v9, v0, LX/14O;->A:Ljava/lang/String;

    .line 1337264
    move-object v0, v0

    .line 1337265
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    goto/16 :goto_2

    .line 1337266
    :cond_4
    iget-boolean v0, p1, LX/8NS;->i:Z

    move v0, v0

    .line 1337267
    if-nez v0, :cond_5

    .line 1337268
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "start_offset"

    .line 1337269
    iget-wide v12, p1, LX/8NS;->c:J

    move-wide v4, v12

    .line 1337270
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337271
    new-instance v0, LX/4cu;

    .line 1337272
    iget-object v2, p1, LX/8NS;->g:Ljava/lang/String;

    move-object v2, v2

    .line 1337273
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1337274
    iget-wide v12, p1, LX/8NS;->c:J

    move-wide v4, v12

    .line 1337275
    iget-wide v12, p1, LX/8NS;->d:J

    move-wide v6, v12

    .line 1337276
    invoke-direct/range {v0 .. v7}, LX/4cu;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JJ)V

    goto :goto_3

    .line 1337277
    :cond_5
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "start_offset"

    .line 1337278
    iget-wide v12, p1, LX/8NS;->c:J

    move-wide v4, v12

    .line 1337279
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337280
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "partition_start_offset"

    .line 1337281
    iget-wide v12, p1, LX/8NS;->j:J

    move-wide v4, v12

    .line 1337282
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337283
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "partition_end_offset"

    .line 1337284
    iget-wide v12, p1, LX/8NS;->k:J

    move-wide v4, v12

    .line 1337285
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337286
    new-instance v0, LX/4cu;

    .line 1337287
    iget-object v2, p1, LX/8NS;->g:Ljava/lang/String;

    move-object v2, v2

    .line 1337288
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1337289
    iget-wide v12, p1, LX/8NS;->c:J

    move-wide v4, v12

    .line 1337290
    iget-wide v12, p1, LX/8NS;->d:J

    move-wide v6, v12

    .line 1337291
    invoke-direct/range {v0 .. v7}, LX/4cu;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JJ)V

    goto/16 :goto_3

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_7
    iget-object v2, p1, LX/8NS;->m:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1337161
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1337162
    new-instance v1, LX/8NT;

    const-string v2, "start_offset"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->D()J

    move-result-wide v2

    const-string v4, "end_offset"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->D()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, LX/8NT;-><init>(JJ)V

    return-object v1
.end method
