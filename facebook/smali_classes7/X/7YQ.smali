.class public LX/7YQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6YV;


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/0yN;

.field private d:LX/0yH;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1219856
    sget-object v0, LX/0yY;->DIALTONE_PHOTO:LX/0yY;

    sget-object v1, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/7YQ;->a:LX/0Rf;

    .line 1219857
    sget-object v0, LX/0yY;->DIALTONE_PHOTO_INTERSTITIAL:LX/0yY;

    sget-object v1, LX/0yY;->DIALTONE_VIDEO_INTERSTITIAL:LX/0yY;

    sget-object v2, LX/0yY;->DIALTONE_TOGGLE_INTERSTITIAL:LX/0yY;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/7YQ;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0yN;LX/0yH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1219850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1219851
    iput-object p1, p0, LX/7YQ;->c:LX/0yN;

    .line 1219852
    iput-object p2, p0, LX/7YQ;->d:LX/0yH;

    .line 1219853
    return-void
.end method

.method public static b(LX/0QB;)LX/7YQ;
    .locals 3

    .prologue
    .line 1219854
    new-instance v2, LX/7YQ;

    invoke-static {p0}, LX/0yM;->a(LX/0QB;)LX/0yM;

    move-result-object v0

    check-cast v0, LX/0yN;

    invoke-static {p0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v1

    check-cast v1, LX/0yH;

    invoke-direct {v2, v0, v1}, LX/7YQ;-><init>(LX/0yN;LX/0yH;)V

    .line 1219855
    return-object v2
.end method


# virtual methods
.method public final a(LX/0yY;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1219858
    iget-object v0, p0, LX/7YQ;->c:LX/0yN;

    invoke-interface {v0}, LX/0yN;->a()LX/1p2;

    move-result-object v0

    .line 1219859
    sget-object v1, LX/7YQ;->b:LX/0Rf;

    invoke-virtual {v1, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1219860
    sget-object v1, LX/7YQ;->b:LX/0Rf;

    invoke-virtual {v1}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v2

    move-object v1, v0

    .line 1219861
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1219862
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yY;

    invoke-static {v0}, LX/0df;->a(LX/0yY;)LX/0Tn;

    move-result-object v0

    invoke-virtual {v0}, LX/0Tn;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, v3}, LX/1p2;->a(Ljava/lang/String;Z)LX/1p2;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 1219863
    :cond_0
    invoke-static {p1}, LX/0df;->a(LX/0yY;)LX/0Tn;

    move-result-object v1

    invoke-virtual {v1}, LX/0Tn;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, LX/1p2;->a(Ljava/lang/String;Z)LX/1p2;

    move-result-object v1

    .line 1219864
    :cond_1
    invoke-interface {v1}, LX/1p2;->a()V

    .line 1219865
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1219848
    iget-object v0, p0, LX/7YQ;->d:LX/0yH;

    sget-object v1, LX/0yY;->UPSELL_DONT_WARN_AGAIN_CHECKBOX_CHECKED:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/0yY;)Z
    .locals 2

    .prologue
    .line 1219849
    iget-object v0, p0, LX/7YQ;->d:LX/0yH;

    sget-object v1, LX/0yY;->UPSELL_DONT_WARN_AGAIN:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/7YQ;->a:LX/0Rf;

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
