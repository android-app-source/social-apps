.class public final LX/6rQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1152208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;)LX/6rQ;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1152209
    new-instance v0, LX/6rQ;

    invoke-direct {v0}, LX/6rQ;-><init>()V

    .line 1152210
    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->a:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1152211
    iput-object v1, v0, LX/6rQ;->a:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1152212
    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->b:LX/0Px;

    .line 1152213
    iput-object v1, v0, LX/6rQ;->b:LX/0Px;

    .line 1152214
    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->c:LX/0Px;

    .line 1152215
    iput-object v1, v0, LX/6rQ;->c:LX/0Px;

    .line 1152216
    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->d:LX/0Px;

    .line 1152217
    iput-object v1, v0, LX/6rQ;->d:LX/0Px;

    .line 1152218
    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->e:Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;

    .line 1152219
    iput-object v1, v0, LX/6rQ;->e:Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;

    .line 1152220
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;
    .locals 2

    .prologue
    .line 1152221
    new-instance v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    invoke-direct {v0, p0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;-><init>(LX/6rQ;)V

    return-object v0
.end method
