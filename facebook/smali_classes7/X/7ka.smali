.class public LX/7ka;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/7ka;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7kZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1232265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232266
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/7ka;->a:Ljava/util/List;

    .line 1232267
    return-void
.end method

.method public static a(LX/0QB;)LX/7ka;
    .locals 3

    .prologue
    .line 1232230
    sget-object v0, LX/7ka;->b:LX/7ka;

    if-nez v0, :cond_1

    .line 1232231
    const-class v1, LX/7ka;

    monitor-enter v1

    .line 1232232
    :try_start_0
    sget-object v0, LX/7ka;->b:LX/7ka;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1232233
    if-eqz v2, :cond_0

    .line 1232234
    :try_start_1
    new-instance v0, LX/7ka;

    invoke-direct {v0}, LX/7ka;-><init>()V

    .line 1232235
    move-object v0, v0

    .line 1232236
    sput-object v0, LX/7ka;->b:LX/7ka;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1232237
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1232238
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1232239
    :cond_1
    sget-object v0, LX/7ka;->b:LX/7ka;

    return-object v0

    .line 1232240
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1232241
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/7ja;)Z
    .locals 1

    .prologue
    .line 1232264
    invoke-interface {p0}, LX/7ja;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/7ja;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232268
    monitor-enter p0

    :try_start_0
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1232269
    iget-object v0, p0, LX/7ka;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7kZ;

    .line 1232270
    iget-object v3, v0, LX/7kZ;->b:LX/7ja;

    move-object v3, v3

    .line 1232271
    invoke-static {v3}, LX/7ka;->a(LX/7ja;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1232272
    iget-object v3, v0, LX/7kZ;->b:LX/7ja;

    move-object v0, v3

    .line 1232273
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1232274
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1232275
    :cond_1
    :try_start_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized a(LX/7ja;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1232261
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7ka;->a:Ljava/util/List;

    new-instance v1, LX/7kZ;

    invoke-direct {v1, p1, p2}, LX/7kZ;-><init>(LX/7ja;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1232262
    monitor-exit p0

    return-void

    .line 1232263
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1232252
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 1232253
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1232254
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/7ka;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1232255
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1232256
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7kZ;

    .line 1232257
    iget-object v2, v0, LX/7kZ;->a:Ljava/lang/String;

    move-object v0, v2

    .line 1232258
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1232259
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1232260
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/0P1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/7ja;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232242
    monitor-enter p0

    :try_start_0
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    .line 1232243
    iget-object v0, p0, LX/7ka;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7kZ;

    .line 1232244
    iget-object v3, v0, LX/7kZ;->b:LX/7ja;

    move-object v3, v3

    .line 1232245
    invoke-static {v3}, LX/7ka;->a(LX/7ja;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1232246
    iget-object v3, v0, LX/7kZ;->b:LX/7ja;

    move-object v3, v3

    .line 1232247
    invoke-interface {v3}, LX/7ja;->e()Ljava/lang/String;

    move-result-object v3

    .line 1232248
    iget-object v4, v0, LX/7kZ;->b:LX/7ja;

    move-object v0, v4

    .line 1232249
    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1232250
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1232251
    :cond_1
    :try_start_1
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method
