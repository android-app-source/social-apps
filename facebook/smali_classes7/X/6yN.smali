.class public final LX/6yN;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:LX/6y8;

.field public final synthetic b:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;LX/6y8;)V
    .locals 0

    .prologue
    .line 1159893
    iput-object p1, p0, LX/6yN;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    iput-object p2, p0, LX/6yN;->a:LX/6y8;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 1159894
    iget-object v0, p0, LX/6yN;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    iget-object v1, p0, LX/6yN;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iget-object v2, p0, LX/6yN;->a:LX/6y8;

    .line 1159895
    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->g:LX/6y1;

    invoke-virtual {v3}, LX/6y1;->b()V

    .line 1159896
    sget-object v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->e:Ljava/lang/Class;

    const-string v4, "Card failed to update card"

    invoke-static {v3, v4, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1159897
    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->c:LX/03V;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    const-string v5, "Attempted to submit card form, but received a response with an error"

    invoke-virtual {v3, v4, v5, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1159898
    const-class v3, LX/2Oo;

    invoke-static {p1, v3}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v3

    check-cast v3, LX/2Oo;

    .line 1159899
    if-nez v3, :cond_0

    .line 1159900
    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->a:LX/0Zb;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->b(Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)LX/6xu;

    move-result-object v5

    invoke-interface {v5, v1}, LX/6xu;->d(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/6xt;

    move-result-object v4

    invoke-virtual {v2}, LX/6y8;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/6xt;->a(Ljava/lang/String;)LX/6xt;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/6xt;->b(Ljava/lang/String;)LX/6xt;

    move-result-object v4

    .line 1159901
    iget-object v5, v4, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    move-object v4, v5

    .line 1159902
    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1159903
    :goto_0
    return-void

    .line 1159904
    :cond_0
    invoke-virtual {v3}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1159905
    iget-object v4, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->a:LX/0Zb;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v5

    iget-object v5, v5, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v5, v5, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->b(Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)LX/6xu;

    move-result-object p0

    invoke-interface {p0, v1}, LX/6xu;->d(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v5, p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/6xt;

    move-result-object v5

    invoke-virtual {v2}, LX/6y8;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/6xt;->a(Ljava/lang/String;)LX/6xt;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/6xt;->b(Ljava/lang/String;)LX/6xt;

    move-result-object v3

    .line 1159906
    iget-object v5, v3, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    move-object v3, v5

    .line 1159907
    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1159908
    iget-object v0, p0, LX/6yN;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    iget-object v1, p0, LX/6yN;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iget-object v2, p0, LX/6yN;->a:LX/6y8;

    .line 1159909
    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->a:LX/0Zb;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object p0

    iget-object p0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object p0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->b(Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)LX/6xu;

    move-result-object p1

    invoke-interface {p1, v1}, LX/6xu;->c(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/6xt;

    move-result-object p0

    invoke-virtual {v2}, LX/6y8;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/6xt;->a(Ljava/lang/String;)LX/6xt;

    move-result-object p0

    .line 1159910
    iget-object p1, p0, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    move-object p0, p1

    .line 1159911
    invoke-interface {v3, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1159912
    return-void
.end method
