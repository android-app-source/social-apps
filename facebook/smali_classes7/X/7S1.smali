.class public LX/7S1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Z

.field public final c:Z

.field public final d:J


# direct methods
.method public constructor <init>(LX/376;)V
    .locals 2

    .prologue
    .line 1208019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1208020
    iget-object v0, p1, LX/376;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1208021
    iput-object v0, p0, LX/7S1;->a:Ljava/lang/String;

    .line 1208022
    iget-boolean v0, p1, LX/376;->e:Z

    move v0, v0

    .line 1208023
    iput-boolean v0, p0, LX/7S1;->b:Z

    .line 1208024
    iget-boolean v0, p1, LX/376;->f:Z

    move v0, v0

    .line 1208025
    iput-boolean v0, p0, LX/7S1;->c:Z

    .line 1208026
    invoke-virtual {p1}, LX/376;->f()J

    move-result-wide v0

    iput-wide v0, p0, LX/7S1;->d:J

    .line 1208027
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1208028
    if-ne p1, p0, :cond_1

    .line 1208029
    :cond_0
    :goto_0
    return v0

    .line 1208030
    :cond_1
    instance-of v2, p1, LX/7S1;

    if-nez v2, :cond_2

    move v0, v1

    .line 1208031
    goto :goto_0

    .line 1208032
    :cond_2
    check-cast p1, LX/7S1;

    .line 1208033
    iget-object v2, p0, LX/7S1;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, LX/7S1;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    :cond_3
    move v0, v1

    .line 1208034
    goto :goto_0

    .line 1208035
    :cond_4
    iget-object v2, p0, LX/7S1;->a:Ljava/lang/String;

    iget-object v3, p1, LX/7S1;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1208036
    :cond_5
    iget-boolean v2, p0, LX/7S1;->b:Z

    iget-boolean v3, p1, LX/7S1;->b:Z

    if-ne v2, v3, :cond_6

    iget-boolean v2, p0, LX/7S1;->c:Z

    iget-boolean v3, p1, LX/7S1;->c:Z

    if-ne v2, v3, :cond_6

    iget-wide v2, p0, LX/7S1;->d:J

    iget-wide v4, p1, LX/7S1;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1208037
    invoke-static {}, LX/1l6;->a()LX/1l6;

    move-result-object v0

    iget-object v1, p0, LX/7S1;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    move-result-object v0

    iget-boolean v1, p0, LX/7S1;->b:Z

    invoke-virtual {v0, v1}, LX/1l6;->a(Z)LX/1l6;

    move-result-object v0

    iget-boolean v1, p0, LX/7S1;->c:Z

    invoke-virtual {v0, v1}, LX/1l6;->a(Z)LX/1l6;

    move-result-object v0

    iget-wide v2, p0, LX/7S1;->d:J

    invoke-virtual {v0, v2, v3}, LX/1l6;->a(J)LX/1l6;

    move-result-object v0

    invoke-virtual {v0}, LX/1l6;->hashCode()I

    move-result v0

    return v0
.end method
