.class public final LX/7Br;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;

.field public static final p:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1179635
    new-instance v0, LX/0U1;

    const-string v1, "internal_id"

    const-string v2, "INTEGER PRIMARY KEY AUTOINCREMENT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->a:LX/0U1;

    .line 1179636
    new-instance v0, LX/0U1;

    const-string v1, "fbid"

    const-string v2, "TEXT UNIQUE"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->b:LX/0U1;

    .line 1179637
    new-instance v0, LX/0U1;

    const-string v1, "name"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->c:LX/0U1;

    .line 1179638
    new-instance v0, LX/0U1;

    const-string v1, "suggestion_text"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->d:LX/0U1;

    .line 1179639
    new-instance v0, LX/0U1;

    const-string v1, "category"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->e:LX/0U1;

    .line 1179640
    new-instance v0, LX/0U1;

    const-string v1, "subtext"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->f:LX/0U1;

    .line 1179641
    new-instance v0, LX/0U1;

    const-string v1, "profile_picture_uri"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->g:LX/0U1;

    .line 1179642
    new-instance v0, LX/0U1;

    const-string v1, "type"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->h:LX/0U1;

    .line 1179643
    new-instance v0, LX/0U1;

    const-string v1, "friendship_status"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->i:LX/0U1;

    .line 1179644
    new-instance v0, LX/0U1;

    const-string v1, "does_viewer_like"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->j:LX/0U1;

    .line 1179645
    new-instance v0, LX/0U1;

    const-string v1, "group_join_state"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->k:LX/0U1;

    .line 1179646
    new-instance v0, LX/0U1;

    const-string v1, "cost"

    const-string v2, "DOUBLE NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->l:LX/0U1;

    .line 1179647
    new-instance v0, LX/0U1;

    const-string v1, "is_verified"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->m:LX/0U1;

    .line 1179648
    new-instance v0, LX/0U1;

    const-string v1, "verification_status"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->n:LX/0U1;

    .line 1179649
    new-instance v0, LX/0U1;

    const-string v1, "redirection_url"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->o:LX/0U1;

    .line 1179650
    new-instance v0, LX/0U1;

    const-string v1, "is_multi_company_group"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7Br;->p:LX/0U1;

    return-void
.end method
