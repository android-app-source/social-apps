.class public final LX/7iC;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:LX/7iD;


# direct methods
.method public constructor <init>(LX/7iD;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1227283
    iput-object p1, p0, LX/7iC;->b:LX/7iD;

    iput-object p2, p0, LX/7iC;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1227284
    iget-object v0, p0, LX/7iC;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1227285
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1227278
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1227279
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1227280
    check-cast v0, Lcom/facebook/browserextensions/core/navigation/graphql/InstantExperiencesUrlQueryGraphQLModels$InstantExperienceUrlQueryModel;

    .line 1227281
    iget-object v1, p0, LX/7iC;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0}, Lcom/facebook/browserextensions/core/navigation/graphql/InstantExperiencesUrlQueryGraphQLModels$InstantExperienceUrlQueryModel;->a()Ljava/lang/String;

    move-result-object v0

    const v2, -0x63bd8f25

    invoke-static {v1, v0, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1227282
    return-void
.end method
