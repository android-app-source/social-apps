.class public LX/730;
.super LX/6sV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sV",
        "<",
        "Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;",
        "Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1165051
    const-class v0, LX/730;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/730;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/0Or;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/common/PaymentNetworkOperationHelper;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1165052
    const-class v0, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResult;

    invoke-direct {p0, p1, v0}, LX/6sV;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 1165053
    iput-object p2, p0, LX/730;->d:LX/0Or;

    .line 1165054
    return-void
.end method

.method public static b(LX/0QB;)LX/730;
    .locals 3

    .prologue
    .line 1165055
    new-instance v1, LX/730;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    const/16 v2, 0x12cb

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/730;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/0Or;)V

    .line 1165056
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1165057
    check-cast p1, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;

    .line 1165058
    iget-object v0, p0, LX/730;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1165059
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1165060
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "addressee"

    .line 1165061
    iget-object v3, p1, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    move-object v3, v3

    .line 1165062
    iget-object v3, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->a:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165063
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "label"

    .line 1165064
    iget-object v3, p1, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    move-object v3, v3

    .line 1165065
    iget-object v3, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->b:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165066
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "street"

    .line 1165067
    iget-object v3, p1, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    move-object v3, v3

    .line 1165068
    iget-object v3, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->c:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165069
    iget-object v0, p1, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    move-object v0, v0

    .line 1165070
    iget-object v0, v0, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1165071
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "building"

    .line 1165072
    iget-object v3, p1, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    move-object v3, v3

    .line 1165073
    iget-object v3, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->d:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165074
    :cond_0
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "city"

    .line 1165075
    iget-object v3, p1, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    move-object v3, v3

    .line 1165076
    iget-object v3, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->e:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165077
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "state"

    .line 1165078
    iget-object v3, p1, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    move-object v3, v3

    .line 1165079
    iget-object v3, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->f:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165080
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "postal_code"

    .line 1165081
    iget-object v3, p1, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    move-object v3, v3

    .line 1165082
    iget-object v3, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->g:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165083
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "country_code"

    .line 1165084
    iget-object v3, p1, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    move-object v3, v3

    .line 1165085
    iget-object v3, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->h:Lcom/facebook/common/locale/Country;

    invoke-virtual {v3}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165086
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "default"

    .line 1165087
    iget-object v0, p1, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    move-object v0, v0

    .line 1165088
    iget-boolean v0, v0, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->i:Z

    if-eqz v0, :cond_1

    const-string v0, "1"

    :goto_0
    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165089
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    sget-object v2, LX/730;->c:Ljava/lang/String;

    .line 1165090
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 1165091
    move-object v0, v0

    .line 1165092
    const-string v2, "POST"

    .line 1165093
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 1165094
    move-object v2, v0

    .line 1165095
    const-string v3, "%d/mailing_addresses"

    iget-object v0, p0, LX/730;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1165096
    iget-object v4, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v4

    .line 1165097
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1165098
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 1165099
    move-object v0, v2

    .line 1165100
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1165101
    move-object v0, v0

    .line 1165102
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 1165103
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1165104
    move-object v0, v0

    .line 1165105
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1165106
    :cond_1
    const-string v0, "0"

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1165107
    const-string v0, "add_mailing_address"

    return-object v0
.end method
