.class public LX/7QR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/7QR;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(IILjava/lang/String;)V
    .locals 0

    .prologue
    .line 1204204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204205
    iput p1, p0, LX/7QR;->a:I

    .line 1204206
    iput p2, p0, LX/7QR;->b:I

    .line 1204207
    iput-object p3, p0, LX/7QR;->c:Ljava/lang/String;

    .line 1204208
    return-void
.end method

.method public constructor <init>(LX/7QR;)V
    .locals 3

    .prologue
    .line 1204209
    iget v0, p1, LX/7QR;->a:I

    iget v1, p1, LX/7QR;->b:I

    iget-object v2, p1, LX/7QR;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, LX/7QR;-><init>(IILjava/lang/String;)V

    .line 1204210
    return-void
.end method


# virtual methods
.method public final compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 1204211
    check-cast p1, LX/7QR;

    .line 1204212
    iget v0, p0, LX/7QR;->a:I

    iget v1, p1, LX/7QR;->a:I

    sub-int/2addr v0, v1

    .line 1204213
    if-eqz v0, :cond_1

    .line 1204214
    :cond_0
    :goto_0
    return v0

    .line 1204215
    :cond_1
    iget v0, p0, LX/7QR;->b:I

    iget v1, p1, LX/7QR;->b:I

    sub-int/2addr v0, v1

    .line 1204216
    if-nez v0, :cond_0

    .line 1204217
    iget-object v0, p0, LX/7QR;->c:Ljava/lang/String;

    iget-object v1, p1, LX/7QR;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1204218
    instance-of v1, p1, LX/7QR;

    if-nez v1, :cond_1

    .line 1204219
    :cond_0
    :goto_0
    return v0

    .line 1204220
    :cond_1
    check-cast p1, LX/7QR;

    .line 1204221
    iget v1, p0, LX/7QR;->a:I

    iget v2, p1, LX/7QR;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/7QR;->b:I

    iget v2, p1, LX/7QR;->b:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/7QR;->c:Ljava/lang/String;

    iget-object v2, p1, LX/7QR;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1204222
    iget v0, p0, LX/7QR;->a:I

    add-int/lit16 v0, v0, 0x275

    .line 1204223
    mul-int/lit8 v0, v0, 0x25

    iget v1, p0, LX/7QR;->b:I

    add-int/2addr v0, v1

    .line 1204224
    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, LX/7QR;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1204225
    return v0
.end method
