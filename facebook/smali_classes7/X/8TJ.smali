.class public final enum LX/8TJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8TJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8TJ;

.field public static final enum IGNORE:LX/8TJ;

.field public static final enum REMOVE:LX/8TJ;

.field public static final enum UPDATE:LX/8TJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1348458
    new-instance v0, LX/8TJ;

    const-string v1, "UPDATE"

    invoke-direct {v0, v1, v2}, LX/8TJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8TJ;->UPDATE:LX/8TJ;

    .line 1348459
    new-instance v0, LX/8TJ;

    const-string v1, "IGNORE"

    invoke-direct {v0, v1, v3}, LX/8TJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8TJ;->IGNORE:LX/8TJ;

    .line 1348460
    new-instance v0, LX/8TJ;

    const-string v1, "REMOVE"

    invoke-direct {v0, v1, v4}, LX/8TJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8TJ;->REMOVE:LX/8TJ;

    .line 1348461
    const/4 v0, 0x3

    new-array v0, v0, [LX/8TJ;

    sget-object v1, LX/8TJ;->UPDATE:LX/8TJ;

    aput-object v1, v0, v2

    sget-object v1, LX/8TJ;->IGNORE:LX/8TJ;

    aput-object v1, v0, v3

    sget-object v1, LX/8TJ;->REMOVE:LX/8TJ;

    aput-object v1, v0, v4

    sput-object v0, LX/8TJ;->$VALUES:[LX/8TJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1348462
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8TJ;
    .locals 1

    .prologue
    .line 1348463
    const-class v0, LX/8TJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8TJ;

    return-object v0
.end method

.method public static values()[LX/8TJ;
    .locals 1

    .prologue
    .line 1348464
    sget-object v0, LX/8TJ;->$VALUES:[LX/8TJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8TJ;

    return-object v0
.end method
