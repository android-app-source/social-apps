.class public LX/8bZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;",
            "LX/8bX;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0hB;

.field private final c:I

.field public final d:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x1f4

    .line 1372166
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/8bZ;->a:Ljava/util/Map;

    .line 1372167
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const v1, 0x7f0d0124

    const/16 v2, 0x190

    invoke-static {v0, v1, v2}, LX/8bZ;->a(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;II)V

    .line 1372168
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->INSTAGRAM:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const v1, 0x7f0d0126

    const/16 v2, 0x21c

    invoke-static {v0, v1, v2}, LX/8bZ;->a(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;II)V

    .line 1372169
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->TWEET:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const v1, 0x7f0d0125

    invoke-static {v0, v1, v3}, LX/8bZ;->a(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;II)V

    .line 1372170
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->FACEBOOK:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const v1, 0x7f0d0129

    invoke-static {v0, v1, v3}, LX/8bZ;->a(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;II)V

    .line 1372171
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->VINE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const v1, 0x7f0d0129

    invoke-static {v0, v1, v3}, LX/8bZ;->a(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;II)V

    .line 1372172
    return-void
.end method

.method public constructor <init>(LX/0hB;LX/0Uh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1372161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1372162
    iput-object p1, p0, LX/8bZ;->b:LX/0hB;

    .line 1372163
    iput-object p2, p0, LX/8bZ;->d:LX/0Uh;

    .line 1372164
    const/high16 v0, 0x44400000    # 768.0f

    iget-object v1, p0, LX/8bZ;->b:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->b()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/8bZ;->c:I

    .line 1372165
    return-void
.end method

.method public static a(LX/0QB;)LX/8bZ;
    .locals 1

    .prologue
    .line 1372160
    invoke-static {p0}, LX/8bZ;->b(LX/0QB;)LX/8bZ;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;II)V
    .locals 2

    .prologue
    .line 1372158
    sget-object v0, LX/8bZ;->a:Ljava/util/Map;

    new-instance v1, LX/8bX;

    invoke-direct {v1, p1, p2}, LX/8bX;-><init>(II)V

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1372159
    return-void
.end method

.method public static b(LX/0QB;)LX/8bZ;
    .locals 3

    .prologue
    .line 1372156
    new-instance v2, LX/8bZ;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v0

    check-cast v0, LX/0hB;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-direct {v2, v0, v1}, LX/8bZ;-><init>(LX/0hB;LX/0Uh;)V

    .line 1372157
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;LX/8bY;)I
    .locals 2

    .prologue
    .line 1372151
    sget-object v0, LX/8bY;->PIXEL:LX/8bY;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, LX/8bZ;->b:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->b()F

    move-result v0

    move v1, v0

    .line 1372152
    :goto_0
    invoke-virtual {p0}, LX/8bZ;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/8bZ;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/8bZ;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bX;

    iget v0, v0, LX/8bX;->b:I

    .line 1372153
    :goto_1
    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0

    .line 1372154
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    move v1, v0

    goto :goto_0

    .line 1372155
    :cond_1
    const/16 v0, 0x300

    goto :goto_1
.end method

.method public final a()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1372145
    iget-object v0, p0, LX/8bZ;->b:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, LX/8bZ;->b:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->b()F

    move-result v2

    div-float v2, v0, v2

    .line 1372146
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_0

    iget-object v0, p0, LX/8bZ;->b:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->e()I

    move-result v0

    .line 1372147
    :goto_0
    int-to-float v0, v0

    iget-object v3, p0, LX/8bZ;->b:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->b()F

    move-result v3

    div-float/2addr v0, v3

    .line 1372148
    iget-object v3, p0, LX/8bZ;->d:LX/0Uh;

    const/16 v4, 0xc2

    invoke-virtual {v3, v4, v1}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v2, 0x44400000    # 768.0f

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 1372149
    :cond_0
    iget-object v0, p0, LX/8bZ;->b:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1372150
    goto :goto_1
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1372142
    invoke-virtual {p0}, LX/8bZ;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/8bZ;->d:LX/0Uh;

    const/16 v2, 0xc1

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1372144
    invoke-virtual {p0}, LX/8bZ;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/8bZ;->d:LX/0Uh;

    const/16 v2, 0xbe

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final f()Z
    .locals 3

    .prologue
    .line 1372143
    iget-object v0, p0, LX/8bZ;->d:LX/0Uh;

    const/16 v1, 0xbf

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
