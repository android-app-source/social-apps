.class public final LX/8KT;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V
    .locals 0

    .prologue
    .line 1330043
    iput-object p1, p0, LX/8KT;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;B)V
    .locals 0

    .prologue
    .line 1330044
    invoke-direct {p0, p1}, LX/8KT;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    return-void
.end method

.method private a(Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;)V
    .locals 2

    .prologue
    .line 1330045
    iget-object v0, p0, LX/8KT;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v0, v0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8KT;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v0, v0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    if-eqz v0, :cond_0

    .line 1330046
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 1330047
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v1

    .line 1330048
    iget-object v1, p0, LX/8KT;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v1, v1, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330049
    iget-object p1, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, p1

    .line 1330050
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1330051
    iget-object v0, p0, LX/8KT;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v0, v0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 1330052
    iget-object v0, p0, LX/8KT;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    invoke-static {v0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->l(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    .line 1330053
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1330054
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    return-object v0
.end method

.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 1330055
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    invoke-direct {p0, p1}, LX/8KT;->a(Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;)V

    return-void
.end method
