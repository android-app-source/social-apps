.class public LX/6wP;
.super LX/0QF;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0QF",
        "<",
        "LX/6vb;",
        "LX/0Px",
        "<+",
        "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Sh;

.field private final b:LX/0QJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QJ",
            "<",
            "LX/6vb;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0TD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1157603
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/6wP;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/6wT;LX/0Sh;LX/0TD;LX/0Or;)V
    .locals 4
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6wT;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0TD;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1157604
    invoke-direct {p0}, LX/0QF;-><init>()V

    .line 1157605
    iput-object p2, p0, LX/6wP;->a:LX/0Sh;

    .line 1157606
    iput-object p3, p0, LX/6wP;->c:LX/0TD;

    .line 1157607
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0xa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    new-instance v1, LX/6wN;

    invoke-direct {v1, p0, p1}, LX/6wN;-><init>(LX/6wP;LX/6wT;)V

    invoke-virtual {v0, v1}, LX/0QN;->a(LX/0QM;)LX/0QJ;

    move-result-object v0

    iput-object v0, p0, LX/6wP;->b:LX/0QJ;

    .line 1157608
    return-void
.end method

.method public static a(LX/0QB;)LX/6wP;
    .locals 10

    .prologue
    .line 1157609
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1157610
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1157611
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1157612
    if-nez v1, :cond_0

    .line 1157613
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1157614
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1157615
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1157616
    sget-object v1, LX/6wP;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1157617
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1157618
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1157619
    :cond_1
    if-nez v1, :cond_4

    .line 1157620
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1157621
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1157622
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1157623
    new-instance v9, LX/6wP;

    invoke-static {v0}, LX/6wT;->a(LX/0QB;)LX/6wT;

    move-result-object v1

    check-cast v1, LX/6wT;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, LX/0TD;

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v9, v1, v7, v8, p0}, LX/6wP;-><init>(LX/6wT;LX/0Sh;LX/0TD;LX/0Or;)V

    .line 1157624
    move-object v1, v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1157625
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1157626
    if-nez v1, :cond_2

    .line 1157627
    sget-object v0, LX/6wP;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6wP;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1157628
    :goto_1
    if-eqz v0, :cond_3

    .line 1157629
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1157630
    :goto_3
    check-cast v0, LX/6wP;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1157631
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1157632
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1157633
    :catchall_1
    move-exception v0

    .line 1157634
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1157635
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1157636
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1157637
    :cond_2
    :try_start_8
    sget-object v0, LX/6wP;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6wP;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/6vb;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6vb;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1157638
    invoke-virtual {p0, p1}, LX/0QG;->b(Ljava/lang/Object;)V

    .line 1157639
    invoke-virtual {p0, p1}, LX/6wP;->b(LX/6vb;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/6vb;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6vb;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1157640
    invoke-virtual {p0, p1}, LX/0QG;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1157641
    if-eqz v0, :cond_0

    .line 1157642
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1157643
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6wP;->c:LX/0TD;

    new-instance v1, LX/6wO;

    invoke-direct {v1, p0, p1}, LX/6wO;-><init>(LX/6wP;LX/6vb;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(LX/6vb;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6vb;",
            ")",
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1157644
    iget-object v0, p0, LX/6wP;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1157645
    invoke-super {p0, p1}, LX/0QF;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1157646
    check-cast p1, LX/6vb;

    invoke-virtual {p0, p1}, LX/6wP;->c(LX/6vb;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()LX/0QI;
    .locals 1

    .prologue
    .line 1157647
    invoke-virtual {p0}, LX/6wP;->f()LX/0QJ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1157648
    invoke-virtual {p0}, LX/6wP;->f()LX/0QJ;

    move-result-object v0

    return-object v0
.end method

.method public final f()LX/0QJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QJ",
            "<",
            "LX/6vb;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1157649
    iget-object v0, p0, LX/6wP;->b:LX/0QJ;

    return-object v0
.end method
