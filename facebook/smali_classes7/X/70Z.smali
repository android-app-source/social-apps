.class public LX/70Z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/70S;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/70U;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/70M;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/70S;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/70U;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/70M;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1162426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1162427
    iput-object p1, p0, LX/70Z;->a:Ljava/util/Set;

    .line 1162428
    iput-object p2, p0, LX/70Z;->b:Ljava/util/Set;

    .line 1162429
    iput-object p3, p0, LX/70Z;->c:Ljava/util/Set;

    .line 1162430
    return-void
.end method

.method public static a(LX/0QB;)LX/70Z;
    .locals 8

    .prologue
    .line 1162431
    const-class v1, LX/70Z;

    monitor-enter v1

    .line 1162432
    :try_start_0
    sget-object v0, LX/70Z;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1162433
    sput-object v2, LX/70Z;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1162434
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1162435
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1162436
    new-instance v3, LX/70Z;

    .line 1162437
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance v6, LX/70c;

    invoke-direct {v6, v0}, LX/70c;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, v6}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 1162438
    new-instance v5, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    new-instance v7, LX/70b;

    invoke-direct {v7, v0}, LX/70b;-><init>(LX/0QB;)V

    invoke-direct {v5, v6, v7}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v5, v5

    .line 1162439
    new-instance v6, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    new-instance p0, LX/70a;

    invoke-direct {p0, v0}, LX/70a;-><init>(LX/0QB;)V

    invoke-direct {v6, v7, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v6

    .line 1162440
    invoke-direct {v3, v4, v5, v6}, LX/70Z;-><init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    .line 1162441
    move-object v0, v3

    .line 1162442
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1162443
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/70Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1162444
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1162445
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/6zU;)LX/70S;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1162446
    iget-object v0, p0, LX/70Z;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/70S;

    .line 1162447
    invoke-interface {v0}, LX/70S;->a()LX/6zU;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 1162448
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
