.class public LX/8I0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:Landroid/content/Context;

.field public final c:LX/17Y;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17Y;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1321962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321963
    const-string v0, "com.facebook.photos.photogallery."

    iput-object v0, p0, LX/8I0;->a:Ljava/lang/String;

    .line 1321964
    iput-object p1, p0, LX/8I0;->b:Landroid/content/Context;

    .line 1321965
    iput-object p2, p0, LX/8I0;->c:LX/17Y;

    .line 1321966
    return-void
.end method

.method public static a(LX/0QB;)LX/8I0;
    .locals 1

    .prologue
    .line 1321967
    invoke-static {p0}, LX/8I0;->b(LX/0QB;)LX/8I0;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8I0;
    .locals 3

    .prologue
    .line 1321968
    new-instance v2, LX/8I0;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v1

    check-cast v1, LX/17Y;

    invoke-direct {v2, v0, v1}, LX/8I0;-><init>(Landroid/content/Context;LX/17Y;)V

    .line 1321969
    return-object v2
.end method


# virtual methods
.method public final a(J)Landroid/content/Intent;
    .locals 7

    .prologue
    .line 1321970
    iget-object v0, p0, LX/8I0;->c:LX/17Y;

    iget-object v1, p0, LX/8I0;->b:Landroid/content/Context;

    sget-object v2, LX/0ax;->aE:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1321971
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1321972
    iget-object v0, p0, LX/8I0;->c:LX/17Y;

    sget-object v1, LX/0ax;->bs:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1321973
    return-object v0

    .line 1321974
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
