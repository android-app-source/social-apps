.class public LX/7X1;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1216903
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1216904
    iput-object p1, p0, LX/7X1;->a:Landroid/content/Context;

    .line 1216905
    new-instance v0, LX/7X0;

    invoke-direct {v0, p0}, LX/7X0;-><init>(LX/7X1;)V

    invoke-virtual {p0, v0}, LX/7X1;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1216906
    const v0, 0x7f080ea1

    invoke-virtual {p0, v0}, LX/7X1;->setTitle(I)V

    .line 1216907
    return-void
.end method

.method public static b(LX/0QB;)LX/7X1;
    .locals 2

    .prologue
    .line 1216908
    new-instance v1, LX/7X1;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/7X1;-><init>(Landroid/content/Context;)V

    .line 1216909
    return-object v1
.end method
