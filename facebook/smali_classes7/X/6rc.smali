.class public final enum LX/6rc;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6LU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6rc;",
        ">;",
        "LX/6LU",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6rc;

.field public static final enum CONTACT_EMAIL:LX/6rc;

.field public static final enum CONTACT_INFO:LX/6rc;

.field public static final enum CONTACT_NAME:LX/6rc;

.field public static final enum CONTACT_PHONE:LX/6rc;

.field public static final enum NOTES:LX/6rc;

.field public static final enum OPTIONS:LX/6rc;

.field public static final enum PAYMENT_METHOD:LX/6rc;

.field public static final enum PIN_AND_FINGERPRINT:LX/6rc;

.field public static final enum PRICE_SELECTOR:LX/6rc;

.field public static final enum SHIPPING_ADDRESS:LX/6rc;


# instance fields
.field public final purchaseInfo:LX/6rp;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1152465
    new-instance v0, LX/6rc;

    const-string v1, "CONTACT_EMAIL"

    sget-object v2, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-direct {v0, v1, v4, v2}, LX/6rc;-><init>(Ljava/lang/String;ILX/6rp;)V

    sput-object v0, LX/6rc;->CONTACT_EMAIL:LX/6rc;

    .line 1152466
    new-instance v0, LX/6rc;

    const-string v1, "CONTACT_INFO"

    sget-object v2, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-direct {v0, v1, v5, v2}, LX/6rc;-><init>(Ljava/lang/String;ILX/6rp;)V

    sput-object v0, LX/6rc;->CONTACT_INFO:LX/6rc;

    .line 1152467
    new-instance v0, LX/6rc;

    const-string v1, "CONTACT_NAME"

    sget-object v2, LX/6rp;->CONTACT_NAME:LX/6rp;

    invoke-direct {v0, v1, v6, v2}, LX/6rc;-><init>(Ljava/lang/String;ILX/6rp;)V

    sput-object v0, LX/6rc;->CONTACT_NAME:LX/6rc;

    .line 1152468
    new-instance v0, LX/6rc;

    const-string v1, "CONTACT_PHONE"

    sget-object v2, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-direct {v0, v1, v7, v2}, LX/6rc;-><init>(Ljava/lang/String;ILX/6rp;)V

    sput-object v0, LX/6rc;->CONTACT_PHONE:LX/6rc;

    .line 1152469
    new-instance v0, LX/6rc;

    const-string v1, "NOTES"

    sget-object v2, LX/6rp;->NOTE:LX/6rp;

    invoke-direct {v0, v1, v8, v2}, LX/6rc;-><init>(Ljava/lang/String;ILX/6rp;)V

    sput-object v0, LX/6rc;->NOTES:LX/6rc;

    .line 1152470
    new-instance v0, LX/6rc;

    const-string v1, "OPTIONS"

    const/4 v2, 0x5

    sget-object v3, LX/6rp;->CHECKOUT_OPTIONS:LX/6rp;

    invoke-direct {v0, v1, v2, v3}, LX/6rc;-><init>(Ljava/lang/String;ILX/6rp;)V

    sput-object v0, LX/6rc;->OPTIONS:LX/6rc;

    .line 1152471
    new-instance v0, LX/6rc;

    const-string v1, "PAYMENT_METHOD"

    const/4 v2, 0x6

    sget-object v3, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    invoke-direct {v0, v1, v2, v3}, LX/6rc;-><init>(Ljava/lang/String;ILX/6rp;)V

    sput-object v0, LX/6rc;->PAYMENT_METHOD:LX/6rc;

    .line 1152472
    new-instance v0, LX/6rc;

    const-string v1, "PIN_AND_FINGERPRINT"

    const/4 v2, 0x7

    sget-object v3, LX/6rp;->AUTHENTICATION:LX/6rp;

    invoke-direct {v0, v1, v2, v3}, LX/6rc;-><init>(Ljava/lang/String;ILX/6rp;)V

    sput-object v0, LX/6rc;->PIN_AND_FINGERPRINT:LX/6rc;

    .line 1152473
    new-instance v0, LX/6rc;

    const-string v1, "PRICE_SELECTOR"

    const/16 v2, 0x8

    sget-object v3, LX/6rp;->PRICE_SELECTOR:LX/6rp;

    invoke-direct {v0, v1, v2, v3}, LX/6rc;-><init>(Ljava/lang/String;ILX/6rp;)V

    sput-object v0, LX/6rc;->PRICE_SELECTOR:LX/6rc;

    .line 1152474
    new-instance v0, LX/6rc;

    const-string v1, "SHIPPING_ADDRESS"

    const/16 v2, 0x9

    sget-object v3, LX/6rp;->MAILING_ADDRESS:LX/6rp;

    invoke-direct {v0, v1, v2, v3}, LX/6rc;-><init>(Ljava/lang/String;ILX/6rp;)V

    sput-object v0, LX/6rc;->SHIPPING_ADDRESS:LX/6rc;

    .line 1152475
    const/16 v0, 0xa

    new-array v0, v0, [LX/6rc;

    sget-object v1, LX/6rc;->CONTACT_EMAIL:LX/6rc;

    aput-object v1, v0, v4

    sget-object v1, LX/6rc;->CONTACT_INFO:LX/6rc;

    aput-object v1, v0, v5

    sget-object v1, LX/6rc;->CONTACT_NAME:LX/6rc;

    aput-object v1, v0, v6

    sget-object v1, LX/6rc;->CONTACT_PHONE:LX/6rc;

    aput-object v1, v0, v7

    sget-object v1, LX/6rc;->NOTES:LX/6rc;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/6rc;->OPTIONS:LX/6rc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6rc;->PAYMENT_METHOD:LX/6rc;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6rc;->PIN_AND_FINGERPRINT:LX/6rc;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6rc;->PRICE_SELECTOR:LX/6rc;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6rc;->SHIPPING_ADDRESS:LX/6rc;

    aput-object v2, v0, v1

    sput-object v0, LX/6rc;->$VALUES:[LX/6rc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/6rp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6rp;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1152476
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1152477
    iput-object p3, p0, LX/6rc;->purchaseInfo:LX/6rp;

    .line 1152478
    return-void
.end method

.method public static forValue(Ljava/lang/String;)LX/6rc;
    .locals 1

    .prologue
    .line 1152479
    invoke-static {}, LX/6rc;->values()[LX/6rc;

    move-result-object v0

    invoke-static {v0, p0}, LX/6LV;->a([LX/6LU;Ljava/lang/Object;)LX/6LU;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6rc;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6rc;
    .locals 1

    .prologue
    .line 1152480
    const-class v0, LX/6rc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6rc;

    return-object v0
.end method

.method public static values()[LX/6rc;
    .locals 1

    .prologue
    .line 1152481
    sget-object v0, LX/6rc;->$VALUES:[LX/6rc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6rc;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1152482
    invoke-virtual {p0}, LX/6rc;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1152483
    invoke-virtual {p0}, LX/6rc;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1152484
    invoke-virtual {p0}, LX/6rc;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
