.class public final LX/7kB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1231140
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1231141
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1231142
    :goto_0
    return v1

    .line 1231143
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1231144
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1231145
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1231146
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1231147
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1231148
    const-string v4, "commerce_merchant_settings"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1231149
    const/4 v3, 0x0

    .line 1231150
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 1231151
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1231152
    :goto_2
    move v2, v3

    .line 1231153
    goto :goto_1

    .line 1231154
    :cond_2
    const-string v4, "ordered_collections"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1231155
    invoke-static {p0, p1}, LX/7jn;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1231156
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1231157
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1231158
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1231159
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1231160
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1231161
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1231162
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1231163
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1231164
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1231165
    const-string v5, "default_currency"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1231166
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_3

    .line 1231167
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1231168
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1231169
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v2, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1231170
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1231171
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1231172
    if-eqz v0, :cond_1

    .line 1231173
    const-string v1, "commerce_merchant_settings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1231174
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1231175
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1231176
    if-eqz v1, :cond_0

    .line 1231177
    const-string v2, "default_currency"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1231178
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1231179
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1231180
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1231181
    if-eqz v0, :cond_2

    .line 1231182
    const-string v1, "ordered_collections"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1231183
    invoke-static {p0, v0, p2, p3}, LX/7jn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1231184
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1231185
    return-void
.end method
