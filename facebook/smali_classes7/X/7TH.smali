.class public LX/7TH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/io/File;

.field public final b:Ljava/io/File;

.field public final c:LX/2Md;

.field public final d:Landroid/graphics/RectF;

.field public final e:LX/7Sv;

.field public final f:Z

.field public final g:Z

.field public final h:Z

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:LX/60y;

.field public final m:LX/7Sy;

.field public final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7TI;)V
    .locals 1

    .prologue
    .line 1210273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1210274
    iget-object v0, p1, LX/7TI;->a:Ljava/io/File;

    move-object v0, v0

    .line 1210275
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, LX/7TH;->a:Ljava/io/File;

    .line 1210276
    iget-object v0, p1, LX/7TI;->b:Ljava/io/File;

    move-object v0, v0

    .line 1210277
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, LX/7TH;->b:Ljava/io/File;

    .line 1210278
    iget-object v0, p1, LX/7TI;->c:LX/2Md;

    move-object v0, v0

    .line 1210279
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Md;

    iput-object v0, p0, LX/7TH;->c:LX/2Md;

    .line 1210280
    iget-object v0, p1, LX/7TI;->d:Landroid/graphics/RectF;

    move-object v0, v0

    .line 1210281
    iput-object v0, p0, LX/7TH;->d:Landroid/graphics/RectF;

    .line 1210282
    iget-object v0, p1, LX/7TI;->e:LX/7Sv;

    move-object v0, v0

    .line 1210283
    iput-object v0, p0, LX/7TH;->e:LX/7Sv;

    .line 1210284
    iget v0, p1, LX/7TI;->f:I

    move v0, v0

    .line 1210285
    iput v0, p0, LX/7TH;->i:I

    .line 1210286
    iget v0, p1, LX/7TI;->g:I

    move v0, v0

    .line 1210287
    iput v0, p0, LX/7TH;->j:I

    .line 1210288
    iget-object v0, p1, LX/7TI;->h:LX/60y;

    move-object v0, v0

    .line 1210289
    iput-object v0, p0, LX/7TH;->l:LX/60y;

    .line 1210290
    iget-boolean v0, p1, LX/7TI;->i:Z

    move v0, v0

    .line 1210291
    iput-boolean v0, p0, LX/7TH;->f:Z

    .line 1210292
    iget-boolean v0, p1, LX/7TI;->j:Z

    move v0, v0

    .line 1210293
    iput-boolean v0, p0, LX/7TH;->g:Z

    .line 1210294
    iget-boolean v0, p1, LX/7TI;->k:Z

    move v0, v0

    .line 1210295
    iput-boolean v0, p0, LX/7TH;->h:Z

    .line 1210296
    iget-object v0, p1, LX/7TI;->l:LX/7Sy;

    move-object v0, v0

    .line 1210297
    iput-object v0, p0, LX/7TH;->m:LX/7Sy;

    .line 1210298
    iget v0, p1, LX/7TI;->m:I

    move v0, v0

    .line 1210299
    iput v0, p0, LX/7TH;->k:I

    .line 1210300
    iget-object v0, p1, LX/7TI;->n:LX/0Px;

    move-object v0, v0

    .line 1210301
    iput-object v0, p0, LX/7TH;->n:Ljava/util/List;

    .line 1210302
    return-void
.end method

.method public static newBuilder()LX/7TI;
    .locals 1

    .prologue
    .line 1210303
    new-instance v0, LX/7TI;

    invoke-direct {v0}, LX/7TI;-><init>()V

    return-object v0
.end method
