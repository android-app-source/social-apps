.class public final enum LX/77S;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/77S;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/77S;

.field public static final enum COUNT_AT_LEAST:LX/77S;

.field public static final enum COUNT_AT_MOST:LX/77S;

.field public static final enum SECONDS_SINCE_GREATER_THAN:LX/77S;

.field public static final enum SECONDS_SINCE_LESS_THAN:LX/77S;

.field public static final enum UNKNOWN:LX/77S;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1171834
    new-instance v0, LX/77S;

    const-string v1, "SECONDS_SINCE_GREATER_THAN"

    invoke-direct {v0, v1, v2}, LX/77S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77S;->SECONDS_SINCE_GREATER_THAN:LX/77S;

    .line 1171835
    new-instance v0, LX/77S;

    const-string v1, "SECONDS_SINCE_LESS_THAN"

    invoke-direct {v0, v1, v3}, LX/77S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77S;->SECONDS_SINCE_LESS_THAN:LX/77S;

    .line 1171836
    new-instance v0, LX/77S;

    const-string v1, "COUNT_AT_LEAST"

    invoke-direct {v0, v1, v4}, LX/77S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77S;->COUNT_AT_LEAST:LX/77S;

    .line 1171837
    new-instance v0, LX/77S;

    const-string v1, "COUNT_AT_MOST"

    invoke-direct {v0, v1, v5}, LX/77S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77S;->COUNT_AT_MOST:LX/77S;

    .line 1171838
    new-instance v0, LX/77S;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, LX/77S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77S;->UNKNOWN:LX/77S;

    .line 1171839
    const/4 v0, 0x5

    new-array v0, v0, [LX/77S;

    sget-object v1, LX/77S;->SECONDS_SINCE_GREATER_THAN:LX/77S;

    aput-object v1, v0, v2

    sget-object v1, LX/77S;->SECONDS_SINCE_LESS_THAN:LX/77S;

    aput-object v1, v0, v3

    sget-object v1, LX/77S;->COUNT_AT_LEAST:LX/77S;

    aput-object v1, v0, v4

    sget-object v1, LX/77S;->COUNT_AT_MOST:LX/77S;

    aput-object v1, v0, v5

    sget-object v1, LX/77S;->UNKNOWN:LX/77S;

    aput-object v1, v0, v6

    sput-object v0, LX/77S;->$VALUES:[LX/77S;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1171833
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/77S;
    .locals 1

    .prologue
    .line 1171840
    if-nez p0, :cond_0

    .line 1171841
    :try_start_0
    sget-object v0, LX/77S;->UNKNOWN:LX/77S;

    .line 1171842
    :goto_0
    return-object v0

    .line 1171843
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/77S;->valueOf(Ljava/lang/String;)LX/77S;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1171844
    :catch_0
    sget-object v0, LX/77S;->UNKNOWN:LX/77S;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/77S;
    .locals 1

    .prologue
    .line 1171831
    const-class v0, LX/77S;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/77S;

    return-object v0
.end method

.method public static values()[LX/77S;
    .locals 1

    .prologue
    .line 1171832
    sget-object v0, LX/77S;->$VALUES:[LX/77S;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/77S;

    return-object v0
.end method
