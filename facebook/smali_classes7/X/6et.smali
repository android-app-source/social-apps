.class public final enum LX/6et;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6et;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6et;

.field public static final enum TURN_OFF_JOINABLE_BY_OWNER:LX/6et;

.field public static final enum TURN_OFF_ON_LAST_OWNER_REMOVED:LX/6et;

.field public static final enum TURN_OFF_ON_LAST_OWNER_UNSUBSCRIBED:LX/6et;

.field public static final enum TURN_ON_JOINABLE_BY_OWNER:LX/6et;

.field public static final enum TURN_ON_JOINABLE_BY_USER:LX/6et;

.field public static final enum UNKNOWN:LX/6et;


# instance fields
.field private final serverEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1118304
    new-instance v0, LX/6et;

    const-string v1, "TURN_ON_JOINABLE_BY_OWNER"

    const-string v2, "turn_on_joinable_by_owner"

    invoke-direct {v0, v1, v4, v2}, LX/6et;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6et;->TURN_ON_JOINABLE_BY_OWNER:LX/6et;

    .line 1118305
    new-instance v0, LX/6et;

    const-string v1, "TURN_ON_JOINABLE_BY_USER"

    const-string v2, "turn_on_joinable_by_user"

    invoke-direct {v0, v1, v5, v2}, LX/6et;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6et;->TURN_ON_JOINABLE_BY_USER:LX/6et;

    .line 1118306
    new-instance v0, LX/6et;

    const-string v1, "TURN_OFF_JOINABLE_BY_OWNER"

    const-string v2, "turn_off_joinable_by_owner"

    invoke-direct {v0, v1, v6, v2}, LX/6et;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6et;->TURN_OFF_JOINABLE_BY_OWNER:LX/6et;

    .line 1118307
    new-instance v0, LX/6et;

    const-string v1, "TURN_OFF_ON_LAST_OWNER_UNSUBSCRIBED"

    const-string v2, "turn_off_on_last_owner_unsubscribed"

    invoke-direct {v0, v1, v7, v2}, LX/6et;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6et;->TURN_OFF_ON_LAST_OWNER_UNSUBSCRIBED:LX/6et;

    .line 1118308
    new-instance v0, LX/6et;

    const-string v1, "TURN_OFF_ON_LAST_OWNER_REMOVED"

    const-string v2, "turn_off_on_last_owner_removed"

    invoke-direct {v0, v1, v8, v2}, LX/6et;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6et;->TURN_OFF_ON_LAST_OWNER_REMOVED:LX/6et;

    .line 1118309
    new-instance v0, LX/6et;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/6et;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6et;->UNKNOWN:LX/6et;

    .line 1118310
    const/4 v0, 0x6

    new-array v0, v0, [LX/6et;

    sget-object v1, LX/6et;->TURN_ON_JOINABLE_BY_OWNER:LX/6et;

    aput-object v1, v0, v4

    sget-object v1, LX/6et;->TURN_ON_JOINABLE_BY_USER:LX/6et;

    aput-object v1, v0, v5

    sget-object v1, LX/6et;->TURN_OFF_JOINABLE_BY_OWNER:LX/6et;

    aput-object v1, v0, v6

    sget-object v1, LX/6et;->TURN_OFF_ON_LAST_OWNER_UNSUBSCRIBED:LX/6et;

    aput-object v1, v0, v7

    sget-object v1, LX/6et;->TURN_OFF_ON_LAST_OWNER_REMOVED:LX/6et;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/6et;->UNKNOWN:LX/6et;

    aput-object v2, v0, v1

    sput-object v0, LX/6et;->$VALUES:[LX/6et;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1118301
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1118302
    iput-object p3, p0, LX/6et;->serverEventName:Ljava/lang/String;

    .line 1118303
    return-void
.end method

.method public static fromValue(Ljava/lang/String;)LX/6et;
    .locals 5
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1118293
    invoke-static {}, LX/6et;->values()[LX/6et;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1118294
    iget-object v4, v0, LX/6et;->serverEventName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1118295
    :goto_1
    return-object v0

    .line 1118296
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1118297
    :cond_1
    sget-object v0, LX/6et;->UNKNOWN:LX/6et;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/6et;
    .locals 1

    .prologue
    .line 1118300
    const-class v0, LX/6et;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6et;

    return-object v0
.end method

.method public static values()[LX/6et;
    .locals 1

    .prologue
    .line 1118299
    sget-object v0, LX/6et;->$VALUES:[LX/6et;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6et;

    return-object v0
.end method


# virtual methods
.method public final toDbValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1118298
    iget-object v0, p0, LX/6et;->serverEventName:Ljava/lang/String;

    return-object v0
.end method
