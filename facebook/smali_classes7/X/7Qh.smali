.class public LX/7Qh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Qf;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0

    .prologue
    .line 1204748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204749
    iput-object p1, p0, LX/7Qh;->a:Ljava/lang/String;

    .line 1204750
    iput p2, p0, LX/7Qh;->b:I

    .line 1204751
    iput p3, p0, LX/7Qh;->c:I

    .line 1204752
    iput-object p4, p0, LX/7Qh;->d:Ljava/lang/String;

    .line 1204753
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 1204743
    sget-object v0, LX/0JS;->UNIT_POSITION:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget v1, p0, LX/7Qh;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204744
    sget-object v0, LX/0JS;->POSITION_IN_UNIT:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget v1, p0, LX/7Qh;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204745
    sget-object v0, LX/0JS;->REACTION_UNIT_TYPE:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget-object v1, p0, LX/7Qh;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204746
    sget-object v0, LX/0JS;->REACTION_COMPONENT_TRACKING_FIELD:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget-object v1, p0, LX/7Qh;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204747
    return-void
.end method
