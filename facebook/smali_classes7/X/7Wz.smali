.class public LX/7Wz;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public b:LX/7Y7;

.field public c:LX/7YP;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/7Y7;LX/7YP;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1216884
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1216885
    iput-object p1, p0, LX/7Wz;->a:Landroid/content/Context;

    .line 1216886
    iput-object p2, p0, LX/7Wz;->b:LX/7Y7;

    .line 1216887
    iput-object p3, p0, LX/7Wz;->c:LX/7YP;

    .line 1216888
    new-instance v0, LX/7Wv;

    invoke-direct {v0, p0}, LX/7Wv;-><init>(LX/7Wz;)V

    invoke-virtual {p0, v0}, LX/7Wz;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1216889
    const v0, 0x7f080ea0

    invoke-virtual {p0, v0}, LX/7Wz;->setTitle(I)V

    .line 1216890
    return-void
.end method

.method public static a$redex0(LX/7Wz;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1216891
    new-instance v0, LX/31Y;

    iget-object v1, p0, LX/7Wz;->a:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/31Y;-><init>(Landroid/content/Context;I)V

    .line 1216892
    const-string v1, "Upsell API Error"

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1216893
    invoke-virtual {v0, p1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1216894
    const-string v1, "OK"

    new-instance v2, LX/7Wy;

    invoke-direct {v2, p0}, LX/7Wy;-><init>(LX/7Wz;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1216895
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 1216896
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1216897
    return-void
.end method

.method public static b(LX/0QB;)LX/7Wz;
    .locals 4

    .prologue
    .line 1216898
    new-instance v3, LX/7Wz;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/7Y7;->a(LX/0QB;)LX/7Y7;

    move-result-object v1

    check-cast v1, LX/7Y7;

    invoke-static {p0}, LX/7YP;->a(LX/0QB;)LX/7YP;

    move-result-object v2

    check-cast v2, LX/7YP;

    invoke-direct {v3, v0, v1, v2}, LX/7Wz;-><init>(Landroid/content/Context;LX/7Y7;LX/7YP;)V

    .line 1216899
    return-object v3
.end method
