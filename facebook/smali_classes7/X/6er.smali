.class public final LX/6er;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1118221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;
    .locals 2

    .prologue
    .line 1118224
    new-instance v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1118225
    invoke-static {p1}, LX/6er;->a(Landroid/os/Parcel;)Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1118222
    new-array v0, p1, [Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;

    move-object v0, v0

    .line 1118223
    return-object v0
.end method
