.class public final LX/70o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/picker/PickerScreenFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/picker/PickerScreenFragment;)V
    .locals 0

    .prologue
    .line 1162770
    iput-object p1, p0, LX/70o;->a:Lcom/facebook/payments/picker/PickerScreenFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 3

    .prologue
    .line 1162771
    add-int v0, p2, p3

    if-lt v0, p4, :cond_0

    if-eqz p4, :cond_0

    .line 1162772
    iget-object v0, p0, LX/70o;->a:Lcom/facebook/payments/picker/PickerScreenFragment;

    iget-object v0, v0, Lcom/facebook/payments/picker/PickerScreenFragment;->r:LX/6w1;

    iget-object v1, p0, LX/70o;->a:Lcom/facebook/payments/picker/PickerScreenFragment;

    iget-object v1, v1, Lcom/facebook/payments/picker/PickerScreenFragment;->y:LX/6zj;

    iget-object v2, p0, LX/70o;->a:Lcom/facebook/payments/picker/PickerScreenFragment;

    iget-object v2, v2, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    invoke-interface {v0, v1, v2}, LX/6w1;->b(LX/6zj;Lcom/facebook/payments/picker/model/PickerRunTimeData;)V

    .line 1162773
    :cond_0
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 1162774
    return-void
.end method
