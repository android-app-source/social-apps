.class public LX/8Nm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3HM;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/3ib;

.field public final e:LX/8Ku;

.field public final f:Ljava/lang/String;

.field public final g:Z

.field public final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/0Ot;LX/3ib;Ljava/lang/String;ZLX/0Px;LX/8Ku;)V
    .locals 0
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/8Ku;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/3HM;",
            ">;",
            "LX/3ib;",
            "Ljava/lang/String;",
            "Z",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/photos/upload/serverprocessing/FeedVideoStatusChecker$Listener;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1337884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1337885
    iput-object p1, p0, LX/8Nm;->a:LX/0tX;

    .line 1337886
    iput-object p2, p0, LX/8Nm;->b:LX/1Ck;

    .line 1337887
    iput-object p3, p0, LX/8Nm;->c:LX/0Ot;

    .line 1337888
    iput-object p8, p0, LX/8Nm;->e:LX/8Ku;

    .line 1337889
    iput-object p5, p0, LX/8Nm;->f:Ljava/lang/String;

    .line 1337890
    iput-boolean p6, p0, LX/8Nm;->g:Z

    .line 1337891
    iput-object p7, p0, LX/8Nm;->h:LX/0Px;

    .line 1337892
    iput-object p4, p0, LX/8Nm;->d:LX/3ib;

    .line 1337893
    return-void
.end method
