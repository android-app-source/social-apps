.class public LX/8NA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0SG;

.field private final d:LX/8Mv;

.field private final e:LX/0lB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1336081
    const-class v0, LX/8NA;

    sput-object v0, LX/8NA;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0SG;LX/8Mv;LX/0lB;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0SG;",
            "LX/8Mv;",
            "LX/0lB;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1336082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1336083
    iput-object p1, p0, LX/8NA;->b:LX/0Or;

    .line 1336084
    iput-object p2, p0, LX/8NA;->c:LX/0SG;

    .line 1336085
    iput-object p3, p0, LX/8NA;->d:LX/8Mv;

    .line 1336086
    iput-object p4, p0, LX/8NA;->e:LX/0lB;

    .line 1336087
    return-void
.end method

.method public static a(LX/0QB;)LX/8NA;
    .locals 5

    .prologue
    .line 1336088
    new-instance v3, LX/8NA;

    const/16 v0, 0x15e7

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/8Mv;->a(LX/0QB;)LX/8Mv;

    move-result-object v1

    check-cast v1, LX/8Mv;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v2

    check-cast v2, LX/0lB;

    invoke-direct {v3, v4, v0, v1, v2}, LX/8NA;-><init>(LX/0Or;LX/0SG;LX/8Mv;LX/0lB;)V

    .line 1336089
    move-object v0, v3

    .line 1336090
    return-object v0
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1336091
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1336092
    :cond_0
    const/4 v0, 0x0

    .line 1336093
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x2c

    invoke-static {v1}, LX/0PO;->on(C)LX/0PO;

    move-result-object v1

    invoke-virtual {v1}, LX/0PO;->skipNulls()LX/0PO;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 12

    .prologue
    .line 1336094
    check-cast p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    const-wide/16 v8, 0x0

    const/4 v5, 0x1

    .line 1336095
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1336096
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->I:LX/5Rn;

    move-object v1, v1

    .line 1336097
    sget-object v2, LX/5Rn;->NORMAL:LX/5Rn;

    if-ne v1, v2, :cond_1f

    .line 1336098
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "published"

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336099
    :cond_0
    :goto_0
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Z:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-object v1, v1

    .line 1336100
    if-eqz v1, :cond_1

    .line 1336101
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "composer_session_events_log"

    iget-object v3, p0, LX/8NA;->e:LX/0lB;

    .line 1336102
    iget-object v4, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Z:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-object v4, v4

    .line 1336103
    invoke-virtual {v3, v4}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336104
    :cond_1
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->j:Ljava/lang/String;

    move-object v1, v1

    .line 1336105
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1336106
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "name"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336107
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->f()Ljava/lang/String;

    move-result-object v1

    .line 1336108
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1336109
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "profile_id"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336110
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->h()Ljava/lang/String;

    move-result-object v1

    .line 1336111
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1336112
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "place"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336113
    :cond_4
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1336114
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1336115
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "text_only_place"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336116
    :cond_5
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "checkin_entry_point"

    .line 1336117
    iget-boolean v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->f:Z

    move v3, v3

    .line 1336118
    invoke-static {v3}, LX/5RB;->a(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336119
    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->g()Ljava/lang/String;

    move-result-object v1

    .line 1336120
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, LX/8NA;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1336121
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "target_id"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336122
    :cond_6
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->l:LX/0Px;

    move-object v1, v1

    .line 1336123
    invoke-static {v1}, LX/8NA;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 1336124
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1336125
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "tags"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336126
    :cond_7
    invoke-static {p1}, LX/8Mv;->a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/lang/String;

    move-result-object v1

    .line 1336127
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1336128
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "stickers"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336129
    :cond_8
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Q:Ljava/lang/String;

    move-object v1, v1

    .line 1336130
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1336131
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "source_type"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336132
    :cond_9
    invoke-static {p1}, LX/8Mv;->b(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/lang/String;

    move-result-object v1

    .line 1336133
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 1336134
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "text_overlay"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336135
    :cond_a
    invoke-static {p1}, LX/8Mv;->c(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1336136
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_cropped"

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336137
    :cond_b
    invoke-static {p1}, LX/8Mv;->d(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1336138
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_rotated"

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336139
    :cond_c
    invoke-static {p1}, LX/8Mv;->e(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 1336140
    invoke-static {p1}, LX/8Mv;->h(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    .line 1336141
    if-eqz v1, :cond_20

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFrameOverlayItems()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_20

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFrameOverlayItems()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_20

    .line 1336142
    const/4 v1, 0x1

    .line 1336143
    :goto_1
    move v1, v1

    .line 1336144
    if-eqz v1, :cond_e

    .line 1336145
    :cond_d
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_filtered"

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336146
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "filter_name"

    .line 1336147
    invoke-static {p1}, LX/8Mv;->h(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    .line 1336148
    if-eqz v3, :cond_21

    .line 1336149
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v3

    .line 1336150
    :goto_2
    move-object v3, v3

    .line 1336151
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336152
    :cond_e
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->n:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-object v1, v1

    .line 1336153
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1336154
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->o:Ljava/lang/String;

    move-object v1, v1

    .line 1336155
    if-eqz v1, :cond_f

    .line 1336156
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "referenced_sticker_id"

    .line 1336157
    iget-object v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->o:Ljava/lang/String;

    move-object v3, v3

    .line 1336158
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336159
    :cond_f
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-object v1, v1

    .line 1336160
    iget-object v2, v1, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->e:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 1336161
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "privacy"

    iget-object v1, v1, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->e:Ljava/lang/String;

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336162
    :cond_10
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "audience_exp"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336163
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p:Ljava/lang/String;

    move-object v1, v1

    .line 1336164
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 1336165
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "qn"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336166
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "composer_session_id"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336167
    :cond_11
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->q:Ljava/lang/String;

    move-object v1, v1

    .line 1336168
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 1336169
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "idempotence_token"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "-publish"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336170
    :cond_12
    iget v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->r:I

    move v1, v1

    .line 1336171
    if-eqz v1, :cond_13

    .line 1336172
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "orientation"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336173
    :cond_13
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_explicit_location"

    .line 1336174
    iget-boolean v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->x:Z

    move v3, v3

    .line 1336175
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336176
    iget-wide v10, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->F:J

    move-wide v2, v10

    .line 1336177
    cmp-long v1, v2, v8

    if-eqz v1, :cond_14

    .line 1336178
    iget-object v1, p0, LX/8NA;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 1336179
    sub-long v2, v4, v2

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 1336180
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "time_since_original_post"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336181
    :cond_14
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->M:Ljava/lang/String;

    move-object v1, v1

    .line 1336182
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 1336183
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "post_channel_id"

    .line 1336184
    iget-object v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->M:Ljava/lang/String;

    move-object v3, v3

    .line 1336185
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336186
    :cond_15
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->N:Ljava/lang/String;

    move-object v1, v1

    .line 1336187
    if-eqz v1, :cond_16

    .line 1336188
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "post_channel_feedback_state"

    .line 1336189
    iget-object v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->N:Ljava/lang/String;

    move-object v3, v3

    .line 1336190
    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336191
    :cond_16
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->L:Ljava/lang/String;

    move-object v1, v1

    .line 1336192
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 1336193
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "append_story_id"

    .line 1336194
    iget-object v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->L:Ljava/lang/String;

    move-object v3, v3

    .line 1336195
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336196
    :cond_17
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->O:LX/0Px;

    move-object v1, v1

    .line 1336197
    if-eqz v1, :cond_18

    .line 1336198
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->O:LX/0Px;

    move-object v1, v1

    .line 1336199
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_18

    .line 1336200
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "contributors"

    .line 1336201
    iget-object v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->O:LX/0Px;

    move-object v3, v3

    .line 1336202
    invoke-virtual {v3}, LX/0Py;->asList()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/8NA;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336203
    :cond_18
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->P:Ljava/lang/String;

    move-object v1, v1

    .line 1336204
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 1336205
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "connection_class"

    .line 1336206
    iget-object v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->P:Ljava/lang/String;

    move-object v3, v3

    .line 1336207
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336208
    :cond_19
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->v:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object v1, v1

    .line 1336209
    if-eqz v1, :cond_1a

    .line 1336210
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "proxied_app_id"

    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336211
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "proxied_app_name"

    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336212
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "android_key_hash"

    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336213
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "attribution_app_id"

    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336214
    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 1336215
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "attribution_app_metadata"

    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336216
    :cond_1a
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->V:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-object v1, v1

    .line 1336217
    if-eqz v1, :cond_1b

    .line 1336218
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "prompt_id"

    iget-object v4, v1, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336219
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "prompt_type"

    iget-object v4, v1, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptType:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336220
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "prompt_tracking_string"

    iget-object v1, v1, Lcom/facebook/productionprompts/logging/PromptAnalytics;->trackingString:Ljava/lang/String;

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336221
    :cond_1b
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->W:LX/0Px;

    move-object v1, v1

    .line 1336222
    if-eqz v1, :cond_1c

    .line 1336223
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "inspiration_prompts"

    iget-object v4, p0, LX/8NA;->e:LX/0lB;

    invoke-virtual {v4, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336224
    :cond_1c
    iget-wide v10, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    move-wide v2, v10

    .line 1336225
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 1336226
    iget-boolean v2, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Y:Z

    move v2, v2

    .line 1336227
    if-eqz v2, :cond_1d

    .line 1336228
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "post_surfaces_blacklist"

    sget-object v4, LX/7mB;->a:LX/0Px;

    invoke-static {v4}, LX/16N;->b(Ljava/util/List;)LX/162;

    move-result-object v4

    invoke-virtual {v4}, LX/162;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336229
    :cond_1d
    iget-object v2, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->aa:Ljava/lang/String;

    move-object v2, v2

    .line 1336230
    if-eqz v2, :cond_1e

    .line 1336231
    iget-object v2, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->aa:Ljava/lang/String;

    move-object v2, v2

    .line 1336232
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "sponsor_id"

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336233
    :cond_1e
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "publish-photo"

    .line 1336234
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1336235
    move-object v2, v2

    .line 1336236
    const-string v3, "POST"

    .line 1336237
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 1336238
    move-object v2, v2

    .line 1336239
    iput-object v1, v2, LX/14O;->d:Ljava/lang/String;

    .line 1336240
    move-object v1, v2

    .line 1336241
    sget-object v2, LX/14S;->STRING:LX/14S;

    .line 1336242
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1336243
    move-object v1, v1

    .line 1336244
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1336245
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1336246
    move-object v0, v1

    .line 1336247
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1336248
    :cond_1f
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "published"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336249
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "unpublished_content_type"

    invoke-virtual {v1}, LX/5Rn;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336250
    iget-wide v10, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->J:J

    move-wide v2, v10

    .line 1336251
    cmp-long v1, v2, v8

    if-eqz v1, :cond_0

    .line 1336252
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "scheduled_publish_time"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    :cond_20
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_21
    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1336253
    check-cast p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    .line 1336254
    iget-wide v2, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    move-wide v0, v2

    .line 1336255
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
