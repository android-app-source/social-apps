.class public LX/749;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/749;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/74I;


# direct methods
.method public constructor <init>(LX/0Zb;LX/74I;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1167541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1167542
    iput-object p1, p0, LX/749;->a:LX/0Zb;

    .line 1167543
    iput-object p2, p0, LX/749;->b:LX/74I;

    .line 1167544
    return-void
.end method

.method public static a(LX/0QB;)LX/749;
    .locals 5

    .prologue
    .line 1167525
    sget-object v0, LX/749;->c:LX/749;

    if-nez v0, :cond_1

    .line 1167526
    const-class v1, LX/749;

    monitor-enter v1

    .line 1167527
    :try_start_0
    sget-object v0, LX/749;->c:LX/749;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1167528
    if-eqz v2, :cond_0

    .line 1167529
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1167530
    new-instance p0, LX/749;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/74I;->a(LX/0QB;)LX/74I;

    move-result-object v4

    check-cast v4, LX/74I;

    invoke-direct {p0, v3, v4}, LX/749;-><init>(LX/0Zb;LX/74I;)V

    .line 1167531
    move-object v0, p0

    .line 1167532
    sput-object v0, LX/749;->c:LX/749;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1167533
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1167534
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1167535
    :cond_1
    sget-object v0, LX/749;->c:LX/749;

    return-object v0

    .line 1167536
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1167537
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/749;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1167538
    iput-object p2, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1167539
    iget-object v0, p0, LX/749;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1167540
    return-void
.end method
