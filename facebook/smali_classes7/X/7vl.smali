.class public LX/7vl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6qa;


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field private final b:LX/6r5;

.field public c:LX/6qb;

.field private d:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/6r5;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1275081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275082
    iput-object p1, p0, LX/7vl;->a:Ljava/util/concurrent/Executor;

    .line 1275083
    iput-object p2, p0, LX/7vl;->b:LX/6r5;

    .line 1275084
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4

    .prologue
    .line 1275085
    iget-object v0, p0, LX/7vl;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1275086
    iget-object v0, p0, LX/7vl;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1275087
    :goto_0
    return-object v0

    .line 1275088
    :cond_0
    iget-object v0, p0, LX/7vl;->b:LX/6r5;

    invoke-virtual {v0, p1}, LX/6r5;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1275089
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1275090
    new-instance v2, LX/7vk;

    invoke-direct {v2, p0, p1}, LX/7vk;-><init>(LX/7vl;Lcom/facebook/payments/checkout/model/CheckoutData;)V

    iget-object v3, p0, LX/7vl;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1275091
    move-object v1, v1

    .line 1275092
    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/7vl;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1275093
    iget-object v0, p0, LX/7vl;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final a(LX/6qb;)V
    .locals 2

    .prologue
    .line 1275094
    iput-object p1, p0, LX/7vl;->c:LX/6qb;

    .line 1275095
    iget-object v0, p0, LX/7vl;->b:LX/6r5;

    iget-object v1, p0, LX/7vl;->c:LX/6qb;

    invoke-virtual {v0, v1}, LX/6r5;->a(LX/6qb;)V

    .line 1275096
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1275097
    iget-object v0, p0, LX/7vl;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    return v0
.end method
