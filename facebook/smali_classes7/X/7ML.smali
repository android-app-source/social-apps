.class public final LX/7ML;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/7M3;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/AdvancePlaybackOnFlingPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/AdvancePlaybackOnFlingPlugin;)V
    .locals 0

    .prologue
    .line 1198574
    iput-object p1, p0, LX/7ML;->a:Lcom/facebook/video/player/plugins/AdvancePlaybackOnFlingPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/7M3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1198575
    const-class v0, LX/7M3;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 1198576
    check-cast p1, LX/7M3;

    .line 1198577
    iget-object v0, p0, LX/7ML;->a:Lcom/facebook/video/player/plugins/AdvancePlaybackOnFlingPlugin;

    iget-object v0, v0, LX/3Gb;->n:LX/7Lf;

    if-nez v0, :cond_1

    .line 1198578
    :cond_0
    :goto_0
    return-void

    .line 1198579
    :cond_1
    iget-object v0, p1, LX/7M3;->b:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v0, p1, LX/7M3;->b:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    .line 1198580
    iget-object v2, p1, LX/7M3;->a:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v3, p1, LX/7M3;->a:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 1198581
    sub-float v4, v1, v2

    sub-float/2addr v0, v3

    div-float v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_3

    const/4 v0, 0x1

    .line 1198582
    :goto_1
    if-eqz v0, :cond_0

    .line 1198583
    iget-object v0, p0, LX/7ML;->a:Lcom/facebook/video/player/plugins/AdvancePlaybackOnFlingPlugin;

    iget-object v0, v0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/7Lh;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 p1, 0x0

    .line 1198584
    sub-float v5, v1, v2

    cmpg-float v5, v5, p1

    if-gez v5, :cond_4

    move v5, v3

    .line 1198585
    :goto_2
    sub-float p0, v1, v2

    cmpl-float p0, p0, p1

    if-lez p0, :cond_5

    .line 1198586
    :goto_3
    if-eqz v5, :cond_6

    invoke-interface {v0}, LX/7Lh;->c()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1198587
    sget-object v3, LX/04g;->BY_USER_SWIPE:LX/04g;

    invoke-interface {v0, v3}, LX/7Lh;->b(LX/04g;)V

    .line 1198588
    :cond_2
    :goto_4
    goto :goto_0

    .line 1198589
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    move v5, v4

    .line 1198590
    goto :goto_2

    :cond_5
    move v3, v4

    .line 1198591
    goto :goto_3

    .line 1198592
    :cond_6
    if-eqz v3, :cond_2

    invoke-interface {v0}, LX/7Lh;->d()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1198593
    sget-object v3, LX/04g;->BY_USER_SWIPE:LX/04g;

    invoke-interface {v0, v3}, LX/7Lh;->c(LX/04g;)V

    goto :goto_4
.end method
