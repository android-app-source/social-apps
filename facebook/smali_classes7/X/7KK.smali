.class public final LX/7KK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2qG;


# instance fields
.field public final synthetic a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)V
    .locals 0

    .prologue
    .line 1194763
    iput-object p1, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194764
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 1194752
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    iget-object v1, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    iget-object v1, v1, LX/2q6;->m:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 1194753
    iput-wide v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->K:J

    .line 1194754
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    iget-boolean v0, v0, LX/2q6;->L:Z

    if-eqz v0, :cond_0

    .line 1194755
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    iget-object v0, v0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    iget-object v1, v1, LX/2q6;->y:LX/04G;

    iget-object v2, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    iget-object v2, v2, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v3, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    iget-object v3, v3, LX/2q6;->w:LX/04D;

    iget-object v4, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    iget-object v4, v4, LX/2q6;->z:LX/04g;

    iget-object v4, v4, LX/04g;->value:Ljava/lang/String;

    iget-object v5, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-virtual {v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->r()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/1C2;->a(LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;)LX/1C2;

    .line 1194756
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    const/4 v1, 0x0

    .line 1194757
    iput-boolean v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->L:Z

    .line 1194758
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    sget-object v1, LX/2qE;->STATE_UPDATED:LX/2qE;

    .line 1194759
    iput-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->V:LX/2qE;

    .line 1194760
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    iget-object v0, v0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 1194761
    iget-object v2, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    iget-object v2, v2, LX/2q6;->M:LX/04g;

    invoke-interface {v0, v2}, LX/2pf;->c(LX/04g;)V

    goto :goto_0

    .line 1194762
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 1194749
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    .line 1194750
    invoke-virtual {v0}, LX/2q6;->y()V

    .line 1194751
    return-void
.end method

.method public final a(LX/7KG;)V
    .locals 3

    .prologue
    .line 1194733
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    const-string v1, "ExoPlayer::VideoSurfaceTarget::onSurfaceUnavailable"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194734
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-virtual {v0}, LX/2q6;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1194735
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, LX/2q6;->c(LX/04g;)V

    .line 1194736
    :cond_0
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->z()V

    .line 1194737
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    sget-object v1, LX/2qk;->FROM_DESTROY_SURFACE:LX/2qk;

    invoke-virtual {v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/2qk;)V

    .line 1194738
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    const/4 v1, 0x0

    .line 1194739
    iput-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->I:Landroid/view/Surface;

    .line 1194740
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    sget-object v1, LX/2qE;->STATE_DESTROYED:LX/2qE;

    .line 1194741
    iput-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->V:LX/2qE;

    .line 1194742
    invoke-virtual {p1}, LX/7KG;->a()V

    .line 1194743
    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 3

    .prologue
    .line 1194744
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    const-string v1, "ExoPlayer::VideoSurfaceTarget::onSurfaceAvailable"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194745
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b(Landroid/view/Surface;)V

    .line 1194746
    iget-object v0, p0, LX/7KK;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    sget-object v1, LX/2qE;->STATE_CREATED:LX/2qE;

    .line 1194747
    iput-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->V:LX/2qE;

    .line 1194748
    return-void
.end method
