.class public final LX/7jn;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1230413
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1230414
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1230415
    :goto_0
    return v1

    .line 1230416
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1230417
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 1230418
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1230419
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1230420
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1230421
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1230422
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1230423
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 1230424
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1230425
    const/4 v3, 0x0

    .line 1230426
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 1230427
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1230428
    :goto_3
    move v2, v3

    .line 1230429
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1230430
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 1230431
    goto :goto_1

    .line 1230432
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1230433
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1230434
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 1230435
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1230436
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1230437
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1230438
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1230439
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1230440
    const-string v5, "collection_product_items"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1230441
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1230442
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_d

    .line 1230443
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1230444
    :goto_5
    move v2, v4

    .line 1230445
    goto :goto_4

    .line 1230446
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1230447
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1230448
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_8
    move v2, v3

    goto :goto_4

    .line 1230449
    :cond_9
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_b

    .line 1230450
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1230451
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1230452
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_9

    if-eqz v7, :cond_9

    .line 1230453
    const-string v8, "featuredProductCount"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1230454
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    move v6, v2

    move v2, v5

    goto :goto_6

    .line 1230455
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_6

    .line 1230456
    :cond_b
    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1230457
    if-eqz v2, :cond_c

    .line 1230458
    invoke-virtual {p1, v4, v6, v4}, LX/186;->a(III)V

    .line 1230459
    :cond_c
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_5

    :cond_d
    move v2, v4

    move v6, v4

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1230460
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1230461
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1230462
    if-eqz v0, :cond_3

    .line 1230463
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1230464
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1230465
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1230466
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 1230467
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1230468
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1230469
    if-eqz v3, :cond_1

    .line 1230470
    const-string p1, "collection_product_items"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1230471
    const/4 p1, 0x0

    .line 1230472
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1230473
    invoke-virtual {p0, v3, p1, p1}, LX/15i;->a(III)I

    move-result p1

    .line 1230474
    if-eqz p1, :cond_0

    .line 1230475
    const-string v2, "featuredProductCount"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1230476
    invoke-virtual {p2, p1}, LX/0nX;->b(I)V

    .line 1230477
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1230478
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1230479
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1230480
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1230481
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1230482
    return-void
.end method
