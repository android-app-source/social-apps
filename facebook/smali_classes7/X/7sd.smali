.class public final LX/7sd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1265396
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1265397
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1265398
    :goto_0
    return v1

    .line 1265399
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1265400
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1265401
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1265402
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1265403
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1265404
    const-string v4, "edges"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1265405
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1265406
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 1265407
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 1265408
    invoke-static {p0, p1}, LX/7sc;->b(LX/15w;LX/186;)I

    move-result v3

    .line 1265409
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1265410
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1265411
    goto :goto_1

    .line 1265412
    :cond_3
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1265413
    invoke-static {p0, p1}, LX/4aB;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1265414
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1265415
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1265416
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1265417
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1
.end method
