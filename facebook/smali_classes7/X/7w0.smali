.class public LX/7w0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6uV;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6uV",
        "<",
        "Lcom/facebook/payments/confirmation/SimpleConfirmationData;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:LX/6ui;

.field private final c:LX/1nQ;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/6ui;LX/1nQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1275337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275338
    iput-object p1, p0, LX/7w0;->a:Landroid/content/res/Resources;

    .line 1275339
    iput-object p2, p0, LX/7w0;->b:LX/6ui;

    .line 1275340
    iput-object p3, p0, LX/7w0;->c:LX/1nQ;

    .line 1275341
    return-void
.end method

.method private a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V
    .locals 4

    .prologue
    .line 1275302
    iget-object v0, p0, LX/7w0;->c:LX/1nQ;

    iget-object v1, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->o:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    iget-object v2, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a:Ljava/lang/String;

    .line 1275303
    iget-object v3, v0, LX/1nQ;->i:LX/0Zb;

    const-string p0, "event_buy_tickets_confirmation_impression"

    const/4 p1, 0x0

    invoke-interface {v3, p0, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 1275304
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1275305
    const-string p0, "event_ticketing"

    invoke-virtual {v3, p0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    iget-object p0, v0, LX/1nQ;->j:LX/0kv;

    iget-object p1, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {p0, p1}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string p0, "EventTicketOrder"

    invoke-virtual {v3, p0}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string p0, "event_id"

    iget-object p1, v1, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->b:Ljava/lang/String;

    invoke-virtual {v3, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string p0, "session_id"

    iget-object p1, v1, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->a:Ljava/lang/String;

    invoke-virtual {v3, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V

    .line 1275306
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/confirmation/ConfirmationData;)LX/0Px;
    .locals 12

    .prologue
    .line 1275307
    check-cast p1, Lcom/facebook/payments/confirmation/SimpleConfirmationData;

    const/4 v4, 0x0

    .line 1275308
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1275309
    invoke-virtual {p1}, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a()Lcom/facebook/payments/confirmation/ConfirmationParams;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;

    .line 1275310
    iget-object v1, p1, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->b:Lcom/facebook/payments/confirmation/ProductConfirmationData;

    move-object v1, v1

    .line 1275311
    check-cast v1, Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;

    .line 1275312
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1275313
    sget-object v3, LX/6uT;->CHECK_MARK:LX/6uT;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1275314
    sget-object v3, LX/6uT;->PRODUCT_PURCHASE_SECTION:LX/6uT;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1275315
    iget-object v3, v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-boolean v3, v3, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->H:Z

    if-eqz v3, :cond_0

    .line 1275316
    sget-object v3, LX/6uT;->DIVIDER:LX/6uT;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1275317
    sget-object v3, LX/6uT;->VIEW_PURCHASED_ITEMS:LX/6uT;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1275318
    :cond_0
    sget-object v3, LX/6uT;->DIVIDER:LX/6uT;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1275319
    sget-object v3, LX/6uT;->SEE_RECEIPT:LX/6uT;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1275320
    iget-object v3, v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v3, v3, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->C:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq v3, v6, :cond_1

    .line 1275321
    sget-object v3, LX/6uT;->DIVIDER:LX/6uT;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1275322
    sget-object v3, LX/6uT;->PRODUCT_USER_ENGAGE_OPTION:LX/6uT;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1275323
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v6, v2

    .line 1275324
    iget-object v2, v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-direct {p0, v2}, LX/7w0;->a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V

    .line 1275325
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v4

    :goto_0
    if-ge v3, v7, :cond_2

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6uT;

    .line 1275326
    sget-object v8, LX/7vz;->a:[I

    invoke-virtual {v2}, LX/6uT;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 1275327
    iget-object v8, p0, LX/7w0;->b:LX/6ui;

    invoke-virtual {v8, v5, v2, p1, v4}, LX/6ui;->a(LX/0Pz;LX/6uT;Lcom/facebook/payments/confirmation/SimpleConfirmationData;Z)V

    .line 1275328
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 1275329
    :pswitch_0
    new-instance v2, LX/7w2;

    iget-object v8, v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v8, v8, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->M:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iget-object v9, v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v9, v9, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->f:Landroid/net/Uri;

    invoke-direct {v2, v8, v9}, LX/7w2;-><init>(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Landroid/net/Uri;)V

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1275330
    goto :goto_1

    .line 1275331
    :pswitch_1
    iget-object v2, v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget v2, v2, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->E:I

    .line 1275332
    new-instance v8, LX/7wF;

    iget-object v9, v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v9, v9, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a:Ljava/lang/String;

    iget-object v10, p0, LX/7w0;->a:Landroid/content/res/Resources;

    const v11, 0x7f0f00eb

    invoke-virtual {v10, v11, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v8, v9, v2}, LX/7wF;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1275333
    goto :goto_1

    .line 1275334
    :pswitch_2
    new-instance v2, LX/7w4;

    iget-boolean v8, v1, Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;->a:Z

    invoke-direct {v2, v8, p1}, LX/7w4;-><init>(ZLcom/facebook/payments/confirmation/SimpleConfirmationData;)V

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1275335
    goto :goto_1

    .line 1275336
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
