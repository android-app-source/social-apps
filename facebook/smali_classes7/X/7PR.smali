.class public LX/7PR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7PJ;


# instance fields
.field private final a:I

.field private final b:LX/0So;

.field private final c:LX/7I0;


# direct methods
.method public constructor <init>(LX/7I0;LX/0So;)V
    .locals 1

    .prologue
    .line 1202853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202854
    const/high16 v0, 0x10000

    iput v0, p0, LX/7PR;->a:I

    .line 1202855
    iput-object p2, p0, LX/7PR;->b:LX/0So;

    .line 1202856
    iput-object p1, p0, LX/7PR;->c:LX/7I0;

    .line 1202857
    return-void
.end method


# virtual methods
.method public final a(LX/7Pw;J)J
    .locals 10

    .prologue
    const-wide/32 v8, 0x10000

    const-wide/16 v0, 0x0

    .line 1202858
    iget-object v2, p1, LX/7Pw;->c:LX/2WF;

    move-object v2, v2

    .line 1202859
    iget-object v3, p0, LX/7PR;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    iget-wide v6, p1, LX/7Pw;->b:J

    sub-long/2addr v4, v6

    .line 1202860
    cmp-long v3, v4, v0

    if-lez v3, :cond_0

    .line 1202861
    iget-object v3, p0, LX/7PR;->c:LX/7I0;

    iget v3, v3, LX/7I0;->b:I

    div-int/lit16 v3, v3, 0x3e8

    int-to-long v6, v3

    mul-long/2addr v4, v6

    .line 1202862
    iget-wide v2, v2, LX/2WF;->a:J

    sub-long v2, p2, v2

    .line 1202863
    cmp-long v6, v2, v0

    if-gez v6, :cond_1

    .line 1202864
    const-wide/16 v0, -0x1

    .line 1202865
    :cond_0
    :goto_0
    return-wide v0

    .line 1202866
    :cond_1
    cmp-long v6, v2, v4

    if-gez v6, :cond_0

    .line 1202867
    sub-long v0, v4, v2

    .line 1202868
    div-long/2addr v0, v8

    mul-long/2addr v0, v8

    goto :goto_0
.end method
