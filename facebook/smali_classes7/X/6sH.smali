.class public LX/6sH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6rs;


# direct methods
.method public constructor <init>(LX/6rs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1153047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153048
    iput-object p1, p0, LX/6sH;->a:LX/6rs;

    .line 1153049
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1153050
    iget-object v0, p0, LX/6sH;->a:LX/6rs;

    .line 1153051
    iget-object v1, v0, LX/6rs;->w:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6rr;

    move-object v0, v1

    .line 1153052
    const-string v1, "entity"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1153053
    iget-object v1, p0, LX/6sH;->a:LX/6rs;

    const-string v2, "1.1.1"

    invoke-virtual {v1, v2}, LX/6rs;->d(Ljava/lang/String;)LX/6rr;

    move-result-object v1

    const-string v2, "1.1.1"

    invoke-interface {v1, v2, p2}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    .line 1153054
    invoke-static {v1}, LX/6rQ;->a(Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;)LX/6rQ;

    move-result-object v2

    .line 1153055
    iput-object v0, v2, LX/6rQ;->a:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1153056
    move-object v2, v2

    .line 1153057
    invoke-virtual {v2}, LX/6rQ;->a()Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    move-result-object v2

    move-object v0, v2

    .line 1153058
    return-object v0
.end method
