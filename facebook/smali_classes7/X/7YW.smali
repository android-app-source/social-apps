.class public LX/7YW;
.super LX/7YU;
.source ""


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:LX/0yV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1220097
    const-class v0, LX/7YW;

    sput-object v0, LX/7YW;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yV;)V
    .locals 0

    .prologue
    .line 1220070
    invoke-direct {p0, p1}, LX/7YU;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1220071
    iput-object p2, p0, LX/7YW;->l:LX/0yV;

    .line 1220072
    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yV;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V
    .locals 1

    .prologue
    .line 1220082
    invoke-direct {p0, p1, p3}, LX/7YU;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;)V

    .line 1220083
    iput-object p2, p0, LX/7YW;->l:LX/0yV;

    .line 1220084
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YW;->c:Ljava/lang/String;

    .line 1220085
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YW;->d:Ljava/lang/String;

    .line 1220086
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->v()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1220087
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1220088
    :goto_0
    iput-object v0, p0, LX/7YW;->e:LX/0Px;

    .line 1220089
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->I()Z

    move-result v0

    iput-boolean v0, p0, LX/7YW;->f:Z

    .line 1220090
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YW;->g:Ljava/lang/String;

    .line 1220091
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YW;->h:Ljava/lang/String;

    .line 1220092
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YW;->i:Ljava/lang/String;

    .line 1220093
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YW;->j:Ljava/lang/String;

    .line 1220094
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7YU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7YW;->k:Ljava/lang/String;

    .line 1220095
    return-void

    .line 1220096
    :cond_0
    invoke-virtual {p3}, Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel$ZeroOptinModel;->v()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1220074
    iget-object v0, p0, LX/7YU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 1220075
    invoke-super {p0, v1}, LX/7YU;->a(LX/0hN;)V

    .line 1220076
    sget-object v0, LX/0df;->C:LX/0Tn;

    const-string v2, "image_url_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YW;->c:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->C:LX/0Tn;

    const-string v3, "facepile_text_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v3, p0, LX/7YW;->d:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->C:LX/0Tn;

    const-string v3, "should_show_confirmation_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-boolean v3, p0, LX/7YW;->f:Z

    invoke-interface {v2, v0, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->C:LX/0Tn;

    const-string v3, "confirmation_title_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v3, p0, LX/7YW;->g:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->C:LX/0Tn;

    const-string v3, "confirmation_description_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v3, p0, LX/7YW;->h:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->C:LX/0Tn;

    const-string v3, "confirmation_primary_button_text_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v3, p0, LX/7YW;->i:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->C:LX/0Tn;

    const-string v3, "confirmation_secondary_button_text_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v3, p0, LX/7YW;->j:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->C:LX/0Tn;

    const-string v3, "confirmation_back_button_behavior_key"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v3, p0, LX/7YW;->k:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1220077
    :try_start_0
    sget-object v0, LX/0df;->C:LX/0Tn;

    const-string v2, "facepile_profile_picture_urls_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/7YW;->l:LX/0yV;

    iget-object v3, p0, LX/7YW;->e:LX/0Px;

    invoke-virtual {v2, v3}, LX/0yV;->a(LX/0Px;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1220078
    :goto_0
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1220079
    return-void

    .line 1220080
    :catch_0
    move-exception v0

    .line 1220081
    sget-object v2, LX/7YW;->b:Ljava/lang/Class;

    const-string v3, "Failed to write zero optin facepile URLs to shared prefs"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final b()LX/0Tn;
    .locals 1

    .prologue
    .line 1220073
    sget-object v0, LX/0df;->C:LX/0Tn;

    return-object v0
.end method
