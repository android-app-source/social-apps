.class public final enum LX/87j;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/87j;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/87j;

.field public static final enum NOT_SELECTED:LX/87j;

.field public static final enum SELECTED:LX/87j;

.field public static final enum SUGGESTED:LX/87j;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1300535
    new-instance v0, LX/87j;

    const-string v1, "NOT_SELECTED"

    invoke-direct {v0, v1, v2}, LX/87j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/87j;->NOT_SELECTED:LX/87j;

    .line 1300536
    new-instance v0, LX/87j;

    const-string v1, "SUGGESTED"

    invoke-direct {v0, v1, v3}, LX/87j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/87j;->SUGGESTED:LX/87j;

    .line 1300537
    new-instance v0, LX/87j;

    const-string v1, "SELECTED"

    invoke-direct {v0, v1, v4}, LX/87j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/87j;->SELECTED:LX/87j;

    .line 1300538
    const/4 v0, 0x3

    new-array v0, v0, [LX/87j;

    sget-object v1, LX/87j;->NOT_SELECTED:LX/87j;

    aput-object v1, v0, v2

    sget-object v1, LX/87j;->SUGGESTED:LX/87j;

    aput-object v1, v0, v3

    sget-object v1, LX/87j;->SELECTED:LX/87j;

    aput-object v1, v0, v4

    sput-object v0, LX/87j;->$VALUES:[LX/87j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1300539
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/87j;
    .locals 1

    .prologue
    .line 1300540
    const-class v0, LX/87j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/87j;

    return-object v0
.end method

.method public static values()[LX/87j;
    .locals 1

    .prologue
    .line 1300541
    sget-object v0, LX/87j;->$VALUES:[LX/87j;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/87j;

    return-object v0
.end method
