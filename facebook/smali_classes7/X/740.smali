.class public LX/740;
.super Ljava/lang/Exception;
.source ""


# instance fields
.field public final mDiagnostic:LX/73x;

.field public final mRetryMightWork:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V
    .locals 1

    .prologue
    .line 1167367
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1167368
    iput-boolean p3, p0, LX/740;->mRetryMightWork:Z

    .line 1167369
    const/4 v0, 0x0

    iput-object v0, p0, LX/740;->mDiagnostic:LX/73x;

    .line 1167370
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 1167371
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/740;-><init>(Ljava/lang/String;ZLX/73x;)V

    .line 1167372
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLX/73x;)V
    .locals 0

    .prologue
    .line 1167373
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 1167374
    iput-boolean p2, p0, LX/740;->mRetryMightWork:Z

    .line 1167375
    iput-object p3, p0, LX/740;->mDiagnostic:LX/73x;

    .line 1167376
    return-void
.end method
