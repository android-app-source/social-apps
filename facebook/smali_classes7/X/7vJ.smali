.class public final enum LX/7vJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7vJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7vJ;

.field public static final enum EMAIL_SYNTHETIC:LX/7vJ;

.field public static final enum EMAIL_USER:LX/7vJ;

.field public static final enum PAGE:LX/7vJ;

.field public static final enum SMS_SYNTHETIC:LX/7vJ;

.field public static final enum SMS_USER:LX/7vJ;

.field public static final enum USER:LX/7vJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1274385
    new-instance v0, LX/7vJ;

    const-string v1, "USER"

    invoke-direct {v0, v1, v3}, LX/7vJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vJ;->USER:LX/7vJ;

    new-instance v0, LX/7vJ;

    const-string v1, "PAGE"

    invoke-direct {v0, v1, v4}, LX/7vJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vJ;->PAGE:LX/7vJ;

    new-instance v0, LX/7vJ;

    const-string v1, "EMAIL_USER"

    invoke-direct {v0, v1, v5}, LX/7vJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vJ;->EMAIL_USER:LX/7vJ;

    new-instance v0, LX/7vJ;

    const-string v1, "EMAIL_SYNTHETIC"

    invoke-direct {v0, v1, v6}, LX/7vJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vJ;->EMAIL_SYNTHETIC:LX/7vJ;

    new-instance v0, LX/7vJ;

    const-string v1, "SMS_USER"

    invoke-direct {v0, v1, v7}, LX/7vJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vJ;->SMS_USER:LX/7vJ;

    new-instance v0, LX/7vJ;

    const-string v1, "SMS_SYNTHETIC"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7vJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vJ;->SMS_SYNTHETIC:LX/7vJ;

    const/4 v0, 0x6

    new-array v0, v0, [LX/7vJ;

    sget-object v1, LX/7vJ;->USER:LX/7vJ;

    aput-object v1, v0, v3

    sget-object v1, LX/7vJ;->PAGE:LX/7vJ;

    aput-object v1, v0, v4

    sget-object v1, LX/7vJ;->EMAIL_USER:LX/7vJ;

    aput-object v1, v0, v5

    sget-object v1, LX/7vJ;->EMAIL_SYNTHETIC:LX/7vJ;

    aput-object v1, v0, v6

    sget-object v1, LX/7vJ;->SMS_USER:LX/7vJ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7vJ;->SMS_SYNTHETIC:LX/7vJ;

    aput-object v2, v0, v1

    sput-object v0, LX/7vJ;->$VALUES:[LX/7vJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1274386
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7vJ;
    .locals 1

    .prologue
    .line 1274387
    const-class v0, LX/7vJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7vJ;

    return-object v0
.end method

.method public static values()[LX/7vJ;
    .locals 1

    .prologue
    .line 1274388
    sget-object v0, LX/7vJ;->$VALUES:[LX/7vJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7vJ;

    return-object v0
.end method
