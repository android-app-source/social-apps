.class public LX/6t3;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/6t1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6t0;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/Integer;

.field public c:Landroid/view/View$OnClickListener;

.field public d:LX/6qh;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1153935
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1153936
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1153941
    sget-object v0, LX/6t2;->PRICE:LX/6t2;

    invoke-virtual {v0}, LX/6t2;->ordinal()I

    move-result v0

    if-eq p2, v0, :cond_0

    sget-object v0, LX/6t2;->CUSTOM:LX/6t2;

    invoke-virtual {v0}, LX/6t2;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 1153942
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1153943
    const v1, 0x7f03100d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    .line 1153944
    new-instance v1, LX/6t1;

    invoke-direct {v1, v0}, LX/6t1;-><init>(Lcom/facebook/fig/button/FigButton;)V

    move-object v0, v1

    .line 1153945
    return-object v0

    .line 1153946
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized row type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 1153947
    check-cast p1, LX/6t1;

    .line 1153948
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 1153949
    sget-object v1, LX/6t2;->PRICE:LX/6t2;

    invoke-virtual {v1}, LX/6t2;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 1153950
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    .line 1153951
    iget-object v1, p0, LX/6t3;->a:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6t0;

    .line 1153952
    iget-object v2, v1, LX/6t0;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1153953
    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 1153954
    iget-object v1, p0, LX/6t3;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/6t3;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p2, v1, :cond_2

    const/16 v1, 0x18

    :goto_0
    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 1153955
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v1, LX/6sz;

    invoke-direct {v1, p0, p2}, LX/6sz;-><init>(LX/6t3;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1153956
    :goto_1
    return-void

    .line 1153957
    :cond_0
    sget-object v1, LX/6t2;->CUSTOM:LX/6t2;

    invoke-virtual {v1}, LX/6t2;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 1153958
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    .line 1153959
    iget-object v1, p0, LX/6t3;->a:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6t0;

    .line 1153960
    iget-object v2, v1, LX/6t0;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1153961
    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 1153962
    iget-object v1, p0, LX/6t3;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/6t3;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p2, v1, :cond_3

    const/16 v1, 0x18

    :goto_2
    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 1153963
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    iget-object v1, p0, LX/6t3;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1153964
    goto :goto_1

    .line 1153965
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized row type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1153966
    :cond_2
    const/16 v1, 0x108

    goto :goto_0

    .line 1153967
    :cond_3
    const/16 v1, 0x108

    goto :goto_2
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1153938
    iget-object v0, p0, LX/6t3;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6t0;

    .line 1153939
    iget-object p0, v0, LX/6t0;->b:LX/6t2;

    move-object v0, p0

    .line 1153940
    invoke-virtual {v0}, LX/6t2;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1153937
    iget-object v0, p0, LX/6t3;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
