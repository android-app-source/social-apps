.class public final LX/78d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;)V
    .locals 0

    .prologue
    .line 1173184
    iput-object p1, p0, LX/78d;->a:Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x6c13e883

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1173185
    iget-object v1, p0, LX/78d;->a:Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;

    .line 1173186
    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1173187
    new-instance v4, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;

    invoke-direct {v4}, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;-><init>()V

    .line 1173188
    iget-object v3, v1, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->s:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 1173189
    iput-object v3, v4, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->r:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 1173190
    iget-object v3, v1, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->t:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7F3;

    .line 1173191
    iget-object p0, v3, LX/7F3;->a:LX/7F7;

    move-object p0, p0

    .line 1173192
    sget-object p1, LX/7F7;->EDITTEXT:LX/7F7;

    if-ne p0, p1, :cond_1

    .line 1173193
    check-cast v3, LX/7F5;

    .line 1173194
    iput-object v3, v4, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->u:LX/7F5;

    .line 1173195
    goto :goto_0

    .line 1173196
    :cond_1
    iget-object p0, v3, LX/7F3;->a:LX/7F7;

    move-object p0, p0

    .line 1173197
    sget-object p1, LX/7F7;->QUESTION:LX/7F7;

    if-ne p0, p1, :cond_0

    .line 1173198
    check-cast v3, LX/7FA;

    .line 1173199
    iput-object v3, v4, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->v:LX/7FA;

    .line 1173200
    goto :goto_0

    .line 1173201
    :cond_2
    iget-object v3, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v3, v3

    .line 1173202
    sget-object v5, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->m:Ljava/lang/String;

    invoke-virtual {v4, v3, v5}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1173203
    const v1, 0x49dd0fc0    # 1810936.0f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
