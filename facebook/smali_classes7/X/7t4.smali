.class public final LX/7t4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1266763
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1266764
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1266765
    :goto_0
    return v1

    .line 1266766
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1266767
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 1266768
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1266769
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1266770
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1266771
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1266772
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1266773
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 1266774
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1266775
    const/4 v3, 0x0

    .line 1266776
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_a

    .line 1266777
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1266778
    :goto_3
    move v2, v3

    .line 1266779
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1266780
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 1266781
    goto :goto_1

    .line 1266782
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1266783
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1266784
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 1266785
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1266786
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_9

    .line 1266787
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1266788
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1266789
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_6

    if-eqz v5, :cond_6

    .line 1266790
    const-string v6, "screen_elements"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1266791
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1266792
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_7

    .line 1266793
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_7

    .line 1266794
    invoke-static {p0, p1}, LX/7t3;->b(LX/15w;LX/186;)I

    move-result v5

    .line 1266795
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1266796
    :cond_7
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1266797
    goto :goto_4

    .line 1266798
    :cond_8
    const-string v6, "target_type"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1266799
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto :goto_4

    .line 1266800
    :cond_9
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1266801
    invoke-virtual {p1, v3, v4}, LX/186;->b(II)V

    .line 1266802
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1266803
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_3

    :cond_a
    move v2, v3

    move v4, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1266804
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1266805
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1266806
    if-eqz v0, :cond_4

    .line 1266807
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266808
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1266809
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 1266810
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 v5, 0x1

    .line 1266811
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1266812
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1266813
    if-eqz v3, :cond_1

    .line 1266814
    const-string v4, "screen_elements"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266815
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1266816
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {p0, v3}, LX/15i;->c(I)I

    move-result p1

    if-ge v4, p1, :cond_0

    .line 1266817
    invoke-virtual {p0, v3, v4}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2, p3}, LX/7t3;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1266818
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1266819
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1266820
    :cond_1
    invoke-virtual {p0, v2, v5}, LX/15i;->g(II)I

    move-result v3

    .line 1266821
    if-eqz v3, :cond_2

    .line 1266822
    const-string v3, "target_type"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266823
    invoke-virtual {p0, v2, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266824
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1266825
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1266826
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1266827
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1266828
    return-void
.end method
