.class public final LX/8Pa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Lcom/facebook/privacy/model/PrivacyOptionsResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/PrivacyOperationsClient;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/PrivacyOperationsClient;)V
    .locals 0

    .prologue
    .line 1341890
    iput-object p1, p0, LX/8Pa;->a:Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1341891
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1341892
    if-nez p1, :cond_0

    .line 1341893
    const/4 v0, 0x0

    .line 1341894
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    goto :goto_0
.end method
