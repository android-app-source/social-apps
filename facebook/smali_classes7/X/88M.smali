.class public final LX/88M;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1302107
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1302108
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1302109
    :goto_0
    return v1

    .line 1302110
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1302111
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1302112
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1302113
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1302114
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1302115
    const-string v4, "feedback_reaction_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1302116
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1302117
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_9

    .line 1302118
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1302119
    :goto_2
    move v2, v3

    .line 1302120
    goto :goto_1

    .line 1302121
    :cond_2
    const-string v4, "node"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1302122
    invoke-static {p0, p1}, LX/88L;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1302123
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1302124
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1302125
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1302126
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1302127
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_7

    .line 1302128
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1302129
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1302130
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_5

    if-eqz v6, :cond_5

    .line 1302131
    const-string v7, "key"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1302132
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    move v5, v2

    move v2, v4

    goto :goto_3

    .line 1302133
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1302134
    :cond_7
    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1302135
    if-eqz v2, :cond_8

    .line 1302136
    invoke-virtual {p1, v3, v5, v3}, LX/186;->a(III)V

    .line 1302137
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v2, v3

    move v5, v3

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1302138
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1302139
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1302140
    if-eqz v0, :cond_1

    .line 1302141
    const-string v1, "feedback_reaction_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1302142
    const/4 v1, 0x0

    .line 1302143
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1302144
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1302145
    if-eqz v1, :cond_0

    .line 1302146
    const-string v2, "key"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1302147
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1302148
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1302149
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1302150
    if-eqz v0, :cond_2

    .line 1302151
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1302152
    invoke-static {p0, v0, p2, p3}, LX/88L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1302153
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1302154
    return-void
.end method
