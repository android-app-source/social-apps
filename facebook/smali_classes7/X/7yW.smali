.class public final LX/7yW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/tagging/model/TaggingProfile;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7ye;

.field public final synthetic b:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;


# direct methods
.method public constructor <init>(Lcom/facebook/facerec/manager/FaceBoxPrioritizer;LX/7ye;)V
    .locals 0

    .prologue
    .line 1279416
    iput-object p1, p0, LX/7yW;->b:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iput-object p2, p0, LX/7yW;->a:LX/7ye;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1279417
    iget-object v0, p0, LX/7yW;->b:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v0, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->j:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1279418
    iget-object v0, p0, LX/7yW;->b:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v1, p0, LX/7yW;->a:LX/7ye;

    invoke-virtual {v0, v1}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->d(LX/7ye;)LX/7ye;

    move-result-object v0

    .line 1279419
    iget-object v1, p0, LX/7yW;->b:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v2, p0, LX/7yW;->a:LX/7ye;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a(LX/7ye;Z)V

    .line 1279420
    instance-of v1, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v1, :cond_0

    .line 1279421
    :goto_0
    return-void

    .line 1279422
    :cond_0
    iget-object v1, p0, LX/7yW;->b:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a(LX/7ye;Ljava/util/List;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1279423
    check-cast p1, Ljava/util/List;

    .line 1279424
    iget-object v0, p0, LX/7yW;->b:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v0, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->j:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1279425
    iget-object v0, p0, LX/7yW;->b:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v1, p0, LX/7yW;->a:LX/7ye;

    invoke-virtual {v0, v1}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->d(LX/7ye;)LX/7ye;

    move-result-object v0

    .line 1279426
    iget-object v1, p0, LX/7yW;->b:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v1, v0, p1}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a(LX/7ye;Ljava/util/List;)V

    .line 1279427
    iget-object v0, p0, LX/7yW;->b:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v1, p0, LX/7yW;->a:LX/7ye;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a(LX/7ye;Z)V

    .line 1279428
    return-void
.end method
