.class public final enum LX/8D7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8D7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8D7;

.field public static final enum FOLLOW_VIDEOS_BUTTON_NUX:LX/8D7;

.field public static final enum MUSIC_STORY_NUX:LX/8D7;

.field public static final enum PAGE_STORY_ADMIN_ATTRIBUTION_NUX:LX/8D7;

.field public static final enum SAVE_NUX:LX/8D7;


# instance fields
.field public final controllerClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "LX/3kT;",
            ">;"
        }
    .end annotation
.end field

.field public final description:Ljava/lang/String;

.field public final interstitialId:Ljava/lang/String;

.field public final nuxDelegate:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/nux/NuxBubbleManager$NuxDelegate;",
            ">;"
        }
    .end annotation
.end field

.field public final prefKey:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 1312340
    new-instance v0, LX/8D7;

    const-string v1, "SAVE_NUX"

    const-class v3, LX/2tb;

    const-class v4, LX/8DD;

    const-string v5, "2447"

    sget-object v6, LX/0pP;->l:LX/0Tn;

    const-string v7, "NUX bubble for the save button"

    invoke-direct/range {v0 .. v7}, LX/8D7;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V

    sput-object v0, LX/8D7;->SAVE_NUX:LX/8D7;

    .line 1312341
    new-instance v3, LX/8D7;

    const-string v4, "PAGE_STORY_ADMIN_ATTRIBUTION_NUX"

    const-class v6, LX/3kU;

    const-class v7, LX/8DB;

    const-string v8, "2449"

    sget-object v9, LX/0pP;->n:LX/0Tn;

    const-string v10, "NUX bubble for admin attribution on Page stories"

    move v5, v11

    invoke-direct/range {v3 .. v10}, LX/8D7;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V

    sput-object v3, LX/8D7;->PAGE_STORY_ADMIN_ATTRIBUTION_NUX:LX/8D7;

    .line 1312342
    new-instance v3, LX/8D7;

    const-string v4, "MUSIC_STORY_NUX"

    const-class v6, LX/8DA;

    const-class v7, LX/8D9;

    const-string v8, "2438"

    sget-object v9, LX/0pP;->s:LX/0Tn;

    const-string v10, "NUX bubble for music preview stories"

    move v5, v12

    invoke-direct/range {v3 .. v10}, LX/8D7;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V

    sput-object v3, LX/8D7;->MUSIC_STORY_NUX:LX/8D7;

    .line 1312343
    new-instance v3, LX/8D7;

    const-string v4, "FOLLOW_VIDEOS_BUTTON_NUX"

    const-class v6, LX/3kv;

    const-class v7, LX/8D8;

    const-string v8, "4141"

    sget-object v9, LX/0pP;->m:LX/0Tn;

    const-string v10, "NUX bubble for follow videos button"

    move v5, v13

    invoke-direct/range {v3 .. v10}, LX/8D7;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V

    sput-object v3, LX/8D7;->FOLLOW_VIDEOS_BUTTON_NUX:LX/8D7;

    .line 1312344
    const/4 v0, 0x4

    new-array v0, v0, [LX/8D7;

    sget-object v1, LX/8D7;->SAVE_NUX:LX/8D7;

    aput-object v1, v0, v2

    sget-object v1, LX/8D7;->PAGE_STORY_ADMIN_ATTRIBUTION_NUX:LX/8D7;

    aput-object v1, v0, v11

    sget-object v1, LX/8D7;->MUSIC_STORY_NUX:LX/8D7;

    aput-object v1, v0, v12

    sget-object v1, LX/8D7;->FOLLOW_VIDEOS_BUTTON_NUX:LX/8D7;

    aput-object v1, v0, v13

    sput-object v0, LX/8D7;->$VALUES:[LX/8D7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/Class;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/3kT;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/nux/NuxBubbleManager$NuxDelegate;",
            ">;",
            "Ljava/lang/String;",
            "LX/0Tn;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1312328
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1312329
    iput-object p3, p0, LX/8D7;->controllerClass:Ljava/lang/Class;

    .line 1312330
    iput-object p4, p0, LX/8D7;->nuxDelegate:Ljava/lang/Class;

    .line 1312331
    iput-object p5, p0, LX/8D7;->interstitialId:Ljava/lang/String;

    .line 1312332
    iput-object p6, p0, LX/8D7;->prefKey:LX/0Tn;

    .line 1312333
    iput-object p7, p0, LX/8D7;->description:Ljava/lang/String;

    .line 1312334
    return-void
.end method

.method public static forControllerClass(Ljava/lang/Class;)LX/8D7;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/3kT;",
            ">;)",
            "LX/8D7;"
        }
    .end annotation

    .prologue
    .line 1312323
    invoke-static {}, LX/8D7;->values()[LX/8D7;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1312324
    iget-object v4, v3, LX/8D7;->controllerClass:Ljava/lang/Class;

    if-ne v4, p0, :cond_0

    .line 1312325
    return-object v3

    .line 1312326
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1312327
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown controller class: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static forNuxDelegate(Ljava/lang/Class;)LX/8D7;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/nux/NuxBubbleManager$NuxDelegate;",
            ">;)",
            "LX/8D7;"
        }
    .end annotation

    .prologue
    .line 1312335
    invoke-static {}, LX/8D7;->values()[LX/8D7;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1312336
    iget-object v4, v3, LX/8D7;->nuxDelegate:Ljava/lang/Class;

    if-ne v4, p0, :cond_0

    .line 1312337
    return-object v3

    .line 1312338
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1312339
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown nux delegate class: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/8D7;
    .locals 1

    .prologue
    .line 1312322
    const-class v0, LX/8D7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8D7;

    return-object v0
.end method

.method public static values()[LX/8D7;
    .locals 1

    .prologue
    .line 1312321
    sget-object v0, LX/8D7;->$VALUES:[LX/8D7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8D7;

    return-object v0
.end method
