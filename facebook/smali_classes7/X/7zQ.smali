.class public final LX/7zQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4ck;
.implements LX/2BD;


# instance fields
.field private a:J

.field private b:Z

.field private final c:LX/7z4;


# direct methods
.method public constructor <init>(JLX/7z4;)V
    .locals 1

    .prologue
    .line 1280935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280936
    iput-wide p1, p0, LX/7zQ;->a:J

    .line 1280937
    iput-object p3, p0, LX/7zQ;->c:LX/7z4;

    .line 1280938
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7zQ;->b:Z

    .line 1280939
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1280947
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7zQ;->b:Z

    .line 1280948
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 1280945
    iget-object v0, p0, LX/7zQ;->c:LX/7z4;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, LX/7z4;->a(I)V

    .line 1280946
    return-void
.end method

.method public final a(JJ)V
    .locals 3

    .prologue
    .line 1280940
    iget-boolean v0, p0, LX/7zQ;->b:Z

    if-nez v0, :cond_0

    .line 1280941
    iget-wide v0, p0, LX/7zQ;->a:J

    sub-long v0, p1, v0

    long-to-int v0, v0

    .line 1280942
    iput-wide p1, p0, LX/7zQ;->a:J

    .line 1280943
    iget-object v1, p0, LX/7zQ;->c:LX/7z4;

    invoke-virtual {v1, v0}, LX/7z4;->a(I)V

    .line 1280944
    :cond_0
    return-void
.end method
