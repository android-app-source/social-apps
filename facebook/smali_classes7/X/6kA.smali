.class public LX/6kA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final activityToken:Ljava/lang/String;

.field public final messageMetadata:LX/6kn;

.field public final replyType:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1135089
    new-instance v0, LX/1sv;

    const-string v1, "DeltaPageAdminReply"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kA;->b:LX/1sv;

    .line 1135090
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kA;->c:LX/1sw;

    .line 1135091
    new-instance v0, LX/1sw;

    const-string v1, "activityToken"

    const/16 v2, 0xb

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kA;->d:LX/1sw;

    .line 1135092
    new-instance v0, LX/1sw;

    const-string v1, "replyType"

    const/16 v2, 0x8

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kA;->e:LX/1sw;

    .line 1135093
    sput-boolean v4, LX/6kA;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6kn;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1135094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1135095
    iput-object p1, p0, LX/6kA;->messageMetadata:LX/6kn;

    .line 1135096
    iput-object p2, p0, LX/6kA;->activityToken:Ljava/lang/String;

    .line 1135097
    iput-object p3, p0, LX/6kA;->replyType:Ljava/lang/Integer;

    .line 1135098
    return-void
.end method

.method public static a(LX/6kA;)V
    .locals 4

    .prologue
    .line 1135099
    iget-object v0, p0, LX/6kA;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1135100
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kA;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1135101
    :cond_0
    iget-object v0, p0, LX/6kA;->replyType:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    sget-object v0, LX/6ks;->a:LX/1sn;

    iget-object v1, p0, LX/6kA;->replyType:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1135102
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'replyType\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6kA;->replyType:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1135103
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1135104
    if-eqz p2, :cond_2

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1135105
    :goto_0
    if-eqz p2, :cond_3

    const-string v0, "\n"

    move-object v1, v0

    .line 1135106
    :goto_1
    if-eqz p2, :cond_4

    const-string v0, " "

    .line 1135107
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaPageAdminReply"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1135108
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135109
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135110
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135111
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135112
    const-string v4, "messageMetadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135113
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135114
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135115
    iget-object v4, p0, LX/6kA;->messageMetadata:LX/6kn;

    if-nez v4, :cond_5

    .line 1135116
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135117
    :goto_3
    iget-object v4, p0, LX/6kA;->activityToken:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1135118
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135119
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135120
    const-string v4, "activityToken"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135121
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135122
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135123
    iget-object v4, p0, LX/6kA;->activityToken:Ljava/lang/String;

    if-nez v4, :cond_6

    .line 1135124
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135125
    :cond_0
    :goto_4
    iget-object v4, p0, LX/6kA;->replyType:Ljava/lang/Integer;

    if-eqz v4, :cond_1

    .line 1135126
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135127
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135128
    const-string v4, "replyType"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135129
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135130
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135131
    iget-object v0, p0, LX/6kA;->replyType:Ljava/lang/Integer;

    if-nez v0, :cond_7

    .line 1135132
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135133
    :cond_1
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135134
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135135
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1135136
    :cond_2
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1135137
    :cond_3
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1135138
    :cond_4
    const-string v0, ""

    goto/16 :goto_2

    .line 1135139
    :cond_5
    iget-object v4, p0, LX/6kA;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1135140
    :cond_6
    iget-object v4, p0, LX/6kA;->activityToken:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1135141
    :cond_7
    sget-object v0, LX/6ks;->b:Ljava/util/Map;

    iget-object v4, p0, LX/6kA;->replyType:Ljava/lang/Integer;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1135142
    if-eqz v0, :cond_8

    .line 1135143
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135144
    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135145
    :cond_8
    iget-object v4, p0, LX/6kA;->replyType:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1135146
    if-eqz v0, :cond_1

    .line 1135147
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1135148
    invoke-static {p0}, LX/6kA;->a(LX/6kA;)V

    .line 1135149
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1135150
    iget-object v0, p0, LX/6kA;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1135151
    sget-object v0, LX/6kA;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135152
    iget-object v0, p0, LX/6kA;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1135153
    :cond_0
    iget-object v0, p0, LX/6kA;->activityToken:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1135154
    iget-object v0, p0, LX/6kA;->activityToken:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1135155
    sget-object v0, LX/6kA;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135156
    iget-object v0, p0, LX/6kA;->activityToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1135157
    :cond_1
    iget-object v0, p0, LX/6kA;->replyType:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1135158
    iget-object v0, p0, LX/6kA;->replyType:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1135159
    sget-object v0, LX/6kA;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135160
    iget-object v0, p0, LX/6kA;->replyType:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1135161
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1135162
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1135163
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1135164
    if-nez p1, :cond_1

    .line 1135165
    :cond_0
    :goto_0
    return v0

    .line 1135166
    :cond_1
    instance-of v1, p1, LX/6kA;

    if-eqz v1, :cond_0

    .line 1135167
    check-cast p1, LX/6kA;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1135168
    if-nez p1, :cond_3

    .line 1135169
    :cond_2
    :goto_1
    move v0, v2

    .line 1135170
    goto :goto_0

    .line 1135171
    :cond_3
    iget-object v0, p0, LX/6kA;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1135172
    :goto_2
    iget-object v3, p1, LX/6kA;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1135173
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1135174
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135175
    iget-object v0, p0, LX/6kA;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6kA;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1135176
    :cond_5
    iget-object v0, p0, LX/6kA;->activityToken:Ljava/lang/String;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1135177
    :goto_4
    iget-object v3, p1, LX/6kA;->activityToken:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1135178
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1135179
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135180
    iget-object v0, p0, LX/6kA;->activityToken:Ljava/lang/String;

    iget-object v3, p1, LX/6kA;->activityToken:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1135181
    :cond_7
    iget-object v0, p0, LX/6kA;->replyType:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1135182
    :goto_6
    iget-object v3, p1, LX/6kA;->replyType:Ljava/lang/Integer;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1135183
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1135184
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135185
    iget-object v0, p0, LX/6kA;->replyType:Ljava/lang/Integer;

    iget-object v3, p1, LX/6kA;->replyType:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 1135186
    goto :goto_1

    :cond_a
    move v0, v2

    .line 1135187
    goto :goto_2

    :cond_b
    move v3, v2

    .line 1135188
    goto :goto_3

    :cond_c
    move v0, v2

    .line 1135189
    goto :goto_4

    :cond_d
    move v3, v2

    .line 1135190
    goto :goto_5

    :cond_e
    move v0, v2

    .line 1135191
    goto :goto_6

    :cond_f
    move v3, v2

    .line 1135192
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1135193
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1135194
    sget-boolean v0, LX/6kA;->a:Z

    .line 1135195
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kA;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1135196
    return-object v0
.end method
