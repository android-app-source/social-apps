.class public final LX/8Xk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/views/loading/CircularProgressView;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/loading/CircularProgressView;)V
    .locals 0

    .prologue
    .line 1355325
    iput-object p1, p0, LX/8Xk;->a:Lcom/facebook/quicksilver/views/loading/CircularProgressView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 1355326
    iget-object v1, p0, LX/8Xk;->a:Lcom/facebook/quicksilver/views/loading/CircularProgressView;

    const/high16 v2, 0x43870000    # 270.0f

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    add-float/2addr v0, v2

    .line 1355327
    iput v0, v1, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->l:F

    .line 1355328
    iget-object v0, p0, LX/8Xk;->a:Lcom/facebook/quicksilver/views/loading/CircularProgressView;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->postInvalidate()V

    .line 1355329
    return-void
.end method
