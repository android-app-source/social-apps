.class public final LX/846;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<*>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/2io;


# direct methods
.method public constructor <init>(LX/2io;LX/0Px;)V
    .locals 0

    .prologue
    .line 1290792
    iput-object p1, p0, LX/846;->b:LX/2io;

    iput-object p2, p0, LX/846;->a:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1290793
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1290794
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1290795
    if-eqz p1, :cond_0

    .line 1290796
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1290797
    if-eqz v0, :cond_0

    .line 1290798
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 1290799
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v0, v1, :cond_0

    .line 1290800
    iget-object v0, p0, LX/846;->b:LX/2io;

    iget-object v0, v0, LX/2io;->a:Ljava/util/Set;

    iget-object v1, p0, LX/846;->a:LX/0Px;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1290801
    :cond_0
    return-void
.end method
