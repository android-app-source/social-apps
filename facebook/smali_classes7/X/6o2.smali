.class public final LX/6o2;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1147757
    iput-object p1, p0, LX/6o2;->b:Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    iput-object p2, p0, LX/6o2;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 1

    .prologue
    .line 1147758
    iget-object v0, p0, LX/6o2;->b:Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    iget-object v0, v0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->s:LX/6ni;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1147759
    iget-object v0, p0, LX/6o2;->b:Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    iget-object v0, v0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->s:LX/6ni;

    invoke-interface {v0}, LX/6ni;->a()V

    .line 1147760
    iget-object v0, p0, LX/6o2;->b:Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1147761
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1147762
    iget-object v0, p0, LX/6o2;->b:Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    iget-object v0, v0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->s:LX/6ni;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1147763
    iget-object v0, p0, LX/6o2;->b:Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    iget-object v0, v0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->s:LX/6ni;

    iget-object v1, p0, LX/6o2;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/6ni;->a(Ljava/lang/String;)V

    .line 1147764
    iget-object v0, p0, LX/6o2;->b:Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1147765
    return-void
.end method
