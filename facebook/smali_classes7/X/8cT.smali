.class public LX/8cT;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final f:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/SearchRequestExecutor;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0TD;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8cF;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Re;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8i9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1374432
    const-string v0, "people_posts_suggestion"

    const-string v1, "people_photos_suggestion"

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/8cT;->f:LX/0Rf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1374433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1374434
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1374435
    iput-object v0, p0, LX/8cT;->a:LX/0Ot;

    .line 1374436
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1374437
    iput-object v0, p0, LX/8cT;->b:LX/0Ot;

    .line 1374438
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1374439
    iput-object v0, p0, LX/8cT;->c:LX/0Ot;

    .line 1374440
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1374441
    iput-object v0, p0, LX/8cT;->d:LX/0Ot;

    .line 1374442
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1374443
    iput-object v0, p0, LX/8cT;->e:LX/0Ot;

    .line 1374444
    return-void
.end method

.method private a(LX/8cH;Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8cH;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/8cK;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1374445
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1374446
    :cond_0
    :try_start_0
    invoke-virtual {p1}, LX/2TZ;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1374447
    invoke-virtual {p1}, LX/2TZ;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8cK;

    .line 1374448
    iget-object v1, p0, LX/8cT;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Re;

    .line 1374449
    const/4 v2, 0x1

    invoke-static {v1, p2, v2}, LX/2Re;->a(LX/2Re;Ljava/lang/String;Z)LX/0Px;

    move-result-object v2

    move-object v7, v2

    .line 1374450
    iget-object v1, p0, LX/8cT;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Re;

    .line 1374451
    iget-object v2, v0, LX/8cI;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1374452
    invoke-virtual {v1, v2}, LX/2Re;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v5, v3

    :goto_0
    if-ge v5, v9, :cond_0

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1374453
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v10

    move v4, v3

    :goto_1
    if-ge v4, v10, :cond_3

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1374454
    const/4 v11, 0x0

    const v12, 0x3ecccccd    # 0.4f

    invoke-static {v2, v1, v11, v12}, LX/8cT;->a(Ljava/lang/String;Ljava/lang/String;IF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1374455
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1374456
    const/4 v1, 0x1

    .line 1374457
    :goto_2
    if-nez v1, :cond_0

    .line 1374458
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_0

    .line 1374459
    :cond_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 1374460
    :cond_2
    invoke-virtual {p1}, LX/2TZ;->close()V

    return-object v6

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, LX/2TZ;->close()V

    throw v0

    :cond_3
    move v1, v3

    goto :goto_2
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;IF)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1374461
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    cmpg-float v0, p3, v4

    if-gtz v0, :cond_0

    move v0, p2

    :goto_0
    invoke-static {p0, v3, v0}, LX/8cU;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 1374462
    cmpg-float v3, p3, v4

    if-gtz v3, :cond_2

    .line 1374463
    if-gt v0, p2, :cond_1

    move v0, v1

    .line 1374464
    :goto_1
    return v0

    .line 1374465
    :cond_0
    const v0, 0x7ffffffe

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1374466
    goto :goto_1

    .line 1374467
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1374468
    if-lez v3, :cond_3

    int-to-float v0, v0

    int-to-float v3, v3

    div-float/2addr v0, v3

    cmpg-float v0, v0, p3

    if-gtz v0, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public static a$redex0(LX/8cT;LX/2TZ;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "LX/8cI;",
            "I:",
            "LX/2TZ",
            "<TM;>;>(TI;)",
            "Ljava/util/List",
            "<TM;>;"
        }
    .end annotation

    .prologue
    .line 1374469
    :try_start_0
    new-instance v0, LX/8cS;

    invoke-direct {v0, p0}, LX/8cS;-><init>(LX/8cT;)V

    invoke-static {p1}, LX/8cS;->a(LX/2TZ;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1374470
    invoke-virtual {p1}, LX/2TZ;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, LX/2TZ;->close()V

    throw v0
.end method

.method public static b(LX/0QB;)LX/8cT;
    .locals 6

    .prologue
    .line 1374471
    new-instance v0, LX/8cT;

    invoke-direct {v0}, LX/8cT;-><init>()V

    .line 1374472
    const/16 v1, 0x1421

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x32bf

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x295

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x3514

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1032

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    .line 1374473
    iput-object v1, v0, LX/8cT;->a:LX/0Ot;

    iput-object v2, v0, LX/8cT;->b:LX/0Ot;

    iput-object v3, v0, LX/8cT;->c:LX/0Ot;

    iput-object v4, v0, LX/8cT;->d:LX/0Ot;

    iput-object v5, v0, LX/8cT;->e:LX/0Ot;

    .line 1374474
    return-object v0
.end method

.method public static c(LX/8cT;Ljava/lang/String;I)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "LX/8cK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1374475
    iget-object v0, p0, LX/8cT;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8cF;

    .line 1374476
    iget-object v1, v0, LX/8cF;->a:LX/7Be;

    .line 1374477
    iget-object v3, v1, LX/7Be;->l:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ad;

    const/4 v4, 0x0

    .line 1374478
    sget-short v5, LX/100;->q:S

    invoke-interface {v3, v5, v4}, LX/0ad;->a(SZ)Z

    move-result v5

    if-nez v5, :cond_0

    sget-short v5, LX/100;->bx:S

    invoke-interface {v3, v5, v4}, LX/0ad;->a(SZ)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    const/4 v4, 0x1

    :cond_1
    move v3, v4

    .line 1374479
    if-eqz v3, :cond_2

    iget-object v3, v1, LX/7Be;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7CU;

    invoke-virtual {v3, p1}, LX/7CU;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v3

    :goto_0
    move-object v3, v3

    .line 1374480
    new-instance v4, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v4}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1374481
    const-string v5, "entities"

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1374482
    sget-object v5, LX/7Br;->a:LX/0U1;

    const-string v6, "entities_data"

    sget-object v7, LX/7Bn;->a:LX/0U1;

    sget-object v8, LX/7Bn;->b:LX/0U1;

    invoke-static {v3, v5, v6, v7, v8}, LX/7Be;->a(LX/0Px;LX/0U1;Ljava/lang/String;LX/0U1;LX/0U1;)Ljava/lang/String;

    move-result-object v6

    .line 1374483
    sget-object v5, LX/7Be;->c:[Ljava/lang/String;

    sget-object v7, LX/7Be;->b:Ljava/lang/String;

    move-object v3, v1

    move v8, p2

    invoke-static/range {v3 .. v8}, LX/7Be;->a(LX/7Be;Landroid/database/sqlite/SQLiteQueryBuilder;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v3

    move-object v1, v3

    .line 1374484
    new-instance v2, LX/8cE;

    invoke-direct {v2, v1}, LX/8cE;-><init>(Landroid/database/Cursor;)V

    move-object v0, v2

    .line 1374485
    invoke-static {p0, v0}, LX/8cT;->a$redex0(LX/8cT;LX/2TZ;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_2
    iget-object v3, v1, LX/7Be;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7CU;

    invoke-virtual {v3, p1}, LX/7CU;->b(Ljava/lang/String;)LX/0Px;

    move-result-object v3

    goto :goto_0
.end method

.method public static d(LX/8cT;Ljava/lang/String;I)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "LX/8cK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1374486
    invoke-static {p0, p1, p2}, LX/8cT;->c(LX/8cT;Ljava/lang/String;I)Ljava/util/List;

    move-result-object v1

    .line 1374487
    iget-object v0, p0, LX/8cT;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/100;->bX:S

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1374488
    iget-object v0, p0, LX/8cT;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8i9;

    invoke-virtual {v0, p1}, LX/8i9;->b(Ljava/lang/String;)LX/8i3;

    move-result-object v2

    .line 1374489
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1374490
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8cK;

    .line 1374491
    iget-object p2, v0, LX/8cI;->b:Ljava/lang/String;

    move-object p2, p2

    .line 1374492
    invoke-virtual {v2, p2}, LX/8i3;->a(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 1374493
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1374494
    :cond_1
    move-object v0, v3

    .line 1374495
    :goto_1
    return-object v0

    .line 1374496
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x5

    if-ge v0, v2, :cond_5

    .line 1374497
    iget-object v0, p0, LX/8cT;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8cF;

    .line 1374498
    iget-object v4, v0, LX/8cF;->a:LX/7Be;

    .line 1374499
    new-instance v7, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v7}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1374500
    const-string v6, "entities"

    invoke-virtual {v7, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1374501
    new-instance v9, LX/0Pz;

    invoke-direct {v9}, LX/0Pz;-><init>()V

    .line 1374502
    iget-object v6, v4, LX/7Be;->k:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/7CV;

    invoke-virtual {v6, p1}, LX/7CV;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    const/4 v6, 0x0

    move v8, v6

    :goto_2
    if-ge v8, v11, :cond_3

    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1374503
    invoke-static {v6}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1374504
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    goto :goto_2

    .line 1374505
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v8, 0xc8

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    sget-object v8, LX/7Br;->a:LX/0U1;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " IN ( SELECT "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v8, LX/7Bp;->a:LX/0U1;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " FROM entities_phonetic_data"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " WHERE "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v8, LX/7Bp;->b:LX/0U1;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " IN ("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ","

    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "))"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v9, v6

    .line 1374506
    sget-object v8, LX/7Be;->c:[Ljava/lang/String;

    sget-object v10, LX/7Be;->b:Ljava/lang/String;

    move-object v6, v4

    move v11, p2

    invoke-static/range {v6 .. v11}, LX/7Be;->a(LX/7Be;Landroid/database/sqlite/SQLiteQueryBuilder;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v6

    move-object v4, v6

    .line 1374507
    new-instance v5, LX/8cH;

    invoke-direct {v5, v4}, LX/8cH;-><init>(Landroid/database/Cursor;)V

    move-object v0, v5

    .line 1374508
    invoke-direct {p0, v0, p1}, LX/8cT;->a(LX/8cH;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1374509
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8cK;

    .line 1374510
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1374511
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    move-object v0, v1

    .line 1374512
    goto/16 :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "LX/8cK;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1374513
    iget-object v0, p0, LX/8cT;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TD;

    new-instance v1, LX/8cN;

    invoke-direct {v1, p0, p1, p2}, LX/8cN;-><init>(LX/8cT;Ljava/lang/String;I)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
