.class public final LX/8SK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/spinner/AudienceSpinner;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/spinner/AudienceSpinner;)V
    .locals 0

    .prologue
    .line 1346458
    iput-object p1, p0, LX/8SK;->a:Lcom/facebook/privacy/spinner/AudienceSpinner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const v0, -0x2a5b56bc

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1346459
    iget-object v0, p0, LX/8SK;->a:Lcom/facebook/privacy/spinner/AudienceSpinner;

    iget-object v0, v0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    .line 1346460
    iget-boolean v2, v0, LX/0ht;->r:Z

    move v0, v2

    .line 1346461
    if-nez v0, :cond_0

    .line 1346462
    iget-object v0, p0, LX/8SK;->a:Lcom/facebook/privacy/spinner/AudienceSpinner;

    iget-object v0, v0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    iget-object v2, p0, LX/8SK;->a:Lcom/facebook/privacy/spinner/AudienceSpinner;

    iget-object v2, v2, Lcom/facebook/privacy/spinner/AudienceSpinner;->b:Lcom/facebook/privacy/ui/PrivacyOptionView;

    invoke-virtual {v0, v2}, LX/0ht;->f(Landroid/view/View;)V

    .line 1346463
    iget-object v0, p0, LX/8SK;->a:Lcom/facebook/privacy/spinner/AudienceSpinner;

    iget-object v0, v0, Lcom/facebook/privacy/spinner/AudienceSpinner;->m:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 1346464
    iget-object v0, p0, LX/8SK;->a:Lcom/facebook/privacy/spinner/AudienceSpinner;

    iget-object v0, v0, Lcom/facebook/privacy/spinner/AudienceSpinner;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8SO;

    .line 1346465
    if-eqz v0, :cond_0

    .line 1346466
    invoke-interface {v0, v4}, LX/8SO;->a(Z)V

    .line 1346467
    :cond_0
    const v0, 0x21c750bb

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
