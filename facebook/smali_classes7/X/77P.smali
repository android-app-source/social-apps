.class public LX/77P;
.super LX/2g7;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/77P;


# instance fields
.field private final a:LX/0kb;


# direct methods
.method public constructor <init>(LX/0kb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171797
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1171798
    iput-object p1, p0, LX/77P;->a:LX/0kb;

    .line 1171799
    return-void
.end method

.method public static a(LX/0QB;)LX/77P;
    .locals 4

    .prologue
    .line 1171800
    sget-object v0, LX/77P;->b:LX/77P;

    if-nez v0, :cond_1

    .line 1171801
    const-class v1, LX/77P;

    monitor-enter v1

    .line 1171802
    :try_start_0
    sget-object v0, LX/77P;->b:LX/77P;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1171803
    if-eqz v2, :cond_0

    .line 1171804
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1171805
    new-instance p0, LX/77P;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-direct {p0, v3}, LX/77P;-><init>(LX/0kb;)V

    .line 1171806
    move-object v0, p0

    .line 1171807
    sput-object v0, LX/77P;->b:LX/77P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1171808
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1171809
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1171810
    :cond_1
    sget-object v0, LX/77P;->b:LX/77P;

    return-object v0

    .line 1171811
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1171812
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 2
    .param p1    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1171813
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1171814
    iget-object v0, p0, LX/77P;->a:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    iget-object v1, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
