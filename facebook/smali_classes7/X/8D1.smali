.class public final enum LX/8D1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8D1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8D1;

.field public static final enum COMMENT:LX/8D1;

.field public static final enum COMMENT_LIKE:LX/8D1;

.field public static final enum EVENT_RSVP:LX/8D1;

.field public static final enum FRIENDING:LX/8D1;

.field public static final enum POST:LX/8D1;

.field public static final enum REACTION:LX/8D1;

.field public static final enum SHARE:LX/8D1;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1312133
    new-instance v0, LX/8D1;

    const-string v1, "COMMENT"

    invoke-direct {v0, v1, v3}, LX/8D1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D1;->COMMENT:LX/8D1;

    .line 1312134
    new-instance v0, LX/8D1;

    const-string v1, "COMMENT_LIKE"

    invoke-direct {v0, v1, v4}, LX/8D1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D1;->COMMENT_LIKE:LX/8D1;

    .line 1312135
    new-instance v0, LX/8D1;

    const-string v1, "EVENT_RSVP"

    invoke-direct {v0, v1, v5}, LX/8D1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D1;->EVENT_RSVP:LX/8D1;

    .line 1312136
    new-instance v0, LX/8D1;

    const-string v1, "FRIENDING"

    invoke-direct {v0, v1, v6}, LX/8D1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D1;->FRIENDING:LX/8D1;

    .line 1312137
    new-instance v0, LX/8D1;

    const-string v1, "POST"

    invoke-direct {v0, v1, v7}, LX/8D1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D1;->POST:LX/8D1;

    .line 1312138
    new-instance v0, LX/8D1;

    const-string v1, "REACTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8D1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D1;->REACTION:LX/8D1;

    .line 1312139
    new-instance v0, LX/8D1;

    const-string v1, "SHARE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8D1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D1;->SHARE:LX/8D1;

    .line 1312140
    const/4 v0, 0x7

    new-array v0, v0, [LX/8D1;

    sget-object v1, LX/8D1;->COMMENT:LX/8D1;

    aput-object v1, v0, v3

    sget-object v1, LX/8D1;->COMMENT_LIKE:LX/8D1;

    aput-object v1, v0, v4

    sget-object v1, LX/8D1;->EVENT_RSVP:LX/8D1;

    aput-object v1, v0, v5

    sget-object v1, LX/8D1;->FRIENDING:LX/8D1;

    aput-object v1, v0, v6

    sget-object v1, LX/8D1;->POST:LX/8D1;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8D1;->REACTION:LX/8D1;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8D1;->SHARE:LX/8D1;

    aput-object v2, v0, v1

    sput-object v0, LX/8D1;->$VALUES:[LX/8D1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1312141
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8D1;
    .locals 1

    .prologue
    .line 1312142
    const-class v0, LX/8D1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8D1;

    return-object v0
.end method

.method public static values()[LX/8D1;
    .locals 1

    .prologue
    .line 1312143
    sget-object v0, LX/8D1;->$VALUES:[LX/8D1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8D1;

    return-object v0
.end method
