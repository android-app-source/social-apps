.class public final LX/7Pj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:LX/7Pm;

.field public e:LX/7Ps;

.field public final synthetic f:LX/7Po;

.field public g:LX/7Pl;

.field private h:LX/7Oi;

.field public i:LX/3Dd;

.field public j:LX/2WF;


# direct methods
.method public constructor <init>(LX/7Po;Landroid/net/Uri;ILjava/lang/String;LX/7Pm;)V
    .locals 0

    .prologue
    .line 1203176
    iput-object p1, p0, LX/7Pj;->f:LX/7Po;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1203177
    iput-object p2, p0, LX/7Pj;->a:Landroid/net/Uri;

    .line 1203178
    iput p3, p0, LX/7Pj;->b:I

    .line 1203179
    iput-object p4, p0, LX/7Pj;->c:Ljava/lang/String;

    .line 1203180
    iput-object p5, p0, LX/7Pj;->d:LX/7Pm;

    .line 1203181
    return-void
.end method

.method public static d(LX/7Pj;)V
    .locals 2

    .prologue
    .line 1203174
    iget-object v0, p0, LX/7Pj;->f:LX/7Po;

    iget-object v0, v0, LX/7Po;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lv;

    iget-object v1, p0, LX/7Pj;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1Lv;->a(Landroid/net/Uri;)V

    .line 1203175
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/OutputStream;)V
    .locals 4

    .prologue
    .line 1203145
    :try_start_0
    new-instance v0, LX/7Pa;

    invoke-direct {v0, p1}, LX/7Pa;-><init>(Ljava/io/OutputStream;)V

    .line 1203146
    new-instance v1, LX/45i;

    new-instance v2, LX/7Pn;

    iget-object v3, p0, LX/7Pj;->f:LX/7Po;

    iget-object p1, p0, LX/7Pj;->g:LX/7Pl;

    invoke-direct {v2, v3, p1}, LX/7Pn;-><init>(LX/7Po;LX/7Pl;)V

    invoke-direct {v1, v0, v2}, LX/45i;-><init>(Ljava/io/OutputStream;LX/3DY;)V

    move-object v0, v1

    .line 1203147
    iget-object v1, p0, LX/7Pj;->e:LX/7Ps;

    .line 1203148
    iget-object v2, v1, LX/7Ps;->p:LX/3DY;

    move-object v2, v2

    .line 1203149
    if-eqz v2, :cond_0

    .line 1203150
    new-instance v1, LX/45i;

    invoke-direct {v1, v0, v2}, LX/45i;-><init>(Ljava/io/OutputStream;LX/3DY;)V

    move-object v0, v1

    .line 1203151
    :cond_0
    move-object v0, v0

    .line 1203152
    iget-object v1, p0, LX/7Pj;->h:LX/7Oi;

    iget-object v2, p0, LX/7Pj;->j:LX/2WF;

    invoke-interface {v1, v2, v0}, LX/7Oi;->a(LX/2WF;Ljava/io/OutputStream;)J

    .line 1203153
    iget-object v0, p0, LX/7Pj;->e:LX/7Ps;

    invoke-virtual {v0}, LX/7Ps;->c()V
    :try_end_0
    .catch LX/7P1; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/7Pc; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/7PO; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1203154
    invoke-static {p0}, LX/7Pj;->d(LX/7Pj;)V

    .line 1203155
    return-void

    .line 1203156
    :catch_0
    move-exception v0

    .line 1203157
    :try_start_1
    iget-object v1, p0, LX/7Pj;->e:LX/7Ps;

    invoke-virtual {v1, v0}, LX/7Ps;->a(Ljava/lang/Throwable;)V

    .line 1203158
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1203159
    :catchall_0
    move-exception v0

    invoke-static {p0}, LX/7Pj;->d(LX/7Pj;)V

    throw v0

    .line 1203160
    :catch_1
    move-exception v0

    .line 1203161
    iget-object v1, p0, LX/7Pj;->e:LX/7Ps;

    invoke-virtual {v1, v0}, LX/7Ps;->a(Ljava/lang/Throwable;)V

    .line 1203162
    throw v0

    .line 1203163
    :catch_2
    move-exception v0

    .line 1203164
    iget-object v1, p0, LX/7Pj;->e:LX/7Ps;

    invoke-virtual {v1, v0}, LX/7Ps;->a(Ljava/lang/Throwable;)V

    .line 1203165
    throw v0

    .line 1203166
    :catch_3
    move-exception v0

    .line 1203167
    sget-object v1, LX/1m0;->c:Ljava/lang/String;

    const-string v2, "Error (not-network) when sending content to client"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1203168
    iget-object v1, p0, LX/7Pj;->e:LX/7Ps;

    invoke-virtual {v1, v0}, LX/7Ps;->a(Ljava/lang/Throwable;)V

    .line 1203169
    throw v0

    .line 1203170
    :catch_4
    move-exception v0

    .line 1203171
    sget-object v1, LX/1m0;->c:Ljava/lang/String;

    const-string v2, "Unknown error when sending content to client"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1203172
    iget-object v1, p0, LX/7Pj;->e:LX/7Ps;

    invoke-virtual {v1, v0}, LX/7Ps;->a(Ljava/lang/Throwable;)V

    .line 1203173
    throw v0
.end method

.method public final a(ZZZ)V
    .locals 13

    .prologue
    .line 1203088
    new-instance v0, LX/7Pl;

    invoke-direct {v0}, LX/7Pl;-><init>()V

    iput-object v0, p0, LX/7Pj;->g:LX/7Pl;

    .line 1203089
    const-wide/16 v0, -0x1

    .line 1203090
    :try_start_0
    iget-object v2, p0, LX/7Pj;->f:LX/7Po;

    iget-object v3, p0, LX/7Pj;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, LX/7Po;->a(Landroid/net/Uri;)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-wide v0

    .line 1203091
    :goto_0
    iget-object v2, p0, LX/7Pj;->f:LX/7Po;

    iget-object v3, p0, LX/7Pj;->c:Ljava/lang/String;

    new-instance v4, LX/2qO;

    iget-object v5, p0, LX/7Pj;->g:LX/7Pl;

    invoke-direct {v4, v5, v0, v1}, LX/2qO;-><init>(LX/7Pl;J)V

    invoke-static {v2, v3, v4}, LX/7Po;->a$redex0(LX/7Po;Ljava/lang/String;LX/1AD;)V

    .line 1203092
    iget-object v0, p0, LX/7Pj;->f:LX/7Po;

    iget-object v1, p0, LX/7Pj;->c:Ljava/lang/String;

    new-instance v2, LX/2qP;

    invoke-direct {v2}, LX/2qP;-><init>()V

    invoke-static {v0, v1, v2}, LX/7Po;->a$redex0(LX/7Po;Ljava/lang/String;LX/1AD;)V

    .line 1203093
    iget-object v0, p0, LX/7Pj;->f:LX/7Po;

    iget-object v0, v0, LX/7Po;->e:LX/1m2;

    iget-object v1, p0, LX/7Pj;->c:Ljava/lang/String;

    iget-object v2, p0, LX/7Pj;->a:Landroid/net/Uri;

    iget v3, p0, LX/7Pj;->b:I

    invoke-virtual {v0, v1, v2, v3}, LX/1m2;->a(Ljava/lang/String;Landroid/net/Uri;I)LX/7Ps;

    move-result-object v0

    iput-object v0, p0, LX/7Pj;->e:LX/7Ps;

    .line 1203094
    :try_start_1
    new-instance v1, Ljava/net/URL;

    iget-object v0, p0, LX/7Pj;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1203095
    iget-object v0, p0, LX/7Pj;->f:LX/7Po;

    iget-object v0, v0, LX/7Po;->f:LX/1Lt;

    iget-object v2, p0, LX/7Pj;->a:Landroid/net/Uri;

    invoke-virtual {v0, v2}, LX/1Lt;->a(Landroid/net/Uri;)LX/37C;

    move-result-object v7

    .line 1203096
    iget-object v0, p0, LX/7Pj;->f:LX/7Po;

    iget-object v0, v0, LX/7Po;->p:LX/19Z;

    iget v2, p0, LX/7Pj;->b:I

    invoke-virtual {v0, v2}, LX/19Z;->b(I)LX/7IG;

    move-result-object v0

    .line 1203097
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/7IG;->e()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    .line 1203098
    :goto_1
    iget-object v0, p0, LX/7Pj;->f:LX/7Po;

    iget v3, p0, LX/7Pj;->b:I

    invoke-static {v0, v3}, LX/7Po;->a$redex0(LX/7Po;I)LX/7Pw;

    move-result-object v3

    .line 1203099
    iget-object v0, p0, LX/7Pj;->f:LX/7Po;

    iget-object v0, v0, LX/7Po;->u:LX/0wr;

    sget-object v4, LX/0wr;->HTTP_1RT_INTERCEPTING:LX/0wr;

    if-eq v0, v4, :cond_2

    const/4 v0, 0x1

    move v4, v0

    .line 1203100
    :goto_2
    iget-object v0, p0, LX/7Pj;->f:LX/7Po;

    iget-object v0, v0, LX/7Po;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lv;

    iget-object v5, p0, LX/7Pj;->a:Landroid/net/Uri;

    invoke-virtual {v0, v5, v4}, LX/1Lv;->a(Landroid/net/Uri;Z)LX/7Ou;
    :try_end_1
    .catch LX/7Ph; {:try_start_1 .. :try_end_1} :catch_1
    .catch LX/7P1; {:try_start_1 .. :try_end_1} :catch_4
    .catch LX/7PO; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_6

    move-result-object v9

    .line 1203101
    :try_start_2
    iget-object v0, p0, LX/7Pj;->f:LX/7Po;

    iget-object v4, p0, LX/7Pj;->c:Ljava/lang/String;

    iget-object v5, p0, LX/7Pj;->e:LX/7Ps;

    iget-object v6, p0, LX/7Pj;->g:LX/7Pl;

    iget-object v8, p0, LX/7Pj;->d:LX/7Pm;

    move v10, p1

    move v11, p2

    move/from16 v12, p3

    invoke-static/range {v0 .. v12}, LX/7Po;->a(LX/7Po;Ljava/net/URL;Lcom/facebook/common/callercontext/CallerContext;LX/7Pw;Ljava/lang/String;LX/7Ps;LX/7Pl;LX/37C;LX/7Pm;LX/7Ou;ZZZ)LX/7Oi;

    move-result-object v0

    iput-object v0, p0, LX/7Pj;->h:LX/7Oi;

    .line 1203102
    iget-object v0, p0, LX/7Pj;->h:LX/7Oi;

    invoke-interface {v0}, LX/7Oi;->a()LX/3Dd;

    move-result-object v0

    iput-object v0, p0, LX/7Pj;->i:LX/3Dd;

    .line 1203103
    iget-object v0, p0, LX/7Pj;->i:LX/3Dd;

    invoke-virtual {v3, v0}, LX/7Pw;->a(LX/3Dd;)V

    .line 1203104
    iget-object v0, p0, LX/7Pj;->i:LX/3Dd;

    iget-wide v0, v0, LX/3Dd;->a:J

    .line 1203105
    iget-object v2, p0, LX/7Pj;->d:LX/7Pm;

    iget-object v2, v2, LX/7Pm;->d:LX/2WF;

    iput-object v2, p0, LX/7Pj;->j:LX/2WF;

    .line 1203106
    iget-object v2, p0, LX/7Pj;->j:LX/2WF;

    if-nez v2, :cond_4

    .line 1203107
    iget-object v2, p0, LX/7Pj;->d:LX/7Pm;

    iget-wide v4, v2, LX/7Pm;->b:J

    cmp-long v2, v4, v0

    if-ltz v2, :cond_3

    .line 1203108
    new-instance v2, LX/7Ph;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, LX/7Pj;->d:LX/7Pm;

    iget-wide v4, v4, LX/7Pm;->b:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0, v1}, LX/7Ph;-><init>(Ljava/lang/String;J)V

    throw v2
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catch LX/7Ph; {:try_start_2 .. :try_end_2} :catch_1
    .catch LX/7P1; {:try_start_2 .. :try_end_2} :catch_4
    .catch LX/7PO; {:try_start_2 .. :try_end_2} :catch_5

    .line 1203109
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1203110
    if-eqz v9, :cond_0

    .line 1203111
    :try_start_3
    iget-object v0, v9, LX/7Ou;->b:LX/7Os;

    invoke-virtual {v0}, LX/7Os;->b()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch LX/7Ph; {:try_start_3 .. :try_end_3} :catch_1
    .catch LX/7P1; {:try_start_3 .. :try_end_3} :catch_4
    .catch LX/7PO; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_6

    .line 1203112
    :cond_0
    :goto_3
    :try_start_4
    iget-object v0, p0, LX/7Pj;->f:LX/7Po;

    iget-object v0, v0, LX/7Po;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lv;

    iget-object v2, p0, LX/7Pj;->a:Landroid/net/Uri;

    invoke-virtual {v0, v2}, LX/1Lv;->a(Landroid/net/Uri;)V

    .line 1203113
    const-class v0, Ljava/io/IOException;

    invoke-static {v1, v0}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 1203114
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Unexpected exception!"

    invoke-direct {v0, v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_4
    .catch LX/7Ph; {:try_start_4 .. :try_end_4} :catch_1
    .catch LX/7P1; {:try_start_4 .. :try_end_4} :catch_4
    .catch LX/7PO; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_6

    .line 1203115
    :catch_1
    move-exception v0

    .line 1203116
    sget-object v1, LX/1m0;->c:Ljava/lang/String;

    const-string v2, "Invalid range specified"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1203117
    iget-object v1, p0, LX/7Pj;->f:LX/7Po;

    iget-object v1, v1, LX/7Po;->d:LX/03V;

    sget-object v2, LX/1m0;->c:Ljava/lang/String;

    const-string v3, "Invalid range specified"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1203118
    iget-object v1, p0, LX/7Pj;->e:LX/7Ps;

    invoke-virtual {v1, v0}, LX/7Ps;->b(Ljava/lang/Throwable;)V

    .line 1203119
    throw v0

    .line 1203120
    :catch_2
    move-exception v2

    .line 1203121
    invoke-static {}, LX/1m0;->d()Ljava/lang/String;

    .line 1203122
    iget-object v3, p0, LX/7Pj;->f:LX/7Po;

    iget-object v3, v3, LX/7Po;->d:LX/03V;

    sget-object v4, LX/1m0;->c:Ljava/lang/String;

    const-string v5, "Error checking video cache for firing RequestBeginEvent"

    invoke-virtual {v3, v4, v5, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1203123
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1203124
    :cond_2
    const/4 v0, 0x0

    move v4, v0

    goto/16 :goto_2

    .line 1203125
    :cond_3
    :try_start_5
    new-instance v2, LX/2WF;

    iget-object v4, p0, LX/7Pj;->d:LX/7Pm;

    iget-wide v4, v4, LX/7Pm;->b:J

    invoke-direct {v2, v4, v5, v0, v1}, LX/2WF;-><init>(JJ)V

    iput-object v2, p0, LX/7Pj;->j:LX/2WF;

    .line 1203126
    :cond_4
    invoke-static {}, LX/1m0;->d()Ljava/lang/String;

    iget-object v0, p0, LX/7Pj;->d:LX/7Pm;

    iget-boolean v0, v0, LX/7Pm;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1203127
    iget-object v0, p0, LX/7Pj;->j:LX/2WF;

    invoke-virtual {v3, v0}, LX/7Pw;->a(LX/2WF;)V

    .line 1203128
    iget-object v0, p0, LX/7Pj;->e:LX/7Ps;

    iget-object v1, p0, LX/7Pj;->d:LX/7Pm;

    iget-boolean v1, v1, LX/7Pm;->a:Z

    iget-object v2, p0, LX/7Pj;->j:LX/2WF;

    invoke-virtual {v0, v1, v2}, LX/7Ps;->a(ZLX/2WF;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catch LX/7Ph; {:try_start_5 .. :try_end_5} :catch_1
    .catch LX/7P1; {:try_start_5 .. :try_end_5} :catch_4
    .catch LX/7PO; {:try_start_5 .. :try_end_5} :catch_5

    .line 1203129
    return-void

    .line 1203130
    :catch_3
    :try_start_6
    invoke-static {}, LX/1m0;->d()Ljava/lang/String;
    :try_end_6
    .catch LX/7Ph; {:try_start_6 .. :try_end_6} :catch_1
    .catch LX/7P1; {:try_start_6 .. :try_end_6} :catch_4
    .catch LX/7PO; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_3

    .line 1203131
    :catch_4
    move-exception v0

    .line 1203132
    invoke-static {}, LX/1m0;->d()Ljava/lang/String;

    .line 1203133
    iget-object v1, p0, LX/7Pj;->e:LX/7Ps;

    invoke-virtual {v1, v0}, LX/7Ps;->b(Ljava/lang/Throwable;)V

    .line 1203134
    throw v0

    .line 1203135
    :catch_5
    move-exception v0

    .line 1203136
    invoke-static {}, LX/1m0;->d()Ljava/lang/String;

    .line 1203137
    iget-object v1, p0, LX/7Pj;->e:LX/7Ps;

    invoke-virtual {v1, v0}, LX/7Ps;->a(Ljava/lang/Throwable;)V

    .line 1203138
    throw v0

    .line 1203139
    :catch_6
    move-exception v0

    .line 1203140
    sget-object v1, LX/1m0;->c:Ljava/lang/String;

    const-string v2, "Error handling local request"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1203141
    iget-object v1, p0, LX/7Pj;->f:LX/7Po;

    iget-object v1, v1, LX/7Po;->d:LX/03V;

    sget-object v2, LX/1m0;->c:Ljava/lang/String;

    const-string v3, "Error handling local request"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1203142
    iget-object v1, p0, LX/7Pj;->e:LX/7Ps;

    invoke-virtual {v1, v0}, LX/7Ps;->b(Ljava/lang/Throwable;)V

    .line 1203143
    const-class v1, Ljava/io/IOException;

    invoke-static {v0, v1}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 1203144
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Unexpected exception"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()LX/3Dd;
    .locals 1

    .prologue
    .line 1203085
    iget-object v0, p0, LX/7Pj;->i:LX/3Dd;

    return-object v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1203086
    invoke-static {p0}, LX/7Pj;->d(LX/7Pj;)V

    .line 1203087
    return-void
.end method
