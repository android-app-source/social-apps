.class public final LX/7vk;
.super LX/2h1;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/checkout/model/CheckoutData;

.field public final synthetic b:LX/7vl;


# direct methods
.method public constructor <init>(LX/7vl;Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 0

    .prologue
    .line 1275075
    iput-object p1, p0, LX/7vk;->b:LX/7vl;

    iput-object p2, p0, LX/7vk;->a:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 1275080
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1275076
    iget-object v0, p0, LX/7vk;->a:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->p()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1275077
    iget-object v0, p0, LX/7vk;->a:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;

    iget-object v0, v0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275078
    iget-object v1, p0, LX/7vk;->b:LX/7vl;

    iget-object v1, v1, LX/7vl;->c:LX/6qb;

    invoke-interface {v1, v0}, LX/6qb;->a(Landroid/os/Parcelable;)V

    .line 1275079
    :cond_0
    return-void
.end method
