.class public final LX/8QJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public final d:Z


# direct methods
.method public constructor <init>(LX/0Px;LX/0Px;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1342838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1342839
    iput-object p1, p0, LX/8QJ;->a:LX/0Px;

    .line 1342840
    iput-object p2, p0, LX/8QJ;->b:LX/0Px;

    .line 1342841
    const/4 v1, 0x0

    .line 1342842
    iget-object v0, p0, LX/8QJ;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p1

    move v2, v1

    :goto_0
    if-ge v2, p1, :cond_2

    iget-object v0, p0, LX/8QJ;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1342843
    invoke-static {v0, p3}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 1342844
    :cond_0
    :goto_1
    move-object v0, v0

    .line 1342845
    iput-object v0, p0, LX/8QJ;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1342846
    iput-boolean p4, p0, LX/8QJ;->d:Z

    .line 1342847
    return-void

    .line 1342848
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1342849
    :cond_2
    iget-object v0, p0, LX/8QJ;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    :goto_2
    if-ge v1, v2, :cond_3

    iget-object v0, p0, LX/8QJ;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1342850
    invoke-static {v0, p3}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 1342851
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1342852
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1342853
    if-nez p1, :cond_1

    .line 1342854
    :cond_0
    :goto_0
    return v3

    .line 1342855
    :cond_1
    if-ne p1, p0, :cond_2

    move v3, v4

    .line 1342856
    goto :goto_0

    .line 1342857
    :cond_2
    instance-of v0, p1, LX/8QJ;

    if-eqz v0, :cond_0

    .line 1342858
    check-cast p1, LX/8QJ;

    .line 1342859
    iget-object v0, p0, LX/8QJ;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v1, p1, LX/8QJ;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    move v2, v3

    .line 1342860
    :goto_1
    iget-object v0, p0, LX/8QJ;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1342861
    iget-object v0, p0, LX/8QJ;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oS;

    iget-object v1, p1, LX/8QJ;->a:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1oS;

    invoke-static {v0, v1}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v0

    .line 1342862
    if-eqz v0, :cond_0

    .line 1342863
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1342864
    :cond_3
    iget-object v0, p0, LX/8QJ;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v1, p1, LX/8QJ;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    move v2, v3

    .line 1342865
    :goto_2
    iget-object v0, p0, LX/8QJ;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 1342866
    iget-object v0, p0, LX/8QJ;->b:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oS;

    iget-object v1, p1, LX/8QJ;->b:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1oS;

    invoke-static {v0, v1}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v0

    .line 1342867
    if-eqz v0, :cond_0

    .line 1342868
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1342869
    :cond_4
    iget-object v0, p0, LX/8QJ;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v1, p1, LX/8QJ;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0, v1}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1342870
    iget-boolean v0, p0, LX/8QJ;->d:Z

    iget-boolean v1, p1, LX/8QJ;->d:Z

    if-ne v0, v1, :cond_0

    move v3, v4

    .line 1342871
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1342872
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/8QJ;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/8QJ;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1342873
    const-class v0, LX/8QJ;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "basicPrivacyOptions"

    iget-object v2, p0, LX/8QJ;->a:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "friendListOptions"

    iget-object v2, p0, LX/8QJ;->b:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "selectedPrivacyOption"

    iget-object v2, p0, LX/8QJ;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "showTagExpansionOption"

    iget-boolean v2, p0, LX/8QJ;->d:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
