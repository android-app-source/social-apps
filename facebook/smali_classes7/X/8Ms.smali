.class public LX/8Ms;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-com.google.common.util.concurrent.Futures.addCallback"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/8Ms;


# instance fields
.field public final a:LX/0tX;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1RW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1335331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1335332
    iput-object p1, p0, LX/8Ms;->a:LX/0tX;

    .line 1335333
    iput-object p2, p0, LX/8Ms;->b:LX/0Or;

    .line 1335334
    return-void
.end method

.method public static a(LX/0QB;)LX/8Ms;
    .locals 5

    .prologue
    .line 1335316
    sget-object v0, LX/8Ms;->d:LX/8Ms;

    if-nez v0, :cond_1

    .line 1335317
    const-class v1, LX/8Ms;

    monitor-enter v1

    .line 1335318
    :try_start_0
    sget-object v0, LX/8Ms;->d:LX/8Ms;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1335319
    if-eqz v2, :cond_0

    .line 1335320
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1335321
    new-instance v4, LX/8Ms;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    const/16 p0, 0x15e7

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/8Ms;-><init>(LX/0tX;LX/0Or;)V

    .line 1335322
    invoke-static {v0}, LX/1RW;->b(LX/0QB;)LX/1RW;

    move-result-object v3

    check-cast v3, LX/1RW;

    .line 1335323
    iput-object v3, v4, LX/8Ms;->c:LX/1RW;

    .line 1335324
    move-object v0, v4

    .line 1335325
    sput-object v0, LX/8Ms;->d:LX/8Ms;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1335326
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1335327
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1335328
    :cond_1
    sget-object v0, LX/8Ms;->d:LX/8Ms;

    return-object v0

    .line 1335329
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1335330
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/8Ms;LX/7mj;)LX/4ED;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1335277
    new-instance v3, LX/4ED;

    invoke-direct {v3}, LX/4ED;-><init>()V

    iget-object v0, p0, LX/8Ms;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1335278
    const-string p0, "actor_id"

    invoke-virtual {v3, p0, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335279
    move-object v0, v3

    .line 1335280
    invoke-virtual {p1}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v3

    .line 1335281
    const-string p0, "draft_id"

    invoke-virtual {v0, p0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335282
    move-object v3, v0

    .line 1335283
    invoke-virtual {p1}, LX/7mi;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1335284
    const-string p0, "message"

    invoke-virtual {v3, p0, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335285
    move-object v0, v3

    .line 1335286
    invoke-virtual {p1}, LX/7mi;->j()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 1335287
    const-string p0, "photo_count"

    invoke-virtual {v0, p0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1335288
    move-object v0, v0

    .line 1335289
    invoke-virtual {p1}, LX/7mi;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 1335290
    const-string p0, "video_count"

    invoke-virtual {v0, p0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1335291
    move-object v0, v0

    .line 1335292
    iget-object v3, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v3, v3

    .line 1335293
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v3

    if-nez v3, :cond_3

    const/4 v3, 0x0

    :goto_0
    move v3, v3

    .line 1335294
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 1335295
    const-string p0, "tag_count"

    invoke-virtual {v0, p0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1335296
    move-object v3, v0

    .line 1335297
    iget-object v0, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v0

    .line 1335298
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1335299
    const-string p0, "minutiae"

    invoke-virtual {v3, p0, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1335300
    move-object v3, v3

    .line 1335301
    iget-object v0, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v0

    .line 1335302
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aC()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1335303
    const-string p0, "sticker"

    invoke-virtual {v3, p0, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1335304
    move-object v0, v3

    .line 1335305
    iget-object v3, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v3, v3

    .line 1335306
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v3

    if-eqz v3, :cond_2

    :goto_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1335307
    const-string v2, "checkin"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1335308
    move-object v0, v0

    .line 1335309
    iget-object v1, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1335310
    invoke-static {v1}, LX/17E;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1335311
    const-string v2, "link_attachment"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1335312
    move-object v0, v0

    .line 1335313
    return-object v0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_2

    :cond_2
    move v1, v2

    goto :goto_3

    .line 1335314
    :cond_3
    iget-object v3, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v3, v3

    .line 1335315
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    goto :goto_0
.end method
