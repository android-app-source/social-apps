.class public final enum LX/7Ve;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Ve;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Ve;

.field public static final enum FREE_MESSENGER:LX/7Ve;

.field public static final enum MESSAGE_CAPPING:LX/7Ve;


# instance fields
.field private mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1215046
    new-instance v0, LX/7Ve;

    const-string v1, "MESSAGE_CAPPING"

    const-string v2, "Message capping"

    invoke-direct {v0, v1, v3, v2}, LX/7Ve;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Ve;->MESSAGE_CAPPING:LX/7Ve;

    .line 1215047
    new-instance v0, LX/7Ve;

    const-string v1, "FREE_MESSENGER"

    const-string v2, "Free messenger"

    invoke-direct {v0, v1, v4, v2}, LX/7Ve;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Ve;->FREE_MESSENGER:LX/7Ve;

    .line 1215048
    const/4 v0, 0x2

    new-array v0, v0, [LX/7Ve;

    sget-object v1, LX/7Ve;->MESSAGE_CAPPING:LX/7Ve;

    aput-object v1, v0, v3

    sget-object v1, LX/7Ve;->FREE_MESSENGER:LX/7Ve;

    aput-object v1, v0, v4

    sput-object v0, LX/7Ve;->$VALUES:[LX/7Ve;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1215049
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1215050
    iput-object p3, p0, LX/7Ve;->mName:Ljava/lang/String;

    .line 1215051
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/7Ve;
    .locals 3

    .prologue
    .line 1215052
    sget-object v0, LX/7Ve;->MESSAGE_CAPPING:LX/7Ve;

    invoke-virtual {v0}, LX/7Ve;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1215053
    sget-object v0, LX/7Ve;->MESSAGE_CAPPING:LX/7Ve;

    .line 1215054
    :goto_0
    return-object v0

    .line 1215055
    :cond_0
    sget-object v0, LX/7Ve;->FREE_MESSENGER:LX/7Ve;

    invoke-virtual {v0}, LX/7Ve;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1215056
    sget-object v0, LX/7Ve;->FREE_MESSENGER:LX/7Ve;

    goto :goto_0

    .line 1215057
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Encountered an unexpected ZeroMessengerType string: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Ve;
    .locals 1

    .prologue
    .line 1215058
    const-class v0, LX/7Ve;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Ve;

    return-object v0
.end method

.method public static values()[LX/7Ve;
    .locals 1

    .prologue
    .line 1215059
    sget-object v0, LX/7Ve;->$VALUES:[LX/7Ve;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Ve;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1215060
    iget-object v0, p0, LX/7Ve;->mName:Ljava/lang/String;

    return-object v0
.end method
