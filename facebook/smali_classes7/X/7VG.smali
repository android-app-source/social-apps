.class public LX/7VG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;

.field private static volatile e:LX/7VG;


# instance fields
.field public final b:LX/1pA;

.field public final c:LX/0tX;

.field public final d:LX/0yP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1214164
    const-class v0, LX/7VG;

    sput-object v0, LX/7VG;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1pA;LX/0tX;LX/0yP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1214165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1214166
    iput-object p1, p0, LX/7VG;->b:LX/1pA;

    .line 1214167
    iput-object p2, p0, LX/7VG;->c:LX/0tX;

    .line 1214168
    iput-object p3, p0, LX/7VG;->d:LX/0yP;

    .line 1214169
    return-void
.end method

.method public static a(LX/0QB;)LX/7VG;
    .locals 6

    .prologue
    .line 1214170
    sget-object v0, LX/7VG;->e:LX/7VG;

    if-nez v0, :cond_1

    .line 1214171
    const-class v1, LX/7VG;

    monitor-enter v1

    .line 1214172
    :try_start_0
    sget-object v0, LX/7VG;->e:LX/7VG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1214173
    if-eqz v2, :cond_0

    .line 1214174
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1214175
    new-instance p0, LX/7VG;

    invoke-static {v0}, LX/1pA;->b(LX/0QB;)LX/1pA;

    move-result-object v3

    check-cast v3, LX/1pA;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0yO;->b(LX/0QB;)LX/0yO;

    move-result-object v5

    check-cast v5, LX/0yP;

    invoke-direct {p0, v3, v4, v5}, LX/7VG;-><init>(LX/1pA;LX/0tX;LX/0yP;)V

    .line 1214176
    move-object v0, p0

    .line 1214177
    sput-object v0, LX/7VG;->e:LX/7VG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1214178
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1214179
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1214180
    :cond_1
    sget-object v0, LX/7VG;->e:LX/7VG;

    return-object v0

    .line 1214181
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1214182
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/graphql/executor/GraphQLResult;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/graphql/calls/ZeroOptinStateValue;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/zero/protocol/graphql/ZeroSetOptinStateMutationModels$ZeroSetOptinStateMutationFieldsModel;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1214183
    if-eqz p0, :cond_0

    .line 1214184
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1214185
    if-eqz v0, :cond_0

    .line 1214186
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1214187
    check-cast v0, Lcom/facebook/zero/protocol/graphql/ZeroSetOptinStateMutationModels$ZeroSetOptinStateMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/zero/protocol/graphql/ZeroSetOptinStateMutationModels$ZeroSetOptinStateMutationFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1214188
    :cond_0
    const/4 v0, 0x0

    .line 1214189
    :goto_0
    return-object v0

    .line 1214190
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1214191
    check-cast v0, Lcom/facebook/zero/protocol/graphql/ZeroSetOptinStateMutationModels$ZeroSetOptinStateMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/zero/protocol/graphql/ZeroSetOptinStateMutationModels$ZeroSetOptinStateMutationFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
