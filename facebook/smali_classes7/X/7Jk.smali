.class public final enum LX/7Jk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Jk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Jk;

.field public static final enum PAUSED:LX/7Jk;

.field public static final enum PLAYING:LX/7Jk;

.field public static final enum STOPPED:LX/7Jk;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1194080
    new-instance v0, LX/7Jk;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v2}, LX/7Jk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Jk;->STOPPED:LX/7Jk;

    .line 1194081
    new-instance v0, LX/7Jk;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v3}, LX/7Jk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Jk;->PLAYING:LX/7Jk;

    .line 1194082
    new-instance v0, LX/7Jk;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v4}, LX/7Jk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Jk;->PAUSED:LX/7Jk;

    .line 1194083
    const/4 v0, 0x3

    new-array v0, v0, [LX/7Jk;

    sget-object v1, LX/7Jk;->STOPPED:LX/7Jk;

    aput-object v1, v0, v2

    sget-object v1, LX/7Jk;->PLAYING:LX/7Jk;

    aput-object v1, v0, v3

    sget-object v1, LX/7Jk;->PAUSED:LX/7Jk;

    aput-object v1, v0, v4

    sput-object v0, LX/7Jk;->$VALUES:[LX/7Jk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1194084
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Jk;
    .locals 1

    .prologue
    .line 1194085
    const-class v0, LX/7Jk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Jk;

    return-object v0
.end method

.method public static values()[LX/7Jk;
    .locals 1

    .prologue
    .line 1194079
    sget-object v0, LX/7Jk;->$VALUES:[LX/7Jk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Jk;

    return-object v0
.end method
