.class public final enum LX/729;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6vZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/729;",
        ">;",
        "LX/6vZ;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/729;

.field public static final enum SHIPPING_ADDRESSES:LX/729;

.field public static final enum SHIPPING_SECURITY_MESSAGE:LX/729;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1163992
    new-instance v0, LX/729;

    const-string v1, "SHIPPING_ADDRESSES"

    invoke-direct {v0, v1, v2}, LX/729;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/729;->SHIPPING_ADDRESSES:LX/729;

    .line 1163993
    new-instance v0, LX/729;

    const-string v1, "SHIPPING_SECURITY_MESSAGE"

    invoke-direct {v0, v1, v3}, LX/729;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/729;->SHIPPING_SECURITY_MESSAGE:LX/729;

    .line 1163994
    const/4 v0, 0x2

    new-array v0, v0, [LX/729;

    sget-object v1, LX/729;->SHIPPING_ADDRESSES:LX/729;

    aput-object v1, v0, v2

    sget-object v1, LX/729;->SHIPPING_SECURITY_MESSAGE:LX/729;

    aput-object v1, v0, v3

    sput-object v0, LX/729;->$VALUES:[LX/729;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1163995
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/729;
    .locals 1

    .prologue
    .line 1163996
    const-class v0, LX/729;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/729;

    return-object v0
.end method

.method public static values()[LX/729;
    .locals 1

    .prologue
    .line 1163997
    sget-object v0, LX/729;->$VALUES:[LX/729;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/729;

    return-object v0
.end method
