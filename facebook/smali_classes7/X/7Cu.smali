.class public LX/7Cu;
.super LX/7Cf;
.source ""


# instance fields
.field public A:Z

.field public B:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:LX/7Ct;

.field public x:LX/7Ct;

.field public y:LX/0wd;

.field public z:LX/0wd;


# direct methods
.method public constructor <init>(Landroid/view/WindowManager;LX/7DG;LX/0wW;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1182004
    invoke-direct {p0, p1, p2}, LX/7Cf;-><init>(Landroid/view/WindowManager;LX/7DG;)V

    .line 1182005
    iput-boolean v1, p0, LX/7Cu;->t:Z

    .line 1182006
    iput-boolean v1, p0, LX/7Cu;->u:Z

    .line 1182007
    iput-boolean v1, p0, LX/7Cu;->v:Z

    .line 1182008
    sget-object v0, LX/7Ct;->NONE:LX/7Ct;

    iput-object v0, p0, LX/7Cu;->w:LX/7Ct;

    .line 1182009
    sget-object v0, LX/7Ct;->NONE:LX/7Ct;

    iput-object v0, p0, LX/7Cu;->x:LX/7Ct;

    .line 1182010
    iput-boolean v1, p0, LX/7Cu;->A:Z

    .line 1182011
    iput-boolean v1, p0, LX/7Cu;->B:Z

    .line 1182012
    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    const-wide/16 v4, 0x0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v2

    .line 1182013
    invoke-virtual {p3}, LX/0wW;->a()LX/0wd;

    move-result-object v3

    iput-object v3, p0, LX/7Cu;->y:LX/0wd;

    .line 1182014
    invoke-virtual {p3}, LX/0wW;->a()LX/0wd;

    move-result-object v3

    iput-object v3, p0, LX/7Cu;->z:LX/0wd;

    .line 1182015
    iget-object v3, p0, LX/7Cu;->y:LX/0wd;

    invoke-virtual {v3, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    .line 1182016
    iget-object v3, p0, LX/7Cu;->z:LX/0wd;

    invoke-virtual {v3, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    .line 1182017
    return-void
.end method

.method private a(ZZFFZ)F
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1181993
    const v0, 0x3dcccccd    # 0.1f

    sub-float v0, p4, v0

    cmpg-float v0, p3, v0

    if-gez v0, :cond_0

    if-nez p1, :cond_0

    .line 1181994
    if-eqz p2, :cond_1

    .line 1181995
    iput-boolean v1, p0, LX/7Cu;->u:Z

    .line 1181996
    sget-object v0, LX/7Ct;->LEFT:LX/7Ct;

    iput-object v0, p0, LX/7Cu;->x:LX/7Ct;

    .line 1181997
    :goto_0
    invoke-direct {p0, p5, p2}, LX/7Cu;->a(ZZ)V

    .line 1181998
    :cond_0
    if-eqz p2, :cond_2

    .line 1181999
    iget v0, p0, LX/7Cf;->b:F

    .line 1182000
    :goto_1
    return v0

    .line 1182001
    :cond_1
    iput-boolean v1, p0, LX/7Cu;->t:Z

    .line 1182002
    sget-object v0, LX/7Ct;->TOP:LX/7Ct;

    iput-object v0, p0, LX/7Cu;->w:LX/7Ct;

    goto :goto_0

    .line 1182003
    :cond_2
    iget v0, p0, LX/7Cf;->a:F

    goto :goto_1
.end method

.method private a(ZZ)V
    .locals 2

    .prologue
    .line 1181932
    if-nez p1, :cond_0

    .line 1181933
    if-eqz p2, :cond_1

    .line 1181934
    iget v0, p0, LX/7Cf;->b:F

    iget v1, p0, LX/7Cf;->d:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LX/7Cu;->b:F

    .line 1181935
    iget v0, p0, LX/7Cf;->b:F

    iget v1, p0, LX/7Cf;->c:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LX/7Cu;->b:F

    .line 1181936
    :cond_0
    :goto_0
    return-void

    .line 1181937
    :cond_1
    iget v0, p0, LX/7Cf;->a:F

    iget v1, p0, LX/7Cf;->e:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LX/7Cu;->a:F

    .line 1181938
    iget v0, p0, LX/7Cf;->a:F

    iget v1, p0, LX/7Cf;->f:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LX/7Cu;->a:F

    goto :goto_0
.end method

.method private b(ZZFFZ)F
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1181982
    const v0, 0x3dcccccd    # 0.1f

    add-float/2addr v0, p4

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    if-nez p1, :cond_0

    .line 1181983
    if-eqz p2, :cond_1

    .line 1181984
    iput-boolean v1, p0, LX/7Cu;->u:Z

    .line 1181985
    sget-object v0, LX/7Ct;->RIGHT:LX/7Ct;

    iput-object v0, p0, LX/7Cu;->x:LX/7Ct;

    .line 1181986
    :goto_0
    invoke-direct {p0, p5, p2}, LX/7Cu;->a(ZZ)V

    .line 1181987
    :cond_0
    if-eqz p2, :cond_2

    .line 1181988
    iget v0, p0, LX/7Cf;->b:F

    .line 1181989
    :goto_1
    return v0

    .line 1181990
    :cond_1
    iput-boolean v1, p0, LX/7Cu;->t:Z

    .line 1181991
    sget-object v0, LX/7Ct;->BOTTOM:LX/7Ct;

    iput-object v0, p0, LX/7Cu;->w:LX/7Ct;

    goto :goto_0

    .line 1181992
    :cond_2
    iget v0, p0, LX/7Cf;->a:F

    goto :goto_1
.end method


# virtual methods
.method public final a(FFZ)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    .line 1181950
    iget-boolean v0, p0, LX/7Cu;->v:Z

    if-eqz v0, :cond_1

    if-nez p3, :cond_1

    .line 1181951
    :cond_0
    :goto_0
    return-void

    .line 1181952
    :cond_1
    iget v0, p0, LX/7Cf;->a:F

    iget v1, p0, LX/7Cf;->e:F

    iget v3, p0, LX/7Cf;->f:F

    invoke-static {v0, p1, v1, v3}, LX/7Cf;->a(FFFF)F

    move-result v0

    .line 1181953
    iget-boolean v1, p0, LX/7Cu;->t:Z

    if-nez v1, :cond_8

    .line 1181954
    if-nez p3, :cond_7

    iget-boolean v1, p0, LX/7Cu;->A:Z

    if-nez v1, :cond_7

    .line 1181955
    iget v1, p0, LX/7Cf;->a:F

    mul-float/2addr v0, p1

    add-float/2addr v0, v1

    iput v0, p0, LX/7Cu;->a:F

    .line 1181956
    :cond_2
    :goto_1
    iget-boolean v1, p0, LX/7Cu;->A:Z

    iget v3, p0, LX/7Cf;->a:F

    iget v4, p0, LX/7Cf;->e:F

    move-object v0, p0

    move v5, p3

    invoke-direct/range {v0 .. v5}, LX/7Cu;->a(ZZFFZ)F

    move-result v0

    iput v0, p0, LX/7Cu;->a:F

    .line 1181957
    iget-boolean v1, p0, LX/7Cu;->A:Z

    iget v3, p0, LX/7Cf;->a:F

    iget v4, p0, LX/7Cf;->f:F

    move-object v0, p0

    move v5, p3

    invoke-direct/range {v0 .. v5}, LX/7Cu;->b(ZZFFZ)F

    move-result v0

    iput v0, p0, LX/7Cu;->a:F

    .line 1181958
    :cond_3
    :goto_2
    if-eqz p3, :cond_4

    iget-boolean v0, p0, LX/7Cu;->t:Z

    if-eqz v0, :cond_4

    .line 1181959
    iget v0, p0, LX/7Cf;->a:F

    mul-float v1, v6, p1

    add-float/2addr v0, v1

    iput v0, p0, LX/7Cu;->a:F

    .line 1181960
    :cond_4
    iget-object v0, p0, LX/7Cf;->g:LX/7DG;

    .line 1181961
    iget-boolean v1, v0, LX/7DG;->e:Z

    move v0, v1

    .line 1181962
    if-eqz v0, :cond_b

    .line 1181963
    iget v0, p0, LX/7Cf;->b:F

    iget v1, p0, LX/7Cf;->d:F

    iget v3, p0, LX/7Cf;->c:F

    invoke-static {v0, p2, v1, v3}, LX/7Cf;->a(FFFF)F

    move-result v0

    .line 1181964
    iget-boolean v1, p0, LX/7Cu;->u:Z

    if-nez v1, :cond_a

    .line 1181965
    if-nez p3, :cond_9

    iget-boolean v1, p0, LX/7Cu;->B:Z

    if-nez v1, :cond_9

    .line 1181966
    iget v1, p0, LX/7Cf;->b:F

    mul-float/2addr v0, p2

    add-float/2addr v0, v1

    iput v0, p0, LX/7Cu;->b:F

    .line 1181967
    :cond_5
    :goto_3
    iget-boolean v1, p0, LX/7Cu;->B:Z

    iget v3, p0, LX/7Cf;->b:F

    iget v4, p0, LX/7Cf;->d:F

    move-object v0, p0

    move v2, v7

    move v5, p3

    invoke-direct/range {v0 .. v5}, LX/7Cu;->a(ZZFFZ)F

    move-result v0

    iput v0, p0, LX/7Cu;->b:F

    .line 1181968
    iget-boolean v1, p0, LX/7Cu;->B:Z

    iget v3, p0, LX/7Cf;->b:F

    iget v4, p0, LX/7Cf;->c:F

    move-object v0, p0

    move v2, v7

    move v5, p3

    invoke-direct/range {v0 .. v5}, LX/7Cu;->b(ZZFFZ)F

    move-result v0

    iput v0, p0, LX/7Cu;->b:F

    .line 1181969
    :cond_6
    :goto_4
    if-eqz p3, :cond_0

    iget-boolean v0, p0, LX/7Cu;->u:Z

    if-eqz v0, :cond_0

    .line 1181970
    iget v0, p0, LX/7Cf;->b:F

    mul-float v1, v6, p2

    add-float/2addr v0, v1

    iput v0, p0, LX/7Cu;->b:F

    goto/16 :goto_0

    .line 1181971
    :cond_7
    if-eqz p3, :cond_2

    .line 1181972
    iget v0, p0, LX/7Cf;->a:F

    add-float/2addr v0, p1

    iput v0, p0, LX/7Cu;->a:F

    goto :goto_1

    .line 1181973
    :cond_8
    iget v0, p0, LX/7Cf;->a:F

    iget v1, p0, LX/7Cf;->e:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    iget v0, p0, LX/7Cf;->a:F

    iget v1, p0, LX/7Cf;->f:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_3

    .line 1181974
    iput-boolean v2, p0, LX/7Cu;->t:Z

    .line 1181975
    sget-object v0, LX/7Ct;->NONE:LX/7Ct;

    iput-object v0, p0, LX/7Cu;->w:LX/7Ct;

    goto :goto_2

    .line 1181976
    :cond_9
    if-eqz p3, :cond_5

    .line 1181977
    iget v0, p0, LX/7Cf;->b:F

    add-float/2addr v0, p2

    iput v0, p0, LX/7Cu;->b:F

    goto :goto_3

    .line 1181978
    :cond_a
    iget v0, p0, LX/7Cf;->b:F

    iget v1, p0, LX/7Cf;->d:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_6

    iget v0, p0, LX/7Cf;->b:F

    iget v1, p0, LX/7Cf;->c:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_6

    .line 1181979
    iput-boolean v2, p0, LX/7Cu;->u:Z

    .line 1181980
    sget-object v0, LX/7Ct;->NONE:LX/7Ct;

    iput-object v0, p0, LX/7Cu;->x:LX/7Ct;

    goto :goto_4

    .line 1181981
    :cond_b
    iget v0, p0, LX/7Cf;->b:F

    add-float/2addr v0, p2

    iput v0, p0, LX/7Cu;->b:F

    goto/16 :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1181939
    iget-object v0, p0, LX/7Cf;->g:LX/7DG;

    move-object v0, v0

    .line 1181940
    iget-boolean v1, v0, LX/7DG;->f:Z

    move v0, v1

    .line 1181941
    if-eqz v0, :cond_1

    iget v0, p0, LX/7Cf;->a:F

    iget v1, p0, LX/7Cf;->f:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, LX/7Cf;->a:F

    iget v1, p0, LX/7Cf;->e:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 1181942
    :cond_0
    iget v0, p0, LX/7Cf;->a:F

    iget v1, p0, LX/7Cf;->e:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LX/7Cu;->a:F

    .line 1181943
    iget v0, p0, LX/7Cf;->a:F

    iget v1, p0, LX/7Cf;->f:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LX/7Cu;->a:F

    .line 1181944
    :cond_1
    iget-object v0, p0, LX/7Cf;->g:LX/7DG;

    move-object v0, v0

    .line 1181945
    iget-boolean v1, v0, LX/7DG;->e:Z

    move v0, v1

    .line 1181946
    if-eqz v0, :cond_3

    iget v0, p0, LX/7Cf;->b:F

    iget v1, p0, LX/7Cf;->c:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_2

    iget v0, p0, LX/7Cf;->b:F

    iget v1, p0, LX/7Cf;->d:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 1181947
    :cond_2
    iget v0, p0, LX/7Cf;->b:F

    iget v1, p0, LX/7Cf;->d:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LX/7Cu;->b:F

    .line 1181948
    iget v0, p0, LX/7Cf;->b:F

    iget v1, p0, LX/7Cf;->c:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LX/7Cu;->b:F

    .line 1181949
    :cond_3
    return-void
.end method
