.class public final LX/8SG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 1346252
    iput-object p1, p0, LX/8SG;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1346253
    iget-object v0, p0, LX/8SG;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    invoke-virtual {v0}, LX/3Tf;->a()LX/333;

    move-result-object v0

    iget-object v1, p0, LX/8SG;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, LX/333;->a(Ljava/lang/CharSequence;)V

    .line 1346254
    iget-object v0, p0, LX/8SG;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v1, p0, LX/8SG;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v1}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;->t:Ljava/util/List;

    .line 1346255
    iget-object v0, p0, LX/8SG;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8SG;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, LX/8SG;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->t:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 1346256
    :goto_0
    return-void

    .line 1346257
    :cond_0
    iget-object v0, p0, LX/8SG;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->t:Ljava/util/List;

    iput-object v0, p0, LX/8SG;->b:Ljava/util/List;

    .line 1346258
    iget-object v0, p0, LX/8SG;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    iget-object v1, p0, LX/8SG;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->t:Ljava/util/List;

    .line 1346259
    iput-object v1, v0, LX/8tB;->i:Ljava/util/List;

    .line 1346260
    iget-object v0, p0, LX/8SG;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    invoke-virtual {v0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->e()V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1346261
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1346262
    return-void
.end method
