.class public final enum LX/7MA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7MA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7MA;

.field public static final enum CALL_ACCEPT:LX/7MA;

.field public static final enum CALL_ENDED:LX/7MA;

.field public static final enum CALL_RECEIVED:LX/7MA;

.field public static final enum CALL_REJECT:LX/7MA;

.field public static final enum PIP_CLICK_CROSS:LX/7MA;

.field public static final enum PIP_CLICK_ROTATE:LX/7MA;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1198515
    new-instance v0, LX/7MA;

    const-string v1, "PIP_CLICK_ROTATE"

    invoke-direct {v0, v1, v3}, LX/7MA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7MA;->PIP_CLICK_ROTATE:LX/7MA;

    .line 1198516
    new-instance v0, LX/7MA;

    const-string v1, "PIP_CLICK_CROSS"

    invoke-direct {v0, v1, v4}, LX/7MA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7MA;->PIP_CLICK_CROSS:LX/7MA;

    .line 1198517
    new-instance v0, LX/7MA;

    const-string v1, "CALL_RECEIVED"

    invoke-direct {v0, v1, v5}, LX/7MA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7MA;->CALL_RECEIVED:LX/7MA;

    .line 1198518
    new-instance v0, LX/7MA;

    const-string v1, "CALL_ENDED"

    invoke-direct {v0, v1, v6}, LX/7MA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7MA;->CALL_ENDED:LX/7MA;

    .line 1198519
    new-instance v0, LX/7MA;

    const-string v1, "CALL_ACCEPT"

    invoke-direct {v0, v1, v7}, LX/7MA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7MA;->CALL_ACCEPT:LX/7MA;

    .line 1198520
    new-instance v0, LX/7MA;

    const-string v1, "CALL_REJECT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7MA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7MA;->CALL_REJECT:LX/7MA;

    .line 1198521
    const/4 v0, 0x6

    new-array v0, v0, [LX/7MA;

    sget-object v1, LX/7MA;->PIP_CLICK_ROTATE:LX/7MA;

    aput-object v1, v0, v3

    sget-object v1, LX/7MA;->PIP_CLICK_CROSS:LX/7MA;

    aput-object v1, v0, v4

    sget-object v1, LX/7MA;->CALL_RECEIVED:LX/7MA;

    aput-object v1, v0, v5

    sget-object v1, LX/7MA;->CALL_ENDED:LX/7MA;

    aput-object v1, v0, v6

    sget-object v1, LX/7MA;->CALL_ACCEPT:LX/7MA;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7MA;->CALL_REJECT:LX/7MA;

    aput-object v2, v0, v1

    sput-object v0, LX/7MA;->$VALUES:[LX/7MA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1198522
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7MA;
    .locals 1

    .prologue
    .line 1198523
    const-class v0, LX/7MA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7MA;

    return-object v0
.end method

.method public static values()[LX/7MA;
    .locals 1

    .prologue
    .line 1198524
    sget-object v0, LX/7MA;->$VALUES:[LX/7MA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7MA;

    return-object v0
.end method
