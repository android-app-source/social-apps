.class public final LX/8b9;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$RichDocumentSubscriptionActionAcceptedModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1371488
    const-class v1, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$RichDocumentSubscriptionActionAcceptedModel;

    const v0, -0xe5cb304    # -1.617252E30f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "RichDocumentSubscriptionActionAcceptedCoreMutation"

    const-string v6, "d8699ad4052346ff62f1a3bcd8e17cf0"

    const-string v7, "instant_article_subscription_action_accepted"

    const-string v8, "0"

    const-string v9, "10155069968916729"

    const/4 v10, 0x0

    .line 1371489
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1371490
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1371491
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1371492
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1371493
    packed-switch v0, :pswitch_data_0

    .line 1371494
    :goto_0
    return-object p1

    .line 1371495
    :pswitch_0
    const-string p1, "0"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5fb57ca
        :pswitch_0
    .end packed-switch
.end method
