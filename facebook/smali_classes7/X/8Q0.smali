.class public final enum LX/8Q0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Q0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Q0;

.field public static final enum ENABLE_NEWCOMER_AUDIENCE:LX/8Q0;

.field public static final enum FETCH_AUDIENCE_EDUCATION:LX/8Q0;

.field public static final enum FETCH_STICKY_GUARDRAIL:LX/8Q0;

.field public static final enum FORCE_AAA_ONLY_ME:LX/8Q0;

.field public static final enum FORCE_AAA_TUX:LX/8Q0;

.field public static final enum LAUNCH_PROFILE_PHOTO_CHECKUP:LX/8Q0;

.field public static final enum REMOVE_DEFAULT_OVERRIDE:LX/8Q0;


# instance fields
.field public final desc:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1342364
    new-instance v0, LX/8Q0;

    const-string v1, "FETCH_STICKY_GUARDRAIL"

    const-string v2, "Refetch Sticky Guardrail Info"

    invoke-direct {v0, v1, v4, v2}, LX/8Q0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Q0;->FETCH_STICKY_GUARDRAIL:LX/8Q0;

    .line 1342365
    new-instance v0, LX/8Q0;

    const-string v1, "ENABLE_NEWCOMER_AUDIENCE"

    const-string v2, "Set NAS to enabled"

    invoke-direct {v0, v1, v5, v2}, LX/8Q0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Q0;->ENABLE_NEWCOMER_AUDIENCE:LX/8Q0;

    .line 1342366
    new-instance v0, LX/8Q0;

    const-string v1, "FORCE_AAA_TUX"

    const-string v2, "Enable AAA TUX"

    invoke-direct {v0, v1, v6, v2}, LX/8Q0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Q0;->FORCE_AAA_TUX:LX/8Q0;

    .line 1342367
    new-instance v0, LX/8Q0;

    const-string v1, "FORCE_AAA_ONLY_ME"

    const-string v2, "Enable AAA Only Me"

    invoke-direct {v0, v1, v7, v2}, LX/8Q0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Q0;->FORCE_AAA_ONLY_ME:LX/8Q0;

    .line 1342368
    new-instance v0, LX/8Q0;

    const-string v1, "FETCH_AUDIENCE_EDUCATION"

    const-string v2, "Refetch Audience Education Info"

    invoke-direct {v0, v1, v8, v2}, LX/8Q0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Q0;->FETCH_AUDIENCE_EDUCATION:LX/8Q0;

    .line 1342369
    new-instance v0, LX/8Q0;

    const-string v1, "REMOVE_DEFAULT_OVERRIDE"

    const/4 v2, 0x5

    const-string v3, "Remove Default Privacy Override"

    invoke-direct {v0, v1, v2, v3}, LX/8Q0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Q0;->REMOVE_DEFAULT_OVERRIDE:LX/8Q0;

    .line 1342370
    new-instance v0, LX/8Q0;

    const-string v1, "LAUNCH_PROFILE_PHOTO_CHECKUP"

    const/4 v2, 0x6

    const-string v3, "Launch profile photo checkup"

    invoke-direct {v0, v1, v2, v3}, LX/8Q0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Q0;->LAUNCH_PROFILE_PHOTO_CHECKUP:LX/8Q0;

    .line 1342371
    const/4 v0, 0x7

    new-array v0, v0, [LX/8Q0;

    sget-object v1, LX/8Q0;->FETCH_STICKY_GUARDRAIL:LX/8Q0;

    aput-object v1, v0, v4

    sget-object v1, LX/8Q0;->ENABLE_NEWCOMER_AUDIENCE:LX/8Q0;

    aput-object v1, v0, v5

    sget-object v1, LX/8Q0;->FORCE_AAA_TUX:LX/8Q0;

    aput-object v1, v0, v6

    sget-object v1, LX/8Q0;->FORCE_AAA_ONLY_ME:LX/8Q0;

    aput-object v1, v0, v7

    sget-object v1, LX/8Q0;->FETCH_AUDIENCE_EDUCATION:LX/8Q0;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/8Q0;->REMOVE_DEFAULT_OVERRIDE:LX/8Q0;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8Q0;->LAUNCH_PROFILE_PHOTO_CHECKUP:LX/8Q0;

    aput-object v2, v0, v1

    sput-object v0, LX/8Q0;->$VALUES:[LX/8Q0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1342372
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1342373
    iput-object p3, p0, LX/8Q0;->desc:Ljava/lang/String;

    .line 1342374
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/8Q0;
    .locals 5

    .prologue
    .line 1342375
    invoke-static {}, LX/8Q0;->values()[LX/8Q0;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1342376
    invoke-virtual {v0}, LX/8Q0;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1342377
    :goto_1
    return-object v0

    .line 1342378
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1342379
    :cond_1
    sget-object v0, LX/8Q0;->FETCH_STICKY_GUARDRAIL:LX/8Q0;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Q0;
    .locals 1

    .prologue
    .line 1342380
    const-class v0, LX/8Q0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Q0;

    return-object v0
.end method

.method public static values()[LX/8Q0;
    .locals 1

    .prologue
    .line 1342381
    sget-object v0, LX/8Q0;->$VALUES:[LX/8Q0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Q0;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1342382
    iget-object v0, p0, LX/8Q0;->desc:Ljava/lang/String;

    return-object v0
.end method
