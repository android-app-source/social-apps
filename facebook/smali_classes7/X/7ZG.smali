.class public LX/7ZG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/gms/cast/MediaQueueItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/google/android/gms/cast/MediaQueueItem;
    .locals 15

    const/4 v13, 0x0

    const/4 v5, 0x0

    const-wide/16 v10, 0x0

    invoke-static {p0}, LX/2xb;->b(Landroid/os/Parcel;)I

    move-result v1

    move-object v12, v13

    move-wide v8, v10

    move-wide v6, v10

    move v4, v5

    move-object v3, v13

    move v2, v5

    :goto_0
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-ge v0, v1, :cond_0

    invoke-static {p0}, LX/2xb;->a(Landroid/os/Parcel;)I

    move-result v0

    invoke-static {v0}, LX/2xb;->a(I)I

    move-result v14

    packed-switch v14, :pswitch_data_0

    invoke-static {p0, v0}, LX/2xb;->a(Landroid/os/Parcel;I)V

    goto :goto_0

    :pswitch_0
    invoke-static {p0, v0}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v2

    goto :goto_0

    :pswitch_1
    sget-object v3, Lcom/google/android/gms/cast/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p0, v0, v3}, LX/2xb;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/MediaInfo;

    move-object v3, v0

    goto :goto_0

    :pswitch_2
    invoke-static {p0, v0}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v4

    goto :goto_0

    :pswitch_3
    invoke-static {p0, v0}, LX/2xb;->b(Landroid/os/Parcel;I)Z

    move-result v5

    goto :goto_0

    :pswitch_4
    invoke-static {p0, v0}, LX/2xb;->l(Landroid/os/Parcel;I)D

    move-result-wide v6

    goto :goto_0

    :pswitch_5
    invoke-static {p0, v0}, LX/2xb;->l(Landroid/os/Parcel;I)D

    move-result-wide v8

    goto :goto_0

    :pswitch_6
    invoke-static {p0, v0}, LX/2xb;->l(Landroid/os/Parcel;I)D

    move-result-wide v10

    goto :goto_0

    :pswitch_7
    invoke-static {p0, v0}, LX/2xb;->t(Landroid/os/Parcel;I)[J

    move-result-object v12

    goto :goto_0

    :pswitch_8
    invoke-static {p0, v0}, LX/2xb;->n(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v13

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-eq v0, v1, :cond_1

    new-instance v0, LX/4sr;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x25

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Overread allowed size end="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/4sr;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v0

    :cond_1
    new-instance v1, Lcom/google/android/gms/cast/MediaQueueItem;

    invoke-direct/range {v1 .. v13}, Lcom/google/android/gms/cast/MediaQueueItem;-><init>(ILcom/google/android/gms/cast/MediaInfo;IZDDD[JLjava/lang/String;)V

    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, LX/7ZG;->a(Landroid/os/Parcel;)Lcom/google/android/gms/cast/MediaQueueItem;

    move-result-object v0

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/cast/MediaQueueItem;

    return-object v0
.end method
