.class public LX/70i;
.super LX/3pF;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0gc;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1162639
    invoke-direct {p0, p1}, LX/3pF;-><init>(LX/0gc;)V

    .line 1162640
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/70i;->b:Ljava/util/List;

    .line 1162641
    iput-object p2, p0, LX/70i;->a:Landroid/content/Context;

    .line 1162642
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 1162643
    new-instance v1, Lcom/facebook/payments/paymentsflow/uicomponents/ScreenshotFragment;

    invoke-direct {v1}, Lcom/facebook/payments/paymentsflow/uicomponents/ScreenshotFragment;-><init>()V

    .line 1162644
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1162645
    const-string v3, "network_image_uri"

    iget-object v0, p0, LX/70i;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1162646
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1162647
    return-object v1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1162648
    iget-object v0, p0, LX/70i;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
