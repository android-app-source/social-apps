.class public LX/7w3;
.super LX/6E8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E8",
        "<",
        "Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;",
        "LX/7w2;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;)V
    .locals 0

    .prologue
    .line 1275377
    invoke-direct {p0, p1}, LX/6E8;-><init>(LX/6E6;)V

    .line 1275378
    return-void
.end method

.method private a(LX/7w2;)V
    .locals 2

    .prologue
    .line 1275379
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;

    .line 1275380
    iget-object v1, p1, LX/7w2;->a:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-virtual {v0, v1}, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->setConfirmationText(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)V

    .line 1275381
    iget-object v1, p1, LX/7w2;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->setLogoUri(Landroid/net/Uri;)V

    .line 1275382
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/6E2;)V
    .locals 0

    .prologue
    .line 1275383
    check-cast p1, LX/7w2;

    invoke-direct {p0, p1}, LX/7w3;->a(LX/7w2;)V

    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1275384
    return-void
.end method
