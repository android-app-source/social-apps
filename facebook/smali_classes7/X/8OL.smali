.class public LX/8OL;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/fbuploader/FbUploader$FbUploadJobHandle;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/7z2;

.field public volatile d:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this for writes"
    .end annotation
.end field

.field private volatile e:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public f:LX/4d1;

.field private g:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7TD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1339041
    const-class v0, LX/8OL;

    sput-object v0, LX/8OL;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1339039
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/8OL;-><init>(LX/7zS;)V

    .line 1339040
    return-void
.end method

.method public constructor <init>(LX/7zS;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1339029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1339030
    iput-object v1, p0, LX/8OL;->a:Ljava/util/ArrayList;

    .line 1339031
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8OL;->d:Z

    .line 1339032
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8OL;->e:Z

    .line 1339033
    new-instance v0, LX/4d1;

    invoke-direct {v0}, LX/4d1;-><init>()V

    iput-object v0, p0, LX/8OL;->f:LX/4d1;

    .line 1339034
    if-eqz p1, :cond_0

    .line 1339035
    iget-object v0, p1, LX/7zS;->a:LX/7z2;

    move-object v0, v0

    .line 1339036
    iput-object v0, p0, LX/8OL;->b:LX/7z2;

    .line 1339037
    :goto_0
    return-void

    .line 1339038
    :cond_0
    iput-object v1, p0, LX/8OL;->b:LX/7z2;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/8OL;
    .locals 1

    .prologue
    .line 1339028
    invoke-static {p0}, LX/8OL;->b(LX/0QB;)LX/8OL;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8OL;
    .locals 2

    .prologue
    .line 1339010
    new-instance v1, LX/8OL;

    invoke-static {p0}, LX/7zS;->a(LX/0QB;)LX/7zS;

    move-result-object v0

    check-cast v0, LX/7zS;

    invoke-direct {v1, v0}, LX/8OL;-><init>(LX/7zS;)V

    .line 1339011
    return-object v1
.end method

.method private static c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1339026
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cancelling at "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1339027
    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0, p0}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 1339021
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/8OL;->d:Z

    .line 1339022
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8OL;->e:Z

    .line 1339023
    new-instance v0, LX/4d1;

    invoke-direct {v0}, LX/4d1;-><init>()V

    iput-object v0, p0, LX/8OL;->f:LX/4d1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1339024
    monitor-exit p0

    return-void

    .line 1339025
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7TD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1339019
    iput-object p1, p0, LX/8OL;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1339020
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1339016
    iget-boolean v0, p0, LX/8OL;->d:Z

    if-eqz v0, :cond_0

    .line 1339017
    invoke-static {p1}, LX/8OL;->c(Ljava/lang/String;)V

    .line 1339018
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/fbuploader/FbUploader$FbUploadJobHandle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1339014
    iput-object p1, p0, LX/8OL;->a:Ljava/util/ArrayList;

    .line 1339015
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1339012
    const/4 v0, 0x0

    iput-object v0, p0, LX/8OL;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1339013
    return-void
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1339005
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/8OL;->e:Z

    .line 1339006
    iget-boolean v0, p0, LX/8OL;->d:Z

    if-eqz v0, :cond_0

    .line 1339007
    invoke-static {p1}, LX/8OL;->c(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1339008
    :cond_0
    monitor-exit p0

    return-void

    .line 1339009
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1338991
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/8OL;->e:Z

    if-eqz v1, :cond_0

    .line 1338992
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/8OL;->d:Z

    .line 1338993
    iget-object v1, p0, LX/8OL;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_1

    .line 1338994
    iget-object v0, p0, LX/8OL;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    .line 1338995
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 1338996
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/8OL;->a:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/8OL;->b:LX/7z2;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/8OL;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1338997
    iget-object v2, p0, LX/8OL;->a:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1338998
    :try_start_2
    iget-object v1, p0, LX/8OL;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    iget-object v0, p0, LX/8OL;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7z0;

    .line 1338999
    iget-object v4, p0, LX/8OL;->b:LX/7z2;

    invoke-virtual {v4, v0}, LX/7z2;->b(LX/7z0;)V

    .line 1339000
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1339001
    :cond_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1339002
    :cond_3
    :try_start_3
    iget-object v0, p0, LX/8OL;->f:LX/4d1;

    invoke-virtual {v0}, LX/4d1;->a()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v0

    goto :goto_0

    .line 1339003
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1339004
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()LX/4d1;
    .locals 1

    .prologue
    .line 1338990
    iget-object v0, p0, LX/8OL;->f:LX/4d1;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1338989
    iget-boolean v0, p0, LX/8OL;->d:Z

    return v0
.end method
