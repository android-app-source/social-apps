.class public interface abstract LX/8IH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8IG;


# virtual methods
.method public abstract A()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract B()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract C()I
.end method

.method public abstract ai_()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aj_()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c()LX/1f8;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract d()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract e()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract j()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract l()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract m()LX/1VU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract n()I
.end method

.method public abstract o()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract p()Z
.end method

.method public abstract q()Z
.end method

.method public abstract r()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract s()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract t()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract u()I
.end method

.method public abstract v()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract w()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract x()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/photos/pandora/protocols/PandoraQueryInterfaces$PandoraMedia$PhotoEncodings;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract y()I
.end method

.method public abstract z()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
