.class public LX/7Xw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/64L;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0yP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0yI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/121;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/13n;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1218849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1218850
    iput-object p1, p0, LX/7Xw;->i:LX/0Ot;

    .line 1218851
    return-void
.end method

.method public static a(LX/7Xw;Z)V
    .locals 4

    .prologue
    .line 1218852
    iget-object v0, p0, LX/7Xw;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13n;

    .line 1218853
    iget-object v1, v0, LX/13n;->a:Landroid/app/Activity;

    move-object v0, v1

    .line 1218854
    new-instance v1, LX/7Xv;

    invoke-direct {v1, p0, p1, v0}, LX/7Xv;-><init>(LX/7Xw;ZLandroid/app/Activity;)V

    .line 1218855
    iget-object v2, p0, LX/7Xw;->a:LX/64L;

    invoke-virtual {v2, v1}, LX/64L;->a(LX/64J;)V

    .line 1218856
    iget-object v1, p0, LX/7Xw;->a:LX/64L;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LX/0tP;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    if-eqz p1, :cond_0

    const-string v0, "dialtone_optin_page"

    :goto_0
    const-string v3, "confirm"

    invoke-virtual {v1, v2, v0, v3}, LX/64L;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1218857
    return-void

    .line 1218858
    :cond_0
    const-string v0, "reconsider_optin_dialog"

    goto :goto_0
.end method
