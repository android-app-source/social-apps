.class public final LX/7zB;
.super Ljava/lang/Exception;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final mBytesTransferred:J

.field public final mFailureReason:Ljava/lang/String;

.field public final mInnerException:Ljava/lang/Exception;

.field public final mIsCancellation:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1280826
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/7zB;-><init>(Ljava/lang/String;JZLjava/lang/Exception;)V

    .line 1280827
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JZLjava/lang/Exception;)V
    .locals 0

    .prologue
    .line 1280828
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 1280829
    iput-object p1, p0, LX/7zB;->mFailureReason:Ljava/lang/String;

    .line 1280830
    iput-wide p2, p0, LX/7zB;->mBytesTransferred:J

    .line 1280831
    iput-boolean p4, p0, LX/7zB;->mIsCancellation:Z

    .line 1280832
    iput-object p5, p0, LX/7zB;->mInnerException:Ljava/lang/Exception;

    .line 1280833
    return-void
.end method


# virtual methods
.method public final getMessage()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1280834
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failure Reason: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/7zB;->mFailureReason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, LX/7zB;->mIsCancellation:Z

    if-eqz v0, :cond_0

    const-string v0, " (Cancellation), "

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "InnerException: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LX/7zB;->mInnerException:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/7zB;->mInnerException:Ljava/lang/Exception;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", InnerStack: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/7zB;->mInnerException:Ljava/lang/Exception;

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ", "

    goto :goto_0

    :cond_1
    const-string v0, "None"

    goto :goto_1
.end method
