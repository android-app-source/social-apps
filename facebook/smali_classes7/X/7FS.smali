.class public LX/7FS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1186235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;LX/1x4;LX/2uF;)Ljava/lang/String;
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getResolvedText"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1186236
    invoke-virtual {p2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    .line 1186237
    const/4 v0, 0x1

    const-class v4, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    invoke-virtual {v2, v3, v0, v4, v5}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    .line 1186238
    sget-object v4, LX/7FQ;->a:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/7FR;

    .line 1186239
    if-nez v4, :cond_1

    .line 1186240
    const/4 v4, 0x0

    .line 1186241
    sget-object v5, LX/7FP;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->ordinal()I

    move-result p2

    aget v5, v5, p2

    packed-switch v5, :pswitch_data_0

    .line 1186242
    :goto_1
    sget-object v5, LX/7FQ;->a:Ljava/util/HashMap;

    invoke-virtual {v5, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1186243
    move-object v4, v4

    .line 1186244
    :cond_1
    move-object v0, v4

    .line 1186245
    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 1186246
    if-eqz v0, :cond_0

    .line 1186247
    iget-object v3, p1, LX/1x4;->e:LX/0P1;

    move-object v3, v3

    .line 1186248
    if-nez v3, :cond_3

    .line 1186249
    const/4 v3, 0x0

    .line 1186250
    :goto_2
    move-object v0, v3

    .line 1186251
    if-eqz v0, :cond_0

    .line 1186252
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "{"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 1186253
    :cond_2
    return-object p0

    .line 1186254
    :pswitch_0
    new-instance v4, LX/7FR;

    invoke-direct {v4}, LX/7FR;-><init>()V

    goto :goto_1

    :cond_3
    invoke-virtual {v3, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
