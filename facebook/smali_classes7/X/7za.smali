.class public LX/7za;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1Aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Aa",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7zZ;LX/1AZ;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1281214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1281215
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p2, p1, v0, v1}, LX/1AZ;->a(LX/1AX;ZZ)LX/1Aa;

    move-result-object v0

    iput-object v0, p0, LX/7za;->a:LX/1Aa;

    .line 1281216
    return-void
.end method

.method public static a(LX/0QB;)LX/7za;
    .locals 5

    .prologue
    .line 1281217
    const-class v1, LX/7za;

    monitor-enter v1

    .line 1281218
    :try_start_0
    sget-object v0, LX/7za;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1281219
    sput-object v2, LX/7za;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1281220
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1281221
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1281222
    new-instance p0, LX/7za;

    invoke-static {v0}, LX/7zZ;->b(LX/0QB;)LX/7zZ;

    move-result-object v3

    check-cast v3, LX/7zZ;

    const-class v4, LX/1AZ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1AZ;

    invoke-direct {p0, v3, v4}, LX/7za;-><init>(LX/7zZ;LX/1AZ;)V

    .line 1281223
    move-object v0, p0

    .line 1281224
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1281225
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/7za;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1281226
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1281227
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/2oV;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "LX/2oV;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1281228
    iget-object v0, p0, LX/7za;->a:LX/1Aa;

    invoke-virtual {v0, p1, p2}, LX/1Aa;->a(Landroid/view/View;LX/2oV;)V

    .line 1281229
    return-void
.end method
