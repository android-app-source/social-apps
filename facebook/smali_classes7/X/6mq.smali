.class public LX/6mq;
.super LX/0B1;
.source ""


# instance fields
.field public e:LX/05u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05u",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/072;LX/07S;IJ)V
    .locals 0

    .prologue
    .line 1146696
    invoke-direct/range {p0 .. p5}, LX/0B1;-><init>(LX/072;LX/07S;IJ)V

    .line 1146697
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1146699
    iget-object v0, p0, LX/6mq;->e:LX/05u;

    if-eqz v0, :cond_0

    .line 1146700
    iget-object v0, p0, LX/6mq;->e:LX/05u;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/05u;->cancel(Z)Z

    .line 1146701
    const/4 v0, 0x0

    iput-object v0, p0, LX/6mq;->e:LX/05u;

    .line 1146702
    :cond_0
    invoke-super {p0, p1}, LX/0B1;->a(Ljava/lang/Throwable;)V

    .line 1146703
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1146704
    iget-object v0, p0, LX/6mq;->e:LX/05u;

    if-eqz v0, :cond_0

    .line 1146705
    iget-object v0, p0, LX/6mq;->e:LX/05u;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/05u;->cancel(Z)Z

    .line 1146706
    const/4 v0, 0x0

    iput-object v0, p0, LX/6mq;->e:LX/05u;

    .line 1146707
    :cond_0
    invoke-super {p0}, LX/0B1;->b()V

    .line 1146708
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1146698
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MqttRetriableOperation{mResponseType="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0B1;->b:LX/07S;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mOperationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0B1;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCreationTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/0B1;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
