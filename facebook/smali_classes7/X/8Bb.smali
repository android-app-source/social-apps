.class public final LX/8Bb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7yp;


# instance fields
.field public final synthetic a:Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;

.field private b:F

.field public c:J

.field private d:J

.field private e:J

.field public f:J

.field public g:J

.field private h:LX/8Oo;

.field private final i:LX/0So;


# direct methods
.method public constructor <init>(Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;LX/8Oo;JJLX/0So;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 1309482
    iput-object p1, p0, LX/8Bb;->a:Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309483
    cmp-long v0, p5, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Length of transfer must be greater than 0"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1309484
    iput-object p2, p0, LX/8Bb;->h:LX/8Oo;

    .line 1309485
    iput-wide p3, p0, LX/8Bb;->c:J

    .line 1309486
    iput-wide p5, p0, LX/8Bb;->d:J

    .line 1309487
    iput-object p7, p0, LX/8Bb;->i:LX/0So;

    .line 1309488
    iput-wide v2, p0, LX/8Bb;->f:J

    .line 1309489
    const/4 v0, 0x0

    iput v0, p0, LX/8Bb;->b:F

    .line 1309490
    iput-wide v2, p0, LX/8Bb;->e:J

    .line 1309491
    return-void

    .line 1309492
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(J)V
    .locals 5

    .prologue
    const/high16 v4, 0x42c80000    # 100.0f

    .line 1309493
    iget-object v0, p0, LX/8Bb;->h:LX/8Oo;

    if-nez v0, :cond_1

    .line 1309494
    :cond_0
    :goto_0
    return-void

    .line 1309495
    :cond_1
    iget-object v0, p0, LX/8Bb;->i:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 1309496
    iget-wide v2, p0, LX/8Bb;->e:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    iget-wide v0, p0, LX/8Bb;->d:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 1309497
    :cond_2
    long-to-float v0, p1

    iget-wide v2, p0, LX/8Bb;->d:J

    long-to-float v1, v2

    div-float/2addr v0, v1

    mul-float/2addr v0, v4

    .line 1309498
    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1309499
    iget v1, p0, LX/8Bb;->b:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 1309500
    iget-object v1, p0, LX/8Bb;->i:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/8Bb;->e:J

    .line 1309501
    iput v0, p0, LX/8Bb;->b:F

    .line 1309502
    iget-object v1, p0, LX/8Bb;->h:LX/8Oo;

    .line 1309503
    iget-object v2, v1, LX/8Oo;->d:LX/8OL;

    .line 1309504
    iget-boolean v3, v2, LX/8OL;->d:Z

    move v2, v3

    .line 1309505
    if-nez v2, :cond_3

    .line 1309506
    iget-object v2, v1, LX/8Oo;->e:LX/0b3;

    new-instance v3, LX/8Kk;

    iget-object v4, v1, LX/8Oo;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    sget-object p0, LX/8KZ;->UPLOADING:LX/8KZ;

    iget-boolean p1, v1, LX/8Oo;->p:Z

    if-eqz p1, :cond_4

    :goto_1
    invoke-direct {v3, v4, p0, v0}, LX/8Kk;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1309507
    :cond_3
    goto :goto_0

    .line 1309508
    :cond_4
    float-to-int p1, v0

    int-to-float v0, p1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1309509
    return-void
.end method

.method public final a(F)V
    .locals 4

    .prologue
    .line 1309510
    iget-wide v0, p0, LX/8Bb;->f:J

    long-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-long v0, v0

    iput-wide v0, p0, LX/8Bb;->g:J

    .line 1309511
    iget-wide v0, p0, LX/8Bb;->c:J

    iget-wide v2, p0, LX/8Bb;->g:J

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, LX/8Bb;->a(J)V

    .line 1309512
    return-void
.end method

.method public final a(LX/7zB;)V
    .locals 0

    .prologue
    .line 1309513
    return-void
.end method

.method public final a(LX/7zL;)V
    .locals 0

    .prologue
    .line 1309514
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1309515
    return-void
.end method
