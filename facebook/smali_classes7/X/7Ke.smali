.class public LX/7Ke;
.super Landroid/widget/VideoView;
.source ""

# interfaces
.implements LX/7Kd;


# instance fields
.field private a:LX/7Js;

.field private b:I

.field private c:I

.field private d:I

.field public final e:LX/3FM;

.field public f:LX/3FJ;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1196132
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7Ke;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1196133
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1196134
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7Ke;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1196135
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1196136
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1196137
    const/4 v0, 0x0

    iput-object v0, p0, LX/7Ke;->a:LX/7Js;

    .line 1196138
    const/4 v0, 0x0

    iput v0, p0, LX/7Ke;->b:I

    .line 1196139
    new-instance v0, LX/3FM;

    invoke-direct {v0}, LX/3FM;-><init>()V

    iput-object v0, p0, LX/7Ke;->e:LX/3FM;

    .line 1196140
    return-void
.end method


# virtual methods
.method public final a(LX/7K4;)V
    .locals 6

    .prologue
    .line 1196141
    invoke-virtual {p1}, LX/7K4;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1196142
    iget v0, p1, LX/7K4;->c:I

    invoke-virtual {p0, v0}, LX/7Ke;->seekTo(I)V

    .line 1196143
    :cond_0
    invoke-virtual {p0}, LX/7Ke;->start()V

    .line 1196144
    iget v0, p0, LX/7Ke;->d:I

    if-lez v0, :cond_2

    .line 1196145
    invoke-virtual {p0}, LX/7Ke;->getCurrentPosition()I

    move-result v0

    .line 1196146
    if-nez v0, :cond_1

    .line 1196147
    iget v0, p0, LX/7Ke;->b:I

    .line 1196148
    :cond_1
    iget v1, p0, LX/7Ke;->d:I

    sub-int v0, v1, v0

    .line 1196149
    iget-object v2, p0, LX/7Ke;->e:LX/3FM;

    sget v3, LX/3FM;->a:I

    invoke-virtual {v2, v3}, LX/3FM;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    .line 1196150
    iget-object v2, p0, LX/7Ke;->f:LX/3FJ;

    iput-object v2, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1196151
    iget-object v5, p0, LX/7Ke;->e:LX/3FM;

    if-lez v0, :cond_3

    int-to-long v2, v0

    :goto_0
    invoke-virtual {v5, v4, v2, v3}, LX/3FM;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1196152
    :cond_2
    return-void

    .line 1196153
    :cond_3
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1196154
    invoke-super {p0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1196155
    invoke-super {p0}, Landroid/widget/VideoView;->pause()V

    .line 1196156
    iget-object v0, p0, LX/7Ke;->a:LX/7Js;

    if-eqz v0, :cond_0

    .line 1196157
    iget-object v0, p0, LX/7Ke;->a:LX/7Js;

    invoke-virtual {v0}, LX/7Js;->a()V

    .line 1196158
    :cond_0
    iget-object v0, p0, LX/7Ke;->e:LX/3FM;

    sget v1, LX/3FM;->a:I

    invoke-virtual {v0, v1}, LX/3FM;->removeMessages(I)V

    .line 1196159
    return-void
.end method

.method public final c()Landroid/view/View;
    .locals 0

    .prologue
    .line 1196160
    return-object p0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1196161
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1196162
    iget-object v0, p0, LX/7Ke;->a:LX/7Js;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 2

    .prologue
    .line 1196163
    iput p1, p0, LX/7Ke;->b:I

    .line 1196164
    iget v0, p0, LX/7Ke;->c:I

    if-lez v0, :cond_0

    .line 1196165
    iget v0, p0, LX/7Ke;->b:I

    iget v1, p0, LX/7Ke;->c:I

    add-int/2addr v0, v1

    iput v0, p0, LX/7Ke;->b:I

    .line 1196166
    :cond_0
    iget v0, p0, LX/7Ke;->b:I

    invoke-super {p0, v0}, Landroid/widget/VideoView;->seekTo(I)V

    .line 1196167
    return-void
.end method

.method public getMetadata()LX/7IE;
    .locals 1

    .prologue
    .line 1196168
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSeekPosition()I
    .locals 1

    .prologue
    .line 1196130
    iget v0, p0, LX/7Ke;->b:I

    return v0
.end method

.method public getTrimStartPositionMs()I
    .locals 1

    .prologue
    .line 1196131
    iget v0, p0, LX/7Ke;->c:I

    return v0
.end method

.method public getVideoViewCurrentPosition()I
    .locals 1

    .prologue
    .line 1196101
    invoke-super {p0}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public getVideoViewDuration()I
    .locals 2

    .prologue
    .line 1196102
    iget v0, p0, LX/7Ke;->d:I

    if-lez v0, :cond_0

    .line 1196103
    iget v0, p0, LX/7Ke;->d:I

    iget v1, p0, LX/7Ke;->c:I

    sub-int/2addr v0, v1

    .line 1196104
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/VideoView;->getDuration()I

    move-result v0

    goto :goto_0
.end method

.method public getVideoViewDurationInMillis()I
    .locals 2

    .prologue
    .line 1196127
    iget v0, p0, LX/7Ke;->d:I

    if-lez v0, :cond_0

    .line 1196128
    iget v0, p0, LX/7Ke;->d:I

    iget v1, p0, LX/7Ke;->c:I

    sub-int/2addr v0, v1

    .line 1196129
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/VideoView;->getDuration()I

    move-result v0

    goto :goto_0
.end method

.method public getVideoViewHeight()I
    .locals 1

    .prologue
    .line 1196105
    invoke-super {p0}, Landroid/widget/VideoView;->getHeight()I

    move-result v0

    return v0
.end method

.method public setDataSource(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1196106
    invoke-super {p0, p1}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 1196107
    return-void
.end method

.method public setDelayedCompletionListener(LX/3FJ;)V
    .locals 0

    .prologue
    .line 1196108
    iput-object p1, p0, LX/7Ke;->f:LX/3FJ;

    .line 1196109
    return-void
.end method

.method public setVideoViewClickable(Z)V
    .locals 0

    .prologue
    .line 1196110
    invoke-super {p0, p1}, Landroid/widget/VideoView;->setClickable(Z)V

    .line 1196111
    return-void
.end method

.method public setVideoViewMediaController(Landroid/widget/MediaController;)V
    .locals 0

    .prologue
    .line 1196112
    invoke-super {p0, p1}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 1196113
    return-void
.end method

.method public setVideoViewOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0

    .prologue
    .line 1196114
    invoke-super {p0, p1}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 1196115
    return-void
.end method

.method public setVideoViewOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 0

    .prologue
    .line 1196116
    invoke-super {p0, p1}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1196117
    return-void
.end method

.method public setVideoViewOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 0

    .prologue
    .line 1196118
    invoke-super {p0, p1}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 1196119
    return-void
.end method

.method public final setVideoViewPath$48ad1708(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1196120
    const/4 v0, 0x0

    iput v0, p0, LX/7Ke;->b:I

    .line 1196121
    iget-object v0, p0, LX/7Ke;->a:LX/7Js;

    if-eqz v0, :cond_0

    .line 1196122
    iget-object v0, p0, LX/7Ke;->a:LX/7Js;

    invoke-virtual {v0}, LX/7Js;->a()V

    .line 1196123
    const/4 v0, 0x0

    iput-object v0, p0, LX/7Ke;->a:LX/7Js;

    .line 1196124
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 1196125
    return-void
.end method

.method public setVideoViewRotation(F)V
    .locals 0

    .prologue
    .line 1196126
    return-void
.end method
