.class public LX/7KF;
.super LX/7IB;
.source ""


# static fields
.field private static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/04g;",
            "LX/7KF;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:LX/04g;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 1194696
    const-class v0, LX/04g;

    invoke-static {v0}, LX/0PM;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    sput-object v0, LX/7KF;->d:Ljava/util/Map;

    .line 1194697
    invoke-static {}, LX/04g;->values()[LX/04g;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1194698
    sget-object v4, LX/7KF;->d:Ljava/util/Map;

    new-instance v5, LX/7KF;

    invoke-direct {v5, v3}, LX/7KF;-><init>(LX/04g;)V

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1194699
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1194700
    :cond_0
    return-void
.end method

.method private constructor <init>(LX/04g;)V
    .locals 0

    .prologue
    .line 1194692
    invoke-direct {p0}, LX/7IB;-><init>()V

    .line 1194693
    iput-object p1, p0, LX/7KF;->c:LX/04g;

    .line 1194694
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1194695
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TriggerTypeReason("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/7KF;->c:LX/04g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
