.class public LX/8Kq;
.super LX/8Ko;
.source ""


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1330783
    invoke-direct {p0}, LX/8Ko;-><init>()V

    .line 1330784
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8Kq;->a:Z

    .line 1330785
    return-void
.end method

.method private static h(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1330782
    const v0, 0x7f082027

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, LX/8Ko;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static i(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330781
    const v0, 0x7f082031

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;)I
    .locals 1

    .prologue
    .line 1330778
    iget-boolean v0, p0, LX/8Kq;->a:Z

    if-nez v0, :cond_0

    .line 1330779
    invoke-super {p0, p1}, LX/8Ko;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)I

    move-result v0

    .line 1330780
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;II)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330777
    invoke-static {p1}, LX/8Kq;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330774
    iget-boolean v0, p0, LX/8Kq;->a:Z

    if-nez v0, :cond_0

    .line 1330775
    invoke-super {p0, p1, p2}, LX/8Ko;->a(Landroid/content/Context;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;

    move-result-object v0

    .line 1330776
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, LX/8Kq;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/Boolean;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330786
    iget-boolean v0, p0, LX/8Kq;->a:Z

    if-nez v0, :cond_0

    .line 1330787
    invoke-super {p0, p1, p2, p3}, LX/8Ko;->a(Landroid/content/Context;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v0

    .line 1330788
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, LX/8Kq;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1330772
    iput-boolean p1, p0, LX/8Kq;->a:Z

    .line 1330773
    return-void
.end method

.method public final b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330767
    invoke-static {p1}, LX/8Kq;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330771
    invoke-static {p1}, LX/8Kq;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330770
    invoke-static {p1}, LX/8Kq;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330769
    invoke-static {p1}, LX/8Kq;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330768
    invoke-static {p1}, LX/8Kq;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
