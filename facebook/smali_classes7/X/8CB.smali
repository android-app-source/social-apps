.class public LX/8CB;
.super Landroid/widget/EditText;
.source ""


# instance fields
.field public a:F

.field public b:F

.field public c:F

.field public d:Landroid/animation/ValueAnimator;

.field public e:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1310353
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1310354
    invoke-virtual {p0}, LX/8CB;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string p1, "input_method"

    invoke-virtual {v0, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, LX/8CB;->e:Landroid/view/inputmethod/InputMethodManager;

    .line 1310355
    invoke-virtual {p0}, LX/8CB;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1310356
    instance-of p1, v0, LX/638;

    if-nez p1, :cond_1

    .line 1310357
    :cond_0
    :goto_0
    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, LX/8CB;->a:F

    .line 1310358
    iget v0, p0, LX/8CB;->a:F

    invoke-virtual {p0, v0}, LX/8CB;->setTextSize(F)V

    .line 1310359
    invoke-virtual {p0}, LX/8CB;->getTextSize()F

    move-result v0

    iput v0, p0, LX/8CB;->a:F

    .line 1310360
    iget v0, p0, LX/8CB;->a:F

    const/high16 p1, 0x40000000    # 2.0f

    div-float/2addr v0, p1

    iput v0, p0, LX/8CB;->b:F

    .line 1310361
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/8CB;->setIncludeFontPadding(Z)V

    .line 1310362
    new-instance v0, LX/8C8;

    invoke-direct {v0, p0}, LX/8C8;-><init>(LX/8CB;)V

    invoke-virtual {p0, v0}, LX/8CB;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1310363
    new-instance v0, LX/8C9;

    invoke-direct {v0, p0}, LX/8C9;-><init>(LX/8CB;)V

    invoke-virtual {p0, v0}, LX/8CB;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1310364
    new-instance v0, LX/8CA;

    invoke-direct {v0, p0}, LX/8CA;-><init>(LX/8CB;)V

    invoke-virtual {p0, v0}, LX/8CB;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1310365
    return-void

    .line 1310366
    :cond_1
    check-cast v0, LX/638;

    invoke-interface {v0}, LX/638;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1310367
    invoke-static {p0}, LX/191;->c(Landroid/widget/TextView;)V

    goto :goto_0
.end method

.method public static b(LX/8CB;)V
    .locals 3

    .prologue
    .line 1310349
    invoke-virtual {p0}, LX/8CB;->clearFocus()V

    .line 1310350
    iget-object v0, p0, LX/8CB;->e:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, LX/8CB;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1310351
    invoke-virtual {p0}, LX/8CB;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1310352
    return-void
.end method


# virtual methods
.method public getCurrentCursorLine()I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 1310345
    invoke-virtual {p0}, LX/8CB;->getSelectionStart()I

    move-result v1

    .line 1310346
    if-eq v1, v0, :cond_0

    .line 1310347
    invoke-virtual {p0}, LX/8CB;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v0

    .line 1310348
    :cond_0
    return v0
.end method

.method public final onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1310342
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 1310343
    invoke-static {p0}, LX/8CB;->b(LX/8CB;)V

    .line 1310344
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p2}, Landroid/widget/EditText;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
