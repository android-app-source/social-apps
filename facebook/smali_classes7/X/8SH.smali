.class public final LX/8SH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 1346281
    iput-object p1, p0, LX/8SH;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/16 v4, 0x19

    const/4 v5, 0x1

    .line 1346263
    iget-object v0, p0, LX/8SH;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v1

    .line 1346264
    iget-object v0, p0, LX/8SH;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    invoke-virtual {v0, p3}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1346265
    if-nez v0, :cond_0

    .line 1346266
    :goto_0
    return-void

    .line 1346267
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lt v2, v4, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1346268
    iget-object v0, p0, LX/8SH;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    const v1, 0x7f081302

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1346269
    new-instance v1, LX/0ju;

    iget-object v2, p0, LX/8SH;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, LX/8SH;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    const v3, 0x7f081300

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto :goto_0

    .line 1346270
    :cond_1
    iget-object v1, p0, LX/8SH;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v2, p0, LX/8SH;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v2, v2, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1346271
    invoke-static {v2}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v3

    .line 1346272
    iget-object v4, v0, LX/8QL;->a:LX/8vA;

    sget-object v5, LX/8vA;->USER:LX/8vA;

    if-eq v4, v5, :cond_2

    iget-object v4, v0, LX/8QL;->a:LX/8vA;

    sget-object v5, LX/8vA;->FRIENDLIST:LX/8vA;

    if-eq v4, v5, :cond_2

    .line 1346273
    sget-object v3, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;->v:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Unexpected selected option token of type %s"

    iget-object v5, v0, LX/8QL;->a:LX/8vA;

    invoke-virtual {v5}, LX/8vA;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v3

    invoke-virtual {v3}, LX/0VK;->g()LX/0VG;

    move-result-object v3

    .line 1346274
    iget-object v4, v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->i:LX/03V;

    invoke-virtual {v4, v3}, LX/03V;->a(LX/0VG;)V

    .line 1346275
    :goto_1
    iget-object v0, p0, LX/8SH;->a:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d()V

    goto/16 :goto_0

    .line 1346276
    :cond_2
    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1346277
    invoke-virtual {v2, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 1346278
    :goto_2
    invoke-virtual {v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->e()V

    .line 1346279
    invoke-virtual {v1}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->e()V

    goto :goto_1

    .line 1346280
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;Z)V

    goto :goto_2
.end method
