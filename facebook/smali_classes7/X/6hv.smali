.class public LX/6hv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field public c:J

.field public d:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1128792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128793
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1128794
    iput-object v0, p0, LX/6hv;->a:Ljava/util/List;

    .line 1128795
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/6hv;->c:J

    .line 1128796
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/6hv;->d:J

    .line 1128797
    return-void
.end method


# virtual methods
.method public final e()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;
    .locals 1

    .prologue
    .line 1128798
    new-instance v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;-><init>(LX/6hv;)V

    return-object v0
.end method
