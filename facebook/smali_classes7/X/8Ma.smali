.class public LX/8Ma;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1334808
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1334809
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/view/View;)Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;
    .locals 18

    .prologue
    .line 1334810
    new-instance v1, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    invoke-static/range {p0 .. p0}, LX/8Mb;->a(LX/0QB;)LX/8Mb;

    move-result-object v4

    check-cast v4, LX/8Mb;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v5

    check-cast v5, LX/0kb;

    invoke-static/range {p0 .. p0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v6

    check-cast v6, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static/range {p0 .. p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v7

    check-cast v7, LX/11S;

    invoke-static/range {p0 .. p0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v8

    check-cast v8, LX/0hy;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/1RW;->a(LX/0QB;)LX/1RW;

    move-result-object v10

    check-cast v10, LX/1RW;

    const/16 v2, 0x12cb

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v2, 0xc49

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v13

    check-cast v13, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v14

    check-cast v14, LX/0b3;

    invoke-static/range {p0 .. p0}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v15

    check-cast v15, LX/1EZ;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v16

    check-cast v16, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v17

    check-cast v17, LX/0ad;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v17}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;-><init>(Landroid/content/Context;Landroid/view/View;LX/8Mb;LX/0kb;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/11S;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/1RW;LX/0Ot;LX/0Ot;LX/0SG;LX/0b3;LX/1EZ;LX/03V;LX/0ad;)V

    .line 1334811
    return-object v1
.end method
