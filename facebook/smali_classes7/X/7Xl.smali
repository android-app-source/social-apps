.class public final LX/7Xl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1rM;


# direct methods
.method public constructor <init>(LX/1rM;)V
    .locals 0

    .prologue
    .line 1218662
    iput-object p1, p0, LX/7Xl;->a:LX/1rM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1218663
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_0

    .line 1218664
    :goto_0
    return-void

    .line 1218665
    :cond_0
    iget-object v0, p0, LX/7Xl;->a:LX/1rM;

    invoke-static {v0, p1}, LX/1rM;->a$redex0(LX/1rM;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1218666
    check-cast p1, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;

    .line 1218667
    iget-object v0, p0, LX/7Xl;->a:LX/1rM;

    .line 1218668
    iget-object v1, v0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v1, LX/0df;->x:LX/0Tn;

    const-string v3, "type_key"

    invoke-virtual {v1, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v1, LX/0df;->x:LX/0Tn;

    const-string v3, "ttl_key"

    invoke-virtual {v1, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->b()I

    move-result v3

    invoke-interface {v2, v1, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v2

    sget-object v1, LX/0df;->x:LX/0Tn;

    const-string v3, "delay_interval_key"

    invoke-virtual {v1, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->c()I

    move-result v3

    invoke-interface {v2, v1, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1218669
    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->c()I

    move-result v2

    int-to-long v3, v2

    invoke-static {v0, v1, v3, v4}, LX/1rM;->a(LX/1rM;Ljava/lang/String;J)V

    .line 1218670
    return-void
.end method
