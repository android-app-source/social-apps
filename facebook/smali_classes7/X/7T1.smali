.class public LX/7T1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/7T1;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/graphics/SurfaceTexture;

.field private final c:LX/7T2;

.field private final d:I

.field private final e:Ljava/lang/Object;

.field private f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1209574
    const-class v0, LX/7T1;

    sput-object v0, LX/7T1;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/SurfaceTexture;LX/7T2;I)V
    .locals 1

    .prologue
    .line 1209529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1209530
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/7T1;->e:Ljava/lang/Object;

    .line 1209531
    iput-object p1, p0, LX/7T1;->b:Landroid/graphics/SurfaceTexture;

    .line 1209532
    iput-object p2, p0, LX/7T1;->c:LX/7T2;

    .line 1209533
    iput p3, p0, LX/7T1;->d:I

    .line 1209534
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 1209535
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 1209536
    iget v2, p0, LX/7T1;->d:I

    int-to-long v2, v2

    const-wide/32 v4, 0xf4240

    mul-long/2addr v2, v4

    .line 1209537
    add-long/2addr v2, v0

    .line 1209538
    iget-object v4, p0, LX/7T1;->e:Ljava/lang/Object;

    monitor-enter v4

    .line 1209539
    :goto_0
    :try_start_0
    iget-boolean v5, p0, LX/7T1;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v5, :cond_0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 1209540
    :try_start_1
    iget-object v0, p0, LX/7T1;->e:Ljava/lang/Object;

    iget v1, p0, LX/7T1;->d:I

    int-to-long v6, v1

    const v1, -0x38416548

    invoke-static {v0, v6, v7, v1}, LX/02L;->a(Ljava/lang/Object;JI)V

    .line 1209541
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    goto :goto_0

    .line 1209542
    :catch_0
    move-exception v0

    .line 1209543
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 1209544
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1209545
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1209546
    :cond_0
    :try_start_3
    iget-boolean v0, p0, LX/7T1;->f:Z

    if-nez v0, :cond_1

    .line 1209547
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Surface frame wait timed out"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209548
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7T1;->f:Z

    .line 1209549
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1209550
    const-string v0, "before updateTexImage"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1209551
    iget-object v0, p0, LX/7T1;->b:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 1209552
    return-void
.end method

.method public final b()V
    .locals 12

    .prologue
    .line 1209553
    iget-object v0, p0, LX/7T1;->c:LX/7T2;

    iget-object v1, p0, LX/7T1;->b:Landroid/graphics/SurfaceTexture;

    .line 1209554
    iget-object v2, v0, LX/7T2;->i:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 1209555
    iget-object v5, v0, LX/7T2;->j:LX/5Pf;

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1209556
    iget-object v5, v0, LX/7T2;->e:[F

    invoke-virtual {v1, v5}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 1209557
    iget-object v5, v0, LX/7T2;->i:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/61B;

    .line 1209558
    iget-object v6, v0, LX/7T2;->e:[F

    iget-object v7, v0, LX/7T2;->f:[F

    iget-object v8, v0, LX/7T2;->g:[F

    const-wide/16 v9, -0x1

    invoke-interface/range {v5 .. v10}, LX/61B;->a([F[F[FJ)V

    goto :goto_0

    .line 1209559
    :cond_0
    :goto_1
    return-void

    .line 1209560
    :cond_1
    const-string v2, "onDrawFrame start"

    invoke-static {v2}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1209561
    iget-object v2, v0, LX/7T2;->e:[F

    invoke-virtual {v1, v2}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 1209562
    const/16 v2, 0x4100

    invoke-static {v2}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1209563
    const v2, 0x84c0

    invoke-static {v2}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 1209564
    const v2, 0x8d65

    iget v3, v0, LX/7T2;->h:I

    invoke-static {v2, v3}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1209565
    iget-object v2, v0, LX/7T2;->c:LX/5Pb;

    invoke-virtual {v2}, LX/5Pb;->a()LX/5Pa;

    move-result-object v2

    const-string v3, "uSTMatrix"

    iget-object v4, v0, LX/7T2;->e:[F

    invoke-virtual {v2, v3, v4}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v2

    const-string v3, "uConstMatrix"

    iget-object v4, v0, LX/7T2;->f:[F

    invoke-virtual {v2, v3, v4}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v2

    iget-object v3, v0, LX/7T2;->b:LX/5PR;

    invoke-virtual {v2, v3}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    .line 1209566
    invoke-static {}, Landroid/opengl/GLES20;->glFinish()V

    goto :goto_1
.end method

.method public final onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .locals 3

    .prologue
    .line 1209567
    iget-object v1, p0, LX/7T1;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 1209568
    :try_start_0
    iget-boolean v0, p0, LX/7T1;->f:Z

    if-eqz v0, :cond_0

    .line 1209569
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "mFrameAvailable already set, frame could be dropped"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209570
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1209571
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/7T1;->f:Z

    .line 1209572
    iget-object v0, p0, LX/7T1;->e:Ljava/lang/Object;

    const v2, 0x6dcda41a

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1209573
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
