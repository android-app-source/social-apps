.class public LX/73q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputMethodManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1166334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1166335
    iput-object p1, p0, LX/73q;->a:Landroid/view/inputmethod/InputMethodManager;

    .line 1166336
    return-void
.end method

.method public static a(LX/0QB;)LX/73q;
    .locals 1

    .prologue
    .line 1166329
    invoke-static {p0}, LX/73q;->b(LX/0QB;)LX/73q;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/73q;
    .locals 2

    .prologue
    .line 1166337
    new-instance v1, LX/73q;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-direct {v1, v0}, LX/73q;-><init>(Landroid/view/inputmethod/InputMethodManager;)V

    .line 1166338
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 1166330
    invoke-virtual {p1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 1166331
    if-eqz v0, :cond_0

    .line 1166332
    iget-object v1, p0, LX/73q;->a:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1166333
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;Landroid/view/View;)V
    .locals 2
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1166321
    if-nez p2, :cond_0

    .line 1166322
    invoke-virtual {p1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object p2

    .line 1166323
    :cond_0
    if-nez p2, :cond_1

    .line 1166324
    :goto_0
    return-void

    .line 1166325
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1166326
    invoke-virtual {p2}, Landroid/view/View;->clearFocus()V

    .line 1166327
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->requestFocus()Z

    .line 1166328
    iget-object v0, p0, LX/73q;->a:Landroid/view/inputmethod/InputMethodManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0
.end method
