.class public final enum LX/7Gm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Gm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Gm;

.field public static final enum COMMENT:LX/7Gm;

.field public static final enum COMPOSER:LX/7Gm;

.field public static final enum PHOTO:LX/7Gm;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1189825
    new-instance v0, LX/7Gm;

    const-string v1, "COMPOSER"

    invoke-direct {v0, v1, v2}, LX/7Gm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Gm;->COMPOSER:LX/7Gm;

    .line 1189826
    new-instance v0, LX/7Gm;

    const-string v1, "COMMENT"

    invoke-direct {v0, v1, v3}, LX/7Gm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Gm;->COMMENT:LX/7Gm;

    .line 1189827
    new-instance v0, LX/7Gm;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v4}, LX/7Gm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Gm;->PHOTO:LX/7Gm;

    .line 1189828
    const/4 v0, 0x3

    new-array v0, v0, [LX/7Gm;

    sget-object v1, LX/7Gm;->COMPOSER:LX/7Gm;

    aput-object v1, v0, v2

    sget-object v1, LX/7Gm;->COMMENT:LX/7Gm;

    aput-object v1, v0, v3

    sget-object v1, LX/7Gm;->PHOTO:LX/7Gm;

    aput-object v1, v0, v4

    sput-object v0, LX/7Gm;->$VALUES:[LX/7Gm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1189822
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Gm;
    .locals 1

    .prologue
    .line 1189823
    const-class v0, LX/7Gm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Gm;

    return-object v0
.end method

.method public static values()[LX/7Gm;
    .locals 1

    .prologue
    .line 1189824
    sget-object v0, LX/7Gm;->$VALUES:[LX/7Gm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Gm;

    return-object v0
.end method
