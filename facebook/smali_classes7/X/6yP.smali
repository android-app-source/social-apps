.class public abstract LX/6yP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<STY",
        "LE_RENDERER::Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleRenderer;",
        "ANA",
        "LYTICS_EVENT_SELECTOR::Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEventSelector;",
        "CONFIGURATOR::",
        "LX/6y0;",
        "VIEW_CONTRO",
        "LLER::Lcom/facebook/payments/paymentmethods/cardform/CardFormViewController;",
        "MUTATOR::",
        "LX/6yL;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/6yO;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TSTY",
            "LE_RENDERER;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TANA",
            "LYTICS_EVENT_SELECTOR;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TCONFIGURATOR;>;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TVIEW_CONTRO",
            "LLER;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TMUTATOR;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/6yO;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6yO;",
            "LX/0Ot",
            "<TSTY",
            "LE_RENDERER;",
            ">;",
            "LX/0Ot",
            "<TANA",
            "LYTICS_EVENT_SELECTOR;",
            ">;",
            "LX/0Ot",
            "<TCONFIGURATOR;>;",
            "LX/0Ot",
            "<TVIEW_CONTRO",
            "LLER;",
            ">;",
            "LX/0Ot",
            "<TMUTATOR;>;)V"
        }
    .end annotation

    .prologue
    .line 1159942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159943
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6yO;

    iput-object v0, p0, LX/6yP;->a:LX/6yO;

    .line 1159944
    iput-object p2, p0, LX/6yP;->b:LX/0Ot;

    .line 1159945
    iput-object p3, p0, LX/6yP;->c:LX/0Ot;

    .line 1159946
    iput-object p4, p0, LX/6yP;->d:LX/0Ot;

    .line 1159947
    iput-object p5, p0, LX/6yP;->e:LX/0Ot;

    .line 1159948
    iput-object p6, p0, LX/6yP;->f:LX/0Ot;

    .line 1159949
    return-void
.end method
