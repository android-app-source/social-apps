.class public LX/8Om;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/8OM;


# static fields
.field private static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/String;",
            "LX/8Ob;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final A:LX/8Oj;

.field private final B:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final C:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final D:LX/0ad;

.field private final E:LX/6bD;

.field private final F:LX/8BN;

.field private G:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "LX/6bA;",
            ">;"
        }
    .end annotation
.end field

.field private H:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "LX/8BM;",
            ">;"
        }
    .end annotation
.end field

.field public final I:LX/0lB;

.field private final J:LX/0Uh;

.field private final K:LX/8K9;

.field public final L:LX/0cX;

.field public a:LX/74n;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/8Of;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/8KY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final f:LX/0SG;

.field public final g:LX/0b3;

.field public final h:LX/11H;

.field public final i:LX/8LX;

.field private final j:LX/0cW;

.field public final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8Ne;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/8OL;

.field private final m:LX/03V;

.field private final n:LX/8NU;

.field private final o:LX/8NX;

.field private final p:LX/8NP;

.field public final q:LX/8NN;

.field public final r:LX/8Ok;

.field public final s:LX/8Oh;

.field private final t:LX/0So;

.field private final u:LX/74B;

.field private final v:Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;

.field public w:LX/8On;

.field private x:LX/8Og;

.field private final y:LX/0Sh;

.field public z:Ljava/util/concurrent/Semaphore;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1340764
    const-class v0, LX/8Om;

    sput-object v0, LX/8Om;->d:Ljava/lang/Class;

    .line 1340765
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, LX/8Om;->e:Ljava/util/WeakHashMap;

    return-void
.end method

.method public constructor <init>(LX/11H;LX/0SG;LX/0b3;LX/8LX;LX/0cW;LX/0Or;LX/8OL;LX/03V;LX/8NU;LX/8NX;LX/8NP;LX/8NN;LX/0So;LX/8Oh;LX/8Og;Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;LX/0Or;LX/0Sh;LX/74B;LX/0Or;LX/0ad;LX/6bD;LX/8BN;LX/0lB;LX/0Uh;LX/8K9;LX/0cX;)V
    .locals 4
    .param p17    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/upload/gatekeeper/VideoUploadCancelRequestEnabled;
        .end annotation
    .end param
    .param p20    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/upload/gatekeeper/VideoUploadCombineRetriesEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/0SG;",
            "LX/0b3;",
            "LX/8LX;",
            "LX/0cW;",
            "LX/0Or",
            "<",
            "LX/8Ne;",
            ">;",
            "LX/8OL;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/8NU;",
            "LX/8NX;",
            "LX/8NP;",
            "LX/8NN;",
            "LX/0So;",
            "LX/8Oh;",
            "LX/8Og;",
            "Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/74B;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0ad;",
            "LX/6bD;",
            "LX/8BN;",
            "LX/0lB;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/8K9;",
            "LX/0cX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1340766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1340767
    new-instance v1, LX/8Oj;

    invoke-direct {v1}, LX/8Oj;-><init>()V

    iput-object v1, p0, LX/8Om;->A:LX/8Oj;

    .line 1340768
    iput-object p1, p0, LX/8Om;->h:LX/11H;

    .line 1340769
    iput-object p2, p0, LX/8Om;->f:LX/0SG;

    .line 1340770
    iput-object p3, p0, LX/8Om;->g:LX/0b3;

    .line 1340771
    iput-object p4, p0, LX/8Om;->i:LX/8LX;

    .line 1340772
    iput-object p5, p0, LX/8Om;->j:LX/0cW;

    .line 1340773
    iput-object p6, p0, LX/8Om;->k:LX/0Or;

    .line 1340774
    iput-object p7, p0, LX/8Om;->l:LX/8OL;

    .line 1340775
    iput-object p8, p0, LX/8Om;->m:LX/03V;

    .line 1340776
    iput-object p9, p0, LX/8Om;->n:LX/8NU;

    .line 1340777
    iput-object p10, p0, LX/8Om;->o:LX/8NX;

    .line 1340778
    iput-object p11, p0, LX/8Om;->p:LX/8NP;

    .line 1340779
    move-object/from16 v0, p12

    iput-object v0, p0, LX/8Om;->q:LX/8NN;

    .line 1340780
    new-instance v1, LX/8Ok;

    iget-object v2, p0, LX/8Om;->f:LX/0SG;

    iget-object v3, p0, LX/8Om;->l:LX/8OL;

    invoke-direct {v1, p0, v2, v3}, LX/8Ok;-><init>(LX/8Om;LX/0SG;LX/8OL;)V

    iput-object v1, p0, LX/8Om;->r:LX/8Ok;

    .line 1340781
    move-object/from16 v0, p13

    iput-object v0, p0, LX/8Om;->t:LX/0So;

    .line 1340782
    move-object/from16 v0, p16

    iput-object v0, p0, LX/8Om;->v:Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;

    .line 1340783
    move-object/from16 v0, p14

    iput-object v0, p0, LX/8Om;->s:LX/8Oh;

    .line 1340784
    move-object/from16 v0, p15

    iput-object v0, p0, LX/8Om;->x:LX/8Og;

    .line 1340785
    move-object/from16 v0, p17

    iput-object v0, p0, LX/8Om;->B:LX/0Or;

    .line 1340786
    move-object/from16 v0, p18

    iput-object v0, p0, LX/8Om;->y:LX/0Sh;

    .line 1340787
    move-object/from16 v0, p19

    iput-object v0, p0, LX/8Om;->u:LX/74B;

    .line 1340788
    move-object/from16 v0, p20

    iput-object v0, p0, LX/8Om;->C:LX/0Or;

    .line 1340789
    move-object/from16 v0, p21

    iput-object v0, p0, LX/8Om;->D:LX/0ad;

    .line 1340790
    move-object/from16 v0, p22

    iput-object v0, p0, LX/8Om;->E:LX/6bD;

    .line 1340791
    move-object/from16 v0, p23

    iput-object v0, p0, LX/8Om;->F:LX/8BN;

    .line 1340792
    move-object/from16 v0, p24

    iput-object v0, p0, LX/8Om;->I:LX/0lB;

    .line 1340793
    move-object/from16 v0, p25

    iput-object v0, p0, LX/8Om;->J:LX/0Uh;

    .line 1340794
    move-object/from16 v0, p26

    iput-object v0, p0, LX/8Om;->K:LX/8K9;

    .line 1340795
    move-object/from16 v0, p27

    iput-object v0, p0, LX/8Om;->L:LX/0cX;

    .line 1340796
    return-void
.end method

.method private static a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ob;LX/8Oa;)LX/8NY;
    .locals 20

    .prologue
    .line 1340797
    move-object/from16 v0, p1

    iget-object v2, v0, LX/8Ob;->E:LX/60x;

    if-eqz v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, LX/8Ob;->E:LX/60x;

    invoke-virtual {v2}, LX/60x;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v17, 0x1

    .line 1340798
    :goto_0
    new-instance v3, LX/8NY;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->C()J

    move-result-wide v4

    move-object/from16 v0, p1

    iget-wide v6, v0, LX/8Ob;->l:J

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    iget-object v9, v0, LX/8Ob;->f:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->P()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->X()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->Y()Z

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->Z()Z

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->an()LX/5Rn;

    move-result-object v2

    sget-object v14, LX/5Rn;->NORMAL:LX/5Rn;

    if-ne v2, v14, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->ao()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v15

    move-object/from16 v16, p2

    invoke-direct/range {v3 .. v17}, LX/8NY;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;ZZLX/0am;LX/0am;LX/8Oa;Z)V

    return-object v3

    .line 1340799
    :cond_0
    const/16 v17, 0x0

    goto :goto_0

    .line 1340800
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static a(LX/8Om;LX/8Ob;LX/8Oa;LX/8Ne;LX/73w;)LX/8NZ;
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 1340801
    const/4 v0, 0x0

    .line 1340802
    iget-object v2, p1, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1340803
    const-wide/16 v6, 0x0

    move-object v9, v8

    move v10, v0

    .line 1340804
    :goto_0
    :try_start_0
    iget-object v1, p1, LX/8Ob;->A:LX/74b;

    invoke-virtual {p1}, LX/8Ob;->b()Ljava/util/Map;

    move-result-object v3

    iget-object v4, p1, LX/8Ob;->q:Ljava/lang/String;

    iget-object v5, p1, LX/8Ob;->k:Ljava/lang/String;

    move-object v0, p4

    invoke-virtual/range {v0 .. v5}, LX/73w;->a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 1340805
    iget-object v0, p0, LX/8Om;->h:LX/11H;

    iget-object v1, p0, LX/8Om;->o:LX/8NX;

    invoke-static {v2, p1, p2}, LX/8Om;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ob;LX/8Oa;)LX/8NY;

    move-result-object v3

    iget-object v4, p1, LX/8Ob;->c:LX/14U;

    invoke-virtual {v0, v1, v3, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8NZ;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1340806
    const/4 v1, 0x1

    .line 1340807
    :goto_1
    if-nez v1, :cond_0

    const-wide/16 v4, 0x1

    add-long/2addr v6, v4

    invoke-interface {p3}, LX/8Ne;->b()I

    move-result v3

    int-to-long v4, v3

    cmp-long v3, v6, v4

    if-lez v3, :cond_3

    .line 1340808
    :cond_0
    if-nez v1, :cond_1

    iget-object v1, p1, LX/8Ob;->w:Ljava/lang/Exception;

    if-eqz v1, :cond_1

    .line 1340809
    iget-object v0, p1, LX/8Ob;->w:Ljava/lang/Exception;

    throw v0

    .line 1340810
    :catch_0
    move-exception v5

    move-object v3, p0

    move-object v4, p1

    .line 1340811
    invoke-direct/range {v3 .. v8}, LX/8Om;->a(LX/8Ob;Ljava/lang/Exception;JLX/8O2;)Landroid/util/Pair;

    move-object v0, v9

    move v1, v10

    goto :goto_1

    .line 1340812
    :cond_1
    if-nez v0, :cond_2

    .line 1340813
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1340814
    :cond_2
    invoke-interface {p3}, LX/8Ne;->a()V

    .line 1340815
    return-object v0

    :cond_3
    move-object v9, v0

    move v10, v1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/8Om;
    .locals 1

    .prologue
    .line 1340816
    invoke-static {p0}, LX/8Om;->b(LX/0QB;)LX/8Om;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/8Ob;Ljava/lang/Exception;JLX/8O2;)Landroid/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8Ob;",
            "Ljava/lang/Exception;",
            "J",
            "LX/8O2;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1340817
    iput-object p2, p1, LX/8Ob;->w:Ljava/lang/Exception;

    .line 1340818
    iget-object v1, p0, LX/8Om;->w:LX/8On;

    iget-object v3, p1, LX/8Ob;->x:LX/8Oi;

    move-object v2, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, LX/8On;->a(Ljava/lang/Exception;LX/8Oi;JLX/8O2;)Landroid/util/Pair;

    move-result-object v1

    .line 1340819
    if-eqz v1, :cond_0

    .line 1340820
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, p1, LX/8Ob;->u:J

    .line 1340821
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, p1, LX/8Ob;->v:J

    .line 1340822
    :cond_0
    return-object v1
.end method

.method private a(LX/73w;LX/8Ob;LX/8Ne;)Ljava/lang/Boolean;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1340823
    iget-object v8, p2, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move v7, v0

    .line 1340824
    :goto_0
    :try_start_0
    iget-object v1, v8, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    move-object v1, v1

    .line 1340825
    sget-object v2, LX/8LR;->MULTIMEDIA:LX/8LR;

    if-eq v1, v2, :cond_0

    .line 1340826
    iget-object v1, v8, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    move-object v1, v1

    .line 1340827
    sget-object v2, LX/8LR;->EDIT_MULTIMEDIA:LX/8LR;

    if-eq v1, v2, :cond_0

    .line 1340828
    iget-object v1, p2, LX/8Ob;->A:LX/74b;

    iget-object v2, p2, LX/8Ob;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2, v8}, LX/73w;->a(LX/74b;Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1340829
    iget-object v1, p0, LX/8Om;->h:LX/11H;

    iget-object v2, p0, LX/8Om;->p:LX/8NP;

    iget-object v3, p2, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v8}, LX/8NQ;->a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)LX/8NQ;

    move-result-object v3

    iget-object v4, p2, LX/8Ob;->c:LX/14U;

    invoke-virtual {v1, v2, v3, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    .line 1340830
    :cond_0
    invoke-interface {p3}, LX/8Ne;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1340831
    const/4 v0, 0x1

    .line 1340832
    :goto_1
    if-nez v0, :cond_1

    add-int/lit8 v1, v7, 0x1

    invoke-interface {p3}, LX/8Ne;->b()I

    move-result v2

    if-le v1, v2, :cond_3

    .line 1340833
    :cond_1
    if-nez v0, :cond_2

    .line 1340834
    iget-object v0, p2, LX/8Ob;->w:Ljava/lang/Exception;

    throw v0

    .line 1340835
    :catch_0
    move-exception v3

    .line 1340836
    int-to-long v4, v7

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v1 .. v6}, LX/8Om;->a(LX/8Ob;Ljava/lang/Exception;JLX/8O2;)Landroid/util/Pair;

    goto :goto_1

    .line 1340837
    :cond_2
    iget-object v1, p2, LX/8Ob;->A:LX/74b;

    iget-object v2, p2, LX/8Ob;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2, v8}, LX/73w;->b(LX/74b;Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1340838
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_3
    move v7, v1

    goto :goto_0
.end method

.method private a(LX/73w;LX/8Ob;LX/8Oa;LX/8Ne;)V
    .locals 19

    .prologue
    .line 1340737
    move-object/from16 v0, p2

    iget-object v8, v0, LX/8Ob;->A:LX/74b;

    const/4 v9, 0x0

    sget-object v10, LX/741;->LOCAL:LX/741;

    move-object/from16 v0, p2

    iget-object v6, v0, LX/8Ob;->j:Ljava/lang/String;

    invoke-static {v6}, LX/8Oh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p2

    iget-wide v12, v0, LX/8Ob;->m:J

    move-object/from16 v0, p2

    iget-wide v14, v0, LX/8Ob;->l:J

    move-object/from16 v0, p2

    iget-wide v0, v0, LX/8Ob;->r:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p2

    iget-object v0, v0, LX/8Ob;->n:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v18}, LX/73w;->a(LX/74b;ILX/741;Ljava/lang/String;JJJLjava/lang/String;)V

    .line 1340738
    move-object/from16 v0, p2

    iget-object v6, v0, LX/8Ob;->c:LX/14U;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, LX/14U;->a(LX/4ck;)V

    .line 1340739
    move-object/from16 v0, p2

    iget-wide v6, v0, LX/8Ob;->m:J

    const-wide/32 v8, 0x17d7840

    cmp-long v6, v6, v8

    if-gez v6, :cond_0

    move-object/from16 v0, p2

    iget-boolean v6, v0, LX/8Ob;->o:Z

    if-nez v6, :cond_0

    .line 1340740
    move-object/from16 v0, p2

    iget-object v6, v0, LX/8Ob;->A:LX/74b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/73w;->a(LX/74b;)V

    .line 1340741
    new-instance v6, Ljava/io/File;

    move-object/from16 v0, p2

    iget-object v7, v0, LX/8Ob;->q:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1340742
    invoke-static {}, LX/51t;->a()LX/51l;

    move-result-object v7

    invoke-static {v6, v7}, LX/1t3;->a(Ljava/io/File;LX/51l;)LX/51o;

    move-result-object v6

    invoke-virtual {v6}, LX/51o;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p2

    iput-object v6, v0, LX/8Ob;->f:Ljava/lang/String;

    .line 1340743
    move-object/from16 v0, p2

    iget-object v6, v0, LX/8Ob;->A:LX/74b;

    move-object/from16 v0, p2

    iget-wide v8, v0, LX/8Ob;->m:J

    move-object/from16 v0, p2

    iget-object v7, v0, LX/8Ob;->f:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v8, v9, v7}, LX/73w;->a(LX/74b;JLjava/lang/String;)V

    .line 1340744
    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p1

    invoke-static {v0, v1, v2, v3, v4}, LX/8Om;->a(LX/8Om;LX/8Ob;LX/8Oa;LX/8Ne;LX/73w;)LX/8NZ;

    move-result-object v6

    .line 1340745
    invoke-virtual {v6}, LX/8NZ;->a()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p2

    iput-object v7, v0, LX/8Ob;->d:Ljava/lang/String;

    .line 1340746
    move-object/from16 v0, p2

    iget-object v7, v0, LX/8Ob;->d:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, p2

    iput-object v7, v0, LX/8Ob;->b:Ljava/lang/Long;

    .line 1340747
    invoke-virtual {v6}, LX/8NZ;->b()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p2

    iput-object v7, v0, LX/8Ob;->e:Ljava/lang/String;

    .line 1340748
    invoke-virtual {v6}, LX/8NZ;->e()Z

    move-result v7

    move-object/from16 v0, p2

    iput-boolean v7, v0, LX/8Ob;->i:Z

    .line 1340749
    move-object/from16 v0, p2

    iget-object v7, v0, LX/8Ob;->A:LX/74b;

    move-object/from16 v0, p2

    iget-object v8, v0, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    move-object/from16 v0, p2

    iget-object v10, v0, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8, v9, v10}, LX/73w;->a(LX/74b;JLcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1340750
    invoke-virtual {v6}, LX/8NZ;->c()J

    move-result-wide v8

    move-object/from16 v0, p2

    iput-wide v8, v0, LX/8Ob;->u:J

    .line 1340751
    invoke-virtual {v6}, LX/8NZ;->d()J

    move-result-wide v8

    invoke-virtual {v6}, LX/8NZ;->c()J

    move-result-wide v6

    sub-long v6, v8, v6

    move-object/from16 v0, p2

    iput-wide v6, v0, LX/8Ob;->v:J

    .line 1340752
    move-object/from16 v0, p0

    iget-object v6, v0, LX/8Om;->l:LX/8OL;

    const-string v7, "after init video"

    invoke-virtual {v6, v7}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1340753
    return-void

    .line 1340754
    :cond_0
    move-object/from16 v0, p2

    iget-object v7, v0, LX/8Ob;->A:LX/74b;

    move-object/from16 v0, p2

    iget-wide v8, v0, LX/8Ob;->m:J

    move-object/from16 v0, p2

    iget-wide v10, v0, LX/8Ob;->l:J

    move-object/from16 v0, p2

    iget-boolean v12, v0, LX/8Ob;->o:Z

    move-object/from16 v6, p1

    invoke-virtual/range {v6 .. v12}, LX/73w;->a(LX/74b;JJZ)V

    goto :goto_0
.end method

.method private static a(LX/8Om;LX/73w;LX/8Ob;LX/8OV;LX/8Ne;Ljava/lang/String;)V
    .locals 18

    .prologue
    .line 1340839
    move-object/from16 v0, p2

    iget-object v14, v0, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1340840
    if-eqz p3, :cond_1

    const/4 v2, 0x1

    .line 1340841
    :goto_0
    move-object/from16 v0, p2

    iget-object v11, v0, LX/8Ob;->g:Ljava/util/Map;

    move-object/from16 v0, p2

    iget-object v12, v0, LX/8Ob;->j:Ljava/lang/String;

    new-instance v3, Lcom/facebook/photos/upload/operation/UploadRecord;

    move-object/from16 v0, p2

    iget-object v4, v0, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, LX/8Om;->f:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-boolean v9, v0, LX/8Ob;->i:Z

    move-object/from16 v0, p2

    iget-object v10, v0, LX/8Ob;->e:Ljava/lang/String;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/photos/upload/operation/UploadRecord;-><init>(JJZZLjava/lang/String;)V

    invoke-interface {v11, v12, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340842
    move-object/from16 v0, p2

    iget-object v11, v0, LX/8Ob;->g:Ljava/util/Map;

    move-object/from16 v0, p2

    iget-object v12, v0, LX/8Ob;->d:Ljava/lang/String;

    new-instance v3, Lcom/facebook/photos/upload/operation/UploadRecord;

    move-object/from16 v0, p2

    iget-wide v4, v0, LX/8Ob;->u:J

    move-object/from16 v0, p2

    iget-wide v6, v0, LX/8Ob;->u:J

    move-object/from16 v0, p2

    iget-wide v8, v0, LX/8Ob;->v:J

    add-long/2addr v6, v8

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, ""

    invoke-direct/range {v3 .. v10}, Lcom/facebook/photos/upload/operation/UploadRecord;-><init>(JJZZLjava/lang/String;)V

    invoke-interface {v11, v12, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340843
    move-object/from16 v0, p2

    iget-object v3, v0, LX/8Ob;->c:LX/14U;

    move-object/from16 v0, p2

    iget-object v4, v0, LX/8Ob;->h:LX/8Oo;

    invoke-virtual {v3, v4}, LX/14U;->a(LX/4ck;)V

    .line 1340844
    move-object/from16 v0, p2

    iget-object v3, v0, LX/8Ob;->A:LX/74b;

    move-object/from16 v0, p2

    iget-wide v4, v0, LX/8Ob;->l:J

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->g()I

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v6}, LX/73w;->b(LX/74b;JI)V

    .line 1340845
    move-object/from16 v0, p2

    iget-object v3, v0, LX/8Ob;->h:LX/8Oo;

    move-object/from16 v0, p2

    iget-wide v4, v0, LX/8Ob;->l:J

    invoke-virtual {v3, v4, v5}, LX/8Oo;->c(J)V

    .line 1340846
    move-object/from16 v0, p0

    iget-object v11, v0, LX/8Om;->j:LX/0cW;

    new-instance v3, Lcom/facebook/photos/upload/operation/UploadRecord;

    move-object/from16 v0, p2

    iget-object v4, v0, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, LX/8Om;->f:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-boolean v9, v0, LX/8Ob;->i:Z

    move-object/from16 v0, p2

    iget-object v10, v0, LX/8Ob;->e:Ljava/lang/String;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/photos/upload/operation/UploadRecord;-><init>(JJZZLjava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v11, v0, v3}, LX/0cW;->a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadRecord;)Z

    move-result v3

    .line 1340847
    if-nez v3, :cond_0

    .line 1340848
    move-object/from16 v0, p2

    iget-object v3, v0, LX/8Ob;->A:LX/74b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/73w;->d(LX/74b;)V

    .line 1340849
    :cond_0
    move-object/from16 v0, p2

    iget-boolean v3, v0, LX/8Ob;->i:Z

    if-eqz v3, :cond_2

    .line 1340850
    move-object/from16 v0, p2

    iget-object v2, v0, LX/8Ob;->A:LX/74b;

    move-object/from16 v0, p2

    iget-object v3, v0, LX/8Ob;->f:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/73w;->a(LX/74b;Ljava/lang/String;)V

    .line 1340851
    :goto_1
    move-object/from16 v0, p2

    iget-object v2, v0, LX/8Ob;->A:LX/74b;

    move-object/from16 v0, p2

    iget-wide v4, v0, LX/8Ob;->l:J

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->d()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5, v3}, LX/73w;->c(LX/74b;JI)V

    .line 1340852
    move-object/from16 v0, p2

    iget-object v2, v0, LX/8Ob;->c:LX/14U;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/14U;->a(LX/4ck;)V

    .line 1340853
    return-void

    .line 1340854
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1340855
    :cond_2
    if-eqz v2, :cond_3

    .line 1340856
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Om;->x:LX/8Og;

    move-object/from16 v0, p2

    iget-object v5, v0, LX/8Ob;->k:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v9, v0, LX/8Ob;->A:LX/74b;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/8Om;->A:LX/8Oj;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/8Om;->l:LX/8OL;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/8Om;->j:LX/0cW;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/8Om;->w:LX/8On;

    move-object v3, v14

    move-object/from16 v4, p2

    move-object/from16 v6, p1

    move-object/from16 v7, p4

    move-object/from16 v8, p3

    invoke-virtual/range {v2 .. v13}, LX/8Og;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ob;Ljava/lang/String;LX/73w;LX/8Ne;LX/8OV;LX/74b;LX/8Oj;LX/8OL;LX/0cW;LX/8On;)V

    goto :goto_1

    .line 1340857
    :cond_3
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/73w;->a(Z)V

    .line 1340858
    new-instance v12, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v12}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 1340859
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Om;->u:LX/74B;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object/from16 v0, p2

    iget-wide v6, v0, LX/8Ob;->l:J

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v2 .. v8}, LX/74B;->a(ZZZJLjava/lang/String;)V

    .line 1340860
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Om;->t:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v16

    .line 1340861
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Om;->v:Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/8Om;->j:LX/0cW;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/8Om;->l:LX/8OL;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/8Om;->z:Ljava/util/concurrent/Semaphore;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/8Om;->w:LX/8On;

    move-object v3, v14

    move-object/from16 v4, p2

    invoke-virtual/range {v2 .. v8}, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ob;LX/0cW;LX/8OL;Ljava/util/concurrent/Semaphore;LX/8On;)V

    .line 1340862
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Om;->t:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v10

    .line 1340863
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Om;->A:LX/8Oj;

    const-wide/16 v4, 0x0

    move-object/from16 v0, p2

    iget-wide v6, v0, LX/8Ob;->l:J

    move-wide/from16 v8, v16

    invoke-virtual/range {v3 .. v11}, LX/8Oj;->a(JJJJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1340864
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Om;->u:LX/74B;

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {v12}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Om;->t:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v8

    sub-long v8, v8, v16

    move-object/from16 v0, p2

    iget-wide v10, v0, LX/8Ob;->l:J

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {v3 .. v12}, LX/74B;->a(ZZZIJJLjava/lang/String;)V

    goto/16 :goto_1

    .line 1340865
    :catch_0
    move-exception v2

    .line 1340866
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Om;->u:LX/74B;

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {v12}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, LX/8Om;->t:LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v8

    sub-long v8, v8, v16

    move-object/from16 v0, p2

    iget-wide v10, v0, LX/8Ob;->l:J

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {v3 .. v13}, LX/74B;->a(ZZZIJJLjava/lang/String;Ljava/lang/String;)V

    .line 1340867
    throw v2
.end method

.method private static a(LX/8Om;LX/74n;LX/8Of;LX/8KY;)V
    .locals 0

    .prologue
    .line 1340868
    iput-object p1, p0, LX/8Om;->a:LX/74n;

    iput-object p2, p0, LX/8Om;->b:LX/8Of;

    iput-object p3, p0, LX/8Om;->c:LX/8KY;

    return-void
.end method

.method public static a(LX/8Om;LX/8Ob;LX/73w;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1340869
    :try_start_0
    iget-boolean v1, p1, LX/8Ob;->I:Z

    if-nez v1, :cond_2

    iget-wide v2, p1, LX/8Ob;->r:J

    const-wide/16 v4, 0x4e20

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    .line 1340870
    iget-object v1, p0, LX/8Om;->D:LX/0ad;

    sget-short v2, LX/8Jz;->M:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v2, v1

    .line 1340871
    :goto_0
    const/4 v1, 0x1

    .line 1340872
    const-wide/16 v4, 0x0

    iput-wide v4, p1, LX/8Ob;->T:J

    .line 1340873
    :goto_1
    if-eqz v1, :cond_0

    .line 1340874
    invoke-static {p0, p1, v2}, LX/8Om;->a(LX/8Om;LX/8Ob;Z)V

    .line 1340875
    invoke-static {p0, p1, p2, p3, v2}, LX/8Om;->a(LX/8Om;LX/8Ob;LX/73w;Lcom/facebook/photos/upload/operation/UploadOperation;Z)Z

    move-result v0

    .line 1340876
    invoke-static {p0, p1, v0}, LX/8Om;->b(LX/8Om;LX/8Ob;Z)V

    .line 1340877
    invoke-static {v0, p1}, LX/8Oh;->a(ZLX/8Ob;)Z

    move-result v1

    .line 1340878
    iget-wide v4, p1, LX/8Ob;->T:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, p1, LX/8Ob;->T:J
    :try_end_0
    .catch LX/74H; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1340879
    :catch_0
    move-exception v0

    .line 1340880
    new-instance v1, LX/8OT;

    invoke-static {v0}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v0

    invoke-direct {v1, v0}, LX/8OT;-><init>(LX/73z;)V

    throw v1

    .line 1340881
    :cond_0
    :try_start_1
    iget-boolean v1, p1, LX/8Ob;->I:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 1340882
    new-instance v0, LX/8Ol;

    const-string v1, "Transcoding failed when editing is specified"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/8Ol;-><init>(Ljava/lang/String;Z)V

    .line 1340883
    new-instance v1, LX/74H;

    invoke-direct {v1, v0}, LX/74H;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catch LX/74H; {:try_start_1 .. :try_end_1} :catch_0

    .line 1340884
    :cond_1
    return-void

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method private static a(LX/8Om;LX/8Ob;Lcom/facebook/photos/upload/operation/UploadRecords;Lcom/facebook/photos/upload/operation/UploadRecord;)V
    .locals 6

    .prologue
    .line 1340885
    iget-wide v0, p3, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, LX/8Ob;->b:Ljava/lang/Long;

    .line 1340886
    iget-boolean v0, p3, Lcom/facebook/photos/upload/operation/UploadRecord;->sameHashExist:Z

    iput-boolean v0, p1, LX/8Ob;->i:Z

    .line 1340887
    iget-object v0, p3, Lcom/facebook/photos/upload/operation/UploadRecord;->videoId:Ljava/lang/String;

    iput-object v0, p1, LX/8Ob;->e:Ljava/lang/String;

    .line 1340888
    iget-object v0, p1, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, LX/8Ob;->d:Ljava/lang/String;

    .line 1340889
    iget-object v0, p1, LX/8Ob;->d:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/facebook/photos/upload/operation/UploadRecords;->a(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadRecord;

    move-result-object v0

    .line 1340890
    if-eqz v0, :cond_0

    .line 1340891
    iget-wide v2, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    iput-wide v2, p1, LX/8Ob;->u:J

    .line 1340892
    iget-wide v2, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->uploadTime:J

    iget-wide v4, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    sub-long/2addr v2, v4

    iput-wide v2, p1, LX/8Ob;->v:J

    .line 1340893
    iget-object v0, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->partitionInfo:Ljava/util/List;

    iput-object v0, p1, LX/8Ob;->B:Ljava/util/List;

    .line 1340894
    :goto_0
    sget-object v0, LX/8Oi;->RECEIVE:LX/8Oi;

    iput-object v0, p1, LX/8Ob;->x:LX/8Oi;

    .line 1340895
    iget-object v0, p0, LX/8Om;->l:LX/8OL;

    const-string v1, "after read partial data"

    invoke-virtual {v0, v1}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1340896
    return-void

    .line 1340897
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p1, LX/8Ob;->u:J

    .line 1340898
    const-wide/16 v0, 0x1000

    iput-wide v0, p1, LX/8Ob;->v:J

    goto :goto_0
.end method

.method private static a(LX/8Om;LX/8Ob;Z)V
    .locals 6

    .prologue
    .line 1340899
    iget-object v0, p1, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v0, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    .line 1340900
    iget-wide v2, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeStartCount:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeStartCount:J

    .line 1340901
    iput-boolean p2, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isSegmentedTranscode:Z

    .line 1340902
    invoke-static {p0, p1}, LX/8Om;->e(LX/8Om;LX/8Ob;)V

    .line 1340903
    return-void
.end method

.method private static a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ob;)V
    .locals 7

    .prologue
    .line 1340904
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v2, v0

    .line 1340905
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1340906
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->p()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1340907
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    .line 1340908
    if-eqz v0, :cond_0

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1340909
    :cond_0
    iget-object v1, p1, LX/8Ob;->y:LX/73w;

    iget-object v2, p1, LX/8Ob;->A:LX/74b;

    iget-object v3, p1, LX/8Ob;->q:Ljava/lang/String;

    iget-object v4, p1, LX/8Ob;->j:Ljava/lang/String;

    iget-boolean v5, p1, LX/8Ob;->o:Z

    .line 1340910
    invoke-virtual {v2}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v6

    .line 1340911
    invoke-static {v1, v6}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1340912
    const-string p0, "video_original_file_path"

    invoke-virtual {v6, p0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340913
    const-string p0, "video_uploaded_file_path"

    invoke-virtual {v6, p0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340914
    const-string p0, "transcode_success"

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340915
    sget-object p0, LX/74R;->MEDIA_UPLOAD_MISSING_ORIGINAL_MEDIA_FILE:LX/74R;

    const/4 p1, 0x0

    invoke-static {v1, p0, v6, p1}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1340916
    new-instance v1, LX/8ON;

    invoke-direct {v1, v0}, LX/8ON;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1340917
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1340918
    :cond_2
    return-void
.end method

.method private a(LX/8Ob;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8OV;LX/8Ne;LX/73w;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    .line 1340919
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->G:Lcom/facebook/photos/upload/operation/UploadRecords;

    move-object v2, v0

    .line 1340920
    iput-wide v6, p1, LX/8Ob;->u:J

    .line 1340921
    iput-wide v6, p1, LX/8Ob;->v:J

    .line 1340922
    iget-object v0, p0, LX/8Om;->l:LX/8OL;

    const-string v3, "before uploading video"

    invoke-virtual {v0, v3}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1340923
    iget-object v5, p1, LX/8Ob;->j:Ljava/lang/String;

    .line 1340924
    if-eqz v2, :cond_5

    .line 1340925
    invoke-virtual {v2, v5}, Lcom/facebook/photos/upload/operation/UploadRecords;->a(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadRecord;

    move-result-object v0

    .line 1340926
    :goto_0
    invoke-static {p0, p1, v0}, LX/8Om;->a(LX/8Om;LX/8Ob;Lcom/facebook/photos/upload/operation/UploadRecord;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1340927
    invoke-static {p0, p1, v2, v0}, LX/8Om;->a(LX/8Om;LX/8Ob;Lcom/facebook/photos/upload/operation/UploadRecords;Lcom/facebook/photos/upload/operation/UploadRecord;)V

    .line 1340928
    :goto_1
    sget-object v0, LX/8Oi;->RECEIVE:LX/8Oi;

    iput-object v0, p1, LX/8Ob;->x:LX/8Oi;

    move-object v0, p0

    move-object v1, p5

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    .line 1340929
    invoke-static/range {v0 .. v5}, LX/8Om;->a(LX/8Om;LX/73w;LX/8Ob;LX/8OV;LX/8Ne;Ljava/lang/String;)V

    .line 1340930
    iget-object v0, p0, LX/8Om;->D:LX/0ad;

    sget-short v1, LX/8Jz;->h:S

    sget-object v2, LX/8K0;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 1340931
    if-eqz v0, :cond_3

    .line 1340932
    iget-object v0, p1, LX/8Ob;->p:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x31fe9182    # -5.4287552E8f

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1340933
    if-nez v0, :cond_4

    .line 1340934
    new-instance v0, Ljava/util/concurrent/CancellationException;

    const-string v1, "cannot be posted"

    invoke-direct {v0, v1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1340935
    :cond_0
    iget-object v0, p1, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v2, v6

    if-gez v0, :cond_2

    .line 1340936
    sget-object v0, LX/8Oi;->START:LX/8Oi;

    iput-object v0, p1, LX/8Ob;->x:LX/8Oi;

    .line 1340937
    if-eqz p3, :cond_1

    .line 1340938
    invoke-virtual {p3}, LX/8OV;->a()LX/8Oa;

    move-result-object v1

    .line 1340939
    :cond_1
    invoke-direct {p0, p5, p1, v1, p4}, LX/8Om;->a(LX/73w;LX/8Ob;LX/8Oa;LX/8Ne;)V

    goto :goto_1

    .line 1340940
    :cond_2
    iget-object v0, p0, LX/8Om;->l:LX/8OL;

    const-string v1, "after retry init"

    invoke-virtual {v0, v1}, LX/8OL;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 1340941
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/8Ob;->a(Z)V

    .line 1340942
    :cond_4
    sget-object v0, LX/8Oi;->POST:LX/8Oi;

    iput-object v0, p1, LX/8Ob;->x:LX/8Oi;

    .line 1340943
    iget-object v0, p0, LX/8Om;->l:LX/8OL;

    const-string v1, "before post video"

    invoke-virtual {v0, v1}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1340944
    invoke-direct {p0, p5, p1, p4}, LX/8Om;->a(LX/73w;LX/8Ob;LX/8Ne;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1340945
    sget-object v1, LX/8Oi;->FINISHED:LX/8Oi;

    iput-object v1, p1, LX/8Ob;->x:LX/8Oi;

    .line 1340946
    invoke-interface {p4}, LX/8Ne;->a()V

    .line 1340947
    return v0

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method private static a(LX/8Om;LX/8Ob;LX/73w;Lcom/facebook/photos/upload/operation/UploadOperation;Z)Z
    .locals 7

    .prologue
    .line 1340755
    const/4 v6, 0x1

    .line 1340756
    if-eqz p4, :cond_1

    .line 1340757
    iget-object v0, p0, LX/8Om;->s:LX/8Oh;

    iget-object v4, p0, LX/8Om;->r:LX/8Ok;

    iget-object v5, p0, LX/8Om;->l:LX/8OL;

    move-object v1, p1

    move-object v2, p3

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LX/8Oh;->a(LX/8Ob;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/8Ok;LX/8OL;)LX/8OV;

    move-result-object v0

    iput-object v0, p1, LX/8Ob;->z:LX/8OV;

    .line 1340758
    iget-object v0, p1, LX/8Ob;->z:LX/8OV;

    if-nez v0, :cond_2

    .line 1340759
    :cond_0
    const/4 v0, 0x0

    .line 1340760
    :goto_0
    return v0

    .line 1340761
    :cond_1
    iget-object v0, p0, LX/8Om;->s:LX/8Oh;

    iget-object v4, p0, LX/8Om;->r:LX/8Ok;

    iget-object v5, p0, LX/8Om;->l:LX/8OL;

    move-object v1, p1

    move-object v2, p3

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LX/8Oh;->b(LX/8Ob;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/8Ok;LX/8OL;)LX/8OX;

    move-result-object v0

    .line 1340762
    if-eqz v0, :cond_0

    .line 1340763
    iget-object v0, v0, LX/8OX;->a:Ljava/lang/String;

    iput-object v0, p1, LX/8Ob;->j:Ljava/lang/String;

    move v0, v6

    goto :goto_0

    :cond_2
    move v0, v6

    goto :goto_0
.end method

.method private static a(LX/8Om;LX/8Ob;Lcom/facebook/photos/upload/operation/UploadRecord;)Z
    .locals 4

    .prologue
    .line 1340418
    if-eqz p2, :cond_1

    .line 1340419
    iget-wide v0, p2, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    invoke-static {p0, p2}, LX/8Om;->a(LX/8Om;Lcom/facebook/photos/upload/operation/UploadRecord;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-wide v2, p2, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1340420
    :cond_0
    const/4 v0, 0x1

    .line 1340421
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/8Om;Lcom/facebook/photos/upload/operation/UploadRecord;)Z
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1340422
    iget-object v0, p0, LX/8Om;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p1, Lcom/facebook/photos/upload/operation/UploadRecord;->uploadTime:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x44aa200

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/8Om;
    .locals 30

    .prologue
    .line 1340423
    new-instance v2, LX/8Om;

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v3

    check-cast v3, LX/11H;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v5

    check-cast v5, LX/0b3;

    invoke-static/range {p0 .. p0}, LX/8LX;->a(LX/0QB;)LX/8LX;

    move-result-object v6

    check-cast v6, LX/8LX;

    invoke-static/range {p0 .. p0}, LX/0cW;->a(LX/0QB;)LX/0cW;

    move-result-object v7

    check-cast v7, LX/0cW;

    const/16 v8, 0x2f01

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/8OL;->a(LX/0QB;)LX/8OL;

    move-result-object v9

    check-cast v9, LX/8OL;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static/range {p0 .. p0}, LX/8NU;->a(LX/0QB;)LX/8NU;

    move-result-object v11

    check-cast v11, LX/8NU;

    invoke-static/range {p0 .. p0}, LX/8NX;->a(LX/0QB;)LX/8NX;

    move-result-object v12

    check-cast v12, LX/8NX;

    invoke-static/range {p0 .. p0}, LX/8NP;->a(LX/0QB;)LX/8NP;

    move-result-object v13

    check-cast v13, LX/8NP;

    invoke-static/range {p0 .. p0}, LX/8NN;->a(LX/0QB;)LX/8NN;

    move-result-object v14

    check-cast v14, LX/8NN;

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v15

    check-cast v15, LX/0So;

    invoke-static/range {p0 .. p0}, LX/8Oh;->a(LX/0QB;)LX/8Oh;

    move-result-object v16

    check-cast v16, LX/8Oh;

    invoke-static/range {p0 .. p0}, LX/8Og;->a(LX/0QB;)LX/8Og;

    move-result-object v17

    check-cast v17, LX/8Og;

    invoke-static/range {p0 .. p0}, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->a(LX/0QB;)Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;

    move-result-object v18

    check-cast v18, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;

    const/16 v19, 0x1550

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v19

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v20

    check-cast v20, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/74B;->a(LX/0QB;)LX/74B;

    move-result-object v21

    check-cast v21, LX/74B;

    const/16 v22, 0x1551

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v22

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v23

    check-cast v23, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/6bD;->a(LX/0QB;)LX/6bD;

    move-result-object v24

    check-cast v24, LX/6bD;

    invoke-static/range {p0 .. p0}, LX/8BN;->a(LX/0QB;)LX/8BN;

    move-result-object v25

    check-cast v25, LX/8BN;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v26

    check-cast v26, LX/0lB;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v27

    check-cast v27, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/8K9;->a(LX/0QB;)LX/8K9;

    move-result-object v28

    check-cast v28, LX/8K9;

    invoke-static/range {p0 .. p0}, LX/0cX;->a(LX/0QB;)LX/0cX;

    move-result-object v29

    check-cast v29, LX/0cX;

    invoke-direct/range {v2 .. v29}, LX/8Om;-><init>(LX/11H;LX/0SG;LX/0b3;LX/8LX;LX/0cW;LX/0Or;LX/8OL;LX/03V;LX/8NU;LX/8NX;LX/8NP;LX/8NN;LX/0So;LX/8Oh;LX/8Og;Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;LX/0Or;LX/0Sh;LX/74B;LX/0Or;LX/0ad;LX/6bD;LX/8BN;LX/0lB;LX/0Uh;LX/8K9;LX/0cX;)V

    .line 1340424
    invoke-static/range {p0 .. p0}, LX/74n;->a(LX/0QB;)LX/74n;

    move-result-object v3

    check-cast v3, LX/74n;

    invoke-static/range {p0 .. p0}, LX/8Of;->a(LX/0QB;)LX/8Of;

    move-result-object v4

    check-cast v4, LX/8Of;

    invoke-static/range {p0 .. p0}, LX/8KY;->a(LX/0QB;)LX/8KY;

    move-result-object v5

    check-cast v5, LX/8KY;

    invoke-static {v2, v3, v4, v5}, LX/8Om;->a(LX/8Om;LX/74n;LX/8Of;LX/8KY;)V

    .line 1340425
    return-object v2
.end method

.method private b(LX/8Ob;)V
    .locals 14

    .prologue
    .line 1340426
    iget-boolean v0, p1, LX/8Ob;->F:Z

    if-eqz v0, :cond_0

    .line 1340427
    iget-object v0, p1, LX/8Ob;->y:LX/73w;

    iget-object v1, p1, LX/8Ob;->A:LX/74b;

    iget-object v2, p1, LX/8Ob;->E:LX/60x;

    iget-object v2, v2, LX/60x;->h:LX/60v;

    iget-object v2, v2, LX/60v;->b:Ljava/lang/String;

    iget-object v3, p1, LX/8Ob;->E:LX/60x;

    iget-object v3, v3, LX/60x;->h:LX/60v;

    iget-object v3, v3, LX/60v;->c:Ljava/lang/String;

    .line 1340428
    invoke-virtual {v1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v4

    .line 1340429
    const-string v5, "spherical_projection_type"

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340430
    const-string v5, "spherical_stereo_mode"

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340431
    sget-object v5, LX/74R;->MEDIA_UPLOAD_ATTEMPT_PRESERVE_SPHERICAL_METADATA:LX/74R;

    const/4 p0, 0x0

    invoke-static {v0, v5, v4, p0}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1340432
    :goto_0
    iget-object v0, p1, LX/8Ob;->z:LX/8OV;

    if-eqz v0, :cond_3

    .line 1340433
    iget-object v0, p1, LX/8Ob;->z:LX/8OV;

    .line 1340434
    iget-object v5, v0, LX/8OV;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    .line 1340435
    iget-object v6, v0, LX/8OV;->b:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/8Oa;

    iget-wide v5, v5, LX/8Oa;->f:J

    .line 1340436
    move-wide v0, v5

    .line 1340437
    iput-wide v0, p1, LX/8Ob;->l:J

    .line 1340438
    iget-object v0, p1, LX/8Ob;->z:LX/8OV;

    .line 1340439
    invoke-virtual {v0}, LX/8OV;->a()LX/8Oa;

    move-result-object v1

    iget-object v1, v1, LX/8Oa;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1340440
    iput-object v0, p1, LX/8Ob;->j:Ljava/lang/String;

    .line 1340441
    :goto_1
    return-void

    .line 1340442
    :cond_0
    invoke-static {p0, p1}, LX/8Om;->g(LX/8Om;LX/8Ob;)V

    .line 1340443
    iget-object v0, p1, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1340444
    iget-boolean v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->as:Z

    move v0, v1

    .line 1340445
    iget-object v1, p1, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1340446
    iget v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->S:I

    move v1, v2

    .line 1340447
    iget-object v2, p1, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1340448
    iget v3, v2, Lcom/facebook/photos/upload/operation/UploadOperation;->U:I

    move v2, v3

    .line 1340449
    iget-object v3, p1, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v3, v3, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    iget-boolean v4, p1, LX/8Ob;->I:Z

    const/4 v11, -0x1

    .line 1340450
    new-instance v5, LX/8OY;

    invoke-direct {v5}, LX/8OY;-><init>()V

    .line 1340451
    if-nez v4, :cond_4

    invoke-static {v0, v1, v3}, LX/8OY;->a(ZILcom/facebook/photos/upload/operation/TranscodeInfo;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1340452
    const/4 v6, 0x1

    iput-boolean v6, v5, LX/8OY;->c:Z

    .line 1340453
    :cond_1
    :goto_2
    move-object v0, v5

    .line 1340454
    iput-object v0, p1, LX/8Ob;->V:LX/8OY;

    .line 1340455
    iget-object v0, p1, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    iget-object v1, p1, LX/8Ob;->V:LX/8OY;

    .line 1340456
    iget-boolean v2, v1, LX/8OY;->c:Z

    move v2, v2

    .line 1340457
    if-eqz v2, :cond_9

    .line 1340458
    const/4 v2, -0x2

    .line 1340459
    :goto_3
    move v1, v2

    .line 1340460
    iput v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->T:I

    .line 1340461
    iget-object v0, p0, LX/8Om;->D:LX/0ad;

    sget-short v1, LX/8Jz;->Y:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 1340462
    if-eqz v0, :cond_2

    .line 1340463
    invoke-static {p0, p1}, LX/8Om;->c(LX/8Om;LX/8Ob;)V

    goto :goto_0

    .line 1340464
    :cond_2
    iget-object v5, p1, LX/8Ob;->y:LX/73w;

    .line 1340465
    iget-object v6, p1, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1340466
    const-string v7, "Logger cannot be null"

    invoke-static {v5, v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340467
    const-string v7, "Upload Operation cannot be null"

    invoke-static {v6, v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340468
    const/4 v7, 0x0

    iput-object v7, p1, LX/8Ob;->z:LX/8OV;

    .line 1340469
    iget-object v7, p0, LX/8Om;->s:LX/8Oh;

    invoke-virtual {v7, p1, v5, v6}, LX/8Oh;->a(LX/8Ob;LX/73w;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v7

    .line 1340470
    if-eqz v7, :cond_b

    .line 1340471
    invoke-static {p0, p1, v5, v6}, LX/8Om;->a(LX/8Om;LX/8Ob;LX/73w;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1340472
    :goto_4
    iget-boolean v5, v6, Lcom/facebook/photos/upload/operation/UploadOperation;->R:Z

    move v5, v5

    .line 1340473
    iput-boolean v5, p1, LX/8Ob;->o:Z

    .line 1340474
    goto/16 :goto_0

    .line 1340475
    :cond_3
    new-instance v0, Ljava/io/File;

    iget-object v1, p1, LX/8Ob;->j:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p1, LX/8Ob;->l:J

    goto :goto_1

    .line 1340476
    :cond_4
    if-nez v0, :cond_5

    iget-boolean v6, v3, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isServerSettingsAvailable:Z

    if-eqz v6, :cond_5

    .line 1340477
    iget-wide v7, v3, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeDimension:J

    long-to-int v6, v7

    iput v6, v5, LX/8OY;->b:I

    .line 1340478
    if-eqz v4, :cond_8

    iget-wide v7, v3, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeBitrate:J

    sget v6, Lcom/facebook/photos/upload/operation/TranscodeInfo;->a:I

    int-to-long v9, v6

    cmp-long v6, v7, v9

    if-nez v6, :cond_8

    .line 1340479
    invoke-static {v5}, LX/8OY;->e(LX/8OY;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1340480
    sget-object v6, LX/2Mg;->h:LX/2Mg;

    iget v6, v6, LX/2Mg;->b:I

    iput v6, v5, LX/8OY;->a:I

    .line 1340481
    :cond_5
    :goto_5
    if-eq v1, v11, :cond_6

    .line 1340482
    mul-int/lit16 v6, v1, 0x3e8

    .line 1340483
    iget v7, v5, LX/8OY;->a:I

    if-le v6, v7, :cond_6

    .line 1340484
    iput v6, v5, LX/8OY;->a:I

    .line 1340485
    :cond_6
    if-eq v2, v11, :cond_1

    .line 1340486
    iget v6, v5, LX/8OY;->b:I

    if-le v2, v6, :cond_1

    .line 1340487
    iput v2, v5, LX/8OY;->b:I

    goto/16 :goto_2

    .line 1340488
    :cond_7
    sget-object v6, LX/2Mg;->h:LX/2Mg;

    iget v6, v6, LX/2Mg;->b:I

    mul-int/lit8 v6, v6, 0x4

    iput v6, v5, LX/8OY;->a:I

    goto :goto_5

    .line 1340489
    :cond_8
    iget-wide v7, v3, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeBitrate:J

    long-to-int v6, v7

    .line 1340490
    iput v6, v5, LX/8OY;->a:I

    goto :goto_5

    :cond_9
    invoke-virtual {v1}, LX/8OY;->c()Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v2, -0x1

    goto/16 :goto_3

    :cond_a
    iget v2, v1, LX/8OY;->a:I

    goto/16 :goto_3

    .line 1340491
    :cond_b
    iget-object v7, p1, LX/8Ob;->A:LX/74b;

    iget-wide v9, p1, LX/8Ob;->m:J

    iget-object v8, p1, LX/8Ob;->V:LX/8OY;

    iget v8, v8, LX/8OY;->a:I

    div-int/lit16 v8, v8, 0x3e8

    .line 1340492
    invoke-virtual {v7}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v11

    .line 1340493
    const-string v12, "original_file_size"

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340494
    const-string v12, "specified_transcode_bit_rate"

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340495
    sget-object v12, LX/74R;->MEDIA_UPLOAD_PROCESS_SKIPPED:LX/74R;

    const/4 v13, 0x0

    invoke-static {v5, v12, v11, v13}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1340496
    goto/16 :goto_4
.end method

.method private static b(LX/8Om;LX/8Ob;Z)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 1340497
    iget-object v0, p1, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v0, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    .line 1340498
    if-eqz p2, :cond_0

    .line 1340499
    iget-wide v2, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeSuccessCount:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeSuccessCount:J

    .line 1340500
    :goto_0
    invoke-static {p0, p1}, LX/8Om;->e(LX/8Om;LX/8Ob;)V

    .line 1340501
    return-void

    .line 1340502
    :cond_0
    iget-wide v2, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeFailCount:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeFailCount:J

    goto :goto_0
.end method

.method private static c(LX/8Om;LX/8Ob;)V
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 1340503
    iput-object v0, p1, LX/8Ob;->z:LX/8OV;

    .line 1340504
    iget-object v0, p1, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1340505
    const-string v1, "Upload Operation cannot be null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340506
    const/4 v4, 0x1

    .line 1340507
    iget-object v1, p1, LX/8Ob;->V:LX/8OY;

    .line 1340508
    iget-boolean v2, v1, LX/8OY;->c:Z

    move v1, v2

    .line 1340509
    if-eqz v1, :cond_3

    .line 1340510
    const/4 v1, 0x0

    .line 1340511
    :goto_0
    move-object v1, v1

    .line 1340512
    if-nez v1, :cond_0

    .line 1340513
    :goto_1
    return-void

    .line 1340514
    :cond_0
    invoke-static {v0}, LX/8Oh;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v2

    .line 1340515
    :try_start_0
    iget-object v3, p0, LX/8Om;->E:LX/6bD;

    .line 1340516
    iget-object v4, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v4

    .line 1340517
    const-string v4, "videouploader"

    .line 1340518
    iget-object v5, v3, LX/6bD;->d:LX/6b8;

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v6

    invoke-virtual {v5, v6, v0, v4}, LX/6b8;->a(LX/4gF;Ljava/lang/String;Ljava/lang/String;)LX/6b7;

    move-result-object v5

    .line 1340519
    iget-object v13, v3, LX/6bD;->a:LX/0TD;

    new-instance v7, LX/6bB;

    move-object v8, v3

    move-object v9, v0

    move-object v10, v2

    move-object v11, v1

    move-object v12, v5

    invoke-direct/range {v7 .. v12}, LX/6bB;-><init>(LX/6bD;Ljava/lang/String;Lcom/facebook/ipc/media/MediaItem;Lcom/facebook/media/transcode/MediaTranscodeParameters;LX/6b7;)V

    invoke-interface {v13, v7}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 1340520
    new-instance v8, LX/6bC;

    invoke-direct {v8, v3, v0}, LX/6bC;-><init>(LX/6bD;Ljava/lang/String;)V

    iget-object v9, v3, LX/6bD;->a:LX/0TD;

    invoke-static {v7, v8, v9}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1340521
    move-object v5, v7

    .line 1340522
    move-object v0, v5

    .line 1340523
    iput-object v0, p0, LX/8Om;->G:Ljava/util/concurrent/Future;

    .line 1340524
    iget-object v0, p0, LX/8Om;->G:Ljava/util/concurrent/Future;

    const v1, 0x4085c836

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6bA;

    .line 1340525
    iget-boolean v1, v0, LX/6bA;->a:Z

    move v1, v1

    .line 1340526
    iput-boolean v1, p1, LX/8Ob;->o:Z

    .line 1340527
    iget-boolean v1, v0, LX/6bA;->a:Z

    move v1, v1

    .line 1340528
    if-eqz v1, :cond_1

    .line 1340529
    iget-object v1, v0, LX/6bA;->b:Lcom/facebook/ipc/media/MediaItem;

    move-object v0, v1

    .line 1340530
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, LX/8Ob;->j:Ljava/lang/String;

    .line 1340531
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/8Om;->G:Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1340532
    :catch_0
    move-exception v0

    .line 1340533
    iget-object v1, p0, LX/8Om;->l:LX/8OL;

    const-string v2, "Transcoding in the media library"

    invoke-virtual {v1, v2}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1340534
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 1340535
    if-eqz v1, :cond_2

    instance-of v2, v1, LX/7T9;

    if-eqz v2, :cond_2

    .line 1340536
    new-instance v0, LX/74H;

    invoke-direct {v0, v1}, LX/74H;-><init>(Ljava/lang/Throwable;)V

    .line 1340537
    new-instance v1, LX/8OT;

    invoke-static {v0}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v0

    invoke-direct {v1, v0}, LX/8OT;-><init>(LX/73z;)V

    throw v1

    .line 1340538
    :cond_2
    throw v0

    .line 1340539
    :cond_3
    new-instance v1, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;

    invoke-direct {v1}, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;-><init>()V

    .line 1340540
    iget-boolean v2, p1, LX/8Ob;->I:Z

    if-eqz v2, :cond_4

    .line 1340541
    new-instance v2, LX/6bM;

    invoke-direct {v2}, LX/6bM;-><init>()V

    iget-boolean v3, p1, LX/8Ob;->L:Z

    .line 1340542
    iput-boolean v3, v2, LX/6bM;->a:Z

    .line 1340543
    move-object v2, v2

    .line 1340544
    iget v3, p1, LX/8Ob;->M:I

    .line 1340545
    iput v3, v2, LX/6bM;->b:I

    .line 1340546
    move-object v2, v2

    .line 1340547
    iget v3, p1, LX/8Ob;->N:I

    .line 1340548
    iput v3, v2, LX/6bM;->c:I

    .line 1340549
    move-object v2, v2

    .line 1340550
    iget-boolean v3, p1, LX/8Ob;->K:Z

    .line 1340551
    iput-boolean v3, v2, LX/6bM;->e:Z

    .line 1340552
    move-object v2, v2

    .line 1340553
    iget v3, p1, LX/8Ob;->J:I

    .line 1340554
    iput v3, v2, LX/6bM;->d:I

    .line 1340555
    move-object v2, v2

    .line 1340556
    iget-object v3, p1, LX/8Ob;->O:Landroid/graphics/RectF;

    .line 1340557
    iput-object v3, v2, LX/6bM;->f:Landroid/graphics/RectF;

    .line 1340558
    move-object v2, v2

    .line 1340559
    invoke-virtual {v2}, LX/6bM;->g()Lcom/facebook/media/transcode/video/VideoEditConfig;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->c:Lcom/facebook/media/transcode/video/VideoEditConfig;

    .line 1340560
    :cond_4
    iget-object v2, p1, LX/8Ob;->V:LX/8OY;

    invoke-virtual {v2}, LX/8OY;->c()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1340561
    iput-boolean v4, v1, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->a:Z

    .line 1340562
    iget-object v2, p1, LX/8Ob;->V:LX/8OY;

    iget v2, v2, LX/8OY;->a:I

    div-int/lit16 v2, v2, 0x3e8

    iput v2, v1, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->b:I

    .line 1340563
    :cond_5
    iget-object v2, p0, LX/8Om;->r:LX/8Ok;

    invoke-virtual {v2, v0, v4}, LX/8Ok;->a(Lcom/facebook/photos/upload/operation/UploadOperation;I)V

    .line 1340564
    iget-object v2, p0, LX/8Om;->r:LX/8Ok;

    iput-object v2, v1, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->d:LX/60y;

    goto/16 :goto_0
.end method

.method public static d(LX/8Ob;)V
    .locals 23

    .prologue
    .line 1340565
    const/4 v2, 0x0

    .line 1340566
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1340567
    invoke-virtual/range {p0 .. p0}, LX/8Ob;->a()Ljava/lang/String;

    move-result-object v4

    .line 1340568
    invoke-virtual {v3}, Lcom/facebook/photos/upload/operation/UploadOperation;->v()Lcom/facebook/photos/upload/operation/UploadRecords;

    move-result-object v3

    .line 1340569
    if-eqz v3, :cond_0

    .line 1340570
    invoke-virtual {v3, v4}, Lcom/facebook/photos/upload/operation/UploadRecords;->a(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadRecord;

    move-result-object v2

    .line 1340571
    :cond_0
    if-eqz v2, :cond_1

    .line 1340572
    move-object/from16 v0, p0

    iput-object v2, v0, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    .line 1340573
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v0, v2, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    move-object/from16 v22, v0

    .line 1340574
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Ob;->y:LX/73w;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Ob;->A:LX/74b;

    move-object/from16 v0, v22

    iget-wide v4, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->flowStartCount:J

    move-object/from16 v0, v22

    iget-wide v6, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeStartCount:J

    move-object/from16 v0, v22

    iget-wide v8, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeSuccessCount:J

    move-object/from16 v0, v22

    iget-wide v10, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeFailCount:J

    move-object/from16 v0, v22

    iget-boolean v12, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isSegmentedTranscode:Z

    move-object/from16 v0, v22

    iget-boolean v13, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isRequestedServerSettings:Z

    move-object/from16 v0, v22

    iget-boolean v14, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isServerSettingsAvailable:Z

    move-object/from16 v0, v22

    iget-wide v15, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeBitrate:J

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeDimension:J

    move-wide/from16 v17, v0

    move-object/from16 v0, v22

    iget-boolean v0, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isUsingContextualConfig:Z

    move/from16 v19, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->skipRatioThreshold:F

    move/from16 v20, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->skipBytesThreshold:I

    move/from16 v21, v0

    move-object/from16 v0, v22

    iget-boolean v0, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->videoCodecResizeInitException:Z

    move/from16 v22, v0

    invoke-virtual/range {v2 .. v22}, LX/73w;->a(LX/74b;JJJJZZZJJZFIZ)V

    .line 1340575
    :goto_0
    return-void

    .line 1340576
    :cond_1
    new-instance v3, Lcom/facebook/photos/upload/operation/UploadRecord;

    const-wide/16 v4, -0x1

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/facebook/photos/upload/operation/UploadRecord;-><init>(JJZ)V

    move-object/from16 v0, p0

    iput-object v3, v0, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    goto :goto_0
.end method

.method public static e(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/8Ob;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1340577
    sget-object v0, LX/8Om;->e:Ljava/util/WeakHashMap;

    .line 1340578
    iget-object v1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 1340579
    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Ob;

    if-nez v0, :cond_1

    .line 1340580
    new-instance v0, LX/8Ob;

    invoke-direct {v0, p0}, LX/8Ob;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1340581
    :cond_0
    :goto_0
    return-object v0

    .line 1340582
    :cond_1
    iget-object v2, v0, LX/8Ob;->q:Ljava/lang/String;

    .line 1340583
    iget-object v1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v1, v1

    .line 1340584
    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1340585
    invoke-virtual {v0, v3}, LX/8Ob;->a(Z)V

    .line 1340586
    new-instance v0, LX/8Ob;

    invoke-direct {v0, p0}, LX/8Ob;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto :goto_0
.end method

.method public static e(LX/8Om;LX/8Ob;)V
    .locals 24

    .prologue
    .line 1340587
    move-object/from16 v0, p1

    iget-object v2, v0, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1340588
    invoke-virtual/range {p1 .. p1}, LX/8Ob;->a()Ljava/lang/String;

    move-result-object v2

    .line 1340589
    move-object/from16 v0, p1

    iget-object v3, v0, LX/8Ob;->g:Ljava/util/Map;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340590
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Om;->j:LX/0cW;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    invoke-virtual {v3, v2, v4}, LX/0cW;->a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadRecord;)Z

    move-result v23

    .line 1340591
    move-object/from16 v0, p1

    iget-object v2, v0, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v0, v2, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    move-object/from16 v22, v0

    .line 1340592
    move-object/from16 v0, p1

    iget-object v2, v0, LX/8Ob;->y:LX/73w;

    move-object/from16 v0, p1

    iget-object v3, v0, LX/8Ob;->A:LX/74b;

    move-object/from16 v0, v22

    iget-wide v4, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->flowStartCount:J

    move-object/from16 v0, v22

    iget-wide v6, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeStartCount:J

    move-object/from16 v0, v22

    iget-wide v8, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeSuccessCount:J

    move-object/from16 v0, v22

    iget-wide v10, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeFailCount:J

    move-object/from16 v0, v22

    iget-boolean v12, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isSegmentedTranscode:Z

    move-object/from16 v0, v22

    iget-boolean v13, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isRequestedServerSettings:Z

    move-object/from16 v0, v22

    iget-boolean v14, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isServerSettingsAvailable:Z

    move-object/from16 v0, v22

    iget-wide v15, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeBitrate:J

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeDimension:J

    move-wide/from16 v17, v0

    move-object/from16 v0, v22

    iget-boolean v0, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isUsingContextualConfig:Z

    move/from16 v19, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->skipRatioThreshold:F

    move/from16 v20, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->skipBytesThreshold:I

    move/from16 v21, v0

    move-object/from16 v0, v22

    iget-boolean v0, v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->videoCodecResizeInitException:Z

    move/from16 v22, v0

    invoke-virtual/range {v2 .. v22}, LX/73w;->b(LX/74b;JJJJZZZJJZFIZ)V

    .line 1340593
    if-nez v23, :cond_0

    .line 1340594
    move-object/from16 v0, p1

    iget-object v2, v0, LX/8Ob;->y:LX/73w;

    move-object/from16 v0, p1

    iget-object v3, v0, LX/8Ob;->A:LX/74b;

    invoke-virtual {v2, v3}, LX/73w;->d(LX/74b;)V

    .line 1340595
    :cond_0
    return-void

    .line 1340596
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private f(LX/8Ob;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 1340597
    iget-object v2, p1, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1340598
    iget-object v9, p1, LX/8Ob;->z:LX/8OV;

    .line 1340599
    iget-object v3, p1, LX/8Ob;->y:LX/73w;

    .line 1340600
    const-string v1, "Logger cannot be null"

    invoke-static {v3, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340601
    const-string v1, "Upload Operation cannot be null"

    invoke-static {v2, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340602
    :try_start_0
    iget-object v1, p0, LX/8Om;->D:LX/0ad;

    sget-short v4, LX/8Jz;->aa:S

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v8

    .line 1340603
    new-instance v1, LX/8Oo;

    iget-object v4, p1, LX/8Ob;->A:LX/74b;

    iget-object v5, p0, LX/8Om;->l:LX/8OL;

    iget-object v6, p0, LX/8Om;->g:LX/0b3;

    iget-object v7, p0, LX/8Om;->f:LX/0SG;

    invoke-direct/range {v1 .. v8}, LX/8Oo;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/74b;LX/8OL;LX/0b3;LX/0SG;Z)V

    iput-object v1, p1, LX/8Ob;->h:LX/8Oo;

    .line 1340604
    iget-object v1, p0, LX/8Om;->w:LX/8On;

    iget-object v4, p1, LX/8Ob;->h:LX/8Oo;

    .line 1340605
    iput-object v4, v1, LX/8On;->h:LX/8Oo;

    .line 1340606
    new-instance v1, LX/14U;

    invoke-direct {v1}, LX/14U;-><init>()V

    iput-object v1, p1, LX/8Ob;->c:LX/14U;

    .line 1340607
    iget-object v1, p1, LX/8Ob;->c:LX/14U;

    iget-object v4, p1, LX/8Ob;->h:LX/8Oo;

    .line 1340608
    iput-object v4, v1, LX/14U;->a:LX/4ck;

    .line 1340609
    iget-object v1, p1, LX/8Ob;->c:LX/14U;

    iget-object v4, p0, LX/8Om;->l:LX/8OL;

    .line 1340610
    iget-object v0, v4, LX/8OL;->f:LX/4d1;

    move-object v4, v0

    .line 1340611
    iput-object v4, v1, LX/14U;->c:LX/4d1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1340612
    :try_start_1
    invoke-static {v2, p1}, LX/8Om;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ob;)V

    .line 1340613
    iget-object v1, p0, LX/8Om;->C:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1340614
    iget-object v1, p0, LX/8Om;->w:LX/8On;

    .line 1340615
    iget-object v0, v1, LX/8On;->b:LX/8Ne;

    move-object v8, v0

    .line 1340616
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p1, LX/8Ob;->w:Ljava/lang/Exception;

    .line 1340617
    iget-object v1, p0, LX/8Om;->D:LX/0ad;

    sget-short v4, LX/8Jz;->Z:S

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 1340618
    if-eqz v1, :cond_5

    iget-object v1, p1, LX/8Ob;->z:LX/8OV;

    if-nez v1, :cond_5

    .line 1340619
    const/16 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1340620
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_6

    .line 1340621
    iget-object v1, p0, LX/8Om;->l:LX/8OL;

    const-string v2, "after uploading video"

    invoke-virtual {v1, v2}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1340622
    iget-object v1, p1, LX/8Ob;->w:Ljava/lang/Exception;

    if-nez v1, :cond_0

    .line 1340623
    iget-object v1, p0, LX/8Om;->m:LX/03V;

    sget-object v2, LX/8Om;->d:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "No fbid"

    invoke-virtual {v1, v2, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1340624
    new-instance v1, LX/740;

    const-string v2, "No fbid"

    const/4 v4, 0x1

    invoke-direct {v1, v2, v4}, LX/740;-><init>(Ljava/lang/String;Z)V

    iput-object v1, p1, LX/8Ob;->w:Ljava/lang/Exception;

    .line 1340625
    :cond_0
    iget-object v1, p1, LX/8Ob;->w:Ljava/lang/Exception;

    throw v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1340626
    :catch_0
    move-exception v1

    .line 1340627
    :try_start_2
    instance-of v2, v1, Ljava/util/concurrent/ExecutionException;

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    instance-of v2, v2, Ljava/lang/Exception;

    if-eqz v2, :cond_1

    .line 1340628
    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    check-cast v1, Ljava/lang/Exception;

    .line 1340629
    :cond_1
    iget-object v2, p0, LX/8Om;->l:LX/8OL;

    .line 1340630
    iget-boolean v0, v2, LX/8OL;->d:Z

    move v2, v0

    .line 1340631
    if-eqz v2, :cond_2

    .line 1340632
    iget-object v2, p1, LX/8Ob;->A:LX/74b;

    sget-object v4, LX/741;->LOCAL:LX/741;

    invoke-virtual {v3, v2, v4}, LX/73w;->a(LX/74b;LX/741;)V

    .line 1340633
    invoke-direct {p0, p1}, LX/8Om;->h(LX/8Ob;)V

    .line 1340634
    iget-object v2, p0, LX/8Om;->l:LX/8OL;

    const-string v4, "video"

    invoke-virtual {v2, v4}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1340635
    :cond_2
    const/4 v2, 0x1

    invoke-static {v1, v2}, LX/0cX;->a(Ljava/lang/Exception;Z)LX/73z;

    move-result-object v1

    .line 1340636
    iget-object v2, p1, LX/8Ob;->A:LX/74b;

    sget-object v4, LX/741;->LOCAL:LX/741;

    invoke-virtual {v3, v2, v4, v1}, LX/73w;->a(LX/74b;LX/741;LX/73y;)V

    .line 1340637
    iget-object v2, p1, LX/8Ob;->g:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 1340638
    new-instance v2, LX/8OT;

    iget-object v3, p1, LX/8Ob;->g:Ljava/util/Map;

    invoke-direct {v2, v1, v3}, LX/8OT;-><init>(LX/73z;Ljava/util/Map;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1340639
    :catchall_0
    move-exception v1

    iput-object v11, p0, LX/8Om;->z:Ljava/util/concurrent/Semaphore;

    .line 1340640
    iget-object v2, p1, LX/8Ob;->h:LX/8Oo;

    if-eqz v2, :cond_3

    .line 1340641
    iget-object v2, p1, LX/8Ob;->h:LX/8Oo;

    invoke-virtual {v2}, LX/8Oo;->b()V

    :cond_3
    throw v1

    .line 1340642
    :cond_4
    :try_start_3
    iget-object v1, p0, LX/8Om;->k:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Ne;

    .line 1340643
    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->i()Z

    move-result v4

    invoke-interface {v1, v4}, LX/8Ne;->a(Z)V

    .line 1340644
    iget-object v4, p0, LX/8Om;->z:Ljava/util/concurrent/Semaphore;

    invoke-interface {v1, v4}, LX/8Ne;->a(Ljava/util/concurrent/Semaphore;)V

    move-object v8, v1

    goto/16 :goto_0

    :cond_5
    move-object v4, p0

    move-object v5, p1

    move-object v6, v2

    move-object v7, v9

    move-object v9, v3

    .line 1340645
    invoke-direct/range {v4 .. v9}, LX/8Om;->a(LX/8Ob;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8OV;LX/8Ne;LX/73w;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_1

    .line 1340646
    :cond_6
    iget-object v1, p1, LX/8Ob;->e:Ljava/lang/String;

    iput-object v1, p1, LX/8Ob;->d:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1340647
    :try_start_4
    iget-object v1, p1, LX/8Ob;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 1340648
    iget-object v4, p1, LX/8Ob;->A:LX/74b;

    sget-object v5, LX/741;->LOCAL:LX/741;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->d()I

    move-result v10

    invoke-virtual/range {v3 .. v10}, LX/73w;->a(LX/74b;LX/741;Ljava/lang/String;Ljava/lang/String;JI)V

    .line 1340649
    iget-object v1, p1, LX/8Ob;->A:LX/74b;

    .line 1340650
    invoke-virtual {v1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1340651
    invoke-static {v3, v0, v2}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1340652
    const-string v4, "media_attachment_count"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340653
    sget-object v4, LX/74R;->MEDIA_UPLOAD_FLOW_SUCCESS:LX/74R;

    const/4 v5, 0x0

    invoke-static {v3, v4, v0, v5}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1340654
    iget-object v0, v3, LX/73w;->F:LX/74E;

    invoke-virtual {v0}, LX/74E;->f()V

    .line 1340655
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1340656
    iget-object v2, p1, LX/8Ob;->q:Ljava/lang/String;

    iget-object v3, p1, LX/8Ob;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1340657
    iget-object v2, p1, LX/8Ob;->d:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/util/Pair;

    const/4 v4, 0x0

    const-string v5, "fbids"

    invoke-static {v5, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;[Landroid/util/Pair;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v1

    .line 1340658
    iput-object v11, p0, LX/8Om;->z:Ljava/util/concurrent/Semaphore;

    .line 1340659
    iget-object v2, p1, LX/8Ob;->h:LX/8Oo;

    if-eqz v2, :cond_7

    .line 1340660
    iget-object v2, p1, LX/8Ob;->h:LX/8Oo;

    invoke-virtual {v2}, LX/8Oo;->b()V

    :cond_7
    return-object v1

    .line 1340661
    :cond_8
    new-instance v2, LX/8OT;

    invoke-direct {v2, v1}, LX/8OT;-><init>(LX/73z;)V

    throw v2
.end method

.method private static g(LX/8Om;LX/8Ob;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v5, 0x1

    .line 1340376
    iget-object v0, p1, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1340377
    iget-object v1, p1, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v9, v1, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    .line 1340378
    iget-object v1, p1, LX/8Ob;->E:LX/60x;

    if-nez v1, :cond_1

    .line 1340379
    :cond_0
    :goto_0
    return-void

    .line 1340380
    :cond_1
    iget-boolean v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->as:Z

    move v1, v1

    .line 1340381
    if-nez v1, :cond_0

    .line 1340382
    iget-boolean v1, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isRequestedServerSettings:Z

    if-nez v1, :cond_2

    .line 1340383
    iget-object v1, p0, LX/8Om;->J:LX/0Uh;

    const/16 v2, 0x2cb

    invoke-virtual {v1, v2, v12}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1340384
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isRequestedServerSettings:Z

    .line 1340385
    iget-object v10, p0, LX/8Om;->h:LX/11H;

    iget-object v11, p0, LX/8Om;->n:LX/8NU;

    new-instance v1, LX/8NV;

    invoke-virtual {v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->C()J

    move-result-wide v2

    .line 1340386
    iget-object v4, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v4, v4

    .line 1340387
    iget-object v5, p1, LX/8Ob;->E:LX/60x;

    iget-wide v6, p1, LX/8Ob;->m:J

    iget-object v8, p1, LX/8Ob;->n:Ljava/lang/String;

    invoke-direct/range {v1 .. v8}, LX/8NV;-><init>(JLjava/lang/String;LX/60x;JLjava/lang/String;)V

    iget-object v0, p1, LX/8Ob;->c:LX/14U;

    invoke-virtual {v10, v11, v1, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8NW;

    .line 1340388
    const/4 v1, 0x1

    iput-boolean v1, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isServerSettingsAvailable:Z

    .line 1340389
    invoke-virtual {v0}, LX/8NW;->a()J

    move-result-wide v2

    iput-wide v2, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeDimension:J

    .line 1340390
    invoke-virtual {v0}, LX/8NW;->b()J

    move-result-wide v0

    iput-wide v0, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeBitrate:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1340391
    :goto_1
    invoke-static {p0, p1}, LX/8Om;->e(LX/8Om;LX/8Ob;)V

    goto :goto_0

    .line 1340392
    :catch_0
    iput-boolean v12, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isServerSettingsAvailable:Z

    goto :goto_1

    .line 1340393
    :cond_2
    iget-object v0, p0, LX/8Om;->K:LX/8K9;

    invoke-virtual {v0}, LX/8K9;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1340394
    iget-object v0, p0, LX/8Om;->K:LX/8K9;

    invoke-virtual {v0, p1}, LX/8K9;->a(LX/8Ob;)V

    .line 1340395
    iput-boolean v5, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isUsingContextualConfig:Z

    .line 1340396
    iput-boolean v5, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isRequestedServerSettings:Z

    .line 1340397
    iget-object v0, p0, LX/8Om;->K:LX/8K9;

    invoke-virtual {v0}, LX/8K9;->b()I

    move-result v0

    .line 1340398
    iget-object v1, p0, LX/8Om;->K:LX/8K9;

    invoke-virtual {v1}, LX/8K9;->c()I

    move-result v1

    .line 1340399
    iget-object v2, p0, LX/8Om;->K:LX/8K9;

    invoke-virtual {v2}, LX/8K9;->d()I

    move-result v2

    .line 1340400
    iget-object v3, p0, LX/8Om;->K:LX/8K9;

    invoke-virtual {v3}, LX/8K9;->e()F

    move-result v3

    .line 1340401
    iget-object v4, p0, LX/8Om;->K:LX/8K9;

    .line 1340402
    iget-object v6, v4, LX/8K9;->c:LX/1jr;

    invoke-virtual {v6}, LX/1jr;->d()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual {v4}, LX/8K9;->d()I

    move-result v6

    if-ltz v6, :cond_5

    invoke-virtual {v4}, LX/8K9;->e()F

    move-result v6

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_5

    invoke-virtual {v4}, LX/8K9;->c()I

    move-result v6

    if-ltz v6, :cond_5

    invoke-virtual {v4}, LX/8K9;->b()I

    move-result v6

    if-ltz v6, :cond_5

    const/4 v6, 0x1

    :goto_2
    move v4, v6

    .line 1340403
    if-nez v4, :cond_3

    .line 1340404
    iput-boolean v12, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isServerSettingsAvailable:Z

    .line 1340405
    :goto_3
    invoke-static {p0, p1}, LX/8Om;->e(LX/8Om;LX/8Ob;)V

    goto/16 :goto_0

    .line 1340406
    :cond_3
    iput-boolean v5, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isServerSettingsAvailable:Z

    .line 1340407
    iget-object v4, p0, LX/8Om;->s:LX/8Oh;

    iget-object v5, p1, LX/8Ob;->E:LX/60x;

    .line 1340408
    iget-object v6, v4, LX/8Oh;->f:LX/7Su;

    invoke-virtual {v6, v1, v0}, LX/7Su;->a(II)V

    .line 1340409
    iget-object v6, v4, LX/8Oh;->i:LX/2Mj;

    iget-object v7, v4, LX/8Oh;->f:LX/7Su;

    const/4 v8, -0x1

    const/4 v10, -0x2

    invoke-virtual {v6, v5, v7, v8, v10}, LX/2Mj;->a(LX/60x;LX/2Md;II)LX/7Sw;

    move-result-object v6

    .line 1340410
    iget v6, v6, LX/7Sw;->c:I

    move v4, v6

    .line 1340411
    iput v2, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->skipBytesThreshold:I

    .line 1340412
    iput v3, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->skipRatioThreshold:F

    .line 1340413
    iget-wide v6, p1, LX/8Ob;->m:J

    invoke-static {v4, v6, v7, v2, v3}, LX/8Oh;->a(IJIF)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1340414
    int-to-long v2, v0

    iput-wide v2, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeDimension:J

    .line 1340415
    int-to-long v0, v1

    iput-wide v0, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeBitrate:J

    goto :goto_3

    .line 1340416
    :cond_4
    iget v0, p1, LX/8Ob;->s:I

    int-to-long v0, v0

    iput-wide v0, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeDimension:J

    .line 1340417
    sget v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->a:I

    int-to-long v0, v0

    iput-wide v0, v9, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeBitrate:J

    goto :goto_3

    :cond_5
    const/4 v6, 0x0

    goto :goto_2
.end method

.method private h(LX/8Ob;)V
    .locals 4

    .prologue
    .line 1340662
    iget-object v0, p0, LX/8Om;->B:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1340663
    iget-object v0, p0, LX/8Om;->y:LX/0Sh;

    new-instance v1, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;-><init>(LX/8Om;LX/8Ob;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    .line 1340664
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 14

    .prologue
    .line 1340682
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1340683
    iget-object v0, p0, LX/8Om;->j:LX/0cW;

    .line 1340684
    const-string v1, "video_upload_in_progress_waterfallid"

    invoke-static {v0, p1, v1}, LX/0cW;->b(LX/0cW;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    .line 1340685
    invoke-static {v0, p1}, LX/0cW;->i(LX/0cW;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1340686
    :cond_0
    const/4 v9, 0x0

    const/4 v13, -0x1

    .line 1340687
    invoke-static {p1}, LX/8Om;->e(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/8Ob;

    move-result-object v12

    .line 1340688
    const-string v3, "Upload Session Context cannot be null"

    invoke-static {v12, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340689
    iget-object v3, p0, LX/8Om;->i:LX/8LX;

    iget-object v4, p0, LX/8Om;->f:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v5

    invoke-virtual {v3, p1, v5, v6}, LX/8LX;->a(Lcom/facebook/photos/upload/operation/UploadOperation;J)LX/73w;

    move-result-object v3

    iput-object v3, v12, LX/8Ob;->y:LX/73w;

    .line 1340690
    iget-object v3, v12, LX/8Ob;->y:LX/73w;

    const-string v4, "2.1"

    sget-object v5, LX/742;->CHUNKED:LX/742;

    invoke-virtual {v3, v4, v5}, LX/73w;->a(Ljava/lang/String;LX/742;)LX/74b;

    move-result-object v3

    iput-object v3, v12, LX/8Ob;->A:LX/74b;

    .line 1340691
    iget-object v3, v12, LX/8Ob;->y:LX/73w;

    iget-object v4, v12, LX/8Ob;->A:LX/74b;

    .line 1340692
    invoke-virtual {v4}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v5

    .line 1340693
    invoke-static {v3, v5, p1}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1340694
    sget-object v6, LX/74R;->MEDIA_UPLOAD_INIT_CONTEXT:LX/74R;

    const/4 v7, 0x0

    invoke-static {v3, v6, v5, v7}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1340695
    iget-object v3, p0, LX/8Om;->k:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/8Ne;

    .line 1340696
    iget-object v3, p0, LX/8Om;->C:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1340697
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->i()Z

    move-result v3

    invoke-interface {v5, v3}, LX/8Ne;->a(Z)V

    .line 1340698
    iget-object v3, p0, LX/8Om;->z:Ljava/util/concurrent/Semaphore;

    invoke-interface {v5, v3}, LX/8Ne;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1340699
    :cond_1
    new-instance v3, LX/8On;

    iget-object v4, p0, LX/8Om;->l:LX/8OL;

    iget-object v6, v12, LX/8Ob;->y:LX/73w;

    iget-object v7, v12, LX/8Ob;->A:LX/74b;

    iget-object v10, p0, LX/8Om;->I:LX/0lB;

    iget-object v11, p0, LX/8Om;->L:LX/0cX;

    move-object v8, p1

    invoke-direct/range {v3 .. v11}, LX/8On;-><init>(LX/8OL;LX/8Ne;LX/73w;LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Oo;LX/0lB;LX/0cX;)V

    iput-object v3, p0, LX/8Om;->w:LX/8On;

    .line 1340700
    invoke-static {p1}, LX/8Oh;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v3

    .line 1340701
    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v12, LX/8Ob;->j:Ljava/lang/String;

    iput-object v4, v12, LX/8Ob;->q:Ljava/lang/String;

    .line 1340702
    new-instance v4, Ljava/io/File;

    iget-object v5, v12, LX/8Ob;->q:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v5

    iput-wide v5, v12, LX/8Ob;->m:J

    .line 1340703
    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v12, LX/8Ob;->k:Ljava/lang/String;

    .line 1340704
    invoke-static {p1, v12}, LX/8Oh;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ob;)V

    .line 1340705
    invoke-static {v12}, LX/8Om;->d(LX/8Ob;)V

    .line 1340706
    iget-object v4, v12, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-object v4, v4, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    iget-wide v5, v4, Lcom/facebook/photos/upload/operation/TranscodeInfo;->flowStartCount:J

    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    iput-wide v5, v4, Lcom/facebook/photos/upload/operation/TranscodeInfo;->flowStartCount:J

    .line 1340707
    invoke-static {p0, v12}, LX/8Om;->e(LX/8Om;LX/8Ob;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1340708
    :try_start_1
    iget-object v4, p0, LX/8Om;->s:LX/8Oh;

    invoke-virtual {v4, v3}, LX/8Oh;->a(Lcom/facebook/photos/base/media/VideoItem;)LX/60x;

    move-result-object v3

    iput-object v3, v12, LX/8Ob;->E:LX/60x;

    .line 1340709
    iget-object v3, v12, LX/8Ob;->E:LX/60x;

    iget-wide v3, v3, LX/60x;->a:J

    iput-wide v3, v12, LX/8Ob;->r:J

    .line 1340710
    iget-object v3, v12, LX/8Ob;->E:LX/60x;

    iget v3, v3, LX/60x;->b:I

    iget-object v4, v12, LX/8Ob;->E:LX/60x;

    iget v4, v4, LX/60x;->c:I

    if-lt v3, v4, :cond_4

    iget-object v3, v12, LX/8Ob;->E:LX/60x;

    iget v3, v3, LX/60x;->b:I

    :goto_0
    iput v3, v12, LX/8Ob;->s:I

    .line 1340711
    iget-object v3, v12, LX/8Ob;->E:LX/60x;

    iget v3, v3, LX/60x;->g:I

    iput v3, v12, LX/8Ob;->t:I

    .line 1340712
    iget-object v3, v12, LX/8Ob;->E:LX/60x;

    invoke-virtual {v3}, LX/60x;->a()Z

    move-result v3

    iput-boolean v3, v12, LX/8Ob;->F:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1340713
    :goto_1
    :try_start_2
    move-object v0, v12

    .line 1340714
    invoke-direct {p0, v0}, LX/8Om;->b(LX/8Ob;)V

    .line 1340715
    invoke-direct {p0, v0}, LX/8Om;->f(LX/8Ob;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 1340716
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1340717
    iget-object v1, p0, LX/8Om;->j:LX/0cW;

    .line 1340718
    iget-object v2, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, v2

    .line 1340719
    invoke-virtual {v1, v2}, LX/0cW;->a(Ljava/lang/String;)V

    :cond_2
    return-object v0

    .line 1340720
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1340721
    iget-object v1, p0, LX/8Om;->j:LX/0cW;

    .line 1340722
    iget-object v2, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, v2

    .line 1340723
    invoke-virtual {v1, v2}, LX/0cW;->a(Ljava/lang/String;)V

    :cond_3
    throw v0

    .line 1340724
    :cond_4
    :try_start_3
    iget-object v3, v12, LX/8Ob;->E:LX/60x;

    iget v3, v3, LX/60x;->c:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1340725
    :catch_0
    move-exception v3

    .line 1340726
    const-wide/16 v5, 0x0

    iput-wide v5, v12, LX/8Ob;->r:J

    .line 1340727
    iput v13, v12, LX/8Ob;->s:I

    .line 1340728
    iput v13, v12, LX/8Ob;->t:I

    .line 1340729
    iput-object v9, v12, LX/8Ob;->E:LX/60x;

    .line 1340730
    const/4 v4, 0x1

    invoke-static {v3, v4}, LX/0cX;->a(Ljava/lang/Exception;Z)LX/73z;

    move-result-object v3

    .line 1340731
    iget-object v4, v12, LX/8Ob;->y:LX/73w;

    iget-object v5, v12, LX/8Ob;->A:LX/74b;

    .line 1340732
    invoke-virtual {v5}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v6

    .line 1340733
    invoke-static {v4, v6}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1340734
    invoke-static {v6, v3}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1340735
    sget-object v7, LX/74R;->MEDIA_UPLOAD_ATTEMPT_GET_METADATA_FAILURE:LX/74R;

    const/4 v8, 0x0

    invoke-static {v4, v7, v6, v8}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1340736
    goto :goto_1
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1340665
    iget-object v0, p0, LX/8Om;->l:LX/8OL;

    invoke-virtual {v0}, LX/8OL;->a()V

    .line 1340666
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, LX/8Om;->z:Ljava/util/concurrent/Semaphore;

    .line 1340667
    return-void
.end method

.method public final a(DLcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 7

    .prologue
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    .line 1340668
    if-nez p3, :cond_0

    .line 1340669
    :goto_0
    return-void

    .line 1340670
    :cond_0
    iget-object v0, p0, LX/8Om;->D:LX/0ad;

    sget-short v1, LX/8Jz;->aa:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 1340671
    if-eqz v0, :cond_1

    mul-double v0, v4, p1

    double-to-float v0, v0

    .line 1340672
    :goto_1
    iget-object v1, p0, LX/8Om;->g:LX/0b3;

    new-instance v2, LX/8Kk;

    sget-object v3, LX/8KZ;->PROCESSING:LX/8KZ;

    invoke-direct {v2, p3, v3, v0}, LX/8Kk;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 1340673
    :cond_1
    mul-double v0, v4, p1

    double-to-int v0, v0

    int-to-float v0, v0

    goto :goto_1
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1340674
    iget-object v0, p0, LX/8Om;->z:Ljava/util/concurrent/Semaphore;

    if-eqz v0, :cond_0

    .line 1340675
    iget-object v0, p0, LX/8Om;->z:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1340676
    :cond_0
    iget-object v0, p0, LX/8Om;->l:LX/8OL;

    invoke-virtual {v0}, LX/8OL;->c()Z

    move-result v0

    .line 1340677
    iget-object v1, p0, LX/8Om;->G:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/8Om;->G:Ljava/util/concurrent/Future;

    invoke-interface {v1}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1340678
    iget-object v1, p0, LX/8Om;->G:Ljava/util/concurrent/Future;

    invoke-interface {v1, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1340679
    :cond_1
    iget-object v1, p0, LX/8Om;->H:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/8Om;->H:Ljava/util/concurrent/Future;

    invoke-interface {v1}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1340680
    iget-object v1, p0, LX/8Om;->H:Ljava/util/concurrent/Future;

    invoke-interface {v1, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1340681
    :cond_2
    return v0
.end method
