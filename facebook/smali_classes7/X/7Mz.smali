.class public abstract LX/7Mz;
.super LX/7Mr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lf;",
        ">",
        "LX/7Mr",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public A:Ljava/lang/String;

.field public a:LX/3Gg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7OG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final o:Landroid/view/View;

.field private final p:Z

.field public q:Z

.field public final r:Lcom/facebook/resources/ui/FbTextView;

.field public final s:Lcom/facebook/resources/ui/FbTextView;

.field public t:Landroid/view/View;

.field public u:LX/3Af;

.field public v:Landroid/graphics/drawable/Drawable;

.field public w:Ljava/lang/String;

.field public final x:I

.field public final y:I

.field public z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1199681
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7Mz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1199682
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1199679
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7Mz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199680
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1199654
    invoke-direct {p0, p1, p2, p3}, LX/7Mr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199655
    iput-boolean v2, p0, LX/7Mz;->z:Z

    .line 1199656
    const-class v0, LX/7Mz;

    invoke-static {v0, p0}, LX/7Mz;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1199657
    const v0, 0x7f0d0553

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/7Mz;->o:Landroid/view/View;

    .line 1199658
    invoke-virtual {p0}, LX/7Mz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1199659
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0xf0

    if-lt v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1199660
    iput-boolean v0, p0, LX/7Mz;->p:Z

    .line 1199661
    invoke-virtual {p0}, LX/7Mz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/7Mz;->x:I

    .line 1199662
    invoke-virtual {p0}, LX/7Mz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a03ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/7Mz;->y:I

    .line 1199663
    invoke-virtual {p0}, LX/7Mz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0207d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/7Mz;->v:Landroid/graphics/drawable/Drawable;

    .line 1199664
    invoke-virtual {p0}, LX/7Mz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080d6b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7Mz;->w:Ljava/lang/String;

    .line 1199665
    iget-object v0, p0, LX/7Mz;->a:LX/3Gg;

    invoke-virtual {v0}, LX/3Gg;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7Mz;->A:Ljava/lang/String;

    .line 1199666
    const v0, 0x7f0d0908

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 1199667
    const v0, 0x7f0d27c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->b(I)LX/0am;

    move-result-object v0

    .line 1199668
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1199669
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1199670
    :goto_1
    move-object v0, v0

    .line 1199671
    iput-object v0, p0, LX/7Mz;->t:Landroid/view/View;

    .line 1199672
    const v0, 0x7f0d27c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->b(I)LX/0am;

    move-result-object v0

    .line 1199673
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1199674
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1199675
    :goto_2
    move-object v0, v0

    .line 1199676
    iput-object v0, p0, LX/7Mz;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 1199677
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7NQ;

    invoke-direct {v1, p0}, LX/7NQ;-><init>(LX/7Mz;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199678
    return-void

    :cond_0
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static a(LX/7Mz;Ljava/lang/String;Z)V
    .locals 10

    .prologue
    .line 1199642
    invoke-static {p0}, LX/7Mz;->getCurrentToggleViewText(LX/7Mz;)Ljava/lang/String;

    move-result-object v8

    .line 1199643
    iget-boolean v0, p0, LX/7Mz;->q:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/7Mz;->s:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_2

    .line 1199644
    iget-object v0, p0, LX/7Mz;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1199645
    :goto_0
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/7Mr;->f:LX/1C2;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 1199646
    iget-object v0, p0, LX/7Mr;->f:LX/1C2;

    iget-object v1, p0, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, p0, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v3, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v3}, LX/2pb;->s()LX/04D;

    move-result-object v3

    iget-object v4, p0, LX/2oy;->j:LX/2pb;

    .line 1199647
    iget-object v5, v4, LX/2pb;->D:LX/04G;

    move-object v4, v5

    .line 1199648
    iget-object v5, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v5}, LX/2pb;->h()I

    move-result v5

    iget-object v6, p0, LX/7Mr;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v6, v6, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    iget-object v9, p0, LX/7Mz;->A:Ljava/lang/String;

    move-object v7, p1

    invoke-virtual/range {v0 .. v9}, LX/1C2;->a(Ljava/lang/String;LX/0lF;LX/04D;LX/04G;IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/1C2;

    .line 1199649
    :cond_0
    iget-object v0, p0, LX/7Mz;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1199650
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2qd;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    sget-object v3, LX/2oi;->CUSTOM_DEFINITION:LX/2oi;

    const-string v4, "Auto"

    invoke-direct {v1, v2, v3, v4}, LX/2qd;-><init>(LX/04g;LX/2oi;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1199651
    :goto_1
    return-void

    .line 1199652
    :cond_1
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2qd;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    sget-object v3, LX/2oi;->CUSTOM_DEFINITION:LX/2oi;

    invoke-direct {v1, v2, v3, p1}, LX/2qd;-><init>(LX/04g;LX/2oi;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_1

    .line 1199653
    :cond_2
    iget-object v0, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/7Mz;

    invoke-static {p0}, LX/3Gg;->b(LX/0QB;)LX/3Gg;

    move-result-object v1

    check-cast v1, LX/3Gg;

    invoke-static {p0}, LX/7OG;->a(LX/0QB;)LX/7OG;

    move-result-object p0

    check-cast p0, LX/7OG;

    iput-object v1, p1, LX/7Mz;->a:LX/3Gg;

    iput-object p0, p1, LX/7Mz;->b:LX/7OG;

    return-void
.end method

.method public static a(LX/7Mz;Ljava/util/List;)Z
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/engine/VideoDataSource;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1199634
    iget-boolean v0, p0, LX/7Mz;->p:Z

    if-eqz v0, :cond_1

    const/4 p0, 0x0

    .line 1199635
    move v1, p0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1199636
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    .line 1199637
    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 1199638
    const/4 p0, 0x1

    .line 1199639
    :cond_0
    move v0, p0

    .line 1199640
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1199641
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static getCurrentToggleViewText(LX/7Mz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1199570
    iget-boolean v0, p0, LX/7Mz;->q:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7Mz;->s:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_0

    .line 1199571
    iget-object v0, p0, LX/7Mz;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1199572
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static j$redex0(LX/7Mz;)V
    .locals 2

    .prologue
    .line 1199626
    iget-object v0, p0, LX/7Mz;->a:LX/3Gg;

    invoke-virtual {v0}, LX/3Gg;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/7Mz;->b:LX/7OG;

    invoke-virtual {v0}, LX/7OG;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1199627
    iget-object v0, p0, LX/7Mz;->b:LX/7OG;

    invoke-virtual {v0}, LX/7OG;->a()Ljava/lang/String;

    move-result-object v0

    .line 1199628
    :goto_0
    move-object v0, v0

    .line 1199629
    if-eqz v0, :cond_1

    .line 1199630
    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v1}, LX/2pb;->g()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1199631
    :cond_0
    iget-object v0, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/7Mz;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1199632
    :cond_1
    :goto_1
    return-void

    .line 1199633
    :cond_2
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/7Mz;->a(LX/7Mz;Ljava/lang/String;Z)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, LX/7Mz;->A:Ljava/lang/String;

    goto :goto_0
.end method

.method public static z(LX/7Mz;)V
    .locals 2

    .prologue
    .line 1199620
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->f()LX/2oi;

    move-result-object v0

    sget-object v1, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/7Mz;->z:Z

    .line 1199621
    iget-object v1, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    iget-boolean v0, p0, LX/7Mz;->z:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/7Mz;->x:I

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1199622
    iget-object v0, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f080d7b

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1199623
    return-void

    .line 1199624
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1199625
    :cond_1
    iget v0, p0, LX/7Mz;->y:I

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/2oi;)V
    .locals 1

    .prologue
    .line 1199617
    iget-boolean v0, p0, LX/7Mz;->q:Z

    if-nez v0, :cond_0

    .line 1199618
    invoke-static {p0}, LX/7Mz;->z(LX/7Mz;)V

    .line 1199619
    :cond_0
    return-void
.end method

.method public a(LX/2pa;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1199590
    invoke-super {p0, p1, p2}, LX/7Mr;->a(LX/2pa;Z)V

    .line 1199591
    if-eqz p2, :cond_0

    .line 1199592
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->o:Z

    iput-boolean v0, p0, LX/7Mz;->q:Z

    .line 1199593
    iget-boolean v0, p0, LX/7Mz;->q:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    .line 1199594
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->g()Ljava/util/List;

    .line 1199595
    iget-object v2, p0, LX/7Mz;->s:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v2, :cond_2

    .line 1199596
    iget-object v2, p0, LX/7Mz;->s:Lcom/facebook/resources/ui/FbTextView;

    const p1, 0x7f080d6b

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1199597
    iget-object v2, p0, LX/7Mz;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/7Mz;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0a00a7

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1199598
    :goto_0
    iget-boolean v2, p0, LX/7Mz;->q:Z

    if-eqz v2, :cond_4

    .line 1199599
    iget-object v2, p0, LX/7Mz;->t:Landroid/view/View;

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/7Mz;->a:LX/3Gg;

    invoke-virtual {v2}, LX/3Gg;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1199600
    iget-object v2, p0, LX/7Mz;->t:Landroid/view/View;

    new-instance p1, LX/7NO;

    invoke-direct {p1, p0}, LX/7NO;-><init>(LX/7Mz;)V

    invoke-virtual {v2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1199601
    :goto_1
    new-instance v2, LX/3Af;

    invoke-virtual {p0}, LX/7Mz;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v2, p1}, LX/3Af;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/7Mz;->u:LX/3Af;

    .line 1199602
    invoke-static {p0}, LX/7Mz;->j$redex0(LX/7Mz;)V

    .line 1199603
    :goto_2
    invoke-virtual {p0, v1}, LX/7Mz;->setSeekBarVisibility(I)V

    .line 1199604
    :cond_0
    invoke-virtual {p0, v1}, LX/7Mz;->setQualitySelectorVisibility(I)V

    .line 1199605
    return-void

    .line 1199606
    :cond_1
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    .line 1199607
    invoke-static {p0, v0}, LX/7Mz;->a(LX/7Mz;Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1199608
    iget-object v2, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    const/16 p1, 0x8

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1199609
    :goto_3
    goto :goto_2

    .line 1199610
    :cond_2
    iget-object v2, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    const p1, 0x7f080d6b

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1199611
    iget-object v2, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/7Mz;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0a004b

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0

    .line 1199612
    :cond_3
    iget-object v2, p0, LX/7Mz;->s:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v2, :cond_4

    .line 1199613
    iget-object v2, p0, LX/7Mz;->s:Lcom/facebook/resources/ui/FbTextView;

    new-instance p1, LX/7NO;

    invoke-direct {p1, p0}, LX/7NO;-><init>(LX/7Mz;)V

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 1199614
    :cond_4
    iget-object v2, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    new-instance p1, LX/7NO;

    invoke-direct {p1, p0}, LX/7NO;-><init>(LX/7Mz;)V

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 1199615
    :cond_5
    iget-object v2, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    new-instance p1, LX/7NP;

    invoke-direct {p1, p0}, LX/7NP;-><init>(LX/7Mz;)V

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1199616
    invoke-static {p0}, LX/7Mz;->z(LX/7Mz;)V

    goto :goto_3
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1199584
    invoke-super {p0}, LX/7Mr;->d()V

    .line 1199585
    iget-boolean v0, p0, LX/7Mz;->q:Z

    if-eqz v0, :cond_0

    .line 1199586
    invoke-virtual {p0, v1}, LX/7Mz;->setQualitySelectorVisibility(I)V

    .line 1199587
    :goto_0
    iget-object v0, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1199588
    return-void

    .line 1199589
    :cond_0
    iget-object v0, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public getActiveThumbResource()I
    .locals 1

    .prologue
    .line 1199583
    const/4 v0, 0x0

    return v0
.end method

.method public setQualitySelectorVisibility(I)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1199575
    iget-boolean v0, p0, LX/7Mz;->q:Z

    if-eqz v0, :cond_0

    .line 1199576
    iget-object v0, p0, LX/7Mz;->t:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/7Mz;->a:LX/3Gg;

    invoke-virtual {v0}, LX/3Gg;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1199577
    iget-object v0, p0, LX/7Mz;->t:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1199578
    iget-object v0, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1199579
    :cond_0
    :goto_0
    return-void

    .line 1199580
    :cond_1
    iget-object v0, p0, LX/7Mz;->s:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_0

    .line 1199581
    iget-object v0, p0, LX/7Mz;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1199582
    iget-object v0, p0, LX/7Mz;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSeekBarVisibility(I)V
    .locals 1

    .prologue
    .line 1199573
    iget-object v0, p0, LX/7Mz;->o:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1199574
    return-void
.end method
