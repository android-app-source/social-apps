.class public LX/6mL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final messageId:Ljava/lang/Integer;

.field public final payload:[B

.field public final topic:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xb

    const/4 v4, 0x1

    .line 1143783
    new-instance v0, LX/1sv;

    const-string v1, "ConnPublishMessage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mL;->b:LX/1sv;

    .line 1143784
    new-instance v0, LX/1sw;

    const-string v1, "topic"

    invoke-direct {v0, v1, v5, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mL;->c:LX/1sw;

    .line 1143785
    new-instance v0, LX/1sw;

    const-string v1, "messageId"

    const/16 v2, 0x8

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mL;->d:LX/1sw;

    .line 1143786
    new-instance v0, LX/1sw;

    const-string v1, "payload"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mL;->e:LX/1sw;

    .line 1143787
    sput-boolean v4, LX/6mL;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;[B)V
    .locals 0

    .prologue
    .line 1143863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1143864
    iput-object p1, p0, LX/6mL;->topic:Ljava/lang/String;

    .line 1143865
    iput-object p2, p0, LX/6mL;->messageId:Ljava/lang/Integer;

    .line 1143866
    iput-object p3, p0, LX/6mL;->payload:[B

    .line 1143867
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x80

    .line 1143821
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1143822
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v2, v0

    .line 1143823
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1143824
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "ConnPublishMessage"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1143825
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143826
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143827
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143828
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143829
    const-string v1, "topic"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143830
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143831
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143832
    iget-object v1, p0, LX/6mL;->topic:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 1143833
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143834
    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143835
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143836
    const-string v1, "messageId"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143837
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143838
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143839
    iget-object v1, p0, LX/6mL;->messageId:Ljava/lang/Integer;

    if-nez v1, :cond_5

    .line 1143840
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143841
    :goto_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143842
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143843
    const-string v1, "payload"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143844
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143845
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143846
    iget-object v0, p0, LX/6mL;->payload:[B

    if-nez v0, :cond_6

    .line 1143847
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143848
    :cond_0
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143849
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143850
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1143851
    :cond_1
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1143852
    :cond_2
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1143853
    :cond_3
    const-string v0, ""

    goto/16 :goto_2

    .line 1143854
    :cond_4
    iget-object v1, p0, LX/6mL;->topic:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1143855
    :cond_5
    iget-object v1, p0, LX/6mL;->messageId:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1143856
    :cond_6
    iget-object v0, p0, LX/6mL;->payload:[B

    array-length v0, v0

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 1143857
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-ge v1, v5, :cond_9

    .line 1143858
    if-eqz v1, :cond_7

    const-string v0, " "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143859
    :cond_7
    iget-object v0, p0, LX/6mL;->payload:[B

    aget-byte v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v6, 0x1

    if-le v0, v6, :cond_8

    iget-object v0, p0, LX/6mL;->payload:[B

    aget-byte v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, LX/6mL;->payload:[B

    aget-byte v6, v6, v1

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    :goto_7
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143860
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1143861
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "0"

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, LX/6mL;->payload:[B

    aget-byte v6, v6, v1

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 1143862
    :cond_9
    iget-object v0, p0, LX/6mL;->payload:[B

    array-length v0, v0

    if-le v0, v7, :cond_0

    const-string v0, " ..."

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1143868
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1143869
    iget-object v0, p0, LX/6mL;->topic:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1143870
    sget-object v0, LX/6mL;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143871
    iget-object v0, p0, LX/6mL;->topic:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1143872
    :cond_0
    iget-object v0, p0, LX/6mL;->messageId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1143873
    sget-object v0, LX/6mL;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143874
    iget-object v0, p0, LX/6mL;->messageId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1143875
    :cond_1
    iget-object v0, p0, LX/6mL;->payload:[B

    if-eqz v0, :cond_2

    .line 1143876
    sget-object v0, LX/6mL;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143877
    iget-object v0, p0, LX/6mL;->payload:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 1143878
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1143879
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1143880
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1143792
    if-nez p1, :cond_1

    .line 1143793
    :cond_0
    :goto_0
    return v0

    .line 1143794
    :cond_1
    instance-of v1, p1, LX/6mL;

    if-eqz v1, :cond_0

    .line 1143795
    check-cast p1, LX/6mL;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1143796
    if-nez p1, :cond_3

    .line 1143797
    :cond_2
    :goto_1
    move v0, v2

    .line 1143798
    goto :goto_0

    .line 1143799
    :cond_3
    iget-object v0, p0, LX/6mL;->topic:Ljava/lang/String;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1143800
    :goto_2
    iget-object v3, p1, LX/6mL;->topic:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1143801
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1143802
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1143803
    iget-object v0, p0, LX/6mL;->topic:Ljava/lang/String;

    iget-object v3, p1, LX/6mL;->topic:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1143804
    :cond_5
    iget-object v0, p0, LX/6mL;->messageId:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1143805
    :goto_4
    iget-object v3, p1, LX/6mL;->messageId:Ljava/lang/Integer;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1143806
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1143807
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1143808
    iget-object v0, p0, LX/6mL;->messageId:Ljava/lang/Integer;

    iget-object v3, p1, LX/6mL;->messageId:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1143809
    :cond_7
    iget-object v0, p0, LX/6mL;->payload:[B

    if-eqz v0, :cond_e

    move v0, v1

    .line 1143810
    :goto_6
    iget-object v3, p1, LX/6mL;->payload:[B

    if-eqz v3, :cond_f

    move v3, v1

    .line 1143811
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1143812
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1143813
    iget-object v0, p0, LX/6mL;->payload:[B

    iget-object v3, p1, LX/6mL;->payload:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 1143814
    goto :goto_1

    :cond_a
    move v0, v2

    .line 1143815
    goto :goto_2

    :cond_b
    move v3, v2

    .line 1143816
    goto :goto_3

    :cond_c
    move v0, v2

    .line 1143817
    goto :goto_4

    :cond_d
    move v3, v2

    .line 1143818
    goto :goto_5

    :cond_e
    move v0, v2

    .line 1143819
    goto :goto_6

    :cond_f
    move v3, v2

    .line 1143820
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1143791
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1143788
    sget-boolean v0, LX/6mL;->a:Z

    .line 1143789
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mL;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1143790
    return-object v0
.end method
