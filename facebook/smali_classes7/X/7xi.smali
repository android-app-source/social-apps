.class public final enum LX/7xi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7xi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7xi;

.field public static final enum EVENT_INFO_HEADER:LX/7xi;

.field public static final enum QUANTITY_SELECT_HEADER:LX/7xi;

.field public static final enum SEAT_MAP_IMAGE:LX/7xi;

.field public static final enum SEAT_SELECTION_NOTE:LX/7xi;

.field public static final enum TICKET_TIER_ITEM:LX/7xi;

.field public static final enum TICKET_TIER_NOT_AVAILABLE_ITEM:LX/7xi;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1277692
    new-instance v0, LX/7xi;

    const-string v1, "EVENT_INFO_HEADER"

    invoke-direct {v0, v1, v3}, LX/7xi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xi;->EVENT_INFO_HEADER:LX/7xi;

    .line 1277693
    new-instance v0, LX/7xi;

    const-string v1, "QUANTITY_SELECT_HEADER"

    invoke-direct {v0, v1, v4}, LX/7xi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xi;->QUANTITY_SELECT_HEADER:LX/7xi;

    .line 1277694
    new-instance v0, LX/7xi;

    const-string v1, "TICKET_TIER_ITEM"

    invoke-direct {v0, v1, v5}, LX/7xi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xi;->TICKET_TIER_ITEM:LX/7xi;

    .line 1277695
    new-instance v0, LX/7xi;

    const-string v1, "TICKET_TIER_NOT_AVAILABLE_ITEM"

    invoke-direct {v0, v1, v6}, LX/7xi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xi;->TICKET_TIER_NOT_AVAILABLE_ITEM:LX/7xi;

    .line 1277696
    new-instance v0, LX/7xi;

    const-string v1, "SEAT_MAP_IMAGE"

    invoke-direct {v0, v1, v7}, LX/7xi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xi;->SEAT_MAP_IMAGE:LX/7xi;

    .line 1277697
    new-instance v0, LX/7xi;

    const-string v1, "SEAT_SELECTION_NOTE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7xi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xi;->SEAT_SELECTION_NOTE:LX/7xi;

    .line 1277698
    const/4 v0, 0x6

    new-array v0, v0, [LX/7xi;

    sget-object v1, LX/7xi;->EVENT_INFO_HEADER:LX/7xi;

    aput-object v1, v0, v3

    sget-object v1, LX/7xi;->QUANTITY_SELECT_HEADER:LX/7xi;

    aput-object v1, v0, v4

    sget-object v1, LX/7xi;->TICKET_TIER_ITEM:LX/7xi;

    aput-object v1, v0, v5

    sget-object v1, LX/7xi;->TICKET_TIER_NOT_AVAILABLE_ITEM:LX/7xi;

    aput-object v1, v0, v6

    sget-object v1, LX/7xi;->SEAT_MAP_IMAGE:LX/7xi;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7xi;->SEAT_SELECTION_NOTE:LX/7xi;

    aput-object v2, v0, v1

    sput-object v0, LX/7xi;->$VALUES:[LX/7xi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1277699
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7xi;
    .locals 1

    .prologue
    .line 1277700
    const-class v0, LX/7xi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7xi;

    return-object v0
.end method

.method public static values()[LX/7xi;
    .locals 1

    .prologue
    .line 1277701
    sget-object v0, LX/7xi;->$VALUES:[LX/7xi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7xi;

    return-object v0
.end method
