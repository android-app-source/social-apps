.class public LX/8Jc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:F

.field public final c:F

.field public final d:F


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1328921
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1328922
    iput-object p1, p0, LX/8Jc;->a:Landroid/content/res/Resources;

    .line 1328923
    iget-object v0, p0, LX/8Jc;->a:Landroid/content/res/Resources;

    .line 1328924
    const p1, 0x7f0b0ffe

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    move v0, p1

    .line 1328925
    iput v0, p0, LX/8Jc;->b:F

    .line 1328926
    iget-object v0, p0, LX/8Jc;->a:Landroid/content/res/Resources;

    .line 1328927
    const p1, 0x7f0b0fff

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    move v0, p1

    .line 1328928
    iput v0, p0, LX/8Jc;->c:F

    .line 1328929
    iget-object v0, p0, LX/8Jc;->a:Landroid/content/res/Resources;

    .line 1328930
    const p1, 0x7f0b1003

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    move v0, p1

    .line 1328931
    iput v0, p0, LX/8Jc;->d:F

    .line 1328932
    return-void
.end method

.method public static a(IILandroid/graphics/RectF;)F
    .locals 3

    .prologue
    .line 1328933
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v0

    int-to-float v1, p1

    mul-float/2addr v0, v1

    .line 1328934
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v1

    int-to-float v2, p0

    mul-float/2addr v1, v2

    .line 1328935
    const v2, 0x3f4ccccd    # 0.8f

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float/2addr v0, v2

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public static a(LX/8Jc;F)LX/8Jb;
    .locals 4

    .prologue
    .line 1328951
    new-instance v1, LX/8Jb;

    invoke-direct {v1, p0}, LX/8Jb;-><init>(LX/8Jc;)V

    .line 1328952
    invoke-direct {p0}, LX/8Jc;->a()Landroid/util/Pair;

    move-result-object v2

    .line 1328953
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 1328954
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1328955
    iput p1, v1, LX/8Jb;->a:F

    .line 1328956
    const/4 v2, 0x0

    iput v2, v1, LX/8Jb;->b:F

    .line 1328957
    add-float/2addr v0, v3

    iput v0, v1, LX/8Jb;->c:F

    .line 1328958
    return-object v1
.end method

.method public static a(LX/8Jc;Landroid/graphics/RectF;FF)LX/8Jb;
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 1328936
    new-instance v1, LX/8Jb;

    invoke-direct {v1, p0}, LX/8Jb;-><init>(LX/8Jc;)V

    .line 1328937
    invoke-direct {p0}, LX/8Jc;->a()Landroid/util/Pair;

    move-result-object v2

    .line 1328938
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 1328939
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1328940
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1328941
    div-float v2, v0, v2

    mul-float/2addr v2, p2

    .line 1328942
    cmpl-float v4, v2, v5

    if-ltz v4, :cond_0

    .line 1328943
    invoke-static {v2, p3}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 1328944
    div-float v2, v4, v2

    mul-float/2addr v2, v0

    iput v2, v1, LX/8Jb;->b:F

    .line 1328945
    iget v2, v1, LX/8Jb;->b:F

    sub-float/2addr v0, v2

    add-float/2addr v0, v3

    iput v0, v1, LX/8Jb;->c:F

    .line 1328946
    iput v4, v1, LX/8Jb;->a:F

    .line 1328947
    :goto_0
    return-object v1

    .line 1328948
    :cond_0
    const/4 v2, 0x0

    iput v2, v1, LX/8Jb;->c:F

    .line 1328949
    add-float/2addr v0, v3

    iput v0, v1, LX/8Jb;->b:F

    .line 1328950
    iput v5, v1, LX/8Jb;->a:F

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/8Jc;
    .locals 1

    .prologue
    .line 1328920
    invoke-static {p0}, LX/8Jc;->b(LX/0QB;)LX/8Jc;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/graphics/PointF;LX/8Jb;FFZ)Landroid/graphics/PointF;
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, 0x0

    .line 1328878
    div-float v2, p3, v9

    .line 1328879
    if-eqz p4, :cond_4

    .line 1328880
    iget v0, p0, Landroid/graphics/PointF;->x:F

    iget v1, p1, LX/8Jb;->a:F

    mul-float/2addr v1, v0

    .line 1328881
    iget v0, p1, LX/8Jb;->a:F

    mul-float v5, p2, v0

    .line 1328882
    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1328883
    sub-float v1, v5, v1

    sub-float v1, p3, v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 1328884
    sub-float v6, v2, v0

    .line 1328885
    sub-float v7, v1, v2

    .line 1328886
    cmpl-float v5, v6, v8

    if-lez v5, :cond_0

    move v5, v3

    :goto_0
    cmpl-float v8, v7, v8

    if-lez v8, :cond_1

    :goto_1
    xor-int/2addr v3, v5

    if-eqz v3, :cond_3

    .line 1328887
    cmpl-float v2, v6, v7

    if-lez v2, :cond_2

    .line 1328888
    :goto_2
    iget v1, p1, LX/8Jb;->c:F

    iget v2, p1, LX/8Jb;->b:F

    div-float/2addr v2, v9

    add-float/2addr v1, v2

    .line 1328889
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v2

    :cond_0
    move v5, v4

    .line 1328890
    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1

    :cond_2
    move v0, v1

    .line 1328891
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1328892
    goto :goto_2

    :cond_4
    move v0, v2

    .line 1328893
    goto :goto_2
.end method

.method public static a(LX/7UZ;Landroid/graphics/RectF;Landroid/graphics/Matrix;)Landroid/graphics/RectF;
    .locals 6

    .prologue
    .line 1328912
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1328913
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1328914
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1328915
    invoke-interface {p0}, LX/7UZ;->e()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1328916
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    invoke-interface {p0}, LX/7UZ;->getPhotoWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p1, Landroid/graphics/RectF;->top:F

    invoke-interface {p0}, LX/7UZ;->getPhotoHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget v3, p1, Landroid/graphics/RectF;->right:F

    invoke-interface {p0}, LX/7UZ;->getPhotoWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    invoke-interface {p0}, LX/7UZ;->getPhotoHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1328917
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1328918
    invoke-virtual {p2, v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1328919
    return-object v1
.end method

.method private a()Landroid/util/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1328894
    iget-object v0, p0, LX/8Jc;->a:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1328895
    iget v1, p0, LX/8Jc;->b:F

    iget v2, p0, LX/8Jc;->c:F

    add-float/2addr v1, v2

    iget v2, p0, LX/8Jc;->d:F

    add-float/2addr v1, v2

    int-to-float v0, v0

    sub-float v0, v1, v0

    move v2, v0

    .line 1328896
    iget v1, p0, LX/8Jc;->b:F

    .line 1328897
    iget v0, p0, LX/8Jc;->c:F

    .line 1328898
    cmpl-float v3, v2, v4

    if-lez v3, :cond_0

    .line 1328899
    add-float/2addr v0, v1

    .line 1328900
    sub-float v2, v0, v2

    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1328901
    div-float v0, v1, v0

    .line 1328902
    mul-float v1, v2, v0

    .line 1328903
    sub-float v0, v2, v1

    .line 1328904
    :cond_0
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8Jc;
    .locals 2

    .prologue
    .line 1328905
    new-instance v1, LX/8Jc;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/8Jc;-><init>(Landroid/content/res/Resources;)V

    .line 1328906
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;)F
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1328907
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1328908
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1328909
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, LX/8Jc;->c:F

    iget v3, p0, LX/8Jc;->c:F

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1328910
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1328911
    const v0, 0x3f4ccccd    # 0.8f

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v0, v2

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    return v0
.end method
