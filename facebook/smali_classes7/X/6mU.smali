.class public LX/6mU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;

.field private static final l:LX/1sw;

.field private static final m:LX/1sw;

.field private static final n:LX/1sw;


# instance fields
.field public final actionId:Ljava/lang/Long;

.field public final actorFbId:Ljava/lang/Long;

.field public final adPageMessageType:Ljava/lang/String;

.field public final mark:Ljava/lang/String;

.field public final otherUserFbId:Ljava/lang/Long;

.field public final shouldSendReadReceipt:Ljava/lang/Boolean;

.field public final state:Ljava/lang/Boolean;

.field public final syncSeqId:Ljava/lang/Long;

.field public final threadFbId:Ljava/lang/Long;

.field public final threadId:Ljava/lang/String;

.field public final titanOriginatedThreadId:Ljava/lang/String;

.field public final watermarkTimestamp:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const/16 v4, 0xb

    const/16 v3, 0xa

    .line 1144878
    new-instance v0, LX/1sv;

    const-string v1, "MarkThread"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mU;->b:LX/1sv;

    .line 1144879
    new-instance v0, LX/1sw;

    const-string v1, "mark"

    invoke-direct {v0, v1, v4, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mU;->c:LX/1sw;

    .line 1144880
    new-instance v0, LX/1sw;

    const-string v1, "state"

    invoke-direct {v0, v1, v5, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mU;->d:LX/1sw;

    .line 1144881
    new-instance v0, LX/1sw;

    const-string v1, "threadId"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mU;->e:LX/1sw;

    .line 1144882
    new-instance v0, LX/1sw;

    const-string v1, "actionId"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mU;->f:LX/1sw;

    .line 1144883
    new-instance v0, LX/1sw;

    const-string v1, "syncSeqId"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mU;->g:LX/1sw;

    .line 1144884
    new-instance v0, LX/1sw;

    const-string v1, "threadFbId"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mU;->h:LX/1sw;

    .line 1144885
    new-instance v0, LX/1sw;

    const-string v1, "otherUserFbId"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mU;->i:LX/1sw;

    .line 1144886
    new-instance v0, LX/1sw;

    const-string v1, "actorFbId"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mU;->j:LX/1sw;

    .line 1144887
    new-instance v0, LX/1sw;

    const-string v1, "watermarkTimestamp"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mU;->k:LX/1sw;

    .line 1144888
    new-instance v0, LX/1sw;

    const-string v1, "titanOriginatedThreadId"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mU;->l:LX/1sw;

    .line 1144889
    new-instance v0, LX/1sw;

    const-string v1, "shouldSendReadReceipt"

    invoke-direct {v0, v1, v5, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mU;->m:LX/1sw;

    .line 1144890
    new-instance v0, LX/1sw;

    const-string v1, "adPageMessageType"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mU;->n:LX/1sw;

    .line 1144891
    sput-boolean v6, LX/6mU;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1144864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1144865
    iput-object p1, p0, LX/6mU;->mark:Ljava/lang/String;

    .line 1144866
    iput-object p2, p0, LX/6mU;->state:Ljava/lang/Boolean;

    .line 1144867
    iput-object p3, p0, LX/6mU;->threadId:Ljava/lang/String;

    .line 1144868
    iput-object p4, p0, LX/6mU;->actionId:Ljava/lang/Long;

    .line 1144869
    iput-object p5, p0, LX/6mU;->syncSeqId:Ljava/lang/Long;

    .line 1144870
    iput-object p6, p0, LX/6mU;->threadFbId:Ljava/lang/Long;

    .line 1144871
    iput-object p7, p0, LX/6mU;->otherUserFbId:Ljava/lang/Long;

    .line 1144872
    iput-object p8, p0, LX/6mU;->actorFbId:Ljava/lang/Long;

    .line 1144873
    iput-object p9, p0, LX/6mU;->watermarkTimestamp:Ljava/lang/Long;

    .line 1144874
    iput-object p10, p0, LX/6mU;->titanOriginatedThreadId:Ljava/lang/String;

    .line 1144875
    iput-object p11, p0, LX/6mU;->shouldSendReadReceipt:Ljava/lang/Boolean;

    .line 1144876
    iput-object p12, p0, LX/6mU;->adPageMessageType:Ljava/lang/String;

    .line 1144877
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1144746
    if-eqz p2, :cond_a

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1144747
    :goto_0
    if-eqz p2, :cond_b

    const-string v0, "\n"

    move-object v1, v0

    .line 1144748
    :goto_1
    if-eqz p2, :cond_c

    const-string v0, " "

    .line 1144749
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MarkThread"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1144750
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144751
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144752
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144753
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144754
    const-string v4, "mark"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144755
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144756
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144757
    iget-object v4, p0, LX/6mU;->mark:Ljava/lang/String;

    if-nez v4, :cond_d

    .line 1144758
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144759
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144760
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144761
    const-string v4, "state"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144762
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144763
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144764
    iget-object v4, p0, LX/6mU;->state:Ljava/lang/Boolean;

    if-nez v4, :cond_e

    .line 1144765
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144766
    :goto_4
    iget-object v4, p0, LX/6mU;->threadId:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1144767
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144768
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144769
    const-string v4, "threadId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144770
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144771
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144772
    iget-object v4, p0, LX/6mU;->threadId:Ljava/lang/String;

    if-nez v4, :cond_f

    .line 1144773
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144774
    :cond_0
    :goto_5
    iget-object v4, p0, LX/6mU;->actionId:Ljava/lang/Long;

    if-eqz v4, :cond_1

    .line 1144775
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144776
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144777
    const-string v4, "actionId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144778
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144779
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144780
    iget-object v4, p0, LX/6mU;->actionId:Ljava/lang/Long;

    if-nez v4, :cond_10

    .line 1144781
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144782
    :cond_1
    :goto_6
    iget-object v4, p0, LX/6mU;->syncSeqId:Ljava/lang/Long;

    if-eqz v4, :cond_2

    .line 1144783
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144784
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144785
    const-string v4, "syncSeqId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144786
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144787
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144788
    iget-object v4, p0, LX/6mU;->syncSeqId:Ljava/lang/Long;

    if-nez v4, :cond_11

    .line 1144789
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144790
    :cond_2
    :goto_7
    iget-object v4, p0, LX/6mU;->threadFbId:Ljava/lang/Long;

    if-eqz v4, :cond_3

    .line 1144791
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144792
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144793
    const-string v4, "threadFbId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144794
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144795
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144796
    iget-object v4, p0, LX/6mU;->threadFbId:Ljava/lang/Long;

    if-nez v4, :cond_12

    .line 1144797
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144798
    :cond_3
    :goto_8
    iget-object v4, p0, LX/6mU;->otherUserFbId:Ljava/lang/Long;

    if-eqz v4, :cond_4

    .line 1144799
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144800
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144801
    const-string v4, "otherUserFbId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144802
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144803
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144804
    iget-object v4, p0, LX/6mU;->otherUserFbId:Ljava/lang/Long;

    if-nez v4, :cond_13

    .line 1144805
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144806
    :cond_4
    :goto_9
    iget-object v4, p0, LX/6mU;->actorFbId:Ljava/lang/Long;

    if-eqz v4, :cond_5

    .line 1144807
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144808
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144809
    const-string v4, "actorFbId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144810
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144811
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144812
    iget-object v4, p0, LX/6mU;->actorFbId:Ljava/lang/Long;

    if-nez v4, :cond_14

    .line 1144813
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144814
    :cond_5
    :goto_a
    iget-object v4, p0, LX/6mU;->watermarkTimestamp:Ljava/lang/Long;

    if-eqz v4, :cond_6

    .line 1144815
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144816
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144817
    const-string v4, "watermarkTimestamp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144818
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144819
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144820
    iget-object v4, p0, LX/6mU;->watermarkTimestamp:Ljava/lang/Long;

    if-nez v4, :cond_15

    .line 1144821
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144822
    :cond_6
    :goto_b
    iget-object v4, p0, LX/6mU;->titanOriginatedThreadId:Ljava/lang/String;

    if-eqz v4, :cond_7

    .line 1144823
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144824
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144825
    const-string v4, "titanOriginatedThreadId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144826
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144827
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144828
    iget-object v4, p0, LX/6mU;->titanOriginatedThreadId:Ljava/lang/String;

    if-nez v4, :cond_16

    .line 1144829
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144830
    :cond_7
    :goto_c
    iget-object v4, p0, LX/6mU;->shouldSendReadReceipt:Ljava/lang/Boolean;

    if-eqz v4, :cond_8

    .line 1144831
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144832
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144833
    const-string v4, "shouldSendReadReceipt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144834
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144835
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144836
    iget-object v4, p0, LX/6mU;->shouldSendReadReceipt:Ljava/lang/Boolean;

    if-nez v4, :cond_17

    .line 1144837
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144838
    :cond_8
    :goto_d
    iget-object v4, p0, LX/6mU;->adPageMessageType:Ljava/lang/String;

    if-eqz v4, :cond_9

    .line 1144839
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144840
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144841
    const-string v4, "adPageMessageType"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144842
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144843
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144844
    iget-object v0, p0, LX/6mU;->adPageMessageType:Ljava/lang/String;

    if-nez v0, :cond_18

    .line 1144845
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144846
    :cond_9
    :goto_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144847
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144848
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1144849
    :cond_a
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1144850
    :cond_b
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1144851
    :cond_c
    const-string v0, ""

    goto/16 :goto_2

    .line 1144852
    :cond_d
    iget-object v4, p0, LX/6mU;->mark:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1144853
    :cond_e
    iget-object v4, p0, LX/6mU;->state:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1144854
    :cond_f
    iget-object v4, p0, LX/6mU;->threadId:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1144855
    :cond_10
    iget-object v4, p0, LX/6mU;->actionId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1144856
    :cond_11
    iget-object v4, p0, LX/6mU;->syncSeqId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1144857
    :cond_12
    iget-object v4, p0, LX/6mU;->threadFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1144858
    :cond_13
    iget-object v4, p0, LX/6mU;->otherUserFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 1144859
    :cond_14
    iget-object v4, p0, LX/6mU;->actorFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 1144860
    :cond_15
    iget-object v4, p0, LX/6mU;->watermarkTimestamp:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    .line 1144861
    :cond_16
    iget-object v4, p0, LX/6mU;->titanOriginatedThreadId:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    .line 1144862
    :cond_17
    iget-object v4, p0, LX/6mU;->shouldSendReadReceipt:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_d

    .line 1144863
    :cond_18
    iget-object v0, p0, LX/6mU;->adPageMessageType:Ljava/lang/String;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_e
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1144696
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1144697
    iget-object v0, p0, LX/6mU;->mark:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1144698
    sget-object v0, LX/6mU;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144699
    iget-object v0, p0, LX/6mU;->mark:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1144700
    :cond_0
    iget-object v0, p0, LX/6mU;->state:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1144701
    sget-object v0, LX/6mU;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144702
    iget-object v0, p0, LX/6mU;->state:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1144703
    :cond_1
    iget-object v0, p0, LX/6mU;->threadId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1144704
    iget-object v0, p0, LX/6mU;->threadId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1144705
    sget-object v0, LX/6mU;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144706
    iget-object v0, p0, LX/6mU;->threadId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1144707
    :cond_2
    iget-object v0, p0, LX/6mU;->actionId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1144708
    iget-object v0, p0, LX/6mU;->actionId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1144709
    sget-object v0, LX/6mU;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144710
    iget-object v0, p0, LX/6mU;->actionId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1144711
    :cond_3
    iget-object v0, p0, LX/6mU;->syncSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1144712
    iget-object v0, p0, LX/6mU;->syncSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1144713
    sget-object v0, LX/6mU;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144714
    iget-object v0, p0, LX/6mU;->syncSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1144715
    :cond_4
    iget-object v0, p0, LX/6mU;->threadFbId:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 1144716
    iget-object v0, p0, LX/6mU;->threadFbId:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 1144717
    sget-object v0, LX/6mU;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144718
    iget-object v0, p0, LX/6mU;->threadFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1144719
    :cond_5
    iget-object v0, p0, LX/6mU;->otherUserFbId:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 1144720
    iget-object v0, p0, LX/6mU;->otherUserFbId:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 1144721
    sget-object v0, LX/6mU;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144722
    iget-object v0, p0, LX/6mU;->otherUserFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1144723
    :cond_6
    iget-object v0, p0, LX/6mU;->actorFbId:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 1144724
    iget-object v0, p0, LX/6mU;->actorFbId:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 1144725
    sget-object v0, LX/6mU;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144726
    iget-object v0, p0, LX/6mU;->actorFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1144727
    :cond_7
    iget-object v0, p0, LX/6mU;->watermarkTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 1144728
    iget-object v0, p0, LX/6mU;->watermarkTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 1144729
    sget-object v0, LX/6mU;->k:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144730
    iget-object v0, p0, LX/6mU;->watermarkTimestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1144731
    :cond_8
    iget-object v0, p0, LX/6mU;->titanOriginatedThreadId:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1144732
    iget-object v0, p0, LX/6mU;->titanOriginatedThreadId:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1144733
    sget-object v0, LX/6mU;->l:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144734
    iget-object v0, p0, LX/6mU;->titanOriginatedThreadId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1144735
    :cond_9
    iget-object v0, p0, LX/6mU;->shouldSendReadReceipt:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 1144736
    iget-object v0, p0, LX/6mU;->shouldSendReadReceipt:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 1144737
    sget-object v0, LX/6mU;->m:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144738
    iget-object v0, p0, LX/6mU;->shouldSendReadReceipt:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1144739
    :cond_a
    iget-object v0, p0, LX/6mU;->adPageMessageType:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 1144740
    iget-object v0, p0, LX/6mU;->adPageMessageType:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 1144741
    sget-object v0, LX/6mU;->n:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144742
    iget-object v0, p0, LX/6mU;->adPageMessageType:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1144743
    :cond_b
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1144744
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1144745
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1144600
    if-nez p1, :cond_1

    .line 1144601
    :cond_0
    :goto_0
    return v0

    .line 1144602
    :cond_1
    instance-of v1, p1, LX/6mU;

    if-eqz v1, :cond_0

    .line 1144603
    check-cast p1, LX/6mU;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1144604
    if-nez p1, :cond_3

    .line 1144605
    :cond_2
    :goto_1
    move v0, v2

    .line 1144606
    goto :goto_0

    .line 1144607
    :cond_3
    iget-object v0, p0, LX/6mU;->mark:Ljava/lang/String;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1144608
    :goto_2
    iget-object v3, p1, LX/6mU;->mark:Ljava/lang/String;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1144609
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1144610
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144611
    iget-object v0, p0, LX/6mU;->mark:Ljava/lang/String;

    iget-object v3, p1, LX/6mU;->mark:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144612
    :cond_5
    iget-object v0, p0, LX/6mU;->state:Ljava/lang/Boolean;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1144613
    :goto_4
    iget-object v3, p1, LX/6mU;->state:Ljava/lang/Boolean;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1144614
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1144615
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144616
    iget-object v0, p0, LX/6mU;->state:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6mU;->state:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144617
    :cond_7
    iget-object v0, p0, LX/6mU;->threadId:Ljava/lang/String;

    if-eqz v0, :cond_20

    move v0, v1

    .line 1144618
    :goto_6
    iget-object v3, p1, LX/6mU;->threadId:Ljava/lang/String;

    if-eqz v3, :cond_21

    move v3, v1

    .line 1144619
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1144620
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144621
    iget-object v0, p0, LX/6mU;->threadId:Ljava/lang/String;

    iget-object v3, p1, LX/6mU;->threadId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144622
    :cond_9
    iget-object v0, p0, LX/6mU;->actionId:Ljava/lang/Long;

    if-eqz v0, :cond_22

    move v0, v1

    .line 1144623
    :goto_8
    iget-object v3, p1, LX/6mU;->actionId:Ljava/lang/Long;

    if-eqz v3, :cond_23

    move v3, v1

    .line 1144624
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1144625
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144626
    iget-object v0, p0, LX/6mU;->actionId:Ljava/lang/Long;

    iget-object v3, p1, LX/6mU;->actionId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144627
    :cond_b
    iget-object v0, p0, LX/6mU;->syncSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_24

    move v0, v1

    .line 1144628
    :goto_a
    iget-object v3, p1, LX/6mU;->syncSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_25

    move v3, v1

    .line 1144629
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1144630
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144631
    iget-object v0, p0, LX/6mU;->syncSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/6mU;->syncSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144632
    :cond_d
    iget-object v0, p0, LX/6mU;->threadFbId:Ljava/lang/Long;

    if-eqz v0, :cond_26

    move v0, v1

    .line 1144633
    :goto_c
    iget-object v3, p1, LX/6mU;->threadFbId:Ljava/lang/Long;

    if-eqz v3, :cond_27

    move v3, v1

    .line 1144634
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1144635
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144636
    iget-object v0, p0, LX/6mU;->threadFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/6mU;->threadFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144637
    :cond_f
    iget-object v0, p0, LX/6mU;->otherUserFbId:Ljava/lang/Long;

    if-eqz v0, :cond_28

    move v0, v1

    .line 1144638
    :goto_e
    iget-object v3, p1, LX/6mU;->otherUserFbId:Ljava/lang/Long;

    if-eqz v3, :cond_29

    move v3, v1

    .line 1144639
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1144640
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144641
    iget-object v0, p0, LX/6mU;->otherUserFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/6mU;->otherUserFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144642
    :cond_11
    iget-object v0, p0, LX/6mU;->actorFbId:Ljava/lang/Long;

    if-eqz v0, :cond_2a

    move v0, v1

    .line 1144643
    :goto_10
    iget-object v3, p1, LX/6mU;->actorFbId:Ljava/lang/Long;

    if-eqz v3, :cond_2b

    move v3, v1

    .line 1144644
    :goto_11
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 1144645
    :cond_12
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144646
    iget-object v0, p0, LX/6mU;->actorFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/6mU;->actorFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144647
    :cond_13
    iget-object v0, p0, LX/6mU;->watermarkTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_2c

    move v0, v1

    .line 1144648
    :goto_12
    iget-object v3, p1, LX/6mU;->watermarkTimestamp:Ljava/lang/Long;

    if-eqz v3, :cond_2d

    move v3, v1

    .line 1144649
    :goto_13
    if-nez v0, :cond_14

    if-eqz v3, :cond_15

    .line 1144650
    :cond_14
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144651
    iget-object v0, p0, LX/6mU;->watermarkTimestamp:Ljava/lang/Long;

    iget-object v3, p1, LX/6mU;->watermarkTimestamp:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144652
    :cond_15
    iget-object v0, p0, LX/6mU;->titanOriginatedThreadId:Ljava/lang/String;

    if-eqz v0, :cond_2e

    move v0, v1

    .line 1144653
    :goto_14
    iget-object v3, p1, LX/6mU;->titanOriginatedThreadId:Ljava/lang/String;

    if-eqz v3, :cond_2f

    move v3, v1

    .line 1144654
    :goto_15
    if-nez v0, :cond_16

    if-eqz v3, :cond_17

    .line 1144655
    :cond_16
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144656
    iget-object v0, p0, LX/6mU;->titanOriginatedThreadId:Ljava/lang/String;

    iget-object v3, p1, LX/6mU;->titanOriginatedThreadId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144657
    :cond_17
    iget-object v0, p0, LX/6mU;->shouldSendReadReceipt:Ljava/lang/Boolean;

    if-eqz v0, :cond_30

    move v0, v1

    .line 1144658
    :goto_16
    iget-object v3, p1, LX/6mU;->shouldSendReadReceipt:Ljava/lang/Boolean;

    if-eqz v3, :cond_31

    move v3, v1

    .line 1144659
    :goto_17
    if-nez v0, :cond_18

    if-eqz v3, :cond_19

    .line 1144660
    :cond_18
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144661
    iget-object v0, p0, LX/6mU;->shouldSendReadReceipt:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6mU;->shouldSendReadReceipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144662
    :cond_19
    iget-object v0, p0, LX/6mU;->adPageMessageType:Ljava/lang/String;

    if-eqz v0, :cond_32

    move v0, v1

    .line 1144663
    :goto_18
    iget-object v3, p1, LX/6mU;->adPageMessageType:Ljava/lang/String;

    if-eqz v3, :cond_33

    move v3, v1

    .line 1144664
    :goto_19
    if-nez v0, :cond_1a

    if-eqz v3, :cond_1b

    .line 1144665
    :cond_1a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144666
    iget-object v0, p0, LX/6mU;->adPageMessageType:Ljava/lang/String;

    iget-object v3, p1, LX/6mU;->adPageMessageType:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1b
    move v2, v1

    .line 1144667
    goto/16 :goto_1

    :cond_1c
    move v0, v2

    .line 1144668
    goto/16 :goto_2

    :cond_1d
    move v3, v2

    .line 1144669
    goto/16 :goto_3

    :cond_1e
    move v0, v2

    .line 1144670
    goto/16 :goto_4

    :cond_1f
    move v3, v2

    .line 1144671
    goto/16 :goto_5

    :cond_20
    move v0, v2

    .line 1144672
    goto/16 :goto_6

    :cond_21
    move v3, v2

    .line 1144673
    goto/16 :goto_7

    :cond_22
    move v0, v2

    .line 1144674
    goto/16 :goto_8

    :cond_23
    move v3, v2

    .line 1144675
    goto/16 :goto_9

    :cond_24
    move v0, v2

    .line 1144676
    goto/16 :goto_a

    :cond_25
    move v3, v2

    .line 1144677
    goto/16 :goto_b

    :cond_26
    move v0, v2

    .line 1144678
    goto/16 :goto_c

    :cond_27
    move v3, v2

    .line 1144679
    goto/16 :goto_d

    :cond_28
    move v0, v2

    .line 1144680
    goto/16 :goto_e

    :cond_29
    move v3, v2

    .line 1144681
    goto/16 :goto_f

    :cond_2a
    move v0, v2

    .line 1144682
    goto/16 :goto_10

    :cond_2b
    move v3, v2

    .line 1144683
    goto/16 :goto_11

    :cond_2c
    move v0, v2

    .line 1144684
    goto/16 :goto_12

    :cond_2d
    move v3, v2

    .line 1144685
    goto/16 :goto_13

    :cond_2e
    move v0, v2

    .line 1144686
    goto/16 :goto_14

    :cond_2f
    move v3, v2

    .line 1144687
    goto/16 :goto_15

    :cond_30
    move v0, v2

    .line 1144688
    goto :goto_16

    :cond_31
    move v3, v2

    .line 1144689
    goto :goto_17

    :cond_32
    move v0, v2

    .line 1144690
    goto :goto_18

    :cond_33
    move v3, v2

    .line 1144691
    goto :goto_19
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1144692
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1144693
    sget-boolean v0, LX/6mU;->a:Z

    .line 1144694
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mU;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1144695
    return-object v0
.end method
