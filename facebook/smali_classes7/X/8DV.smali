.class public final LX/8DV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/3iO;


# direct methods
.method public constructor <init>(LX/3iO;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1312712
    iput-object p1, p0, LX/8DV;->b:LX/3iO;

    iput-object p2, p0, LX/8DV;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1312713
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1312714
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1312715
    if-eqz p1, :cond_0

    .line 1312716
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1312717
    if-nez v0, :cond_1

    .line 1312718
    :cond_0
    return-void

    .line 1312719
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1312720
    check-cast v0, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    .line 1312721
    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1312722
    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-static {v0}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v0

    iget-object v1, p0, LX/8DV;->a:Ljava/lang/String;

    .line 1312723
    iput-object v1, v0, LX/4Vu;->B:Ljava/lang/String;

    .line 1312724
    move-object v0, v0

    .line 1312725
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v2

    .line 1312726
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1312727
    iget-object v0, p0, LX/8DV;->b:LX/3iO;

    iget-object v1, v0, LX/3iO;->b:Ljava/util/Set;

    monitor-enter v1

    .line 1312728
    :try_start_0
    iget-object v0, p0, LX/8DV;->b:LX/3iO;

    iget-object v0, v0, LX/3iO;->b:Ljava/util/Set;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1312729
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1312730
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iN;

    .line 1312731
    invoke-interface {v0, v2}, LX/3iN;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1312732
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1312733
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
