.class public final LX/7Fo;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1188272
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1188273
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1188274
    :goto_0
    return v1

    .line 1188275
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1188276
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1188277
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1188278
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1188279
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1188280
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1188281
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1188282
    :cond_2
    const-string v5, "name"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1188283
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1188284
    :cond_3
    const-string v5, "survey_flow"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1188285
    invoke-static {p0, p1}, LX/7Fi;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1188286
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1188287
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1188288
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1188289
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1188290
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1188291
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1188292
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1188293
    if-eqz v0, :cond_0

    .line 1188294
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188295
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1188296
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1188297
    if-eqz v0, :cond_1

    .line 1188298
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188299
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1188300
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1188301
    if-eqz v0, :cond_2

    .line 1188302
    const-string v1, "survey_flow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188303
    invoke-static {p0, v0, p2, p3}, LX/7Fi;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1188304
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1188305
    return-void
.end method
