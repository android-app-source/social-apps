.class public LX/6ev;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public b:I

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:I

.field public r:Z

.field public s:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:LX/6et;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Z

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1118501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118502
    return-void
.end method

.method public static l(Ljava/lang/String;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1118486
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/16N;->b(Lorg/json/JSONArray;)LX/0Px;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1118487
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(Ljava/lang/String;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1118488
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1118489
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1118490
    invoke-virtual {v8}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1118491
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1118492
    const-string v1, "botID"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1118493
    const-string v1, "title"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1118494
    const-string v1, "description"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1118495
    const-string v1, "icon"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1118496
    new-instance v1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;

    invoke-direct/range {v1 .. v6}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1118497
    :catch_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 1118498
    :cond_0
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0Px;)LX/6ev;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/6ev;"
        }
    .end annotation

    .prologue
    .line 1118499
    iput-object p1, p0, LX/6ev;->g:LX/0Px;

    .line 1118500
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;)LX/6ev;
    .locals 0

    .prologue
    .line 1118503
    iput-object p1, p0, LX/6ev;->a:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 1118504
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;)LX/6ev;
    .locals 0
    .param p1    # Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1118507
    iput-object p1, p0, LX/6ev;->x:Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    .line 1118508
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;)LX/6ev;
    .locals 0
    .param p1    # Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1118505
    iput-object p1, p0, LX/6ev;->o:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;

    .line 1118506
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/6ev;
    .locals 2

    .prologue
    .line 1118512
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1118513
    const/16 v0, 0x10

    invoke-static {p1, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, LX/6ev;->b:I

    .line 1118514
    :cond_0
    return-object p0
.end method

.method public final a(Z)LX/6ev;
    .locals 0

    .prologue
    .line 1118515
    iput-boolean p1, p0, LX/6ev;->m:Z

    .line 1118516
    return-object p0
.end method

.method public final a()Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;
    .locals 26

    .prologue
    .line 1118511
    new-instance v1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/6ev;->a:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    move-object/from16 v0, p0

    iget v3, v0, LX/6ev;->b:I

    move-object/from16 v0, p0

    iget-object v4, v0, LX/6ev;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/6ev;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/6ev;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v7, v0, LX/6ev;->f:I

    move-object/from16 v0, p0

    iget-object v8, v0, LX/6ev;->g:LX/0Px;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/6ev;->h:LX/0Px;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/6ev;->i:LX/0Px;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/6ev;->j:LX/0Px;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/6ev;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/6ev;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v14, v0, LX/6ev;->m:Z

    move-object/from16 v0, p0

    iget-object v15, v0, LX/6ev;->n:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/6ev;->o:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/6ev;->p:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/6ev;->q:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/6ev;->r:Z

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/6ev;->s:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/6ev;->t:LX/6et;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/6ev;->u:Z

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/6ev;->v:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/6ev;->w:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/6ev;->x:Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    move-object/from16 v25, v0

    invoke-direct/range {v1 .. v25}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;-><init>(Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/0Px;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;Ljava/lang/String;IZLcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;LX/6et;ZLjava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;)V

    return-object v1
.end method

.method public final b(LX/0Px;)LX/6ev;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/6ev;"
        }
    .end annotation

    .prologue
    .line 1118509
    iput-object p1, p0, LX/6ev;->h:LX/0Px;

    .line 1118510
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/6ev;
    .locals 0

    .prologue
    .line 1118482
    iput-object p1, p0, LX/6ev;->c:Ljava/lang/String;

    .line 1118483
    return-object p0
.end method

.method public final b(Z)LX/6ev;
    .locals 0

    .prologue
    .line 1118484
    iput-boolean p1, p0, LX/6ev;->r:Z

    .line 1118485
    return-object p0
.end method

.method public final c(I)LX/6ev;
    .locals 0

    .prologue
    .line 1118456
    iput p1, p0, LX/6ev;->f:I

    .line 1118457
    return-object p0
.end method

.method public final c(LX/0Px;)LX/6ev;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;",
            ">;)",
            "LX/6ev;"
        }
    .end annotation

    .prologue
    .line 1118458
    iput-object p1, p0, LX/6ev;->i:LX/0Px;

    .line 1118459
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/6ev;
    .locals 0

    .prologue
    .line 1118460
    iput-object p1, p0, LX/6ev;->d:Ljava/lang/String;

    .line 1118461
    return-object p0
.end method

.method public final d(I)LX/6ev;
    .locals 0

    .prologue
    .line 1118462
    iput p1, p0, LX/6ev;->q:I

    .line 1118463
    return-object p0
.end method

.method public final d(LX/0Px;)LX/6ev;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;",
            ">;)",
            "LX/6ev;"
        }
    .end annotation

    .prologue
    .line 1118464
    iput-object p1, p0, LX/6ev;->j:LX/0Px;

    .line 1118465
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/6ev;
    .locals 0

    .prologue
    .line 1118466
    iput-object p1, p0, LX/6ev;->e:Ljava/lang/String;

    .line 1118467
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/6ev;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1118468
    iput-object p1, p0, LX/6ev;->k:Ljava/lang/String;

    .line 1118469
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/6ev;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1118470
    iput-object p1, p0, LX/6ev;->l:Ljava/lang/String;

    .line 1118471
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/6ev;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1118472
    iput-object p1, p0, LX/6ev;->n:Ljava/lang/String;

    .line 1118473
    return-object p0
.end method

.method public final h(Ljava/lang/String;)LX/6ev;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1118480
    iput-object p1, p0, LX/6ev;->p:Ljava/lang/String;

    .line 1118481
    return-object p0
.end method

.method public final i(Ljava/lang/String;)LX/6ev;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1118474
    invoke-static {p1}, LX/6et;->fromValue(Ljava/lang/String;)LX/6et;

    move-result-object v0

    iput-object v0, p0, LX/6ev;->t:LX/6et;

    .line 1118475
    return-object p0
.end method

.method public final j(Ljava/lang/String;)LX/6ev;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1118476
    iput-object p1, p0, LX/6ev;->v:Ljava/lang/String;

    .line 1118477
    return-object p0
.end method

.method public final k(Ljava/lang/String;)LX/6ev;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1118478
    iput-object p1, p0, LX/6ev;->w:Ljava/lang/String;

    .line 1118479
    return-object p0
.end method
