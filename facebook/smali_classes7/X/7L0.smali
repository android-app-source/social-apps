.class public final LX/7L0;
.super Landroid/animation/AnimatorListenerAdapter;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/FullScreenVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V
    .locals 0

    .prologue
    .line 1196724
    iput-object p1, p0, LX/7L0;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1196725
    iget-object v0, p0, LX/7L0;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, LX/7L0;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget v2, v2, Lcom/facebook/video/player/FullScreenVideoPlayer;->I:I

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1196726
    iget-object v0, p0, LX/7L0;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 1196727
    iget-object v0, p0, LX/7L0;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->k()V

    .line 1196728
    iget-object v0, p0, LX/7L0;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-static {v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->t(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    .line 1196729
    iget-object v0, p0, LX/7L0;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-static {v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->w(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    .line 1196730
    iget-object v0, p0, LX/7L0;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-boolean v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ah:Z

    if-eqz v0, :cond_1

    .line 1196731
    iget-object v0, p0, LX/7L0;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-boolean v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ai:Z

    if-eqz v0, :cond_0

    .line 1196732
    iget-object v0, p0, LX/7L0;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->R:Landroid/view/WindowManager;

    iget-object v1, p0, LX/7L0;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 1196733
    iget-object v0, p0, LX/7L0;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    .line 1196734
    iput-boolean v3, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ai:Z

    .line 1196735
    :cond_0
    iget-object v0, p0, LX/7L0;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    .line 1196736
    iput-boolean v3, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ah:Z

    .line 1196737
    :cond_1
    return-void
.end method
