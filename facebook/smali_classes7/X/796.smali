.class public abstract LX/796;
.super LX/78l;
.source ""


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/795;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1MT;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportActivityFileProvider;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Ze;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/4lA;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/792;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/793;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0l0;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0W9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/795;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1MT;",
            ">;>;",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportActivityFileProvider;",
            ">;>;",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Ze;",
            ">;>;",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/4lA;",
            ">;>;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "LX/792;",
            ">;",
            "LX/0Or",
            "<",
            "LX/793;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0l0;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0W9;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1173862
    invoke-direct {p0, p1}, LX/78l;-><init>(Landroid/content/Context;)V

    .line 1173863
    iput-object p2, p0, LX/796;->b:LX/0Or;

    .line 1173864
    iput-object p3, p0, LX/796;->c:LX/0Or;

    .line 1173865
    iput-object p4, p0, LX/796;->d:LX/0Or;

    .line 1173866
    iput-object p5, p0, LX/796;->e:LX/0Or;

    .line 1173867
    iput-object p6, p0, LX/796;->f:LX/0Or;

    .line 1173868
    iput-object p7, p0, LX/796;->g:LX/0Or;

    .line 1173869
    iput-object p8, p0, LX/796;->h:LX/0Or;

    .line 1173870
    iput-object p9, p0, LX/796;->i:LX/0Or;

    .line 1173871
    iput-object p10, p0, LX/796;->j:LX/0Or;

    .line 1173872
    iput-object p11, p0, LX/796;->k:LX/0Or;

    .line 1173873
    iput-object p12, p0, LX/796;->l:LX/0Or;

    .line 1173874
    iput-object p13, p0, LX/796;->m:LX/0Or;

    .line 1173875
    return-void
.end method


# virtual methods
.method public final f()Z
    .locals 1

    .prologue
    .line 1173876
    iget-object v0, p0, LX/796;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1173860
    iget-object v0, p0, LX/796;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1173861
    iget-object v0, p0, LX/796;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-virtual {v0}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/795;
    .locals 1

    .prologue
    .line 1173825
    iget-object v0, p0, LX/796;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/795;

    return-object v0
.end method

.method public final j()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1173832
    invoke-super {p0}, LX/78l;->j()Ljava/util/Map;

    move-result-object v0

    .line 1173833
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v0}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    move-object v1, v1

    .line 1173834
    iget-object v0, p0, LX/796;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/793;

    .line 1173835
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 1173836
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v3

    .line 1173837
    const-string v4, "image_file_bytes"

    invoke-virtual {v3, v4}, LX/009;->getCustomData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1173838
    :try_start_0
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1173839
    const-string v3, "image_cache_size_bytes"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1173840
    :goto_0
    iget-object v3, v0, LX/793;->a:LX/0V8;

    sget-object v4, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v3, v4}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v4

    .line 1173841
    const-string v3, "free_internal_storage_bytes"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1173842
    const/4 v3, 0x0

    .line 1173843
    :try_start_1
    iget-object v4, v0, LX/793;->b:Landroid/content/pm/PackageManager;

    invoke-static {}, LX/007;->p()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 1173844
    :goto_1
    move-object v3, v3

    .line 1173845
    const-string v4, "first_install_time"

    iget-wide v6, v3, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1173846
    const-string v4, "last_upgrade_time"

    iget-wide v6, v3, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1173847
    iget-object v3, v0, LX/793;->c:LX/0kb;

    .line 1173848
    iget-object v4, v3, LX/0kb;->t:LX/0kh;

    move-object v3, v4

    .line 1173849
    if-eqz v3, :cond_1

    .line 1173850
    iget v4, v3, LX/0kh;->c:I

    move v4, v4

    .line 1173851
    if-ltz v4, :cond_0

    const/16 v5, 0x64

    if-gt v4, v5, :cond_0

    .line 1173852
    const-string v5, "inet_cond"

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1173853
    :cond_0
    invoke-virtual {v3}, LX/0kh;->c()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v3

    .line 1173854
    if-eqz v3, :cond_1

    .line 1173855
    const-string v4, "connection_state"

    invoke-virtual {v3}, Landroid/net/NetworkInfo$DetailedState;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1173856
    :cond_1
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    move-object v0, v2

    .line 1173857
    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1173858
    return-object v1

    :catch_0
    goto :goto_0

    :catch_1
    goto :goto_1

    .line 1173859
    :catch_2
    goto :goto_1
.end method

.method public final k()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/1MT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1173826
    invoke-super {p0}, LX/78l;->k()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1173827
    iget-object v0, p0, LX/796;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1173828
    return-object v1
.end method

.method public final l()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0Ze;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1173829
    invoke-super {p0}, LX/78l;->l()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1173830
    iget-object v0, p0, LX/796;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1173831
    return-object v1
.end method
