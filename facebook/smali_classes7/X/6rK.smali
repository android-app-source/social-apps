.class public final LX/6rK;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6rL;


# direct methods
.method public constructor <init>(LX/6rL;)V
    .locals 0

    .prologue
    .line 1152042
    iput-object p1, p0, LX/6rK;->a:LX/6rL;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1152043
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1152044
    check-cast p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    .line 1152045
    iget-object v0, p0, LX/6rK;->a:LX/6rL;

    .line 1152046
    iget-object v1, v0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {v1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;)Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    .line 1152047
    iget-object v2, v0, LX/6rL;->f:LX/6qd;

    iget-object v3, v0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-interface {v2, v3, v1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    .line 1152048
    iget-object v1, v0, LX/6rL;->f:LX/6qd;

    iget-object v2, v0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-interface {v1, v2}, LX/6qd;->b(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1152049
    iget-object v1, v0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {v1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->v:LX/0Px;

    invoke-static {v1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a(LX/0Px;)LX/0P1;

    move-result-object v1

    .line 1152050
    invoke-virtual {v1}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1152051
    iget-object p0, v0, LX/6rL;->f:LX/6qd;

    iget-object p1, v0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Px;

    invoke-interface {p0, p1, v2, v1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;LX/0Px;)V

    goto :goto_0

    .line 1152052
    :cond_0
    return-void
.end method
