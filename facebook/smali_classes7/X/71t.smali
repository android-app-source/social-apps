.class public LX/71t;
.super LX/6vv;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6vv",
        "<",
        "Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;",
        "Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;",
        "Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;",
        "Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;",
        "LX/729;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1163747
    invoke-direct {p0}, LX/6vv;-><init>()V

    .line 1163748
    return-void
.end method

.method public static a(LX/0QB;)LX/71t;
    .locals 3

    .prologue
    .line 1163749
    const-class v1, LX/71t;

    monitor-enter v1

    .line 1163750
    :try_start_0
    sget-object v0, LX/71t;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1163751
    sput-object v2, LX/71t;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1163752
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1163753
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1163754
    new-instance v0, LX/71t;

    invoke-direct {v0}, LX/71t;-><init>()V

    .line 1163755
    move-object v0, v0

    .line 1163756
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1163757
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/71t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1163758
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1163759
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 1163760
    check-cast p1, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;

    .line 1163761
    new-instance v0, Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;

    invoke-direct {v0, p1}, Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;-><init>(Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 1163762
    check-cast p1, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;

    check-cast p2, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;

    check-cast p3, Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;

    .line 1163763
    new-instance v0, Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;-><init>(Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;LX/0P1;)V

    return-object v0
.end method
