.class public final LX/7lk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;)V
    .locals 0

    .prologue
    .line 1235670
    iput-object p1, p0, LX/7lk;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1235659
    return-void
.end method

.method public final onActivityDestroyed(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 1235665
    iget-object v0, p0, LX/7lk;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->v:Landroid/content/Context;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1235666
    iget-object v0, p0, LX/7lk;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->v:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    iget-object v1, p0, LX/7lk;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v1, v1, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->w:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {v0, v1}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 1235667
    iget-object v0, p0, LX/7lk;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v1, p0, LX/7lk;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v1, v1, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->u:Landroid/content/Context;

    .line 1235668
    iput-object v1, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->v:Landroid/content/Context;

    .line 1235669
    :cond_0
    return-void
.end method

.method public final onActivityPaused(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1235664
    return-void
.end method

.method public final onActivityResumed(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1235663
    return-void
.end method

.method public final onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1235662
    return-void
.end method

.method public final onActivityStarted(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1235661
    return-void
.end method

.method public final onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1235660
    return-void
.end method
