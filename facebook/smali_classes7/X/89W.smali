.class public final enum LX/89W;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/89W;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/89W;

.field public static final enum CREATIVE_CAM_BACK:LX/89W;

.field public static final enum CREATIVE_CAM_FRONT:LX/89W;

.field public static final enum CREATIVE_CAM_PICKER:LX/89W;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1305040
    new-instance v0, LX/89W;

    const-string v1, "CREATIVE_CAM_FRONT"

    const-string v2, "creative_cam_front"

    invoke-direct {v0, v1, v3, v2}, LX/89W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89W;->CREATIVE_CAM_FRONT:LX/89W;

    .line 1305041
    new-instance v0, LX/89W;

    const-string v1, "CREATIVE_CAM_BACK"

    const-string v2, "creative_cam_back"

    invoke-direct {v0, v1, v4, v2}, LX/89W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89W;->CREATIVE_CAM_BACK:LX/89W;

    .line 1305042
    new-instance v0, LX/89W;

    const-string v1, "CREATIVE_CAM_PICKER"

    const-string v2, "creative_cam_picker"

    invoke-direct {v0, v1, v5, v2}, LX/89W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89W;->CREATIVE_CAM_PICKER:LX/89W;

    .line 1305043
    const/4 v0, 0x3

    new-array v0, v0, [LX/89W;

    sget-object v1, LX/89W;->CREATIVE_CAM_FRONT:LX/89W;

    aput-object v1, v0, v3

    sget-object v1, LX/89W;->CREATIVE_CAM_BACK:LX/89W;

    aput-object v1, v0, v4

    sget-object v1, LX/89W;->CREATIVE_CAM_PICKER:LX/89W;

    aput-object v1, v0, v5

    sput-object v0, LX/89W;->$VALUES:[LX/89W;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1305044
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1305045
    iput-object p3, p0, LX/89W;->name:Ljava/lang/String;

    .line 1305046
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/89W;
    .locals 1

    .prologue
    .line 1305047
    const-class v0, LX/89W;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/89W;

    return-object v0
.end method

.method public static values()[LX/89W;
    .locals 1

    .prologue
    .line 1305048
    sget-object v0, LX/89W;->$VALUES:[LX/89W;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/89W;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1305049
    iget-object v0, p0, LX/89W;->name:Ljava/lang/String;

    return-object v0
.end method
