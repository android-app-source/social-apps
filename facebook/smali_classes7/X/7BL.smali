.class public LX/7BL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Z

.field public B:Z

.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;

.field public m:LX/7BK;

.field public n:J

.field public o:Z

.field public p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Z

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;

.field public u:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/api/model/GraphSearchQueryFragment;",
            ">;"
        }
    .end annotation
.end field

.field public v:I

.field public w:I

.field public x:Z

.field public y:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1178423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178424
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7BL;->x:Z

    return-void
.end method


# virtual methods
.method public final a(I)LX/7BL;
    .locals 0

    .prologue
    .line 1178425
    iput p1, p0, LX/7BL;->v:I

    .line 1178426
    return-object p0
.end method

.method public final a(J)LX/7BL;
    .locals 1

    .prologue
    .line 1178427
    iput-wide p1, p0, LX/7BL;->n:J

    .line 1178428
    return-object p0
.end method

.method public final a(LX/0Px;)LX/7BL;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/7BL;"
        }
    .end annotation

    .prologue
    .line 1178429
    iput-object p1, p0, LX/7BL;->s:LX/0Px;

    .line 1178430
    return-object p0
.end method

.method public final a(LX/7BK;)LX/7BL;
    .locals 0

    .prologue
    .line 1178431
    iput-object p1, p0, LX/7BL;->m:LX/7BK;

    .line 1178432
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/7BL;
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1178433
    iput-object p1, p0, LX/7BL;->c:Landroid/net/Uri;

    .line 1178434
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/7BL;
    .locals 0
    .param p1    # Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1178435
    iput-object p1, p0, LX/7BL;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1178436
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;)LX/7BL;
    .locals 0

    .prologue
    .line 1178437
    iput-object p1, p0, LX/7BL;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1178438
    return-object p0
.end method

.method public final a(Ljava/lang/Boolean;)LX/7BL;
    .locals 1

    .prologue
    .line 1178439
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/7BL;->q:Z

    .line 1178440
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/7BL;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1178441
    iput-object p1, p0, LX/7BL;->a:Ljava/lang/String;

    .line 1178442
    return-object p0
.end method

.method public final a(Ljava/util/List;)LX/7BL;
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/7BL;"
        }
    .end annotation

    .prologue
    .line 1178420
    iput-object p1, p0, LX/7BL;->r:Ljava/util/List;

    .line 1178421
    return-object p0
.end method

.method public final a(Ljava/util/Map;)LX/7BL;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "LX/7BL;"
        }
    .end annotation

    .prologue
    .line 1178443
    iput-object p1, p0, LX/7BL;->y:Ljava/util/Map;

    .line 1178444
    return-object p0
.end method

.method public final a(Z)LX/7BL;
    .locals 0

    .prologue
    .line 1178445
    iput-boolean p1, p0, LX/7BL;->x:Z

    .line 1178446
    return-object p0
.end method

.method public final a()Lcom/facebook/search/api/SearchTypeaheadResult;
    .locals 1

    .prologue
    .line 1178422
    new-instance v0, Lcom/facebook/search/api/SearchTypeaheadResult;

    invoke-direct {v0, p0}, Lcom/facebook/search/api/SearchTypeaheadResult;-><init>(LX/7BL;)V

    return-object v0
.end method

.method public final b(I)LX/7BL;
    .locals 0

    .prologue
    .line 1178447
    iput p1, p0, LX/7BL;->w:I

    .line 1178448
    return-object p0
.end method

.method public final b(LX/0Px;)LX/7BL;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/api/model/GraphSearchQueryFragment;",
            ">;)",
            "LX/7BL;"
        }
    .end annotation

    .prologue
    .line 1178392
    iput-object p1, p0, LX/7BL;->u:LX/0Px;

    .line 1178393
    return-object p0
.end method

.method public final b(Landroid/net/Uri;)LX/7BL;
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1178394
    iput-object p1, p0, LX/7BL;->d:Landroid/net/Uri;

    .line 1178395
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/7BL;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1178396
    iput-object p1, p0, LX/7BL;->g:Ljava/lang/String;

    .line 1178397
    return-object p0
.end method

.method public final b(Z)LX/7BL;
    .locals 0

    .prologue
    .line 1178398
    iput-boolean p1, p0, LX/7BL;->o:Z

    .line 1178399
    return-object p0
.end method

.method public final c(Landroid/net/Uri;)LX/7BL;
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1178400
    iput-object p1, p0, LX/7BL;->e:Landroid/net/Uri;

    .line 1178401
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/7BL;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1178402
    iput-object p1, p0, LX/7BL;->h:Ljava/lang/String;

    .line 1178403
    return-object p0
.end method

.method public final c(Z)LX/7BL;
    .locals 0

    .prologue
    .line 1178404
    iput-boolean p1, p0, LX/7BL;->A:Z

    .line 1178405
    return-object p0
.end method

.method public final d(Landroid/net/Uri;)LX/7BL;
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1178406
    iput-object p1, p0, LX/7BL;->f:Landroid/net/Uri;

    .line 1178407
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/7BL;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1178408
    iput-object p1, p0, LX/7BL;->i:Ljava/lang/String;

    .line 1178409
    return-object p0
.end method

.method public final d(Z)LX/7BL;
    .locals 0

    .prologue
    .line 1178410
    iput-boolean p1, p0, LX/7BL;->B:Z

    .line 1178411
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/7BL;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1178412
    iput-object p1, p0, LX/7BL;->j:Ljava/lang/String;

    .line 1178413
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/7BL;
    .locals 0

    .prologue
    .line 1178414
    iput-object p1, p0, LX/7BL;->l:Ljava/lang/String;

    .line 1178415
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/7BL;
    .locals 0

    .prologue
    .line 1178416
    iput-object p1, p0, LX/7BL;->z:Ljava/lang/String;

    .line 1178417
    return-object p0
.end method

.method public final h(Ljava/lang/String;)LX/7BL;
    .locals 0

    .prologue
    .line 1178418
    iput-object p1, p0, LX/7BL;->t:Ljava/lang/String;

    .line 1178419
    return-object p0
.end method
