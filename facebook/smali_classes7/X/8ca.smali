.class public final enum LX/8ca;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8ca;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8ca;

.field public static final enum FRIENDSHIP_STATUS_CHANGE:LX/8ca;

.field public static final enum PROFILE_VIEW:LX/8ca;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1374672
    new-instance v0, LX/8ca;

    const-string v1, "PROFILE_VIEW"

    invoke-direct {v0, v1, v2}, LX/8ca;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ca;->PROFILE_VIEW:LX/8ca;

    .line 1374673
    new-instance v0, LX/8ca;

    const-string v1, "FRIENDSHIP_STATUS_CHANGE"

    invoke-direct {v0, v1, v3}, LX/8ca;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ca;->FRIENDSHIP_STATUS_CHANGE:LX/8ca;

    .line 1374674
    const/4 v0, 0x2

    new-array v0, v0, [LX/8ca;

    sget-object v1, LX/8ca;->PROFILE_VIEW:LX/8ca;

    aput-object v1, v0, v2

    sget-object v1, LX/8ca;->FRIENDSHIP_STATUS_CHANGE:LX/8ca;

    aput-object v1, v0, v3

    sput-object v0, LX/8ca;->$VALUES:[LX/8ca;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1374675
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8ca;
    .locals 1

    .prologue
    .line 1374676
    const-class v0, LX/8ca;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8ca;

    return-object v0
.end method

.method public static values()[LX/8ca;
    .locals 1

    .prologue
    .line 1374677
    sget-object v0, LX/8ca;->$VALUES:[LX/8ca;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8ca;

    return-object v0
.end method
