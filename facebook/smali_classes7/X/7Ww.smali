.class public final LX/7Ww;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7Wz;


# direct methods
.method public constructor <init>(LX/7Wz;)V
    .locals 0

    .prologue
    .line 1216842
    iput-object p1, p0, LX/7Ww;->a:LX/7Wz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1216843
    iget-object v0, p0, LX/7Ww;->a:LX/7Wz;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/7Wz;->a$redex0(LX/7Wz;Ljava/lang/String;)V

    .line 1216844
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1216845
    check-cast p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

    .line 1216846
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1216847
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1216848
    iget-object v0, p0, LX/7Ww;->a:LX/7Wz;

    .line 1216849
    iget-object v1, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->i:Ljava/lang/String;

    move-object v1, v1

    .line 1216850
    invoke-static {v0, v1}, LX/7Wz;->a$redex0(LX/7Wz;Ljava/lang/String;)V

    .line 1216851
    :goto_0
    return-void

    .line 1216852
    :cond_0
    iget-object v0, p0, LX/7Ww;->a:LX/7Wz;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1216853
    new-instance v4, LX/31Y;

    iget-object v3, v0, LX/7Wz;->a:Landroid/content/Context;

    invoke-direct {v4, v3, v1}, LX/31Y;-><init>(Landroid/content/Context;I)V

    .line 1216854
    const-string v3, "Upsell API Success"

    invoke-virtual {v4, v3}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1216855
    const-string v3, "OK"

    new-instance v5, LX/7Wx;

    invoke-direct {v5, v0}, LX/7Wx;-><init>(LX/7Wz;)V

    invoke-virtual {v4, v3, v5}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1216856
    iget-object v3, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->j:Ljava/lang/String;

    move-object v3, v3

    .line 1216857
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1216858
    :goto_1
    const-string v3, ""

    .line 1216859
    if-eqz v1, :cond_2

    .line 1216860
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "Current Promo: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1216861
    iget-object v3, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->k:Ljava/lang/String;

    move-object v3, v3

    .line 1216862
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1216863
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "Recommended Promos Count: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1216864
    iget-object v3, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->c:LX/0Px;

    move-object v3, v3

    .line 1216865
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1216866
    iget-object v3, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->c:LX/0Px;

    move-object v5, v3

    .line 1216867
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move-object v3, v1

    :goto_3
    if-ge v2, v6, :cond_3

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    .line 1216868
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string p0, " * "

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1216869
    iget-object p0, v1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->b:Ljava/lang/String;

    move-object p0, p0

    .line 1216870
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string p0, " "

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1216871
    iget-object p0, v1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->f:Ljava/lang/String;

    move-object v1, p0

    .line 1216872
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1216873
    add-int/lit8 v2, v2, 0x1

    move-object v3, v1

    goto :goto_3

    :cond_1
    move v1, v2

    .line 1216874
    goto/16 :goto_1

    .line 1216875
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "No Current Promo\n\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 1216876
    :cond_3
    invoke-virtual {v4, v3}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1216877
    invoke-virtual {v4}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    .line 1216878
    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 1216879
    goto/16 :goto_0
.end method
