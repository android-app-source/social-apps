.class public final LX/7Bz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public e:Landroid/net/Uri;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

.field public j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public k:Z

.field public l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public n:D

.field public o:Ljava/lang/String;

.field public p:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1179706
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1179707
    iput-boolean v1, p0, LX/7Bz;->h:Z

    .line 1179708
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->NOT_VERIFIED:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    iput-object v0, p0, LX/7Bz;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1179709
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/7Bz;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1179710
    iput-boolean v1, p0, LX/7Bz;->k:Z

    .line 1179711
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, LX/7Bz;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)LX/7Bz;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/7Bz;"
        }
    .end annotation

    .prologue
    .line 1179699
    if-nez p1, :cond_0

    .line 1179700
    sget-object p1, LX/0Q7;->a:LX/0Px;

    move-object p1, p1

    .line 1179701
    :cond_0
    iput-object p1, p0, LX/7Bz;->m:LX/0Px;

    .line 1179702
    return-object p0
.end method

.method public final q()LX/7C0;
    .locals 2

    .prologue
    .line 1179703
    new-instance v0, LX/7C0;

    invoke-direct {v0, p0}, LX/7C0;-><init>(LX/7Bz;)V

    .line 1179704
    invoke-static {v0}, LX/7C0;->q(LX/7C0;)V

    .line 1179705
    return-object v0
.end method
