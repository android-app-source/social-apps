.class public LX/7iW;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field public a:F

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(ILandroid/view/View;)V
    .locals 2

    .prologue
    .line 1227668
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1227669
    iput-object p2, p0, LX/7iW;->b:Landroid/view/View;

    .line 1227670
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, LX/7iW;->setDuration(J)V

    .line 1227671
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 2

    .prologue
    .line 1227672
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 1227673
    iget-object v0, p0, LX/7iW;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1227674
    :goto_0
    iget-object v0, p0, LX/7iW;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1227675
    return-void

    .line 1227676
    :cond_0
    iget-object v0, p0, LX/7iW;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, LX/7iW;->a:F

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method public final willChangeBounds()Z
    .locals 1

    .prologue
    .line 1227677
    const/4 v0, 0x1

    return v0
.end method
