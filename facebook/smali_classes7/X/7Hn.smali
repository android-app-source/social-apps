.class public LX/7Hn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/7Hi",
            "<TT;>;>;>;"
        }
    .end annotation
.end field

.field public final b:LX/7Hi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Hi",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1191131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1191132
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7Hn;->a:Ljava/util/Map;

    .line 1191133
    new-instance v0, LX/7Hi;

    sget-object v1, LX/7B6;->a:LX/7B6;

    new-instance v2, LX/7Hc;

    .line 1191134
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1191135
    invoke-direct {v2, v3}, LX/7Hc;-><init>(LX/0Px;)V

    sget-object v3, LX/7HY;->UNSET:LX/7HY;

    sget-object v4, LX/7Ha;->UNSET:LX/7Ha;

    invoke-direct {v0, v1, v2, v3, v4}, LX/7Hi;-><init>(LX/7B6;LX/7Hc;LX/7HY;LX/7Ha;)V

    iput-object v0, p0, LX/7Hn;->b:LX/7Hi;

    .line 1191136
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/7Hi;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/7Hi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1191137
    if-nez p1, :cond_0

    const-string p1, ""

    .line 1191138
    :cond_0
    iget-object v0, p0, LX/7Hn;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/7Hn;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Hi;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1191139
    iget-object v0, p0, LX/7Hn;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1191140
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/7Hi;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/7Hi",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1191141
    if-nez p1, :cond_0

    const-string p1, ""

    .line 1191142
    :cond_0
    iget-object v0, p0, LX/7Hn;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1191143
    if-nez v0, :cond_1

    .line 1191144
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1191145
    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1191146
    iget-object v1, p0, LX/7Hn;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1191147
    :goto_0
    return-void

    .line 1191148
    :cond_1
    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1191149
    if-nez p1, :cond_0

    const-string p1, ""

    .line 1191150
    :cond_0
    iget-object v0, p0, LX/7Hn;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/7Hn;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
