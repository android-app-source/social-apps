.class public final LX/86u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1297998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1297999
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    invoke-direct {v0, p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1298000
    new-array v0, p1, [Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    return-object v0
.end method
