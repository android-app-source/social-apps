.class public final LX/7Pg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Oi;


# instance fields
.field public final synthetic a:LX/7Po;

.field private final b:LX/7Oi;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:LX/04n;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7Po;LX/7Oi;Ljava/lang/String;LX/04n;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/04n;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1203053
    iput-object p1, p0, LX/7Pg;->a:LX/7Po;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1203054
    iput-object p2, p0, LX/7Pg;->b:LX/7Oi;

    .line 1203055
    iput-object p3, p0, LX/7Pg;->c:Ljava/lang/String;

    .line 1203056
    iput-object p4, p0, LX/7Pg;->d:LX/04n;

    .line 1203057
    return-void
.end method


# virtual methods
.method public final a(LX/2WF;Ljava/io/OutputStream;)J
    .locals 14

    .prologue
    .line 1203058
    iget-wide v2, p1, LX/2WF;->a:J

    .line 1203059
    iget-wide v4, p1, LX/2WF;->b:J

    .line 1203060
    iget-object v0, p0, LX/7Pg;->d:LX/04n;

    if-eqz v0, :cond_0

    .line 1203061
    iget-object v0, p0, LX/7Pg;->d:LX/04n;

    invoke-interface {v0}, LX/04n;->b()V

    .line 1203062
    :cond_0
    iget-object v1, p0, LX/7Pg;->a:LX/7Po;

    iget-object v6, p0, LX/7Pg;->c:Ljava/lang/String;

    invoke-static/range {v1 .. v6}, LX/7Po;->a(LX/7Po;JJLjava/lang/String;)V

    .line 1203063
    :try_start_0
    new-instance v7, LX/7Pf;

    move-object v8, p0

    move-object/from16 v9, p2

    move-wide v10, v2

    move-wide v12, v4

    invoke-direct/range {v7 .. v13}, LX/7Pf;-><init>(LX/7Pg;Ljava/io/OutputStream;JJ)V

    .line 1203064
    iget-object v0, p0, LX/7Pg;->b:LX/7Oi;

    invoke-interface {v0, p1, v7}, LX/7Oi;->a(LX/2WF;Ljava/io/OutputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 1203065
    iget-object v2, p0, LX/7Pg;->d:LX/04n;

    if-eqz v2, :cond_1

    .line 1203066
    iget-object v2, p0, LX/7Pg;->d:LX/04n;

    invoke-interface {v2}, LX/04n;->c()V

    .line 1203067
    :cond_1
    iget-object v2, p0, LX/7Pg;->a:LX/7Po;

    iget-object v3, p0, LX/7Pg;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/7Po;->a$redex0(LX/7Po;Ljava/lang/String;)V

    return-wide v0

    .line 1203068
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/7Pg;->d:LX/04n;

    if-eqz v1, :cond_2

    .line 1203069
    iget-object v1, p0, LX/7Pg;->d:LX/04n;

    invoke-interface {v1}, LX/04n;->c()V

    .line 1203070
    :cond_2
    iget-object v1, p0, LX/7Pg;->a:LX/7Po;

    iget-object v2, p0, LX/7Pg;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/7Po;->a$redex0(LX/7Po;Ljava/lang/String;)V

    throw v0
.end method

.method public final a()LX/3Dd;
    .locals 8

    .prologue
    const-wide/16 v2, -0x1

    .line 1203071
    iget-object v1, p0, LX/7Pg;->a:LX/7Po;

    iget-object v6, p0, LX/7Pg;->c:Ljava/lang/String;

    move-wide v4, v2

    .line 1203072
    if-eqz v6, :cond_0

    .line 1203073
    new-instance v0, LX/2qQ;

    invoke-direct {v0, v2, v3, v4, v5}, LX/2qQ;-><init>(JJ)V

    invoke-static {v1, v6, v0}, LX/7Po;->a$redex0(LX/7Po;Ljava/lang/String;LX/1AD;)V

    .line 1203074
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/7Pg;->b:LX/7Oi;

    invoke-interface {v0}, LX/7Oi;->a()LX/3Dd;

    move-result-object v0

    .line 1203075
    iget-object v1, p0, LX/7Pg;->a:LX/7Po;

    const-wide/16 v2, -0x1

    const-wide/16 v4, -0x1

    iget-object v6, p0, LX/7Pg;->c:Ljava/lang/String;

    .line 1203076
    if-eqz v6, :cond_1

    .line 1203077
    new-instance v7, LX/2qR;

    invoke-direct {v7, v2, v3, v4, v5}, LX/2qR;-><init>(JJ)V

    invoke-static {v1, v6, v7}, LX/7Po;->a$redex0(LX/7Po;Ljava/lang/String;LX/1AD;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1203078
    :cond_1
    iget-object v1, p0, LX/7Pg;->a:LX/7Po;

    iget-object v2, p0, LX/7Pg;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/7Po;->a$redex0(LX/7Po;Ljava/lang/String;)V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/7Pg;->a:LX/7Po;

    iget-object v2, p0, LX/7Pg;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/7Po;->a$redex0(LX/7Po;Ljava/lang/String;)V

    throw v0
.end method
