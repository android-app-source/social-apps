.class public LX/6qD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/6qD;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1150237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150238
    return-void
.end method

.method private static a(LX/0lF;Ljava/lang/String;)LX/0lF;
    .locals 4

    .prologue
    .line 1150188
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v1, "field %s was not found in parent %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    return-object v0
.end method

.method public static a(LX/0QB;)LX/6qD;
    .locals 3

    .prologue
    .line 1150225
    sget-object v0, LX/6qD;->a:LX/6qD;

    if-nez v0, :cond_1

    .line 1150226
    const-class v1, LX/6qD;

    monitor-enter v1

    .line 1150227
    :try_start_0
    sget-object v0, LX/6qD;->a:LX/6qD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1150228
    if-eqz v2, :cond_0

    .line 1150229
    :try_start_1
    new-instance v0, LX/6qD;

    invoke-direct {v0}, LX/6qD;-><init>()V

    .line 1150230
    move-object v0, v0

    .line 1150231
    sput-object v0, LX/6qD;->a:LX/6qD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1150232
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1150233
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1150234
    :cond_1
    sget-object v0, LX/6qD;->a:LX/6qD;

    return-object v0

    .line 1150235
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1150236
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0lF;Ljava/lang/String;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1150239
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1150240
    invoke-static {p0, p1}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1150241
    const-string v3, "id"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1150242
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1150208
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1150209
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "q"

    const-string v3, "viewer() {  peer_to_peer_payments {    peer_to_peer_payment_pin {      id,      payments_protected,      protected_thread_profiles {        id      },      unprotected_thread_profiles {        id      }    }  }}"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150210
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "fetch_payment_pin_status"

    .line 1150211
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1150212
    move-object v1, v1

    .line 1150213
    const-string v2, "GET"

    .line 1150214
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1150215
    move-object v1, v1

    .line 1150216
    const-string v2, "graphql"

    .line 1150217
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1150218
    move-object v1, v1

    .line 1150219
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1150220
    move-object v0, v1

    .line 1150221
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1150222
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1150223
    move-object v0, v0

    .line 1150224
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1150189
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1150190
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "viewer"

    invoke-static {v0, v1}, LX/6qD;->a(LX/0lF;Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1150191
    const-string v1, "peer_to_peer_payments"

    invoke-static {v0, v1}, LX/6qD;->a(LX/0lF;Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1150192
    const-string v1, "peer_to_peer_payment_pin"

    invoke-static {v0, v1}, LX/6qD;->a(LX/0lF;Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1150193
    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 1150194
    if-nez v1, :cond_0

    .line 1150195
    sget-object v0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->a:Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;

    .line 1150196
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    .line 1150197
    new-instance p0, LX/6or;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, LX/6or;-><init>(Ljava/lang/String;)V

    move-object v1, p0

    .line 1150198
    const-string v2, "payments_protected"

    invoke-static {v0, v2}, LX/6qD;->a(LX/0lF;Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->F()Z

    move-result v2

    .line 1150199
    iput-boolean v2, v1, LX/6or;->b:Z

    .line 1150200
    move-object v1, v1

    .line 1150201
    const-string v2, "protected_thread_profiles"

    invoke-static {v0, v2}, LX/6qD;->b(LX/0lF;Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 1150202
    iput-object v2, v1, LX/6or;->c:LX/0Px;

    .line 1150203
    move-object v1, v1

    .line 1150204
    const-string v2, "unprotected_thread_profiles"

    invoke-static {v0, v2}, LX/6qD;->b(LX/0lF;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 1150205
    iput-object v0, v1, LX/6or;->d:LX/0Px;

    .line 1150206
    move-object v0, v1

    .line 1150207
    invoke-virtual {v0}, LX/6or;->a()Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;

    move-result-object v0

    goto :goto_0
.end method
