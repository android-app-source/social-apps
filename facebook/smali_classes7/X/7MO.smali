.class public final LX/7MO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1198671
    iput-object p1, p0, LX/7MO;->b:Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;

    iput-object p2, p0, LX/7MO;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0xe07136

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1198663
    iget-object v2, p0, LX/7MO;->b:Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;

    iget-object v3, p0, LX/7MO;->b:Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;

    iget-boolean v3, v3, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->p:Z

    if-nez v3, :cond_0

    :goto_0
    invoke-static {v2, v0}, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->setButtonState(Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;Z)V

    .line 1198664
    iget-object v0, p0, LX/7MO;->b:Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;

    iget-boolean v0, v0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->p:Z

    if-eqz v0, :cond_1

    .line 1198665
    iget-object v0, p0, LX/7MO;->b:Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->a:LX/1Sj;

    iget-object v2, p0, LX/7MO;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const-string v3, "toggle_button"

    const-string v4, "video_channel"

    invoke-virtual {v0, v2, v3, v4}, LX/1Sj;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    .line 1198666
    iget-object v0, p0, LX/7MO;->b:Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->b:LX/79m;

    invoke-virtual {v0}, LX/79m;->a()V

    .line 1198667
    :goto_1
    const v0, -0xddaa0a3

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1198668
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1198669
    :cond_1
    iget-object v0, p0, LX/7MO;->b:Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->a:LX/1Sj;

    iget-object v2, p0, LX/7MO;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const-string v3, "toggle_button"

    const-string v4, "video_channel"

    invoke-virtual {v0, v2, v3, v4}, LX/1Sj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    .line 1198670
    iget-object v0, p0, LX/7MO;->b:Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->b:LX/79m;

    invoke-virtual {v0, p1}, LX/79m;->a(Landroid/view/View;)V

    goto :goto_1
.end method
