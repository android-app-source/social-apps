.class public final enum LX/6zJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6zJ;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6zJ;

.field public static final enum CREDIT:LX/6zJ;

.field public static final enum DEBIT:LX/6zJ;

.field public static final enum UNKNOWN:LX/6zJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1161014
    new-instance v0, LX/6zJ;

    const-string v1, "DEBIT"

    invoke-direct {v0, v1, v2}, LX/6zJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6zJ;->DEBIT:LX/6zJ;

    .line 1161015
    new-instance v0, LX/6zJ;

    const-string v1, "CREDIT"

    invoke-direct {v0, v1, v3}, LX/6zJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6zJ;->CREDIT:LX/6zJ;

    .line 1161016
    new-instance v0, LX/6zJ;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/6zJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6zJ;->UNKNOWN:LX/6zJ;

    .line 1161017
    const/4 v0, 0x3

    new-array v0, v0, [LX/6zJ;

    sget-object v1, LX/6zJ;->DEBIT:LX/6zJ;

    aput-object v1, v0, v2

    sget-object v1, LX/6zJ;->CREDIT:LX/6zJ;

    aput-object v1, v0, v3

    sget-object v1, LX/6zJ;->UNKNOWN:LX/6zJ;

    aput-object v1, v0, v4

    sput-object v0, LX/6zJ;->$VALUES:[LX/6zJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1161018
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forValue(Ljava/lang/String;)LX/6zJ;
    .locals 2

    .prologue
    .line 1161013
    const-class v0, LX/6zJ;

    sget-object v1, LX/6zJ;->UNKNOWN:LX/6zJ;

    invoke-static {v0, p0, v1}, LX/47c;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6zJ;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6zJ;
    .locals 1

    .prologue
    .line 1161012
    const-class v0, LX/6zJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6zJ;

    return-object v0
.end method

.method public static values()[LX/6zJ;
    .locals 1

    .prologue
    .line 1161011
    sget-object v0, LX/6zJ;->$VALUES:[LX/6zJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6zJ;

    return-object v0
.end method
