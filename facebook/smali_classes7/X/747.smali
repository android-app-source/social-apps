.class public LX/747;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1167500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1167501
    iput-object p1, p0, LX/747;->a:LX/0Zb;

    .line 1167502
    return-void
.end method

.method public static a(LX/746;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1167503
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p0}, LX/746;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/747;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 1167504
    const-string v0, "composer"

    .line 1167505
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1167506
    iget-object v0, p0, LX/747;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1167507
    iget-object v0, p0, LX/747;->b:Ljava/lang/String;

    .line 1167508
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1167509
    :cond_0
    iget-object v0, p0, LX/747;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1167510
    return-void
.end method

.method public static b(LX/0QB;)LX/747;
    .locals 2

    .prologue
    .line 1167511
    new-instance v1, LX/747;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/747;-><init>(LX/0Zb;)V

    .line 1167512
    return-object v1
.end method
