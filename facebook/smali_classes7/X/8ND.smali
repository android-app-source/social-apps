.class public LX/8ND;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1336337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/8ND;
    .locals 1

    .prologue
    .line 1336338
    new-instance v0, LX/8ND;

    invoke-direct {v0}, LX/8ND;-><init>()V

    .line 1336339
    move-object v0, v0

    .line 1336340
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1336341
    check-cast p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    .line 1336342
    iget-wide v5, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    move-wide v0, v5

    .line 1336343
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1336344
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1336345
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->j:Ljava/lang/String;

    move-object v1, v1

    .line 1336346
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1336347
    iget-boolean v2, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->k:Z

    move v2, v2

    .line 1336348
    if-nez v2, :cond_d

    .line 1336349
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "caption"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336350
    :cond_0
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->f()Ljava/lang/String;

    move-result-object v1

    .line 1336351
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1336352
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "profile_id"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336353
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->h()Ljava/lang/String;

    move-result-object v1

    .line 1336354
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1336355
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "place"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336356
    :cond_2
    iget-object v2, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->e:Ljava/lang/String;

    move-object v2, v2

    .line 1336357
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1336358
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "text_only_place"

    invoke-direct {v1, v3, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336359
    :cond_3
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "checkin_entry_point"

    .line 1336360
    iget-boolean v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->f:Z

    move v3, v3

    .line 1336361
    invoke-static {v3}, LX/5RB;->a(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336362
    iget-boolean v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->u:Z

    move v1, v1

    .line 1336363
    if-eqz v1, :cond_5

    .line 1336364
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->l:LX/0Px;

    move-object v1, v1

    .line 1336365
    if-eqz v1, :cond_5

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1336366
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "tags"

    .line 1336367
    if-eqz v1, :cond_4

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1336368
    :cond_4
    const/4 v4, 0x0

    .line 1336369
    :goto_2
    move-object v1, v4

    .line 1336370
    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336371
    :cond_5
    iget v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->E:I

    move v1, v1

    .line 1336372
    const/4 v2, -0x1

    if-eq v1, v2, :cond_6

    .line 1336373
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "publish_order"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336374
    :cond_6
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->q:Ljava/lang/String;

    move-object v1, v1

    .line 1336375
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1336376
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "idempotence_token"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336377
    :cond_7
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->n:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-object v1, v1

    .line 1336378
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1336379
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->o:Ljava/lang/String;

    move-object v1, v1

    .line 1336380
    if-eqz v1, :cond_8

    .line 1336381
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "referenced_sticker_id"

    .line 1336382
    iget-object v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->o:Ljava/lang/String;

    move-object v3, v3

    .line 1336383
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336384
    :cond_8
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p:Ljava/lang/String;

    move-object v1, v1

    .line 1336385
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1336386
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "qn"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336387
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "composer_session_id"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336388
    :cond_9
    iget v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->r:I

    move v1, v1

    .line 1336389
    if-eqz v1, :cond_a

    .line 1336390
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "orientation"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336391
    :cond_a
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->v:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object v1, v1

    .line 1336392
    if-eqz v1, :cond_b

    .line 1336393
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "proxied_app_id"

    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336394
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "proxied_app_name"

    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336395
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "android_key_hash"

    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336396
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "user_selected_tags"

    .line 1336397
    iget-boolean v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->w:Z

    move v3, v3

    .line 1336398
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336399
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "user_selected_place"

    .line 1336400
    iget-boolean v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->x:Z

    move v3, v3

    .line 1336401
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336402
    :cond_b
    iget-wide v5, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    move-wide v2, v5

    .line 1336403
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 1336404
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "update-photo-info"

    .line 1336405
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1336406
    move-object v2, v2

    .line 1336407
    const-string v3, "POST"

    .line 1336408
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 1336409
    move-object v2, v2

    .line 1336410
    iput-object v1, v2, LX/14O;->d:Ljava/lang/String;

    .line 1336411
    move-object v1, v2

    .line 1336412
    sget-object v2, LX/14S;->STRING:LX/14S;

    .line 1336413
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1336414
    move-object v1, v1

    .line 1336415
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1336416
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1336417
    move-object v0, v1

    .line 1336418
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1336419
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1336420
    :cond_d
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "name"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    :cond_e
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x2c

    invoke-static {v5}, LX/0PO;->on(C)LX/0PO;

    move-result-object v5

    invoke-virtual {v5}, LX/0PO;->skipNulls()LX/0PO;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1336421
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1336422
    invoke-virtual {p2}, LX/1pN;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
