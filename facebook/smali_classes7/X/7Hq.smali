.class public LX/7Hq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0c5;"
    }
.end annotation


# instance fields
.field public final a:LX/7Hn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Hn",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:LX/7Hn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Hn",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final c:LX/7Hn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Hn",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final d:LX/7Hn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Hn",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7Hn;LX/7Hn;LX/7Hn;LX/7Hn;)V
    .locals 0

    .prologue
    .line 1191198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1191199
    iput-object p1, p0, LX/7Hq;->a:LX/7Hn;

    .line 1191200
    iput-object p2, p0, LX/7Hq;->b:LX/7Hn;

    .line 1191201
    iput-object p3, p0, LX/7Hq;->c:LX/7Hn;

    .line 1191202
    iput-object p4, p0, LX/7Hq;->d:LX/7Hn;

    .line 1191203
    return-void
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    .line 1191204
    iget-object v0, p0, LX/7Hq;->a:LX/7Hn;

    if-eqz v0, :cond_0

    .line 1191205
    iget-object v0, p0, LX/7Hq;->a:LX/7Hn;

    invoke-virtual {v0}, LX/7Hn;->a()V

    .line 1191206
    :cond_0
    iget-object v0, p0, LX/7Hq;->b:LX/7Hn;

    if-eqz v0, :cond_1

    .line 1191207
    iget-object v0, p0, LX/7Hq;->b:LX/7Hn;

    invoke-virtual {v0}, LX/7Hn;->a()V

    .line 1191208
    :cond_1
    iget-object v0, p0, LX/7Hq;->c:LX/7Hn;

    if-eqz v0, :cond_2

    .line 1191209
    iget-object v0, p0, LX/7Hq;->c:LX/7Hn;

    invoke-virtual {v0}, LX/7Hn;->a()V

    .line 1191210
    :cond_2
    iget-object v0, p0, LX/7Hq;->d:LX/7Hn;

    if-eqz v0, :cond_3

    .line 1191211
    iget-object v0, p0, LX/7Hq;->d:LX/7Hn;

    invoke-virtual {v0}, LX/7Hn;->a()V

    .line 1191212
    :cond_3
    return-void
.end method
