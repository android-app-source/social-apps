.class public LX/8QP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/4YH;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1343030
    new-instance v0, LX/4YH;

    invoke-direct {v0}, LX/4YH;-><init>()V

    invoke-direct {p0, v0}, LX/8QP;-><init>(LX/4YH;)V

    .line 1343031
    return-void
.end method

.method private constructor <init>(LX/4YH;)V
    .locals 1

    .prologue
    .line 1343025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1343026
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8QP;->b:Ljava/util/ArrayList;

    .line 1343027
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8QP;->c:Ljava/util/ArrayList;

    .line 1343028
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4YH;

    iput-object v0, p0, LX/8QP;->a:LX/4YH;

    .line 1343029
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QP;
    .locals 2

    .prologue
    .line 1343024
    new-instance v0, LX/8QP;

    invoke-static {p0}, LX/4YH;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/4YH;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8QP;-><init>(LX/4YH;)V

    return-object v0
.end method


# virtual methods
.method public final a()LX/8QP;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1342959
    iget-object v0, p0, LX/8QP;->a:LX/4YH;

    iget-object v0, v0, LX/4YH;->k:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8QP;->a:LX/4YH;

    iget-object v0, v0, LX/4YH;->k:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gt v0, v2, :cond_1

    .line 1342960
    :cond_0
    :goto_0
    return-object p0

    .line 1342961
    :cond_1
    iget-object v1, p0, LX/8QP;->a:LX/4YH;

    iget-object v0, p0, LX/8QP;->a:LX/4YH;

    iget-object v0, v0, LX/4YH;->k:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    iput-object v0, v1, LX/4YH;->b:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    goto :goto_0
.end method

.method public final a(LX/0Px;)LX/8QP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)",
            "LX/8QP;"
        }
    .end annotation

    .prologue
    .line 1343021
    iget-object v0, p0, LX/8QP;->a:LX/4YH;

    .line 1343022
    iput-object p1, v0, LX/4YH;->g:LX/0Px;

    .line 1343023
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLImage;)LX/8QP;
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLImage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1343018
    iget-object v0, p0, LX/8QP;->a:LX/4YH;

    .line 1343019
    iput-object p1, v0, LX/4YH;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1343020
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/8QP;
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1343015
    iget-object v0, p0, LX/8QP;->a:LX/4YH;

    .line 1343016
    iput-object p1, v0, LX/4YH;->j:Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    .line 1343017
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/8QP;
    .locals 1

    .prologue
    .line 1343013
    iget-object v0, p0, LX/8QP;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1343014
    return-object p0
.end method

.method public final b(LX/0Px;)LX/8QP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)",
            "LX/8QP;"
        }
    .end annotation

    .prologue
    .line 1343010
    iget-object v0, p0, LX/8QP;->a:LX/4YH;

    .line 1343011
    iput-object p1, v0, LX/4YH;->c:LX/0Px;

    .line 1343012
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/8QP;
    .locals 1

    .prologue
    .line 1343008
    iget-object v0, p0, LX/8QP;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1343009
    return-object p0
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 5

    .prologue
    .line 1342962
    iget-object v0, p0, LX/8QP;->a:LX/4YH;

    invoke-virtual {v0}, LX/4YH;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    .line 1342963
    invoke-static {v1}, LX/2cA;->g(LX/1oS;)Z

    move-result v2

    .line 1342964
    iget-object v0, p0, LX/8QP;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8QP;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez v2, :cond_0

    move-object v0, v1

    .line 1342965
    :goto_0
    return-object v0

    .line 1342966
    :cond_0
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/facebook/privacy/model/PrivacyParameter;

    invoke-virtual {v0, v3, v4}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/PrivacyParameter;

    .line 1342967
    if-nez v0, :cond_1

    move-object v0, v1

    .line 1342968
    goto :goto_0

    .line 1342969
    :cond_1
    new-instance v1, LX/8QS;

    invoke-direct {v1}, LX/8QS;-><init>()V

    .line 1342970
    iget-object v3, v0, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    iput-object v3, v1, LX/8QS;->a:Ljava/lang/String;

    .line 1342971
    iget-object v3, v0, Lcom/facebook/privacy/model/PrivacyParameter;->allow:Ljava/lang/String;

    iput-object v3, v1, LX/8QS;->b:Ljava/lang/String;

    .line 1342972
    iget-object v3, v0, Lcom/facebook/privacy/model/PrivacyParameter;->deny:Ljava/lang/String;

    iput-object v3, v1, LX/8QS;->c:Ljava/lang/String;

    .line 1342973
    iget-object v3, v0, Lcom/facebook/privacy/model/PrivacyParameter;->friends:Ljava/lang/String;

    iput-object v3, v1, LX/8QS;->d:Ljava/lang/String;

    .line 1342974
    iget-object v3, v0, Lcom/facebook/privacy/model/PrivacyParameter;->settings:Lcom/facebook/privacy/model/PrivacyParameter$Settings;

    if-eqz v3, :cond_2

    .line 1342975
    iget-object v3, v0, Lcom/facebook/privacy/model/PrivacyParameter;->settings:Lcom/facebook/privacy/model/PrivacyParameter$Settings;

    iget-boolean v3, v3, Lcom/facebook/privacy/model/PrivacyParameter$Settings;->noTagExpansion:Z

    iput-boolean v3, v1, LX/8QS;->e:Z

    .line 1342976
    :cond_2
    move-object v0, v1

    .line 1342977
    iget-object v1, p0, LX/8QP;->b:Ljava/util/ArrayList;

    .line 1342978
    iget-object v3, v0, LX/8QS;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1342979
    move-object v0, v0

    .line 1342980
    iget-object v1, p0, LX/8QP;->c:Ljava/util/ArrayList;

    .line 1342981
    iget-object v3, v0, LX/8QS;->g:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1342982
    move-object v0, v0

    .line 1342983
    iput-boolean v2, v0, LX/8QS;->e:Z

    .line 1342984
    move-object v0, v0

    .line 1342985
    iget-object v1, v0, LX/8QS;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    .line 1342986
    sget-object v1, LX/8QR;->ALL_FRIENDS:LX/8QR;

    invoke-virtual {v1}, LX/8QR;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, LX/8QS;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, LX/8QR;->FRIENDS_OF_FRIENDS:LX/8QR;

    invoke-virtual {v1}, LX/8QR;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, LX/8QS;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1342987
    :cond_3
    const-string v1, ""

    iput-object v1, v0, LX/8QS;->b:Ljava/lang/String;

    .line 1342988
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1342989
    iget-object v1, v0, LX/8QS;->b:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1342990
    iget-object v1, v0, LX/8QS;->b:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1342991
    const-string v1, ","

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1342992
    :cond_5
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    iget-object v1, v0, LX/8QS;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_7

    .line 1342993
    if-eqz v2, :cond_6

    .line 1342994
    const-string v1, ","

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1342995
    :cond_6
    iget-object v1, v0, LX/8QS;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1342996
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1342997
    :cond_7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8QS;->b:Ljava/lang/String;

    .line 1342998
    :cond_8
    :goto_2
    iget-object v1, v0, LX/8QS;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    .line 1342999
    const-string v1, ","

    invoke-static {v1}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v1

    iget-object v2, v0, LX/8QS;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8QS;->c:Ljava/lang/String;

    .line 1343000
    :cond_9
    sget-object v1, LX/8QT;->CUSTOM:LX/8QT;

    invoke-virtual {v1}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8QS;->a:Ljava/lang/String;

    .line 1343001
    new-instance v1, Lcom/facebook/privacy/model/PrivacyParameter;

    invoke-direct {v1, v0}, Lcom/facebook/privacy/model/PrivacyParameter;-><init>(LX/8QS;)V

    move-object v0, v1

    .line 1343002
    iget-object v1, p0, LX/8QP;->a:LX/4YH;

    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1343003
    iput-object v0, v1, LX/4YH;->h:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1343004
    iget-object v0, p0, LX/8QP;->a:LX/4YH;

    invoke-virtual {v0}, LX/4YH;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    goto/16 :goto_0

    .line 1343005
    :catch_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not construct privacy."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1343006
    :cond_a
    sget-object v1, LX/8QT;->ALL_FRIENDS:LX/8QT;

    invoke-virtual {v1}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, LX/8QS;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    sget-object v1, LX/8QT;->FRIENDS_OF_FRIENDS:LX/8QT;

    invoke-virtual {v1}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, LX/8QS;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1343007
    :cond_b
    iget-object v1, v0, LX/8QS;->a:Ljava/lang/String;

    iput-object v1, v0, LX/8QS;->b:Ljava/lang/String;

    goto :goto_2
.end method

.method public final c(LX/0Px;)LX/8QP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;",
            ">;)",
            "LX/8QP;"
        }
    .end annotation

    .prologue
    .line 1342956
    iget-object v0, p0, LX/8QP;->a:LX/4YH;

    .line 1342957
    iput-object p1, v0, LX/4YH;->k:LX/0Px;

    .line 1342958
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/8QP;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1342953
    iget-object v0, p0, LX/8QP;->a:LX/4YH;

    .line 1342954
    iput-object p1, v0, LX/4YH;->h:Ljava/lang/String;

    .line 1342955
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/8QP;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1342950
    iget-object v0, p0, LX/8QP;->a:LX/4YH;

    .line 1342951
    iput-object p1, v0, LX/4YH;->i:Ljava/lang/String;

    .line 1342952
    return-object p0
.end method
