.class public LX/8DI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/nux/status/UpdateNuxStatusParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1312457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1312458
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1312459
    check-cast p1, Lcom/facebook/nux/status/UpdateNuxStatusParams;

    .line 1312460
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1312461
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "JSON"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1312462
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "nux_id"

    .line 1312463
    iget-object v2, p1, Lcom/facebook/nux/status/UpdateNuxStatusParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1312464
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1312465
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "step"

    .line 1312466
    iget-object v2, p1, Lcom/facebook/nux/status/UpdateNuxStatusParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1312467
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1312468
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "status"

    .line 1312469
    iget-object v2, p1, Lcom/facebook/nux/status/UpdateNuxStatusParams;->d:LX/8DK;

    move-object v2, v2

    .line 1312470
    sget-object v3, LX/8DK;->COMPLETE:LX/8DK;

    if-ne v2, v3, :cond_1

    .line 1312471
    const-string v3, "COMPLETE"

    .line 1312472
    :goto_0
    move-object v2, v3

    .line 1312473
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1312474
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "extra_data"

    .line 1312475
    iget-object v2, p1, Lcom/facebook/nux/status/UpdateNuxStatusParams;->e:LX/0P1;

    move-object v2, v2

    .line 1312476
    new-instance p0, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {p0, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 1312477
    invoke-virtual {v2}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v3

    invoke-virtual {v3}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1312478
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v5, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_1

    .line 1312479
    :cond_0
    invoke-virtual {p0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 1312480
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1312481
    new-instance v0, LX/14N;

    const-string v1, "updateNuxStatus"

    const-string v2, "GET"

    const-string v3, "method/user.updateNuxStatus"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    :cond_1
    const-string v3, "SKIPPED"

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1312482
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0lF;->a(Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
