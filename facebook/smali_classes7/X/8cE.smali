.class public LX/8cE;
.super LX/2TZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2TZ",
        "<",
        "LX/8cK;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 1374107
    invoke-direct {p0, p1}, LX/2TZ;-><init>(Landroid/database/Cursor;)V

    .line 1374108
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1374109
    const-wide/16 v6, 0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1374110
    invoke-static {}, LX/8cK;->q()LX/8cJ;

    move-result-object v0

    sget-object v3, LX/7Br;->b:LX/0U1;

    .line 1374111
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374112
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374113
    iput-object v3, v0, LX/8cJ;->c:Ljava/lang/String;

    .line 1374114
    move-object v0, v0

    .line 1374115
    sget-object v3, LX/7Br;->c:LX/0U1;

    .line 1374116
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374117
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374118
    iput-object v3, v0, LX/8cJ;->d:Ljava/lang/String;

    .line 1374119
    move-object v0, v0

    .line 1374120
    sget-object v3, LX/7Br;->d:LX/0U1;

    .line 1374121
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374122
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374123
    iput-object v3, v0, LX/8cJ;->e:Ljava/lang/String;

    .line 1374124
    move-object v0, v0

    .line 1374125
    sget-object v3, LX/7Br;->e:LX/0U1;

    .line 1374126
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374127
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374128
    iput-object v3, v0, LX/8cJ;->f:Ljava/lang/String;

    .line 1374129
    move-object v0, v0

    .line 1374130
    sget-object v3, LX/7Br;->f:LX/0U1;

    .line 1374131
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374132
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374133
    iput-object v3, v0, LX/8cJ;->g:Ljava/lang/String;

    .line 1374134
    move-object v0, v0

    .line 1374135
    sget-object v3, LX/7Br;->g:LX/0U1;

    .line 1374136
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374137
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374138
    iput-object v3, v0, LX/8cJ;->h:Ljava/lang/String;

    .line 1374139
    move-object v0, v0

    .line 1374140
    sget-object v3, LX/7Br;->h:LX/0U1;

    .line 1374141
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374142
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374143
    iput-object v3, v0, LX/8cJ;->i:Ljava/lang/String;

    .line 1374144
    move-object v0, v0

    .line 1374145
    sget-object v3, LX/7Br;->i:LX/0U1;

    .line 1374146
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374147
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    .line 1374148
    iput-object v3, v0, LX/8cJ;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1374149
    move-object v3, v0

    .line 1374150
    sget-object v0, LX/7Br;->j:LX/0U1;

    .line 1374151
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 1374152
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v0, v1

    .line 1374153
    :goto_0
    iput-boolean v0, v3, LX/8cJ;->l:Z

    .line 1374154
    move-object v0, v3

    .line 1374155
    sget-object v3, LX/7Br;->k:LX/0U1;

    .line 1374156
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374157
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    .line 1374158
    iput-object v3, v0, LX/8cJ;->m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1374159
    move-object v0, v0

    .line 1374160
    sget-object v3, LX/7Br;->l:LX/0U1;

    .line 1374161
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374162
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    .line 1374163
    iput-wide v4, v0, LX/8cJ;->j:D

    .line 1374164
    move-object v3, v0

    .line 1374165
    sget-object v0, LX/7Br;->m:LX/0U1;

    .line 1374166
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 1374167
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    move v0, v1

    .line 1374168
    :goto_1
    iput-boolean v0, v3, LX/8cJ;->a:Z

    .line 1374169
    move-object v0, v3

    .line 1374170
    sget-object v3, LX/7Br;->n:LX/0U1;

    .line 1374171
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374172
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v3

    .line 1374173
    iput-object v3, v0, LX/8cJ;->b:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1374174
    move-object v0, v0

    .line 1374175
    sget-object v3, LX/7Br;->o:LX/0U1;

    .line 1374176
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374177
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1374178
    iput-object v3, v0, LX/8cJ;->o:Ljava/lang/String;

    .line 1374179
    move-object v0, v0

    .line 1374180
    sget-object v3, LX/7Br;->p:LX/0U1;

    .line 1374181
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1374182
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    .line 1374183
    :goto_2
    iput-boolean v1, v0, LX/8cJ;->p:Z

    .line 1374184
    move-object v0, v0

    .line 1374185
    invoke-virtual {v0}, LX/8cJ;->q()LX/8cK;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method
