.class public final LX/7QG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1203932
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1203933
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1203934
    :goto_0
    return v1

    .line 1203935
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1203936
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 1203937
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1203938
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1203939
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1203940
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1203941
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1203942
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 1203943
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1203944
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1203945
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_a

    .line 1203946
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1203947
    :goto_3
    move v2, v3

    .line 1203948
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1203949
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 1203950
    goto :goto_1

    .line 1203951
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1203952
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1203953
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 1203954
    :cond_5
    const-string v8, "is_default"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1203955
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v2

    move v5, v2

    move v2, v4

    .line 1203956
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_8

    .line 1203957
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1203958
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1203959
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_6

    if-eqz v7, :cond_6

    .line 1203960
    const-string v8, "autoplay_setting"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1203961
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto :goto_4

    .line 1203962
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 1203963
    :cond_8
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1203964
    invoke-virtual {p1, v3, v6}, LX/186;->b(II)V

    .line 1203965
    if-eqz v2, :cond_9

    .line 1203966
    invoke-virtual {p1, v4, v5}, LX/186;->a(IZ)V

    .line 1203967
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_a
    move v2, v3

    move v5, v3

    move v6, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1203968
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1203969
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1203970
    if-eqz v0, :cond_3

    .line 1203971
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1203972
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1203973
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1203974
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 p3, 0x0

    .line 1203975
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1203976
    invoke-virtual {p0, v2, p3}, LX/15i;->g(II)I

    move-result p1

    .line 1203977
    if-eqz p1, :cond_0

    .line 1203978
    const-string p1, "autoplay_setting"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1203979
    invoke-virtual {p0, v2, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1203980
    :cond_0
    const/4 p1, 0x1

    invoke-virtual {p0, v2, p1}, LX/15i;->b(II)Z

    move-result p1

    .line 1203981
    if-eqz p1, :cond_1

    .line 1203982
    const-string p3, "is_default"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1203983
    invoke-virtual {p2, p1}, LX/0nX;->a(Z)V

    .line 1203984
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1203985
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1203986
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1203987
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1203988
    return-void
.end method
