.class public final enum LX/7gi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gi;

.field public static final enum PHOTO:LX/7gi;

.field public static final enum VIDEO:LX/7gi;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1224779
    new-instance v0, LX/7gi;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v2}, LX/7gi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gi;->PHOTO:LX/7gi;

    .line 1224780
    new-instance v0, LX/7gi;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/7gi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gi;->VIDEO:LX/7gi;

    .line 1224781
    const/4 v0, 0x2

    new-array v0, v0, [LX/7gi;

    sget-object v1, LX/7gi;->PHOTO:LX/7gi;

    aput-object v1, v0, v2

    sget-object v1, LX/7gi;->VIDEO:LX/7gi;

    aput-object v1, v0, v3

    sput-object v0, LX/7gi;->$VALUES:[LX/7gi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1224782
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gi;
    .locals 1

    .prologue
    .line 1224783
    const-class v0, LX/7gi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gi;

    return-object v0
.end method

.method public static values()[LX/7gi;
    .locals 1

    .prologue
    .line 1224784
    sget-object v0, LX/7gi;->$VALUES:[LX/7gi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gi;

    return-object v0
.end method
