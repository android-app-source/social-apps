.class public LX/74B;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Z


# instance fields
.field private final c:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1167622
    const-class v0, LX/74B;

    sput-object v0, LX/74B;->a:Ljava/lang/Class;

    .line 1167623
    const-string v0, "ResumableUploadLogger"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, LX/74B;->b:Z

    return-void
.end method

.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1167557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1167558
    iput-object p1, p0, LX/74B;->c:LX/0Zb;

    .line 1167559
    return-void
.end method

.method public static a(LX/0QB;)LX/74B;
    .locals 1

    .prologue
    .line 1167621
    invoke-static {p0}, LX/74B;->b(LX/0QB;)LX/74B;

    move-result-object v0

    return-object v0
.end method

.method private static a(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1167620
    if-eqz p0, :cond_0

    const-string v0, "resumable_upload"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "control"

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJJ",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1167610
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1167611
    const-string v1, "namespace"

    invoke-virtual {v0, v1, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167612
    const-string v1, "file_key"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167613
    const-string v1, "experiment_group"

    const/4 v2, 0x1

    invoke-static {v2}, LX/74B;->a(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167614
    const-string v1, "duration_millis"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167615
    const-string v1, "file_size"

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167616
    const-string v1, "total_file_size"

    invoke-static {p6, p7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167617
    const-string v1, "waterfall_id"

    invoke-virtual {v0, v1, p8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167618
    const-string v1, "media_type"

    const-string v2, "video"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167619
    return-object v0
.end method

.method private static a(LX/74B;LX/74A;Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74A;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1167594
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p1}, LX/74A;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1167595
    const-string v0, "composer"

    .line 1167596
    iput-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1167597
    if-nez p2, :cond_0

    .line 1167598
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 1167599
    :cond_0
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1167600
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 1167601
    :cond_1
    sget-boolean v0, LX/74B;->b:Z

    if-eqz v0, :cond_3

    .line 1167602
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1167603
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1167604
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1167605
    const-string v7, "%s%s:%s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    if-eqz v1, :cond_2

    const-string v1, ""

    :goto_2
    aput-object v1, v8, v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v8, v3

    const/4 v1, 0x2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v8, v1

    invoke-static {v7, v8}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v4

    .line 1167606
    goto :goto_1

    .line 1167607
    :cond_2
    const-string v1, ", "

    goto :goto_2

    .line 1167608
    :cond_3
    iget-object v0, p0, LX/74B;->c:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1167609
    return-void
.end method

.method public static b(LX/0QB;)LX/74B;
    .locals 2

    .prologue
    .line 1167592
    new-instance v1, LX/74B;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/74B;-><init>(LX/0Zb;)V

    .line 1167593
    return-object v1
.end method

.method private static b(ZZZJLjava/lang/String;)Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZJ",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1167584
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1167585
    const-string v1, "experiment_group"

    invoke-static {p0}, LX/74B;->a(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167586
    const-string v1, "using_new_chunk_flow"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167587
    const-string v1, "fallback_enabled"

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167588
    const-string v1, "file_size"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167589
    const-string v1, "waterfall_id"

    invoke-virtual {v0, v1, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167590
    const-string v1, "media_type"

    const-string v2, "video"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167591
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)V
    .locals 10

    .prologue
    .line 1167580
    move-object v0, p1

    move-object v1, p2

    move-wide v2, p4

    move-wide/from16 v4, p6

    move-wide/from16 v6, p8

    move-object/from16 v8, p10

    invoke-static/range {v0 .. v8}, LX/74B;->a(Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    .line 1167581
    const-string v1, "result_handle"

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167582
    sget-object v1, LX/74A;->RESUMABLE_UPLOAD_SUCCEEDED:LX/74A;

    invoke-static {p0, v1, v0}, LX/74B;->a(LX/74B;LX/74A;Ljava/util/Map;)V

    .line 1167583
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJJJJLjava/lang/String;)V
    .locals 9

    .prologue
    .line 1167574
    move-object v0, p1

    move-object v1, p2

    move-wide/from16 v2, p7

    move-wide/from16 v4, p9

    move-wide/from16 v6, p11

    move-object/from16 v8, p13

    invoke-static/range {v0 .. v8}, LX/74B;->a(Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    .line 1167575
    const-string v1, "error"

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167576
    const-string v1, "is_cancellation"

    invoke-static {p4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167577
    const-string v1, "bytes_transferred"

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167578
    sget-object v1, LX/74A;->RESUMABLE_UPLOAD_FAILED:LX/74A;

    invoke-static {p0, v1, v0}, LX/74B;->a(LX/74B;LX/74A;Ljava/util/Map;)V

    .line 1167579
    return-void
.end method

.method public final a(ZZZIJJLjava/lang/String;)V
    .locals 7

    .prologue
    .line 1167569
    move v1, p1

    move v2, p2

    move v3, p3

    move-wide v4, p7

    move-object/from16 v6, p9

    invoke-static/range {v1 .. v6}, LX/74B;->b(ZZZJLjava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    .line 1167570
    const-string v1, "duration_millis"

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167571
    const-string v1, "num_fallbacks"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167572
    sget-object v1, LX/74A;->FBUPLOAD_FILE_UPLOAD_SUCCEEDED:LX/74A;

    invoke-static {p0, v1, v0}, LX/74B;->a(LX/74B;LX/74A;Ljava/util/Map;)V

    .line 1167573
    return-void
.end method

.method public final a(ZZZIJJLjava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 1167563
    move v3, p1

    move v4, p2

    move v5, p3

    move-wide/from16 v6, p7

    move-object/from16 v8, p9

    invoke-static/range {v3 .. v8}, LX/74B;->b(ZZZJLjava/lang/String;)Ljava/util/HashMap;

    move-result-object v2

    .line 1167564
    const-string v3, "error"

    move-object/from16 v0, p10

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167565
    const-string v3, "duration_millis"

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167566
    const-string v3, "num_fallbacks"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167567
    sget-object v3, LX/74A;->FBUPLOAD_FILE_UPLOAD_FAILED:LX/74A;

    invoke-static {p0, v3, v2}, LX/74B;->a(LX/74B;LX/74A;Ljava/util/Map;)V

    .line 1167568
    return-void
.end method

.method public final a(ZZZJLjava/lang/String;)V
    .locals 2

    .prologue
    .line 1167560
    invoke-static/range {p1 .. p6}, LX/74B;->b(ZZZJLjava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    .line 1167561
    sget-object v1, LX/74A;->FBUPLOAD_FILE_UPLOAD_STARTED:LX/74A;

    invoke-static {p0, v1, v0}, LX/74B;->a(LX/74B;LX/74A;Ljava/util/Map;)V

    .line 1167562
    return-void
.end method
