.class public LX/6kM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final action:Ljava/lang/Integer;

.field public final threadKey:LX/6l9;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1136356
    new-instance v0, LX/1sv;

    const-string v1, "DeltaThreadAction"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kM;->b:LX/1sv;

    .line 1136357
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kM;->c:LX/1sw;

    .line 1136358
    new-instance v0, LX/1sw;

    const-string v1, "action"

    const/16 v2, 0x8

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kM;->d:LX/1sw;

    .line 1136359
    sput-boolean v4, LX/6kM;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6l9;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1136360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1136361
    iput-object p1, p0, LX/6kM;->threadKey:LX/6l9;

    .line 1136362
    iput-object p2, p0, LX/6kM;->action:Ljava/lang/Integer;

    .line 1136363
    return-void
.end method

.method public static a(LX/6kM;)V
    .locals 3

    .prologue
    .line 1136364
    iget-object v0, p0, LX/6kM;->action:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, LX/6l6;->a:LX/1sn;

    iget-object v1, p0, LX/6kM;->action:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1136365
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'action\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6kM;->action:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1136366
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1136367
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1136368
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1136369
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1136370
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "DeltaThreadAction"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1136371
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136372
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136373
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136374
    const/4 v1, 0x1

    .line 1136375
    iget-object v5, p0, LX/6kM;->threadKey:LX/6l9;

    if-eqz v5, :cond_0

    .line 1136376
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136377
    const-string v1, "threadKey"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136378
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136379
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136380
    iget-object v1, p0, LX/6kM;->threadKey:LX/6l9;

    if-nez v1, :cond_6

    .line 1136381
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136382
    :goto_3
    const/4 v1, 0x0

    .line 1136383
    :cond_0
    iget-object v5, p0, LX/6kM;->action:Ljava/lang/Integer;

    if-eqz v5, :cond_2

    .line 1136384
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136385
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136386
    const-string v1, "action"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136387
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136388
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136389
    iget-object v0, p0, LX/6kM;->action:Ljava/lang/Integer;

    if-nez v0, :cond_7

    .line 1136390
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136391
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136392
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136393
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1136394
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1136395
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1136396
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1136397
    :cond_6
    iget-object v1, p0, LX/6kM;->threadKey:LX/6l9;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1136398
    :cond_7
    sget-object v0, LX/6l6;->b:Ljava/util/Map;

    iget-object v1, p0, LX/6kM;->action:Ljava/lang/Integer;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1136399
    if-eqz v0, :cond_8

    .line 1136400
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136401
    const-string v1, " ("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136402
    :cond_8
    iget-object v1, p0, LX/6kM;->action:Ljava/lang/Integer;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1136403
    if-eqz v0, :cond_2

    .line 1136404
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1136405
    invoke-static {p0}, LX/6kM;->a(LX/6kM;)V

    .line 1136406
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1136407
    iget-object v0, p0, LX/6kM;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1136408
    iget-object v0, p0, LX/6kM;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1136409
    sget-object v0, LX/6kM;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136410
    iget-object v0, p0, LX/6kM;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1136411
    :cond_0
    iget-object v0, p0, LX/6kM;->action:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1136412
    iget-object v0, p0, LX/6kM;->action:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1136413
    sget-object v0, LX/6kM;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136414
    iget-object v0, p0, LX/6kM;->action:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1136415
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1136416
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1136417
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1136418
    if-nez p1, :cond_1

    .line 1136419
    :cond_0
    :goto_0
    return v0

    .line 1136420
    :cond_1
    instance-of v1, p1, LX/6kM;

    if-eqz v1, :cond_0

    .line 1136421
    check-cast p1, LX/6kM;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1136422
    if-nez p1, :cond_3

    .line 1136423
    :cond_2
    :goto_1
    move v0, v2

    .line 1136424
    goto :goto_0

    .line 1136425
    :cond_3
    iget-object v0, p0, LX/6kM;->threadKey:LX/6l9;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1136426
    :goto_2
    iget-object v3, p1, LX/6kM;->threadKey:LX/6l9;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1136427
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1136428
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136429
    iget-object v0, p0, LX/6kM;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6kM;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1136430
    :cond_5
    iget-object v0, p0, LX/6kM;->action:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1136431
    :goto_4
    iget-object v3, p1, LX/6kM;->action:Ljava/lang/Integer;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1136432
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1136433
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136434
    iget-object v0, p0, LX/6kM;->action:Ljava/lang/Integer;

    iget-object v3, p1, LX/6kM;->action:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1136435
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1136436
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1136437
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1136438
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1136439
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1136440
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1136441
    sget-boolean v0, LX/6kM;->a:Z

    .line 1136442
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kM;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1136443
    return-object v0
.end method
