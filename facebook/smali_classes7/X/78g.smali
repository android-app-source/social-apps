.class public LX/78g;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Landroid/graphics/drawable/Drawable;

.field public b:Landroid/graphics/Rect;

.field public c:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1173277
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1173278
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/78g;->setWillNotDraw(Z)V

    .line 1173279
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1173280
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 1173281
    iget-object v0, p0, LX/78g;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 1173282
    :goto_0
    return-void

    .line 1173283
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1173284
    iget-object v0, p0, LX/78g;->b:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 1173285
    iget-object v0, p0, LX/78g;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1173286
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public setDrawableBounds(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 1173287
    iget-object v0, p0, LX/78g;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1173288
    iget-object v0, p0, LX/78g;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1173289
    :cond_0
    iput-object p1, p0, LX/78g;->c:Landroid/graphics/Rect;

    .line 1173290
    return-void
.end method
