.class public final LX/89I;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:LX/2rw;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;


# direct methods
.method public constructor <init>(JLX/2rw;)V
    .locals 1

    .prologue
    .line 1304537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304538
    const-string v0, ""

    iput-object v0, p0, LX/89I;->c:Ljava/lang/String;

    .line 1304539
    const-string v0, ""

    iput-object v0, p0, LX/89I;->d:Ljava/lang/String;

    .line 1304540
    const/4 v0, 0x0

    iput-object v0, p0, LX/89I;->e:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1304541
    iput-wide p1, p0, LX/89I;->a:J

    .line 1304542
    if-eqz p3, :cond_0

    :goto_0
    iput-object p3, p0, LX/89I;->b:LX/2rw;

    .line 1304543
    return-void

    .line 1304544
    :cond_0
    sget-object p3, LX/2rw;->UNDIRECTED:LX/2rw;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V
    .locals 2

    .prologue
    .line 1304527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304528
    const-string v0, ""

    iput-object v0, p0, LX/89I;->c:Ljava/lang/String;

    .line 1304529
    const-string v0, ""

    iput-object v0, p0, LX/89I;->d:Ljava/lang/String;

    .line 1304530
    const/4 v0, 0x0

    iput-object v0, p0, LX/89I;->e:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1304531
    iget-wide v0, p1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    iput-wide v0, p0, LX/89I;->a:J

    .line 1304532
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    iput-object v0, p0, LX/89I;->b:LX/2rw;

    .line 1304533
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    iput-object v0, p0, LX/89I;->c:Ljava/lang/String;

    .line 1304534
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetProfilePicUrl:Ljava/lang/String;

    iput-object v0, p0, LX/89I;->d:Ljava/lang/String;

    .line 1304535
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetPrivacy:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    iput-object v0, p0, LX/89I;->e:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1304536
    return-void
.end method


# virtual methods
.method public final a(LX/2rX;)LX/89I;
    .locals 1

    .prologue
    .line 1304525
    invoke-static {p1}, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;->a(LX/2rX;)Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/89I;->e:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1304526
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/89I;
    .locals 0

    .prologue
    .line 1304520
    iput-object p1, p0, LX/89I;->c:Ljava/lang/String;

    .line 1304521
    return-object p0
.end method

.method public final a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 2

    .prologue
    .line 1304524
    new-instance v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/intent/ComposerTargetData;-><init>(LX/89I;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/89I;
    .locals 0

    .prologue
    .line 1304522
    iput-object p1, p0, LX/89I;->d:Ljava/lang/String;

    .line 1304523
    return-object p0
.end method
