.class public final LX/7QX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/7Qb;


# direct methods
.method public constructor <init>(LX/7Qb;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1204521
    iput-object p1, p0, LX/7QX;->b:LX/7Qb;

    iput-object p2, p0, LX/7QX;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1204522
    const-string v0, "BroadcastStatusUpdateManager"

    const-string v1, "LiveVideoBroadcastStatusUpdateSubscription query failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1204523
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1204524
    check-cast p1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateSubscriptionModel;

    .line 1204525
    iget-object v1, p0, LX/7QX;->b:LX/7Qb;

    iget-object v2, p0, LX/7QX;->a:Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v2, v0}, LX/7Qb;->a$redex0(LX/7Qb;Ljava/lang/String;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V

    .line 1204526
    return-void

    .line 1204527
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateSubscriptionModel;->a()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;

    move-result-object v0

    goto :goto_0
.end method
