.class public LX/8RM;
.super LX/8RL;
.source ""


# instance fields
.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/3fr;LX/0Sh;LX/3Oq;LX/2RQ;)V
    .locals 0
    .param p3    # LX/3Oq;
        .annotation runtime Lcom/facebook/contacts/module/ContactLinkQueryType;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1344642
    invoke-direct {p0, p1, p2, p3, p4}, LX/8RL;-><init>(LX/3fr;LX/0Sh;LX/3Oq;LX/2RQ;)V

    .line 1344643
    return-void
.end method

.method public static c(LX/0QB;)LX/8RM;
    .locals 5

    .prologue
    .line 1344644
    new-instance v4, LX/8RM;

    invoke-static {p0}, LX/3fr;->a(LX/0QB;)LX/3fr;

    move-result-object v0

    check-cast v0, LX/3fr;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/6NS;->b(LX/0QB;)LX/3Oq;

    move-result-object v2

    check-cast v2, LX/3Oq;

    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v3

    check-cast v3, LX/2RQ;

    invoke-direct {v4, v0, v1, v2, v3}, LX/8RM;-><init>(LX/3fr;LX/0Sh;LX/3Oq;LX/2RQ;)V

    .line 1344645
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1344646
    iput-object p1, p0, LX/8RM;->e:Ljava/lang/String;

    .line 1344647
    invoke-virtual {p0, p1}, LX/8RL;->b(Ljava/lang/String;)V

    .line 1344648
    return-void
.end method

.method public final a(LX/8QL;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1344649
    instance-of v1, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    if-eqz v1, :cond_1

    .line 1344650
    iget-object v0, p0, LX/8RL;->a:Ljava/util/Set;

    check-cast p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1344651
    iget-object v1, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 1344652
    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 1344653
    :cond_0
    :goto_0
    return v0

    .line 1344654
    :cond_1
    instance-of v1, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    if-nez v1, :cond_2

    instance-of v1, p1, LX/8QM;

    if-eqz v1, :cond_0

    .line 1344655
    :cond_2
    invoke-virtual {p1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1344656
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/8RM;->e:Ljava/lang/String;

    invoke-static {v2}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
