.class public final LX/7Ck;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# instance fields
.field public final synthetic a:Lcom/facebook/spherical/GlMediaRenderThread;


# direct methods
.method public constructor <init>(Lcom/facebook/spherical/GlMediaRenderThread;)V
    .locals 0

    .prologue
    .line 1181528
    iput-object p1, p0, LX/7Ck;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final doFrame(J)V
    .locals 2

    .prologue
    .line 1181529
    iget-object v0, p0, LX/7Ck;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget-boolean v0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->t:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/7Ck;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget-object v0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->h:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 1181530
    :cond_0
    iget-object v0, p0, LX/7Ck;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget-object v0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->I:Landroid/view/Choreographer;

    iget-object v1, p0, LX/7Ck;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget-object v1, v1, Lcom/facebook/spherical/GlMediaRenderThread;->G:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1181531
    :goto_0
    return-void

    .line 1181532
    :cond_1
    iget-object v0, p0, LX/7Ck;->a:Lcom/facebook/spherical/GlMediaRenderThread;

    iget-object v0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->h:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
