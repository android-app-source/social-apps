.class public final LX/7sO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 1264838
    const/4 v15, 0x0

    .line 1264839
    const/4 v14, 0x0

    .line 1264840
    const-wide/16 v12, 0x0

    .line 1264841
    const/4 v11, 0x0

    .line 1264842
    const/4 v10, 0x0

    .line 1264843
    const/4 v9, 0x0

    .line 1264844
    const/4 v8, 0x0

    .line 1264845
    const/4 v7, 0x0

    .line 1264846
    const/4 v6, 0x0

    .line 1264847
    const/4 v5, 0x0

    .line 1264848
    const/4 v4, 0x0

    .line 1264849
    const/4 v3, 0x0

    .line 1264850
    const/4 v2, 0x0

    .line 1264851
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_f

    .line 1264852
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1264853
    const/4 v2, 0x0

    .line 1264854
    :goto_0
    return v2

    .line 1264855
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_c

    .line 1264856
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1264857
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1264858
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v18

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 1264859
    const-string v6, "buyer_email"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1264860
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 1264861
    :cond_1
    const-string v6, "buyer_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1264862
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 1264863
    :cond_2
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1264864
    const/4 v2, 0x1

    .line 1264865
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1264866
    :cond_3
    const-string v6, "credit_card_used"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1264867
    invoke-static/range {p0 .. p1}, LX/7sP;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 1264868
    :cond_4
    const-string v6, "event"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1264869
    invoke-static/range {p0 .. p1}, LX/7sN;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 1264870
    :cond_5
    const-string v6, "event_tickets"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1264871
    invoke-static/range {p0 .. p1}, LX/7sV;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 1264872
    :cond_6
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1264873
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 1264874
    :cond_7
    const-string v6, "order_action_link"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1264875
    invoke-static/range {p0 .. p1}, LX/7sW;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1264876
    :cond_8
    const-string v6, "ticket_order_items"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1264877
    invoke-static/range {p0 .. p1}, LX/7sX;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 1264878
    :cond_9
    const-string v6, "tickets_count"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1264879
    const/4 v2, 0x1

    .line 1264880
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v10, v6

    goto/16 :goto_1

    .line 1264881
    :cond_a
    const-string v6, "total_order_price"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1264882
    invoke-static/range {p0 .. p1}, LX/7sY;->a(LX/15w;LX/186;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 1264883
    :cond_b
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1264884
    :cond_c
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1264885
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1264886
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1264887
    if-eqz v3, :cond_d

    .line 1264888
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1264889
    :cond_d
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1264890
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1264891
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1264892
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1264893
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1264894
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1264895
    if-eqz v8, :cond_e

    .line 1264896
    const/16 v2, 0x9

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10, v3}, LX/186;->a(III)V

    .line 1264897
    :cond_e
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1264898
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_f
    move/from16 v16, v11

    move/from16 v17, v15

    move v11, v6

    move v15, v10

    move v10, v5

    move/from16 v19, v8

    move v8, v2

    move/from16 v20, v9

    move v9, v4

    move-wide v4, v12

    move/from16 v13, v19

    move v12, v7

    move v7, v14

    move/from16 v14, v20

    goto/16 :goto_1
.end method
