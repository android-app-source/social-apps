.class public LX/7vu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6E0;


# instance fields
.field private final a:LX/6tM;


# direct methods
.method public constructor <init>(LX/6tM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1275237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275238
    iput-object p1, p0, LX/7vu;->a:LX/6tM;

    .line 1275239
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)Lcom/facebook/payments/confirmation/ConfirmationParams;
    .locals 4

    .prologue
    .line 1275241
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;

    move-object v1, p2

    .line 1275242
    check-cast v1, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;

    .line 1275243
    iget-object v2, v1, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;->b:LX/0lF;

    move-object v1, v2

    .line 1275244
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 1275245
    const-string v2, "event_ticketing_receipt_url"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 1275246
    new-instance v2, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;

    sget-object v3, LX/6uW;->EVENT_TICKETING:LX/6uW;

    invoke-static {p1, p2, v3}, LX/6tM;->b(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;LX/6uW;)LX/6uM;

    move-result-object v3

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1275247
    iput-object v1, v3, LX/6uM;->f:Ljava/lang/String;

    .line 1275248
    move-object v1, v3

    .line 1275249
    invoke-virtual {v1}, LX/6uM;->f()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->p()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-direct {v2, v3, v1, v0}, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;-><init>(Lcom/facebook/payments/confirmation/ConfirmationCommonParams;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;Lcom/facebook/events/common/EventAnalyticsParams;)V

    return-object v2
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;
    .locals 1

    .prologue
    .line 1275240
    iget-object v0, p0, LX/7vu;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;
    .locals 1

    .prologue
    .line 1275236
    iget-object v0, p0, LX/7vu;->a:LX/6tM;

    invoke-virtual {v0, p1, p2}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;
    .locals 1

    .prologue
    .line 1275235
    iget-object v0, p0, LX/7vu;->a:LX/6tM;

    invoke-virtual {v0, p1, p2}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;
    .locals 1

    .prologue
    .line 1275234
    iget-object v0, p0, LX/7vu;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->b(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;
    .locals 1

    .prologue
    .line 1275250
    iget-object v0, p0, LX/7vu;->a:LX/6tM;

    invoke-virtual {v0, p1, p2}, LX/6tM;->b(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;
    .locals 1

    .prologue
    .line 1275233
    iget-object v0, p0, LX/7vu;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->c(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;
    .locals 1

    .prologue
    .line 1275232
    iget-object v0, p0, LX/7vu;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->d(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/model/ShippingParams;
    .locals 1

    .prologue
    .line 1275231
    iget-object v0, p0, LX/7vu;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->e(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/model/ShippingParams;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;
    .locals 1

    .prologue
    .line 1275228
    iget-object v0, p0, LX/7vu;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->f(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final g(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;
    .locals 1

    .prologue
    .line 1275230
    iget-object v0, p0, LX/7vu;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->g(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final h(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;
    .locals 1

    .prologue
    .line 1275229
    iget-object v0, p0, LX/7vu;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->h(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method
