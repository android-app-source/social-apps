.class public LX/73T;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/73S;

.field public final b:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(LX/73S;)V
    .locals 1

    .prologue
    .line 1165859
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    .line 1165860
    return-void
.end method

.method public constructor <init>(LX/73S;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165862
    iput-object p1, p0, LX/73T;->a:LX/73S;

    .line 1165863
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/73T;->b:Landroid/os/Bundle;

    .line 1165864
    if-eqz p2, :cond_0

    .line 1165865
    iget-object v0, p0, LX/73T;->b:Landroid/os/Bundle;

    invoke-virtual {v0, p2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1165866
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/os/Parcelable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 1165867
    iget-object v0, p0, LX/73T;->b:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165868
    iget-object v0, p0, LX/73T;->b:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Ljava/io/Serializable;
    .locals 1

    .prologue
    .line 1165869
    iget-object v0, p0, LX/73T;->b:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    return-object v0
.end method
