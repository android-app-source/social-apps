.class public LX/7WS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/0tX;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1216404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1216405
    iput-object p1, p0, LX/7WS;->a:LX/0tX;

    .line 1216406
    iput-object p2, p0, LX/7WS;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1216407
    iput-object p3, p0, LX/7WS;->c:Ljava/util/concurrent/ExecutorService;

    .line 1216408
    return-void
.end method

.method public static b(LX/0QB;)LX/7WS;
    .locals 4

    .prologue
    .line 1216409
    new-instance v3, LX/7WS;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v3, v0, v1, v2}, LX/7WS;-><init>(LX/0tX;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/concurrent/ExecutorService;)V

    .line 1216410
    return-object v3
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1216411
    new-instance v0, LX/7WR;

    iget-object v1, p0, LX/7WS;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v0, v1}, LX/7WR;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    invoke-virtual {p0, v0}, LX/7WS;->a(LX/0TF;)V

    .line 1216412
    return-void
.end method

.method public final a(LX/0TF;)V
    .locals 2

    .prologue
    .line 1216413
    new-instance v0, LX/7WT;

    invoke-direct {v0}, LX/7WT;-><init>()V

    move-object v0, v0

    .line 1216414
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1216415
    iget-object v1, p0, LX/7WS;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1216416
    iget-object v1, p0, LX/7WS;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, p1, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1216417
    return-void
.end method
