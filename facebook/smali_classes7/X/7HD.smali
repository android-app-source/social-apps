.class public final enum LX/7HD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7HD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7HD;

.field public static final enum PREDEFINED:LX/7HD;

.field public static final enum RECENTLY_USED:LX/7HD;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1190495
    new-instance v0, LX/7HD;

    const-string v1, "PREDEFINED"

    invoke-direct {v0, v1, v2}, LX/7HD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7HD;->PREDEFINED:LX/7HD;

    .line 1190496
    new-instance v0, LX/7HD;

    const-string v1, "RECENTLY_USED"

    invoke-direct {v0, v1, v3}, LX/7HD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7HD;->RECENTLY_USED:LX/7HD;

    .line 1190497
    const/4 v0, 0x2

    new-array v0, v0, [LX/7HD;

    sget-object v1, LX/7HD;->PREDEFINED:LX/7HD;

    aput-object v1, v0, v2

    sget-object v1, LX/7HD;->RECENTLY_USED:LX/7HD;

    aput-object v1, v0, v3

    sput-object v0, LX/7HD;->$VALUES:[LX/7HD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1190498
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7HD;
    .locals 1

    .prologue
    .line 1190499
    const-class v0, LX/7HD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7HD;

    return-object v0
.end method

.method public static values()[LX/7HD;
    .locals 1

    .prologue
    .line 1190500
    sget-object v0, LX/7HD;->$VALUES:[LX/7HD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7HD;

    return-object v0
.end method
