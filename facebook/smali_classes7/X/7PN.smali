.class public LX/7PN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Oi;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/7Oi;

.field private final c:LX/7Pw;

.field private final d:LX/7PJ;

.field private final e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1202788
    const-class v0, LX/7PN;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7PN;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/7Oi;LX/7Pw;LX/7PJ;)V
    .locals 1

    .prologue
    .line 1202789
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202790
    const/16 v0, 0x14

    iput v0, p0, LX/7PN;->e:I

    .line 1202791
    iput-object p1, p0, LX/7PN;->b:LX/7Oi;

    .line 1202792
    iput-object p2, p0, LX/7PN;->c:LX/7Pw;

    .line 1202793
    iput-object p3, p0, LX/7PN;->d:LX/7PJ;

    .line 1202794
    return-void
.end method

.method private static a(LX/2WF;J)LX/2WF;
    .locals 7

    .prologue
    .line 1202795
    invoke-virtual {p0}, LX/2WF;->a()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    .line 1202796
    new-instance v0, LX/2WF;

    iget-wide v2, p0, LX/2WF;->a:J

    iget-wide v4, p0, LX/2WF;->a:J

    add-long/2addr v4, p1

    invoke-direct {v0, v2, v3, v4, v5}, LX/2WF;-><init>(JJ)V

    move-object p0, v0

    .line 1202797
    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a(LX/2WF;Ljava/io/OutputStream;)J
    .locals 4

    .prologue
    .line 1202798
    :goto_0
    iget-object v0, p0, LX/7PN;->d:LX/7PJ;

    iget-object v1, p0, LX/7PN;->c:LX/7Pw;

    iget-wide v2, p1, LX/2WF;->a:J

    invoke-interface {v0, v1, v2, v3}, LX/7PJ;->a(LX/7Pw;J)J

    move-result-wide v0

    .line 1202799
    const-wide/16 v2, -0x2

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 1202800
    invoke-virtual {p1}, LX/2WF;->a()J

    move-result-wide v0

    .line 1202801
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 1202802
    invoke-static {p1, v0, v1}, LX/7PN;->a(LX/2WF;J)LX/2WF;

    move-result-object v0

    .line 1202803
    iget-object v1, p0, LX/7PN;->b:LX/7Oi;

    invoke-interface {v1, v0, p2}, LX/7Oi;->a(LX/2WF;Ljava/io/OutputStream;)J

    move-result-wide v0

    .line 1202804
    return-wide v0

    .line 1202805
    :cond_1
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 1202806
    new-instance v0, LX/7PO;

    const-string v1, "Throttling policy asks to stop"

    invoke-direct {v0, v1}, LX/7PO;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1202807
    :cond_2
    const-wide/16 v0, 0x14

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1202808
    :catch_0
    move-exception v0

    .line 1202809
    new-instance v1, Ljava/io/InterruptedIOException;

    const-string v2, "Throttling interrupted!"

    invoke-direct {v1, v2}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    .line 1202810
    invoke-virtual {v1, v0}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1202811
    throw v1
.end method

.method public final a()LX/3Dd;
    .locals 1

    .prologue
    .line 1202812
    iget-object v0, p0, LX/7PN;->b:LX/7Oi;

    invoke-interface {v0}, LX/7Oi;->a()LX/3Dd;

    move-result-object v0

    return-object v0
.end method
