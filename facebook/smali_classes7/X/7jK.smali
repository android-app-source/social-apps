.class public LX/7jK;
.super LX/7jF;
.source ""


# instance fields
.field public a:Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

.field public b:Ljava/lang/String;


# direct methods
.method private constructor <init>(LX/7jE;LX/7jD;Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;Ljava/lang/String;)V
    .locals 0
    .param p3    # Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1229281
    invoke-direct {p0, p1, p2}, LX/7jF;-><init>(LX/7jE;LX/7jD;)V

    .line 1229282
    iput-object p3, p0, LX/7jK;->a:Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    .line 1229283
    iput-object p4, p0, LX/7jK;->b:Ljava/lang/String;

    .line 1229284
    return-void
.end method

.method public static a(LX/7jD;Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;Ljava/lang/String;)LX/7jK;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1229286
    new-instance v0, LX/7jK;

    sget-object v1, LX/7jE;->SUCCESS:LX/7jE;

    invoke-direct {v0, v1, p0, p1, p2}, LX/7jK;-><init>(LX/7jE;LX/7jD;Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/7jD;Ljava/lang/String;)LX/7jK;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1229285
    new-instance v0, LX/7jK;

    sget-object v1, LX/7jE;->FAILED:LX/7jE;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2, p1}, LX/7jK;-><init>(LX/7jE;LX/7jD;Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;Ljava/lang/String;)V

    return-object v0
.end method
