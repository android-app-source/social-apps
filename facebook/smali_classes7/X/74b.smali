.class public LX/74b;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/0ck;

.field public c:LX/741;

.field private final d:Ljava/lang/String;

.field private final e:LX/742;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0ck;LX/742;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1168142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1168143
    iput-object p1, p0, LX/74b;->a:Ljava/lang/String;

    .line 1168144
    iput-object p2, p0, LX/74b;->b:LX/0ck;

    .line 1168145
    sget-object v0, LX/741;->UNKNOWN:LX/741;

    iput-object v0, p0, LX/74b;->c:LX/741;

    .line 1168146
    iput-object p4, p0, LX/74b;->d:Ljava/lang/String;

    .line 1168147
    iput-object p3, p0, LX/74b;->e:LX/742;

    .line 1168148
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1168149
    iget-object v0, p0, LX/74b;->b:LX/0ck;

    .line 1168150
    sget-object v1, LX/0ck;->PHOTO:LX/0ck;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/0ck;->VIDEO:LX/0ck;

    if-ne v0, v1, :cond_4

    .line 1168151
    :cond_0
    const/4 v1, 0x1

    .line 1168152
    :goto_0
    move v0, v1

    .line 1168153
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1168154
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 1168155
    const-string v0, "version"

    iget-object v2, p0, LX/74b;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1168156
    const-string v0, "media_type"

    iget-object v2, p0, LX/74b;->b:LX/0ck;

    .line 1168157
    sget-object v3, LX/74a;->a:[I

    invoke-virtual {v2}, LX/0ck;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1168158
    const/4 v3, 0x0

    :goto_1
    move-object v2, v3

    .line 1168159
    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1168160
    iget-object v0, p0, LX/74b;->c:LX/741;

    sget-object v2, LX/741;->UNKNOWN:LX/741;

    if-eq v0, v2, :cond_1

    .line 1168161
    const-string v2, "is_vault"

    iget-object v0, p0, LX/74b;->c:LX/741;

    sget-object v3, LX/741;->VAULT:LX/741;

    if-ne v0, v3, :cond_3

    const-string v0, "1"

    :goto_2
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1168162
    :cond_1
    const-string v0, "is_native_resizing"

    iget-object v2, p0, LX/74b;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1168163
    iget-object v0, p0, LX/74b;->e:LX/742;

    iget-object v0, v0, LX/742;->value:Ljava/lang/String;

    sget-object v2, LX/742;->NOT_RELEVANT:LX/742;

    iget-object v2, v2, LX/742;->value:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1168164
    const-string v0, "upload_method"

    iget-object v2, p0, LX/74b;->e:LX/742;

    iget-object v2, v2, LX/742;->value:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1168165
    :cond_2
    return-object v1

    .line 1168166
    :cond_3
    const-string v0, "0"

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    .line 1168167
    :pswitch_0
    const-string v3, "photo"

    goto :goto_1

    .line 1168168
    :pswitch_1
    const-string v3, "video"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/741;)V
    .locals 0

    .prologue
    .line 1168169
    iput-object p1, p0, LX/74b;->c:LX/741;

    .line 1168170
    return-void
.end method
