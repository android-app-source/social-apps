.class public final LX/7cv;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/7Zn;

.field public static final c:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/7d8;

.field public static final e:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<",
            "LX/7e2;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:LX/7e0;

.field public static final g:LX/7ek;

.field public static final h:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:LX/7ZW;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, LX/2vs;

    const-string v1, "Nearby.CONNECTIONS_API"

    sget-object v2, LX/7Zo;->b:LX/2vq;

    sget-object v3, LX/7Zo;->a:LX/2vn;

    invoke-direct {v0, v1, v2, v3}, LX/2vs;-><init>(Ljava/lang/String;LX/2vq;LX/2vn;)V

    sput-object v0, LX/7cv;->a:LX/2vs;

    new-instance v0, LX/7Zo;

    invoke-direct {v0}, LX/7Zo;-><init>()V

    sput-object v0, LX/7cv;->b:LX/7Zn;

    new-instance v0, LX/2vs;

    const-string v1, "Nearby.CONNECTIONS_DEV_API"

    sget-object v2, LX/7dO;->b:LX/2vq;

    sget-object v3, LX/7dO;->a:LX/2vn;

    invoke-direct {v0, v1, v2, v3}, LX/2vs;-><init>(Ljava/lang/String;LX/2vq;LX/2vn;)V

    sput-object v0, LX/7cv;->c:LX/2vs;

    new-instance v0, LX/7dO;

    invoke-direct {v0}, LX/7dO;-><init>()V

    sput-object v0, LX/7cv;->d:LX/7d8;

    new-instance v0, LX/2vs;

    const-string v1, "Nearby.MESSAGES_API"

    sget-object v2, LX/7ej;->b:LX/2vq;

    sget-object v3, LX/7ej;->a:LX/2vn;

    invoke-direct {v0, v1, v2, v3}, LX/2vs;-><init>(Ljava/lang/String;LX/2vq;LX/2vn;)V

    sput-object v0, LX/7cv;->e:LX/2vs;

    new-instance v0, LX/7ej;

    invoke-direct {v0}, LX/7ej;-><init>()V

    sput-object v0, LX/7cv;->f:LX/7e0;

    new-instance v0, LX/7el;

    invoke-direct {v0}, LX/7el;-><init>()V

    sput-object v0, LX/7cv;->g:LX/7ek;

    new-instance v0, LX/2vs;

    const-string v1, "Nearby.BOOTSTRAP_API"

    sget-object v2, LX/7ZX;->b:LX/2vq;

    sget-object v3, LX/7ZX;->a:LX/2vn;

    invoke-direct {v0, v1, v2, v3}, LX/2vs;-><init>(Ljava/lang/String;LX/2vq;LX/2vn;)V

    sput-object v0, LX/7cv;->h:LX/2vs;

    new-instance v0, LX/7ZX;

    invoke-direct {v0}, LX/7ZX;-><init>()V

    sput-object v0, LX/7cv;->i:LX/7ZW;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
