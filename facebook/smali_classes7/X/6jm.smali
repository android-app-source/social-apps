.class public LX/6jm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;


# instance fields
.field public final actorFbId:Ljava/lang/Long;

.field public final appId:Ljava/lang/Long;

.field public final deliveredWatermarkTimestampMs:Ljava/lang/Long;

.field public final deviceId:Ljava/lang/String;

.field public final messageIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final threadKey:LX/6l9;

.field public final timestampMs:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0xa

    .line 1132573
    new-instance v0, LX/1sv;

    const-string v1, "DeltaDeliveryReceipt"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jm;->b:LX/1sv;

    .line 1132574
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jm;->c:LX/1sw;

    .line 1132575
    new-instance v0, LX/1sw;

    const-string v1, "actorFbId"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jm;->d:LX/1sw;

    .line 1132576
    new-instance v0, LX/1sw;

    const-string v1, "deviceId"

    const/16 v2, 0xb

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jm;->e:LX/1sw;

    .line 1132577
    new-instance v0, LX/1sw;

    const-string v1, "appId"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jm;->f:LX/1sw;

    .line 1132578
    new-instance v0, LX/1sw;

    const-string v1, "timestampMs"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jm;->g:LX/1sw;

    .line 1132579
    new-instance v0, LX/1sw;

    const-string v1, "messageIds"

    const/16 v2, 0xf

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jm;->h:LX/1sw;

    .line 1132580
    new-instance v0, LX/1sw;

    const-string v1, "deliveredWatermarkTimestampMs"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jm;->i:LX/1sw;

    .line 1132581
    sput-boolean v5, LX/6jm;->a:Z

    return-void
.end method

.method private constructor <init>(LX/6l9;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Long;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6l9;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1132582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1132583
    iput-object p1, p0, LX/6jm;->threadKey:LX/6l9;

    .line 1132584
    iput-object p2, p0, LX/6jm;->actorFbId:Ljava/lang/Long;

    .line 1132585
    iput-object p3, p0, LX/6jm;->deviceId:Ljava/lang/String;

    .line 1132586
    iput-object p4, p0, LX/6jm;->appId:Ljava/lang/Long;

    .line 1132587
    iput-object p5, p0, LX/6jm;->timestampMs:Ljava/lang/Long;

    .line 1132588
    iput-object p6, p0, LX/6jm;->messageIds:Ljava/util/List;

    .line 1132589
    iput-object p7, p0, LX/6jm;->deliveredWatermarkTimestampMs:Ljava/lang/Long;

    .line 1132590
    return-void
.end method

.method public static b(LX/1su;)LX/6jm;
    .locals 13

    .prologue
    const/4 v8, 0x0

    const/16 v12, 0xa

    const/4 v7, 0x0

    .line 1132591
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v6, v7

    move-object v5, v7

    move-object v4, v7

    move-object v3, v7

    move-object v2, v7

    move-object v1, v7

    .line 1132592
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1132593
    iget-byte v9, v0, LX/1sw;->b:B

    if-eqz v9, :cond_9

    .line 1132594
    iget-short v9, v0, LX/1sw;->c:S

    packed-switch v9, :pswitch_data_0

    .line 1132595
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1132596
    :pswitch_0
    iget-byte v9, v0, LX/1sw;->b:B

    const/16 v10, 0xc

    if-ne v9, v10, :cond_1

    .line 1132597
    invoke-static {p0}, LX/6l9;->b(LX/1su;)LX/6l9;

    move-result-object v1

    goto :goto_0

    .line 1132598
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1132599
    :pswitch_1
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_2

    .line 1132600
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0

    .line 1132601
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1132602
    :pswitch_2
    iget-byte v9, v0, LX/1sw;->b:B

    const/16 v10, 0xb

    if-ne v9, v10, :cond_3

    .line 1132603
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 1132604
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1132605
    :pswitch_3
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_4

    .line 1132606
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_0

    .line 1132607
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1132608
    :pswitch_4
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_5

    .line 1132609
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0

    .line 1132610
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1132611
    :pswitch_5
    iget-byte v9, v0, LX/1sw;->b:B

    const/16 v10, 0xf

    if-ne v9, v10, :cond_7

    .line 1132612
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v9

    .line 1132613
    new-instance v6, Ljava/util/ArrayList;

    iget v0, v9, LX/1u3;->b:I

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v8

    .line 1132614
    :goto_1
    iget v10, v9, LX/1u3;->b:I

    if-gez v10, :cond_6

    invoke-static {}, LX/1su;->t()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1132615
    :goto_2
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v10

    .line 1132616
    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1132617
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1132618
    :cond_6
    iget v10, v9, LX/1u3;->b:I

    if-ge v0, v10, :cond_0

    goto :goto_2

    .line 1132619
    :cond_7
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1132620
    :pswitch_6
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_8

    .line 1132621
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    goto/16 :goto_0

    .line 1132622
    :cond_8
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1132623
    :cond_9
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1132624
    new-instance v0, LX/6jm;

    invoke-direct/range {v0 .. v7}, LX/6jm;-><init>(LX/6l9;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Long;)V

    .line 1132625
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1132626
    if-eqz p2, :cond_c

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1132627
    :goto_0
    if-eqz p2, :cond_d

    const-string v0, "\n"

    move-object v3, v0

    .line 1132628
    :goto_1
    if-eqz p2, :cond_e

    const-string v0, " "

    .line 1132629
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "DeltaDeliveryReceipt"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1132630
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132631
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132632
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132633
    const/4 v1, 0x1

    .line 1132634
    iget-object v6, p0, LX/6jm;->threadKey:LX/6l9;

    if-eqz v6, :cond_0

    .line 1132635
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132636
    const-string v1, "threadKey"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132637
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132638
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132639
    iget-object v1, p0, LX/6jm;->threadKey:LX/6l9;

    if-nez v1, :cond_f

    .line 1132640
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1132641
    :cond_0
    iget-object v6, p0, LX/6jm;->actorFbId:Ljava/lang/Long;

    if-eqz v6, :cond_2

    .line 1132642
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132643
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132644
    const-string v1, "actorFbId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132645
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132646
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132647
    iget-object v1, p0, LX/6jm;->actorFbId:Ljava/lang/Long;

    if-nez v1, :cond_10

    .line 1132648
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 1132649
    :cond_2
    iget-object v6, p0, LX/6jm;->deviceId:Ljava/lang/String;

    if-eqz v6, :cond_4

    .line 1132650
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132651
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132652
    const-string v1, "deviceId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132653
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132654
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132655
    iget-object v1, p0, LX/6jm;->deviceId:Ljava/lang/String;

    if-nez v1, :cond_11

    .line 1132656
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v1, v2

    .line 1132657
    :cond_4
    iget-object v6, p0, LX/6jm;->appId:Ljava/lang/Long;

    if-eqz v6, :cond_6

    .line 1132658
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132659
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132660
    const-string v1, "appId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132661
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132662
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132663
    iget-object v1, p0, LX/6jm;->appId:Ljava/lang/Long;

    if-nez v1, :cond_12

    .line 1132664
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    move v1, v2

    .line 1132665
    :cond_6
    iget-object v6, p0, LX/6jm;->timestampMs:Ljava/lang/Long;

    if-eqz v6, :cond_8

    .line 1132666
    if-nez v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132667
    :cond_7
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132668
    const-string v1, "timestampMs"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132669
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132670
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132671
    iget-object v1, p0, LX/6jm;->timestampMs:Ljava/lang/Long;

    if-nez v1, :cond_13

    .line 1132672
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_7
    move v1, v2

    .line 1132673
    :cond_8
    iget-object v6, p0, LX/6jm;->messageIds:Ljava/util/List;

    if-eqz v6, :cond_16

    .line 1132674
    if-nez v1, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132675
    :cond_9
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132676
    const-string v1, "messageIds"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132677
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132678
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132679
    iget-object v1, p0, LX/6jm;->messageIds:Ljava/util/List;

    if-nez v1, :cond_14

    .line 1132680
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132681
    :goto_8
    iget-object v1, p0, LX/6jm;->deliveredWatermarkTimestampMs:Ljava/lang/Long;

    if-eqz v1, :cond_b

    .line 1132682
    if-nez v2, :cond_a

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132683
    :cond_a
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132684
    const-string v1, "deliveredWatermarkTimestampMs"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132685
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132686
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132687
    iget-object v0, p0, LX/6jm;->deliveredWatermarkTimestampMs:Ljava/lang/Long;

    if-nez v0, :cond_15

    .line 1132688
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132689
    :cond_b
    :goto_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132690
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132691
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1132692
    :cond_c
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1132693
    :cond_d
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1132694
    :cond_e
    const-string v0, ""

    goto/16 :goto_2

    .line 1132695
    :cond_f
    iget-object v1, p0, LX/6jm;->threadKey:LX/6l9;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1132696
    :cond_10
    iget-object v1, p0, LX/6jm;->actorFbId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1132697
    :cond_11
    iget-object v1, p0, LX/6jm;->deviceId:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1132698
    :cond_12
    iget-object v1, p0, LX/6jm;->appId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1132699
    :cond_13
    iget-object v1, p0, LX/6jm;->timestampMs:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1132700
    :cond_14
    iget-object v1, p0, LX/6jm;->messageIds:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1132701
    :cond_15
    iget-object v0, p0, LX/6jm;->deliveredWatermarkTimestampMs:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    :cond_16
    move v2, v1

    goto/16 :goto_8
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1132702
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1132703
    iget-object v0, p0, LX/6jm;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1132704
    iget-object v0, p0, LX/6jm;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1132705
    sget-object v0, LX/6jm;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132706
    iget-object v0, p0, LX/6jm;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1132707
    :cond_0
    iget-object v0, p0, LX/6jm;->actorFbId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1132708
    iget-object v0, p0, LX/6jm;->actorFbId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1132709
    sget-object v0, LX/6jm;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132710
    iget-object v0, p0, LX/6jm;->actorFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1132711
    :cond_1
    iget-object v0, p0, LX/6jm;->deviceId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1132712
    iget-object v0, p0, LX/6jm;->deviceId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1132713
    sget-object v0, LX/6jm;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132714
    iget-object v0, p0, LX/6jm;->deviceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1132715
    :cond_2
    iget-object v0, p0, LX/6jm;->appId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1132716
    iget-object v0, p0, LX/6jm;->appId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1132717
    sget-object v0, LX/6jm;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132718
    iget-object v0, p0, LX/6jm;->appId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1132719
    :cond_3
    iget-object v0, p0, LX/6jm;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1132720
    iget-object v0, p0, LX/6jm;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1132721
    sget-object v0, LX/6jm;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132722
    iget-object v0, p0, LX/6jm;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1132723
    :cond_4
    iget-object v0, p0, LX/6jm;->messageIds:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 1132724
    iget-object v0, p0, LX/6jm;->messageIds:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 1132725
    sget-object v0, LX/6jm;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132726
    new-instance v0, LX/1u3;

    const/16 v1, 0xb

    iget-object v2, p0, LX/6jm;->messageIds:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1132727
    iget-object v0, p0, LX/6jm;->messageIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1132728
    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1132729
    :cond_5
    iget-object v0, p0, LX/6jm;->deliveredWatermarkTimestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 1132730
    iget-object v0, p0, LX/6jm;->deliveredWatermarkTimestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 1132731
    sget-object v0, LX/6jm;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132732
    iget-object v0, p0, LX/6jm;->deliveredWatermarkTimestampMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1132733
    :cond_6
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1132734
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1132735
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1132736
    if-nez p1, :cond_1

    .line 1132737
    :cond_0
    :goto_0
    return v0

    .line 1132738
    :cond_1
    instance-of v1, p1, LX/6jm;

    if-eqz v1, :cond_0

    .line 1132739
    check-cast p1, LX/6jm;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1132740
    if-nez p1, :cond_3

    .line 1132741
    :cond_2
    :goto_1
    move v0, v2

    .line 1132742
    goto :goto_0

    .line 1132743
    :cond_3
    iget-object v0, p0, LX/6jm;->threadKey:LX/6l9;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1132744
    :goto_2
    iget-object v3, p1, LX/6jm;->threadKey:LX/6l9;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1132745
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1132746
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132747
    iget-object v0, p0, LX/6jm;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6jm;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132748
    :cond_5
    iget-object v0, p0, LX/6jm;->actorFbId:Ljava/lang/Long;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1132749
    :goto_4
    iget-object v3, p1, LX/6jm;->actorFbId:Ljava/lang/Long;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1132750
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1132751
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132752
    iget-object v0, p0, LX/6jm;->actorFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/6jm;->actorFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132753
    :cond_7
    iget-object v0, p0, LX/6jm;->deviceId:Ljava/lang/String;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1132754
    :goto_6
    iget-object v3, p1, LX/6jm;->deviceId:Ljava/lang/String;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1132755
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1132756
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132757
    iget-object v0, p0, LX/6jm;->deviceId:Ljava/lang/String;

    iget-object v3, p1, LX/6jm;->deviceId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132758
    :cond_9
    iget-object v0, p0, LX/6jm;->appId:Ljava/lang/Long;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1132759
    :goto_8
    iget-object v3, p1, LX/6jm;->appId:Ljava/lang/Long;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1132760
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1132761
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132762
    iget-object v0, p0, LX/6jm;->appId:Ljava/lang/Long;

    iget-object v3, p1, LX/6jm;->appId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132763
    :cond_b
    iget-object v0, p0, LX/6jm;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1132764
    :goto_a
    iget-object v3, p1, LX/6jm;->timestampMs:Ljava/lang/Long;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1132765
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1132766
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132767
    iget-object v0, p0, LX/6jm;->timestampMs:Ljava/lang/Long;

    iget-object v3, p1, LX/6jm;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132768
    :cond_d
    iget-object v0, p0, LX/6jm;->messageIds:Ljava/util/List;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1132769
    :goto_c
    iget-object v3, p1, LX/6jm;->messageIds:Ljava/util/List;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1132770
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1132771
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132772
    iget-object v0, p0, LX/6jm;->messageIds:Ljava/util/List;

    iget-object v3, p1, LX/6jm;->messageIds:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132773
    :cond_f
    iget-object v0, p0, LX/6jm;->deliveredWatermarkTimestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1132774
    :goto_e
    iget-object v3, p1, LX/6jm;->deliveredWatermarkTimestampMs:Ljava/lang/Long;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1132775
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1132776
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132777
    iget-object v0, p0, LX/6jm;->deliveredWatermarkTimestampMs:Ljava/lang/Long;

    iget-object v3, p1, LX/6jm;->deliveredWatermarkTimestampMs:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_11
    move v2, v1

    .line 1132778
    goto/16 :goto_1

    :cond_12
    move v0, v2

    .line 1132779
    goto/16 :goto_2

    :cond_13
    move v3, v2

    .line 1132780
    goto/16 :goto_3

    :cond_14
    move v0, v2

    .line 1132781
    goto/16 :goto_4

    :cond_15
    move v3, v2

    .line 1132782
    goto/16 :goto_5

    :cond_16
    move v0, v2

    .line 1132783
    goto/16 :goto_6

    :cond_17
    move v3, v2

    .line 1132784
    goto/16 :goto_7

    :cond_18
    move v0, v2

    .line 1132785
    goto/16 :goto_8

    :cond_19
    move v3, v2

    .line 1132786
    goto :goto_9

    :cond_1a
    move v0, v2

    .line 1132787
    goto :goto_a

    :cond_1b
    move v3, v2

    .line 1132788
    goto :goto_b

    :cond_1c
    move v0, v2

    .line 1132789
    goto :goto_c

    :cond_1d
    move v3, v2

    .line 1132790
    goto :goto_d

    :cond_1e
    move v0, v2

    .line 1132791
    goto :goto_e

    :cond_1f
    move v3, v2

    .line 1132792
    goto :goto_f
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1132793
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1132794
    sget-boolean v0, LX/6jm;->a:Z

    .line 1132795
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jm;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1132796
    return-object v0
.end method
