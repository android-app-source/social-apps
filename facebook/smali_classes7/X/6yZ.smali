.class public final LX/6yZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/6yO;",
            "LX/6yP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/6yP;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1160125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160126
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1160127
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6yP;

    .line 1160128
    iget-object v3, v0, LX/6yP;->a:LX/6yO;

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1160129
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/6yZ;->a:LX/0P1;

    .line 1160130
    return-void
.end method

.method public static a(LX/0QB;)LX/6yZ;
    .locals 6

    .prologue
    .line 1160131
    const-class v1, LX/6yZ;

    monitor-enter v1

    .line 1160132
    :try_start_0
    sget-object v0, LX/6yZ;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1160133
    sput-object v2, LX/6yZ;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1160134
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1160135
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1160136
    new-instance v3, LX/6yZ;

    .line 1160137
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/6yV;

    invoke-direct {p0, v0}, LX/6yV;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 1160138
    invoke-direct {v3, v4}, LX/6yZ;-><init>(Ljava/util/Set;)V

    .line 1160139
    move-object v0, v3

    .line 1160140
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1160141
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6yZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1160142
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1160143
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(LX/6yO;)LX/6xu;
    .locals 2

    .prologue
    .line 1160144
    iget-object v0, p0, LX/6yZ;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6yZ;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6yP;

    iget-object v0, v0, LX/6yP;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    check-cast v0, LX/6xu;

    check-cast v0, LX/6xu;

    return-object v0

    :cond_0
    iget-object v0, p0, LX/6yZ;->a:LX/0P1;

    sget-object v1, LX/6yO;->SIMPLE:LX/6yO;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6yP;

    iget-object v0, v0, LX/6yP;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(LX/6yO;)LX/6y0;
    .locals 2

    .prologue
    .line 1160145
    iget-object v0, p0, LX/6yZ;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6yZ;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6yP;

    iget-object v0, v0, LX/6yP;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    check-cast v0, LX/6y0;

    check-cast v0, LX/6y0;

    return-object v0

    :cond_0
    iget-object v0, p0, LX/6yZ;->a:LX/0P1;

    sget-object v1, LX/6yO;->SIMPLE:LX/6yO;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6yP;

    iget-object v0, v0, LX/6yP;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final d(LX/6yO;)LX/6yT;
    .locals 2

    .prologue
    .line 1160146
    iget-object v0, p0, LX/6yZ;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6yZ;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6yP;

    iget-object v0, v0, LX/6yP;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    check-cast v0, LX/6yT;

    check-cast v0, LX/6yT;

    return-object v0

    :cond_0
    iget-object v0, p0, LX/6yZ;->a:LX/0P1;

    sget-object v1, LX/6yO;->SIMPLE:LX/6yO;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6yP;

    iget-object v0, v0, LX/6yP;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
