.class public LX/88r;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public a:Ljava/util/EnumSet;

.field public final b:LX/88t;

.field public final c:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/88p;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/1Ck;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1302983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1302984
    new-instance v0, LX/88t;

    invoke-direct {v0}, LX/88t;-><init>()V

    iput-object v0, p0, LX/88r;->b:LX/88t;

    .line 1302985
    sget-object v0, LX/88q;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->clone()Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LX/88r;->a:Ljava/util/EnumSet;

    .line 1302986
    iput-object p1, p0, LX/88r;->c:LX/1Ck;

    .line 1302987
    iput-object p2, p0, LX/88r;->d:Ljava/util/concurrent/ExecutorService;

    .line 1302988
    return-void
.end method

.method public static a(LX/0QB;)LX/88r;
    .locals 5

    .prologue
    .line 1302989
    const-class v1, LX/88r;

    monitor-enter v1

    .line 1302990
    :try_start_0
    sget-object v0, LX/88r;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1302991
    sput-object v2, LX/88r;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1302992
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1302993
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1302994
    new-instance p0, LX/88r;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v3, v4}, LX/88r;-><init>(LX/1Ck;Ljava/util/concurrent/ExecutorService;)V

    .line 1302995
    move-object v0, p0

    .line 1302996
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1302997
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/88r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1302998
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1302999
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/88r;)V
    .locals 5

    .prologue
    .line 1303000
    iget-object v0, p0, LX/88r;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1303001
    iget-object v0, p0, LX/88r;->b:LX/88t;

    invoke-virtual {v0}, LX/88t;->b()LX/88q;

    move-result-object v0

    .line 1303002
    :goto_0
    if-nez v0, :cond_1

    .line 1303003
    :goto_1
    return-void

    .line 1303004
    :cond_0
    iget-object v0, p0, LX/88r;->b:LX/88t;

    .line 1303005
    iget-object v1, v0, LX/88t;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/88q;

    move-object v0, v1

    .line 1303006
    goto :goto_0

    .line 1303007
    :cond_1
    iget-object v1, v0, LX/88q;->d:LX/88p;

    move-object v1, v1

    .line 1303008
    iget-object v2, p0, LX/88r;->c:LX/1Ck;

    .line 1303009
    iget-object v3, v0, LX/88q;->b:Ljava/util/concurrent/Callable;

    move-object v3, v3

    .line 1303010
    iget-object v4, v0, LX/88q;->c:LX/0Ve;

    move-object v0, v4

    .line 1303011
    invoke-virtual {v2, v1, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1303012
    iget-object v0, p0, LX/88r;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1303013
    iget-object v0, p0, LX/88r;->c:LX/1Ck;

    invoke-virtual {v0, v1}, LX/1Ck;->b(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v2, Lcom/facebook/groups/feed/executor/GroupsTaskExecutor$1;

    invoke-direct {v2, p0, v1}, Lcom/facebook/groups/feed/executor/GroupsTaskExecutor$1;-><init>(LX/88r;LX/88p;)V

    iget-object v1, p0, LX/88r;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v2, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 1303014
    :cond_2
    invoke-static {p0}, LX/88r;->b(LX/88r;)V

    goto :goto_1
.end method
