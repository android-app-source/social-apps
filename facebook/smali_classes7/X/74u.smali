.class public final LX/74u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/photos/base/photos/LocalPhoto;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1168644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1168645
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1168646
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1168647
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1168648
    const-class v0, Lcom/facebook/photos/base/tagging/Tag;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1168649
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1168650
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1168651
    :cond_0
    move-object v4, v3

    .line 1168652
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1168653
    const/4 v0, -0x1

    if-ne v3, v0, :cond_3

    .line 1168654
    const/4 v0, 0x0

    .line 1168655
    :goto_1
    move-object v5, v0

    .line 1168656
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1168657
    iget-object v2, v0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v2, v2

    .line 1168658
    if-eqz v5, :cond_1

    instance-of v3, v2, Lcom/facebook/photos/base/tagging/FaceBox;

    if-nez v3, :cond_5

    .line 1168659
    :cond_1
    :goto_3
    move-object v2, v2

    .line 1168660
    iput-object v2, v0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    .line 1168661
    goto :goto_2

    .line 1168662
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 1168663
    new-instance v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    const/4 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/photos/base/photos/LocalPhoto;-><init>(Landroid/os/Parcel;JLjava/util/List;Ljava/util/List;B)V

    return-object v0

    .line 1168664
    :cond_3
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1168665
    const/4 v0, 0x0

    move v2, v0

    :goto_4
    if-ge v2, v3, :cond_4

    .line 1168666
    const-class v0, Lcom/facebook/photos/base/tagging/FaceBox;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1168667
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1168668
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_4
    move-object v0, v1

    .line 1168669
    goto :goto_1

    :cond_5
    move-object v3, v2

    .line 1168670
    check-cast v3, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1168671
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1168672
    iget-object v8, v3, Lcom/facebook/photos/base/tagging/FaceBox;->a:Ljava/lang/String;

    move-object v8, v8

    .line 1168673
    iget-object p0, v6, Lcom/facebook/photos/base/tagging/FaceBox;->a:Ljava/lang/String;

    move-object p0, p0

    .line 1168674
    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    move-object v2, v6

    .line 1168675
    goto :goto_3
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1168676
    new-array v0, p1, [Lcom/facebook/photos/base/photos/LocalPhoto;

    return-object v0
.end method
