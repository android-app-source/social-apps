.class public LX/76U;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:LX/78A;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;LX/78A;)V
    .locals 0

    .prologue
    .line 1171054
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1171055
    iput-object p1, p0, LX/76U;->b:Ljava/lang/Runnable;

    .line 1171056
    iput-object p2, p0, LX/76U;->a:LX/78A;

    .line 1171057
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1171058
    iget-object v0, p0, LX/76U;->a:LX/78A;

    invoke-virtual {v0}, LX/78A;->a()V

    .line 1171059
    iget-object v0, p0, LX/76U;->a:LX/78A;

    new-instance v1, LX/77n;

    invoke-direct {v1}, LX/77n;-><init>()V

    invoke-virtual {v0, v1}, LX/78A;->a(LX/77n;)V

    .line 1171060
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 1171061
    iget-object v0, p0, LX/76U;->a:LX/78A;

    invoke-virtual {v0}, LX/78A;->b()V

    .line 1171062
    iget-object v0, p0, LX/76U;->a:LX/78A;

    invoke-virtual {v0}, LX/78A;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1171063
    invoke-virtual {p0}, LX/76U;->f()V

    .line 1171064
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1171065
    iget-object v0, p0, LX/76U;->a:LX/78A;

    invoke-virtual {v0}, LX/78A;->f()V

    .line 1171066
    iget-object v0, p0, LX/76U;->a:LX/78A;

    invoke-virtual {v0}, LX/78A;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1171067
    invoke-virtual {p0}, LX/76U;->f()V

    .line 1171068
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1171069
    iget-object v0, p0, LX/76U;->a:LX/78A;

    invoke-virtual {v0}, LX/78A;->i()V

    .line 1171070
    iget-object v0, p0, LX/76U;->a:LX/78A;

    invoke-virtual {v0}, LX/78A;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1171071
    invoke-virtual {p0}, LX/76U;->f()V

    .line 1171072
    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 1171073
    iget-object v0, p0, LX/76U;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1171074
    iget-object v0, p0, LX/76U;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1171075
    :cond_0
    return-void
.end method
