.class public interface abstract LX/7ja;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract b()Z
.end method

.method public abstract c()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract d()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract e()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ev_()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ew_()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsInterfaces$AdminCommerceProductItem$OrderedImages;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract j()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsInterfaces$AdminCommerceProductItem$ProductImagesLarge;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract k()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
