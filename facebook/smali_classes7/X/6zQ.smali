.class public final enum LX/6zQ;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6zP;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6zQ;",
        ">;",
        "LX/6zP;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6zQ;

.field public static final enum ALTPAY_ADYEN:LX/6zQ;

.field public static final enum NEW_CREDIT_CARD:LX/6zQ;

.field public static final enum NEW_MANUAL_TRANSFER:LX/6zQ;

.field public static final enum NEW_NET_BANKING:LX/6zQ;

.field public static final enum NEW_PAYPAL:LX/6zQ;

.field public static final enum NEW_PAY_OVER_COUNTER:LX/6zQ;

.field public static final enum UNKNOWN:LX/6zQ;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1161123
    new-instance v0, LX/6zQ;

    const-string v1, "NEW_CREDIT_CARD"

    const-string v2, "new_cc"

    invoke-direct {v0, v1, v4, v2}, LX/6zQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6zQ;->NEW_CREDIT_CARD:LX/6zQ;

    .line 1161124
    new-instance v0, LX/6zQ;

    const-string v1, "NEW_MANUAL_TRANSFER"

    const-string v2, "manual_transfer"

    invoke-direct {v0, v1, v5, v2}, LX/6zQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6zQ;->NEW_MANUAL_TRANSFER:LX/6zQ;

    .line 1161125
    new-instance v0, LX/6zQ;

    const-string v1, "NEW_NET_BANKING"

    const-string v2, "net_banking"

    invoke-direct {v0, v1, v6, v2}, LX/6zQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6zQ;->NEW_NET_BANKING:LX/6zQ;

    .line 1161126
    new-instance v0, LX/6zQ;

    const-string v1, "NEW_PAYPAL"

    const-string v2, "paypal"

    invoke-direct {v0, v1, v7, v2}, LX/6zQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6zQ;->NEW_PAYPAL:LX/6zQ;

    .line 1161127
    new-instance v0, LX/6zQ;

    const-string v1, "NEW_PAY_OVER_COUNTER"

    const-string v2, "pay_over_counter"

    invoke-direct {v0, v1, v8, v2}, LX/6zQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6zQ;->NEW_PAY_OVER_COUNTER:LX/6zQ;

    .line 1161128
    new-instance v0, LX/6zQ;

    const-string v1, "ALTPAY_ADYEN"

    const/4 v2, 0x5

    const-string v3, "adyen"

    invoke-direct {v0, v1, v2, v3}, LX/6zQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6zQ;->ALTPAY_ADYEN:LX/6zQ;

    .line 1161129
    new-instance v0, LX/6zQ;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x6

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/6zQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6zQ;->UNKNOWN:LX/6zQ;

    .line 1161130
    const/4 v0, 0x7

    new-array v0, v0, [LX/6zQ;

    sget-object v1, LX/6zQ;->NEW_CREDIT_CARD:LX/6zQ;

    aput-object v1, v0, v4

    sget-object v1, LX/6zQ;->NEW_MANUAL_TRANSFER:LX/6zQ;

    aput-object v1, v0, v5

    sget-object v1, LX/6zQ;->NEW_NET_BANKING:LX/6zQ;

    aput-object v1, v0, v6

    sget-object v1, LX/6zQ;->NEW_PAYPAL:LX/6zQ;

    aput-object v1, v0, v7

    sget-object v1, LX/6zQ;->NEW_PAY_OVER_COUNTER:LX/6zQ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/6zQ;->ALTPAY_ADYEN:LX/6zQ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6zQ;->UNKNOWN:LX/6zQ;

    aput-object v2, v0, v1

    sput-object v0, LX/6zQ;->$VALUES:[LX/6zQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1161131
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1161132
    iput-object p3, p0, LX/6zQ;->mValue:Ljava/lang/String;

    .line 1161133
    return-void
.end method

.method public static forValue(Ljava/lang/String;)LX/6zQ;
    .locals 2
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation

    .prologue
    .line 1161134
    invoke-static {}, LX/6zQ;->values()[LX/6zQ;

    move-result-object v0

    invoke-static {v0, p0}, LX/6LV;->a([LX/6LU;Ljava/lang/Object;)LX/6LU;

    move-result-object v0

    sget-object v1, LX/6zQ;->UNKNOWN:LX/6zQ;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6zQ;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6zQ;
    .locals 1

    .prologue
    .line 1161135
    const-class v0, LX/6zQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6zQ;

    return-object v0
.end method

.method public static values()[LX/6zQ;
    .locals 1

    .prologue
    .line 1161136
    sget-object v0, LX/6zQ;->$VALUES:[LX/6zQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6zQ;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1161137
    invoke-virtual {p0}, LX/6zQ;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1161138
    iget-object v0, p0, LX/6zQ;->mValue:Ljava/lang/String;

    return-object v0
.end method
