.class public LX/6rw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6rs;


# direct methods
.method public constructor <init>(LX/6rs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1152793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152794
    iput-object p1, p0, LX/6rw;->a:LX/6rs;

    .line 1152795
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1152796
    const-string v0, "payment_info"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152797
    iget-object v0, p0, LX/6rw;->a:LX/6rs;

    .line 1152798
    iget-object v1, v0, LX/6rs;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6rr;

    move-object v0, v1

    .line 1152799
    const-string v1, "payment_info"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;

    .line 1152800
    const-string v1, "order_status_model"

    invoke-virtual {p2, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1152801
    iget-object v1, p0, LX/6rw;->a:LX/6rs;

    .line 1152802
    iget-object v2, v1, LX/6rs;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6rr;

    move-object v1, v2

    .line 1152803
    const-string v2, "order_status_model"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-interface {v1, p1, v2}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6re;

    .line 1152804
    iget-object v2, p0, LX/6rw;->a:LX/6rs;

    invoke-virtual {v2, p1}, LX/6rs;->d(Ljava/lang/String;)LX/6rr;

    move-result-object v2

    const-string v3, "content_configuration"

    invoke-virtual {p2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-interface {v2, p1, v3}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    .line 1152805
    new-instance v3, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;

    invoke-direct {v3, p1, v0, v1, v2}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;-><init>(Ljava/lang/String;Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;LX/6re;Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;)V

    return-object v3
.end method
