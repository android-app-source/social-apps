.class public final LX/7JI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/38g;

.field private final b:LX/2wX;

.field public final c:Ljava/lang/String;

.field public final d:LX/7JH;

.field private final e:LX/0SF;

.field private f:I

.field public g:I

.field public h:D

.field public i:D

.field public j:Ljava/lang/String;

.field public k:J


# direct methods
.method public constructor <init>(LX/38g;LX/0SF;LX/2wX;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1193368
    iput-object p1, p0, LX/7JI;->a:LX/38g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1193369
    iput-object p2, p0, LX/7JI;->e:LX/0SF;

    .line 1193370
    iput-object p3, p0, LX/7JI;->b:LX/2wX;

    .line 1193371
    iput-object p4, p0, LX/7JI;->c:Ljava/lang/String;

    .line 1193372
    new-instance v0, LX/7JH;

    invoke-direct {v0, p0}, LX/7JH;-><init>(LX/7JI;)V

    iput-object v0, p0, LX/7JI;->d:LX/7JH;

    .line 1193373
    const/4 v0, 0x1

    iput v0, p0, LX/7JI;->f:I

    .line 1193374
    invoke-static {p0, v1}, LX/7JI;->a(LX/7JI;I)V

    .line 1193375
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/7JI;->h:D

    .line 1193376
    return-void
.end method

.method public static a(LX/7JI;I)V
    .locals 2

    .prologue
    .line 1193361
    iget v0, p0, LX/7JI;->g:I

    if-ne p1, v0, :cond_0

    .line 1193362
    :goto_0
    return-void

    .line 1193363
    :cond_0
    iget v0, p0, LX/7JI;->g:I

    .line 1193364
    iput p1, p0, LX/7JI;->g:I

    .line 1193365
    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    iget v0, p0, LX/7JI;->g:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 1193366
    iget-object v0, p0, LX/7JI;->a:LX/38g;

    iget-object v0, v0, LX/38g;->h:LX/38i;

    invoke-virtual {v0}, LX/38i;->b()V

    .line 1193367
    :cond_1
    iget-object v0, p0, LX/7JI;->a:LX/38g;

    iget-object v0, v0, LX/38g;->h:LX/38i;

    invoke-virtual {v0}, LX/38i;->d()V

    goto :goto_0
.end method

.method public static a(LX/7JI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/27U;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/27U",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1193355
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1193356
    :goto_0
    invoke-static {p0, p1, p2, v0, p4}, LX/7JI;->a(LX/7JI;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;LX/27U;)V

    .line 1193357
    return-void

    .line 1193358
    :catch_0
    move-exception v0

    .line 1193359
    iget-object v1, p0, LX/7JI;->a:LX/38g;

    iget-object v1, v1, LX/38g;->c:LX/37e;

    sget-object v2, LX/7JJ;->FbAppPlayer_SendMessage:LX/7JJ;

    invoke-virtual {v1, v2, v0}, LX/37e;->a(LX/7JJ;Ljava/lang/Exception;)V

    .line 1193360
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/7JI;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;LX/27U;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            "LX/27U",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1193341
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1193342
    const-string v1, "type"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1193343
    const-string v1, "target"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1193344
    const-string v1, "num"

    iget v2, p0, LX/7JI;->f:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/7JI;->f:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1193345
    const-string v1, "timestamp"

    iget-object v2, p0, LX/7JI;->e:LX/0SF;

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1193346
    const-string v1, "data"

    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1193347
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1193348
    :goto_0
    :try_start_1
    sget-object v1, LX/7Yq;->c:LX/7Yl;

    iget-object v2, p0, LX/7JI;->b:LX/2wX;

    iget-object v3, p0, LX/7JI;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, LX/7Yl;->a(LX/2wX;Ljava/lang/String;Ljava/lang/String;)LX/2wg;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/2wg;->a(LX/27U;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1193349
    :goto_1
    return-void

    .line 1193350
    :catch_0
    move-exception v0

    .line 1193351
    iget-object v1, p0, LX/7JI;->a:LX/38g;

    iget-object v1, v1, LX/38g;->c:LX/37e;

    sget-object v2, LX/7JJ;->FbAppPlayer_SendMessage:LX/7JJ;

    invoke-virtual {v1, v2, v0}, LX/37e;->a(LX/7JJ;Ljava/lang/Exception;)V

    .line 1193352
    const-string v0, "{}"

    goto :goto_0

    .line 1193353
    :catch_1
    move-exception v0

    .line 1193354
    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0xd

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to send message due to: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    invoke-interface {p4, v1}, LX/27U;->a(LX/2NW;)V

    goto :goto_1
.end method

.method public static a$redex0(LX/7JI;Lorg/json/JSONObject;)V
    .locals 6

    .prologue
    .line 1193310
    const-string v0, ""

    .line 1193311
    const-string v1, "state"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1193312
    const-string v0, "state"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1193313
    :cond_0
    iget-wide v2, p0, LX/7JI;->h:D

    .line 1193314
    const-wide/16 v4, 0x0

    iput-wide v4, p0, LX/7JI;->h:D

    .line 1193315
    const-string v1, "position"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "position"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1193316
    const-string v1, "position"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, p0, LX/7JI;->h:D

    .line 1193317
    :cond_1
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_2
    move v0, v1

    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1193318
    :goto_1
    iget-wide v0, p0, LX/7JI;->h:D

    cmpl-double v0, v2, v0

    if-eqz v0, :cond_3

    .line 1193319
    iget-object v0, p0, LX/7JI;->a:LX/38g;

    iget-object v0, v0, LX/38g;->h:LX/38i;

    invoke-virtual {v0}, LX/38i;->e()V

    .line 1193320
    :cond_3
    return-void

    .line 1193321
    :sswitch_0
    const-string v4, "ended"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v4, "play"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v4, "paused"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    .line 1193322
    :pswitch_0
    const/4 v0, 0x3

    invoke-static {p0, v0}, LX/7JI;->a(LX/7JI;I)V

    .line 1193323
    iget-object v0, p0, LX/7JI;->a:LX/38g;

    iget-object v0, v0, LX/38g;->h:LX/38i;

    invoke-virtual {v0}, LX/38i;->f()V

    goto :goto_1

    .line 1193324
    :pswitch_1
    const/4 v0, 0x6

    invoke-static {p0, v0}, LX/7JI;->a(LX/7JI;I)V

    goto :goto_1

    .line 1193325
    :pswitch_2
    const/4 v0, 0x7

    invoke-static {p0, v0}, LX/7JI;->a(LX/7JI;I)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x3b5366d2 -> :sswitch_2
        0x348b34 -> :sswitch_1
        0x5c2caba -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final g()V
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v8, 0x5

    .line 1193326
    iget v0, p0, LX/7JI;->g:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/7JI;->g:I

    if-eq v0, v8, :cond_0

    iget v0, p0, LX/7JI;->g:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    .line 1193327
    iget-object v0, p0, LX/7JI;->a:LX/38g;

    iget-object v0, v0, LX/38g;->c:LX/37e;

    sget-object v1, LX/7JJ;->FbAppPlayer_StartPlaying:LX/7JJ;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Incorrect state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, LX/7JI;->g:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/37e;->a(LX/7JJ;Ljava/lang/String;)V

    .line 1193328
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1193329
    const-string v1, "cmd"

    const-string v2, "play_video"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1193330
    iget-wide v2, p0, LX/7JI;->k:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 1193331
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1193332
    const-string v2, "position"

    iget-wide v4, p0, LX/7JI;->k:J

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 1193333
    const-string v2, "params"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1193334
    :cond_1
    const-wide/16 v2, 0x0

    iput-wide v2, p0, LX/7JI;->k:J
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1193335
    invoke-static {p0, v8}, LX/7JI;->a(LX/7JI;I)V

    .line 1193336
    const-string v1, "experience_command"

    iget-object v2, p0, LX/7JI;->j:Ljava/lang/String;

    new-instance v3, LX/7JC;

    invoke-direct {v3, p0}, LX/7JC;-><init>(LX/7JI;)V

    invoke-static {p0, v1, v2, v0, v3}, LX/7JI;->a(LX/7JI;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;LX/27U;)V

    .line 1193337
    :goto_0
    return-void

    .line 1193338
    :catch_0
    move-exception v0

    .line 1193339
    iget-object v1, p0, LX/7JI;->a:LX/38g;

    iget-object v1, v1, LX/38g;->d:LX/37f;

    const/16 v2, 0xd

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FbAppPlayer.startPlaying(): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/37f;->a(ILjava/lang/String;)V

    .line 1193340
    iget-object v0, p0, LX/7JI;->a:LX/38g;

    iget-object v0, v0, LX/38g;->h:LX/38i;

    invoke-virtual {v0}, LX/38i;->c()V

    goto :goto_0
.end method
