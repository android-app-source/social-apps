.class public LX/8YX;
.super LX/2Vx;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2Vx",
        "<",
        "LX/8YY;",
        "[B>;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>(LX/0SG;LX/0pi;LX/1Gm;LX/03V;LX/0rb;LX/1Ha;LX/1GQ;)V
    .locals 10
    .param p5    # LX/0rb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/1Ha;
        .annotation build Lcom/facebook/richdocument/fonts/FontFileCache;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1356314
    invoke-static {}, LX/8YX;->b()LX/2W2;

    move-result-object v5

    invoke-static {}, LX/8YX;->a()LX/2W4;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, LX/2Vx;-><init>(LX/0SG;LX/0pi;LX/1Gm;LX/03V;LX/2W2;LX/0rb;LX/1Ha;LX/2W4;LX/1GQ;)V

    .line 1356315
    return-void
.end method

.method private static a()LX/2W4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2W4",
            "<",
            "LX/8YY;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 1356316
    new-instance v0, LX/8YW;

    invoke-direct {v0}, LX/8YW;-><init>()V

    return-object v0
.end method

.method public static a(LX/0QB;)LX/8YX;
    .locals 11

    .prologue
    .line 1356317
    const-class v1, LX/8YX;

    monitor-enter v1

    .line 1356318
    :try_start_0
    sget-object v0, LX/8YX;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1356319
    sput-object v2, LX/8YX;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1356320
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1356321
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1356322
    new-instance v3, LX/8YX;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v5

    check-cast v5, LX/0pi;

    invoke-static {v0}, LX/1Gm;->a(LX/0QB;)LX/1Gm;

    move-result-object v6

    check-cast v6, LX/1Gm;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v8

    check-cast v8, LX/0rb;

    invoke-static {v0}, LX/8YV;->a(LX/0QB;)LX/1Ha;

    move-result-object v9

    check-cast v9, LX/1Ha;

    invoke-static {v0}, LX/1GP;->a(LX/0QB;)LX/1GP;

    move-result-object v10

    check-cast v10, LX/1GQ;

    invoke-direct/range {v3 .. v10}, LX/8YX;-><init>(LX/0SG;LX/0pi;LX/1Gm;LX/03V;LX/0rb;LX/1Ha;LX/1GQ;)V

    .line 1356323
    move-object v0, v3

    .line 1356324
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1356325
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8YX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1356326
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1356327
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b()LX/2W2;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1356328
    new-instance v0, LX/2W2;

    invoke-direct {v0}, LX/2W2;-><init>()V

    const-string v1, "custom_fonts"

    .line 1356329
    iput-object v1, v0, LX/2W2;->a:Ljava/lang/String;

    .line 1356330
    move-object v0, v0

    .line 1356331
    const-string v1, "custom_fonts"

    .line 1356332
    iput-object v1, v0, LX/2W2;->b:Ljava/lang/String;

    .line 1356333
    move-object v0, v0

    .line 1356334
    iput-boolean v2, v0, LX/2W2;->c:Z

    .line 1356335
    move-object v0, v0

    .line 1356336
    const/16 v1, 0x64

    .line 1356337
    iput v1, v0, LX/2W2;->f:I

    .line 1356338
    move-object v0, v0

    .line 1356339
    iput v2, v0, LX/2W2;->d:I

    .line 1356340
    move-object v0, v0

    .line 1356341
    iput v2, v0, LX/2W2;->e:I

    .line 1356342
    move-object v0, v0

    .line 1356343
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1356344
    check-cast p1, [B

    .line 1356345
    array-length v0, p1

    return v0
.end method

.method public final a(Ljava/util/Set;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/8YY;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "LX/8YY;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1356346
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1356347
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8YY;

    .line 1356348
    invoke-virtual {p0, v0}, LX/2Vx;->a(LX/2WG;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1356349
    invoke-virtual {p0, v0}, LX/2Vx;->c(LX/2WG;)LX/1gI;

    move-result-object v1

    check-cast v1, LX/1gH;

    .line 1356350
    if-eqz v1, :cond_0

    .line 1356351
    iget-object v4, v1, LX/1gH;->a:Ljava/io/File;

    move-object v1, v4

    .line 1356352
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1356353
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1356354
    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1356355
    :cond_1
    return-object v2
.end method
