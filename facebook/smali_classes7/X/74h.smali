.class public LX/74h;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private final c:Landroid/content/ContentResolver;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0W3;

.field private final f:LX/0ad;

.field public final g:LX/7Dh;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Qx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1168351
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "mime_type"

    aput-object v1, v0, v5

    const-string v1, "_display_name"

    aput-object v1, v0, v6

    const-string v1, "width"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "height"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "mini_thumb_magic"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "orientation"

    aput-object v2, v0, v1

    sput-object v0, LX/74h;->a:[Ljava/lang/String;

    .line 1168352
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "mime_type"

    aput-object v1, v0, v5

    const-string v1, "_display_name"

    aput-object v1, v0, v6

    const-string v1, "width"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "height"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "mini_thumb_magic"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "duration"

    aput-object v2, v0, v1

    sput-object v0, LX/74h;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/0Ot;LX/0ad;LX/7Dh;LX/0Ot;LX/0W3;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0ad;",
            "LX/7Dh;",
            "LX/0Ot",
            "<",
            "LX/2Qx;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1168343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1168344
    iput-object p1, p0, LX/74h;->c:Landroid/content/ContentResolver;

    .line 1168345
    iput-object p2, p0, LX/74h;->d:LX/0Ot;

    .line 1168346
    iput-object p3, p0, LX/74h;->f:LX/0ad;

    .line 1168347
    iput-object p4, p0, LX/74h;->g:LX/7Dh;

    .line 1168348
    iput-object p5, p0, LX/74h;->h:LX/0Ot;

    .line 1168349
    iput-object p6, p0, LX/74h;->e:LX/0W3;

    .line 1168350
    return-void
.end method

.method private static a(LX/4gQ;JLjava/lang/String;Lcom/facebook/ipc/media/data/MimeType;ILX/434;Landroid/database/Cursor;)Lcom/facebook/ipc/media/data/LocalMediaData;
    .locals 6

    .prologue
    .line 1168321
    new-instance v0, LX/4gP;

    invoke-direct {v0}, LX/4gP;-><init>()V

    new-instance v1, Lcom/facebook/ipc/media/MediaIdKey;

    invoke-direct {v1, p3, p1, p2}, Lcom/facebook/ipc/media/MediaIdKey;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/4gP;->a(Lcom/facebook/ipc/media/data/MimeType;)LX/4gP;

    move-result-object v0

    .line 1168322
    iput p5, v0, LX/4gP;->e:I

    .line 1168323
    move-object v0, v0

    .line 1168324
    iget v1, p6, LX/434;->b:I

    .line 1168325
    iput v1, v0, LX/4gP;->f:I

    .line 1168326
    move-object v0, v0

    .line 1168327
    iget v1, p6, LX/434;->a:I

    .line 1168328
    iput v1, v0, LX/4gP;->g:I

    .line 1168329
    move-object v0, v0

    .line 1168330
    iget v1, p6, LX/434;->b:I

    iget v2, p6, LX/434;->a:I

    invoke-static {v1, v2, p5}, LX/74d;->a(III)F

    move-result v1

    .line 1168331
    iput v1, v0, LX/4gP;->h:F

    .line 1168332
    move-object v0, v0

    .line 1168333
    const/4 v1, 0x7

    invoke-interface {p7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/4gP;->a(D)LX/4gP;

    move-result-object v0

    const/16 v1, 0x8

    invoke-interface {p7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/4gP;->b(D)LX/4gP;

    move-result-object v0

    invoke-virtual {v0}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1168334
    new-instance v1, LX/4gN;

    invoke-direct {v1}, LX/4gN;-><init>()V

    invoke-virtual {v1, v0}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v0

    const/4 v1, 0x6

    invoke-interface {p7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1168335
    iput-wide v2, v0, LX/4gN;->b:J

    .line 1168336
    move-object v0, v0

    .line 1168337
    iput-wide p1, v0, LX/4gN;->d:J

    .line 1168338
    move-object v0, v0

    .line 1168339
    const/4 v1, 0x3

    invoke-interface {p7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1168340
    if-eqz v1, :cond_0

    .line 1168341
    invoke-virtual {v0, v1}, LX/4gN;->a(Ljava/lang/String;)LX/4gN;

    .line 1168342
    :cond_0
    invoke-virtual {v0}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/74h;JLcom/facebook/ipc/media/data/MimeType;Landroid/database/Cursor;)Lcom/facebook/ipc/media/data/LocalMediaData;
    .locals 9

    .prologue
    .line 1168310
    sget-object v0, Lcom/facebook/ipc/media/data/MimeType;->d:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v0, p3}, Lcom/facebook/ipc/media/data/MimeType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v1, LX/4gQ;->Video:LX/4gQ;

    .line 1168311
    :goto_0
    const/4 v0, 0x1

    invoke-interface {p4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1168312
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1168313
    new-instance v0, LX/74g;

    invoke-direct {v0}, LX/74g;-><init>()V

    throw v0

    .line 1168314
    :cond_0
    sget-object v1, LX/4gQ;->Photo:LX/4gQ;

    goto :goto_0

    .line 1168315
    :cond_1
    const/16 v0, 0xa

    invoke-interface {p4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 1168316
    invoke-direct {p0, p4}, LX/74h;->c(Landroid/database/Cursor;)LX/434;

    move-result-object v7

    .line 1168317
    if-nez v7, :cond_2

    .line 1168318
    iget-object v0, p0, LX/74h;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v4}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v0

    move-object v7, v0

    .line 1168319
    :cond_2
    move-wide v2, p1

    move-object v5, p3

    move-object v8, p4

    .line 1168320
    invoke-static/range {v1 .. v8}, LX/74h;->a(LX/4gQ;JLjava/lang/String;Lcom/facebook/ipc/media/data/MimeType;ILX/434;Landroid/database/Cursor;)Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)I
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1168306
    if-nez p0, :cond_1

    .line 1168307
    :cond_0
    :goto_0
    return v0

    .line 1168308
    :cond_1
    invoke-static {p0}, LX/0a4;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 1168309
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/74h;
    .locals 7

    .prologue
    .line 1168304
    new-instance v0, LX/74h;

    invoke-static {p0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v1

    check-cast v1, Landroid/content/ContentResolver;

    const/16 v2, 0x259

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {p0}, LX/7Dh;->a(LX/0QB;)LX/7Dh;

    move-result-object v4

    check-cast v4, LX/7Dh;

    const/16 v5, 0x1d4

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v6

    check-cast v6, LX/0W3;

    invoke-direct/range {v0 .. v6}, LX/74h;-><init>(Landroid/content/ContentResolver;LX/0Ot;LX/0ad;LX/7Dh;LX/0Ot;LX/0W3;)V

    .line 1168305
    return-object v0
.end method

.method public static b(LX/74h;JLcom/facebook/ipc/media/data/MimeType;Landroid/database/Cursor;)Lcom/facebook/ipc/media/data/LocalMediaData;
    .locals 9

    .prologue
    .line 1168208
    const/4 v0, 0x1

    invoke-interface {p4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1168209
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1168210
    new-instance v0, LX/74g;

    invoke-direct {v0}, LX/74g;-><init>()V

    throw v0

    .line 1168211
    :cond_0
    invoke-direct {p0, p4}, LX/74h;->c(Landroid/database/Cursor;)LX/434;

    move-result-object v7

    .line 1168212
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1168213
    :try_start_0
    invoke-virtual {v1, v4}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 1168214
    const/16 v0, 0x18

    invoke-virtual {v1, v0}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/74h;->b(Ljava/lang/String;)I

    move-result v6

    .line 1168215
    if-nez v7, :cond_1

    .line 1168216
    const/16 v0, 0x12

    invoke-virtual {v1, v0}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/74h;->b(Ljava/lang/String;)I

    move-result v0

    .line 1168217
    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/74h;->b(Ljava/lang/String;)I

    move-result v2

    .line 1168218
    new-instance v3, LX/434;

    invoke-direct {v3, v0, v2}, LX/434;-><init>(II)V

    move-object v7, v3
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1168219
    :cond_1
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 1168220
    sget-object v1, LX/4gQ;->Video:LX/4gQ;

    move-wide v2, p1

    move-object v5, p3

    move-object v8, p4

    invoke-static/range {v1 .. v8}, LX/74h;->a(LX/4gQ;JLjava/lang/String;Lcom/facebook/ipc/media/data/MimeType;ILX/434;Landroid/database/Cursor;)Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    return-object v0

    .line 1168221
    :catch_0
    move-exception v0

    .line 1168222
    :try_start_1
    new-instance v2, LX/74e;

    invoke-direct {v2, v0}, LX/74e;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1168223
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v0
.end method

.method private c(Landroid/database/Cursor;)LX/434;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1168295
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_2

    .line 1168296
    const/4 v0, 0x4

    :try_start_0
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1168297
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1168298
    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    :cond_0
    move-object v0, v1

    .line 1168299
    :goto_0
    return-object v0

    .line 1168300
    :cond_1
    new-instance v0, LX/434;

    invoke-direct {v0, v2, v3}, LX/434;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1168301
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 1168302
    iget-object v0, p0, LX/74h;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "MediaItemCursorUtil"

    const-string v4, "getDimensionsFromMediaStore failed"

    invoke-virtual {v0, v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    move-object v0, v1

    .line 1168303
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1168290
    :try_start_0
    iget-object v0, p0, LX/74h;->c:Landroid/content/ContentResolver;

    sget-object v2, LX/74h;->a:[Ljava/lang/String;

    const-string v5, ""

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1168291
    :goto_0
    return-object v0

    .line 1168292
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1168293
    iget-object v0, p0, LX/74h;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "MediaItemCursorUtil"

    const-string v3, "createImageCursor: error creating cursor"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1168294
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1168285
    :try_start_0
    iget-object v0, p0, LX/74h;->c:Landroid/content/ContentResolver;

    sget-object v2, LX/74h;->b:[Ljava/lang/String;

    const-string v5, ""

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1168286
    :goto_0
    return-object v0

    .line 1168287
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1168288
    iget-object v0, p0, LX/74h;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "MediaItemCursorUtil"

    const-string v3, "createVideoCursor: error creating cursor"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1168289
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/database/Cursor;)Lcom/facebook/ipc/media/MediaItem;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1168224
    const-string v0, "mime_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/ipc/media/data/MimeType;->a(Ljava/lang/String;)Lcom/facebook/ipc/media/data/MimeType;

    move-result-object v0

    .line 1168225
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MimeType;->mRawType:Ljava/lang/String;

    move-object v1, v1

    .line 1168226
    invoke-static {v1}, LX/46I;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1168227
    const/4 v2, 0x0

    :try_start_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1168228
    invoke-static {p0, v2, v3, v0, p1}, LX/74h;->b(LX/74h;JLcom/facebook/ipc/media/data/MimeType;Landroid/database/Cursor;)Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v2

    .line 1168229
    new-instance v3, LX/74m;

    invoke-direct {v3}, LX/74m;-><init>()V

    .line 1168230
    iput-object v2, v3, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1168231
    move-object v2, v3

    .line 1168232
    const/16 v3, 0x9

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1168233
    iput-wide v4, v2, LX/74m;->a:J

    .line 1168234
    move-object v2, v2

    .line 1168235
    const/16 v3, 0xa

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1168236
    iput-wide v4, v2, LX/74m;->d:J

    .line 1168237
    move-object v2, v2

    .line 1168238
    invoke-virtual {v2}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;
    :try_end_0
    .catch LX/74e; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/74g; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 1168239
    :goto_0
    move-object v0, v2

    .line 1168240
    :goto_1
    return-object v0

    .line 1168241
    :cond_0
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1168242
    invoke-static {p0, v2, v3, v0, p1}, LX/74h;->a(LX/74h;JLcom/facebook/ipc/media/data/MimeType;Landroid/database/Cursor;)Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v2

    .line 1168243
    invoke-virtual {v2}, Lcom/facebook/ipc/media/data/LocalMediaData;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    .line 1168244
    iget-object v4, v3, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v3, v4

    .line 1168245
    sget-object v4, LX/4gQ;->Video:LX/4gQ;

    if-ne v3, v4, :cond_1

    .line 1168246
    new-instance v3, LX/74m;

    invoke-direct {v3}, LX/74m;-><init>()V

    .line 1168247
    iput-object v2, v3, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1168248
    move-object v2, v3

    .line 1168249
    const/16 v3, 0x9

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1168250
    iput-wide v4, v2, LX/74m;->a:J

    .line 1168251
    move-object v2, v2

    .line 1168252
    invoke-virtual {v2}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v2
    :try_end_1
    .catch LX/74g; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_4

    .line 1168253
    :goto_2
    move-object v0, v2

    .line 1168254
    goto :goto_1

    .line 1168255
    :catch_0
    move-exception v2

    .line 1168256
    :goto_3
    const-string v3, "MediaItemCursorUtil"

    const-string v4, "createAsVideoItem"

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1168257
    :goto_4
    const/4 v2, 0x0

    goto :goto_0

    .line 1168258
    :catch_1
    move-exception v2

    move-object v3, v2

    .line 1168259
    iget-object v2, p0, LX/74h;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v4, "MediaItemCursorUtil"

    const-string v5, "createAsVideoItem"

    invoke-virtual {v2, v4, v5, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 1168260
    :catch_2
    move-exception v2

    goto :goto_3

    .line 1168261
    :cond_1
    :try_start_2
    new-instance v3, LX/74k;

    invoke-direct {v3}, LX/74k;-><init>()V

    .line 1168262
    iput-object v2, v3, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1168263
    move-object v3, v3

    .line 1168264
    const/16 v4, 0x9

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1168265
    iput-wide v4, v3, LX/74k;->b:J

    .line 1168266
    move-object v3, v3

    .line 1168267
    iget-object v4, p0, LX/74h;->g:LX/7Dh;

    invoke-virtual {v4}, LX/7Dh;->b()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1168268
    invoke-virtual {v2}, Lcom/facebook/ipc/media/data/LocalMediaData;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v2

    .line 1168269
    iget-object v4, v2, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v2, v4

    .line 1168270
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, LX/74h;->e:LX/0W3;

    invoke-static {v2, v4}, LX/7E2;->a(Ljava/lang/String;LX/0W3;)LX/7E1;

    move-result-object v2

    .line 1168271
    iget-boolean v4, v2, LX/7E1;->b:Z

    move v4, v4

    .line 1168272
    if-eqz v4, :cond_2

    .line 1168273
    const/4 v4, 0x1

    .line 1168274
    iput-boolean v4, v3, LX/74k;->c:Z

    .line 1168275
    iget-object v4, v2, LX/7E1;->a:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-object v2, v4

    .line 1168276
    iput-object v2, v3, LX/74k;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    .line 1168277
    :cond_2
    const/4 v2, 0x1

    .line 1168278
    iput-boolean v2, v3, LX/74k;->d:Z

    .line 1168279
    :cond_3
    invoke-virtual {v3}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;
    :try_end_2
    .catch LX/74g; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v2

    goto :goto_2

    .line 1168280
    :catch_3
    move-exception v2

    move-object v3, v2

    .line 1168281
    iget-object v2, p0, LX/74h;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v4, "MediaItemCursorUtil"

    const-string v5, "createAsPhotoItem"

    invoke-virtual {v2, v4, v5, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1168282
    :goto_5
    const/4 v2, 0x0

    goto :goto_2

    .line 1168283
    :catch_4
    move-exception v2

    move-object v3, v2

    .line 1168284
    iget-object v2, p0, LX/74h;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v4, "MediaItemCursorUtil"

    const-string v5, "createAsPhotoItem"

    invoke-virtual {v2, v4, v5, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5
.end method
