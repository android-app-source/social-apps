.class public final enum LX/77w;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/77w;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/77w;

.field public static final enum PRIMARY:LX/77w;

.field public static final enum PROMO:LX/77w;

.field public static final enum SPECIAL:LX/77w;


# instance fields
.field public final backgroundResId:I

.field public final textColorResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1172273
    new-instance v0, LX/77w;

    const-string v1, "PRIMARY"

    const v2, 0x7f021575

    const v3, 0x7f0a00d2

    invoke-direct {v0, v1, v4, v2, v3}, LX/77w;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/77w;->PRIMARY:LX/77w;

    .line 1172274
    new-instance v0, LX/77w;

    const-string v1, "SPECIAL"

    const v2, 0x7f021577

    const v3, 0x7f0a00d5

    invoke-direct {v0, v1, v5, v2, v3}, LX/77w;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/77w;->SPECIAL:LX/77w;

    .line 1172275
    new-instance v0, LX/77w;

    const-string v1, "PROMO"

    const v2, 0x7f021576

    const v3, 0x7f0a00d5

    invoke-direct {v0, v1, v6, v2, v3}, LX/77w;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/77w;->PROMO:LX/77w;

    .line 1172276
    const/4 v0, 0x3

    new-array v0, v0, [LX/77w;

    sget-object v1, LX/77w;->PRIMARY:LX/77w;

    aput-object v1, v0, v4

    sget-object v1, LX/77w;->SPECIAL:LX/77w;

    aput-object v1, v0, v5

    sget-object v1, LX/77w;->PROMO:LX/77w;

    aput-object v1, v0, v6

    sput-object v0, LX/77w;->$VALUES:[LX/77w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1172277
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1172278
    iput p3, p0, LX/77w;->backgroundResId:I

    .line 1172279
    iput p4, p0, LX/77w;->textColorResId:I

    .line 1172280
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/77w;
    .locals 1

    .prologue
    .line 1172281
    const-class v0, LX/77w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/77w;

    return-object v0
.end method

.method public static values()[LX/77w;
    .locals 1

    .prologue
    .line 1172282
    sget-object v0, LX/77w;->$VALUES:[LX/77w;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/77w;

    return-object v0
.end method
