.class public final LX/8XL;
.super LX/2s5;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;

.field private b:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;

.field private c:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

.field private d:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;LX/0gc;)V
    .locals 0

    .prologue
    .line 1354945
    iput-object p1, p0, LX/8XL;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;

    .line 1354946
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 1354947
    return-void
.end method

.method private d()Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;
    .locals 2

    .prologue
    .line 1354953
    iget-object v0, p0, LX/8XL;->b:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;

    if-nez v0, :cond_0

    .line 1354954
    new-instance v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;

    invoke-direct {v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;-><init>()V

    iput-object v0, p0, LX/8XL;->b:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;

    .line 1354955
    iget-object v0, p0, LX/8XL;->b:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;

    new-instance v1, LX/8XI;

    invoke-direct {v1, p0}, LX/8XI;-><init>(LX/8XL;)V

    .line 1354956
    iput-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->e:LX/8XI;

    .line 1354957
    :cond_0
    iget-object v0, p0, LX/8XL;->b:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;

    return-object v0
.end method

.method private e()Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;
    .locals 2

    .prologue
    .line 1354850
    iget-object v0, p0, LX/8XL;->d:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    if-nez v0, :cond_0

    .line 1354851
    new-instance v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    invoke-direct {v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;-><init>()V

    iput-object v0, p0, LX/8XL;->d:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    .line 1354852
    iget-object v0, p0, LX/8XL;->d:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    new-instance v1, LX/8XJ;

    invoke-direct {v1, p0}, LX/8XJ;-><init>(LX/8XL;)V

    .line 1354853
    iput-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->k:LX/8XJ;

    .line 1354854
    :cond_0
    iget-object v0, p0, LX/8XL;->d:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    return-object v0
.end method

.method public static f(LX/8XL;)Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;
    .locals 2

    .prologue
    .line 1354948
    iget-object v0, p0, LX/8XL;->c:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

    if-nez v0, :cond_0

    .line 1354949
    new-instance v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

    invoke-direct {v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;-><init>()V

    iput-object v0, p0, LX/8XL;->c:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

    .line 1354950
    iget-object v0, p0, LX/8XL;->c:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

    new-instance v1, LX/8XK;

    invoke-direct {v1, p0}, LX/8XK;-><init>(LX/8XL;)V

    .line 1354951
    iput-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->l:LX/8XK;

    .line 1354952
    :cond_0
    iget-object v0, p0, LX/8XL;->c:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

    return-object v0
.end method

.method public static g(LX/8XL;)V
    .locals 7

    .prologue
    .line 1354871
    invoke-direct {p0}, LX/8XL;->e()Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    move-result-object v0

    const/4 v2, 0x0

    .line 1354872
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->i:Lcom/facebook/widget/FbScrollView;

    invoke-virtual {v1, v2, v2}, Lcom/facebook/widget/FbScrollView;->scrollTo(II)V

    .line 1354873
    invoke-direct {p0}, LX/8XL;->d()Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;

    move-result-object v0

    const/4 v2, 0x0

    .line 1354874
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->h:Lcom/facebook/widget/FbScrollView;

    invoke-virtual {v1, v2, v2}, Lcom/facebook/widget/FbScrollView;->scrollTo(II)V

    .line 1354875
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->setVisibility(I)V

    .line 1354876
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->f:LX/8Wb;

    .line 1354877
    iget-object v2, v1, LX/8Wb;->a:LX/8Wa;

    iget-object v2, v2, LX/8Wa;->c:Landroid/widget/TextView;

    iget-object v3, v1, LX/8Wb;->d:LX/8TA;

    .line 1354878
    iget-object v4, v3, LX/8TA;->c:Landroid/text/Spannable;

    if-nez v4, :cond_0

    .line 1354879
    invoke-static {}, Landroid/text/Spannable$Factory;->getInstance()Landroid/text/Spannable$Factory;

    move-result-object v4

    const-string v5, "                                        "

    invoke-virtual {v4, v5}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v4

    iput-object v4, v3, LX/8TA;->c:Landroid/text/Spannable;

    .line 1354880
    iget-object v4, v3, LX/8TA;->c:Landroid/text/Spannable;

    new-instance v5, LX/63F;

    iget v6, v3, LX/8TA;->b:I

    iget-object v0, v3, LX/8TA;->a:Landroid/content/Context;

    const v1, 0x7f0a0718

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {v5, v6, v0}, LX/63F;-><init>(II)V

    const/4 v6, 0x0

    const/16 v0, 0x28

    const/16 v1, 0x21

    invoke-interface {v4, v5, v6, v0, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1354881
    :cond_0
    iget-object v4, v3, LX/8TA;->c:Landroid/text/Spannable;

    move-object v3, v4

    .line 1354882
    sget-object v4, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 1354883
    invoke-static {p0}, LX/8XL;->f(LX/8XL;)Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

    move-result-object v0

    .line 1354884
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->m:LX/8Xe;

    .line 1354885
    iget-object v2, v1, LX/8Xe;->c:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v2}, Lcom/facebook/drawingview/DrawingView;->a()V

    .line 1354886
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/8Xe;->a(Z)V

    .line 1354887
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->a(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;Z)V

    .line 1354888
    return-void
.end method

.method public static h(LX/8XL;)V
    .locals 11

    .prologue
    .line 1354889
    invoke-direct {p0}, LX/8XL;->e()Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    move-result-object v0

    .line 1354890
    iget-object v2, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;

    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8TS;

    .line 1354891
    iget-object v3, v1, LX/8TS;->e:LX/8TO;

    move-object v1, v3

    .line 1354892
    const/16 v6, 0x8

    .line 1354893
    iget-object v3, v1, LX/8TO;->j:Landroid/net/Uri;

    move-object v3, v3

    .line 1354894
    if-eqz v3, :cond_0

    .line 1354895
    iget-object v3, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1354896
    iget-object v4, v1, LX/8TO;->j:Landroid/net/Uri;

    move-object v4, v4

    .line 1354897
    iget-object v5, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1354898
    :cond_0
    iget-object v3, v1, LX/8TO;->g:Ljava/lang/String;

    move-object v3, v3

    .line 1354899
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1354900
    iget-object v3, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1354901
    iget-object v4, v1, LX/8TO;->g:Ljava/lang/String;

    move-object v4, v4

    .line 1354902
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v5, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1354903
    :cond_1
    iget-object v3, v1, LX/8TO;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1354904
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1354905
    iget-object v3, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->c:Landroid/widget/TextView;

    .line 1354906
    iget-object v4, v1, LX/8TO;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1354907
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354908
    :goto_0
    iget-object v3, v1, LX/8TO;->i:Ljava/lang/String;

    move-object v3, v3

    .line 1354909
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1354910
    iget-object v3, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->d:Landroid/widget/TextView;

    .line 1354911
    iget-object v4, v1, LX/8TO;->i:Ljava/lang/String;

    move-object v4, v4

    .line 1354912
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354913
    iget-object v3, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->d:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1354914
    :goto_1
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->h:Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;

    invoke-virtual {v1}, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->a()V

    .line 1354915
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->j:LX/8X0;

    .line 1354916
    iget-object v3, v1, LX/8X0;->b:LX/8Tm;

    .line 1354917
    iget-object v4, v3, LX/8Tm;->b:LX/1Ck;

    const-string v5, "games_list_query"

    invoke-virtual {v4, v5}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1354918
    iget-object v3, v1, LX/8X0;->b:LX/8Tm;

    iget-object v4, v1, LX/8X0;->d:LX/8TS;

    .line 1354919
    iget-object v5, v4, LX/8TS;->j:LX/8TP;

    move-object v4, v5

    .line 1354920
    iget-object v5, v1, LX/8X0;->d:LX/8TS;

    .line 1354921
    iget-object v6, v5, LX/8TS;->e:LX/8TO;

    move-object v5, v6

    .line 1354922
    iget-object v6, v5, LX/8TO;->b:Ljava/lang/String;

    move-object v5, v6

    .line 1354923
    new-instance v6, LX/8Wz;

    invoke-direct {v6, v1}, LX/8Wz;-><init>(LX/8X0;)V

    .line 1354924
    new-instance v7, LX/8VH;

    invoke-direct {v7}, LX/8VH;-><init>()V

    move-object v7, v7

    .line 1354925
    new-instance v8, LX/4GZ;

    invoke-direct {v8}, LX/4GZ;-><init>()V

    .line 1354926
    invoke-virtual {v4}, LX/8TP;->d()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1354927
    new-instance v9, LX/4Ga;

    invoke-direct {v9}, LX/4Ga;-><init>()V

    .line 1354928
    iget-object v10, v4, LX/8TP;->b:Ljava/lang/String;

    move-object v10, v10

    .line 1354929
    invoke-virtual {v9, v10}, LX/4Ga;->a(Ljava/lang/String;)LX/4Ga;

    .line 1354930
    invoke-virtual {v8, v9}, LX/4GZ;->a(LX/4Ga;)LX/4GZ;

    .line 1354931
    :cond_2
    new-instance v9, LX/4Gb;

    invoke-direct {v9}, LX/4Gb;-><init>()V

    .line 1354932
    const-string v10, "END_SCREEN_RECS"

    invoke-virtual {v9, v10}, LX/4Gb;->a(Ljava/lang/String;)LX/4Gb;

    .line 1354933
    if-eqz v5, :cond_3

    .line 1354934
    const-string v10, "game_id"

    invoke-virtual {v9, v10, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1354935
    :cond_3
    invoke-virtual {v8, v9}, LX/4GZ;->a(LX/4Gb;)LX/4GZ;

    .line 1354936
    const-string v9, "input"

    invoke-virtual {v7, v9, v8}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1354937
    move-object v7, v7

    .line 1354938
    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    sget-object v8, LX/0zS;->a:LX/0zS;

    invoke-virtual {v7, v8}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v7

    const-wide/16 v9, 0x12c

    invoke-virtual {v7, v9, v10}, LX/0zO;->a(J)LX/0zO;

    move-result-object v7

    .line 1354939
    iget-object v8, v3, LX/8Tm;->a:LX/0tX;

    invoke-virtual {v8, v7}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v7

    .line 1354940
    new-instance v8, LX/8Tl;

    invoke-direct {v8, v3, v6}, LX/8Tl;-><init>(LX/8Tm;LX/8Wz;)V

    .line 1354941
    iget-object v9, v3, LX/8Tm;->b:LX/1Ck;

    const-string v10, "games_list_query"

    invoke-virtual {v9, v10, v7, v8}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1354942
    return-void

    .line 1354943
    :cond_4
    iget-object v3, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1354944
    :cond_5
    iget-object v3, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 1354866
    packed-switch p1, :pswitch_data_0

    .line 1354867
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1354868
    :pswitch_0
    invoke-direct {p0}, LX/8XL;->e()Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    move-result-object v0

    goto :goto_0

    .line 1354869
    :pswitch_1
    invoke-direct {p0}, LX/8XL;->d()Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;

    move-result-object v0

    goto :goto_0

    .line 1354870
    :pswitch_2
    invoke-static {p0}, LX/8XL;->f(LX/8XL;)Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1354863
    invoke-static {p0}, LX/8XL;->f(LX/8XL;)Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

    move-result-object v0

    .line 1354864
    iget-object p0, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->h:Lcom/facebook/widget/FbImageView;

    invoke-virtual {p0, p1}, Lcom/facebook/widget/FbImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1354865
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1354855
    invoke-super {p0, p1, p2, p3}, LX/2s5;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 1354856
    iget-object v0, p0, LX/8XL;->d:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    if-ne p3, v0, :cond_1

    .line 1354857
    iput-object v1, p0, LX/8XL;->d:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    .line 1354858
    :cond_0
    :goto_0
    return-void

    .line 1354859
    :cond_1
    iget-object v0, p0, LX/8XL;->b:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;

    if-ne p3, v0, :cond_2

    .line 1354860
    iput-object v1, p0, LX/8XL;->b:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;

    goto :goto_0

    .line 1354861
    :cond_2
    iget-object v0, p0, LX/8XL;->c:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

    if-ne p3, v0, :cond_0

    .line 1354862
    iput-object v1, p0, LX/8XL;->c:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Ljava/util/List;LX/8Vb;)V
    .locals 7
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "LX/8Vb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1354804
    invoke-direct {p0}, LX/8XL;->d()Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 1354805
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->b:LX/8TS;

    .line 1354806
    iget-object v2, v1, LX/8TS;->h:LX/8TW;

    move-object v1, v2

    .line 1354807
    iget-object v2, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->b:LX/8TS;

    .line 1354808
    iget-object v5, v2, LX/8TS;->j:LX/8TP;

    move-object v2, v5

    .line 1354809
    invoke-virtual {v2}, LX/8TP;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1354810
    if-eqz v1, :cond_0

    .line 1354811
    iget-object v2, v1, LX/8TW;->a:Ljava/util/List;

    move-object v2, v2

    .line 1354812
    invoke-static {v2}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1354813
    iget-object v2, v1, LX/8TW;->a:Ljava/util/List;

    move-object v1, v2

    .line 1354814
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v4, :cond_1

    .line 1354815
    :cond_0
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    sget-object v2, LX/8WQ;->DISABLED:LX/8WQ;

    .line 1354816
    iput-object v2, v1, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->k:LX/8WQ;

    .line 1354817
    :goto_0
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    .line 1354818
    iget-object v2, v1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v1, v2

    .line 1354819
    check-cast v1, LX/8WX;

    .line 1354820
    iget-object v2, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->b:LX/8TS;

    .line 1354821
    iget-object v3, v2, LX/8TS;->j:LX/8TP;

    move-object v2, v3

    .line 1354822
    iget-object v3, v2, LX/8TP;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1354823
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v2, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->b:LX/8TS;

    .line 1354824
    iget-object v5, v2, LX/8TS;->o:Ljava/lang/String;

    move-object v5, v5

    .line 1354825
    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, LX/8WX;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Z)V

    .line 1354826
    invoke-static {v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->d(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;)V

    .line 1354827
    :goto_1
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->f:LX/8Wb;

    invoke-virtual {v1, p3}, LX/8Wb;->a(LX/8Vb;)V

    .line 1354828
    invoke-direct {p0}, LX/8XL;->e()Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1354829
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8TS;

    .line 1354830
    iget-object v2, v1, LX/8TS;->j:LX/8TP;

    move-object v1, v2

    .line 1354831
    invoke-virtual {v1}, LX/8TP;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p2}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1354832
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    invoke-virtual {v1, v4}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->setVisibility(I)V

    .line 1354833
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x5

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1354834
    invoke-interface {p2, v4, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 1354835
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    .line 1354836
    iget-object v5, v1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v1, v5

    .line 1354837
    check-cast v1, LX/8WX;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v6, 0x1

    move-object v5, v3

    invoke-virtual/range {v1 .. v6}, LX/8WX;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Z)V

    .line 1354838
    :goto_2
    return-void

    .line 1354839
    :cond_1
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    sget-object v2, LX/8WQ;->SHOW_CHALLENGE_POPOVER:LX/8WQ;

    .line 1354840
    iput-object v2, v1, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->k:LX/8WQ;

    .line 1354841
    goto :goto_0

    .line 1354842
    :cond_2
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    sget-object v2, LX/8WQ;->SHOW_CHALLENGE_POPOVER:LX/8WQ;

    .line 1354843
    iput-object v2, v1, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->k:LX/8WQ;

    .line 1354844
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    .line 1354845
    iget-object v2, v1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v1, v2

    .line 1354846
    check-cast v1, LX/8WX;

    .line 1354847
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v2, p2

    move-object v5, v3

    invoke-virtual/range {v1 .. v6}, LX/8WX;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Z)V

    .line 1354848
    invoke-static {v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->d(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;)V

    goto :goto_1

    .line 1354849
    :cond_3
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->setVisibility(I)V

    goto :goto_2
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1354803
    const/4 v0, 0x3

    return v0
.end method
