.class public final LX/8Ic;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:I

.field public D:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Z

.field public w:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1325309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;
    .locals 22

    .prologue
    .line 1325310
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1325311
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Ic;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1325312
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8Ic;->k:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1325313
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8Ic;->m:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1325314
    move-object/from16 v0, p0

    iget-object v6, v0, LX/8Ic;->n:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    invoke-static {v2, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1325315
    move-object/from16 v0, p0

    iget-object v7, v0, LX/8Ic;->p:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1325316
    move-object/from16 v0, p0

    iget-object v8, v0, LX/8Ic;->q:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1325317
    move-object/from16 v0, p0

    iget-object v9, v0, LX/8Ic;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1325318
    move-object/from16 v0, p0

    iget-object v10, v0, LX/8Ic;->s:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    invoke-static {v2, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1325319
    move-object/from16 v0, p0

    iget-object v11, v0, LX/8Ic;->t:Ljava/lang/String;

    invoke-virtual {v2, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1325320
    move-object/from16 v0, p0

    iget-object v12, v0, LX/8Ic;->u:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    invoke-static {v2, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1325321
    move-object/from16 v0, p0

    iget-object v13, v0, LX/8Ic;->w:LX/0Px;

    invoke-static {v2, v13}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 1325322
    move-object/from16 v0, p0

    iget-object v14, v0, LX/8Ic;->x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    invoke-static {v2, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1325323
    move-object/from16 v0, p0

    iget-object v15, v0, LX/8Ic;->y:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-static {v2, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1325324
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8Ic;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1325325
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8Ic;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1325326
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8Ic;->B:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1325327
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8Ic;->D:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1325328
    const/16 v20, 0x1e

    move/from16 v0, v20

    invoke-virtual {v2, v0}, LX/186;->c(I)V

    .line 1325329
    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8Ic;->a:Z

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 1325330
    const/16 v20, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8Ic;->b:Z

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 1325331
    const/16 v20, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8Ic;->c:Z

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 1325332
    const/16 v20, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8Ic;->d:Z

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 1325333
    const/16 v20, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8Ic;->e:Z

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 1325334
    const/16 v20, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8Ic;->f:Z

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 1325335
    const/16 v20, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8Ic;->g:Z

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 1325336
    const/16 v20, 0x7

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8Ic;->h:Z

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 1325337
    const/16 v20, 0x8

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/8Ic;->i:Z

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 1325338
    const/16 v20, 0x9

    move/from16 v0, v20

    invoke-virtual {v2, v0, v3}, LX/186;->b(II)V

    .line 1325339
    const/16 v3, 0xa

    invoke-virtual {v2, v3, v4}, LX/186;->b(II)V

    .line 1325340
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8Ic;->l:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1325341
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1325342
    const/16 v3, 0xd

    invoke-virtual {v2, v3, v6}, LX/186;->b(II)V

    .line 1325343
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8Ic;->o:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1325344
    const/16 v3, 0xf

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1325345
    const/16 v3, 0x10

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1325346
    const/16 v3, 0x11

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1325347
    const/16 v3, 0x12

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1325348
    const/16 v3, 0x13

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1325349
    const/16 v3, 0x14

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 1325350
    const/16 v3, 0x15

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8Ic;->v:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1325351
    const/16 v3, 0x16

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 1325352
    const/16 v3, 0x17

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 1325353
    const/16 v3, 0x18

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 1325354
    const/16 v3, 0x19

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1325355
    const/16 v3, 0x1a

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1325356
    const/16 v3, 0x1b

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1325357
    const/16 v3, 0x1c

    move-object/from16 v0, p0

    iget v4, v0, LX/8Ic;->C:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1325358
    const/16 v3, 0x1d

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1325359
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1325360
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1325361
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1325362
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1325363
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1325364
    new-instance v3, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    invoke-direct {v3, v2}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;-><init>(LX/15i;)V

    .line 1325365
    return-object v3
.end method
