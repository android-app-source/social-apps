.class public LX/6up;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1156171
    new-instance v0, LX/6uo;

    invoke-direct {v0}, LX/6uo;-><init>()V

    sput-object v0, LX/6up;->a:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1156172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1156173
    sget-object v0, LX/6up;->a:Landroid/content/DialogInterface$OnClickListener;

    invoke-static {p0, v0}, LX/6up;->a(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1156174
    return-void
.end method

.method public static a(Landroid/content/Context;ILandroid/content/DialogInterface$OnClickListener;)V
    .locals 2

    .prologue
    .line 1156175
    new-instance v0, LX/31Y;

    invoke-direct {v0, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f081e1c

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    invoke-virtual {v0, v1, p2}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1156176
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 2

    .prologue
    .line 1156177
    new-instance v0, LX/31Y;

    invoke-direct {v0, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v1, 0x7f081e19

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f081e1a

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    invoke-virtual {v0, v1, p1}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1156178
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 1

    .prologue
    .line 1156179
    sget-object v0, LX/6up;->a:Landroid/content/DialogInterface$OnClickListener;

    invoke-static {p0, p1, v0}, LX/6up;->a(Landroid/content/Context;Lcom/facebook/fbservice/service/ServiceException;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1156180
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/fbservice/service/ServiceException;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 2

    .prologue
    .line 1156181
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 1156182
    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 1156183
    invoke-static {p0, p2}, LX/6up;->a(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1156184
    :goto_0
    return-void

    .line 1156185
    :cond_0
    const v0, 0x7f081e1b

    invoke-static {p0, v0, p2}, LX/6up;->a(Landroid/content/Context;ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1156186
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1156187
    const-class v0, Lcom/facebook/fbservice/service/ServiceException;

    invoke-static {p1, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/ServiceException;

    .line 1156188
    if-eqz v0, :cond_0

    .line 1156189
    sget-object v1, LX/6up;->a:Landroid/content/DialogInterface$OnClickListener;

    invoke-static {p0, v0, v1}, LX/6up;->a(Landroid/content/Context;Lcom/facebook/fbservice/service/ServiceException;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1156190
    :cond_0
    return-void
.end method
