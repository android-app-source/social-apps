.class public LX/7Sl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Sb;


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 1209127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1209128
    iput p1, p0, LX/7Sl;->a:I

    .line 1209129
    iput p2, p0, LX/7Sl;->b:I

    .line 1209130
    return-void
.end method


# virtual methods
.method public final a()LX/7Sc;
    .locals 1

    .prologue
    .line 1209126
    sget-object v0, LX/7Sc;->INPUT_PREVIEW_SIZE:LX/7Sc;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1209134
    const/4 v0, 0x1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1209135
    if-ne p0, p1, :cond_1

    .line 1209136
    :cond_0
    :goto_0
    return v0

    .line 1209137
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1209138
    goto :goto_0

    .line 1209139
    :cond_3
    check-cast p1, LX/7Sl;

    .line 1209140
    iget v2, p0, LX/7Sl;->a:I

    iget v3, p1, LX/7Sl;->a:I

    if-ne v2, v3, :cond_4

    iget v2, p0, LX/7Sl;->b:I

    iget v3, p1, LX/7Sl;->b:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1209131
    iget v0, p0, LX/7Sl;->a:I

    .line 1209132
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/7Sl;->b:I

    add-int/2addr v0, v1

    .line 1209133
    return v0
.end method
