.class public LX/6lu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field public final a:LX/6lW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/6lW",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            "LX/0Ot",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 4
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/6lZ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1143003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1143004
    new-instance v0, LX/6lt;

    invoke-direct {v0, p0}, LX/6lt;-><init>(LX/6lu;)V

    iput-object v0, p0, LX/6lu;->a:LX/6lW;

    .line 1143005
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1143006
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6lZ;

    .line 1143007
    iget-object v3, v0, LX/6lZ;->b:LX/0Ot;

    if-eqz v3, :cond_0

    .line 1143008
    iget-object v3, v0, LX/6lZ;->a:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    iget-object v0, v0, LX/6lZ;->b:LX/0Ot;

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1143009
    :cond_1
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/6lu;->b:LX/0P1;

    .line 1143010
    return-void
.end method
