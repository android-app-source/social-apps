.class public final LX/8JZ;
.super Landroid/widget/Filter;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;)V
    .locals 0

    .prologue
    .line 1328756
    iput-object p1, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;B)V
    .locals 0

    .prologue
    .line 1328757
    invoke-direct {p0, p1}, LX/8JZ;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;)V

    return-void
.end method


# virtual methods
.method public final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 12

    .prologue
    const/4 v2, 0x1

    .line 1328758
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1328759
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 1328760
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 1328761
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 1328762
    :goto_0
    return-object v0

    .line 1328763
    :cond_0
    iget-object v0, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    iget-object v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->b:LX/8nB;

    invoke-virtual {v0}, LX/8nB;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1328764
    iget-object v0, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    iget-object v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->b:LX/8nB;

    move-object v1, p1

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, LX/8nB;->a(Ljava/lang/CharSequence;ZZZZ)Ljava/util/List;

    move-result-object v1

    .line 1328765
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 1328766
    iput-object v1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 1328767
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v0, Landroid/widget/Filter$FilterResults;->count:I

    goto :goto_0

    .line 1328768
    :cond_1
    iget-object v0, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    iget-object v3, v0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->b:LX/8nB;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_1
    new-instance v11, LX/8JY;

    invoke-direct {v11, p0}, LX/8JY;-><init>(LX/8JZ;)V

    move-object v4, p1

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    invoke-virtual/range {v3 .. v11}, LX/8nB;->a(Ljava/lang/CharSequence;Ljava/lang/String;ZZZZZLX/8JX;)V

    .line 1328769
    const/4 v0, 0x0

    goto :goto_0

    .line 1328770
    :cond_2
    const-string v5, ""

    goto :goto_1
.end method

.method public final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 1328771
    iget-object v0, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    iget-boolean v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->f:Z

    if-nez v0, :cond_1

    .line 1328772
    :cond_0
    :goto_0
    return-void

    .line 1328773
    :cond_1
    iget-object v0, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_0

    .line 1328774
    :cond_2
    iget-object v0, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    iget-object v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->h:Landroid/widget/Filter$FilterListener;

    if-eqz v0, :cond_3

    .line 1328775
    iget-object v0, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    iget-object v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->h:Landroid/widget/Filter$FilterListener;

    iget v1, p2, Landroid/widget/Filter$FilterResults;->count:I

    invoke-interface {v0, v1}, Landroid/widget/Filter$FilterListener;->onFilterComplete(I)V

    .line 1328776
    :cond_3
    iget-object v0, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->setNotifyOnChange(Z)V

    .line 1328777
    iget-object v0, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->clear()V

    .line 1328778
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1328779
    iget-object v0, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    .line 1328780
    invoke-static {v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->e$redex0(Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;)V

    .line 1328781
    iget-object v0, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    const v1, -0x5f63c22a

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 1328782
    :cond_4
    if-eqz p2, :cond_5

    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_5

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-nez v0, :cond_6

    .line 1328783
    :cond_5
    iget-object v0, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->setNotifyOnChange(Z)V

    .line 1328784
    iget-object v0, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    const v1, 0xcab060e

    invoke-static {v0, v1}, LX/08p;->b(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 1328785
    :cond_6
    iget-object v1, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v1, v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->b(Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;Ljava/util/List;)V

    .line 1328786
    iget-object v0, p0, LX/8JZ;->a:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    const v1, -0x40f5e418

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method
