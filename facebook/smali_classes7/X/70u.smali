.class public LX/70u;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements LX/6vq;
.implements LX/6E6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomViewGroup;",
        "LX/6vq",
        "<",
        "LX/71G;",
        ">;",
        "LX/6E6;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

.field public b:LX/6qh;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1162900
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1162901
    invoke-virtual {p0}, LX/70u;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031637

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p0, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    iput-object v0, p0, LX/70u;->a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    .line 1162902
    iget-object v0, p0, LX/70u;->a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    invoke-virtual {p0, v0}, LX/70u;->addView(Landroid/view/View;)V

    .line 1162903
    invoke-virtual {p0}, LX/70u;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0545

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1162904
    invoke-virtual {p0}, LX/70u;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0b056d

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1162905
    invoke-virtual {p0, v0, v1, v0, v1}, LX/70u;->setPadding(IIII)V

    .line 1162906
    return-void
.end method


# virtual methods
.method public final a(LX/71G;)V
    .locals 3

    .prologue
    .line 1162907
    iget-object v0, p0, LX/70u;->a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    iget-object v1, p0, LX/70u;->b:LX/6qh;

    invoke-virtual {v0, v1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1162908
    iget-object v0, p0, LX/70u;->a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    iget-object v1, p1, LX/71G;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->setText(Ljava/lang/String;)V

    .line 1162909
    sget-object v0, LX/70t;->a:[I

    iget-object v1, p1, LX/71G;->b:LX/71F;

    invoke-virtual {v1}, LX/71F;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1162910
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not handled "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, LX/71G;->b:LX/71F;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1162911
    :pswitch_0
    iget-object v0, p0, LX/70u;->a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    const-string v1, "https://m.facebook.com/payer_protection"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->setLearnMoreUri(Landroid/net/Uri;)V

    .line 1162912
    :goto_0
    :pswitch_1
    return-void

    .line 1162913
    :pswitch_2
    iget-object v0, p0, LX/70u;->a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    const-string v1, "https://m.facebook.com/payer_protection"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "https://m.facebook.com/payments_terms"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->a(Landroid/net/Uri;Landroid/net/Uri;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 1162914
    return-void
.end method
