.class public LX/778;
.super LX/2g7;
.source ""


# instance fields
.field private final a:LX/76P;


# direct methods
.method public constructor <init>(LX/76P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171622
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1171623
    iput-object p1, p0, LX/778;->a:LX/76P;

    .line 1171624
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 3
    .param p1    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1171632
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 1171633
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/778;->a:LX/76P;

    .line 1171634
    iget-object v1, v0, LX/76P;->a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {v1, p1}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Ljava/util/Map;

    move-result-object v1

    .line 1171635
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1bf;

    .line 1171636
    iget-object p0, v0, LX/76P;->a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    .line 1171637
    iget-object p2, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->c:LX/1HI;

    .line 1171638
    iget-object p1, v1, LX/1bf;->b:Landroid/net/Uri;

    move-object p1, p1

    .line 1171639
    invoke-virtual {p2, p1}, LX/1HI;->b(Landroid/net/Uri;)Z

    move-result p2

    move v1, p2

    .line 1171640
    if-nez v1, :cond_0

    .line 1171641
    const/4 v1, 0x0

    .line 1171642
    :goto_0
    move v0, v1

    .line 1171643
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)V
    .locals 3
    .param p1    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1171625
    invoke-super {p0, p1, p2}, LX/2g7;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)V

    .line 1171626
    iget-object v0, p0, LX/778;->a:LX/76P;

    .line 1171627
    iget-object v1, v0, LX/76P;->a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    .line 1171628
    invoke-virtual {v1, p1}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Ljava/util/Map;

    move-result-object v2

    .line 1171629
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1bf;

    .line 1171630
    iget-object p2, v1, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->c:LX/1HI;

    sget-object v0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p2, v2, v0}, LX/1HI;->d(LX/1bf;Ljava/lang/Object;)LX/1ca;

    goto :goto_0

    .line 1171631
    :cond_0
    return-void
.end method
