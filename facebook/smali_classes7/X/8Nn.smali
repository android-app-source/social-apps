.class public final LX/8Nn;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8Np;


# direct methods
.method public constructor <init>(LX/8Np;)V
    .locals 0

    .prologue
    .line 1337900
    iput-object p1, p0, LX/8Nn;->a:LX/8Np;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1337901
    iget-object v0, p0, LX/8Nn;->a:LX/8Np;

    iget-object v0, v0, LX/8Np;->c:LX/8Nj;

    invoke-interface {v0}, LX/8Nj;->b()V

    .line 1337902
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1337903
    check-cast p1, Ljava/util/List;

    .line 1337904
    if-nez p1, :cond_0

    .line 1337905
    :goto_0
    return-void

    .line 1337906
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;

    .line 1337907
    if-nez v0, :cond_2

    .line 1337908
    iget-object v0, p0, LX/8Nn;->a:LX/8Np;

    iget-object v0, v0, LX/8Np;->c:LX/8Nj;

    invoke-interface {v0}, LX/8Nj;->b()V

    goto :goto_0

    .line 1337909
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    move-result-object v0

    .line 1337910
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->ENCODING:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-eq v0, v3, :cond_3

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->ENCODED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-eq v0, v3, :cond_3

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UPLOADING:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-eq v0, v3, :cond_3

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UPLOADED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-ne v0, v3, :cond_6

    .line 1337911
    :cond_3
    sget-object v3, LX/8No;->VIDEO_PROCESSING_STATUS_PROCESSING:LX/8No;

    .line 1337912
    :goto_1
    move-object v3, v3

    .line 1337913
    move-object v0, v3

    .line 1337914
    sget-object v2, LX/8No;->VIDEO_PROCESSING_STATUS_FAILED:LX/8No;

    if-ne v0, v2, :cond_4

    .line 1337915
    iget-object v0, p0, LX/8Nn;->a:LX/8Np;

    iget-object v0, v0, LX/8Np;->c:LX/8Nj;

    invoke-interface {v0}, LX/8Nj;->b()V

    goto :goto_0

    .line 1337916
    :cond_4
    sget-object v2, LX/8No;->VIDEO_PROCESSING_STATUS_PROCESSING:LX/8No;

    if-ne v0, v2, :cond_1

    .line 1337917
    iget-object v0, p0, LX/8Nn;->a:LX/8Np;

    iget-object v0, v0, LX/8Np;->e:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/photos/upload/serverprocessing/VideoStatusChecker$2$1;

    invoke-direct {v1, p0}, Lcom/facebook/photos/upload/serverprocessing/VideoStatusChecker$2$1;-><init>(LX/8Nn;)V

    const-wide/16 v2, 0x3e8

    const v4, 0x16d23119

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0

    .line 1337918
    :cond_5
    iget-object v0, p0, LX/8Nn;->a:LX/8Np;

    iget-object v0, v0, LX/8Np;->c:LX/8Nj;

    invoke-interface {v0}, LX/8Nj;->a()V

    goto :goto_0

    .line 1337919
    :cond_6
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->OK:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-eq v0, v3, :cond_7

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNPUBLISHED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-eq v0, v3, :cond_7

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNDISCOVERABLE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-ne v0, v3, :cond_8

    .line 1337920
    :cond_7
    sget-object v3, LX/8No;->VIDEO_PROCESSING_STATUS_READY:LX/8No;

    goto :goto_1

    .line 1337921
    :cond_8
    sget-object v3, LX/8No;->VIDEO_PROCESSING_STATUS_FAILED:LX/8No;

    goto :goto_1
.end method
