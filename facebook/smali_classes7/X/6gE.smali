.class public LX/6gE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1121478
    sget-object v0, LX/0db;->a:LX/0Tn;

    const-string v1, "montage/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1121479
    sput-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "montage_inbox_unit_was_short_form"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->b:LX/0Tn;

    .line 1121480
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "montage_prefetch_expiration"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->c:LX/0Tn;

    .line 1121481
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "montage_hidden_user_last_server_update_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->d:LX/0Tn;

    .line 1121482
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "montage_hidden_user_ids"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->e:LX/0Tn;

    .line 1121483
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "montage_converted_message_ids"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->f:LX/0Tn;

    .line 1121484
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "montage_forwarded_message_ids"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->g:LX/0Tn;

    .line 1121485
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "has_viewed_montage_nux_thread"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->h:LX/0Tn;

    .line 1121486
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "add_to_montage_message_upsell_tooltip/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->i:LX/0Tn;

    .line 1121487
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "add_to_montage_message_upsell_card_as_nux/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->j:LX/0Tn;

    .line 1121488
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "add_to_montage_message_upsell_click_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->k:LX/0Tn;

    .line 1121489
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "has_shown_composer_capture_tooltip"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->l:LX/0Tn;

    .line 1121490
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "has_performed_my_montage_fbid_fetch"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->m:LX/0Tn;

    .line 1121491
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "montage_num_videos_taken"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->n:LX/0Tn;

    .line 1121492
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "montage_num_times_canvas_edit_tooltop_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->o:LX/0Tn;

    .line 1121493
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "montage_time_canvas_edit_tooltip_shown_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->p:LX/0Tn;

    .line 1121494
    sget-object v0, LX/6gE;->a:LX/0Tn;

    const-string v1, "montage_has_dismissed_canvas_edit_tooltip"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6gE;->q:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1121495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1121496
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 1121497
    sget-object v0, LX/6gE;->c:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1121498
    sget-object v0, LX/6gE;->b:LX/0Tn;

    sget-object v1, LX/6gE;->c:LX/0Tn;

    sget-object v2, LX/6gE;->d:LX/0Tn;

    sget-object v3, LX/6gE;->e:LX/0Tn;

    sget-object v4, LX/6gE;->f:LX/0Tn;

    sget-object v5, LX/6gE;->g:LX/0Tn;

    const/4 v6, 0x6

    new-array v6, v6, [LX/0Tn;

    const/4 v7, 0x0

    sget-object v8, LX/6gE;->h:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, LX/6gE;->k:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    sget-object v8, LX/6gE;->i:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    sget-object v8, LX/6gE;->j:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x4

    sget-object v8, LX/6gE;->l:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x5

    sget-object v8, LX/6gE;->m:LX/0Tn;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
