.class public final LX/843;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;",
        ">;",
        "LX/84h;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2dk;


# direct methods
.method public constructor <init>(LX/2dk;)V
    .locals 0

    .prologue
    .line 1290764
    iput-object p1, p0, LX/843;->a:LX/2dk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290765
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1290766
    if-eqz p1, :cond_0

    .line 1290767
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1290768
    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    .line 1290769
    const/4 v0, 0x0

    .line 1290770
    :goto_1
    return-object v0

    .line 1290771
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1290772
    check-cast v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1290773
    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1290774
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1290775
    check-cast v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v4, -0x3ac381e3

    invoke-static {v3, v0, v1, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v3

    .line 1290776
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1290777
    check-cast v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 1290778
    new-instance v1, LX/84h;

    if-eqz v3, :cond_4

    invoke-static {v3}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_2
    invoke-static {v0}, LX/2dk;->a(LX/2uF;)LX/0Px;

    move-result-object v3

    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v4, v5, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-static {v0}, LX/2dk;->a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-direct {v1, v3, v0}, LX/84h;-><init>(LX/0Px;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    move-object v0, v1

    goto :goto_1

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_2
.end method
