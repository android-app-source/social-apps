.class public LX/7V8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field public final a:LX/0yP;

.field public final b:LX/64L;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7Ux;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Sh;

.field public e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/64J;


# direct methods
.method public constructor <init>(LX/0yP;LX/64L;LX/0Or;LX/0Sh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/token/ZeroTokenFetcher;",
            "LX/64L;",
            "LX/0Or",
            "<",
            "LX/7Ux;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1214035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1214036
    iput-object p1, p0, LX/7V8;->a:LX/0yP;

    .line 1214037
    iput-object p2, p0, LX/7V8;->b:LX/64L;

    .line 1214038
    iput-object p3, p0, LX/7V8;->c:LX/0Or;

    .line 1214039
    iput-object p4, p0, LX/7V8;->d:LX/0Sh;

    .line 1214040
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/7V8;->e:Ljava/util/Set;

    .line 1214041
    new-instance v0, LX/7V7;

    invoke-direct {v0, p0}, LX/7V7;-><init>(LX/7V8;)V

    iput-object v0, p0, LX/7V8;->f:LX/64J;

    .line 1214042
    return-void
.end method
