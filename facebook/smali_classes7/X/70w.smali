.class public LX/70w;
.super LX/6vv;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1162946
    invoke-direct {p0}, LX/6vv;-><init>()V

    .line 1162947
    return-void
.end method

.method public static a(LX/0QB;)LX/70w;
    .locals 3

    .prologue
    .line 1162948
    const-class v1, LX/70w;

    monitor-enter v1

    .line 1162949
    :try_start_0
    sget-object v0, LX/70w;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1162950
    sput-object v2, LX/70w;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1162951
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1162952
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1162953
    new-instance v0, LX/70w;

    invoke-direct {v0}, LX/70w;-><init>()V

    .line 1162954
    move-object v0, v0

    .line 1162955
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1162956
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/70w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1162957
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1162958
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 1162959
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 1162960
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
