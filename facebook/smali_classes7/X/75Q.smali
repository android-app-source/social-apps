.class public LX/75Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/75Q;


# instance fields
.field private final a:LX/747;

.field private final b:LX/75L;

.field public final c:LX/6Z0;

.field private final d:LX/0SG;

.field public final e:Landroid/content/Context;

.field private f:LX/75J;

.field private final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/ipc/media/MediaIdKey;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/747;LX/75L;LX/6Z0;LX/0SG;Landroid/content/Context;LX/75J;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1169480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169481
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1169482
    iput-object v0, p0, LX/75Q;->g:LX/0Px;

    .line 1169483
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/75Q;->h:Ljava/util/Map;

    .line 1169484
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/75Q;->i:Ljava/util/Set;

    .line 1169485
    iput-object p1, p0, LX/75Q;->a:LX/747;

    .line 1169486
    iput-object p2, p0, LX/75Q;->b:LX/75L;

    .line 1169487
    iput-object p3, p0, LX/75Q;->c:LX/6Z0;

    .line 1169488
    iput-object p4, p0, LX/75Q;->d:LX/0SG;

    .line 1169489
    iput-object p5, p0, LX/75Q;->e:Landroid/content/Context;

    .line 1169490
    iput-object p6, p0, LX/75Q;->f:LX/75J;

    .line 1169491
    return-void
.end method

.method public static a(LX/0QB;)LX/75Q;
    .locals 10

    .prologue
    .line 1169467
    sget-object v0, LX/75Q;->j:LX/75Q;

    if-nez v0, :cond_1

    .line 1169468
    const-class v1, LX/75Q;

    monitor-enter v1

    .line 1169469
    :try_start_0
    sget-object v0, LX/75Q;->j:LX/75Q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1169470
    if-eqz v2, :cond_0

    .line 1169471
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1169472
    new-instance v3, LX/75Q;

    invoke-static {v0}, LX/747;->b(LX/0QB;)LX/747;

    move-result-object v4

    check-cast v4, LX/747;

    invoke-static {v0}, LX/75L;->a(LX/0QB;)LX/75L;

    move-result-object v5

    check-cast v5, LX/75L;

    invoke-static {v0}, LX/6Z0;->a(LX/0QB;)LX/6Z0;

    move-result-object v6

    check-cast v6, LX/6Z0;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/75J;->a(LX/0QB;)LX/75J;

    move-result-object v9

    check-cast v9, LX/75J;

    invoke-direct/range {v3 .. v9}, LX/75Q;-><init>(LX/747;LX/75L;LX/6Z0;LX/0SG;Landroid/content/Context;LX/75J;)V

    .line 1169473
    move-object v0, v3

    .line 1169474
    sput-object v0, LX/75Q;->j:LX/75Q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1169475
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1169476
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1169477
    :cond_1
    sget-object v0, LX/75Q;->j:LX/75Q;

    return-object v0

    .line 1169478
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1169479
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/photos/base/media/PhotoItem;J)V
    .locals 8

    .prologue
    .line 1169454
    iget-object v0, p1, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v0

    .line 1169455
    if-eqz v0, :cond_2

    .line 1169456
    invoke-virtual {p0, p1}, LX/75Q;->b(Lcom/facebook/photos/base/media/PhotoItem;)Ljava/util/List;

    move-result-object v2

    .line 1169457
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1169458
    iget-wide v6, v0, Lcom/facebook/photos/base/tagging/Tag;->c:J

    move-wide v4, v6

    .line 1169459
    cmp-long v3, v4, p2

    if-nez v3, :cond_0

    .line 1169460
    iget-object v1, v0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v1, v1

    .line 1169461
    instance-of v1, v1, Lcom/facebook/photos/base/tagging/FaceBox;

    if-eqz v1, :cond_1

    .line 1169462
    iget-object v1, v0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v1, v1

    .line 1169463
    check-cast v1, Lcom/facebook/photos/base/tagging/FaceBox;

    const/4 v3, 0x0

    .line 1169464
    iput-boolean v3, v1, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    .line 1169465
    :cond_1
    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1169466
    :cond_2
    return-void
.end method

.method public static b(F)D
    .locals 4

    .prologue
    .line 1169453
    const v0, 0x49742400    # 1000000.0f

    mul-float/2addr v0, p0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-double v0, v0

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/media/MediaIdKey;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ipc/media/MediaIdKey;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1169450
    iget-object v0, p0, LX/75Q;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1169451
    iget-object v0, p0, LX/75Q;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1169452
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/75Q;->g:LX/0Px;

    goto :goto_0
.end method

.method public final a(LX/74x;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74x;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1169384
    iget-object v0, p0, LX/75Q;->h:Ljava/util/Map;

    invoke-virtual {p1}, LX/74x;->a()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1169385
    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    return-object v0
.end method

.method public final a(LX/0Px;LX/0Rf;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            ">;",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1169442
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1169443
    :cond_0
    return-void

    .line 1169444
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1169445
    invoke-virtual {p2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 1169446
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {p0, v0, v6, v7}, LX/75Q;->a(Lcom/facebook/photos/base/media/PhotoItem;J)V

    goto :goto_1

    .line 1169447
    :cond_2
    new-instance v1, LX/75P;

    iget-object v5, p0, LX/75Q;->e:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/facebook/photos/base/media/PhotoItem;->w()Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, LX/75Q;->c:LX/6Z0;

    invoke-direct {v1, v5, v0, p2, v6}, LX/75P;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Set;LX/6Z0;)V

    .line 1169448
    iget-object v0, p0, LX/75Q;->e:Landroid/content/Context;

    new-array v5, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v0, v5}, LX/3nE;->a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;

    .line 1169449
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public final a(LX/74x;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74x;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1169440
    iget-object v0, p0, LX/75Q;->h:Ljava/util/Map;

    invoke-virtual {p1}, LX/74x;->a()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1169441
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/photos/base/media/PhotoItem;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    .line 1169425
    if-eqz p2, :cond_0

    .line 1169426
    iget-object v0, p2, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v0

    .line 1169427
    if-eqz v0, :cond_0

    .line 1169428
    iget-object v0, p2, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v0

    .line 1169429
    instance-of v0, v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    if-nez v0, :cond_1

    .line 1169430
    :cond_0
    :goto_0
    return-void

    .line 1169431
    :cond_1
    iget-object v0, p2, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v0

    .line 1169432
    check-cast v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1169433
    iget-boolean v1, v0, Lcom/facebook/photos/base/photos/LocalPhoto;->f:Z

    move v1, v1

    .line 1169434
    if-nez v1, :cond_0

    .line 1169435
    iput-boolean v2, v0, Lcom/facebook/photos/base/photos/LocalPhoto;->f:Z

    .line 1169436
    iget-object v0, p0, LX/75Q;->f:LX/75J;

    .line 1169437
    new-instance v3, LX/75I;

    iget-object v5, v0, LX/75J;->a:LX/6Z0;

    iget-object v6, v0, LX/75J;->b:LX/0So;

    move-object v4, p1

    move-object v7, p2

    move v8, v2

    invoke-direct/range {v3 .. v8}, LX/75I;-><init>(Landroid/content/Context;LX/6Z0;LX/0So;Lcom/facebook/photos/base/media/PhotoItem;Z)V

    .line 1169438
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v3, p1, v4}, LX/3nE;->a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;

    .line 1169439
    goto :goto_0
.end method

.method public final a(Lcom/facebook/photos/base/media/PhotoItem;Lcom/facebook/photos/base/tagging/Tag;)V
    .locals 6

    .prologue
    .line 1169413
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1169414
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1169415
    iget-object v0, p0, LX/75Q;->h:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1169416
    iget-object v0, p0, LX/75Q;->h:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1169417
    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1169418
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1169419
    :cond_0
    :goto_0
    new-instance v0, LX/75M;

    iget-object v1, p0, LX/75Q;->e:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/facebook/photos/base/media/PhotoItem;->w()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, LX/75Q;->c:LX/6Z0;

    iget-object v5, p0, LX/75Q;->d:LX/0SG;

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, LX/75M;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/photos/base/tagging/Tag;LX/6Z0;LX/0SG;)V

    .line 1169420
    iget-object v1, p0, LX/75Q;->e:Landroid/content/Context;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/3nE;->a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;

    .line 1169421
    return-void

    .line 1169422
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1169423
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1169424
    iget-object v1, p0, LX/75Q;->h:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1169411
    iget-object v0, p0, LX/75Q;->i:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1169412
    return-void
.end method

.method public final b(Lcom/facebook/photos/base/media/PhotoItem;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1169409
    iget-object v0, p0, LX/75Q;->h:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1169410
    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    return-object v0
.end method

.method public final b(Lcom/facebook/photos/base/media/PhotoItem;Lcom/facebook/photos/base/tagging/Tag;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1169392
    invoke-virtual {p0, p1}, LX/75Q;->b(Lcom/facebook/photos/base/media/PhotoItem;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1169393
    iget-object v0, p2, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v0, v0

    .line 1169394
    instance-of v0, v0, Lcom/facebook/photos/base/tagging/FaceBox;

    if-eqz v0, :cond_0

    .line 1169395
    iget-object v0, p2, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v0, v0

    .line 1169396
    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1169397
    iput-boolean v4, v0, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    .line 1169398
    :cond_0
    iget-boolean v0, p2, Lcom/facebook/photos/base/tagging/Tag;->e:Z

    move v0, v0

    .line 1169399
    if-eqz v0, :cond_1

    .line 1169400
    iget-object v0, p0, LX/75Q;->a:LX/747;

    .line 1169401
    sget-object v1, LX/746;->PREFILLED_TAG_DELETED:LX/746;

    invoke-static {v1}, LX/747;->a(LX/746;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/747;->a(LX/747;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1169402
    iget-object v0, p0, LX/75Q;->b:LX/75L;

    iget-object v1, p0, LX/75Q;->e:Landroid/content/Context;

    .line 1169403
    iget-wide v5, p2, Lcom/facebook/photos/base/tagging/Tag;->c:J

    move-wide v2, v5

    .line 1169404
    invoke-virtual {v0, v1, p1, v2, v3}, LX/75L;->a(Landroid/content/Context;Lcom/facebook/photos/base/media/PhotoItem;J)V

    .line 1169405
    iget-object v0, p0, LX/75Q;->e:Landroid/content/Context;

    invoke-virtual {p0, v0, p1}, LX/75Q;->a(Landroid/content/Context;Lcom/facebook/photos/base/media/PhotoItem;)V

    .line 1169406
    :cond_1
    new-instance v0, LX/75O;

    iget-object v1, p0, LX/75Q;->e:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/facebook/photos/base/media/PhotoItem;->w()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/75Q;->c:LX/6Z0;

    invoke-direct {v0, v1, v2, p2, v3}, LX/75O;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/photos/base/tagging/Tag;LX/6Z0;)V

    .line 1169407
    iget-object v1, p0, LX/75Q;->e:Landroid/content/Context;

    new-array v2, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/3nE;->a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;

    .line 1169408
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1169388
    iget-object v0, p0, LX/75Q;->i:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1169389
    iget-object v0, p0, LX/75Q;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1169390
    invoke-virtual {p0}, LX/75Q;->clearUserData()V

    .line 1169391
    :cond_0
    return-void
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 1169386
    iget-object v0, p0, LX/75Q;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1169387
    return-void
.end method
