.class public LX/6vK;
.super LX/6vJ;
.source ""


# instance fields
.field private final a:LX/6wT;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;LX/6wT;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1156906
    invoke-direct {p0, p1, p2, p3}, LX/6vJ;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;LX/6wT;)V

    .line 1156907
    iput-object p3, p0, LX/6vK;->a:LX/6wT;

    .line 1156908
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 1156903
    iget-object v0, p0, LX/6vK;->a:LX/6wT;

    .line 1156904
    iget-object p0, v0, LX/6wT;->b:LX/6wW;

    invoke-virtual {p0, p1}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    move-object v0, p0

    .line 1156905
    return-object v0
.end method

.method public final a(Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 1156900
    iget-object v0, p0, LX/6vK;->a:LX/6wT;

    .line 1156901
    iget-object p0, v0, LX/6wT;->c:LX/6wY;

    invoke-virtual {p0, p1}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    move-object v0, p0

    .line 1156902
    return-object v0
.end method
