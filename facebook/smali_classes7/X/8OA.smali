.class public final LX/8OA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4ck;
.implements LX/2BD;


# instance fields
.field private final a:LX/73w;

.field private final b:LX/74b;

.field private final c:LX/8OG;

.field private final d:LX/8OL;

.field private final e:LX/0ad;

.field private f:LX/0b3;

.field private g:LX/1EZ;

.field private h:J

.field private i:J

.field private j:J

.field private k:Z

.field private l:Z

.field private m:I

.field private n:Z


# direct methods
.method public constructor <init>(LX/8OG;LX/73w;LX/74b;LX/8OL;LX/0b3;LX/1EZ;LX/0ad;)V
    .locals 1

    .prologue
    .line 1338489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1338490
    iput-object p1, p0, LX/8OA;->c:LX/8OG;

    .line 1338491
    iput-object p2, p0, LX/8OA;->a:LX/73w;

    .line 1338492
    iput-object p3, p0, LX/8OA;->b:LX/74b;

    .line 1338493
    iput-object p4, p0, LX/8OA;->d:LX/8OL;

    .line 1338494
    iput-object p5, p0, LX/8OA;->f:LX/0b3;

    .line 1338495
    iput-object p6, p0, LX/8OA;->g:LX/1EZ;

    .line 1338496
    iput-object p7, p0, LX/8OA;->e:LX/0ad;

    .line 1338497
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8OA;->n:Z

    .line 1338498
    return-void
.end method

.method private b(J)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    .line 1338499
    iget-wide v0, p0, LX/8OA;->j:J

    add-long/2addr v0, p1

    iget-wide v2, p0, LX/8OA;->h:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/8OA;->h:J

    .line 1338500
    iget-wide v0, p0, LX/8OA;->h:J

    iget-wide v2, p0, LX/8OA;->i:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    move v0, v7

    .line 1338501
    :goto_0
    iget-object v1, p0, LX/8OA;->c:LX/8OG;

    long-to-float v2, p1

    iget-wide v4, p0, LX/8OA;->i:J

    long-to-float v3, v4

    div-float/2addr v2, v3

    invoke-interface {v1, v2}, LX/8OG;->a(F)V

    .line 1338502
    new-instance v1, LX/8Kb;

    iget-wide v4, p0, LX/8OA;->i:J

    iget-object v2, p0, LX/8OA;->a:LX/73w;

    .line 1338503
    iget-object v3, v2, LX/73w;->j:Ljava/lang/String;

    move-object v6, v3

    .line 1338504
    move-wide v2, p1

    invoke-direct/range {v1 .. v6}, LX/8Kb;-><init>(JJLjava/lang/String;)V

    .line 1338505
    iget-object v2, p0, LX/8OA;->f:LX/0b3;

    invoke-virtual {v2, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1338506
    iget-object v2, p0, LX/8OA;->g:LX/1EZ;

    .line 1338507
    iput-object v1, v2, LX/1EZ;->B:LX/8Kb;

    .line 1338508
    iget-boolean v1, p0, LX/8OA;->k:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/8OA;->l:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 1338509
    iget-object v0, p0, LX/8OA;->a:LX/73w;

    iget-object v1, p0, LX/8OA;->b:LX/74b;

    iget-wide v2, p0, LX/8OA;->i:J

    iget v4, p0, LX/8OA;->m:I

    invoke-virtual {v0, v1, v2, v3, v4}, LX/73w;->c(LX/74b;JI)V

    .line 1338510
    iput-boolean v7, p0, LX/8OA;->l:Z

    .line 1338511
    :cond_0
    return-void

    .line 1338512
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1338513
    iget-object v0, p0, LX/8OA;->e:LX/0ad;

    sget-short v1, LX/1aO;->am:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/8OA;->n:Z

    .line 1338514
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 1338515
    iget-boolean v0, p0, LX/8OA;->n:Z

    if-eqz v0, :cond_0

    .line 1338516
    iget-wide v0, p0, LX/8OA;->j:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/8OA;->j:J

    .line 1338517
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, LX/8OA;->b(J)V

    .line 1338518
    :cond_0
    return-void
.end method

.method public final a(JI)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 1338519
    iput-wide v2, p0, LX/8OA;->h:J

    .line 1338520
    iput-wide v2, p0, LX/8OA;->j:J

    .line 1338521
    iput-wide p1, p0, LX/8OA;->i:J

    .line 1338522
    cmp-long v0, p1, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/8OA;->k:Z

    .line 1338523
    iput-boolean v1, p0, LX/8OA;->l:Z

    .line 1338524
    iput p3, p0, LX/8OA;->m:I

    .line 1338525
    iget-boolean v0, p0, LX/8OA;->k:Z

    if-eqz v0, :cond_0

    .line 1338526
    iget-object v0, p0, LX/8OA;->a:LX/73w;

    iget-object v1, p0, LX/8OA;->b:LX/74b;

    iget v2, p0, LX/8OA;->m:I

    invoke-virtual {v0, v1, p1, p2, v2}, LX/73w;->b(LX/74b;JI)V

    .line 1338527
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1338528
    goto :goto_0
.end method

.method public final a(JJ)V
    .locals 1

    .prologue
    .line 1338529
    iget-boolean v0, p0, LX/8OA;->n:Z

    if-nez v0, :cond_0

    .line 1338530
    invoke-direct {p0, p1, p2}, LX/8OA;->b(J)V

    .line 1338531
    :cond_0
    return-void
.end method

.method public final a(LX/73z;)V
    .locals 8

    .prologue
    .line 1338532
    iget-boolean v0, p0, LX/8OA;->k:Z

    if-eqz v0, :cond_0

    .line 1338533
    iget-object v0, p0, LX/8OA;->d:LX/8OL;

    .line 1338534
    iget-boolean v1, v0, LX/8OL;->d:Z

    move v0, v1

    .line 1338535
    if-eqz v0, :cond_1

    .line 1338536
    iget-object v0, p0, LX/8OA;->a:LX/73w;

    iget-object v1, p0, LX/8OA;->b:LX/74b;

    iget-wide v2, p0, LX/8OA;->h:J

    iget-wide v4, p0, LX/8OA;->i:J

    iget v6, p0, LX/8OA;->m:I

    invoke-virtual/range {v0 .. v6}, LX/73w;->a(LX/74b;JJI)V

    .line 1338537
    :cond_0
    :goto_0
    return-void

    .line 1338538
    :cond_1
    if-eqz p1, :cond_2

    .line 1338539
    iget-object v0, p0, LX/8OA;->a:LX/73w;

    iget-object v1, p0, LX/8OA;->b:LX/74b;

    iget-wide v2, p0, LX/8OA;->h:J

    iget-wide v4, p0, LX/8OA;->i:J

    iget v6, p0, LX/8OA;->m:I

    move-object v7, p1

    invoke-virtual/range {v0 .. v7}, LX/73w;->a(LX/74b;JJILX/73y;)V

    goto :goto_0

    .line 1338540
    :cond_2
    iget-boolean v0, p0, LX/8OA;->l:Z

    if-nez v0, :cond_0

    .line 1338541
    iget-object v0, p0, LX/8OA;->a:LX/73w;

    iget-object v1, p0, LX/8OA;->b:LX/74b;

    iget-wide v2, p0, LX/8OA;->i:J

    iget v4, p0, LX/8OA;->m:I

    invoke-virtual {v0, v1, v2, v3, v4}, LX/73w;->c(LX/74b;JI)V

    goto :goto_0
.end method
