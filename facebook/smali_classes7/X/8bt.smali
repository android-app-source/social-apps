.class public final LX/8bt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1373094
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1373095
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1373096
    :goto_0
    return v1

    .line 1373097
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1373098
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1373099
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1373100
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1373101
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1373102
    const-string v3, "phonetic_name"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1373103
    const/4 v2, 0x0

    .line 1373104
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 1373105
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1373106
    :goto_2
    move v0, v2

    .line 1373107
    goto :goto_1

    .line 1373108
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1373109
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1373110
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 1373111
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1373112
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 1373113
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1373114
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1373115
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_5

    if-eqz v4, :cond_5

    .line 1373116
    const-string v5, "parts"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1373117
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1373118
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_6

    .line 1373119
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_6

    .line 1373120
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1373121
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v7, :cond_11

    .line 1373122
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1373123
    :goto_5
    move v4, v5

    .line 1373124
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1373125
    :cond_6
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1373126
    goto :goto_3

    .line 1373127
    :cond_7
    const-string v5, "text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1373128
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_3

    .line 1373129
    :cond_8
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1373130
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 1373131
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1373132
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_2

    :cond_9
    move v0, v2

    move v3, v2

    goto :goto_3

    .line 1373133
    :cond_a
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_e

    .line 1373134
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1373135
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1373136
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_a

    if-eqz v11, :cond_a

    .line 1373137
    const-string v12, "length"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 1373138
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v10, v7

    move v7, v6

    goto :goto_6

    .line 1373139
    :cond_b
    const-string v12, "offset"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 1373140
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v9, v4

    move v4, v6

    goto :goto_6

    .line 1373141
    :cond_c
    const-string v12, "part"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 1373142
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto :goto_6

    .line 1373143
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_6

    .line 1373144
    :cond_e
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1373145
    if-eqz v7, :cond_f

    .line 1373146
    invoke-virtual {p1, v5, v10, v5}, LX/186;->a(III)V

    .line 1373147
    :cond_f
    if-eqz v4, :cond_10

    .line 1373148
    invoke-virtual {p1, v6, v9, v5}, LX/186;->a(III)V

    .line 1373149
    :cond_10
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v8}, LX/186;->b(II)V

    .line 1373150
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_5

    :cond_11
    move v4, v5

    move v7, v5

    move v8, v5

    move v9, v5

    move v10, v5

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1373151
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1373152
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1373153
    if-eqz v0, :cond_6

    .line 1373154
    const-string v1, "phonetic_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373155
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1373156
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1373157
    if-eqz v1, :cond_4

    .line 1373158
    const-string v2, "parts"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373159
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1373160
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 1373161
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    const/4 p3, 0x2

    const/4 p1, 0x0

    .line 1373162
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1373163
    invoke-virtual {p0, v3, p1, p1}, LX/15i;->a(III)I

    move-result v4

    .line 1373164
    if-eqz v4, :cond_0

    .line 1373165
    const-string v5, "length"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373166
    invoke-virtual {p2, v4}, LX/0nX;->b(I)V

    .line 1373167
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4, p1}, LX/15i;->a(III)I

    move-result v4

    .line 1373168
    if-eqz v4, :cond_1

    .line 1373169
    const-string v5, "offset"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373170
    invoke-virtual {p2, v4}, LX/0nX;->b(I)V

    .line 1373171
    :cond_1
    invoke-virtual {p0, v3, p3}, LX/15i;->g(II)I

    move-result v4

    .line 1373172
    if-eqz v4, :cond_2

    .line 1373173
    const-string v4, "part"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373174
    invoke-virtual {p0, v3, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1373175
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1373176
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1373177
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1373178
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1373179
    if-eqz v1, :cond_5

    .line 1373180
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1373181
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1373182
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1373183
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1373184
    return-void
.end method
