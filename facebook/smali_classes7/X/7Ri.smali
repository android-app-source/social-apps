.class public final LX/7Ri;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1207263
    iput-object p1, p0, LX/7Ri;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    .line 1207264
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1207265
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 1207266
    iget v0, p1, Landroid/os/Message;->what:I

    .line 1207267
    packed-switch v0, :pswitch_data_0

    .line 1207268
    :pswitch_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled msg what="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1207269
    :pswitch_1
    iget-object v0, p0, LX/7Ri;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    const/4 v3, 0x0

    .line 1207270
    iget-boolean v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->t:Z

    const-string v2, "videoTargetId wasn\'t set"

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1207271
    iput-object v3, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    .line 1207272
    iput-object v3, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    .line 1207273
    invoke-static {v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    .line 1207274
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$4;

    invoke-direct {v2, v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$4;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    const v3, 0x63606e61

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1207275
    :goto_0
    return-void

    .line 1207276
    :pswitch_2
    iget-object v0, p0, LX/7Ri;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    .line 1207277
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    sget-object v2, LX/7Rc;->STREAMING_STARTED:LX/7Rc;

    if-ne v1, v2, :cond_4

    .line 1207278
    sget-object v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a:Ljava/lang/String;

    const-string v2, "Duplicate start request. Streaming already started."

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207279
    invoke-static {v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->E(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    .line 1207280
    :goto_1
    goto :goto_0

    .line 1207281
    :pswitch_3
    iget-object v0, p0, LX/7Ri;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-static {v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->z(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    goto :goto_0

    .line 1207282
    :pswitch_4
    iget-object v0, p0, LX/7Ri;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    .line 1207283
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    sget-object v2, LX/7Rc;->STREAMING_STARTED:LX/7Rc;

    if-eq v1, v2, :cond_9

    .line 1207284
    :cond_0
    :goto_2
    goto :goto_0

    .line 1207285
    :pswitch_5
    iget-object v0, p0, LX/7Ri;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-static {v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->z(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    .line 1207286
    iget-object v0, p0, LX/7Ri;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-static {v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->J(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    goto :goto_0

    .line 1207287
    :pswitch_6
    iget-object v0, p0, LX/7Ri;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-static {v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->z(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    .line 1207288
    iget-object v0, p0, LX/7Ri;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-static {v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->J(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    .line 1207289
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto :goto_0

    .line 1207290
    :pswitch_7
    iget-object v1, p0, LX/7Ri;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/7RX;

    .line 1207291
    iget-object v2, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    sget-object v3, LX/7Rc;->STREAMING_INIT_COMPLETE:LX/7Rc;

    if-eq v2, v3, :cond_1

    iget-object v2, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    sget-object v3, LX/7Rc;->STREAMING_STARTED:LX/7Rc;

    if-ne v2, v3, :cond_a

    .line 1207292
    :cond_1
    sget-object v2, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a:Ljava/lang/String;

    const-string v3, "Init already done. returning"

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207293
    invoke-static {v1}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->L(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    .line 1207294
    :goto_3
    goto :goto_0

    .line 1207295
    :pswitch_8
    iget-object v0, p0, LX/7Ri;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    .line 1207296
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v1}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->close()V

    .line 1207297
    goto :goto_0

    .line 1207298
    :pswitch_9
    iget-object v0, p0, LX/7Ri;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-static {v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->N(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    goto :goto_0

    .line 1207299
    :pswitch_a
    iget-object v0, p0, LX/7Ri;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    .line 1207300
    const/4 v1, -0x1

    .line 1207301
    iget-object v2, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    if-eqz v2, :cond_2

    .line 1207302
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v1}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->getCurrentNetworkState()I

    move-result v1

    .line 1207303
    :cond_2
    iget-object v2, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->l:LX/1bL;

    if-eqz v2, :cond_3

    .line 1207304
    iget-object v2, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->w:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$7;

    invoke-direct {v3, v0, v1}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$7;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;I)V

    const p0, 0x15643aaa

    invoke-static {v2, v3, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1207305
    :cond_3
    goto/16 :goto_0

    .line 1207306
    :pswitch_b
    iget-object v0, p0, LX/7Ri;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    .line 1207307
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$6;

    invoke-direct {v2, v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$6;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    const v3, -0x2d674b56

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1207308
    goto/16 :goto_0

    .line 1207309
    :cond_4
    const/4 v2, 0x0

    .line 1207310
    iget-boolean v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->y:Z

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    if-eqz v1, :cond_8

    .line 1207311
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    if-eqz v1, :cond_5

    .line 1207312
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v1}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->close()V

    .line 1207313
    :cond_5
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    iput-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    .line 1207314
    iput-object v2, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    .line 1207315
    :cond_6
    :goto_4
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    sget-object v2, LX/7Rc;->STREAMING_INIT_COMPLETE:LX/7Rc;

    if-ne v1, v2, :cond_7

    const/4 v1, 0x1

    :goto_5
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1207316
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    invoke-virtual {v1}, LX/7RU;->e()V

    .line 1207317
    sget-object v1, LX/7Rc;->STREAMING_STARTED:LX/7Rc;

    iput-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    .line 1207318
    invoke-static {v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->E(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    goto/16 :goto_1

    .line 1207319
    :cond_7
    const/4 v1, 0x0

    goto :goto_5

    .line 1207320
    :cond_8
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    if-eqz v1, :cond_6

    .line 1207321
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v1}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->close()V

    .line 1207322
    iput-object v2, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    goto :goto_4

    .line 1207323
    :cond_9
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    invoke-virtual {v1}, LX/7RV;->h()V

    .line 1207324
    iget-boolean v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    if-eqz v1, :cond_0

    .line 1207325
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    invoke-virtual {v1}, LX/7RV;->h()V

    goto/16 :goto_2

    .line 1207326
    :cond_a
    iget-object v2, v0, LX/7RX;->b:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-object v2, v2

    .line 1207327
    if-eqz v2, :cond_c

    .line 1207328
    iget-object v2, v0, LX/7RX;->b:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-object v2, v2

    .line 1207329
    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->y:Z

    .line 1207330
    iput-object v2, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    .line 1207331
    iget-object v3, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    if-eqz v3, :cond_b

    .line 1207332
    iget-object v3, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    invoke-virtual {v3}, LX/7RU;->d()V

    .line 1207333
    new-instance v3, LX/7RU;

    iget-object p0, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->i:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v3, p0}, LX/7RU;-><init>(Ljava/util/concurrent/ExecutorService;)V

    iput-object v3, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    .line 1207334
    :cond_b
    invoke-static {v1}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    .line 1207335
    iget-object v3, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->L:LX/0Ab;

    invoke-static {v1, v2, v3}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;LX/0Ab;)V

    .line 1207336
    :cond_c
    iget v2, v0, LX/7RX;->a:F

    move v2, v2

    .line 1207337
    iget-object v3, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    invoke-virtual {v3}, LX/7RU;->a()V

    .line 1207338
    iget-boolean v3, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->x:Z

    if-eqz v3, :cond_d

    .line 1207339
    const/high16 v2, -0x40800000    # -1.0f

    .line 1207340
    iget-object v3, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    const/4 p0, 0x0

    .line 1207341
    iput-boolean p0, v3, LX/7RV;->l:Z

    .line 1207342
    iget-object v3, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-object v3, v3, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->audioOnlyVideoStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    .line 1207343
    new-instance p0, LX/60k;

    iget-object v0, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->audioStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    invoke-direct {p0, v0}, LX/60k;-><init>(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;)V

    iget-object v0, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget v0, v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mAudioOnlyFormatBitRate:I

    .line 1207344
    iput v0, p0, LX/60k;->b:I

    .line 1207345
    move-object p0, p0

    .line 1207346
    invoke-virtual {p0}, LX/60k;->a()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    move-result-object p0

    .line 1207347
    iget-object v0, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    invoke-virtual {v0, v3, p0}, LX/7RV;->a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;)V

    .line 1207348
    iget-boolean v0, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    if-eqz v0, :cond_d

    .line 1207349
    iget-object v0, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    invoke-virtual {v0, v3, p0}, LX/7RV;->a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;)V

    .line 1207350
    :cond_d
    :try_start_0
    iget-object v3, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    invoke-virtual {v3, v2}, LX/7RV;->a(F)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1207351
    iget-boolean v3, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    if-eqz v3, :cond_e

    iget-object v3, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    if-eqz v3, :cond_e

    .line 1207352
    iget-object v3, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    invoke-virtual {v3, v2}, LX/7RV;->a(F)V

    .line 1207353
    :cond_e
    sget-object v2, LX/7Rc;->STREAMING_INIT_COMPLETE:LX/7Rc;

    iput-object v2, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    .line 1207354
    invoke-static {v1}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->L(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    goto/16 :goto_3

    .line 1207355
    :catch_0
    move-exception v2

    .line 1207356
    new-instance v3, Lcom/facebook/video/videostreaming/LiveStreamingError;

    invoke-direct {v3, v2}, Lcom/facebook/video/videostreaming/LiveStreamingError;-><init>(Ljava/lang/Exception;)V

    iget-object v2, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v1, v3, v2}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a(Lcom/facebook/video/videostreaming/LiveStreamingError;Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;)V

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
