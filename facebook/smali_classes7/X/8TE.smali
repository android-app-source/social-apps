.class public final enum LX/8TE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8TE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8TE;

.field public static final enum ADD_TO_LIBRARY:LX/8TE;

.field public static final enum ALL_MATCHES:LX/8TE;

.field public static final enum CHALLENGE_CREATION_NO_THREAD_ID:LX/8TE;

.field public static final enum CURRENT_MATCH:LX/8TE;

.field public static final enum CURRENT_ROUND_CONTEXT:LX/8TE;

.field public static final enum CURRENT_ROUND_CONTEXT_ID:LX/8TE;

.field public static final enum CURRENT_ROUND_THREAD_ID_DEPRECATED:LX/8TE;

.field public static final enum DESTINATION:LX/8TE;

.field public static final enum DESTINATION_ID:LX/8TE;

.field public static final enum EXCEPTION_MESSAGE:LX/8TE;

.field public static final enum EXCEPTION_TRACE:LX/8TE;

.field public static final enum EXTRAS_CONTEXT:LX/8TE;

.field public static final enum EXTRAS_CONTEXT_ID:LX/8TE;

.field public static final enum EXTRAS_FAIL_REASON:LX/8TE;

.field public static final enum EXTRAS_IS_RETRY:LX/8TE;

.field public static final enum EXTRAS_SCORE:LX/8TE;

.field public static final enum EXTRAS_SCREENSHOT_IMAGE_FBID:LX/8TE;

.field public static final enum EXTRAS_SCREENSHOT_IMAGE_HANDLE:LX/8TE;

.field public static final enum EXTRAS_SEND_ADMIN_MESSAGE:LX/8TE;

.field public static final enum EXTRAS_THREAD_ID:LX/8TE;

.field public static final enum EXTRAS_THREAD_IDS:LX/8TE;

.field public static final enum FACEBOOK:LX/8TE;

.field public static final enum FRIENDS_LEADERBOARD:LX/8TE;

.field public static final enum FUNNEL_ACTION_TAG_CONTEXT:LX/8TE;

.field public static final enum FUNNEL_ACTION_TAG_CONTEXT_ID:LX/8TE;

.field public static final enum FUNNEL_ACTION_TAG_CREATION_FAILURE:LX/8TE;

.field public static final enum FUNNEL_ACTION_TAG_GAME_ID:LX/8TE;

.field public static final enum FUNNEL_ACTION_TAG_GAME_PLAY_ID:LX/8TE;

.field public static final enum FUNNEL_ACTION_TAG_INDEX:LX/8TE;

.field public static final enum FUNNEL_ACTION_TAG_SCORE:LX/8TE;

.field public static final enum FUNNEL_ACTION_TAG_SOURCE:LX/8TE;

.field public static final enum FUNNEL_CHALLENGE_CARD_POPOVER_SELECT:LX/8TE;

.field public static final enum FUNNEL_CHALLENGE_CARD_POPOVER_SHOWN:LX/8TE;

.field public static final enum FUNNEL_CHALLENGE_LIST_SHOWN:LX/8TE;

.field public static final enum FUNNEL_GAME_BACKGROUNDED:LX/8TE;

.field public static final enum FUNNEL_GAME_CONTEXT_UPDATE:LX/8TE;

.field public static final enum FUNNEL_GAME_FOREGROUNDED:LX/8TE;

.field public static final enum FUNNEL_GAME_INFO_QUERY_FAILURE:LX/8TE;

.field public static final enum FUNNEL_GAME_INFO_QUERY_START:LX/8TE;

.field public static final enum FUNNEL_GAME_INFO_QUERY_SUCCESS:LX/8TE;

.field public static final enum FUNNEL_GAME_PLAY_END:LX/8TE;

.field public static final enum FUNNEL_GAME_PLAY_START:LX/8TE;

.field public static final enum FUNNEL_GAME_READY:LX/8TE;

.field public static final enum FUNNEL_GAME_SWITCH:LX/8TE;

.field public static final enum FUNNEL_LOADING_STARTED:LX/8TE;

.field public static final enum FUNNEL_QUICKSILVER_END:LX/8TE;

.field public static final enum FUNNEL_QUICKSILVER_START:LX/8TE;

.field public static final enum FUNNEL_SOURCE_MINUS_ONE_SCREEN:LX/8TE;

.field public static final enum FUNNEL_SOURCE_TOP_BAR:LX/8TE;

.field public static final enum FUNNEL_TAG_GAME_ID:LX/8TE;

.field public static final enum FUNNEL_TAG_IS_FAST_START:LX/8TE;

.field public static final enum FUNNEL_TAG_IS_MVP:LX/8TE;

.field public static final enum FUNNEL_TAG_SOURCE:LX/8TE;

.field public static final enum FUNNEL_TAG_SOURCE_ID:LX/8TE;

.field public static final enum GAME_ID:LX/8TE;

.field public static final enum GAME_INFO_FETCH:LX/8TE;

.field public static final enum GAME_PLAY_ID:LX/8TE;

.field public static final enum GAME_RECOMMENDATIONS_FETCH:LX/8TE;

.field public static final enum GAME_SESSION_ID:LX/8TE;

.field public static final enum INITIALIZATION_ERROR:LX/8TE;

.field public static final enum INSTANT_GAME_ADD_SCORE:LX/8TE;

.field public static final enum JAVASCRIPT_INTERFACE_ERROR:LX/8TE;

.field public static final enum MESSENGER:LX/8TE;

.field public static final enum MESSENGER_GAME_SCORE_SHARE:LX/8TE;

.field public static final enum MESSENGER_GAME_SHARE:LX/8TE;

.field public static final enum MUTATION_TYPE:LX/8TE;

.field public static final enum NO_THREAD_PARTICIPANT:LX/8TE;

.field public static final enum ON_SCORE_CALLED_AFTER_GAME_END:LX/8TE;

.field public static final enum PLAYER_SCORE:LX/8TE;

.field public static final enum QUERY_TYPE:LX/8TE;

.field public static final enum QUICKSILVER_NOT_AVAILABLE:LX/8TE;

.field public static final enum QUICKSILVER_VIEW_ERROR:LX/8TE;

.field public static final enum REFERRAL:LX/8TE;

.field public static final enum REFERRAL_CONTEXT:LX/8TE;

.field public static final enum REFERRAL_CONTEXT_ID:LX/8TE;

.field public static final enum REFERRAL_ID_DEPRECATED:LX/8TE;

.field public static final enum SAVE_PLAYER_STATE:LX/8TE;

.field public static final enum SCREENSHOT_HANDLE_GENERATION_FAILURE:LX/8TE;

.field public static final enum SCREENSHOT_RETRIEVE_FAILURE:LX/8TE;

.field public static final enum SCREENSHOT_UPLOAD:LX/8TE;

.field public static final enum SDK_INFO_FETCH:LX/8TE;

.field public static final enum SEND_MESSAGE_ERROR:LX/8TE;

.field public static final enum UNKNOWN_SOURCE:LX/8TE;

.field public static final enum USER_SCOPE_FETCH:LX/8TE;

.field public static final enum WEB_CLIENT_ERROR:LX/8TE;


# instance fields
.field public value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1348297
    new-instance v0, LX/8TE;

    const-string v1, "GAME_ID"

    const-string v2, "game_id"

    invoke-direct {v0, v1, v4, v2}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->GAME_ID:LX/8TE;

    .line 1348298
    new-instance v0, LX/8TE;

    const-string v1, "REFERRAL"

    const-string v2, "referral"

    invoke-direct {v0, v1, v5, v2}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->REFERRAL:LX/8TE;

    .line 1348299
    new-instance v0, LX/8TE;

    const-string v1, "REFERRAL_CONTEXT_ID"

    const-string v2, "referral_context_id"

    invoke-direct {v0, v1, v6, v2}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->REFERRAL_CONTEXT_ID:LX/8TE;

    .line 1348300
    new-instance v0, LX/8TE;

    const-string v1, "REFERRAL_CONTEXT"

    const-string v2, "referral_context"

    invoke-direct {v0, v1, v7, v2}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->REFERRAL_CONTEXT:LX/8TE;

    .line 1348301
    new-instance v0, LX/8TE;

    const-string v1, "REFERRAL_ID_DEPRECATED"

    const-string v2, "referral_id"

    invoke-direct {v0, v1, v8, v2}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->REFERRAL_ID_DEPRECATED:LX/8TE;

    .line 1348302
    new-instance v0, LX/8TE;

    const-string v1, "CURRENT_ROUND_CONTEXT_ID"

    const/4 v2, 0x5

    const-string v3, "current_round_context_id"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->CURRENT_ROUND_CONTEXT_ID:LX/8TE;

    .line 1348303
    new-instance v0, LX/8TE;

    const-string v1, "CURRENT_ROUND_CONTEXT"

    const/4 v2, 0x6

    const-string v3, "current_round_context"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->CURRENT_ROUND_CONTEXT:LX/8TE;

    .line 1348304
    new-instance v0, LX/8TE;

    const-string v1, "CURRENT_ROUND_THREAD_ID_DEPRECATED"

    const/4 v2, 0x7

    const-string v3, "current_round_thread_id"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->CURRENT_ROUND_THREAD_ID_DEPRECATED:LX/8TE;

    .line 1348305
    new-instance v0, LX/8TE;

    const-string v1, "EXCEPTION_MESSAGE"

    const/16 v2, 0x8

    const-string v3, "exception_message"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->EXCEPTION_MESSAGE:LX/8TE;

    .line 1348306
    new-instance v0, LX/8TE;

    const-string v1, "EXCEPTION_TRACE"

    const/16 v2, 0x9

    const-string v3, "exception_trace"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->EXCEPTION_TRACE:LX/8TE;

    .line 1348307
    new-instance v0, LX/8TE;

    const-string v1, "GAME_SESSION_ID"

    const/16 v2, 0xa

    const-string v3, "game_session_id"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->GAME_SESSION_ID:LX/8TE;

    .line 1348308
    new-instance v0, LX/8TE;

    const-string v1, "GAME_PLAY_ID"

    const/16 v2, 0xb

    const-string v3, "game_play_id"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->GAME_PLAY_ID:LX/8TE;

    .line 1348309
    new-instance v0, LX/8TE;

    const-string v1, "QUERY_TYPE"

    const/16 v2, 0xc

    const-string v3, "query_type"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->QUERY_TYPE:LX/8TE;

    .line 1348310
    new-instance v0, LX/8TE;

    const-string v1, "CURRENT_MATCH"

    const/16 v2, 0xd

    const-string v3, "current_match"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->CURRENT_MATCH:LX/8TE;

    .line 1348311
    new-instance v0, LX/8TE;

    const-string v1, "ALL_MATCHES"

    const/16 v2, 0xe

    const-string v3, "all_matches"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->ALL_MATCHES:LX/8TE;

    .line 1348312
    new-instance v0, LX/8TE;

    const-string v1, "FRIENDS_LEADERBOARD"

    const/16 v2, 0xf

    const-string v3, "friends_leaderboard"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FRIENDS_LEADERBOARD:LX/8TE;

    .line 1348313
    new-instance v0, LX/8TE;

    const-string v1, "PLAYER_SCORE"

    const/16 v2, 0x10

    const-string v3, "player_score"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->PLAYER_SCORE:LX/8TE;

    .line 1348314
    new-instance v0, LX/8TE;

    const-string v1, "GAME_INFO_FETCH"

    const/16 v2, 0x11

    const-string v3, "game_info_fetch"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->GAME_INFO_FETCH:LX/8TE;

    .line 1348315
    new-instance v0, LX/8TE;

    const-string v1, "SDK_INFO_FETCH"

    const/16 v2, 0x12

    const-string v3, "sdk_info_fetch"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->SDK_INFO_FETCH:LX/8TE;

    .line 1348316
    new-instance v0, LX/8TE;

    const-string v1, "USER_SCOPE_FETCH"

    const/16 v2, 0x13

    const-string v3, "user_scope_fetch"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->USER_SCOPE_FETCH:LX/8TE;

    .line 1348317
    new-instance v0, LX/8TE;

    const-string v1, "GAME_RECOMMENDATIONS_FETCH"

    const/16 v2, 0x14

    const-string v3, "game_recommendations_fetch"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->GAME_RECOMMENDATIONS_FETCH:LX/8TE;

    .line 1348318
    new-instance v0, LX/8TE;

    const-string v1, "MUTATION_TYPE"

    const/16 v2, 0x15

    const-string v3, "mutation_type"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->MUTATION_TYPE:LX/8TE;

    .line 1348319
    new-instance v0, LX/8TE;

    const-string v1, "MESSENGER_GAME_SHARE"

    const/16 v2, 0x16

    const-string v3, "messenger_game_share"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->MESSENGER_GAME_SHARE:LX/8TE;

    .line 1348320
    new-instance v0, LX/8TE;

    const-string v1, "MESSENGER_GAME_SCORE_SHARE"

    const/16 v2, 0x17

    const-string v3, "messenger_game_score_share"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->MESSENGER_GAME_SCORE_SHARE:LX/8TE;

    .line 1348321
    new-instance v0, LX/8TE;

    const-string v1, "SCREENSHOT_UPLOAD"

    const/16 v2, 0x18

    const-string v3, "screenshot_upload"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->SCREENSHOT_UPLOAD:LX/8TE;

    .line 1348322
    new-instance v0, LX/8TE;

    const-string v1, "ADD_TO_LIBRARY"

    const/16 v2, 0x19

    const-string v3, "add_to_library"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->ADD_TO_LIBRARY:LX/8TE;

    .line 1348323
    new-instance v0, LX/8TE;

    const-string v1, "INSTANT_GAME_ADD_SCORE"

    const/16 v2, 0x1a

    const-string v3, "instant_game_add_score"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->INSTANT_GAME_ADD_SCORE:LX/8TE;

    .line 1348324
    new-instance v0, LX/8TE;

    const-string v1, "SAVE_PLAYER_STATE"

    const/16 v2, 0x1b

    const-string v3, "save_player_state"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->SAVE_PLAYER_STATE:LX/8TE;

    .line 1348325
    new-instance v0, LX/8TE;

    const-string v1, "EXTRAS_THREAD_ID"

    const/16 v2, 0x1c

    const-string v3, "extras_thread_id"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->EXTRAS_THREAD_ID:LX/8TE;

    .line 1348326
    new-instance v0, LX/8TE;

    const-string v1, "EXTRAS_CONTEXT_ID"

    const/16 v2, 0x1d

    const-string v3, "extras_context_id"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->EXTRAS_CONTEXT_ID:LX/8TE;

    .line 1348327
    new-instance v0, LX/8TE;

    const-string v1, "EXTRAS_CONTEXT"

    const/16 v2, 0x1e

    const-string v3, "extras_context"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->EXTRAS_CONTEXT:LX/8TE;

    .line 1348328
    new-instance v0, LX/8TE;

    const-string v1, "EXTRAS_SCORE"

    const/16 v2, 0x1f

    const-string v3, "extras_score"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->EXTRAS_SCORE:LX/8TE;

    .line 1348329
    new-instance v0, LX/8TE;

    const-string v1, "EXTRAS_SEND_ADMIN_MESSAGE"

    const/16 v2, 0x20

    const-string v3, "extras_send_admin_message"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->EXTRAS_SEND_ADMIN_MESSAGE:LX/8TE;

    .line 1348330
    new-instance v0, LX/8TE;

    const-string v1, "EXTRAS_THREAD_IDS"

    const/16 v2, 0x21

    const-string v3, "extras_thread_ids"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->EXTRAS_THREAD_IDS:LX/8TE;

    .line 1348331
    new-instance v0, LX/8TE;

    const-string v1, "EXTRAS_SCREENSHOT_IMAGE_HANDLE"

    const/16 v2, 0x22

    const-string v3, "extras_screenshot_image_handle"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->EXTRAS_SCREENSHOT_IMAGE_HANDLE:LX/8TE;

    .line 1348332
    new-instance v0, LX/8TE;

    const-string v1, "EXTRAS_SCREENSHOT_IMAGE_FBID"

    const/16 v2, 0x23

    const-string v3, "extras_screenshot_image_fbid"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->EXTRAS_SCREENSHOT_IMAGE_FBID:LX/8TE;

    .line 1348333
    new-instance v0, LX/8TE;

    const-string v1, "EXTRAS_FAIL_REASON"

    const/16 v2, 0x24

    const-string v3, "extras_fail_reason"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->EXTRAS_FAIL_REASON:LX/8TE;

    .line 1348334
    new-instance v0, LX/8TE;

    const-string v1, "EXTRAS_IS_RETRY"

    const/16 v2, 0x25

    const-string v3, "extras_is_retry"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->EXTRAS_IS_RETRY:LX/8TE;

    .line 1348335
    new-instance v0, LX/8TE;

    const-string v1, "QUICKSILVER_NOT_AVAILABLE"

    const/16 v2, 0x26

    const-string v3, "quicksilver_not_available"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->QUICKSILVER_NOT_AVAILABLE:LX/8TE;

    .line 1348336
    new-instance v0, LX/8TE;

    const-string v1, "NO_THREAD_PARTICIPANT"

    const/16 v2, 0x27

    const-string v3, "no_thread_participant"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->NO_THREAD_PARTICIPANT:LX/8TE;

    .line 1348337
    new-instance v0, LX/8TE;

    const-string v1, "WEB_CLIENT_ERROR"

    const/16 v2, 0x28

    const-string v3, "web_client_error"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->WEB_CLIENT_ERROR:LX/8TE;

    .line 1348338
    new-instance v0, LX/8TE;

    const-string v1, "JAVASCRIPT_INTERFACE_ERROR"

    const/16 v2, 0x29

    const-string v3, "javascript_interface_error"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->JAVASCRIPT_INTERFACE_ERROR:LX/8TE;

    .line 1348339
    new-instance v0, LX/8TE;

    const-string v1, "QUICKSILVER_VIEW_ERROR"

    const/16 v2, 0x2a

    const-string v3, "quicksilver_view_error"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->QUICKSILVER_VIEW_ERROR:LX/8TE;

    .line 1348340
    new-instance v0, LX/8TE;

    const-string v1, "SEND_MESSAGE_ERROR"

    const/16 v2, 0x2b

    const-string v3, "send_message_error"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->SEND_MESSAGE_ERROR:LX/8TE;

    .line 1348341
    new-instance v0, LX/8TE;

    const-string v1, "INITIALIZATION_ERROR"

    const/16 v2, 0x2c

    const-string v3, "initialization_error"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->INITIALIZATION_ERROR:LX/8TE;

    .line 1348342
    new-instance v0, LX/8TE;

    const-string v1, "CHALLENGE_CREATION_NO_THREAD_ID"

    const/16 v2, 0x2d

    const-string v3, "challenge_creation_no_thread_id"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->CHALLENGE_CREATION_NO_THREAD_ID:LX/8TE;

    .line 1348343
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_ACTION_TAG_CREATION_FAILURE"

    const/16 v2, 0x2e

    const-string v3, "funnel_action_tag_creation_failure"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_ACTION_TAG_CREATION_FAILURE:LX/8TE;

    .line 1348344
    new-instance v0, LX/8TE;

    const-string v1, "ON_SCORE_CALLED_AFTER_GAME_END"

    const/16 v2, 0x2f

    const-string v3, "on_score_called_after_game_end"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->ON_SCORE_CALLED_AFTER_GAME_END:LX/8TE;

    .line 1348345
    new-instance v0, LX/8TE;

    const-string v1, "SCREENSHOT_HANDLE_GENERATION_FAILURE"

    const/16 v2, 0x30

    const-string v3, "screenshot_handle_generation_failure"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->SCREENSHOT_HANDLE_GENERATION_FAILURE:LX/8TE;

    .line 1348346
    new-instance v0, LX/8TE;

    const-string v1, "SCREENSHOT_RETRIEVE_FAILURE"

    const/16 v2, 0x31

    const-string v3, "screenshot_retrieve_failure"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->SCREENSHOT_RETRIEVE_FAILURE:LX/8TE;

    .line 1348347
    new-instance v0, LX/8TE;

    const-string v1, "UNKNOWN_SOURCE"

    const/16 v2, 0x32

    const-string v3, "unknown_source"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->UNKNOWN_SOURCE:LX/8TE;

    .line 1348348
    new-instance v0, LX/8TE;

    const-string v1, "DESTINATION"

    const/16 v2, 0x33

    const-string v3, "destination"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->DESTINATION:LX/8TE;

    .line 1348349
    new-instance v0, LX/8TE;

    const-string v1, "DESTINATION_ID"

    const/16 v2, 0x34

    const-string v3, "destination_id"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->DESTINATION_ID:LX/8TE;

    .line 1348350
    new-instance v0, LX/8TE;

    const-string v1, "MESSENGER"

    const/16 v2, 0x35

    const-string v3, "messenger"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->MESSENGER:LX/8TE;

    .line 1348351
    new-instance v0, LX/8TE;

    const-string v1, "FACEBOOK"

    const/16 v2, 0x36

    const-string v3, "facebook"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FACEBOOK:LX/8TE;

    .line 1348352
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_QUICKSILVER_START"

    const/16 v2, 0x37

    const-string v3, "quicksilver_start"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_QUICKSILVER_START:LX/8TE;

    .line 1348353
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_GAME_INFO_QUERY_START"

    const/16 v2, 0x38

    const-string v3, "game_info_query_start"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_GAME_INFO_QUERY_START:LX/8TE;

    .line 1348354
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_GAME_INFO_QUERY_SUCCESS"

    const/16 v2, 0x39

    const-string v3, "game_info_query_success"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_GAME_INFO_QUERY_SUCCESS:LX/8TE;

    .line 1348355
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_GAME_INFO_QUERY_FAILURE"

    const/16 v2, 0x3a

    const-string v3, "game_info_query_failure"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_GAME_INFO_QUERY_FAILURE:LX/8TE;

    .line 1348356
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_LOADING_STARTED"

    const/16 v2, 0x3b

    const-string v3, "loading_started"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_LOADING_STARTED:LX/8TE;

    .line 1348357
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_GAME_READY"

    const/16 v2, 0x3c

    const-string v3, "game_ready"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_GAME_READY:LX/8TE;

    .line 1348358
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_GAME_BACKGROUNDED"

    const/16 v2, 0x3d

    const-string v3, "game_backgrounded"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_GAME_BACKGROUNDED:LX/8TE;

    .line 1348359
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_GAME_FOREGROUNDED"

    const/16 v2, 0x3e

    const-string v3, "game_foregrounded"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_GAME_FOREGROUNDED:LX/8TE;

    .line 1348360
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_GAME_PLAY_START"

    const/16 v2, 0x3f

    const-string v3, "game_play_start"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_GAME_PLAY_START:LX/8TE;

    .line 1348361
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_GAME_PLAY_END"

    const/16 v2, 0x40

    const-string v3, "game_play_end"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_GAME_PLAY_END:LX/8TE;

    .line 1348362
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_GAME_CONTEXT_UPDATE"

    const/16 v2, 0x41

    const-string v3, "game_context_update"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_GAME_CONTEXT_UPDATE:LX/8TE;

    .line 1348363
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_CHALLENGE_LIST_SHOWN"

    const/16 v2, 0x42

    const-string v3, "game_challenge_list_shown"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_CHALLENGE_LIST_SHOWN:LX/8TE;

    .line 1348364
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_GAME_SWITCH"

    const/16 v2, 0x43

    const-string v3, "game_switch"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_GAME_SWITCH:LX/8TE;

    .line 1348365
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_QUICKSILVER_END"

    const/16 v2, 0x44

    const-string v3, "quicksilver_end"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_QUICKSILVER_END:LX/8TE;

    .line 1348366
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_CHALLENGE_CARD_POPOVER_SHOWN"

    const/16 v2, 0x45

    const-string v3, "challenge_card_popover_shown"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_CHALLENGE_CARD_POPOVER_SHOWN:LX/8TE;

    .line 1348367
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_CHALLENGE_CARD_POPOVER_SELECT"

    const/16 v2, 0x46

    const-string v3, "challenge_card_popover_select"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_CHALLENGE_CARD_POPOVER_SELECT:LX/8TE;

    .line 1348368
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_TAG_GAME_ID"

    const/16 v2, 0x47

    const-string v3, "app"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_TAG_GAME_ID:LX/8TE;

    .line 1348369
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_TAG_SOURCE"

    const/16 v2, 0x48

    const-string v3, "source"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_TAG_SOURCE:LX/8TE;

    .line 1348370
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_TAG_SOURCE_ID"

    const/16 v2, 0x49

    const-string v3, "source_id"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_TAG_SOURCE_ID:LX/8TE;

    .line 1348371
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_TAG_IS_MVP"

    const/16 v2, 0x4a

    const-string v3, "sees_mvp_experience"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_TAG_IS_MVP:LX/8TE;

    .line 1348372
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_TAG_IS_FAST_START"

    const/16 v2, 0x4b

    const-string v3, "fast_start"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_TAG_IS_FAST_START:LX/8TE;

    .line 1348373
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_ACTION_TAG_CONTEXT_ID"

    const/16 v2, 0x4c

    const-string v3, "context_id"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_ACTION_TAG_CONTEXT_ID:LX/8TE;

    .line 1348374
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_ACTION_TAG_CONTEXT"

    const/16 v2, 0x4d

    const-string v3, "context"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_ACTION_TAG_CONTEXT:LX/8TE;

    .line 1348375
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_ACTION_TAG_GAME_PLAY_ID"

    const/16 v2, 0x4e

    const-string v3, "game_play_id"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_ACTION_TAG_GAME_PLAY_ID:LX/8TE;

    .line 1348376
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_ACTION_TAG_INDEX"

    const/16 v2, 0x4f

    const-string v3, "index"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_ACTION_TAG_INDEX:LX/8TE;

    .line 1348377
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_ACTION_TAG_SOURCE"

    const/16 v2, 0x50

    const-string v3, "source"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_ACTION_TAG_SOURCE:LX/8TE;

    .line 1348378
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_ACTION_TAG_GAME_ID"

    const/16 v2, 0x51

    const-string v3, "game_id"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_ACTION_TAG_GAME_ID:LX/8TE;

    .line 1348379
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_ACTION_TAG_SCORE"

    const/16 v2, 0x52

    const-string v3, "score"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_ACTION_TAG_SCORE:LX/8TE;

    .line 1348380
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_SOURCE_MINUS_ONE_SCREEN"

    const/16 v2, 0x53

    const-string v3, "minus_one_screen"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_SOURCE_MINUS_ONE_SCREEN:LX/8TE;

    .line 1348381
    new-instance v0, LX/8TE;

    const-string v1, "FUNNEL_SOURCE_TOP_BAR"

    const/16 v2, 0x54

    const-string v3, "top_bar"

    invoke-direct {v0, v1, v2, v3}, LX/8TE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8TE;->FUNNEL_SOURCE_TOP_BAR:LX/8TE;

    .line 1348382
    const/16 v0, 0x55

    new-array v0, v0, [LX/8TE;

    sget-object v1, LX/8TE;->GAME_ID:LX/8TE;

    aput-object v1, v0, v4

    sget-object v1, LX/8TE;->REFERRAL:LX/8TE;

    aput-object v1, v0, v5

    sget-object v1, LX/8TE;->REFERRAL_CONTEXT_ID:LX/8TE;

    aput-object v1, v0, v6

    sget-object v1, LX/8TE;->REFERRAL_CONTEXT:LX/8TE;

    aput-object v1, v0, v7

    sget-object v1, LX/8TE;->REFERRAL_ID_DEPRECATED:LX/8TE;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/8TE;->CURRENT_ROUND_CONTEXT_ID:LX/8TE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8TE;->CURRENT_ROUND_CONTEXT:LX/8TE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8TE;->CURRENT_ROUND_THREAD_ID_DEPRECATED:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8TE;->EXCEPTION_MESSAGE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8TE;->EXCEPTION_TRACE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8TE;->GAME_SESSION_ID:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8TE;->GAME_PLAY_ID:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/8TE;->QUERY_TYPE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/8TE;->CURRENT_MATCH:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/8TE;->ALL_MATCHES:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/8TE;->FRIENDS_LEADERBOARD:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/8TE;->PLAYER_SCORE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/8TE;->GAME_INFO_FETCH:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/8TE;->SDK_INFO_FETCH:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/8TE;->USER_SCOPE_FETCH:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/8TE;->GAME_RECOMMENDATIONS_FETCH:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/8TE;->MUTATION_TYPE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/8TE;->MESSENGER_GAME_SHARE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/8TE;->MESSENGER_GAME_SCORE_SHARE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/8TE;->SCREENSHOT_UPLOAD:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/8TE;->ADD_TO_LIBRARY:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/8TE;->INSTANT_GAME_ADD_SCORE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/8TE;->SAVE_PLAYER_STATE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/8TE;->EXTRAS_THREAD_ID:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/8TE;->EXTRAS_CONTEXT_ID:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/8TE;->EXTRAS_CONTEXT:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/8TE;->EXTRAS_SCORE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/8TE;->EXTRAS_SEND_ADMIN_MESSAGE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/8TE;->EXTRAS_THREAD_IDS:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/8TE;->EXTRAS_SCREENSHOT_IMAGE_HANDLE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/8TE;->EXTRAS_SCREENSHOT_IMAGE_FBID:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/8TE;->EXTRAS_FAIL_REASON:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/8TE;->EXTRAS_IS_RETRY:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/8TE;->QUICKSILVER_NOT_AVAILABLE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/8TE;->NO_THREAD_PARTICIPANT:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/8TE;->WEB_CLIENT_ERROR:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/8TE;->JAVASCRIPT_INTERFACE_ERROR:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/8TE;->QUICKSILVER_VIEW_ERROR:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/8TE;->SEND_MESSAGE_ERROR:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/8TE;->INITIALIZATION_ERROR:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/8TE;->CHALLENGE_CREATION_NO_THREAD_ID:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, LX/8TE;->FUNNEL_ACTION_TAG_CREATION_FAILURE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, LX/8TE;->ON_SCORE_CALLED_AFTER_GAME_END:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, LX/8TE;->SCREENSHOT_HANDLE_GENERATION_FAILURE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, LX/8TE;->SCREENSHOT_RETRIEVE_FAILURE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, LX/8TE;->UNKNOWN_SOURCE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, LX/8TE;->DESTINATION:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, LX/8TE;->DESTINATION_ID:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, LX/8TE;->MESSENGER:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, LX/8TE;->FACEBOOK:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, LX/8TE;->FUNNEL_QUICKSILVER_START:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, LX/8TE;->FUNNEL_GAME_INFO_QUERY_START:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, LX/8TE;->FUNNEL_GAME_INFO_QUERY_SUCCESS:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, LX/8TE;->FUNNEL_GAME_INFO_QUERY_FAILURE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, LX/8TE;->FUNNEL_LOADING_STARTED:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, LX/8TE;->FUNNEL_GAME_READY:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, LX/8TE;->FUNNEL_GAME_BACKGROUNDED:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, LX/8TE;->FUNNEL_GAME_FOREGROUNDED:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, LX/8TE;->FUNNEL_GAME_PLAY_START:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, LX/8TE;->FUNNEL_GAME_PLAY_END:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, LX/8TE;->FUNNEL_GAME_CONTEXT_UPDATE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, LX/8TE;->FUNNEL_CHALLENGE_LIST_SHOWN:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, LX/8TE;->FUNNEL_GAME_SWITCH:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, LX/8TE;->FUNNEL_QUICKSILVER_END:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, LX/8TE;->FUNNEL_CHALLENGE_CARD_POPOVER_SHOWN:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, LX/8TE;->FUNNEL_CHALLENGE_CARD_POPOVER_SELECT:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, LX/8TE;->FUNNEL_TAG_GAME_ID:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, LX/8TE;->FUNNEL_TAG_SOURCE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, LX/8TE;->FUNNEL_TAG_SOURCE_ID:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, LX/8TE;->FUNNEL_TAG_IS_MVP:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, LX/8TE;->FUNNEL_TAG_IS_FAST_START:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, LX/8TE;->FUNNEL_ACTION_TAG_CONTEXT_ID:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, LX/8TE;->FUNNEL_ACTION_TAG_CONTEXT:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, LX/8TE;->FUNNEL_ACTION_TAG_GAME_PLAY_ID:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, LX/8TE;->FUNNEL_ACTION_TAG_INDEX:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, LX/8TE;->FUNNEL_ACTION_TAG_SOURCE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, LX/8TE;->FUNNEL_ACTION_TAG_GAME_ID:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, LX/8TE;->FUNNEL_ACTION_TAG_SCORE:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, LX/8TE;->FUNNEL_SOURCE_MINUS_ONE_SCREEN:LX/8TE;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, LX/8TE;->FUNNEL_SOURCE_TOP_BAR:LX/8TE;

    aput-object v2, v0, v1

    sput-object v0, LX/8TE;->$VALUES:[LX/8TE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1348383
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1348384
    iput-object p3, p0, LX/8TE;->value:Ljava/lang/String;

    .line 1348385
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8TE;
    .locals 1

    .prologue
    .line 1348386
    const-class v0, LX/8TE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8TE;

    return-object v0
.end method

.method public static values()[LX/8TE;
    .locals 1

    .prologue
    .line 1348387
    sget-object v0, LX/8TE;->$VALUES:[LX/8TE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8TE;

    return-object v0
.end method
