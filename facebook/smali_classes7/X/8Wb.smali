.class public LX/8Wb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/8Wa;

.field private final b:LX/8TS;

.field private final c:LX/8Vb;

.field public d:LX/8TA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;LX/8TS;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1354257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1354258
    iput-object p2, p0, LX/8Wb;->b:LX/8TS;

    .line 1354259
    new-instance v0, LX/8Wa;

    invoke-direct {v0, p1}, LX/8Wa;-><init>(Landroid/view/View;)V

    iput-object v0, p0, LX/8Wb;->a:LX/8Wa;

    .line 1354260
    iget-object v0, p0, LX/8Wb;->b:LX/8TS;

    invoke-virtual {v0}, LX/8TS;->c()LX/8Vb;

    move-result-object v0

    .line 1354261
    invoke-static {}, LX/8Vb;->b()LX/8Va;

    move-result-object v1

    iget-object v2, v0, LX/8Vb;->a:Ljava/lang/String;

    .line 1354262
    iput-object v2, v1, LX/8Va;->a:Ljava/lang/String;

    .line 1354263
    move-object v1, v1

    .line 1354264
    iget-object v2, v0, LX/8Vb;->c:Ljava/lang/String;

    .line 1354265
    iput-object v2, v1, LX/8Va;->c:Ljava/lang/String;

    .line 1354266
    move-object v1, v1

    .line 1354267
    iget-object v2, v0, LX/8Vb;->f:Ljava/lang/String;

    .line 1354268
    iput-object v2, v1, LX/8Va;->j:Ljava/lang/String;

    .line 1354269
    move-object v1, v1

    .line 1354270
    iget-object v0, v0, LX/8Vb;->i:LX/8Vd;

    .line 1354271
    iput-object v0, v1, LX/8Va;->k:LX/8Vd;

    .line 1354272
    move-object v0, v1

    .line 1354273
    invoke-virtual {v0}, LX/8Va;->a()LX/8Vb;

    move-result-object v0

    iput-object v0, p0, LX/8Wb;->c:LX/8Vb;

    .line 1354274
    iget-object v0, p0, LX/8Wb;->a:LX/8Wa;

    iget-object v1, p0, LX/8Wb;->c:LX/8Vb;

    invoke-virtual {v0, v1}, LX/8Wa;->a(LX/8Vb;)V

    .line 1354275
    return-void
.end method


# virtual methods
.method public final a(LX/8Vb;)V
    .locals 7

    .prologue
    .line 1354276
    iget-object v1, p0, LX/8Wb;->c:LX/8Vb;

    iget-wide v2, p1, LX/8Vb;->l:J

    iget-wide v4, p1, LX/8Vb;->l:J

    iget-object v6, p1, LX/8Vb;->n:Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, LX/8Vb;->a(JJLjava/lang/String;)V

    .line 1354277
    iget-object v0, p0, LX/8Wb;->a:LX/8Wa;

    iget-object v1, p0, LX/8Wb;->c:LX/8Vb;

    invoke-virtual {v0, v1}, LX/8Wa;->a(LX/8Vb;)V

    .line 1354278
    return-void
.end method
