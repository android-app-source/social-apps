.class public final enum LX/7gU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gU;

.field public static final enum CLICK:LX/7gU;

.field public static final enum CLICK_LEFT:LX/7gU;

.field public static final enum CLICK_RIGHT:LX/7gU;

.field public static final enum SWIPE_DOWN:LX/7gU;

.field public static final enum SWIPE_LEFT:LX/7gU;

.field public static final enum SWIPE_RIGHT:LX/7gU;

.field public static final enum UNKNOWN:LX/7gU;


# instance fields
.field private final mGesture:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1224436
    new-instance v0, LX/7gU;

    const-string v1, "SWIPE_RIGHT"

    const-string v2, "swipe_right"

    invoke-direct {v0, v1, v4, v2}, LX/7gU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gU;->SWIPE_RIGHT:LX/7gU;

    .line 1224437
    new-instance v0, LX/7gU;

    const-string v1, "SWIPE_DOWN"

    const-string v2, "swipe_down"

    invoke-direct {v0, v1, v5, v2}, LX/7gU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gU;->SWIPE_DOWN:LX/7gU;

    .line 1224438
    new-instance v0, LX/7gU;

    const-string v1, "CLICK"

    const-string v2, "click"

    invoke-direct {v0, v1, v6, v2}, LX/7gU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gU;->CLICK:LX/7gU;

    .line 1224439
    new-instance v0, LX/7gU;

    const-string v1, "CLICK_LEFT"

    const-string v2, "click_left"

    invoke-direct {v0, v1, v7, v2}, LX/7gU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gU;->CLICK_LEFT:LX/7gU;

    .line 1224440
    new-instance v0, LX/7gU;

    const-string v1, "CLICK_RIGHT"

    const-string v2, "click_right"

    invoke-direct {v0, v1, v8, v2}, LX/7gU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gU;->CLICK_RIGHT:LX/7gU;

    .line 1224441
    new-instance v0, LX/7gU;

    const-string v1, "SWIPE_LEFT"

    const/4 v2, 0x5

    const-string v3, "swipe_left"

    invoke-direct {v0, v1, v2, v3}, LX/7gU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gU;->SWIPE_LEFT:LX/7gU;

    .line 1224442
    new-instance v0, LX/7gU;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x6

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/7gU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gU;->UNKNOWN:LX/7gU;

    .line 1224443
    const/4 v0, 0x7

    new-array v0, v0, [LX/7gU;

    sget-object v1, LX/7gU;->SWIPE_RIGHT:LX/7gU;

    aput-object v1, v0, v4

    sget-object v1, LX/7gU;->SWIPE_DOWN:LX/7gU;

    aput-object v1, v0, v5

    sget-object v1, LX/7gU;->CLICK:LX/7gU;

    aput-object v1, v0, v6

    sget-object v1, LX/7gU;->CLICK_LEFT:LX/7gU;

    aput-object v1, v0, v7

    sget-object v1, LX/7gU;->CLICK_RIGHT:LX/7gU;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7gU;->SWIPE_LEFT:LX/7gU;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7gU;->UNKNOWN:LX/7gU;

    aput-object v2, v0, v1

    sput-object v0, LX/7gU;->$VALUES:[LX/7gU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1224432
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1224433
    iput-object p3, p0, LX/7gU;->mGesture:Ljava/lang/String;

    .line 1224434
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gU;
    .locals 1

    .prologue
    .line 1224435
    const-class v0, LX/7gU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gU;

    return-object v0
.end method

.method public static values()[LX/7gU;
    .locals 1

    .prologue
    .line 1224430
    sget-object v0, LX/7gU;->$VALUES:[LX/7gU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gU;

    return-object v0
.end method


# virtual methods
.method public final getGesture()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224431
    iget-object v0, p0, LX/7gU;->mGesture:Ljava/lang/String;

    return-object v0
.end method
