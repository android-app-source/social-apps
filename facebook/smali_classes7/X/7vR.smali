.class public final LX/7vR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsMutationsModels$EventRsvpMutationModel;",
        ">;",
        "LX/7vS;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1274560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1274556
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1274557
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1274558
    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventRsvpMutationModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventRsvpMutationModel;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    move-result-object v1

    .line 1274559
    if-eqz v1, :cond_0

    new-instance v0, LX/7vS;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->k()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LX/7vS;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
