.class public final enum LX/7F7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7F7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7F7;

.field public static final enum CHECKBOX:LX/7F7;

.field public static final enum CHECKBOXWRITEIN:LX/7F7;

.field public static final enum DIVIDER:LX/7F7;

.field public static final enum EDITTEXT:LX/7F7;

.field public static final enum IMAGEBLOCK:LX/7F7;

.field public static final enum MESSAGE:LX/7F7;

.field public static final enum NOTIFICATION:LX/7F7;

.field public static final enum QUESTION:LX/7F7;

.field public static final enum RADIO:LX/7F7;

.field public static final enum RADIOWRITEIN:LX/7F7;

.field public static final enum WHITESPACE:LX/7F7;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1185947
    new-instance v0, LX/7F7;

    const-string v1, "QUESTION"

    invoke-direct {v0, v1, v3}, LX/7F7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7F7;->QUESTION:LX/7F7;

    .line 1185948
    new-instance v0, LX/7F7;

    const-string v1, "RADIO"

    invoke-direct {v0, v1, v4}, LX/7F7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7F7;->RADIO:LX/7F7;

    .line 1185949
    new-instance v0, LX/7F7;

    const-string v1, "CHECKBOX"

    invoke-direct {v0, v1, v5}, LX/7F7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7F7;->CHECKBOX:LX/7F7;

    .line 1185950
    new-instance v0, LX/7F7;

    const-string v1, "EDITTEXT"

    invoke-direct {v0, v1, v6}, LX/7F7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7F7;->EDITTEXT:LX/7F7;

    .line 1185951
    new-instance v0, LX/7F7;

    const-string v1, "MESSAGE"

    invoke-direct {v0, v1, v7}, LX/7F7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7F7;->MESSAGE:LX/7F7;

    .line 1185952
    new-instance v0, LX/7F7;

    const-string v1, "IMAGEBLOCK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7F7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7F7;->IMAGEBLOCK:LX/7F7;

    .line 1185953
    new-instance v0, LX/7F7;

    const-string v1, "DIVIDER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/7F7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7F7;->DIVIDER:LX/7F7;

    .line 1185954
    new-instance v0, LX/7F7;

    const-string v1, "WHITESPACE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/7F7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7F7;->WHITESPACE:LX/7F7;

    .line 1185955
    new-instance v0, LX/7F7;

    const-string v1, "RADIOWRITEIN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/7F7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7F7;->RADIOWRITEIN:LX/7F7;

    .line 1185956
    new-instance v0, LX/7F7;

    const-string v1, "CHECKBOXWRITEIN"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/7F7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7F7;->CHECKBOXWRITEIN:LX/7F7;

    .line 1185957
    new-instance v0, LX/7F7;

    const-string v1, "NOTIFICATION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/7F7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7F7;->NOTIFICATION:LX/7F7;

    .line 1185958
    const/16 v0, 0xb

    new-array v0, v0, [LX/7F7;

    sget-object v1, LX/7F7;->QUESTION:LX/7F7;

    aput-object v1, v0, v3

    sget-object v1, LX/7F7;->RADIO:LX/7F7;

    aput-object v1, v0, v4

    sget-object v1, LX/7F7;->CHECKBOX:LX/7F7;

    aput-object v1, v0, v5

    sget-object v1, LX/7F7;->EDITTEXT:LX/7F7;

    aput-object v1, v0, v6

    sget-object v1, LX/7F7;->MESSAGE:LX/7F7;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7F7;->IMAGEBLOCK:LX/7F7;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7F7;->DIVIDER:LX/7F7;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7F7;->WHITESPACE:LX/7F7;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7F7;->RADIOWRITEIN:LX/7F7;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7F7;->CHECKBOXWRITEIN:LX/7F7;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7F7;->NOTIFICATION:LX/7F7;

    aput-object v2, v0, v1

    sput-object v0, LX/7F7;->$VALUES:[LX/7F7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1185959
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromInt(I)LX/7F7;
    .locals 1

    .prologue
    .line 1185960
    packed-switch p0, :pswitch_data_0

    .line 1185961
    sget-object v0, LX/7F7;->WHITESPACE:LX/7F7;

    :goto_0
    return-object v0

    .line 1185962
    :pswitch_0
    sget-object v0, LX/7F7;->QUESTION:LX/7F7;

    goto :goto_0

    .line 1185963
    :pswitch_1
    sget-object v0, LX/7F7;->RADIO:LX/7F7;

    goto :goto_0

    .line 1185964
    :pswitch_2
    sget-object v0, LX/7F7;->CHECKBOX:LX/7F7;

    goto :goto_0

    .line 1185965
    :pswitch_3
    sget-object v0, LX/7F7;->EDITTEXT:LX/7F7;

    goto :goto_0

    .line 1185966
    :pswitch_4
    sget-object v0, LX/7F7;->MESSAGE:LX/7F7;

    goto :goto_0

    .line 1185967
    :pswitch_5
    sget-object v0, LX/7F7;->IMAGEBLOCK:LX/7F7;

    goto :goto_0

    .line 1185968
    :pswitch_6
    sget-object v0, LX/7F7;->DIVIDER:LX/7F7;

    goto :goto_0

    .line 1185969
    :pswitch_7
    sget-object v0, LX/7F7;->WHITESPACE:LX/7F7;

    goto :goto_0

    .line 1185970
    :pswitch_8
    sget-object v0, LX/7F7;->RADIOWRITEIN:LX/7F7;

    goto :goto_0

    .line 1185971
    :pswitch_9
    sget-object v0, LX/7F7;->CHECKBOXWRITEIN:LX/7F7;

    goto :goto_0

    .line 1185972
    :pswitch_a
    sget-object v0, LX/7F7;->NOTIFICATION:LX/7F7;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/7F7;
    .locals 1

    .prologue
    .line 1185973
    const-class v0, LX/7F7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7F7;

    return-object v0
.end method

.method public static values()[LX/7F7;
    .locals 1

    .prologue
    .line 1185974
    sget-object v0, LX/7F7;->$VALUES:[LX/7F7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7F7;

    return-object v0
.end method
