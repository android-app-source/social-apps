.class public final LX/7yu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1280430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280431
    const/16 v0, 0x32

    iput v0, p0, LX/7yu;->a:I

    .line 1280432
    const/4 v0, 0x1

    iput v0, p0, LX/7yu;->b:I

    .line 1280433
    const/16 v0, 0x1f4

    iput v0, p0, LX/7yu;->c:I

    .line 1280434
    return-void
.end method

.method public constructor <init>(III)V
    .locals 4

    .prologue
    const/16 v3, 0x1388

    const/4 v2, 0x1

    .line 1280435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280436
    const/16 v0, 0x1f4

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/7yu;->a:I

    .line 1280437
    invoke-static {p2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/7yu;->b:I

    .line 1280438
    invoke-static {p3, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/7yu;->c:I

    .line 1280439
    return-void
.end method
