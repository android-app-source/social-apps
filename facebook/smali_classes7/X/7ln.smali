.class public final LX/7ln;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/Intent;

.field public final synthetic b:Lcom/facebook/composer/publish/common/PublishPostParams;

.field public final synthetic c:LX/7ll;

.field public final synthetic d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Landroid/content/Intent;Lcom/facebook/composer/publish/common/PublishPostParams;LX/7ll;)V
    .locals 0

    .prologue
    .line 1235687
    iput-object p1, p0, LX/7ln;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iput-object p2, p0, LX/7ln;->a:Landroid/content/Intent;

    iput-object p3, p0, LX/7ln;->b:Lcom/facebook/composer/publish/common/PublishPostParams;

    iput-object p4, p0, LX/7ln;->c:LX/7ll;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1235688
    iget-object v0, p0, LX/7ln;->a:Landroid/content/Intent;

    const-string v1, "publishPostParams"

    new-instance v2, LX/5M9;

    iget-object v3, p0, LX/7ln;->b:Lcom/facebook/composer/publish/common/PublishPostParams;

    invoke-direct {v2, v3}, LX/5M9;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    const/4 v3, 0x1

    .line 1235689
    iput-boolean v3, v2, LX/5M9;->ac:Z

    .line 1235690
    move-object v2, v2

    .line 1235691
    invoke-virtual {v2}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1235692
    iget-object v0, p0, LX/7ln;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v1, p0, LX/7ln;->a:Landroid/content/Intent;

    iget-object v2, p0, LX/7ln;->c:LX/7ll;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/content/Intent;LX/7ll;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1235693
    return-void
.end method

.method public final b()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 1235694
    iget-object v0, p0, LX/7ln;->a:Landroid/content/Intent;

    const-string v1, "extra_optimistic_feed_story"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1235695
    :goto_0
    return-void

    .line 1235696
    :cond_0
    iget-object v0, p0, LX/7ln;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v1, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c:LX/7m8;

    sget-object v2, LX/7m7;->CANCELLED:LX/7m7;

    iget-object v0, p0, LX/7ln;->b:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-object v5, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    iget-object v0, p0, LX/7ln;->b:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-wide v6, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    new-instance v0, LX/2rc;

    invoke-direct {v0}, LX/2rc;-><init>()V

    const/4 v4, 0x0

    .line 1235697
    iput-boolean v4, v0, LX/2rc;->a:Z

    .line 1235698
    move-object v0, v0

    .line 1235699
    invoke-virtual {v0}, LX/2rc;->a()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v8

    move-object v4, v3

    invoke-virtual/range {v1 .. v8}, LX/7m8;->a(LX/7m7;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;JLcom/facebook/composer/publish/common/ErrorDetails;)V

    .line 1235700
    iget-object v0, p0, LX/7ln;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7md;

    iget-object v1, p0, LX/7ln;->b:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-object v1, v1, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/7md;->a(Ljava/lang/String;)V

    .line 1235701
    iget-object v0, p0, LX/7ln;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v0, v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    iget-object v1, p0, LX/7ln;->b:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-object v1, v1, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
