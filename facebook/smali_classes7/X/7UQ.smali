.class public LX/7UQ;
.super Landroid/widget/ImageView;
.source ""


# static fields
.field public static final l:Ljava/lang/String;


# instance fields
.field public A:I

.field public B:Z

.field public C:I

.field public D:F

.field public E:F

.field public final F:F

.field public final G:I

.field public H:Landroid/graphics/RectF;

.field public I:Landroid/graphics/RectF;

.field public J:Landroid/graphics/RectF;

.field private a:LX/7Ua;

.field public m:LX/7Uc;

.field public n:Landroid/graphics/Matrix;

.field public o:Landroid/graphics/Matrix;

.field public p:Landroid/graphics/Matrix;

.field public q:Landroid/graphics/Matrix;

.field public r:Landroid/graphics/Matrix;

.field public s:Landroid/graphics/Matrix;

.field public t:Landroid/os/Handler;

.field public u:Ljava/lang/Runnable;

.field public v:F

.field public w:F

.field public final x:Landroid/graphics/Matrix;

.field public final y:[F

.field public z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1212695
    const-class v0, LX/7UQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7UQ;->l:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 1212696
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1212697
    new-instance v0, LX/7Uc;

    invoke-direct {v0}, LX/7Uc;-><init>()V

    iput-object v0, p0, LX/7UQ;->m:LX/7Uc;

    .line 1212698
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/7UQ;->n:Landroid/graphics/Matrix;

    .line 1212699
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/7UQ;->o:Landroid/graphics/Matrix;

    .line 1212700
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/7UQ;->p:Landroid/graphics/Matrix;

    .line 1212701
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/7UQ;->q:Landroid/graphics/Matrix;

    .line 1212702
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/7UQ;->r:Landroid/graphics/Matrix;

    .line 1212703
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/7UQ;->s:Landroid/graphics/Matrix;

    .line 1212704
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/7UQ;->t:Landroid/os/Handler;

    .line 1212705
    const/4 v0, 0x0

    iput-object v0, p0, LX/7UQ;->u:Ljava/lang/Runnable;

    .line 1212706
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, LX/7UQ;->w:F

    .line 1212707
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/7UQ;->x:Landroid/graphics/Matrix;

    .line 1212708
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, LX/7UQ;->y:[F

    .line 1212709
    iput v1, p0, LX/7UQ;->z:I

    iput v1, p0, LX/7UQ;->A:I

    .line 1212710
    iput-boolean v2, p0, LX/7UQ;->B:Z

    .line 1212711
    iput v2, p0, LX/7UQ;->C:I

    .line 1212712
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, LX/7UQ;->F:F

    .line 1212713
    const/16 v0, 0x190

    iput v0, p0, LX/7UQ;->G:I

    .line 1212714
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/7UQ;->H:Landroid/graphics/RectF;

    .line 1212715
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/7UQ;->I:Landroid/graphics/RectF;

    .line 1212716
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/7UQ;->J:Landroid/graphics/RectF;

    .line 1212717
    invoke-virtual {p0}, LX/7UQ;->a()V

    .line 1212718
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 1212719
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1212720
    new-instance v0, LX/7Uc;

    invoke-direct {v0}, LX/7Uc;-><init>()V

    iput-object v0, p0, LX/7UQ;->m:LX/7Uc;

    .line 1212721
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/7UQ;->n:Landroid/graphics/Matrix;

    .line 1212722
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/7UQ;->o:Landroid/graphics/Matrix;

    .line 1212723
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/7UQ;->p:Landroid/graphics/Matrix;

    .line 1212724
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/7UQ;->q:Landroid/graphics/Matrix;

    .line 1212725
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/7UQ;->r:Landroid/graphics/Matrix;

    .line 1212726
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/7UQ;->s:Landroid/graphics/Matrix;

    .line 1212727
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/7UQ;->t:Landroid/os/Handler;

    .line 1212728
    const/4 v0, 0x0

    iput-object v0, p0, LX/7UQ;->u:Ljava/lang/Runnable;

    .line 1212729
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, LX/7UQ;->w:F

    .line 1212730
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/7UQ;->x:Landroid/graphics/Matrix;

    .line 1212731
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, LX/7UQ;->y:[F

    .line 1212732
    iput v1, p0, LX/7UQ;->z:I

    iput v1, p0, LX/7UQ;->A:I

    .line 1212733
    iput-boolean v2, p0, LX/7UQ;->B:Z

    .line 1212734
    iput v2, p0, LX/7UQ;->C:I

    .line 1212735
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, LX/7UQ;->F:F

    .line 1212736
    const/16 v0, 0x190

    iput v0, p0, LX/7UQ;->G:I

    .line 1212737
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/7UQ;->H:Landroid/graphics/RectF;

    .line 1212738
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/7UQ;->I:Landroid/graphics/RectF;

    .line 1212739
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/7UQ;->J:Landroid/graphics/RectF;

    .line 1212740
    invoke-virtual {p0}, LX/7UQ;->a()V

    .line 1212741
    return-void
.end method

.method private a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 1212742
    iget-object v0, p0, LX/7UQ;->x:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/7UQ;->n:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1212743
    iget-object v0, p0, LX/7UQ;->x:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1212744
    iget-object v0, p0, LX/7UQ;->x:Landroid/graphics/Matrix;

    return-object v0
.end method

.method private a(FF)V
    .locals 1

    .prologue
    .line 1212745
    iget-object v0, p0, LX/7UQ;->o:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1212746
    invoke-virtual {p0}, LX/7UQ;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/7UQ;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1212747
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 1212748
    invoke-virtual {p0}, LX/7UQ;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 1212749
    invoke-virtual {p0}, LX/7UQ;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 1212750
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    .line 1212751
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    int-to-float v3, v3

    .line 1212752
    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    .line 1212753
    cmpl-float v4, v2, v0

    if-gtz v4, :cond_0

    cmpl-float v4, v3, v1

    if-lez v4, :cond_1

    .line 1212754
    :cond_0
    div-float v4, v0, v2

    invoke-static {v4, v7}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 1212755
    div-float v5, v1, v3

    invoke-static {v5, v7}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 1212756
    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 1212757
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "scale: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1212758
    invoke-virtual {p2, v4, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1212759
    mul-float/2addr v2, v4

    sub-float/2addr v0, v2

    div-float/2addr v0, v7

    .line 1212760
    mul-float v2, v3, v4

    sub-float/2addr v1, v2

    div-float/2addr v1, v7

    .line 1212761
    invoke-virtual {p2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1212762
    :goto_0
    return-void

    .line 1212763
    :cond_1
    sub-float/2addr v0, v2

    div-float/2addr v0, v7

    .line 1212764
    sub-float/2addr v1, v3

    div-float/2addr v1, v7

    .line 1212765
    invoke-virtual {p2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0
.end method

.method private b()F
    .locals 3

    .prologue
    .line 1212766
    invoke-virtual {p0}, LX/7UQ;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1212767
    if-nez v0, :cond_0

    .line 1212768
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1212769
    :goto_0
    return v0

    .line 1212770
    :cond_0
    invoke-virtual {p0}, LX/7UQ;->getPhotoWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LX/7UQ;->z:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1212771
    invoke-virtual {p0}, LX/7UQ;->getPhotoHeight()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, LX/7UQ;->A:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 1212772
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v0, v1

    .line 1212773
    goto :goto_0
.end method

.method private b(Landroid/graphics/Matrix;)V
    .locals 9

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 1212774
    invoke-virtual {p0}, LX/7UQ;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 1212775
    invoke-virtual {p0}, LX/7UQ;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 1212776
    invoke-virtual {p0}, LX/7UQ;->getPhotoWidth()I

    move-result v2

    int-to-float v2, v2

    .line 1212777
    invoke-virtual {p0}, LX/7UQ;->getPhotoHeight()I

    move-result v3

    int-to-float v3, v3

    .line 1212778
    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    .line 1212779
    const/high16 v8, 0x40000000    # 2.0f

    .line 1212780
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 1212781
    iget v5, p0, LX/7UQ;->C:I

    if-eqz v5, :cond_0

    .line 1212782
    invoke-virtual {p0}, LX/7UQ;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    neg-int v5, v5

    int-to-float v5, v5

    div-float/2addr v5, v8

    invoke-virtual {p0}, LX/7UQ;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    neg-int v7, v7

    int-to-float v7, v7

    div-float/2addr v7, v8

    invoke-virtual {v4, v5, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1212783
    iget v5, p0, LX/7UQ;->C:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1212784
    invoke-virtual {p0}, LX/7UQ;->getPhotoWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v8

    invoke-virtual {p0}, LX/7UQ;->getPhotoHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v8

    invoke-virtual {v4, v5, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1212785
    :cond_0
    move-object v4, v4

    .line 1212786
    iput-object v4, p0, LX/7UQ;->p:Landroid/graphics/Matrix;

    .line 1212787
    const/high16 v8, 0x40000000    # 2.0f

    .line 1212788
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 1212789
    iget v5, p0, LX/7UQ;->C:I

    if-eqz v5, :cond_1

    .line 1212790
    invoke-virtual {p0}, LX/7UQ;->getPhotoWidth()I

    move-result v5

    neg-int v5, v5

    int-to-float v5, v5

    div-float/2addr v5, v8

    invoke-virtual {p0}, LX/7UQ;->getPhotoHeight()I

    move-result v7

    neg-int v7, v7

    int-to-float v7, v7

    div-float/2addr v7, v8

    invoke-virtual {v4, v5, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1212791
    iget v5, p0, LX/7UQ;->C:I

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1212792
    invoke-virtual {p0}, LX/7UQ;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v8

    invoke-virtual {p0}, LX/7UQ;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v8

    invoke-virtual {v4, v5, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1212793
    :cond_1
    move-object v4, v4

    .line 1212794
    iput-object v4, p0, LX/7UQ;->q:Landroid/graphics/Matrix;

    .line 1212795
    iget-object v4, p0, LX/7UQ;->p:Landroid/graphics/Matrix;

    invoke-virtual {p1, v4}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1212796
    div-float v4, v0, v2

    .line 1212797
    div-float v5, v1, v3

    .line 1212798
    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 1212799
    invoke-virtual {p1, v4, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1212800
    mul-float/2addr v3, v4

    sub-float/2addr v1, v3

    div-float/2addr v1, v6

    .line 1212801
    iget v3, p0, LX/7UQ;->E:F

    add-float/2addr v3, v1

    const/4 v5, 0x0

    invoke-static {v3, v5}, Ljava/lang/Math;->max(FF)F

    move-result v3

    sub-float/2addr v3, v1

    iput v3, p0, LX/7UQ;->D:F

    .line 1212802
    mul-float/2addr v2, v4

    sub-float/2addr v0, v2

    div-float/2addr v0, v6

    iget v2, p0, LX/7UQ;->D:F

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1212803
    iget-object v0, p0, LX/7UQ;->r:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1212804
    iget-object v0, p0, LX/7UQ;->r:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/7UQ;->q:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 1212805
    return-void
.end method

.method private c(Landroid/graphics/Matrix;)Landroid/graphics/RectF;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1212806
    invoke-virtual {p0}, LX/7UQ;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1212807
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1212808
    :goto_0
    return-object v0

    .line 1212809
    :cond_0
    invoke-direct {p0, p1}, LX/7UQ;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v1

    .line 1212810
    iget-object v2, p0, LX/7UQ;->H:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v2, v4, v4, v3, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1212811
    iget-object v0, p0, LX/7UQ;->H:Landroid/graphics/RectF;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1212812
    iget-object v0, p0, LX/7UQ;->H:Landroid/graphics/RectF;

    goto :goto_0
.end method

.method private d(Landroid/graphics/Matrix;)F
    .locals 2

    .prologue
    .line 1212813
    const/4 v0, 0x0

    .line 1212814
    iget-object v1, p0, LX/7UQ;->y:[F

    invoke-virtual {p1, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1212815
    iget-object v1, p0, LX/7UQ;->y:[F

    aget v1, v1, v0

    move v0, v1

    .line 1212816
    return v0
.end method

.method private d()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1212817
    iget v1, p0, LX/7UQ;->C:I

    div-int/lit8 v1, v1, 0x5a

    rem-int/lit8 v1, v1, 0x2

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;ZZ)Landroid/graphics/RectF;
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 1212818
    invoke-virtual {p0}, LX/7UQ;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1212819
    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1212820
    :goto_0
    return-object v0

    .line 1212821
    :cond_0
    iget-object v0, p0, LX/7UQ;->I:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1212822
    invoke-direct {p0, p1}, LX/7UQ;->c(Landroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object v3

    .line 1212823
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v0

    .line 1212824
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v4

    .line 1212825
    if-eqz p3, :cond_6

    .line 1212826
    invoke-virtual {p0}, LX/7UQ;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget v5, p0, LX/7UQ;->D:F

    mul-float/2addr v5, v6

    add-float/2addr v2, v5

    float-to-int v2, v2

    .line 1212827
    int-to-float v5, v2

    cmpg-float v5, v0, v5

    if-gez v5, :cond_1

    .line 1212828
    int-to-float v2, v2

    sub-float v0, v2, v0

    div-float/2addr v0, v6

    iget v2, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v2

    move v2, v0

    .line 1212829
    :goto_1
    if-eqz p2, :cond_5

    .line 1212830
    invoke-virtual {p0}, LX/7UQ;->getWidth()I

    move-result v0

    .line 1212831
    int-to-float v5, v0

    cmpg-float v5, v4, v5

    if-gez v5, :cond_3

    .line 1212832
    int-to-float v0, v0

    sub-float/2addr v0, v4

    div-float/2addr v0, v6

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v3

    .line 1212833
    :goto_2
    iget-object v3, p0, LX/7UQ;->I:Landroid/graphics/RectF;

    invoke-virtual {v3, v0, v2, v1, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1212834
    iget-object v0, p0, LX/7UQ;->I:Landroid/graphics/RectF;

    goto :goto_0

    .line 1212835
    :cond_1
    iget v0, v3, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 1212836
    iget v0, v3, Landroid/graphics/RectF;->top:F

    neg-float v0, v0

    move v2, v0

    goto :goto_1

    .line 1212837
    :cond_2
    iget v0, v3, Landroid/graphics/RectF;->bottom:F

    int-to-float v2, v2

    cmpg-float v0, v0, v2

    if-gez v0, :cond_6

    .line 1212838
    invoke-virtual {p0}, LX/7UQ;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v2, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v2

    move v2, v0

    goto :goto_1

    .line 1212839
    :cond_3
    iget v4, v3, Landroid/graphics/RectF;->left:F

    cmpl-float v4, v4, v1

    if-lez v4, :cond_4

    .line 1212840
    iget v0, v3, Landroid/graphics/RectF;->left:F

    neg-float v0, v0

    goto :goto_2

    .line 1212841
    :cond_4
    iget v4, v3, Landroid/graphics/RectF;->right:F

    int-to-float v5, v0

    cmpg-float v4, v4, v5

    if-gez v4, :cond_5

    .line 1212842
    int-to-float v0, v0

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v3

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    move v2, v1

    goto :goto_1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 1212849
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, LX/7UQ;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1212850
    return-void
.end method

.method public a(DD)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1212843
    invoke-virtual {p0}, LX/7UQ;->getBitmapRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 1212844
    iget-object v1, p0, LX/7UQ;->J:Landroid/graphics/RectF;

    double-to-float v2, p1

    double-to-float v3, p3

    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1212845
    iget-object v1, p0, LX/7UQ;->J:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, LX/7UQ;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 1212846
    iget-object v0, p0, LX/7UQ;->J:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, LX/7UQ;->J:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    invoke-direct {p0, v0, v1}, LX/7UQ;->a(FF)V

    .line 1212847
    invoke-virtual {p0, v5, v5}, LX/7UQ;->a(ZZ)V

    .line 1212848
    return-void
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 1212907
    return-void
.end method

.method public final a(FFD)V
    .locals 11

    .prologue
    .line 1212902
    float-to-double v6, p1

    .line 1212903
    float-to-double v8, p2

    .line 1212904
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1212905
    iget-object v10, p0, LX/7UQ;->t:Landroid/os/Handler;

    new-instance v0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;

    move-object v1, p0

    move-wide v2, p3

    invoke-direct/range {v0 .. v9}, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;-><init>(LX/7UQ;DJDD)V

    const v1, -0x1ced24b8

    invoke-static {v10, v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1212906
    return-void
.end method

.method public final a(FFF)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1212894
    invoke-virtual {p0}, LX/7UQ;->getScale()F

    move-result v0

    .line 1212895
    div-float v0, p1, v0

    .line 1212896
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "zoomTo: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", center: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1212897
    iget-object v1, p0, LX/7UQ;->o:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0, v0, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1212898
    invoke-virtual {p0}, LX/7UQ;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/7UQ;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1212899
    invoke-virtual {p0}, LX/7UQ;->getScale()F

    move-result v0

    invoke-virtual {p0, v0}, LX/7UQ;->a(F)V

    .line 1212900
    invoke-virtual {p0, v3, v3}, LX/7UQ;->a(ZZ)V

    .line 1212901
    return-void
.end method

.method public a(FFFF)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    .line 1212680
    invoke-virtual {p0}, LX/7UQ;->getMaxZoom()F

    move-result v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, LX/7UQ;->getMaxZoom()F

    move-result p1

    .line 1212681
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1212682
    invoke-virtual {p0}, LX/7UQ;->getScale()F

    move-result v7

    .line 1212683
    sub-float v6, p1, v7

    .line 1212684
    new-instance v0, Landroid/graphics/Matrix;

    iget-object v1, p0, LX/7UQ;->o:Landroid/graphics/Matrix;

    invoke-direct {v0, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 1212685
    invoke-virtual {v0, p1, p1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1212686
    invoke-virtual {p0, v0, v2, v2}, LX/7UQ;->a(Landroid/graphics/Matrix;ZZ)Landroid/graphics/RectF;

    move-result-object v0

    .line 1212687
    iget v1, v0, Landroid/graphics/RectF;->left:F

    mul-float/2addr v1, p1

    add-float v8, p2, v1

    .line 1212688
    iget v0, v0, Landroid/graphics/RectF;->top:F

    mul-float/2addr v0, p1

    add-float v9, p3, v0

    .line 1212689
    iget-object v0, p0, LX/7UQ;->t:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;

    move-object v2, p0

    move v3, p4

    invoke-direct/range {v1 .. v9}, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;-><init>(LX/7UQ;FJFFFF)V

    const v2, -0x715964b1

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1212690
    return-void
.end method

.method public a(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1212877
    invoke-virtual {p0}, LX/7UQ;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 1212878
    invoke-virtual {p0}, LX/7UQ;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 1212879
    iget v2, p1, Landroid/graphics/RectF;->top:F

    cmpl-float v2, v2, v4

    if-ltz v2, :cond_0

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v2, v2, v1

    if-gtz v2, :cond_0

    iput v4, p2, Landroid/graphics/RectF;->top:F

    .line 1212880
    :cond_0
    iget v2, p1, Landroid/graphics/RectF;->left:F

    cmpl-float v2, v2, v4

    if-ltz v2, :cond_1

    iget v2, p1, Landroid/graphics/RectF;->right:F

    cmpg-float v2, v2, v0

    if-gtz v2, :cond_1

    iput v4, p2, Landroid/graphics/RectF;->left:F

    .line 1212881
    :cond_1
    iget v2, p1, Landroid/graphics/RectF;->top:F

    iget v3, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v3

    cmpl-float v2, v2, v4

    if-ltz v2, :cond_2

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_2

    .line 1212882
    iget v2, p1, Landroid/graphics/RectF;->top:F

    sub-float v2, v4, v2

    float-to-int v2, v2

    int-to-float v2, v2

    iput v2, p2, Landroid/graphics/RectF;->top:F

    .line 1212883
    :cond_2
    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    iget v3, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v3

    sub-float v3, v1, v4

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_3

    iget v2, p1, Landroid/graphics/RectF;->top:F

    cmpg-float v2, v2, v4

    if-gez v2, :cond_3

    .line 1212884
    sub-float/2addr v1, v4

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v1, v1

    iput v1, p2, Landroid/graphics/RectF;->top:F

    .line 1212885
    :cond_3
    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v2

    cmpl-float v1, v1, v4

    if-ltz v1, :cond_4

    iget v1, p1, Landroid/graphics/RectF;->left:F

    sub-float v1, v4, v1

    float-to-int v1, v1

    int-to-float v1, v1

    iput v1, p2, Landroid/graphics/RectF;->left:F

    .line 1212886
    :cond_4
    iget v1, p1, Landroid/graphics/RectF;->right:F

    iget v2, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v2

    sub-float v2, v0, v4

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_5

    .line 1212887
    sub-float/2addr v0, v4

    iget v1, p1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p2, Landroid/graphics/RectF;->left:F

    .line 1212888
    :cond_5
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1212876
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;ZLandroid/graphics/Matrix;F)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/Matrix;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1212859
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1212860
    if-eqz p1, :cond_3

    .line 1212861
    iget-boolean v0, p0, LX/7UQ;->B:Z

    if-eqz v0, :cond_2

    .line 1212862
    iget-object v0, p0, LX/7UQ;->n:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, LX/7UQ;->b(Landroid/graphics/Matrix;)V

    .line 1212863
    iget-object v0, p0, LX/7UQ;->n:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, LX/7UQ;->d(Landroid/graphics/Matrix;)F

    move-result v0

    invoke-virtual {p0, v0}, LX/7UQ;->setMinZoom(F)V

    .line 1212864
    :goto_0
    if-eqz p2, :cond_0

    .line 1212865
    iget-object v0, p0, LX/7UQ;->o:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1212866
    if-eqz p3, :cond_0

    .line 1212867
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0, p3}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v0, p0, LX/7UQ;->o:Landroid/graphics/Matrix;

    .line 1212868
    :cond_0
    invoke-virtual {p0}, LX/7UQ;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/7UQ;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1212869
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p4, v0

    if-gez v0, :cond_1

    invoke-direct {p0}, LX/7UQ;->b()F

    move-result p4

    .line 1212870
    :cond_1
    iput p4, p0, LX/7UQ;->v:F

    .line 1212871
    invoke-virtual {p0, p1}, LX/7UQ;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1212872
    return-void

    .line 1212873
    :cond_2
    iget-object v0, p0, LX/7UQ;->n:Landroid/graphics/Matrix;

    invoke-direct {p0, p1, v0}, LX/7UQ;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;)V

    .line 1212874
    invoke-virtual {p0}, LX/7UQ;->getMinZoom()F

    move-result v0

    invoke-virtual {p0, v0}, LX/7UQ;->setMinZoom(F)V

    goto :goto_0

    .line 1212875
    :cond_3
    iget-object v0, p0, LX/7UQ;->n:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    goto :goto_0
.end method

.method public a(ZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1212852
    invoke-virtual {p0}, LX/7UQ;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1212853
    if-nez v0, :cond_1

    .line 1212854
    :cond_0
    :goto_0
    return-void

    .line 1212855
    :cond_1
    iget-object v0, p0, LX/7UQ;->o:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0, p1, p2}, LX/7UQ;->a(Landroid/graphics/Matrix;ZZ)Landroid/graphics/RectF;

    move-result-object v0

    .line 1212856
    iget v1, v0, Landroid/graphics/RectF;->left:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    iget v1, v0, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 1212857
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "center.rect: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1212858
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v0, v0, Landroid/graphics/RectF;->top:F

    invoke-direct {p0, v1, v0}, LX/7UQ;->a(FF)V

    goto :goto_0
.end method

.method public b(F)V
    .locals 0

    .prologue
    .line 1212851
    return-void
.end method

.method public final b(FF)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1212691
    invoke-virtual {p0}, LX/7UQ;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    .line 1212692
    invoke-virtual {p0}, LX/7UQ;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    .line 1212693
    invoke-virtual {p0, p1, v0, v1, p2}, LX/7UQ;->a(FFFF)V

    .line 1212694
    return-void
.end method

.method public final b(Landroid/graphics/drawable/Drawable;ZLandroid/graphics/Matrix;F)V
    .locals 6
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/Matrix;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1212889
    invoke-virtual {p0}, LX/7UQ;->getWidth()I

    move-result v0

    .line 1212890
    if-gtz v0, :cond_0

    .line 1212891
    new-instance v0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$1;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$1;-><init>(LX/7UQ;Landroid/graphics/drawable/Drawable;ZLandroid/graphics/Matrix;F)V

    iput-object v0, p0, LX/7UQ;->u:Ljava/lang/Runnable;

    .line 1212892
    :goto_0
    return-void

    .line 1212893
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, LX/7UQ;->a(Landroid/graphics/drawable/Drawable;ZLandroid/graphics/Matrix;F)V

    goto :goto_0
.end method

.method public c(F)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1212648
    invoke-virtual {p0}, LX/7UQ;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    .line 1212649
    invoke-virtual {p0}, LX/7UQ;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    .line 1212650
    invoke-virtual {p0, p1, v0, v1}, LX/7UQ;->a(FFF)V

    .line 1212651
    return-void
.end method

.method public final c(FF)V
    .locals 4

    .prologue
    .line 1212646
    float-to-double v0, p1

    float-to-double v2, p2

    invoke-virtual {p0, v0, v1, v2, v3}, LX/7UQ;->a(DD)V

    .line 1212647
    return-void
.end method

.method public getBaseMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 1212645
    iget-object v0, p0, LX/7UQ;->n:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getBasePhotoDisplayMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 1212616
    iget-object v0, p0, LX/7UQ;->r:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getBitmapRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1212628
    iget-object v0, p0, LX/7UQ;->o:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, LX/7UQ;->c(Landroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayMatrix()Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 1212627
    new-instance v0, Landroid/graphics/Matrix;

    iget-object v1, p0, LX/7UQ;->o:Landroid/graphics/Matrix;

    invoke-direct {v0, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    return-object v0
.end method

.method public getImageViewMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 1212626
    iget-object v0, p0, LX/7UQ;->o:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, LX/7UQ;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public getMaxZoom()F
    .locals 2

    .prologue
    .line 1212623
    iget v0, p0, LX/7UQ;->v:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1212624
    invoke-direct {p0}, LX/7UQ;->b()F

    move-result v0

    iput v0, p0, LX/7UQ;->v:F

    .line 1212625
    :cond_0
    iget v0, p0, LX/7UQ;->v:F

    return v0
.end method

.method public getMinZoom()F
    .locals 2

    .prologue
    .line 1212619
    iget v0, p0, LX/7UQ;->w:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1212620
    const/high16 v0, 0x3f800000    # 1.0f

    move v0, v0

    .line 1212621
    iput v0, p0, LX/7UQ;->w:F

    .line 1212622
    :cond_0
    iget v0, p0, LX/7UQ;->w:F

    return v0
.end method

.method public getPhotoDisplayMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 1212618
    iget-object v0, p0, LX/7UQ;->s:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getPhotoHeight()I
    .locals 1

    .prologue
    .line 1212617
    invoke-direct {p0}, LX/7UQ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/7UQ;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/7UQ;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getPhotoWidth()I
    .locals 1

    .prologue
    .line 1212652
    invoke-direct {p0}, LX/7UQ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/7UQ;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/7UQ;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    goto :goto_0
.end method

.method public getRotation()F
    .locals 1

    .prologue
    .line 1212653
    const/4 v0, 0x0

    return v0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 1212654
    iget-object v0, p0, LX/7UQ;->o:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, LX/7UQ;->d(Landroid/graphics/Matrix;)F

    move-result v0

    return v0
.end method

.method public onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1212629
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 1212630
    sub-int v0, p4, p2

    iput v0, p0, LX/7UQ;->z:I

    .line 1212631
    sub-int v0, p5, p3

    iput v0, p0, LX/7UQ;->A:I

    .line 1212632
    iget-object v0, p0, LX/7UQ;->u:Ljava/lang/Runnable;

    .line 1212633
    if-eqz v0, :cond_0

    .line 1212634
    const/4 v1, 0x0

    iput-object v1, p0, LX/7UQ;->u:Ljava/lang/Runnable;

    .line 1212635
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1212636
    :cond_0
    invoke-virtual {p0}, LX/7UQ;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1212637
    iget-boolean v0, p0, LX/7UQ;->B:Z

    if-eqz v0, :cond_2

    .line 1212638
    iget-object v0, p0, LX/7UQ;->n:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, LX/7UQ;->b(Landroid/graphics/Matrix;)V

    .line 1212639
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, LX/7UQ;->setMinZoom(F)V

    .line 1212640
    :goto_0
    invoke-virtual {p0}, LX/7UQ;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/7UQ;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1212641
    invoke-virtual {p0}, LX/7UQ;->getMinZoom()F

    move-result v0

    invoke-virtual {p0, v0}, LX/7UQ;->c(F)V

    .line 1212642
    :cond_1
    return-void

    .line 1212643
    :cond_2
    invoke-virtual {p0}, LX/7UQ;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, LX/7UQ;->n:Landroid/graphics/Matrix;

    invoke-direct {p0, v0, v1}, LX/7UQ;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;)V

    .line 1212644
    invoke-virtual {p0}, LX/7UQ;->getMinZoom()F

    move-result v0

    invoke-virtual {p0, v0}, LX/7UQ;->setMinZoom(F)V

    goto :goto_0
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 1212655
    const/4 v0, 0x1

    .line 1212656
    const/4 v1, 0x0

    .line 1212657
    const/high16 v2, -0x40800000    # -1.0f

    .line 1212658
    if-eqz p1, :cond_0

    .line 1212659
    new-instance v3, LX/7Ud;

    invoke-direct {v3, p1}, LX/7Ud;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v3, v0, v1, v2}, LX/7UQ;->b(Landroid/graphics/drawable/Drawable;ZLandroid/graphics/Matrix;F)V

    .line 1212660
    :goto_0
    return-void

    .line 1212661
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p0, v3, v0, v1, v2}, LX/7UQ;->b(Landroid/graphics/drawable/Drawable;ZLandroid/graphics/Matrix;F)V

    goto :goto_0
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1212662
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    invoke-virtual {p0, p1, v0, v1, v2}, LX/7UQ;->b(Landroid/graphics/drawable/Drawable;ZLandroid/graphics/Matrix;F)V

    .line 1212663
    return-void
.end method

.method public setImageMatrix(Landroid/graphics/Matrix;)V
    .locals 2

    .prologue
    .line 1212664
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1212665
    iget-object v0, p0, LX/7UQ;->s:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1212666
    iget-object v0, p0, LX/7UQ;->s:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/7UQ;->q:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 1212667
    return-void
.end method

.method public setImageResource(I)V
    .locals 1

    .prologue
    .line 1212668
    invoke-virtual {p0}, LX/7UQ;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/7UQ;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1212669
    return-void
.end method

.method public setMinZoom(F)V
    .locals 2

    .prologue
    .line 1212670
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "minZoom: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1212671
    iput p1, p0, LX/7UQ;->w:F

    .line 1212672
    return-void
.end method

.method public setOnBitmapChangedListener(LX/7Ua;)V
    .locals 0

    .prologue
    .line 1212673
    iput-object p1, p0, LX/7UQ;->a:LX/7Ua;

    .line 1212674
    return-void
.end method

.method public setPhotoOffset(F)V
    .locals 0

    .prologue
    .line 1212675
    iput p1, p0, LX/7UQ;->E:F

    .line 1212676
    return-void
.end method

.method public setRotation(I)V
    .locals 1

    .prologue
    .line 1212677
    rem-int/lit8 v0, p1, 0x5a

    if-nez v0, :cond_0

    .line 1212678
    rem-int/lit16 v0, p1, 0x168

    iput v0, p0, LX/7UQ;->C:I

    .line 1212679
    :cond_0
    return-void
.end method
