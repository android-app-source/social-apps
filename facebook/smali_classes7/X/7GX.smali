.class public LX/7GX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;

.field private static final l:LX/1sw;

.field private static final m:LX/1sw;

.field private static final n:LX/1sw;


# instance fields
.field public final deltaBatchSize:Ljava/lang/Integer;

.field public final deviceId:Ljava/lang/String;

.field public final deviceParams:Ljava/lang/String;

.field public final encoding:Ljava/lang/String;

.field public final entityFbid:Ljava/lang/Long;

.field public final lastSeqId:Ljava/lang/Long;

.field public final maxDeltasAbleToProcess:Ljava/lang/Integer;

.field public final queueParams:Ljava/lang/String;

.field public final queueType:Ljava/lang/String;

.field public final syncApiVersion:Ljava/lang/Integer;

.field public final syncToken:Ljava/lang/String;

.field public final syncTokenLong:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/16 v5, 0xa

    const/16 v4, 0x8

    const/16 v3, 0xb

    .line 1189261
    new-instance v0, LX/1sv;

    const-string v1, "GetIrisDiffs"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/7GX;->b:LX/1sv;

    .line 1189262
    new-instance v0, LX/1sw;

    const-string v1, "syncToken"

    invoke-direct {v0, v1, v3, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/7GX;->c:LX/1sw;

    .line 1189263
    new-instance v0, LX/1sw;

    const-string v1, "lastSeqId"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/7GX;->d:LX/1sw;

    .line 1189264
    new-instance v0, LX/1sw;

    const-string v1, "maxDeltasAbleToProcess"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/7GX;->e:LX/1sw;

    .line 1189265
    new-instance v0, LX/1sw;

    const-string v1, "deltaBatchSize"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/7GX;->f:LX/1sw;

    .line 1189266
    new-instance v0, LX/1sw;

    const-string v1, "encoding"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/7GX;->g:LX/1sw;

    .line 1189267
    new-instance v0, LX/1sw;

    const-string v1, "queueType"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/7GX;->h:LX/1sw;

    .line 1189268
    new-instance v0, LX/1sw;

    const-string v1, "syncApiVersion"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/7GX;->i:LX/1sw;

    .line 1189269
    new-instance v0, LX/1sw;

    const-string v1, "deviceId"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/7GX;->j:LX/1sw;

    .line 1189270
    new-instance v0, LX/1sw;

    const-string v1, "deviceParams"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/7GX;->k:LX/1sw;

    .line 1189271
    new-instance v0, LX/1sw;

    const-string v1, "queueParams"

    invoke-direct {v0, v1, v3, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/7GX;->l:LX/1sw;

    .line 1189272
    new-instance v0, LX/1sw;

    const-string v1, "entityFbid"

    invoke-direct {v0, v1, v5, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/7GX;->m:LX/1sw;

    .line 1189273
    new-instance v0, LX/1sw;

    const-string v1, "syncTokenLong"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/7GX;->n:LX/1sw;

    .line 1189274
    sput-boolean v6, LX/7GX;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1189495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189496
    iput-object p1, p0, LX/7GX;->syncToken:Ljava/lang/String;

    .line 1189497
    iput-object p2, p0, LX/7GX;->lastSeqId:Ljava/lang/Long;

    .line 1189498
    iput-object p3, p0, LX/7GX;->maxDeltasAbleToProcess:Ljava/lang/Integer;

    .line 1189499
    iput-object p4, p0, LX/7GX;->deltaBatchSize:Ljava/lang/Integer;

    .line 1189500
    iput-object p5, p0, LX/7GX;->encoding:Ljava/lang/String;

    .line 1189501
    iput-object p6, p0, LX/7GX;->queueType:Ljava/lang/String;

    .line 1189502
    iput-object p7, p0, LX/7GX;->syncApiVersion:Ljava/lang/Integer;

    .line 1189503
    iput-object p8, p0, LX/7GX;->deviceId:Ljava/lang/String;

    .line 1189504
    iput-object p9, p0, LX/7GX;->deviceParams:Ljava/lang/String;

    .line 1189505
    iput-object p10, p0, LX/7GX;->queueParams:Ljava/lang/String;

    .line 1189506
    iput-object p11, p0, LX/7GX;->entityFbid:Ljava/lang/Long;

    .line 1189507
    iput-object p12, p0, LX/7GX;->syncTokenLong:Ljava/lang/Long;

    .line 1189508
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1189492
    iget-object v0, p0, LX/7GX;->lastSeqId:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 1189493
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'lastSeqId\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/7GX;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1189494
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1189371
    if-eqz p2, :cond_c

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1189372
    :goto_0
    if-eqz p2, :cond_d

    const-string v0, "\n"

    move-object v2, v0

    .line 1189373
    :goto_1
    if-eqz p2, :cond_e

    const-string v0, " "

    .line 1189374
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "GetIrisDiffs"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1189375
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189376
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189377
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189378
    const/4 v1, 0x1

    .line 1189379
    iget-object v5, p0, LX/7GX;->syncToken:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 1189380
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189381
    const-string v1, "syncToken"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189382
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189383
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189384
    iget-object v1, p0, LX/7GX;->syncToken:Ljava/lang/String;

    if-nez v1, :cond_f

    .line 1189385
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189386
    :goto_3
    const/4 v1, 0x0

    .line 1189387
    :cond_0
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189388
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189389
    const-string v1, "lastSeqId"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189390
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189391
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189392
    iget-object v1, p0, LX/7GX;->lastSeqId:Ljava/lang/Long;

    if-nez v1, :cond_10

    .line 1189393
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189394
    :goto_4
    iget-object v1, p0, LX/7GX;->maxDeltasAbleToProcess:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1189395
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189396
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189397
    const-string v1, "maxDeltasAbleToProcess"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189398
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189399
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189400
    iget-object v1, p0, LX/7GX;->maxDeltasAbleToProcess:Ljava/lang/Integer;

    if-nez v1, :cond_11

    .line 1189401
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189402
    :cond_2
    :goto_5
    iget-object v1, p0, LX/7GX;->deltaBatchSize:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1189403
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189404
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189405
    const-string v1, "deltaBatchSize"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189406
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189407
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189408
    iget-object v1, p0, LX/7GX;->deltaBatchSize:Ljava/lang/Integer;

    if-nez v1, :cond_12

    .line 1189409
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189410
    :cond_3
    :goto_6
    iget-object v1, p0, LX/7GX;->encoding:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1189411
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189412
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189413
    const-string v1, "encoding"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189414
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189415
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189416
    iget-object v1, p0, LX/7GX;->encoding:Ljava/lang/String;

    if-nez v1, :cond_13

    .line 1189417
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189418
    :cond_4
    :goto_7
    iget-object v1, p0, LX/7GX;->queueType:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1189419
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189420
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189421
    const-string v1, "queueType"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189422
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189423
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189424
    iget-object v1, p0, LX/7GX;->queueType:Ljava/lang/String;

    if-nez v1, :cond_14

    .line 1189425
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189426
    :cond_5
    :goto_8
    iget-object v1, p0, LX/7GX;->syncApiVersion:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 1189427
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189428
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189429
    const-string v1, "syncApiVersion"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189430
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189431
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189432
    iget-object v1, p0, LX/7GX;->syncApiVersion:Ljava/lang/Integer;

    if-nez v1, :cond_15

    .line 1189433
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189434
    :cond_6
    :goto_9
    iget-object v1, p0, LX/7GX;->deviceId:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1189435
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189436
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189437
    const-string v1, "deviceId"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189438
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189439
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189440
    iget-object v1, p0, LX/7GX;->deviceId:Ljava/lang/String;

    if-nez v1, :cond_16

    .line 1189441
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189442
    :cond_7
    :goto_a
    iget-object v1, p0, LX/7GX;->deviceParams:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 1189443
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189444
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189445
    const-string v1, "deviceParams"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189446
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189447
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189448
    iget-object v1, p0, LX/7GX;->deviceParams:Ljava/lang/String;

    if-nez v1, :cond_17

    .line 1189449
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189450
    :cond_8
    :goto_b
    iget-object v1, p0, LX/7GX;->queueParams:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1189451
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189452
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189453
    const-string v1, "queueParams"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189454
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189455
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189456
    iget-object v1, p0, LX/7GX;->queueParams:Ljava/lang/String;

    if-nez v1, :cond_18

    .line 1189457
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189458
    :cond_9
    :goto_c
    iget-object v1, p0, LX/7GX;->entityFbid:Ljava/lang/Long;

    if-eqz v1, :cond_a

    .line 1189459
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189460
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189461
    const-string v1, "entityFbid"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189462
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189463
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189464
    iget-object v1, p0, LX/7GX;->entityFbid:Ljava/lang/Long;

    if-nez v1, :cond_19

    .line 1189465
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189466
    :cond_a
    :goto_d
    iget-object v1, p0, LX/7GX;->syncTokenLong:Ljava/lang/Long;

    if-eqz v1, :cond_b

    .line 1189467
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189468
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189469
    const-string v1, "syncTokenLong"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189470
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189471
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189472
    iget-object v0, p0, LX/7GX;->syncTokenLong:Ljava/lang/Long;

    if-nez v0, :cond_1a

    .line 1189473
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189474
    :cond_b
    :goto_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189475
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189476
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1189477
    :cond_c
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1189478
    :cond_d
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1189479
    :cond_e
    const-string v0, ""

    goto/16 :goto_2

    .line 1189480
    :cond_f
    iget-object v1, p0, LX/7GX;->syncToken:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1189481
    :cond_10
    iget-object v1, p0, LX/7GX;->lastSeqId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1189482
    :cond_11
    iget-object v1, p0, LX/7GX;->maxDeltasAbleToProcess:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1189483
    :cond_12
    iget-object v1, p0, LX/7GX;->deltaBatchSize:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1189484
    :cond_13
    iget-object v1, p0, LX/7GX;->encoding:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1189485
    :cond_14
    iget-object v1, p0, LX/7GX;->queueType:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1189486
    :cond_15
    iget-object v1, p0, LX/7GX;->syncApiVersion:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 1189487
    :cond_16
    iget-object v1, p0, LX/7GX;->deviceId:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 1189488
    :cond_17
    iget-object v1, p0, LX/7GX;->deviceParams:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    .line 1189489
    :cond_18
    iget-object v1, p0, LX/7GX;->queueParams:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    .line 1189490
    :cond_19
    iget-object v1, p0, LX/7GX;->entityFbid:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_d

    .line 1189491
    :cond_1a
    iget-object v0, p0, LX/7GX;->syncTokenLong:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_e
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1189509
    invoke-direct {p0}, LX/7GX;->a()V

    .line 1189510
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1189511
    iget-object v0, p0, LX/7GX;->syncToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1189512
    iget-object v0, p0, LX/7GX;->syncToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1189513
    sget-object v0, LX/7GX;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1189514
    iget-object v0, p0, LX/7GX;->syncToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1189515
    :cond_0
    iget-object v0, p0, LX/7GX;->lastSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1189516
    sget-object v0, LX/7GX;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1189517
    iget-object v0, p0, LX/7GX;->lastSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1189518
    :cond_1
    iget-object v0, p0, LX/7GX;->maxDeltasAbleToProcess:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1189519
    iget-object v0, p0, LX/7GX;->maxDeltasAbleToProcess:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1189520
    sget-object v0, LX/7GX;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1189521
    iget-object v0, p0, LX/7GX;->maxDeltasAbleToProcess:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1189522
    :cond_2
    iget-object v0, p0, LX/7GX;->deltaBatchSize:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1189523
    iget-object v0, p0, LX/7GX;->deltaBatchSize:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1189524
    sget-object v0, LX/7GX;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1189525
    iget-object v0, p0, LX/7GX;->deltaBatchSize:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1189526
    :cond_3
    iget-object v0, p0, LX/7GX;->encoding:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1189527
    iget-object v0, p0, LX/7GX;->encoding:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1189528
    sget-object v0, LX/7GX;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1189529
    iget-object v0, p0, LX/7GX;->encoding:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1189530
    :cond_4
    iget-object v0, p0, LX/7GX;->queueType:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1189531
    iget-object v0, p0, LX/7GX;->queueType:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1189532
    sget-object v0, LX/7GX;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1189533
    iget-object v0, p0, LX/7GX;->queueType:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1189534
    :cond_5
    iget-object v0, p0, LX/7GX;->syncApiVersion:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 1189535
    iget-object v0, p0, LX/7GX;->syncApiVersion:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 1189536
    sget-object v0, LX/7GX;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1189537
    iget-object v0, p0, LX/7GX;->syncApiVersion:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1189538
    :cond_6
    iget-object v0, p0, LX/7GX;->deviceId:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1189539
    iget-object v0, p0, LX/7GX;->deviceId:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1189540
    sget-object v0, LX/7GX;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1189541
    iget-object v0, p0, LX/7GX;->deviceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1189542
    :cond_7
    iget-object v0, p0, LX/7GX;->deviceParams:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1189543
    iget-object v0, p0, LX/7GX;->deviceParams:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1189544
    sget-object v0, LX/7GX;->k:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1189545
    iget-object v0, p0, LX/7GX;->deviceParams:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1189546
    :cond_8
    iget-object v0, p0, LX/7GX;->queueParams:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1189547
    iget-object v0, p0, LX/7GX;->queueParams:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1189548
    sget-object v0, LX/7GX;->l:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1189549
    iget-object v0, p0, LX/7GX;->queueParams:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1189550
    :cond_9
    iget-object v0, p0, LX/7GX;->entityFbid:Ljava/lang/Long;

    if-eqz v0, :cond_a

    .line 1189551
    iget-object v0, p0, LX/7GX;->entityFbid:Ljava/lang/Long;

    if-eqz v0, :cond_a

    .line 1189552
    sget-object v0, LX/7GX;->m:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1189553
    iget-object v0, p0, LX/7GX;->entityFbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1189554
    :cond_a
    iget-object v0, p0, LX/7GX;->syncTokenLong:Ljava/lang/Long;

    if-eqz v0, :cond_b

    .line 1189555
    iget-object v0, p0, LX/7GX;->syncTokenLong:Ljava/lang/Long;

    if-eqz v0, :cond_b

    .line 1189556
    sget-object v0, LX/7GX;->n:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1189557
    iget-object v0, p0, LX/7GX;->syncTokenLong:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1189558
    :cond_b
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1189559
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1189560
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1189279
    if-nez p1, :cond_1

    .line 1189280
    :cond_0
    :goto_0
    return v0

    .line 1189281
    :cond_1
    instance-of v1, p1, LX/7GX;

    if-eqz v1, :cond_0

    .line 1189282
    check-cast p1, LX/7GX;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1189283
    if-nez p1, :cond_3

    .line 1189284
    :cond_2
    :goto_1
    move v0, v2

    .line 1189285
    goto :goto_0

    .line 1189286
    :cond_3
    iget-object v0, p0, LX/7GX;->syncToken:Ljava/lang/String;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1189287
    :goto_2
    iget-object v3, p1, LX/7GX;->syncToken:Ljava/lang/String;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1189288
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1189289
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1189290
    iget-object v0, p0, LX/7GX;->syncToken:Ljava/lang/String;

    iget-object v3, p1, LX/7GX;->syncToken:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1189291
    :cond_5
    iget-object v0, p0, LX/7GX;->lastSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1189292
    :goto_4
    iget-object v3, p1, LX/7GX;->lastSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1189293
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1189294
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1189295
    iget-object v0, p0, LX/7GX;->lastSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/7GX;->lastSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1189296
    :cond_7
    iget-object v0, p0, LX/7GX;->maxDeltasAbleToProcess:Ljava/lang/Integer;

    if-eqz v0, :cond_20

    move v0, v1

    .line 1189297
    :goto_6
    iget-object v3, p1, LX/7GX;->maxDeltasAbleToProcess:Ljava/lang/Integer;

    if-eqz v3, :cond_21

    move v3, v1

    .line 1189298
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1189299
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1189300
    iget-object v0, p0, LX/7GX;->maxDeltasAbleToProcess:Ljava/lang/Integer;

    iget-object v3, p1, LX/7GX;->maxDeltasAbleToProcess:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1189301
    :cond_9
    iget-object v0, p0, LX/7GX;->deltaBatchSize:Ljava/lang/Integer;

    if-eqz v0, :cond_22

    move v0, v1

    .line 1189302
    :goto_8
    iget-object v3, p1, LX/7GX;->deltaBatchSize:Ljava/lang/Integer;

    if-eqz v3, :cond_23

    move v3, v1

    .line 1189303
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1189304
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1189305
    iget-object v0, p0, LX/7GX;->deltaBatchSize:Ljava/lang/Integer;

    iget-object v3, p1, LX/7GX;->deltaBatchSize:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1189306
    :cond_b
    iget-object v0, p0, LX/7GX;->encoding:Ljava/lang/String;

    if-eqz v0, :cond_24

    move v0, v1

    .line 1189307
    :goto_a
    iget-object v3, p1, LX/7GX;->encoding:Ljava/lang/String;

    if-eqz v3, :cond_25

    move v3, v1

    .line 1189308
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1189309
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1189310
    iget-object v0, p0, LX/7GX;->encoding:Ljava/lang/String;

    iget-object v3, p1, LX/7GX;->encoding:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1189311
    :cond_d
    iget-object v0, p0, LX/7GX;->queueType:Ljava/lang/String;

    if-eqz v0, :cond_26

    move v0, v1

    .line 1189312
    :goto_c
    iget-object v3, p1, LX/7GX;->queueType:Ljava/lang/String;

    if-eqz v3, :cond_27

    move v3, v1

    .line 1189313
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1189314
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1189315
    iget-object v0, p0, LX/7GX;->queueType:Ljava/lang/String;

    iget-object v3, p1, LX/7GX;->queueType:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1189316
    :cond_f
    iget-object v0, p0, LX/7GX;->syncApiVersion:Ljava/lang/Integer;

    if-eqz v0, :cond_28

    move v0, v1

    .line 1189317
    :goto_e
    iget-object v3, p1, LX/7GX;->syncApiVersion:Ljava/lang/Integer;

    if-eqz v3, :cond_29

    move v3, v1

    .line 1189318
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1189319
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1189320
    iget-object v0, p0, LX/7GX;->syncApiVersion:Ljava/lang/Integer;

    iget-object v3, p1, LX/7GX;->syncApiVersion:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1189321
    :cond_11
    iget-object v0, p0, LX/7GX;->deviceId:Ljava/lang/String;

    if-eqz v0, :cond_2a

    move v0, v1

    .line 1189322
    :goto_10
    iget-object v3, p1, LX/7GX;->deviceId:Ljava/lang/String;

    if-eqz v3, :cond_2b

    move v3, v1

    .line 1189323
    :goto_11
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 1189324
    :cond_12
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1189325
    iget-object v0, p0, LX/7GX;->deviceId:Ljava/lang/String;

    iget-object v3, p1, LX/7GX;->deviceId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1189326
    :cond_13
    iget-object v0, p0, LX/7GX;->deviceParams:Ljava/lang/String;

    if-eqz v0, :cond_2c

    move v0, v1

    .line 1189327
    :goto_12
    iget-object v3, p1, LX/7GX;->deviceParams:Ljava/lang/String;

    if-eqz v3, :cond_2d

    move v3, v1

    .line 1189328
    :goto_13
    if-nez v0, :cond_14

    if-eqz v3, :cond_15

    .line 1189329
    :cond_14
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1189330
    iget-object v0, p0, LX/7GX;->deviceParams:Ljava/lang/String;

    iget-object v3, p1, LX/7GX;->deviceParams:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1189331
    :cond_15
    iget-object v0, p0, LX/7GX;->queueParams:Ljava/lang/String;

    if-eqz v0, :cond_2e

    move v0, v1

    .line 1189332
    :goto_14
    iget-object v3, p1, LX/7GX;->queueParams:Ljava/lang/String;

    if-eqz v3, :cond_2f

    move v3, v1

    .line 1189333
    :goto_15
    if-nez v0, :cond_16

    if-eqz v3, :cond_17

    .line 1189334
    :cond_16
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1189335
    iget-object v0, p0, LX/7GX;->queueParams:Ljava/lang/String;

    iget-object v3, p1, LX/7GX;->queueParams:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1189336
    :cond_17
    iget-object v0, p0, LX/7GX;->entityFbid:Ljava/lang/Long;

    if-eqz v0, :cond_30

    move v0, v1

    .line 1189337
    :goto_16
    iget-object v3, p1, LX/7GX;->entityFbid:Ljava/lang/Long;

    if-eqz v3, :cond_31

    move v3, v1

    .line 1189338
    :goto_17
    if-nez v0, :cond_18

    if-eqz v3, :cond_19

    .line 1189339
    :cond_18
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1189340
    iget-object v0, p0, LX/7GX;->entityFbid:Ljava/lang/Long;

    iget-object v3, p1, LX/7GX;->entityFbid:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1189341
    :cond_19
    iget-object v0, p0, LX/7GX;->syncTokenLong:Ljava/lang/Long;

    if-eqz v0, :cond_32

    move v0, v1

    .line 1189342
    :goto_18
    iget-object v3, p1, LX/7GX;->syncTokenLong:Ljava/lang/Long;

    if-eqz v3, :cond_33

    move v3, v1

    .line 1189343
    :goto_19
    if-nez v0, :cond_1a

    if-eqz v3, :cond_1b

    .line 1189344
    :cond_1a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1189345
    iget-object v0, p0, LX/7GX;->syncTokenLong:Ljava/lang/Long;

    iget-object v3, p1, LX/7GX;->syncTokenLong:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1b
    move v2, v1

    .line 1189346
    goto/16 :goto_1

    :cond_1c
    move v0, v2

    .line 1189347
    goto/16 :goto_2

    :cond_1d
    move v3, v2

    .line 1189348
    goto/16 :goto_3

    :cond_1e
    move v0, v2

    .line 1189349
    goto/16 :goto_4

    :cond_1f
    move v3, v2

    .line 1189350
    goto/16 :goto_5

    :cond_20
    move v0, v2

    .line 1189351
    goto/16 :goto_6

    :cond_21
    move v3, v2

    .line 1189352
    goto/16 :goto_7

    :cond_22
    move v0, v2

    .line 1189353
    goto/16 :goto_8

    :cond_23
    move v3, v2

    .line 1189354
    goto/16 :goto_9

    :cond_24
    move v0, v2

    .line 1189355
    goto/16 :goto_a

    :cond_25
    move v3, v2

    .line 1189356
    goto/16 :goto_b

    :cond_26
    move v0, v2

    .line 1189357
    goto/16 :goto_c

    :cond_27
    move v3, v2

    .line 1189358
    goto/16 :goto_d

    :cond_28
    move v0, v2

    .line 1189359
    goto/16 :goto_e

    :cond_29
    move v3, v2

    .line 1189360
    goto/16 :goto_f

    :cond_2a
    move v0, v2

    .line 1189361
    goto/16 :goto_10

    :cond_2b
    move v3, v2

    .line 1189362
    goto/16 :goto_11

    :cond_2c
    move v0, v2

    .line 1189363
    goto/16 :goto_12

    :cond_2d
    move v3, v2

    .line 1189364
    goto/16 :goto_13

    :cond_2e
    move v0, v2

    .line 1189365
    goto/16 :goto_14

    :cond_2f
    move v3, v2

    .line 1189366
    goto/16 :goto_15

    :cond_30
    move v0, v2

    .line 1189367
    goto :goto_16

    :cond_31
    move v3, v2

    .line 1189368
    goto :goto_17

    :cond_32
    move v0, v2

    .line 1189369
    goto :goto_18

    :cond_33
    move v3, v2

    .line 1189370
    goto :goto_19
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1189278
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1189275
    sget-boolean v0, LX/7GX;->a:Z

    .line 1189276
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/7GX;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1189277
    return-object v0
.end method
