.class public final LX/8JV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/tagging/shared/TagTypeahead;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V
    .locals 0

    .prologue
    .line 1328520
    iput-object p1, p0, LX/8JV;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;B)V
    .locals 0

    .prologue
    .line 1328534
    invoke-direct {p0, p1}, LX/8JV;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1328523
    iget-object v0, p0, LX/8JV;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->b:LX/73w;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    .line 1328524
    iput v1, v0, LX/73w;->C:I

    .line 1328525
    iget-object v0, p0, LX/8JV;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-boolean v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->u:Z

    if-nez v0, :cond_0

    .line 1328526
    iget-object v0, p0, LX/8JV;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->r:LX/8JT;

    invoke-interface {v0}, LX/8JT;->b()V

    .line 1328527
    iget-object v0, p0, LX/8JV;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    const/4 v1, 0x1

    .line 1328528
    iput-boolean v1, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->u:Z

    .line 1328529
    :cond_0
    iget-object v0, p0, LX/8JV;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1328530
    iget-object v0, p0, LX/8JV;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    .line 1328531
    invoke-static {v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->k$redex0(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    .line 1328532
    :cond_1
    iget-object v0, p0, LX/8JV;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(Landroid/text/Editable;)V

    .line 1328533
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1328522
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1328521
    return-void
.end method
