.class public final enum LX/6yO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6yO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6yO;

.field public static final enum CONFIRM_CSC:LX/6yO;

.field public static final enum MESSENGER_PAY_ADD:LX/6yO;

.field public static final enum MESSENGER_PAY_EDIT:LX/6yO;

.field public static final enum SIMPLE:LX/6yO;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1159937
    new-instance v0, LX/6yO;

    const-string v1, "MESSENGER_PAY_ADD"

    invoke-direct {v0, v1, v2}, LX/6yO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6yO;->MESSENGER_PAY_ADD:LX/6yO;

    .line 1159938
    new-instance v0, LX/6yO;

    const-string v1, "MESSENGER_PAY_EDIT"

    invoke-direct {v0, v1, v3}, LX/6yO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6yO;->MESSENGER_PAY_EDIT:LX/6yO;

    .line 1159939
    new-instance v0, LX/6yO;

    const-string v1, "CONFIRM_CSC"

    invoke-direct {v0, v1, v4}, LX/6yO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6yO;->CONFIRM_CSC:LX/6yO;

    .line 1159940
    new-instance v0, LX/6yO;

    const-string v1, "SIMPLE"

    invoke-direct {v0, v1, v5}, LX/6yO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6yO;->SIMPLE:LX/6yO;

    .line 1159941
    const/4 v0, 0x4

    new-array v0, v0, [LX/6yO;

    sget-object v1, LX/6yO;->MESSENGER_PAY_ADD:LX/6yO;

    aput-object v1, v0, v2

    sget-object v1, LX/6yO;->MESSENGER_PAY_EDIT:LX/6yO;

    aput-object v1, v0, v3

    sget-object v1, LX/6yO;->CONFIRM_CSC:LX/6yO;

    aput-object v1, v0, v4

    sget-object v1, LX/6yO;->SIMPLE:LX/6yO;

    aput-object v1, v0, v5

    sput-object v0, LX/6yO;->$VALUES:[LX/6yO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1159934
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6yO;
    .locals 1

    .prologue
    .line 1159936
    const-class v0, LX/6yO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6yO;

    return-object v0
.end method

.method public static values()[LX/6yO;
    .locals 1

    .prologue
    .line 1159935
    sget-object v0, LX/6yO;->$VALUES:[LX/6yO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6yO;

    return-object v0
.end method
