.class public LX/8Sg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/8TY;


# direct methods
.method public constructor <init>(LX/8TY;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1347251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1347252
    iput-object p1, p0, LX/8Sg;->a:LX/8TY;

    .line 1347253
    return-void
.end method

.method public static a(LX/0QB;)LX/8Sg;
    .locals 4

    .prologue
    .line 1347254
    const-class v1, LX/8Sg;

    monitor-enter v1

    .line 1347255
    :try_start_0
    sget-object v0, LX/8Sg;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1347256
    sput-object v2, LX/8Sg;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1347257
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1347258
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1347259
    new-instance p0, LX/8Sg;

    invoke-static {v0}, LX/8TY;->a(LX/0QB;)LX/8TY;

    move-result-object v3

    check-cast v3, LX/8TY;

    invoke-direct {p0, v3}, LX/8Sg;-><init>(LX/8TY;)V

    .line 1347260
    move-object v0, p0

    .line 1347261
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1347262
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8Sg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1347263
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1347264
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
