.class public LX/8Hu;
.super LX/3hi;
.source ""


# instance fields
.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private d:Lcom/facebook/photos/imageprocessing/FiltersEngine;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Px;Lcom/facebook/photos/imageprocessing/FiltersEngine;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Landroid/graphics/RectF;",
            ">;",
            "Lcom/facebook/photos/imageprocessing/FiltersEngine;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1321735
    invoke-direct {p0}, LX/3hi;-><init>()V

    .line 1321736
    iput-object p2, p0, LX/8Hu;->b:LX/0Px;

    .line 1321737
    iput-object p1, p0, LX/8Hu;->c:Ljava/lang/String;

    .line 1321738
    iput-object p3, p0, LX/8Hu;->d:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    .line 1321739
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 1321740
    iget-object v0, p0, LX/8Hu;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v0}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/8Hu;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8Hu;->b:LX/0Px;

    if-nez v0, :cond_1

    .line 1321741
    :cond_0
    :goto_0
    return-void

    .line 1321742
    :cond_1
    const/4 v1, 0x0

    .line 1321743
    :try_start_0
    iget-object v0, p0, LX/8Hu;->d:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a(Landroid/graphics/Bitmap;)LX/8Ht;

    move-result-object v1

    .line 1321744
    iget-object v0, p0, LX/8Hu;->b:LX/0Px;

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, LX/0Px;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/graphics/RectF;

    invoke-virtual {v1, v0}, LX/8Ht;->a([Landroid/graphics/RectF;)V

    .line 1321745
    iget-object v0, p0, LX/8Hu;->c:Ljava/lang/String;

    invoke-virtual {v1, p1, v0}, LX/8Ht;->a(Landroid/graphics/Bitmap;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1321746
    if-eqz v1, :cond_0

    .line 1321747
    :try_start_1
    invoke-virtual {v1}, LX/8Ht;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1321748
    :catch_0
    goto :goto_0

    .line 1321749
    :catchall_0
    move-exception v0

    .line 1321750
    if-eqz v1, :cond_2

    .line 1321751
    :try_start_2
    invoke-virtual {v1}, LX/8Ht;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1321752
    :cond_2
    :goto_1
    throw v0

    :catch_1
    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1321753
    const-class v0, LX/8Hu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
