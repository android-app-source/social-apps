.class public final LX/7u3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1269776
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_c

    .line 1269777
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1269778
    :goto_0
    return v1

    .line 1269779
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1269780
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_b

    .line 1269781
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1269782
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1269783
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1269784
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1269785
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1269786
    :cond_2
    const-string v12, "legacy_api_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1269787
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1269788
    :cond_3
    const-string v12, "present_participle"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1269789
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1269790
    :cond_4
    const-string v12, "previewTemplateAtPlace"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1269791
    invoke-static {p0, p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1269792
    :cond_5
    const-string v12, "previewTemplateNoTags"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1269793
    invoke-static {p0, p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1269794
    :cond_6
    const-string v12, "previewTemplateWithPeople"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1269795
    invoke-static {p0, p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1269796
    :cond_7
    const-string v12, "previewTemplateWithPeopleAtPlace"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1269797
    invoke-static {p0, p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1269798
    :cond_8
    const-string v12, "previewTemplateWithPerson"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1269799
    invoke-static {p0, p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1269800
    :cond_9
    const-string v12, "previewTemplateWithPersonAtPlace"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1269801
    invoke-static {p0, p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1269802
    :cond_a
    const-string v12, "prompt"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1269803
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 1269804
    :cond_b
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1269805
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 1269806
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1269807
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1269808
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1269809
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1269810
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1269811
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1269812
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1269813
    const/16 v1, 0x8

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1269814
    const/16 v1, 0x9

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1269815
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_c
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1269816
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1269817
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1269818
    if-eqz v0, :cond_0

    .line 1269819
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1269820
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1269821
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1269822
    if-eqz v0, :cond_1

    .line 1269823
    const-string v1, "legacy_api_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1269824
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1269825
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1269826
    if-eqz v0, :cond_2

    .line 1269827
    const-string v1, "present_participle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1269828
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1269829
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1269830
    if-eqz v0, :cond_3

    .line 1269831
    const-string v1, "previewTemplateAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1269832
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1269833
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1269834
    if-eqz v0, :cond_4

    .line 1269835
    const-string v1, "previewTemplateNoTags"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1269836
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1269837
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1269838
    if-eqz v0, :cond_5

    .line 1269839
    const-string v1, "previewTemplateWithPeople"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1269840
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1269841
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1269842
    if-eqz v0, :cond_6

    .line 1269843
    const-string v1, "previewTemplateWithPeopleAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1269844
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1269845
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1269846
    if-eqz v0, :cond_7

    .line 1269847
    const-string v1, "previewTemplateWithPerson"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1269848
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1269849
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1269850
    if-eqz v0, :cond_8

    .line 1269851
    const-string v1, "previewTemplateWithPersonAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1269852
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1269853
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1269854
    if-eqz v0, :cond_9

    .line 1269855
    const-string v1, "prompt"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1269856
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1269857
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1269858
    return-void
.end method
