.class public LX/7Wm;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public final a:LX/0yQ;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0yQ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1216777
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1216778
    iput-object p2, p0, LX/7Wm;->a:LX/0yQ;

    .line 1216779
    new-instance v0, LX/7Wl;

    invoke-direct {v0, p0}, LX/7Wl;-><init>(LX/7Wm;)V

    invoke-virtual {p0, v0}, LX/7Wm;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1216780
    const v0, 0x7f080ea3

    invoke-virtual {p0, v0}, LX/7Wm;->setTitle(I)V

    .line 1216781
    return-void
.end method

.method public static b(LX/0QB;)LX/7Wm;
    .locals 3

    .prologue
    .line 1216782
    new-instance v2, LX/7Wm;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0yQ;->a(LX/0QB;)LX/0yQ;

    move-result-object v1

    check-cast v1, LX/0yQ;

    invoke-direct {v2, v0, v1}, LX/7Wm;-><init>(Landroid/content/Context;LX/0yQ;)V

    .line 1216783
    return-object v2
.end method
