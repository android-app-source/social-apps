.class public final LX/8J0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1327158
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1327159
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1327160
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1327161
    invoke-static {p0, p1}, LX/8J0;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1327162
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1327163
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1327164
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1327165
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1327166
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/8J0;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1327167
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1327168
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1327169
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 38

    .prologue
    .line 1327170
    const/16 v34, 0x0

    .line 1327171
    const/16 v33, 0x0

    .line 1327172
    const/16 v32, 0x0

    .line 1327173
    const/16 v31, 0x0

    .line 1327174
    const/16 v30, 0x0

    .line 1327175
    const/16 v29, 0x0

    .line 1327176
    const/16 v28, 0x0

    .line 1327177
    const/16 v27, 0x0

    .line 1327178
    const/16 v26, 0x0

    .line 1327179
    const/16 v25, 0x0

    .line 1327180
    const/16 v24, 0x0

    .line 1327181
    const/16 v23, 0x0

    .line 1327182
    const/16 v22, 0x0

    .line 1327183
    const/16 v21, 0x0

    .line 1327184
    const/16 v20, 0x0

    .line 1327185
    const/16 v19, 0x0

    .line 1327186
    const/16 v18, 0x0

    .line 1327187
    const/16 v17, 0x0

    .line 1327188
    const/16 v16, 0x0

    .line 1327189
    const/4 v15, 0x0

    .line 1327190
    const/4 v14, 0x0

    .line 1327191
    const/4 v13, 0x0

    .line 1327192
    const/4 v12, 0x0

    .line 1327193
    const/4 v11, 0x0

    .line 1327194
    const/4 v10, 0x0

    .line 1327195
    const/4 v9, 0x0

    .line 1327196
    const/4 v8, 0x0

    .line 1327197
    const/4 v7, 0x0

    .line 1327198
    const/4 v6, 0x0

    .line 1327199
    const/4 v5, 0x0

    .line 1327200
    const/4 v4, 0x0

    .line 1327201
    const/4 v3, 0x0

    .line 1327202
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v35

    sget-object v36, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_1

    .line 1327203
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1327204
    const/4 v3, 0x0

    .line 1327205
    :goto_0
    return v3

    .line 1327206
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1327207
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v35

    sget-object v36, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_1c

    .line 1327208
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v35

    .line 1327209
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1327210
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v36

    sget-object v37, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    if-eq v0, v1, :cond_1

    if-eqz v35, :cond_1

    .line 1327211
    const-string v36, "__type__"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-nez v36, :cond_2

    const-string v36, "__typename"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_3

    .line 1327212
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v34

    goto :goto_1

    .line 1327213
    :cond_3
    const-string v36, "accessibility_caption"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_4

    .line 1327214
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    goto :goto_1

    .line 1327215
    :cond_4
    const-string v36, "creation_story"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_5

    .line 1327216
    invoke-static/range {p0 .. p1}, LX/8Iy;->a(LX/15w;LX/186;)I

    move-result v32

    goto :goto_1

    .line 1327217
    :cond_5
    const-string v36, "feedback"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_6

    .line 1327218
    invoke-static/range {p0 .. p1}, LX/4aV;->a(LX/15w;LX/186;)I

    move-result v31

    goto :goto_1

    .line 1327219
    :cond_6
    const-string v36, "focus"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_7

    .line 1327220
    invoke-static/range {p0 .. p1}, LX/4aC;->a(LX/15w;LX/186;)I

    move-result v30

    goto :goto_1

    .line 1327221
    :cond_7
    const-string v36, "height"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_8

    .line 1327222
    const/4 v8, 0x1

    .line 1327223
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v29

    goto/16 :goto_1

    .line 1327224
    :cond_8
    const-string v36, "id"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_9

    .line 1327225
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto/16 :goto_1

    .line 1327226
    :cond_9
    const-string v36, "image"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_a

    .line 1327227
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v27

    goto/16 :goto_1

    .line 1327228
    :cond_a
    const-string v36, "imageHigh"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_b

    .line 1327229
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 1327230
    :cond_b
    const-string v36, "imageLow"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_c

    .line 1327231
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 1327232
    :cond_c
    const-string v36, "imageMedium"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_d

    .line 1327233
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 1327234
    :cond_d
    const-string v36, "imageThumbnail"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_e

    .line 1327235
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 1327236
    :cond_e
    const-string v36, "is_looping"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_f

    .line 1327237
    const/4 v7, 0x1

    .line 1327238
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto/16 :goto_1

    .line 1327239
    :cond_f
    const-string v36, "is_playable"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_10

    .line 1327240
    const/4 v6, 0x1

    .line 1327241
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto/16 :goto_1

    .line 1327242
    :cond_10
    const-string v36, "landscape"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_11

    .line 1327243
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 1327244
    :cond_11
    const-string v36, "largePortraitImage"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_12

    .line 1327245
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1327246
    :cond_12
    const-string v36, "largeThumbnail"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_13

    .line 1327247
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 1327248
    :cond_13
    const-string v36, "loop_count"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_14

    .line 1327249
    const/4 v5, 0x1

    .line 1327250
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v17

    goto/16 :goto_1

    .line 1327251
    :cond_14
    const-string v36, "narrowLandscapeImage"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_15

    .line 1327252
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 1327253
    :cond_15
    const-string v36, "narrowPortraitImage"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_16

    .line 1327254
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1327255
    :cond_16
    const-string v36, "photo_encodings"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_17

    .line 1327256
    invoke-static/range {p0 .. p1}, LX/8Iz;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 1327257
    :cond_17
    const-string v36, "playable_duration_in_ms"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_18

    .line 1327258
    const/4 v4, 0x1

    .line 1327259
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto/16 :goto_1

    .line 1327260
    :cond_18
    const-string v36, "playable_url"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_19

    .line 1327261
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 1327262
    :cond_19
    const-string v36, "portrait"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_1a

    .line 1327263
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1327264
    :cond_1a
    const-string v36, "squareLargeImage"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_1b

    .line 1327265
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1327266
    :cond_1b
    const-string v36, "width"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_0

    .line 1327267
    const/4 v3, 0x1

    .line 1327268
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto/16 :goto_1

    .line 1327269
    :cond_1c
    const/16 v35, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1327270
    const/16 v35, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v35

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1327271
    const/16 v34, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v34

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1327272
    const/16 v33, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v33

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1327273
    const/16 v32, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v32

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1327274
    const/16 v31, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1327275
    if-eqz v8, :cond_1d

    .line 1327276
    const/4 v8, 0x5

    const/16 v30, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v8, v1, v2}, LX/186;->a(III)V

    .line 1327277
    :cond_1d
    const/4 v8, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1327278
    const/4 v8, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1327279
    const/16 v8, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1327280
    const/16 v8, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1327281
    const/16 v8, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1327282
    const/16 v8, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1327283
    if-eqz v7, :cond_1e

    .line 1327284
    const/16 v7, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1327285
    :cond_1e
    if-eqz v6, :cond_1f

    .line 1327286
    const/16 v6, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1327287
    :cond_1f
    const/16 v6, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1327288
    const/16 v6, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1327289
    const/16 v6, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1327290
    if-eqz v5, :cond_20

    .line 1327291
    const/16 v5, 0x11

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1, v6}, LX/186;->a(III)V

    .line 1327292
    :cond_20
    const/16 v5, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1327293
    const/16 v5, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->b(II)V

    .line 1327294
    const/16 v5, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->b(II)V

    .line 1327295
    if-eqz v4, :cond_21

    .line 1327296
    const/16 v4, 0x15

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13, v5}, LX/186;->a(III)V

    .line 1327297
    :cond_21
    const/16 v4, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 1327298
    const/16 v4, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 1327299
    const/16 v4, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1327300
    if-eqz v3, :cond_22

    .line 1327301
    const/16 v3, 0x19

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v4}, LX/186;->a(III)V

    .line 1327302
    :cond_22
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1327303
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1327304
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1327305
    if-eqz v0, :cond_0

    .line 1327306
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327307
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1327308
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1327309
    if-eqz v0, :cond_1

    .line 1327310
    const-string v1, "accessibility_caption"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327311
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1327312
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327313
    if-eqz v0, :cond_2

    .line 1327314
    const-string v1, "creation_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327315
    invoke-static {p0, v0, p2, p3}, LX/8Iy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1327316
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327317
    if-eqz v0, :cond_3

    .line 1327318
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327319
    invoke-static {p0, v0, p2, p3}, LX/4aV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1327320
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327321
    if-eqz v0, :cond_4

    .line 1327322
    const-string v1, "focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327323
    invoke-static {p0, v0, p2}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 1327324
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1327325
    if-eqz v0, :cond_5

    .line 1327326
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327327
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1327328
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1327329
    if-eqz v0, :cond_6

    .line 1327330
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327331
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1327332
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327333
    if-eqz v0, :cond_7

    .line 1327334
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327335
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1327336
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327337
    if-eqz v0, :cond_8

    .line 1327338
    const-string v1, "imageHigh"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327339
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1327340
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327341
    if-eqz v0, :cond_9

    .line 1327342
    const-string v1, "imageLow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327343
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1327344
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327345
    if-eqz v0, :cond_a

    .line 1327346
    const-string v1, "imageMedium"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327347
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1327348
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327349
    if-eqz v0, :cond_b

    .line 1327350
    const-string v1, "imageThumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327351
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1327352
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1327353
    if-eqz v0, :cond_c

    .line 1327354
    const-string v1, "is_looping"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327355
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1327356
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1327357
    if-eqz v0, :cond_d

    .line 1327358
    const-string v1, "is_playable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327359
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1327360
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327361
    if-eqz v0, :cond_e

    .line 1327362
    const-string v1, "landscape"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327363
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1327364
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327365
    if-eqz v0, :cond_f

    .line 1327366
    const-string v1, "largePortraitImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327367
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1327368
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327369
    if-eqz v0, :cond_10

    .line 1327370
    const-string v1, "largeThumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327371
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1327372
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1327373
    if-eqz v0, :cond_11

    .line 1327374
    const-string v1, "loop_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327375
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1327376
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327377
    if-eqz v0, :cond_12

    .line 1327378
    const-string v1, "narrowLandscapeImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327379
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1327380
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327381
    if-eqz v0, :cond_13

    .line 1327382
    const-string v1, "narrowPortraitImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327383
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1327384
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327385
    if-eqz v0, :cond_14

    .line 1327386
    const-string v1, "photo_encodings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327387
    invoke-static {p0, v0, p2, p3}, LX/8Iz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1327388
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1327389
    if-eqz v0, :cond_15

    .line 1327390
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327391
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1327392
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1327393
    if-eqz v0, :cond_16

    .line 1327394
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327395
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1327396
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327397
    if-eqz v0, :cond_17

    .line 1327398
    const-string v1, "portrait"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327399
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1327400
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327401
    if-eqz v0, :cond_18

    .line 1327402
    const-string v1, "squareLargeImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327403
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1327404
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1327405
    if-eqz v0, :cond_19

    .line 1327406
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327407
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1327408
    :cond_19
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1327409
    return-void
.end method
