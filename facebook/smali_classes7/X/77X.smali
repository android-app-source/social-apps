.class public final enum LX/77X;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/77X;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/77X;

.field public static final enum MESSAGE_RECEIVED:LX/77X;

.field public static final enum MESSAGE_SENT:LX/77X;

.field public static final enum THREAD_ACTIVITY:LX/77X;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1171889
    new-instance v0, LX/77X;

    const-string v1, "MESSAGE_SENT"

    invoke-direct {v0, v1, v2}, LX/77X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77X;->MESSAGE_SENT:LX/77X;

    .line 1171890
    new-instance v0, LX/77X;

    const-string v1, "MESSAGE_RECEIVED"

    invoke-direct {v0, v1, v3}, LX/77X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77X;->MESSAGE_RECEIVED:LX/77X;

    .line 1171891
    new-instance v0, LX/77X;

    const-string v1, "THREAD_ACTIVITY"

    invoke-direct {v0, v1, v4}, LX/77X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77X;->THREAD_ACTIVITY:LX/77X;

    .line 1171892
    const/4 v0, 0x3

    new-array v0, v0, [LX/77X;

    sget-object v1, LX/77X;->MESSAGE_SENT:LX/77X;

    aput-object v1, v0, v2

    sget-object v1, LX/77X;->MESSAGE_RECEIVED:LX/77X;

    aput-object v1, v0, v3

    sget-object v1, LX/77X;->THREAD_ACTIVITY:LX/77X;

    aput-object v1, v0, v4

    sput-object v0, LX/77X;->$VALUES:[LX/77X;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1171893
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/77X;
    .locals 1

    .prologue
    .line 1171894
    const-class v0, LX/77X;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/77X;

    return-object v0
.end method

.method public static values()[LX/77X;
    .locals 1

    .prologue
    .line 1171895
    sget-object v0, LX/77X;->$VALUES:[LX/77X;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/77X;

    return-object v0
.end method


# virtual methods
.method public final toEventName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1171896
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "QuickPromotionUserEvent:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/77X;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
