.class public final LX/8aj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1368510
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1368511
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1368512
    :goto_0
    return v1

    .line 1368513
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1368514
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 1368515
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1368516
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1368517
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1368518
    const-string v7, "annotation_rich_text"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1368519
    invoke-static {p0, p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1368520
    :cond_2
    const-string v7, "annotation_text"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1368521
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1368522
    :cond_3
    const-string v7, "annotation_text_alignment"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1368523
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLTextAnnotationHorizontalPosition;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTextAnnotationHorizontalPosition;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_1

    .line 1368524
    :cond_4
    const-string v7, "annotation_text_size"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1368525
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLTextAnnotationPresentationStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTextAnnotationPresentationStyle;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto :goto_1

    .line 1368526
    :cond_5
    const-string v7, "annotation_text_slot"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1368527
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_1

    .line 1368528
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1368529
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1368530
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1368531
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1368532
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1368533
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1368534
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    .line 1368535
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1368536
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368537
    if-eqz v0, :cond_0

    .line 1368538
    const-string v1, "annotation_rich_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368539
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368540
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1368541
    if-eqz v0, :cond_1

    .line 1368542
    const-string v1, "annotation_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368543
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1368544
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1368545
    if-eqz v0, :cond_2

    .line 1368546
    const-string v0, "annotation_text_alignment"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368547
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1368548
    :cond_2
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1368549
    if-eqz v0, :cond_3

    .line 1368550
    const-string v0, "annotation_text_size"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368551
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1368552
    :cond_3
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1368553
    if-eqz v0, :cond_4

    .line 1368554
    const-string v0, "annotation_text_slot"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368555
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1368556
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1368557
    return-void
.end method
