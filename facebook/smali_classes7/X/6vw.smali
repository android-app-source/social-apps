.class public LX/6vw;
.super LX/6vv;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6vv",
        "<",
        "Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;",
        "Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;",
        "Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;",
        "Lcom/facebook/payments/contactinfo/picker/ContactInfoCoreClientData;",
        "LX/6va;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1157336
    invoke-direct {p0}, LX/6vv;-><init>()V

    .line 1157337
    return-void
.end method

.method public static a(LX/0QB;)LX/6vw;
    .locals 3

    .prologue
    .line 1157325
    const-class v1, LX/6vw;

    monitor-enter v1

    .line 1157326
    :try_start_0
    sget-object v0, LX/6vw;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1157327
    sput-object v2, LX/6vw;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1157328
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1157329
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1157330
    new-instance v0, LX/6vw;

    invoke-direct {v0}, LX/6vw;-><init>()V

    .line 1157331
    move-object v0, v0

    .line 1157332
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1157333
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6vw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1157334
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1157335
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 1157323
    check-cast p1, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    .line 1157324
    new-instance v0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;

    invoke-direct {v0, p1}, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;-><init>(Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 1157321
    check-cast p1, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    check-cast p2, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;

    check-cast p3, Lcom/facebook/payments/contactinfo/picker/ContactInfoCoreClientData;

    .line 1157322
    new-instance v0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;-><init>(Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;Lcom/facebook/payments/contactinfo/picker/ContactInfoCoreClientData;LX/0P1;)V

    return-object v0
.end method
