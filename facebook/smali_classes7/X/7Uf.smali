.class public LX/7Uf;
.super Landroid/view/View;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:LX/7Ue;

.field private f:[F

.field private g:[F

.field private h:F

.field public i:Landroid/os/Handler;

.field public j:Ljava/lang/Runnable;

.field private k:Landroid/graphics/Paint;

.field private l:Landroid/animation/ValueAnimator;

.field private m:Ljava/util/Random;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1213271
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1213272
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/7Uf;->h:F

    .line 1213273
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7Uf;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1213274
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1213209
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1213210
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/7Uf;->h:F

    .line 1213211
    invoke-direct {p0, p1, p2}, LX/7Uf;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1213212
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1213213
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1213214
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/7Uf;->h:F

    .line 1213215
    invoke-direct {p0, p1, p2}, LX/7Uf;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1213216
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 1213217
    sget-object v0, LX/03r;->ValueBasedSoundWaveView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1213218
    if-eqz v0, :cond_0

    .line 1213219
    const/16 v1, 0x1

    invoke-virtual {p0}, LX/7Uf;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0048

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, LX/7Uf;->d:I

    .line 1213220
    const/16 v1, 0x0

    invoke-virtual {p0}, LX/7Uf;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0651

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/7Uf;->c:I

    .line 1213221
    const/16 v1, 0x2

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, LX/7Uf;->b:I

    .line 1213222
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1213223
    :cond_0
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/7Uf;->m:Ljava/util/Random;

    .line 1213224
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/7Uf;->i:Landroid/os/Handler;

    .line 1213225
    iget v0, p0, LX/7Uf;->b:I

    new-array v0, v0, [F

    iput-object v0, p0, LX/7Uf;->f:[F

    .line 1213226
    iget v0, p0, LX/7Uf;->b:I

    new-array v0, v0, [F

    iput-object v0, p0, LX/7Uf;->g:[F

    .line 1213227
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/7Uf;->k:Landroid/graphics/Paint;

    .line 1213228
    iget-object v0, p0, LX/7Uf;->k:Landroid/graphics/Paint;

    iget v1, p0, LX/7Uf;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1213229
    iget-object v0, p0, LX/7Uf;->k:Landroid/graphics/Paint;

    iget v1, p0, LX/7Uf;->d:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213230
    iget-object v0, p0, LX/7Uf;->k:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1213231
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/7Uf;->l:Landroid/animation/ValueAnimator;

    .line 1213232
    iget-object v0, p0, LX/7Uf;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1213233
    iget-object v0, p0, LX/7Uf;->l:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1213234
    new-instance v0, Lcom/facebook/widget/soundwave/ControlledSoundWaveView$1;

    invoke-direct {v0, p0}, Lcom/facebook/widget/soundwave/ControlledSoundWaveView$1;-><init>(LX/7Uf;)V

    iput-object v0, p0, LX/7Uf;->j:Ljava/lang/Runnable;

    .line 1213235
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static a$redex0(LX/7Uf;)V
    .locals 4

    .prologue
    .line 1213236
    invoke-direct {p0}, LX/7Uf;->b()V

    .line 1213237
    iget-object v0, p0, LX/7Uf;->e:LX/7Ue;

    invoke-interface {v0}, LX/7Ue;->a()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1213238
    iget-object v1, p0, LX/7Uf;->f:[F

    const/4 v2, 0x0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v0, p0, LX/7Uf;->m:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    :cond_0
    aput v0, v1, v2

    .line 1213239
    const/4 v0, 0x0

    iput v0, p0, LX/7Uf;->h:F

    .line 1213240
    iget-object v0, p0, LX/7Uf;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1213241
    iget-object v0, p0, LX/7Uf;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1213242
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 1213243
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, LX/7Uf;->b:I

    if-ge v0, v1, :cond_1

    .line 1213244
    iget-object v1, p0, LX/7Uf;->f:[F

    aget v1, v1, v0

    iget-object v2, p0, LX/7Uf;->g:[F

    aget v2, v2, v0

    sub-float/2addr v1, v2

    iget v2, p0, LX/7Uf;->h:F

    mul-float/2addr v1, v2

    .line 1213245
    iget-object v2, p0, LX/7Uf;->g:[F

    iget-object v3, p0, LX/7Uf;->g:[F

    aget v3, v3, v0

    add-float/2addr v1, v3

    aput v1, v2, v0

    .line 1213246
    if-eqz v0, :cond_0

    .line 1213247
    iget-object v1, p0, LX/7Uf;->f:[F

    iget-object v2, p0, LX/7Uf;->g:[F

    add-int/lit8 v3, v0, -0x1

    aget v2, v2, v3

    aput v2, v1, v0

    .line 1213248
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1213249
    :cond_1
    return-void
.end method


# virtual methods
.method public getCurrentValues()[F
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1213250
    iget-object v0, p0, LX/7Uf;->f:[F

    return-object v0
.end method

.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 1213251
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, LX/7Uf;->h:F

    .line 1213252
    invoke-virtual {p0}, LX/7Uf;->invalidate()V

    .line 1213253
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 1213254
    invoke-virtual {p0}, LX/7Uf;->getPaddingLeft()I

    move-result v1

    int-to-float v7, v1

    .line 1213255
    invoke-virtual {p0}, LX/7Uf;->getPaddingTop()I

    move-result v1

    int-to-float v8, v1

    .line 1213256
    invoke-virtual {p0}, LX/7Uf;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, LX/7Uf;->getPaddingRight()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v7

    sub-float/2addr v1, v2

    .line 1213257
    invoke-virtual {p0}, LX/7Uf;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, LX/7Uf;->getPaddingBottom()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v3, v8

    sub-float v9, v2, v3

    .line 1213258
    iget v2, p0, LX/7Uf;->c:I

    iget v3, p0, LX/7Uf;->b:I

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v2, v3

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, p0, LX/7Uf;->b:I

    int-to-float v2, v2

    div-float v10, v1, v2

    .line 1213259
    const/4 v1, 0x0

    cmpg-float v1, v10, v1

    if-gez v1, :cond_1

    .line 1213260
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1213261
    :cond_0
    return-void

    .line 1213262
    :cond_1
    iget-object v1, p0, LX/7Uf;->k:Landroid/graphics/Paint;

    invoke-virtual {v1, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move v6, v0

    .line 1213263
    :goto_0
    iget v0, p0, LX/7Uf;->b:I

    if-eq v6, v0, :cond_0

    .line 1213264
    iget-object v0, p0, LX/7Uf;->f:[F

    aget v0, v0, v6

    iget-object v1, p0, LX/7Uf;->g:[F

    aget v1, v1, v6

    sub-float/2addr v0, v1

    iget v1, p0, LX/7Uf;->h:F

    mul-float/2addr v0, v1

    .line 1213265
    iget-object v1, p0, LX/7Uf;->g:[F

    aget v1, v1, v6

    add-float/2addr v0, v1

    .line 1213266
    iget v1, p0, LX/7Uf;->c:I

    int-to-float v1, v1

    add-float/2addr v1, v10

    int-to-float v2, v6

    mul-float/2addr v1, v2

    .line 1213267
    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v10, v2

    add-float/2addr v1, v2

    .line 1213268
    add-float/2addr v1, v7

    .line 1213269
    add-float v2, v8, v9

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v0, v3, v0

    mul-float/2addr v0, v9

    add-float v4, v8, v0

    iget-object v5, p0, LX/7Uf;->k:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1213270
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0
.end method
