.class public LX/74E;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/74D;


# instance fields
.field public final b:LX/11i;

.field public final c:LX/74C;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1167670
    new-instance v0, LX/74D;

    invoke-direct {v0}, LX/74D;-><init>()V

    sput-object v0, LX/74E;->a:LX/74D;

    return-void
.end method

.method public constructor <init>(LX/11i;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1167671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1167672
    new-instance v0, LX/74C;

    invoke-direct {v0}, LX/74C;-><init>()V

    iput-object v0, p0, LX/74E;->c:LX/74C;

    .line 1167673
    iput-object p1, p0, LX/74E;->b:LX/11i;

    .line 1167674
    return-void
.end method

.method public static a(LX/74E;Z)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1167675
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1167676
    const-string v1, "waterfall_id"

    iget-object v2, p0, LX/74E;->c:LX/74C;

    iget-object v2, v2, LX/74C;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167677
    const-string v1, "start_times"

    .line 1167678
    iget-object v2, p0, LX/74E;->c:LX/74C;

    iget-object v2, v2, LX/74C;->b:Ljava/util/List;

    if-nez v2, :cond_1

    const-string v2, ""

    :goto_0
    move-object v2, v2

    .line 1167679
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167680
    if-eqz p1, :cond_0

    .line 1167681
    const-string v1, "is_cancelled"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167682
    :cond_0
    return-object v0

    :cond_1
    const-string v2, ","

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/74E;->c:LX/74C;

    iget-object v5, v5, LX/74C;->b:Ljava/util/List;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static a(LX/74E;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1167685
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/74E;->a(LX/74E;Z)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/74E;->a(LX/74E;Ljava/lang/String;LX/0P1;)V

    .line 1167686
    return-void
.end method

.method public static a(LX/74E;Ljava/lang/String;LX/0P1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1167683
    invoke-static {p0}, LX/74E;->x(LX/74E;)LX/11o;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x127018ab

    invoke-static {v0, p1, v1, p2, v2}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 1167684
    return-void
.end method

.method public static b(LX/74E;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1167687
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/74E;->a(LX/74E;Z)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/74E;->b(LX/74E;Ljava/lang/String;LX/0P1;)V

    .line 1167688
    return-void
.end method

.method private static b(LX/74E;Ljava/lang/String;LX/0P1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1167666
    invoke-static {p0}, LX/74E;->x(LX/74E;)LX/11o;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x1f6aa2ab

    invoke-static {v0, p1, v1, p2, v2}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 1167667
    return-void
.end method

.method public static c(LX/74E;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1167668
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/74E;->a(LX/74E;Z)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/74E;->c(LX/74E;Ljava/lang/String;LX/0P1;)V

    .line 1167669
    return-void
.end method

.method public static c(LX/74E;Ljava/lang/String;LX/0P1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1167664
    invoke-static {p0}, LX/74E;->x(LX/74E;)LX/11o;

    move-result-object v0

    const/4 v1, 0x0

    const v2, -0xa43d991

    invoke-static {v0, p1, v1, p2, v2}, LX/096;->c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 1167665
    return-void
.end method

.method public static d(LX/74E;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1167661
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/74E;->a(LX/74E;Z)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    .line 1167662
    invoke-static {p0}, LX/74E;->x(LX/74E;)LX/11o;

    move-result-object v1

    const/4 v2, 0x0

    const v3, -0x651c05da

    invoke-static {v1, p1, v2, v0, v3}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 1167663
    return-void
.end method

.method public static x(LX/74E;)LX/11o;
    .locals 3

    .prologue
    .line 1167657
    iget-object v0, p0, LX/74E;->b:LX/11i;

    sget-object v1, LX/74E;->a:LX/74D;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1167658
    if-nez v0, :cond_0

    .line 1167659
    iget-object v0, p0, LX/74E;->b:LX/11i;

    sget-object v1, LX/74E;->a:LX/74D;

    const/4 v2, 0x0

    invoke-static {p0, v2}, LX/74E;->a(LX/74E;Z)Ljava/util/Map;

    move-result-object v2

    invoke-static {v2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/11i;->a(LX/0Pq;LX/0P1;)LX/11o;

    move-result-object v0

    move-object v0, v0

    .line 1167660
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(IJJI)V
    .locals 4

    .prologue
    .line 1167650
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/74E;->a(LX/74E;Z)Ljava/util/Map;

    move-result-object v0

    .line 1167651
    const-string v1, "chunk_id"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167652
    const-string v1, "chunk_size"

    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167653
    const-string v1, "chunk_offset"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167654
    const-string v1, "retry_count"

    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167655
    const-string v1, "ChunkMarker"

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    invoke-static {p0, v1, v0}, LX/74E;->a(LX/74E;Ljava/lang/String;LX/0P1;)V

    .line 1167656
    return-void
.end method

.method public final a(Ljava/lang/String;IIIIIIIIJ)V
    .locals 4

    .prologue
    .line 1167637
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/74E;->a(LX/74E;Z)Ljava/util/Map;

    move-result-object v0

    .line 1167638
    const-string v1, "format"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167639
    const-string v1, "source_width"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167640
    const-string v1, "source_height"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167641
    const-string v1, "source_bit_rate"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167642
    const-string v1, "source_frame_rate"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167643
    const-string v1, "target_width"

    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167644
    const-string v1, "target_height"

    invoke-static {p7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167645
    const-string v1, "target_bit_rate"

    invoke-static {p8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167646
    const-string v1, "target_frame_rate"

    invoke-static {p9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167647
    const-string v1, "video_time"

    invoke-static {p10, p11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167648
    const-string v1, "ProcessVideoMarker"

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    invoke-static {p0, v1, v0}, LX/74E;->b(LX/74E;Ljava/lang/String;LX/0P1;)V

    .line 1167649
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1167634
    const-string v0, "FlowMarker"

    invoke-static {p0, v0}, LX/74E;->d(LX/74E;Ljava/lang/String;)V

    .line 1167635
    iget-object v0, p0, LX/74E;->b:LX/11i;

    sget-object v1, LX/74E;->a:LX/74D;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V

    .line 1167636
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1167631
    const-string v0, "FlowMarker"

    invoke-static {p0, v0}, LX/74E;->b(LX/74E;Ljava/lang/String;)V

    .line 1167632
    iget-object v0, p0, LX/74E;->b:LX/11i;

    sget-object v1, LX/74E;->a:LX/74D;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V

    .line 1167633
    return-void
.end method

.method public final t()V
    .locals 1

    .prologue
    .line 1167629
    const-string v0, "ChunkMarker"

    invoke-static {p0, v0}, LX/74E;->b(LX/74E;Ljava/lang/String;)V

    .line 1167630
    return-void
.end method
