.class public LX/72Y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/73Y;

.field public b:Lcom/facebook/payments/shipping/model/ShippingCommonParams;

.field public c:LX/6qh;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1164661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164662
    return-void
.end method

.method public static a$redex0(LX/72Y;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1164663
    iget-object v0, p0, LX/72Y;->b:Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    iget-object v0, v0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1164664
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1164665
    const-string v1, "extra_mutation"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164666
    const-string v1, "shipping_address_id"

    iget-object v2, p0, LX/72Y;->b:Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    iget-object v2, v2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164667
    new-instance v1, LX/73T;

    sget-object v2, LX/73S;->MUTATION:LX/73S;

    invoke-direct {v1, v2, v0}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    .line 1164668
    iget-object v0, p0, LX/72Y;->c:LX/6qh;

    invoke-virtual {v0, v1}, LX/6qh;->a(LX/73T;)V

    .line 1164669
    return-void
.end method

.method public static e(LX/72Y;)V
    .locals 3

    .prologue
    .line 1164657
    iget-object v0, p0, LX/72Y;->a:LX/73Y;

    const v1, 0x7f081e59

    .line 1164658
    iget-object v2, v0, LX/73Y;->e:Lcom/facebook/payments/ui/CallToActionSummaryView;

    invoke-virtual {v2, v1}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->setText(I)V

    .line 1164659
    iget-object v0, p0, LX/72Y;->a:LX/73Y;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/73Y;->setVisibilityOfDefaultActionSummary(I)V

    .line 1164660
    return-void
.end method

.method public static f(LX/72Y;)V
    .locals 2

    .prologue
    .line 1164655
    iget-object v0, p0, LX/72Y;->a:LX/73Y;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/73Y;->setVisibilityOfDefaultActionSummary(I)V

    .line 1164656
    return-void
.end method
