.class public LX/8Bh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public mEndOffset:J

.field public mSessionFbid:Ljava/lang/String;

.field public mSkipUpload:Z

.field public mStartOffset:J

.field public mVideoFbid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JJZ)V
    .locals 1

    .prologue
    .line 1309753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309754
    iput-object p1, p0, LX/8Bh;->mSessionFbid:Ljava/lang/String;

    .line 1309755
    iput-object p2, p0, LX/8Bh;->mVideoFbid:Ljava/lang/String;

    .line 1309756
    iput-wide p3, p0, LX/8Bh;->mStartOffset:J

    .line 1309757
    iput-wide p5, p0, LX/8Bh;->mEndOffset:J

    .line 1309758
    iput-boolean p7, p0, LX/8Bh;->mSkipUpload:Z

    .line 1309759
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 1309766
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/8Bh;->mSessionFbid:Ljava/lang/String;

    .line 1309767
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/8Bh;->mVideoFbid:Ljava/lang/String;

    .line 1309768
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, LX/8Bh;->mStartOffset:J

    .line 1309769
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, LX/8Bh;->mEndOffset:J

    .line 1309770
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, LX/8Bh;->mSkipUpload:Z

    .line 1309771
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    .prologue
    .line 1309760
    iget-object v0, p0, LX/8Bh;->mSessionFbid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 1309761
    iget-object v0, p0, LX/8Bh;->mVideoFbid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 1309762
    iget-wide v0, p0, LX/8Bh;->mStartOffset:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 1309763
    iget-wide v0, p0, LX/8Bh;->mEndOffset:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 1309764
    iget-boolean v0, p0, LX/8Bh;->mSkipUpload:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    .line 1309765
    return-void
.end method
