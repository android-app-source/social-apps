.class public final enum LX/89m;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/89m;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/89m;

.field public static final enum AD_PREVIEW_PYML_JSON:LX/89m;

.field public static final enum AD_PREVIEW_STORY_JSON:LX/89m;

.field public static final enum FEEDBACK_ID_KEY:LX/89m;

.field public static final enum FEED_STORY_ID_KEY:LX/89m;

.field public static final enum FEED_STORY_JSON:LX/89m;

.field public static final enum NOTIF_STORY_ID_KEY:LX/89m;

.field public static final enum NOTIF_STORY_JSON:LX/89m;

.field public static final enum PLATFORM_KEY:LX/89m;

.field public static final enum STORY_FBID_KEY:LX/89m;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1305264
    new-instance v0, LX/89m;

    const-string v1, "PLATFORM_KEY"

    invoke-direct {v0, v1, v3}, LX/89m;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89m;->PLATFORM_KEY:LX/89m;

    .line 1305265
    new-instance v0, LX/89m;

    const-string v1, "FEED_STORY_ID_KEY"

    invoke-direct {v0, v1, v4}, LX/89m;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89m;->FEED_STORY_ID_KEY:LX/89m;

    .line 1305266
    new-instance v0, LX/89m;

    const-string v1, "FEED_STORY_JSON"

    invoke-direct {v0, v1, v5}, LX/89m;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89m;->FEED_STORY_JSON:LX/89m;

    .line 1305267
    new-instance v0, LX/89m;

    const-string v1, "NOTIF_STORY_JSON"

    invoke-direct {v0, v1, v6}, LX/89m;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89m;->NOTIF_STORY_JSON:LX/89m;

    .line 1305268
    new-instance v0, LX/89m;

    const-string v1, "NOTIF_STORY_ID_KEY"

    invoke-direct {v0, v1, v7}, LX/89m;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89m;->NOTIF_STORY_ID_KEY:LX/89m;

    .line 1305269
    new-instance v0, LX/89m;

    const-string v1, "FEEDBACK_ID_KEY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/89m;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89m;->FEEDBACK_ID_KEY:LX/89m;

    .line 1305270
    new-instance v0, LX/89m;

    const-string v1, "AD_PREVIEW_PYML_JSON"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/89m;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89m;->AD_PREVIEW_PYML_JSON:LX/89m;

    .line 1305271
    new-instance v0, LX/89m;

    const-string v1, "AD_PREVIEW_STORY_JSON"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/89m;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89m;->AD_PREVIEW_STORY_JSON:LX/89m;

    .line 1305272
    new-instance v0, LX/89m;

    const-string v1, "STORY_FBID_KEY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/89m;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/89m;->STORY_FBID_KEY:LX/89m;

    .line 1305273
    const/16 v0, 0x9

    new-array v0, v0, [LX/89m;

    sget-object v1, LX/89m;->PLATFORM_KEY:LX/89m;

    aput-object v1, v0, v3

    sget-object v1, LX/89m;->FEED_STORY_ID_KEY:LX/89m;

    aput-object v1, v0, v4

    sget-object v1, LX/89m;->FEED_STORY_JSON:LX/89m;

    aput-object v1, v0, v5

    sget-object v1, LX/89m;->NOTIF_STORY_JSON:LX/89m;

    aput-object v1, v0, v6

    sget-object v1, LX/89m;->NOTIF_STORY_ID_KEY:LX/89m;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/89m;->FEEDBACK_ID_KEY:LX/89m;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/89m;->AD_PREVIEW_PYML_JSON:LX/89m;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/89m;->AD_PREVIEW_STORY_JSON:LX/89m;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/89m;->STORY_FBID_KEY:LX/89m;

    aput-object v2, v0, v1

    sput-object v0, LX/89m;->$VALUES:[LX/89m;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1305274
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/89m;
    .locals 1

    .prologue
    .line 1305275
    const-class v0, LX/89m;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/89m;

    return-object v0
.end method

.method public static values()[LX/89m;
    .locals 1

    .prologue
    .line 1305276
    sget-object v0, LX/89m;->$VALUES:[LX/89m;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/89m;

    return-object v0
.end method


# virtual methods
.method public final isJsonType()Z
    .locals 1

    .prologue
    .line 1305277
    sget-object v0, LX/89m;->FEED_STORY_JSON:LX/89m;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/89m;->NOTIF_STORY_JSON:LX/89m;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/89m;->AD_PREVIEW_STORY_JSON:LX/89m;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/89m;->AD_PREVIEW_PYML_JSON:LX/89m;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isNotificationType()Z
    .locals 1

    .prologue
    .line 1305278
    sget-object v0, LX/89m;->NOTIF_STORY_JSON:LX/89m;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/89m;->NOTIF_STORY_ID_KEY:LX/89m;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
