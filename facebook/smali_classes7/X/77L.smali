.class public LX/77L;
.super LX/2g7;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/77L;


# instance fields
.field private final a:LX/1JG;


# direct methods
.method public constructor <init>(LX/1JG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171714
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1171715
    iput-object p1, p0, LX/77L;->a:LX/1JG;

    .line 1171716
    return-void
.end method

.method public static a(LX/0QB;)LX/77L;
    .locals 4

    .prologue
    .line 1171717
    sget-object v0, LX/77L;->b:LX/77L;

    if-nez v0, :cond_1

    .line 1171718
    const-class v1, LX/77L;

    monitor-enter v1

    .line 1171719
    :try_start_0
    sget-object v0, LX/77L;->b:LX/77L;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1171720
    if-eqz v2, :cond_0

    .line 1171721
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1171722
    new-instance p0, LX/77L;

    invoke-static {v0}, LX/1JG;->a(LX/0QB;)LX/1JG;

    move-result-object v3

    check-cast v3, LX/1JG;

    invoke-direct {p0, v3}, LX/77L;-><init>(LX/1JG;)V

    .line 1171723
    move-object v0, p0

    .line 1171724
    sput-object v0, LX/77L;->b:LX/77L;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1171725
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1171726
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1171727
    :cond_1
    sget-object v0, LX/77L;->b:LX/77L;

    return-object v0

    .line 1171728
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1171729
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 4
    .param p1    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1171730
    invoke-virtual {p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->b()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1171731
    invoke-virtual {p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->b()Ljava/util/Map;

    move-result-object v1

    .line 1171732
    const-string v0, "timeframe"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1171733
    const-string v0, "min_refreshes"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1171734
    iget-object v1, p0, LX/77L;->a:LX/1JG;

    invoke-virtual {v1, v2, v3}, LX/1JG;->a(J)I

    move-result v1

    if-lt v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
