.class public LX/8FY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1318005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1318006
    invoke-virtual {p0, v0}, Landroid/view/View;->setClickable(Z)V

    .line 1318007
    new-array v0, v0, [I

    const v1, 0x101030e

    aput v1, v0, v2

    .line 1318008
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1318009
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1318010
    invoke-virtual {p0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1318011
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1318012
    return-void
.end method
