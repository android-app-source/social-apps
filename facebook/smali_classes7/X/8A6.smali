.class public final LX/8A6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:LX/4gI;

.field public c:Z

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/8AB;

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:I

.field public v:I

.field public w:Z

.field public x:I

.field public y:I

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1305612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305613
    iput-boolean v2, p0, LX/8A6;->a:Z

    .line 1305614
    sget-object v0, LX/4gI;->ALL:LX/4gI;

    iput-object v0, p0, LX/8A6;->b:LX/4gI;

    .line 1305615
    iput-boolean v2, p0, LX/8A6;->c:Z

    .line 1305616
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1305617
    iput-object v0, p0, LX/8A6;->d:LX/0Px;

    .line 1305618
    iput-boolean v1, p0, LX/8A6;->f:Z

    .line 1305619
    iput-boolean v1, p0, LX/8A6;->g:Z

    .line 1305620
    iput-boolean v1, p0, LX/8A6;->h:Z

    .line 1305621
    iput-boolean v1, p0, LX/8A6;->i:Z

    .line 1305622
    iput-boolean v1, p0, LX/8A6;->j:Z

    .line 1305623
    iput-boolean v2, p0, LX/8A6;->k:Z

    .line 1305624
    iput-boolean v2, p0, LX/8A6;->l:Z

    .line 1305625
    iput-boolean v1, p0, LX/8A6;->m:Z

    .line 1305626
    iput-boolean v1, p0, LX/8A6;->n:Z

    .line 1305627
    iput-boolean v1, p0, LX/8A6;->o:Z

    .line 1305628
    iput-boolean v1, p0, LX/8A6;->p:Z

    .line 1305629
    iput-boolean v1, p0, LX/8A6;->q:Z

    .line 1305630
    iput-boolean v1, p0, LX/8A6;->r:Z

    .line 1305631
    iput-boolean v1, p0, LX/8A6;->s:Z

    .line 1305632
    iput-boolean v1, p0, LX/8A6;->t:Z

    .line 1305633
    iput v1, p0, LX/8A6;->u:I

    .line 1305634
    iput v1, p0, LX/8A6;->v:I

    .line 1305635
    iput-boolean v1, p0, LX/8A6;->w:Z

    .line 1305636
    iput v1, p0, LX/8A6;->x:I

    .line 1305637
    iput v1, p0, LX/8A6;->y:I

    .line 1305638
    iput-boolean v1, p0, LX/8A6;->z:Z

    .line 1305639
    return-void
.end method


# virtual methods
.method public final a(II)LX/8A6;
    .locals 2

    .prologue
    .line 1305640
    if-ltz p1, :cond_1

    if-ge p2, p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "min >= 0 AND (max >= min OR max == NO_MAX)"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1305641
    iput p1, p0, LX/8A6;->u:I

    .line 1305642
    iput p2, p0, LX/8A6;->v:I

    .line 1305643
    return-object p0

    .line 1305644
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()LX/8A6;
    .locals 1

    .prologue
    .line 1305645
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8A6;->h:Z

    .line 1305646
    return-object p0
.end method
