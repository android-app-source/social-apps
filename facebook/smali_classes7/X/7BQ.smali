.class public LX/7BQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;",
        "LX/7Hc;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/266;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/266",
            "<",
            "Lcom/facebook/search/api/model/BatchGraphSearchJsonResponse;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/7Bb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/7B0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1178887
    new-instance v0, LX/7BP;

    invoke-direct {v0}, LX/7BP;-><init>()V

    sput-object v0, LX/7BQ;->a:LX/266;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1178886
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 1178849
    check-cast p1, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;

    .line 1178850
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1178851
    iget-object v1, p0, LX/7BQ;->b:LX/7Bb;

    invoke-virtual {v1, p1, v0}, LX/7Bb;->a(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;Ljava/util/List;)V

    .line 1178852
    iget-object v1, p0, LX/7BQ;->b:LX/7Bb;

    invoke-virtual {v1, p1, v0}, LX/7Bb;->b(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;Ljava/util/List;)V

    .line 1178853
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "query"

    invoke-static {p1}, LX/7Bb;->a(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1178854
    const-string v1, "graphsearch_typeahead"

    invoke-static {p1, v1}, LX/7Bb;->a(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1178855
    iget-object v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    sget-object v3, LX/7BZ;->DEFAULT_KEYWORD_MODE:LX/7BZ;

    if-ne v2, v3, :cond_3

    .line 1178856
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "type"

    const-string v4, "blendedtypeahead"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1178857
    :goto_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "summary"

    const-string v4, "num_topresults_to_show"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1178858
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "max_results"

    iget v4, p1, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1178859
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "batch_num"

    iget v4, p1, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1178860
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "offset"

    iget v4, p1, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1178861
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "can_override_max_results"

    iget-boolean v4, p1, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->d:Z

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1178862
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    .line 1178863
    iput-object v1, v2, LX/14O;->b:Ljava/lang/String;

    .line 1178864
    move-object v1, v2

    .line 1178865
    const-string v2, "GET"

    .line 1178866
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1178867
    move-object v1, v1

    .line 1178868
    const-string v2, "search"

    .line 1178869
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1178870
    move-object v1, v1

    .line 1178871
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1, v2}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v1

    .line 1178872
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1178873
    move-object v0, v1

    .line 1178874
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 1178875
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1178876
    move-object v1, v0

    .line 1178877
    iget-object v0, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    sget-object v2, LX/7BZ;->KEYWORD_ONLY_MODE:LX/7BZ;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, LX/7BQ;->c:LX/0ad;

    sget-short v2, LX/100;->bf:S

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/7BQ;->d:LX/7B0;

    invoke-virtual {v0}, LX/7B0;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1178878
    :cond_1
    const/4 v0, 0x1

    .line 1178879
    iput-boolean v0, v1, LX/14O;->B:Z

    .line 1178880
    :cond_2
    iget-object v2, p0, LX/7BQ;->d:LX/7B0;

    iget-object v0, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1178881
    iget-object v3, v0, LX/7B6;->b:Ljava/lang/String;

    move-object v0, v3

    .line 1178882
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, LX/7Az;->CACHEWARMER:LX/7Az;

    :goto_1
    invoke-virtual {v2, v0, v1}, LX/7B0;->a(LX/7Az;LX/14O;)V

    .line 1178883
    invoke-virtual {v1}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1178884
    :cond_3
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "type"

    const-string v4, "typeahead"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1178885
    :cond_4
    sget-object v0, LX/7Az;->TYPEAHEAD:LX/7Az;

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1178846
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    sget-object v1, LX/7BQ;->a:LX/266;

    invoke-virtual {v0, v1}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/model/BatchGraphSearchJsonResponse;

    .line 1178847
    iget-object v1, v0, Lcom/facebook/search/api/model/BatchGraphSearchJsonResponse;->data:Ljava/util/List;

    .line 1178848
    new-instance v2, LX/7Hc;

    invoke-static {v1}, LX/7BD;->a(Ljava/util/List;)LX/0Px;

    move-result-object v3

    iget-object v1, v0, Lcom/facebook/search/api/model/BatchGraphSearchJsonResponse;->numTopResultsToShow:Ljava/util/Map;

    const-string v4, "num_topresults_to_show"

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lcom/facebook/search/api/model/BatchGraphSearchJsonResponse;->numTopResultsToShow:Ljava/util/Map;

    const-string v1, "num_topresults_to_show"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    invoke-direct {v2, v3, v0}, LX/7Hc;-><init>(LX/0Px;I)V

    return-object v2

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
