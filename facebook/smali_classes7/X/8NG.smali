.class public LX/8NG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1336467
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/8NG;
    .locals 1

    .prologue
    .line 1336475
    new-instance v0, LX/8NG;

    invoke-direct {v0}, LX/8NG;-><init>()V

    .line 1336476
    move-object v0, v0

    .line 1336477
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 1336478
    check-cast p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    .line 1336479
    iget-object v0, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->m:LX/0Px;

    move-object v1, v0

    .line 1336480
    iget-wide v8, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    move-wide v2, v8

    .line 1336481
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 1336482
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1336483
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1336484
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "tags"

    invoke-static {v1}, LX/759;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v4, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336485
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v3, "photoUpdate"

    .line 1336486
    iput-object v3, v1, LX/14O;->b:Ljava/lang/String;

    .line 1336487
    move-object v1, v1

    .line 1336488
    const-string v3, "POST"

    .line 1336489
    iput-object v3, v1, LX/14O;->c:Ljava/lang/String;

    .line 1336490
    move-object v1, v1

    .line 1336491
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 1336492
    const-string v3, "%s/tags"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 1336493
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1336494
    move-object v1, v1

    .line 1336495
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1336496
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1336497
    move-object v0, v1

    .line 1336498
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1336499
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1336500
    move-object v0, v0

    .line 1336501
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1336502
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1336468
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1336469
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 1336470
    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 1336471
    iget-object v0, p2, LX/1pN;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1336472
    instance-of v0, v0, LX/1Xb;

    if-eqz v0, :cond_0

    .line 1336473
    iget-object v0, p2, LX/1pN;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1336474
    check-cast v0, LX/1Xb;

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
