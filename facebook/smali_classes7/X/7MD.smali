.class public final enum LX/7MD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7MD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7MD;

.field public static final enum ALWAYS_INVISIBLE:LX/7MD;

.field public static final enum DEFAULT:LX/7MD;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1198531
    new-instance v0, LX/7MD;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/7MD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7MD;->DEFAULT:LX/7MD;

    .line 1198532
    new-instance v0, LX/7MD;

    const-string v1, "ALWAYS_INVISIBLE"

    invoke-direct {v0, v1, v3}, LX/7MD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7MD;->ALWAYS_INVISIBLE:LX/7MD;

    .line 1198533
    const/4 v0, 0x2

    new-array v0, v0, [LX/7MD;

    sget-object v1, LX/7MD;->DEFAULT:LX/7MD;

    aput-object v1, v0, v2

    sget-object v1, LX/7MD;->ALWAYS_INVISIBLE:LX/7MD;

    aput-object v1, v0, v3

    sput-object v0, LX/7MD;->$VALUES:[LX/7MD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1198534
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7MD;
    .locals 1

    .prologue
    .line 1198535
    const-class v0, LX/7MD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7MD;

    return-object v0
.end method

.method public static values()[LX/7MD;
    .locals 1

    .prologue
    .line 1198536
    sget-object v0, LX/7MD;->$VALUES:[LX/7MD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7MD;

    return-object v0
.end method
