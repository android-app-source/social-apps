.class public final LX/7Rl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:Landroid/opengl/EGLDisplay;

.field private c:Landroid/opengl/EGLContext;

.field private d:Landroid/opengl/EGLConfig;

.field private e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1207835
    const-class v0, LX/7Rl;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Rl;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1207833
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/7Rl;-><init>(Landroid/opengl/EGLContext;I)V

    .line 1207834
    return-void
.end method

.method public constructor <init>(Landroid/opengl/EGLContext;I)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 1207793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1207794
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    iput-object v0, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    .line 1207795
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    iput-object v0, p0, LX/7Rl;->c:Landroid/opengl/EGLContext;

    .line 1207796
    iput-object v2, p0, LX/7Rl;->d:Landroid/opengl/EGLConfig;

    .line 1207797
    const/4 v0, -0x1

    iput v0, p0, LX/7Rl;->e:I

    .line 1207798
    iget-object v0, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-eq v0, v1, :cond_0

    .line 1207799
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "EGL already set up"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1207800
    :cond_0
    if-nez p1, :cond_1

    .line 1207801
    sget-object p1, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    .line 1207802
    :cond_1
    invoke-static {v4}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    .line 1207803
    iget-object v0, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-ne v0, v1, :cond_2

    .line 1207804
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to get EGL14 display"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1207805
    :cond_2
    new-array v0, v6, [I

    .line 1207806
    iget-object v1, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    invoke-static {v1, v0, v4, v0, v7}, Landroid/opengl/EGL14;->eglInitialize(Landroid/opengl/EGLDisplay;[II[II)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1207807
    iput-object v2, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    .line 1207808
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to initialize EGL14"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1207809
    :cond_3
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_4

    .line 1207810
    invoke-direct {p0, p2, v5}, LX/7Rl;->a(II)Landroid/opengl/EGLConfig;

    move-result-object v0

    .line 1207811
    if-eqz v0, :cond_4

    .line 1207812
    new-array v1, v5, [I

    fill-array-data v1, :array_0

    .line 1207813
    iget-object v2, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    invoke-static {v2, v0, p1, v1, v4}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v1

    .line 1207814
    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v2

    const/16 v3, 0x3000

    if-ne v2, v3, :cond_4

    .line 1207815
    iput-object v0, p0, LX/7Rl;->d:Landroid/opengl/EGLConfig;

    .line 1207816
    iput-object v1, p0, LX/7Rl;->c:Landroid/opengl/EGLContext;

    .line 1207817
    iput v5, p0, LX/7Rl;->e:I

    .line 1207818
    :cond_4
    iget-object v0, p0, LX/7Rl;->c:Landroid/opengl/EGLContext;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    if-ne v0, v1, :cond_6

    .line 1207819
    invoke-direct {p0, p2, v6}, LX/7Rl;->a(II)Landroid/opengl/EGLConfig;

    move-result-object v0

    .line 1207820
    if-nez v0, :cond_5

    .line 1207821
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to find a suitable EGLConfig"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1207822
    :cond_5
    new-array v1, v5, [I

    fill-array-data v1, :array_1

    .line 1207823
    iget-object v2, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    invoke-static {v2, v0, p1, v1, v4}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v1

    .line 1207824
    const-string v2, "eglCreateContext"

    invoke-static {v2}, LX/7Rl;->a(Ljava/lang/String;)V

    .line 1207825
    iput-object v0, p0, LX/7Rl;->d:Landroid/opengl/EGLConfig;

    .line 1207826
    iput-object v1, p0, LX/7Rl;->c:Landroid/opengl/EGLContext;

    .line 1207827
    iput v6, p0, LX/7Rl;->e:I

    .line 1207828
    :cond_6
    new-array v0, v7, [I

    .line 1207829
    iget-object v1, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, LX/7Rl;->c:Landroid/opengl/EGLContext;

    const/16 v3, 0x3098

    invoke-static {v1, v2, v3, v0, v4}, Landroid/opengl/EGL14;->eglQueryContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;I[II)Z

    .line 1207830
    return-void

    .line 1207831
    :array_0
    .array-data 4
        0x3098
        0x3
        0x3038
    .end array-data

    .line 1207832
    :array_1
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method private a(II)Landroid/opengl/EGLConfig;
    .locals 9

    .prologue
    const/4 v3, 0x4

    const/4 v8, 0x3

    const/16 v7, 0x8

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1207752
    if-lt p2, v8, :cond_2

    .line 1207753
    const/16 v0, 0x44

    .line 1207754
    :goto_0
    const/16 v1, 0xd

    new-array v1, v1, [I

    const/16 v4, 0x3024

    aput v4, v1, v2

    aput v7, v1, v5

    const/4 v4, 0x2

    const/16 v6, 0x3023

    aput v6, v1, v4

    aput v7, v1, v8

    const/16 v4, 0x3022

    aput v4, v1, v3

    const/4 v3, 0x5

    aput v7, v1, v3

    const/4 v3, 0x6

    const/16 v4, 0x3021

    aput v4, v1, v3

    const/4 v3, 0x7

    aput v7, v1, v3

    const/16 v3, 0x3040

    aput v3, v1, v7

    const/16 v3, 0x9

    aput v0, v1, v3

    const/16 v0, 0xa

    const/16 v3, 0x3038

    aput v3, v1, v0

    const/16 v0, 0xb

    aput v2, v1, v0

    const/16 v0, 0xc

    const/16 v3, 0x3038

    aput v3, v1, v0

    .line 1207755
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    .line 1207756
    const/16 v0, 0xa

    const/16 v3, 0x3142

    aput v3, v1, v0

    .line 1207757
    const/16 v0, 0xb

    aput v5, v1, v0

    .line 1207758
    :cond_0
    new-array v3, v5, [Landroid/opengl/EGLConfig;

    .line 1207759
    new-array v6, v5, [I

    .line 1207760
    iget-object v0, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    move v4, v2

    move v7, v2

    invoke-static/range {v0 .. v7}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1207761
    sget-object v0, LX/7Rl;->a:Ljava/lang/String;

    const-string v1, "unable to find RGB8888 /%d EGLConfig"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v1, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1207762
    const/4 v0, 0x0

    .line 1207763
    :goto_1
    return-object v0

    :cond_1
    aget-object v0, v3, v2

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1207790
    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v0

    const/16 v1, 0x3000

    if-eq v0, v1, :cond_0

    .line 1207791
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": EGL error: 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1207792
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Landroid/opengl/EGLSurface;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1207782
    instance-of v0, p1, Landroid/view/Surface;

    if-nez v0, :cond_0

    instance-of v0, p1, Landroid/graphics/SurfaceTexture;

    if-nez v0, :cond_0

    .line 1207783
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid surface: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1207784
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/16 v1, 0x3038

    aput v1, v0, v3

    .line 1207785
    iget-object v1, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, LX/7Rl;->d:Landroid/opengl/EGLConfig;

    invoke-static {v1, v2, p1, v0, v3}, Landroid/opengl/EGL14;->eglCreateWindowSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Ljava/lang/Object;[II)Landroid/opengl/EGLSurface;

    move-result-object v0

    .line 1207786
    const-string v1, "eglCreateWindowSurface"

    invoke-static {v1}, LX/7Rl;->a(Ljava/lang/String;)V

    .line 1207787
    if-nez v0, :cond_1

    .line 1207788
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "surface was null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1207789
    :cond_1
    return-object v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 1207773
    iget-object v0, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-eq v0, v1, :cond_0

    .line 1207774
    iget-object v0, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 1207775
    iget-object v0, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, LX/7Rl;->c:Landroid/opengl/EGLContext;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    .line 1207776
    invoke-static {}, Landroid/opengl/EGL14;->eglReleaseThread()Z

    .line 1207777
    iget-object v0, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    invoke-static {v0}, Landroid/opengl/EGL14;->eglTerminate(Landroid/opengl/EGLDisplay;)Z

    .line 1207778
    :cond_0
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    iput-object v0, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    .line 1207779
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    iput-object v0, p0, LX/7Rl;->c:Landroid/opengl/EGLContext;

    .line 1207780
    const/4 v0, 0x0

    iput-object v0, p0, LX/7Rl;->d:Landroid/opengl/EGLConfig;

    .line 1207781
    return-void
.end method

.method public final b(Landroid/opengl/EGLSurface;)V
    .locals 2

    .prologue
    .line 1207770
    iget-object v0, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, LX/7Rl;->c:Landroid/opengl/EGLContext;

    invoke-static {v0, p1, p1, v1}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1207771
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglMakeCurrent failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1207772
    :cond_0
    return-void
.end method

.method public final finalize()V
    .locals 2

    .prologue
    .line 1207764
    :try_start_0
    iget-object v0, p0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-eq v0, v1, :cond_0

    .line 1207765
    sget-object v0, LX/7Rl;->a:Ljava/lang/String;

    const-string v1, "WARNING: EglCore was not explicitly released -- state may be leaked"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207766
    invoke-virtual {p0}, LX/7Rl;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1207767
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 1207768
    return-void

    .line 1207769
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
