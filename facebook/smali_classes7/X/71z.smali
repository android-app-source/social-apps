.class public final LX/71z;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/payments/shipping/model/MailingAddress;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6zj;

.field public final synthetic b:Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;

.field public final synthetic c:LX/720;


# direct methods
.method public constructor <init>(LX/720;LX/6zj;Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;)V
    .locals 0

    .prologue
    .line 1163851
    iput-object p1, p0, LX/71z;->c:LX/720;

    iput-object p2, p0, LX/71z;->a:LX/6zj;

    iput-object p3, p0, LX/71z;->b:Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1163852
    iget-object v0, p0, LX/71z;->c:LX/720;

    iget-object v0, v0, LX/720;->e:LX/70k;

    new-instance v1, LX/71y;

    invoke-direct {v1, p0}, LX/71y;-><init>(LX/71z;)V

    invoke-virtual {v0, v1}, LX/70k;->a(LX/1DI;)V

    .line 1163853
    const-class v0, LX/2Oo;

    invoke-static {p1, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 1163854
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/2Oo;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 1163855
    :goto_0
    iget-object v1, p0, LX/71z;->c:LX/720;

    iget-object v1, v1, LX/720;->c:LX/03V;

    sget-object v2, LX/720;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Get mailing addresses for the logged-in user failed. "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1163856
    return-void

    .line 1163857
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1163858
    check-cast p1, LX/0Px;

    .line 1163859
    iget-object v0, p0, LX/71z;->c:LX/720;

    iget-object v0, v0, LX/720;->e:LX/70k;

    invoke-virtual {v0}, LX/70k;->b()V

    .line 1163860
    iget-object v0, p0, LX/71z;->a:LX/6zj;

    new-instance v1, Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;

    invoke-direct {v1, p1}, Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;-><init>(LX/0Px;)V

    invoke-interface {v0, v1}, LX/6zj;->a(Lcom/facebook/payments/picker/model/CoreClientData;)V

    .line 1163861
    return-void
.end method
