.class public LX/8Nb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/8Nc;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0lB;


# direct methods
.method public constructor <init>(LX/0SG;LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1337672
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1337673
    iput-object p1, p0, LX/8Nb;->a:LX/0SG;

    .line 1337674
    iput-object p2, p0, LX/8Nb;->b:LX/0lB;

    .line 1337675
    return-void
.end method

.method public static a(LX/0QB;)LX/8Nb;
    .locals 3

    .prologue
    .line 1337676
    new-instance v2, LX/8Nb;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v1

    check-cast v1, LX/0lB;

    invoke-direct {v2, v0, v1}, LX/8Nb;-><init>(LX/0SG;LX/0lB;)V

    .line 1337677
    move-object v0, v2

    .line 1337678
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1337679
    check-cast p1, LX/8Nc;

    .line 1337680
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1337681
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    .line 1337682
    iget-object v2, p0, LX/8Nb;->a:LX/0SG;

    iget-object v3, p0, LX/8Nb;->b:LX/0lB;

    invoke-static {v0, v1, p1, v2, v3}, LX/8Na;->a(LX/0Pz;LX/14O;LX/8NQ;LX/0SG;LX/0lB;)V

    .line 1337683
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 1337684
    const-string v2, "images_urls"

    .line 1337685
    iget-object v3, p1, LX/8Nc;->c:LX/0Px;

    move-object v3, v3

    .line 1337686
    invoke-static {v3}, LX/8Na;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1337687
    const-string v2, "duration_ms"

    .line 1337688
    iget v3, p1, LX/8Nc;->d:I

    move v3, v3

    .line 1337689
    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1337690
    const-string v2, "transition_ms"

    .line 1337691
    iget v3, p1, LX/8Nc;->e:I

    move v3, v3

    .line 1337692
    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1337693
    iget-object v2, p1, LX/8Nc;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1337694
    if-eqz v2, :cond_0

    .line 1337695
    const-string v2, "storyline_mood_id"

    .line 1337696
    iget-object v3, p1, LX/8Nc;->f:Ljava/lang/String;

    move-object v3, v3

    .line 1337697
    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1337698
    :cond_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "slideshow_spec"

    invoke-virtual {v1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337699
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v2.3/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1337700
    iget-wide v4, p1, LX/8NQ;->i:J

    move-wide v2, v4

    .line 1337701
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/videos"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1337702
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "post-slideshow-video"

    .line 1337703
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1337704
    move-object v2, v2

    .line 1337705
    const-string v3, "POST"

    .line 1337706
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 1337707
    move-object v2, v2

    .line 1337708
    sget-object v3, LX/14S;->JSON:LX/14S;

    .line 1337709
    iput-object v3, v2, LX/14O;->k:LX/14S;

    .line 1337710
    move-object v2, v2

    .line 1337711
    iput-object v1, v2, LX/14O;->d:Ljava/lang/String;

    .line 1337712
    move-object v1, v2

    .line 1337713
    const/4 v2, 0x1

    .line 1337714
    iput-boolean v2, v1, LX/14O;->n:Z

    .line 1337715
    move-object v1, v1

    .line 1337716
    iget-object v2, p1, LX/8NQ;->m:Ljava/lang/String;

    move-object v2, v2

    .line 1337717
    iput-object v2, v1, LX/14O;->A:Ljava/lang/String;

    .line 1337718
    move-object v1, v1

    .line 1337719
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1337720
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1337721
    move-object v0, v1

    .line 1337722
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1337723
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1337724
    :cond_0
    const/4 v0, 0x0

    .line 1337725
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
