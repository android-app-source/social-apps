.class public LX/88t;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "LX/88q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 1303021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1303022
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-static {}, LX/88p;->values()[LX/88p;

    move-result-object v1

    array-length v1, v1

    new-instance v2, LX/88s;

    invoke-direct {v2}, LX/88s;-><init>()V

    invoke-direct {v0, v1, v2}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v0, p0, LX/88t;->a:Ljava/util/PriorityQueue;

    .line 1303023
    return-void
.end method


# virtual methods
.method public final b()LX/88q;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1303024
    iget-object v2, p0, LX/88t;->a:Ljava/util/PriorityQueue;

    monitor-enter v2

    .line 1303025
    :try_start_0
    iget-object v0, p0, LX/88t;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/88q;

    .line 1303026
    if-nez v0, :cond_0

    .line 1303027
    monitor-exit v2

    move-object v0, v1

    .line 1303028
    :goto_0
    return-object v0

    .line 1303029
    :cond_0
    iget-object v3, v0, LX/88q;->d:LX/88p;

    move-object v0, v3

    .line 1303030
    sget-object v3, LX/88q;->a:Ljava/util/EnumSet;

    invoke-virtual {v3, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 1303031
    if-eqz v0, :cond_1

    .line 1303032
    iget-object v0, p0, LX/88t;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/88q;

    monitor-exit v2

    goto :goto_0

    .line 1303033
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1303034
    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_0
.end method
