.class public final LX/85N;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1292993
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1292994
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1292995
    :goto_0
    return v1

    .line 1292996
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1292997
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1292998
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1292999
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1293000
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1293001
    const-string v4, "edges"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1293002
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1293003
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 1293004
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 1293005
    const/4 v4, 0x0

    .line 1293006
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_c

    .line 1293007
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1293008
    :goto_3
    move v3, v4

    .line 1293009
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1293010
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1293011
    goto :goto_1

    .line 1293012
    :cond_3
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1293013
    invoke-static {p0, p1}, LX/3Bn;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1293014
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1293015
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1293016
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1293017
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1293018
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1293019
    :cond_7
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_b

    .line 1293020
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1293021
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1293022
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_7

    if-eqz v8, :cond_7

    .line 1293023
    const-string v9, "contact_name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 1293024
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_4

    .line 1293025
    :cond_8
    const-string v9, "display_credential"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1293026
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_4

    .line 1293027
    :cond_9
    const-string v9, "node"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 1293028
    const/4 v8, 0x0

    .line 1293029
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v9, :cond_12

    .line 1293030
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1293031
    :goto_5
    move v5, v8

    .line 1293032
    goto :goto_4

    .line 1293033
    :cond_a
    const-string v9, "tracking"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1293034
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_4

    .line 1293035
    :cond_b
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1293036
    invoke-virtual {p1, v4, v7}, LX/186;->b(II)V

    .line 1293037
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v6}, LX/186;->b(II)V

    .line 1293038
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v5}, LX/186;->b(II)V

    .line 1293039
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 1293040
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_3

    :cond_c
    move v3, v4

    move v5, v4

    move v6, v4

    move v7, v4

    goto :goto_4

    .line 1293041
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1293042
    :cond_e
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_11

    .line 1293043
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1293044
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1293045
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_e

    if-eqz v10, :cond_e

    .line 1293046
    const-string v11, "__type__"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_f

    const-string v11, "__typename"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_10

    .line 1293047
    :cond_f
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_6

    .line 1293048
    :cond_10
    const-string v11, "__typename"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 1293049
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_6

    .line 1293050
    :cond_11
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1293051
    invoke-virtual {p1, v8, v9}, LX/186;->b(II)V

    .line 1293052
    const/4 v8, 0x1

    invoke-virtual {p1, v8, v5}, LX/186;->b(II)V

    .line 1293053
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_5

    :cond_12
    move v5, v8

    move v9, v8

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1293054
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1293055
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1293056
    if-eqz v0, :cond_1

    .line 1293057
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1293058
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1293059
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1293060
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/85M;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1293061
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1293062
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1293063
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1293064
    if-eqz v0, :cond_2

    .line 1293065
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1293066
    invoke-static {p0, v0, p2}, LX/3Bn;->a(LX/15i;ILX/0nX;)V

    .line 1293067
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1293068
    return-void
.end method
