.class public final LX/784;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)V
    .locals 0

    .prologue
    .line 1172369
    iput-object p1, p0, LX/784;->a:Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 1172370
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1172371
    check-cast p2, LX/0Px;

    .line 1172372
    iget-object v0, p0, LX/784;->a:Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    .line 1172373
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 1172374
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 1172375
    new-instance p1, LX/6UY;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p1, v1}, LX/6UY;-><init>(Landroid/net/Uri;)V

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1172376
    :cond_0
    iget-object v1, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->p:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 1172377
    iget-object v1, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->p:Lcom/facebook/fbui/facepile/FacepileView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 1172378
    iget-object v1, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->p:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v1}, Lcom/facebook/fbui/facepile/FacepileView;->postInvalidate()V

    .line 1172379
    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1172380
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1172381
    return-void
.end method
