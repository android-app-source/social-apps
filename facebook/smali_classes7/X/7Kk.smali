.class public LX/7Kk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Iw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Z

.field private static volatile j:LX/7Kk;


# instance fields
.field private final c:I

.field private final d:D

.field public final e:LX/7Kh;

.field public final f:Landroid/content/Context;

.field public final g:Landroid/app/Notification;

.field private final h:Landroid/app/NotificationManager;

.field public i:LX/7Kj;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1196267
    const-class v0, LX/7Kk;

    sput-object v0, LX/7Kk;->a:Ljava/lang/Class;

    .line 1196268
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/7Kk;->b:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(LX/7Kh;Landroid/content/Context;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1196269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1196270
    iput v2, p0, LX/7Kk;->c:I

    .line 1196271
    const-wide v0, 0x3ffc71c71c71c71cL    # 1.7777777777777777

    iput-wide v0, p0, LX/7Kk;->d:D

    .line 1196272
    iput-object p1, p0, LX/7Kk;->e:LX/7Kh;

    .line 1196273
    iput-object p2, p0, LX/7Kk;->f:Landroid/content/Context;

    .line 1196274
    new-instance v0, LX/2HB;

    invoke-direct {v0, p2}, LX/2HB;-><init>(Landroid/content/Context;)V

    const v1, 0x7f020cf3

    invoke-virtual {v0, v1}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v0

    const/4 v1, 0x2

    .line 1196275
    iput v1, v0, LX/2HB;->j:I

    .line 1196276
    move-object v0, v0

    .line 1196277
    const-string v1, "transport"

    .line 1196278
    iput-object v1, v0, LX/2HB;->w:Ljava/lang/String;

    .line 1196279
    move-object v0, v0

    .line 1196280
    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, LX/7Kk;->g:Landroid/app/Notification;

    .line 1196281
    const-string v0, "notification"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, LX/7Kk;->h:Landroid/app/NotificationManager;

    .line 1196282
    new-instance v0, LX/7Kj;

    invoke-direct {v0, p0}, LX/7Kj;-><init>(LX/7Kk;)V

    iput-object v0, p0, LX/7Kk;->i:LX/7Kj;

    .line 1196283
    return-void
.end method

.method public static a(LX/0QB;)LX/7Kk;
    .locals 5

    .prologue
    .line 1196284
    sget-object v0, LX/7Kk;->j:LX/7Kk;

    if-nez v0, :cond_1

    .line 1196285
    const-class v1, LX/7Kk;

    monitor-enter v1

    .line 1196286
    :try_start_0
    sget-object v0, LX/7Kk;->j:LX/7Kk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1196287
    if-eqz v2, :cond_0

    .line 1196288
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1196289
    new-instance p0, LX/7Kk;

    invoke-static {v0}, LX/7Kh;->a(LX/0QB;)LX/7Kh;

    move-result-object v3

    check-cast v3, LX/7Kh;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {p0, v3, v4}, LX/7Kk;-><init>(LX/7Kh;Landroid/content/Context;)V

    .line 1196290
    move-object v0, p0

    .line 1196291
    sput-object v0, LX/7Kk;->j:LX/7Kk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1196292
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1196293
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1196294
    :cond_1
    sget-object v0, LX/7Kk;->j:LX/7Kk;

    return-object v0

    .line 1196295
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1196296
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/7Kk;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p0    # LX/7Kk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1196297
    if-eqz p1, :cond_0

    .line 1196298
    iget-object v0, p0, LX/7Kk;->g:Landroid/app/Notification;

    iget-object v0, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v1, 0x7f0d08e6

    invoke-virtual {v0, v1, p1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 1196299
    sget-boolean v0, LX/7Kk;->b:Z

    if-eqz v0, :cond_0

    .line 1196300
    iget-object v0, p0, LX/7Kk;->g:Landroid/app/Notification;

    iget-object v0, v0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    const v1, 0x7f0d08e6

    invoke-virtual {v0, v1, p1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 1196301
    :cond_0
    iget-object v0, p0, LX/7Kk;->h:Landroid/app/NotificationManager;

    const/4 v1, 0x1

    iget-object v2, p0, LX/7Kk;->g:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 1196302
    return-void
.end method

.method private f()V
    .locals 7

    .prologue
    .line 1196303
    iget-object v0, p0, LX/7Kk;->e:LX/7Kh;

    .line 1196304
    iget-object v1, v0, LX/7Kh;->d:LX/37Y;

    invoke-virtual {v1}, LX/37Y;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/7Kh;->d:LX/37Y;

    invoke-virtual {v1}, LX/37Y;->g()LX/38p;

    move-result-object v1

    invoke-virtual {v1}, LX/38p;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/7Kh;->d:LX/37Y;

    invoke-virtual {v1}, LX/37Y;->s()LX/38j;

    move-result-object v1

    invoke-virtual {v1}, LX/38j;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1196305
    if-eqz v0, :cond_2

    .line 1196306
    iget-object v1, p0, LX/7Kk;->e:LX/7Kh;

    invoke-virtual {v1}, LX/7Kh;->g()LX/1bf;

    move-result-object v1

    .line 1196307
    iget-object v2, p0, LX/7Kk;->f:Landroid/content/Context;

    iget-object v3, p0, LX/7Kk;->e:LX/7Kh;

    .line 1196308
    iget-object v4, v3, LX/7Kh;->d:LX/37Y;

    .line 1196309
    iget-object v3, v4, LX/37Y;->u:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object v4, v3

    .line 1196310
    move-object v3, v4

    .line 1196311
    if-eqz v1, :cond_4

    .line 1196312
    iget-object v4, v1, LX/1bf;->b:Landroid/net/Uri;

    move-object v1, v4

    .line 1196313
    :goto_1
    const-wide v5, 0x3ffc71c71c71c71cL    # 1.7777777777777777

    invoke-static {v2, v3, v1, v5, v6}, Lcom/facebook/video/player/FullScreenCastActivity;->a(Landroid/content/Context;Lcom/facebook/video/engine/VideoPlayerParams;Landroid/net/Uri;D)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1196314
    iget-object v2, p0, LX/7Kk;->g:Landroid/app/Notification;

    iput-object v1, v2, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 1196315
    iget-object v1, p0, LX/7Kk;->i:LX/7Kj;

    .line 1196316
    invoke-virtual {v1}, LX/7Kj;->d()V

    .line 1196317
    new-instance v2, Landroid/widget/RemoteViews;

    iget-object v3, v1, LX/7Kj;->a:LX/7Kk;

    iget-object v3, v3, LX/7Kk;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f030256

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v2, v1, LX/7Kj;->d:Landroid/widget/RemoteViews;

    .line 1196318
    iget-object v2, v1, LX/7Kj;->d:Landroid/widget/RemoteViews;

    invoke-static {v1, v2}, LX/7Kj;->a(LX/7Kj;Landroid/widget/RemoteViews;)V

    .line 1196319
    sget-boolean v2, LX/7Kk;->b:Z

    if-eqz v2, :cond_0

    .line 1196320
    new-instance v2, Landroid/widget/RemoteViews;

    iget-object v3, v1, LX/7Kj;->a:LX/7Kk;

    iget-object v3, v3, LX/7Kk;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f030257

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v2, v1, LX/7Kj;->e:Landroid/widget/RemoteViews;

    .line 1196321
    iget-object v2, v1, LX/7Kj;->e:Landroid/widget/RemoteViews;

    invoke-static {v1, v2}, LX/7Kj;->a(LX/7Kj;Landroid/widget/RemoteViews;)V

    .line 1196322
    :cond_0
    iget-object v1, p0, LX/7Kk;->g:Landroid/app/Notification;

    iget-object v2, p0, LX/7Kk;->i:LX/7Kj;

    .line 1196323
    iget-object v3, v2, LX/7Kj;->d:Landroid/widget/RemoteViews;

    move-object v2, v3

    .line 1196324
    iput-object v2, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 1196325
    sget-boolean v1, LX/7Kk;->b:Z

    if-eqz v1, :cond_1

    .line 1196326
    iget-object v1, p0, LX/7Kk;->g:Landroid/app/Notification;

    iget-object v2, p0, LX/7Kk;->i:LX/7Kj;

    .line 1196327
    iget-object v3, v2, LX/7Kj;->e:Landroid/widget/RemoteViews;

    move-object v2, v3

    .line 1196328
    iput-object v2, v1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 1196329
    :cond_1
    iget-object v1, p0, LX/7Kk;->e:LX/7Kh;

    new-instance v2, LX/7Ki;

    invoke-direct {v2, p0}, LX/7Ki;-><init>(LX/7Kk;)V

    .line 1196330
    invoke-virtual {v1}, LX/7Kh;->g()LX/1bf;

    move-result-object v3

    .line 1196331
    if-nez v3, :cond_5

    .line 1196332
    sget-object v3, LX/7Kh;->a:Ljava/lang/Class;

    const-string v4, "fetchCoverImage(): no cover image request."

    invoke-static {v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1196333
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/7Ki;->a(Landroid/graphics/Bitmap;)V

    .line 1196334
    :goto_2
    return-void

    .line 1196335
    :cond_2
    invoke-static {p0}, LX/7Kk;->h(LX/7Kk;)V

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 1196336
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 1196337
    :cond_5
    invoke-static {}, LX/4AN;->b()LX/1HI;

    move-result-object v4

    .line 1196338
    iget-object v5, v1, LX/7Kh;->b:Landroid/content/Context;

    invoke-virtual {v4, v3, v5}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v3

    .line 1196339
    new-instance v4, LX/7Kg;

    invoke-direct {v4, v1, v2, v3}, LX/7Kg;-><init>(LX/7Kh;LX/7Ki;LX/1ca;)V

    iget-object v5, v1, LX/7Kh;->c:Ljava/util/concurrent/Executor;

    invoke-interface {v3, v4, v5}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_2
.end method

.method public static h(LX/7Kk;)V
    .locals 2

    .prologue
    .line 1196340
    iget-object v0, p0, LX/7Kk;->h:Landroid/app/NotificationManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1196341
    iget-object v0, p0, LX/7Kk;->i:LX/7Kj;

    invoke-virtual {v0}, LX/7Kj;->d()V

    .line 1196342
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1196343
    invoke-direct {p0}, LX/7Kk;->f()V

    .line 1196344
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1196345
    invoke-direct {p0}, LX/7Kk;->f()V

    .line 1196346
    return-void
.end method

.method public final dK_()V
    .locals 0

    .prologue
    .line 1196347
    invoke-direct {p0}, LX/7Kk;->f()V

    .line 1196348
    return-void
.end method

.method public final dL_()V
    .locals 0

    .prologue
    .line 1196349
    return-void
.end method
