.class public final LX/7hD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I

.field private h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:Z

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/7hG;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1225793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225794
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1225795
    iput-object v0, p0, LX/7hD;->m:LX/0Px;

    .line 1225796
    iput-object p1, p0, LX/7hD;->a:Ljava/lang/String;

    .line 1225797
    iput-object p2, p0, LX/7hD;->c:Ljava/lang/String;

    .line 1225798
    iput-object p3, p0, LX/7hD;->d:Ljava/lang/String;

    .line 1225799
    iput-object p4, p0, LX/7hD;->e:Ljava/lang/String;

    .line 1225800
    iput-object p5, p0, LX/7hD;->j:Ljava/lang/String;

    .line 1225801
    iput-object p6, p0, LX/7hD;->o:Ljava/lang/String;

    .line 1225802
    return-void
.end method


# virtual methods
.method public final a(I)LX/7hD;
    .locals 0

    .prologue
    .line 1225778
    iput p1, p0, LX/7hD;->g:I

    .line 1225779
    return-object p0
.end method

.method public final a(LX/0Px;)LX/7hD;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/7hD;"
        }
    .end annotation

    .prologue
    .line 1225791
    iput-object p1, p0, LX/7hD;->k:LX/0Px;

    .line 1225792
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/7hD;
    .locals 0

    .prologue
    .line 1225789
    iput-object p1, p0, LX/7hD;->b:Ljava/lang/String;

    .line 1225790
    return-object p0
.end method

.method public final a(Z)LX/7hD;
    .locals 0

    .prologue
    .line 1225787
    iput-boolean p1, p0, LX/7hD;->l:Z

    .line 1225788
    return-object p0
.end method

.method public final a()LX/7hE;
    .locals 18

    .prologue
    .line 1225786
    new-instance v1, LX/7hE;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7hD;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7hD;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/7hD;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7hD;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/7hD;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7hD;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7hD;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v9, v0, LX/7hD;->g:I

    move-object/from16 v0, p0

    iget-object v10, v0, LX/7hD;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/7hD;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/7hD;->k:LX/0Px;

    move-object/from16 v0, p0

    iget-boolean v13, v0, LX/7hD;->l:Z

    move-object/from16 v0, p0

    iget-object v14, v0, LX/7hD;->m:LX/0Px;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/7hD;->n:LX/0Px;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7hD;->o:Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-direct/range {v1 .. v17}, LX/7hE;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0Px;ZLX/0Px;LX/0Px;Ljava/lang/String;B)V

    return-object v1
.end method

.method public final b(Ljava/lang/String;)LX/7hD;
    .locals 0

    .prologue
    .line 1225784
    iput-object p1, p0, LX/7hD;->f:Ljava/lang/String;

    .line 1225785
    return-object p0
.end method

.method public final c(LX/0Px;)LX/7hD;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;)",
            "LX/7hD;"
        }
    .end annotation

    .prologue
    .line 1225782
    iput-object p1, p0, LX/7hD;->n:LX/0Px;

    .line 1225783
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/7hD;
    .locals 0

    .prologue
    .line 1225780
    iput-object p1, p0, LX/7hD;->i:Ljava/lang/String;

    .line 1225781
    return-object p0
.end method
