.class public final LX/6qr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6qb;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/checkout/CheckoutFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V
    .locals 0

    .prologue
    .line 1151138
    iput-object p1, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1151139
    iget-object v0, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    invoke-static {v0}, Lcom/facebook/payments/checkout/CheckoutFragment;->D(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    .line 1151140
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1151135
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151136
    iget-object v0, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)V

    .line 1151137
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1151132
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151133
    iget-object v0, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Landroid/os/Parcelable;)V

    .line 1151134
    return-void
.end method

.method public final a(Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V
    .locals 2

    .prologue
    .line 1151141
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151142
    iget-object v0, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V

    .line 1151143
    return-void
.end method

.method public final a(Lcom/facebook/payments/model/PaymentsPin;)V
    .locals 2

    .prologue
    .line 1151129
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151130
    iget-object v0, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/model/PaymentsPin;)V

    .line 1151131
    return-void
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)V
    .locals 2

    .prologue
    .line 1151127
    iget-object v0, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)V

    .line 1151128
    return-void
.end method

.method public final b(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1151121
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151122
    iget-object v0, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->b(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)V

    .line 1151123
    return-void
.end method

.method public final c(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1151124
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151125
    iget-object v0, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qr;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->c(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)V

    .line 1151126
    return-void
.end method
