.class public final LX/7XR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1218209
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1218210
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1218211
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1218212
    const/4 v2, 0x0

    .line 1218213
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_9

    .line 1218214
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1218215
    :goto_1
    move v1, v2

    .line 1218216
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1218217
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1218218
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1218219
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_8

    .line 1218220
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1218221
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1218222
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_2

    if-eqz v6, :cond_2

    .line 1218223
    const-string v7, "__type__"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    const-string v7, "__typename"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1218224
    :cond_3
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v5

    goto :goto_2

    .line 1218225
    :cond_4
    const-string v7, "graphql"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1218226
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1218227
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_5

    .line 1218228
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_5

    .line 1218229
    const/4 v7, 0x0

    .line 1218230
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v8, :cond_f

    .line 1218231
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1218232
    :goto_4
    move v6, v7

    .line 1218233
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1218234
    :cond_5
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1218235
    goto :goto_2

    .line 1218236
    :cond_6
    const-string v7, "mqtt"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1218237
    invoke-static {p0, p1}, LX/7XU;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_2

    .line 1218238
    :cond_7
    const-string v7, "url_rules"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1218239
    invoke-static {p0, p1}, LX/7XU;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_2

    .line 1218240
    :cond_8
    const/4 v6, 0x4

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1218241
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 1218242
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1218243
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 1218244
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 1218245
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_1

    :cond_9
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    goto/16 :goto_2

    .line 1218246
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1218247
    :cond_b
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_e

    .line 1218248
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1218249
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1218250
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_b

    if-eqz v10, :cond_b

    .line 1218251
    const-string v11, "friendly_names"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 1218252
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_5

    .line 1218253
    :cond_c
    const-string v11, "matcher"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 1218254
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_5

    .line 1218255
    :cond_d
    const-string v11, "replacer"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1218256
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_5

    .line 1218257
    :cond_e
    const/4 v10, 0x3

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1218258
    invoke-virtual {p1, v7, v9}, LX/186;->b(II)V

    .line 1218259
    const/4 v7, 0x1

    invoke-virtual {p1, v7, v8}, LX/186;->b(II)V

    .line 1218260
    const/4 v7, 0x2

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 1218261
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_4

    :cond_f
    move v6, v7

    move v8, v7

    move v9, v7

    goto :goto_5
.end method
