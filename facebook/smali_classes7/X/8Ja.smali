.class public final enum LX/8Ja;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Ja;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Ja;

.field public static final enum ITEM_VIEW:LX/8Ja;

.field public static final enum SECTION_HEADER_VIEW:LX/8Ja;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1328790
    new-instance v0, LX/8Ja;

    const-string v1, "ITEM_VIEW"

    invoke-direct {v0, v1, v2}, LX/8Ja;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Ja;->ITEM_VIEW:LX/8Ja;

    .line 1328791
    new-instance v0, LX/8Ja;

    const-string v1, "SECTION_HEADER_VIEW"

    invoke-direct {v0, v1, v3}, LX/8Ja;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Ja;->SECTION_HEADER_VIEW:LX/8Ja;

    .line 1328792
    const/4 v0, 0x2

    new-array v0, v0, [LX/8Ja;

    sget-object v1, LX/8Ja;->ITEM_VIEW:LX/8Ja;

    aput-object v1, v0, v2

    sget-object v1, LX/8Ja;->SECTION_HEADER_VIEW:LX/8Ja;

    aput-object v1, v0, v3

    sput-object v0, LX/8Ja;->$VALUES:[LX/8Ja;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1328787
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Ja;
    .locals 1

    .prologue
    .line 1328789
    const-class v0, LX/8Ja;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Ja;

    return-object v0
.end method

.method public static values()[LX/8Ja;
    .locals 1

    .prologue
    .line 1328788
    sget-object v0, LX/8Ja;->$VALUES:[LX/8Ja;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Ja;

    return-object v0
.end method
