.class public final LX/7NT;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7Mr;


# direct methods
.method public constructor <init>(LX/7Mr;)V
    .locals 0

    .prologue
    .line 1200305
    iput-object p1, p0, LX/7NT;->a:LX/7Mr;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1200317
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1200306
    check-cast p1, LX/2ou;

    .line 1200307
    iget-object v0, p0, LX/7NT;->a:LX/7Mr;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_1

    .line 1200308
    :cond_0
    :goto_0
    return-void

    .line 1200309
    :cond_1
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-ne v0, v1, :cond_3

    .line 1200310
    iget-object v0, p0, LX/7NT;->a:LX/7Mr;

    iget v0, v0, LX/7Mr;->d:I

    if-gtz v0, :cond_2

    .line 1200311
    iget-object v0, p0, LX/7NT;->a:LX/7Mr;

    iget-object v1, p0, LX/7NT;->a:LX/7Mr;

    iget-object v1, v1, LX/2oy;->j:LX/2pb;

    invoke-virtual {v1}, LX/2pb;->k()I

    move-result v1

    iput v1, v0, LX/7Mr;->d:I

    .line 1200312
    :cond_2
    iget-object v0, p0, LX/7NT;->a:LX/7Mr;

    invoke-virtual {v0}, LX/7Mr;->i()V

    goto :goto_0

    .line 1200313
    :cond_3
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PAUSED:LX/2qV;

    if-ne v0, v1, :cond_4

    .line 1200314
    iget-object v0, p0, LX/7NT;->a:LX/7Mr;

    invoke-virtual {v0}, LX/7Mr;->x()V

    goto :goto_0

    .line 1200315
    :cond_4
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 1200316
    iget-object v0, p0, LX/7NT;->a:LX/7Mr;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/7Mr;->a$redex0(LX/7Mr;Z)V

    goto :goto_0
.end method
