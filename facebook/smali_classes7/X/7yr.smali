.class public final enum LX/7yr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7yr;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7yr;

.field public static final enum SHA256:LX/7yr;


# instance fields
.field private final mDigestInstanceString:Ljava/lang/String;

.field private final mHeaderPrefix:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1280388
    new-instance v0, LX/7yr;

    const-string v1, "SHA256"

    const-string v2, "SHA-256"

    const-string v3, "sha256"

    invoke-direct {v0, v1, v4, v2, v3}, LX/7yr;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7yr;->SHA256:LX/7yr;

    .line 1280389
    const/4 v0, 0x1

    new-array v0, v0, [LX/7yr;

    sget-object v1, LX/7yr;->SHA256:LX/7yr;

    aput-object v1, v0, v4

    sput-object v0, LX/7yr;->$VALUES:[LX/7yr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1280390
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1280391
    iput-object p3, p0, LX/7yr;->mDigestInstanceString:Ljava/lang/String;

    .line 1280392
    iput-object p4, p0, LX/7yr;->mHeaderPrefix:Ljava/lang/String;

    .line 1280393
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7yr;
    .locals 1

    .prologue
    .line 1280394
    const-class v0, LX/7yr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7yr;

    return-object v0
.end method

.method public static values()[LX/7yr;
    .locals 1

    .prologue
    .line 1280395
    sget-object v0, LX/7yr;->$VALUES:[LX/7yr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7yr;

    return-object v0
.end method


# virtual methods
.method public final getDigestInstanceString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1280396
    iget-object v0, p0, LX/7yr;->mDigestInstanceString:Ljava/lang/String;

    return-object v0
.end method

.method public final getHeaderPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1280397
    iget-object v0, p0, LX/7yr;->mHeaderPrefix:Ljava/lang/String;

    return-object v0
.end method
