.class public final LX/6zs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wI",
        "<",
        "Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;",
        "LX/708;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1161575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161576
    iput-object p1, p0, LX/6zs;->a:Landroid/content/Context;

    .line 1161577
    return-void
.end method

.method public static a(LX/6zs;Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;
    .locals 4
    .param p1    # Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1161552
    iget-object v0, p1, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-static {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a(Ljava/lang/String;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6xw;

    move-result-object v1

    if-nez p2, :cond_0

    sget-object v0, LX/6xZ;->ADD_CARD:LX/6xZ;

    .line 1161553
    :goto_0
    iput-object v0, v1, LX/6xw;->c:LX/6xZ;

    .line 1161554
    move-object v0, v1

    .line 1161555
    invoke-virtual {v0}, LX/6xw;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    move-result-object v1

    .line 1161556
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v0

    iget-object v2, p1, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iget-object v2, v2, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0, v2}, LX/6wu;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6wu;

    move-result-object v2

    if-nez p2, :cond_1

    sget-object v0, LX/6ws;->MODAL_BOTTOM:LX/6ws;

    .line 1161557
    :goto_1
    iput-object v0, v2, LX/6wu;->a:LX/6ws;

    .line 1161558
    move-object v0, v2

    .line 1161559
    invoke-virtual {v0}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v0

    .line 1161560
    invoke-static {}, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->newBuilder()LX/6yR;

    move-result-object v2

    .line 1161561
    iput-object v0, v2, LX/6yR;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1161562
    move-object v0, v2

    .line 1161563
    invoke-virtual {v0}, LX/6yR;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    move-result-object v0

    .line 1161564
    sget-object v2, LX/6yO;->SIMPLE:LX/6yO;

    iget-object v3, p1, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    invoke-static {v2, v1, v3}, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a(LX/6yO;Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;LX/6xg;)LX/6xy;

    move-result-object v1

    .line 1161565
    iput-object p2, v1, LX/6xy;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    .line 1161566
    move-object v1, v1

    .line 1161567
    iput-object v0, v1, LX/6xy;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    .line 1161568
    move-object v0, v1

    .line 1161569
    iput-object p3, v0, LX/6xy;->g:Lcom/facebook/common/locale/Country;

    .line 1161570
    move-object v0, v0

    .line 1161571
    invoke-virtual {v0}, LX/6xy;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    .line 1161572
    iget-object v1, p0, LX/6zs;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 1161573
    :cond_0
    sget-object v0, LX/6xZ;->UPDATE_CARD:LX/6xZ;

    goto :goto_0

    .line 1161574
    :cond_1
    sget-object v0, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    goto :goto_1
.end method

.method private a(LX/0Pz;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;LX/708;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/6vm;",
            ">;",
            "Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;",
            "LX/708;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1161578
    sget-object v0, LX/6zr;->a:[I

    invoke-virtual {p3}, LX/708;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1161579
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled section type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1161580
    :pswitch_0
    iget-object v0, p2, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    move-object v0, v0

    .line 1161581
    check-cast v0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;

    .line 1161582
    iget-object v1, p2, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v1, v1

    .line 1161583
    check-cast v1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1161584
    iget-object v2, v1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->b:Lcom/facebook/common/locale/Country;

    move-object v1, v2

    .line 1161585
    const/4 v2, 0x1

    .line 1161586
    new-instance v3, LX/700;

    invoke-direct {v3, v0, v1, v2}, LX/700;-><init>(Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;Lcom/facebook/common/locale/Country;Z)V

    invoke-virtual {p1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1161587
    :goto_0
    return-void

    .line 1161588
    :pswitch_1
    iget-object v0, p2, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 1161589
    check-cast v0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;

    iget-object v2, v0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    invoke-virtual {p2}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->b:LX/0Px;

    invoke-virtual {p2}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v4

    sget-object v0, LX/708;->SELECT_PAYMENT_METHOD:LX/708;

    invoke-virtual {p2, v0}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a(LX/6vZ;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    .line 1161590
    iget-object v6, v2, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    move-object v8, v6

    .line 1161591
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v6, 0x0

    move v7, v6

    :goto_1
    if-ge v7, v9, :cond_2

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1161592
    invoke-interface {v6}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->b()LX/6zU;

    move-result-object p0

    invoke-virtual {p0}, LX/6zU;->toNewPaymentOptionType()LX/6zQ;

    move-result-object p0

    .line 1161593
    invoke-virtual {v3, p0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    .line 1161594
    invoke-static {}, LX/701;->newBuilder()LX/702;

    move-result-object p0

    .line 1161595
    iput-object v6, p0, LX/702;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1161596
    move-object p0, p0

    .line 1161597
    invoke-interface {v6}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    .line 1161598
    iput-boolean p1, p0, LX/702;->b:Z

    .line 1161599
    move-object p0, p0

    .line 1161600
    iget-object p1, v4, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iget-object p1, p1, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1161601
    iput-object p1, p0, LX/702;->e:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1161602
    move-object p0, p0

    .line 1161603
    sget-object p1, LX/6zr;->b:[I

    invoke-interface {v6}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->b()LX/6zU;

    move-result-object p2

    invoke-virtual {p2}, LX/6zU;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_1

    .line 1161604
    :cond_0
    :goto_2
    invoke-virtual {p0}, LX/702;->f()LX/701;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1161605
    :cond_1
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1

    .line 1161606
    :pswitch_2
    check-cast v6, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    .line 1161607
    iget-object p1, v6, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->c:LX/6zT;

    sget-object p2, LX/6zT;->MIB:LX/6zT;

    if-ne p1, p2, :cond_0

    .line 1161608
    iget-object p1, v4, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iget-object p1, p1, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1161609
    new-instance p2, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;

    invoke-direct {p2, v6}, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;-><init>(Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;)V

    invoke-static {p2, p1}, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->a(Lcom/facebook/payments/consent/model/ConsentExtraData;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6v0;

    move-result-object p2

    invoke-virtual {p2}, LX/6v0;->a()Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    move-result-object p2

    .line 1161610
    iget-object p3, v0, LX/6zs;->a:Landroid/content/Context;

    invoke-static {p3, p2}, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;)Landroid/content/Intent;

    move-result-object p2

    move-object v6, p2

    .line 1161611
    iput-object v6, p0, LX/702;->c:Landroid/content/Intent;

    .line 1161612
    move-object v6, p0

    .line 1161613
    const/4 p1, 0x3

    .line 1161614
    iput p1, v6, LX/702;->d:I

    .line 1161615
    goto :goto_2

    .line 1161616
    :pswitch_3
    check-cast v6, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    .line 1161617
    invoke-virtual {v2}, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f()Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;

    move-result-object p1

    .line 1161618
    invoke-virtual {v6, p1}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a(Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1161619
    iget-object p1, v2, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->b:Lcom/facebook/common/locale/Country;

    move-object p1, p1

    .line 1161620
    invoke-static {v0, v4, v6, p1}, LX/6zs;->a(LX/6zs;Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;

    move-result-object v6

    .line 1161621
    iput-object v6, p0, LX/702;->c:Landroid/content/Intent;

    .line 1161622
    move-object v6, p0

    .line 1161623
    const/4 p1, 0x4

    .line 1161624
    iput p1, v6, LX/702;->d:I

    .line 1161625
    goto :goto_2

    .line 1161626
    :cond_2
    goto/16 :goto_0

    .line 1161627
    :pswitch_4
    invoke-static {p1}, LX/714;->a(LX/0Pz;)V

    goto/16 :goto_0

    .line 1161628
    :pswitch_5
    iget-object v0, p2, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 1161629
    check-cast v0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;

    iget-object v4, v0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1161630
    invoke-virtual {p2}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    .line 1161631
    iget-object v1, v4, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f:LX/0Px;

    move-object v5, v1

    .line 1161632
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v3, v1

    :goto_3
    if-ge v3, v6, :cond_4

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;

    .line 1161633
    iget-object v2, v0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->b:LX/0Px;

    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;->d()LX/6zQ;

    move-result-object v7

    invoke-virtual {v2, v7}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1161634
    invoke-virtual {p2}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    invoke-virtual {v2}, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v2

    invoke-virtual {p0, v1, p1, v4, v2}, LX/6zs;->a(Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;LX/0Pz;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;)V

    .line 1161635
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 1161636
    :cond_4
    goto/16 :goto_0

    .line 1161637
    :pswitch_6
    new-instance v0, LX/71G;

    iget-object v1, p0, LX/6zs;->a:Landroid/content/Context;

    const v2, 0x7f080c75

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/71F;->LEARN_MORE_AND_TERMS:LX/71F;

    invoke-direct {v0, v1, v2}, LX/71G;-><init>(Ljava/lang/String;LX/71F;)V

    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1161638
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/6zs;
    .locals 2

    .prologue
    .line 1161535
    new-instance v1, LX/6zs;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/6zs;-><init>(Landroid/content/Context;)V

    .line 1161536
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;LX/0Px;)LX/0Px;
    .locals 4

    .prologue
    .line 1161546
    check-cast p1, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;

    .line 1161547
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1161548
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/708;

    .line 1161549
    invoke-direct {p0, v2, p1, v0}, LX/6zs;->a(LX/0Pz;Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerRunTimeData;LX/708;)V

    .line 1161550
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1161551
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;LX/0Pz;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;",
            "LX/0Pz",
            "<",
            "LX/6vm;",
            ">;",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
            "Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1161537
    sget-object v0, LX/6zr;->c:[I

    invoke-virtual {p1}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;->d()LX/6zQ;

    move-result-object v1

    invoke-virtual {v1}, LX/6zQ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1161538
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;->d()LX/6zQ;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not to add a Payment method"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1161539
    :pswitch_0
    const/4 v0, 0x0

    .line 1161540
    iget-object v1, p3, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->b:Lcom/facebook/common/locale/Country;

    move-object v1, v1

    .line 1161541
    invoke-static {p0, p4, v0, v1}, LX/6zs;->a(LX/6zs;Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;Lcom/facebook/common/locale/Country;)Landroid/content/Intent;

    move-result-object v0

    .line 1161542
    new-instance v1, LX/6zy;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, LX/6zy;-><init>(Landroid/content/Intent;I)V

    invoke-virtual {p2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1161543
    :goto_0
    :pswitch_1
    return-void

    .line 1161544
    :pswitch_2
    check-cast p1, Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;

    .line 1161545
    new-instance v1, LX/6zz;

    iget-object v0, p1, Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v2, 0x2

    invoke-direct {v1, v0, v2}, LX/6zz;-><init>(Landroid/net/Uri;I)V

    invoke-virtual {p2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
