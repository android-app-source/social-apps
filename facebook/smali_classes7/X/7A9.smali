.class public final LX/7A9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 52

    .prologue
    .line 1175759
    const/16 v46, 0x0

    .line 1175760
    const/16 v45, 0x0

    .line 1175761
    const/16 v44, 0x0

    .line 1175762
    const/16 v43, 0x0

    .line 1175763
    const/16 v42, 0x0

    .line 1175764
    const-wide/16 v40, 0x0

    .line 1175765
    const/16 v39, 0x0

    .line 1175766
    const/16 v38, 0x0

    .line 1175767
    const/16 v37, 0x0

    .line 1175768
    const/16 v36, 0x0

    .line 1175769
    const/16 v35, 0x0

    .line 1175770
    const/16 v34, 0x0

    .line 1175771
    const/16 v33, 0x0

    .line 1175772
    const/16 v32, 0x0

    .line 1175773
    const/16 v29, 0x0

    .line 1175774
    const-wide/16 v30, 0x0

    .line 1175775
    const/16 v28, 0x0

    .line 1175776
    const/16 v27, 0x0

    .line 1175777
    const/16 v26, 0x0

    .line 1175778
    const-wide/16 v24, 0x0

    .line 1175779
    const-wide/16 v22, 0x0

    .line 1175780
    const/16 v21, 0x0

    .line 1175781
    const/16 v20, 0x0

    .line 1175782
    const/16 v19, 0x0

    .line 1175783
    const/16 v18, 0x0

    .line 1175784
    const/16 v17, 0x0

    .line 1175785
    const/16 v16, 0x0

    .line 1175786
    const/4 v15, 0x0

    .line 1175787
    const/4 v14, 0x0

    .line 1175788
    const/4 v13, 0x0

    .line 1175789
    const/4 v12, 0x0

    .line 1175790
    const/4 v11, 0x0

    .line 1175791
    const/4 v10, 0x0

    .line 1175792
    const/4 v9, 0x0

    .line 1175793
    const/4 v8, 0x0

    .line 1175794
    const/4 v7, 0x0

    .line 1175795
    const/4 v6, 0x0

    .line 1175796
    const/4 v5, 0x0

    .line 1175797
    const/4 v4, 0x0

    .line 1175798
    const/4 v3, 0x0

    .line 1175799
    const/4 v2, 0x0

    .line 1175800
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v47

    sget-object v48, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v47

    move-object/from16 v1, v48

    if-eq v0, v1, :cond_2c

    .line 1175801
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1175802
    const/4 v2, 0x0

    .line 1175803
    :goto_0
    return v2

    .line 1175804
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v48, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v48

    if-eq v2, v0, :cond_1d

    .line 1175805
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1175806
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1175807
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v48

    sget-object v49, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v48

    move-object/from16 v1, v49

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 1175808
    const-string v48, "__type__"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-nez v48, :cond_1

    const-string v48, "__typename"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_2

    .line 1175809
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move/from16 v47, v2

    goto :goto_1

    .line 1175810
    :cond_2
    const-string v48, "can_viewer_rate"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_3

    .line 1175811
    const/4 v2, 0x1

    .line 1175812
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    move/from16 v46, v18

    move/from16 v18, v2

    goto :goto_1

    .line 1175813
    :cond_3
    const-string v48, "can_viewer_share"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_4

    .line 1175814
    const/4 v2, 0x1

    .line 1175815
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v45, v7

    move v7, v2

    goto :goto_1

    .line 1175816
    :cond_4
    const-string v48, "enable_focus"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_5

    .line 1175817
    const/4 v2, 0x1

    .line 1175818
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v44, v6

    move v6, v2

    goto :goto_1

    .line 1175819
    :cond_5
    const-string v48, "event_viewer_capability"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_6

    .line 1175820
    invoke-static/range {p0 .. p1}, LX/7AH;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v43, v2

    goto/16 :goto_1

    .line 1175821
    :cond_6
    const-string v48, "focus_width_degrees"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_7

    .line 1175822
    const/4 v2, 0x1

    .line 1175823
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 1175824
    :cond_7
    const-string v48, "global_share"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_8

    .line 1175825
    invoke-static/range {p0 .. p1}, LX/7AI;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v42, v2

    goto/16 :goto_1

    .line 1175826
    :cond_8
    const-string v48, "guided_tour"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_9

    .line 1175827
    invoke-static/range {p0 .. p1}, LX/5CB;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v41, v2

    goto/16 :goto_1

    .line 1175828
    :cond_9
    const-string v48, "id"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_a

    .line 1175829
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v40, v2

    goto/16 :goto_1

    .line 1175830
    :cond_a
    const-string v48, "initial_view_heading_degrees"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_b

    .line 1175831
    const/4 v2, 0x1

    .line 1175832
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v17

    move/from16 v39, v17

    move/from16 v17, v2

    goto/16 :goto_1

    .line 1175833
    :cond_b
    const-string v48, "initial_view_pitch_degrees"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_c

    .line 1175834
    const/4 v2, 0x1

    .line 1175835
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    move/from16 v38, v16

    move/from16 v16, v2

    goto/16 :goto_1

    .line 1175836
    :cond_c
    const-string v48, "initial_view_roll_degrees"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_d

    .line 1175837
    const/4 v2, 0x1

    .line 1175838
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    move/from16 v37, v15

    move v15, v2

    goto/16 :goto_1

    .line 1175839
    :cond_d
    const-string v48, "is_playable"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_e

    .line 1175840
    const/4 v2, 0x1

    .line 1175841
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    move/from16 v36, v14

    move v14, v2

    goto/16 :goto_1

    .line 1175842
    :cond_e
    const-string v48, "is_spherical"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_f

    .line 1175843
    const/4 v2, 0x1

    .line 1175844
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    move/from16 v35, v13

    move v13, v2

    goto/16 :goto_1

    .line 1175845
    :cond_f
    const-string v48, "name"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_10

    .line 1175846
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v34, v2

    goto/16 :goto_1

    .line 1175847
    :cond_10
    const-string v48, "off_focus_level"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_11

    .line 1175848
    const/4 v2, 0x1

    .line 1175849
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v32

    move v12, v2

    goto/16 :goto_1

    .line 1175850
    :cond_11
    const-string v48, "playable_url"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_12

    .line 1175851
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 1175852
    :cond_12
    const-string v48, "projection_type"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_13

    .line 1175853
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 1175854
    :cond_13
    const-string v48, "should_open_single_publisher"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_14

    .line 1175855
    const/4 v2, 0x1

    .line 1175856
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    move/from16 v25, v11

    move v11, v2

    goto/16 :goto_1

    .line 1175857
    :cond_14
    const-string v48, "sphericalFullscreenAspectRatio"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_15

    .line 1175858
    const/4 v2, 0x1

    .line 1175859
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v28

    move v10, v2

    goto/16 :goto_1

    .line 1175860
    :cond_15
    const-string v48, "sphericalInlineAspectRatio"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_16

    .line 1175861
    const/4 v2, 0x1

    .line 1175862
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v26

    move v9, v2

    goto/16 :goto_1

    .line 1175863
    :cond_16
    const-string v48, "sphericalPlayableUrlHdString"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_17

    .line 1175864
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 1175865
    :cond_17
    const-string v48, "sphericalPlayableUrlSdString"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_18

    .line 1175866
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 1175867
    :cond_18
    const-string v48, "sphericalPreferredFov"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_19

    .line 1175868
    const/4 v2, 0x1

    .line 1175869
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    move/from16 v22, v8

    move v8, v2

    goto/16 :goto_1

    .line 1175870
    :cond_19
    const-string v48, "video_channel"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_1a

    .line 1175871
    invoke-static/range {p0 .. p1}, LX/5CD;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 1175872
    :cond_1a
    const-string v48, "viewer_recommendation"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_1b

    .line 1175873
    invoke-static/range {p0 .. p1}, LX/7AJ;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 1175874
    :cond_1b
    const-string v48, "viewer_saved_state"

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 1175875
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLSavedState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 1175876
    :cond_1c
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1175877
    :cond_1d
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1175878
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1175879
    if-eqz v18, :cond_1e

    .line 1175880
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1175881
    :cond_1e
    if-eqz v7, :cond_1f

    .line 1175882
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1175883
    :cond_1f
    if-eqz v6, :cond_20

    .line 1175884
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1175885
    :cond_20
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1175886
    if-eqz v3, :cond_21

    .line 1175887
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1175888
    :cond_21
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1175889
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1175890
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1175891
    if-eqz v17, :cond_22

    .line 1175892
    const/16 v2, 0x9

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1175893
    :cond_22
    if-eqz v16, :cond_23

    .line 1175894
    const/16 v2, 0xa

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1175895
    :cond_23
    if-eqz v15, :cond_24

    .line 1175896
    const/16 v2, 0xb

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1175897
    :cond_24
    if-eqz v14, :cond_25

    .line 1175898
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1175899
    :cond_25
    if-eqz v13, :cond_26

    .line 1175900
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1175901
    :cond_26
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1175902
    if-eqz v12, :cond_27

    .line 1175903
    const/16 v3, 0xf

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v32

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1175904
    :cond_27
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1175905
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1175906
    if-eqz v11, :cond_28

    .line 1175907
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1175908
    :cond_28
    if-eqz v10, :cond_29

    .line 1175909
    const/16 v3, 0x13

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v28

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1175910
    :cond_29
    if-eqz v9, :cond_2a

    .line 1175911
    const/16 v3, 0x14

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v26

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1175912
    :cond_2a
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1175913
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1175914
    if-eqz v8, :cond_2b

    .line 1175915
    const/16 v2, 0x17

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1175916
    :cond_2b
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1175917
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1175918
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1175919
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_2c
    move/from16 v47, v46

    move/from16 v46, v45

    move/from16 v45, v44

    move/from16 v44, v43

    move/from16 v43, v42

    move/from16 v42, v39

    move/from16 v39, v36

    move/from16 v36, v33

    move/from16 v50, v10

    move v10, v4

    move/from16 v51, v11

    move v11, v5

    move-wide/from16 v4, v40

    move/from16 v40, v37

    move/from16 v41, v38

    move/from16 v37, v34

    move/from16 v38, v35

    move/from16 v34, v29

    move/from16 v35, v32

    move-wide/from16 v32, v30

    move/from16 v30, v27

    move/from16 v31, v28

    move-wide/from16 v28, v24

    move/from16 v25, v26

    move/from16 v24, v21

    move-wide/from16 v26, v22

    move/from16 v21, v18

    move/from16 v22, v19

    move/from16 v23, v20

    move/from16 v18, v15

    move v15, v9

    move/from16 v20, v17

    move/from16 v19, v16

    move/from16 v17, v51

    move/from16 v16, v50

    move v9, v3

    move v3, v12

    move v12, v6

    move v6, v13

    move v13, v7

    move v7, v14

    move v14, v8

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x1a

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 1175920
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1175921
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1175922
    if-eqz v0, :cond_0

    .line 1175923
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175924
    invoke-static {p0, p1, v3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1175925
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1175926
    if-eqz v0, :cond_1

    .line 1175927
    const-string v1, "can_viewer_rate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175928
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1175929
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1175930
    if-eqz v0, :cond_2

    .line 1175931
    const-string v1, "can_viewer_share"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175932
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1175933
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1175934
    if-eqz v0, :cond_3

    .line 1175935
    const-string v1, "enable_focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175936
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1175937
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1175938
    if-eqz v0, :cond_4

    .line 1175939
    const-string v1, "event_viewer_capability"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175940
    invoke-static {p0, v0, p2}, LX/7AH;->a(LX/15i;ILX/0nX;)V

    .line 1175941
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1175942
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_5

    .line 1175943
    const-string v2, "focus_width_degrees"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175944
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1175945
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1175946
    if-eqz v0, :cond_6

    .line 1175947
    const-string v1, "global_share"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175948
    invoke-static {p0, v0, p2}, LX/7AI;->a(LX/15i;ILX/0nX;)V

    .line 1175949
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1175950
    if-eqz v0, :cond_7

    .line 1175951
    const-string v1, "guided_tour"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175952
    invoke-static {p0, v0, p2, p3}, LX/5CB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1175953
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1175954
    if-eqz v0, :cond_8

    .line 1175955
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175956
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1175957
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1175958
    if-eqz v0, :cond_9

    .line 1175959
    const-string v1, "initial_view_heading_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175960
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1175961
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1175962
    if-eqz v0, :cond_a

    .line 1175963
    const-string v1, "initial_view_pitch_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175964
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1175965
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1175966
    if-eqz v0, :cond_b

    .line 1175967
    const-string v1, "initial_view_roll_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175968
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1175969
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1175970
    if-eqz v0, :cond_c

    .line 1175971
    const-string v1, "is_playable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175972
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1175973
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1175974
    if-eqz v0, :cond_d

    .line 1175975
    const-string v1, "is_spherical"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175976
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1175977
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1175978
    if-eqz v0, :cond_e

    .line 1175979
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175980
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1175981
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1175982
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_f

    .line 1175983
    const-string v2, "off_focus_level"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175984
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1175985
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1175986
    if-eqz v0, :cond_10

    .line 1175987
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175988
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1175989
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1175990
    if-eqz v0, :cond_11

    .line 1175991
    const-string v1, "projection_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175992
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1175993
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1175994
    if-eqz v0, :cond_12

    .line 1175995
    const-string v1, "should_open_single_publisher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175996
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1175997
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1175998
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_13

    .line 1175999
    const-string v2, "sphericalFullscreenAspectRatio"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176000
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1176001
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1176002
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_14

    .line 1176003
    const-string v2, "sphericalInlineAspectRatio"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176004
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1176005
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1176006
    if-eqz v0, :cond_15

    .line 1176007
    const-string v1, "sphericalPlayableUrlHdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176008
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1176009
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1176010
    if-eqz v0, :cond_16

    .line 1176011
    const-string v1, "sphericalPlayableUrlSdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176012
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1176013
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1176014
    if-eqz v0, :cond_17

    .line 1176015
    const-string v1, "sphericalPreferredFov"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176016
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1176017
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1176018
    if-eqz v0, :cond_18

    .line 1176019
    const-string v1, "video_channel"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176020
    invoke-static {p0, v0, p2}, LX/5CD;->a(LX/15i;ILX/0nX;)V

    .line 1176021
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1176022
    if-eqz v0, :cond_19

    .line 1176023
    const-string v1, "viewer_recommendation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176024
    invoke-static {p0, v0, p2}, LX/7AJ;->a(LX/15i;ILX/0nX;)V

    .line 1176025
    :cond_19
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1176026
    if-eqz v0, :cond_1a

    .line 1176027
    const-string v0, "viewer_saved_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1176028
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1176029
    :cond_1a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1176030
    return-void
.end method
