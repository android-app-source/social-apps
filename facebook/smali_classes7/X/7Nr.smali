.class public final LX/7Nr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V
    .locals 0

    .prologue
    .line 1200746
    iput-object p1, p0, LX/7Nr;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x6ca80160

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1200747
    iget-object v1, p0, LX/7Nr;->a:Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    .line 1200748
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->F:Lcom/facebook/resources/ui/FbButton;

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1200749
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->R:LX/7Ny;

    sget-object p0, LX/7Ny;->DISCONNECTED:LX/7Ny;

    if-ne v3, p0, :cond_0

    .line 1200750
    invoke-static {v1}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->x(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V

    .line 1200751
    :goto_0
    const v1, -0x1d629afe

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1200752
    :cond_0
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->r:LX/7J3;

    const-string p0, "disconnect"

    sget-object p1, LX/38a;->DISCONNECTED:LX/38a;

    invoke-virtual {v3, p0, p1}, LX/7J3;->a(Ljava/lang/String;LX/38a;)V

    .line 1200753
    iget-object v3, v1, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v3}, LX/37Y;->e()V

    goto :goto_0
.end method
