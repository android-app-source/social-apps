.class public LX/7vy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6uU;


# instance fields
.field private final a:LX/6uf;


# direct methods
.method public constructor <init>(LX/6uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1275290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275291
    iput-object p1, p0, LX/7vy;->a:LX/6uf;

    .line 1275292
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;LX/6uT;)LX/6E8;
    .locals 2

    .prologue
    .line 1275293
    sget-object v0, LX/7vx;->a:[I

    invoke-virtual {p2}, LX/6uT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1275294
    iget-object v0, p0, LX/7vy;->a:LX/6uf;

    invoke-virtual {v0, p1, p2}, LX/6uf;->a(Landroid/view/ViewGroup;LX/6uT;)LX/6E8;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1275295
    :pswitch_0
    new-instance v1, LX/7w3;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const p0, 0x7f031632

    const/4 p2, 0x0

    invoke-virtual {v0, p0, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;

    invoke-direct {v1, v0}, LX/7w3;-><init>(Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;)V

    move-object v0, v1

    .line 1275296
    goto :goto_0

    .line 1275297
    :pswitch_1
    new-instance v1, LX/6uX;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const p0, 0x7f03165e

    const/4 p2, 0x0

    invoke-virtual {v0, p0, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;

    invoke-direct {v1, v0}, LX/6uX;-><init>(Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;)V

    move-object v0, v1

    .line 1275298
    goto :goto_0

    .line 1275299
    :pswitch_2
    new-instance v1, LX/7w7;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const p0, 0x7f031633

    const/4 p2, 0x0

    invoke-virtual {v0, p0, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;

    invoke-direct {v1, v0}, LX/7w7;-><init>(Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;)V

    move-object v0, v1

    .line 1275300
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
