.class public final LX/7R9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1205553
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1205554
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1205555
    :goto_0
    return v1

    .line 1205556
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1205557
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 1205558
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1205559
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1205560
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1205561
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1205562
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 1205563
    :cond_3
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1205564
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1205565
    :cond_4
    const-string v5, "video_channel_creator_info"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1205566
    const/4 v4, 0x0

    .line 1205567
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_a

    .line 1205568
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1205569
    :goto_2
    move v0, v4

    .line 1205570
    goto :goto_1

    .line 1205571
    :cond_5
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1205572
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1205573
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1205574
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1205575
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 1205576
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1205577
    :cond_8
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_9

    .line 1205578
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1205579
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1205580
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_8

    if-eqz v5, :cond_8

    .line 1205581
    const-string v6, "creator_status"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1205582
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_3

    .line 1205583
    :cond_9
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1205584
    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1205585
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_a
    move v0, v4

    goto :goto_3
.end method
