.class public LX/6lh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            "LX/0Ot",
            "<",
            "LX/6lX;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/6lX;


# direct methods
.method public constructor <init>(Ljava/util/Set;LX/6lX;)V
    .locals 5
    .param p2    # LX/6lX;
        .annotation runtime Lcom/facebook/messaging/xma/FallbackSnippetCreator;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/6li;",
            ">;",
            "LX/6lX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1142864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142865
    iput-object p2, p0, LX/6lh;->b:LX/6lX;

    .line 1142866
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1142867
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6li;

    .line 1142868
    iget-boolean v3, v0, LX/6li;->d:Z

    if-nez v3, :cond_0

    iget-object v3, v0, LX/6li;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-static {}, LX/6lb;->a()LX/6lb;

    move-result-object v4

    if-eq v3, v4, :cond_0

    .line 1142869
    iget-object v3, v0, LX/6li;->a:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    iget-object v0, v0, LX/6li;->c:LX/0Ot;

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1142870
    :cond_1
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/6lh;->a:LX/0P1;

    .line 1142871
    return-void
.end method


# virtual methods
.method public final a(LX/6lg;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1142872
    iget-object v0, p1, LX/6lg;->b:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->c()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    .line 1142873
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->j()LX/0Px;

    move-result-object v0

    .line 1142874
    if-nez v0, :cond_0

    .line 1142875
    iget-object v0, p0, LX/6lh;->b:LX/6lX;

    invoke-interface {v0, p1}, LX/6lX;->a(LX/6lg;)Ljava/lang/String;

    move-result-object v0

    .line 1142876
    :goto_0
    return-object v0

    .line 1142877
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 1142878
    iget-object v2, p0, LX/6lh;->a:LX/0P1;

    invoke-virtual {v2, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1142879
    iget-object v1, p0, LX/6lh;->a:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6lX;

    invoke-interface {v0, p1}, LX/6lX;->a(LX/6lg;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1142880
    :cond_2
    iget-object v0, p0, LX/6lh;->b:LX/6lX;

    invoke-interface {v0, p1}, LX/6lX;->a(LX/6lg;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
