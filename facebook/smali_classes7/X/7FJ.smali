.class public final enum LX/7FJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7FJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7FJ;

.field public static final enum COMPLETE:LX/7FJ;

.field public static final enum IMPRESSION:LX/7FJ;

.field public static final enum INVITATION_IMPRESSION:LX/7FJ;

.field public static final enum INVITATION_OPENED:LX/7FJ;

.field public static final enum SKIP:LX/7FJ;

.field public static final enum START:LX/7FJ;


# instance fields
.field private final mImpressionEvent:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1186104
    new-instance v0, LX/7FJ;

    const-string v1, "INVITATION_IMPRESSION"

    const-string v2, "invitation_impression"

    invoke-direct {v0, v1, v4, v2}, LX/7FJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7FJ;->INVITATION_IMPRESSION:LX/7FJ;

    .line 1186105
    new-instance v0, LX/7FJ;

    const-string v1, "INVITATION_OPENED"

    const-string v2, "invitation_opened"

    invoke-direct {v0, v1, v5, v2}, LX/7FJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7FJ;->INVITATION_OPENED:LX/7FJ;

    .line 1186106
    new-instance v0, LX/7FJ;

    const-string v1, "IMPRESSION"

    const-string v2, "impression"

    invoke-direct {v0, v1, v6, v2}, LX/7FJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7FJ;->IMPRESSION:LX/7FJ;

    .line 1186107
    new-instance v0, LX/7FJ;

    const-string v1, "START"

    const-string v2, "start"

    invoke-direct {v0, v1, v7, v2}, LX/7FJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7FJ;->START:LX/7FJ;

    .line 1186108
    new-instance v0, LX/7FJ;

    const-string v1, "COMPLETE"

    const-string v2, "completion"

    invoke-direct {v0, v1, v8, v2}, LX/7FJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7FJ;->COMPLETE:LX/7FJ;

    .line 1186109
    new-instance v0, LX/7FJ;

    const-string v1, "SKIP"

    const/4 v2, 0x5

    const-string v3, "skip"

    invoke-direct {v0, v1, v2, v3}, LX/7FJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7FJ;->SKIP:LX/7FJ;

    .line 1186110
    const/4 v0, 0x6

    new-array v0, v0, [LX/7FJ;

    sget-object v1, LX/7FJ;->INVITATION_IMPRESSION:LX/7FJ;

    aput-object v1, v0, v4

    sget-object v1, LX/7FJ;->INVITATION_OPENED:LX/7FJ;

    aput-object v1, v0, v5

    sget-object v1, LX/7FJ;->IMPRESSION:LX/7FJ;

    aput-object v1, v0, v6

    sget-object v1, LX/7FJ;->START:LX/7FJ;

    aput-object v1, v0, v7

    sget-object v1, LX/7FJ;->COMPLETE:LX/7FJ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7FJ;->SKIP:LX/7FJ;

    aput-object v2, v0, v1

    sput-object v0, LX/7FJ;->$VALUES:[LX/7FJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1186111
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1186112
    iput-object p3, p0, LX/7FJ;->mImpressionEvent:Ljava/lang/String;

    .line 1186113
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7FJ;
    .locals 1

    .prologue
    .line 1186114
    const-class v0, LX/7FJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7FJ;

    return-object v0
.end method

.method public static values()[LX/7FJ;
    .locals 1

    .prologue
    .line 1186115
    sget-object v0, LX/7FJ;->$VALUES:[LX/7FJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7FJ;

    return-object v0
.end method


# virtual methods
.method public final getImpressionEvent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1186116
    iget-object v0, p0, LX/7FJ;->mImpressionEvent:Ljava/lang/String;

    return-object v0
.end method
