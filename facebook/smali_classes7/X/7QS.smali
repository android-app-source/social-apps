.class public final LX/7QS;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2q9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Looper;LX/2q9;)V
    .locals 1

    .prologue
    .line 1204265
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1204266
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/7QS;->a:Ljava/lang/ref/WeakReference;

    .line 1204267
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    .line 1204226
    iget v0, p1, Landroid/os/Message;->what:I

    .line 1204227
    packed-switch v0, :pswitch_data_0

    .line 1204228
    :cond_0
    :goto_0
    return-void

    .line 1204229
    :pswitch_0
    iget-object v0, p0, LX/7QS;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1204230
    iget-object v0, p0, LX/7QS;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2q9;

    .line 1204231
    iget-object v1, v0, LX/2q9;->b:LX/7QP;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/2q9;->b:LX/7QP;

    invoke-virtual {v1}, LX/7QP;->b()Z

    move-result v1

    if-nez v1, :cond_1

    iget v1, v0, LX/2q9;->g:I

    iget-object v2, v0, LX/2q9;->b:LX/7QP;

    invoke-virtual {v2}, LX/7QP;->a()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget v1, v0, LX/2q9;->g:I

    if-gez v1, :cond_2

    .line 1204232
    :cond_1
    :goto_1
    goto :goto_0

    .line 1204233
    :cond_2
    iget-object v1, v0, LX/2q9;->b:LX/7QP;

    iget v2, v0, LX/2q9;->g:I

    invoke-virtual {v1, v2}, LX/7QP;->a(I)LX/7QR;

    move-result-object v1

    .line 1204234
    iget-object v2, v0, LX/2q9;->h:LX/2pw;

    invoke-interface {v2}, LX/2pw;->a()I

    move-result v2

    .line 1204235
    const/16 v3, 0xa

    .line 1204236
    add-int v4, v2, v3

    iget v5, v1, LX/7QR;->a:I

    if-ge v4, v5, :cond_6

    .line 1204237
    sget-object v4, LX/7QQ;->EARLY:LX/7QQ;

    .line 1204238
    :goto_2
    move-object v3, v4

    .line 1204239
    sget-object v4, LX/7QQ;->IN_RANGE:LX/7QQ;

    if-ne v3, v4, :cond_3

    .line 1204240
    iget-object v3, v0, LX/2q9;->a:LX/2qI;

    new-instance v4, LX/7QO;

    const/4 v5, 0x0

    .line 1204241
    iget-object v6, v1, LX/7QR;->c:Ljava/lang/String;

    move-object v6, v6

    .line 1204242
    iget v7, v1, LX/7QR;->b:I

    move v7, v7

    .line 1204243
    iget v8, v1, LX/7QR;->a:I

    move v1, v8

    .line 1204244
    sub-int v1, v7, v1

    int-to-long v7, v1

    invoke-direct {v4, v5, v6, v7, v8}, LX/7QO;-><init>(Landroid/graphics/Rect;Ljava/lang/String;J)V

    invoke-interface {v3, v4}, LX/2qI;->a(LX/7QO;)V

    .line 1204245
    iget v1, v0, LX/2q9;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/2q9;->g:I

    .line 1204246
    invoke-static {v0, v2}, LX/2q9;->b(LX/2q9;I)V

    goto :goto_1

    .line 1204247
    :cond_3
    sget-object v1, LX/7QQ;->EARLY:LX/7QQ;

    if-ne v3, v1, :cond_4

    .line 1204248
    iget-boolean v1, v0, LX/2q9;->i:Z

    if-nez v1, :cond_1

    .line 1204249
    invoke-static {v0, v2}, LX/2q9;->b(LX/2q9;I)V

    goto :goto_1

    .line 1204250
    :cond_4
    iget v1, v0, LX/2q9;->g:I

    if-gez v1, :cond_8

    .line 1204251
    invoke-static {v0, v2}, LX/2q9;->d(LX/2q9;I)I

    move-result v1

    .line 1204252
    :cond_5
    :goto_3
    move v1, v1

    .line 1204253
    iput v1, v0, LX/2q9;->g:I

    .line 1204254
    iget v1, v0, LX/2q9;->g:I

    if-ltz v1, :cond_1

    .line 1204255
    invoke-static {v0, v2}, LX/2q9;->b(LX/2q9;I)V

    goto :goto_1

    .line 1204256
    :cond_6
    iget v4, v1, LX/7QR;->b:I

    add-int/2addr v4, v3

    if-le v2, v4, :cond_7

    .line 1204257
    sget-object v4, LX/7QQ;->LATE:LX/7QQ;

    goto :goto_2

    .line 1204258
    :cond_7
    sget-object v4, LX/7QQ;->IN_RANGE:LX/7QQ;

    goto :goto_2

    .line 1204259
    :cond_8
    iget v1, v0, LX/2q9;->g:I

    :goto_4
    iget-object v3, v0, LX/2q9;->b:LX/7QP;

    invoke-virtual {v3}, LX/7QP;->a()I

    move-result v3

    if-ge v1, v3, :cond_9

    .line 1204260
    iget-object v3, v0, LX/2q9;->b:LX/7QP;

    invoke-virtual {v3, v1}, LX/7QP;->a(I)LX/7QR;

    move-result-object v3

    .line 1204261
    iget v4, v3, LX/7QR;->a:I

    move v3, v4

    .line 1204262
    if-ge v3, v2, :cond_5

    .line 1204263
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1204264
    :cond_9
    const/4 v1, -0x1

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1337c0de
        :pswitch_0
    .end packed-switch
.end method
