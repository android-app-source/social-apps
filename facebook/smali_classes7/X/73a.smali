.class public LX/73a;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1165935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165936
    iput-object p1, p0, LX/73a;->a:Ljava/util/concurrent/ExecutorService;

    .line 1165937
    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 4
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 1165938
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1165939
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 1165940
    const v2, 0x7f010055

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1165941
    iget v0, v0, Landroid/util/TypedValue;->data:I

    return v0
.end method

.method public static b(LX/0QB;)LX/73a;
    .locals 2

    .prologue
    .line 1165942
    new-instance v1, LX/73a;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v1, v0}, LX/73a;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 1165943
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1165944
    iget-object v0, p0, LX/73a;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/payments/ui/PaymentsViewHelper$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/payments/ui/PaymentsViewHelper$1;-><init>(LX/73a;Landroid/view/View;)V

    const v2, -0x26e22818

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1165945
    return-void
.end method
