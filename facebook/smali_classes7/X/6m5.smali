.class public LX/6m5;
.super Lcom/facebook/widget/ListViewFriendlyViewPager;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1143281
    invoke-direct {p0, p1}, Lcom/facebook/widget/ListViewFriendlyViewPager;-><init>(Landroid/content/Context;)V

    .line 1143282
    return-void
.end method


# virtual methods
.method public getMeasuredHeightOfFirstItem()I
    .locals 8

    .prologue
    .line 1143283
    invoke-virtual {p0}, LX/6m5;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, LX/6m5;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, LX/6m5;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1143284
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1143285
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    check-cast v0, LX/6lz;

    const/4 v5, 0x0

    .line 1143286
    iget-object v2, v0, LX/6lz;->f:LX/6lf;

    if-nez v2, :cond_0

    move v4, v5

    .line 1143287
    :goto_0
    move v0, v4

    .line 1143288
    return v0

    .line 1143289
    :cond_0
    iget-object v2, v0, LX/6lz;->f:LX/6lf;

    check-cast v2, LX/6lf;

    .line 1143290
    iget-object v3, v0, LX/6lz;->c:LX/6lx;

    invoke-virtual {v2, v3}, LX/6lf;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 1143291
    iget-object v3, v0, LX/6lz;->e:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result p0

    move v6, v5

    move v4, v5

    :goto_1
    if-ge v6, p0, :cond_2

    iget-object v3, v0, LX/6lz;->e:LX/0Px;

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;

    .line 1143292
    invoke-virtual {v2, v7, v3}, LX/6lf;->a(Landroid/view/View;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;)V

    .line 1143293
    invoke-virtual {v7, v1, v5}, Landroid/view/View;->measure(II)V

    .line 1143294
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    if-le v3, v4, :cond_1

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 1143295
    :goto_2
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v4, v3

    goto :goto_1

    :cond_1
    move v3, v4

    .line 1143296
    goto :goto_2

    .line 1143297
    :cond_2
    iget-object v2, v0, LX/6lz;->f:LX/6lf;

    invoke-virtual {v2, v7}, LX/6lf;->a(Landroid/view/View;)V

    goto :goto_0
.end method
