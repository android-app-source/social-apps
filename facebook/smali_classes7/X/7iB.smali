.class public final LX/7iB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1227150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1227151
    iput-object p1, p0, LX/7iB;->a:Landroid/content/Context;

    .line 1227152
    iput-object p2, p0, LX/7iB;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1227153
    iput-object p3, p0, LX/7iB;->c:Ljava/lang/String;

    .line 1227154
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1227155
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, LX/7iB;->a:Landroid/content/Context;

    const-class v2, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1227156
    iget-object v0, p0, LX/7iB;->c:Ljava/lang/String;

    .line 1227157
    const-string v2, "shortcut_id_key"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1227158
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1227159
    invoke-static {v0}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->c(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 1227160
    iget-object v2, p0, LX/7iB;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    const-string v3, "url"

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v3, p0, LX/7iB;->c:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1227161
    return-object v1
.end method
