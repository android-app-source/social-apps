.class public final LX/6yB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6u8;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V
    .locals 0

    .prologue
    .line 1159567
    iput-object p1, p0, LX/6yB;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/locale/Country;)V
    .locals 2

    .prologue
    .line 1159568
    iget-object v0, p0, LX/6yB;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    const/4 p0, 0x0

    .line 1159569
    sget-object v1, Lcom/facebook/common/locale/Country;->a:Lcom/facebook/common/locale/Country;

    invoke-virtual {v1, p1}, Lcom/facebook/common/locale/Country;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1159570
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d()V

    .line 1159571
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const/4 p0, 0x1

    .line 1159572
    iput-boolean p0, v1, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->g:Z

    .line 1159573
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setVisibility(I)V

    .line 1159574
    :goto_0
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    if-eqz v1, :cond_0

    .line 1159575
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    invoke-virtual {v1}, LX/6y3;->a()V

    .line 1159576
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->c()Z

    move-result p0

    invoke-virtual {v1, p0}, LX/6y3;->a(Z)V

    .line 1159577
    :cond_0
    return-void

    .line 1159578
    :cond_1
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1159579
    iput-boolean p0, v1, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->g:Z

    .line 1159580
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1, p0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setVisibility(I)V

    goto :goto_0
.end method
