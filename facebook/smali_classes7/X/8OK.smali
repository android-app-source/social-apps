.class public LX/8OK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/8O7;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final c:I


# direct methods
.method public constructor <init>(LX/8O7;I)V
    .locals 1

    .prologue
    .line 1338974
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1338975
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8OK;->b:Ljava/util/Map;

    .line 1338976
    iput-object p1, p0, LX/8OK;->a:LX/8O7;

    .line 1338977
    iput p2, p0, LX/8OK;->c:I

    .line 1338978
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(IF)V
    .locals 4

    .prologue
    .line 1338979
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8OK;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1338980
    iget-object v0, p0, LX/8OK;->a:LX/8O7;

    iget-object v1, p0, LX/8OK;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    iget v2, p0, LX/8OK;->c:I

    .line 1338981
    const/4 v3, 0x0

    .line 1338982
    iget-object p1, p0, LX/8OK;->b:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    move p1, v3

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    .line 1338983
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    add-float/2addr v3, p1

    move p1, v3

    .line 1338984
    goto :goto_0

    .line 1338985
    :cond_0
    iget v3, p0, LX/8OK;->c:I

    int-to-float v3, v3

    div-float v3, p1, v3

    const/high16 p1, 0x42c80000    # 100.0f

    mul-float/2addr v3, p1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move v3, v3

    .line 1338986
    invoke-virtual {v0, v1, v2, v3}, LX/8O7;->a(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1338987
    monitor-exit p0

    return-void

    .line 1338988
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
