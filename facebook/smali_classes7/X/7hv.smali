.class public LX/7hv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static volatile f:LX/7hv;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0ad;

.field private d:Landroid/database/sqlite/SQLiteDatabase;

.field private e:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1226634
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "creation_utc"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "host_key"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "value"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "path"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "expires_utc"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "secure"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "httponly"

    aput-object v2, v0, v1

    sput-object v0, LX/7hv;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1226635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1226636
    iput-object p1, p0, LX/7hv;->b:Landroid/content/Context;

    .line 1226637
    iput-object p2, p0, LX/7hv;->c:LX/0ad;

    .line 1226638
    return-void
.end method

.method public static a(LX/0QB;)LX/7hv;
    .locals 5

    .prologue
    .line 1226639
    sget-object v0, LX/7hv;->f:LX/7hv;

    if-nez v0, :cond_1

    .line 1226640
    const-class v1, LX/7hv;

    monitor-enter v1

    .line 1226641
    :try_start_0
    sget-object v0, LX/7hv;->f:LX/7hv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1226642
    if-eqz v2, :cond_0

    .line 1226643
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1226644
    new-instance p0, LX/7hv;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/7hv;-><init>(Landroid/content/Context;LX/0ad;)V

    .line 1226645
    move-object v0, p0

    .line 1226646
    sput-object v0, LX/7hv;->f:LX/7hv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1226647
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1226648
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1226649
    :cond_1
    sget-object v0, LX/7hv;->f:LX/7hv;

    return-object v0

    .line 1226650
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1226651
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a(LX/7hv;Ljava/io/File;I)Landroid/database/sqlite/SQLiteDatabase;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1226652
    monitor-enter p0

    packed-switch p2, :pswitch_data_0

    .line 1226653
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    .line 1226654
    :pswitch_0
    :try_start_0
    iget-object v0, p0, LX/7hv;->d:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    .line 1226655
    invoke-static {p1}, LX/7hv;->a(Ljava/io/File;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, LX/7hv;->d:Landroid/database/sqlite/SQLiteDatabase;

    .line 1226656
    :cond_0
    iget-object v0, p0, LX/7hv;->d:Landroid/database/sqlite/SQLiteDatabase;

    goto :goto_0

    .line 1226657
    :pswitch_1
    iget-object v0, p0, LX/7hv;->e:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_1

    .line 1226658
    invoke-static {p1}, LX/7hv;->a(Ljava/io/File;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, LX/7hv;->e:Landroid/database/sqlite/SQLiteDatabase;

    .line 1226659
    :cond_1
    iget-object v0, p0, LX/7hv;->e:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1226660
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/io/File;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1226661
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1226662
    :goto_0
    return-object v0

    :catch_0
    goto :goto_0
.end method

.method public static a(Landroid/database/Cursor;)Ljava/util/List;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/0EC;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1226663
    const-string v2, "host_key"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 1226664
    const-string v2, "name"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 1226665
    const-string v2, "value"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 1226666
    const-string v2, "path"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 1226667
    const-string v2, "expires_utc"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 1226668
    const-string v2, "secure"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 1226669
    const-string v2, "httponly"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 1226670
    const-string v2, "creation_utc"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 1226671
    const/4 v2, -0x1

    if-eq v14, v2, :cond_0

    const/4 v2, -0x1

    if-eq v15, v2, :cond_0

    const/4 v2, -0x1

    move/from16 v0, v16

    if-eq v0, v2, :cond_0

    const/4 v2, -0x1

    move/from16 v0, v17

    if-eq v0, v2, :cond_0

    const/4 v2, -0x1

    move/from16 v0, v18

    if-eq v0, v2, :cond_0

    const/4 v2, -0x1

    move/from16 v0, v19

    if-eq v0, v2, :cond_0

    const/4 v2, -0x1

    move/from16 v0, v20

    if-ne v0, v2, :cond_2

    .line 1226672
    :cond_0
    const/4 v2, 0x0

    .line 1226673
    :cond_1
    return-object v2

    .line 1226674
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1226675
    :goto_0
    invoke-interface/range {p0 .. p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1226676
    new-instance v3, LX/0EC;

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    if-lez v12, :cond_3

    const/4 v12, 0x1

    :goto_1
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    if-lez v13, :cond_4

    const/4 v13, 0x1

    :goto_2
    invoke-direct/range {v3 .. v13}, LX/0EC;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    .line 1226677
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1226678
    :cond_3
    const/4 v12, 0x0

    goto :goto_1

    :cond_4
    const/4 v13, 0x0

    goto :goto_2
.end method

.method public static a(LX/7hv;)Z
    .locals 3

    .prologue
    .line 1226679
    iget-object v0, p0, LX/7hv;->c:LX/0ad;

    sget-short v1, LX/1Bm;->D:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static c(LX/7hv;I)Ljava/io/File;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 1226680
    packed-switch p1, :pswitch_data_0

    move-object v0, v1

    .line 1226681
    :goto_0
    return-object v0

    .line 1226682
    :pswitch_0
    iget-object v0, p0, LX/7hv;->b:Landroid/content/Context;

    const-string v2, "webview"

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 1226683
    :goto_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 1226684
    goto :goto_0

    .line 1226685
    :pswitch_1
    iget-object v0, p0, LX/7hv;->b:Landroid/content/Context;

    const-string v2, "browser_proc_webview"

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    goto :goto_1

    .line 1226686
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Cookies"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1226687
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->canRead()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move-object v0, v1

    .line 1226688
    goto :goto_0

    :cond_3
    move-object v0, v2

    .line 1226689
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(ILjava/lang/String;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/0EC;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v10, 0x0

    .line 1226690
    invoke-static {p0, p1}, LX/7hv;->c(LX/7hv;I)Ljava/io/File;

    move-result-object v1

    .line 1226691
    if-nez v1, :cond_0

    move-object v0, v10

    .line 1226692
    :goto_0
    return-object v0

    .line 1226693
    :cond_0
    invoke-static {p0}, LX/7hv;->a(LX/7hv;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1226694
    invoke-static {p0, v1, p1}, LX/7hv;->a(LX/7hv;Ljava/io/File;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move v11, v0

    move-object v0, v1

    .line 1226695
    :goto_1
    if-nez v0, :cond_2

    move-object v0, v10

    .line 1226696
    goto :goto_0

    .line 1226697
    :cond_1
    invoke-static {v1}, LX/7hv;->a(Ljava/io/File;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1226698
    const/4 v0, 0x1

    move v11, v0

    move-object v0, v1

    goto :goto_1

    .line 1226699
    :cond_2
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 1226700
    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1226701
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "."

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1226702
    const/4 v1, 0x0

    .line 1226703
    :goto_2
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 1226704
    const/16 v3, 0x2e

    invoke-virtual {p2, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 1226705
    const/4 v4, -0x1

    if-ne v3, v4, :cond_c

    .line 1226706
    invoke-virtual {p2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1226707
    :cond_3
    move-object v5, v2

    .line 1226708
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    .line 1226709
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1226710
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v1, :cond_5

    .line 1226711
    const-string v4, "host_key = ? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1226712
    add-int/lit8 v4, v1, -0x1

    if-ge v2, v4, :cond_4

    .line 1226713
    const-string v4, " OR "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1226714
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1226715
    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    .line 1226716
    const/4 v1, 0x0

    :try_start_0
    const-string v2, "cookies"

    sget-object v3, LX/7hv;->a:[Ljava/lang/String;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1226717
    :try_start_1
    invoke-static {v2}, LX/7hv;->a(Landroid/database/Cursor;)Ljava/util/List;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 1226718
    if-eqz v2, :cond_6

    .line 1226719
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1226720
    :cond_6
    if-eqz v11, :cond_7

    .line 1226721
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_7
    move-object v0, v1

    goto/16 :goto_0

    .line 1226722
    :catch_0
    move-object v1, v10

    :goto_4
    if-eqz v1, :cond_8

    .line 1226723
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1226724
    :cond_8
    if-eqz v11, :cond_9

    .line 1226725
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_9
    move-object v0, v10

    goto/16 :goto_0

    .line 1226726
    :catchall_0
    move-exception v1

    :goto_5
    if-eqz v10, :cond_a

    .line 1226727
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1226728
    :cond_a
    if-eqz v11, :cond_b

    .line 1226729
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_b
    throw v1

    .line 1226730
    :catchall_1
    move-exception v1

    move-object v10, v2

    goto :goto_5

    :catch_1
    move-object v1, v2

    goto :goto_4

    .line 1226731
    :cond_c
    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1226732
    add-int/lit8 v1, v3, 0x1

    .line 1226733
    goto/16 :goto_2
.end method
