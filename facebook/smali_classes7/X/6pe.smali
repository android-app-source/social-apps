.class public LX/6pe;
.super LX/6pY;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6pe;


# instance fields
.field public final a:LX/6p6;


# direct methods
.method public constructor <init>(LX/6p6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1149539
    invoke-direct {p0}, LX/6pY;-><init>()V

    .line 1149540
    iput-object p1, p0, LX/6pe;->a:LX/6p6;

    .line 1149541
    return-void
.end method

.method public static a(LX/0QB;)LX/6pe;
    .locals 4

    .prologue
    .line 1149542
    sget-object v0, LX/6pe;->b:LX/6pe;

    if-nez v0, :cond_1

    .line 1149543
    const-class v1, LX/6pe;

    monitor-enter v1

    .line 1149544
    :try_start_0
    sget-object v0, LX/6pe;->b:LX/6pe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1149545
    if-eqz v2, :cond_0

    .line 1149546
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1149547
    new-instance p0, LX/6pe;

    invoke-static {v0}, LX/6p6;->a(LX/0QB;)LX/6p6;

    move-result-object v3

    check-cast v3, LX/6p6;

    invoke-direct {p0, v3}, LX/6pe;-><init>(LX/6p6;)V

    .line 1149548
    move-object v0, p0

    .line 1149549
    sput-object v0, LX/6pe;->b:LX/6pe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1149550
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1149551
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1149552
    :cond_1
    sget-object v0, LX/6pe;->b:LX/6pe;

    return-object v0

    .line 1149553
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1149554
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/6pM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1149555
    sget-object v0, LX/6pM;->CREATE:LX/6pM;

    sget-object v1, LX/6pM;->CREATE_CONFIRMATION:LX/6pM;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;LX/6pM;)LX/6od;
    .locals 3

    .prologue
    .line 1149556
    sget-object v0, LX/6pd;->a:[I

    invoke-virtual {p3}, LX/6pM;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1149557
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected PinPage "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1149558
    :pswitch_0
    new-instance v0, LX/6pa;

    invoke-direct {v0, p0, p1}, LX/6pa;-><init>(LX/6pe;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    .line 1149559
    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, LX/6pb;

    invoke-direct {v0, p0, p1, p2}, LX/6pb;-><init>(LX/6pe;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1149560
    iget-object v0, p0, LX/6pe;->a:LX/6p6;

    invoke-virtual {v0}, LX/6p6;->a()V

    .line 1149561
    return-void
.end method
