.class public final LX/6rW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Rl",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)V
    .locals 0

    .prologue
    .line 1152338
    iput-object p1, p0, LX/6rW;->a:Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1152339
    check-cast p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    .line 1152340
    iget-object v0, p0, LX/6rW;->a:Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    iget-object v0, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->e:LX/0Px;

    iget-object v1, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
