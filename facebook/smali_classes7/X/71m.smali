.class public LX/71m;
.super Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:Landroid/widget/TextView;

.field public b:Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1163684
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;)V

    .line 1163685
    const p1, 0x7f031328

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1163686
    const p1, 0x7f0d2c71

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/71m;->a:Landroid/widget/TextView;

    .line 1163687
    invoke-virtual {p0, p0}, LX/71m;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1163688
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0xe57103b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1163689
    iget-object v1, p0, LX/71m;->b:Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;

    iget-object v1, v1, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;->b:Landroid/content/Intent;

    iget-object v2, p0, LX/71m;->b:Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;

    iget v2, v2, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;->c:I

    invoke-virtual {p0, v1, v2}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a(Landroid/content/Intent;I)V

    .line 1163690
    const v1, 0x312c5cd9

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
