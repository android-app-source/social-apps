.class public final enum LX/743;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/743;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/743;

.field public static final enum COMPOSER:LX/743;

.field public static final enum FACECAST_LIVE_VIDEO:LX/743;

.field public static final enum GIF:LX/743;

.field public static final enum NOT_RELEVANT:LX/743;

.field public static final enum PROFILE_VIDEO:LX/743;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1167394
    new-instance v0, LX/743;

    const-string v1, "NOT_RELEVANT"

    const-string v2, "none"

    invoke-direct {v0, v1, v3, v2}, LX/743;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/743;->NOT_RELEVANT:LX/743;

    .line 1167395
    new-instance v0, LX/743;

    const-string v1, "PROFILE_VIDEO"

    const-string v2, "profile_video"

    invoke-direct {v0, v1, v4, v2}, LX/743;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/743;->PROFILE_VIDEO:LX/743;

    .line 1167396
    new-instance v0, LX/743;

    const-string v1, "FACECAST_LIVE_VIDEO"

    const-string v2, "facecast_live_video"

    invoke-direct {v0, v1, v5, v2}, LX/743;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/743;->FACECAST_LIVE_VIDEO:LX/743;

    .line 1167397
    new-instance v0, LX/743;

    const-string v1, "GIF"

    const-string v2, "gif"

    invoke-direct {v0, v1, v6, v2}, LX/743;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/743;->GIF:LX/743;

    .line 1167398
    new-instance v0, LX/743;

    const-string v1, "COMPOSER"

    const-string v2, "composer"

    invoke-direct {v0, v1, v7, v2}, LX/743;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/743;->COMPOSER:LX/743;

    .line 1167399
    const/4 v0, 0x5

    new-array v0, v0, [LX/743;

    sget-object v1, LX/743;->NOT_RELEVANT:LX/743;

    aput-object v1, v0, v3

    sget-object v1, LX/743;->PROFILE_VIDEO:LX/743;

    aput-object v1, v0, v4

    sget-object v1, LX/743;->FACECAST_LIVE_VIDEO:LX/743;

    aput-object v1, v0, v5

    sget-object v1, LX/743;->GIF:LX/743;

    aput-object v1, v0, v6

    sget-object v1, LX/743;->COMPOSER:LX/743;

    aput-object v1, v0, v7

    sput-object v0, LX/743;->$VALUES:[LX/743;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1167400
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1167401
    iput-object p3, p0, LX/743;->value:Ljava/lang/String;

    .line 1167402
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/743;
    .locals 1

    .prologue
    .line 1167403
    const-class v0, LX/743;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/743;

    return-object v0
.end method

.method public static values()[LX/743;
    .locals 1

    .prologue
    .line 1167404
    sget-object v0, LX/743;->$VALUES:[LX/743;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/743;

    return-object v0
.end method
