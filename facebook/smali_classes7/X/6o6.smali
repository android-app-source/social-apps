.class public LX/6o6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/6oL;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/app/KeyguardManager;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Uh;

.field private final d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(LX/6oL;LX/0Ot;LX/0Uh;Ljava/lang/Integer;)V
    .locals 0
    .param p4    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/common/android/AndroidSdkVersion;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6oL;",
            "LX/0Ot",
            "<",
            "Landroid/app/KeyguardManager;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1147884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1147885
    iput-object p1, p0, LX/6o6;->a:LX/6oL;

    .line 1147886
    iput-object p2, p0, LX/6o6;->b:LX/0Ot;

    .line 1147887
    iput-object p3, p0, LX/6o6;->c:LX/0Uh;

    .line 1147888
    iput-object p4, p0, LX/6o6;->d:Ljava/lang/Integer;

    .line 1147889
    return-void
.end method

.method public static b(LX/0QB;)LX/6o6;
    .locals 5

    .prologue
    .line 1147890
    new-instance v3, LX/6o6;

    invoke-static {p0}, LX/6oL;->a(LX/0QB;)LX/6oL;

    move-result-object v0

    check-cast v0, LX/6oL;

    const/16 v1, 0x5

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0}, LX/43s;->a(LX/0QB;)Ljava/lang/Integer;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-direct {v3, v0, v4, v1, v2}, LX/6o6;-><init>(LX/6oL;LX/0Ot;LX/0Uh;Ljava/lang/Integer;)V

    .line 1147891
    return-object v3
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 1147892
    iget-object v0, p0, LX/6o6;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, LX/6o6;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1147893
    iget-object v0, p0, LX/6o6;->a:LX/6oL;

    invoke-virtual {v0}, LX/6oL;->a()Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/fingerprint/FingerprintManager;->isHardwareDetected()Z

    move-result v0

    move v0, v0

    .line 1147894
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/6oB;)LX/6o5;
    .locals 2
    .param p1    # LX/6oB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 1147895
    invoke-direct {p0}, LX/6o6;->c()Z

    move-result v0

    const-string v1, "Please check isFingerprintSupported() before calling this method"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1147896
    iget-object v0, p0, LX/6o6;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1147897
    sget-object v0, LX/6o5;->LOCK_SCREEN_NOT_SETUP:LX/6o5;

    .line 1147898
    :goto_0
    return-object v0

    .line 1147899
    :cond_0
    iget-object v0, p0, LX/6o6;->a:LX/6oL;

    invoke-virtual {v0}, LX/6oL;->a()Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/fingerprint/FingerprintManager;->hasEnrolledFingerprints()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1147900
    sget-object v0, LX/6o5;->NO_ENROLLED_FINGERPRINTS:LX/6o5;

    goto :goto_0

    .line 1147901
    :cond_1
    if-eqz p1, :cond_2

    invoke-interface {p1}, LX/6oB;->c()LX/6oJ;

    move-result-object v0

    sget-object v1, LX/6oJ;->INVALID:LX/6oJ;

    if-ne v0, v1, :cond_2

    .line 1147902
    sget-object v0, LX/6o5;->KEY_PAIR_INVALIDATED:LX/6o5;

    goto :goto_0

    .line 1147903
    :cond_2
    sget-object v0, LX/6o5;->AVAILABLE:LX/6o5;

    goto :goto_0
.end method

.method public final a()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 1147904
    invoke-direct {p0}, LX/6o6;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/6o6;->a(LX/6oB;)LX/6o5;

    move-result-object v0

    sget-object v1, LX/6o5;->AVAILABLE:LX/6o5;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 1147905
    iget-object v0, p0, LX/6o6;->c:LX/0Uh;

    const/16 v1, 0x22c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
