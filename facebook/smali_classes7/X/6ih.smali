.class public LX/6ih;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Lcom/facebook/ui/media/attachments/MediaResource;

.field public g:Z

.field public h:Lcom/facebook/messaging/model/threads/NotificationSetting;

.field public i:Z

.field public j:Z

.field public k:Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;

.field public l:Lcom/facebook/messaging/model/threads/ThreadCustomization;

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:I

.field public p:I

.field public q:LX/03R;

.field public r:Ljava/lang/String;

.field public s:LX/03R;

.field public t:LX/03R;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1129772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129773
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/6ih;->q:LX/03R;

    .line 1129774
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/6ih;->s:LX/03R;

    .line 1129775
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/6ih;->t:LX/03R;

    .line 1129776
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/service/model/ModifyThreadParams;)V
    .locals 2

    .prologue
    .line 1129777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129778
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/6ih;->q:LX/03R;

    .line 1129779
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/6ih;->s:LX/03R;

    .line 1129780
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/6ih;->t:LX/03R;

    .line 1129781
    iget-object v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 1129782
    iput-object v0, p0, LX/6ih;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1129783
    iget-object v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1129784
    iput-object v0, p0, LX/6ih;->b:Ljava/lang/String;

    .line 1129785
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->c:Z

    move v0, v0

    .line 1129786
    if-eqz v0, :cond_0

    .line 1129787
    iget-object v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1129788
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/6ih;->c:Z

    .line 1129789
    iput-object v0, p0, LX/6ih;->d:Ljava/lang/String;

    .line 1129790
    :cond_0
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->f:Z

    move v0, v0

    .line 1129791
    if-eqz v0, :cond_1

    .line 1129792
    iget-object v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v0, v0

    .line 1129793
    invoke-virtual {p0, v0}, LX/6ih;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ih;

    .line 1129794
    :cond_1
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->g:Z

    move v0, v0

    .line 1129795
    if-eqz v0, :cond_2

    .line 1129796
    iget-object v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-object v0, v0

    .line 1129797
    invoke-virtual {p0, v0}, LX/6ih;->a(Lcom/facebook/messaging/model/threads/NotificationSetting;)LX/6ih;

    .line 1129798
    :cond_2
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->i:Z

    move v0, v0

    .line 1129799
    iput-boolean v0, p0, LX/6ih;->i:Z

    .line 1129800
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->j:Z

    move v0, v0

    .line 1129801
    iput-boolean v0, p0, LX/6ih;->j:Z

    .line 1129802
    iget-object v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->k:Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;

    move-object v0, v0

    .line 1129803
    iput-object v0, p0, LX/6ih;->k:Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;

    .line 1129804
    iget-object v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->l:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    move-object v0, v0

    .line 1129805
    iput-object v0, p0, LX/6ih;->l:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 1129806
    iget-object v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->m:Ljava/lang/String;

    move-object v0, v0

    .line 1129807
    iput-object v0, p0, LX/6ih;->m:Ljava/lang/String;

    .line 1129808
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->n:Z

    move v0, v0

    .line 1129809
    if-eqz v0, :cond_3

    .line 1129810
    iget v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->o:I

    move v0, v0

    .line 1129811
    iput v0, p0, LX/6ih;->o:I

    .line 1129812
    :cond_3
    iget v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->p:I

    move v0, v0

    .line 1129813
    iput v0, p0, LX/6ih;->p:I

    .line 1129814
    iget-object v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->q:LX/03R;

    move-object v0, v0

    .line 1129815
    iput-object v0, p0, LX/6ih;->q:LX/03R;

    .line 1129816
    iget-object v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->s:Ljava/lang/String;

    move-object v0, v0

    .line 1129817
    iput-object v0, p0, LX/6ih;->r:Ljava/lang/String;

    .line 1129818
    iget-object v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->t:LX/03R;

    move-object v0, v0

    .line 1129819
    iput-object v0, p0, LX/6ih;->s:LX/03R;

    .line 1129820
    iget-object v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->r:LX/03R;

    move-object v0, v0

    .line 1129821
    iput-object v0, p0, LX/6ih;->t:LX/03R;

    .line 1129822
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threads/NotificationSetting;)LX/6ih;
    .locals 1

    .prologue
    .line 1129823
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6ih;->g:Z

    .line 1129824
    iput-object p1, p0, LX/6ih;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1129825
    return-object p0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ih;
    .locals 1

    .prologue
    .line 1129826
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6ih;->e:Z

    .line 1129827
    iput-object p1, p0, LX/6ih;->f:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1129828
    return-object p0
.end method

.method public final u()Lcom/facebook/messaging/service/model/ModifyThreadParams;
    .locals 1

    .prologue
    .line 1129829
    new-instance v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/service/model/ModifyThreadParams;-><init>(LX/6ih;)V

    return-object v0
.end method
