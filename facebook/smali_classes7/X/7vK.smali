.class public final enum LX/7vK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7vK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7vK;

.field public static final enum ADMIN:LX/7vK;

.field public static final enum CREATE_REPEAT_EVENT:LX/7vK;

.field public static final enum DECLINE:LX/7vK;

.field public static final enum DELETE:LX/7vK;

.field public static final enum EDIT:LX/7vK;

.field public static final enum EDIT_HOST:LX/7vK;

.field public static final enum EDIT_PROMOTION:LX/7vK;

.field public static final enum INVITE:LX/7vK;

.field public static final enum JOIN:LX/7vK;

.field public static final enum MAYBE:LX/7vK;

.field public static final enum POST:LX/7vK;

.field public static final enum PROMOTE:LX/7vK;

.field public static final enum REMOVE_SELF:LX/7vK;

.field public static final enum REPORT:LX/7vK;

.field public static final enum SAVE:LX/7vK;

.field public static final enum SEEN:LX/7vK;

.field public static final enum SEND_MESSAGE_TO_GUESTS:LX/7vK;

.field public static final enum SHARE:LX/7vK;

.field public static final enum UNKNOWN_SEEN_STATE:LX/7vK;

.field public static final enum UNSEEN:LX/7vK;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1274432
    new-instance v0, LX/7vK;

    const-string v1, "ADMIN"

    invoke-direct {v0, v1, v3}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->ADMIN:LX/7vK;

    .line 1274433
    new-instance v0, LX/7vK;

    const-string v1, "JOIN"

    invoke-direct {v0, v1, v4}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->JOIN:LX/7vK;

    .line 1274434
    new-instance v0, LX/7vK;

    const-string v1, "MAYBE"

    invoke-direct {v0, v1, v5}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->MAYBE:LX/7vK;

    .line 1274435
    new-instance v0, LX/7vK;

    const-string v1, "DECLINE"

    invoke-direct {v0, v1, v6}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->DECLINE:LX/7vK;

    .line 1274436
    new-instance v0, LX/7vK;

    const-string v1, "INVITE"

    invoke-direct {v0, v1, v7}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->INVITE:LX/7vK;

    .line 1274437
    new-instance v0, LX/7vK;

    const-string v1, "POST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->POST:LX/7vK;

    .line 1274438
    new-instance v0, LX/7vK;

    const-string v1, "EDIT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->EDIT:LX/7vK;

    .line 1274439
    new-instance v0, LX/7vK;

    const-string v1, "REPORT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->REPORT:LX/7vK;

    .line 1274440
    new-instance v0, LX/7vK;

    const-string v1, "DELETE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->DELETE:LX/7vK;

    .line 1274441
    new-instance v0, LX/7vK;

    const-string v1, "SAVE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->SAVE:LX/7vK;

    .line 1274442
    new-instance v0, LX/7vK;

    const-string v1, "SHARE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->SHARE:LX/7vK;

    .line 1274443
    new-instance v0, LX/7vK;

    const-string v1, "SEND_MESSAGE_TO_GUESTS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->SEND_MESSAGE_TO_GUESTS:LX/7vK;

    .line 1274444
    new-instance v0, LX/7vK;

    const-string v1, "EDIT_HOST"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->EDIT_HOST:LX/7vK;

    .line 1274445
    new-instance v0, LX/7vK;

    const-string v1, "REMOVE_SELF"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->REMOVE_SELF:LX/7vK;

    .line 1274446
    new-instance v0, LX/7vK;

    const-string v1, "SEEN"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->SEEN:LX/7vK;

    .line 1274447
    new-instance v0, LX/7vK;

    const-string v1, "UNSEEN"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->UNSEEN:LX/7vK;

    .line 1274448
    new-instance v0, LX/7vK;

    const-string v1, "UNKNOWN_SEEN_STATE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->UNKNOWN_SEEN_STATE:LX/7vK;

    .line 1274449
    new-instance v0, LX/7vK;

    const-string v1, "PROMOTE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->PROMOTE:LX/7vK;

    .line 1274450
    new-instance v0, LX/7vK;

    const-string v1, "EDIT_PROMOTION"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->EDIT_PROMOTION:LX/7vK;

    .line 1274451
    new-instance v0, LX/7vK;

    const-string v1, "CREATE_REPEAT_EVENT"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/7vK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7vK;->CREATE_REPEAT_EVENT:LX/7vK;

    .line 1274452
    const/16 v0, 0x14

    new-array v0, v0, [LX/7vK;

    sget-object v1, LX/7vK;->ADMIN:LX/7vK;

    aput-object v1, v0, v3

    sget-object v1, LX/7vK;->JOIN:LX/7vK;

    aput-object v1, v0, v4

    sget-object v1, LX/7vK;->MAYBE:LX/7vK;

    aput-object v1, v0, v5

    sget-object v1, LX/7vK;->DECLINE:LX/7vK;

    aput-object v1, v0, v6

    sget-object v1, LX/7vK;->INVITE:LX/7vK;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7vK;->POST:LX/7vK;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7vK;->EDIT:LX/7vK;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7vK;->REPORT:LX/7vK;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7vK;->DELETE:LX/7vK;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7vK;->SAVE:LX/7vK;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7vK;->SHARE:LX/7vK;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7vK;->SEND_MESSAGE_TO_GUESTS:LX/7vK;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7vK;->EDIT_HOST:LX/7vK;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/7vK;->REMOVE_SELF:LX/7vK;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/7vK;->SEEN:LX/7vK;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/7vK;->UNSEEN:LX/7vK;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/7vK;->UNKNOWN_SEEN_STATE:LX/7vK;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/7vK;->PROMOTE:LX/7vK;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/7vK;->EDIT_PROMOTION:LX/7vK;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/7vK;->CREATE_REPEAT_EVENT:LX/7vK;

    aput-object v2, v0, v1

    sput-object v0, LX/7vK;->$VALUES:[LX/7vK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1274453
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static deserializeCapabilities(J)Ljava/util/EnumSet;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/EnumSet",
            "<",
            "LX/7vK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1274454
    const-class v0, LX/7vK;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 1274455
    invoke-static {}, LX/7vK;->values()[LX/7vK;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 1274456
    const-wide/16 v6, 0x1

    invoke-virtual {v4}, LX/7vK;->ordinal()I

    move-result v5

    shl-long/2addr v6, v5

    .line 1274457
    and-long v8, p0, v6

    cmp-long v5, v8, v6

    if-nez v5, :cond_0

    .line 1274458
    invoke-virtual {v1, v4}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1274459
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1274460
    :cond_1
    return-object v1
.end method

.method public static serializeCapabilities(Ljava/util/EnumSet;)J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "LX/7vK;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 1274461
    const-wide/16 v0, 0x0

    .line 1274462
    invoke-virtual {p0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    .line 1274463
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1274464
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7vK;

    .line 1274465
    const-wide/16 v6, 0x1

    invoke-virtual {v0}, LX/7vK;->ordinal()I

    move-result v0

    shl-long v0, v6, v0

    or-long/2addr v0, v2

    move-wide v2, v0

    .line 1274466
    goto :goto_0

    .line 1274467
    :cond_0
    return-wide v2
.end method

.method public static valueOf(Ljava/lang/String;)LX/7vK;
    .locals 1

    .prologue
    .line 1274468
    const-class v0, LX/7vK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7vK;

    return-object v0
.end method

.method public static values()[LX/7vK;
    .locals 1

    .prologue
    .line 1274469
    sget-object v0, LX/7vK;->$VALUES:[LX/7vK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7vK;

    return-object v0
.end method
