.class public LX/8TS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static s:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8VZ;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/8Ve;

.field private final d:LX/8TM;

.field public e:LX/8TO;

.field public f:LX/8Tb;

.field public g:LX/8Vb;

.field public h:LX/8TW;

.field public i:Z

.field public j:LX/8TP;

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/8TU;

.field public m:LX/8TV;

.field public n:I

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/2th;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/8Ve;LX/0Ot;LX/8TM;)V
    .locals 3
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/8Ve;",
            "LX/0Ot",
            "<",
            "LX/8VZ;",
            ">;",
            "LX/8TM;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1348673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1348674
    new-instance v0, LX/8TP;

    sget-object v1, LX/8Ta;->None:LX/8Ta;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/8TP;-><init>(LX/8Ta;Ljava/lang/String;)V

    iput-object v0, p0, LX/8TS;->j:LX/8TP;

    .line 1348675
    sget-object v0, LX/8TV;->START_SCREEN:LX/8TV;

    iput-object v0, p0, LX/8TS;->m:LX/8TV;

    .line 1348676
    iput-object p1, p0, LX/8TS;->a:LX/0Or;

    .line 1348677
    iput-object p2, p0, LX/8TS;->c:LX/8Ve;

    .line 1348678
    iput-object p3, p0, LX/8TS;->b:LX/0Ot;

    .line 1348679
    iput-object p4, p0, LX/8TS;->d:LX/8TM;

    .line 1348680
    sget-object v0, LX/8TV;->START_SCREEN:LX/8TV;

    iput-object v0, p0, LX/8TS;->m:LX/8TV;

    .line 1348681
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8TS;->q:Ljava/lang/String;

    .line 1348682
    return-void
.end method

.method public static a(LX/0QB;)LX/8TS;
    .locals 7

    .prologue
    .line 1348694
    const-class v1, LX/8TS;

    monitor-enter v1

    .line 1348695
    :try_start_0
    sget-object v0, LX/8TS;->s:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1348696
    sput-object v2, LX/8TS;->s:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1348697
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1348698
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1348699
    new-instance v5, LX/8TS;

    const/16 v3, 0x12cb

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/8Ve;->b(LX/0QB;)LX/8Ve;

    move-result-object v3

    check-cast v3, LX/8Ve;

    const/16 v4, 0x3083

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/8TM;->a(LX/0QB;)LX/8TM;

    move-result-object v4

    check-cast v4, LX/8TM;

    invoke-direct {v5, v6, v3, p0, v4}, LX/8TS;-><init>(LX/0Or;LX/8Ve;LX/0Ot;LX/8TM;)V

    .line 1348700
    move-object v0, v5

    .line 1348701
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1348702
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8TS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1348703
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1348704
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/8Tb;)LX/8TS;
    .locals 3

    .prologue
    .line 1348683
    iput-object p1, p0, LX/8TS;->f:LX/8Tb;

    .line 1348684
    iget-object v0, p0, LX/8TS;->f:LX/8Tb;

    .line 1348685
    iget-object v1, v0, LX/8Tb;->c:LX/8Ta;

    move-object v0, v1

    .line 1348686
    sget-object v1, LX/8Ta;->Story:LX/8Ta;

    if-eq v0, v1, :cond_0

    .line 1348687
    new-instance v0, LX/8TP;

    iget-object v1, p0, LX/8TS;->f:LX/8Tb;

    .line 1348688
    iget-object v2, v1, LX/8Tb;->c:LX/8Ta;

    move-object v1, v2

    .line 1348689
    iget-object v2, p0, LX/8TS;->f:LX/8Tb;

    .line 1348690
    iget-object p1, v2, LX/8Tb;->a:Ljava/lang/String;

    move-object v2, p1

    .line 1348691
    invoke-direct {v0, v1, v2}, LX/8TP;-><init>(LX/8Ta;Ljava/lang/String;)V

    iput-object v0, p0, LX/8TS;->j:LX/8TP;

    .line 1348692
    :goto_0
    return-object p0

    .line 1348693
    :cond_0
    new-instance v0, LX/8TP;

    sget-object v1, LX/8Ta;->None:LX/8Ta;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/8TP;-><init>(LX/8Ta;Ljava/lang/String;)V

    iput-object v0, p0, LX/8TS;->j:LX/8TP;

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 1348663
    iput p1, p0, LX/8TS;->n:I

    .line 1348664
    invoke-virtual {p0}, LX/8TS;->i()LX/8TU;

    move-result-object v0

    iget-object v1, p0, LX/8TS;->j:LX/8TP;

    .line 1348665
    iget-object v2, p0, LX/8TS;->e:LX/8TO;

    move-object v2, v2

    .line 1348666
    iget-object p0, v2, LX/8TO;->h:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    move-object v2, p0

    .line 1348667
    iget-object v3, v0, LX/8TU;->a:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1348668
    sget-object v3, LX/8TT;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;->ordinal()I

    move-result p0

    aget v3, v3, p0

    packed-switch v3, :pswitch_data_0

    .line 1348669
    iget-object v3, v0, LX/8TU;->a:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {p1, v3}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 1348670
    :cond_0
    :goto_0
    iget-object v3, v0, LX/8TU;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v3, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348671
    return-void

    .line 1348672
    :pswitch_0
    iget-object v3, v0, LX/8TU;->a:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {p1, v3}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(JJ)V
    .locals 11

    .prologue
    const/4 v5, 0x1

    .line 1348647
    invoke-virtual {p0}, LX/8TS;->c()LX/8Vb;

    move-result-object v0

    .line 1348648
    iget-wide v2, v0, LX/8Vb;->l:J

    .line 1348649
    iget-object v1, p0, LX/8TS;->j:LX/8TP;

    move-object v1, v1

    .line 1348650
    if-nez v1, :cond_0

    .line 1348651
    iget-wide v2, v0, LX/8Vb;->l:J

    invoke-static {p1, p2, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 1348652
    :cond_0
    iget-wide v0, v0, LX/8Vb;->m:J

    invoke-static {p3, p4, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    .line 1348653
    invoke-virtual {p0}, LX/8TS;->c()LX/8Vb;

    move-result-object v0

    iget-object v1, p0, LX/8TS;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8VZ;

    iget-object v4, p0, LX/8TS;->h:LX/8TW;

    if-eqz v4, :cond_1

    iget-object v4, p0, LX/8TS;->h:LX/8TW;

    .line 1348654
    iget-object v6, v4, LX/8TW;->a:Ljava/util/List;

    move-object v4, v6

    .line 1348655
    if-eqz v4, :cond_1

    iget-object v4, p0, LX/8TS;->h:LX/8TW;

    .line 1348656
    iget-object v6, v4, LX/8TW;->a:Ljava/util/List;

    move-object v4, v6

    .line 1348657
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v5, :cond_1

    move v4, v5

    .line 1348658
    :goto_0
    iget-object v6, p0, LX/8TS;->j:LX/8TP;

    move-object v6, v6

    .line 1348659
    iget-object v7, v6, LX/8TP;->b:Ljava/lang/String;

    move-object v6, v7

    .line 1348660
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    invoke-virtual/range {v1 .. v6}, LX/8VZ;->a(JZZZ)Ljava/lang/String;

    move-result-object v6

    move-object v1, v0

    move-wide v4, v8

    invoke-virtual/range {v1 .. v6}, LX/8Vb;->a(JJLjava/lang/String;)V

    .line 1348661
    return-void

    .line 1348662
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final a(LX/8TV;)V
    .locals 2

    .prologue
    .line 1348705
    iput-object p1, p0, LX/8TS;->m:LX/8TV;

    .line 1348706
    sget-object v0, LX/8TR;->b:[I

    iget-object v1, p0, LX/8TS;->m:LX/8TV;

    invoke-virtual {v1}, LX/8TV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1348707
    const/4 v0, 0x0

    iput-object v0, p0, LX/8TS;->r:Ljava/lang/String;

    .line 1348708
    :goto_0
    return-void

    .line 1348709
    :pswitch_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8TS;->r:Ljava/lang/String;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/8Vb;LX/8TJ;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1348633
    sget-object v0, LX/8TR;->a:[I

    invoke-virtual {p2}, LX/8TJ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1348634
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unhandled Play.SourceEffect"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1348635
    :pswitch_0
    iget-object v0, p1, LX/8Vb;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1348636
    new-instance v0, LX/8TP;

    sget-object v1, LX/8Ta;->Thread:LX/8Ta;

    iget-object v2, p1, LX/8Vb;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, LX/8TP;-><init>(LX/8Ta;Ljava/lang/String;)V

    iput-object v0, p0, LX/8TS;->j:LX/8TP;

    .line 1348637
    :goto_0
    :pswitch_1
    const/4 v0, 0x0

    iput v0, p0, LX/8TS;->n:I

    .line 1348638
    iget-object v0, p0, LX/8TS;->j:LX/8TP;

    .line 1348639
    iget-object v1, v0, LX/8TP;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1348640
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8TS;->p:LX/2th;

    if-eqz v0, :cond_0

    .line 1348641
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1348642
    invoke-virtual {p0}, LX/8TS;->c()LX/8Vb;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1348643
    invoke-virtual {p0, v0, v3}, LX/8TS;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 1348644
    :cond_0
    return-void

    .line 1348645
    :cond_1
    new-instance v0, LX/8TP;

    sget-object v1, LX/8Ta;->Thread:LX/8Ta;

    iget-object v2, p1, LX/8Vb;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, LX/8TP;-><init>(LX/8Ta;Ljava/lang/String;)V

    iput-object v0, p0, LX/8TS;->j:LX/8TP;

    goto :goto_0

    .line 1348646
    :pswitch_2
    new-instance v0, LX/8TP;

    sget-object v1, LX/8Ta;->None:LX/8Ta;

    invoke-direct {v0, v1, v3}, LX/8TP;-><init>(LX/8Ta;Ljava/lang/String;)V

    iput-object v0, p0, LX/8TS;->j:LX/8TP;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;)V
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1348620
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1348621
    :cond_0
    const-string v0, "GameSessionContextManager"

    const-string v1, "Empty opponent list provided to update opponent info"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1348622
    :cond_1
    :goto_0
    return-void

    .line 1348623
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1348624
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Vb;

    .line 1348625
    invoke-virtual {p0}, LX/8TS;->c()LX/8Vb;

    move-result-object v3

    iget-object v3, v3, LX/8Vb;->a:Ljava/lang/String;

    iget-object v4, v0, LX/8Vb;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1348626
    iget-wide v4, v0, LX/8Vb;->l:J

    iget-wide v6, v0, LX/8Vb;->m:J

    invoke-virtual {p0, v4, v5, v6, v7}, LX/8TS;->a(JJ)V

    goto :goto_1

    .line 1348627
    :cond_3
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1348628
    :cond_4
    new-instance v0, LX/8TW;

    invoke-direct {v0, v1}, LX/8TW;-><init>(Ljava/util/List;)V

    .line 1348629
    iput-object v0, p0, LX/8TS;->h:LX/8TW;

    .line 1348630
    iput-object p2, p0, LX/8TS;->o:Ljava/lang/String;

    .line 1348631
    iget-object v0, p0, LX/8TS;->p:LX/2th;

    if-eqz v0, :cond_1

    .line 1348632
    iget-object v0, p0, LX/8TS;->p:LX/2th;

    invoke-virtual {v0, v1}, LX/2th;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1348585
    iget-object v0, p0, LX/8TS;->d:LX/8TM;

    new-instance v1, LX/8TQ;

    invoke-direct {v1, p0}, LX/8TQ;-><init>(LX/8TS;)V

    .line 1348586
    iget-object v2, v0, LX/8TM;->a:LX/8Up;

    new-instance v3, LX/8TL;

    invoke-direct {v3, v0, p1, v1}, LX/8TL;-><init>(LX/8TM;Ljava/lang/String;LX/8TQ;)V

    .line 1348587
    new-instance v4, LX/8Um;

    invoke-direct {v4}, LX/8Um;-><init>()V

    move-object v4, v4

    .line 1348588
    const-string v5, "app_id"

    invoke-virtual {v4, v5, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1348589
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 1348590
    iget-object v5, v2, LX/8Up;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 1348591
    new-instance v5, LX/8Uo;

    invoke-direct {v5, v2, v3}, LX/8Uo;-><init>(LX/8Up;LX/8TL;)V

    .line 1348592
    iget-object v0, v2, LX/8Up;->b:LX/1Ck;

    const-string v1, "quicksilver_game_info_query"

    invoke-virtual {v0, v1, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1348593
    iget-object v0, p0, LX/8TS;->p:LX/2th;

    if-eqz v0, :cond_0

    .line 1348594
    iget-object v0, p0, LX/8TS;->p:LX/2th;

    invoke-virtual {v0, p1}, LX/2th;->a(Ljava/lang/String;)V

    .line 1348595
    :cond_0
    return-void
.end method

.method public final c()LX/8Vb;
    .locals 5

    .prologue
    .line 1348599
    iget-object v0, p0, LX/8TS;->g:LX/8Vb;

    if-nez v0, :cond_1

    .line 1348600
    iget-object v0, p0, LX/8TS;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1348601
    invoke-static {}, LX/8Vb;->b()LX/8Va;

    move-result-object v1

    .line 1348602
    if-eqz v0, :cond_0

    .line 1348603
    iget-object v2, v0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v2, v2

    .line 1348604
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1348605
    iput-object v3, v1, LX/8Va;->a:Ljava/lang/String;

    .line 1348606
    move-object v4, v1

    .line 1348607
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    .line 1348608
    iput-object v0, v4, LX/8Va;->d:Ljava/lang/String;

    .line 1348609
    move-object v0, v4

    .line 1348610
    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->j()Ljava/lang/String;

    move-result-object v4

    .line 1348611
    iput-object v4, v0, LX/8Va;->e:Ljava/lang/String;

    .line 1348612
    move-object v0, v0

    .line 1348613
    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->f()Ljava/lang/String;

    move-result-object v2

    .line 1348614
    iput-object v2, v0, LX/8Va;->c:Ljava/lang/String;

    .line 1348615
    move-object v0, v0

    .line 1348616
    iget-object v2, p0, LX/8TS;->c:LX/8Ve;

    invoke-virtual {v2, v3}, LX/8Ve;->a(Ljava/lang/String;)LX/8Vd;

    move-result-object v2

    .line 1348617
    iput-object v2, v0, LX/8Va;->k:LX/8Vd;

    .line 1348618
    :cond_0
    invoke-virtual {v1}, LX/8Va;->a()LX/8Vb;

    move-result-object v0

    iput-object v0, p0, LX/8TS;->g:LX/8Vb;

    .line 1348619
    :cond_1
    iget-object v0, p0, LX/8TS;->g:LX/8Vb;

    return-object v0
.end method

.method public final i()LX/8TU;
    .locals 1

    .prologue
    .line 1348596
    iget-object v0, p0, LX/8TS;->l:LX/8TU;

    if-nez v0, :cond_0

    .line 1348597
    new-instance v0, LX/8TU;

    invoke-direct {v0}, LX/8TU;-><init>()V

    iput-object v0, p0, LX/8TS;->l:LX/8TU;

    .line 1348598
    :cond_0
    iget-object v0, p0, LX/8TS;->l:LX/8TU;

    return-object v0
.end method
