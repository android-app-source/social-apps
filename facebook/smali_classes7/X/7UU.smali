.class public final LX/7UU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:F

.field public final synthetic b:F

.field public final synthetic c:F

.field public final synthetic d:F

.field public final synthetic e:F

.field public final synthetic f:F

.field public final synthetic g:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;FFFFFF)V
    .locals 0

    .prologue
    .line 1212978
    iput-object p1, p0, LX/7UU;->g:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iput p2, p0, LX/7UU;->a:F

    iput p3, p0, LX/7UU;->b:F

    iput p4, p0, LX/7UU;->c:F

    iput p5, p0, LX/7UU;->d:F

    iput p6, p0, LX/7UU;->e:F

    iput p7, p0, LX/7UU;->f:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 8

    .prologue
    .line 1212979
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1212980
    iget v1, p0, LX/7UU;->a:F

    mul-float/2addr v1, v0

    .line 1212981
    iget v2, p0, LX/7UU;->b:F

    mul-float/2addr v2, v0

    .line 1212982
    iget v3, p0, LX/7UU;->c:F

    mul-float/2addr v0, v3

    .line 1212983
    iget-object v3, p0, LX/7UU;->g:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget v4, p0, LX/7UU;->d:F

    add-float/2addr v1, v4

    iget v4, p0, LX/7UU;->e:F

    iget-object v5, p0, LX/7UU;->g:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget v5, v5, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->O:F

    add-float/2addr v4, v5

    iget v5, p0, LX/7UU;->f:F

    iget-object v6, p0, LX/7UU;->g:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget v6, v6, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->P:F

    add-float/2addr v5, v6

    invoke-virtual {v3, v1, v4, v5}, LX/7UQ;->a(FFF)V

    .line 1212984
    iget-object v1, p0, LX/7UU;->g:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget-object v3, p0, LX/7UU;->g:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget v3, v3, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->O:F

    sub-float v3, v2, v3

    float-to-double v4, v3

    iget-object v3, p0, LX/7UU;->g:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    iget v3, v3, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->P:F

    sub-float v3, v0, v3

    float-to-double v6, v3

    invoke-virtual {v1, v4, v5, v6, v7}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->a(DD)V

    .line 1212985
    iget-object v1, p0, LX/7UU;->g:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    .line 1212986
    iput v2, v1, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->O:F

    .line 1212987
    iget-object v1, p0, LX/7UU;->g:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    .line 1212988
    iput v0, v1, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->P:F

    .line 1212989
    return-void
.end method
