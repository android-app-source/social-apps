.class public final LX/7Xo;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "Lcom/facebook/zero/service/ZeroUpdateStatusManager;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/7Xo;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/zero/service/ZeroUpdateStatusManager;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1218681
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 1218682
    return-void
.end method

.method public static a(LX/0QB;)LX/7Xo;
    .locals 4

    .prologue
    .line 1218689
    sget-object v0, LX/7Xo;->a:LX/7Xo;

    if-nez v0, :cond_1

    .line 1218690
    const-class v1, LX/7Xo;

    monitor-enter v1

    .line 1218691
    :try_start_0
    sget-object v0, LX/7Xo;->a:LX/7Xo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1218692
    if-eqz v2, :cond_0

    .line 1218693
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1218694
    new-instance v3, LX/7Xo;

    const/16 p0, 0x3933

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/7Xo;-><init>(LX/0Ot;)V

    .line 1218695
    move-object v0, v3

    .line 1218696
    sput-object v0, LX/7Xo;->a:LX/7Xo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1218697
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1218698
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1218699
    :cond_1
    sget-object v0, LX/7Xo;->a:LX/7Xo;

    return-object v0

    .line 1218700
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1218701
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1218683
    check-cast p3, Lcom/facebook/zero/service/ZeroUpdateStatusManager;

    .line 1218684
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1218685
    const-string v1, "com.facebook.zero.ACTION_ZERO_UPDATE_STATUS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1218686
    const-string v0, "zero_status_to_update"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1218687
    invoke-virtual {p3, v0}, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->a(Ljava/lang/String;)V

    .line 1218688
    :cond_0
    return-void
.end method
