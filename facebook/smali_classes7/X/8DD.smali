.class public LX/8DD;
.super LX/8D6;
.source ""


# instance fields
.field private final a:LX/16H;

.field private b:LX/8DC;


# direct methods
.method public constructor <init>(LX/0lC;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0iA;LX/16H;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1312367
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/8D6;-><init>(LX/0lC;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0iA;LX/0SG;)V

    .line 1312368
    iput-object p6, p0, LX/8DD;->a:LX/16H;

    .line 1312369
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 1312370
    invoke-super {p0}, LX/8D6;->a()V

    .line 1312371
    iget-object v0, p0, LX/8DD;->a:LX/16H;

    iget-object v1, p0, LX/8DD;->b:LX/8DC;

    invoke-interface {v1}, LX/8DC;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/8DD;->b:LX/8DC;

    invoke-interface {v2}, LX/8DC;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/8DD;->b:LX/8DC;

    invoke-interface {v3}, LX/8DC;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/8DD;->b:LX/8DC;

    invoke-interface {v4}, LX/8DC;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/8DD;->b:LX/8DC;

    invoke-interface {v5}, LX/8DC;->f()Ljava/lang/String;

    move-result-object v5

    .line 1312372
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "saved_button_nux_imp"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1312373
    iput-object v1, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1312374
    move-object v6, v6

    .line 1312375
    const-string v7, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "object_id"

    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "collection_id"

    invoke-static {v3}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "surface"

    invoke-virtual {v6, v7, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "mechanism"

    invoke-virtual {v6, v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 1312376
    iget-object v7, v0, LX/16H;->a:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1312377
    return-void
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    .locals 1

    .prologue
    .line 1312378
    iget-object v0, p0, LX/8DD;->b:LX/8DC;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8DD;->b:LX/8DC;

    invoke-interface {v0}, LX/8DC;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1312379
    :cond_0
    const/4 v0, 0x0

    .line 1312380
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, LX/8D6;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v0

    goto :goto_0
.end method
