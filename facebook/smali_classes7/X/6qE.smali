.class public LX/6qE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;",
        "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3Ed;


# direct methods
.method public constructor <init>(LX/3Ed;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1150243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150244
    iput-object p1, p0, LX/6qE;->a:LX/3Ed;

    .line 1150245
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 1150246
    check-cast p1, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;

    .line 1150247
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1150248
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "pin"

    .line 1150249
    iget-object v3, p1, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1150250
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150251
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150252
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "idempotence_token"

    const-string v3, "%d_%s"

    iget-object v4, p0, LX/6qE;->a:LX/3Ed;

    invoke-virtual {v4}, LX/3Ed;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, "p2p_payment_pins"

    invoke-static {v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150253
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "p2p_payment_pins"

    .line 1150254
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1150255
    move-object v1, v1

    .line 1150256
    const-string v2, "POST"

    .line 1150257
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1150258
    move-object v1, v1

    .line 1150259
    const-string v2, "/%d/%s"

    .line 1150260
    iget-wide v6, p1, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->c:J

    move-wide v4, v6

    .line 1150261
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v4, "p2p_payment_pins"

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1150262
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1150263
    move-object v1, v1

    .line 1150264
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1150265
    move-object v0, v1

    .line 1150266
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 1150267
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1150268
    move-object v0, v0

    .line 1150269
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1150270
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1150271
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    return-object v0
.end method
