.class public final LX/7kV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/commerce/publishing/graphql/CommerceProductDeleteMutationModels$CommerceProductDeleteMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/7kY;


# direct methods
.method public constructor <init>(LX/7kY;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1232056
    iput-object p1, p0, LX/7kV;->b:LX/7kY;

    iput-object p2, p0, LX/7kV;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1232057
    iget-object v0, p0, LX/7kV;->b:LX/7kY;

    iget-object v0, v0, LX/7kY;->b:LX/7jB;

    sget-object v1, LX/7jD;->DELETE:LX/7jD;

    iget-object v2, p0, LX/7kV;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/7jK;->a(LX/7jD;Ljava/lang/String;)LX/7jK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1232058
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1232059
    iget-object v0, p0, LX/7kV;->b:LX/7kY;

    invoke-static {v0}, LX/7kY;->a$redex0(LX/7kY;)V

    .line 1232060
    iget-object v0, p0, LX/7kV;->b:LX/7kY;

    iget-object v0, v0, LX/7kY;->b:LX/7jB;

    sget-object v1, LX/7jD;->DELETE:LX/7jD;

    const/4 v2, 0x0

    iget-object v3, p0, LX/7kV;->a:Ljava/lang/String;

    invoke-static {v1, v2, v3}, LX/7jK;->a(LX/7jD;Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;Ljava/lang/String;)LX/7jK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1232061
    return-void
.end method
