.class public final LX/6tw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Dw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Dw",
        "<",
        "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1155410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1155411
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;
    .locals 3

    .prologue
    .line 1155412
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 1155413
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 1155414
    sget-object v1, LX/6tr;->PREPARE_CHECKOUT:LX/6tr;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1155415
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    .line 1155416
    sget-object v2, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1155417
    sget-object v2, LX/6tr;->VERIFY_PAYMENT_METHOD:LX/6tr;

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1155418
    sget-object v2, LX/6tr;->PROCESSING_VERIFY_PAYMENT_METHOD:LX/6tr;

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1155419
    :cond_0
    sget-object v2, LX/6rp;->AUTHENTICATION:LX/6rp;

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1155420
    sget-object v1, LX/6tr;->CHECK_AUTHENTICATION:LX/6tr;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1155421
    sget-object v1, LX/6tr;->PROCESSING_CHECK_AUTHENTICATION:LX/6tr;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1155422
    :cond_1
    sget-object v1, LX/6tr;->SEND_PAYMENT:LX/6tr;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1155423
    sget-object v1, LX/6tr;->PROCESSING_SEND_PAYMENT:LX/6tr;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1155424
    sget-object v1, LX/6tr;->FINISH:LX/6tr;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1155425
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
