.class public final enum LX/6vb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6vb;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6vb;

.field public static final enum EMAIL:LX/6vb;

.field public static final enum NAME:LX/6vb;

.field public static final enum PHONE_NUMBER:LX/6vb;


# instance fields
.field private final mContactInfoFormStyle:LX/6vY;

.field private final mSectionType:LX/6va;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1157051
    new-instance v0, LX/6vb;

    const-string v1, "EMAIL"

    sget-object v2, LX/6vY;->EMAIL:LX/6vY;

    sget-object v3, LX/6va;->CONTACT_EMAIL:LX/6va;

    invoke-direct {v0, v1, v4, v2, v3}, LX/6vb;-><init>(Ljava/lang/String;ILX/6vY;LX/6va;)V

    sput-object v0, LX/6vb;->EMAIL:LX/6vb;

    .line 1157052
    new-instance v0, LX/6vb;

    const-string v1, "NAME"

    sget-object v2, LX/6vY;->NAME:LX/6vY;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v5, v2, v3}, LX/6vb;-><init>(Ljava/lang/String;ILX/6vY;LX/6va;)V

    sput-object v0, LX/6vb;->NAME:LX/6vb;

    .line 1157053
    new-instance v0, LX/6vb;

    const-string v1, "PHONE_NUMBER"

    sget-object v2, LX/6vY;->PHONE_NUMBER:LX/6vY;

    sget-object v3, LX/6va;->CONTACT_PHONE_NUMBER:LX/6va;

    invoke-direct {v0, v1, v6, v2, v3}, LX/6vb;-><init>(Ljava/lang/String;ILX/6vY;LX/6va;)V

    sput-object v0, LX/6vb;->PHONE_NUMBER:LX/6vb;

    .line 1157054
    const/4 v0, 0x3

    new-array v0, v0, [LX/6vb;

    sget-object v1, LX/6vb;->EMAIL:LX/6vb;

    aput-object v1, v0, v4

    sget-object v1, LX/6vb;->NAME:LX/6vb;

    aput-object v1, v0, v5

    sget-object v1, LX/6vb;->PHONE_NUMBER:LX/6vb;

    aput-object v1, v0, v6

    sput-object v0, LX/6vb;->$VALUES:[LX/6vb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/6vY;LX/6va;)V
    .locals 0
    .param p4    # LX/6va;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6vY;",
            "LX/6va;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1157059
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1157060
    iput-object p3, p0, LX/6vb;->mContactInfoFormStyle:LX/6vY;

    .line 1157061
    iput-object p4, p0, LX/6vb;->mSectionType:LX/6va;

    .line 1157062
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6vb;
    .locals 1

    .prologue
    .line 1157058
    const-class v0, LX/6vb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6vb;

    return-object v0
.end method

.method public static values()[LX/6vb;
    .locals 1

    .prologue
    .line 1157057
    sget-object v0, LX/6vb;->$VALUES:[LX/6vb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6vb;

    return-object v0
.end method


# virtual methods
.method public final getContactInfoFormStyle()LX/6vY;
    .locals 1

    .prologue
    .line 1157056
    iget-object v0, p0, LX/6vb;->mContactInfoFormStyle:LX/6vY;

    return-object v0
.end method

.method public final getSectionType()LX/6va;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1157055
    iget-object v0, p0, LX/6vb;->mSectionType:LX/6va;

    return-object v0
.end method
