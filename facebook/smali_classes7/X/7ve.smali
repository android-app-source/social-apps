.class public final LX/7ve;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationAsyncModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7vr;

.field public final synthetic b:LX/7vi;


# direct methods
.method public constructor <init>(LX/7vi;LX/7vr;)V
    .locals 0

    .prologue
    .line 1274942
    iput-object p1, p0, LX/7ve;->b:LX/7vi;

    iput-object p2, p0, LX/7ve;->a:LX/7vr;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1274943
    instance-of v0, p1, LX/4Ua;

    if-eqz v0, :cond_0

    .line 1274944
    check-cast p1, LX/4Ua;

    iget-object v0, p0, LX/7ve;->a:LX/7vr;

    invoke-static {p1, v0}, LX/7vi;->b(LX/4Ua;LX/7vr;)V

    .line 1274945
    :goto_0
    return-void

    .line 1274946
    :cond_0
    iget-object v0, p0, LX/7ve;->a:LX/7vr;

    invoke-virtual {v0, p1}, LX/7vr;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1274947
    check-cast p1, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationAsyncModel;

    .line 1274948
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationAsyncModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1274949
    :cond_0
    iget-object v0, p0, LX/7ve;->a:LX/7vr;

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null result from GraphQL"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/7vr;->a(Ljava/lang/Throwable;)V

    .line 1274950
    :cond_1
    :goto_0
    return-void

    .line 1274951
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationAsyncModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationAsyncModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->n()Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    move-result-object v0

    .line 1274952
    :goto_1
    iget-object v1, p0, LX/7ve;->b:LX/7vi;

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationAsyncModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;

    move-result-object v2

    iget-object v3, p0, LX/7ve;->a:LX/7vr;

    invoke-static {v1, v2, v3}, LX/7vi;->a$redex0(LX/7vi;Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;LX/7vr;)V

    .line 1274953
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->RESERVED:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    if-ne v0, v1, :cond_1

    .line 1274954
    iget-object v0, p0, LX/7ve;->b:LX/7vi;

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationAsyncModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/7ve;->a:LX/7vr;

    invoke-virtual {v0, v1, v2}, LX/7vi;->a(Ljava/lang/String;LX/7vr;)V

    goto :goto_0

    .line 1274955
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    goto :goto_1
.end method
