.class public LX/8SS;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/ToggleButton;

.field private final d:Lcom/facebook/fbui/glyph/GlyphView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Lcom/facebook/widget/FbImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1346662
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1346663
    const-class v0, LX/8SS;

    invoke-static {v0, p0}, LX/8SS;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1346664
    const v0, 0x7f03013b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1346665
    const v0, 0x7f0d051b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/8SS;->b:Landroid/view/View;

    .line 1346666
    const v0, 0x7f0d05fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, LX/8SS;->c:Landroid/widget/ToggleButton;

    .line 1346667
    const v0, 0x7f0d05ff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/8SS;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1346668
    const v0, 0x7f0d0600

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8SS;->e:Landroid/widget/TextView;

    .line 1346669
    const v0, 0x7f0d0601

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8SS;->f:Landroid/widget/TextView;

    .line 1346670
    const v0, 0x7f0d0602

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, LX/8SS;->g:Lcom/facebook/widget/FbImageView;

    .line 1346671
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/8SS;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object p0, p1, LX/8SS;->a:LX/0wM;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;LX/03R;ZZ)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1346672
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1346673
    iget-object v0, p0, LX/8SS;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1346674
    iget-object v0, p0, LX/8SS;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1346675
    :goto_0
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1346676
    iget-object v0, p0, LX/8SS;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1346677
    iget-object v0, p0, LX/8SS;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1346678
    :goto_1
    if-eqz p3, :cond_2

    .line 1346679
    iget-object v0, p0, LX/8SS;->d:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v3, p0, LX/8SS;->a:LX/0wM;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const v5, -0x958e80

    invoke-virtual {v3, v4, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1346680
    iget-object v0, p0, LX/8SS;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1346681
    :goto_2
    iget-object v3, p0, LX/8SS;->c:Landroid/widget/ToggleButton;

    sget-object v0, LX/03R;->YES:LX/03R;

    if-ne p4, v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 1346682
    iget-object v3, p0, LX/8SS;->c:Landroid/widget/ToggleButton;

    sget-object v0, LX/03R;->UNSET:LX/03R;

    if-ne p4, v0, :cond_4

    const/4 v0, 0x4

    :goto_4
    invoke-virtual {v3, v0}, Landroid/widget/ToggleButton;->setVisibility(I)V

    .line 1346683
    iget-object v3, p0, LX/8SS;->b:Landroid/view/View;

    if-eqz p5, :cond_5

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_5
    invoke-virtual {v3, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1346684
    iget-object v0, p0, LX/8SS;->g:Lcom/facebook/widget/FbImageView;

    if-eqz p6, :cond_6

    :goto_6
    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1346685
    return-void

    .line 1346686
    :cond_0
    iget-object v0, p0, LX/8SS;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1346687
    :cond_1
    iget-object v0, p0, LX/8SS;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 1346688
    :cond_2
    iget-object v0, p0, LX/8SS;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_2

    :cond_3
    move v0, v1

    .line 1346689
    goto :goto_3

    :cond_4
    move v0, v1

    .line 1346690
    goto :goto_4

    .line 1346691
    :cond_5
    const v0, 0x3e99999a    # 0.3f

    goto :goto_5

    :cond_6
    move v1, v2

    .line 1346692
    goto :goto_6
.end method
