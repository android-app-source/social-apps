.class public final enum LX/8LS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8LS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8LS;

.field public static final enum ALBUM:LX/8LS;

.field public static final enum COMMENT_VIDEO:LX/8LS;

.field public static final enum COVER_PHOTO:LX/8LS;

.field public static final enum GIF:LX/8LS;

.field public static final enum LIFE_EVENT:LX/8LS;

.field public static final enum LIVE_VIDEO:LX/8LS;

.field public static final enum MENU_PHOTO:LX/8LS;

.field public static final enum MOMENTS_VIDEO:LX/8LS;

.field public static final enum MULTIMEDIA:LX/8LS;

.field public static final enum OWN_TIMELINE:LX/8LS;

.field public static final enum PHOTO_REVIEW:LX/8LS;

.field public static final enum PLACE_PHOTO:LX/8LS;

.field public static final enum PRODUCT_IMAGE:LX/8LS;

.field public static final enum PROFILE_INTRO_CARD_VIDEO:LX/8LS;

.field public static final enum PROFILE_PIC:LX/8LS;

.field public static final enum PROFILE_VIDEO:LX/8LS;

.field public static final enum TARGET:LX/8LS;

.field public static final enum VIDEO:LX/8LS;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1332069
    new-instance v0, LX/8LS;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v3}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->ALBUM:LX/8LS;

    .line 1332070
    new-instance v0, LX/8LS;

    const-string v1, "OWN_TIMELINE"

    invoke-direct {v0, v1, v4}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->OWN_TIMELINE:LX/8LS;

    .line 1332071
    new-instance v0, LX/8LS;

    const-string v1, "TARGET"

    invoke-direct {v0, v1, v5}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->TARGET:LX/8LS;

    .line 1332072
    new-instance v0, LX/8LS;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v6}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->VIDEO:LX/8LS;

    .line 1332073
    new-instance v0, LX/8LS;

    const-string v1, "PHOTO_REVIEW"

    invoke-direct {v0, v1, v7}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->PHOTO_REVIEW:LX/8LS;

    .line 1332074
    new-instance v0, LX/8LS;

    const-string v1, "LIFE_EVENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->LIFE_EVENT:LX/8LS;

    .line 1332075
    new-instance v0, LX/8LS;

    const-string v1, "PROFILE_PIC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->PROFILE_PIC:LX/8LS;

    .line 1332076
    new-instance v0, LX/8LS;

    const-string v1, "PROFILE_VIDEO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->PROFILE_VIDEO:LX/8LS;

    .line 1332077
    new-instance v0, LX/8LS;

    const-string v1, "PROFILE_INTRO_CARD_VIDEO"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->PROFILE_INTRO_CARD_VIDEO:LX/8LS;

    .line 1332078
    new-instance v0, LX/8LS;

    const-string v1, "COVER_PHOTO"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->COVER_PHOTO:LX/8LS;

    .line 1332079
    new-instance v0, LX/8LS;

    const-string v1, "PLACE_PHOTO"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->PLACE_PHOTO:LX/8LS;

    .line 1332080
    new-instance v0, LX/8LS;

    const-string v1, "MENU_PHOTO"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->MENU_PHOTO:LX/8LS;

    .line 1332081
    new-instance v0, LX/8LS;

    const-string v1, "PRODUCT_IMAGE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->PRODUCT_IMAGE:LX/8LS;

    .line 1332082
    new-instance v0, LX/8LS;

    const-string v1, "MULTIMEDIA"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->MULTIMEDIA:LX/8LS;

    .line 1332083
    new-instance v0, LX/8LS;

    const-string v1, "MOMENTS_VIDEO"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->MOMENTS_VIDEO:LX/8LS;

    .line 1332084
    new-instance v0, LX/8LS;

    const-string v1, "COMMENT_VIDEO"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->COMMENT_VIDEO:LX/8LS;

    .line 1332085
    new-instance v0, LX/8LS;

    const-string v1, "LIVE_VIDEO"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->LIVE_VIDEO:LX/8LS;

    .line 1332086
    new-instance v0, LX/8LS;

    const-string v1, "GIF"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/8LS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8LS;->GIF:LX/8LS;

    .line 1332087
    const/16 v0, 0x12

    new-array v0, v0, [LX/8LS;

    sget-object v1, LX/8LS;->ALBUM:LX/8LS;

    aput-object v1, v0, v3

    sget-object v1, LX/8LS;->OWN_TIMELINE:LX/8LS;

    aput-object v1, v0, v4

    sget-object v1, LX/8LS;->TARGET:LX/8LS;

    aput-object v1, v0, v5

    sget-object v1, LX/8LS;->VIDEO:LX/8LS;

    aput-object v1, v0, v6

    sget-object v1, LX/8LS;->PHOTO_REVIEW:LX/8LS;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8LS;->LIFE_EVENT:LX/8LS;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8LS;->PROFILE_PIC:LX/8LS;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8LS;->PROFILE_VIDEO:LX/8LS;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8LS;->PROFILE_INTRO_CARD_VIDEO:LX/8LS;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8LS;->COVER_PHOTO:LX/8LS;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8LS;->PLACE_PHOTO:LX/8LS;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8LS;->MENU_PHOTO:LX/8LS;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/8LS;->PRODUCT_IMAGE:LX/8LS;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/8LS;->MULTIMEDIA:LX/8LS;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/8LS;->MOMENTS_VIDEO:LX/8LS;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/8LS;->COMMENT_VIDEO:LX/8LS;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/8LS;->LIVE_VIDEO:LX/8LS;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/8LS;->GIF:LX/8LS;

    aput-object v2, v0, v1

    sput-object v0, LX/8LS;->$VALUES:[LX/8LS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1332088
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8LS;
    .locals 1

    .prologue
    .line 1332089
    const-class v0, LX/8LS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8LS;

    return-object v0
.end method

.method public static values()[LX/8LS;
    .locals 1

    .prologue
    .line 1332090
    sget-object v0, LX/8LS;->$VALUES:[LX/8LS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8LS;

    return-object v0
.end method
