.class public final LX/79j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Sk;


# direct methods
.method public constructor <init>(LX/1Sk;)V
    .locals 0

    .prologue
    .line 1174377
    iput-object p1, p0, LX/79j;->a:LX/1Sk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1174378
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1174379
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1174380
    if-eqz p1, :cond_0

    .line 1174381
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1174382
    if-eqz v0, :cond_0

    .line 1174383
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1174384
    check-cast v0, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel;->a()Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1174385
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1174386
    check-cast v0, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel;->a()Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel;->a()Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1174387
    invoke-virtual {v0}, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel;->j()I

    move-result v1

    .line 1174388
    invoke-virtual {v0}, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel;->a()Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/79k;->convertString(Ljava/lang/String;)LX/79k;

    move-result-object v0

    .line 1174389
    iget-object v2, p0, LX/79j;->a:LX/1Sk;

    invoke-static {v2, v1, v0}, LX/1Sk;->a$redex0(LX/1Sk;ILX/79k;)V

    .line 1174390
    :cond_0
    return-void
.end method
