.class public final enum LX/7yv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7yv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7yv;

.field public static final enum HANDLE:LX/7yv;

.field public static final enum MEDIA_ID:LX/7yv;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1280443
    new-instance v0, LX/7yv;

    const-string v1, "MEDIA_ID"

    invoke-direct {v0, v1, v2}, LX/7yv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7yv;->MEDIA_ID:LX/7yv;

    .line 1280444
    new-instance v0, LX/7yv;

    const-string v1, "HANDLE"

    invoke-direct {v0, v1, v3}, LX/7yv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7yv;->HANDLE:LX/7yv;

    .line 1280445
    const/4 v0, 0x2

    new-array v0, v0, [LX/7yv;

    sget-object v1, LX/7yv;->MEDIA_ID:LX/7yv;

    aput-object v1, v0, v2

    sget-object v1, LX/7yv;->HANDLE:LX/7yv;

    aput-object v1, v0, v3

    sput-object v0, LX/7yv;->$VALUES:[LX/7yv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1280442
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7yv;
    .locals 1

    .prologue
    .line 1280441
    const-class v0, LX/7yv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7yv;

    return-object v0
.end method

.method public static values()[LX/7yv;
    .locals 1

    .prologue
    .line 1280440
    sget-object v0, LX/7yv;->$VALUES:[LX/7yv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7yv;

    return-object v0
.end method
