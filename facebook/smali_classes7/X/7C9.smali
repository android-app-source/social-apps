.class public final enum LX/7C9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7C9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7C9;

.field public static final enum ATTACHED_HEADERS:LX/7C9;

.field public static final enum CELEBRITY:LX/7C9;

.field public static final enum CELEBRITY_TOP_MEDIA:LX/7C9;

.field public static final enum COMBINED_MEDIA:LX/7C9;

.field public static final enum COMMERCE_GROUPS_SEARCH:LX/7C9;

.field public static final enum DENSE_RESULT_PAGE:LX/7C9;

.field public static final enum DISAMBIGUATION_HIDE_LOW_QUALITY_ENTITY_MODULES:LX/7C9;

.field public static final enum DISAMBIGUATION_INFINITE_SCROLL:LX/7C9;

.field public static final enum DISAMBIGUATION_MULTIPLE_POSTS_PER_MODULE:LX/7C9;

.field public static final enum DISAMBIGUATION_NON_BLENDER_ENTITY_MODULES:LX/7C9;

.field public static final enum DISAMBIGUATION_SEND_EMPTY_MODULES:LX/7C9;

.field public static final enum DISAMBIGUATION_TABLE_OF_CONTENTS_POST_MODULE:LX/7C9;

.field public static final enum FAST_FILTERS:LX/7C9;

.field public static final enum FILTERS:LX/7C9;

.field public static final enum FILTERS_AS_SEE_MORE:LX/7C9;

.field public static final enum I18N_POST_SEARCH_EXPANSION:LX/7C9;

.field public static final enum I18N_POST_SEARCH_USER_EXPANSION:LX/7C9;

.field public static final enum KEYWORD_ONLY:LX/7C9;

.field public static final enum POST_SETS:LX/7C9;


# instance fields
.field public final serverValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1179880
    new-instance v0, LX/7C9;

    const-string v1, "I18N_POST_SEARCH_EXPANSION"

    const-string v2, "i18n_post_search_expansion"

    invoke-direct {v0, v1, v4, v2}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->I18N_POST_SEARCH_EXPANSION:LX/7C9;

    .line 1179881
    new-instance v0, LX/7C9;

    const-string v1, "I18N_POST_SEARCH_USER_EXPANSION"

    const-string v2, "i18n_post_search_user_expansion"

    invoke-direct {v0, v1, v5, v2}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->I18N_POST_SEARCH_USER_EXPANSION:LX/7C9;

    .line 1179882
    new-instance v0, LX/7C9;

    const-string v1, "CELEBRITY"

    const-string v2, "celebrity"

    invoke-direct {v0, v1, v6, v2}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->CELEBRITY:LX/7C9;

    .line 1179883
    new-instance v0, LX/7C9;

    const-string v1, "CELEBRITY_TOP_MEDIA"

    const-string v2, "celebrity_top_media"

    invoke-direct {v0, v1, v7, v2}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->CELEBRITY_TOP_MEDIA:LX/7C9;

    .line 1179884
    new-instance v0, LX/7C9;

    const-string v1, "COMBINED_MEDIA"

    const-string v2, "COMBINED_MEDIA"

    invoke-direct {v0, v1, v8, v2}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->COMBINED_MEDIA:LX/7C9;

    .line 1179885
    new-instance v0, LX/7C9;

    const-string v1, "COMMERCE_GROUPS_SEARCH"

    const/4 v2, 0x5

    const-string v3, "commerce_groups_search"

    invoke-direct {v0, v1, v2, v3}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->COMMERCE_GROUPS_SEARCH:LX/7C9;

    .line 1179886
    new-instance v0, LX/7C9;

    const-string v1, "DENSE_RESULT_PAGE"

    const/4 v2, 0x6

    const-string v3, "dense_result_page"

    invoke-direct {v0, v1, v2, v3}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->DENSE_RESULT_PAGE:LX/7C9;

    .line 1179887
    new-instance v0, LX/7C9;

    const-string v1, "POST_SETS"

    const/4 v2, 0x7

    const-string v3, "post_sets"

    invoke-direct {v0, v1, v2, v3}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->POST_SETS:LX/7C9;

    .line 1179888
    new-instance v0, LX/7C9;

    const-string v1, "DISAMBIGUATION_TABLE_OF_CONTENTS_POST_MODULE"

    const/16 v2, 0x8

    const-string v3, "disambiguation_table_of_contents_post_module"

    invoke-direct {v0, v1, v2, v3}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->DISAMBIGUATION_TABLE_OF_CONTENTS_POST_MODULE:LX/7C9;

    .line 1179889
    new-instance v0, LX/7C9;

    const-string v1, "DISAMBIGUATION_NON_BLENDER_ENTITY_MODULES"

    const/16 v2, 0x9

    const-string v3, "disambiguation_non_blender_entity_modules"

    invoke-direct {v0, v1, v2, v3}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->DISAMBIGUATION_NON_BLENDER_ENTITY_MODULES:LX/7C9;

    .line 1179890
    new-instance v0, LX/7C9;

    const-string v1, "DISAMBIGUATION_MULTIPLE_POSTS_PER_MODULE"

    const/16 v2, 0xa

    const-string v3, "disambiguation_multiple_posts_per_module"

    invoke-direct {v0, v1, v2, v3}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->DISAMBIGUATION_MULTIPLE_POSTS_PER_MODULE:LX/7C9;

    .line 1179891
    new-instance v0, LX/7C9;

    const-string v1, "DISAMBIGUATION_INFINITE_SCROLL"

    const/16 v2, 0xb

    const-string v3, "infinite_disambiguation_serp"

    invoke-direct {v0, v1, v2, v3}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->DISAMBIGUATION_INFINITE_SCROLL:LX/7C9;

    .line 1179892
    new-instance v0, LX/7C9;

    const-string v1, "DISAMBIGUATION_SEND_EMPTY_MODULES"

    const/16 v2, 0xc

    const-string v3, "disambiguation_send_empty_modules"

    invoke-direct {v0, v1, v2, v3}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->DISAMBIGUATION_SEND_EMPTY_MODULES:LX/7C9;

    .line 1179893
    new-instance v0, LX/7C9;

    const-string v1, "DISAMBIGUATION_HIDE_LOW_QUALITY_ENTITY_MODULES"

    const/16 v2, 0xd

    const-string v3, "disambiguation_hide_low_quality_entity_modules"

    invoke-direct {v0, v1, v2, v3}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->DISAMBIGUATION_HIDE_LOW_QUALITY_ENTITY_MODULES:LX/7C9;

    .line 1179894
    new-instance v0, LX/7C9;

    const-string v1, "FAST_FILTERS"

    const/16 v2, 0xe

    const-string v3, "FAST_FILTERS"

    invoke-direct {v0, v1, v2, v3}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->FAST_FILTERS:LX/7C9;

    .line 1179895
    new-instance v0, LX/7C9;

    const-string v1, "FILTERS"

    const/16 v2, 0xf

    const-string v3, "FILTERS"

    invoke-direct {v0, v1, v2, v3}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->FILTERS:LX/7C9;

    .line 1179896
    new-instance v0, LX/7C9;

    const-string v1, "FILTERS_AS_SEE_MORE"

    const/16 v2, 0x10

    const-string v3, "FILTERS_AS_SEE_MORE"

    invoke-direct {v0, v1, v2, v3}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->FILTERS_AS_SEE_MORE:LX/7C9;

    .line 1179897
    new-instance v0, LX/7C9;

    const-string v1, "KEYWORD_ONLY"

    const/16 v2, 0x11

    const-string v3, "keyword_only"

    invoke-direct {v0, v1, v2, v3}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->KEYWORD_ONLY:LX/7C9;

    .line 1179898
    new-instance v0, LX/7C9;

    const-string v1, "ATTACHED_HEADERS"

    const/16 v2, 0x12

    const-string v3, "attached_headers"

    invoke-direct {v0, v1, v2, v3}, LX/7C9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C9;->ATTACHED_HEADERS:LX/7C9;

    .line 1179899
    const/16 v0, 0x13

    new-array v0, v0, [LX/7C9;

    sget-object v1, LX/7C9;->I18N_POST_SEARCH_EXPANSION:LX/7C9;

    aput-object v1, v0, v4

    sget-object v1, LX/7C9;->I18N_POST_SEARCH_USER_EXPANSION:LX/7C9;

    aput-object v1, v0, v5

    sget-object v1, LX/7C9;->CELEBRITY:LX/7C9;

    aput-object v1, v0, v6

    sget-object v1, LX/7C9;->CELEBRITY_TOP_MEDIA:LX/7C9;

    aput-object v1, v0, v7

    sget-object v1, LX/7C9;->COMBINED_MEDIA:LX/7C9;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7C9;->COMMERCE_GROUPS_SEARCH:LX/7C9;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7C9;->DENSE_RESULT_PAGE:LX/7C9;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7C9;->POST_SETS:LX/7C9;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7C9;->DISAMBIGUATION_TABLE_OF_CONTENTS_POST_MODULE:LX/7C9;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7C9;->DISAMBIGUATION_NON_BLENDER_ENTITY_MODULES:LX/7C9;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7C9;->DISAMBIGUATION_MULTIPLE_POSTS_PER_MODULE:LX/7C9;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7C9;->DISAMBIGUATION_INFINITE_SCROLL:LX/7C9;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7C9;->DISAMBIGUATION_SEND_EMPTY_MODULES:LX/7C9;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/7C9;->DISAMBIGUATION_HIDE_LOW_QUALITY_ENTITY_MODULES:LX/7C9;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/7C9;->FAST_FILTERS:LX/7C9;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/7C9;->FILTERS:LX/7C9;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/7C9;->FILTERS_AS_SEE_MORE:LX/7C9;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/7C9;->KEYWORD_ONLY:LX/7C9;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/7C9;->ATTACHED_HEADERS:LX/7C9;

    aput-object v2, v0, v1

    sput-object v0, LX/7C9;->$VALUES:[LX/7C9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1179877
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1179878
    iput-object p3, p0, LX/7C9;->serverValue:Ljava/lang/String;

    .line 1179879
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7C9;
    .locals 1

    .prologue
    .line 1179875
    const-class v0, LX/7C9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7C9;

    return-object v0
.end method

.method public static values()[LX/7C9;
    .locals 1

    .prologue
    .line 1179876
    sget-object v0, LX/7C9;->$VALUES:[LX/7C9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7C9;

    return-object v0
.end method
