.class public final LX/7UM;
.super LX/5ze;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/widget/images/UrlImage;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/images/UrlImage;)V
    .locals 0

    .prologue
    .line 1211730
    iput-object p1, p0, LX/7UM;->a:Lcom/facebook/widget/images/UrlImage;

    invoke-direct {p0}, LX/5ze;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(I)V
    .locals 4

    .prologue
    .line 1211731
    iget-object v0, p0, LX/7UM;->a:Lcom/facebook/widget/images/UrlImage;

    iget-object v0, v0, Lcom/facebook/widget/images/UrlImage;->E:LX/7UL;

    sget-object v1, LX/7UL;->PROGRESS_BAR_DETERMINATE_WITH_PLACEHOLDER:LX/7UL;

    if-eq v0, v1, :cond_1

    .line 1211732
    :cond_0
    :goto_0
    return-void

    .line 1211733
    :cond_1
    iget-object v0, p0, LX/7UM;->a:Lcom/facebook/widget/images/UrlImage;

    iget-object v0, v0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7UM;->a:Lcom/facebook/widget/images/UrlImage;

    iget v0, v0, Lcom/facebook/widget/images/UrlImage;->V:I

    if-le p1, v0, :cond_0

    .line 1211734
    iget-object v0, p0, LX/7UM;->a:Lcom/facebook/widget/images/UrlImage;

    .line 1211735
    iput p1, v0, Lcom/facebook/widget/images/UrlImage;->V:I

    .line 1211736
    iget-object v0, p0, LX/7UM;->a:Lcom/facebook/widget/images/UrlImage;

    iget-object v0, v0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

    iget-object v1, p0, LX/7UM;->a:Lcom/facebook/widget/images/UrlImage;

    iget v1, v1, Lcom/facebook/widget/images/UrlImage;->V:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgress(J)V

    goto :goto_0
.end method
