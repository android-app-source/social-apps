.class public final enum LX/7Rp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Rp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Rp;

.field public static final enum APP_BACKGROUNDED:LX/7Rp;

.field public static final enum APP_FOREGROUNDED:LX/7Rp;

.field public static final enum PLAYER_DISMISSED:LX/7Rp;

.field public static final enum PLAYER_DOCKED:LX/7Rp;

.field public static final enum PLAYER_TAPPED:LX/7Rp;

.field public static final enum SESSION_FINISHED:LX/7Rp;

.field public static final enum SESSION_PAUSED:LX/7Rp;

.field public static final enum SESSION_RESUMED:LX/7Rp;

.field public static final enum SESSION_STARTED:LX/7Rp;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1207882
    new-instance v0, LX/7Rp;

    const-string v1, "SESSION_STARTED"

    const-string v2, "wng_session_started"

    invoke-direct {v0, v1, v4, v2}, LX/7Rp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rp;->SESSION_STARTED:LX/7Rp;

    .line 1207883
    new-instance v0, LX/7Rp;

    const-string v1, "SESSION_PAUSED"

    const-string v2, "wng_session_paused"

    invoke-direct {v0, v1, v5, v2}, LX/7Rp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rp;->SESSION_PAUSED:LX/7Rp;

    .line 1207884
    new-instance v0, LX/7Rp;

    const-string v1, "SESSION_RESUMED"

    const-string v2, "wng_session_resumed"

    invoke-direct {v0, v1, v6, v2}, LX/7Rp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rp;->SESSION_RESUMED:LX/7Rp;

    .line 1207885
    new-instance v0, LX/7Rp;

    const-string v1, "SESSION_FINISHED"

    const-string v2, "wng_session_finished"

    invoke-direct {v0, v1, v7, v2}, LX/7Rp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rp;->SESSION_FINISHED:LX/7Rp;

    .line 1207886
    new-instance v0, LX/7Rp;

    const-string v1, "PLAYER_TAPPED"

    const-string v2, "wng_player_tapped"

    invoke-direct {v0, v1, v8, v2}, LX/7Rp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rp;->PLAYER_TAPPED:LX/7Rp;

    .line 1207887
    new-instance v0, LX/7Rp;

    const-string v1, "PLAYER_DISMISSED"

    const/4 v2, 0x5

    const-string v3, "wng_player_dismissed"

    invoke-direct {v0, v1, v2, v3}, LX/7Rp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rp;->PLAYER_DISMISSED:LX/7Rp;

    .line 1207888
    new-instance v0, LX/7Rp;

    const-string v1, "PLAYER_DOCKED"

    const/4 v2, 0x6

    const-string v3, "wng_player_docked"

    invoke-direct {v0, v1, v2, v3}, LX/7Rp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rp;->PLAYER_DOCKED:LX/7Rp;

    .line 1207889
    new-instance v0, LX/7Rp;

    const-string v1, "APP_BACKGROUNDED"

    const/4 v2, 0x7

    const-string v3, "wng_app_backgrounded"

    invoke-direct {v0, v1, v2, v3}, LX/7Rp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rp;->APP_BACKGROUNDED:LX/7Rp;

    .line 1207890
    new-instance v0, LX/7Rp;

    const-string v1, "APP_FOREGROUNDED"

    const/16 v2, 0x8

    const-string v3, "wng_app_foregrounded"

    invoke-direct {v0, v1, v2, v3}, LX/7Rp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rp;->APP_FOREGROUNDED:LX/7Rp;

    .line 1207891
    const/16 v0, 0x9

    new-array v0, v0, [LX/7Rp;

    sget-object v1, LX/7Rp;->SESSION_STARTED:LX/7Rp;

    aput-object v1, v0, v4

    sget-object v1, LX/7Rp;->SESSION_PAUSED:LX/7Rp;

    aput-object v1, v0, v5

    sget-object v1, LX/7Rp;->SESSION_RESUMED:LX/7Rp;

    aput-object v1, v0, v6

    sget-object v1, LX/7Rp;->SESSION_FINISHED:LX/7Rp;

    aput-object v1, v0, v7

    sget-object v1, LX/7Rp;->PLAYER_TAPPED:LX/7Rp;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7Rp;->PLAYER_DISMISSED:LX/7Rp;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7Rp;->PLAYER_DOCKED:LX/7Rp;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7Rp;->APP_BACKGROUNDED:LX/7Rp;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7Rp;->APP_FOREGROUNDED:LX/7Rp;

    aput-object v2, v0, v1

    sput-object v0, LX/7Rp;->$VALUES:[LX/7Rp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1207892
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1207893
    iput-object p3, p0, LX/7Rp;->value:Ljava/lang/String;

    .line 1207894
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/7Rp;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1207895
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1207896
    :cond_0
    :goto_0
    return-object v0

    .line 1207897
    :cond_1
    invoke-static {}, LX/7Rp;->values()[LX/7Rp;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 1207898
    iget-object v5, v1, LX/7Rp;->value:Ljava/lang/String;

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 1207899
    goto :goto_0

    .line 1207900
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Rp;
    .locals 1

    .prologue
    .line 1207901
    const-class v0, LX/7Rp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Rp;

    return-object v0
.end method

.method public static values()[LX/7Rp;
    .locals 1

    .prologue
    .line 1207902
    sget-object v0, LX/7Rp;->$VALUES:[LX/7Rp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Rp;

    return-object v0
.end method
