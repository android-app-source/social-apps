.class public LX/6f6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/2MM;


# direct methods
.method public constructor <init>(LX/2MM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1119076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119077
    iput-object p1, p0, LX/6f6;->a:LX/2MM;

    .line 1119078
    return-void
.end method

.method public static a(LX/0QB;)LX/6f6;
    .locals 1

    .prologue
    .line 1119120
    invoke-static {p0}, LX/6f6;->b(LX/0QB;)LX/6f6;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/6f6;
    .locals 2

    .prologue
    .line 1119121
    new-instance v1, LX/6f6;

    invoke-static {p0}, LX/2MM;->a(LX/0QB;)LX/2MM;

    move-result-object v0

    check-cast v0, LX/2MM;

    invoke-direct {v1, v0}, LX/6f6;-><init>(LX/2MM;)V

    .line 1119122
    return-object v1
.end method

.method public static c(Lcom/facebook/messaging/model/messages/Message;)LX/6f5;
    .locals 8

    .prologue
    .line 1119089
    new-instance v0, LX/6f5;

    invoke-direct {v0}, LX/6f5;-><init>()V

    move-object v2, v0

    .line 1119090
    invoke-virtual {p0}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1119091
    iget-object v5, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v6, LX/2MK;->AUDIO:LX/2MK;

    if-ne v5, v6, :cond_1

    .line 1119092
    iget v5, v2, LX/6f5;->f:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v2, LX/6f5;->f:I

    .line 1119093
    :cond_0
    :goto_1
    iget-object v5, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    iget-object v6, v0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    .line 1119094
    iget-object v7, v2, LX/6f5;->a:LX/0Xu;

    invoke-interface {v7, v5, v6}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1119095
    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    .line 1119096
    if-eqz v0, :cond_3

    .line 1119097
    invoke-virtual {v2, v0}, LX/6f5;->a(Ljava/lang/String;)V

    .line 1119098
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1119099
    :cond_1
    iget-object v5, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v6, LX/2MK;->PHOTO:LX/2MK;

    if-ne v5, v6, :cond_2

    .line 1119100
    iget v5, v2, LX/6f5;->d:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v2, LX/6f5;->d:I

    .line 1119101
    goto :goto_1

    .line 1119102
    :cond_2
    iget-object v5, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v6, LX/2MK;->VIDEO:LX/2MK;

    if-ne v5, v6, :cond_0

    .line 1119103
    iget v5, v2, LX/6f5;->e:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v2, LX/6f5;->e:I

    .line 1119104
    goto :goto_1

    .line 1119105
    :cond_3
    const-string v0, "unknown"

    invoke-virtual {v2, v0}, LX/6f5;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 1119106
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-eqz v0, :cond_6

    .line 1119107
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->a:LX/6fR;

    sget-object v1, LX/6fR;->SHARE:LX/6fR;

    if-ne v0, v1, :cond_8

    .line 1119108
    iget v0, v2, LX/6f5;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, LX/6f5;->i:I

    .line 1119109
    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->toArray()[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    .line 1119110
    iget v1, v2, LX/6f5;->i:I

    add-int/2addr v1, v0

    iput v1, v2, LX/6f5;->i:I

    .line 1119111
    :cond_6
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1119112
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-static {v0}, LX/4m9;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1119113
    iget v0, v2, LX/6f5;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, LX/6f5;->h:I

    .line 1119114
    :cond_7
    :goto_4
    return-object v2

    .line 1119115
    :cond_8
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->a:LX/6fR;

    sget-object v1, LX/6fR;->PAYMENT:LX/6fR;

    if-ne v0, v1, :cond_5

    .line 1119116
    iget v0, v2, LX/6f5;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, LX/6f5;->j:I

    .line 1119117
    goto :goto_3

    .line 1119118
    :cond_9
    iget v0, v2, LX/6f5;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, LX/6f5;->g:I

    .line 1119119
    goto :goto_4
.end method


# virtual methods
.method public final b(Lcom/facebook/messaging/model/messages/Message;)LX/6f4;
    .locals 12

    .prologue
    .line 1119079
    invoke-static {p1}, LX/6f6;->c(Lcom/facebook/messaging/model/messages/Message;)LX/6f5;

    move-result-object v0

    .line 1119080
    const-wide/16 v6, 0x0

    .line 1119081
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v9, :cond_0

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1119082
    iget-object v10, p0, LX/6f6;->a:LX/2MM;

    iget-object v4, v4, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v10, v4}, LX/2MM;->b(Landroid/net/Uri;)J

    move-result-wide v10

    add-long/2addr v6, v10

    .line 1119083
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 1119084
    :cond_0
    move-wide v2, v6

    .line 1119085
    iget-wide v4, v0, LX/6f5;->c:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    .line 1119086
    iput-wide v2, v0, LX/6f5;->c:J

    .line 1119087
    :goto_1
    invoke-virtual {v0}, LX/6f5;->s()LX/6f4;

    move-result-object v0

    return-object v0

    .line 1119088
    :cond_1
    iget-wide v4, v0, LX/6f5;->c:J

    add-long/2addr v4, v2

    iput-wide v4, v0, LX/6f5;->c:J

    goto :goto_1
.end method
