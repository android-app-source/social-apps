.class public LX/6jZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;

.field private static final l:LX/1sw;

.field private static final m:LX/1sw;


# instance fields
.field public final attributionInfo:LX/6ja;

.field public final audioMetadata:LX/6jd;

.field public final data:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final fbid:Ljava/lang/Long;

.field public final fileSize:Ljava/lang/Long;

.field public final filename:Ljava/lang/String;

.field public final id:Ljava/lang/String;

.field public final imageMetadata:LX/6ke;

.field public final mimeType:Ljava/lang/String;

.field public final videoMetadata:LX/6lC;

.field public final xmaGraphQL:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0xd

    const/4 v6, 0x1

    const/16 v5, 0xa

    const/16 v4, 0xc

    const/16 v3, 0xb

    .line 1131232
    new-instance v0, LX/1sv;

    const-string v1, "Attachment"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jZ;->b:LX/1sv;

    .line 1131233
    new-instance v0, LX/1sw;

    const-string v1, "id"

    invoke-direct {v0, v1, v3, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jZ;->c:LX/1sw;

    .line 1131234
    new-instance v0, LX/1sw;

    const-string v1, "mimeType"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jZ;->d:LX/1sw;

    .line 1131235
    new-instance v0, LX/1sw;

    const-string v1, "filename"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jZ;->e:LX/1sw;

    .line 1131236
    new-instance v0, LX/1sw;

    const-string v1, "fbid"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jZ;->f:LX/1sw;

    .line 1131237
    new-instance v0, LX/1sw;

    const-string v1, "fileSize"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jZ;->g:LX/1sw;

    .line 1131238
    new-instance v0, LX/1sw;

    const-string v1, "attributionInfo"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jZ;->h:LX/1sw;

    .line 1131239
    new-instance v0, LX/1sw;

    const-string v1, "xmaGraphQL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jZ;->i:LX/1sw;

    .line 1131240
    new-instance v0, LX/1sw;

    const-string v1, "imageMetadata"

    invoke-direct {v0, v1, v4, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jZ;->j:LX/1sw;

    .line 1131241
    new-instance v0, LX/1sw;

    const-string v1, "videoMetadata"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jZ;->k:LX/1sw;

    .line 1131242
    new-instance v0, LX/1sw;

    const-string v1, "audioMetadata"

    invoke-direct {v0, v1, v4, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jZ;->l:LX/1sw;

    .line 1131243
    new-instance v0, LX/1sw;

    const-string v1, "data"

    invoke-direct {v0, v1, v7, v7}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jZ;->m:LX/1sw;

    .line 1131244
    sput-boolean v6, LX/6jZ;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;LX/6ja;Ljava/lang/String;LX/6ke;LX/6lC;LX/6jd;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "LX/6ja;",
            "Ljava/lang/String;",
            "LX/6ke;",
            "LX/6lC;",
            "LX/6jd;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1131219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1131220
    iput-object p1, p0, LX/6jZ;->id:Ljava/lang/String;

    .line 1131221
    iput-object p2, p0, LX/6jZ;->mimeType:Ljava/lang/String;

    .line 1131222
    iput-object p3, p0, LX/6jZ;->filename:Ljava/lang/String;

    .line 1131223
    iput-object p4, p0, LX/6jZ;->fbid:Ljava/lang/Long;

    .line 1131224
    iput-object p5, p0, LX/6jZ;->fileSize:Ljava/lang/Long;

    .line 1131225
    iput-object p6, p0, LX/6jZ;->attributionInfo:LX/6ja;

    .line 1131226
    iput-object p7, p0, LX/6jZ;->xmaGraphQL:Ljava/lang/String;

    .line 1131227
    iput-object p8, p0, LX/6jZ;->imageMetadata:LX/6ke;

    .line 1131228
    iput-object p9, p0, LX/6jZ;->videoMetadata:LX/6lC;

    .line 1131229
    iput-object p10, p0, LX/6jZ;->audioMetadata:LX/6jd;

    .line 1131230
    iput-object p11, p0, LX/6jZ;->data:Ljava/util/Map;

    .line 1131231
    return-void
.end method

.method public static b(LX/1su;)LX/6jZ;
    .locals 15

    .prologue
    .line 1130893
    const/4 v1, 0x0

    .line 1130894
    const/4 v2, 0x0

    .line 1130895
    const/4 v3, 0x0

    .line 1130896
    const/4 v4, 0x0

    .line 1130897
    const/4 v5, 0x0

    .line 1130898
    const/4 v6, 0x0

    .line 1130899
    const/4 v7, 0x0

    .line 1130900
    const/4 v8, 0x0

    .line 1130901
    const/4 v9, 0x0

    .line 1130902
    const/4 v10, 0x0

    .line 1130903
    const/4 v11, 0x0

    .line 1130904
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    .line 1130905
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1130906
    iget-byte v12, v0, LX/1sw;->b:B

    if-eqz v12, :cond_10

    .line 1130907
    iget-short v12, v0, LX/1sw;->c:S

    packed-switch v12, :pswitch_data_0

    .line 1130908
    :pswitch_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1130909
    :pswitch_1
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xb

    if-ne v12, v13, :cond_1

    .line 1130910
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1130911
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1130912
    :pswitch_2
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xb

    if-ne v12, v13, :cond_2

    .line 1130913
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1130914
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1130915
    :pswitch_3
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xb

    if-ne v12, v13, :cond_3

    .line 1130916
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 1130917
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1130918
    :pswitch_4
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xa

    if-ne v12, v13, :cond_4

    .line 1130919
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_0

    .line 1130920
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1130921
    :pswitch_5
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xa

    if-ne v12, v13, :cond_5

    .line 1130922
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0

    .line 1130923
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1130924
    :pswitch_6
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xc

    if-ne v12, v13, :cond_6

    .line 1130925
    invoke-static {p0}, LX/6ja;->b(LX/1su;)LX/6ja;

    move-result-object v6

    goto :goto_0

    .line 1130926
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1130927
    :pswitch_7
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xb

    if-ne v12, v13, :cond_7

    .line 1130928
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1130929
    :cond_7
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1130930
    :pswitch_8
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xc

    if-ne v12, v13, :cond_8

    .line 1130931
    invoke-static {p0}, LX/6ke;->b(LX/1su;)LX/6ke;

    move-result-object v8

    goto/16 :goto_0

    .line 1130932
    :cond_8
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1130933
    :pswitch_9
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xc

    if-ne v12, v13, :cond_9

    .line 1130934
    invoke-static {p0}, LX/6lC;->b(LX/1su;)LX/6lC;

    move-result-object v9

    goto/16 :goto_0

    .line 1130935
    :cond_9
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1130936
    :pswitch_a
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xc

    if-ne v12, v13, :cond_d

    .line 1130937
    const/4 v0, 0x0

    .line 1130938
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v10, v0

    .line 1130939
    :goto_1
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v12

    .line 1130940
    iget-byte v13, v12, LX/1sw;->b:B

    if-eqz v13, :cond_c

    .line 1130941
    iget-short v13, v12, LX/1sw;->c:S

    packed-switch v13, :pswitch_data_1

    .line 1130942
    iget-byte v12, v12, LX/1sw;->b:B

    invoke-static {p0, v12}, LX/3ae;->a(LX/1su;B)V

    goto :goto_1

    .line 1130943
    :pswitch_b
    iget-byte v13, v12, LX/1sw;->b:B

    const/4 v14, 0x2

    if-ne v13, v14, :cond_a

    .line 1130944
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto :goto_1

    .line 1130945
    :cond_a
    iget-byte v12, v12, LX/1sw;->b:B

    invoke-static {p0, v12}, LX/3ae;->a(LX/1su;B)V

    goto :goto_1

    .line 1130946
    :pswitch_c
    iget-byte v13, v12, LX/1sw;->b:B

    const/16 v14, 0xb

    if-ne v13, v14, :cond_b

    .line 1130947
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1130948
    :cond_b
    iget-byte v12, v12, LX/1sw;->b:B

    invoke-static {p0, v12}, LX/3ae;->a(LX/1su;B)V

    goto :goto_1

    .line 1130949
    :cond_c
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1130950
    new-instance v12, LX/6jd;

    invoke-direct {v12, v10, v0}, LX/6jd;-><init>(Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 1130951
    move-object v10, v12

    .line 1130952
    goto/16 :goto_0

    .line 1130953
    :cond_d
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1130954
    :pswitch_d
    iget-byte v12, v0, LX/1sw;->b:B

    const/16 v13, 0xd

    if-ne v12, v13, :cond_f

    .line 1130955
    invoke-virtual {p0}, LX/1su;->g()LX/7H3;

    move-result-object v12

    .line 1130956
    new-instance v11, Ljava/util/HashMap;

    const/4 v0, 0x0

    iget v13, v12, LX/7H3;->c:I

    mul-int/lit8 v13, v13, 0x2

    invoke-static {v0, v13}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v11, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 1130957
    const/4 v0, 0x0

    .line 1130958
    :goto_2
    iget v13, v12, LX/7H3;->c:I

    if-gez v13, :cond_e

    invoke-static {}, LX/1su;->s()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1130959
    :goto_3
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v13

    .line 1130960
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v14

    .line 1130961
    invoke-interface {v11, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1130962
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1130963
    :cond_e
    iget v13, v12, LX/7H3;->c:I

    if-ge v0, v13, :cond_0

    goto :goto_3

    .line 1130964
    :cond_f
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1130965
    :cond_10
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1130966
    new-instance v0, LX/6jZ;

    invoke-direct/range {v0 .. v11}, LX/6jZ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;LX/6ja;Ljava/lang/String;LX/6ke;LX/6lC;LX/6jd;Ljava/util/Map;)V

    .line 1130967
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_d
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1131107
    if-eqz p2, :cond_14

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1131108
    :goto_0
    if-eqz p2, :cond_15

    const-string v0, "\n"

    move-object v3, v0

    .line 1131109
    :goto_1
    if-eqz p2, :cond_16

    const-string v0, " "

    .line 1131110
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "Attachment"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1131111
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131112
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131113
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131114
    const/4 v1, 0x1

    .line 1131115
    iget-object v6, p0, LX/6jZ;->id:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 1131116
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131117
    const-string v1, "id"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131118
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131119
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131120
    iget-object v1, p0, LX/6jZ;->id:Ljava/lang/String;

    if-nez v1, :cond_17

    .line 1131121
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1131122
    :cond_0
    iget-object v6, p0, LX/6jZ;->mimeType:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 1131123
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131124
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131125
    const-string v1, "mimeType"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131126
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131127
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131128
    iget-object v1, p0, LX/6jZ;->mimeType:Ljava/lang/String;

    if-nez v1, :cond_18

    .line 1131129
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 1131130
    :cond_2
    iget-object v6, p0, LX/6jZ;->filename:Ljava/lang/String;

    if-eqz v6, :cond_4

    .line 1131131
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131132
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131133
    const-string v1, "filename"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131134
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131135
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131136
    iget-object v1, p0, LX/6jZ;->filename:Ljava/lang/String;

    if-nez v1, :cond_19

    .line 1131137
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v1, v2

    .line 1131138
    :cond_4
    iget-object v6, p0, LX/6jZ;->fbid:Ljava/lang/Long;

    if-eqz v6, :cond_6

    .line 1131139
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131140
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131141
    const-string v1, "fbid"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131142
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131143
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131144
    iget-object v1, p0, LX/6jZ;->fbid:Ljava/lang/Long;

    if-nez v1, :cond_1a

    .line 1131145
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    move v1, v2

    .line 1131146
    :cond_6
    iget-object v6, p0, LX/6jZ;->fileSize:Ljava/lang/Long;

    if-eqz v6, :cond_8

    .line 1131147
    if-nez v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131148
    :cond_7
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131149
    const-string v1, "fileSize"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131150
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131151
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131152
    iget-object v1, p0, LX/6jZ;->fileSize:Ljava/lang/Long;

    if-nez v1, :cond_1b

    .line 1131153
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_7
    move v1, v2

    .line 1131154
    :cond_8
    iget-object v6, p0, LX/6jZ;->attributionInfo:LX/6ja;

    if-eqz v6, :cond_a

    .line 1131155
    if-nez v1, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131156
    :cond_9
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131157
    const-string v1, "attributionInfo"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131158
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131159
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131160
    iget-object v1, p0, LX/6jZ;->attributionInfo:LX/6ja;

    if-nez v1, :cond_1c

    .line 1131161
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_8
    move v1, v2

    .line 1131162
    :cond_a
    iget-object v6, p0, LX/6jZ;->xmaGraphQL:Ljava/lang/String;

    if-eqz v6, :cond_c

    .line 1131163
    if-nez v1, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131164
    :cond_b
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131165
    const-string v1, "xmaGraphQL"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131166
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131167
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131168
    iget-object v1, p0, LX/6jZ;->xmaGraphQL:Ljava/lang/String;

    if-nez v1, :cond_1d

    .line 1131169
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_9
    move v1, v2

    .line 1131170
    :cond_c
    iget-object v6, p0, LX/6jZ;->imageMetadata:LX/6ke;

    if-eqz v6, :cond_e

    .line 1131171
    if-nez v1, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131172
    :cond_d
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131173
    const-string v1, "imageMetadata"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131174
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131175
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131176
    iget-object v1, p0, LX/6jZ;->imageMetadata:LX/6ke;

    if-nez v1, :cond_1e

    .line 1131177
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_a
    move v1, v2

    .line 1131178
    :cond_e
    iget-object v6, p0, LX/6jZ;->videoMetadata:LX/6lC;

    if-eqz v6, :cond_10

    .line 1131179
    if-nez v1, :cond_f

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131180
    :cond_f
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131181
    const-string v1, "videoMetadata"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131182
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131183
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131184
    iget-object v1, p0, LX/6jZ;->videoMetadata:LX/6lC;

    if-nez v1, :cond_1f

    .line 1131185
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_b
    move v1, v2

    .line 1131186
    :cond_10
    iget-object v6, p0, LX/6jZ;->audioMetadata:LX/6jd;

    if-eqz v6, :cond_22

    .line 1131187
    if-nez v1, :cond_11

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131188
    :cond_11
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131189
    const-string v1, "audioMetadata"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131190
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131191
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131192
    iget-object v1, p0, LX/6jZ;->audioMetadata:LX/6jd;

    if-nez v1, :cond_20

    .line 1131193
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131194
    :goto_c
    iget-object v1, p0, LX/6jZ;->data:Ljava/util/Map;

    if-eqz v1, :cond_13

    .line 1131195
    if-nez v2, :cond_12

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131196
    :cond_12
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131197
    const-string v1, "data"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131198
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131199
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131200
    iget-object v0, p0, LX/6jZ;->data:Ljava/util/Map;

    if-nez v0, :cond_21

    .line 1131201
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131202
    :cond_13
    :goto_d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131203
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131204
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1131205
    :cond_14
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1131206
    :cond_15
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1131207
    :cond_16
    const-string v0, ""

    goto/16 :goto_2

    .line 1131208
    :cond_17
    iget-object v1, p0, LX/6jZ;->id:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1131209
    :cond_18
    iget-object v1, p0, LX/6jZ;->mimeType:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1131210
    :cond_19
    iget-object v1, p0, LX/6jZ;->filename:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1131211
    :cond_1a
    iget-object v1, p0, LX/6jZ;->fbid:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1131212
    :cond_1b
    iget-object v1, p0, LX/6jZ;->fileSize:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1131213
    :cond_1c
    iget-object v1, p0, LX/6jZ;->attributionInfo:LX/6ja;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1131214
    :cond_1d
    iget-object v1, p0, LX/6jZ;->xmaGraphQL:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 1131215
    :cond_1e
    iget-object v1, p0, LX/6jZ;->imageMetadata:LX/6ke;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 1131216
    :cond_1f
    iget-object v1, p0, LX/6jZ;->videoMetadata:LX/6lC;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    .line 1131217
    :cond_20
    iget-object v1, p0, LX/6jZ;->audioMetadata:LX/6jd;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    .line 1131218
    :cond_21
    iget-object v0, p0, LX/6jZ;->data:Ljava/util/Map;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_d

    :cond_22
    move v2, v1

    goto/16 :goto_c
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    const/16 v2, 0xb

    .line 1131056
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1131057
    iget-object v0, p0, LX/6jZ;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1131058
    iget-object v0, p0, LX/6jZ;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1131059
    sget-object v0, LX/6jZ;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131060
    iget-object v0, p0, LX/6jZ;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1131061
    :cond_0
    iget-object v0, p0, LX/6jZ;->mimeType:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1131062
    iget-object v0, p0, LX/6jZ;->mimeType:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1131063
    sget-object v0, LX/6jZ;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131064
    iget-object v0, p0, LX/6jZ;->mimeType:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1131065
    :cond_1
    iget-object v0, p0, LX/6jZ;->filename:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1131066
    iget-object v0, p0, LX/6jZ;->filename:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1131067
    sget-object v0, LX/6jZ;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131068
    iget-object v0, p0, LX/6jZ;->filename:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1131069
    :cond_2
    iget-object v0, p0, LX/6jZ;->fbid:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1131070
    iget-object v0, p0, LX/6jZ;->fbid:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1131071
    sget-object v0, LX/6jZ;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131072
    iget-object v0, p0, LX/6jZ;->fbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1131073
    :cond_3
    iget-object v0, p0, LX/6jZ;->fileSize:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1131074
    iget-object v0, p0, LX/6jZ;->fileSize:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1131075
    sget-object v0, LX/6jZ;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131076
    iget-object v0, p0, LX/6jZ;->fileSize:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1131077
    :cond_4
    iget-object v0, p0, LX/6jZ;->attributionInfo:LX/6ja;

    if-eqz v0, :cond_5

    .line 1131078
    iget-object v0, p0, LX/6jZ;->attributionInfo:LX/6ja;

    if-eqz v0, :cond_5

    .line 1131079
    sget-object v0, LX/6jZ;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131080
    iget-object v0, p0, LX/6jZ;->attributionInfo:LX/6ja;

    invoke-virtual {v0, p1}, LX/6ja;->a(LX/1su;)V

    .line 1131081
    :cond_5
    iget-object v0, p0, LX/6jZ;->xmaGraphQL:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1131082
    iget-object v0, p0, LX/6jZ;->xmaGraphQL:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1131083
    sget-object v0, LX/6jZ;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131084
    iget-object v0, p0, LX/6jZ;->xmaGraphQL:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1131085
    :cond_6
    iget-object v0, p0, LX/6jZ;->imageMetadata:LX/6ke;

    if-eqz v0, :cond_7

    .line 1131086
    iget-object v0, p0, LX/6jZ;->imageMetadata:LX/6ke;

    if-eqz v0, :cond_7

    .line 1131087
    sget-object v0, LX/6jZ;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131088
    iget-object v0, p0, LX/6jZ;->imageMetadata:LX/6ke;

    invoke-virtual {v0, p1}, LX/6ke;->a(LX/1su;)V

    .line 1131089
    :cond_7
    iget-object v0, p0, LX/6jZ;->videoMetadata:LX/6lC;

    if-eqz v0, :cond_8

    .line 1131090
    iget-object v0, p0, LX/6jZ;->videoMetadata:LX/6lC;

    if-eqz v0, :cond_8

    .line 1131091
    sget-object v0, LX/6jZ;->k:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131092
    iget-object v0, p0, LX/6jZ;->videoMetadata:LX/6lC;

    invoke-virtual {v0, p1}, LX/6lC;->a(LX/1su;)V

    .line 1131093
    :cond_8
    iget-object v0, p0, LX/6jZ;->audioMetadata:LX/6jd;

    if-eqz v0, :cond_9

    .line 1131094
    iget-object v0, p0, LX/6jZ;->audioMetadata:LX/6jd;

    if-eqz v0, :cond_9

    .line 1131095
    sget-object v0, LX/6jZ;->l:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131096
    iget-object v0, p0, LX/6jZ;->audioMetadata:LX/6jd;

    invoke-virtual {v0, p1}, LX/6jd;->a(LX/1su;)V

    .line 1131097
    :cond_9
    iget-object v0, p0, LX/6jZ;->data:Ljava/util/Map;

    if-eqz v0, :cond_a

    .line 1131098
    iget-object v0, p0, LX/6jZ;->data:Ljava/util/Map;

    if-eqz v0, :cond_a

    .line 1131099
    sget-object v0, LX/6jZ;->m:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131100
    new-instance v0, LX/7H3;

    iget-object v1, p0, LX/6jZ;->data:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v2, v2, v1}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 1131101
    iget-object v0, p0, LX/6jZ;->data:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1131102
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/1su;->a(Ljava/lang/String;)V

    .line 1131103
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1131104
    :cond_a
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1131105
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1131106
    return-void
.end method

.method public final a(LX/6jZ;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1130976
    if-nez p1, :cond_1

    .line 1130977
    :cond_0
    :goto_0
    return v2

    .line 1130978
    :cond_1
    iget-object v0, p0, LX/6jZ;->id:Ljava/lang/String;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1130979
    :goto_1
    iget-object v3, p1, LX/6jZ;->id:Ljava/lang/String;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1130980
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1130981
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1130982
    iget-object v0, p0, LX/6jZ;->id:Ljava/lang/String;

    iget-object v3, p1, LX/6jZ;->id:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1130983
    :cond_3
    iget-object v0, p0, LX/6jZ;->mimeType:Ljava/lang/String;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1130984
    :goto_3
    iget-object v3, p1, LX/6jZ;->mimeType:Ljava/lang/String;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1130985
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1130986
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1130987
    iget-object v0, p0, LX/6jZ;->mimeType:Ljava/lang/String;

    iget-object v3, p1, LX/6jZ;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1130988
    :cond_5
    iget-object v0, p0, LX/6jZ;->filename:Ljava/lang/String;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1130989
    :goto_5
    iget-object v3, p1, LX/6jZ;->filename:Ljava/lang/String;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1130990
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1130991
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1130992
    iget-object v0, p0, LX/6jZ;->filename:Ljava/lang/String;

    iget-object v3, p1, LX/6jZ;->filename:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1130993
    :cond_7
    iget-object v0, p0, LX/6jZ;->fbid:Ljava/lang/Long;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1130994
    :goto_7
    iget-object v3, p1, LX/6jZ;->fbid:Ljava/lang/Long;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1130995
    :goto_8
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1130996
    :cond_8
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1130997
    iget-object v0, p0, LX/6jZ;->fbid:Ljava/lang/Long;

    iget-object v3, p1, LX/6jZ;->fbid:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1130998
    :cond_9
    iget-object v0, p0, LX/6jZ;->fileSize:Ljava/lang/Long;

    if-eqz v0, :cond_20

    move v0, v1

    .line 1130999
    :goto_9
    iget-object v3, p1, LX/6jZ;->fileSize:Ljava/lang/Long;

    if-eqz v3, :cond_21

    move v3, v1

    .line 1131000
    :goto_a
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1131001
    :cond_a
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131002
    iget-object v0, p0, LX/6jZ;->fileSize:Ljava/lang/Long;

    iget-object v3, p1, LX/6jZ;->fileSize:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131003
    :cond_b
    iget-object v0, p0, LX/6jZ;->attributionInfo:LX/6ja;

    if-eqz v0, :cond_22

    move v0, v1

    .line 1131004
    :goto_b
    iget-object v3, p1, LX/6jZ;->attributionInfo:LX/6ja;

    if-eqz v3, :cond_23

    move v3, v1

    .line 1131005
    :goto_c
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1131006
    :cond_c
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131007
    iget-object v0, p0, LX/6jZ;->attributionInfo:LX/6ja;

    iget-object v3, p1, LX/6jZ;->attributionInfo:LX/6ja;

    invoke-virtual {v0, v3}, LX/6ja;->a(LX/6ja;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131008
    :cond_d
    iget-object v0, p0, LX/6jZ;->xmaGraphQL:Ljava/lang/String;

    if-eqz v0, :cond_24

    move v0, v1

    .line 1131009
    :goto_d
    iget-object v3, p1, LX/6jZ;->xmaGraphQL:Ljava/lang/String;

    if-eqz v3, :cond_25

    move v3, v1

    .line 1131010
    :goto_e
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1131011
    :cond_e
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131012
    iget-object v0, p0, LX/6jZ;->xmaGraphQL:Ljava/lang/String;

    iget-object v3, p1, LX/6jZ;->xmaGraphQL:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131013
    :cond_f
    iget-object v0, p0, LX/6jZ;->imageMetadata:LX/6ke;

    if-eqz v0, :cond_26

    move v0, v1

    .line 1131014
    :goto_f
    iget-object v3, p1, LX/6jZ;->imageMetadata:LX/6ke;

    if-eqz v3, :cond_27

    move v3, v1

    .line 1131015
    :goto_10
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1131016
    :cond_10
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131017
    iget-object v0, p0, LX/6jZ;->imageMetadata:LX/6ke;

    iget-object v3, p1, LX/6jZ;->imageMetadata:LX/6ke;

    invoke-virtual {v0, v3}, LX/6ke;->a(LX/6ke;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131018
    :cond_11
    iget-object v0, p0, LX/6jZ;->videoMetadata:LX/6lC;

    if-eqz v0, :cond_28

    move v0, v1

    .line 1131019
    :goto_11
    iget-object v3, p1, LX/6jZ;->videoMetadata:LX/6lC;

    if-eqz v3, :cond_29

    move v3, v1

    .line 1131020
    :goto_12
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 1131021
    :cond_12
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131022
    iget-object v0, p0, LX/6jZ;->videoMetadata:LX/6lC;

    iget-object v3, p1, LX/6jZ;->videoMetadata:LX/6lC;

    invoke-virtual {v0, v3}, LX/6lC;->a(LX/6lC;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131023
    :cond_13
    iget-object v0, p0, LX/6jZ;->audioMetadata:LX/6jd;

    if-eqz v0, :cond_2a

    move v0, v1

    .line 1131024
    :goto_13
    iget-object v3, p1, LX/6jZ;->audioMetadata:LX/6jd;

    if-eqz v3, :cond_2b

    move v3, v1

    .line 1131025
    :goto_14
    if-nez v0, :cond_14

    if-eqz v3, :cond_15

    .line 1131026
    :cond_14
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131027
    iget-object v0, p0, LX/6jZ;->audioMetadata:LX/6jd;

    iget-object v3, p1, LX/6jZ;->audioMetadata:LX/6jd;

    invoke-virtual {v0, v3}, LX/6jd;->a(LX/6jd;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131028
    :cond_15
    iget-object v0, p0, LX/6jZ;->data:Ljava/util/Map;

    if-eqz v0, :cond_2c

    move v0, v1

    .line 1131029
    :goto_15
    iget-object v3, p1, LX/6jZ;->data:Ljava/util/Map;

    if-eqz v3, :cond_2d

    move v3, v1

    .line 1131030
    :goto_16
    if-nez v0, :cond_16

    if-eqz v3, :cond_17

    .line 1131031
    :cond_16
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131032
    iget-object v0, p0, LX/6jZ;->data:Ljava/util/Map;

    iget-object v3, p1, LX/6jZ;->data:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_17
    move v2, v1

    .line 1131033
    goto/16 :goto_0

    :cond_18
    move v0, v2

    .line 1131034
    goto/16 :goto_1

    :cond_19
    move v3, v2

    .line 1131035
    goto/16 :goto_2

    :cond_1a
    move v0, v2

    .line 1131036
    goto/16 :goto_3

    :cond_1b
    move v3, v2

    .line 1131037
    goto/16 :goto_4

    :cond_1c
    move v0, v2

    .line 1131038
    goto/16 :goto_5

    :cond_1d
    move v3, v2

    .line 1131039
    goto/16 :goto_6

    :cond_1e
    move v0, v2

    .line 1131040
    goto/16 :goto_7

    :cond_1f
    move v3, v2

    .line 1131041
    goto/16 :goto_8

    :cond_20
    move v0, v2

    .line 1131042
    goto/16 :goto_9

    :cond_21
    move v3, v2

    .line 1131043
    goto/16 :goto_a

    :cond_22
    move v0, v2

    .line 1131044
    goto/16 :goto_b

    :cond_23
    move v3, v2

    .line 1131045
    goto/16 :goto_c

    :cond_24
    move v0, v2

    .line 1131046
    goto/16 :goto_d

    :cond_25
    move v3, v2

    .line 1131047
    goto/16 :goto_e

    :cond_26
    move v0, v2

    .line 1131048
    goto/16 :goto_f

    :cond_27
    move v3, v2

    .line 1131049
    goto/16 :goto_10

    :cond_28
    move v0, v2

    .line 1131050
    goto/16 :goto_11

    :cond_29
    move v3, v2

    .line 1131051
    goto/16 :goto_12

    :cond_2a
    move v0, v2

    .line 1131052
    goto :goto_13

    :cond_2b
    move v3, v2

    .line 1131053
    goto :goto_14

    :cond_2c
    move v0, v2

    .line 1131054
    goto :goto_15

    :cond_2d
    move v3, v2

    .line 1131055
    goto :goto_16
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1130972
    if-nez p1, :cond_1

    .line 1130973
    :cond_0
    :goto_0
    return v0

    .line 1130974
    :cond_1
    instance-of v1, p1, LX/6jZ;

    if-eqz v1, :cond_0

    .line 1130975
    check-cast p1, LX/6jZ;

    invoke-virtual {p0, p1}, LX/6jZ;->a(LX/6jZ;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1130971
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1130968
    sget-boolean v0, LX/6jZ;->a:Z

    .line 1130969
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jZ;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1130970
    return-object v0
.end method
