.class public final enum LX/7iO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7iO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7iO;

.field public static final enum COLLECTION_GRID:LX/7iO;

.field public static final enum PAGE_STOREFRONT_ENTRY_GRID:LX/7iO;

.field public static final enum PDFY_FEED_UNIT:LX/7iO;

.field public static final enum PRODUCT_DETAILS_PAGE:LX/7iO;

.field public static final enum STOREFRONT_BANNER:LX/7iO;

.field public static final enum STOREFRONT_COLLECTION:LX/7iO;

.field public static final enum STOREFRONT_COLLECTION_HEADER:LX/7iO;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1227441
    new-instance v0, LX/7iO;

    const-string v1, "PAGE_STOREFRONT_ENTRY_GRID"

    const-string v2, "page_storefront_entry_grid"

    invoke-direct {v0, v1, v4, v2}, LX/7iO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iO;->PAGE_STOREFRONT_ENTRY_GRID:LX/7iO;

    .line 1227442
    new-instance v0, LX/7iO;

    const-string v1, "COLLECTION_GRID"

    const-string v2, "collection_grid"

    invoke-direct {v0, v1, v5, v2}, LX/7iO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iO;->COLLECTION_GRID:LX/7iO;

    .line 1227443
    new-instance v0, LX/7iO;

    const-string v1, "STOREFRONT_BANNER"

    const-string v2, "storefront_banner"

    invoke-direct {v0, v1, v6, v2}, LX/7iO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iO;->STOREFRONT_BANNER:LX/7iO;

    .line 1227444
    new-instance v0, LX/7iO;

    const-string v1, "STOREFRONT_COLLECTION"

    const-string v2, "storefront_collection"

    invoke-direct {v0, v1, v7, v2}, LX/7iO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iO;->STOREFRONT_COLLECTION:LX/7iO;

    .line 1227445
    new-instance v0, LX/7iO;

    const-string v1, "STOREFRONT_COLLECTION_HEADER"

    const-string v2, "storefront_collection_header"

    invoke-direct {v0, v1, v8, v2}, LX/7iO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iO;->STOREFRONT_COLLECTION_HEADER:LX/7iO;

    .line 1227446
    new-instance v0, LX/7iO;

    const-string v1, "PRODUCT_DETAILS_PAGE"

    const/4 v2, 0x5

    const-string v3, "product_details_checkout_button"

    invoke-direct {v0, v1, v2, v3}, LX/7iO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iO;->PRODUCT_DETAILS_PAGE:LX/7iO;

    .line 1227447
    new-instance v0, LX/7iO;

    const-string v1, "PDFY_FEED_UNIT"

    const/4 v2, 0x6

    const-string v3, "PdfyFeedUnit"

    invoke-direct {v0, v1, v2, v3}, LX/7iO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iO;->PDFY_FEED_UNIT:LX/7iO;

    .line 1227448
    const/4 v0, 0x7

    new-array v0, v0, [LX/7iO;

    sget-object v1, LX/7iO;->PAGE_STOREFRONT_ENTRY_GRID:LX/7iO;

    aput-object v1, v0, v4

    sget-object v1, LX/7iO;->COLLECTION_GRID:LX/7iO;

    aput-object v1, v0, v5

    sget-object v1, LX/7iO;->STOREFRONT_BANNER:LX/7iO;

    aput-object v1, v0, v6

    sget-object v1, LX/7iO;->STOREFRONT_COLLECTION:LX/7iO;

    aput-object v1, v0, v7

    sget-object v1, LX/7iO;->STOREFRONT_COLLECTION_HEADER:LX/7iO;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7iO;->PRODUCT_DETAILS_PAGE:LX/7iO;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7iO;->PDFY_FEED_UNIT:LX/7iO;

    aput-object v2, v0, v1

    sput-object v0, LX/7iO;->$VALUES:[LX/7iO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1227449
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1227450
    iput-object p3, p0, LX/7iO;->value:Ljava/lang/String;

    .line 1227451
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7iO;
    .locals 1

    .prologue
    .line 1227452
    const-class v0, LX/7iO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7iO;

    return-object v0
.end method

.method public static values()[LX/7iO;
    .locals 1

    .prologue
    .line 1227453
    sget-object v0, LX/7iO;->$VALUES:[LX/7iO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7iO;

    return-object v0
.end method
