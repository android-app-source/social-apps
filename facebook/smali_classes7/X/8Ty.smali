.class public final LX/8Ty;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1349538
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 1349539
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1349540
    :goto_0
    return v1

    .line 1349541
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 1349542
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1349543
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1349544
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 1349545
    const-string v7, "has_scores"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1349546
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1349547
    :cond_1
    const-string v7, "leaderboard_entries"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1349548
    invoke-static {p0, p1}, LX/8U0;->b(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1349549
    :cond_2
    const-string v7, "message_thread"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1349550
    invoke-static {p0, p1}, LX/8Tx;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1349551
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1349552
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1349553
    if-eqz v0, :cond_5

    .line 1349554
    invoke-virtual {p1, v1, v5}, LX/186;->a(IZ)V

    .line 1349555
    :cond_5
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1349556
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1349557
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1349558
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1349559
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1349560
    if-eqz v0, :cond_0

    .line 1349561
    const-string v1, "has_scores"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1349562
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1349563
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1349564
    if-eqz v0, :cond_1

    .line 1349565
    const-string v1, "leaderboard_entries"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1349566
    invoke-static {p0, v0, p2, p3}, LX/8U0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1349567
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1349568
    if-eqz v0, :cond_2

    .line 1349569
    const-string v1, "message_thread"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1349570
    invoke-static {p0, v0, p2}, LX/8Tx;->a(LX/15i;ILX/0nX;)V

    .line 1349571
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1349572
    return-void
.end method
