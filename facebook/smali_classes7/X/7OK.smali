.class public final LX/7OK;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7OL;


# direct methods
.method public constructor <init>(LX/7OL;)V
    .locals 0

    .prologue
    .line 1201337
    iput-object p1, p0, LX/7OK;->a:LX/7OL;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1201329
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1201330
    check-cast p1, LX/2ou;

    .line 1201331
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    invoke-virtual {v0}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1201332
    iget-object v0, p0, LX/7OK;->a:LX/7OL;

    iget-object v0, v0, LX/7OL;->q:Lcom/facebook/widget/FbImageView;

    const v1, 0x7f021a38

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setImageResource(I)V

    .line 1201333
    iget-object v0, p0, LX/7OK;->a:LX/7OL;

    iget-object v0, v0, LX/7OL;->q:Lcom/facebook/widget/FbImageView;

    iget-object v1, p0, LX/7OK;->a:LX/7OL;

    invoke-virtual {v1}, LX/7OL;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080da0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1201334
    :goto_0
    return-void

    .line 1201335
    :cond_0
    iget-object v0, p0, LX/7OK;->a:LX/7OL;

    iget-object v0, v0, LX/7OL;->q:Lcom/facebook/widget/FbImageView;

    const v1, 0x7f021a39

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setImageResource(I)V

    .line 1201336
    iget-object v0, p0, LX/7OK;->a:LX/7OL;

    iget-object v0, v0, LX/7OL;->q:Lcom/facebook/widget/FbImageView;

    iget-object v1, p0, LX/7OK;->a:LX/7OL;

    invoke-virtual {v1}, LX/7OL;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080d9f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
