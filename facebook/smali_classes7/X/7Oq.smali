.class public LX/7Oq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/04m;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/os/Handler;

.field public final c:LX/0O4;

.field private final d:LX/0So;

.field private final e:LX/08I;

.field private f:J

.field private g:J

.field private h:J

.field private i:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1202279
    const-class v0, LX/7Oq;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Oq;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0So;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1202277
    invoke-direct {p0, v0, v0, p1}, LX/7Oq;-><init>(Landroid/os/Handler;LX/0O4;LX/0So;)V

    .line 1202278
    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;LX/0O4;LX/0So;)V
    .locals 1

    .prologue
    .line 1202275
    const/16 v0, 0x7d0

    invoke-direct {p0, p1, p2, p3, v0}, LX/7Oq;-><init>(Landroid/os/Handler;LX/0O4;LX/0So;I)V

    .line 1202276
    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;LX/0O4;LX/0So;I)V
    .locals 2

    .prologue
    .line 1202241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202242
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/7Oq;->f:J

    .line 1202243
    iput-object p1, p0, LX/7Oq;->b:Landroid/os/Handler;

    .line 1202244
    iput-object p2, p0, LX/7Oq;->c:LX/0O4;

    .line 1202245
    iput-object p3, p0, LX/7Oq;->d:LX/0So;

    .line 1202246
    new-instance v0, LX/08I;

    invoke-direct {v0, p4}, LX/08I;-><init>(I)V

    iput-object v0, p0, LX/7Oq;->e:LX/08I;

    .line 1202247
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/7Oq;->h:J

    .line 1202248
    return-void
.end method

.method private a(IJJ)V
    .locals 8

    .prologue
    const/4 v6, 0x3

    const-wide/16 v4, 0x3e8

    .line 1202280
    invoke-static {v6}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1202281
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    div-long v2, p4, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    aput-object p0, v0, v6

    const/4 v1, 0x4

    iget-object v2, p0, LX/7Oq;->e:LX/08I;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/08I;->a(F)F

    move-result v2

    float-to-long v2, v2

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LX/7Oq;->e:LX/08I;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, LX/08I;->a(F)F

    move-result v2

    float-to-long v2, v2

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1202282
    :cond_0
    iget-object v0, p0, LX/7Oq;->b:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/7Oq;->c:LX/0O4;

    if-eqz v0, :cond_1

    .line 1202283
    iget-object v0, p0, LX/7Oq;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/video/server/CustomBandwidthMeter$1;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/video/server/CustomBandwidthMeter$1;-><init>(LX/7Oq;IJJ)V

    const v2, 0x52dd130d

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1202284
    :cond_1
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()J
    .locals 2

    .prologue
    .line 1202274
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/7Oq;->h:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)V
    .locals 4

    .prologue
    .line 1202271
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/7Oq;->f:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/7Oq;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1202272
    monitor-exit p0

    return-void

    .line 1202273
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 1202266
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/7Oq;->i:I

    if-nez v0, :cond_0

    .line 1202267
    iget-object v0, p0, LX/7Oq;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/7Oq;->g:J

    .line 1202268
    :cond_0
    iget v0, p0, LX/7Oq;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7Oq;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1202269
    monitor-exit p0

    return-void

    .line 1202270
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 8

    .prologue
    .line 1202249
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/7Oq;->i:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1202250
    iget-object v0, p0, LX/7Oq;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    .line 1202251
    iget-wide v0, p0, LX/7Oq;->g:J

    sub-long v0, v6, v0

    long-to-int v1, v0

    .line 1202252
    if-lez v1, :cond_0

    .line 1202253
    iget-wide v2, p0, LX/7Oq;->f:J

    const-wide/16 v4, 0x1f40

    mul-long/2addr v2, v4

    int-to-long v4, v1

    div-long/2addr v2, v4

    long-to-float v0, v2

    .line 1202254
    iget-object v2, p0, LX/7Oq;->e:LX/08I;

    iget-wide v4, p0, LX/7Oq;->f:J

    long-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-int v3, v4

    invoke-virtual {v2, v3, v0}, LX/08I;->a(IF)V

    .line 1202255
    iget-object v0, p0, LX/7Oq;->e:LX/08I;

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v2}, LX/08I;->a(F)F

    move-result v0

    .line 1202256
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_3

    const-wide/16 v2, -0x1

    :goto_1
    iput-wide v2, p0, LX/7Oq;->h:J

    .line 1202257
    iget-wide v2, p0, LX/7Oq;->f:J

    iget-wide v4, p0, LX/7Oq;->h:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/7Oq;->a(IJJ)V

    .line 1202258
    :cond_0
    iget v0, p0, LX/7Oq;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/7Oq;->i:I

    .line 1202259
    iget v0, p0, LX/7Oq;->i:I

    if-lez v0, :cond_1

    .line 1202260
    iput-wide v6, p0, LX/7Oq;->g:J

    .line 1202261
    :cond_1
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/7Oq;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1202262
    monitor-exit p0

    return-void

    .line 1202263
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1202264
    :cond_3
    float-to-long v2, v0

    goto :goto_1

    .line 1202265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
