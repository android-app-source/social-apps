.class public LX/8Y7;
.super Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;
.source ""


# instance fields
.field public a:LX/8TS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/8TD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/8WJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/8WM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/8Ve;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/8T7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private g:LX/8Sk;

.field public h:Landroid/widget/TextView;

.field public i:Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

.field public j:Lcom/facebook/quicksilver/views/loading/GameLoadingCard;

.field public k:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

.field public l:LX/8WI;

.field public m:LX/8WL;

.field public n:Landroid/view/View;

.field public o:Landroid/view/View;

.field public p:Landroid/widget/ScrollView;

.field public q:Z

.field public r:Z

.field private s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1355819
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/8Y7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355820
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    .line 1355821
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355822
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/8Y7;

    invoke-static {v0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v3

    check-cast v3, LX/8TS;

    invoke-static {v0}, LX/8TD;->a(LX/0QB;)LX/8TD;

    move-result-object v4

    check-cast v4, LX/8TD;

    const-class p1, LX/8WJ;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/8WJ;

    const-class p2, LX/8WM;

    invoke-interface {v0, p2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p2

    check-cast p2, LX/8WM;

    invoke-static {v0}, LX/8Ve;->b(LX/0QB;)LX/8Ve;

    move-result-object p3

    check-cast p3, LX/8Ve;

    invoke-static {v0}, LX/8T7;->b(LX/0QB;)LX/8T7;

    move-result-object v0

    check-cast v0, LX/8T7;

    iput-object v3, v2, LX/8Y7;->a:LX/8TS;

    iput-object v4, v2, LX/8Y7;->b:LX/8TD;

    iput-object p1, v2, LX/8Y7;->c:LX/8WJ;

    iput-object p2, v2, LX/8Y7;->d:LX/8WM;

    iput-object p3, v2, LX/8Y7;->e:LX/8Ve;

    iput-object v0, v2, LX/8Y7;->f:LX/8T7;

    .line 1355823
    invoke-virtual {p0}, LX/8Y7;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0310e3

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1355824
    const v0, 0x7f0d281b

    invoke-virtual {p0, v0}, LX/8Y7;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    iput-object v0, p0, LX/8Y7;->k:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    .line 1355825
    iget-object v1, p0, LX/8Y7;->c:LX/8WJ;

    iget-object v2, p0, LX/8Y7;->k:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    iget-object v0, p0, LX/8Y7;->k:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    .line 1355826
    iget-object v3, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v3

    .line 1355827
    check-cast v0, LX/8WX;

    invoke-virtual {v1, v2, v0}, LX/8WJ;->a(Landroid/support/v7/widget/RecyclerView;LX/8VY;)LX/8WI;

    move-result-object v0

    iput-object v0, p0, LX/8Y7;->l:LX/8WI;

    .line 1355828
    iget-object v1, p0, LX/8Y7;->d:LX/8WM;

    iget-object v0, p0, LX/8Y7;->k:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    .line 1355829
    iget-object v2, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v2

    .line 1355830
    check-cast v0, LX/8WX;

    .line 1355831
    new-instance v4, LX/8WL;

    invoke-static {v1}, LX/8Tk;->b(LX/0QB;)LX/8Tk;

    move-result-object v2

    check-cast v2, LX/8Tk;

    invoke-static {v1}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v3

    check-cast v3, LX/8TS;

    invoke-direct {v4, v0, v2, v3}, LX/8WL;-><init>(LX/8VY;LX/8Tk;LX/8TS;)V

    .line 1355832
    move-object v0, v4

    .line 1355833
    iput-object v0, p0, LX/8Y7;->m:LX/8WL;

    .line 1355834
    iget-object v0, p0, LX/8Y7;->l:LX/8WI;

    new-instance v1, LX/8Xy;

    invoke-direct {v1, p0}, LX/8Xy;-><init>(LX/8Y7;)V

    .line 1355835
    iput-object v1, v0, LX/8WI;->f:LX/8WH;

    .line 1355836
    iget-object v0, p0, LX/8Y7;->m:LX/8WL;

    new-instance v1, LX/8Xz;

    invoke-direct {v1, p0}, LX/8Xz;-><init>(LX/8Y7;)V

    .line 1355837
    iput-object v1, v0, LX/8WL;->d:LX/8Xz;

    .line 1355838
    const v0, 0x7f0d281a

    invoke-virtual {p0, v0}, LX/8Y7;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;

    iput-object v0, p0, LX/8Y7;->j:Lcom/facebook/quicksilver/views/loading/GameLoadingCard;

    .line 1355839
    const v0, 0x7f0d2819

    invoke-virtual {p0, v0}, LX/8Y7;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/8Y7;->o:Landroid/view/View;

    .line 1355840
    const v0, 0x7f0d2818

    invoke-virtual {p0, v0}, LX/8Y7;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, LX/8Y7;->p:Landroid/widget/ScrollView;

    .line 1355841
    const v0, 0x7f0d281e

    invoke-virtual {p0, v0}, LX/8Y7;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/8Y7;->n:Landroid/view/View;

    .line 1355842
    const v0, 0x7f0d13fc

    invoke-virtual {p0, v0}, LX/8Y7;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8Y7;->h:Landroid/widget/TextView;

    .line 1355843
    const v0, 0x7f0d281d

    invoke-virtual {p0, v0}, LX/8Y7;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    iput-object v0, p0, LX/8Y7;->i:Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    .line 1355844
    const/16 v2, 0x8

    .line 1355845
    iget-object v0, p0, LX/8Y7;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1355846
    iget-object v0, p0, LX/8Y7;->h:Landroid/widget/TextView;

    new-instance v1, LX/8Y0;

    invoke-direct {v1, p0}, LX/8Y0;-><init>(LX/8Y7;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1355847
    iget-object v0, p0, LX/8Y7;->i:Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    invoke-virtual {v0, v2}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->setVisibility(I)V

    .line 1355848
    iget-object v0, p0, LX/8Y7;->i:Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    new-instance v1, LX/8Y1;

    invoke-direct {v1, p0}, LX/8Y1;-><init>(LX/8Y7;)V

    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->setPlayButtonListener(Landroid/view/View$OnClickListener;)V

    .line 1355849
    return-void
.end method

.method private a(Z)Z
    .locals 1

    .prologue
    .line 1355911
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/8Y7;->a:LX/8TS;

    .line 1355912
    iget-object p1, v0, LX/8TS;->h:LX/8TW;

    move-object v0, p1

    .line 1355913
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8Y7;->a:LX/8TS;

    .line 1355914
    iget-object p0, v0, LX/8TS;->h:LX/8TW;

    move-object v0, p0

    .line 1355915
    iget-object p0, v0, LX/8TW;->a:Ljava/util/List;

    move-object v0, p0

    .line 1355916
    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/8Y7;ZLjava/lang/String;)V
    .locals 10
    .param p1    # Z
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1355850
    invoke-direct {p0, p1}, LX/8Y7;->a(Z)Z

    move-result v0

    iput-boolean v0, p0, LX/8Y7;->q:Z

    .line 1355851
    iget-boolean v0, p0, LX/8Y7;->q:Z

    if-eqz v0, :cond_2

    .line 1355852
    iget-object v0, p0, LX/8Y7;->i:Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    const/4 p1, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1355853
    iget-object v1, p0, LX/8Y7;->a:LX/8TS;

    .line 1355854
    iget-object v2, v1, LX/8TS;->h:LX/8TW;

    move-object v1, v2

    .line 1355855
    iget-object v2, v1, LX/8TW;->a:Ljava/util/List;

    move-object v2, v2

    .line 1355856
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1355857
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1355858
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Vb;

    .line 1355859
    iget-object v6, v1, LX/8Vb;->a:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1355860
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v6, p1, :cond_0

    .line 1355861
    iget-object v1, v1, LX/8Vb;->d:Ljava/lang/String;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1355862
    :cond_1
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1355863
    :goto_1
    invoke-static {}, LX/8Vb;->b()LX/8Va;

    move-result-object v1

    .line 1355864
    iput-object p2, v1, LX/8Va;->c:Ljava/lang/String;

    .line 1355865
    move-object v1, v1

    .line 1355866
    iget-object v2, p0, LX/8Y7;->e:LX/8Ve;

    invoke-virtual {v2, v3}, LX/8Ve;->a(Ljava/util/List;)LX/8Vd;

    move-result-object v2

    .line 1355867
    iput-object v2, v1, LX/8Va;->k:LX/8Vd;

    .line 1355868
    move-object v2, v1

    .line 1355869
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v9, :cond_6

    .line 1355870
    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1355871
    iput-object v1, v2, LX/8Va;->a:Ljava/lang/String;

    .line 1355872
    :goto_2
    invoke-virtual {v2}, LX/8Va;->a()LX/8Vb;

    move-result-object v1

    move-object v1, v1

    .line 1355873
    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->a(LX/8Vb;)V

    .line 1355874
    iget-object v0, p0, LX/8Y7;->k:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->setVisibility(I)V

    .line 1355875
    :goto_3
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8Y7;->s:Z

    .line 1355876
    invoke-static {p0}, LX/8Y7;->h(LX/8Y7;)V

    .line 1355877
    return-void

    .line 1355878
    :cond_2
    iget-object v0, p0, LX/8Y7;->k:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    invoke-virtual {p0}, LX/8Y7;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->setTranslationY(F)V

    .line 1355879
    iget-object v0, p0, LX/8Y7;->f:LX/8T7;

    iget-object v1, p0, LX/8Y7;->k:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    new-instance v2, LX/8Y4;

    invoke-direct {v2, p0}, LX/8Y4;-><init>(LX/8Y7;)V

    invoke-virtual {v0, v1, v2}, LX/8T7;->a(Landroid/view/View;LX/8Sl;)V

    .line 1355880
    goto :goto_3

    .line 1355881
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v9, :cond_4

    .line 1355882
    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object p2, v1

    goto :goto_1

    .line 1355883
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, p1, :cond_5

    .line 1355884
    invoke-virtual {p0}, LX/8Y7;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08236a

    new-array v5, p1, [Ljava/lang/Object;

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v5, v9

    invoke-virtual {v1, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 1355885
    :cond_5
    invoke-virtual {p0}, LX/8Y7;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f08236b

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v6, v9

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, p1

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 1355886
    :cond_6
    iget-object v1, p0, LX/8Y7;->a:LX/8TS;

    .line 1355887
    iget-object v3, v1, LX/8TS;->j:LX/8TP;

    move-object v1, v3

    .line 1355888
    iget-object v3, v1, LX/8TP;->b:Ljava/lang/String;

    move-object v1, v3

    .line 1355889
    iput-object v1, v2, LX/8Va;->b:Ljava/lang/String;

    .line 1355890
    goto/16 :goto_2
.end method

.method public static b(LX/8Y7;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1355891
    invoke-virtual {p0}, LX/8Y7;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1811

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v2, v0

    .line 1355892
    invoke-virtual {p0}, LX/8Y7;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, LX/8Y7;->j:Lcom/facebook/quicksilver/views/loading/GameLoadingCard;

    invoke-virtual {v1}, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, LX/8Y7;->n:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    sub-int v1, v0, v2

    .line 1355893
    iget-object v0, p0, LX/8Y7;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v2

    .line 1355894
    iget-object v3, p0, LX/8Y7;->i:Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    invoke-virtual {v3}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    .line 1355895
    iget-object v3, p0, LX/8Y7;->i:Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    invoke-virtual {v3}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v1, v3

    .line 1355896
    iget-object v3, p0, LX/8Y7;->i:Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    invoke-virtual {v3}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v0, v3

    .line 1355897
    :cond_0
    :goto_0
    iget-object v3, p0, LX/8Y7;->k:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    invoke-virtual {v3}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    .line 1355898
    invoke-virtual {p0}, LX/8Y7;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1804

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    .line 1355899
    :cond_1
    if-eqz p1, :cond_3

    .line 1355900
    const/4 v2, 0x2

    new-array v2, v2, [I

    iget-object v3, p0, LX/8Y7;->o:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    aput v3, v2, v5

    const/4 v3, 0x1

    aput v1, v2, v3

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 1355901
    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1355902
    new-instance v2, LX/8Y5;

    invoke-direct {v2, p0, v0}, LX/8Y5;-><init>(LX/8Y7;I)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1355903
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1355904
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 1355905
    :goto_1
    return-void

    .line 1355906
    :cond_2
    iget-object v3, p0, LX/8Y7;->h:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 1355907
    invoke-virtual {p0}, LX/8Y7;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1810

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    .line 1355908
    sub-int/2addr v1, v3

    .line 1355909
    add-int/2addr v0, v3

    goto :goto_0

    .line 1355910
    :cond_3
    iget-object v0, p0, LX/8Y7;->o:Landroid/view/View;

    invoke-virtual {v0, v5, v1, v5, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1
.end method

.method public static h(LX/8Y7;)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 1355802
    iget-boolean v0, p0, LX/8Y7;->r:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/8Y7;->s:Z

    if-eqz v0, :cond_0

    .line 1355803
    iget-boolean v0, p0, LX/8Y7;->q:Z

    if-eqz v0, :cond_1

    .line 1355804
    iget-object v0, p0, LX/8Y7;->i:Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1355805
    iget-object v0, p0, LX/8Y7;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1355806
    iget-object v0, p0, LX/8Y7;->f:LX/8T7;

    iget-object v1, p0, LX/8Y7;->i:Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    new-instance v2, LX/8Y2;

    invoke-direct {v2, p0}, LX/8Y2;-><init>(LX/8Y7;)V

    invoke-virtual {v0, v1, v2}, LX/8T7;->e(Landroid/view/View;LX/8Sl;)V

    .line 1355807
    :cond_0
    :goto_0
    return-void

    .line 1355808
    :cond_1
    iget-object v0, p0, LX/8Y7;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1355809
    iget-object v0, p0, LX/8Y7;->i:Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->setVisibility(I)V

    .line 1355810
    iget-object v0, p0, LX/8Y7;->f:LX/8T7;

    iget-object v1, p0, LX/8Y7;->h:Landroid/widget/TextView;

    new-instance v2, LX/8Y3;

    invoke-direct {v2, p0}, LX/8Y3;-><init>(LX/8Y7;)V

    invoke-virtual {v0, v1, v2}, LX/8T7;->e(Landroid/view/View;LX/8Sl;)V

    goto :goto_0
.end method

.method public static l(LX/8Y7;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1355811
    iget-object v0, p0, LX/8Y7;->g:LX/8Sk;

    if-eqz v0, :cond_0

    .line 1355812
    iget-object v0, p0, LX/8Y7;->g:LX/8Sk;

    const/4 v1, 0x0

    sget-object v2, LX/8TX;->START_SCREEN:LX/8TX;

    invoke-virtual {v0, v1, v2, v3}, LX/8Sk;->a(LX/8Vb;LX/8TX;I)V

    .line 1355813
    :cond_0
    iput-boolean v3, p0, LX/8Y7;->s:Z

    .line 1355814
    const/4 v2, 0x0

    .line 1355815
    iget-boolean v0, p0, LX/8Y7;->q:Z

    if-eqz v0, :cond_1

    .line 1355816
    iget-object v0, p0, LX/8Y7;->f:LX/8T7;

    iget-object v1, p0, LX/8Y7;->i:Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    invoke-virtual {v0, v1, v2}, LX/8T7;->f(Landroid/view/View;LX/8Sl;)V

    .line 1355817
    :goto_0
    return-void

    .line 1355818
    :cond_1
    iget-object v0, p0, LX/8Y7;->f:LX/8T7;

    iget-object v1, p0, LX/8Y7;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2}, LX/8T7;->f(Landroid/view/View;LX/8Sl;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1355787
    invoke-super {p0}, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->a()V

    .line 1355788
    iget-object v0, p0, LX/8Y7;->j:Lcom/facebook/quicksilver/views/loading/GameLoadingCard;

    iget-object v1, p0, LX/8Y7;->a:LX/8TS;

    .line 1355789
    iget-object v4, v1, LX/8TS;->e:LX/8TO;

    move-object v1, v4

    .line 1355790
    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->setGameInfo(LX/8TO;)V

    .line 1355791
    iput-boolean v2, p0, LX/8Y7;->r:Z

    .line 1355792
    iput-boolean v2, p0, LX/8Y7;->s:Z

    .line 1355793
    iget-object v0, p0, LX/8Y7;->j:Lcom/facebook/quicksilver/views/loading/GameLoadingCard;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->setAlpha(F)V

    .line 1355794
    iget-object v0, p0, LX/8Y7;->j:Lcom/facebook/quicksilver/views/loading/GameLoadingCard;

    invoke-virtual {v0, v2}, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->setProgress(I)V

    .line 1355795
    iget-object v0, p0, LX/8Y7;->j:Lcom/facebook/quicksilver/views/loading/GameLoadingCard;

    invoke-virtual {v0, v2}, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->setVisibility(I)V

    .line 1355796
    iget-object v0, p0, LX/8Y7;->k:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    invoke-virtual {v0, v3}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->setVisibility(I)V

    .line 1355797
    iget-object v0, p0, LX/8Y7;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1355798
    iget-object v0, p0, LX/8Y7;->i:Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    invoke-virtual {v0, v3}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->setVisibility(I)V

    .line 1355799
    invoke-virtual {p0}, LX/8Y7;->c()V

    .line 1355800
    invoke-static {p0, v2}, LX/8Y7;->b(LX/8Y7;Z)V

    .line 1355801
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1355785
    iget-object v0, p0, LX/8Y7;->j:Lcom/facebook/quicksilver/views/loading/GameLoadingCard;

    invoke-virtual {v0, p1}, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->setMaxProgress(I)V

    .line 1355786
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1355782
    iget-object v0, p0, LX/8Y7;->j:Lcom/facebook/quicksilver/views/loading/GameLoadingCard;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->a()V

    .line 1355783
    iget-object v0, p0, LX/8Y7;->j:Lcom/facebook/quicksilver/views/loading/GameLoadingCard;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/8Xx;

    invoke-direct {v1, p0}, LX/8Xx;-><init>(LX/8Y7;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1355784
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1355780
    iget-object v0, p0, LX/8Y7;->j:Lcom/facebook/quicksilver/views/loading/GameLoadingCard;

    invoke-virtual {v0, p1}, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->setProgress(I)V

    .line 1355781
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1355778
    iget-object v0, p0, LX/8Y7;->f:LX/8T7;

    iget-object v1, p0, LX/8Y7;->k:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    new-instance v2, LX/8Y6;

    invoke-direct {v2, p0}, LX/8Y6;-><init>(LX/8Y7;)V

    invoke-virtual {v0, v1, v2}, LX/8T7;->b(Landroid/view/View;LX/8Sl;)V

    .line 1355779
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x55c3d140

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1355775
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->onSizeChanged(IIII)V

    .line 1355776
    const/4 v1, 0x0

    invoke-static {p0, v1}, LX/8Y7;->b(LX/8Y7;Z)V

    .line 1355777
    const/16 v1, 0x2d

    const v2, -0x405e293e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setCallbackDelegate(LX/8Sk;)V
    .locals 0

    .prologue
    .line 1355773
    iput-object p1, p0, LX/8Y7;->g:LX/8Sk;

    .line 1355774
    return-void
.end method
