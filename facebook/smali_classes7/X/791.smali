.class public LX/791;
.super LX/78l;
.source ""


# static fields
.field private static final b:Ljava/lang/UnsupportedOperationException;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1173755
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Provide a binding to your own implementation"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/791;->b:Ljava/lang/UnsupportedOperationException;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1173753
    invoke-direct {p0, p1}, LX/78l;-><init>(Landroid/content/Context;)V

    .line 1173754
    sget-object v0, LX/791;->b:Ljava/lang/UnsupportedOperationException;

    throw v0
.end method


# virtual methods
.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1173752
    sget-object v0, LX/791;->b:Ljava/lang/UnsupportedOperationException;

    throw v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1173751
    sget-object v0, LX/791;->b:Ljava/lang/UnsupportedOperationException;

    throw v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1173750
    sget-object v0, LX/791;->b:Ljava/lang/UnsupportedOperationException;

    throw v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1173748
    sget-object v0, LX/791;->b:Ljava/lang/UnsupportedOperationException;

    throw v0
.end method

.method public final i()LX/795;
    .locals 1

    .prologue
    .line 1173749
    sget-object v0, LX/791;->b:Ljava/lang/UnsupportedOperationException;

    throw v0
.end method
