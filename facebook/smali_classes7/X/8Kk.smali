.class public LX/8Kk;
.super LX/0b5;
.source ""


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V
    .locals 1

    .prologue
    .line 1330548
    invoke-direct {p0, p1, p2, p3}, LX/0b5;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V

    .line 1330549
    sget-object v0, LX/8KZ;->PROCESSING:LX/8KZ;

    if-ne p2, v0, :cond_1

    .line 1330550
    const/high16 p0, 0x42c80000    # 100.0f

    .line 1330551
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-ltz v0, :cond_0

    cmpg-float v0, p3, p0

    if-gez v0, :cond_0

    .line 1330552
    iget v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->X:I

    int-to-float v0, v0

    mul-float/2addr v0, p3

    div-float/2addr v0, p0

    .line 1330553
    iget p0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->Y:F

    invoke-static {v0, p0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->Y:F

    .line 1330554
    :cond_0
    :goto_0
    return-void

    .line 1330555
    :cond_1
    sget-object v0, LX/8KZ;->UPLOADING:LX/8KZ;

    if-ne p2, v0, :cond_0

    .line 1330556
    iget v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->Z:F

    invoke-static {p3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->Z:F

    .line 1330557
    goto :goto_0
.end method
