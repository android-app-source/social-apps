.class public abstract LX/72Q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<STY",
        "LE_RENDERER::Lcom/facebook/payments/shipping/form/ShippingStyleRenderer;",
        "MUTATOR::",
        "Lcom/facebook/payments/shipping/form/ShippingAddressMutator;",
        "STATE_INPUT_VA",
        "LIDATOR::Lcom/facebook/payments/shipping/validation/ShippingStateInputValidator;",
        "ZIP_INPUT_VA",
        "LIDATOR::Lcom/facebook/payments/shipping/validation/ShippingZipInputValidator;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/72g;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TSTY",
            "LE_RENDERER;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TMUTATOR;>;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TSTATE_INPUT_VA",
            "LIDATOR;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TZIP_INPUT_VA",
            "LIDATOR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/72g;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/72g;",
            "LX/0Ot",
            "<TSTY",
            "LE_RENDERER;",
            ">;",
            "LX/0Ot",
            "<TMUTATOR;>;",
            "LX/0Ot",
            "<TSTATE_INPUT_VA",
            "LIDATOR;",
            ">;",
            "LX/0Ot",
            "<TZIP_INPUT_VA",
            "LIDATOR;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1164494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164495
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/72g;

    iput-object v0, p0, LX/72Q;->a:LX/72g;

    .line 1164496
    iput-object p2, p0, LX/72Q;->b:LX/0Ot;

    .line 1164497
    iput-object p3, p0, LX/72Q;->c:LX/0Ot;

    .line 1164498
    iput-object p4, p0, LX/72Q;->d:LX/0Ot;

    .line 1164499
    iput-object p5, p0, LX/72Q;->e:LX/0Ot;

    .line 1164500
    return-void
.end method
