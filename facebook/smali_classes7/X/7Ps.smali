.class public final LX/7Ps;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/1m2;

.field private b:Ljava/lang/String;

.field private c:Landroid/net/Uri;

.field private d:I

.field public e:J

.field public f:J

.field private g:J

.field public h:J

.field private i:J

.field private j:Z

.field private k:LX/2WF;

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:LX/3DY;

.field public p:LX/3DY;


# direct methods
.method public constructor <init>(LX/1m2;Ljava/lang/String;Landroid/net/Uri;IZ)V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 1203433
    iput-object p1, p0, LX/7Ps;->a:LX/1m2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1203434
    iput-object p2, p0, LX/7Ps;->b:Ljava/lang/String;

    .line 1203435
    iput-object p3, p0, LX/7Ps;->c:Landroid/net/Uri;

    .line 1203436
    iput p4, p0, LX/7Ps;->d:I

    .line 1203437
    iput-wide v0, p0, LX/7Ps;->e:J

    .line 1203438
    iput-wide v0, p0, LX/7Ps;->f:J

    .line 1203439
    iget-object v0, p1, LX/1m2;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/7Ps;->g:J

    .line 1203440
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/7Ps;->h:J

    .line 1203441
    iput-boolean p5, p0, LX/7Ps;->l:Z

    .line 1203442
    new-instance v0, LX/7Pq;

    invoke-direct {v0, p0, p1}, LX/7Pq;-><init>(LX/7Ps;LX/1m2;)V

    iput-object v0, p0, LX/7Ps;->o:LX/3DY;

    .line 1203443
    new-instance v0, LX/7Pr;

    invoke-direct {v0, p0, p1}, LX/7Pr;-><init>(LX/7Ps;LX/1m2;)V

    iput-object v0, p0, LX/7Ps;->p:LX/3DY;

    .line 1203444
    return-void
.end method

.method private static d(LX/7Ps;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1203445
    iget-object v0, p0, LX/7Ps;->a:LX/1m2;

    iget-object v0, v0, LX/1m2;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/7Ps;->i:J

    .line 1203446
    iget-object v0, p0, LX/7Ps;->a:LX/1m2;

    iget-object v0, v0, LX/1m2;->b:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1203447
    if-nez v2, :cond_1

    const-string v0, "not_available"

    :goto_0
    iput-object v0, p0, LX/7Ps;->m:Ljava/lang/String;

    .line 1203448
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, LX/7Ps;->n:Z

    .line 1203449
    iget-boolean v0, p0, LX/7Ps;->l:Z

    if-eqz v0, :cond_0

    .line 1203450
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "video_download"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/7Ps;->n:Z

    if-eqz v0, :cond_3

    const-string v0, "_wifi"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_received"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1203451
    iget-object v1, p0, LX/7Ps;->a:LX/1m2;

    iget-object v1, v1, LX/1m2;->d:LX/0Vw;

    iget-wide v2, p0, LX/7Ps;->e:J

    invoke-virtual {v1, v0, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 1203452
    :cond_0
    return-void

    .line 1203453
    :cond_1
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1203454
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1203455
    :cond_3
    const-string v0, "_mobile"

    goto :goto_2
.end method


# virtual methods
.method public final a()LX/3DY;
    .locals 1

    .prologue
    .line 1203456
    iget-object v0, p0, LX/7Ps;->o:LX/3DY;

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 22

    .prologue
    .line 1203457
    invoke-static/range {p0 .. p0}, LX/7Ps;->d(LX/7Ps;)V

    .line 1203458
    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Ps;->a:LX/1m2;

    iget-object v2, v2, LX/1m2;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0A1;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/7Ps;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7Ps;->c:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget v6, v0, LX/7Ps;->d:I

    move-object/from16 v0, p0

    iget-wide v8, v0, LX/7Ps;->f:J

    move-object/from16 v0, p0

    iget-wide v10, v0, LX/7Ps;->e:J

    move-object/from16 v0, p0

    iget-boolean v12, v0, LX/7Ps;->j:Z

    move-object/from16 v0, p0

    iget-object v13, v0, LX/7Ps;->k:LX/2WF;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/7Ps;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v15, v0, LX/7Ps;->n:Z

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/7Ps;->i:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/7Ps;->g:J

    move-wide/from16 v18, v0

    sub-long v16, v16, v18

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/7Ps;->h:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/7Ps;->g:J

    move-wide/from16 v20, v0

    sub-long v18, v18, v20

    move-object/from16 v7, p1

    invoke-virtual/range {v3 .. v19}, LX/0A1;->a(Ljava/lang/String;Landroid/net/Uri;ILjava/lang/Throwable;JJZLX/2WF;Ljava/lang/String;ZJJ)V

    .line 1203459
    return-void
.end method

.method public final a(ZLX/2WF;)V
    .locals 0

    .prologue
    .line 1203460
    iput-boolean p1, p0, LX/7Ps;->j:Z

    .line 1203461
    iput-object p2, p0, LX/7Ps;->k:LX/2WF;

    .line 1203462
    return-void
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 12

    .prologue
    .line 1203463
    iget-object v0, p0, LX/7Ps;->a:LX/1m2;

    iget-object v0, v0, LX/1m2;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0A1;

    iget-object v1, p0, LX/7Ps;->b:Ljava/lang/String;

    iget-object v2, p0, LX/7Ps;->c:Landroid/net/Uri;

    iget v3, p0, LX/7Ps;->d:I

    iget-boolean v5, p0, LX/7Ps;->j:Z

    iget-object v6, p0, LX/7Ps;->k:LX/2WF;

    iget-object v7, p0, LX/7Ps;->m:Ljava/lang/String;

    iget-wide v8, p0, LX/7Ps;->i:J

    iget-wide v10, p0, LX/7Ps;->g:J

    sub-long/2addr v8, v10

    move-object v4, p1

    invoke-virtual/range {v0 .. v9}, LX/0A1;->a(Ljava/lang/String;Landroid/net/Uri;ILjava/lang/Throwable;ZLX/2WF;Ljava/lang/String;J)V

    .line 1203464
    return-void
.end method

.method public final c()V
    .locals 20

    .prologue
    .line 1203465
    invoke-static/range {p0 .. p0}, LX/7Ps;->d(LX/7Ps;)V

    .line 1203466
    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Ps;->a:LX/1m2;

    iget-object v2, v2, LX/1m2;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0A1;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7Ps;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/7Ps;->c:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget v5, v0, LX/7Ps;->d:I

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/7Ps;->f:J

    move-object/from16 v0, p0

    iget-wide v8, v0, LX/7Ps;->e:J

    move-object/from16 v0, p0

    iget-boolean v10, v0, LX/7Ps;->j:Z

    move-object/from16 v0, p0

    iget-object v11, v0, LX/7Ps;->k:LX/2WF;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/7Ps;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, LX/7Ps;->n:Z

    move-object/from16 v0, p0

    iget-wide v14, v0, LX/7Ps;->i:J

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/7Ps;->g:J

    move-wide/from16 v16, v0

    sub-long v14, v14, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/7Ps;->h:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/7Ps;->g:J

    move-wide/from16 v18, v0

    sub-long v16, v16, v18

    invoke-virtual/range {v2 .. v17}, LX/0A1;->a(Ljava/lang/String;Landroid/net/Uri;IJJZLX/2WF;Ljava/lang/String;ZJJ)V

    .line 1203467
    return-void
.end method
