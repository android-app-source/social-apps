.class public final LX/7mF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1236743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1236744
    new-instance v0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;

    invoke-direct {v0, p1}, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1236745
    new-array v0, p1, [Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;

    return-object v0
.end method
