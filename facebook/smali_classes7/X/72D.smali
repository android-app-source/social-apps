.class public final LX/72D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/shipping/model/MailingAddress;

.field public final synthetic b:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;Lcom/facebook/payments/shipping/model/MailingAddress;)V
    .locals 0

    .prologue
    .line 1164049
    iput-object p1, p0, LX/72D;->b:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iput-object p2, p0, LX/72D;->a:Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1164051
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1164052
    iget-object v1, p0, LX/72D;->a:Lcom/facebook/payments/shipping/model/MailingAddress;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/72D;->a:Lcom/facebook/payments/shipping/model/MailingAddress;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/72D;->a:Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/MailingAddress;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1164053
    :cond_0
    :goto_0
    return-void

    .line 1164054
    :cond_1
    iget-object v0, p0, LX/72D;->b:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->d:LX/72M;

    iget-object v1, p0, LX/72D;->b:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    invoke-virtual {v1}, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/72M;->a(Z)V

    goto :goto_0
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1164050
    return-void
.end method
