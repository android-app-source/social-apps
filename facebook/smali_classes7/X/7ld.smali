.class public final LX/7ld;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 1235334
    const/4 v13, 0x0

    .line 1235335
    const-wide/16 v14, 0x0

    .line 1235336
    const/4 v12, 0x0

    .line 1235337
    const/4 v11, 0x0

    .line 1235338
    const/4 v10, 0x0

    .line 1235339
    const/4 v9, 0x0

    .line 1235340
    const/4 v8, 0x0

    .line 1235341
    const/4 v7, 0x0

    .line 1235342
    const/4 v6, 0x0

    .line 1235343
    const/4 v5, 0x0

    .line 1235344
    const/4 v4, 0x0

    .line 1235345
    const/4 v3, 0x0

    .line 1235346
    const/4 v2, 0x0

    .line 1235347
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_10

    .line 1235348
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1235349
    const/4 v2, 0x0

    .line 1235350
    :goto_0
    return v2

    .line 1235351
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_d

    .line 1235352
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1235353
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1235354
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v18

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 1235355
    const-string v6, "__type__"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "__typename"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1235356
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 1235357
    :cond_2
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1235358
    const/4 v2, 0x1

    .line 1235359
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1235360
    :cond_3
    const-string v6, "creator"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1235361
    invoke-static/range {p0 .. p1}, LX/5u6;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 1235362
    :cond_4
    const-string v6, "feedback"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1235363
    invoke-static/range {p0 .. p1}, LX/4aV;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 1235364
    :cond_5
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1235365
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 1235366
    :cond_6
    const-string v6, "page_rating"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1235367
    const/4 v2, 0x1

    .line 1235368
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v14, v6

    goto/16 :goto_1

    .line 1235369
    :cond_7
    const-string v6, "photos"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1235370
    invoke-static/range {p0 .. p1}, LX/7lc;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 1235371
    :cond_8
    const-string v6, "privacy_scope"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1235372
    invoke-static/range {p0 .. p1}, LX/5uE;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1235373
    :cond_9
    const-string v6, "reviewer_context"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1235374
    invoke-static/range {p0 .. p1}, LX/5uA;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 1235375
    :cond_a
    const-string v6, "story"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1235376
    invoke-static/range {p0 .. p1}, LX/5u7;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 1235377
    :cond_b
    const-string v6, "value"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1235378
    invoke-static/range {p0 .. p1}, LX/5u1;->a(LX/15w;LX/186;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 1235379
    :cond_c
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1235380
    :cond_d
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1235381
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1235382
    if-eqz v3, :cond_e

    .line 1235383
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1235384
    :cond_e
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1235385
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1235386
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1235387
    if-eqz v8, :cond_f

    .line 1235388
    const/4 v2, 0x5

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14, v3}, LX/186;->a(III)V

    .line 1235389
    :cond_f
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1235390
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1235391
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1235392
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1235393
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1235394
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_10
    move/from16 v16, v11

    move/from16 v17, v12

    move v11, v6

    move v12, v7

    move v7, v13

    move v13, v8

    move v8, v2

    move/from16 v19, v10

    move v10, v5

    move/from16 v20, v9

    move v9, v4

    move-wide v4, v14

    move/from16 v14, v20

    move/from16 v15, v19

    goto/16 :goto_1
.end method
