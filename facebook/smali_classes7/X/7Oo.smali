.class public final LX/7Oo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Da;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Da",
        "<",
        "LX/37C;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7Op;

.field public final b:LX/37C;

.field private final c:LX/3Dd;

.field private d:LX/3Da;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Da",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/3Da;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Da",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7Op;LX/37C;LX/3Da;LX/3Da;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/37C;",
            "LX/3Da",
            "<",
            "LX/37C;",
            ">;",
            "LX/3Da",
            "<",
            "LX/37C;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1202197
    iput-object p1, p0, LX/7Oo;->a:LX/7Op;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202198
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/37C;

    iput-object v0, p0, LX/7Oo;->b:LX/37C;

    .line 1202199
    invoke-static {p3, p4}, LX/7Oo;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Da;

    .line 1202200
    invoke-interface {v0}, LX/3Da;->b()LX/3Dd;

    move-result-object v0

    iput-object v0, p0, LX/7Oo;->c:LX/3Dd;

    .line 1202201
    iput-object p3, p0, LX/7Oo;->d:LX/3Da;

    .line 1202202
    iput-object p4, p0, LX/7Oo;->e:LX/3Da;

    .line 1202203
    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 1202149
    if-eqz p0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    move-object p0, p1

    goto :goto_0
.end method

.method private f()J
    .locals 2

    .prologue
    .line 1202150
    iget-object v0, p0, LX/7Oo;->c:LX/3Dd;

    iget-wide v0, v0, LX/3Dd;->a:J

    return-wide v0
.end method


# virtual methods
.method public final a(J)Ljava/io/OutputStream;
    .locals 9

    .prologue
    .line 1202151
    iget-object v0, p0, LX/7Oo;->b:LX/37C;

    move-object v1, v0

    .line 1202152
    iget v1, v1, LX/37C;->a:I

    int-to-long v6, v1

    .line 1202153
    cmp-long v1, v6, p1

    if-gtz v1, :cond_1

    .line 1202154
    iget-object v1, p0, LX/7Oo;->e:LX/3Da;

    if-nez v1, :cond_0

    .line 1202155
    iget-object v1, p0, LX/7Oo;->a:LX/7Op;

    iget-object v1, v1, LX/7Op;->b:LX/1Ln;

    .line 1202156
    iget-object v0, p0, LX/7Oo;->b:LX/37C;

    move-object v2, v0

    .line 1202157
    invoke-virtual {p0}, LX/7Oo;->b()LX/3Dd;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/1Ln;->a(Ljava/lang/Object;LX/3Dd;)LX/3Da;

    move-result-object v1

    iput-object v1, p0, LX/7Oo;->e:LX/3Da;

    .line 1202158
    :cond_0
    iget-object v1, p0, LX/7Oo;->e:LX/3Da;

    invoke-interface {v1, p1, p2}, LX/3Da;->a(J)Ljava/io/OutputStream;

    move-result-object v1

    .line 1202159
    :goto_0
    return-object v1

    .line 1202160
    :cond_1
    iget-object v1, p0, LX/7Oo;->d:LX/3Da;

    if-nez v1, :cond_2

    .line 1202161
    iget-object v1, p0, LX/7Oo;->a:LX/7Op;

    iget-object v1, v1, LX/7Op;->a:LX/1Ln;

    .line 1202162
    iget-object v0, p0, LX/7Oo;->b:LX/37C;

    move-object v2, v0

    .line 1202163
    invoke-virtual {p0}, LX/7Oo;->b()LX/3Dd;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/1Ln;->a(Ljava/lang/Object;LX/3Dd;)LX/3Da;

    move-result-object v1

    iput-object v1, p0, LX/7Oo;->d:LX/3Da;

    .line 1202164
    :cond_2
    iget-object v1, p0, LX/7Oo;->d:LX/3Da;

    invoke-interface {v1, p1, p2}, LX/3Da;->a(J)Ljava/io/OutputStream;

    move-result-object v5

    .line 1202165
    new-instance v1, LX/7On;

    move-object v2, p0

    move-wide v3, p1

    invoke-direct/range {v1 .. v7}, LX/7On;-><init>(LX/7Oo;JLjava/io/OutputStream;J)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1202166
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getCacheKey not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()LX/3Dd;
    .locals 1

    .prologue
    .line 1202167
    iget-object v0, p0, LX/7Oo;->c:LX/3Dd;

    return-object v0
.end method

.method public final b(J)Ljava/io/InputStream;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1202168
    iget-object v0, p0, LX/7Oo;->b:LX/37C;

    move-object v0, v0

    .line 1202169
    iget v0, v0, LX/37C;->a:I

    int-to-long v2, v0

    cmp-long v0, p1, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    .line 1202170
    :goto_0
    if-eqz v0, :cond_1

    iget-object v2, p0, LX/7Oo;->d:LX/3Da;

    if-eqz v2, :cond_1

    .line 1202171
    iget-object v0, p0, LX/7Oo;->d:LX/3Da;

    invoke-interface {v0, p1, p2}, LX/3Da;->b(J)Ljava/io/InputStream;

    move-result-object v0

    .line 1202172
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 1202173
    goto :goto_0

    .line 1202174
    :cond_1
    if-nez v0, :cond_2

    iget-object v0, p0, LX/7Oo;->e:LX/3Da;

    if-eqz v0, :cond_2

    .line 1202175
    iget-object v0, p0, LX/7Oo;->e:LX/3Da;

    invoke-interface {v0, p1, p2}, LX/3Da;->b(J)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_1

    .line 1202176
    :cond_2
    new-instance v0, Ljava/io/ByteArrayInputStream;

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    goto :goto_1
.end method

.method public final c()J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 1202177
    iget-object v2, p0, LX/7Oo;->d:LX/3Da;

    if-eqz v2, :cond_0

    .line 1202178
    iget-object v2, p0, LX/7Oo;->d:LX/3Da;

    invoke-interface {v2}, LX/3Da;->c()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 1202179
    :cond_0
    iget-object v2, p0, LX/7Oo;->e:LX/3Da;

    if-eqz v2, :cond_1

    .line 1202180
    iget-object v2, p0, LX/7Oo;->e:LX/3Da;

    invoke-interface {v2}, LX/3Da;->c()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 1202181
    :cond_1
    return-wide v0
.end method

.method public final d()J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 1202182
    iget-object v2, p0, LX/7Oo;->d:LX/3Da;

    if-eqz v2, :cond_0

    .line 1202183
    iget-object v2, p0, LX/7Oo;->d:LX/3Da;

    invoke-interface {v2}, LX/3Da;->c()J

    move-result-wide v2

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1202184
    :cond_0
    iget-object v2, p0, LX/7Oo;->e:LX/3Da;

    if-eqz v2, :cond_1

    .line 1202185
    iget-object v2, p0, LX/7Oo;->e:LX/3Da;

    invoke-interface {v2}, LX/3Da;->c()J

    move-result-wide v2

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 1202186
    :cond_1
    return-wide v0
.end method

.method public final g()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2WF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1202187
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1202188
    iget-object v1, p0, LX/7Oo;->b:LX/37C;

    move-object v1, v1

    .line 1202189
    iget v1, v1, LX/37C;->a:I

    .line 1202190
    iget-object v2, p0, LX/7Oo;->d:LX/3Da;

    if-eqz v2, :cond_0

    .line 1202191
    new-instance v2, LX/2WF;

    const-wide/16 v4, 0x0

    int-to-long v6, v1

    invoke-direct {v2, v4, v5, v6, v7}, LX/2WF;-><init>(JJ)V

    iget-object v3, p0, LX/7Oo;->d:LX/3Da;

    invoke-interface {v3}, LX/3Da;->g()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2WF;->a(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1202192
    :cond_0
    iget-object v2, p0, LX/7Oo;->e:LX/3Da;

    if-eqz v2, :cond_1

    invoke-direct {p0}, LX/7Oo;->f()J

    move-result-wide v2

    int-to-long v4, v1

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 1202193
    new-instance v2, LX/2WF;

    int-to-long v4, v1

    invoke-direct {p0}, LX/7Oo;->f()J

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, LX/2WF;-><init>(JJ)V

    iget-object v1, p0, LX/7Oo;->e:LX/3Da;

    invoke-interface {v1}, LX/3Da;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/2WF;->a(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1202194
    :cond_1
    return-object v0
.end method

.method public final synthetic h()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1202195
    iget-object v0, p0, LX/7Oo;->b:LX/37C;

    move-object v0, v0

    .line 1202196
    return-object v0
.end method
