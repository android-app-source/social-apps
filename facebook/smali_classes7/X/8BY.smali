.class public LX/8BY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/8BZ;",
        "LX/8Bd;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1309456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 9

    .prologue
    .line 1309410
    check-cast p1, LX/8BZ;

    const/4 v6, 0x1

    .line 1309411
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1309412
    iget-object v1, p1, LX/8BZ;->d:Lcom/facebook/media/upload/MediaUploadParameters;

    move-object v1, v1

    .line 1309413
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "start_offset"

    .line 1309414
    iget-wide v7, p1, LX/8BZ;->b:J

    move-wide v4, v7

    .line 1309415
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1309416
    iget-object v2, v1, Lcom/facebook/media/upload/MediaUploadParameters;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1309417
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1309418
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "composer_session_id"

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1309419
    :cond_0
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "target"

    .line 1309420
    iget-object v5, v1, Lcom/facebook/media/upload/MediaUploadParameters;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1309421
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1309422
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "upload_speed"

    .line 1309423
    iget v5, p1, LX/8BZ;->c:F

    move v5, v5

    .line 1309424
    invoke-static {v5}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1309425
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "upload_phase"

    const-string v5, "transfer"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1309426
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "upload_session_id"

    .line 1309427
    iget-object v5, p1, LX/8BZ;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1309428
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1309429
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "fbuploader_video_file_chunk"

    .line 1309430
    iget-object v5, p1, LX/8BZ;->e:Ljava/lang/String;

    move-object v5, v5

    .line 1309431
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1309432
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "v2.3/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1309433
    iget-object v4, v1, Lcom/facebook/media/upload/MediaUploadParameters;->b:Ljava/lang/String;

    move-object v1, v4

    .line 1309434
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/videos"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1309435
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v3

    const-string v4, "upload-video-chunk-receive"

    .line 1309436
    iput-object v4, v3, LX/14O;->b:Ljava/lang/String;

    .line 1309437
    move-object v3, v3

    .line 1309438
    const-string v4, "POST"

    .line 1309439
    iput-object v4, v3, LX/14O;->c:Ljava/lang/String;

    .line 1309440
    move-object v3, v3

    .line 1309441
    iput-object v1, v3, LX/14O;->d:Ljava/lang/String;

    .line 1309442
    move-object v1, v3

    .line 1309443
    sget-object v3, LX/14S;->JSON:LX/14S;

    .line 1309444
    iput-object v3, v1, LX/14O;->k:LX/14S;

    .line 1309445
    move-object v1, v1

    .line 1309446
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1309447
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1309448
    move-object v0, v1

    .line 1309449
    iput-boolean v6, v0, LX/14O;->n:Z

    .line 1309450
    move-object v0, v0

    .line 1309451
    iput-boolean v6, v0, LX/14O;->p:Z

    .line 1309452
    move-object v0, v0

    .line 1309453
    iput-object v2, v0, LX/14O;->A:Ljava/lang/String;

    .line 1309454
    move-object v0, v0

    .line 1309455
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1309457
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1309458
    new-instance v1, LX/8Bd;

    const-string v2, "start_offset"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->D()J

    move-result-wide v2

    const-string v4, "end_offset"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->D()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, LX/8Bd;-><init>(JJ)V

    return-object v1
.end method
