.class public final LX/7Po;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1Ln;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ln",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Sh;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/03V;

.field public final e:LX/1m2;

.field public final f:LX/1Lt;

.field private final g:Ljava/util/concurrent/ScheduledExecutorService;

.field private final h:LX/0So;

.field private final i:LX/7PM;

.field private final j:LX/0oz;

.field private final k:LX/1Lw;

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Lv;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/04m;

.field private final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0ad;

.field public final p:LX/19Z;

.field public final q:LX/16V;

.field private final r:LX/0WJ;

.field private final s:LX/0lC;

.field private final t:LX/1Lu;

.field public final u:LX/0wr;


# direct methods
.method public constructor <init>(LX/1Ln;LX/0Sh;LX/16V;LX/03V;LX/0Or;LX/1m2;LX/0ad;LX/0Ot;LX/0So;LX/19Z;LX/1Lt;Ljava/util/concurrent/ScheduledExecutorService;LX/7PM;LX/0oz;LX/1Lw;LX/0WJ;LX/0lC;LX/0Or;LX/0wr;LX/1Lu;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ln",
            "<",
            "LX/37C;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/16V;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/video/server/VideoServerListener;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/1Lv;",
            ">;",
            "LX/0So;",
            "LX/19Z;",
            "Lcom/facebook/video/server/VideoKeyCreator;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/7PM;",
            "LX/0oz;",
            "Lcom/facebook/video/server/NetworkProcessor;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0lC;",
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;",
            "LX/0wr;",
            "LX/1Lu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1203397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1203398
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ln;

    iput-object v1, p0, LX/7Po;->a:LX/1Ln;

    .line 1203399
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Sh;

    iput-object v1, p0, LX/7Po;->b:LX/0Sh;

    .line 1203400
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16V;

    iput-object v1, p0, LX/7Po;->q:LX/16V;

    .line 1203401
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    iput-object v1, p0, LX/7Po;->d:LX/03V;

    .line 1203402
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Or;

    iput-object v1, p0, LX/7Po;->c:LX/0Or;

    .line 1203403
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1m2;

    iput-object v1, p0, LX/7Po;->e:LX/1m2;

    .line 1203404
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    iput-object v1, p0, LX/7Po;->o:LX/0ad;

    .line 1203405
    iput-object p8, p0, LX/7Po;->l:LX/0Ot;

    .line 1203406
    iput-object p9, p0, LX/7Po;->h:LX/0So;

    .line 1203407
    iput-object p10, p0, LX/7Po;->p:LX/19Z;

    .line 1203408
    invoke-static {p11}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Lt;

    iput-object v1, p0, LX/7Po;->f:LX/1Lt;

    .line 1203409
    iput-object p12, p0, LX/7Po;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1203410
    iput-object p13, p0, LX/7Po;->i:LX/7PM;

    .line 1203411
    move-object/from16 v0, p14

    iput-object v0, p0, LX/7Po;->j:LX/0oz;

    .line 1203412
    move-object/from16 v0, p15

    iput-object v0, p0, LX/7Po;->k:LX/1Lw;

    .line 1203413
    move-object/from16 v0, p16

    iput-object v0, p0, LX/7Po;->r:LX/0WJ;

    .line 1203414
    move-object/from16 v0, p17

    iput-object v0, p0, LX/7Po;->s:LX/0lC;

    .line 1203415
    move-object/from16 v0, p19

    iput-object v0, p0, LX/7Po;->u:LX/0wr;

    .line 1203416
    move-object/from16 v0, p20

    iput-object v0, p0, LX/7Po;->t:LX/1Lu;

    .line 1203417
    new-instance v1, LX/7Oq;

    invoke-direct {v1, p9}, LX/7Oq;-><init>(LX/0So;)V

    iput-object v1, p0, LX/7Po;->m:LX/04m;

    .line 1203418
    move-object/from16 v0, p18

    iput-object v0, p0, LX/7Po;->n:LX/0Or;

    .line 1203419
    return-void
.end method

.method private static a(LX/7Po;LX/3DW;LX/3DY;LX/7Pl;LX/04n;)LX/3DW;
    .locals 2

    .prologue
    .line 1203395
    new-instance v0, LX/7PX;

    invoke-direct {v0, p0, p2, p3, p4}, LX/7PX;-><init>(LX/7Po;LX/3DY;LX/7Pl;LX/04n;)V

    .line 1203396
    new-instance v1, LX/7PZ;

    invoke-direct {v1, p0, p1, v0}, LX/7PZ;-><init>(LX/7Po;LX/3DW;LX/3DY;)V

    return-object v1
.end method

.method private static a(LX/7Po;LX/3DW;Ljava/lang/String;)LX/3DW;
    .locals 2

    .prologue
    .line 1203393
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1203394
    :goto_0
    return-object p1

    :cond_0
    new-instance v0, LX/7Pe;

    iget-object v1, p0, LX/7Po;->m:LX/04m;

    invoke-direct {v0, p0, p1, p2, v1}, LX/7Pe;-><init>(LX/7Po;LX/3DW;Ljava/lang/String;LX/04n;)V

    move-object p1, v0

    goto :goto_0
.end method

.method private static a(LX/7Oi;)LX/7Oi;
    .locals 4

    .prologue
    .line 1203392
    new-instance v0, LX/7Om;

    const-wide/32 v2, 0x32000

    invoke-direct {v0, p0, v2, v3}, LX/7Om;-><init>(LX/7Oi;J)V

    return-object v0
.end method

.method private static a(LX/7Po;LX/7Oi;LX/1Ln;LX/37C;)LX/7Oi;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Oi;",
            "LX/1Ln",
            "<",
            "LX/37C;",
            ">;",
            "LX/37C;",
            ")",
            "LX/7Oi;"
        }
    .end annotation

    .prologue
    .line 1203390
    iget-object v0, p0, LX/7Po;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1203391
    :goto_0
    return-object p1

    :cond_0
    new-instance v0, LX/7Ok;

    iget-object v1, p0, LX/7Po;->n:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wp;

    iget-wide v4, v1, LX/0wp;->M:J

    move-object v1, p2

    move-object v2, p3

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/7Ok;-><init>(LX/1Ln;LX/37C;LX/7Oi;J)V

    move-object p1, v0

    goto :goto_0
.end method

.method private static a(LX/7Po;LX/7Oi;LX/3DY;LX/7Pl;LX/04n;)LX/7Oi;
    .locals 2

    .prologue
    .line 1203388
    new-instance v0, LX/7PV;

    invoke-direct {v0, p0, p2, p3, p4}, LX/7PV;-><init>(LX/7Po;LX/3DY;LX/7Pl;LX/04n;)V

    .line 1203389
    new-instance v1, LX/7PW;

    invoke-direct {v1, p0, v0, p1}, LX/7PW;-><init>(LX/7Po;LX/3DY;LX/7Oi;)V

    return-object v1
.end method

.method private static a(LX/7Po;LX/7Oi;Ljava/lang/String;)LX/7Oi;
    .locals 2

    .prologue
    .line 1203386
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1203387
    :goto_0
    return-object p1

    :cond_0
    new-instance v0, LX/7Pg;

    iget-object v1, p0, LX/7Po;->m:LX/04m;

    invoke-direct {v0, p0, p1, p2, v1}, LX/7Pg;-><init>(LX/7Po;LX/7Oi;Ljava/lang/String;LX/04n;)V

    move-object p1, v0

    goto :goto_0
.end method

.method private static a(LX/7Po;LX/7Oi;Ljava/net/URL;LX/7Pw;)LX/7Oi;
    .locals 4

    .prologue
    .line 1203376
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1203377
    invoke-virtual {p2}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 1203378
    if-eqz v2, :cond_1

    const-string v3, ".mpd"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    .line 1203379
    :goto_0
    if-nez v2, :cond_2

    new-instance v2, LX/7I0;

    iget-object v3, p0, LX/7Po;->o:LX/0ad;

    invoke-direct {v2, v3}, LX/7I0;-><init>(LX/0ad;)V

    iget-boolean v2, v2, LX/7I0;->a:Z

    if-eqz v2, :cond_2

    :goto_1
    move v0, v0

    .line 1203380
    if-nez v0, :cond_0

    .line 1203381
    :goto_2
    return-object p1

    .line 1203382
    :cond_0
    new-instance v0, LX/7I0;

    iget-object v1, p0, LX/7Po;->o:LX/0ad;

    invoke-direct {v0, v1}, LX/7I0;-><init>(LX/0ad;)V

    iget-object v1, p0, LX/7Po;->h:LX/0So;

    iget-object v2, p0, LX/7Po;->p:LX/19Z;

    iget-object v3, p0, LX/7Po;->j:LX/0oz;

    invoke-static {v0, v1, v2, v3}, LX/7PM;->a(LX/7I0;LX/0So;LX/19Z;LX/0oz;)LX/7PJ;

    move-result-object v1

    .line 1203383
    new-instance v0, LX/7PN;

    invoke-direct {v0, p1, p3, v1}, LX/7PN;-><init>(LX/7Oi;LX/7Pw;LX/7PJ;)V

    move-object p1, v0

    goto :goto_2

    :cond_1
    move v2, v1

    .line 1203384
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1203385
    goto :goto_1
.end method

.method public static synthetic a(LX/7Po;Ljava/net/URL;Lcom/facebook/common/callercontext/CallerContext;LX/7Pw;Ljava/lang/String;LX/7Ps;LX/7Pl;LX/37C;LX/7Pm;LX/7Ou;ZZZ)LX/7Oi;
    .locals 1

    .prologue
    .line 1203375
    invoke-static/range {p0 .. p12}, LX/7Po;->a$redex0(LX/7Po;Ljava/net/URL;Lcom/facebook/common/callercontext/CallerContext;LX/7Pw;Ljava/lang/String;LX/7Ps;LX/7Pl;LX/37C;LX/7Pm;LX/7Ou;ZZZ)LX/7Oi;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lorg/apache/http/HttpRequest;)LX/7Pm;
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    const-wide/16 v4, -0x1

    const/4 v1, 0x0

    .line 1203230
    const-string v0, "Range"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 1203231
    if-nez v0, :cond_0

    .line 1203232
    new-instance v0, LX/7Pm;

    invoke-direct/range {v0 .. v5}, LX/7Pm;-><init>(ZJJ)V

    .line 1203233
    :goto_0
    return-object v0

    .line 1203234
    :cond_0
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 1203235
    const-string v6, "bytes="

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1203236
    new-instance v0, LX/7Pm;

    invoke-direct/range {v0 .. v5}, LX/7Pm;-><init>(ZJJ)V

    goto :goto_0

    .line 1203237
    :cond_1
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 1203238
    const/16 v0, 0x2d

    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1203239
    if-gez v0, :cond_2

    .line 1203240
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    .line 1203241
    :cond_2
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v6, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1203242
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1203243
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    .line 1203244
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v4, 0x1

    add-long/2addr v4, v0

    .line 1203245
    cmp-long v0, v2, v4

    if-ltz v0, :cond_3

    .line 1203246
    new-instance v0, LX/7Ph;

    invoke-direct {v0, v6}, LX/7Ph;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1203247
    :catch_0
    new-instance v0, LX/7Ph;

    invoke-direct {v0, v6}, LX/7Ph;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1203248
    :cond_3
    :try_start_1
    new-instance v0, LX/7Pm;

    const/4 v1, 0x1

    invoke-direct/range {v0 .. v5}, LX/7Pm;-><init>(ZJJ)V

    goto :goto_0

    .line 1203249
    :cond_4
    new-instance v0, LX/7Pm;

    const/4 v1, 0x1

    const-wide/16 v4, -0x1

    invoke-direct/range {v0 .. v5}, LX/7Pm;-><init>(ZJJ)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private static a(Z)Lcom/facebook/http/interfaces/RequestPriority;
    .locals 1

    .prologue
    .line 1203374
    if-eqz p0, :cond_0

    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method

.method public static synthetic a(LX/7Po;JJLjava/lang/String;)V
    .locals 1

    .prologue
    .line 1203371
    if-eqz p5, :cond_0

    .line 1203372
    new-instance v0, LX/2qQ;

    invoke-direct {v0, p1, p2, p3, p4}, LX/2qQ;-><init>(JJ)V

    invoke-static {p0, p5, v0}, LX/7Po;->a$redex0(LX/7Po;Ljava/lang/String;LX/1AD;)V

    .line 1203373
    :cond_0
    return-void
.end method

.method private static a$redex0(LX/7Po;Ljava/net/URL;Lcom/facebook/common/callercontext/CallerContext;LX/7Pw;Ljava/lang/String;LX/7Ps;LX/7Pl;LX/37C;LX/7Pm;LX/7Ou;ZZZ)LX/7Oi;
    .locals 15

    .prologue
    .line 1203343
    invoke-virtual/range {p1 .. p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 1203344
    invoke-static/range {p11 .. p11}, LX/7Po;->a(Z)Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v4

    .line 1203345
    invoke-static {}, LX/1m0;->d()Ljava/lang/String;

    invoke-static/range {p11 .. p11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1203346
    iget-object v2, p0, LX/7Po;->u:LX/0wr;

    sget-object v3, LX/0wr;->HTTP:LX/0wr;

    if-ne v2, v3, :cond_1

    .line 1203347
    new-instance v2, Lcom/facebook/video/server/NetworkRangeWriter;

    iget-object v6, p0, LX/7Po;->k:LX/1Lw;

    iget-object v7, p0, LX/7Po;->d:LX/03V;

    iget-object v8, p0, LX/7Po;->r:LX/0WJ;

    iget-object v9, p0, LX/7Po;->s:LX/0lC;

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    invoke-direct/range {v2 .. v9}, Lcom/facebook/video/server/NetworkRangeWriter;-><init>(Ljava/net/URL;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/common/callercontext/CallerContext;LX/1Lw;LX/03V;LX/0WJ;LX/0lC;)V

    .line 1203348
    invoke-virtual/range {p5 .. p5}, LX/7Ps;->a()LX/3DY;

    move-result-object v3

    iget-object v4, p0, LX/7Po;->m:LX/04m;

    move-object/from16 v0, p6

    invoke-static {p0, v2, v3, v0, v4}, LX/7Po;->a(LX/7Po;LX/7Oi;LX/3DY;LX/7Pl;LX/04n;)LX/7Oi;

    move-result-object v2

    .line 1203349
    move-object/from16 v0, p4

    invoke-static {p0, v2, v0}, LX/7Po;->a(LX/7Po;LX/7Oi;Ljava/lang/String;)LX/7Oi;

    move-result-object v2

    .line 1203350
    invoke-static {v2}, LX/7Po;->a(LX/7Oi;)LX/7Oi;

    move-result-object v2

    .line 1203351
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {p0, v2, v0, v1}, LX/7Po;->a(LX/7Po;LX/7Oi;Ljava/net/URL;LX/7Pw;)LX/7Oi;

    move-result-object v2

    .line 1203352
    new-instance v3, LX/7Pb;

    invoke-direct {v3, v2}, LX/7Pb;-><init>(LX/7Oi;)V

    .line 1203353
    :goto_0
    if-nez p10, :cond_0

    .line 1203354
    iget-object v2, p0, LX/7Po;->a:LX/1Ln;

    move-object/from16 v0, p7

    invoke-static {p0, v3, v2, v0}, LX/7Po;->a(LX/7Po;LX/7Oi;LX/1Ln;LX/37C;)LX/7Oi;

    move-result-object v3

    .line 1203355
    :cond_0
    if-eqz p9, :cond_4

    .line 1203356
    invoke-static {}, LX/1m0;->d()Ljava/lang/String;

    .line 1203357
    new-instance v2, LX/7Ov;

    move-object/from16 v0, p9

    invoke-direct {v2, v0, v3}, LX/7Ov;-><init>(LX/7Ou;LX/7Oi;)V

    .line 1203358
    :goto_1
    return-object v2

    .line 1203359
    :cond_1
    new-instance v5, LX/3DV;

    iget-object v7, p0, LX/7Po;->k:LX/1Lw;

    if-eqz p12, :cond_3

    const-string v10, "getLiveVideo"

    :goto_2
    iget-object v11, p0, LX/7Po;->d:LX/03V;

    iget-object v12, p0, LX/7Po;->r:LX/0WJ;

    iget-object v13, p0, LX/7Po;->s:LX/0lC;

    iget-object v14, p0, LX/7Po;->t:LX/1Lu;

    move-object/from16 v8, p2

    move-object v9, v4

    invoke-direct/range {v5 .. v14}, LX/3DV;-><init>(Landroid/net/Uri;LX/1Lw;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;Ljava/lang/String;LX/03V;LX/0WJ;LX/0lC;LX/1Lu;)V

    .line 1203360
    invoke-virtual/range {p5 .. p5}, LX/7Ps;->a()LX/3DY;

    move-result-object v2

    iget-object v3, p0, LX/7Po;->m:LX/04m;

    move-object/from16 v0, p6

    invoke-static {p0, v5, v2, v0, v3}, LX/7Po;->a(LX/7Po;LX/3DW;LX/3DY;LX/7Pl;LX/04n;)LX/3DW;

    move-result-object v2

    .line 1203361
    move-object/from16 v0, p4

    invoke-static {p0, v2, v0}, LX/7Po;->a(LX/7Po;LX/3DW;Ljava/lang/String;)LX/3DW;

    move-result-object v4

    .line 1203362
    new-instance v3, LX/7I0;

    iget-object v2, p0, LX/7Po;->o:LX/0ad;

    invoke-direct {v3, v2}, LX/7I0;-><init>(LX/0ad;)V

    .line 1203363
    iget-object v2, p0, LX/7Po;->h:LX/0So;

    iget-object v5, p0, LX/7Po;->p:LX/19Z;

    iget-object v6, p0, LX/7Po;->j:LX/0oz;

    invoke-static {v3, v2, v5, v6}, LX/7PM;->a(LX/7I0;LX/0So;LX/19Z;LX/0oz;)LX/7PJ;

    move-result-object v6

    .line 1203364
    new-instance v2, LX/7PI;

    iget-object v5, p0, LX/7Po;->g:Ljava/util/concurrent/ScheduledExecutorService;

    iget-boolean v7, v3, LX/7I0;->v:Z

    const v8, 0x32000

    move-object/from16 v3, p3

    invoke-direct/range {v2 .. v8}, LX/7PI;-><init>(LX/7Pw;LX/3DW;Ljava/util/concurrent/ScheduledExecutorService;LX/7PJ;ZI)V

    .line 1203365
    move-object/from16 v0, p8

    iget-wide v6, v0, LX/7Pm;->c:J

    .line 1203366
    iget-object v3, p0, LX/7Po;->n:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0wp;

    iget-wide v4, v3, LX/0wp;->M:J

    .line 1203367
    const-wide/16 v8, 0x0

    cmp-long v3, v4, v8

    if-lez v3, :cond_2

    move-object/from16 v0, p8

    iget-object v3, v0, LX/7Pm;->d:LX/2WF;

    if-eqz v3, :cond_2

    move-object/from16 v0, p8

    iget-object v3, v0, LX/7Pm;->d:LX/2WF;

    invoke-virtual {v3}, LX/2WF;->a()J

    move-result-wide v8

    cmp-long v3, v8, v4

    if-gtz v3, :cond_2

    .line 1203368
    move-object/from16 v0, p8

    iget-wide v6, v0, LX/7Pm;->b:J

    add-long/2addr v6, v4

    .line 1203369
    :cond_2
    new-instance v3, LX/7Oj;

    move-object/from16 v0, p8

    iget-wide v4, v0, LX/7Pm;->b:J

    move-object v8, v2

    invoke-direct/range {v3 .. v8}, LX/7Oj;-><init>(JJLX/3DW;)V

    goto/16 :goto_0

    .line 1203370
    :cond_3
    const-string v10, "getVideo-1RT"

    goto :goto_2

    :cond_4
    move-object v2, v3

    goto/16 :goto_1
.end method

.method public static a$redex0(LX/7Po;I)LX/7Pw;
    .locals 4

    .prologue
    .line 1203342
    new-instance v0, LX/7Pw;

    iget-object v1, p0, LX/7Po;->h:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-direct {v0, p1, v2, v3}, LX/7Pw;-><init>(IJ)V

    return-object v0
.end method

.method public static a$redex0(LX/7Po;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1203339
    if-eqz p1, :cond_0

    .line 1203340
    new-instance v0, LX/2qS;

    invoke-direct {v0}, LX/2qS;-><init>()V

    invoke-static {p0, p1, v0}, LX/7Po;->a$redex0(LX/7Po;Ljava/lang/String;LX/1AD;)V

    .line 1203341
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/7Po;Ljava/lang/String;LX/1AD;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/1AD",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1203336
    new-instance v0, LX/2qj;

    invoke-direct {v0, p1, p2}, LX/2qj;-><init>(Ljava/lang/String;LX/1AD;)V

    .line 1203337
    iget-object v1, p0, LX/7Po;->b:LX/0Sh;

    new-instance v2, Lcom/facebook/video/server/VideoServer$VideoServerWorker$5;

    invoke-direct {v2, p0, v0}, Lcom/facebook/video/server/VideoServer$VideoServerWorker$5;-><init>(LX/7Po;LX/2qj;)V

    invoke-virtual {v1, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 1203338
    return-void
.end method

.method public static b(LX/3Dd;)V
    .locals 4

    .prologue
    .line 1203332
    iget-wide v0, p0, LX/3Dd;->a:J

    .line 1203333
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 1203334
    new-instance v2, LX/7Pi;

    invoke-direct {v2, v0, v1}, LX/7Pi;-><init>(J)V

    throw v2

    .line 1203335
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)J
    .locals 2

    .prologue
    .line 1203323
    iget-object v0, p0, LX/7Po;->f:LX/1Lt;

    invoke-virtual {v0, p1}, LX/1Lt;->a(Landroid/net/Uri;)LX/37C;

    move-result-object v0

    .line 1203324
    iget-object v1, p0, LX/7Po;->a:LX/1Ln;

    invoke-interface {v1, v0}, LX/1Ln;->b(Ljava/lang/Object;)LX/3Da;

    move-result-object v0

    .line 1203325
    if-nez v0, :cond_0

    .line 1203326
    const-wide/16 v0, 0x0

    .line 1203327
    :goto_0
    return-wide v0

    .line 1203328
    :cond_0
    invoke-interface {v0}, LX/3Da;->g()Ljava/util/List;

    move-result-object v0

    .line 1203329
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1203330
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 1203331
    :cond_2
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2WF;

    iget-wide v0, v0, LX/2WF;->a:J

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;JJLjava/lang/String;ILX/3Di;)V
    .locals 10

    .prologue
    .line 1203305
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gtz v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, p4, v2

    if-lez v2, :cond_1

    :cond_0
    const/4 v3, 0x1

    .line 1203306
    :goto_0
    new-instance v2, LX/7Pm;

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v2 .. v7}, LX/7Pm;-><init>(ZJJ)V

    .line 1203307
    new-instance v3, LX/7Pj;

    move-object v4, p0

    move-object v5, p1

    move/from16 v6, p7

    move-object/from16 v7, p6

    move-object v8, v2

    invoke-direct/range {v3 .. v8}, LX/7Pj;-><init>(LX/7Po;Landroid/net/Uri;ILjava/lang/String;LX/7Pm;)V

    .line 1203308
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v3, v2, v4, v5}, LX/7Pj;->a(ZZZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1203309
    invoke-virtual {v3}, LX/7Pj;->b()LX/3Dd;

    move-result-object v2

    move-object/from16 v0, p8

    invoke-interface {v0, v2}, LX/3Di;->a(LX/3Dd;)Ljava/io/OutputStream;

    move-result-object v4

    .line 1203310
    if-nez v4, :cond_2

    .line 1203311
    invoke-virtual {v3}, LX/7Pj;->c()V

    .line 1203312
    :goto_1
    return-void

    .line 1203313
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 1203314
    :catch_0
    move-exception v2

    .line 1203315
    move-object/from16 v0, p8

    invoke-interface {v0, v2}, LX/3Di;->a(Ljava/io/IOException;)V

    goto :goto_1

    .line 1203316
    :cond_2
    :try_start_1
    invoke-virtual {v3, v4}, LX/7Pj;->a(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1203317
    const/4 v2, 0x0

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v2}, LX/3Di;->a(Ljava/io/OutputStream;Ljava/io/IOException;)V

    goto :goto_1

    .line 1203318
    :catch_1
    move-exception v2

    .line 1203319
    move-object/from16 v0, p8

    invoke-interface {v0, v4, v2}, LX/3Di;->a(Ljava/io/OutputStream;Ljava/io/IOException;)V

    goto :goto_1

    .line 1203320
    :catch_2
    move-exception v2

    .line 1203321
    :try_start_2
    new-instance v3, Ljava/io/IOException;

    const-string v5, "Unexpected runtime exception"

    invoke-direct {v3, v5, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1203322
    move-object/from16 v0, p8

    invoke-interface {v0, v4, v3}, LX/3Di;->a(Ljava/io/OutputStream;Ljava/io/IOException;)V

    goto :goto_1

    :catchall_0
    move-exception v2

    const/4 v3, 0x0

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v3}, LX/3Di;->a(Ljava/io/OutputStream;Ljava/io/IOException;)V

    throw v2
.end method

.method public final a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)V
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1203250
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1203251
    invoke-static {v0}, LX/1m0;->g(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 1203252
    invoke-static {v0}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v3

    .line 1203253
    invoke-static {v0}, LX/1m0;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    .line 1203254
    const/4 v1, 0x0

    .line 1203255
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    const-string v5, "127.0.0.1"

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1203256
    :cond_0
    :goto_0
    move v8, v1

    .line 1203257
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1203258
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_8

    const-string v9, "127.0.0.1"

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_8

    .line 1203259
    :cond_1
    :goto_1
    move v9, v1

    .line 1203260
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    .line 1203261
    :try_start_0
    invoke-static {p1}, LX/7Po;->a(Lorg/apache/http/HttpRequest;)LX/7Pm;
    :try_end_0
    .catch LX/7Ph; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 1203262
    iget-boolean v0, v5, LX/7Pm;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1203263
    new-instance v0, LX/7Pj;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/7Pj;-><init>(LX/7Po;Landroid/net/Uri;ILjava/lang/String;LX/7Pm;)V

    .line 1203264
    if-eqz v9, :cond_3

    iget-object v1, p0, LX/7Po;->o:LX/0ad;

    sget-short v2, LX/0ws;->bX:S

    invoke-interface {v1, v2, v7}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v6

    .line 1203265
    :goto_2
    :try_start_1
    invoke-virtual {v0, v8, v1, v9}, LX/7Pj;->a(ZZZ)V
    :try_end_1
    .catch LX/7Ph; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3

    .line 1203266
    :try_start_2
    iget-object v1, v0, LX/7Pj;->i:LX/3Dd;

    move-object v1, v1

    .line 1203267
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Dd;

    .line 1203268
    iget-boolean v2, v5, LX/7Pm;->a:Z

    if-eqz v2, :cond_5

    const/16 v2, 0xce

    .line 1203269
    :goto_3
    new-instance v3, Lorg/apache/http/ProtocolVersion;

    const-string v4, "HTTP"

    const/4 v6, 0x1

    const/4 v8, 0x1

    invoke-direct {v3, v4, v6, v8}, Lorg/apache/http/ProtocolVersion;-><init>(Ljava/lang/String;II)V

    const-string v4, "OK"

    invoke-interface {p2, v3, v2, v4}, Lorg/apache/http/HttpResponse;->setStatusLine(Lorg/apache/http/ProtocolVersion;ILjava/lang/String;)V

    .line 1203270
    const-string v2, "Connection"

    const-string v3, "keep-alive"

    invoke-interface {p2, v2, v3}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1203271
    const-string v2, "Accept-Ranges"

    const-string v3, "bytes"

    invoke-interface {p2, v2, v3}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1203272
    const-string v2, "Content-Type"

    iget-object v3, v1, LX/3Dd;->b:Ljava/lang/String;

    invoke-interface {p2, v2, v3}, Lorg/apache/http/HttpResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1203273
    iget-boolean v2, v5, LX/7Pm;->a:Z

    if-eqz v2, :cond_2

    .line 1203274
    iget-object v2, v0, LX/7Pj;->j:LX/2WF;

    move-object v2, v2

    .line 1203275
    const-string v3, "bytes %d-%d/%d"

    iget-wide v4, v2, LX/2WF;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-wide v8, v2, LX/2WF;->b:J

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v8, v1, LX/3Dd;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v3, v4, v2, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1203276
    const-string v2, "Content-Range"

    invoke-interface {p2, v2, v1}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_4

    .line 1203277
    :cond_2
    new-instance v1, LX/7Pk;

    invoke-direct {v1, v0}, LX/7Pk;-><init>(LX/7Pj;)V

    invoke-interface {p2, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1203278
    :goto_4
    return-void

    .line 1203279
    :catch_0
    move-exception v0

    .line 1203280
    sget-object v1, LX/1m0;->c:Ljava/lang/String;

    const-string v2, "Invalid range specification"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1203281
    const/16 v0, 0x1a0

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto :goto_4

    :cond_3
    move v1, v7

    .line 1203282
    goto :goto_2

    .line 1203283
    :catch_1
    const/16 v0, 0x190

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto :goto_4

    .line 1203284
    :catch_2
    move-exception v0

    .line 1203285
    const/16 v1, 0x1f6

    .line 1203286
    instance-of v2, v0, LX/7P2;

    if-eqz v2, :cond_6

    .line 1203287
    check-cast v0, LX/7P2;

    .line 1203288
    iget v2, v0, LX/7P2;->responseCode:I

    const/16 v3, 0x194

    if-eq v2, v3, :cond_4

    iget v2, v0, LX/7P2;->responseCode:I

    const/16 v3, 0x19a

    if-ne v2, v3, :cond_6

    .line 1203289
    :cond_4
    iget v0, v0, LX/7P2;->responseCode:I

    .line 1203290
    :goto_5
    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto :goto_4

    .line 1203291
    :catch_3
    move-exception v0

    .line 1203292
    new-instance v1, Lorg/apache/http/HttpException;

    const-string v2, "Error handling local request"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/HttpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1203293
    :cond_5
    const/16 v2, 0xc8

    goto/16 :goto_3

    .line 1203294
    :catch_4
    move-exception v1

    .line 1203295
    invoke-static {v0}, LX/7Pj;->d(LX/7Pj;)V

    .line 1203296
    new-instance v0, Lorg/apache/http/HttpException;

    const-string v2, "Error handling local request"

    invoke-direct {v0, v2, v1}, Lorg/apache/http/HttpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_6
    move v0, v1

    goto :goto_5

    .line 1203297
    :cond_7
    const-string v5, "disable-cache"

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1203298
    if-eqz v5, :cond_0

    .line 1203299
    :try_start_3
    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_5

    move-result v1

    goto/16 :goto_0

    .line 1203300
    :catch_5
    goto/16 :goto_0

    .line 1203301
    :cond_8
    const-string v9, "is-live"

    invoke-virtual {v0, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1203302
    if-eqz v9, :cond_1

    .line 1203303
    :try_start_4
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_6

    move-result v9

    if-ne v9, v5, :cond_1

    move v1, v5

    goto/16 :goto_1

    .line 1203304
    :catch_6
    goto/16 :goto_1
.end method
