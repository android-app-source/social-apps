.class public final LX/7hS;
.super LX/7hR;
.source ""


# instance fields
.field public final synthetic a:LX/7hT;


# direct methods
.method public constructor <init>(LX/7hT;)V
    .locals 0

    .prologue
    .line 1226134
    iput-object p1, p0, LX/7hS;->a:LX/7hT;

    invoke-direct {p0}, LX/7hR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1226135
    sget-object v0, LX/7hT;->a:Ljava/util/HashMap;

    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget-object v1, v1, LX/7hT;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1226136
    iget-object v0, p0, LX/7hS;->a:LX/7hT;

    iget v0, v0, LX/7hT;->A:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1226137
    iget-object v0, p0, LX/7hS;->a:LX/7hT;

    iget-object v0, v0, LX/7hT;->c:Landroid/view/View;

    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget v1, v1, LX/7hT;->A:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1226138
    :cond_0
    iget-object v0, p0, LX/7hS;->a:LX/7hT;

    iget-object v0, v0, LX/7hT;->e:LX/7hO;

    if-eqz v0, :cond_1

    .line 1226139
    iget-object v0, p0, LX/7hS;->a:LX/7hT;

    iget-object v0, v0, LX/7hT;->e:LX/7hO;

    invoke-interface {v0}, LX/7hO;->a()V

    .line 1226140
    :cond_1
    return-void
.end method

.method public final a(LX/8YK;)V
    .locals 4

    .prologue
    .line 1226141
    invoke-virtual {p1}, LX/8YK;->b()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1226142
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget-boolean v1, v1, LX/7hT;->f:Z

    if-eqz v1, :cond_0

    .line 1226143
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget-object v1, v1, LX/7hT;->c:Landroid/view/View;

    iget-object v2, p0, LX/7hS;->a:LX/7hT;

    iget v2, v2, LX/7hT;->g:F

    iget-object v3, p0, LX/7hS;->a:LX/7hT;

    iget v3, v3, LX/7hT;->h:F

    invoke-static {v2, v3, v0}, LX/7hT;->b(FFF)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setX(F)V

    .line 1226144
    :cond_0
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget-boolean v1, v1, LX/7hT;->i:Z

    if-eqz v1, :cond_1

    .line 1226145
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget-object v1, v1, LX/7hT;->c:Landroid/view/View;

    iget-object v2, p0, LX/7hS;->a:LX/7hT;

    iget v2, v2, LX/7hT;->j:F

    iget-object v3, p0, LX/7hS;->a:LX/7hT;

    iget v3, v3, LX/7hT;->k:F

    invoke-static {v2, v3, v0}, LX/7hT;->b(FFF)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setY(F)V

    .line 1226146
    :cond_1
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget-boolean v1, v1, LX/7hT;->l:Z

    if-eqz v1, :cond_2

    .line 1226147
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget-object v1, v1, LX/7hT;->c:Landroid/view/View;

    iget-object v2, p0, LX/7hS;->a:LX/7hT;

    iget v2, v2, LX/7hT;->o:F

    invoke-virtual {v1, v2}, Landroid/view/View;->setPivotX(F)V

    .line 1226148
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget-object v1, v1, LX/7hT;->c:Landroid/view/View;

    iget-object v2, p0, LX/7hS;->a:LX/7hT;

    iget v2, v2, LX/7hT;->m:F

    iget-object v3, p0, LX/7hS;->a:LX/7hT;

    iget v3, v3, LX/7hT;->n:F

    invoke-static {v2, v3, v0}, LX/7hT;->b(FFF)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setScaleX(F)V

    .line 1226149
    :cond_2
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget-boolean v1, v1, LX/7hT;->p:Z

    if-eqz v1, :cond_3

    .line 1226150
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget-object v1, v1, LX/7hT;->c:Landroid/view/View;

    iget-object v2, p0, LX/7hS;->a:LX/7hT;

    iget v2, v2, LX/7hT;->s:F

    invoke-virtual {v1, v2}, Landroid/view/View;->setPivotY(F)V

    .line 1226151
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget-object v1, v1, LX/7hT;->c:Landroid/view/View;

    iget-object v2, p0, LX/7hS;->a:LX/7hT;

    iget v2, v2, LX/7hT;->q:F

    iget-object v3, p0, LX/7hS;->a:LX/7hT;

    iget v3, v3, LX/7hT;->r:F

    invoke-static {v2, v3, v0}, LX/7hT;->b(FFF)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setScaleY(F)V

    .line 1226152
    :cond_3
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget-boolean v1, v1, LX/7hT;->t:Z

    if-eqz v1, :cond_4

    .line 1226153
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget v1, v1, LX/7hT;->u:F

    iget-object v2, p0, LX/7hS;->a:LX/7hT;

    iget v2, v2, LX/7hT;->v:F

    invoke-static {v1, v2, v0}, LX/7hT;->b(FFF)F

    move-result v1

    .line 1226154
    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 1226155
    iget-object v2, p0, LX/7hS;->a:LX/7hT;

    iget-object v2, v2, LX/7hT;->c:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1226156
    :cond_4
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget-boolean v1, v1, LX/7hT;->w:Z

    if-eqz v1, :cond_5

    .line 1226157
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget v1, v1, LX/7hT;->x:F

    iget-object v2, p0, LX/7hS;->a:LX/7hT;

    iget v2, v2, LX/7hT;->y:F

    invoke-static {v1, v2, v0}, LX/7hT;->b(FFF)F

    move-result v0

    .line 1226158
    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget-object v1, v1, LX/7hT;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setRotation(F)V

    .line 1226159
    :cond_5
    iget-object v0, p0, LX/7hS;->a:LX/7hT;

    iget-object v0, v0, LX/7hT;->d:LX/7hP;

    if-eqz v0, :cond_6

    .line 1226160
    iget-object v0, p0, LX/7hS;->a:LX/7hT;

    iget-object v0, v0, LX/7hT;->d:LX/7hP;

    invoke-interface {v0}, LX/7hP;->a()V

    .line 1226161
    :cond_6
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1226162
    iget-object v0, p0, LX/7hS;->a:LX/7hT;

    iget v0, v0, LX/7hT;->z:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1226163
    iget-object v0, p0, LX/7hS;->a:LX/7hT;

    iget-object v0, v0, LX/7hT;->c:Landroid/view/View;

    iget-object v1, p0, LX/7hS;->a:LX/7hT;

    iget v1, v1, LX/7hT;->z:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1226164
    :cond_0
    return-void
.end method
