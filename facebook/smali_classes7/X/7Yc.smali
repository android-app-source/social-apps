.class public final LX/7Yc;
.super LX/2vq;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2vq",
        "<",
        "LX/7Z3;",
        "LX/7Yo;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, LX/2vq;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/os/Looper;LX/2wA;LX/7Yo;LX/1qf;LX/1qg;)LX/7Z3;
    .locals 11

    const-string v0, "Setting the API options is required."

    invoke-static {p3, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, LX/7Z3;

    iget-object v5, p3, LX/7Yo;->a:Lcom/google/android/gms/cast/CastDevice;

    iget v0, p3, LX/7Yo;->c:I

    int-to-long v6, v0

    iget-object v8, p3, LX/7Yo;->b:LX/38Y;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v9, p4

    move-object/from16 v10, p5

    invoke-direct/range {v1 .. v10}, LX/7Z3;-><init>(Landroid/content/Context;Landroid/os/Looper;LX/2wA;Lcom/google/android/gms/cast/CastDevice;JLX/38Y;LX/1qf;LX/1qg;)V

    return-object v1
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/content/Context;Landroid/os/Looper;LX/2wA;Ljava/lang/Object;LX/1qf;LX/1qg;)LX/2wJ;
    .locals 6

    move-object v3, p4

    check-cast v3, LX/7Yo;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v4, p5

    move-object v5, p6

    invoke-static/range {v0 .. v5}, LX/7Yc;->a(Landroid/content/Context;Landroid/os/Looper;LX/2wA;LX/7Yo;LX/1qf;LX/1qg;)LX/7Z3;

    move-result-object v0

    return-object v0
.end method
