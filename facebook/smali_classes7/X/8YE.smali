.class public final LX/8YE;
.super LX/8YD;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final b:Landroid/view/Choreographer;

.field public final c:Landroid/view/Choreographer$FrameCallback;

.field public d:Z

.field public e:J


# direct methods
.method public constructor <init>(Landroid/view/Choreographer;)V
    .locals 1

    .prologue
    .line 1355953
    invoke-direct {p0}, LX/8YD;-><init>()V

    .line 1355954
    iput-object p1, p0, LX/8YE;->b:Landroid/view/Choreographer;

    .line 1355955
    new-instance v0, LX/8YC;

    invoke-direct {v0, p0}, LX/8YC;-><init>(LX/8YE;)V

    iput-object v0, p0, LX/8YE;->c:Landroid/view/Choreographer$FrameCallback;

    .line 1355956
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1355957
    iget-boolean v0, p0, LX/8YE;->d:Z

    if-eqz v0, :cond_0

    .line 1355958
    :goto_0
    return-void

    .line 1355959
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8YE;->d:Z

    .line 1355960
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/8YE;->e:J

    .line 1355961
    iget-object v0, p0, LX/8YE;->b:Landroid/view/Choreographer;

    iget-object v1, p0, LX/8YE;->c:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1355962
    iget-object v0, p0, LX/8YE;->b:Landroid/view/Choreographer;

    iget-object v1, p0, LX/8YE;->c:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1355963
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8YE;->d:Z

    .line 1355964
    iget-object v0, p0, LX/8YE;->b:Landroid/view/Choreographer;

    iget-object v1, p0, LX/8YE;->c:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1355965
    return-void
.end method
