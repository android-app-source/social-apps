.class public LX/75D;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Lcom/facebook/widget/images/ImageCacheReader;

.field private b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/images/ImageCacheReader;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1169055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169056
    iput-object p1, p0, LX/75D;->a:Lcom/facebook/widget/images/ImageCacheReader;

    .line 1169057
    iput-object p2, p0, LX/75D;->b:Landroid/content/res/Resources;

    .line 1169058
    return-void
.end method

.method public static b(LX/0QB;)LX/75D;
    .locals 3

    .prologue
    .line 1169059
    new-instance v2, LX/75D;

    invoke-static {p0}, Lcom/facebook/widget/images/ImageCacheReader;->b(LX/0QB;)Lcom/facebook/widget/images/ImageCacheReader;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/images/ImageCacheReader;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-direct {v2, v0, v1}, LX/75D;-><init>(Lcom/facebook/widget/images/ImageCacheReader;Landroid/content/res/Resources;)V

    .line 1169060
    return-object v2
.end method

.method public static b(LX/75D;LX/74w;LX/74z;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1169061
    iget-object v0, p0, LX/75D;->b:Landroid/content/res/Resources;

    .line 1169062
    invoke-virtual {p1, p2}, LX/74w;->a(LX/74z;)LX/4n9;

    move-result-object v1

    .line 1169063
    invoke-static {v1, v0}, LX/4eI;->a(LX/4n9;Landroid/content/res/Resources;)LX/1bX;

    move-result-object v1

    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    move-object v0, v1

    .line 1169064
    invoke-virtual {p0, v0}, LX/75D;->a(LX/1bf;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1bf;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1    # LX/1bf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1169065
    iget-object v0, p0, LX/75D;->a:Lcom/facebook/widget/images/ImageCacheReader;

    .line 1169066
    if-nez p1, :cond_0

    .line 1169067
    const/4 p0, 0x0

    .line 1169068
    :goto_0
    move-object v0, p0

    .line 1169069
    return-object v0

    :cond_0
    invoke-static {v0, p1}, Lcom/facebook/widget/images/ImageCacheReader;->b(Lcom/facebook/widget/images/ImageCacheReader;LX/1bf;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    goto :goto_0
.end method
