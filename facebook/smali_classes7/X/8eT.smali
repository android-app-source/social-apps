.class public final LX/8eT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1382844
    const/4 v8, 0x0

    .line 1382845
    const/4 v5, 0x0

    .line 1382846
    const-wide/16 v6, 0x0

    .line 1382847
    const/4 v4, 0x0

    .line 1382848
    const/4 v3, 0x0

    .line 1382849
    const/4 v2, 0x0

    .line 1382850
    const/4 v1, 0x0

    .line 1382851
    const/4 v0, 0x0

    .line 1382852
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 1382853
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1382854
    const/4 v0, 0x0

    .line 1382855
    :goto_0
    return v0

    .line 1382856
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v10, :cond_8

    .line 1382857
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 1382858
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1382859
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v1, :cond_0

    .line 1382860
    const-string v10, "attribution"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1382861
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v5, v1

    goto :goto_1

    .line 1382862
    :cond_1
    const-string v10, "context_photo"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1382863
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v1

    move v4, v1

    goto :goto_1

    .line 1382864
    :cond_2
    const-string v10, "creation_time"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1382865
    const/4 v0, 0x1

    .line 1382866
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 1382867
    :cond_3
    const-string v10, "description"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1382868
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v9, v1

    goto :goto_1

    .line 1382869
    :cond_4
    const-string v10, "headline"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1382870
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 1382871
    :cond_5
    const-string v10, "media"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1382872
    invoke-static {p0, p1}, LX/8eS;->a(LX/15w;LX/186;)I

    move-result v1

    move v7, v1

    goto :goto_1

    .line 1382873
    :cond_6
    const-string v10, "publisher"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1382874
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 1382875
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1382876
    :cond_8
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1382877
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1382878
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1382879
    if-eqz v0, :cond_9

    .line 1382880
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1382881
    :cond_9
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1382882
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1382883
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1382884
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1382885
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v9, v4

    move v4, v5

    move v5, v8

    move v8, v3

    move-wide v12, v6

    move v7, v2

    move v6, v1

    move-wide v2, v12

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1382886
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1382887
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1382888
    if-eqz v0, :cond_0

    .line 1382889
    const-string v1, "attribution"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382890
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1382891
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1382892
    if-eqz v0, :cond_1

    .line 1382893
    const-string v1, "context_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382894
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1382895
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1382896
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 1382897
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382898
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1382899
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1382900
    if-eqz v0, :cond_3

    .line 1382901
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382902
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1382903
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1382904
    if-eqz v0, :cond_4

    .line 1382905
    const-string v1, "headline"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382906
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1382907
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1382908
    if-eqz v0, :cond_5

    .line 1382909
    const-string v1, "media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382910
    invoke-static {p0, v0, p2, p3}, LX/8eS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1382911
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1382912
    if-eqz v0, :cond_6

    .line 1382913
    const-string v1, "publisher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1382914
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1382915
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1382916
    return-void
.end method
