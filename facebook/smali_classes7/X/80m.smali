.class public LX/80m;
.super LX/0rn;
.source ""


# instance fields
.field private final d:LX/0w9;

.field private final e:LX/0rq;


# direct methods
.method public constructor <init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0w9;LX/0rq;LX/0So;LX/0ad;LX/0sZ;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1284302
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-direct/range {v0 .. v7}, LX/0rn;-><init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0ad;LX/0sZ;)V

    .line 1284303
    iput-object p5, p0, LX/80m;->d:LX/0w9;

    .line 1284304
    iput-object p6, p0, LX/80m;->e:LX/0rq;

    .line 1284305
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1284301
    const/4 v0, 0x2

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1284300
    const-string v0, "fetch_hashtag_feed"

    return-object v0
.end method

.method public final c(Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1284285
    const-string v0, "HashTagFeedNetworkTime"

    return-object v0
.end method

.method public final d(Lcom/facebook/api/feed/FetchFeedParams;)I
    .locals 1

    .prologue
    .line 1284299
    const v0, 0xa0083

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 1284286
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    .line 1284287
    new-instance v0, LX/80k;

    invoke-direct {v0}, LX/80k;-><init>()V

    move-object v0, v0

    .line 1284288
    invoke-static {v0}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 1284289
    const-string v1, "before_home_story_param"

    const-string v2, "after_home_story_param"

    invoke-static {v0, p1, v1, v2}, LX/0w9;->a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1284290
    iget-object v1, p0, LX/80m;->d:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 1284291
    iget-object v1, p0, LX/80m;->d:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->c(LX/0gW;)LX/0gW;

    .line 1284292
    invoke-static {v0}, LX/0w9;->d(LX/0gW;)LX/0gW;

    .line 1284293
    const-string v1, "hashtag"

    .line 1284294
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 1284295
    invoke-virtual {v2}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first_home_story_param"

    .line 1284296
    iget v3, p1, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v3, v3

    .line 1284297
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "media_type"

    iget-object v3, p0, LX/80m;->e:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->c()LX/0wF;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1284298
    return-object v0
.end method
