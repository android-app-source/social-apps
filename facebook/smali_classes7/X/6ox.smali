.class public final LX/6ox;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QR",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/6p6;


# direct methods
.method public constructor <init>(LX/6p6;JLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1148724
    iput-object p1, p0, LX/6ox;->d:LX/6p6;

    iput-wide p2, p0, LX/6ox;->a:J

    iput-object p4, p0, LX/6ox;->b:Ljava/lang/String;

    iput-object p5, p0, LX/6ox;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 14

    .prologue
    .line 1148725
    iget-object v0, p0, LX/6ox;->d:LX/6p6;

    iget-object v0, v0, LX/6p6;->i:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    iget-wide v2, p0, LX/6ox;->a:J

    iget-object v1, p0, LX/6ox;->b:Ljava/lang/String;

    iget-object v4, p0, LX/6ox;->c:Ljava/lang/String;

    .line 1148726
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1148727
    sget-object v13, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->a:Ljava/lang/String;

    new-instance v6, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;

    sget-object v11, LX/03R;->UNSET:LX/03R;

    const/4 v12, 0x0

    move-wide v7, v2

    move-object v9, v1

    move-object v10, v4

    invoke-direct/range {v6 .. v12}, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;-><init>(JLjava/lang/String;Ljava/lang/String;LX/03R;Ljava/util/Map;)V

    invoke-virtual {v5, v13, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1148728
    const-string v6, "update_payment_pin_status"

    invoke-static {v0, v5, v6}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v0, v5

    .line 1148729
    return-object v0
.end method
