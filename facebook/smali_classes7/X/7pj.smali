.class public final LX/7pj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1248799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v11, 0x0

    const/4 v2, 0x0

    .line 1248800
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1248801
    iget-object v1, p0, LX/7pj;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1248802
    iget-object v3, p0, LX/7pj;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1248803
    iget-object v5, p0, LX/7pj;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1248804
    iget-object v6, p0, LX/7pj;->d:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1248805
    iget-object v7, p0, LX/7pj;->e:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1248806
    iget-object v8, p0, LX/7pj;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1248807
    iget-object v9, p0, LX/7pj;->g:Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1248808
    const/4 v10, 0x7

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1248809
    invoke-virtual {v0, v11, v1}, LX/186;->b(II)V

    .line 1248810
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1248811
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1248812
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1248813
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1248814
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1248815
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1248816
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1248817
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1248818
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1248819
    invoke-virtual {v1, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1248820
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1248821
    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    invoke-direct {v1, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;-><init>(LX/15i;)V

    .line 1248822
    return-object v1
.end method
