.class public LX/7E3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1184195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/spherical/photo/model/SphericalPhotoParams;
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7Dw;",
            ">;ZI",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/spherical/photo/model/SphericalPhotoParams;"
        }
    .end annotation

    .prologue
    .line 1184196
    invoke-static {p0, p2, p3, p4, p5}, LX/7E3;->a(Ljava/util/List;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 1184197
    if-eqz p1, :cond_0

    sget-object v1, LX/19o;->TILED_CUBEMAP:LX/19o;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1184198
    sget-object v1, LX/19o;->TILED_CUBEMAP:LX/19o;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1184199
    :goto_0
    return-object v0

    .line 1184200
    :cond_0
    sget-object v1, LX/19o;->CUBESTRIP:LX/19o;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1184201
    sget-object v1, LX/19o;->CUBESTRIP:LX/19o;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    goto :goto_0

    .line 1184202
    :cond_1
    new-instance v0, LX/7Dp;

    invoke-direct {v0}, LX/7Dp;-><init>()V

    invoke-virtual {v0}, LX/7Dp;->a()Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 16
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7Dw;",
            ">;I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "LX/19o;",
            "Lcom/facebook/spherical/photo/model/SphericalPhotoParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1184203
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 1184204
    invoke-static/range {p0 .. p1}, LX/7Dt;->a(Ljava/util/List;I)Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

    move-result-object v8

    .line 1184205
    const/4 v4, 0x0

    .line 1184206
    if-eqz p4, :cond_4

    .line 1184207
    new-instance v4, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    invoke-virtual {v8}, Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;->c()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-direct {v4, v5, v0, v1, v2}, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v4

    .line 1184208
    :goto_0
    const/16 v4, 0x7dc

    move/from16 v0, p1

    if-gt v0, v4, :cond_1

    .line 1184209
    const/4 v4, 0x0

    move v5, v4

    .line 1184210
    :goto_1
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/7Dw;

    .line 1184211
    invoke-interface {v4}, LX/7Dw;->b()LX/19o;

    move-result-object v10

    .line 1184212
    invoke-virtual {v7, v10}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    invoke-static {v10}, LX/7E3;->a(LX/19o;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1184213
    invoke-interface {v4}, LX/7Dw;->e()LX/7Dy;

    move-result-object v11

    .line 1184214
    invoke-static {v4}, LX/7Dv;->a(LX/7Dw;)Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    move-result-object v12

    .line 1184215
    new-instance v13, LX/7Dp;

    invoke-direct {v13}, LX/7Dp;-><init>()V

    invoke-interface {v11}, LX/7Dy;->a()I

    move-result v14

    invoke-virtual {v13, v14}, LX/7Dp;->a(I)LX/7Dp;

    move-result-object v13

    invoke-interface {v11}, LX/7Dy;->c()I

    move-result v14

    invoke-virtual {v13, v14}, LX/7Dp;->b(I)LX/7Dp;

    move-result-object v13

    invoke-interface {v11}, LX/7Dy;->b()I

    move-result v14

    invoke-virtual {v13, v14}, LX/7Dp;->c(I)LX/7Dp;

    move-result-object v13

    invoke-interface {v11}, LX/7Dy;->d()I

    move-result v14

    invoke-virtual {v13, v14}, LX/7Dp;->d(I)LX/7Dp;

    move-result-object v13

    invoke-virtual {v13, v5}, LX/7Dp;->e(I)LX/7Dp;

    move-result-object v13

    invoke-interface {v11}, LX/7Dy;->g()D

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, LX/7Dp;->a(D)LX/7Dp;

    move-result-object v13

    invoke-interface {v11}, LX/7Dy;->h()D

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, LX/7Dp;->b(D)LX/7Dp;

    move-result-object v13

    invoke-interface {v11}, LX/7Dy;->i()D

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, LX/7Dp;->c(D)LX/7Dp;

    move-result-object v13

    invoke-interface {v11}, LX/7Dy;->j()D

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, LX/7Dp;->d(D)LX/7Dp;

    move-result-object v11

    invoke-interface {v4}, LX/7Dw;->a()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, LX/7Dp;->a(Ljava/lang/String;)LX/7Dp;

    move-result-object v11

    invoke-virtual {v11, v10}, LX/7Dp;->a(LX/19o;)LX/7Dp;

    move-result-object v10

    invoke-interface {v4}, LX/7Dw;->f()Ljava/util/List;

    move-result-object v11

    invoke-virtual {v10, v11}, LX/7Dp;->a(Ljava/util/List;)LX/7Dp;

    move-result-object v10

    invoke-virtual {v10, v8}, LX/7Dp;->a(Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;)LX/7Dp;

    move-result-object v10

    invoke-virtual {v10, v12}, LX/7Dp;->a(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;)LX/7Dp;

    move-result-object v10

    invoke-virtual {v10, v6}, LX/7Dp;->a(Lcom/facebook/spherical/photo/model/PhotoVRCastParams;)LX/7Dp;

    move-result-object v10

    invoke-virtual {v10}, LX/7Dp;->a()Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    move-result-object v10

    .line 1184216
    invoke-interface {v4}, LX/7Dw;->b()LX/19o;

    move-result-object v4

    invoke-virtual {v7, v4, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 1184217
    :cond_1
    const/16 v4, 0x7dd

    move/from16 v0, p1

    if-ne v0, v4, :cond_2

    .line 1184218
    const/4 v4, 0x1

    move v5, v4

    goto/16 :goto_1

    .line 1184219
    :cond_2
    const/4 v4, 0x2

    move v5, v4

    goto/16 :goto_1

    .line 1184220
    :cond_3
    return-object v7

    :cond_4
    move-object v6, v4

    goto/16 :goto_0
.end method

.method private static a(LX/19o;)Z
    .locals 1

    .prologue
    .line 1184221
    sget-object v0, LX/19o;->CUBESTRIP:LX/19o;

    if-eq v0, p0, :cond_0

    sget-object v0, LX/19o;->TILED_CUBEMAP:LX/19o;

    if-ne v0, p0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
