.class public final enum LX/71X;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6vZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/71X;",
        ">;",
        "LX/6vZ;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/71X;

.field public static final enum PAYMENTS_PICKER_OPTION:LX/71X;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1163400
    new-instance v0, LX/71X;

    const-string v1, "PAYMENTS_PICKER_OPTION"

    invoke-direct {v0, v1, v2}, LX/71X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71X;->PAYMENTS_PICKER_OPTION:LX/71X;

    .line 1163401
    const/4 v0, 0x1

    new-array v0, v0, [LX/71X;

    sget-object v1, LX/71X;->PAYMENTS_PICKER_OPTION:LX/71X;

    aput-object v1, v0, v2

    sput-object v0, LX/71X;->$VALUES:[LX/71X;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1163402
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/71X;
    .locals 1

    .prologue
    .line 1163403
    const-class v0, LX/71X;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/71X;

    return-object v0
.end method

.method public static values()[LX/71X;
    .locals 1

    .prologue
    .line 1163404
    sget-object v0, LX/71X;->$VALUES:[LX/71X;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/71X;

    return-object v0
.end method
