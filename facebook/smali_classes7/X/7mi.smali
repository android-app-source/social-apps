.class public LX/7mi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLStory;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1237364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1237365
    iput-object p1, p0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1237366
    return-void
.end method


# virtual methods
.method public a()J
    .locals 4

    .prologue
    .line 1237367
    iget-object v0, p0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v2

    .line 1237368
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Please use a valid UNIX timestamp"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1237369
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0

    .line 1237370
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1237371
    iget-object v0, p0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v0

    .line 1237372
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1237373
    iget-object v0, p0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v0

    .line 1237374
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1237375
    :cond_0
    iget-object v0, p0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v0

    .line 1237376
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()I
    .locals 6

    .prologue
    .line 1237377
    iget-object v0, p0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x0

    .line 1237378
    invoke-static {v0}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/17E;->m(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1237379
    :cond_0
    move v0, v2

    .line 1237380
    return v0

    .line 1237381
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v4

    .line 1237382
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1237383
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v1, p0}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1237384
    add-int/lit8 v1, v2, 0x1

    .line 1237385
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final g()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1237386
    iget-object v0, p0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1237387
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 1237388
    :goto_0
    return-object v0

    .line 1237389
    :cond_0
    iget-object v0, p0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v0

    .line 1237390
    invoke-static {v0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final j()I
    .locals 6

    .prologue
    .line 1237391
    iget-object v0, p0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x0

    .line 1237392
    invoke-static {v0}, LX/17E;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    .line 1237393
    :goto_0
    move v0, v1

    .line 1237394
    return v0

    .line 1237395
    :cond_0
    invoke-static {v0}, LX/17E;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {v0}, LX/17E;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 1237396
    if-eqz v1, :cond_1

    .line 1237397
    const/4 v1, 0x1

    goto :goto_0

    .line 1237398
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1237399
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_3

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1237400
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v1, p0}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result p0

    if-eqz p0, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 1237401
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    goto :goto_0

    .line 1237402
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_3
    move v1, v2

    .line 1237403
    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 1237404
    invoke-virtual {p0}, LX/7mi;->j()I

    move-result v0

    invoke-virtual {p0}, LX/7mi;->f()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
