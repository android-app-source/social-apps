.class public final LX/6iB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1129029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1129030
    new-instance v0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1129031
    new-array v0, p1, [Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;

    return-object v0
.end method
