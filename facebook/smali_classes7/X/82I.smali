.class public LX/82I;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/82I;


# instance fields
.field private final a:LX/17Q;

.field private final b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final c:LX/0Zb;

.field private final d:LX/1Ay;

.field private final e:LX/0bH;

.field public final f:Lcom/facebook/content/SecureContextHelper;

.field private final g:LX/1Um;

.field private final h:LX/17d;


# direct methods
.method public constructor <init>(LX/17Q;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Zb;LX/1Ay;LX/0bH;Lcom/facebook/content/SecureContextHelper;LX/1Um;LX/17d;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1288015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1288016
    iput-object p1, p0, LX/82I;->a:LX/17Q;

    .line 1288017
    iput-object p2, p0, LX/82I;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1288018
    iput-object p3, p0, LX/82I;->c:LX/0Zb;

    .line 1288019
    iput-object p4, p0, LX/82I;->d:LX/1Ay;

    .line 1288020
    iput-object p5, p0, LX/82I;->e:LX/0bH;

    .line 1288021
    iput-object p6, p0, LX/82I;->f:Lcom/facebook/content/SecureContextHelper;

    .line 1288022
    iput-object p7, p0, LX/82I;->g:LX/1Um;

    .line 1288023
    iput-object p8, p0, LX/82I;->h:LX/17d;

    .line 1288024
    return-void
.end method

.method public static a(LX/0QB;)LX/82I;
    .locals 12

    .prologue
    .line 1288025
    sget-object v0, LX/82I;->i:LX/82I;

    if-nez v0, :cond_1

    .line 1288026
    const-class v1, LX/82I;

    monitor-enter v1

    .line 1288027
    :try_start_0
    sget-object v0, LX/82I;->i:LX/82I;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1288028
    if-eqz v2, :cond_0

    .line 1288029
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1288030
    new-instance v3, LX/82I;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v4

    check-cast v4, LX/17Q;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v5

    check-cast v5, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, LX/1Ay;->a(LX/0QB;)LX/1Ay;

    move-result-object v7

    check-cast v7, LX/1Ay;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v8

    check-cast v8, LX/0bH;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1Um;->b(LX/0QB;)LX/1Um;

    move-result-object v10

    check-cast v10, LX/1Um;

    invoke-static {v0}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v11

    check-cast v11, LX/17d;

    invoke-direct/range {v3 .. v11}, LX/82I;-><init>(LX/17Q;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Zb;LX/1Ay;LX/0bH;Lcom/facebook/content/SecureContextHelper;LX/1Um;LX/17d;)V

    .line 1288031
    move-object v0, v3

    .line 1288032
    sput-object v0, LX/82I;->i:LX/82I;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1288033
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1288034
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1288035
    :cond_1
    sget-object v0, LX/82I;->i:LX/82I;

    return-object v0

    .line 1288036
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1288037
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/82I;Ljava/lang/String;Landroid/view/View;LX/0lF;Ljava/util/Map;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 5
    .param p0    # LX/82I;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            "LX/0lF;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1288038
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1288039
    :cond_0
    :goto_0
    return-void

    .line 1288040
    :cond_1
    const v0, 0x7f0d0081

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 1288041
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    :goto_1
    move v2, v0

    .line 1288042
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1288043
    const v0, 0x7f0d0084

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5P2;

    .line 1288044
    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/32x;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1288045
    const-string v1, "timeline_friend_request_ref"

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1288046
    :cond_2
    const v0, 0x7f0d0085

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/common/ActionSource;

    .line 1288047
    if-eqz v0, :cond_3

    .line 1288048
    const-string v1, "action_ref"

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1288049
    :cond_3
    const v0, 0x7f0d0082

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/162;

    .line 1288050
    const v1, 0x7f0d0087

    invoke-virtual {p2, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 1288051
    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1288052
    const-string v1, "tracking_codes"

    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1288053
    :cond_4
    move-object v3, v3

    .line 1288054
    const v0, 0x7f0d0082

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/162;

    .line 1288055
    const v1, 0x7f0d0067

    invoke-virtual {p2, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0bJ;

    .line 1288056
    if-nez p3, :cond_c

    .line 1288057
    if-eqz v0, :cond_5

    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v2

    if-lez v2, :cond_5

    .line 1288058
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0lF;->a(I)LX/0lF;

    move-result-object p3

    .line 1288059
    :cond_5
    :goto_2
    const v0, 0x7f0d007d

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1288060
    if-eqz v0, :cond_6

    if-eqz p4, :cond_6

    .line 1288061
    const-string v2, "item_index"

    invoke-interface {p4, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288062
    :cond_6
    const v0, 0x7f0d007f

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    .line 1288063
    const v0, 0x7f0d0080

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/47G;

    .line 1288064
    if-eqz v0, :cond_7

    .line 1288065
    iput-object v3, v0, LX/47G;->r:Landroid/os/Bundle;

    .line 1288066
    iput-object p4, v0, LX/47G;->s:Ljava/util/Map;

    .line 1288067
    :cond_7
    invoke-static {p1}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1288068
    invoke-static {}, LX/47I;->e()LX/47H;

    move-result-object v2

    .line 1288069
    iput-object p1, v2, LX/47H;->a:Ljava/lang/String;

    .line 1288070
    move-object v2, v2

    .line 1288071
    iput-object v0, v2, LX/47H;->d:LX/47G;

    .line 1288072
    move-object v0, v2

    .line 1288073
    invoke-virtual {v0, p4}, LX/47H;->a(Ljava/util/Map;)LX/47H;

    move-result-object v0

    .line 1288074
    iput-object v3, v0, LX/47H;->b:Landroid/os/Bundle;

    .line 1288075
    move-object v0, v0

    .line 1288076
    invoke-virtual {v0}, LX/47H;->a()LX/47I;

    move-result-object v0

    .line 1288077
    iget-object v2, p0, LX/82I;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;LX/47I;)Z

    .line 1288078
    :cond_8
    :goto_3
    if-eqz p5, :cond_a

    .line 1288079
    invoke-static {p5}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1288080
    invoke-static {p5, p2}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 1288081
    :cond_9
    iget-object v0, p0, LX/82I;->c:LX/0Zb;

    invoke-interface {v0, p5}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1288082
    :cond_a
    if-eqz v1, :cond_b

    .line 1288083
    iget-object v0, p0, LX/82I;->e:LX/0bH;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1288084
    :cond_b
    if-eqz p3, :cond_0

    .line 1288085
    iget-object v0, p0, LX/82I;->d:LX/1Ay;

    invoke-virtual {v0, p3, p1}, LX/1Ay;->a(LX/0lF;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1288086
    :cond_c
    new-instance v0, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v4}, LX/162;-><init>(LX/0mC;)V

    .line 1288087
    invoke-virtual {v0, p3}, LX/162;->a(LX/0lF;)LX/162;

    .line 1288088
    invoke-static {v2, p3}, LX/17Q;->c(ZLX/0lF;)Ljava/util/Map;

    move-result-object p4

    .line 1288089
    const-string v2, "tn"

    invoke-interface {p4, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1288090
    :cond_d
    if-eqz p5, :cond_e

    .line 1288091
    iget-boolean v0, p5, Lcom/facebook/analytics/logger/HoneyClientEvent;->j:Z

    move v0, v0

    .line 1288092
    if-eqz v0, :cond_e

    .line 1288093
    iget-object v0, p0, LX/82I;->g:LX/1Um;

    invoke-virtual {v0}, LX/1Um;->a()Ljava/lang/String;

    move-result-object v0

    .line 1288094
    const-string v2, "browser_metrics_join_key"

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1288095
    const-string v2, "browser_metrics_join_key"

    invoke-virtual {p5, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1288096
    :cond_e
    iget-object v0, p0, LX/82I;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v2, p1, v3, p4}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1288097
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1288098
    const-string v2, "http://maps.google.com"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1288099
    const-string v2, "com.facebook.intent.extra.SKIP_IN_APP_BROWSER"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1288100
    :cond_f
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1288101
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1288102
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/high16 v4, 0x10000

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 1288103
    if-eqz v3, :cond_12

    .line 1288104
    iget-object v3, p0, LX/82I;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1288105
    :cond_10
    :goto_4
    goto/16 :goto_3

    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1288106
    :cond_12
    iget-object v3, p0, LX/82I;->c:LX/0Zb;

    const-string v4, "external_activity_launch_failure"

    const/4 p4, 0x0

    invoke-interface {v3, v4, p4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 1288107
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1288108
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_13

    .line 1288109
    const-string v4, "url"

    invoke-virtual {v3, v4, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    .line 1288110
    :cond_13
    invoke-virtual {v3}, LX/0oG;->d()V

    goto :goto_4
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/view/View;LX/0lF;)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1288111
    const v0, 0x7f0d007a

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    const v0, 0x7f0d006a

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v5}, LX/82I;->a(LX/82I;Ljava/lang/String;Landroid/view/View;LX/0lF;Ljava/util/Map;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1288112
    return-void
.end method
