.class public LX/6sA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6rs;


# direct methods
.method public constructor <init>(LX/6rs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1152952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152953
    iput-object p1, p0, LX/6sA;->a:LX/6rs;

    .line 1152954
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1152955
    const-string v0, "title"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152956
    const-string v0, "actionable_title"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152957
    const-string v0, "form_definition"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152958
    const-string v0, "disclaimer"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152959
    new-instance v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;

    const-string v0, "title"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "actionable_title"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/6sA;->a:LX/6rs;

    invoke-virtual {v0, p1}, LX/6rs;->q(Ljava/lang/String;)LX/6rr;

    move-result-object v0

    const-string v4, "form_definition"

    invoke-virtual {p2, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-interface {v0, p1, v4}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    const-string v4, "disclaimer"

    invoke-virtual {p2, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;)V

    return-object v1
.end method
