.class public final LX/8Lz;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8M0;


# direct methods
.method public constructor <init>(LX/8M0;)V
    .locals 0

    .prologue
    .line 1334127
    iput-object p1, p0, LX/8Lz;->a:LX/8M0;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 1334128
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 8

    .prologue
    .line 1334116
    const/4 v2, 0x0

    .line 1334117
    iget-object v0, p0, LX/8Lz;->a:LX/8M0;

    iget-object v0, v0, LX/8M0;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ml;

    .line 1334118
    iget-object v1, p0, LX/8Lz;->a:LX/8M0;

    iget-object v1, v1, LX/8M0;->d:LX/7ma;

    invoke-virtual {v1}, LX/7ma;->c()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_4

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7ml;

    .line 1334119
    invoke-virtual {v1}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1334120
    const/4 v1, 0x1

    .line 1334121
    :goto_1
    if-nez v1, :cond_0

    .line 1334122
    iget-object v1, p0, LX/8Lz;->a:LX/8M0;

    iget-object v1, v1, LX/8M0;->s:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1334123
    iget-object v1, p0, LX/8Lz;->a:LX/8M0;

    iget-object v1, v1, LX/8M0;->s:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Lc;

    invoke-virtual {v1, v0}, LX/8Lc;->a(LX/7mi;)V

    .line 1334124
    :cond_1
    iget-object v1, p0, LX/8Lz;->a:LX/8M0;

    iget-object v1, v1, LX/8M0;->o:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1334125
    :cond_2
    return-void

    .line 1334126
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1
.end method
