.class public final LX/8E3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:LX/8E5;

.field public b:Ljava/lang/String;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public d:J

.field public e:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<TT;>;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8E4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1313409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1313410
    sget-object v0, LX/8E5;->CAN_WAIT:LX/8E5;

    iput-object v0, p0, LX/8E3;->a:LX/8E5;

    .line 1313411
    const-string v0, "default_group_id"

    iput-object v0, p0, LX/8E3;->b:Ljava/lang/String;

    .line 1313412
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8E3;->c:Ljava/util/List;

    .line 1313413
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/8E3;->d:J

    .line 1313414
    const/4 v0, 0x0

    iput-object v0, p0, LX/8E3;->e:Ljava/util/concurrent/Callable;

    .line 1313415
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8E3;->f:Z

    .line 1313416
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8E3;->g:Ljava/util/List;

    .line 1313417
    return-void
.end method


# virtual methods
.method public final a(LX/0Rf;)LX/8E3;
    .locals 4
    .param p1    # LX/0Rf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "LX/8E7;",
            ">;)",
            "LX/8E3;"
        }
    .end annotation

    .prologue
    .line 1313418
    iget-object v0, p0, LX/8E3;->c:Ljava/util/List;

    invoke-static {p1}, LX/8E6;->a(LX/0Rf;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1313419
    return-object p0
.end method

.method public final a(Ljava/util/concurrent/Callable;Z)LX/8E3;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;Z)",
            "LX/8E3;"
        }
    .end annotation

    .prologue
    .line 1313420
    iput-object p1, p0, LX/8E3;->e:Ljava/util/concurrent/Callable;

    .line 1313421
    iput-boolean p2, p0, LX/8E3;->f:Z

    .line 1313422
    return-object p0
.end method

.method public final a()LX/8E6;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/8E6",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1313423
    new-instance v0, LX/8E6;

    invoke-direct {v0, p0}, LX/8E6;-><init>(LX/8E3;)V

    return-object v0
.end method

.method public final b(LX/0Rf;)LX/8E3;
    .locals 2
    .param p1    # LX/0Rf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "LX/8E7;",
            ">;)",
            "LX/8E3;"
        }
    .end annotation

    .prologue
    .line 1313424
    invoke-static {p1}, LX/8E6;->a(LX/0Rf;)J

    move-result-wide v0

    iput-wide v0, p0, LX/8E3;->d:J

    .line 1313425
    return-object p0
.end method
