.class public LX/6mc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final batchId:Ljava/lang/Long;

.field public final fbTraceMeta:Ljava/lang/String;

.field public final requests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6mb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1145739
    new-instance v0, LX/1sv;

    const-string v1, "SendMessageRequestBatch"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mc;->b:LX/1sv;

    .line 1145740
    new-instance v0, LX/1sw;

    const-string v1, "batchId"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mc;->c:LX/1sw;

    .line 1145741
    new-instance v0, LX/1sw;

    const-string v1, "fbTraceMeta"

    const/16 v2, 0xb

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mc;->d:LX/1sw;

    .line 1145742
    new-instance v0, LX/1sw;

    const-string v1, "requests"

    const/16 v2, 0xf

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mc;->e:LX/1sw;

    .line 1145743
    sput-boolean v4, LX/6mc;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/6mb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1145744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1145745
    iput-object p1, p0, LX/6mc;->batchId:Ljava/lang/Long;

    .line 1145746
    iput-object p2, p0, LX/6mc;->fbTraceMeta:Ljava/lang/String;

    .line 1145747
    iput-object p3, p0, LX/6mc;->requests:Ljava/util/List;

    .line 1145748
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1145749
    if-eqz p2, :cond_2

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1145750
    :goto_0
    if-eqz p2, :cond_3

    const-string v0, "\n"

    move-object v1, v0

    .line 1145751
    :goto_1
    if-eqz p2, :cond_4

    const-string v0, " "

    .line 1145752
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SendMessageRequestBatch"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1145753
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145754
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145755
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145756
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145757
    const-string v4, "batchId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145758
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145759
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145760
    iget-object v4, p0, LX/6mc;->batchId:Ljava/lang/Long;

    if-nez v4, :cond_5

    .line 1145761
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145762
    :goto_3
    iget-object v4, p0, LX/6mc;->fbTraceMeta:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1145763
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145764
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145765
    const-string v4, "fbTraceMeta"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145766
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145767
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145768
    iget-object v4, p0, LX/6mc;->fbTraceMeta:Ljava/lang/String;

    if-nez v4, :cond_6

    .line 1145769
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145770
    :cond_0
    :goto_4
    iget-object v4, p0, LX/6mc;->requests:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 1145771
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145772
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145773
    const-string v4, "requests"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145774
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145775
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145776
    iget-object v0, p0, LX/6mc;->requests:Ljava/util/List;

    if-nez v0, :cond_7

    .line 1145777
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145778
    :cond_1
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145779
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145780
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1145781
    :cond_2
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1145782
    :cond_3
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1145783
    :cond_4
    const-string v0, ""

    goto/16 :goto_2

    .line 1145784
    :cond_5
    iget-object v4, p0, LX/6mc;->batchId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1145785
    :cond_6
    iget-object v4, p0, LX/6mc;->fbTraceMeta:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1145786
    :cond_7
    iget-object v0, p0, LX/6mc;->requests:Ljava/util/List;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1145787
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1145788
    iget-object v0, p0, LX/6mc;->batchId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1145789
    sget-object v0, LX/6mc;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145790
    iget-object v0, p0, LX/6mc;->batchId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1145791
    :cond_0
    iget-object v0, p0, LX/6mc;->fbTraceMeta:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1145792
    iget-object v0, p0, LX/6mc;->fbTraceMeta:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1145793
    sget-object v0, LX/6mc;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145794
    iget-object v0, p0, LX/6mc;->fbTraceMeta:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145795
    :cond_1
    iget-object v0, p0, LX/6mc;->requests:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 1145796
    iget-object v0, p0, LX/6mc;->requests:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 1145797
    sget-object v0, LX/6mc;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145798
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/6mc;->requests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1145799
    iget-object v0, p0, LX/6mc;->requests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6mb;

    .line 1145800
    invoke-virtual {v0, p1}, LX/6mb;->a(LX/1su;)V

    goto :goto_0

    .line 1145801
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1145802
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1145803
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1145804
    if-nez p1, :cond_1

    .line 1145805
    :cond_0
    :goto_0
    return v0

    .line 1145806
    :cond_1
    instance-of v1, p1, LX/6mc;

    if-eqz v1, :cond_0

    .line 1145807
    check-cast p1, LX/6mc;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1145808
    if-nez p1, :cond_3

    .line 1145809
    :cond_2
    :goto_1
    move v0, v2

    .line 1145810
    goto :goto_0

    .line 1145811
    :cond_3
    iget-object v0, p0, LX/6mc;->batchId:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1145812
    :goto_2
    iget-object v3, p1, LX/6mc;->batchId:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1145813
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1145814
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145815
    iget-object v0, p0, LX/6mc;->batchId:Ljava/lang/Long;

    iget-object v3, p1, LX/6mc;->batchId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145816
    :cond_5
    iget-object v0, p0, LX/6mc;->fbTraceMeta:Ljava/lang/String;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1145817
    :goto_4
    iget-object v3, p1, LX/6mc;->fbTraceMeta:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1145818
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1145819
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145820
    iget-object v0, p0, LX/6mc;->fbTraceMeta:Ljava/lang/String;

    iget-object v3, p1, LX/6mc;->fbTraceMeta:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145821
    :cond_7
    iget-object v0, p0, LX/6mc;->requests:Ljava/util/List;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1145822
    :goto_6
    iget-object v3, p1, LX/6mc;->requests:Ljava/util/List;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1145823
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1145824
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145825
    iget-object v0, p0, LX/6mc;->requests:Ljava/util/List;

    iget-object v3, p1, LX/6mc;->requests:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 1145826
    goto :goto_1

    :cond_a
    move v0, v2

    .line 1145827
    goto :goto_2

    :cond_b
    move v3, v2

    .line 1145828
    goto :goto_3

    :cond_c
    move v0, v2

    .line 1145829
    goto :goto_4

    :cond_d
    move v3, v2

    .line 1145830
    goto :goto_5

    :cond_e
    move v0, v2

    .line 1145831
    goto :goto_6

    :cond_f
    move v3, v2

    .line 1145832
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1145833
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1145834
    sget-boolean v0, LX/6mc;->a:Z

    .line 1145835
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mc;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1145836
    return-object v0
.end method
