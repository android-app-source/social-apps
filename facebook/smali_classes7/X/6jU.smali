.class public LX/6jU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;


# instance fields
.field public final disableBroadcasting:Ljava/lang/Boolean;

.field public final hideAppIcon:Ljava/lang/Boolean;

.field public final hideAttribution:Ljava/lang/Boolean;

.field public final hideInstallButton:Ljava/lang/Boolean;

.field public final hideReplyButton:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 1130848
    new-instance v0, LX/1sv;

    const-string v1, "AppAttributionVisibility"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jU;->b:LX/1sv;

    .line 1130849
    new-instance v0, LX/1sw;

    const-string v1, "hideAttribution"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jU;->c:LX/1sw;

    .line 1130850
    new-instance v0, LX/1sw;

    const-string v1, "hideInstallButton"

    invoke-direct {v0, v1, v3, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jU;->d:LX/1sw;

    .line 1130851
    new-instance v0, LX/1sw;

    const-string v1, "hideReplyButton"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jU;->e:LX/1sw;

    .line 1130852
    new-instance v0, LX/1sw;

    const-string v1, "disableBroadcasting"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jU;->f:LX/1sw;

    .line 1130853
    new-instance v0, LX/1sw;

    const-string v1, "hideAppIcon"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jU;->g:LX/1sw;

    .line 1130854
    sput-boolean v4, LX/6jU;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1130841
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130842
    iput-object p1, p0, LX/6jU;->hideAttribution:Ljava/lang/Boolean;

    .line 1130843
    iput-object p2, p0, LX/6jU;->hideInstallButton:Ljava/lang/Boolean;

    .line 1130844
    iput-object p3, p0, LX/6jU;->hideReplyButton:Ljava/lang/Boolean;

    .line 1130845
    iput-object p4, p0, LX/6jU;->disableBroadcasting:Ljava/lang/Boolean;

    .line 1130846
    iput-object p5, p0, LX/6jU;->hideAppIcon:Ljava/lang/Boolean;

    .line 1130847
    return-void
.end method

.method public static b(LX/1su;)LX/6jU;
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x2

    .line 1130818
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v4, v5

    move-object v3, v5

    move-object v2, v5

    move-object v1, v5

    .line 1130819
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1130820
    iget-byte v6, v0, LX/1sw;->b:B

    if-eqz v6, :cond_5

    .line 1130821
    iget-short v6, v0, LX/1sw;->c:S

    packed-switch v6, :pswitch_data_0

    .line 1130822
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1130823
    :pswitch_0
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v7, :cond_0

    .line 1130824
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 1130825
    :cond_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1130826
    :pswitch_1
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v7, :cond_1

    .line 1130827
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 1130828
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1130829
    :pswitch_2
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v7, :cond_2

    .line 1130830
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto :goto_0

    .line 1130831
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1130832
    :pswitch_3
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v7, :cond_3

    .line 1130833
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_0

    .line 1130834
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1130835
    :pswitch_4
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v7, :cond_4

    .line 1130836
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_0

    .line 1130837
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1130838
    :cond_5
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1130839
    new-instance v0, LX/6jU;

    invoke-direct/range {v0 .. v5}, LX/6jU;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 1130840
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1130714
    if-eqz p2, :cond_8

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1130715
    :goto_0
    if-eqz p2, :cond_9

    const-string v0, "\n"

    move-object v3, v0

    .line 1130716
    :goto_1
    if-eqz p2, :cond_a

    const-string v0, " "

    .line 1130717
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "AppAttributionVisibility"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1130718
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130719
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130720
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130721
    const/4 v1, 0x1

    .line 1130722
    iget-object v6, p0, LX/6jU;->hideAttribution:Ljava/lang/Boolean;

    if-eqz v6, :cond_0

    .line 1130723
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130724
    const-string v1, "hideAttribution"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130725
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130726
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130727
    iget-object v1, p0, LX/6jU;->hideAttribution:Ljava/lang/Boolean;

    if-nez v1, :cond_b

    .line 1130728
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1130729
    :cond_0
    iget-object v6, p0, LX/6jU;->hideInstallButton:Ljava/lang/Boolean;

    if-eqz v6, :cond_2

    .line 1130730
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130731
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130732
    const-string v1, "hideInstallButton"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130733
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130734
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130735
    iget-object v1, p0, LX/6jU;->hideInstallButton:Ljava/lang/Boolean;

    if-nez v1, :cond_c

    .line 1130736
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 1130737
    :cond_2
    iget-object v6, p0, LX/6jU;->hideReplyButton:Ljava/lang/Boolean;

    if-eqz v6, :cond_4

    .line 1130738
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130739
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130740
    const-string v1, "hideReplyButton"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130741
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130742
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130743
    iget-object v1, p0, LX/6jU;->hideReplyButton:Ljava/lang/Boolean;

    if-nez v1, :cond_d

    .line 1130744
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v1, v2

    .line 1130745
    :cond_4
    iget-object v6, p0, LX/6jU;->disableBroadcasting:Ljava/lang/Boolean;

    if-eqz v6, :cond_10

    .line 1130746
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130747
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130748
    const-string v1, "disableBroadcasting"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130749
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130750
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130751
    iget-object v1, p0, LX/6jU;->disableBroadcasting:Ljava/lang/Boolean;

    if-nez v1, :cond_e

    .line 1130752
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130753
    :goto_6
    iget-object v1, p0, LX/6jU;->hideAppIcon:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 1130754
    if-nez v2, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130755
    :cond_6
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130756
    const-string v1, "hideAppIcon"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130757
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130758
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130759
    iget-object v0, p0, LX/6jU;->hideAppIcon:Ljava/lang/Boolean;

    if-nez v0, :cond_f

    .line 1130760
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130761
    :cond_7
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130762
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130763
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1130764
    :cond_8
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1130765
    :cond_9
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1130766
    :cond_a
    const-string v0, ""

    goto/16 :goto_2

    .line 1130767
    :cond_b
    iget-object v1, p0, LX/6jU;->hideAttribution:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1130768
    :cond_c
    iget-object v1, p0, LX/6jU;->hideInstallButton:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1130769
    :cond_d
    iget-object v1, p0, LX/6jU;->hideReplyButton:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1130770
    :cond_e
    iget-object v1, p0, LX/6jU;->disableBroadcasting:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1130771
    :cond_f
    iget-object v0, p0, LX/6jU;->hideAppIcon:Ljava/lang/Boolean;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    :cond_10
    move v2, v1

    goto/16 :goto_6
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1130855
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1130856
    iget-object v0, p0, LX/6jU;->hideAttribution:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1130857
    iget-object v0, p0, LX/6jU;->hideAttribution:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1130858
    sget-object v0, LX/6jU;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1130859
    iget-object v0, p0, LX/6jU;->hideAttribution:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1130860
    :cond_0
    iget-object v0, p0, LX/6jU;->hideInstallButton:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1130861
    iget-object v0, p0, LX/6jU;->hideInstallButton:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1130862
    sget-object v0, LX/6jU;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1130863
    iget-object v0, p0, LX/6jU;->hideInstallButton:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1130864
    :cond_1
    iget-object v0, p0, LX/6jU;->hideReplyButton:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1130865
    iget-object v0, p0, LX/6jU;->hideReplyButton:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1130866
    sget-object v0, LX/6jU;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1130867
    iget-object v0, p0, LX/6jU;->hideReplyButton:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1130868
    :cond_2
    iget-object v0, p0, LX/6jU;->disableBroadcasting:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1130869
    iget-object v0, p0, LX/6jU;->disableBroadcasting:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1130870
    sget-object v0, LX/6jU;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1130871
    iget-object v0, p0, LX/6jU;->disableBroadcasting:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1130872
    :cond_3
    iget-object v0, p0, LX/6jU;->hideAppIcon:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1130873
    iget-object v0, p0, LX/6jU;->hideAppIcon:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1130874
    sget-object v0, LX/6jU;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1130875
    iget-object v0, p0, LX/6jU;->hideAppIcon:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1130876
    :cond_4
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1130877
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1130878
    return-void
.end method

.method public final a(LX/6jU;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1130780
    if-nez p1, :cond_1

    .line 1130781
    :cond_0
    :goto_0
    return v2

    .line 1130782
    :cond_1
    iget-object v0, p0, LX/6jU;->hideAttribution:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1130783
    :goto_1
    iget-object v3, p1, LX/6jU;->hideAttribution:Ljava/lang/Boolean;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1130784
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1130785
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1130786
    iget-object v0, p0, LX/6jU;->hideAttribution:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6jU;->hideAttribution:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1130787
    :cond_3
    iget-object v0, p0, LX/6jU;->hideInstallButton:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1130788
    :goto_3
    iget-object v3, p1, LX/6jU;->hideInstallButton:Ljava/lang/Boolean;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1130789
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1130790
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1130791
    iget-object v0, p0, LX/6jU;->hideInstallButton:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6jU;->hideInstallButton:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1130792
    :cond_5
    iget-object v0, p0, LX/6jU;->hideReplyButton:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1130793
    :goto_5
    iget-object v3, p1, LX/6jU;->hideReplyButton:Ljava/lang/Boolean;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1130794
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1130795
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1130796
    iget-object v0, p0, LX/6jU;->hideReplyButton:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6jU;->hideReplyButton:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1130797
    :cond_7
    iget-object v0, p0, LX/6jU;->disableBroadcasting:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1130798
    :goto_7
    iget-object v3, p1, LX/6jU;->disableBroadcasting:Ljava/lang/Boolean;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1130799
    :goto_8
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1130800
    :cond_8
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1130801
    iget-object v0, p0, LX/6jU;->disableBroadcasting:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6jU;->disableBroadcasting:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1130802
    :cond_9
    iget-object v0, p0, LX/6jU;->hideAppIcon:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1130803
    :goto_9
    iget-object v3, p1, LX/6jU;->hideAppIcon:Ljava/lang/Boolean;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1130804
    :goto_a
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1130805
    :cond_a
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1130806
    iget-object v0, p0, LX/6jU;->hideAppIcon:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6jU;->hideAppIcon:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_b
    move v2, v1

    .line 1130807
    goto/16 :goto_0

    :cond_c
    move v0, v2

    .line 1130808
    goto/16 :goto_1

    :cond_d
    move v3, v2

    .line 1130809
    goto/16 :goto_2

    :cond_e
    move v0, v2

    .line 1130810
    goto :goto_3

    :cond_f
    move v3, v2

    .line 1130811
    goto :goto_4

    :cond_10
    move v0, v2

    .line 1130812
    goto :goto_5

    :cond_11
    move v3, v2

    .line 1130813
    goto :goto_6

    :cond_12
    move v0, v2

    .line 1130814
    goto :goto_7

    :cond_13
    move v3, v2

    .line 1130815
    goto :goto_8

    :cond_14
    move v0, v2

    .line 1130816
    goto :goto_9

    :cond_15
    move v3, v2

    .line 1130817
    goto :goto_a
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1130776
    if-nez p1, :cond_1

    .line 1130777
    :cond_0
    :goto_0
    return v0

    .line 1130778
    :cond_1
    instance-of v1, p1, LX/6jU;

    if-eqz v1, :cond_0

    .line 1130779
    check-cast p1, LX/6jU;

    invoke-virtual {p0, p1}, LX/6jU;->a(LX/6jU;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1130775
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1130772
    sget-boolean v0, LX/6jU;->a:Z

    .line 1130773
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jU;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1130774
    return-object v0
.end method
