.class public final LX/8XY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;)V
    .locals 0

    .prologue
    .line 1355140
    iput-object p1, p0, LX/8XY;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x50f32761

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1355141
    iget-object v1, p0, LX/8XY;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

    .line 1355142
    iget-object v3, v1, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->b:LX/1FZ;

    iget-object v4, v1, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->g:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v4}, Lcom/facebook/resources/ui/FbFrameLayout;->getWidth()I

    move-result v4

    iget-object v5, v1, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->g:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v5}, Lcom/facebook/resources/ui/FbFrameLayout;->getHeight()I

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/1FZ;->a(II)LX/1FJ;

    move-result-object v9

    .line 1355143
    invoke-virtual {v9}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    .line 1355144
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1355145
    iget-object v5, v1, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->g:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v5, v4}, Lcom/facebook/resources/ui/FbFrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1355146
    iget-object v4, v1, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->f:LX/8Vg;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, LX/8Vg;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v7

    .line 1355147
    iget-object v3, v1, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->c:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    iget-object v4, v1, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->e:LX/8TS;

    .line 1355148
    iget-object v5, v4, LX/8TS;->e:LX/8TO;

    move-object v4, v5

    .line 1355149
    iget-object v5, v4, LX/8TO;->b:Ljava/lang/String;

    move-object v4, v5

    .line 1355150
    iget-object v5, v1, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->e:LX/8TS;

    .line 1355151
    iget-object v6, v5, LX/8TS;->e:LX/8TO;

    move-object v5, v6

    .line 1355152
    iget-object v6, v5, LX/8TO;->c:Ljava/lang/String;

    move-object v5, v6

    .line 1355153
    iget-object v6, v1, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->e:LX/8TS;

    .line 1355154
    iget v8, v6, LX/8TS;->n:I

    move v6, v8

    .line 1355155
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;)LX/8Tp;

    move-result-object v3

    .line 1355156
    iget-object v4, v1, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->d:LX/8T9;

    invoke-virtual {v4, v3}, LX/8T9;->a(LX/8Tp;)V

    .line 1355157
    invoke-virtual {v9}, LX/1FJ;->close()V

    .line 1355158
    const v1, 0x5658ed24

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
