.class public final LX/6pc;
.super LX/6nn;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6nn",
        "<",
        "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

.field public final synthetic b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/6pe;


# direct methods
.method public constructor <init>(LX/6pe;Lcom/facebook/payments/auth/pin/EnterPinFragment;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1149535
    iput-object p1, p0, LX/6pc;->d:LX/6pe;

    iput-object p2, p0, LX/6pc;->a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    iput-object p3, p0, LX/6pc;->b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iput-object p4, p0, LX/6pc;->c:Ljava/lang/String;

    invoke-direct {p0}, LX/6nn;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1149533
    iget-object v0, p0, LX/6pc;->a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/EnterPinFragment;->d()V

    .line 1149534
    return-void
.end method

.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1149536
    iget-object v0, p0, LX/6pc;->b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v1, p0, LX/6pc;->a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/payments/auth/pin/EnterPinFragment;Z)V

    .line 1149537
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1149530
    iget-object v0, p0, LX/6pc;->b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    const v1, 0x7f081dd4

    invoke-virtual {v0, v1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(I)V

    .line 1149531
    iget-object v0, p0, LX/6pc;->b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v1, p0, LX/6pc;->a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    iget-object v2, p0, LX/6pc;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V

    .line 1149532
    return-void
.end method
