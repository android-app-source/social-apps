.class public final LX/84A;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/84H;

.field public final synthetic b:J

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic d:Z

.field public final synthetic e:LX/3UJ;


# direct methods
.method public constructor <init>(LX/3UJ;LX/84H;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V
    .locals 1

    .prologue
    .line 1290811
    iput-object p1, p0, LX/84A;->e:LX/3UJ;

    iput-object p2, p0, LX/84A;->a:LX/84H;

    iput-wide p3, p0, LX/84A;->b:J

    iput-object p5, p0, LX/84A;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-boolean p6, p0, LX/84A;->d:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1290812
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1290813
    iget-object v1, p0, LX/84A;->e:LX/3UJ;

    iget-object v1, v1, LX/3UJ;->b:LX/2do;

    new-instance v2, LX/2f2;

    iget-wide v4, p0, LX/84A;->b:J

    invoke-direct {v2, v4, v5, v0, v6}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1290814
    iget-boolean v0, p0, LX/84A;->d:Z

    if-eqz v0, :cond_1

    .line 1290815
    iget-object v0, p0, LX/84A;->e:LX/3UJ;

    iget-object v0, v0, LX/3UJ;->c:LX/2hZ;

    iget-object v1, p0, LX/84A;->e:LX/3UJ;

    iget-wide v2, p0, LX/84A;->b:J

    .line 1290816
    new-instance v4, LX/84G;

    invoke-direct {v4, v1, v2, v3}, LX/84G;-><init>(LX/3UJ;J)V

    move-object v1, v4

    .line 1290817
    invoke-virtual {v0, p1, v1}, LX/2hZ;->a(Ljava/lang/Throwable;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1290818
    :goto_0
    iget-object v0, p0, LX/84A;->a:LX/84H;

    if-eqz v0, :cond_0

    .line 1290819
    iget-object v0, p0, LX/84A;->a:LX/84H;

    invoke-interface {v0, v6}, LX/84H;->a(Z)V

    .line 1290820
    :cond_0
    return-void

    .line 1290821
    :cond_1
    iget-object v0, p0, LX/84A;->e:LX/3UJ;

    iget-object v0, v0, LX/3UJ;->c:LX/2hZ;

    invoke-virtual {v0, p1}, LX/2hZ;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1290822
    iget-object v0, p0, LX/84A;->a:LX/84H;

    if-eqz v0, :cond_0

    .line 1290823
    iget-object v0, p0, LX/84A;->a:LX/84H;

    invoke-interface {v0}, LX/84H;->a()V

    .line 1290824
    :cond_0
    iget-object v0, p0, LX/84A;->e:LX/3UJ;

    iget-object v0, v0, LX/3UJ;->b:LX/2do;

    new-instance v1, LX/2f2;

    iget-wide v2, p0, LX/84A;->b:J

    iget-object v4, p0, LX/84A;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1290825
    return-void
.end method
