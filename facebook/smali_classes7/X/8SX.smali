.class public LX/8SX;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/3kW;

.field public b:Lcom/facebook/content/SecureContextHelper;

.field public final c:Landroid/view/View;

.field private final d:Landroid/view/View;

.field public final e:Landroid/view/View;

.field public final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;

.field public final i:Landroid/widget/TextView;

.field private final j:Landroid/widget/ImageView;

.field private final k:Landroid/widget/ImageView;

.field public l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1346719
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1346720
    const v0, 0x7f030466

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1346721
    const-class v0, LX/8SX;

    invoke-static {v0, p0}, LX/8SX;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1346722
    const v0, 0x7f0d0d3f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/8SX;->c:Landroid/view/View;

    .line 1346723
    const v0, 0x7f0d0d39

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/8SX;->d:Landroid/view/View;

    .line 1346724
    const v0, 0x7f0d0d3e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/8SX;->e:Landroid/view/View;

    .line 1346725
    const v0, 0x7f0d0d41

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8SX;->f:Landroid/widget/TextView;

    .line 1346726
    const v0, 0x7f0d0d3b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8SX;->g:Landroid/widget/TextView;

    .line 1346727
    const v0, 0x7f0d0d3c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8SX;->h:Landroid/widget/TextView;

    .line 1346728
    const v0, 0x7f0d0d3d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8SX;->i:Landroid/widget/TextView;

    .line 1346729
    const v0, 0x7f0d0d40

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/8SX;->j:Landroid/widget/ImageView;

    .line 1346730
    const v0, 0x7f0d0d3a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/8SX;->k:Landroid/widget/ImageView;

    .line 1346731
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/8SX;

    invoke-static {p0}, LX/3kW;->a(LX/0QB;)LX/3kW;

    move-result-object v1

    check-cast v1, LX/3kW;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    iput-object v1, p1, LX/8SX;->a:LX/3kW;

    iput-object p0, p1, LX/8SX;->b:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static setBannerExpanded(LX/8SX;Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 1346732
    iget-object v3, p0, LX/8SX;->c:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1346733
    iget-object v0, p0, LX/8SX;->d:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1346734
    return-void

    :cond_0
    move v0, v2

    .line 1346735
    goto :goto_0

    :cond_1
    move v2, v1

    .line 1346736
    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1346737
    iget-object v0, p0, LX/8SX;->a:LX/3kW;

    invoke-virtual {v0, p1}, LX/3kW;->a(Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;)LX/8QZ;

    move-result-object v0

    .line 1346738
    iget-boolean v1, v0, LX/8QZ;->f:Z

    invoke-static {p0, v1}, LX/8SX;->setBannerExpanded(LX/8SX;Z)V

    .line 1346739
    iget-object v1, p0, LX/8SX;->f:Landroid/widget/TextView;

    iget-object v2, v0, LX/8QZ;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1346740
    iget-object v1, p0, LX/8SX;->g:Landroid/widget/TextView;

    iget-object v2, v0, LX/8QZ;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1346741
    iget-object v1, p0, LX/8SX;->h:Landroid/widget/TextView;

    iget-object v2, v0, LX/8QZ;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1346742
    iget-object v1, v0, LX/8QZ;->e:Ljava/lang/String;

    iput-object v1, p0, LX/8SX;->l:Ljava/lang/String;

    .line 1346743
    iget-object v1, p0, LX/8SX;->i:Landroid/widget/TextView;

    iget-object v2, v0, LX/8QZ;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1346744
    iget-object v1, p0, LX/8SX;->j:Landroid/widget/ImageView;

    iget-object v2, v0, LX/8QZ;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1346745
    iget-object v1, p0, LX/8SX;->k:Landroid/widget/ImageView;

    iget-object v2, v0, LX/8QZ;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1346746
    iget-object v1, p0, LX/8SX;->m:Ljava/lang/String;

    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1346747
    iget-object v1, p0, LX/8SX;->a:LX/3kW;

    iget-object v2, v0, LX/8QZ;->a:LX/8Qa;

    iget-boolean v3, v0, LX/8QZ;->f:Z

    .line 1346748
    if-eqz v3, :cond_1

    .line 1346749
    invoke-static {v1}, LX/3kW;->b(LX/3kW;)Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    move-result-object v4

    .line 1346750
    invoke-static {v1}, LX/3kW;->b(LX/3kW;)Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;->a(LX/8Qa;)I

    move-result v5

    move v5, v5

    .line 1346751
    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v2, v5}, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;->a(LX/8Qa;I)V

    .line 1346752
    invoke-static {v1}, LX/3kW;->c(LX/3kW;)V

    .line 1346753
    sget-object v4, LX/5nx;->Seen:LX/5nx;

    invoke-static {v1, v2, v4}, LX/3kW;->a(LX/3kW;LX/8Qa;LX/5nx;)V

    .line 1346754
    :cond_0
    :goto_0
    iput-object p2, p0, LX/8SX;->m:Ljava/lang/String;

    .line 1346755
    iget-object v0, v0, LX/8QZ;->a:LX/8Qa;

    .line 1346756
    iget-object v1, p0, LX/8SX;->c:Landroid/view/View;

    new-instance v2, LX/8SU;

    invoke-direct {v2, p0, v0}, LX/8SU;-><init>(LX/8SX;LX/8Qa;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1346757
    iget-object v1, p0, LX/8SX;->e:Landroid/view/View;

    new-instance v2, LX/8SV;

    invoke-direct {v2, p0, v0}, LX/8SV;-><init>(LX/8SX;LX/8Qa;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1346758
    iget-object v1, p0, LX/8SX;->i:Landroid/widget/TextView;

    new-instance v2, LX/8SW;

    invoke-direct {v2, p0}, LX/8SW;-><init>(LX/8SX;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1346759
    return-void

    .line 1346760
    :cond_1
    iget-object v4, v1, LX/3kW;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2c9;

    const-string v5, "permalink"

    invoke-virtual {v2}, LX/8Qa;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1346761
    iget-object v1, v4, LX/2c9;->a:LX/0Zb;

    const-string v2, "privacy_education_seen_after_collapsed"

    invoke-static {v2, v5, p1}, LX/2c9;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1346762
    goto :goto_0
.end method
