.class public LX/7PS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0So;

.field private final b:LX/7I0;


# direct methods
.method public constructor <init>(LX/0So;LX/7I0;)V
    .locals 0

    .prologue
    .line 1202869
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202870
    iput-object p1, p0, LX/7PS;->a:LX/0So;

    .line 1202871
    iput-object p2, p0, LX/7PS;->b:LX/7I0;

    .line 1202872
    return-void
.end method

.method public static a(J)J
    .locals 6

    .prologue
    const-wide/32 v4, 0x10000

    .line 1202908
    add-long v0, p0, v4

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    div-long/2addr v0, v4

    mul-long/2addr v0, v4

    return-wide v0
.end method

.method private static a(LX/7PS;JIII)J
    .locals 5

    .prologue
    .line 1202903
    iget-object v0, p0, LX/7PS;->b:LX/7I0;

    iget v0, v0, LX/7I0;->g:I

    int-to-long v0, v0

    mul-long/2addr v0, p1

    const-wide/16 v2, 0x64

    div-long/2addr v0, v2

    .line 1202904
    iget-object v2, p0, LX/7PS;->b:LX/7I0;

    iget v2, v2, LX/7I0;->e:I

    int-to-long v2, v2

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1202905
    invoke-static {p5, p3, p4}, LX/7PS;->a(III)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1202906
    div-int/lit8 v0, p4, 0x8

    mul-int/2addr v0, p5

    add-int/2addr v0, p3

    int-to-long v0, v0

    .line 1202907
    :cond_0
    invoke-static {v0, v1}, LX/7PS;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(LX/7PS;LX/7Pw;JJIJ)J
    .locals 12

    .prologue
    .line 1202889
    invoke-virtual {p1}, LX/7Pw;->a()LX/2WF;

    move-result-object v2

    iget-wide v2, v2, LX/2WF;->a:J

    sub-long v4, p2, v2

    .line 1202890
    invoke-virtual {p1}, LX/7Pw;->a()LX/2WF;

    move-result-object v2

    iget-wide v2, v2, LX/2WF;->a:J

    sub-long v6, p7, v2

    .line 1202891
    iget-object v2, p0, LX/7PS;->a:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v8, p1, LX/7Pw;->b:J

    sub-long v8, v2, v8

    .line 1202892
    iget-object v2, p0, LX/7PS;->b:LX/7I0;

    iget v2, v2, LX/7I0;->d:I

    int-to-long v2, v2

    .line 1202893
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-lez v10, :cond_0

    const-wide/16 v10, 0x0

    cmp-long v10, v4, v10

    if-ltz v10, :cond_0

    .line 1202894
    mul-long v2, v8, p4

    const-wide/16 v8, 0x3e8

    div-long/2addr v2, v8

    .line 1202895
    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 1202896
    cmp-long v6, v2, v4

    if-lez v6, :cond_1

    .line 1202897
    sub-long/2addr v2, v4

    .line 1202898
    move/from16 v0, p6

    int-to-long v4, v0

    mul-long v4, v4, p4

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 1202899
    invoke-static {v2, v3}, LX/7PS;->a(J)J

    move-result-wide v2

    .line 1202900
    iget-object v4, p0, LX/7PS;->b:LX/7I0;

    iget v4, v4, LX/7I0;->d:I

    int-to-long v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 1202901
    :cond_0
    :goto_0
    return-wide v2

    .line 1202902
    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method private static a(III)Z
    .locals 1

    .prologue
    .line 1202888
    if-lez p0, :cond_0

    if-lez p1, :cond_0

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7Pw;JIIJJ)J
    .locals 12

    .prologue
    .line 1202873
    invoke-virtual {p1}, LX/7Pw;->a()LX/2WF;

    move-result-object v2

    .line 1202874
    iget-wide v4, v2, LX/2WF;->a:J

    cmp-long v3, v4, p2

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/7PS;->b:LX/7I0;

    iget-boolean v3, v3, LX/7I0;->u:Z

    if-eqz v3, :cond_1

    .line 1202875
    :cond_0
    invoke-virtual {p1}, LX/7Pw;->a()LX/2WF;

    move-result-object v3

    iget-wide v4, v3, LX/2WF;->a:J

    sub-long v10, p2, v4

    .line 1202876
    invoke-virtual {v2}, LX/2WF;->a()J

    move-result-wide v4

    iget-object v2, p0, LX/7PS;->b:LX/7I0;

    iget v8, v2, LX/7I0;->h:I

    move-object v3, p0

    move/from16 v6, p4

    move/from16 v7, p5

    invoke-static/range {v3 .. v8}, LX/7PS;->a(LX/7PS;JIII)J

    move-result-wide v2

    .line 1202877
    cmp-long v4, v10, v2

    if-gez v4, :cond_1

    .line 1202878
    invoke-static {v2, v3}, LX/7PS;->a(J)J

    move-result-wide v2

    .line 1202879
    :goto_0
    return-wide v2

    .line 1202880
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v2, p6, v2

    if-lez v2, :cond_2

    .line 1202881
    move-wide/from16 v0, p6

    long-to-float v2, v0

    iget-object v3, p0, LX/7PS;->b:LX/7I0;

    iget v3, v3, LX/7I0;->j:F

    mul-float/2addr v2, v3

    float-to-long v6, v2

    iget-object v2, p0, LX/7PS;->b:LX/7I0;

    iget v8, v2, LX/7I0;->i:I

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide/from16 v9, p8

    invoke-static/range {v2 .. v10}, LX/7PS;->a(LX/7PS;LX/7Pw;JJIJ)J

    move-result-wide v2

    goto :goto_0

    .line 1202882
    :cond_2
    iget-object v2, p0, LX/7PS;->b:LX/7I0;

    iget v2, v2, LX/7I0;->b:I

    .line 1202883
    const-wide/16 v9, -0x1

    .line 1202884
    iget-object v3, p0, LX/7PS;->b:LX/7I0;

    iget v3, v3, LX/7I0;->h:I

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-static {v3, v0, v1}, LX/7PS;->a(III)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1202885
    const/4 v2, 0x0

    .line 1202886
    div-int/lit8 v3, p5, 0x8

    iget-object v4, p0, LX/7PS;->b:LX/7I0;

    iget v4, v4, LX/7I0;->h:I

    mul-int/2addr v3, v4

    add-int v3, v3, p4

    int-to-long v9, v3

    .line 1202887
    :cond_3
    int-to-long v6, v2

    const/4 v8, 0x0

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-static/range {v2 .. v10}, LX/7PS;->a(LX/7PS;LX/7Pw;JJIJ)J

    move-result-wide v2

    goto :goto_0
.end method
