.class public final LX/6sk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/6sf;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/6sf;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1153695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153696
    iput-object p1, p0, LX/6sk;->a:LX/0QB;

    .line 1153697
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1153698
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/6sk;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1153699
    packed-switch p2, :pswitch_data_0

    .line 1153700
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1153701
    :pswitch_0
    new-instance v0, LX/6sg;

    invoke-direct {v0}, LX/6sg;-><init>()V

    .line 1153702
    move-object v0, v0

    .line 1153703
    move-object v0, v0

    .line 1153704
    :goto_0
    return-object v0

    .line 1153705
    :pswitch_1
    new-instance v0, LX/6sh;

    invoke-direct {v0}, LX/6sh;-><init>()V

    .line 1153706
    move-object v0, v0

    .line 1153707
    move-object v0, v0

    .line 1153708
    goto :goto_0

    .line 1153709
    :pswitch_2
    new-instance v0, LX/J0t;

    invoke-direct {v0}, LX/J0t;-><init>()V

    .line 1153710
    move-object v0, v0

    .line 1153711
    move-object v0, v0

    .line 1153712
    goto :goto_0

    .line 1153713
    :pswitch_3
    new-instance v0, LX/J0v;

    invoke-direct {v0}, LX/J0v;-><init>()V

    .line 1153714
    move-object v0, v0

    .line 1153715
    move-object v0, v0

    .line 1153716
    goto :goto_0

    .line 1153717
    :pswitch_4
    new-instance v0, LX/J10;

    invoke-direct {v0}, LX/J10;-><init>()V

    .line 1153718
    move-object v0, v0

    .line 1153719
    move-object v0, v0

    .line 1153720
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1153721
    const/4 v0, 0x5

    return v0
.end method
