.class public final enum LX/8TX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8TX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8TX;

.field public static final enum EXISTING_MATCH:LX/8TX;

.field public static final enum PLAY_SOLO:LX/8TX;

.field public static final enum REPLAY_CURRENT:LX/8TX;

.field public static final enum START_SCREEN:LX/8TX;

.field public static final enum SUGGESTED_MATCH:LX/8TX;

.field public static final enum TOP_SCORE:LX/8TX;


# instance fields
.field public final effect:LX/8TJ;

.field public final loggingTag:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1348737
    new-instance v0, LX/8TX;

    const-string v1, "START_SCREEN"

    const-string v2, "start_screen"

    sget-object v3, LX/8TJ;->IGNORE:LX/8TJ;

    invoke-direct {v0, v1, v5, v2, v3}, LX/8TX;-><init>(Ljava/lang/String;ILjava/lang/String;LX/8TJ;)V

    sput-object v0, LX/8TX;->START_SCREEN:LX/8TX;

    .line 1348738
    new-instance v0, LX/8TX;

    const-string v1, "REPLAY_CURRENT"

    const-string v2, "replay"

    sget-object v3, LX/8TJ;->IGNORE:LX/8TJ;

    invoke-direct {v0, v1, v6, v2, v3}, LX/8TX;-><init>(Ljava/lang/String;ILjava/lang/String;LX/8TJ;)V

    sput-object v0, LX/8TX;->REPLAY_CURRENT:LX/8TX;

    .line 1348739
    new-instance v0, LX/8TX;

    const-string v1, "TOP_SCORE"

    const-string v2, "top_score_page"

    sget-object v3, LX/8TJ;->UPDATE:LX/8TJ;

    invoke-direct {v0, v1, v7, v2, v3}, LX/8TX;-><init>(Ljava/lang/String;ILjava/lang/String;LX/8TJ;)V

    sput-object v0, LX/8TX;->TOP_SCORE:LX/8TX;

    .line 1348740
    new-instance v0, LX/8TX;

    const-string v1, "SUGGESTED_MATCH"

    const-string v2, "suggested_match_page"

    sget-object v3, LX/8TJ;->UPDATE:LX/8TJ;

    invoke-direct {v0, v1, v8, v2, v3}, LX/8TX;-><init>(Ljava/lang/String;ILjava/lang/String;LX/8TJ;)V

    sput-object v0, LX/8TX;->SUGGESTED_MATCH:LX/8TX;

    .line 1348741
    new-instance v0, LX/8TX;

    const-string v1, "EXISTING_MATCH"

    const-string v2, "existing_match_page"

    sget-object v3, LX/8TJ;->UPDATE:LX/8TJ;

    invoke-direct {v0, v1, v9, v2, v3}, LX/8TX;-><init>(Ljava/lang/String;ILjava/lang/String;LX/8TJ;)V

    sput-object v0, LX/8TX;->EXISTING_MATCH:LX/8TX;

    .line 1348742
    new-instance v0, LX/8TX;

    const-string v1, "PLAY_SOLO"

    const/4 v2, 0x5

    const-string v3, "play_solo"

    sget-object v4, LX/8TJ;->REMOVE:LX/8TJ;

    invoke-direct {v0, v1, v2, v3, v4}, LX/8TX;-><init>(Ljava/lang/String;ILjava/lang/String;LX/8TJ;)V

    sput-object v0, LX/8TX;->PLAY_SOLO:LX/8TX;

    .line 1348743
    const/4 v0, 0x6

    new-array v0, v0, [LX/8TX;

    sget-object v1, LX/8TX;->START_SCREEN:LX/8TX;

    aput-object v1, v0, v5

    sget-object v1, LX/8TX;->REPLAY_CURRENT:LX/8TX;

    aput-object v1, v0, v6

    sget-object v1, LX/8TX;->TOP_SCORE:LX/8TX;

    aput-object v1, v0, v7

    sget-object v1, LX/8TX;->SUGGESTED_MATCH:LX/8TX;

    aput-object v1, v0, v8

    sget-object v1, LX/8TX;->EXISTING_MATCH:LX/8TX;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/8TX;->PLAY_SOLO:LX/8TX;

    aput-object v2, v0, v1

    sput-object v0, LX/8TX;->$VALUES:[LX/8TX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;LX/8TJ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/8TJ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1348745
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1348746
    iput-object p3, p0, LX/8TX;->loggingTag:Ljava/lang/String;

    .line 1348747
    iput-object p4, p0, LX/8TX;->effect:LX/8TJ;

    .line 1348748
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8TX;
    .locals 1

    .prologue
    .line 1348749
    const-class v0, LX/8TX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8TX;

    return-object v0
.end method

.method public static values()[LX/8TX;
    .locals 1

    .prologue
    .line 1348744
    sget-object v0, LX/8TX;->$VALUES:[LX/8TX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8TX;

    return-object v0
.end method
