.class public LX/727;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wI",
        "<",
        "Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;",
        "LX/729;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1163922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163923
    iput-object p1, p0, LX/727;->a:Landroid/content/Context;

    .line 1163924
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;LX/0Px;)LX/0Px;
    .locals 13

    .prologue
    .line 1163925
    check-cast p1, Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;

    .line 1163926
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1163927
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/729;

    .line 1163928
    sget-object v4, LX/726;->a:[I

    invoke-virtual {v0}, LX/729;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1163929
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled section type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1163930
    :pswitch_0
    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;

    .line 1163931
    iget-object v4, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v4, v4

    .line 1163932
    check-cast v4, Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;

    iget-object v4, v4, Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    .line 1163933
    iget-object v4, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v4, v4

    .line 1163934
    check-cast v4, Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;

    iget-object v7, v4, Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;->a:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v5, v4

    :goto_1
    if-ge v5, v8, :cond_0

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1163935
    invoke-static {}, LX/72c;->newBuilder()LX/72b;

    move-result-object v9

    iget-object v10, v0, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->b:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v10}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v10

    iget-object v10, v10, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->e:LX/72f;

    .line 1163936
    iput-object v10, v9, LX/72b;->a:LX/72f;

    .line 1163937
    move-object v9, v9

    .line 1163938
    iget-object v10, p0, LX/727;->a:Landroid/content/Context;

    invoke-static {}, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->newBuilder()LX/72e;

    move-result-object v11

    iget-object v12, v0, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->b:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v12}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v12

    invoke-virtual {v11, v12}, LX/72e;->a(Lcom/facebook/payments/shipping/model/ShippingCommonParams;)LX/72e;

    move-result-object v11

    .line 1163939
    iput v6, v11, LX/72e;->g:I

    .line 1163940
    move-object v11, v11

    .line 1163941
    iput-object v4, v11, LX/72e;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1163942
    move-object v11, v11

    .line 1163943
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v12

    .line 1163944
    iput-object v12, v11, LX/72e;->f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1163945
    move-object v11, v11

    .line 1163946
    invoke-virtual {v0}, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v12

    iget-object v12, v12, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iget-object v12, v12, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1163947
    iput-object v12, v11, LX/72e;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1163948
    move-object v11, v11

    .line 1163949
    invoke-virtual {v0}, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v12

    iget-object v12, v12, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    .line 1163950
    iput-object v12, v11, LX/72e;->i:LX/6xg;

    .line 1163951
    move-object v11, v11

    .line 1163952
    invoke-virtual {v11}, LX/72e;->j()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/facebook/payments/shipping/form/ShippingAddressActivity;->a(Landroid/content/Context;Lcom/facebook/payments/shipping/model/ShippingParams;)Landroid/content/Intent;

    move-result-object v10

    move-object v10, v10

    .line 1163953
    iput-object v10, v9, LX/72b;->b:Landroid/content/Intent;

    .line 1163954
    move-object v9, v9

    .line 1163955
    const/16 v10, 0x66

    .line 1163956
    iput v10, v9, LX/72b;->c:I

    .line 1163957
    move-object v9, v9

    .line 1163958
    iput-object v4, v9, LX/72b;->g:Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1163959
    move-object v9, v9

    .line 1163960
    const-string v10, "%s, %s, %s, %s, %s, %s"

    invoke-interface {v4, v10}, Lcom/facebook/payments/shipping/model/MailingAddress;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1163961
    iput-object v10, v9, LX/72b;->d:Ljava/lang/String;

    .line 1163962
    move-object v9, v9

    .line 1163963
    invoke-interface {v4}, Lcom/facebook/payments/shipping/model/MailingAddress;->g()Ljava/lang/String;

    move-result-object v10

    .line 1163964
    iput-object v10, v9, LX/72b;->e:Ljava/lang/String;

    .line 1163965
    move-object v9, v9

    .line 1163966
    invoke-interface {v4}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object v4

    sget-object v10, LX/729;->SHIPPING_ADDRESSES:LX/729;

    invoke-virtual {p1, v10}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a(LX/6vZ;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 1163967
    iput-boolean v4, v9, LX/72b;->f:Z

    .line 1163968
    move-object v4, v9

    .line 1163969
    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->f()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v9

    iget-object v9, v9, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1163970
    iput-object v9, v4, LX/72b;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1163971
    move-object v4, v4

    .line 1163972
    invoke-virtual {v4}, LX/72b;->a()LX/72c;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1163973
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_1

    .line 1163974
    :cond_0
    new-instance v4, LX/71o;

    invoke-static {}, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->newBuilder()LX/72e;

    move-result-object v5

    iget-object v7, v0, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->b:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v7}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/72e;->a(Lcom/facebook/payments/shipping/model/ShippingCommonParams;)LX/72e;

    move-result-object v5

    .line 1163975
    iput v6, v5, LX/72e;->g:I

    .line 1163976
    move-object v5, v5

    .line 1163977
    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->f()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v6

    iget-object v6, v6, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1163978
    iput-object v6, v5, LX/72e;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1163979
    move-object v5, v5

    .line 1163980
    invoke-virtual {v0}, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    .line 1163981
    iput-object v0, v5, LX/72e;->i:LX/6xg;

    .line 1163982
    move-object v0, v5

    .line 1163983
    invoke-virtual {v0}, LX/72e;->j()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v0

    invoke-direct {v4, v0}, LX/71o;-><init>(Lcom/facebook/payments/shipping/model/ShippingParams;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1163984
    invoke-static {v2}, LX/714;->a(LX/0Pz;)V

    .line 1163985
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 1163986
    :pswitch_1
    new-instance v0, LX/71G;

    iget-object v4, p0, LX/727;->a:Landroid/content/Context;

    const v5, 0x7f081e67

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/71F;->LEARN_MORE:LX/71F;

    invoke-direct {v0, v4, v5}, LX/71G;-><init>(Ljava/lang/String;LX/71F;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1163987
    goto :goto_2

    .line 1163988
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
