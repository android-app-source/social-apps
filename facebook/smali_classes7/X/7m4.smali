.class public LX/7m4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;
.implements LX/1MT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/7m4;


# instance fields
.field private final b:LX/0W3;

.field private final c:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field private final d:LX/0lB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1236201
    const-class v0, LX/7m4;

    sput-object v0, LX/7m4;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0W3;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1236202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236203
    iput-object p1, p0, LX/7m4;->b:LX/0W3;

    .line 1236204
    iput-object p2, p0, LX/7m4;->c:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1236205
    iput-object p3, p0, LX/7m4;->d:LX/0lB;

    .line 1236206
    return-void
.end method

.method public static a(LX/0QB;)LX/7m4;
    .locals 6

    .prologue
    .line 1236207
    sget-object v0, LX/7m4;->e:LX/7m4;

    if-nez v0, :cond_1

    .line 1236208
    const-class v1, LX/7m4;

    monitor-enter v1

    .line 1236209
    :try_start_0
    sget-object v0, LX/7m4;->e:LX/7m4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1236210
    if-eqz v2, :cond_0

    .line 1236211
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1236212
    new-instance p0, LX/7m4;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lB;

    invoke-direct {p0, v3, v4, v5}, LX/7m4;-><init>(LX/0W3;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/0lB;)V

    .line 1236213
    move-object v0, p0

    .line 1236214
    sput-object v0, LX/7m4;->e:LX/7m4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1236215
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1236216
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1236217
    :cond_1
    sget-object v0, LX/7m4;->e:LX/7m4;

    return-object v0

    .line 1236218
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1236219
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/io/File;)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1236220
    new-instance v0, Ljava/io/File;

    const-string v1, "pending_stories.txt"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1236221
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1236222
    :try_start_0
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1236223
    :try_start_1
    iget-object v3, p0, LX/7m4;->c:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v3}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a()LX/0Px;

    move-result-object v3

    .line 1236224
    iget-object v4, p0, LX/7m4;->d:LX/0lB;

    invoke-virtual {v4, v3}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1236225
    const/4 v3, 0x0

    :try_start_2
    invoke-static {v2, v3}, LX/1md;->a(Ljava/io/Closeable;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1236226
    invoke-static {v1, v5}, LX/1md;->a(Ljava/io/Closeable;Z)V

    .line 1236227
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 1236228
    :catchall_0
    move-exception v0

    const/4 v3, 0x0

    :try_start_3
    invoke-static {v2, v3}, LX/1md;->a(Ljava/io/Closeable;Z)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1236229
    :catchall_1
    move-exception v0

    invoke-static {v1, v5}, LX/1md;->a(Ljava/io/Closeable;Z)V

    throw v0
.end method


# virtual methods
.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1236230
    iget-object v1, p0, LX/7m4;->c:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1236231
    :goto_0
    return-object v0

    .line 1236232
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, LX/7m4;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1236233
    const-string v2, "pending_stories.txt"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1236234
    :catch_0
    move-exception v1

    .line 1236235
    sget-object v2, LX/7m4;->a:Ljava/lang/Class;

    const-string v3, "Exception saving pending stories"

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1236236
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1236237
    iget-object v1, p0, LX/7m4;->c:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1236238
    :goto_0
    return-object v0

    .line 1236239
    :cond_0
    invoke-direct {p0, p1}, LX/7m4;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1236240
    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    const-string v3, "pending_stories.txt"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "text/json"

    invoke-direct {v2, v3, v1, v4}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 1236241
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 1236242
    iget-object v0, p0, LX/7m4;->b:LX/0W3;

    sget-wide v2, LX/0X5;->be:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
