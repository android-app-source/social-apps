.class public LX/6l4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final count:Ljava/lang/Integer;

.field public final hasMore:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1142175
    new-instance v0, LX/1sv;

    const-string v1, "TagCount"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6l4;->b:LX/1sv;

    .line 1142176
    new-instance v0, LX/1sw;

    const-string v1, "count"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6l4;->c:LX/1sw;

    .line 1142177
    new-instance v0, LX/1sw;

    const-string v1, "hasMore"

    invoke-direct {v0, v1, v4, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6l4;->d:LX/1sw;

    .line 1142178
    sput-boolean v3, LX/6l4;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1142171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142172
    iput-object p1, p0, LX/6l4;->count:Ljava/lang/Integer;

    .line 1142173
    iput-object p2, p0, LX/6l4;->hasMore:Ljava/lang/Boolean;

    .line 1142174
    return-void
.end method

.method public static a(LX/6l4;)V
    .locals 4

    .prologue
    .line 1142168
    iget-object v0, p0, LX/6l4;->count:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 1142169
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'count\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6l4;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1142170
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1142139
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1142140
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1142141
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1142142
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TagCount"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1142143
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142144
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142145
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142146
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142147
    const-string v4, "count"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142148
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142149
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142150
    iget-object v4, p0, LX/6l4;->count:Ljava/lang/Integer;

    if-nez v4, :cond_4

    .line 1142151
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142152
    :goto_3
    iget-object v4, p0, LX/6l4;->hasMore:Ljava/lang/Boolean;

    if-eqz v4, :cond_0

    .line 1142153
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142154
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142155
    const-string v4, "hasMore"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142156
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142157
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142158
    iget-object v0, p0, LX/6l4;->hasMore:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    .line 1142159
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142160
    :cond_0
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142161
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142162
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1142163
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1142164
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1142165
    :cond_3
    const-string v0, ""

    goto/16 :goto_2

    .line 1142166
    :cond_4
    iget-object v4, p0, LX/6l4;->count:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1142167
    :cond_5
    iget-object v0, p0, LX/6l4;->hasMore:Ljava/lang/Boolean;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1142127
    invoke-static {p0}, LX/6l4;->a(LX/6l4;)V

    .line 1142128
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1142129
    iget-object v0, p0, LX/6l4;->count:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1142130
    sget-object v0, LX/6l4;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142131
    iget-object v0, p0, LX/6l4;->count:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1142132
    :cond_0
    iget-object v0, p0, LX/6l4;->hasMore:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1142133
    iget-object v0, p0, LX/6l4;->hasMore:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1142134
    sget-object v0, LX/6l4;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142135
    iget-object v0, p0, LX/6l4;->hasMore:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1142136
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1142137
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1142138
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1142105
    if-nez p1, :cond_1

    .line 1142106
    :cond_0
    :goto_0
    return v0

    .line 1142107
    :cond_1
    instance-of v1, p1, LX/6l4;

    if-eqz v1, :cond_0

    .line 1142108
    check-cast p1, LX/6l4;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1142109
    if-nez p1, :cond_3

    .line 1142110
    :cond_2
    :goto_1
    move v0, v2

    .line 1142111
    goto :goto_0

    .line 1142112
    :cond_3
    iget-object v0, p0, LX/6l4;->count:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1142113
    :goto_2
    iget-object v3, p1, LX/6l4;->count:Ljava/lang/Integer;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1142114
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1142115
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1142116
    iget-object v0, p0, LX/6l4;->count:Ljava/lang/Integer;

    iget-object v3, p1, LX/6l4;->count:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1142117
    :cond_5
    iget-object v0, p0, LX/6l4;->hasMore:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1142118
    :goto_4
    iget-object v3, p1, LX/6l4;->hasMore:Ljava/lang/Boolean;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1142119
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1142120
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1142121
    iget-object v0, p0, LX/6l4;->hasMore:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6l4;->hasMore:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1142122
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1142123
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1142124
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1142125
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1142126
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1142104
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1142101
    sget-boolean v0, LX/6l4;->a:Z

    .line 1142102
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6l4;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1142103
    return-object v0
.end method
