.class public LX/6l3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;


# instance fields
.field public final deltas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6kW;",
            ">;"
        }
    .end annotation
.end field

.field public final errorCode:Ljava/lang/String;

.field public final failedSend:LX/6kY;

.field public final firstDeltaSeqId:Ljava/lang/Long;

.field public final lastIssuedSeqId:Ljava/lang/Long;

.field public final queueEntityId:Ljava/lang/Long;

.field public final syncToken:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0xc

    const/4 v5, 0x1

    const/16 v4, 0xb

    const/16 v3, 0xa

    .line 1142049
    new-instance v0, LX/1sv;

    const-string v1, "SyncPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6l3;->b:LX/1sv;

    .line 1142050
    new-instance v0, LX/1sw;

    const-string v1, "deltas"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6l3;->c:LX/1sw;

    .line 1142051
    new-instance v0, LX/1sw;

    const-string v1, "firstDeltaSeqId"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6l3;->d:LX/1sw;

    .line 1142052
    new-instance v0, LX/1sw;

    const-string v1, "lastIssuedSeqId"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6l3;->e:LX/1sw;

    .line 1142053
    new-instance v0, LX/1sw;

    const-string v1, "queueEntityId"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6l3;->f:LX/1sw;

    .line 1142054
    new-instance v0, LX/1sw;

    const-string v1, "failedSend"

    invoke-direct {v0, v1, v6, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6l3;->g:LX/1sw;

    .line 1142055
    new-instance v0, LX/1sw;

    const-string v1, "syncToken"

    invoke-direct {v0, v1, v4, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6l3;->h:LX/1sw;

    .line 1142056
    new-instance v0, LX/1sw;

    const-string v1, "errorCode"

    invoke-direct {v0, v1, v4, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6l3;->i:LX/1sw;

    .line 1142057
    sput-boolean v5, LX/6l3;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;LX/6kY;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6kW;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "LX/6kY;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1142058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142059
    iput-object p1, p0, LX/6l3;->deltas:Ljava/util/List;

    .line 1142060
    iput-object p2, p0, LX/6l3;->firstDeltaSeqId:Ljava/lang/Long;

    .line 1142061
    iput-object p3, p0, LX/6l3;->lastIssuedSeqId:Ljava/lang/Long;

    .line 1142062
    iput-object p4, p0, LX/6l3;->queueEntityId:Ljava/lang/Long;

    .line 1142063
    iput-object p5, p0, LX/6l3;->failedSend:LX/6kY;

    .line 1142064
    iput-object p6, p0, LX/6l3;->syncToken:Ljava/lang/String;

    .line 1142065
    iput-object p7, p0, LX/6l3;->errorCode:Ljava/lang/String;

    .line 1142066
    return-void
.end method

.method public static b(LX/1su;)LX/6l3;
    .locals 15

    .prologue
    const/16 v13, 0xb

    const/4 v8, 0x0

    const/16 v12, 0xa

    const/4 v7, 0x0

    .line 1141865
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v6, v7

    move-object v5, v7

    move-object v4, v7

    move-object v3, v7

    move-object v2, v7

    move-object v1, v7

    .line 1141866
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1141867
    iget-byte v9, v0, LX/1sw;->b:B

    if-eqz v9, :cond_a

    .line 1141868
    iget-short v9, v0, LX/1sw;->c:S

    packed-switch v9, :pswitch_data_0

    .line 1141869
    :pswitch_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141870
    :pswitch_1
    iget-byte v9, v0, LX/1sw;->b:B

    const/16 v10, 0xf

    if-ne v9, v10, :cond_3

    .line 1141871
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v9

    .line 1141872
    new-instance v1, Ljava/util/ArrayList;

    iget v0, v9, LX/1u3;->b:I

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v8

    .line 1141873
    :goto_1
    iget v10, v9, LX/1u3;->b:I

    if-gez v10, :cond_2

    invoke-static {}, LX/1su;->t()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1141874
    :goto_2
    new-instance v10, LX/6kW;

    invoke-direct {v10}, LX/6kW;-><init>()V

    .line 1141875
    new-instance v10, LX/6kW;

    invoke-direct {v10}, LX/6kW;-><init>()V

    .line 1141876
    const/4 v11, 0x0

    iput v11, v10, LX/6kW;->setField_:I

    .line 1141877
    const/4 v11, 0x0

    iput-object v11, v10, LX/6kW;->value_:Ljava/lang/Object;

    .line 1141878
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    .line 1141879
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v11

    .line 1141880
    invoke-virtual {v10, p0, v11}, LX/6kW;->a(LX/1su;LX/1sw;)Ljava/lang/Object;

    move-result-object v14

    iput-object v14, v10, LX/6kW;->value_:Ljava/lang/Object;

    .line 1141881
    iget-object v14, v10, LX/6kT;->value_:Ljava/lang/Object;

    if-eqz v14, :cond_1

    .line 1141882
    iget-short v11, v11, LX/1sw;->c:S

    iput v11, v10, LX/6kW;->setField_:I

    .line 1141883
    :cond_1
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    .line 1141884
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1141885
    move-object v10, v10

    .line 1141886
    move-object v10, v10

    .line 1141887
    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1141888
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1141889
    :cond_2
    iget v10, v9, LX/1u3;->b:I

    if-ge v0, v10, :cond_0

    goto :goto_2

    .line 1141890
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141891
    :pswitch_2
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_4

    .line 1141892
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0

    .line 1141893
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141894
    :pswitch_3
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_5

    .line 1141895
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto/16 :goto_0

    .line 1141896
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141897
    :pswitch_4
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_6

    .line 1141898
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto/16 :goto_0

    .line 1141899
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141900
    :pswitch_5
    iget-byte v9, v0, LX/1sw;->b:B

    const/16 v10, 0xc

    if-ne v9, v10, :cond_7

    .line 1141901
    invoke-static {p0}, LX/6kY;->b(LX/1su;)LX/6kY;

    move-result-object v5

    goto/16 :goto_0

    .line 1141902
    :cond_7
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141903
    :pswitch_6
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v13, :cond_8

    .line 1141904
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 1141905
    :cond_8
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141906
    :pswitch_7
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v13, :cond_9

    .line 1141907
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1141908
    :cond_9
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141909
    :cond_a
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1141910
    new-instance v0, LX/6l3;

    invoke-direct/range {v0 .. v7}, LX/6l3;-><init>(Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;LX/6kY;Ljava/lang/String;Ljava/lang/String;)V

    .line 1141911
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1141973
    if-eqz p2, :cond_c

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1141974
    :goto_0
    if-eqz p2, :cond_d

    const-string v0, "\n"

    move-object v3, v0

    .line 1141975
    :goto_1
    if-eqz p2, :cond_e

    const-string v0, " "

    .line 1141976
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "SyncPayload"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1141977
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141978
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141979
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141980
    const/4 v1, 0x1

    .line 1141981
    iget-object v6, p0, LX/6l3;->deltas:Ljava/util/List;

    if-eqz v6, :cond_0

    .line 1141982
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141983
    const-string v1, "deltas"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141984
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141985
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141986
    iget-object v1, p0, LX/6l3;->deltas:Ljava/util/List;

    if-nez v1, :cond_f

    .line 1141987
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1141988
    :cond_0
    iget-object v6, p0, LX/6l3;->firstDeltaSeqId:Ljava/lang/Long;

    if-eqz v6, :cond_2

    .line 1141989
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141990
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141991
    const-string v1, "firstDeltaSeqId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141992
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141993
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141994
    iget-object v1, p0, LX/6l3;->firstDeltaSeqId:Ljava/lang/Long;

    if-nez v1, :cond_10

    .line 1141995
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 1141996
    :cond_2
    iget-object v6, p0, LX/6l3;->lastIssuedSeqId:Ljava/lang/Long;

    if-eqz v6, :cond_4

    .line 1141997
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141998
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141999
    const-string v1, "lastIssuedSeqId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142000
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142001
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142002
    iget-object v1, p0, LX/6l3;->lastIssuedSeqId:Ljava/lang/Long;

    if-nez v1, :cond_11

    .line 1142003
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v1, v2

    .line 1142004
    :cond_4
    iget-object v6, p0, LX/6l3;->queueEntityId:Ljava/lang/Long;

    if-eqz v6, :cond_6

    .line 1142005
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142006
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142007
    const-string v1, "queueEntityId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142008
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142009
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142010
    iget-object v1, p0, LX/6l3;->queueEntityId:Ljava/lang/Long;

    if-nez v1, :cond_12

    .line 1142011
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    move v1, v2

    .line 1142012
    :cond_6
    iget-object v6, p0, LX/6l3;->failedSend:LX/6kY;

    if-eqz v6, :cond_8

    .line 1142013
    if-nez v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142014
    :cond_7
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142015
    const-string v1, "failedSend"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142016
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142017
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142018
    iget-object v1, p0, LX/6l3;->failedSend:LX/6kY;

    if-nez v1, :cond_13

    .line 1142019
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_7
    move v1, v2

    .line 1142020
    :cond_8
    iget-object v6, p0, LX/6l3;->syncToken:Ljava/lang/String;

    if-eqz v6, :cond_16

    .line 1142021
    if-nez v1, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142022
    :cond_9
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142023
    const-string v1, "syncToken"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142024
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142025
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142026
    iget-object v1, p0, LX/6l3;->syncToken:Ljava/lang/String;

    if-nez v1, :cond_14

    .line 1142027
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142028
    :goto_8
    iget-object v1, p0, LX/6l3;->errorCode:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 1142029
    if-nez v2, :cond_a

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142030
    :cond_a
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142031
    const-string v1, "errorCode"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142032
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142033
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142034
    iget-object v0, p0, LX/6l3;->errorCode:Ljava/lang/String;

    if-nez v0, :cond_15

    .line 1142035
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142036
    :cond_b
    :goto_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142037
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142038
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1142039
    :cond_c
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1142040
    :cond_d
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1142041
    :cond_e
    const-string v0, ""

    goto/16 :goto_2

    .line 1142042
    :cond_f
    iget-object v1, p0, LX/6l3;->deltas:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1142043
    :cond_10
    iget-object v1, p0, LX/6l3;->firstDeltaSeqId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1142044
    :cond_11
    iget-object v1, p0, LX/6l3;->lastIssuedSeqId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1142045
    :cond_12
    iget-object v1, p0, LX/6l3;->queueEntityId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1142046
    :cond_13
    iget-object v1, p0, LX/6l3;->failedSend:LX/6kY;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1142047
    :cond_14
    iget-object v1, p0, LX/6l3;->syncToken:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1142048
    :cond_15
    iget-object v0, p0, LX/6l3;->errorCode:Ljava/lang/String;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    :cond_16
    move v2, v1

    goto/16 :goto_8
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1142067
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1142068
    iget-object v0, p0, LX/6l3;->deltas:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1142069
    iget-object v0, p0, LX/6l3;->deltas:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1142070
    sget-object v0, LX/6l3;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142071
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/6l3;->deltas:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1142072
    iget-object v0, p0, LX/6l3;->deltas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kW;

    .line 1142073
    invoke-virtual {v0, p1}, LX/6kT;->a(LX/1su;)V

    goto :goto_0

    .line 1142074
    :cond_0
    iget-object v0, p0, LX/6l3;->firstDeltaSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1142075
    iget-object v0, p0, LX/6l3;->firstDeltaSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1142076
    sget-object v0, LX/6l3;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142077
    iget-object v0, p0, LX/6l3;->firstDeltaSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1142078
    :cond_1
    iget-object v0, p0, LX/6l3;->lastIssuedSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1142079
    iget-object v0, p0, LX/6l3;->lastIssuedSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1142080
    sget-object v0, LX/6l3;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142081
    iget-object v0, p0, LX/6l3;->lastIssuedSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1142082
    :cond_2
    iget-object v0, p0, LX/6l3;->queueEntityId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1142083
    iget-object v0, p0, LX/6l3;->queueEntityId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1142084
    sget-object v0, LX/6l3;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142085
    iget-object v0, p0, LX/6l3;->queueEntityId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1142086
    :cond_3
    iget-object v0, p0, LX/6l3;->failedSend:LX/6kY;

    if-eqz v0, :cond_4

    .line 1142087
    iget-object v0, p0, LX/6l3;->failedSend:LX/6kY;

    if-eqz v0, :cond_4

    .line 1142088
    sget-object v0, LX/6l3;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142089
    iget-object v0, p0, LX/6l3;->failedSend:LX/6kY;

    invoke-virtual {v0, p1}, LX/6kY;->a(LX/1su;)V

    .line 1142090
    :cond_4
    iget-object v0, p0, LX/6l3;->syncToken:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1142091
    iget-object v0, p0, LX/6l3;->syncToken:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1142092
    sget-object v0, LX/6l3;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142093
    iget-object v0, p0, LX/6l3;->syncToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1142094
    :cond_5
    iget-object v0, p0, LX/6l3;->errorCode:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1142095
    iget-object v0, p0, LX/6l3;->errorCode:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1142096
    sget-object v0, LX/6l3;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142097
    iget-object v0, p0, LX/6l3;->errorCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1142098
    :cond_6
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1142099
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1142100
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1141916
    if-nez p1, :cond_1

    .line 1141917
    :cond_0
    :goto_0
    return v0

    .line 1141918
    :cond_1
    instance-of v1, p1, LX/6l3;

    if-eqz v1, :cond_0

    .line 1141919
    check-cast p1, LX/6l3;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1141920
    if-nez p1, :cond_3

    .line 1141921
    :cond_2
    :goto_1
    move v0, v2

    .line 1141922
    goto :goto_0

    .line 1141923
    :cond_3
    iget-object v0, p0, LX/6l3;->deltas:Ljava/util/List;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1141924
    :goto_2
    iget-object v3, p1, LX/6l3;->deltas:Ljava/util/List;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1141925
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1141926
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1141927
    iget-object v0, p0, LX/6l3;->deltas:Ljava/util/List;

    iget-object v3, p1, LX/6l3;->deltas:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1141928
    :cond_5
    iget-object v0, p0, LX/6l3;->firstDeltaSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1141929
    :goto_4
    iget-object v3, p1, LX/6l3;->firstDeltaSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1141930
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1141931
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1141932
    iget-object v0, p0, LX/6l3;->firstDeltaSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/6l3;->firstDeltaSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1141933
    :cond_7
    iget-object v0, p0, LX/6l3;->lastIssuedSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1141934
    :goto_6
    iget-object v3, p1, LX/6l3;->lastIssuedSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1141935
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1141936
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1141937
    iget-object v0, p0, LX/6l3;->lastIssuedSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/6l3;->lastIssuedSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1141938
    :cond_9
    iget-object v0, p0, LX/6l3;->queueEntityId:Ljava/lang/Long;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1141939
    :goto_8
    iget-object v3, p1, LX/6l3;->queueEntityId:Ljava/lang/Long;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1141940
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1141941
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1141942
    iget-object v0, p0, LX/6l3;->queueEntityId:Ljava/lang/Long;

    iget-object v3, p1, LX/6l3;->queueEntityId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1141943
    :cond_b
    iget-object v0, p0, LX/6l3;->failedSend:LX/6kY;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1141944
    :goto_a
    iget-object v3, p1, LX/6l3;->failedSend:LX/6kY;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1141945
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1141946
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1141947
    iget-object v0, p0, LX/6l3;->failedSend:LX/6kY;

    iget-object v3, p1, LX/6l3;->failedSend:LX/6kY;

    invoke-virtual {v0, v3}, LX/6kY;->a(LX/6kY;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1141948
    :cond_d
    iget-object v0, p0, LX/6l3;->syncToken:Ljava/lang/String;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1141949
    :goto_c
    iget-object v3, p1, LX/6l3;->syncToken:Ljava/lang/String;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1141950
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1141951
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1141952
    iget-object v0, p0, LX/6l3;->syncToken:Ljava/lang/String;

    iget-object v3, p1, LX/6l3;->syncToken:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1141953
    :cond_f
    iget-object v0, p0, LX/6l3;->errorCode:Ljava/lang/String;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1141954
    :goto_e
    iget-object v3, p1, LX/6l3;->errorCode:Ljava/lang/String;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1141955
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1141956
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1141957
    iget-object v0, p0, LX/6l3;->errorCode:Ljava/lang/String;

    iget-object v3, p1, LX/6l3;->errorCode:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_11
    move v2, v1

    .line 1141958
    goto/16 :goto_1

    :cond_12
    move v0, v2

    .line 1141959
    goto/16 :goto_2

    :cond_13
    move v3, v2

    .line 1141960
    goto/16 :goto_3

    :cond_14
    move v0, v2

    .line 1141961
    goto/16 :goto_4

    :cond_15
    move v3, v2

    .line 1141962
    goto/16 :goto_5

    :cond_16
    move v0, v2

    .line 1141963
    goto/16 :goto_6

    :cond_17
    move v3, v2

    .line 1141964
    goto/16 :goto_7

    :cond_18
    move v0, v2

    .line 1141965
    goto/16 :goto_8

    :cond_19
    move v3, v2

    .line 1141966
    goto :goto_9

    :cond_1a
    move v0, v2

    .line 1141967
    goto :goto_a

    :cond_1b
    move v3, v2

    .line 1141968
    goto :goto_b

    :cond_1c
    move v0, v2

    .line 1141969
    goto :goto_c

    :cond_1d
    move v3, v2

    .line 1141970
    goto :goto_d

    :cond_1e
    move v0, v2

    .line 1141971
    goto :goto_e

    :cond_1f
    move v3, v2

    .line 1141972
    goto :goto_f
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1141915
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1141912
    sget-boolean v0, LX/6l3;->a:Z

    .line 1141913
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6l3;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1141914
    return-object v0
.end method
