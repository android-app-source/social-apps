.class public LX/6tG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6sl;


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1154124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1154125
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The bundle of SimpleBundledCheckoutRow found to be empty."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1154126
    iput-object p1, p0, LX/6tG;->a:LX/0Px;

    .line 1154127
    return-void

    .line 1154128
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1

    .prologue
    .line 1154129
    iget-object v0, p0, LX/6tG;->a:LX/0Px;

    return-object v0
.end method

.method public final b()LX/6so;
    .locals 2

    .prologue
    .line 1154130
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SimpleBundledCheckoutRow does not have a common type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1154131
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1154132
    const/4 v0, 0x0

    return v0
.end method
