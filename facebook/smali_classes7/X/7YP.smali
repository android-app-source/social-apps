.class public LX/7YP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/7YP;


# instance fields
.field public final a:LX/0aG;

.field public final b:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0tX;

.field public final e:LX/1mR;

.field public final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final g:LX/0Or;
    .annotation runtime Lcom/facebook/zero/upsell/annotations/IsInZeroUpsellGetPromosGraphQLGatekeeper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0aG;Lcom/facebook/zero/service/ZeroHeaderRequestManager;LX/0Ot;LX/0tX;LX/1mR;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V
    .locals 1
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/upsell/annotations/IsInZeroUpsellGetPromosGraphQLGatekeeper;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "Lcom/facebook/zero/service/ZeroHeaderRequestManager;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0tX;",
            "LX/1mR;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1219747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1219748
    const-string v0, ""

    iput-object v0, p0, LX/7YP;->h:Ljava/lang/String;

    .line 1219749
    iput-object p1, p0, LX/7YP;->a:LX/0aG;

    .line 1219750
    iput-object p2, p0, LX/7YP;->b:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    .line 1219751
    iput-object p3, p0, LX/7YP;->c:LX/0Ot;

    .line 1219752
    iput-object p4, p0, LX/7YP;->d:LX/0tX;

    .line 1219753
    iput-object p5, p0, LX/7YP;->e:LX/1mR;

    .line 1219754
    iput-object p6, p0, LX/7YP;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1219755
    iput-object p7, p0, LX/7YP;->g:LX/0Or;

    .line 1219756
    return-void
.end method

.method public static a(LX/0QB;)LX/7YP;
    .locals 11

    .prologue
    .line 1219757
    sget-object v0, LX/7YP;->i:LX/7YP;

    if-nez v0, :cond_1

    .line 1219758
    const-class v1, LX/7YP;

    monitor-enter v1

    .line 1219759
    :try_start_0
    sget-object v0, LX/7YP;->i:LX/7YP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1219760
    if-eqz v2, :cond_0

    .line 1219761
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1219762
    new-instance v3, LX/7YP;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->a(LX/0QB;)Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    move-result-object v5

    check-cast v5, Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    const/16 v6, 0x140f

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v0}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v8

    check-cast v8, LX/1mR;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v10, 0x15b5

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/7YP;-><init>(LX/0aG;Lcom/facebook/zero/service/ZeroHeaderRequestManager;LX/0Ot;LX/0tX;LX/1mR;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V

    .line 1219763
    move-object v0, v3

    .line 1219764
    sput-object v0, LX/7YP;->i:LX/7YP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1219765
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1219766
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1219767
    :cond_1
    sget-object v0, LX/7YP;->i:LX/7YP;

    return-object v0

    .line 1219768
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1219769
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/15i;I)Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;
    .locals 39
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "buildPromoResult"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1219770
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1219771
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v3}, LX/15i;->g(II)I

    move-result v3

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v36

    .line 1219772
    const/4 v3, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v37

    .line 1219773
    const/4 v3, 0x5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v38

    .line 1219774
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v34

    .line 1219775
    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v35

    .line 1219776
    const/4 v3, 0x1

    const v4, -0x443ad4c4

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    .line 1219777
    if-eqz v2, :cond_0

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    .line 1219778
    :goto_0
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 1219779
    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v19

    :goto_1
    invoke-interface/range {v19 .. v19}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1219780
    invoke-interface/range {v19 .. v19}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    .line 1219781
    iget-object v15, v2, LX/1vs;->a:LX/15i;

    iget v0, v2, LX/1vs;->b:I

    move/from16 v16, v0

    .line 1219782
    const/4 v2, 0x1

    const-class v3, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel$MobileCarrierAccountModel$CarrierAccountUpsellProductsModel$EdgesModel$NodeModel;

    move/from16 v0, v16

    invoke-virtual {v15, v0, v2, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel$MobileCarrierAccountModel$CarrierAccountUpsellProductsModel$EdgesModel$NodeModel;

    .line 1219783
    new-instance v2, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    invoke-virtual {v11}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel$MobileCarrierAccountModel$CarrierAccountUpsellProductsModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v11}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel$MobileCarrierAccountModel$CarrierAccountUpsellProductsModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel$MobileCarrierAccountModel$CarrierAccountUpsellProductsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    move/from16 v0, v16

    invoke-virtual {v15, v0, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const-string v9, ""

    const-string v10, ""

    invoke-virtual {v11}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel$MobileCarrierAccountModel$CarrierAccountUpsellProductsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    const-string v12, ""

    const/4 v13, 0x1

    const/4 v14, 0x1

    const/16 v17, 0x2

    invoke-virtual/range {v15 .. v17}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v15

    const-string v16, ""

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZZLjava/lang/String;Ljava/lang/String;LX/0Px;)V

    .line 1219784
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1219785
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 1219786
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_0

    .line 1219787
    :cond_1
    invoke-static/range {v18 .. v18}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v18

    .line 1219788
    new-instance v3, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const-wide/16 v16, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v23

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    move-object/from16 v4, v37

    move-object/from16 v5, v38

    move-object/from16 v6, v36

    invoke-direct/range {v3 .. v35}, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;JLX/0Px;Ljava/lang/String;Ljava/lang/String;IILX/0Px;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1219789
    return-object v3
.end method

.method public static a$redex0(LX/7YP;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel;",
            ">;)",
            "Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;"
        }
    .end annotation

    .prologue
    .line 1219790
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1219791
    check-cast v0, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel;

    invoke-virtual {v0}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1219792
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1219793
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1219794
    iget-object v3, p0, LX/7YP;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/0df;->v:LX/0Tn;

    const/4 v5, 0x2

    invoke-virtual {v1, v2, v5}, LX/15i;->j(II)I

    move-result v2

    int-to-long v6, v2

    invoke-interface {v3, v4, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1219795
    invoke-static {v1, v0}, LX/7YP;->a(LX/15i;I)Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

    move-result-object v0

    .line 1219796
    return-object v0

    .line 1219797
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1219798
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1219799
    iget-object v0, p0, LX/7YP;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1219800
    new-instance v1, LX/7YB;

    invoke-direct {v1}, LX/7YB;-><init>()V

    const-string v2, "location"

    invoke-virtual {p1}, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "feature"

    invoke-virtual {p1}, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "size"

    const-string v3, "MEGAPHONE_2X"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    .line 1219801
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 1219802
    iget-object v2, p0, LX/7YP;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0df;->v:LX/0Tn;

    const-wide/16 v5, 0xe10

    invoke-interface {v2, v3, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v3

    .line 1219803
    new-instance v2, LX/7YB;

    invoke-direct {v2}, LX/7YB;-><init>()V

    move-object v2, v2

    .line 1219804
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    const-string v5, "ZeroUpsellRequest"

    invoke-static {v5}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v5

    .line 1219805
    iput-object v5, v2, LX/0zO;->d:Ljava/util/Set;

    .line 1219806
    move-object v2, v2

    .line 1219807
    invoke-virtual {v2, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->a:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    invoke-virtual {v1, v3, v4}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    .line 1219808
    iget-object v2, p0, LX/7YP;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0df;->j:LX/0Tn;

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1219809
    iget-object v3, p0, LX/7YP;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1219810
    iget-object v3, p0, LX/7YP;->e:LX/1mR;

    const-string v4, "ZeroUpsellRequest"

    invoke-static {v4}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1219811
    iput-object v2, p0, LX/7YP;->h:Ljava/lang/String;

    .line 1219812
    :cond_0
    iget-object v2, p0, LX/7YP;->d:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1219813
    new-instance v3, LX/7YM;

    invoke-direct {v3, p0}, LX/7YM;-><init>(LX/7YP;)V

    iget-object v1, p0, LX/7YP;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 1219814
    :goto_0
    return-object v0

    .line 1219815
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1219816
    const-string v1, "zeroBuyPromoParams"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1219817
    iget-object v1, p0, LX/7YP;->a:LX/0aG;

    const-string v2, "zero_get_recommended_promo"

    const v3, 0x41edc94a

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 1219818
    new-instance v2, LX/7YL;

    invoke-direct {v2, p0}, LX/7YL;-><init>(LX/7YP;)V

    iget-object v0, p0, LX/7YP;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 1219819
    goto :goto_0
.end method
