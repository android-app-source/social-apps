.class public LX/7ym;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;

.field public b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/7yo;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1280291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280292
    const/4 v0, 0x0

    iput-object v0, p0, LX/7ym;->a:Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;

    .line 1280293
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/7ym;->b:Ljava/util/Set;

    .line 1280294
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7ym;->c:Z

    .line 1280295
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7ym;->d:Z

    .line 1280296
    return-void
.end method

.method public static a(LX/0QB;)LX/7ym;
    .locals 3

    .prologue
    .line 1280280
    const-class v1, LX/7ym;

    monitor-enter v1

    .line 1280281
    :try_start_0
    sget-object v0, LX/7ym;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1280282
    sput-object v2, LX/7ym;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1280283
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1280284
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1280285
    new-instance v0, LX/7ym;

    invoke-direct {v0}, LX/7ym;-><init>()V

    .line 1280286
    move-object v0, v0

    .line 1280287
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1280288
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/7ym;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1280289
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1280290
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
