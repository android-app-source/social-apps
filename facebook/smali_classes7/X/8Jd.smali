.class public LX/8Jd;
.super Landroid/view/View;
.source ""


# instance fields
.field private final a:[I

.field private final b:[F

.field private c:Landroid/graphics/PointF;

.field private d:F

.field private e:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1328959
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/8Jd;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1328960
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 1328961
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1328962
    new-array v0, v4, [I

    aput v2, v0, v2

    const/4 v1, 0x1

    aput v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, LX/8Jd;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a05bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, LX/8Jd;->a:[I

    .line 1328963
    new-array v0, v4, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, LX/8Jd;->b:[F

    .line 1328964
    return-void

    :array_0
    .array-data 4
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1328965
    iget-object v0, p0, LX/8Jd;->c:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    iget v0, p0, LX/8Jd;->d:F

    cmpl-float v0, v0, v7

    if-lez v0, :cond_0

    iget-object v0, p0, LX/8Jd;->e:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    .line 1328966
    iget-object v0, p0, LX/8Jd;->e:Landroid/graphics/Matrix;

    iget v1, p0, LX/8Jd;->d:F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRadius(F)F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    div-float v3, v0, v1

    .line 1328967
    const/4 v0, 0x2

    new-array v2, v0, [F

    iget-object v0, p0, LX/8Jd;->c:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    aput v0, v2, v4

    iget-object v0, p0, LX/8Jd;->c:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    aput v0, v2, v8

    .line 1328968
    iget-object v0, p0, LX/8Jd;->e:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1328969
    new-instance v0, Landroid/graphics/RadialGradient;

    aget v1, v2, v4

    aget v2, v2, v8

    iget-object v4, p0, LX/8Jd;->a:[I

    iget-object v5, p0, LX/8Jd;->b:[F

    sget-object v6, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v6}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 1328970
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 1328971
    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setDither(Z)V

    .line 1328972
    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1328973
    invoke-virtual {p0}, LX/8Jd;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, LX/8Jd;->getHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, p1

    move v1, v7

    move v2, v7

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1328974
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1328975
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1328976
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1328977
    invoke-virtual {p0, v0, v1}, LX/8Jd;->setMeasuredDimension(II)V

    .line 1328978
    return-void
.end method

.method public setPosition(Landroid/graphics/PointF;)V
    .locals 0

    .prologue
    .line 1328979
    iput-object p1, p0, LX/8Jd;->c:Landroid/graphics/PointF;

    .line 1328980
    invoke-virtual {p0}, LX/8Jd;->invalidate()V

    .line 1328981
    return-void
.end method

.method public setRadius(F)V
    .locals 0

    .prologue
    .line 1328982
    iput p1, p0, LX/8Jd;->d:F

    .line 1328983
    invoke-virtual {p0}, LX/8Jd;->invalidate()V

    .line 1328984
    return-void
.end method

.method public setTransformMatrix(Landroid/graphics/Matrix;)V
    .locals 0

    .prologue
    .line 1328985
    iput-object p1, p0, LX/8Jd;->e:Landroid/graphics/Matrix;

    .line 1328986
    invoke-virtual {p0}, LX/8Jd;->invalidate()V

    .line 1328987
    return-void
.end method
