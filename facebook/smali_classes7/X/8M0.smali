.class public LX/8M0;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:LX/0b3;

.field public final d:LX/7ma;

.field public final e:LX/7mf;

.field public final f:LX/7mW;

.field public final g:LX/7mY;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0ad;

.field public final j:LX/7mc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7mc",
            "<",
            "LX/7mo;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/7mc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7mc",
            "<",
            "LX/7mj;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/7mc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7mc",
            "<",
            "LX/7ml;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/7mc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7mc",
            "<",
            "LX/7ml;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/8KR;

.field public final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7ml;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7mo;",
            ">;"
        }
    .end annotation
.end field

.field public final q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7mj;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7ml;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/photos/upload/progresspage/CompostStoryFetcher$CompostDataListener;",
            ">;"
        }
    .end annotation
.end field

.field public t:I

.field public u:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1334250
    const-class v0, LX/8M0;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/8M0;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0b3;LX/7ma;LX/7mf;LX/7mW;LX/7mY;LX/0Ot;LX/0ad;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0b3;",
            "LX/7ma;",
            "LX/7mf;",
            "LX/7mW;",
            "LX/7mY;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1334231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1334232
    new-instance v0, LX/8Ls;

    invoke-direct {v0, p0}, LX/8Ls;-><init>(LX/8M0;)V

    iput-object v0, p0, LX/8M0;->j:LX/7mc;

    .line 1334233
    new-instance v0, LX/8Lt;

    invoke-direct {v0, p0}, LX/8Lt;-><init>(LX/8M0;)V

    iput-object v0, p0, LX/8M0;->k:LX/7mc;

    .line 1334234
    new-instance v0, LX/8Lu;

    invoke-direct {v0, p0}, LX/8Lu;-><init>(LX/8M0;)V

    iput-object v0, p0, LX/8M0;->l:LX/7mc;

    .line 1334235
    new-instance v0, LX/8Lv;

    invoke-direct {v0, p0}, LX/8Lv;-><init>(LX/8M0;)V

    iput-object v0, p0, LX/8M0;->m:LX/7mc;

    .line 1334236
    new-instance v0, LX/8Lz;

    invoke-direct {v0, p0}, LX/8Lz;-><init>(LX/8M0;)V

    iput-object v0, p0, LX/8M0;->n:LX/8KR;

    .line 1334237
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8M0;->o:Ljava/util/List;

    .line 1334238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8M0;->p:Ljava/util/List;

    .line 1334239
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8M0;->q:Ljava/util/List;

    .line 1334240
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8M0;->r:Ljava/util/List;

    .line 1334241
    iput-object p1, p0, LX/8M0;->c:LX/0b3;

    .line 1334242
    iput-object p2, p0, LX/8M0;->d:LX/7ma;

    .line 1334243
    iput-object p3, p0, LX/8M0;->e:LX/7mf;

    .line 1334244
    iput-object p4, p0, LX/8M0;->f:LX/7mW;

    .line 1334245
    iput-object p5, p0, LX/8M0;->g:LX/7mY;

    .line 1334246
    iput-object p6, p0, LX/8M0;->h:LX/0Ot;

    .line 1334247
    iput-object p7, p0, LX/8M0;->i:LX/0ad;

    .line 1334248
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/8M0;->s:LX/0am;

    .line 1334249
    return-void
.end method

.method public static a(LX/8M0;LX/8Ly;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/compost/story/CompostStory;",
            ">(",
            "LX/8Ly;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1334229
    iget-object v0, p0, LX/8M0;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    new-instance v1, LX/8Lw;

    invoke-direct {v1, p0, p2}, LX/8Lw;-><init>(LX/8M0;Lcom/google/common/util/concurrent/ListenableFuture;)V

    new-instance v2, LX/8Lx;

    invoke-direct {v2, p0}, LX/8Lx;-><init>(LX/8M0;)V

    invoke-virtual {v0, p1, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1334230
    return-void
.end method

.method public static b(LX/0QB;)LX/8M0;
    .locals 8

    .prologue
    .line 1334225
    new-instance v0, LX/8M0;

    invoke-static {p0}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v1

    check-cast v1, LX/0b3;

    invoke-static {p0}, LX/7ma;->a(LX/0QB;)LX/7ma;

    move-result-object v2

    check-cast v2, LX/7ma;

    invoke-static {p0}, LX/7mf;->a(LX/0QB;)LX/7mf;

    move-result-object v3

    check-cast v3, LX/7mf;

    invoke-static {p0}, LX/7mW;->a(LX/0QB;)LX/7mW;

    move-result-object v4

    check-cast v4, LX/7mW;

    invoke-static {p0}, LX/7mY;->a(LX/0QB;)LX/7mY;

    move-result-object v5

    check-cast v5, LX/7mY;

    const/16 v6, 0x12b1

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-direct/range {v0 .. v7}, LX/8M0;-><init>(LX/0b3;LX/7ma;LX/7mf;LX/7mW;LX/7mY;LX/0Ot;LX/0ad;)V

    .line 1334226
    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    .line 1334227
    iput-object v1, v0, LX/8M0;->b:LX/0SG;

    .line 1334228
    return-object v0
.end method

.method public static synthetic c(LX/8M0;)I
    .locals 2

    .prologue
    .line 1334224
    iget v0, p0, LX/8M0;->t:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/8M0;->t:I

    return v0
.end method

.method public static k(LX/8M0;)Z
    .locals 2

    .prologue
    .line 1334251
    iget v0, p0, LX/8M0;->t:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(LX/8M0;)V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1334152
    iget-object v0, p0, LX/8M0;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1334153
    iget-object v0, p0, LX/8M0;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1334154
    iget-object v0, p0, LX/8M0;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1334155
    iget-object v0, p0, LX/8M0;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1334156
    :try_start_0
    iget-object v0, p0, LX/8M0;->d:LX/7ma;

    invoke-virtual {v0}, LX/7ma;->c()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ml;

    .line 1334157
    iget-object v5, v0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v5, v5

    .line 1334158
    invoke-virtual {p0, v5}, LX/8M0;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1334159
    iget-object v5, p0, LX/8M0;->o:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1334160
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1334161
    :cond_1
    iget-object v0, p0, LX/8M0;->e:LX/7mf;

    invoke-virtual {v0}, LX/7mV;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const v1, 0x43a3298a

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_3

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7mo;

    .line 1334162
    iget-object v5, v1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v5, v5

    .line 1334163
    invoke-virtual {p0, v5}, LX/8M0;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1334164
    iget-object v5, p0, LX/8M0;->p:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1334165
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 1334166
    :cond_3
    iget-object v0, p0, LX/8M0;->f:LX/7mW;

    invoke-virtual {v0}, LX/7mV;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const v1, 0x33fdeeff

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_4

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7mj;

    .line 1334167
    iget-object v5, p0, LX/8M0;->q:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1334168
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 1334169
    :cond_4
    iget-object v0, p0, LX/8M0;->g:LX/7mY;

    invoke-virtual {v0}, LX/7mY;->c()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_3
    if-ge v1, v4, :cond_5

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ml;

    .line 1334170
    iget-object v2, p0, LX/8M0;->r:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1334171
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1334172
    :catch_0
    move-exception v0

    .line 1334173
    sget-object v1, LX/8M0;->a:Ljava/lang/String;

    const-string v2, "Interrupted while fetching stories"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1334174
    iput-boolean v6, p0, LX/8M0;->u:Z

    .line 1334175
    :cond_5
    :goto_4
    invoke-static {p0}, LX/8M0;->k(LX/8M0;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1334176
    iget-object v0, p0, LX/8M0;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1334177
    iget-object v0, p0, LX/8M0;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Lc;

    .line 1334178
    iget-object v1, v0, LX/8Lc;->a:Lcom/facebook/photos/upload/progresspage/CompostFragment;

    .line 1334179
    invoke-static {v1}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->m(Lcom/facebook/photos/upload/progresspage/CompostFragment;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1334180
    iget-object v2, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    .line 1334181
    iget-boolean v3, v2, LX/8M0;->u:Z

    move v2, v3

    .line 1334182
    if-eqz v2, :cond_7

    .line 1334183
    iget-object v2, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->b:LX/1RW;

    .line 1334184
    iget-object v3, v2, LX/1RW;->a:LX/0Zb;

    const-string v4, "show_failure_state"

    invoke-static {v2, v4}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1334185
    iget-object v2, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    new-instance v3, LX/8Lh;

    invoke-direct {v3, v1}, LX/8Lh;-><init>(Lcom/facebook/photos/upload/progresspage/CompostFragment;)V

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(LX/1DI;Ljava/lang/Runnable;)V

    .line 1334186
    :goto_5
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 1334187
    iget-object v3, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    invoke-virtual {v3}, LX/8M0;->g()LX/0Px;

    move-result-object v4

    .line 1334188
    iget-boolean v3, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->p:Z

    if-eqz v3, :cond_6

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 1334189
    iput-boolean v2, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->p:Z

    .line 1334190
    iget-object v3, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->t:Ljava/lang/String;

    if-nez v3, :cond_b

    .line 1334191
    iget-object v3, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->d:LX/8Mg;

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7mj;

    invoke-virtual {v3, v2, v1, v8}, LX/8Mg;->a(LX/7mj;Landroid/support/v4/app/Fragment;I)V

    .line 1334192
    :cond_6
    return-void

    .line 1334193
    :catch_1
    move-exception v0

    .line 1334194
    sget-object v1, LX/8M0;->a:Ljava/lang/String;

    const-string v2, "Failed to fetch stories"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1334195
    iput-boolean v6, p0, LX/8M0;->u:Z

    goto :goto_4

    .line 1334196
    :cond_7
    invoke-static {v1}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->p(Lcom/facebook/photos/upload/progresspage/CompostFragment;)V

    goto :goto_5

    .line 1334197
    :cond_8
    const/16 p0, 0x8

    const/4 v9, 0x0

    .line 1334198
    iget-object v2, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1334199
    iget-object v2, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v2, p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 1334200
    iget-object v2, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    invoke-virtual {v2}, LX/8M0;->d()LX/0Px;

    move-result-object v2

    .line 1334201
    iget-object v3, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    invoke-virtual {v3}, LX/8M0;->e()LX/0Px;

    move-result-object v3

    .line 1334202
    iget-object v4, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    invoke-virtual {v4}, LX/8M0;->g()LX/0Px;

    move-result-object v4

    .line 1334203
    iget-object v5, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    invoke-virtual {v5}, LX/8M0;->h()LX/0Px;

    move-result-object v5

    .line 1334204
    iget-object v6, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    .line 1334205
    iget-object v7, v6, LX/8M0;->r:Ljava/util/List;

    invoke-static {v7}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v7

    move-object v6, v7

    .line 1334206
    iget-object v7, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    sget-object v8, LX/8K6;->PENDING_SECTION:LX/8K6;

    invoke-virtual {v7, v8, v2}, LX/8Lq;->a(LX/8K6;Ljava/util/List;)V

    .line 1334207
    iget-object v7, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    sget-object v8, LX/8K6;->UPLOADED_SECTION:LX/8K6;

    invoke-virtual {v7, v8, v3}, LX/8Lq;->a(LX/8K6;Ljava/util/List;)V

    .line 1334208
    iget-object v7, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    sget-object v8, LX/8K6;->DRAFT_SECTION:LX/8K6;

    invoke-virtual {v7, v8, v4}, LX/8Lq;->a(LX/8K6;Ljava/util/List;)V

    .line 1334209
    iget-object v7, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->l:LX/0ad;

    sget-short v8, LX/1aO;->az:S

    invoke-interface {v7, v8, v9}, LX/0ad;->a(SZ)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1334210
    iget-object v7, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    sget-object v8, LX/8K6;->SCHEDULED_SECTION:LX/8K6;

    invoke-virtual {v7, v8, v5}, LX/8Lq;->a(LX/8K6;Ljava/util/List;)V

    .line 1334211
    :cond_9
    iget-object v7, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->l:LX/0ad;

    sget-short v8, LX/1aO;->as:S

    invoke-interface {v7, v8, v9}, LX/0ad;->a(SZ)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1334212
    iget-object v7, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    sget-object v8, LX/8K6;->FATAL_SECTION:LX/8K6;

    invoke-virtual {v7, v8, v6}, LX/8Lq;->a(LX/8K6;Ljava/util/List;)V

    .line 1334213
    :cond_a
    iget-object v6, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->b:LX/1RW;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    .line 1334214
    iget-object v7, v6, LX/1RW;->a:LX/0Zb;

    const-string v8, "num_rows"

    invoke-static {v6, v8}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v0, "uploading"

    invoke-virtual {v8, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v0, "uploaded"

    invoke-virtual {v8, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v0, "drafts"

    invoke-virtual {v8, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v0, "scheduled"

    invoke-virtual {v8, v0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    invoke-interface {v7, v8}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1334215
    invoke-static {v1, v9}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->a(Lcom/facebook/photos/upload/progresspage/CompostFragment;I)V

    .line 1334216
    invoke-static {v1, p0}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->d(Lcom/facebook/photos/upload/progresspage/CompostFragment;I)V

    .line 1334217
    iget-object v2, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->q:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, LX/1P1;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1334218
    iget-object v2, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1334219
    goto/16 :goto_5

    .line 1334220
    :cond_b
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_6
    if-ge v3, v5, :cond_6

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7mj;

    .line 1334221
    iget-object v6, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->t:Ljava/lang/String;

    invoke-virtual {v2}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1334222
    iget-object v6, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->d:LX/8Mg;

    invoke-virtual {v6, v2, v1, v8}, LX/8Mg;->a(LX/7mj;Landroid/support/v4/app/Fragment;I)V

    .line 1334223
    :cond_c
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 1334143
    iget-object v1, p0, LX/8M0;->i:LX/0ad;

    sget-short v2, LX/1aO;->S:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1334144
    invoke-static {p1}, LX/17E;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1334145
    iget-object v0, p0, LX/8M0;->i:LX/0ad;

    sget-short v1, LX/1aO;->T:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 1334146
    :cond_0
    :goto_0
    return v0

    .line 1334147
    :cond_1
    invoke-static {p1}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1334148
    iget-object v0, p0, LX/8M0;->i:LX/0ad;

    sget-short v1, LX/1aO;->X:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0

    .line 1334149
    :cond_2
    invoke-static {p1}, LX/17E;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1334150
    iget-object v0, p0, LX/8M0;->i:LX/0ad;

    sget-short v1, LX/1aO;->W:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0

    .line 1334151
    :cond_3
    invoke-static {p1}, LX/17E;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/7ml;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1334142
    iget-object v0, p0, LX/8M0;->o:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/7mo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1334141
    iget-object v0, p0, LX/8M0;->p:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/7mj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1334136
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1334137
    iget-object v0, p0, LX/8M0;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7mj;

    .line 1334138
    invoke-virtual {v0}, LX/7mj;->c()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1334139
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1334140
    :cond_1
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final h()LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/7mj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1334129
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1334130
    iget-object v0, p0, LX/8M0;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7mj;

    .line 1334131
    invoke-virtual {v0}, LX/7mj;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1334132
    iget-wide v10, v0, LX/7mj;->c:J

    move-wide v4, v10

    .line 1334133
    iget-object v3, p0, LX/8M0;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 1334134
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1334135
    :cond_1
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
