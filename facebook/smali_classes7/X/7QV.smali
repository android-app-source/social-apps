.class public LX/7QV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x8
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:I

.field private final d:I

.field public e:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "LX/03z;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "LX/19o;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1204402
    const-class v0, LX/7QV;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7QV;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1204403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204404
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/7QV;->b:Ljava/lang/String;

    .line 1204405
    iget-object v0, p0, LX/7QV;->b:Ljava/lang/String;

    const-string v1, "AdaptationSet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/7QV;->c:I

    .line 1204406
    iget-object v0, p0, LX/7QV;->b:Ljava/lang/String;

    const-string v1, "AdaptationSet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0xd

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7QV;->d:I

    .line 1204407
    return-void
.end method

.method public static a(LX/7QV;I)Z
    .locals 3

    .prologue
    .line 1204408
    iget-object v0, p0, LX/7QV;->b:Ljava/lang/String;

    const/16 v1, 0x3c

    invoke-virtual {v0, v1, p1}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v0

    .line 1204409
    iget-object v1, p0, LX/7QV;->b:Ljava/lang/String;

    const-string v2, "AudioChannelConfiguration"

    invoke-virtual {v1, v2, p1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result v1

    .line 1204410
    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/List;)LX/19o;
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/19o;",
            ">;)",
            "LX/19o;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1204411
    const/4 v1, 0x0

    .line 1204412
    invoke-virtual {p0}, LX/7QV;->a()Ljava/util/LinkedHashSet;

    move-result-object v2

    .line 1204413
    invoke-static {p1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1204414
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19o;

    .line 1204415
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v1, v0

    .line 1204416
    :cond_1
    iget-object v0, p0, LX/7QV;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    .line 1204417
    iget-object v0, p0, LX/7QV;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 1204418
    if-nez v1, :cond_5

    .line 1204419
    const-string v0, ""

    iput-object v0, p0, LX/7QV;->g:Ljava/lang/String;

    .line 1204420
    :goto_0
    return-object v1

    .line 1204421
    :cond_2
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19o;

    .line 1204422
    if-eqz v1, :cond_3

    iget v3, v0, LX/19o;->priority:I

    iget v4, v1, LX/19o;->priority:I

    if-le v3, v4, :cond_4

    :cond_3
    :goto_2
    move-object v1, v0

    .line 1204423
    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_2

    .line 1204424
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "FBProjection=\""

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/19o;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1204425
    iget-object v2, p0, LX/7QV;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1204426
    if-gez v0, :cond_6

    .line 1204427
    const-string v0, ""

    iput-object v0, p0, LX/7QV;->g:Ljava/lang/String;

    goto :goto_0

    .line 1204428
    :cond_6
    iget-object v2, p0, LX/7QV;->b:Ljava/lang/String;

    const-string v3, "AdaptationSet"

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result v2

    .line 1204429
    iget-object v3, p0, LX/7QV;->b:Ljava/lang/String;

    const-string v4, "AdaptationSet"

    invoke-virtual {v3, v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 1204430
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/7QV;->b:Ljava/lang/String;

    invoke-virtual {v4, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "AdaptationSet>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7QV;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a()Ljava/util/LinkedHashSet;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashSet",
            "<",
            "LX/19o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1204431
    iget-object v0, p0, LX/7QV;->f:Ljava/util/LinkedHashSet;

    if-nez v0, :cond_1

    .line 1204432
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, LX/7QV;->f:Ljava/util/LinkedHashSet;

    .line 1204433
    iget v0, p0, LX/7QV;->c:I

    .line 1204434
    :cond_0
    :goto_0
    if-ltz v0, :cond_1

    .line 1204435
    iget-object v1, p0, LX/7QV;->b:Ljava/lang/String;

    const-string v2, "FBProjection=\""

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 1204436
    if-ltz v0, :cond_0

    .line 1204437
    iget-object v1, p0, LX/7QV;->b:Ljava/lang/String;

    const-string v2, "\""

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 1204438
    iget-object v1, p0, LX/7QV;->b:Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, LX/7QV;->b:Ljava/lang/String;

    const-string v4, "\""

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/19o;->fromString(Ljava/lang/String;)LX/19o;

    move-result-object v1

    .line 1204439
    sget-object v2, LX/19o;->UNKNOWN:LX/19o;

    if-eq v1, v2, :cond_0

    .line 1204440
    iget-object v2, p0, LX/7QV;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1204441
    :cond_1
    iget-object v0, p0, LX/7QV;->f:Ljava/util/LinkedHashSet;

    return-object v0
.end method

.method public final b(Ljava/util/List;)LX/03z;
    .locals 6
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/03z;",
            ">;)",
            "LX/03z;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1204442
    const/4 v1, 0x0

    .line 1204443
    const/16 v5, 0x22

    .line 1204444
    iget-object v0, p0, LX/7QV;->e:Ljava/util/LinkedHashSet;

    if-nez v0, :cond_2

    .line 1204445
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, LX/7QV;->e:Ljava/util/LinkedHashSet;

    .line 1204446
    iget v0, p0, LX/7QV;->c:I

    .line 1204447
    :cond_0
    :goto_0
    if-ltz v0, :cond_2

    .line 1204448
    iget-object v2, p0, LX/7QV;->b:Ljava/lang/String;

    const-string v3, "value=\""

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 1204449
    if-ltz v0, :cond_0

    .line 1204450
    invoke-static {p0, v0}, LX/7QV;->a(LX/7QV;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1204451
    iget-object v2, p0, LX/7QV;->b:Ljava/lang/String;

    invoke-virtual {v2, v5, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 1204452
    iget-object v2, p0, LX/7QV;->b:Ljava/lang/String;

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 1204453
    if-ltz v2, :cond_1

    .line 1204454
    iget-object v3, p0, LX/7QV;->b:Ljava/lang/String;

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/03z;->fromChannelConfiguration(Ljava/lang/String;)LX/03z;

    move-result-object v2

    .line 1204455
    sget-object v3, LX/03z;->UNKNOWN:LX/03z;

    if-eq v2, v3, :cond_1

    .line 1204456
    iget-object v3, p0, LX/7QV;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v3, v2}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 1204457
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1204458
    :cond_2
    iget-object v0, p0, LX/7QV;->e:Ljava/util/LinkedHashSet;

    move-object v2, v0

    .line 1204459
    invoke-static {p1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1204460
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03z;

    .line 1204461
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v1, v0

    .line 1204462
    :cond_4
    iget-object v0, p0, LX/7QV;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    .line 1204463
    iget-object v0, p0, LX/7QV;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 1204464
    if-nez v1, :cond_8

    .line 1204465
    const-string v0, ""

    iput-object v0, p0, LX/7QV;->h:Ljava/lang/String;

    .line 1204466
    :goto_1
    return-object v1

    .line 1204467
    :cond_5
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03z;

    .line 1204468
    if-eqz v1, :cond_6

    iget v3, v0, LX/03z;->priority:I

    iget v4, v1, LX/03z;->priority:I

    if-le v3, v4, :cond_7

    :cond_6
    :goto_3
    move-object v1, v0

    .line 1204469
    goto :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_3

    .line 1204470
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "value=\""

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v1, LX/03z;->channelConfiguration:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1204471
    iget-object v2, p0, LX/7QV;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1204472
    if-ltz v0, :cond_9

    invoke-static {p0, v0}, LX/7QV;->a(LX/7QV;I)Z

    move-result v2

    if-nez v2, :cond_a

    .line 1204473
    :cond_9
    const-string v0, ""

    iput-object v0, p0, LX/7QV;->h:Ljava/lang/String;

    goto :goto_1

    .line 1204474
    :cond_a
    iget-object v2, p0, LX/7QV;->b:Ljava/lang/String;

    const-string v3, "AdaptationSet"

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result v2

    .line 1204475
    iget-object v3, p0, LX/7QV;->b:Ljava/lang/String;

    const-string v4, "AdaptationSet"

    invoke-virtual {v3, v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 1204476
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/7QV;->b:Ljava/lang/String;

    invoke-virtual {v4, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "AdaptationSet>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7QV;->h:Ljava/lang/String;

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1204477
    iget-object v0, p0, LX/7QV;->h:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/7QV;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1204478
    :cond_0
    iget-object v0, p0, LX/7QV;->b:Ljava/lang/String;

    .line 1204479
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, LX/7QV;->b:Ljava/lang/String;

    const/4 v2, 0x0

    iget v3, p0, LX/7QV;->c:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/7QV;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/7QV;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/7QV;->b:Ljava/lang/String;

    iget v2, p0, LX/7QV;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
