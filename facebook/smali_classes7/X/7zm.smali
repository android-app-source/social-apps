.class public LX/7zm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1jf;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/7zm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1281366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1281367
    return-void
.end method

.method public static a(LX/0QB;)LX/7zm;
    .locals 3

    .prologue
    .line 1281349
    sget-object v0, LX/7zm;->a:LX/7zm;

    if-nez v0, :cond_1

    .line 1281350
    const-class v1, LX/7zm;

    monitor-enter v1

    .line 1281351
    :try_start_0
    sget-object v0, LX/7zm;->a:LX/7zm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1281352
    if-eqz v2, :cond_0

    .line 1281353
    :try_start_1
    new-instance v0, LX/7zm;

    invoke-direct {v0}, LX/7zm;-><init>()V

    .line 1281354
    move-object v0, v0

    .line 1281355
    sput-object v0, LX/7zm;->a:LX/7zm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1281356
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1281357
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1281358
    :cond_1
    sget-object v0, LX/7zm;->a:LX/7zm;

    return-object v0

    .line 1281359
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1281360
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1281361
    const-string v0, "FreshFeedSortKeyRanker.rerank"

    const v1, -0x2d121a7

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1281362
    :try_start_0
    sget-object v0, LX/69P;->a:Ljava/util/Comparator;

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1281363
    const v0, -0x6e802763

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1281364
    return-void

    .line 1281365
    :catchall_0
    move-exception v0

    const v1, 0x7381ac8a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
