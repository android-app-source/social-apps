.class public LX/77J;
.super LX/2g7;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/77J;


# instance fields
.field private final a:LX/0W9;


# direct methods
.method public constructor <init>(LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171705
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1171706
    iput-object p1, p0, LX/77J;->a:LX/0W9;

    .line 1171707
    return-void
.end method

.method public static a(LX/0QB;)LX/77J;
    .locals 4

    .prologue
    .line 1171692
    sget-object v0, LX/77J;->b:LX/77J;

    if-nez v0, :cond_1

    .line 1171693
    const-class v1, LX/77J;

    monitor-enter v1

    .line 1171694
    :try_start_0
    sget-object v0, LX/77J;->b:LX/77J;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1171695
    if-eqz v2, :cond_0

    .line 1171696
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1171697
    new-instance p0, LX/77J;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-direct {p0, v3}, LX/77J;-><init>(LX/0W9;)V

    .line 1171698
    move-object v0, p0

    .line 1171699
    sput-object v0, LX/77J;->b:LX/77J;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1171700
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1171701
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1171702
    :cond_1
    sget-object v0, LX/77J;->b:LX/77J;

    return-object v0

    .line 1171703
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1171704
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 2

    .prologue
    .line 1171690
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1171691
    iget-object v0, p0, LX/77J;->a:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
