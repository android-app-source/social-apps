.class public final enum LX/7iu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7iu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7iu;

.field public static final enum NOTIFY:LX/7iu;

.field public static final enum WARN:LX/7iu;


# instance fields
.field public colorResId:I

.field public iconResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1228836
    new-instance v0, LX/7iu;

    const-string v1, "NOTIFY"

    const v2, 0x7f020c0c

    const v3, 0x7f0a00d2

    invoke-direct {v0, v1, v4, v2, v3}, LX/7iu;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/7iu;->NOTIFY:LX/7iu;

    .line 1228837
    new-instance v0, LX/7iu;

    const-string v1, "WARN"

    const v2, 0x7f020c0b

    const v3, 0x7f0a00d3

    invoke-direct {v0, v1, v5, v2, v3}, LX/7iu;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/7iu;->WARN:LX/7iu;

    .line 1228838
    const/4 v0, 0x2

    new-array v0, v0, [LX/7iu;

    sget-object v1, LX/7iu;->NOTIFY:LX/7iu;

    aput-object v1, v0, v4

    sget-object v1, LX/7iu;->WARN:LX/7iu;

    aput-object v1, v0, v5

    sput-object v0, LX/7iu;->$VALUES:[LX/7iu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1228839
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1228840
    iput p3, p0, LX/7iu;->iconResId:I

    .line 1228841
    iput p4, p0, LX/7iu;->colorResId:I

    .line 1228842
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7iu;
    .locals 1

    .prologue
    .line 1228843
    const-class v0, LX/7iu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7iu;

    return-object v0
.end method

.method public static values()[LX/7iu;
    .locals 1

    .prologue
    .line 1228844
    sget-object v0, LX/7iu;->$VALUES:[LX/7iu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7iu;

    return-object v0
.end method
