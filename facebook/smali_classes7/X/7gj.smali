.class public LX/7gj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/util/Date;

.field public b:LX/7gi;

.field public c:Landroid/graphics/Bitmap;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:Z

.field public i:Ljava/lang/String;

.field public j:I

.field public k:F

.field public l:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/7gi;Landroid/graphics/Bitmap;Ljava/lang/String;IIZF)V
    .locals 1

    .prologue
    .line 1224785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224786
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, LX/7gj;->a:Ljava/util/Date;

    .line 1224787
    iput-object p1, p0, LX/7gj;->b:LX/7gi;

    .line 1224788
    iput-object p2, p0, LX/7gj;->c:Landroid/graphics/Bitmap;

    .line 1224789
    iput-object p3, p0, LX/7gj;->d:Ljava/lang/String;

    .line 1224790
    iput p4, p0, LX/7gj;->g:I

    .line 1224791
    iput-boolean p6, p0, LX/7gj;->h:Z

    .line 1224792
    const-string v0, ""

    iput-object v0, p0, LX/7gj;->i:Ljava/lang/String;

    .line 1224793
    iput p5, p0, LX/7gj;->j:I

    .line 1224794
    iput p7, p0, LX/7gj;->k:F

    .line 1224795
    const-string v0, ""

    iput-object v0, p0, LX/7gj;->l:Ljava/lang/String;

    .line 1224796
    return-void
.end method


# virtual methods
.method public final h()Z
    .locals 2

    .prologue
    .line 1224797
    iget-object v0, p0, LX/7gj;->b:LX/7gi;

    sget-object v1, LX/7gi;->PHOTO:LX/7gi;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 1224798
    iget-boolean v0, p0, LX/7gj;->h:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/7gj;->g:I

    iget v1, p0, LX/7gj;->j:I

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/7gj;->g:I

    iget v1, p0, LX/7gj;->j:I

    add-int/2addr v0, v1

    goto :goto_0
.end method
