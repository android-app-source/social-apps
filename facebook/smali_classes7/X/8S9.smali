.class public LX/8S9;
.super LX/8RL;
.source ""


# instance fields
.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/3fr;LX/0Sh;LX/3Oq;LX/2RQ;)V
    .locals 0
    .param p3    # LX/3Oq;
        .annotation runtime Lcom/facebook/contacts/module/ContactLinkQueryType;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1346199
    invoke-direct {p0, p1, p2, p3, p4}, LX/8RL;-><init>(LX/3fr;LX/0Sh;LX/3Oq;LX/2RQ;)V

    .line 1346200
    return-void
.end method

.method public static c(LX/0QB;)LX/8S9;
    .locals 5

    .prologue
    .line 1346201
    new-instance v4, LX/8S9;

    invoke-static {p0}, LX/3fr;->a(LX/0QB;)LX/3fr;

    move-result-object v0

    check-cast v0, LX/3fr;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/6NS;->b(LX/0QB;)LX/3Oq;

    move-result-object v2

    check-cast v2, LX/3Oq;

    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v3

    check-cast v3, LX/2RQ;

    invoke-direct {v4, v0, v1, v2, v3}, LX/8S9;-><init>(LX/3fr;LX/0Sh;LX/3Oq;LX/2RQ;)V

    .line 1346202
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1346203
    iput-object p1, p0, LX/8S9;->e:Ljava/lang/String;

    .line 1346204
    invoke-virtual {p0, p1}, LX/8RL;->b(Ljava/lang/String;)V

    .line 1346205
    return-void
.end method

.method public final a(LX/8QL;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1346206
    instance-of v1, p1, LX/8QM;

    if-eqz v1, :cond_0

    .line 1346207
    invoke-virtual {p1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1346208
    iget-object v2, p0, LX/8S9;->e:Ljava/lang/String;

    invoke-static {v2}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1346209
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 1346210
    :cond_0
    return v0
.end method
