.class public final LX/6vR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/6vH;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/6vH;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1156950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156951
    iput-object p1, p0, LX/6vR;->a:LX/0QB;

    .line 1156952
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1156953
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/6vR;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1156954
    packed-switch p2, :pswitch_data_0

    .line 1156955
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1156956
    :pswitch_0
    new-instance v0, LX/6vL;

    const/16 v1, 0x2cf0

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x2d0b

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 p2, 0x2cef

    invoke-static {p1, p2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-direct {v0, v1, p0, p2}, LX/6vL;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1156957
    move-object v0, v0

    .line 1156958
    :goto_0
    return-object v0

    .line 1156959
    :pswitch_1
    new-instance v0, LX/6vN;

    const/16 v1, 0x2cf9

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x2d0d

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 p2, 0x2cf2

    invoke-static {p1, p2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-direct {v0, v1, p0, p2}, LX/6vN;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1156960
    move-object v0, v0

    .line 1156961
    goto :goto_0

    .line 1156962
    :pswitch_2
    new-instance v0, LX/6vQ;

    const/16 v1, 0x2cf5

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x2d0c

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 p2, 0x2cf4

    invoke-static {p1, p2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-direct {v0, v1, p0, p2}, LX/6vQ;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1156963
    move-object v0, v0

    .line 1156964
    goto :goto_0

    .line 1156965
    :pswitch_3
    new-instance v0, LX/6vX;

    const/16 v1, 0x2cf9

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x2d0d

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 p2, 0x2cf7

    invoke-static {p1, p2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-direct {v0, v1, p0, p2}, LX/6vX;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1156966
    move-object v0, v0

    .line 1156967
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1156968
    const/4 v0, 0x4

    return v0
.end method
