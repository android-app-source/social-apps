.class public LX/7yk;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:I

.field public b:I

.field public c:[I

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7yn;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/view/ViewGroup;

.field private g:I

.field private h:[I

.field private i:Landroid/graphics/Rect;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1280173
    const-class v0, LX/7yk;

    sput-object v0, LX/7yk;->f:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x2

    const/4 v0, -0x1

    .line 1280162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280163
    iput v0, p0, LX/7yk;->g:I

    .line 1280164
    iput v0, p0, LX/7yk;->a:I

    .line 1280165
    iput v0, p0, LX/7yk;->b:I

    .line 1280166
    new-array v0, v1, [I

    iput-object v0, p0, LX/7yk;->c:[I

    .line 1280167
    new-array v0, v1, [I

    iput-object v0, p0, LX/7yk;->h:[I

    .line 1280168
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/7yk;->i:Landroid/graphics/Rect;

    .line 1280169
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7yk;->d:Ljava/util/List;

    .line 1280170
    const/4 v0, 0x0

    iput-object v0, p0, LX/7yk;->e:Landroid/view/ViewGroup;

    .line 1280171
    const/high16 v0, 0x42400000    # 48.0f

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/7yk;->g:I

    .line 1280172
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1280159
    iget-object v0, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1280160
    const/4 v0, 0x0

    iput-object v0, p0, LX/7yk;->e:Landroid/view/ViewGroup;

    .line 1280161
    return-void
.end method

.method public static c(LX/7yk;Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1280043
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 1280044
    :cond_0
    :goto_0
    return v0

    .line 1280045
    :cond_1
    instance-of v1, p1, Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 1280046
    check-cast p1, Landroid/widget/TextView;

    const/4 v1, 0x0

    .line 1280047
    invoke-virtual {p1}, Landroid/widget/TextView;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1280048
    invoke-virtual {p0, p1}, LX/7yk;->a(Landroid/view/View;)Z

    move-result v0

    .line 1280049
    :goto_1
    move v0, v0

    .line 1280050
    goto :goto_0

    .line 1280051
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->isClickable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1280052
    invoke-virtual {p0, p1}, LX/7yk;->a(Landroid/view/View;)Z

    move-result v0

    goto :goto_0

    .line 1280053
    :cond_3
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1280054
    instance-of v2, v0, Landroid/text/Spannable;

    if-nez v2, :cond_4

    move v0, v1

    .line 1280055
    goto :goto_1

    .line 1280056
    :cond_4
    invoke-virtual {p0, p1}, LX/7yk;->b(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    .line 1280057
    iget v3, p0, LX/7yk;->a:I

    iget v4, p0, LX/7yk;->b:I

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-nez v3, :cond_5

    move v0, v1

    .line 1280058
    goto :goto_1

    .line 1280059
    :cond_5
    check-cast v0, Landroid/text/Spannable;

    .line 1280060
    iget v3, p0, LX/7yk;->a:I

    iget v4, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    .line 1280061
    iget v4, p0, LX/7yk;->b:I

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int v2, v4, v2

    .line 1280062
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    .line 1280063
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v4

    sub-int/2addr v2, v4

    .line 1280064
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    move-result v4

    add-int/2addr v3, v4

    .line 1280065
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    move-result v4

    add-int/2addr v2, v4

    .line 1280066
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v4

    .line 1280067
    if-nez v4, :cond_6

    move v0, v1

    .line 1280068
    goto :goto_1

    .line 1280069
    :cond_6
    invoke-virtual {v4, v2}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v2

    .line 1280070
    int-to-float v3, v3

    invoke-virtual {v4, v2, v3}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v2

    .line 1280071
    const-class v3, Landroid/text/style/ClickableSpan;

    invoke-interface {v0, v2, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ClickableSpan;

    .line 1280072
    array-length v0, v0

    if-eqz v0, :cond_7

    .line 1280073
    const/4 v0, 0x1

    goto :goto_1

    :cond_7
    move v0, v1

    .line 1280074
    goto :goto_1
.end method

.method private d(Landroid/view/View;)Z
    .locals 3

    .prologue
    .line 1280157
    invoke-virtual {p0, p1}, LX/7yk;->b(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1280158
    iget v1, p0, LX/7yk;->a:I

    iget v2, p0, LX/7yk;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;II)LX/7yn;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1280133
    if-eqz p1, :cond_0

    iget v0, p0, LX/7yk;->g:I

    if-gtz v0, :cond_1

    .line 1280134
    :cond_0
    :goto_0
    return-object v1

    .line 1280135
    :cond_1
    invoke-direct {p0}, LX/7yk;->a()V

    .line 1280136
    iput p2, p0, LX/7yk;->a:I

    .line 1280137
    iput p3, p0, LX/7yk;->b:I

    .line 1280138
    iget-object v0, p0, LX/7yk;->c:[I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    .line 1280139
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/7yk;->a(Landroid/view/ViewGroup;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1280140
    invoke-direct {p0}, LX/7yk;->a()V

    goto :goto_0

    .line 1280141
    :cond_2
    iget-object v0, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1280142
    packed-switch v0, :pswitch_data_0

    .line 1280143
    const v3, 0x7fffffff

    .line 1280144
    iget-object v0, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yn;

    .line 1280145
    iget v2, p0, LX/7yk;->a:I

    iget v5, p0, LX/7yk;->b:I

    invoke-virtual {v0, v2, v5}, LX/7yn;->a(II)I

    move-result v2

    .line 1280146
    if-ge v2, v3, :cond_4

    move v1, v2

    :goto_2
    move v3, v1

    move-object v1, v0

    .line 1280147
    goto :goto_1

    .line 1280148
    :pswitch_0
    invoke-direct {p0}, LX/7yk;->a()V

    goto :goto_0

    .line 1280149
    :pswitch_1
    iget-object v0, p0, LX/7yk;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yn;

    .line 1280150
    iget-object v1, p0, LX/7yk;->e:Landroid/view/ViewGroup;

    .line 1280151
    iput-object v1, v0, LX/7yn;->b:Landroid/view/ViewGroup;

    .line 1280152
    invoke-direct {p0}, LX/7yk;->a()V

    move-object v1, v0

    .line 1280153
    goto :goto_0

    .line 1280154
    :cond_3
    iget-object v0, p0, LX/7yk;->e:Landroid/view/ViewGroup;

    .line 1280155
    iput-object v0, v1, LX/7yn;->b:Landroid/view/ViewGroup;

    .line 1280156
    invoke-direct {p0}, LX/7yk;->a()V

    goto :goto_0

    :cond_4
    move-object v0, v1

    move v1, v3

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/view/View;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1280115
    invoke-virtual {p0, p1}, LX/7yk;->b(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    .line 1280116
    iget v2, p0, LX/7yk;->a:I

    iget v3, p0, LX/7yk;->b:I

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1280117
    const/4 v0, 0x1

    .line 1280118
    :cond_0
    :goto_0
    return v0

    .line 1280119
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 1280120
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v3

    .line 1280121
    iget v4, p0, LX/7yk;->g:I

    if-lt v2, v4, :cond_2

    iget v4, p0, LX/7yk;->g:I

    if-ge v3, v4, :cond_0

    .line 1280122
    :cond_2
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1280123
    iget v5, p0, LX/7yk;->g:I

    if-ge v2, v5, :cond_3

    .line 1280124
    iget v5, p0, LX/7yk;->g:I

    sub-int v2, v5, v2

    div-int/lit8 v2, v2, 0x2

    .line 1280125
    iget v5, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v2

    iput v5, v4, Landroid/graphics/Rect;->left:I

    .line 1280126
    iget v5, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v5

    iput v2, v4, Landroid/graphics/Rect;->right:I

    .line 1280127
    :cond_3
    iget v2, p0, LX/7yk;->g:I

    if-ge v3, v2, :cond_4

    .line 1280128
    iget v2, p0, LX/7yk;->g:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 1280129
    iget v3, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v2

    iput v3, v4, Landroid/graphics/Rect;->top:I

    .line 1280130
    iget v3, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v3

    iput v2, v4, Landroid/graphics/Rect;->bottom:I

    .line 1280131
    :cond_4
    iget v2, p0, LX/7yk;->a:I

    iget v3, p0, LX/7yk;->b:I

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1280132
    iget-object v2, p0, LX/7yk;->d:Ljava/util/List;

    new-instance v3, LX/7yn;

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-direct {v3, p1, v5, v4}, LX/7yn;-><init>(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;Z)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1280084
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1280085
    :goto_0
    return v0

    .line 1280086
    :cond_0
    if-nez p2, :cond_4

    .line 1280087
    instance-of v0, p1, LX/7yo;

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, LX/7yk;->d(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 1280088
    goto :goto_0

    .line 1280089
    :cond_1
    instance-of v0, p1, Landroid/widget/ListView;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, LX/7yk;->d(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1280090
    iget-object v0, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1280091
    iput-object p1, p0, LX/7yk;->e:Landroid/view/ViewGroup;

    .line 1280092
    check-cast p1, Landroid/widget/ListView;

    const/4 v2, 0x0

    .line 1280093
    move v1, v2

    :goto_1
    invoke-virtual {p1}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1280094
    invoke-virtual {p1, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1280095
    invoke-virtual {p0, v0}, LX/7yk;->b(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    .line 1280096
    iget v4, p0, LX/7yk;->a:I

    iget p2, p0, LX/7yk;->b:I

    invoke-virtual {v3, v4, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1280097
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_8

    .line 1280098
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, v2}, LX/7yk;->a(Landroid/view/ViewGroup;Z)Z

    move-result v2

    .line 1280099
    :cond_2
    :goto_2
    move v0, v2

    .line 1280100
    goto :goto_0

    .line 1280101
    :cond_3
    invoke-static {p0, p1}, LX/7yk;->c(LX/7yk;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    .line 1280102
    goto :goto_0

    .line 1280103
    :cond_4
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 1280104
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_3
    if-ltz v3, :cond_7

    .line 1280105
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1280106
    instance-of v4, v0, Landroid/view/ViewGroup;

    if-eqz v4, :cond_5

    .line 1280107
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, v1}, LX/7yk;->a(Landroid/view/ViewGroup;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 1280108
    goto :goto_0

    .line 1280109
    :cond_5
    invoke-static {p0, v0}, LX/7yk;->c(LX/7yk;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 1280110
    goto :goto_0

    .line 1280111
    :cond_6
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_3

    :cond_7
    move v0, v1

    .line 1280112
    goto :goto_0

    .line 1280113
    :cond_8
    invoke-static {p0, v0}, LX/7yk;->c(LX/7yk;Landroid/view/View;)Z

    move-result v2

    goto :goto_2

    .line 1280114
    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final b(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1280075
    iget-object v0, p0, LX/7yk;->i:Landroid/graphics/Rect;

    .line 1280076
    iget-object v1, p0, LX/7yk;->h:[I

    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1280077
    iget-object v1, p0, LX/7yk;->h:[I

    aget v1, v1, v3

    iget-object v2, p0, LX/7yk;->c:[I

    aget v2, v2, v3

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1280078
    iget-object v1, p0, LX/7yk;->h:[I

    aget v1, v1, v4

    iget-object v2, p0, LX/7yk;->c:[I

    aget v2, v2, v4

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1280079
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1280080
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    .line 1280081
    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v3

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1280082
    iget v1, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1280083
    return-object v0
.end method
