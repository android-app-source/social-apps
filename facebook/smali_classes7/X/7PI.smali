.class public LX/7PI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3DW;


# instance fields
.field public final a:LX/3DW;

.field public final b:LX/7PJ;

.field public final c:LX/7Pw;

.field public final d:Ljava/util/concurrent/ScheduledExecutorService;

.field public final e:Z

.field private final f:J


# direct methods
.method public constructor <init>(LX/7Pw;LX/3DW;Ljava/util/concurrent/ScheduledExecutorService;LX/7PJ;ZI)V
    .locals 2

    .prologue
    .line 1202753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202754
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Pw;

    iput-object v0, p0, LX/7PI;->c:LX/7Pw;

    .line 1202755
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3DW;

    iput-object v0, p0, LX/7PI;->a:LX/3DW;

    .line 1202756
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7PJ;

    iput-object v0, p0, LX/7PI;->b:LX/7PJ;

    .line 1202757
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v0, p0, LX/7PI;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1202758
    iput-boolean p5, p0, LX/7PI;->e:Z

    .line 1202759
    int-to-long v0, p6

    iput-wide v0, p0, LX/7PI;->f:J

    .line 1202760
    return-void
.end method

.method public static a$redex0(LX/7PI;JJ)J
    .locals 5

    .prologue
    .line 1202761
    iget-wide v0, p0, LX/7PI;->f:J

    add-long/2addr v0, p1

    .line 1202762
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-lez v2, :cond_0

    cmp-long v2, v0, p3

    if-gez v2, :cond_1

    :cond_0
    move-wide p3, v0

    :cond_1
    return-wide p3
.end method


# virtual methods
.method public final a(JJLX/3Di;)V
    .locals 7

    .prologue
    .line 1202763
    new-instance v0, LX/7PH;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/7PH;-><init>(LX/7PI;JJLX/3Di;)V

    invoke-static {v0}, LX/7PH;->a$redex0(LX/7PH;)V

    .line 1202764
    return-void
.end method
