.class public final LX/8EN;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1313578
    const-class v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    const v0, 0x9ae97e2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "PageAdminCallToActionById"

    const-string v6, "f7136132d6f0ec68b97466ad05805f62"

    const-string v7, "page_call_to_action"

    const-string v8, "10155207368596729"

    const-string v9, "10155259089466729"

    .line 1313579
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1313580
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1313581
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1313586
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1313587
    sparse-switch v0, :sswitch_data_0

    .line 1313588
    :goto_0
    return-object p1

    .line 1313589
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1313590
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1313591
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1313592
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x76efa4c9 -> :sswitch_3
        -0x71e86c6d -> :sswitch_0
        -0x50800a56 -> :sswitch_2
        0x78326898 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1313582
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1313583
    :goto_1
    return v0

    .line 1313584
    :pswitch_0
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1313585
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x33
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
