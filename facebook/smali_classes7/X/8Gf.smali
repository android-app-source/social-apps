.class public LX/8Gf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1319880
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/text/TextPaint;Landroid/text/DynamicLayout;)I
    .locals 9

    .prologue
    .line 1319881
    invoke-virtual {p1}, Landroid/text/DynamicLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    .line 1319882
    const/4 v1, 0x0

    .line 1319883
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/text/DynamicLayout;->getLineCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1319884
    invoke-virtual {p1, v0}, Landroid/text/DynamicLayout;->getLineStart(I)I

    move-result v2

    .line 1319885
    invoke-virtual {p1, v0}, Landroid/text/DynamicLayout;->getLineEnd(I)I

    move-result v4

    .line 1319886
    invoke-interface {v3, v2, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1319887
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    new-array v6, v4, [F

    .line 1319888
    invoke-virtual {p0, v2, v6}, Landroid/text/TextPaint;->getTextWidths(Ljava/lang/String;[F)I

    .line 1319889
    const/4 v5, 0x0

    .line 1319890
    array-length v7, v6

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v7, :cond_0

    aget v8, v6, v4

    .line 1319891
    add-float/2addr v5, v8

    .line 1319892
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1319893
    :cond_0
    move v2, v5

    .line 1319894
    cmpg-float v4, v1, v2

    if-gez v4, :cond_1

    move v1, v2

    .line 1319895
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1319896
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method
