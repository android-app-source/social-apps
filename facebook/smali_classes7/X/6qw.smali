.class public final enum LX/6qw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6qw;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6qw;

.field public static final enum BROWSER_EXTENSION:LX/6qw;

.field public static final enum EVENT_TICKETING:LX/6qw;

.field public static final enum FUNDRAISER_DONATION:LX/6qw;

.field public static final enum INSTANT_WORKFLOWS:LX/6qw;

.field public static final enum INVOICE_CREATION:LX/6qw;

.field public static final enum JS_BASED:LX/6qw;

.field public static final enum M:LX/6qw;

.field public static final enum MESSENGER_COMMERCE:LX/6qw;

.field public static final enum OMNI_M:LX/6qw;

.field public static final enum PAGES_COMMERCE:LX/6qw;

.field public static final enum PAYMENTS_FLOW_SAMPLE:LX/6qw;

.field public static final enum SIMPLE:LX/6qw;

.field public static final enum TIP_JAR:LX/6qw;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1151327
    new-instance v0, LX/6qw;

    const-string v1, "EVENT_TICKETING"

    invoke-direct {v0, v1, v3}, LX/6qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qw;->EVENT_TICKETING:LX/6qw;

    .line 1151328
    new-instance v0, LX/6qw;

    const-string v1, "INSTANT_WORKFLOWS"

    invoke-direct {v0, v1, v4}, LX/6qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qw;->INSTANT_WORKFLOWS:LX/6qw;

    .line 1151329
    new-instance v0, LX/6qw;

    const-string v1, "M"

    invoke-direct {v0, v1, v5}, LX/6qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qw;->M:LX/6qw;

    .line 1151330
    new-instance v0, LX/6qw;

    const-string v1, "OMNI_M"

    invoke-direct {v0, v1, v6}, LX/6qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qw;->OMNI_M:LX/6qw;

    .line 1151331
    new-instance v0, LX/6qw;

    const-string v1, "MESSENGER_COMMERCE"

    invoke-direct {v0, v1, v7}, LX/6qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qw;->MESSENGER_COMMERCE:LX/6qw;

    .line 1151332
    new-instance v0, LX/6qw;

    const-string v1, "BROWSER_EXTENSION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qw;->BROWSER_EXTENSION:LX/6qw;

    .line 1151333
    new-instance v0, LX/6qw;

    const-string v1, "INVOICE_CREATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/6qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qw;->INVOICE_CREATION:LX/6qw;

    .line 1151334
    new-instance v0, LX/6qw;

    const-string v1, "PAGES_COMMERCE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/6qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qw;->PAGES_COMMERCE:LX/6qw;

    .line 1151335
    new-instance v0, LX/6qw;

    const-string v1, "PAYMENTS_FLOW_SAMPLE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/6qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qw;->PAYMENTS_FLOW_SAMPLE:LX/6qw;

    .line 1151336
    new-instance v0, LX/6qw;

    const-string v1, "SIMPLE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/6qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qw;->SIMPLE:LX/6qw;

    .line 1151337
    new-instance v0, LX/6qw;

    const-string v1, "TIP_JAR"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/6qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qw;->TIP_JAR:LX/6qw;

    .line 1151338
    new-instance v0, LX/6qw;

    const-string v1, "FUNDRAISER_DONATION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/6qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qw;->FUNDRAISER_DONATION:LX/6qw;

    .line 1151339
    new-instance v0, LX/6qw;

    const-string v1, "JS_BASED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/6qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qw;->JS_BASED:LX/6qw;

    .line 1151340
    const/16 v0, 0xd

    new-array v0, v0, [LX/6qw;

    sget-object v1, LX/6qw;->EVENT_TICKETING:LX/6qw;

    aput-object v1, v0, v3

    sget-object v1, LX/6qw;->INSTANT_WORKFLOWS:LX/6qw;

    aput-object v1, v0, v4

    sget-object v1, LX/6qw;->M:LX/6qw;

    aput-object v1, v0, v5

    sget-object v1, LX/6qw;->OMNI_M:LX/6qw;

    aput-object v1, v0, v6

    sget-object v1, LX/6qw;->MESSENGER_COMMERCE:LX/6qw;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6qw;->BROWSER_EXTENSION:LX/6qw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6qw;->INVOICE_CREATION:LX/6qw;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6qw;->PAGES_COMMERCE:LX/6qw;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6qw;->PAYMENTS_FLOW_SAMPLE:LX/6qw;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6qw;->SIMPLE:LX/6qw;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6qw;->TIP_JAR:LX/6qw;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/6qw;->FUNDRAISER_DONATION:LX/6qw;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/6qw;->JS_BASED:LX/6qw;

    aput-object v2, v0, v1

    sput-object v0, LX/6qw;->$VALUES:[LX/6qw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1151326
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6qw;
    .locals 1

    .prologue
    .line 1151325
    const-class v0, LX/6qw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6qw;

    return-object v0
.end method

.method public static values()[LX/6qw;
    .locals 1

    .prologue
    .line 1151324
    sget-object v0, LX/6qw;->$VALUES:[LX/6qw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6qw;

    return-object v0
.end method
