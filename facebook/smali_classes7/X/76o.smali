.class public final LX/76o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:[Ljava/lang/CharSequence;

.field public final synthetic b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public final synthetic c:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;[Ljava/lang/CharSequence;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)V
    .locals 0

    .prologue
    .line 1171270
    iput-object p1, p0, LX/76o;->c:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    iput-object p2, p0, LX/76o;->a:[Ljava/lang/CharSequence;

    iput-object p3, p0, LX/76o;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1171271
    iget-object v0, p0, LX/76o;->c:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/76o;->a:[Ljava/lang/CharSequence;

    aget-object v1, v1, p2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1171272
    iget-object v0, p0, LX/76o;->c:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    iget-object v0, v0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/76o;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-static {v1}, LX/2fw;->c(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1, p2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1171273
    iget-object v0, p0, LX/76o;->c:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    .line 1171274
    invoke-static {v0}, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->a$redex0(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;)V

    .line 1171275
    return-void
.end method
