.class public LX/6lM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;

.field public final b:Landroid/graphics/Paint;

.field public final c:Landroid/text/TextPaint;

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(LX/6lL;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1142663
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142664
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/6lM;->b:Landroid/graphics/Paint;

    .line 1142665
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, LX/6lM;->c:Landroid/text/TextPaint;

    .line 1142666
    iget-object v0, p1, LX/6lL;->a:Landroid/content/res/Resources;

    iput-object v0, p0, LX/6lM;->a:Landroid/content/res/Resources;

    .line 1142667
    iget v0, p1, LX/6lL;->b:I

    iput v0, p0, LX/6lM;->d:I

    .line 1142668
    iget-object v0, p0, LX/6lM;->b:Landroid/graphics/Paint;

    iget v1, p1, LX/6lL;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1142669
    iget-object v0, p0, LX/6lM;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1142670
    iget-object v0, p0, LX/6lM;->c:Landroid/text/TextPaint;

    iget v1, p1, LX/6lL;->d:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 1142671
    iget-object v0, p0, LX/6lM;->c:Landroid/text/TextPaint;

    iget v1, p1, LX/6lL;->e:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1142672
    iget-object v0, p0, LX/6lM;->c:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1142673
    iget-object v0, p0, LX/6lM;->c:Landroid/text/TextPaint;

    iget-object v1, p1, LX/6lL;->f:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1142674
    iget-object v0, p0, LX/6lM;->c:Landroid/text/TextPaint;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 1142675
    return-void
.end method
