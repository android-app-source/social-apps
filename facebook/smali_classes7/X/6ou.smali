.class public final LX/6ou;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1148666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1148664
    new-instance v0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;

    invoke-direct {v0, p1}, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1148665
    new-array v0, p1, [Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;

    return-object v0
.end method
