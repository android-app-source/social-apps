.class public final LX/860;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1295965
    const/4 v13, 0x0

    .line 1295966
    const/4 v12, 0x0

    .line 1295967
    const/4 v11, 0x0

    .line 1295968
    const/4 v10, 0x0

    .line 1295969
    const/4 v9, 0x0

    .line 1295970
    const/4 v8, 0x0

    .line 1295971
    const/4 v7, 0x0

    .line 1295972
    const/4 v6, 0x0

    .line 1295973
    const/4 v5, 0x0

    .line 1295974
    const/4 v4, 0x0

    .line 1295975
    const/4 v3, 0x0

    .line 1295976
    const/4 v2, 0x0

    .line 1295977
    const/4 v1, 0x0

    .line 1295978
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 1295979
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1295980
    const/4 v1, 0x0

    .line 1295981
    :goto_0
    return v1

    .line 1295982
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1295983
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_b

    .line 1295984
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 1295985
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1295986
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 1295987
    const-string v15, "__type__"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    const-string v15, "__typename"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1295988
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v13

    goto :goto_1

    .line 1295989
    :cond_3
    const-string v15, "can_viewer_message"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1295990
    const/4 v4, 0x1

    .line 1295991
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 1295992
    :cond_4
    const-string v15, "can_viewer_poke"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1295993
    const/4 v3, 0x1

    .line 1295994
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 1295995
    :cond_5
    const-string v15, "can_viewer_post"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1295996
    const/4 v2, 0x1

    .line 1295997
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 1295998
    :cond_6
    const-string v15, "friendship_status"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 1295999
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 1296000
    :cond_7
    const-string v15, "id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 1296001
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 1296002
    :cond_8
    const-string v15, "notification_status"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 1296003
    const/4 v1, 0x1

    .line 1296004
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto/16 :goto_1

    .line 1296005
    :cond_9
    const-string v15, "secondary_subscribe_status"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 1296006
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto/16 :goto_1

    .line 1296007
    :cond_a
    const-string v15, "subscribe_status"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1296008
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto/16 :goto_1

    .line 1296009
    :cond_b
    const/16 v14, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 1296010
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1296011
    if-eqz v4, :cond_c

    .line 1296012
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->a(IZ)V

    .line 1296013
    :cond_c
    if-eqz v3, :cond_d

    .line 1296014
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->a(IZ)V

    .line 1296015
    :cond_d
    if-eqz v2, :cond_e

    .line 1296016
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->a(IZ)V

    .line 1296017
    :cond_e
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1296018
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1296019
    if-eqz v1, :cond_f

    .line 1296020
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 1296021
    :cond_f
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1296022
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1296023
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x7

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 1296024
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1296025
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1296026
    if-eqz v0, :cond_0

    .line 1296027
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1296028
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1296029
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1296030
    if-eqz v0, :cond_1

    .line 1296031
    const-string v1, "can_viewer_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1296032
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1296033
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1296034
    if-eqz v0, :cond_2

    .line 1296035
    const-string v1, "can_viewer_poke"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1296036
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1296037
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1296038
    if-eqz v0, :cond_3

    .line 1296039
    const-string v1, "can_viewer_post"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1296040
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1296041
    :cond_3
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1296042
    if-eqz v0, :cond_4

    .line 1296043
    const-string v0, "friendship_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1296044
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1296045
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1296046
    if-eqz v0, :cond_5

    .line 1296047
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1296048
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1296049
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1296050
    if-eqz v0, :cond_6

    .line 1296051
    const-string v1, "notification_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1296052
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1296053
    :cond_6
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1296054
    if-eqz v0, :cond_7

    .line 1296055
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1296056
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1296057
    :cond_7
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1296058
    if-eqz v0, :cond_8

    .line 1296059
    const-string v0, "subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1296060
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1296061
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1296062
    return-void
.end method
