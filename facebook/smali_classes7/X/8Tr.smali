.class public LX/8Tr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/8TD;

.field public final c:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/8TD;LX/1Ck;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1349194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1349195
    iput-object p1, p0, LX/8Tr;->a:LX/0tX;

    .line 1349196
    iput-object p2, p0, LX/8Tr;->b:LX/8TD;

    .line 1349197
    iput-object p3, p0, LX/8Tr;->c:LX/1Ck;

    .line 1349198
    return-void
.end method

.method public static synthetic a(LX/8Tr;Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel;)J
    .locals 8

    .prologue
    .line 1349199
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1349200
    if-nez p1, :cond_0

    move-wide v2, v4

    .line 1349201
    :goto_0
    move-wide v0, v2

    .line 1349202
    return-wide v0

    .line 1349203
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel;->a()LX/2uF;

    move-result-object v6

    .line 1349204
    if-eqz v6, :cond_2

    invoke-virtual {v6}, LX/39O;->c()I

    move-result v2

    if-lez v2, :cond_2

    .line 1349205
    invoke-virtual {v6, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v7, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 1349206
    invoke-virtual {v7, v2, v3}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_3

    .line 1349207
    invoke-virtual {v6, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v4, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v4, v2, v3}, LX/15i;->g(II)I

    move-result v2

    invoke-virtual {v4, v2, v3}, LX/15i;->j(II)I

    move-result v2

    int-to-long v2, v2

    goto :goto_0

    :cond_1
    move v2, v3

    .line 1349208
    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_1

    :cond_3
    move-wide v2, v4

    .line 1349209
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/8Tr;
    .locals 6

    .prologue
    .line 1349210
    const-class v1, LX/8Tr;

    monitor-enter v1

    .line 1349211
    :try_start_0
    sget-object v0, LX/8Tr;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1349212
    sput-object v2, LX/8Tr;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1349213
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1349214
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1349215
    new-instance p0, LX/8Tr;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/8TD;->a(LX/0QB;)LX/8TD;

    move-result-object v4

    check-cast v4, LX/8TD;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-direct {p0, v3, v4, v5}, LX/8Tr;-><init>(LX/0tX;LX/8TD;LX/1Ck;)V

    .line 1349216
    move-object v0, p0

    .line 1349217
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1349218
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8Tr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1349219
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1349220
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
