.class public final enum LX/7Az;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Az;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Az;

.field public static final enum BOOTSTRAP:LX/7Az;

.field public static final enum CACHEWARMER:LX/7Az;

.field public static final enum NULLSTATE:LX/7Az;

.field public static final enum SERP:LX/7Az;

.field public static final enum TYPEAHEAD:LX/7Az;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1177653
    new-instance v0, LX/7Az;

    const-string v1, "TYPEAHEAD"

    invoke-direct {v0, v1, v2}, LX/7Az;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Az;->TYPEAHEAD:LX/7Az;

    .line 1177654
    new-instance v0, LX/7Az;

    const-string v1, "SERP"

    invoke-direct {v0, v1, v3}, LX/7Az;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Az;->SERP:LX/7Az;

    .line 1177655
    new-instance v0, LX/7Az;

    const-string v1, "NULLSTATE"

    invoke-direct {v0, v1, v4}, LX/7Az;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Az;->NULLSTATE:LX/7Az;

    .line 1177656
    new-instance v0, LX/7Az;

    const-string v1, "BOOTSTRAP"

    invoke-direct {v0, v1, v5}, LX/7Az;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Az;->BOOTSTRAP:LX/7Az;

    .line 1177657
    new-instance v0, LX/7Az;

    const-string v1, "CACHEWARMER"

    invoke-direct {v0, v1, v6}, LX/7Az;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Az;->CACHEWARMER:LX/7Az;

    .line 1177658
    const/4 v0, 0x5

    new-array v0, v0, [LX/7Az;

    sget-object v1, LX/7Az;->TYPEAHEAD:LX/7Az;

    aput-object v1, v0, v2

    sget-object v1, LX/7Az;->SERP:LX/7Az;

    aput-object v1, v0, v3

    sget-object v1, LX/7Az;->NULLSTATE:LX/7Az;

    aput-object v1, v0, v4

    sget-object v1, LX/7Az;->BOOTSTRAP:LX/7Az;

    aput-object v1, v0, v5

    sget-object v1, LX/7Az;->CACHEWARMER:LX/7Az;

    aput-object v1, v0, v6

    sput-object v0, LX/7Az;->$VALUES:[LX/7Az;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1177659
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Az;
    .locals 1

    .prologue
    .line 1177660
    const-class v0, LX/7Az;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Az;

    return-object v0
.end method

.method public static values()[LX/7Az;
    .locals 1

    .prologue
    .line 1177661
    sget-object v0, LX/7Az;->$VALUES:[LX/7Az;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Az;

    return-object v0
.end method
