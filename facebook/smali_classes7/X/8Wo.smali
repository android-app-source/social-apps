.class public LX/8Wo;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:LX/8TS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/8Ws;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/8WZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/8Wr;

.field public e:LX/8So;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1354407
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/8Wo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1354408
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1354409
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1354410
    const-class p1, LX/8Wo;

    invoke-static {p1, p0}, LX/8Wo;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1354411
    invoke-virtual {p0}, LX/8Wo;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f030261

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1354412
    invoke-virtual {p0}, LX/8Wo;->getContext()Landroid/content/Context;

    .line 1354413
    const p1, 0x7f0d08fb

    invoke-virtual {p0, p1}, LX/8Wo;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/support/v7/widget/RecyclerView;

    .line 1354414
    new-instance p2, LX/62V;

    invoke-virtual {p0}, LX/8Wo;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {p2, p3}, LX/62V;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, p2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1354415
    iget-object p2, p0, LX/8Wo;->b:LX/8Ws;

    invoke-virtual {p2, p1}, LX/8Ws;->a(Landroid/support/v7/widget/RecyclerView;)LX/8Wr;

    move-result-object p1

    iput-object p1, p0, LX/8Wo;->d:LX/8Wr;

    .line 1354416
    iget-object p1, p0, LX/8Wo;->d:LX/8Wr;

    new-instance p2, LX/8Wm;

    invoke-direct {p2, p0}, LX/8Wm;-><init>(LX/8Wo;)V

    .line 1354417
    iput-object p2, p1, LX/8Wr;->d:LX/8Wj;

    .line 1354418
    const p1, 0x7f0d08f7

    invoke-virtual {p0, p1}, LX/8Wo;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 1354419
    new-instance p2, LX/8Wn;

    invoke-direct {p2, p0}, LX/8Wn;-><init>(LX/8Wo;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1354420
    iget-object p1, p0, LX/8Wo;->c:LX/8WZ;

    const p2, 0x7f0d08fa

    invoke-virtual {p0, p2}, LX/8Wo;->findViewById(I)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/8WZ;->a(Landroid/view/View;)V

    .line 1354421
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/8Wo;

    invoke-static {p0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v1

    check-cast v1, LX/8TS;

    const-class v2, LX/8Ws;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/8Ws;

    invoke-static {p0}, LX/8WZ;->a(LX/0QB;)LX/8WZ;

    move-result-object p0

    check-cast p0, LX/8WZ;

    iput-object v1, p1, LX/8Wo;->a:LX/8TS;

    iput-object v2, p1, LX/8Wo;->b:LX/8Ws;

    iput-object p0, p1, LX/8Wo;->c:LX/8WZ;

    return-void
.end method
