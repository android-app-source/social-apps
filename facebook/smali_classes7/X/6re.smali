.class public final enum LX/6re;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6LU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6re;",
        ">;",
        "LX/6LU",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6re;

.field public static final enum FIXED_AMOUNT:LX/6re;

.field public static final enum JS_UPDATE_CHECKOUT:LX/6re;

.field public static final enum PLATFORM_CONTEXT:LX/6re;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum UPDATE_CHECKOUT_API:LX/6re;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1152505
    new-instance v0, LX/6re;

    const-string v1, "FIXED_AMOUNT"

    invoke-direct {v0, v1, v2}, LX/6re;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6re;->FIXED_AMOUNT:LX/6re;

    .line 1152506
    new-instance v0, LX/6re;

    const-string v1, "PLATFORM_CONTEXT"

    invoke-direct {v0, v1, v3}, LX/6re;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6re;->PLATFORM_CONTEXT:LX/6re;

    .line 1152507
    new-instance v0, LX/6re;

    const-string v1, "UPDATE_CHECKOUT_API"

    invoke-direct {v0, v1, v4}, LX/6re;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6re;->UPDATE_CHECKOUT_API:LX/6re;

    .line 1152508
    new-instance v0, LX/6re;

    const-string v1, "JS_UPDATE_CHECKOUT"

    invoke-direct {v0, v1, v5}, LX/6re;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6re;->JS_UPDATE_CHECKOUT:LX/6re;

    .line 1152509
    const/4 v0, 0x4

    new-array v0, v0, [LX/6re;

    sget-object v1, LX/6re;->FIXED_AMOUNT:LX/6re;

    aput-object v1, v0, v2

    sget-object v1, LX/6re;->PLATFORM_CONTEXT:LX/6re;

    aput-object v1, v0, v3

    sget-object v1, LX/6re;->UPDATE_CHECKOUT_API:LX/6re;

    aput-object v1, v0, v4

    sget-object v1, LX/6re;->JS_UPDATE_CHECKOUT:LX/6re;

    aput-object v1, v0, v5

    sput-object v0, LX/6re;->$VALUES:[LX/6re;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1152510
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forValue(Ljava/lang/String;)LX/6re;
    .locals 1

    .prologue
    .line 1152511
    invoke-static {}, LX/6re;->values()[LX/6re;

    move-result-object v0

    invoke-static {v0, p0}, LX/6LV;->a([LX/6LU;Ljava/lang/Object;)LX/6LU;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6re;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6re;
    .locals 1

    .prologue
    .line 1152504
    const-class v0, LX/6re;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6re;

    return-object v0
.end method

.method public static values()[LX/6re;
    .locals 1

    .prologue
    .line 1152503
    sget-object v0, LX/6re;->$VALUES:[LX/6re;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6re;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1152500
    invoke-virtual {p0}, LX/6re;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1152502
    invoke-virtual {p0}, LX/6re;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1152501
    invoke-virtual {p0}, LX/6re;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
