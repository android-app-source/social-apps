.class public final LX/6qo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6qn;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/checkout/CheckoutFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V
    .locals 0

    .prologue
    .line 1151114
    iput-object p1, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)V
    .locals 2

    .prologue
    .line 1151112
    iget-object v0, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    .line 1151113
    return-void
.end method

.method public final a(Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V
    .locals 2

    .prologue
    .line 1151098
    iget-object v0, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V

    .line 1151099
    return-void
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V
    .locals 2

    .prologue
    .line 1151110
    iget-object v0, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V

    .line 1151111
    return-void
.end method

.method public final a(Lcom/facebook/payments/shipping/model/MailingAddress;)V
    .locals 2

    .prologue
    .line 1151108
    iget-object v0, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/shipping/model/MailingAddress;)V

    .line 1151109
    return-void
.end method

.method public final a(Lcom/facebook/payments/shipping/model/ShippingOption;)V
    .locals 2

    .prologue
    .line 1151106
    iget-object v0, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/shipping/model/ShippingOption;)V

    .line 1151107
    return-void
.end method

.method public final a(Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 2

    .prologue
    .line 1151104
    iget-object v0, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1, p2}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)V

    .line 1151105
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1151102
    iget-object v0, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1, p2}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;LX/0Px;)V

    .line 1151103
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1151100
    iget-object v0, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, LX/6qo;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/util/List;)V

    .line 1151101
    return-void
.end method
