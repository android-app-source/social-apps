.class public final LX/7kv;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/7kv;


# instance fields
.field public b:Lcom/facebook/ipc/media/MediaItem;

.field public c:Landroid/net/Uri;

.field public d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I

.field public i:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1232707
    new-instance v0, LX/7kv;

    sget-object v1, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-direct {v0, v2, v2, v1}, LX/7kv;-><init>(Lcom/facebook/ipc/media/MediaItem;Landroid/net/Uri;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    sput-object v0, LX/7kv;->a:LX/7kv;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1232708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/facebook/ipc/media/MediaItem;Landroid/net/Uri;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1232709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232710
    iput-object p1, p0, LX/7kv;->b:Lcom/facebook/ipc/media/MediaItem;

    .line 1232711
    iput-object p2, p0, LX/7kv;->c:Landroid/net/Uri;

    .line 1232712
    iput-object p3, p0, LX/7kv;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1232713
    const/4 v0, -0x1

    iput v0, p0, LX/7kv;->h:I

    .line 1232714
    iput-object v1, p0, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1232715
    iput-object v1, p0, LX/7kv;->f:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1232716
    iput-object v1, p0, LX/7kv;->g:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    .line 1232717
    const-string v0, "standard"

    iput-object v0, p0, LX/7kv;->i:Ljava/lang/String;

    .line 1232718
    return-void
.end method

.method public static a(Landroid/net/Uri;LX/74n;)LX/7kv;
    .locals 3
    .param p0    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/74n;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1232682
    if-nez p0, :cond_0

    .line 1232683
    sget-object v0, LX/7kv;->a:LX/7kv;

    .line 1232684
    :goto_0
    return-object v0

    .line 1232685
    :cond_0
    invoke-static {p0}, LX/74n;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1232686
    new-instance v0, LX/7kv;

    const/4 v1, 0x0

    sget-object v2, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-direct {v0, v1, p0, v2}, LX/7kv;-><init>(Lcom/facebook/ipc/media/MediaItem;Landroid/net/Uri;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    goto :goto_0

    .line 1232687
    :cond_1
    sget-object v0, LX/74j;->REMOTE_MEDIA:LX/74j;

    invoke-virtual {p1, p0, v0}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1232688
    if-eqz v0, :cond_2

    .line 1232689
    invoke-static {v0}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;

    move-result-object v0

    goto :goto_0

    .line 1232690
    :cond_2
    sget-object v0, LX/7kv;->a:LX/7kv;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;
    .locals 4
    .param p0    # Lcom/facebook/composer/attachments/ComposerAttachment;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1232691
    new-instance v0, LX/7kv;

    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCaption:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-direct {v0, v1, v2, v3}, LX/7kv;-><init>(Lcom/facebook/ipc/media/MediaItem;Landroid/net/Uri;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->i()I

    move-result v1

    .line 1232692
    iput v1, v0, LX/7kv;->h:I

    .line 1232693
    move-object v0, v0

    .line 1232694
    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCreativeEditingData:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1232695
    iput-object v1, v0, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1232696
    move-object v0, v0

    .line 1232697
    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoCreativeEditingData:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1232698
    iput-object v1, v0, LX/7kv;->f:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1232699
    move-object v0, v0

    .line 1232700
    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoTaggingInfo:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    .line 1232701
    iput-object v1, v0, LX/7kv;->g:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    .line 1232702
    move-object v0, v0

    .line 1232703
    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoUploadQuality:Ljava/lang/String;

    .line 1232704
    iput-object v1, v0, LX/7kv;->i:Ljava/lang/String;

    .line 1232705
    move-object v0, v0

    .line 1232706
    return-object v0
.end method

.method public static a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;
    .locals 3
    .param p0    # Lcom/facebook/ipc/media/MediaItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1232681
    if-eqz p0, :cond_0

    new-instance v0, LX/7kv;

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-direct {v0, p0, v1, v2}, LX/7kv;-><init>(Lcom/facebook/ipc/media/MediaItem;Landroid/net/Uri;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/7kv;->a:LX/7kv;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/7kv;
    .locals 0

    .prologue
    .line 1232679
    iput-object p1, p0, LX/7kv;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1232680
    return-object p0
.end method

.method public final a(Lcom/facebook/ipc/media/MediaItem;LX/74n;)LX/7kv;
    .locals 1
    .param p2    # LX/74n;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1232674
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1232675
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/74n;->a(Landroid/net/Uri;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1232676
    iput-object p1, p0, LX/7kv;->b:Lcom/facebook/ipc/media/MediaItem;

    .line 1232677
    iget-object v0, p0, LX/7kv;->b:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/7kv;->c:Landroid/net/Uri;

    .line 1232678
    return-object p0
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/7kv;
    .locals 0

    .prologue
    .line 1232672
    iput-object p1, p0, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1232673
    return-object p0
.end method

.method public final a()Lcom/facebook/composer/attachments/ComposerAttachment;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1232669
    sget-object v0, LX/7kv;->a:LX/7kv;

    if-ne p0, v0, :cond_0

    .line 1232670
    const/4 v0, 0x0

    .line 1232671
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-direct {v0, p0}, Lcom/facebook/composer/attachments/ComposerAttachment;-><init>(LX/7kv;)V

    goto :goto_0
.end method
