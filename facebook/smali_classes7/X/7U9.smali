.class public LX/7U9;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:Landroid/graphics/Bitmap;

.field public c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1211631
    const-class v0, LX/7U9;

    sput-object v0, LX/7U9;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1211632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1211633
    iput-object p1, p0, LX/7U9;->b:Landroid/graphics/Bitmap;

    .line 1211634
    const/4 v0, 0x0

    iput v0, p0, LX/7U9;->c:I

    .line 1211635
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;I)V
    .locals 1

    .prologue
    .line 1211636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1211637
    iput-object p1, p0, LX/7U9;->b:Landroid/graphics/Bitmap;

    .line 1211638
    rem-int/lit16 v0, p2, 0x168

    iput v0, p0, LX/7U9;->c:I

    .line 1211639
    return-void
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 1211640
    iget v0, p0, LX/7U9;->c:I

    div-int/lit8 v0, v0, 0x5a

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c()Landroid/graphics/Matrix;
    .locals 3

    .prologue
    .line 1211641
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1211642
    iget v1, p0, LX/7U9;->c:I

    if-eqz v1, :cond_0

    .line 1211643
    iget-object v1, p0, LX/7U9;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 1211644
    iget-object v2, p0, LX/7U9;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 1211645
    neg-int v1, v1

    int-to-float v1, v1

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 1211646
    iget v1, p0, LX/7U9;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1211647
    invoke-virtual {p0}, LX/7U9;->e()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, LX/7U9;->d()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1211648
    :cond_0
    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1211649
    invoke-direct {p0}, LX/7U9;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211650
    iget-object v0, p0, LX/7U9;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 1211651
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/7U9;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1211652
    invoke-direct {p0}, LX/7U9;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211653
    iget-object v0, p0, LX/7U9;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 1211654
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/7U9;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    goto :goto_0
.end method
