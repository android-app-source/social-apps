.class public final LX/6yE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V
    .locals 0

    .prologue
    .line 1159630
    iput-object p1, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1159600
    iget-object v0, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    if-eqz v0, :cond_0

    .line 1159601
    iget-object v0, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    invoke-virtual {v0}, LX/6y3;->a()V

    .line 1159602
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1159603
    iget-object v0, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v0, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->a:LX/6z7;

    iget-object v4, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-static {v4}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)LX/6z9;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/6z7;->c(LX/6z8;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    .line 1159604
    iget-object v0, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->i:LX/6yZ;

    iget-object v3, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v3}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v0, v3}, LX/6yZ;->d(LX/6yO;)LX/6yT;

    move-result-object v0

    iget-object v3, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0, v3, v2}, LX/6yT;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1159605
    iget-object v0, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    .line 1159606
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1159607
    iget-boolean v4, v3, Lcom/facebook/payments/ui/PaymentFormEditTextView;->b:Z

    move v3, v4

    .line 1159608
    if-nez v3, :cond_1

    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->a:LX/6z7;

    invoke-static {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)LX/6z9;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/6z7;->c(LX/6z8;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1159609
    :cond_1
    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->n:Z

    .line 1159610
    :cond_2
    :goto_1
    iget-object v0, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->a:LX/6z7;

    iget-object v3, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-static {v3}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)LX/6z9;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/6z7;->a(LX/6z8;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1159611
    iget-object v0, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->k:LX/73a;

    iget-object v1, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->s:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v1}, LX/73a;->a(Landroid/view/View;)V

    .line 1159612
    :cond_3
    :goto_2
    iget-object v0, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    iget-object v1, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/6y3;->a(Z)V

    .line 1159613
    return-void

    :cond_4
    move v0, v1

    .line 1159614
    goto :goto_0

    .line 1159615
    :cond_5
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 1159616
    iget-object v0, p0, LX/6yE;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    goto :goto_2

    .line 1159617
    :cond_6
    iget-boolean v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->n:Z

    if-nez v3, :cond_7

    .line 1159618
    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->n:Z

    goto :goto_1

    .line 1159619
    :cond_7
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    const-string v4, "unsupported_association_dialog"

    invoke-virtual {v3, v4}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    .line 1159620
    if-eqz v3, :cond_8

    .line 1159621
    iget-object v4, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->B:LX/6wv;

    .line 1159622
    iput-object v4, v3, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 1159623
    goto :goto_1

    .line 1159624
    :cond_8
    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->i:LX/6yZ;

    iget-object v4, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v4}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v3, v4}, LX/6yZ;->c(LX/6yO;)LX/6y0;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v3, v4}, LX/6y0;->b(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v3

    .line 1159625
    if-eqz v3, :cond_2

    .line 1159626
    invoke-static {v3}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->b(Lcom/facebook/messaging/dialog/ConfirmActionParams;)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    move-result-object v3

    .line 1159627
    iget-object v4, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->B:LX/6wv;

    .line 1159628
    iput-object v4, v3, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 1159629
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v4

    const-string p1, "unsupported_association_dialog"

    invoke-virtual {v3, v4, p1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1159599
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1159598
    return-void
.end method
