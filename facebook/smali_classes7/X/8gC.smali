.class public final LX/8gC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1386657
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1386658
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1386659
    :goto_0
    return v1

    .line 1386660
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1386661
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1386662
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1386663
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1386664
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1386665
    const-string v4, "highlight_snippets"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1386666
    invoke-static {p0, p1}, LX/8gJ;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1386667
    :cond_2
    const-string v4, "match_words"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1386668
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1386669
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1386670
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1386671
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1386672
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1386673
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1386674
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1386675
    if-eqz v0, :cond_0

    .line 1386676
    const-string v1, "highlight_snippets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1386677
    invoke-static {p0, v0, p2, p3}, LX/8gJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1386678
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1386679
    if-eqz v0, :cond_1

    .line 1386680
    const-string v0, "match_words"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1386681
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1386682
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1386683
    return-void
.end method
