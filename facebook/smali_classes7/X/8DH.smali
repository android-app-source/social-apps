.class public LX/8DH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1312436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1312437
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 2

    .prologue
    .line 1312438
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "resetNuxStatus"

    .line 1312439
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1312440
    move-object v0, v0

    .line 1312441
    const-string v1, "POST"

    .line 1312442
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1312443
    move-object v0, v0

    .line 1312444
    const-string v1, "me/nux_wizard_user_state"

    .line 1312445
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 1312446
    move-object v0, v0

    .line 1312447
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1312448
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1312449
    move-object v0, v0

    .line 1312450
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1312451
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1312452
    move-object v0, v0

    .line 1312453
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1312454
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1312455
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1312456
    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
