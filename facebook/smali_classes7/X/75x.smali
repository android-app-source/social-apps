.class public final enum LX/75x;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/75x;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/75x;

.field public static final enum BODY_ARRIVED:LX/75x;

.field public static final enum ERROR:LX/75x;

.field public static final enum HEADERS_ARRIVED:LX/75x;

.field public static final enum NO_RESPONSE:LX/75x;

.field public static final enum RESPONSE_COMPLETED:LX/75x;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1170208
    new-instance v0, LX/75x;

    const-string v1, "NO_RESPONSE"

    invoke-direct {v0, v1, v2}, LX/75x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/75x;->NO_RESPONSE:LX/75x;

    .line 1170209
    new-instance v0, LX/75x;

    const-string v1, "HEADERS_ARRIVED"

    invoke-direct {v0, v1, v3}, LX/75x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/75x;->HEADERS_ARRIVED:LX/75x;

    .line 1170210
    new-instance v0, LX/75x;

    const-string v1, "BODY_ARRIVED"

    invoke-direct {v0, v1, v4}, LX/75x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/75x;->BODY_ARRIVED:LX/75x;

    .line 1170211
    new-instance v0, LX/75x;

    const-string v1, "RESPONSE_COMPLETED"

    invoke-direct {v0, v1, v5}, LX/75x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/75x;->RESPONSE_COMPLETED:LX/75x;

    .line 1170212
    new-instance v0, LX/75x;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v6}, LX/75x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/75x;->ERROR:LX/75x;

    .line 1170213
    const/4 v0, 0x5

    new-array v0, v0, [LX/75x;

    sget-object v1, LX/75x;->NO_RESPONSE:LX/75x;

    aput-object v1, v0, v2

    sget-object v1, LX/75x;->HEADERS_ARRIVED:LX/75x;

    aput-object v1, v0, v3

    sget-object v1, LX/75x;->BODY_ARRIVED:LX/75x;

    aput-object v1, v0, v4

    sget-object v1, LX/75x;->RESPONSE_COMPLETED:LX/75x;

    aput-object v1, v0, v5

    sget-object v1, LX/75x;->ERROR:LX/75x;

    aput-object v1, v0, v6

    sput-object v0, LX/75x;->$VALUES:[LX/75x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1170214
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/75x;
    .locals 1

    .prologue
    .line 1170215
    const-class v0, LX/75x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/75x;

    return-object v0
.end method

.method public static values()[LX/75x;
    .locals 1

    .prologue
    .line 1170216
    sget-object v0, LX/75x;->$VALUES:[LX/75x;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/75x;

    return-object v0
.end method
