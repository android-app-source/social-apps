.class public final LX/6gr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 1126418
    const-wide/16 v12, 0x0

    .line 1126419
    const-wide/16 v10, 0x0

    .line 1126420
    const-wide/16 v8, 0x0

    .line 1126421
    const-wide/16 v6, 0x0

    .line 1126422
    const/4 v5, 0x0

    .line 1126423
    const/4 v4, 0x0

    .line 1126424
    const/4 v3, 0x0

    .line 1126425
    const/4 v2, 0x0

    .line 1126426
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_a

    .line 1126427
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1126428
    const/4 v2, 0x0

    .line 1126429
    :goto_0
    return v2

    .line 1126430
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_5

    .line 1126431
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1126432
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1126433
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1126434
    const-string v6, "w"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1126435
    const/4 v2, 0x1

    .line 1126436
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1126437
    :cond_1
    const-string v6, "x"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1126438
    const/4 v2, 0x1

    .line 1126439
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v16, v6

    goto :goto_1

    .line 1126440
    :cond_2
    const-string v6, "y"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1126441
    const/4 v2, 0x1

    .line 1126442
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide v14, v6

    goto :goto_1

    .line 1126443
    :cond_3
    const-string v6, "z"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1126444
    const/4 v2, 0x1

    .line 1126445
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v12, v6

    goto :goto_1

    .line 1126446
    :cond_4
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1126447
    :cond_5
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1126448
    if-eqz v3, :cond_6

    .line 1126449
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126450
    :cond_6
    if-eqz v10, :cond_7

    .line 1126451
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126452
    :cond_7
    if-eqz v9, :cond_8

    .line 1126453
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126454
    :cond_8
    if-eqz v8, :cond_9

    .line 1126455
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1126456
    :cond_9
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move-wide v14, v8

    move-wide/from16 v16, v10

    move v10, v4

    move v8, v2

    move v9, v3

    move v3, v5

    move-wide v4, v12

    move-wide v12, v6

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1126457
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1126458
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126459
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 1126460
    const-string v2, "w"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126461
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126462
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126463
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 1126464
    const-string v2, "x"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126465
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126466
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126467
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 1126468
    const-string v2, "y"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126469
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126470
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1126471
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_3

    .line 1126472
    const-string v2, "z"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1126473
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1126474
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1126475
    return-void
.end method
