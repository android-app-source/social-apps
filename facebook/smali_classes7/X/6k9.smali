.class public LX/6k9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;


# instance fields
.field public final messageMetadata:LX/6kn;

.field public final messageType:Ljava/lang/Integer;

.field public final requestId:Ljava/lang/Long;

.field public final transferId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x1

    .line 1135030
    new-instance v0, LX/1sv;

    const-string v1, "DeltaP2PPaymentMessage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6k9;->b:LX/1sv;

    .line 1135031
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k9;->c:LX/1sw;

    .line 1135032
    new-instance v0, LX/1sw;

    const-string v1, "transferId"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k9;->d:LX/1sw;

    .line 1135033
    new-instance v0, LX/1sw;

    const-string v1, "messageType"

    const/16 v2, 0x8

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k9;->e:LX/1sw;

    .line 1135034
    new-instance v0, LX/1sw;

    const-string v1, "requestId"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6k9;->f:LX/1sw;

    .line 1135035
    sput-boolean v4, LX/6k9;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6kn;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1135024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1135025
    iput-object p1, p0, LX/6k9;->messageMetadata:LX/6kn;

    .line 1135026
    iput-object p2, p0, LX/6k9;->transferId:Ljava/lang/Long;

    .line 1135027
    iput-object p3, p0, LX/6k9;->messageType:Ljava/lang/Integer;

    .line 1135028
    iput-object p4, p0, LX/6k9;->requestId:Ljava/lang/Long;

    .line 1135029
    return-void
.end method

.method public static a(LX/6k9;)V
    .locals 4

    .prologue
    .line 1135019
    iget-object v0, p0, LX/6k9;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1135020
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6k9;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1135021
    :cond_0
    iget-object v0, p0, LX/6k9;->messageType:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    sget-object v0, LX/6kw;->a:LX/1sn;

    iget-object v1, p0, LX/6k9;->messageType:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1135022
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'messageType\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6k9;->messageType:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1135023
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1135036
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1135037
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1135038
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    move-object v1, v0

    .line 1135039
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "DeltaP2PPaymentMessage"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1135040
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135041
    const-string v0, "("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135042
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135043
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135044
    const-string v0, "messageMetadata"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135045
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135046
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135047
    iget-object v0, p0, LX/6k9;->messageMetadata:LX/6kn;

    if-nez v0, :cond_6

    .line 1135048
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135049
    :goto_3
    iget-object v0, p0, LX/6k9;->transferId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1135050
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135051
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135052
    const-string v0, "transferId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135053
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135054
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135055
    iget-object v0, p0, LX/6k9;->transferId:Ljava/lang/Long;

    if-nez v0, :cond_7

    .line 1135056
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135057
    :cond_0
    :goto_4
    iget-object v0, p0, LX/6k9;->messageType:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1135058
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135059
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135060
    const-string v0, "messageType"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135061
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135062
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135063
    iget-object v0, p0, LX/6k9;->messageType:Ljava/lang/Integer;

    if-nez v0, :cond_8

    .line 1135064
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135065
    :cond_1
    :goto_5
    iget-object v0, p0, LX/6k9;->requestId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1135066
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135067
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135068
    const-string v0, "requestId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135069
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135070
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135071
    iget-object v0, p0, LX/6k9;->requestId:Ljava/lang/Long;

    if-nez v0, :cond_a

    .line 1135072
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135073
    :cond_2
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135074
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135075
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1135076
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1135077
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1135078
    :cond_5
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 1135079
    :cond_6
    iget-object v0, p0, LX/6k9;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1135080
    :cond_7
    iget-object v0, p0, LX/6k9;->transferId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1135081
    :cond_8
    sget-object v0, LX/6kw;->b:Ljava/util/Map;

    iget-object v5, p0, LX/6k9;->messageType:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1135082
    if-eqz v0, :cond_9

    .line 1135083
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135084
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135085
    :cond_9
    iget-object v5, p0, LX/6k9;->messageType:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1135086
    if-eqz v0, :cond_1

    .line 1135087
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1135088
    :cond_a
    iget-object v0, p0, LX/6k9;->requestId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1134999
    invoke-static {p0}, LX/6k9;->a(LX/6k9;)V

    .line 1135000
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1135001
    iget-object v0, p0, LX/6k9;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1135002
    sget-object v0, LX/6k9;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135003
    iget-object v0, p0, LX/6k9;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1135004
    :cond_0
    iget-object v0, p0, LX/6k9;->transferId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1135005
    iget-object v0, p0, LX/6k9;->transferId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1135006
    sget-object v0, LX/6k9;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135007
    iget-object v0, p0, LX/6k9;->transferId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1135008
    :cond_1
    iget-object v0, p0, LX/6k9;->messageType:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1135009
    iget-object v0, p0, LX/6k9;->messageType:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1135010
    sget-object v0, LX/6k9;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135011
    iget-object v0, p0, LX/6k9;->messageType:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1135012
    :cond_2
    iget-object v0, p0, LX/6k9;->requestId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1135013
    iget-object v0, p0, LX/6k9;->requestId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1135014
    sget-object v0, LX/6k9;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135015
    iget-object v0, p0, LX/6k9;->requestId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1135016
    :cond_3
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1135017
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1135018
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1134963
    if-nez p1, :cond_1

    .line 1134964
    :cond_0
    :goto_0
    return v0

    .line 1134965
    :cond_1
    instance-of v1, p1, LX/6k9;

    if-eqz v1, :cond_0

    .line 1134966
    check-cast p1, LX/6k9;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1134967
    if-nez p1, :cond_3

    .line 1134968
    :cond_2
    :goto_1
    move v0, v2

    .line 1134969
    goto :goto_0

    .line 1134970
    :cond_3
    iget-object v0, p0, LX/6k9;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1134971
    :goto_2
    iget-object v3, p1, LX/6k9;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1134972
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1134973
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1134974
    iget-object v0, p0, LX/6k9;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6k9;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1134975
    :cond_5
    iget-object v0, p0, LX/6k9;->transferId:Ljava/lang/Long;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1134976
    :goto_4
    iget-object v3, p1, LX/6k9;->transferId:Ljava/lang/Long;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1134977
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1134978
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1134979
    iget-object v0, p0, LX/6k9;->transferId:Ljava/lang/Long;

    iget-object v3, p1, LX/6k9;->transferId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1134980
    :cond_7
    iget-object v0, p0, LX/6k9;->messageType:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1134981
    :goto_6
    iget-object v3, p1, LX/6k9;->messageType:Ljava/lang/Integer;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1134982
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1134983
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1134984
    iget-object v0, p0, LX/6k9;->messageType:Ljava/lang/Integer;

    iget-object v3, p1, LX/6k9;->messageType:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1134985
    :cond_9
    iget-object v0, p0, LX/6k9;->requestId:Ljava/lang/Long;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1134986
    :goto_8
    iget-object v3, p1, LX/6k9;->requestId:Ljava/lang/Long;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1134987
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1134988
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1134989
    iget-object v0, p0, LX/6k9;->requestId:Ljava/lang/Long;

    iget-object v3, p1, LX/6k9;->requestId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_b
    move v2, v1

    .line 1134990
    goto :goto_1

    :cond_c
    move v0, v2

    .line 1134991
    goto :goto_2

    :cond_d
    move v3, v2

    .line 1134992
    goto :goto_3

    :cond_e
    move v0, v2

    .line 1134993
    goto :goto_4

    :cond_f
    move v3, v2

    .line 1134994
    goto :goto_5

    :cond_10
    move v0, v2

    .line 1134995
    goto :goto_6

    :cond_11
    move v3, v2

    .line 1134996
    goto :goto_7

    :cond_12
    move v0, v2

    .line 1134997
    goto :goto_8

    :cond_13
    move v3, v2

    .line 1134998
    goto :goto_9
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1134962
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1134959
    sget-boolean v0, LX/6k9;->a:Z

    .line 1134960
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6k9;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1134961
    return-object v0
.end method
