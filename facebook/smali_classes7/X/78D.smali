.class public final LX/78D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47M;


# instance fields
.field public final synthetic a:LX/78E;


# direct methods
.method public constructor <init>(LX/78E;)V
    .locals 0

    .prologue
    .line 1172493
    iput-object p1, p0, LX/78D;->a:LX/78E;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1172494
    const-string v0, "action"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/78C;->fromString(Ljava/lang/String;)LX/78C;

    move-result-object v1

    .line 1172495
    const-string v0, "data"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1172496
    const/4 v0, 0x0

    .line 1172497
    sget-object v3, LX/78B;->a:[I

    invoke-virtual {v1}, LX/78C;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    .line 1172498
    :goto_0
    if-nez v0, :cond_0

    .line 1172499
    const-string v0, "fallback_url"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1172500
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1172501
    :cond_0
    return-object v0

    .line 1172502
    :pswitch_0
    iget-object v0, p0, LX/78D;->a:LX/78E;

    iget-object v0, v0, LX/78E;->a:LX/17T;

    invoke-virtual {v0, v2}, LX/17T;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
