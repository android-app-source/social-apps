.class public final enum LX/6xZ;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6LU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6xZ;",
        ">;",
        "LX/6LU",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6xZ;

.field public static final enum ADD_CARD:LX/6xZ;

.field public static final enum ADD_SHIPPING_ADDRESS:LX/6xZ;

.field public static final enum CART_ITEM_SEARCH:LX/6xZ;

.field public static final enum CHECKOUT:LX/6xZ;

.field public static final enum CONFIRM_SECURITY_CODE:LX/6xZ;

.field public static final enum INVOICE:LX/6xZ;

.field public static final enum MANUAL_BANK_TRANSFER:LX/6xZ;

.field public static final enum PAYMENT:LX/6xZ;

.field public static final enum PAYMENTS_SETTINGS:LX/6xZ;

.field public static final enum PROOF_OF_PAYMENT:LX/6xZ;

.field public static final enum REVIEW:LX/6xZ;

.field public static final enum SEE_RECEIPT:LX/6xZ;

.field public static final enum SELECT_BANK_ACCOUNT:LX/6xZ;

.field public static final enum SELECT_CHECKOUT_OPTION:LX/6xZ;

.field public static final enum SELECT_CONTACT_INFO:LX/6xZ;

.field public static final enum SELECT_PAYMENT_METHOD:LX/6xZ;

.field public static final enum SELECT_SHIPPING_ADDRESS:LX/6xZ;

.field public static final enum SELECT_SHIPPING_METHOD:LX/6xZ;

.field public static final enum SEND:LX/6xZ;

.field public static final enum SHOW_WEB_VIEW:LX/6xZ;

.field public static final enum UPDATE_CARD:LX/6xZ;

.field public static final enum UPDATE_SHIPPING_ADDRESS:LX/6xZ;

.field public static final enum VIEW_CART:LX/6xZ;


# instance fields
.field private mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1158923
    new-instance v0, LX/6xZ;

    const-string v1, "ADD_CARD"

    const-string v2, "add_card"

    invoke-direct {v0, v1, v4, v2}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->ADD_CARD:LX/6xZ;

    .line 1158924
    new-instance v0, LX/6xZ;

    const-string v1, "ADD_SHIPPING_ADDRESS"

    const-string v2, "add_shipping_address"

    invoke-direct {v0, v1, v5, v2}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->ADD_SHIPPING_ADDRESS:LX/6xZ;

    .line 1158925
    new-instance v0, LX/6xZ;

    const-string v1, "CART_ITEM_SEARCH"

    const-string v2, "item_search"

    invoke-direct {v0, v1, v6, v2}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->CART_ITEM_SEARCH:LX/6xZ;

    .line 1158926
    new-instance v0, LX/6xZ;

    const-string v1, "CHECKOUT"

    const-string v2, "checkout"

    invoke-direct {v0, v1, v7, v2}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->CHECKOUT:LX/6xZ;

    .line 1158927
    new-instance v0, LX/6xZ;

    const-string v1, "CONFIRM_SECURITY_CODE"

    const-string v2, "confirm_security_code"

    invoke-direct {v0, v1, v8, v2}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->CONFIRM_SECURITY_CODE:LX/6xZ;

    .line 1158928
    new-instance v0, LX/6xZ;

    const-string v1, "INVOICE"

    const/4 v2, 0x5

    const-string v3, "invoice"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->INVOICE:LX/6xZ;

    .line 1158929
    new-instance v0, LX/6xZ;

    const-string v1, "REVIEW"

    const/4 v2, 0x6

    const-string v3, "review"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->REVIEW:LX/6xZ;

    .line 1158930
    new-instance v0, LX/6xZ;

    const-string v1, "SEND"

    const/4 v2, 0x7

    const-string v3, "send"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->SEND:LX/6xZ;

    .line 1158931
    new-instance v0, LX/6xZ;

    const-string v1, "MANUAL_BANK_TRANSFER"

    const/16 v2, 0x8

    const-string v3, "manual_bank_transfer"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->MANUAL_BANK_TRANSFER:LX/6xZ;

    .line 1158932
    new-instance v0, LX/6xZ;

    const-string v1, "PAYMENT"

    const/16 v2, 0x9

    const-string v3, "payment"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->PAYMENT:LX/6xZ;

    .line 1158933
    new-instance v0, LX/6xZ;

    const-string v1, "PAYMENTS_SETTINGS"

    const/16 v2, 0xa

    const-string v3, "payments_settings"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->PAYMENTS_SETTINGS:LX/6xZ;

    .line 1158934
    new-instance v0, LX/6xZ;

    const-string v1, "PROOF_OF_PAYMENT"

    const/16 v2, 0xb

    const-string v3, "proof_of_payment"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->PROOF_OF_PAYMENT:LX/6xZ;

    .line 1158935
    new-instance v0, LX/6xZ;

    const-string v1, "SEE_RECEIPT"

    const/16 v2, 0xc

    const-string v3, "see_receipt"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->SEE_RECEIPT:LX/6xZ;

    .line 1158936
    new-instance v0, LX/6xZ;

    const-string v1, "SELECT_BANK_ACCOUNT"

    const/16 v2, 0xd

    const-string v3, "select_bank_account"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->SELECT_BANK_ACCOUNT:LX/6xZ;

    .line 1158937
    new-instance v0, LX/6xZ;

    const-string v1, "SELECT_CONTACT_INFO"

    const/16 v2, 0xe

    const-string v3, "select_contact_info"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->SELECT_CONTACT_INFO:LX/6xZ;

    .line 1158938
    new-instance v0, LX/6xZ;

    const-string v1, "SELECT_PAYMENT_METHOD"

    const/16 v2, 0xf

    const-string v3, "select_payment_method"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->SELECT_PAYMENT_METHOD:LX/6xZ;

    .line 1158939
    new-instance v0, LX/6xZ;

    const-string v1, "SELECT_CHECKOUT_OPTION"

    const/16 v2, 0x10

    const-string v3, "select_checkout_option"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->SELECT_CHECKOUT_OPTION:LX/6xZ;

    .line 1158940
    new-instance v0, LX/6xZ;

    const-string v1, "SELECT_SHIPPING_ADDRESS"

    const/16 v2, 0x11

    const-string v3, "select_shipping_address"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->SELECT_SHIPPING_ADDRESS:LX/6xZ;

    .line 1158941
    new-instance v0, LX/6xZ;

    const-string v1, "SELECT_SHIPPING_METHOD"

    const/16 v2, 0x12

    const-string v3, "select_shipping_method"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->SELECT_SHIPPING_METHOD:LX/6xZ;

    .line 1158942
    new-instance v0, LX/6xZ;

    const-string v1, "SHOW_WEB_VIEW"

    const/16 v2, 0x13

    const-string v3, "show_web_view"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->SHOW_WEB_VIEW:LX/6xZ;

    .line 1158943
    new-instance v0, LX/6xZ;

    const-string v1, "UPDATE_CARD"

    const/16 v2, 0x14

    const-string v3, "update_card"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->UPDATE_CARD:LX/6xZ;

    .line 1158944
    new-instance v0, LX/6xZ;

    const-string v1, "UPDATE_SHIPPING_ADDRESS"

    const/16 v2, 0x15

    const-string v3, "update_shipping_address"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->UPDATE_SHIPPING_ADDRESS:LX/6xZ;

    .line 1158945
    new-instance v0, LX/6xZ;

    const-string v1, "VIEW_CART"

    const/16 v2, 0x16

    const-string v3, "view_cart"

    invoke-direct {v0, v1, v2, v3}, LX/6xZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6xZ;->VIEW_CART:LX/6xZ;

    .line 1158946
    const/16 v0, 0x17

    new-array v0, v0, [LX/6xZ;

    sget-object v1, LX/6xZ;->ADD_CARD:LX/6xZ;

    aput-object v1, v0, v4

    sget-object v1, LX/6xZ;->ADD_SHIPPING_ADDRESS:LX/6xZ;

    aput-object v1, v0, v5

    sget-object v1, LX/6xZ;->CART_ITEM_SEARCH:LX/6xZ;

    aput-object v1, v0, v6

    sget-object v1, LX/6xZ;->CHECKOUT:LX/6xZ;

    aput-object v1, v0, v7

    sget-object v1, LX/6xZ;->CONFIRM_SECURITY_CODE:LX/6xZ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/6xZ;->INVOICE:LX/6xZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6xZ;->REVIEW:LX/6xZ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6xZ;->SEND:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6xZ;->MANUAL_BANK_TRANSFER:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6xZ;->PAYMENT:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6xZ;->PAYMENTS_SETTINGS:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/6xZ;->PROOF_OF_PAYMENT:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/6xZ;->SEE_RECEIPT:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/6xZ;->SELECT_BANK_ACCOUNT:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/6xZ;->SELECT_CONTACT_INFO:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/6xZ;->SELECT_PAYMENT_METHOD:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/6xZ;->SELECT_CHECKOUT_OPTION:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/6xZ;->SELECT_SHIPPING_ADDRESS:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/6xZ;->SELECT_SHIPPING_METHOD:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/6xZ;->SHOW_WEB_VIEW:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/6xZ;->UPDATE_CARD:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/6xZ;->UPDATE_SHIPPING_ADDRESS:LX/6xZ;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/6xZ;->VIEW_CART:LX/6xZ;

    aput-object v2, v0, v1

    sput-object v0, LX/6xZ;->$VALUES:[LX/6xZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1158947
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1158948
    iput-object p3, p0, LX/6xZ;->mValue:Ljava/lang/String;

    .line 1158949
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6xZ;
    .locals 1

    .prologue
    .line 1158950
    const-class v0, LX/6xZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xZ;

    return-object v0
.end method

.method public static values()[LX/6xZ;
    .locals 1

    .prologue
    .line 1158951
    sget-object v0, LX/6xZ;->$VALUES:[LX/6xZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6xZ;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1158952
    invoke-virtual {p0}, LX/6xZ;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1158953
    iget-object v0, p0, LX/6xZ;->mValue:Ljava/lang/String;

    return-object v0
.end method
