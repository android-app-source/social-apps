.class public LX/8Oa;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public final c:LX/8OZ;

.field public final d:I

.field public e:J

.field public f:J

.field public g:LX/7T7;


# direct methods
.method public constructor <init>(LX/8OZ;I)V
    .locals 0

    .prologue
    .line 1339647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1339648
    iput-object p1, p0, LX/8Oa;->c:LX/8OZ;

    .line 1339649
    iput p2, p0, LX/8Oa;->d:I

    .line 1339650
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1339651
    iget-object v0, p0, LX/8Oa;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1339652
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/8Oa;->c:LX/8OZ;

    invoke-virtual {v1}, LX/8OZ;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/8Oa;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1339653
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8Oa;->b:Ljava/lang/String;

    .line 1339654
    return-void
.end method

.method public final a(Ljava/lang/String;JJ)V
    .locals 2

    .prologue
    .line 1339655
    iput-object p1, p0, LX/8Oa;->a:Ljava/lang/String;

    .line 1339656
    iput-wide p4, p0, LX/8Oa;->e:J

    .line 1339657
    add-long v0, p4, p2

    iput-wide v0, p0, LX/8Oa;->f:J

    .line 1339658
    return-void
.end method
