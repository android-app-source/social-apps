.class public LX/8OT;
.super Ljava/lang/Exception;
.source ""

# interfaces
.implements LX/2Op;


# instance fields
.field private final mCause:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

.field private final mUploadRecords:Lcom/facebook/photos/upload/operation/UploadRecords;


# direct methods
.method public constructor <init>(LX/73z;)V
    .locals 2

    .prologue
    .line 1339478
    iget-object v0, p1, LX/73z;->g:Ljava/lang/Exception;

    move-object v0, v0

    .line 1339479
    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 1339480
    new-instance v0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    invoke-direct {v0, p1}, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;-><init>(LX/73z;)V

    iput-object v0, p0, LX/8OT;->mCause:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    .line 1339481
    new-instance v0, Lcom/facebook/photos/upload/operation/UploadRecords;

    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/upload/operation/UploadRecords;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, LX/8OT;->mUploadRecords:Lcom/facebook/photos/upload/operation/UploadRecords;

    .line 1339482
    return-void
.end method

.method public constructor <init>(LX/73z;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/73z;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/operation/UploadRecord;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1339473
    iget-object v0, p1, LX/73z;->g:Ljava/lang/Exception;

    move-object v0, v0

    .line 1339474
    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 1339475
    new-instance v0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    invoke-direct {v0, p1}, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;-><init>(LX/73z;)V

    iput-object v0, p0, LX/8OT;->mCause:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    .line 1339476
    new-instance v0, Lcom/facebook/photos/upload/operation/UploadRecords;

    invoke-direct {v0, p2}, Lcom/facebook/photos/upload/operation/UploadRecords;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, LX/8OT;->mUploadRecords:Lcom/facebook/photos/upload/operation/UploadRecords;

    .line 1339477
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/facebook/photos/upload/operation/UploadInterruptionCause;
    .locals 1

    .prologue
    .line 1339472
    const-string v0, "interruption_cause"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    return-object v0
.end method

.method public static b(Landroid/os/Bundle;)Lcom/facebook/photos/upload/operation/UploadRecords;
    .locals 1

    .prologue
    .line 1339471
    const-string v0, "upload_records"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadRecords;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/upload/operation/UploadInterruptionCause;
    .locals 1

    .prologue
    .line 1339470
    iget-object v0, p0, LX/8OT;->mCause:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    return-object v0
.end method

.method public final b()Lcom/facebook/photos/upload/operation/UploadRecords;
    .locals 1

    .prologue
    .line 1339469
    iget-object v0, p0, LX/8OT;->mUploadRecords:Lcom/facebook/photos/upload/operation/UploadRecords;

    return-object v0
.end method

.method public final getExtraData()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 1339465
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1339466
    const-string v1, "interruption_cause"

    iget-object v2, p0, LX/8OT;->mCause:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1339467
    const-string v1, "upload_records"

    iget-object v2, p0, LX/8OT;->mUploadRecords:Lcom/facebook/photos/upload/operation/UploadRecords;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1339468
    return-object v0
.end method
