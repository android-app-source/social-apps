.class public final LX/8M2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/7mj;

.field public final synthetic b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7mj;)V
    .locals 0

    .prologue
    .line 1334265
    iput-object p1, p0, LX/8M2;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iput-object p2, p0, LX/8M2;->a:LX/7mj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 1334258
    iget-object v0, p0, LX/8M2;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v1, p0, LX/8M2;->a:LX/7mj;

    .line 1334259
    new-instance v2, LX/0ju;

    iget-object v3, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f082068

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    const v3, 0x7f082067

    new-instance p1, LX/8MJ;

    invoke-direct {p1, v0}, LX/8MJ;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;)V

    invoke-virtual {v2, v3, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    const v3, 0x7f08104c

    new-instance p1, LX/8MI;

    invoke-direct {p1, v0, v1}, LX/8MI;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7mj;)V

    invoke-virtual {v2, v3, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    .line 1334260
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v2

    .line 1334261
    new-instance v3, LX/8MK;

    invoke-direct {v3, v0}, LX/8MK;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;)V

    invoke-static {v0, v2, v3}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/2EJ;Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1334262
    invoke-virtual {v2}, LX/2EJ;->show()V

    .line 1334263
    iget-object v0, p0, LX/8M2;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    sget-object v1, LX/8K6;->DRAFT_SECTION:LX/8K6;

    iget-object v1, v1, LX/8K6;->analyticsName:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1RW;->b(Ljava/lang/String;)V

    .line 1334264
    const/4 v0, 0x1

    return v0
.end method
