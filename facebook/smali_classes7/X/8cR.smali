.class public final LX/8cR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0Px",
        "<",
        "LX/8cI;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:I

.field public final synthetic c:I

.field public final synthetic d:LX/8cT;


# direct methods
.method public constructor <init>(LX/8cT;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 1374398
    iput-object p1, p0, LX/8cR;->d:LX/8cT;

    iput-object p2, p0, LX/8cR;->a:Ljava/lang/String;

    iput p3, p0, LX/8cR;->b:I

    iput p4, p0, LX/8cR;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1374399
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1374400
    iget-object v0, p0, LX/8cR;->d:LX/8cT;

    iget-object v0, v0, LX/8cT;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8cF;

    iget-object v1, p0, LX/8cR;->a:Ljava/lang/String;

    iget v3, p0, LX/8cR;->b:I

    invoke-virtual {v0, v1, v3}, LX/8cF;->c(Ljava/lang/String;I)LX/8cG;

    move-result-object v0

    .line 1374401
    iget-object v1, p0, LX/8cR;->d:LX/8cT;

    invoke-static {v1, v0}, LX/8cT;->a$redex0(LX/8cT;LX/2TZ;)Ljava/util/List;

    move-result-object v1

    .line 1374402
    iget-object v0, p0, LX/8cR;->d:LX/8cT;

    iget-object v3, p0, LX/8cR;->a:Ljava/lang/String;

    iget v4, p0, LX/8cR;->c:I

    invoke-static {v0, v3, v4}, LX/8cT;->d(LX/8cT;Ljava/lang/String;I)Ljava/util/List;

    move-result-object v3

    .line 1374403
    iget-object v0, p0, LX/8cR;->d:LX/8cT;

    iget-object v0, v0, LX/8cT;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v4, LX/100;->bS:S

    const/4 v5, 0x0

    invoke-interface {v0, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1374404
    iget v0, p0, LX/8cR;->c:I

    if-nez v0, :cond_0

    .line 1374405
    invoke-virtual {v2, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1374406
    :goto_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1374407
    :cond_0
    iget v0, p0, LX/8cR;->b:I

    if-nez v0, :cond_1

    .line 1374408
    invoke-virtual {v2, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_0

    .line 1374409
    :cond_1
    invoke-static {v3, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    new-instance v1, LX/8cQ;

    invoke-direct {v1, p0}, LX/8cQ;-><init>(LX/8cR;)V

    invoke-static {v1}, LX/1sm;->a(Ljava/util/Comparator;)LX/1sm;

    move-result-object v1

    .line 1374410
    const-string v3, "iterables"

    invoke-static {v0, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1374411
    const-string v3, "comparator"

    invoke-static {v1, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1374412
    new-instance v3, LX/4yn;

    invoke-direct {v3, v0, v1}, LX/4yn;-><init>(Ljava/lang/Iterable;Ljava/util/Comparator;)V

    .line 1374413
    new-instance v4, LX/4yq;

    invoke-direct {v4, v3}, LX/4yq;-><init>(Ljava/lang/Iterable;)V

    move-object v0, v4

    .line 1374414
    iget v1, p0, LX/8cR;->c:I

    iget v3, p0, LX/8cR;->b:I

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, LX/0Ph;->c(Ljava/lang/Iterable;I)Ljava/lang/Iterable;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_0

    .line 1374415
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1374416
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1374417
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8cM;

    .line 1374418
    sget-object v6, LX/8cT;->f:LX/0Rf;

    .line 1374419
    iget-object p0, v0, LX/8cM;->c:Ljava/lang/String;

    move-object p0, p0

    .line 1374420
    invoke-virtual {v6, p0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1374421
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1374422
    :cond_4
    move-object v0, v4

    .line 1374423
    :goto_2
    invoke-virtual {v2, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_0

    :cond_5
    move-object v0, v1

    .line 1374424
    goto :goto_2
.end method
