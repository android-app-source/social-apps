.class public final LX/7SK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;


# direct methods
.method public constructor <init>(Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;Ljava/lang/String;Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;)V
    .locals 0

    .prologue
    .line 1208512
    iput-object p1, p0, LX/7SK;->a:Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1208513
    iput-object p2, p0, LX/7SK;->b:Ljava/lang/String;

    .line 1208514
    iput-object p3, p0, LX/7SK;->c:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    .line 1208515
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1208516
    const-string v0, "ShaderRenderer"

    const-string v1, "Error loading image from disk"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1208517
    iget-object v0, p0, LX/7SK;->a:Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

    const/4 v1, 0x0

    .line 1208518
    iput-object v1, v0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->l:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    .line 1208519
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1208520
    check-cast p1, LX/1FJ;

    .line 1208521
    iget-object v0, p0, LX/7SK;->a:Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

    .line 1208522
    iget v1, v0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->o:I

    add-int/lit8 v2, v1, -0x1

    iput v2, v0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->o:I

    .line 1208523
    iget-object v0, p0, LX/7SK;->a:Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

    iget-object v0, v0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->l:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    iget-object v1, p0, LX/7SK;->c:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    if-eq v0, v1, :cond_1

    .line 1208524
    if-eqz p1, :cond_0

    .line 1208525
    invoke-virtual {p1}, LX/1FJ;->close()V

    .line 1208526
    :cond_0
    :goto_0
    return-void

    .line 1208527
    :cond_1
    if-eqz p1, :cond_0

    .line 1208528
    iget-object v0, p0, LX/7SK;->a:Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

    iget-object v0, v0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->s:Ljava/util/HashMap;

    iget-object v1, p0, LX/7SK;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
