.class public LX/6o4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# instance fields
.field public final a:LX/6oL;

.field public b:Landroid/os/CancellationSignal;

.field public c:Z


# direct methods
.method public constructor <init>(LX/6oL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1147868
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1147869
    iput-object p1, p0, LX/6o4;->a:LX/6oL;

    .line 1147870
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1147871
    iget-object v0, p0, LX/6o4;->b:Landroid/os/CancellationSignal;

    if-eqz v0, :cond_0

    .line 1147872
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6o4;->c:Z

    .line 1147873
    iget-object v0, p0, LX/6o4;->b:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    .line 1147874
    const/4 v0, 0x0

    iput-object v0, p0, LX/6o4;->b:Landroid/os/CancellationSignal;

    .line 1147875
    :cond_0
    return-void
.end method
