.class public LX/8Te;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/8Ve;

.field private final c:LX/0tX;

.field private final d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/8TD;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8Ve;LX/0tX;LX/1Ck;LX/8TD;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1348881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1348882
    iput-object p1, p0, LX/8Te;->a:Landroid/content/Context;

    .line 1348883
    iput-object p2, p0, LX/8Te;->b:LX/8Ve;

    .line 1348884
    iput-object p3, p0, LX/8Te;->c:LX/0tX;

    .line 1348885
    iput-object p4, p0, LX/8Te;->d:LX/1Ck;

    .line 1348886
    iput-object p5, p0, LX/8Te;->e:LX/8TD;

    .line 1348887
    return-void
.end method

.method public static a$redex0(LX/8Te;Ljava/lang/String;LX/0Px;Ljava/util/List;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1348842
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v4

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_5

    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;

    .line 1348843
    invoke-static {}, LX/8Vb;->b()LX/8Va;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->n()Ljava/lang/String;

    move-result-object v5

    .line 1348844
    iput-object v5, v1, LX/8Va;->c:Ljava/lang/String;

    .line 1348845
    move-object v1, v1

    .line 1348846
    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->j()Ljava/lang/String;

    move-result-object v5

    .line 1348847
    iput-object v5, v1, LX/8Va;->h:Ljava/lang/String;

    .line 1348848
    move-object v1, v1

    .line 1348849
    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->k()Ljava/lang/String;

    move-result-object v5

    .line 1348850
    iput-object v5, v1, LX/8Va;->i:Ljava/lang/String;

    .line 1348851
    move-object v5, v1

    .line 1348852
    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->m()LX/1vs;

    move-result-object v1

    iget-object v6, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v6, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1348853
    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->l()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/8Te;->b(LX/0Px;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1348854
    iput-object v1, v5, LX/8Va;->a:Ljava/lang/String;

    .line 1348855
    iget-object v6, p0, LX/8Te;->b:LX/8Ve;

    invoke-virtual {v6, v1}, LX/8Ve;->a(Ljava/lang/String;)LX/8Vd;

    move-result-object v1

    .line 1348856
    iput-object v1, v5, LX/8Va;->k:LX/8Vd;

    .line 1348857
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1348858
    iget-object v1, p0, LX/8Te;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f08235a

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1348859
    :goto_2
    iput-object v1, v5, LX/8Va;->l:Ljava/lang/String;

    .line 1348860
    invoke-virtual {v5}, LX/8Va;->a()LX/8Vb;

    move-result-object v1

    .line 1348861
    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1348862
    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1348863
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1348864
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->l()LX/0Px;

    move-result-object v1

    .line 1348865
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 1348866
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v9

    const/4 v6, 0x0

    move v7, v6

    :goto_4
    if-ge v7, v9, :cond_2

    invoke-virtual {v1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$ParticipantsModel;

    .line 1348867
    invoke-virtual {v6}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$ParticipantsModel;->j()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 1348868
    invoke-virtual {v6}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$ParticipantsModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1348869
    :cond_1
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_4

    .line 1348870
    :cond_2
    move-object v1, v8

    .line 1348871
    iget-object v6, p0, LX/8Te;->b:LX/8Ve;

    invoke-virtual {v6, v1}, LX/8Ve;->a(Ljava/util/List;)LX/8Vd;

    move-result-object v1

    .line 1348872
    iput-object v1, v5, LX/8Va;->k:LX/8Vd;

    .line 1348873
    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->m()LX/1vs;

    move-result-object v1

    iget-object v6, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v6, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 1348874
    iput-object v1, v5, LX/8Va;->b:Ljava/lang/String;

    .line 1348875
    goto :goto_1

    .line 1348876
    :cond_3
    iget-object v1, p0, LX/8Te;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f08235b

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 1348877
    :cond_4
    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1348878
    :cond_5
    return-void
.end method

.method public static b(LX/0QB;)LX/8Te;
    .locals 6

    .prologue
    .line 1348879
    new-instance v0, LX/8Te;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/8Ve;->b(LX/0QB;)LX/8Ve;

    move-result-object v2

    check-cast v2, LX/8Ve;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p0}, LX/8TD;->a(LX/0QB;)LX/8TD;

    move-result-object v5

    check-cast v5, LX/8TD;

    invoke-direct/range {v0 .. v5}, LX/8Te;-><init>(Landroid/content/Context;LX/8Ve;LX/0tX;LX/1Ck;LX/8TD;)V

    .line 1348880
    return-object v0
.end method

.method private static b(LX/0Px;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$ParticipantsModel;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1348835
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1348836
    invoke-virtual {p0, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$ParticipantsModel;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$ParticipantsModel;->j()Ljava/lang/String;

    move-result-object p1

    .line 1348837
    :cond_0
    :goto_0
    return-object p1

    .line 1348838
    :cond_1
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$ParticipantsModel;

    .line 1348839
    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$ParticipantsModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1348840
    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$ParticipantsModel;->j()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 1348841
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/8TP;LX/8Td;)V
    .locals 4

    .prologue
    .line 1348823
    new-instance v0, LX/8U2;

    invoke-direct {v0}, LX/8U2;-><init>()V

    move-object v0, v0

    .line 1348824
    const-string v1, "game_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1348825
    new-instance v1, LX/4GU;

    invoke-direct {v1}, LX/4GU;-><init>()V

    .line 1348826
    invoke-virtual {p3}, LX/8TP;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1348827
    iget-object v2, p3, LX/8TP;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1348828
    const-string v3, "thread_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1348829
    :cond_0
    const-string v2, "context"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1348830
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 1348831
    iget-object v1, p0, LX/8Te;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1348832
    new-instance v1, LX/8Tc;

    invoke-direct {v1, p0, p2, p4}, LX/8Tc;-><init>(LX/8Te;Ljava/lang/String;LX/8Td;)V

    .line 1348833
    iget-object v2, p0, LX/8Te;->d:LX/1Ck;

    const-string v3, "games_all_matches_query"

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1348834
    return-void
.end method
