.class public LX/86i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/86d;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/86d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/86f;LX/86e;LX/86g;LX/86h;LX/86j;LX/86k;LX/86l;)V
    .locals 7
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1297662
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p7

    move-object v6, p6

    .line 1297663
    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/86i;->a:LX/0Px;

    .line 1297664
    return-void
.end method

.method public static a(LX/0QB;)LX/86i;
    .locals 11

    .prologue
    .line 1297665
    const-class v1, LX/86i;

    monitor-enter v1

    .line 1297666
    :try_start_0
    sget-object v0, LX/86i;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1297667
    sput-object v2, LX/86i;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1297668
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1297669
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1297670
    new-instance v3, LX/86i;

    invoke-static {v0}, LX/86f;->a(LX/0QB;)LX/86f;

    move-result-object v4

    check-cast v4, LX/86f;

    invoke-static {v0}, LX/86e;->a(LX/0QB;)LX/86e;

    move-result-object v5

    check-cast v5, LX/86e;

    invoke-static {v0}, LX/86g;->a(LX/0QB;)LX/86g;

    move-result-object v6

    check-cast v6, LX/86g;

    invoke-static {v0}, LX/86h;->a(LX/0QB;)LX/86h;

    move-result-object v7

    check-cast v7, LX/86h;

    invoke-static {v0}, LX/86j;->a(LX/0QB;)LX/86j;

    move-result-object v8

    check-cast v8, LX/86j;

    invoke-static {v0}, LX/86k;->a(LX/0QB;)LX/86k;

    move-result-object v9

    check-cast v9, LX/86k;

    invoke-static {v0}, LX/86l;->a(LX/0QB;)LX/86l;

    move-result-object v10

    check-cast v10, LX/86l;

    invoke-direct/range {v3 .. v10}, LX/86i;-><init>(LX/86f;LX/86e;LX/86g;LX/86h;LX/86j;LX/86k;LX/86l;)V

    .line 1297671
    move-object v0, v3

    .line 1297672
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1297673
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/86i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1297674
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1297675
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;ZZ)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1297676
    iget-object v0, p0, LX/86i;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, LX/86i;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/86d;

    .line 1297677
    invoke-interface {v0, p1, p2, p3}, LX/86d;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1297678
    const/4 v0, 0x1

    .line 1297679
    :goto_1
    return v0

    .line 1297680
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1297681
    goto :goto_1
.end method
