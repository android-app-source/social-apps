.class public LX/7Tj;
.super LX/0ht;
.source ""


# instance fields
.field public final a:Z

.field public final l:LX/0W9;

.field public final m:LX/3Lz;

.field public n:Landroid/view/View;

.field public o:Lcom/facebook/ui/search/SearchEditText;

.field public p:Lcom/facebook/widget/listview/BetterListView;

.field public q:Lcom/facebook/resources/ui/FbButton;

.field public r:Ljava/util/Locale;

.field public s:[LX/7Tl;

.field public t:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "LX/7Tl;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/6u6;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLX/0W9;LX/3Lz;)V
    .locals 5
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1210884
    invoke-direct {p0, p1}, LX/0ht;-><init>(Landroid/content/Context;)V

    .line 1210885
    iput-boolean p2, p0, LX/7Tj;->a:Z

    .line 1210886
    iput-object p3, p0, LX/7Tj;->l:LX/0W9;

    .line 1210887
    iput-object p4, p0, LX/7Tj;->m:LX/3Lz;

    .line 1210888
    const/4 v1, 0x0

    .line 1210889
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030399

    iget-object v3, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/7Tj;->n:Landroid/view/View;

    .line 1210890
    iget-object v0, p0, LX/7Tj;->n:Landroid/view/View;

    const v2, 0x7f0d0b84

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, LX/7Tj;->o:Lcom/facebook/ui/search/SearchEditText;

    .line 1210891
    iget-object v0, p0, LX/7Tj;->n:Landroid/view/View;

    const v2, 0x7f0d0b86

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, LX/7Tj;->p:Lcom/facebook/widget/listview/BetterListView;

    .line 1210892
    iget-object v0, p0, LX/7Tj;->n:Landroid/view/View;

    const v2, 0x7f0d09a9

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/7Tj;->q:Lcom/facebook/resources/ui/FbButton;

    .line 1210893
    iget-object v0, p0, LX/7Tj;->o:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->requestFocus()Z

    .line 1210894
    new-instance v0, LX/7Td;

    invoke-direct {v0, p0}, LX/7Td;-><init>(LX/7Tj;)V

    iput-object v0, p0, LX/7Tj;->u:LX/6u6;

    .line 1210895
    iget-object v0, p0, LX/7Tj;->l:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, LX/7Tj;->r:Ljava/util/Locale;

    .line 1210896
    invoke-static {}, Ljava/util/Locale;->getISOCountries()[Ljava/lang/String;

    move-result-object v2

    .line 1210897
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 1210898
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 1210899
    aget-object v4, v2, v0

    .line 1210900
    iget-object p1, p0, LX/7Tj;->m:LX/3Lz;

    invoke-virtual {p1, v4}, LX/3Lz;->getCountryCodeForRegion(Ljava/lang/String;)I

    move-result p2

    .line 1210901
    if-nez p2, :cond_2

    .line 1210902
    const/4 p1, 0x0

    .line 1210903
    :goto_1
    move-object v4, p1

    .line 1210904
    if-eqz v4, :cond_0

    .line 1210905
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1210906
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1210907
    :cond_1
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1210908
    new-array v0, v1, [LX/7Tl;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Tl;

    iput-object v0, p0, LX/7Tj;->s:[LX/7Tl;

    .line 1210909
    new-instance v0, LX/7Te;

    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030398

    iget-object v3, p0, LX/7Tj;->s:[LX/7Tl;

    invoke-direct {v0, p0, v1, v2, v3}, LX/7Te;-><init>(LX/7Tj;Landroid/content/Context;I[LX/7Tl;)V

    iput-object v0, p0, LX/7Tj;->t:Landroid/widget/ArrayAdapter;

    .line 1210910
    iget-object v0, p0, LX/7Tj;->p:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, LX/7Tj;->t:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1210911
    iget-object v0, p0, LX/7Tj;->p:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/7Tf;

    invoke-direct {v1, p0}, LX/7Tf;-><init>(LX/7Tj;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1210912
    iget-object v0, p0, LX/7Tj;->o:Lcom/facebook/ui/search/SearchEditText;

    new-instance v1, LX/7Th;

    invoke-direct {v1, p0}, LX/7Th;-><init>(LX/7Tj;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1210913
    iget-object v0, p0, LX/7Tj;->q:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/7Ti;

    invoke-direct {v1, p0}, LX/7Ti;-><init>(LX/7Tj;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1210914
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1210915
    const/4 v1, -0x1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1210916
    const/16 v0, 0x3eb

    .line 1210917
    iput v0, p0, LX/0ht;->s:I

    .line 1210918
    return-void

    :cond_2
    new-instance p1, LX/7Tl;

    new-instance p3, Ljava/lang/StringBuilder;

    const-string p4, "+"

    invoke-direct {p3, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    new-instance p3, Ljava/util/Locale;

    iget-object p4, p0, LX/7Tj;->r:Ljava/util/Locale;

    invoke-virtual {p4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p4

    invoke-direct {p3, p4, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p4, p0, LX/7Tj;->r:Ljava/util/Locale;

    invoke-virtual {p3, p4}, Ljava/util/Locale;->getDisplayCountry(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, v4, p2, p3}, LX/7Tl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final l()V
    .locals 3

    .prologue
    .line 1210919
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1210920
    iget-object v1, p0, LX/7Tj;->o:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1210921
    invoke-super {p0}, LX/0ht;->l()V

    .line 1210922
    return-void
.end method
