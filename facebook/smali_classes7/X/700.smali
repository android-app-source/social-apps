.class public LX/700;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6vm;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/common/locale/Country;

.field public final b:Z

.field public final c:Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;Lcom/facebook/common/locale/Country;Z)V
    .locals 0

    .prologue
    .line 1161730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161731
    iput-object p1, p0, LX/700;->c:Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;

    .line 1161732
    iput-object p2, p0, LX/700;->a:Lcom/facebook/common/locale/Country;

    .line 1161733
    iput-boolean p3, p0, LX/700;->b:Z

    .line 1161734
    return-void
.end method


# virtual methods
.method public final a()LX/71I;
    .locals 1

    .prologue
    .line 1161735
    sget-object v0, LX/71I;->COUNTRY_SELECTOR:LX/71I;

    return-object v0
.end method
