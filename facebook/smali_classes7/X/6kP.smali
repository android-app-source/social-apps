.class public LX/6kP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final image:LX/6jZ;

.field public final messageMetadata:LX/6kn;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xc

    const/4 v3, 0x1

    .line 1136662
    new-instance v0, LX/1sv;

    const-string v1, "DeltaThreadImage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kP;->b:LX/1sv;

    .line 1136663
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kP;->c:LX/1sw;

    .line 1136664
    new-instance v0, LX/1sw;

    const-string v1, "image"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kP;->d:LX/1sw;

    .line 1136665
    sput-boolean v3, LX/6kP;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6kn;LX/6jZ;)V
    .locals 0

    .prologue
    .line 1136658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1136659
    iput-object p1, p0, LX/6kP;->messageMetadata:LX/6kn;

    .line 1136660
    iput-object p2, p0, LX/6kP;->image:LX/6jZ;

    .line 1136661
    return-void
.end method

.method public static a(LX/6kP;)V
    .locals 4

    .prologue
    .line 1136655
    iget-object v0, p0, LX/6kP;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1136656
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kP;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1136657
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1136626
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1136627
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1136628
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1136629
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaThreadImage"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1136630
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136631
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136632
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136633
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136634
    const-string v4, "messageMetadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136635
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136636
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136637
    iget-object v4, p0, LX/6kP;->messageMetadata:LX/6kn;

    if-nez v4, :cond_4

    .line 1136638
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136639
    :goto_3
    iget-object v4, p0, LX/6kP;->image:LX/6jZ;

    if-eqz v4, :cond_0

    .line 1136640
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136641
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136642
    const-string v4, "image"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136643
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136644
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136645
    iget-object v0, p0, LX/6kP;->image:LX/6jZ;

    if-nez v0, :cond_5

    .line 1136646
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136647
    :cond_0
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136648
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136649
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1136650
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1136651
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1136652
    :cond_3
    const-string v0, ""

    goto/16 :goto_2

    .line 1136653
    :cond_4
    iget-object v4, p0, LX/6kP;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1136654
    :cond_5
    iget-object v0, p0, LX/6kP;->image:LX/6jZ;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1136614
    invoke-static {p0}, LX/6kP;->a(LX/6kP;)V

    .line 1136615
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1136616
    iget-object v0, p0, LX/6kP;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1136617
    sget-object v0, LX/6kP;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136618
    iget-object v0, p0, LX/6kP;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1136619
    :cond_0
    iget-object v0, p0, LX/6kP;->image:LX/6jZ;

    if-eqz v0, :cond_1

    .line 1136620
    iget-object v0, p0, LX/6kP;->image:LX/6jZ;

    if-eqz v0, :cond_1

    .line 1136621
    sget-object v0, LX/6kP;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1136622
    iget-object v0, p0, LX/6kP;->image:LX/6jZ;

    invoke-virtual {v0, p1}, LX/6jZ;->a(LX/1su;)V

    .line 1136623
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1136624
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1136625
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1136588
    if-nez p1, :cond_1

    .line 1136589
    :cond_0
    :goto_0
    return v0

    .line 1136590
    :cond_1
    instance-of v1, p1, LX/6kP;

    if-eqz v1, :cond_0

    .line 1136591
    check-cast p1, LX/6kP;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1136592
    if-nez p1, :cond_3

    .line 1136593
    :cond_2
    :goto_1
    move v0, v2

    .line 1136594
    goto :goto_0

    .line 1136595
    :cond_3
    iget-object v0, p0, LX/6kP;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1136596
    :goto_2
    iget-object v3, p1, LX/6kP;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1136597
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1136598
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136599
    iget-object v0, p0, LX/6kP;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6kP;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1136600
    :cond_5
    iget-object v0, p0, LX/6kP;->image:LX/6jZ;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1136601
    :goto_4
    iget-object v3, p1, LX/6kP;->image:LX/6jZ;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1136602
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1136603
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1136604
    iget-object v0, p0, LX/6kP;->image:LX/6jZ;

    iget-object v3, p1, LX/6kP;->image:LX/6jZ;

    invoke-virtual {v0, v3}, LX/6jZ;->a(LX/6jZ;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1136605
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1136606
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1136607
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1136608
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1136609
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1136613
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1136610
    sget-boolean v0, LX/6kP;->a:Z

    .line 1136611
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kP;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1136612
    return-object v0
.end method
