.class public final LX/80J;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1282609
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 1282610
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1282611
    :goto_0
    return v1

    .line 1282612
    :cond_0
    const-string v8, "undoable"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1282613
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    .line 1282614
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_6

    .line 1282615
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1282616
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1282617
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1282618
    const-string v8, "actions"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1282619
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1282620
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_2

    .line 1282621
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_2

    .line 1282622
    invoke-static {p0, p1}, LX/80I;->b(LX/15w;LX/186;)I

    move-result v7

    .line 1282623
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1282624
    :cond_2
    invoke-static {v6, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 1282625
    goto :goto_1

    .line 1282626
    :cond_3
    const-string v8, "current_action"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1282627
    const/4 v7, 0x0

    .line 1282628
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_c

    .line 1282629
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1282630
    :goto_3
    move v5, v7

    .line 1282631
    goto :goto_1

    .line 1282632
    :cond_4
    const-string v8, "feedback_text"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1282633
    const/4 v7, 0x0

    .line 1282634
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v8, :cond_10

    .line 1282635
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1282636
    :goto_4
    move v4, v7

    .line 1282637
    goto :goto_1

    .line 1282638
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1282639
    :cond_6
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1282640
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1282641
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 1282642
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1282643
    if-eqz v0, :cond_7

    .line 1282644
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 1282645
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1

    .line 1282646
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1282647
    :cond_a
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_b

    .line 1282648
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1282649
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1282650
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_a

    if-eqz v8, :cond_a

    .line 1282651
    const-string v9, "negative_feedback_action_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1282652
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_5

    .line 1282653
    :cond_b
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1282654
    invoke-virtual {p1, v7, v5}, LX/186;->b(II)V

    .line 1282655
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_3

    :cond_c
    move v5, v7

    goto :goto_5

    .line 1282656
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1282657
    :cond_e
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_f

    .line 1282658
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1282659
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1282660
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_e

    if-eqz v8, :cond_e

    .line 1282661
    const-string v9, "text"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 1282662
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_6

    .line 1282663
    :cond_f
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1282664
    invoke-virtual {p1, v7, v4}, LX/186;->b(II)V

    .line 1282665
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_4

    :cond_10
    move v4, v7

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1282666
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1282667
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1282668
    if-eqz v0, :cond_1

    .line 1282669
    const-string v1, "actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1282670
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1282671
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1282672
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/80I;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1282673
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1282674
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1282675
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1282676
    if-eqz v0, :cond_3

    .line 1282677
    const-string v1, "current_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1282678
    const/4 v2, 0x0

    .line 1282679
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1282680
    invoke-virtual {p0, v0, v2}, LX/15i;->g(II)I

    move-result v1

    .line 1282681
    if-eqz v1, :cond_2

    .line 1282682
    const-string v1, "negative_feedback_action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1282683
    invoke-virtual {p0, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1282684
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1282685
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1282686
    if-eqz v0, :cond_5

    .line 1282687
    const-string v1, "feedback_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1282688
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1282689
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1282690
    if-eqz v1, :cond_4

    .line 1282691
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1282692
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1282693
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1282694
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1282695
    if-eqz v0, :cond_6

    .line 1282696
    const-string v1, "undoable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1282697
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1282698
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1282699
    return-void
.end method
