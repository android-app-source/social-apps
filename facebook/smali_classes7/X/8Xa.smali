.class public LX/8Xa;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements LX/8VV;


# instance fields
.field public a:LX/8TS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/8TD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/8Sn;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1355221
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/8Xa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1355222
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1355219
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/8Xa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355220
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1355213
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355214
    const-class p1, LX/8Xa;

    invoke-static {p1, p0}, LX/8Xa;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1355215
    invoke-virtual {p0}, LX/8Xa;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f030776

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1355216
    invoke-static {p0}, LX/8Xa;->getGLEndgameCardFragment(LX/8Xa;)Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;

    move-result-object p1

    new-instance p2, LX/8XZ;

    invoke-direct {p2, p0}, LX/8XZ;-><init>(LX/8Xa;)V

    .line 1355217
    iput-object p2, p1, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->d:LX/8XH;

    .line 1355218
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/8Xa;

    invoke-static {p0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v1

    check-cast v1, LX/8TS;

    invoke-static {p0}, LX/8TD;->a(LX/0QB;)LX/8TD;

    move-result-object p0

    check-cast p0, LX/8TD;

    iput-object v1, p1, LX/8Xa;->a:LX/8TS;

    iput-object p0, p1, LX/8Xa;->b:LX/8TD;

    return-void
.end method

.method public static getGLEndgameCardFragment(LX/8Xa;)Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;
    .locals 2

    .prologue
    .line 1355211
    invoke-virtual {p0}, LX/8Xa;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/QuicksilverActivity;

    .line 1355212
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d13f5

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1355208
    invoke-static {p0}, LX/8Xa;->getGLEndgameCardFragment(LX/8Xa;)Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->b()V

    .line 1355209
    invoke-static {p0}, LX/8Xa;->getGLEndgameCardFragment(LX/8Xa;)Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->c()V

    .line 1355210
    return-void
.end method

.method public final a(Ljava/util/List;Ljava/util/List;LX/8Vb;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "LX/8Vb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1355206
    invoke-static {p0}, LX/8Xa;->getGLEndgameCardFragment(LX/8Xa;)Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->a(Ljava/util/List;Ljava/util/List;LX/8Vb;)V

    .line 1355207
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1355205
    invoke-static {p0}, LX/8Xa;->getGLEndgameCardFragment(LX/8Xa;)Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->d()Z

    move-result v0

    return v0
.end method

.method public setCallbackDelegate(LX/8Sn;)V
    .locals 0

    .prologue
    .line 1355201
    iput-object p1, p0, LX/8Xa;->c:LX/8Sn;

    .line 1355202
    return-void
.end method

.method public setScreenshot(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1355203
    invoke-static {p0}, LX/8Xa;->getGLEndgameCardFragment(LX/8Xa;)Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->a(Landroid/graphics/Bitmap;)V

    .line 1355204
    return-void
.end method
