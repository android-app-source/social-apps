.class public LX/7Xr;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements LX/12Q;


# instance fields
.field public a:LX/11x;

.field private final b:Landroid/content/res/Resources;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1218799
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7Xr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1218800
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1218797
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7Xr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1218798
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1218788
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1218789
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 1218790
    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, LX/7Xr;->b:Landroid/content/res/Resources;

    .line 1218791
    const v0, 0x7f030243

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1218792
    const v0, 0x7f0d08b4

    invoke-virtual {p0, v0}, LX/7Xr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/7Xr;->c:Landroid/widget/TextView;

    .line 1218793
    const v0, 0x7f0d08b5

    invoke-virtual {p0, v0}, LX/7Xr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/7Xr;->d:Landroid/widget/TextView;

    .line 1218794
    const v0, 0x7f0d08b6

    invoke-virtual {p0, v0}, LX/7Xr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LX/7Xr;->e:Landroid/widget/Button;

    .line 1218795
    iget-object v0, p0, LX/7Xr;->e:Landroid/widget/Button;

    new-instance v1, LX/7Xp;

    invoke-direct {v1, p0}, LX/7Xp;-><init>(LX/7Xr;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1218796
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1218786
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/7Xr;->setVisibility(I)V

    .line 1218787
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1218768
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/7Xr;->setVisibility(I)V

    .line 1218769
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1218785
    invoke-virtual {p0}, LX/7Xr;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1218784
    iget-object v0, p0, LX/7Xr;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setIndicatorData(Lcom/facebook/zero/sdk/request/ZeroIndicatorData;)V
    .locals 6

    .prologue
    const/16 v5, 0x21

    .line 1218772
    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;->d()Ljava/lang/String;

    move-result-object v0

    .line 1218773
    new-instance v1, LX/63A;

    invoke-direct {v1}, LX/63A;-><init>()V

    .line 1218774
    new-instance v2, LX/7Xq;

    invoke-direct {v2, p0, v0}, LX/7Xq;-><init>(LX/7Xr;Ljava/lang/String;)V

    .line 1218775
    iput-object v2, v1, LX/63A;->a:LX/639;

    .line 1218776
    new-instance v0, LX/47x;

    iget-object v2, p0, LX/7Xr;->b:Landroid/content/res/Resources;

    invoke-direct {v0, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1218777
    const v2, 0x7f080e46

    invoke-virtual {v0, v2}, LX/47x;->a(I)LX/47x;

    .line 1218778
    const-string v2, "[[content]]"

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4, v5}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 1218779
    const-string v2, "[[cta]]"

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1, v5}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 1218780
    iget-object v1, p0, LX/7Xr;->c:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1218781
    iget-object v1, p0, LX/7Xr;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1218782
    iget-object v0, p0, LX/7Xr;->d:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1218783
    return-void
.end method

.method public setListener(LX/11x;)V
    .locals 0

    .prologue
    .line 1218770
    iput-object p1, p0, LX/7Xr;->a:LX/11x;

    .line 1218771
    return-void
.end method
