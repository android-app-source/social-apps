.class public LX/6wJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wI",
        "<",
        "Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;",
        "LX/6va;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1157508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157509
    iput-object p1, p0, LX/6wJ;->a:Landroid/content/Context;

    .line 1157510
    return-void
.end method

.method private a(Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;LX/6vY;ILcom/facebook/payments/contactinfo/model/ContactInfo;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1157511
    iget-object v0, p1, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-static {v0}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v0

    .line 1157512
    iget-object v1, p0, LX/6wJ;->a:Landroid/content/Context;

    invoke-static {}, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->newBuilder()LX/6v2;

    move-result-object v2

    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v3

    .line 1157513
    iput-object v3, v2, LX/6v2;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1157514
    move-object v2, v2

    .line 1157515
    iput-object p2, v2, LX/6v2;->a:LX/6vY;

    .line 1157516
    move-object v2, v2

    .line 1157517
    iput-object v0, v2, LX/6v2;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1157518
    move-object v0, v2

    .line 1157519
    iput p3, v0, LX/6v2;->d:I

    .line 1157520
    move-object v0, v0

    .line 1157521
    iput-object p4, v0, LX/6v2;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1157522
    move-object v0, v0

    .line 1157523
    invoke-static {p1}, LX/6wJ;->a(Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;)Z

    move-result v2

    .line 1157524
    iput-boolean v2, v0, LX/6v2;->e:Z

    .line 1157525
    move-object v0, v0

    .line 1157526
    invoke-virtual {v0}, LX/6v2;->f()Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/6wJ;LX/0Pz;Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;LX/6vb;LX/6vY;Ljava/lang/String;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/6vm;",
            ">;",
            "Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;",
            "LX/6vb;",
            "LX/6vY;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1157527
    invoke-virtual {p2}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    .line 1157528
    invoke-virtual {p2}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->g()Lcom/facebook/payments/picker/model/CoreClientData;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/contactinfo/picker/ContactInfoCoreClientData;

    iget-object v1, v1, Lcom/facebook/payments/contactinfo/picker/ContactInfoCoreClientData;->a:LX/0Px;

    invoke-static {v1}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v1

    new-instance v2, LX/6wE;

    invoke-direct {v2, p0, p3}, LX/6wE;-><init>(LX/6wJ;LX/6vb;)V

    invoke-virtual {v1, v2}, LX/0wv;->a(LX/0Rl;)LX/0wv;

    move-result-object v1

    invoke-virtual {v1}, LX/0wv;->b()LX/0Px;

    move-result-object v9

    .line 1157529
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    .line 1157530
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v11

    const/4 v1, 0x0

    move v8, v1

    :goto_0
    if-ge v8, v11, :cond_0

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1157531
    new-instance v1, LX/6w8;

    move-object/from16 v0, p4

    invoke-direct {p0, v7, v0, v10, v5}, LX/6wJ;->a(Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;LX/6vY;ILcom/facebook/payments/contactinfo/model/ContactInfo;)Landroid/content/Intent;

    move-result-object v2

    const/16 v3, 0x1f6

    invoke-static {p2, v5}, LX/6wJ;->a(Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;Lcom/facebook/payments/contactinfo/model/ContactInfo;)Z

    move-result v4

    iget-object v6, v7, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->c:LX/71H;

    invoke-direct/range {v1 .. v6}, LX/6w8;-><init>(Landroid/content/Intent;IZLcom/facebook/payments/contactinfo/model/ContactInfo;LX/71H;)V

    invoke-virtual {p1, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1157532
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_0

    .line 1157533
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    iget-object v1, v1, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iget-object v1, v1, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iget-object v1, v1, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-static {v1}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v1

    .line 1157534
    new-instance v2, LX/6vn;

    invoke-static {}, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->newBuilder()LX/6v2;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, LX/6v2;->a(LX/6vY;)LX/6v2;

    move-result-object v3

    invoke-virtual {v3, v10}, LX/6v2;->a(I)LX/6v2;

    move-result-object v3

    invoke-static {v7}, LX/6wJ;->a(Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;)Z

    move-result v4

    invoke-virtual {v3, v4}, LX/6v2;->a(Z)LX/6v2;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/6v2;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6v2;

    move-result-object v1

    invoke-virtual {v1}, LX/6v2;->f()Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-direct {v2, v1, v0}, LX/6vn;-><init>(Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;Ljava/lang/String;)V

    invoke-virtual {p1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1157535
    return-void
.end method

.method private static a(Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;Lcom/facebook/payments/contactinfo/model/ContactInfo;)Z
    .locals 2

    .prologue
    .line 1157536
    invoke-interface {p1}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->d()LX/6vb;

    move-result-object v1

    invoke-virtual {v1}, LX/6vb;->getSectionType()LX/6va;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a(LX/6vZ;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static a(Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;)Z
    .locals 2

    .prologue
    .line 1157537
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->c:LX/71H;

    sget-object v1, LX/71H;->SELECTABLE:LX/71H;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;LX/0Px;)LX/0Px;
    .locals 12

    .prologue
    .line 1157538
    check-cast p1, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;

    .line 1157539
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1157540
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6va;

    .line 1157541
    sget-object v4, LX/6wH;->a:[I

    invoke-virtual {v0}, LX/6va;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1157542
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled section type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1157543
    :pswitch_0
    new-instance v6, LX/716;

    iget-object v7, p0, LX/6wJ;->a:Landroid/content/Context;

    const v8, 0x7f081e28

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, LX/716;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1157544
    sget-object v9, LX/6vb;->EMAIL:LX/6vb;

    sget-object v10, LX/6vY;->EMAIL:LX/6vY;

    iget-object v6, p0, LX/6wJ;->a:Landroid/content/Context;

    const v7, 0x7f081e3b

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object v6, p0

    move-object v7, v2

    move-object v8, p1

    invoke-static/range {v6 .. v11}, LX/6wJ;->a(LX/6wJ;LX/0Pz;Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;LX/6vb;LX/6vY;Ljava/lang/String;)V

    .line 1157545
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1157546
    :pswitch_1
    new-instance v6, LX/716;

    iget-object v7, p0, LX/6wJ;->a:Landroid/content/Context;

    const v8, 0x7f081e29

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, LX/716;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1157547
    sget-object v9, LX/6vb;->PHONE_NUMBER:LX/6vb;

    sget-object v10, LX/6vY;->PHONE_NUMBER:LX/6vY;

    iget-object v6, p0, LX/6wJ;->a:Landroid/content/Context;

    const v7, 0x7f081e3c

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object v6, p0

    move-object v7, v2

    move-object v8, p1

    invoke-static/range {v6 .. v11}, LX/6wJ;->a(LX/6wJ;LX/0Pz;Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;LX/6vb;LX/6vY;Ljava/lang/String;)V

    .line 1157548
    goto :goto_1

    .line 1157549
    :pswitch_2
    new-instance v0, LX/6wF;

    invoke-direct {v0}, LX/6wF;-><init>()V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1157550
    goto :goto_1

    .line 1157551
    :pswitch_3
    new-instance v0, LX/6wG;

    invoke-direct {v0}, LX/6wG;-><init>()V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1157552
    goto :goto_1

    .line 1157553
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
