.class public final LX/8Qc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 1343463
    iput-object p1, p0, LX/8Qc;->a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1343447
    iget-object v0, p0, LX/8Qc;->a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->k:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    sget-object v1, LX/3Oq;->FRIENDS:LX/0Px;

    .line 1343448
    iput-object v1, v0, LX/2RR;->c:Ljava/util/Collection;

    .line 1343449
    move-object v0, v0

    .line 1343450
    sget-object v1, LX/2RS;->NAME:LX/2RS;

    .line 1343451
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 1343452
    move-object v0, v0

    .line 1343453
    const/4 v1, 0x0

    .line 1343454
    iput-boolean v1, v0, LX/2RR;->o:Z

    .line 1343455
    move-object v0, v0

    .line 1343456
    iget-object v1, p0, LX/8Qc;->a:Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->c:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 1343457
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1343458
    :goto_0
    :try_start_0
    invoke-interface {v1}, LX/3On;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1343459
    invoke-interface {v1}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-static {v0}, LX/8uo;->a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 1343460
    new-instance v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-direct {v3, v0}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/User;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1343461
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/3On;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, LX/3On;->close()V

    .line 1343462
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
