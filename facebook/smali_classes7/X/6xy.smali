.class public final LX/6xy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/6yO;

.field public final b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

.field public final c:LX/6xg;

.field public d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

.field public e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

.field public f:Z

.field public g:Lcom/facebook/common/locale/Country;


# direct methods
.method public constructor <init>(LX/6yO;Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;LX/6xg;)V
    .locals 1

    .prologue
    .line 1159269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159270
    invoke-static {}, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->newBuilder()LX/6yR;

    move-result-object v0

    invoke-virtual {v0}, LX/6yR;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    move-result-object v0

    iput-object v0, p0, LX/6xy;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    .line 1159271
    sget-object v0, Lcom/facebook/common/locale/Country;->a:Lcom/facebook/common/locale/Country;

    iput-object v0, p0, LX/6xy;->g:Lcom/facebook/common/locale/Country;

    .line 1159272
    iput-object p1, p0, LX/6xy;->a:LX/6yO;

    .line 1159273
    iput-object p2, p0, LX/6xy;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    .line 1159274
    iput-object p3, p0, LX/6xy;->c:LX/6xg;

    .line 1159275
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;
    .locals 1

    .prologue
    .line 1159276
    new-instance v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    invoke-direct {v0, p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;-><init>(LX/6xy;)V

    return-object v0
.end method
