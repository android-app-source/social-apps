.class public LX/8SB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View$OnClickListener;

.field private c:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1346214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1346215
    iget-object v0, p0, LX/8SB;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1346216
    iget-object v1, p0, LX/8SB;->a:Landroid/view/View;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 1346217
    :cond_0
    iput-object p1, p0, LX/8SB;->b:Landroid/view/View$OnClickListener;

    .line 1346218
    return-void

    .line 1346219
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1346220
    iget-object v0, p0, LX/8SB;->a:Landroid/view/View;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0nE;->a(Z)V

    .line 1346221
    iput-object p1, p0, LX/8SB;->a:Landroid/view/View;

    .line 1346222
    const-string v0, "input_method"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, LX/8SB;->c:Landroid/view/inputmethod/InputMethodManager;

    .line 1346223
    iget-object v0, p0, LX/8SB;->b:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 1346224
    iget-object v0, p0, LX/8SB;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 1346225
    :cond_0
    iget-object v0, p0, LX/8SB;->a:Landroid/view/View;

    new-instance v1, LX/8SA;

    invoke-direct {v1, p0}, LX/8SA;-><init>(LX/8SB;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1346226
    return-void

    :cond_1
    move v0, v1

    .line 1346227
    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1346228
    iget-object v0, p0, LX/8SB;->b:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8SB;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1346229
    invoke-virtual {p0}, LX/8SB;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1346230
    :goto_0
    return-void

    .line 1346231
    :cond_0
    iget-object v0, p0, LX/8SB;->b:Landroid/view/View$OnClickListener;

    iget-object v1, p0, LX/8SB;->a:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1346232
    iget-object v0, p0, LX/8SB;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 1346233
    iget-object v0, p0, LX/8SB;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 1346234
    iget-object v0, p0, LX/8SB;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1346235
    iget-object v0, p0, LX/8SB;->c:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/8SB;->a:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1346236
    iget-object v0, p0, LX/8SB;->b:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 1346237
    iget-object v0, p0, LX/8SB;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 1346238
    :cond_0
    return-void
.end method
