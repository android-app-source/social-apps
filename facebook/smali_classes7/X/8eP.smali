.class public final LX/8eP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1382649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1382650
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1382651
    iget-object v1, p0, LX/8eP;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1382652
    iget-object v3, p0, LX/8eP;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1382653
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1382654
    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1382655
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1382656
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1382657
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1382658
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1382659
    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1382660
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1382661
    new-instance v1, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    invoke-direct {v1, v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;-><init>(LX/15i;)V

    .line 1382662
    iget-object v0, p0, LX/8eP;->b:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 1382663
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;->b()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iget-object v2, p0, LX/8eP;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->L_()LX/0x2;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/model/GraphQLStory;->a(LX/0x2;)V

    .line 1382664
    :cond_0
    return-object v1
.end method
