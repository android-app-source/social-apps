.class public LX/8cD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/8cD;


# instance fields
.field public final a:LX/0SG;

.field public final b:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1374090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1374091
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, LX/8cD;->b:Ljava/util/concurrent/atomic/AtomicLong;

    .line 1374092
    iput-object p1, p0, LX/8cD;->a:LX/0SG;

    .line 1374093
    return-void
.end method

.method public static a(LX/0QB;)LX/8cD;
    .locals 4

    .prologue
    .line 1374094
    sget-object v0, LX/8cD;->c:LX/8cD;

    if-nez v0, :cond_1

    .line 1374095
    const-class v1, LX/8cD;

    monitor-enter v1

    .line 1374096
    :try_start_0
    sget-object v0, LX/8cD;->c:LX/8cD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1374097
    if-eqz v2, :cond_0

    .line 1374098
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1374099
    new-instance p0, LX/8cD;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {p0, v3}, LX/8cD;-><init>(LX/0SG;)V

    .line 1374100
    move-object v0, p0

    .line 1374101
    sput-object v0, LX/8cD;->c:LX/8cD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1374102
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1374103
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1374104
    :cond_1
    sget-object v0, LX/8cD;->c:LX/8cD;

    return-object v0

    .line 1374105
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1374106
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
