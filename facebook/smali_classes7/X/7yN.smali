.class public LX/7yN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/7yN;


# instance fields
.field public a:Ljava/lang/Boolean;

.field public final b:LX/0Uh;

.field public final c:LX/1tw;


# direct methods
.method public constructor <init>(LX/0Uh;LX/1tw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1279208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279209
    iput-object p1, p0, LX/7yN;->b:LX/0Uh;

    .line 1279210
    iput-object p2, p0, LX/7yN;->c:LX/1tw;

    .line 1279211
    return-void
.end method

.method public static a(LX/0QB;)LX/7yN;
    .locals 5

    .prologue
    .line 1279212
    sget-object v0, LX/7yN;->d:LX/7yN;

    if-nez v0, :cond_1

    .line 1279213
    const-class v1, LX/7yN;

    monitor-enter v1

    .line 1279214
    :try_start_0
    sget-object v0, LX/7yN;->d:LX/7yN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1279215
    if-eqz v2, :cond_0

    .line 1279216
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1279217
    new-instance p0, LX/7yN;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/1tw;->a(LX/0QB;)LX/1tw;

    move-result-object v4

    check-cast v4, LX/1tw;

    invoke-direct {p0, v3, v4}, LX/7yN;-><init>(LX/0Uh;LX/1tw;)V

    .line 1279218
    move-object v0, p0

    .line 1279219
    sput-object v0, LX/7yN;->d:LX/7yN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1279220
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1279221
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1279222
    :cond_1
    sget-object v0, LX/7yN;->d:LX/7yN;

    return-object v0

    .line 1279223
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1279224
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
