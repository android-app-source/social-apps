.class public LX/7Bc;
.super LX/1qS;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/7Bc;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/7Bx;LX/7Bl;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1179217
    invoke-static {p4, p5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const-string v5, "search_bootstrap_db"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/1qS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V

    .line 1179218
    return-void
.end method

.method public static a(LX/0QB;)LX/7Bc;
    .locals 9

    .prologue
    .line 1179219
    sget-object v0, LX/7Bc;->a:LX/7Bc;

    if-nez v0, :cond_1

    .line 1179220
    const-class v1, LX/7Bc;

    monitor-enter v1

    .line 1179221
    :try_start_0
    sget-object v0, LX/7Bc;->a:LX/7Bc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1179222
    if-eqz v2, :cond_0

    .line 1179223
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1179224
    new-instance v3, LX/7Bc;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v5

    check-cast v5, LX/0Tt;

    invoke-static {v0}, LX/1qT;->a(LX/0QB;)LX/1qT;

    move-result-object v6

    check-cast v6, LX/1qU;

    invoke-static {v0}, LX/7Bx;->a(LX/0QB;)LX/7Bx;

    move-result-object v7

    check-cast v7, LX/7Bx;

    invoke-static {v0}, LX/7Bl;->a(LX/0QB;)LX/7Bl;

    move-result-object v8

    check-cast v8, LX/7Bl;

    invoke-direct/range {v3 .. v8}, LX/7Bc;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/7Bx;LX/7Bl;)V

    .line 1179225
    move-object v0, v3

    .line 1179226
    sput-object v0, LX/7Bc;->a:LX/7Bc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1179227
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1179228
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1179229
    :cond_1
    sget-object v0, LX/7Bc;->a:LX/7Bc;

    return-object v0

    .line 1179230
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1179231
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
