.class public final enum LX/7gQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gQ;

.field public static final enum AUDIENCE_DESELECT_ALL:LX/7gQ;

.field public static final enum AUDIENCE_SELECTION_FINISHED:LX/7gQ;

.field public static final enum AUDIENCE_SELECTION_STARTED:LX/7gQ;

.field public static final enum AUDIENCE_SELECT_ALL:LX/7gQ;

.field public static final enum CAMERA_CAMERA_ADD_CAPTION:LX/7gQ;

.field public static final enum CAMERA_CANCEL:LX/7gQ;

.field public static final enum CAMERA_MEDIA_PREVIEW:LX/7gQ;

.field public static final enum CAMERA_OPEN:LX/7gQ;

.field public static final enum CAMERA_PHOTO_CAPTURE:LX/7gQ;

.field public static final enum CAMERA_PHOTO_CAPTURE_FRONT:LX/7gQ;

.field public static final enum CAMERA_VIDEO_CAPTURE:LX/7gQ;

.field public static final enum CAMERA_VIDEO_CAPTURE_FRONT:LX/7gQ;

.field public static final enum CLOSE:LX/7gQ;

.field public static final enum CLOSED_SELF_STACK:LX/7gQ;

.field public static final enum CLOSED_STACK:LX/7gQ;

.field public static final enum CLOSE_REPLY_LIST:LX/7gQ;

.field public static final enum EMOJI_REPLY_MENU_DISMISSED:LX/7gQ;

.field public static final enum EMOJI_REPLY_MENU_OPEN:LX/7gQ;

.field public static final enum EMOJI_REPLY_SELECTED:LX/7gQ;

.field public static final enum ENTRY:LX/7gQ;

.field public static final enum HIDE_STACK:LX/7gQ;

.field public static final enum IMPORT_CANCEL:LX/7gQ;

.field public static final enum IMPORT_ENTER:LX/7gQ;

.field public static final enum IMPORT_GALLERY_DESELECT_ALL:LX/7gQ;

.field public static final enum IMPORT_GALLERY_SELECT_ALL:LX/7gQ;

.field public static final enum IMPORT_GRID_TO_PUBLISHER:LX/7gQ;

.field public static final enum IMPORT_PUBLISHER_TO_GRID:LX/7gQ;

.field public static final enum IMPORT_SHARE:LX/7gQ;

.field public static final enum MESSENGER_REPLY:LX/7gQ;

.field public static final enum NUX_CANCEL:LX/7gQ;

.field public static final enum NUX_COMPLETE:LX/7gQ;

.field public static final enum NUX_START:LX/7gQ;

.field public static final enum NUX_STEP_TWO_SHOWN:LX/7gQ;

.field public static final enum OPENED_SELF_STACK:LX/7gQ;

.field public static final enum OPENED_STACK:LX/7gQ;

.field public static final enum OPEN_REPLY_LIST:LX/7gQ;

.field public static final enum OPEN_REPLY_THREAD:LX/7gQ;

.field public static final enum SAVE_SELF_STACK:LX/7gQ;

.field public static final enum SUBMITTED_REPLY:LX/7gQ;

.field public static final enum UPLOAD_COMPLETED:LX/7gQ;

.field public static final enum UPLOAD_FAILED:LX/7gQ;

.field public static final enum UPLOAD_STARTED:LX/7gQ;

.field public static final enum VIEW_STACK_STORY:LX/7gQ;


# instance fields
.field private mModuleName:Ljava/lang/String;

.field private mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1224239
    new-instance v0, LX/7gQ;

    const-string v1, "ENTRY"

    const-string v2, "backstage_entry"

    const-string v3, "infra"

    invoke-direct {v0, v1, v5, v2, v3}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->ENTRY:LX/7gQ;

    .line 1224240
    new-instance v0, LX/7gQ;

    const-string v1, "CLOSE"

    const-string v2, "backstage_exit"

    const-string v3, "infra"

    invoke-direct {v0, v1, v6, v2, v3}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->CLOSE:LX/7gQ;

    .line 1224241
    new-instance v0, LX/7gQ;

    const-string v1, "IMPORT_SHARE"

    const-string v2, "backstage_import_share"

    const-string v3, "production"

    invoke-direct {v0, v1, v7, v2, v3}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->IMPORT_SHARE:LX/7gQ;

    .line 1224242
    new-instance v0, LX/7gQ;

    const-string v1, "IMPORT_CANCEL"

    const-string v2, "backstage_import_cancel"

    const-string v3, "production"

    invoke-direct {v0, v1, v8, v2, v3}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->IMPORT_CANCEL:LX/7gQ;

    .line 1224243
    new-instance v0, LX/7gQ;

    const-string v1, "IMPORT_PUBLISHER_TO_GRID"

    const-string v2, "backstage_publisher_to_grid"

    const-string v3, "production"

    invoke-direct {v0, v1, v9, v2, v3}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->IMPORT_PUBLISHER_TO_GRID:LX/7gQ;

    .line 1224244
    new-instance v0, LX/7gQ;

    const-string v1, "IMPORT_GALLERY_SELECT_ALL"

    const/4 v2, 0x5

    const-string v3, "backstage_import_select_all"

    const-string v4, "production"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->IMPORT_GALLERY_SELECT_ALL:LX/7gQ;

    .line 1224245
    new-instance v0, LX/7gQ;

    const-string v1, "IMPORT_GALLERY_DESELECT_ALL"

    const/4 v2, 0x6

    const-string v3, "backstage_import_deselect_all"

    const-string v4, "production"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->IMPORT_GALLERY_DESELECT_ALL:LX/7gQ;

    .line 1224246
    new-instance v0, LX/7gQ;

    const-string v1, "IMPORT_GRID_TO_PUBLISHER"

    const/4 v2, 0x7

    const-string v3, "backstage_entered_publish_step"

    const-string v4, "production"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->IMPORT_GRID_TO_PUBLISHER:LX/7gQ;

    .line 1224247
    new-instance v0, LX/7gQ;

    const-string v1, "IMPORT_ENTER"

    const/16 v2, 0x8

    const-string v3, "backstage_entered_import_gallery"

    const-string v4, "production"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->IMPORT_ENTER:LX/7gQ;

    .line 1224248
    new-instance v0, LX/7gQ;

    const-string v1, "EMOJI_REPLY_MENU_OPEN"

    const/16 v2, 0x9

    const-string v3, "backstage_emote_flow_started"

    const-string v4, "conversations"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->EMOJI_REPLY_MENU_OPEN:LX/7gQ;

    .line 1224249
    new-instance v0, LX/7gQ;

    const-string v1, "EMOJI_REPLY_MENU_DISMISSED"

    const/16 v2, 0xa

    const-string v3, "backstage_emote_flow_abandoned"

    const-string v4, "conversations"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->EMOJI_REPLY_MENU_DISMISSED:LX/7gQ;

    .line 1224250
    new-instance v0, LX/7gQ;

    const-string v1, "EMOJI_REPLY_SELECTED"

    const/16 v2, 0xb

    const-string v3, "backstage_emote_flow_sent"

    const-string v4, "conversations"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->EMOJI_REPLY_SELECTED:LX/7gQ;

    .line 1224251
    new-instance v0, LX/7gQ;

    const-string v1, "NUX_START"

    const/16 v2, 0xc

    const-string v3, "backstage_onboarding_start"

    const-string v4, "onboarding"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->NUX_START:LX/7gQ;

    .line 1224252
    new-instance v0, LX/7gQ;

    const-string v1, "NUX_STEP_TWO_SHOWN"

    const/16 v2, 0xd

    const-string v3, "backstage_onboarding_step2"

    const-string v4, "onboarding"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->NUX_STEP_TWO_SHOWN:LX/7gQ;

    .line 1224253
    new-instance v0, LX/7gQ;

    const-string v1, "NUX_CANCEL"

    const/16 v2, 0xe

    const-string v3, "backstage_onboarding_cancel"

    const-string v4, "onboarding"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->NUX_CANCEL:LX/7gQ;

    .line 1224254
    new-instance v0, LX/7gQ;

    const-string v1, "NUX_COMPLETE"

    const/16 v2, 0xf

    const-string v3, "backstage_onboarding_complete"

    const-string v4, "onboarding"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->NUX_COMPLETE:LX/7gQ;

    .line 1224255
    new-instance v0, LX/7gQ;

    const-string v1, "CAMERA_OPEN"

    const/16 v2, 0x10

    const-string v3, "backstage_enter_camera"

    const-string v4, "camera"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->CAMERA_OPEN:LX/7gQ;

    .line 1224256
    new-instance v0, LX/7gQ;

    const-string v1, "CAMERA_CANCEL"

    const/16 v2, 0x11

    const-string v3, "backstage_camera_cancel"

    const-string v4, "camera"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->CAMERA_CANCEL:LX/7gQ;

    .line 1224257
    new-instance v0, LX/7gQ;

    const-string v1, "CAMERA_PHOTO_CAPTURE"

    const/16 v2, 0x12

    const-string v3, "backstage_camera_photo"

    const-string v4, "camera"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->CAMERA_PHOTO_CAPTURE:LX/7gQ;

    .line 1224258
    new-instance v0, LX/7gQ;

    const-string v1, "CAMERA_VIDEO_CAPTURE"

    const/16 v2, 0x13

    const-string v3, "backstage_camera_video"

    const-string v4, "camera"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->CAMERA_VIDEO_CAPTURE:LX/7gQ;

    .line 1224259
    new-instance v0, LX/7gQ;

    const-string v1, "CAMERA_PHOTO_CAPTURE_FRONT"

    const/16 v2, 0x14

    const-string v3, "backstage_camera_photo_front"

    const-string v4, "camera"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->CAMERA_PHOTO_CAPTURE_FRONT:LX/7gQ;

    .line 1224260
    new-instance v0, LX/7gQ;

    const-string v1, "CAMERA_VIDEO_CAPTURE_FRONT"

    const/16 v2, 0x15

    const-string v3, "backstage_camera_video_front"

    const-string v4, "camera"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->CAMERA_VIDEO_CAPTURE_FRONT:LX/7gQ;

    .line 1224261
    new-instance v0, LX/7gQ;

    const-string v1, "CAMERA_MEDIA_PREVIEW"

    const/16 v2, 0x16

    const-string v3, "backstage_camera_publish"

    const-string v4, "camera"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->CAMERA_MEDIA_PREVIEW:LX/7gQ;

    .line 1224262
    new-instance v0, LX/7gQ;

    const-string v1, "CAMERA_CAMERA_ADD_CAPTION"

    const/16 v2, 0x17

    const-string v3, "backstage_camera_add_caption"

    const-string v4, "camera"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->CAMERA_CAMERA_ADD_CAPTION:LX/7gQ;

    .line 1224263
    new-instance v0, LX/7gQ;

    const-string v1, "AUDIENCE_SELECTION_STARTED"

    const/16 v2, 0x18

    const-string v3, "backstage_audience_start"

    const-string v4, "infra"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->AUDIENCE_SELECTION_STARTED:LX/7gQ;

    .line 1224264
    new-instance v0, LX/7gQ;

    const-string v1, "AUDIENCE_SELECTION_FINISHED"

    const/16 v2, 0x19

    const-string v3, "backstage_audience_done"

    const-string v4, "infra"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->AUDIENCE_SELECTION_FINISHED:LX/7gQ;

    .line 1224265
    new-instance v0, LX/7gQ;

    const-string v1, "AUDIENCE_SELECT_ALL"

    const/16 v2, 0x1a

    const-string v3, "backstage_audience_select_all"

    const-string v4, "infra"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->AUDIENCE_SELECT_ALL:LX/7gQ;

    .line 1224266
    new-instance v0, LX/7gQ;

    const-string v1, "AUDIENCE_DESELECT_ALL"

    const/16 v2, 0x1b

    const-string v3, "backstage_audience_deselect_all"

    const-string v4, "infra"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->AUDIENCE_DESELECT_ALL:LX/7gQ;

    .line 1224267
    new-instance v0, LX/7gQ;

    const-string v1, "UPLOAD_STARTED"

    const/16 v2, 0x1c

    const-string v3, "backstage_upload_start"

    const-string v4, "production"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->UPLOAD_STARTED:LX/7gQ;

    .line 1224268
    new-instance v0, LX/7gQ;

    const-string v1, "UPLOAD_COMPLETED"

    const/16 v2, 0x1d

    const-string v3, "backstage_upload_complete"

    const-string v4, "production"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->UPLOAD_COMPLETED:LX/7gQ;

    .line 1224269
    new-instance v0, LX/7gQ;

    const-string v1, "UPLOAD_FAILED"

    const/16 v2, 0x1e

    const-string v3, "backstage_upload_fail"

    const-string v4, "production"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->UPLOAD_FAILED:LX/7gQ;

    .line 1224270
    new-instance v0, LX/7gQ;

    const-string v1, "OPENED_SELF_STACK"

    const/16 v2, 0x1f

    const-string v3, "backstage_selfstack_open"

    const-string v4, "consumption"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->OPENED_SELF_STACK:LX/7gQ;

    .line 1224271
    new-instance v0, LX/7gQ;

    const-string v1, "CLOSED_SELF_STACK"

    const/16 v2, 0x20

    const-string v3, "backstage_selfstack_close"

    const-string v4, "consumption"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->CLOSED_SELF_STACK:LX/7gQ;

    .line 1224272
    new-instance v0, LX/7gQ;

    const-string v1, "SAVE_SELF_STACK"

    const/16 v2, 0x21

    const-string v3, "backstage_camera_roll_save"

    const-string v4, "consumption"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->SAVE_SELF_STACK:LX/7gQ;

    .line 1224273
    new-instance v0, LX/7gQ;

    const-string v1, "OPENED_STACK"

    const/16 v2, 0x22

    const-string v3, "backstage_stack_open"

    const-string v4, "consumption"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->OPENED_STACK:LX/7gQ;

    .line 1224274
    new-instance v0, LX/7gQ;

    const-string v1, "CLOSED_STACK"

    const/16 v2, 0x23

    const-string v3, "backstage_stack_close"

    const-string v4, "consumption"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->CLOSED_STACK:LX/7gQ;

    .line 1224275
    new-instance v0, LX/7gQ;

    const-string v1, "VIEW_STACK_STORY"

    const/16 v2, 0x24

    const-string v3, "backstage_stack_impression"

    const-string v4, "consumption"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->VIEW_STACK_STORY:LX/7gQ;

    .line 1224276
    new-instance v0, LX/7gQ;

    const-string v1, "HIDE_STACK"

    const/16 v2, 0x25

    const-string v3, "backstage_stack_hide"

    const-string v4, "consumption"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->HIDE_STACK:LX/7gQ;

    .line 1224277
    new-instance v0, LX/7gQ;

    const-string v1, "OPEN_REPLY_LIST"

    const/16 v2, 0x26

    const-string v3, "backstage_reactions_list_open"

    const-string v4, "consumption"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->OPEN_REPLY_LIST:LX/7gQ;

    .line 1224278
    new-instance v0, LX/7gQ;

    const-string v1, "CLOSE_REPLY_LIST"

    const/16 v2, 0x27

    const-string v3, "backstage_reactions_list_closed"

    const-string v4, "consumption"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->CLOSE_REPLY_LIST:LX/7gQ;

    .line 1224279
    new-instance v0, LX/7gQ;

    const-string v1, "OPEN_REPLY_THREAD"

    const/16 v2, 0x28

    const-string v3, "backstage_reactions_thread_open"

    const-string v4, "consumption"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->OPEN_REPLY_THREAD:LX/7gQ;

    .line 1224280
    new-instance v0, LX/7gQ;

    const-string v1, "SUBMITTED_REPLY"

    const/16 v2, 0x29

    const-string v3, "backstage_reactions_flow_sent"

    const-string v4, "conversations"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->SUBMITTED_REPLY:LX/7gQ;

    .line 1224281
    new-instance v0, LX/7gQ;

    const-string v1, "MESSENGER_REPLY"

    const/16 v2, 0x2a

    const-string v3, "backstage_messenger_reply_sent"

    const-string v4, "conversations"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gQ;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gQ;->MESSENGER_REPLY:LX/7gQ;

    .line 1224282
    const/16 v0, 0x2b

    new-array v0, v0, [LX/7gQ;

    sget-object v1, LX/7gQ;->ENTRY:LX/7gQ;

    aput-object v1, v0, v5

    sget-object v1, LX/7gQ;->CLOSE:LX/7gQ;

    aput-object v1, v0, v6

    sget-object v1, LX/7gQ;->IMPORT_SHARE:LX/7gQ;

    aput-object v1, v0, v7

    sget-object v1, LX/7gQ;->IMPORT_CANCEL:LX/7gQ;

    aput-object v1, v0, v8

    sget-object v1, LX/7gQ;->IMPORT_PUBLISHER_TO_GRID:LX/7gQ;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/7gQ;->IMPORT_GALLERY_SELECT_ALL:LX/7gQ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7gQ;->IMPORT_GALLERY_DESELECT_ALL:LX/7gQ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7gQ;->IMPORT_GRID_TO_PUBLISHER:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7gQ;->IMPORT_ENTER:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7gQ;->EMOJI_REPLY_MENU_OPEN:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7gQ;->EMOJI_REPLY_MENU_DISMISSED:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7gQ;->EMOJI_REPLY_SELECTED:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7gQ;->NUX_START:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/7gQ;->NUX_STEP_TWO_SHOWN:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/7gQ;->NUX_CANCEL:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/7gQ;->NUX_COMPLETE:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/7gQ;->CAMERA_OPEN:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/7gQ;->CAMERA_CANCEL:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/7gQ;->CAMERA_PHOTO_CAPTURE:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/7gQ;->CAMERA_VIDEO_CAPTURE:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/7gQ;->CAMERA_PHOTO_CAPTURE_FRONT:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/7gQ;->CAMERA_VIDEO_CAPTURE_FRONT:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/7gQ;->CAMERA_MEDIA_PREVIEW:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/7gQ;->CAMERA_CAMERA_ADD_CAPTION:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/7gQ;->AUDIENCE_SELECTION_STARTED:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/7gQ;->AUDIENCE_SELECTION_FINISHED:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/7gQ;->AUDIENCE_SELECT_ALL:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/7gQ;->AUDIENCE_DESELECT_ALL:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/7gQ;->UPLOAD_STARTED:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/7gQ;->UPLOAD_COMPLETED:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/7gQ;->UPLOAD_FAILED:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/7gQ;->OPENED_SELF_STACK:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/7gQ;->CLOSED_SELF_STACK:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/7gQ;->SAVE_SELF_STACK:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/7gQ;->OPENED_STACK:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/7gQ;->CLOSED_STACK:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/7gQ;->VIEW_STACK_STORY:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/7gQ;->HIDE_STACK:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/7gQ;->OPEN_REPLY_LIST:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/7gQ;->CLOSE_REPLY_LIST:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/7gQ;->OPEN_REPLY_THREAD:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/7gQ;->SUBMITTED_REPLY:LX/7gQ;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/7gQ;->MESSENGER_REPLY:LX/7gQ;

    aput-object v2, v0, v1

    sput-object v0, LX/7gQ;->$VALUES:[LX/7gQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1224283
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1224284
    iput-object p3, p0, LX/7gQ;->mName:Ljava/lang/String;

    .line 1224285
    iput-object p4, p0, LX/7gQ;->mModuleName:Ljava/lang/String;

    .line 1224286
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gQ;
    .locals 1

    .prologue
    .line 1224235
    const-class v0, LX/7gQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gQ;

    return-object v0
.end method

.method public static values()[LX/7gQ;
    .locals 1

    .prologue
    .line 1224238
    sget-object v0, LX/7gQ;->$VALUES:[LX/7gQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gQ;

    return-object v0
.end method


# virtual methods
.method public final getModuleName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224237
    iget-object v0, p0, LX/7gQ;->mModuleName:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224236
    iget-object v0, p0, LX/7gQ;->mName:Ljava/lang/String;

    return-object v0
.end method
