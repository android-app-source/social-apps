.class public final enum LX/7Rr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Rr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Rr;

.field public static final enum BOTTOM_LEFT:LX/7Rr;

.field public static final enum BOTTOM_RIGHT:LX/7Rr;

.field public static final enum TOP_LEFT:LX/7Rr;

.field public static final enum TOP_RIGHT:LX/7Rr;

.field public static final enum UNDOCKED:LX/7Rr;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1207932
    new-instance v0, LX/7Rr;

    const-string v1, "TOP_LEFT"

    const-string v2, "top_left"

    invoke-direct {v0, v1, v3, v2}, LX/7Rr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rr;->TOP_LEFT:LX/7Rr;

    .line 1207933
    new-instance v0, LX/7Rr;

    const-string v1, "TOP_RIGHT"

    const-string v2, "top_right"

    invoke-direct {v0, v1, v4, v2}, LX/7Rr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rr;->TOP_RIGHT:LX/7Rr;

    .line 1207934
    new-instance v0, LX/7Rr;

    const-string v1, "BOTTOM_LEFT"

    const-string v2, "bottom_left"

    invoke-direct {v0, v1, v5, v2}, LX/7Rr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rr;->BOTTOM_LEFT:LX/7Rr;

    .line 1207935
    new-instance v0, LX/7Rr;

    const-string v1, "BOTTOM_RIGHT"

    const-string v2, "bottom_right"

    invoke-direct {v0, v1, v6, v2}, LX/7Rr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rr;->BOTTOM_RIGHT:LX/7Rr;

    .line 1207936
    new-instance v0, LX/7Rr;

    const-string v1, "UNDOCKED"

    const-string v2, "undocked"

    invoke-direct {v0, v1, v7, v2}, LX/7Rr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Rr;->UNDOCKED:LX/7Rr;

    .line 1207937
    const/4 v0, 0x5

    new-array v0, v0, [LX/7Rr;

    sget-object v1, LX/7Rr;->TOP_LEFT:LX/7Rr;

    aput-object v1, v0, v3

    sget-object v1, LX/7Rr;->TOP_RIGHT:LX/7Rr;

    aput-object v1, v0, v4

    sget-object v1, LX/7Rr;->BOTTOM_LEFT:LX/7Rr;

    aput-object v1, v0, v5

    sget-object v1, LX/7Rr;->BOTTOM_RIGHT:LX/7Rr;

    aput-object v1, v0, v6

    sget-object v1, LX/7Rr;->UNDOCKED:LX/7Rr;

    aput-object v1, v0, v7

    sput-object v0, LX/7Rr;->$VALUES:[LX/7Rr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1207921
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1207922
    iput-object p3, p0, LX/7Rr;->value:Ljava/lang/String;

    .line 1207923
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/7Rr;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1207924
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1207925
    :cond_0
    :goto_0
    return-object v0

    .line 1207926
    :cond_1
    invoke-static {}, LX/7Rr;->values()[LX/7Rr;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 1207927
    iget-object v5, v1, LX/7Rr;->value:Ljava/lang/String;

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 1207928
    goto :goto_0

    .line 1207929
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Rr;
    .locals 1

    .prologue
    .line 1207930
    const-class v0, LX/7Rr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Rr;

    return-object v0
.end method

.method public static values()[LX/7Rr;
    .locals 1

    .prologue
    .line 1207931
    sget-object v0, LX/7Rr;->$VALUES:[LX/7Rr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Rr;

    return-object v0
.end method
