.class public LX/8H5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/data/method/DeletePhotoTagParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320446
    const-class v0, LX/8H5;

    sput-object v0, LX/8H5;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320448
    return-void
.end method

.method public static a(LX/0QB;)LX/8H5;
    .locals 1

    .prologue
    .line 1320449
    new-instance v0, LX/8H5;

    invoke-direct {v0}, LX/8H5;-><init>()V

    .line 1320450
    move-object v0, v0

    .line 1320451
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1320452
    check-cast p1, Lcom/facebook/photos/data/method/DeletePhotoTagParams;

    .line 1320453
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320454
    iget-object v0, p1, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1320455
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320456
    invoke-virtual {p1}, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->d()Ljava/lang/String;

    .line 1320457
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    sget-object v1, LX/8H5;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1320458
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1320459
    move-object v0, v0

    .line 1320460
    const-string v1, "DELETE"

    .line 1320461
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1320462
    move-object v0, v0

    .line 1320463
    iget-object v1, p1, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1320464
    const-string v2, "%s/tags"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v1, v3, p0

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1320465
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 1320466
    move-object v0, v0

    .line 1320467
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tags"

    invoke-virtual {p1}, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1320468
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1320469
    move-object v0, v0

    .line 1320470
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1320471
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1320472
    move-object v0, v0

    .line 1320473
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1320474
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1320475
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1320476
    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
