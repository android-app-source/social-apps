.class public final enum LX/7iL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7iL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7iL;

.field public static final enum AD_ID:LX/7iL;

.field public static final enum COLLECTION_ID:LX/7iL;

.field public static final enum COMPONENT:LX/7iL;

.field public static final enum CONTAINER_TYPE:LX/7iL;

.field public static final enum EVENT:LX/7iL;

.field public static final enum EVENTS:LX/7iL;

.field public static final enum LOGGER_CREATION_TIME:LX/7iL;

.field public static final enum LOGGING_EVENT_TIME:LX/7iL;

.field public static final enum LOGGING_START_TIME:LX/7iL;

.field public static final enum LOGGING_STOP_TIME:LX/7iL;

.field public static final enum LOG_ONLY_SUBEVENTS:LX/7iL;

.field public static final enum PAGE_ID:LX/7iL;

.field public static final enum POST_ID:LX/7iL;

.field public static final enum PRODUCT_ID:LX/7iL;

.field public static final enum REF_ID:LX/7iL;

.field public static final enum REF_TYPE:LX/7iL;

.field public static final enum SECTION_TYPE:LX/7iL;

.field public static final enum SESSION_ID:LX/7iL;

.field public static final enum TOP_LEVEL_POST_ID:LX/7iL;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1227390
    new-instance v0, LX/7iL;

    const-string v1, "AD_ID"

    const-string v2, "ad_id"

    invoke-direct {v0, v1, v4, v2}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->AD_ID:LX/7iL;

    .line 1227391
    new-instance v0, LX/7iL;

    const-string v1, "COLLECTION_ID"

    const-string v2, "collection_id"

    invoke-direct {v0, v1, v5, v2}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->COLLECTION_ID:LX/7iL;

    .line 1227392
    new-instance v0, LX/7iL;

    const-string v1, "COMPONENT"

    const-string v2, "component"

    invoke-direct {v0, v1, v6, v2}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->COMPONENT:LX/7iL;

    .line 1227393
    new-instance v0, LX/7iL;

    const-string v1, "CONTAINER_TYPE"

    const-string v2, "container_type"

    invoke-direct {v0, v1, v7, v2}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->CONTAINER_TYPE:LX/7iL;

    .line 1227394
    new-instance v0, LX/7iL;

    const-string v1, "EVENT"

    const-string v2, "event"

    invoke-direct {v0, v1, v8, v2}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->EVENT:LX/7iL;

    .line 1227395
    new-instance v0, LX/7iL;

    const-string v1, "EVENTS"

    const/4 v2, 0x5

    const-string v3, "events"

    invoke-direct {v0, v1, v2, v3}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->EVENTS:LX/7iL;

    .line 1227396
    new-instance v0, LX/7iL;

    const-string v1, "LOG_ONLY_SUBEVENTS"

    const/4 v2, 0x6

    const-string v3, "log_only_subevents"

    invoke-direct {v0, v1, v2, v3}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->LOG_ONLY_SUBEVENTS:LX/7iL;

    .line 1227397
    new-instance v0, LX/7iL;

    const-string v1, "LOGGER_CREATION_TIME"

    const/4 v2, 0x7

    const-string v3, "logger_creation_time"

    invoke-direct {v0, v1, v2, v3}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->LOGGER_CREATION_TIME:LX/7iL;

    .line 1227398
    new-instance v0, LX/7iL;

    const-string v1, "LOGGING_START_TIME"

    const/16 v2, 0x8

    const-string v3, "logging_start_time"

    invoke-direct {v0, v1, v2, v3}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->LOGGING_START_TIME:LX/7iL;

    .line 1227399
    new-instance v0, LX/7iL;

    const-string v1, "LOGGING_STOP_TIME"

    const/16 v2, 0x9

    const-string v3, "logging_stop_time"

    invoke-direct {v0, v1, v2, v3}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->LOGGING_STOP_TIME:LX/7iL;

    .line 1227400
    new-instance v0, LX/7iL;

    const-string v1, "LOGGING_EVENT_TIME"

    const/16 v2, 0xa

    const-string v3, "logging_event_time"

    invoke-direct {v0, v1, v2, v3}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->LOGGING_EVENT_TIME:LX/7iL;

    .line 1227401
    new-instance v0, LX/7iL;

    const-string v1, "POST_ID"

    const/16 v2, 0xb

    const-string v3, "post_id"

    invoke-direct {v0, v1, v2, v3}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->POST_ID:LX/7iL;

    .line 1227402
    new-instance v0, LX/7iL;

    const-string v1, "PRODUCT_ID"

    const/16 v2, 0xc

    const-string v3, "product_id"

    invoke-direct {v0, v1, v2, v3}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->PRODUCT_ID:LX/7iL;

    .line 1227403
    new-instance v0, LX/7iL;

    const-string v1, "PAGE_ID"

    const/16 v2, 0xd

    const-string v3, "page_id"

    invoke-direct {v0, v1, v2, v3}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->PAGE_ID:LX/7iL;

    .line 1227404
    new-instance v0, LX/7iL;

    const-string v1, "REF_ID"

    const/16 v2, 0xe

    const-string v3, "ref_id"

    invoke-direct {v0, v1, v2, v3}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->REF_ID:LX/7iL;

    .line 1227405
    new-instance v0, LX/7iL;

    const-string v1, "REF_TYPE"

    const/16 v2, 0xf

    const-string v3, "ref_type"

    invoke-direct {v0, v1, v2, v3}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->REF_TYPE:LX/7iL;

    .line 1227406
    new-instance v0, LX/7iL;

    const-string v1, "SECTION_TYPE"

    const/16 v2, 0x10

    const-string v3, "section_type"

    invoke-direct {v0, v1, v2, v3}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->SECTION_TYPE:LX/7iL;

    .line 1227407
    new-instance v0, LX/7iL;

    const-string v1, "TOP_LEVEL_POST_ID"

    const/16 v2, 0x11

    const-string v3, "top_level_post_id"

    invoke-direct {v0, v1, v2, v3}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->TOP_LEVEL_POST_ID:LX/7iL;

    .line 1227408
    new-instance v0, LX/7iL;

    const-string v1, "SESSION_ID"

    const/16 v2, 0x12

    const-string v3, "session_id"

    invoke-direct {v0, v1, v2, v3}, LX/7iL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7iL;->SESSION_ID:LX/7iL;

    .line 1227409
    const/16 v0, 0x13

    new-array v0, v0, [LX/7iL;

    sget-object v1, LX/7iL;->AD_ID:LX/7iL;

    aput-object v1, v0, v4

    sget-object v1, LX/7iL;->COLLECTION_ID:LX/7iL;

    aput-object v1, v0, v5

    sget-object v1, LX/7iL;->COMPONENT:LX/7iL;

    aput-object v1, v0, v6

    sget-object v1, LX/7iL;->CONTAINER_TYPE:LX/7iL;

    aput-object v1, v0, v7

    sget-object v1, LX/7iL;->EVENT:LX/7iL;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7iL;->EVENTS:LX/7iL;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7iL;->LOG_ONLY_SUBEVENTS:LX/7iL;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7iL;->LOGGER_CREATION_TIME:LX/7iL;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7iL;->LOGGING_START_TIME:LX/7iL;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7iL;->LOGGING_STOP_TIME:LX/7iL;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7iL;->LOGGING_EVENT_TIME:LX/7iL;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7iL;->POST_ID:LX/7iL;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7iL;->PRODUCT_ID:LX/7iL;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/7iL;->PAGE_ID:LX/7iL;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/7iL;->REF_ID:LX/7iL;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/7iL;->REF_TYPE:LX/7iL;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/7iL;->SECTION_TYPE:LX/7iL;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/7iL;->TOP_LEVEL_POST_ID:LX/7iL;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/7iL;->SESSION_ID:LX/7iL;

    aput-object v2, v0, v1

    sput-object v0, LX/7iL;->$VALUES:[LX/7iL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1227411
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1227412
    iput-object p3, p0, LX/7iL;->value:Ljava/lang/String;

    .line 1227413
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7iL;
    .locals 1

    .prologue
    .line 1227414
    const-class v0, LX/7iL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7iL;

    return-object v0
.end method

.method public static values()[LX/7iL;
    .locals 1

    .prologue
    .line 1227410
    sget-object v0, LX/7iL;->$VALUES:[LX/7iL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7iL;

    return-object v0
.end method
