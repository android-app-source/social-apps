.class public final enum LX/8DO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8DO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8DO;

.field public static final enum DISMISSING:LX/8DO;

.field public static final enum GONE:LX/8DO;

.field public static final enum REVEALING:LX/8DO;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1312527
    new-instance v0, LX/8DO;

    const-string v1, "GONE"

    invoke-direct {v0, v1, v2}, LX/8DO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8DO;->GONE:LX/8DO;

    .line 1312528
    new-instance v0, LX/8DO;

    const-string v1, "REVEALING"

    invoke-direct {v0, v1, v3}, LX/8DO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8DO;->REVEALING:LX/8DO;

    .line 1312529
    new-instance v0, LX/8DO;

    const-string v1, "DISMISSING"

    invoke-direct {v0, v1, v4}, LX/8DO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8DO;->DISMISSING:LX/8DO;

    .line 1312530
    const/4 v0, 0x3

    new-array v0, v0, [LX/8DO;

    sget-object v1, LX/8DO;->GONE:LX/8DO;

    aput-object v1, v0, v2

    sget-object v1, LX/8DO;->REVEALING:LX/8DO;

    aput-object v1, v0, v3

    sget-object v1, LX/8DO;->DISMISSING:LX/8DO;

    aput-object v1, v0, v4

    sput-object v0, LX/8DO;->$VALUES:[LX/8DO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1312532
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8DO;
    .locals 1

    .prologue
    .line 1312533
    const-class v0, LX/8DO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8DO;

    return-object v0
.end method

.method public static values()[LX/8DO;
    .locals 1

    .prologue
    .line 1312531
    sget-object v0, LX/8DO;->$VALUES:[LX/8DO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8DO;

    return-object v0
.end method
