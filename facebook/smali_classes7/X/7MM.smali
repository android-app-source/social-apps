.class public abstract LX/7MM;
.super LX/3Gb;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lf;",
        ">",
        "LX/3Gb",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/7Me;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1198654
    invoke-direct {p0, p1, p2, p3}, LX/3Gb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198655
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/7MM;->a:Ljava/util/Queue;

    .line 1198656
    new-instance v0, LX/7Me;

    invoke-direct {v0, p1}, LX/7Me;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/7MM;->b:LX/7Me;

    .line 1198657
    return-void
.end method


# virtual methods
.method public a(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V
    .locals 2

    .prologue
    .line 1198658
    invoke-super {p0, p1, p2, p3}, LX/3Gb;->a(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    .line 1198659
    iget-object v0, p0, LX/7MM;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 1198660
    invoke-virtual {v0, p1, p2, p3}, LX/2oy;->a(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    goto :goto_0

    .line 1198661
    :cond_0
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 1198620
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1198621
    iput-object p1, p0, LX/7MM;->g:Landroid/view/ViewGroup;

    .line 1198622
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, LX/7MM;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1198623
    invoke-virtual {p0, v1}, LX/7MM;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1198624
    instance-of v2, v0, LX/3Gb;

    if-eqz v2, :cond_1

    .line 1198625
    check-cast v0, LX/3Gb;

    .line 1198626
    iget-object v2, p0, LX/3Gb;->n:LX/7Lf;

    invoke-virtual {v0, v2}, LX/3Gb;->setEnvironment(LX/7Lf;)V

    .line 1198627
    iget-object v2, p0, LX/7MM;->a:Ljava/util/Queue;

    invoke-interface {v2, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1198628
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1198629
    :cond_1
    instance-of v2, v0, LX/2oy;

    if-eqz v2, :cond_0

    .line 1198630
    iget-object v2, p0, LX/7MM;->a:Ljava/util/Queue;

    check-cast v0, LX/2oy;

    invoke-interface {v2, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1198631
    :cond_2
    iget-object v0, p0, LX/7MM;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1198632
    invoke-virtual {p0, v0}, LX/7MM;->removeView(Landroid/view/View;)V

    goto :goto_2

    .line 1198633
    :cond_3
    iget-object v0, p0, LX/7MM;->a:Ljava/util/Queue;

    iget-object v1, p0, LX/7MM;->b:LX/7Me;

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1198634
    invoke-virtual {p0}, LX/7MM;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1198635
    if-eqz v0, :cond_4

    .line 1198636
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1198637
    :cond_4
    iget-object v0, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1198638
    iget-object v0, p0, LX/7MM;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 1198639
    invoke-virtual {v0, p0}, LX/2oy;->a(Landroid/view/ViewGroup;)V

    goto :goto_3

    .line 1198640
    :cond_5
    const v0, 0x7f0d0950

    invoke-virtual {p0, v0}, LX/2oX;->setInnerResource(I)V

    .line 1198641
    return-void
.end method

.method public final b()Landroid/view/ViewGroup;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1198642
    iget-object v0, p0, LX/7MM;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 1198643
    invoke-virtual {v0}, LX/2oy;->b()Landroid/view/ViewGroup;

    goto :goto_0

    .line 1198644
    :cond_0
    iget-object v0, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1198645
    :cond_1
    :goto_1
    iget-object v0, p0, LX/7MM;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1198646
    iget-object v0, p0, LX/7MM;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 1198647
    instance-of v1, v0, LX/7Me;

    if-nez v1, :cond_1

    .line 1198648
    instance-of v1, v0, LX/3Gb;

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 1198649
    check-cast v1, LX/3Gb;

    invoke-virtual {v1, v2}, LX/3Gb;->setEnvironment(LX/7Lf;)V

    .line 1198650
    :cond_2
    invoke-virtual {p0, v0}, LX/7MM;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 1198651
    :cond_3
    iget-object v0, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    .line 1198652
    iput-object v2, p0, LX/7MM;->g:Landroid/view/ViewGroup;

    .line 1198653
    return-object v0
.end method

.method public b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V
    .locals 2

    .prologue
    .line 1198616
    invoke-super {p0, p1, p2, p3}, LX/3Gb;->b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    .line 1198617
    iget-object v0, p0, LX/7MM;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 1198618
    invoke-virtual {v0, p1, p2, p3}, LX/2oy;->b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    goto :goto_0

    .line 1198619
    :cond_0
    return-void
.end method

.method public final dJ_()V
    .locals 2

    .prologue
    .line 1198612
    invoke-super {p0}, LX/3Gb;->dJ_()V

    .line 1198613
    iget-object v0, p0, LX/7MM;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 1198614
    invoke-virtual {v0}, LX/2oy;->dJ_()V

    goto :goto_0

    .line 1198615
    :cond_0
    return-void
.end method

.method public final im_()V
    .locals 2

    .prologue
    .line 1198608
    iget-object v0, p0, LX/7MM;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 1198609
    invoke-virtual {v0}, LX/2oy;->im_()V

    goto :goto_0

    .line 1198610
    :cond_0
    invoke-super {p0}, LX/3Gb;->im_()V

    .line 1198611
    return-void
.end method

.method public setEnvironment(LX/7Lf;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 1198603
    invoke-super {p0, p1}, LX/3Gb;->setEnvironment(LX/7Lf;)V

    .line 1198604
    iget-object v0, p0, LX/7MM;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 1198605
    instance-of v2, v0, LX/3Gb;

    if-eqz v2, :cond_0

    .line 1198606
    check-cast v0, LX/3Gb;

    invoke-virtual {v0, p1}, LX/3Gb;->setEnvironment(LX/7Lf;)V

    goto :goto_0

    .line 1198607
    :cond_1
    return-void
.end method

.method public setEventBus(LX/2oj;)V
    .locals 2

    .prologue
    .line 1198599
    invoke-super {p0, p1}, LX/3Gb;->setEventBus(LX/2oj;)V

    .line 1198600
    iget-object v0, p0, LX/7MM;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 1198601
    invoke-virtual {v0, p1}, LX/2oy;->setEventBus(LX/2oj;)V

    goto :goto_0

    .line 1198602
    :cond_0
    return-void
.end method
