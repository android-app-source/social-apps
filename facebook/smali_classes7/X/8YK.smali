.class public LX/8YK;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:I


# instance fields
.field private b:LX/8YL;

.field public c:Z

.field public final d:Ljava/lang/String;

.field public final e:LX/8YJ;

.field private final f:LX/8YJ;

.field private final g:LX/8YJ;

.field public h:D

.field public i:D

.field public j:Z

.field public k:D

.field public l:D

.field private m:D

.field public final n:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "LX/7hQ;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/8YH;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1356159
    const/4 v0, 0x0

    sput v0, LX/8YK;->a:I

    return-void
.end method

.method public constructor <init>(LX/8YH;)V
    .locals 4

    .prologue
    const-wide v2, 0x3f747ae147ae147bL    # 0.005

    .line 1356038
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1356039
    new-instance v0, LX/8YJ;

    invoke-direct {v0}, LX/8YJ;-><init>()V

    iput-object v0, p0, LX/8YK;->e:LX/8YJ;

    .line 1356040
    new-instance v0, LX/8YJ;

    invoke-direct {v0}, LX/8YJ;-><init>()V

    iput-object v0, p0, LX/8YK;->f:LX/8YJ;

    .line 1356041
    new-instance v0, LX/8YJ;

    invoke-direct {v0}, LX/8YJ;-><init>()V

    iput-object v0, p0, LX/8YK;->g:LX/8YJ;

    .line 1356042
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8YK;->j:Z

    .line 1356043
    iput-wide v2, p0, LX/8YK;->k:D

    .line 1356044
    iput-wide v2, p0, LX/8YK;->l:D

    .line 1356045
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/8YK;->m:D

    .line 1356046
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LX/8YK;->n:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 1356047
    if-nez p1, :cond_0

    .line 1356048
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Spring cannot be created outside of a BaseSpringSystem"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1356049
    :cond_0
    iput-object p1, p0, LX/8YK;->o:LX/8YH;

    .line 1356050
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "spring:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v1, LX/8YK;->a:I

    add-int/lit8 v2, v1, 0x1

    sput v2, LX/8YK;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8YK;->d:Ljava/lang/String;

    .line 1356051
    sget-object v0, LX/8YL;->c:LX/8YL;

    invoke-virtual {p0, v0}, LX/8YK;->a(LX/8YL;)LX/8YK;

    .line 1356052
    return-void
.end method

.method private static g(LX/8YK;D)V
    .locals 11

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 1356156
    iget-object v0, p0, LX/8YK;->e:LX/8YJ;

    iget-object v1, p0, LX/8YK;->e:LX/8YJ;

    iget-wide v2, v1, LX/8YJ;->a:D

    mul-double/2addr v2, p1

    iget-object v1, p0, LX/8YK;->f:LX/8YJ;

    iget-wide v4, v1, LX/8YJ;->a:D

    sub-double v6, v8, p1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iput-wide v2, v0, LX/8YJ;->a:D

    .line 1356157
    iget-object v0, p0, LX/8YK;->e:LX/8YJ;

    iget-object v1, p0, LX/8YK;->e:LX/8YJ;

    iget-wide v2, v1, LX/8YJ;->b:D

    mul-double/2addr v2, p1

    iget-object v1, p0, LX/8YK;->f:LX/8YJ;

    iget-wide v4, v1, LX/8YJ;->b:D

    sub-double v6, v8, p1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iput-wide v2, v0, LX/8YJ;->b:D

    .line 1356158
    return-void
.end method

.method private static h(LX/8YK;)Z
    .locals 4

    .prologue
    .line 1356155
    iget-object v0, p0, LX/8YK;->b:LX/8YL;

    iget-wide v0, v0, LX/8YL;->b:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    iget-wide v0, p0, LX/8YK;->h:D

    iget-wide v2, p0, LX/8YK;->i:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    invoke-virtual {p0}, LX/8YK;->b()D

    move-result-wide v0

    iget-wide v2, p0, LX/8YK;->i:D

    cmpl-double v0, v0, v2

    if-gtz v0, :cond_1

    :cond_0
    iget-wide v0, p0, LX/8YK;->h:D

    iget-wide v2, p0, LX/8YK;->i:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    invoke-virtual {p0}, LX/8YK;->b()D

    move-result-wide v0

    iget-wide v2, p0, LX/8YK;->i:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(D)LX/8YK;
    .locals 4

    .prologue
    .line 1356143
    const/4 v0, 0x1

    .line 1356144
    iput-wide p1, p0, LX/8YK;->h:D

    .line 1356145
    iget-object v1, p0, LX/8YK;->e:LX/8YJ;

    iput-wide p1, v1, LX/8YJ;->a:D

    .line 1356146
    iget-object v1, p0, LX/8YK;->o:LX/8YH;

    .line 1356147
    iget-object v2, p0, LX/8YK;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1356148
    invoke-virtual {v1, v2}, LX/8YH;->a(Ljava/lang/String;)V

    .line 1356149
    iget-object v1, p0, LX/8YK;->n:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7hQ;

    .line 1356150
    invoke-interface {v1, p0}, LX/7hQ;->a(LX/8YK;)V

    goto :goto_0

    .line 1356151
    :cond_0
    if-eqz v0, :cond_1

    .line 1356152
    invoke-virtual {p0}, LX/8YK;->f()LX/8YK;

    .line 1356153
    :cond_1
    move-object v0, p0

    .line 1356154
    return-object v0
.end method

.method public final a(LX/7hQ;)LX/8YK;
    .locals 2

    .prologue
    .line 1356139
    if-nez p1, :cond_0

    .line 1356140
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "newListener is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1356141
    :cond_0
    iget-object v0, p0, LX/8YK;->n:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 1356142
    return-object p0
.end method

.method public final a(LX/8YL;)LX/8YK;
    .locals 2

    .prologue
    .line 1356135
    if-nez p1, :cond_0

    .line 1356136
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "springConfig is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1356137
    :cond_0
    iput-object p1, p0, LX/8YK;->b:LX/8YL;

    .line 1356138
    return-object p0
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 1356134
    iget-object v0, p0, LX/8YK;->e:LX/8YJ;

    iget-wide v0, v0, LX/8YJ;->a:D

    return-wide v0
.end method

.method public final b(D)LX/8YK;
    .locals 3

    .prologue
    .line 1356125
    iget-wide v0, p0, LX/8YK;->i:D

    cmpl-double v0, v0, p1

    if-nez v0, :cond_1

    invoke-virtual {p0}, LX/8YK;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1356126
    :cond_0
    return-object p0

    .line 1356127
    :cond_1
    invoke-virtual {p0}, LX/8YK;->b()D

    move-result-wide v0

    iput-wide v0, p0, LX/8YK;->h:D

    .line 1356128
    iput-wide p1, p0, LX/8YK;->i:D

    .line 1356129
    iget-object v0, p0, LX/8YK;->o:LX/8YH;

    .line 1356130
    iget-object v1, p0, LX/8YK;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1356131
    invoke-virtual {v0, v1}, LX/8YH;->a(Ljava/lang/String;)V

    .line 1356132
    iget-object v0, p0, LX/8YK;->n:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7hQ;

    .line 1356133
    invoke-interface {v0, p0}, LX/7hQ;->b(LX/8YK;)V

    goto :goto_0
.end method

.method public final c(D)LX/8YK;
    .locals 3

    .prologue
    .line 1356119
    iget-object v0, p0, LX/8YK;->e:LX/8YJ;

    iget-wide v0, v0, LX/8YJ;->b:D

    cmpl-double v0, p1, v0

    if-nez v0, :cond_0

    .line 1356120
    :goto_0
    return-object p0

    .line 1356121
    :cond_0
    iget-object v0, p0, LX/8YK;->e:LX/8YJ;

    iput-wide p1, v0, LX/8YJ;->b:D

    .line 1356122
    iget-object v0, p0, LX/8YK;->o:LX/8YH;

    .line 1356123
    iget-object v1, p0, LX/8YK;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1356124
    invoke-virtual {v0, v1}, LX/8YH;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final e()Z
    .locals 8

    .prologue
    .line 1356116
    iget-object v0, p0, LX/8YK;->e:LX/8YJ;

    iget-wide v0, v0, LX/8YJ;->b:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iget-wide v2, p0, LX/8YK;->k:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    iget-object v0, p0, LX/8YK;->e:LX/8YJ;

    .line 1356117
    iget-wide v4, p0, LX/8YK;->i:D

    iget-wide v6, v0, LX/8YJ;->a:D

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    move-wide v0, v4

    .line 1356118
    iget-wide v2, p0, LX/8YK;->l:D

    cmpg-double v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, LX/8YK;->b:LX/8YL;

    iget-wide v0, v0, LX/8YL;->b:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()LX/8YK;
    .locals 4

    .prologue
    .line 1356112
    iget-object v0, p0, LX/8YK;->e:LX/8YJ;

    iget-wide v0, v0, LX/8YJ;->a:D

    iput-wide v0, p0, LX/8YK;->i:D

    .line 1356113
    iget-object v0, p0, LX/8YK;->g:LX/8YJ;

    iget-object v1, p0, LX/8YK;->e:LX/8YJ;

    iget-wide v2, v1, LX/8YJ;->a:D

    iput-wide v2, v0, LX/8YJ;->a:D

    .line 1356114
    iget-object v0, p0, LX/8YK;->e:LX/8YJ;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, LX/8YJ;->b:D

    .line 1356115
    return-object p0
.end method

.method public final f(D)V
    .locals 33

    .prologue
    .line 1356053
    invoke-virtual/range {p0 .. p0}, LX/8YK;->e()Z

    move-result v10

    .line 1356054
    if-eqz v10, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/8YK;->j:Z

    if-eqz v2, :cond_1

    .line 1356055
    :cond_0
    return-void

    .line 1356056
    :cond_1
    const-wide v2, 0x3fb0624dd2f1a9fcL    # 0.064

    cmpl-double v2, p1, v2

    if-lez v2, :cond_2

    .line 1356057
    const-wide p1, 0x3fb0624dd2f1a9fcL    # 0.064

    .line 1356058
    :cond_2
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/8YK;->m:D

    add-double v2, v2, p1

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/8YK;->m:D

    .line 1356059
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8YK;->b:LX/8YL;

    iget-wide v12, v2, LX/8YL;->b:D

    .line 1356060
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8YK;->b:LX/8YL;

    iget-wide v14, v2, LX/8YL;->a:D

    .line 1356061
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8YK;->e:LX/8YJ;

    iget-wide v8, v2, LX/8YJ;->a:D

    .line 1356062
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8YK;->e:LX/8YJ;

    iget-wide v6, v2, LX/8YJ;->b:D

    .line 1356063
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8YK;->g:LX/8YJ;

    iget-wide v4, v2, LX/8YJ;->a:D

    .line 1356064
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8YK;->g:LX/8YJ;

    iget-wide v2, v2, LX/8YJ;->b:D

    .line 1356065
    :goto_0
    move-object/from16 v0, p0

    iget-wide v0, v0, LX/8YK;->m:D

    move-wide/from16 v16, v0

    const-wide v18, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v11, v16, v18

    if-ltz v11, :cond_4

    .line 1356066
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/8YK;->m:D

    const-wide v16, 0x3f50624dd2f1a9fcL    # 0.001

    sub-double v2, v2, v16

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/8YK;->m:D

    .line 1356067
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/8YK;->m:D

    const-wide v16, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v2, v2, v16

    if-gez v2, :cond_3

    .line 1356068
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8YK;->f:LX/8YJ;

    iput-wide v8, v2, LX/8YJ;->a:D

    .line 1356069
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8YK;->f:LX/8YJ;

    iput-wide v6, v2, LX/8YJ;->b:D

    .line 1356070
    :cond_3
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/8YK;->i:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v12

    mul-double v4, v14, v6

    sub-double v16, v2, v4

    .line 1356071
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v2, v6

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v2, v4

    add-double/2addr v2, v8

    .line 1356072
    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v4, v4, v16

    const-wide/high16 v18, 0x3fe0000000000000L    # 0.5

    mul-double v4, v4, v18

    add-double v18, v6, v4

    .line 1356073
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8YK;->i:D

    sub-double v2, v4, v2

    mul-double/2addr v2, v12

    mul-double v4, v14, v18

    sub-double v20, v2, v4

    .line 1356074
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v2, v2, v18

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v2, v4

    add-double/2addr v2, v8

    .line 1356075
    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v4, v4, v20

    const-wide/high16 v22, 0x3fe0000000000000L    # 0.5

    mul-double v4, v4, v22

    add-double v22, v6, v4

    .line 1356076
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8YK;->i:D

    sub-double v2, v4, v2

    mul-double/2addr v2, v12

    mul-double v4, v14, v22

    sub-double v24, v2, v4

    .line 1356077
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v2, v2, v22

    add-double v4, v8, v2

    .line 1356078
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v2, v2, v24

    add-double/2addr v2, v6

    .line 1356079
    move-object/from16 v0, p0

    iget-wide v0, v0, LX/8YK;->i:D

    move-wide/from16 v26, v0

    sub-double v26, v26, v4

    mul-double v26, v26, v12

    mul-double v28, v14, v2

    sub-double v26, v26, v28

    .line 1356080
    const-wide v28, 0x3fc5555555555555L    # 0.16666666666666666

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    add-double v18, v18, v22

    mul-double v18, v18, v30

    add-double v18, v18, v6

    add-double v18, v18, v2

    mul-double v18, v18, v28

    .line 1356081
    const-wide v22, 0x3fc5555555555555L    # 0.16666666666666666

    const-wide/high16 v28, 0x4000000000000000L    # 2.0

    add-double v20, v20, v24

    mul-double v20, v20, v28

    add-double v16, v16, v20

    add-double v16, v16, v26

    mul-double v16, v16, v22

    .line 1356082
    const-wide v20, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v18, v18, v20

    add-double v8, v8, v18

    .line 1356083
    const-wide v18, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v16, v16, v18

    add-double v6, v6, v16

    goto/16 :goto_0

    .line 1356084
    :cond_4
    move-object/from16 v0, p0

    iget-object v11, v0, LX/8YK;->g:LX/8YJ;

    iput-wide v4, v11, LX/8YJ;->a:D

    .line 1356085
    move-object/from16 v0, p0

    iget-object v4, v0, LX/8YK;->g:LX/8YJ;

    iput-wide v2, v4, LX/8YJ;->b:D

    .line 1356086
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8YK;->e:LX/8YJ;

    iput-wide v8, v2, LX/8YJ;->a:D

    .line 1356087
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8YK;->e:LX/8YJ;

    iput-wide v6, v2, LX/8YJ;->b:D

    .line 1356088
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/8YK;->m:D

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_5

    .line 1356089
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/8YK;->m:D

    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    div-double/2addr v2, v4

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, LX/8YK;->g(LX/8YK;D)V

    .line 1356090
    :cond_5
    invoke-virtual/range {p0 .. p0}, LX/8YK;->e()Z

    move-result v2

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/8YK;->c:Z

    if-eqz v2, :cond_c

    invoke-static/range {p0 .. p0}, LX/8YK;->h(LX/8YK;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1356091
    :cond_6
    const-wide/16 v2, 0x0

    cmpl-double v2, v12, v2

    if-lez v2, :cond_a

    .line 1356092
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/8YK;->i:D

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/8YK;->h:D

    .line 1356093
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8YK;->e:LX/8YJ;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/8YK;->i:D

    iput-wide v4, v2, LX/8YJ;->a:D

    .line 1356094
    :goto_1
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/8YK;->c(D)LX/8YK;

    .line 1356095
    const/4 v2, 0x1

    .line 1356096
    :goto_2
    const/4 v3, 0x0

    .line 1356097
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/8YK;->j:Z

    if-eqz v4, :cond_b

    .line 1356098
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, LX/8YK;->j:Z

    .line 1356099
    const/4 v3, 0x1

    move v4, v3

    .line 1356100
    :goto_3
    const/4 v3, 0x0

    .line 1356101
    if-eqz v2, :cond_7

    .line 1356102
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/8YK;->j:Z

    .line 1356103
    const/4 v2, 0x1

    move v3, v2

    .line 1356104
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8YK;->n:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_8
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7hQ;

    .line 1356105
    if-eqz v4, :cond_9

    .line 1356106
    invoke-interface {v2}, LX/7hQ;->b()V

    .line 1356107
    :cond_9
    move-object/from16 v0, p0

    invoke-interface {v2, v0}, LX/7hQ;->a(LX/8YK;)V

    .line 1356108
    if-eqz v3, :cond_8

    .line 1356109
    invoke-interface {v2}, LX/7hQ;->a()V

    goto :goto_4

    .line 1356110
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8YK;->e:LX/8YJ;

    iget-wide v2, v2, LX/8YJ;->a:D

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/8YK;->i:D

    .line 1356111
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/8YK;->i:D

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/8YK;->h:D

    goto :goto_1

    :cond_b
    move v4, v3

    goto :goto_3

    :cond_c
    move v2, v10

    goto :goto_2
.end method
