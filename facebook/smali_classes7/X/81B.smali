.class public LX/81B;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;",
        "Lcom/facebook/graphql/model/GraphQLStorySet;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/0sa;

.field private final c:LX/0rq;

.field private final d:LX/0w9;


# direct methods
.method public constructor <init>(LX/0sa;LX/0rq;LX/0w9;LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1285296
    invoke-direct {p0, p4}, LX/0ro;-><init>(LX/0sO;)V

    .line 1285297
    iput-object p1, p0, LX/81B;->b:LX/0sa;

    .line 1285298
    iput-object p2, p0, LX/81B;->c:LX/0rq;

    .line 1285299
    iput-object p3, p0, LX/81B;->d:LX/0w9;

    .line 1285300
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1285282
    iget-object v0, p0, LX/0ro;->a:LX/0sO;

    const-class v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    const-string v2, "PaginatedRelatedStory"

    invoke-virtual {v0, p3, v1, v2}, LX/0sO;->a(LX/15w;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1285283
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 1285284
    check-cast p1, Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;

    .line 1285285
    new-instance v0, LX/81C;

    invoke-direct {v0}, LX/81C;-><init>()V

    move-object v0, v0

    .line 1285286
    invoke-static {v0}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 1285287
    iget-object v1, p0, LX/81B;->d:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 1285288
    const-string v1, "node_id"

    .line 1285289
    iget-object v2, p1, Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1285290
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_pic_swipe_size_param"

    iget-object v3, p0, LX/81B;->b:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->m()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "pymk_size_param"

    iget-object v3, p0, LX/81B;->b:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->f()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "ad_media_type"

    iget-object v3, p0, LX/81B;->c:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->a()LX/0wF;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "after_cursor_param"

    .line 1285291
    iget-object v3, p1, Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1285292
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "num_next_related_stories"

    .line 1285293
    iget v3, p1, Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;->c:I

    move v3, v3

    .line 1285294
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1285295
    return-object v0
.end method
