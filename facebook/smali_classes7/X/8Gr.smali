.class public LX/8Gr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320085
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320086
    return-void
.end method

.method public static a(LX/0QB;)LX/8Gr;
    .locals 1

    .prologue
    .line 1320088
    new-instance v0, LX/8Gr;

    invoke-direct {v0}, LX/8Gr;-><init>()V

    .line 1320089
    move-object v0, v0

    .line 1320090
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 1320091
    check-cast p1, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;

    .line 1320092
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1320093
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320094
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "name"

    iget-object v2, p1, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320095
    iget-object v0, p1, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1320096
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "description"

    iget-object v2, p1, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320097
    :cond_0
    iget-object v0, p1, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1320098
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "location"

    iget-object v2, p1, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320099
    :cond_1
    new-instance v0, LX/14N;

    const-string v1, "createPagePhotoAlbum"

    const-string v2, "POST"

    const-string v3, "/%d/albums"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-wide v8, p1, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1320087
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
