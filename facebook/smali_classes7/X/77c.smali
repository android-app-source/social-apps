.class public abstract LX/77c;
.super LX/2g7;
.source ""


# instance fields
.field private final a:LX/0SF;


# direct methods
.method public constructor <init>(LX/0SF;)V
    .locals 0

    .prologue
    .line 1171962
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1171963
    iput-object p1, p0, LX/77c;->a:LX/0SF;

    .line 1171964
    return-void
.end method


# virtual methods
.method public abstract a(JJ)Z
.end method

.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 7
    .param p1    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 1171965
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1171966
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1171967
    iget-object v2, p0, LX/77c;->a:LX/0SF;

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    .line 1171968
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 1171969
    invoke-virtual {v4, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1171970
    const/16 v5, 0xe

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 1171971
    const/16 v5, 0xd

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 1171972
    const/16 v5, 0xc

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 1171973
    const/16 v5, 0xb

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 1171974
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1171975
    invoke-virtual {p0, v2, v3, v0, v1}, LX/77c;->a(JJ)Z

    move-result v0

    return v0
.end method
