.class public final LX/7GR;
.super LX/76J;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RESPONSE:",
        "Ljava/lang/Object;",
        "PAY",
        "LOAD:Ljava/lang/Object;",
        ">",
        "LX/76J",
        "<TRESPONSE;>;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/7GS;

.field private c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TPAY",
            "LOAD;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/7GQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7GQ",
            "<TRESPONSE;TPAY",
            "LOAD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7GS;Ljava/lang/String;LX/0Xl;LX/0So;LX/1fU;LX/7GQ;)V
    .locals 7
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Xl;",
            "LX/0So;",
            "LX/1fU;",
            "LX/7GQ",
            "<TRESPONSE;TPAY",
            "LOAD;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1189188
    iput-object p1, p0, LX/7GR;->b:LX/7GS;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v6, v5

    .line 1189189
    invoke-direct/range {v0 .. v6}, LX/76J;-><init>(Ljava/lang/String;LX/0Xl;LX/0So;LX/1fU;LX/2gV;LX/03V;)V

    .line 1189190
    iput-object p6, p0, LX/7GR;->d:LX/7GQ;

    .line 1189191
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;[B)V
    .locals 2

    .prologue
    .line 1189183
    :try_start_0
    invoke-static {p2}, LX/7GV;->a([B)LX/7GU;

    move-result-object v0

    .line 1189184
    iget-object v1, p0, LX/7GR;->d:LX/7GQ;

    iget v0, v0, LX/7GU;->b:I

    invoke-interface {v1, p2, v0}, LX/7GQ;->a([BI)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/7GR;->c:Ljava/lang/Object;
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    .line 1189185
    :goto_0
    return-void

    .line 1189186
    :catch_0
    move-exception v0

    .line 1189187
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1189182
    iget-object v0, p0, LX/7GR;->d:LX/7GQ;

    iget-object v1, p0, LX/7GR;->c:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/7GQ;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRESPONSE;"
        }
    .end annotation

    .prologue
    .line 1189181
    iget-object v0, p0, LX/7GR;->d:LX/7GQ;

    iget-object v1, p0, LX/7GR;->c:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/7GQ;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
