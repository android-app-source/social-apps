.class public LX/7Rm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/7Rl;

.field public c:Landroid/opengl/EGLSurface;

.field public d:I

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1207851
    const-class v0, LX/7Rm;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Rm;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/7Rl;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1207836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1207837
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    iput-object v0, p0, LX/7Rm;->c:Landroid/opengl/EGLSurface;

    .line 1207838
    iput v1, p0, LX/7Rm;->d:I

    .line 1207839
    iput v1, p0, LX/7Rm;->e:I

    .line 1207840
    iput-object p1, p0, LX/7Rm;->b:LX/7Rl;

    .line 1207841
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1207847
    iget-object v0, p0, LX/7Rm;->c:Landroid/opengl/EGLSurface;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-eq v0, v1, :cond_0

    .line 1207848
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "surface already created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1207849
    :cond_0
    iget-object v0, p0, LX/7Rm;->b:LX/7Rl;

    invoke-virtual {v0, p1}, LX/7Rl;->a(Ljava/lang/Object;)Landroid/opengl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, LX/7Rm;->c:Landroid/opengl/EGLSurface;

    .line 1207850
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1207845
    iget-object v0, p0, LX/7Rm;->b:LX/7Rl;

    iget-object v1, p0, LX/7Rm;->c:Landroid/opengl/EGLSurface;

    invoke-virtual {v0, v1}, LX/7Rl;->b(Landroid/opengl/EGLSurface;)V

    .line 1207846
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1207842
    iget-object v0, p0, LX/7Rm;->b:LX/7Rl;

    iget-object v1, p0, LX/7Rm;->c:Landroid/opengl/EGLSurface;

    .line 1207843
    iget-object p0, v0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    invoke-static {p0, v1}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    move-result p0

    move v0, p0

    .line 1207844
    return v0
.end method
