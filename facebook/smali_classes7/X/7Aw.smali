.class public LX/7Aw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/7Aw;


# instance fields
.field private final a:LX/0ad;

.field public final b:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0ad;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1177631
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1177632
    iput-object p1, p0, LX/7Aw;->a:LX/0ad;

    .line 1177633
    iput-object p2, p0, LX/7Aw;->b:LX/0Uh;

    .line 1177634
    return-void
.end method

.method public static a(LX/0QB;)LX/7Aw;
    .locals 5

    .prologue
    .line 1177635
    sget-object v0, LX/7Aw;->c:LX/7Aw;

    if-nez v0, :cond_1

    .line 1177636
    const-class v1, LX/7Aw;

    monitor-enter v1

    .line 1177637
    :try_start_0
    sget-object v0, LX/7Aw;->c:LX/7Aw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1177638
    if-eqz v2, :cond_0

    .line 1177639
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1177640
    new-instance p0, LX/7Aw;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/7Aw;-><init>(LX/0ad;LX/0Uh;)V

    .line 1177641
    move-object v0, p0

    .line 1177642
    sput-object v0, LX/7Aw;->c:LX/7Aw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1177643
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1177644
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1177645
    :cond_1
    sget-object v0, LX/7Aw;->c:LX/7Aw;

    return-object v0

    .line 1177646
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1177647
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
