.class public LX/7iz;
.super LX/1a1;
.source ""


# instance fields
.field public final l:Landroid/widget/FrameLayout;

.field private final m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final n:Lcom/facebook/widget/text/BetterTextView;

.field public final o:Lcom/facebook/widget/text/BetterTextView;

.field public final p:Lcom/facebook/fbui/glyph/GlyphView;

.field public final q:LX/0zw;

.field public final r:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1228978
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1228979
    const v0, 0x7f0d26ff

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/7iz;->l:Landroid/widget/FrameLayout;

    .line 1228980
    const v0, 0x7f0d2700

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/7iz;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1228981
    const v0, 0x7f0d2703

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/7iz;->n:Lcom/facebook/widget/text/BetterTextView;

    .line 1228982
    const v0, 0x7f0d2704

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/7iz;->o:Lcom/facebook/widget/text/BetterTextView;

    .line 1228983
    const v0, 0x7f0d2705

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/7iz;->p:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1228984
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2706

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/7iz;->q:LX/0zw;

    .line 1228985
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/7iz;->r:Landroid/content/Context;

    .line 1228986
    return-void
.end method

.method public static a(LX/7iz;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p0    # LX/7iz;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1228998
    iget-object v0, p0, LX/7iz;->l:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 1228999
    return-void
.end method

.method public static a(LX/7iz;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1228995
    iget-object v0, p0, LX/7iz;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1228996
    iget-object v0, p0, LX/7iz;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1228997
    return-void
.end method

.method public static d(LX/7iz;Z)V
    .locals 2

    .prologue
    .line 1228989
    if-eqz p1, :cond_0

    .line 1228990
    iget-object v0, p0, LX/7iz;->p:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1228991
    iget-object v0, p0, LX/7iz;->p:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f020c10

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1228992
    iget-object v0, p0, LX/7iz;->p:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f020a2e

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setBackgroundResource(I)V

    .line 1228993
    :goto_0
    return-void

    .line 1228994
    :cond_0
    iget-object v0, p0, LX/7iz;->p:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1228987
    iget-object v0, p0, LX/7iz;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1228988
    return-void
.end method
