.class public LX/82e;
.super LX/82c;
.source ""


# instance fields
.field public a:J

.field public b:I

.field public c:Landroid/graphics/drawable/Drawable;

.field public d:LX/82d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1288385
    invoke-direct {p0}, LX/82c;-><init>()V

    .line 1288386
    return-void
.end method

.method public constructor <init>([Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1288387
    invoke-direct {p0, p1}, LX/82c;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 1288388
    const/4 v0, 0x1

    aget-object v0, p1, v0

    iput-object v0, p0, LX/82e;->c:Landroid/graphics/drawable/Drawable;

    .line 1288389
    sget-object v0, LX/82d;->TRANSITION_NONE:LX/82d;

    iput-object v0, p0, LX/82e;->d:LX/82d;

    .line 1288390
    const/16 v0, 0xff

    iput v0, p0, LX/82e;->b:I

    .line 1288391
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    .line 1288392
    iget-object v0, p0, LX/82e;->d:LX/82d;

    sget-object v1, LX/82d;->TRANSITION_RUNNING:LX/82d;

    if-ne v0, v1, :cond_1

    .line 1288393
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, LX/82e;->a:J

    sub-long/2addr v2, v4

    long-to-double v2, v2

    const-wide v4, 0x4082c00000000000L    # 600.0

    div-double/2addr v2, v4

    .line 1288394
    const-wide v4, 0x406fe00000000000L    # 255.0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double v2, v6, v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, p0, LX/82e;->b:I

    .line 1288395
    iget v2, p0, LX/82e;->b:I

    if-gtz v2, :cond_0

    .line 1288396
    const/4 v2, 0x0

    iput v2, p0, LX/82e;->b:I

    .line 1288397
    sget-object v2, LX/82d;->TRANSITION_FINISHED:LX/82d;

    iput-object v2, p0, LX/82e;->d:LX/82d;

    .line 1288398
    :cond_0
    iget-object v2, p0, LX/82e;->c:Landroid/graphics/drawable/Drawable;

    iget v3, p0, LX/82e;->b:I

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1288399
    iget-object v2, p0, LX/82e;->d:LX/82d;

    sget-object v3, LX/82d;->TRANSITION_RUNNING:LX/82d;

    if-ne v2, v3, :cond_1

    .line 1288400
    invoke-virtual {p0}, LX/82e;->invalidateSelf()V

    .line 1288401
    :cond_1
    invoke-super {p0, p1}, LX/82c;->draw(Landroid/graphics/Canvas;)V

    .line 1288402
    return-void
.end method
