.class public final LX/8Tl;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8Wz;

.field public final synthetic b:LX/8Tm;


# direct methods
.method public constructor <init>(LX/8Tm;LX/8Wz;)V
    .locals 0

    .prologue
    .line 1349016
    iput-object p1, p0, LX/8Tl;->b:LX/8Tm;

    iput-object p2, p0, LX/8Tl;->a:LX/8Wz;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1349069
    iget-object v0, p0, LX/8Tl;->b:LX/8Tm;

    iget-object v0, v0, LX/8Tm;->c:LX/8TD;

    sget-object v1, LX/8TE;->GAME_RECOMMENDATIONS_FETCH:LX/8TE;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 1349070
    iget-object v0, p0, LX/8Tl;->a:LX/8Wz;

    .line 1349071
    iget-object v1, v0, LX/8Wz;->a:LX/8X0;

    iget-object v1, v1, LX/8X0;->e:LX/8XO;

    if-eqz v1, :cond_0

    .line 1349072
    iget-object v1, v0, LX/8Wz;->a:LX/8X0;

    iget-object v1, v1, LX/8X0;->e:LX/8XO;

    .line 1349073
    iget-object v2, v1, LX/8XO;->a:Landroid/support/v7/widget/RecyclerView;

    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 1349074
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 1349017
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1349018
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1349019
    if-eqz v0, :cond_4

    .line 1349020
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1349021
    check-cast v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;->a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1349022
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1349023
    check-cast v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;->a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;

    move-result-object v0

    .line 1349024
    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1349025
    new-instance v10, Ljava/util/LinkedList;

    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V

    .line 1349026
    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v9, v8

    :goto_0
    if-ge v9, v12, :cond_1

    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel$NodesModel;

    .line 1349027
    invoke-virtual {v2}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel$NodesModel;->k()LX/0Px;

    move-result-object v6

    .line 1349028
    if-eqz v6, :cond_7

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v3

    if-lez v3, :cond_7

    .line 1349029
    invoke-virtual {v6, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListItemModel$TextLinesModel;

    invoke-virtual {v3}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListItemModel$TextLinesModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 1349030
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v3

    if-le v3, v7, :cond_6

    .line 1349031
    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListItemModel$TextLinesModel;

    invoke-virtual {v3}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListItemModel$TextLinesModel;->a()Ljava/lang/String;

    move-result-object v3

    move-object v6, v4

    move-object v4, v3

    .line 1349032
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel$NodesModel;->j()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v2}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel$NodesModel;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-lez v3, :cond_5

    .line 1349033
    invoke-virtual {v2}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel$NodesModel;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    .line 1349034
    sget-object p1, Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;->NEW:Lcom/facebook/graphql/enums/GraphQLInstantGameListItemTag;

    if-ne v3, p1, :cond_0

    move v3, v7

    .line 1349035
    :goto_2
    new-instance p1, LX/8Ww;

    invoke-direct {p1}, LX/8Ww;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel$NodesModel;->a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListApplicationFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListApplicationFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 1349036
    iput-object v1, p1, LX/8Ww;->a:Ljava/lang/String;

    .line 1349037
    move-object p1, p1

    .line 1349038
    invoke-virtual {v2}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel$NodesModel;->a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListApplicationFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListApplicationFragmentModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 1349039
    iput-object v1, p1, LX/8Ww;->b:Ljava/lang/String;

    .line 1349040
    move-object p1, p1

    .line 1349041
    invoke-virtual {v2}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel$NodesModel;->a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListApplicationFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListApplicationFragmentModel;->k()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListApplicationFragmentModel$InstantGameInfoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListApplicationFragmentModel$InstantGameInfoModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 1349042
    iput-object v2, p1, LX/8Ww;->c:Ljava/lang/String;

    .line 1349043
    move-object v2, p1

    .line 1349044
    iput-object v6, v2, LX/8Ww;->d:Ljava/lang/String;

    .line 1349045
    move-object v2, v2

    .line 1349046
    iput-object v4, v2, LX/8Ww;->e:Ljava/lang/String;

    .line 1349047
    move-object v2, v2

    .line 1349048
    iput-boolean v3, v2, LX/8Ww;->f:Z

    .line 1349049
    move-object v2, v2

    .line 1349050
    new-instance v3, LX/8Wx;

    invoke-direct {v3, v2}, LX/8Wx;-><init>(LX/8Ww;)V

    move-object v2, v3

    .line 1349051
    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1349052
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto/16 :goto_0

    :cond_0
    move v3, v8

    .line 1349053
    goto :goto_2

    .line 1349054
    :cond_1
    invoke-static {v10}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 1349055
    move-object v0, v2

    .line 1349056
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1349057
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Empty game list"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/8Tl;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1349058
    :goto_3
    return-void

    .line 1349059
    :cond_2
    iget-object v1, p0, LX/8Tl;->b:LX/8Tm;

    iget-object v1, v1, LX/8Tm;->c:LX/8TD;

    sget-object v2, LX/8TE;->GAME_RECOMMENDATIONS_FETCH:LX/8TE;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 1349060
    iget-object v1, p0, LX/8Tl;->a:LX/8Wz;

    .line 1349061
    iget-object v2, v1, LX/8Wz;->a:LX/8X0;

    iget-object v2, v2, LX/8X0;->a:Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;

    .line 1349062
    iput-object v0, v2, Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;->b:Ljava/util/List;

    .line 1349063
    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 1349064
    iget-object v2, v1, LX/8Wz;->a:LX/8X0;

    iget-object v2, v2, LX/8X0;->e:LX/8XO;

    if-eqz v2, :cond_3

    .line 1349065
    iget-object v2, v1, LX/8Wz;->a:LX/8X0;

    iget-object v2, v2, LX/8X0;->e:LX/8XO;

    .line 1349066
    iget-object v1, v2, LX/8XO;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 1349067
    :cond_3
    goto :goto_3

    .line 1349068
    :cond_4
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "null result"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/8Tl;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    move v3, v8

    goto/16 :goto_2

    :cond_6
    move-object v6, v4

    move-object v4, v5

    goto/16 :goto_1

    :cond_7
    move-object v4, v5

    move-object v6, v5

    goto/16 :goto_1
.end method
