.class public final enum LX/8QA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8QA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8QA;

.field public static final enum FETCH_PRIVACY_FROM_ATF:LX/8QA;

.field public static final enum SET_ALBUM_PRIVACY:LX/8QA;

.field public static final enum SET_STORY_PRIVACY_FROM_ATF:LX/8QA;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1342597
    new-instance v0, LX/8QA;

    const-string v1, "FETCH_PRIVACY_FROM_ATF"

    invoke-direct {v0, v1, v2}, LX/8QA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8QA;->FETCH_PRIVACY_FROM_ATF:LX/8QA;

    .line 1342598
    new-instance v0, LX/8QA;

    const-string v1, "SET_STORY_PRIVACY_FROM_ATF"

    invoke-direct {v0, v1, v3}, LX/8QA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8QA;->SET_STORY_PRIVACY_FROM_ATF:LX/8QA;

    .line 1342599
    new-instance v0, LX/8QA;

    const-string v1, "SET_ALBUM_PRIVACY"

    invoke-direct {v0, v1, v4}, LX/8QA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8QA;->SET_ALBUM_PRIVACY:LX/8QA;

    .line 1342600
    const/4 v0, 0x3

    new-array v0, v0, [LX/8QA;

    sget-object v1, LX/8QA;->FETCH_PRIVACY_FROM_ATF:LX/8QA;

    aput-object v1, v0, v2

    sget-object v1, LX/8QA;->SET_STORY_PRIVACY_FROM_ATF:LX/8QA;

    aput-object v1, v0, v3

    sget-object v1, LX/8QA;->SET_ALBUM_PRIVACY:LX/8QA;

    aput-object v1, v0, v4

    sput-object v0, LX/8QA;->$VALUES:[LX/8QA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1342596
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8QA;
    .locals 1

    .prologue
    .line 1342602
    const-class v0, LX/8QA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8QA;

    return-object v0
.end method

.method public static values()[LX/8QA;
    .locals 1

    .prologue
    .line 1342601
    sget-object v0, LX/8QA;->$VALUES:[LX/8QA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8QA;

    return-object v0
.end method
