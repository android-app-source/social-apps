.class public final LX/7sz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1266264
    const/4 v10, 0x0

    .line 1266265
    const/4 v9, 0x0

    .line 1266266
    const/4 v8, 0x0

    .line 1266267
    const/4 v7, 0x0

    .line 1266268
    const/4 v6, 0x0

    .line 1266269
    const/4 v5, 0x0

    .line 1266270
    const/4 v4, 0x0

    .line 1266271
    const/4 v3, 0x0

    .line 1266272
    const/4 v2, 0x0

    .line 1266273
    const/4 v1, 0x0

    .line 1266274
    const/4 v0, 0x0

    .line 1266275
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 1266276
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1266277
    const/4 v0, 0x0

    .line 1266278
    :goto_0
    return v0

    .line 1266279
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1266280
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_c

    .line 1266281
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1266282
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1266283
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1266284
    const-string v12, "address_one"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1266285
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1266286
    :cond_2
    const-string v12, "address_two"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1266287
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1266288
    :cond_3
    const-string v12, "city"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1266289
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1266290
    :cond_4
    const-string v12, "email"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1266291
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1266292
    :cond_5
    const-string v12, "first_name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1266293
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1266294
    :cond_6
    const-string v12, "item_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1266295
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1266296
    :cond_7
    const-string v12, "last_name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1266297
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 1266298
    :cond_8
    const-string v12, "phone"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1266299
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1266300
    :cond_9
    const-string v12, "state"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1266301
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 1266302
    :cond_a
    const-string v12, "value"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 1266303
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 1266304
    :cond_b
    const-string v12, "zip_code"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1266305
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 1266306
    :cond_c
    const/16 v11, 0xb

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1266307
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 1266308
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1266309
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1266310
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1266311
    const/4 v7, 0x4

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 1266312
    const/4 v6, 0x5

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 1266313
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 1266314
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 1266315
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1266316
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 1266317
    const/16 v1, 0xa

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1266318
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 1266319
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1266320
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266321
    if-eqz v0, :cond_0

    .line 1266322
    const-string v1, "address_one"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266323
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266324
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266325
    if-eqz v0, :cond_1

    .line 1266326
    const-string v1, "address_two"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266327
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266328
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266329
    if-eqz v0, :cond_2

    .line 1266330
    const-string v1, "city"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266331
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266332
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266333
    if-eqz v0, :cond_3

    .line 1266334
    const-string v1, "email"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266335
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266336
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266337
    if-eqz v0, :cond_4

    .line 1266338
    const-string v1, "first_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266339
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266340
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266341
    if-eqz v0, :cond_5

    .line 1266342
    const-string v1, "item_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266343
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266344
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266345
    if-eqz v0, :cond_6

    .line 1266346
    const-string v1, "last_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266347
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266348
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266349
    if-eqz v0, :cond_7

    .line 1266350
    const-string v1, "phone"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266351
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266352
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266353
    if-eqz v0, :cond_8

    .line 1266354
    const-string v1, "state"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266355
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266356
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266357
    if-eqz v0, :cond_9

    .line 1266358
    const-string v1, "value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266359
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266360
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1266361
    if-eqz v0, :cond_a

    .line 1266362
    const-string v1, "zip_code"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1266363
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1266364
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1266365
    return-void
.end method
