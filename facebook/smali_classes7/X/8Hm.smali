.class public LX/8Hm;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field private a:Landroid/view/View;

.field private b:LX/8Hk;

.field private c:LX/8Hl;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/view/View;LX/8Hk;LX/8Hl;II)V
    .locals 0

    .prologue
    .line 1321532
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1321533
    iput-object p1, p0, LX/8Hm;->a:Landroid/view/View;

    .line 1321534
    iput-object p2, p0, LX/8Hm;->b:LX/8Hk;

    .line 1321535
    iput-object p3, p0, LX/8Hm;->c:LX/8Hl;

    .line 1321536
    iput p4, p0, LX/8Hm;->d:I

    .line 1321537
    iput p5, p0, LX/8Hm;->e:I

    .line 1321538
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 1321539
    iget-object v0, p0, LX/8Hm;->c:LX/8Hl;

    sget-object v1, LX/8Hl;->COLLAPSE:LX/8Hl;

    if-ne v0, v1, :cond_0

    .line 1321540
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float p1, v0, p1

    .line 1321541
    :cond_0
    iget v0, p0, LX/8Hm;->d:I

    iget v1, p0, LX/8Hm;->e:I

    iget v2, p0, LX/8Hm;->d:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 1321542
    iget-object v1, p0, LX/8Hm;->b:LX/8Hk;

    sget-object v2, LX/8Hk;->WIDTH:LX/8Hk;

    if-ne v1, v2, :cond_1

    .line 1321543
    iget-object v1, p0, LX/8Hm;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1321544
    :goto_0
    iget-object v0, p0, LX/8Hm;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1321545
    return-void

    .line 1321546
    :cond_1
    iget-object v1, p0, LX/8Hm;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method public final willChangeBounds()Z
    .locals 1

    .prologue
    .line 1321547
    const/4 v0, 0x1

    return v0
.end method
