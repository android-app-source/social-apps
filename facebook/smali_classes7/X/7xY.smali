.class public LX/7xY;
.super LX/62U;
.source ""


# instance fields
.field public m:Lcom/facebook/resources/ui/FbTextView;

.field public n:Lcom/facebook/resources/ui/FbTextView;

.field public o:Lcom/facebook/resources/ui/FbTextView;

.field public p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

.field private q:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

.field public r:I

.field public s:LX/7we;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final t:LX/7xW;

.field public u:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/7xC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/7xH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1277492
    invoke-direct {p0, p1}, LX/62U;-><init>(Landroid/view/View;)V

    .line 1277493
    new-instance v0, LX/7xX;

    invoke-direct {v0, p0}, LX/7xX;-><init>(LX/7xY;)V

    iput-object v0, p0, LX/7xY;->t:LX/7xW;

    .line 1277494
    const-class v0, LX/7xY;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, LX/7xY;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1277495
    const v0, 0x7f0d0e7b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7xY;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 1277496
    const v0, 0x7f0d0e7c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7xY;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1277497
    const v0, 0x7f0d0e7d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/7xY;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 1277498
    const v0, 0x7f0d0e7f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    iput-object v0, p0, LX/7xY;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    .line 1277499
    const v0, 0x7f0d0e7e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1277500
    const v1, 0x7f0304a1

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1277501
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    iput-object v0, p0, LX/7xY;->q:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    .line 1277502
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/7xY;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/7xC;->a(LX/0QB;)LX/7xC;

    move-result-object v1

    check-cast v1, LX/7xC;

    invoke-static {p0}, LX/7xH;->a(LX/0QB;)LX/7xH;

    move-result-object p0

    check-cast p0, LX/7xH;

    iput-object v0, p1, LX/7xY;->u:Landroid/content/Context;

    iput-object v1, p1, LX/7xY;->v:LX/7xC;

    iput-object p0, p1, LX/7xY;->w:LX/7xH;

    return-void
.end method

.method public static b(LX/7xY;Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1277480
    if-eqz p2, :cond_0

    .line 1277481
    iget-object v0, p0, LX/7xY;->q:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    invoke-virtual {v0, v2}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->setMinimumQuantity(I)V

    .line 1277482
    iget-object v0, p0, LX/7xY;->q:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    iget v1, p1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->h:I

    .line 1277483
    iput v1, v0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->e:I

    .line 1277484
    iget-object v0, p0, LX/7xY;->q:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    iget v1, p1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    invoke-virtual {v0, v1}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->setCurrentQuantity(I)V

    .line 1277485
    :goto_0
    iget-object v0, p0, LX/7xY;->q:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    iget v1, p1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->g:I

    invoke-virtual {v0, v1}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->setMaximumQuantity(I)V

    .line 1277486
    iget-object v0, p0, LX/7xY;->q:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    iget-object v1, p0, LX/7xY;->t:LX/7xW;

    .line 1277487
    iput-object v1, v0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->i:LX/7xW;

    .line 1277488
    iget-object v0, p0, LX/7xY;->q:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    invoke-virtual {v0, v2}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->setVisibility(I)V

    .line 1277489
    return-void

    .line 1277490
    :cond_0
    iget-object v0, p0, LX/7xY;->q:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    iget v1, p1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->h:I

    invoke-virtual {v0, v1}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->setMinimumQuantity(I)V

    .line 1277491
    iget-object v0, p0, LX/7xY;->q:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    iget v1, p1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->h:I

    invoke-virtual {v0, v1}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->setCurrentQuantity(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(ILcom/facebook/events/tickets/modal/model/EventTicketTierModel;ZLX/7we;)V
    .locals 9

    .prologue
    const/16 v1, 0x8

    .line 1277453
    iput p1, p0, LX/7xY;->r:I

    .line 1277454
    iput-object p4, p0, LX/7xY;->s:LX/7we;

    .line 1277455
    iget-object v0, p2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1277456
    iget-object v0, p0, LX/7xY;->m:Lcom/facebook/resources/ui/FbTextView;

    iget-object p1, p0, LX/7xY;->w:LX/7xH;

    iget-object p4, p2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->i:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, p4}, LX/7xH;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277457
    iget-object v0, p0, LX/7xY;->n:Lcom/facebook/resources/ui/FbTextView;

    const/16 p1, 0x8

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277458
    :goto_0
    iget-object v0, p0, LX/7xY;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277459
    iget-object v0, p0, LX/7xY;->q:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    invoke-virtual {v0, v1}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->setVisibility(I)V

    .line 1277460
    const/4 v8, 0x0

    .line 1277461
    iget-wide v2, p2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->e:J

    .line 1277462
    iget-wide v4, p2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->f:J

    .line 1277463
    sget v6, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->a:I

    int-to-long v6, v6

    cmp-long v6, v4, v6

    if-nez v6, :cond_2

    .line 1277464
    invoke-static {p0, p2, p3}, LX/7xY;->b(LX/7xY;Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;Z)V

    .line 1277465
    :cond_0
    :goto_1
    iget-object v0, p2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->j:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1277466
    iget-object v0, p0, LX/7xY;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setVisibility(I)V

    .line 1277467
    :goto_2
    return-void

    .line 1277468
    :cond_1
    iget-object v0, p0, LX/7xY;->m:Lcom/facebook/resources/ui/FbTextView;

    iget-object p1, p2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->c:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277469
    iget-object v0, p0, LX/7xY;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object p1, p0, LX/7xY;->w:LX/7xH;

    iget-object p4, p2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->i:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, p4}, LX/7xH;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1277470
    :cond_2
    iget-object v6, p2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->d:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;->PRE_SALE:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    if-ne v6, v7, :cond_3

    .line 1277471
    iget-object v4, p0, LX/7xY;->o:Lcom/facebook/resources/ui/FbTextView;

    iget-object v5, p0, LX/7xY;->v:LX/7xC;

    invoke-virtual {v5, v2, v3}, LX/7xC;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277472
    iget-object v2, p0, LX/7xY;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1

    .line 1277473
    :cond_3
    iget-object v2, p2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->d:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;->ON_SALE:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    if-ne v2, v3, :cond_0

    .line 1277474
    invoke-static {v4, v5}, LX/7xC;->c(J)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1277475
    iget-object v2, p0, LX/7xY;->o:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, LX/7xY;->v:LX/7xC;

    invoke-virtual {v3, v4, v5}, LX/7xC;->b(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277476
    iget-object v2, p0, LX/7xY;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277477
    :cond_4
    invoke-static {p0, p2, p3}, LX/7xY;->b(LX/7xY;Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;Z)V

    goto :goto_1

    .line 1277478
    :cond_5
    iget-object v0, p0, LX/7xY;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    iget-object v1, p2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277479
    iget-object v0, p0, LX/7xY;->p:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setVisibility(I)V

    goto :goto_2
.end method
