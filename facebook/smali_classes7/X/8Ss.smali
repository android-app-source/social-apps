.class public final LX/8Ss;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/QuicksilverFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V
    .locals 0

    .prologue
    .line 1347394
    iput-object p1, p0, LX/8Ss;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;)V
    .locals 4

    .prologue
    .line 1347395
    invoke-virtual {p1}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1347396
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Could not retrieve player token for instant game"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/8Ss;->a(Ljava/lang/Throwable;)V

    .line 1347397
    :cond_0
    :goto_0
    return-void

    .line 1347398
    :cond_1
    iget-object v0, p0, LX/8Ss;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v1, LX/8TE;->SDK_INFO_FETCH:LX/8TE;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 1347399
    iget-object v0, p0, LX/8Ss;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->x:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    .line 1347400
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1347401
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1347402
    const/16 v2, 0x5f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1347403
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1347404
    iget-object v0, p0, LX/8Ss;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1347405
    iput-object v1, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->j:Ljava/lang/String;

    .line 1347406
    iget-object v0, p0, LX/8Ss;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {p1}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1347407
    iput-object v1, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->h:Ljava/lang/String;

    .line 1347408
    invoke-virtual {p1}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->j()Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1347409
    iget-object v0, p0, LX/8Ss;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {p1}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->j()Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1347410
    iput-object v1, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->i:Ljava/lang/String;

    .line 1347411
    :cond_2
    iget-object v0, p0, LX/8Ss;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1347412
    if-eqz v0, :cond_0

    .line 1347413
    new-instance v1, Lcom/facebook/quicksilver/QuicksilverFragment$8$1;

    invoke-direct {v1, p0}, Lcom/facebook/quicksilver/QuicksilverFragment$8$1;-><init>(LX/8Ss;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1347414
    iget-object v0, p0, LX/8Ss;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v1, LX/8TE;->SDK_INFO_FETCH:LX/8TE;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 1347415
    iget-object v0, p0, LX/8Ss;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1347416
    if-nez v0, :cond_0

    .line 1347417
    :goto_0
    return-void

    .line 1347418
    :cond_0
    new-instance v1, Lcom/facebook/quicksilver/QuicksilverFragment$8$2;

    invoke-direct {v1, p0}, Lcom/facebook/quicksilver/QuicksilverFragment$8$2;-><init>(LX/8Ss;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
