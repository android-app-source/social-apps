.class public LX/6zC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wl;


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1160854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160855
    iput-object p1, p0, LX/6zC;->a:Landroid/content/res/Resources;

    .line 1160856
    return-void
.end method

.method public static a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)I
    .locals 1

    .prologue
    .line 1160857
    sget-object v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->AMEX:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/6zC;
    .locals 2

    .prologue
    .line 1160858
    new-instance v1, LX/6zC;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/6zC;-><init>(Landroid/content/res/Resources;)V

    .line 1160859
    return-object v1
.end method


# virtual methods
.method public final a(LX/6z8;)Z
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1160860
    check-cast p1, LX/6zD;

    .line 1160861
    invoke-virtual {p1}, LX/6zD;->a()Ljava/lang/String;

    move-result-object v2

    .line 1160862
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1160863
    :cond_0
    :goto_0
    return v0

    .line 1160864
    :cond_1
    const-string v3, "\\d{3,4}"

    invoke-virtual {v2, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1160865
    iget-object v3, p1, LX/6zD;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-object v3, v3

    .line 1160866
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    .line 1160867
    sget-object v4, LX/6zB;->a:[I

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    .line 1160868
    if-ne v2, v5, :cond_0

    move v0, v1

    goto :goto_0

    .line 1160869
    :pswitch_0
    if-ne v2, v6, :cond_0

    move v0, v1

    goto :goto_0

    .line 1160870
    :pswitch_1
    if-eq v2, v6, :cond_2

    if-ne v2, v5, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(LX/6z8;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1160871
    check-cast p1, LX/6zD;

    .line 1160872
    iget-object v0, p1, LX/6zD;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-object v0, v0

    .line 1160873
    sget-object v1, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->AMEX:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/6zC;->a:Landroid/content/res/Resources;

    const v1, 0x7f081e72

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6zC;->a:Landroid/content/res/Resources;

    const v1, 0x7f081e71

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
