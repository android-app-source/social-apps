.class public final LX/7wc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V
    .locals 0

    .prologue
    .line 1276011
    iput-object p1, p0, LX/7wc;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 7

    .prologue
    .line 1276012
    iget-object v0, p0, LX/7wc;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->N:LX/7wp;

    sget-object v1, LX/7wp;->SELECT:LX/7wp;

    if-ne v0, v1, :cond_0

    .line 1276013
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 1276014
    iget-object v0, p0, LX/7wc;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    .line 1276015
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 1276016
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1276017
    :goto_0
    iget-object v0, p0, LX/7wc;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    const/4 v5, 0x0

    .line 1276018
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v3, v2

    .line 1276019
    if-nez v3, :cond_2

    .line 1276020
    const/4 v2, 0x0

    .line 1276021
    :goto_1
    move-object v0, v2

    .line 1276022
    if-eqz v0, :cond_0

    .line 1276023
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1276024
    :cond_0
    return-void

    .line 1276025
    :cond_1
    iget-object v0, p0, LX/7wc;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    .line 1276026
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 1276027
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 1276028
    :cond_2
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 1276029
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0b1476

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1276030
    iget-object v6, v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->q:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v6}, Lcom/facebook/widget/CustomLinearLayout;->getHeight()I

    move-result v6

    sub-int/2addr v6, v4

    if-lez v6, :cond_3

    iget-object v6, v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->q:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v6}, Lcom/facebook/widget/CustomLinearLayout;->getHeight()I

    move-result v6

    sub-int v4, v6, v4

    :goto_2
    move v4, v4

    .line 1276031
    aput v4, v2, v5

    const/4 v4, 0x1

    aput v5, v2, v4

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 1276032
    new-instance v4, LX/7wh;

    invoke-direct {v4, v0, v3}, LX/7wh;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;Landroid/view/View;)V

    invoke-virtual {v2, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1276033
    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    goto :goto_2
.end method
