.class public LX/7Tu;
.super LX/25T;
.source ""


# instance fields
.field public final e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

.field private final f:LX/7Ts;

.field public final g:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Ljava/util/List",
            "<",
            "LX/1a1;",
            ">;>;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/1OD;

.field private final t:Z

.field public u:Z

.field public v:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;Landroid/content/Context;LX/25U;LX/25V;LX/0ad;)V
    .locals 3
    .param p1    # Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1211159
    invoke-direct {p0, p2, p3, p4}, LX/25T;-><init>(Landroid/content/Context;LX/25U;LX/25V;)V

    .line 1211160
    new-instance v1, LX/7Ts;

    invoke-direct {v1}, LX/7Ts;-><init>()V

    iput-object v1, p0, LX/7Tu;->f:LX/7Ts;

    .line 1211161
    new-instance v1, LX/0YU;

    invoke-direct {v1}, LX/0YU;-><init>()V

    iput-object v1, p0, LX/7Tu;->g:LX/0YU;

    .line 1211162
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, LX/7Tu;->h:Ljava/util/Set;

    .line 1211163
    new-instance v1, LX/7Tr;

    invoke-direct {v1, p0}, LX/7Tr;-><init>(LX/7Tu;)V

    iput-object v1, p0, LX/7Tu;->i:LX/1OD;

    .line 1211164
    const/4 v1, 0x0

    iput-object v1, p0, LX/7Tu;->v:Landroid/view/View;

    .line 1211165
    iput-object p1, p0, LX/7Tu;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1211166
    iget-object v1, p0, LX/7Tu;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    new-instance v2, LX/7Tt;

    invoke-direct {v2, p0}, LX/7Tt;-><init>(LX/7Tu;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setViewCacheExtension(LX/1PB;)V

    .line 1211167
    invoke-virtual {p0, v0}, LX/1P1;->b(I)V

    .line 1211168
    if-nez p5, :cond_0

    :goto_0
    iput-boolean v0, p0, LX/7Tu;->t:Z

    .line 1211169
    return-void

    .line 1211170
    :cond_0
    sget-short v1, LX/1PC;->a:S

    invoke-interface {p5, v1, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 1211145
    iget-object v0, p0, LX/7Tu;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v1

    .line 1211146
    if-eqz p2, :cond_1

    .line 1211147
    iget-object v0, p0, LX/7Tu;->h:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1211148
    :goto_0
    invoke-virtual {p0, p1}, LX/1OR;->e(Landroid/view/View;)V

    .line 1211149
    iget v0, v1, LX/1a1;->e:I

    move v2, v0

    .line 1211150
    iget-object v0, p0, LX/7Tu;->g:LX/0YU;

    invoke-virtual {v0, v2}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1211151
    if-nez v0, :cond_0

    .line 1211152
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1211153
    iget-object v3, p0, LX/7Tu;->g:LX/0YU;

    invoke-virtual {v3, v2, v0}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 1211154
    :cond_0
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1211155
    return-void

    .line 1211156
    :cond_1
    iget-object v0, p0, LX/7Tu;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1211157
    iget-object v2, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v2

    .line 1211158
    invoke-virtual {v0, v1}, LX/1OM;->a(LX/1a1;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/7Tu;LX/1a1;IZ)V
    .locals 2

    .prologue
    .line 1211132
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/1aL;->b(Landroid/view/View;)LX/1Ra;

    move-result-object v0

    .line 1211133
    iget-object v1, p0, LX/7Tu;->h:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1211134
    :goto_0
    if-eqz p3, :cond_1

    if-eqz v0, :cond_1

    .line 1211135
    iget-object v0, p0, LX/7Tu;->f:LX/7Ts;

    invoke-virtual {v0, p1, p2}, LX/1OM;->b(LX/1a1;I)V

    .line 1211136
    :goto_1
    return-void

    .line 1211137
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1211138
    :cond_1
    if-eqz v0, :cond_2

    .line 1211139
    iget-object v0, p0, LX/7Tu;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1211140
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 1211141
    invoke-virtual {v0, p1}, LX/1OM;->a(LX/1a1;)V

    .line 1211142
    :cond_2
    iget-object v0, p0, LX/7Tu;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1211143
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 1211144
    invoke-virtual {v0, p1, p2}, LX/1OM;->b(LX/1a1;I)V

    goto :goto_1
.end method

.method private f(II)I
    .locals 1

    .prologue
    .line 1211130
    sparse-switch p1, :sswitch_data_0

    .line 1211131
    iget-object v0, p0, LX/7Tu;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-static {v0}, LX/0vv;->u(Landroid/view/View;)I

    move-result p2

    :sswitch_0
    return p2

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method private m(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 1211127
    iget-boolean v0, p0, LX/7Tu;->t:Z

    if-nez v0, :cond_0

    instance-of v0, p1, LX/1a7;

    if-eqz v0, :cond_0

    .line 1211128
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    invoke-virtual {v0}, LX/1a3;->c()Z

    move-result v0

    move v0, v0

    .line 1211129
    if-nez v0, :cond_0

    check-cast p1, LX/1a7;

    invoke-interface {p1}, LX/1a7;->cr_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILX/1Od;)V
    .locals 1

    .prologue
    .line 1211045
    invoke-virtual {p0, p1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/1OR;->a(Landroid/view/View;LX/1Od;)V

    .line 1211046
    return-void
.end method

.method public final a(LX/1OM;LX/1OM;)V
    .locals 1

    .prologue
    .line 1211121
    if-eqz p1, :cond_0

    .line 1211122
    iget-object v0, p0, LX/7Tu;->i:LX/1OD;

    invoke-virtual {p1, v0}, LX/1OM;->b(LX/1OD;)V

    .line 1211123
    :cond_0
    if-eqz p2, :cond_1

    .line 1211124
    iget-object v0, p0, LX/7Tu;->i:LX/1OD;

    invoke-virtual {p2, v0}, LX/1OM;->a(LX/1OD;)V

    .line 1211125
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/7Tu;->v:Landroid/view/View;

    .line 1211126
    return-void
.end method

.method public final a(LX/1Od;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1211111
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    .line 1211112
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_2

    .line 1211113
    invoke-virtual {p0, v2}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v3

    .line 1211114
    invoke-direct {p0, v3}, LX/7Tu;->m(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211115
    iget-boolean v0, p0, LX/7Tu;->u:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v3, v0}, LX/7Tu;->a(Landroid/view/View;Z)V

    .line 1211116
    :cond_0
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1211117
    goto :goto_1

    .line 1211118
    :cond_2
    iput-boolean v1, p0, LX/7Tu;->u:Z

    .line 1211119
    invoke-super {p0, p1}, LX/25T;->a(LX/1Od;)V

    .line 1211120
    return-void
.end method

.method public final a(LX/1Od;LX/1Ok;II)V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/high16 v7, 0x40000000    # 2.0f

    .line 1211057
    :try_start_0
    const-string v0, "HScrollLinearLayoutManager.onMeasure"

    const v1, -0x4c8ddc80

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1211058
    iget-object v0, p0, LX/7Tu;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1211059
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 1211060
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 1211061
    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 1211062
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 1211063
    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 1211064
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v5

    if-nez v5, :cond_1

    .line 1211065
    :cond_0
    invoke-direct {p0, v1, v3}, LX/7Tu;->f(II)I

    move-result v0

    invoke-direct {p0, v2, v4}, LX/7Tu;->f(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/1OR;->e(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1211066
    const v0, -0x5caed44

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1211067
    :goto_0
    return-void

    .line 1211068
    :cond_1
    :try_start_1
    iget-object v5, p0, LX/7Tu;->v:Landroid/view/View;

    if-nez v5, :cond_2

    .line 1211069
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, LX/1OM;->getItemViewType(I)I

    move-result v5

    .line 1211070
    iget-object v6, p0, LX/7Tu;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v0, v6, v5}, LX/1OM;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    iput-object v0, p0, LX/7Tu;->v:Landroid/view/View;

    .line 1211071
    :cond_2
    if-ne v1, v7, :cond_3

    .line 1211072
    iget v0, p0, LX/1P1;->j:I

    move v0, v0

    .line 1211073
    if-eq v0, v8, :cond_4

    :cond_3
    if-ne v2, v7, :cond_5

    .line 1211074
    iget v0, p0, LX/1P1;->j:I

    move v0, v0

    .line 1211075
    if-nez v0, :cond_5

    .line 1211076
    :cond_4
    invoke-super {p0, p1, p2, p3, p4}, LX/25T;->a(LX/1Od;LX/1Ok;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1211077
    const v0, 0x30277d35

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 1211078
    :cond_5
    :try_start_2
    iget-object v0, p0, LX/7Tu;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1211079
    iget-object v5, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v5

    .line 1211080
    instance-of v0, v0, LX/2eO;

    if-eqz v0, :cond_b

    .line 1211081
    iget-object v0, p0, LX/7Tu;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1211082
    iget-object v5, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v5

    .line 1211083
    check-cast v0, LX/2eO;

    .line 1211084
    iget-object v5, v0, LX/2eO;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object v0, v5

    .line 1211085
    :goto_1
    move-object v5, v0

    .line 1211086
    iget-object v0, p0, LX/25T;->d:LX/25V;

    invoke-virtual {v0, v5}, LX/25V;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1211087
    iget-object v0, p0, LX/25T;->d:LX/25V;

    invoke-virtual {v0, v5}, LX/25V;->a(Ljava/lang/String;)[I

    move-result-object v0

    .line 1211088
    :cond_6
    :goto_2
    if-ne v1, v7, :cond_7

    .line 1211089
    const/4 v5, 0x0

    invoke-direct {p0, v1, v3}, LX/7Tu;->f(II)I

    move-result v1

    aput v1, v0, v5

    .line 1211090
    :cond_7
    if-ne v2, v7, :cond_8

    .line 1211091
    const/4 v1, 0x1

    invoke-direct {p0, v2, v4}, LX/7Tu;->f(II)I

    move-result v2

    aput v2, v0, v1

    .line 1211092
    :cond_8
    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v0, v0, v2

    invoke-virtual {p0, v1, v0}, LX/1OR;->e(II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1211093
    const v0, -0x3e8be5f8

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 1211094
    :cond_9
    const/4 v0, 0x2

    :try_start_3
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 1211095
    invoke-virtual {p0}, LX/1OR;->D()I

    move-result v6

    if-lez v6, :cond_6

    .line 1211096
    const/4 p4, 0x1

    const/4 p3, 0x0

    .line 1211097
    iget-object v9, p0, LX/7Tu;->v:Landroid/view/View;

    iget-object v6, p0, LX/7Tu;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1211098
    iget-object v8, v6, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v6, v8

    .line 1211099
    check-cast v6, LX/2eO;

    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    invoke-static {v9, v6, p1, p2}, LX/25U;->a(Landroid/view/View;LX/2eO;II)[I

    move-result-object v6

    iput-object v6, p0, LX/7Tu;->b:[I

    .line 1211100
    iget v6, p0, LX/1P1;->j:I

    move v6, v6

    .line 1211101
    if-nez v6, :cond_c

    .line 1211102
    aget v6, v0, p3

    iget-object v8, p0, LX/25T;->b:[I

    aget v8, v8, p3

    add-int/2addr v6, v8

    aput v6, v0, p3

    .line 1211103
    iget-object v6, p0, LX/25T;->b:[I

    aget v6, v6, p4

    invoke-virtual {p0}, LX/1OR;->z()I

    move-result v8

    add-int/2addr v6, v8

    invoke-virtual {p0}, LX/1OR;->B()I

    move-result v8

    add-int/2addr v6, v8

    aput v6, v0, p4

    .line 1211104
    :goto_3
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 1211105
    iget-object v6, p0, LX/25T;->d:LX/25V;

    invoke-virtual {v6, v5, v0}, LX/25V;->a(Ljava/lang/String;[I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1211106
    :cond_a
    goto :goto_2

    .line 1211107
    :catchall_0
    move-exception v0

    const v1, -0x4ca6cf64

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 1211108
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :cond_b
    :try_start_4
    iget-object v0, p0, LX/25T;->a:Ljava/lang/String;

    goto/16 :goto_1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1211109
    :cond_c
    aget v6, v0, p4

    iget-object v8, p0, LX/25T;->b:[I

    aget v8, v8, p4

    add-int/2addr v6, v8

    aput v6, v0, p4

    .line 1211110
    iget-object v6, p0, LX/25T;->b:[I

    aget v6, v6, p3

    invoke-virtual {p0}, LX/1OR;->y()I

    move-result v8

    add-int/2addr v6, v8

    invoke-virtual {p0}, LX/1OR;->A()I

    move-result v8

    add-int/2addr v6, v8

    aput v6, v0, p3

    goto :goto_3
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V
    .locals 2

    .prologue
    .line 1211051
    iget-object v0, p0, LX/7Tu;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 1211052
    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-super {p0, v0, p2}, LX/25T;->a(Landroid/view/View;LX/1Od;)V

    goto :goto_0

    .line 1211053
    :cond_0
    iget-object v0, p0, LX/7Tu;->g:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->b()V

    .line 1211054
    iget-object v0, p0, LX/7Tu;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1211055
    invoke-super {p0, p1, p2}, LX/25T;->a(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V

    .line 1211056
    return-void
.end method

.method public final a(Landroid/view/View;LX/1Od;)V
    .locals 1

    .prologue
    .line 1211047
    invoke-direct {p0, p1}, LX/7Tu;->m(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211048
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7Tu;->a(Landroid/view/View;Z)V

    .line 1211049
    :goto_0
    return-void

    .line 1211050
    :cond_0
    invoke-super {p0, p1, p2}, LX/25T;->a(Landroid/view/View;LX/1Od;)V

    goto :goto_0
.end method
