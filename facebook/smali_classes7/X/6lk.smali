.class public LX/6lk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/CJj;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field public final c:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1142908
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/6lk;-><init>(Ljava/lang/String;Landroid/os/Bundle;LX/CJj;)V

    .line 1142909
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/os/Bundle;LX/CJj;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/CJj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1142918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142919
    iput-object p1, p0, LX/6lk;->b:Ljava/lang/String;

    .line 1142920
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/6lk;->c:Landroid/os/Bundle;

    .line 1142921
    if-eqz p2, :cond_0

    .line 1142922
    iget-object v0, p0, LX/6lk;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1142923
    :cond_0
    iput-object p3, p0, LX/6lk;->a:LX/CJj;

    .line 1142924
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1142911
    if-ne p0, p1, :cond_0

    .line 1142912
    const/4 v0, 0x1

    .line 1142913
    :goto_0
    return v0

    .line 1142914
    :cond_0
    if-eqz p1, :cond_1

    instance-of v0, p1, LX/6lk;

    if-nez v0, :cond_2

    .line 1142915
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1142916
    :cond_2
    check-cast p1, LX/6lk;

    .line 1142917
    iget-object v0, p1, LX/6lk;->b:Ljava/lang/String;

    iget-object v1, p0, LX/6lk;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1142910
    iget-object v0, p0, LX/6lk;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
