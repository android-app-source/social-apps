.class public LX/6fA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field private final a:Lcom/facebook/user/model/UserKey;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1119334
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/6fA;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/user/model/UserKey;)V
    .locals 0
    .param p1    # Lcom/facebook/user/model/UserKey;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserKey;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1119335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119336
    iput-object p1, p0, LX/6fA;->a:Lcom/facebook/user/model/UserKey;

    .line 1119337
    return-void
.end method

.method public static a(LX/0QB;)LX/6fA;
    .locals 7

    .prologue
    .line 1119338
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1119339
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1119340
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1119341
    if-nez v1, :cond_0

    .line 1119342
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1119343
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1119344
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1119345
    sget-object v1, LX/6fA;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1119346
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1119347
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1119348
    :cond_1
    if-nez v1, :cond_4

    .line 1119349
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1119350
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1119351
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1119352
    new-instance p0, LX/6fA;

    invoke-static {v0}, LX/2Ot;->b(LX/0QB;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/UserKey;

    invoke-direct {p0, v1}, LX/6fA;-><init>(Lcom/facebook/user/model/UserKey;)V

    .line 1119353
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1119354
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1119355
    if-nez v1, :cond_2

    .line 1119356
    sget-object v0, LX/6fA;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6fA;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1119357
    :goto_1
    if-eqz v0, :cond_3

    .line 1119358
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1119359
    :goto_3
    check-cast v0, LX/6fA;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1119360
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1119361
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1119362
    :catchall_1
    move-exception v0

    .line 1119363
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1119364
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1119365
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1119366
    :cond_2
    :try_start_8
    sget-object v0, LX/6fA;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6fA;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method
