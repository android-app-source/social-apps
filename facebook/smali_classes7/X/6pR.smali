.class public final LX/6pR;
.super LX/6pP;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

.field public final synthetic b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

.field public final synthetic c:LX/6pZ;


# direct methods
.method public constructor <init>(LX/6pZ;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;)V
    .locals 0

    .prologue
    .line 1149427
    iput-object p1, p0, LX/6pR;->c:LX/6pZ;

    iput-object p2, p0, LX/6pR;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iput-object p3, p0, LX/6pR;->b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    invoke-direct {p0}, LX/6pP;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 1149428
    iget-object v0, p0, LX/6pR;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    sget-object v1, LX/6pM;->CHANGE_CREATE_NEW:LX/6pM;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(LX/6pM;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1149429
    iget-object v0, p0, LX/6pR;->c:LX/6pZ;

    iget-object v1, p0, LX/6pR;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v2, p0, LX/6pR;->b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    .line 1149430
    sget-object v3, LX/6pM;->CHANGE_ENTER_OLD:LX/6pM;

    invoke-virtual {v1, v3}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(LX/6pM;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 1149431
    iget-object v4, v0, LX/6pZ;->b:LX/6p6;

    invoke-virtual {v1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->b()J

    move-result-wide v5

    new-instance v9, LX/6pU;

    invoke-direct {v9, v0, v2, v1, p1}, LX/6pU;-><init>(LX/6pZ;Lcom/facebook/payments/auth/pin/EnterPinFragment;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Ljava/lang/String;)V

    move-object v8, p1

    invoke-virtual/range {v4 .. v9}, LX/6p6;->a(JLjava/lang/String;Ljava/lang/String;LX/6nn;)V

    .line 1149432
    :goto_0
    return-void

    .line 1149433
    :cond_0
    iget-object v0, p0, LX/6pR;->b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/EnterPinFragment;->c()V

    goto :goto_0
.end method
