.class public final LX/7SY;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;


# direct methods
.method public constructor <init>(Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;)V
    .locals 0

    .prologue
    .line 1208969
    iput-object p1, p0, LX/7SY;->a:Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1208953
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1208954
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1208955
    iget-object v1, p0, LX/7SY;->a:Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;

    iget-object v1, v1, Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;->a:LX/7SZ;

    iget-object v1, v1, LX/7SZ;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1208956
    :try_start_0
    iget-object v1, p0, LX/7SY;->a:Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;

    iget-object v2, v1, Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;->a:LX/7SZ;

    if-nez v0, :cond_1

    const/4 v1, 0x0

    .line 1208957
    :goto_0
    iput-object v1, v2, LX/7SZ;->h:LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1208958
    iget-object v1, p0, LX/7SY;->a:Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;

    iget-object v1, v1, Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;->a:LX/7SZ;

    iget-object v1, v1, LX/7SZ;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1208959
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1208960
    :cond_0
    return-void

    .line 1208961
    :cond_1
    :try_start_1
    invoke-virtual {v0}, LX/1FJ;->b()LX/1FJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 1208962
    :catchall_0
    move-exception v1

    iget-object v2, p0, LX/7SY;->a:Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;

    iget-object v2, v2, Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;->a:LX/7SZ;

    iget-object v2, v2, LX/7SZ;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1208963
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    throw v1
.end method

.method public final f(LX/1ca;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1208964
    invoke-interface {p1}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object v0

    .line 1208965
    if-nez v0, :cond_0

    .line 1208966
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "DataSourceFailed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 1208967
    :cond_0
    const-class v1, LX/7SZ;

    const-string v2, "Failed to retrieve texture image: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/7SY;->a:Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;

    iget-object v5, v5, Lcom/facebook/videocodec/effects/renderers/animatedsprites/StaticTexture$1;->a:LX/7SZ;

    iget-object v5, v5, LX/7SZ;->a:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1208968
    return-void
.end method
