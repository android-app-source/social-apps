.class public final LX/75M;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Lcom/facebook/photos/base/tagging/Tag;

.field private final d:LX/6Z0;

.field private final e:LX/0SG;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/photos/base/tagging/Tag;LX/6Z0;LX/0SG;)V
    .locals 0

    .prologue
    .line 1169259
    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 1169260
    iput-object p1, p0, LX/75M;->a:Landroid/content/Context;

    .line 1169261
    iput-object p2, p0, LX/75M;->b:Ljava/lang/String;

    .line 1169262
    iput-object p3, p0, LX/75M;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169263
    iput-object p4, p0, LX/75M;->d:LX/6Z0;

    .line 1169264
    iput-object p5, p0, LX/75M;->e:LX/0SG;

    .line 1169265
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1169266
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1169267
    sget-object v0, LX/6Yy;->a:LX/0U1;

    .line 1169268
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1169269
    iget-object v2, p0, LX/75M;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169270
    iget-wide v4, v2, Lcom/facebook/photos/base/tagging/Tag;->c:J

    move-wide v2, v4

    .line 1169271
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1169272
    sget-object v0, LX/6Yy;->b:LX/0U1;

    .line 1169273
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1169274
    iget-object v2, p0, LX/75M;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169275
    iget-object v3, v2, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v2, v3

    .line 1169276
    invoke-interface {v2}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-static {v2}, LX/75Q;->b(F)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1169277
    sget-object v0, LX/6Yy;->c:LX/0U1;

    .line 1169278
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1169279
    iget-object v2, p0, LX/75M;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169280
    iget-object v3, v2, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v2, v3

    .line 1169281
    invoke-interface {v2}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-static {v2}, LX/75Q;->b(F)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1169282
    sget-object v0, LX/6Yy;->d:LX/0U1;

    .line 1169283
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1169284
    iget-object v2, p0, LX/75M;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169285
    iget-object v3, v2, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v2, v3

    .line 1169286
    invoke-interface {v2}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->right:F

    invoke-static {v2}, LX/75Q;->b(F)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1169287
    sget-object v0, LX/6Yy;->e:LX/0U1;

    .line 1169288
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1169289
    iget-object v2, p0, LX/75M;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169290
    iget-object v3, v2, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v2, v3

    .line 1169291
    invoke-interface {v2}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    invoke-static {v2}, LX/75Q;->b(F)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1169292
    sget-object v0, LX/6Yy;->f:LX/0U1;

    .line 1169293
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1169294
    iget-object v2, p0, LX/75M;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169295
    iget-object v3, v2, Lcom/facebook/photos/base/tagging/Tag;->f:LX/7Gr;

    move-object v2, v3

    .line 1169296
    invoke-virtual {v2}, LX/7Gr;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1169297
    sget-object v0, LX/6Yy;->g:LX/0U1;

    .line 1169298
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1169299
    iget-object v0, p0, LX/75M;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169300
    iget-boolean v3, v0, Lcom/facebook/photos/base/tagging/Tag;->e:Z

    move v0, v3

    .line 1169301
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1169302
    sget-object v0, LX/6Yy;->h:LX/0U1;

    .line 1169303
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1169304
    iget-object v2, p0, LX/75M;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1169305
    sget-object v0, LX/6Yy;->i:LX/0U1;

    .line 1169306
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1169307
    iget-object v2, p0, LX/75M;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169308
    iget-object v3, v2, Lcom/facebook/photos/base/tagging/Tag;->b:Lcom/facebook/user/model/Name;

    move-object v2, v3

    .line 1169309
    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169310
    sget-object v0, LX/6Yy;->j:LX/0U1;

    .line 1169311
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1169312
    iget-object v2, p0, LX/75M;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169313
    iget-object v3, v2, Lcom/facebook/photos/base/tagging/Tag;->b:Lcom/facebook/user/model/Name;

    move-object v2, v3

    .line 1169314
    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169315
    sget-object v0, LX/6Yy;->k:LX/0U1;

    .line 1169316
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1169317
    iget-object v2, p0, LX/75M;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169318
    iget-object v0, p0, LX/75M;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, LX/75M;->d:LX/6Z0;

    iget-object v2, v2, LX/6Z0;->d:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1169319
    const/4 v0, 0x0

    return-object v0

    .line 1169320
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
