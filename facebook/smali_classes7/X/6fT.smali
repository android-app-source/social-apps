.class public LX/6fT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/share/ShareMedia;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/share/ShareProperty;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

.field public m:Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1119994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119995
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/6fT;->g:Ljava/util/List;

    .line 1119996
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/6fT;->h:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final n()Lcom/facebook/messaging/model/share/Share;
    .locals 1

    .prologue
    .line 1119997
    new-instance v0, Lcom/facebook/messaging/model/share/Share;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/share/Share;-><init>(LX/6fT;)V

    return-object v0
.end method
