.class public final LX/8Ct;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:I

.field public final b:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/8Cs;


# direct methods
.method public constructor <init>(IILX/8Cs;)V
    .locals 2

    .prologue
    .line 1311926
    const/4 v0, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1, p3}, LX/8Ct;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;LX/8Cs;)V

    .line 1311927
    return-void
.end method

.method public constructor <init>(ILjava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1311928
    sget-object v0, LX/8Cs;->START:LX/8Cs;

    invoke-direct {p0, p1, p2, p3, v0}, LX/8Ct;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;LX/8Cs;)V

    .line 1311929
    return-void
.end method

.method private constructor <init>(ILjava/lang/Integer;Ljava/lang/Integer;LX/8Cs;)V
    .locals 0
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1311930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1311931
    iput p1, p0, LX/8Ct;->a:I

    .line 1311932
    iput-object p2, p0, LX/8Ct;->c:Ljava/lang/Integer;

    .line 1311933
    iput-object p3, p0, LX/8Ct;->b:Ljava/lang/Integer;

    .line 1311934
    iput-object p4, p0, LX/8Ct;->d:LX/8Cs;

    .line 1311935
    return-void
.end method
