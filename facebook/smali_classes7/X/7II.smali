.class public LX/7II;
.super LX/7IG;
.source ""


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2q8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1191590
    const-class v0, LX/7II;

    sput-object v0, LX/7II;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2q8;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1191591
    invoke-direct {p0, p2}, LX/7IG;-><init>(Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1191592
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/7II;->d:Ljava/lang/ref/WeakReference;

    .line 1191593
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 1191584
    const/4 v1, -0x1

    .line 1191585
    iget-object v0, p0, LX/7II;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2q8;

    .line 1191586
    if-eqz v0, :cond_0

    .line 1191587
    :try_start_0
    invoke-interface {v0}, LX/2q8;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1191588
    invoke-interface {v0}, LX/2q8;->b()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    move v1, v0

    .line 1191589
    :cond_0
    :goto_1
    return v1

    :catch_0
    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 1191583
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/7IG;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1191582
    const/4 v0, 0x0

    return v0
.end method
