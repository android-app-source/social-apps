.class public LX/8CH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/8CH;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1310655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1310656
    iput-object p1, p0, LX/8CH;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1310657
    return-void
.end method

.method public static a(LX/0QB;)LX/8CH;
    .locals 4

    .prologue
    .line 1310658
    sget-object v0, LX/8CH;->b:LX/8CH;

    if-nez v0, :cond_1

    .line 1310659
    const-class v1, LX/8CH;

    monitor-enter v1

    .line 1310660
    :try_start_0
    sget-object v0, LX/8CH;->b:LX/8CH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1310661
    if-eqz v2, :cond_0

    .line 1310662
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1310663
    new-instance p0, LX/8CH;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/8CH;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1310664
    move-object v0, p0

    .line 1310665
    sput-object v0, LX/8CH;->b:LX/8CH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1310666
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1310667
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1310668
    :cond_1
    sget-object v0, LX/8CH;->b:LX/8CH;

    return-object v0

    .line 1310669
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1310670
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/2Oo;)V
    .locals 3

    .prologue
    .line 1310671
    invoke-virtual {p1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    .line 1310672
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    const/16 v1, 0xbe

    if-ne v0, v1, :cond_0

    .line 1310673
    iget-object v0, p0, LX/8CH;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2X7;->b:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1310674
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 1310675
    instance-of v0, p1, LX/2Oo;

    if-eqz v0, :cond_1

    .line 1310676
    check-cast p1, LX/2Oo;

    invoke-direct {p0, p1}, LX/8CH;->a(LX/2Oo;)V

    .line 1310677
    :cond_0
    :goto_0
    return-void

    .line 1310678
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, LX/2Oo;

    if-eqz v0, :cond_0

    .line 1310679
    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    invoke-direct {p0, v0}, LX/8CH;->a(LX/2Oo;)V

    goto :goto_0
.end method
