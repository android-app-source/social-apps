.class public final LX/7Rf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7RW;


# instance fields
.field public final synthetic a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V
    .locals 0

    .prologue
    .line 1207179
    iput-object p1, p0, LX/7Rf;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;IIIIIILandroid/media/MediaCodec$BufferInfo;)V
    .locals 8

    .prologue
    .line 1207180
    iget-object v0, p0, LX/7Rf;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iget-boolean v0, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    if-eqz v0, :cond_0

    .line 1207181
    iget-object v0, p0, LX/7Rf;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->H:LX/7Ra;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p6

    move v6, p7

    move-object/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, LX/7Ra;->a(Ljava/nio/ByteBuffer;IIIIILandroid/media/MediaCodec$BufferInfo;)V

    .line 1207182
    :cond_0
    iget-object v0, p0, LX/7Rf;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->sendAudioData(Ljava/nio/ByteBuffer;IIIII)V

    .line 1207183
    iget-object v0, p0, LX/7Rf;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    int-to-long v2, p5

    invoke-virtual {v0, v2, v3}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a(J)V

    .line 1207184
    return-void
.end method

.method public final b(Ljava/nio/ByteBuffer;IIIIIILandroid/media/MediaCodec$BufferInfo;)V
    .locals 7

    .prologue
    .line 1207185
    iget-object v0, p0, LX/7Rf;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->sendVideoData(Ljava/nio/ByteBuffer;IIIII)V

    .line 1207186
    return-void
.end method
