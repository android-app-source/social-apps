.class public LX/8Q3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1342403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1342404
    iput-object p1, p0, LX/8Q3;->a:Landroid/content/Context;

    .line 1342405
    return-void
.end method

.method public static a(LX/0QB;)LX/8Q3;
    .locals 1

    .prologue
    .line 1342406
    invoke-static {p0}, LX/8Q3;->b(LX/0QB;)LX/8Q3;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8Q3;
    .locals 3

    .prologue
    .line 1342407
    new-instance v1, LX/8Q3;

    const-class v0, Landroid/content/Context;

    const-class v2, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/8Q3;-><init>(Landroid/content/Context;)V

    .line 1342408
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 1342409
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/8Q3;->a:Landroid/content/Context;

    const-class v2, Lcom/facebook/privacy/edit/EditStoryPrivacyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1342410
    new-instance v1, LX/8QC;

    invoke-direct {v1}, LX/8QC;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    .line 1342411
    iput-object v2, v1, LX/8QC;->c:Ljava/lang/String;

    .line 1342412
    move-object v1, v1

    .line 1342413
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    .line 1342414
    iput-object v2, v1, LX/8QC;->d:Ljava/lang/String;

    .line 1342415
    move-object v1, v1

    .line 1342416
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v2

    .line 1342417
    iput-object v2, v1, LX/8QC;->e:Ljava/lang/String;

    .line 1342418
    move-object v1, v1

    .line 1342419
    const/4 v4, 0x1

    const p0, 0x285feb

    const/4 v5, 0x0

    .line 1342420
    const/4 v2, 0x0

    .line 1342421
    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    if-ne v3, p0, :cond_4

    .line 1342422
    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 1342423
    :goto_0
    invoke-static {p1}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 1342424
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 1342425
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v6, v5

    :goto_1
    if-ge v6, v8, :cond_1

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 1342426
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v9

    if-ne v9, p0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v4

    .line 1342427
    :goto_2
    move v2, v2

    .line 1342428
    iput-boolean v2, v1, LX/8QC;->f:Z

    .line 1342429
    move-object v1, v1

    .line 1342430
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1342431
    iput-object v2, v1, LX/8QC;->a:Ljava/lang/Boolean;

    .line 1342432
    move-object v1, v1

    .line 1342433
    const-string v2, "params"

    invoke-virtual {v1}, LX/8QC;->a()Lcom/facebook/privacy/edit/EditStoryPrivacyParams;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1342434
    return-object v0

    .line 1342435
    :cond_0
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_1

    .line 1342436
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1342437
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v7

    .line 1342438
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v6, v5

    :goto_3
    if-ge v6, v8, :cond_3

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1342439
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v9

    if-ne v9, p0, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v4

    .line 1342440
    goto :goto_2

    .line 1342441
    :cond_2
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_3

    :cond_3
    move v2, v5

    .line 1342442
    goto :goto_2

    :cond_4
    move-object v3, v2

    goto/16 :goto_0
.end method
