.class public final LX/6fr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/messaging/model/threads/NicknamesMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1120572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120573
    new-instance v0, Lcom/facebook/messaging/model/threads/NicknamesMap;

    invoke-direct {v0}, Lcom/facebook/messaging/model/threads/NicknamesMap;-><init>()V

    iput-object v0, p0, LX/6fr;->f:Lcom/facebook/messaging/model/threads/NicknamesMap;

    .line 1120574
    return-void
.end method


# virtual methods
.method public final a(I)LX/6fr;
    .locals 0

    .prologue
    .line 1120570
    iput p1, p0, LX/6fr;->a:I

    .line 1120571
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/NicknamesMap;)LX/6fr;
    .locals 0

    .prologue
    .line 1120553
    iput-object p1, p0, LX/6fr;->f:Lcom/facebook/messaging/model/threads/NicknamesMap;

    .line 1120554
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadCustomization;)LX/6fr;
    .locals 1

    .prologue
    .line 1120563
    iget v0, p1, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    iput v0, p0, LX/6fr;->a:I

    .line 1120564
    iget v0, p1, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    iput v0, p0, LX/6fr;->b:I

    .line 1120565
    iget v0, p1, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    iput v0, p0, LX/6fr;->c:I

    .line 1120566
    iget v0, p1, Lcom/facebook/messaging/model/threads/ThreadCustomization;->e:I

    iput v0, p0, LX/6fr;->d:I

    .line 1120567
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    iput-object v0, p0, LX/6fr;->e:Ljava/lang/String;

    .line 1120568
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    iput-object v0, p0, LX/6fr;->f:Lcom/facebook/messaging/model/threads/NicknamesMap;

    .line 1120569
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/6fr;
    .locals 0

    .prologue
    .line 1120561
    iput-object p1, p0, LX/6fr;->e:Ljava/lang/String;

    .line 1120562
    return-object p0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1120560
    iget v0, p0, LX/6fr;->b:I

    return v0
.end method

.method public final b(I)LX/6fr;
    .locals 0

    .prologue
    .line 1120558
    iput p1, p0, LX/6fr;->b:I

    .line 1120559
    return-object p0
.end method

.method public final c(I)LX/6fr;
    .locals 0

    .prologue
    .line 1120556
    iput p1, p0, LX/6fr;->c:I

    .line 1120557
    return-object p0
.end method

.method public final g()Lcom/facebook/messaging/model/threads/ThreadCustomization;
    .locals 2

    .prologue
    .line 1120555
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/threads/ThreadCustomization;-><init>(LX/6fr;)V

    return-object v0
.end method
