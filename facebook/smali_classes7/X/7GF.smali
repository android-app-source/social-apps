.class public LX/7GF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/7GF;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/6hN;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/6hN;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1189027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189028
    iput-object p1, p0, LX/7GF;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1189029
    iput-object p2, p0, LX/7GF;->b:LX/6hN;

    .line 1189030
    return-void
.end method

.method public static a(LX/0QB;)LX/7GF;
    .locals 5

    .prologue
    .line 1189031
    sget-object v0, LX/7GF;->c:LX/7GF;

    if-nez v0, :cond_1

    .line 1189032
    const-class v1, LX/7GF;

    monitor-enter v1

    .line 1189033
    :try_start_0
    sget-object v0, LX/7GF;->c:LX/7GF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1189034
    if-eqz v2, :cond_0

    .line 1189035
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1189036
    new-instance p0, LX/7GF;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/6hN;->a(LX/0QB;)LX/6hN;

    move-result-object v4

    check-cast v4, LX/6hN;

    invoke-direct {p0, v3, v4}, LX/7GF;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/6hN;)V

    .line 1189037
    move-object v0, p0

    .line 1189038
    sput-object v0, LX/7GF;->c:LX/7GF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1189039
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1189040
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1189041
    :cond_1
    sget-object v0, LX/7GF;->c:LX/7GF;

    return-object v0

    .line 1189042
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1189043
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(I)I
    .locals 3

    .prologue
    .line 1189044
    iget-object v0, p0, LX/7GF;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, LX/7GE;->a(Ljava/lang/Integer;)LX/0Tn;

    move-result-object v1

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method

.method public final b(I)I
    .locals 3

    .prologue
    .line 1189045
    iget-object v0, p0, LX/7GF;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, LX/7GE;->b(Ljava/lang/Integer;)LX/0Tn;

    move-result-object v1

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method

.method public final b()LX/0m9;
    .locals 4

    .prologue
    .line 1189046
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 1189047
    sget-object v1, LX/5dQ;->FULL_SCREEN:LX/5dQ;

    iget-object v1, v1, LX/5dQ;->persistentIndex:Ljava/lang/Integer;

    .line 1189048
    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, LX/7GF;->a(I)I

    move-result v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, LX/7GF;->b(I)I

    move-result v1

    invoke-static {v3, v1}, LX/6hN;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1189049
    sget-object v1, LX/5dQ;->LARGE_PREVIEW:LX/5dQ;

    iget-object v1, v1, LX/5dQ;->persistentIndex:Ljava/lang/Integer;

    .line 1189050
    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, LX/7GF;->a(I)I

    move-result v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, LX/7GF;->b(I)I

    move-result v1

    invoke-static {v3, v1}, LX/6hN;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1189051
    sget-object v1, LX/5dQ;->MEDIUM_PREVIEW:LX/5dQ;

    iget-object v1, v1, LX/5dQ;->persistentIndex:Ljava/lang/Integer;

    .line 1189052
    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, LX/7GF;->a(I)I

    move-result v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, LX/7GF;->b(I)I

    move-result v1

    invoke-static {v3, v1}, LX/6hN;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1189053
    sget-object v1, LX/5dQ;->SMALL_PREVIEW:LX/5dQ;

    iget-object v1, v1, LX/5dQ;->persistentIndex:Ljava/lang/Integer;

    .line 1189054
    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, LX/7GF;->a(I)I

    move-result v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, LX/7GF;->b(I)I

    move-result v1

    invoke-static {v3, v1}, LX/6hN;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1189055
    return-object v0
.end method
