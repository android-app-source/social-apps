.class public LX/6lj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/xma/StyleRenderer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/xma/StyleRenderer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;LX/0Ot;)V
    .locals 4
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/messaging/xma/annotations/FallBackStyleRenderer;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/6li;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/xma/StyleRenderer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1142889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142890
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1142891
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6li;

    .line 1142892
    iget-boolean v3, v0, LX/6li;->d:Z

    if-nez v3, :cond_0

    .line 1142893
    iget-object v3, v0, LX/6li;->a:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    iget-object v0, v0, LX/6li;->b:LX/0Ot;

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1142894
    :cond_1
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/6lj;->a:LX/0P1;

    .line 1142895
    iput-object p2, p0, LX/6lj;->b:LX/0Ot;

    .line 1142896
    return-void
.end method

.method public static a(LX/0QB;)LX/6lj;
    .locals 5

    .prologue
    .line 1142897
    const-class v1, LX/6lj;

    monitor-enter v1

    .line 1142898
    :try_start_0
    sget-object v0, LX/6lj;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1142899
    sput-object v2, LX/6lj;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1142900
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1142901
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1142902
    new-instance v3, LX/6lj;

    invoke-static {v0}, LX/6lc;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v4

    const/16 p0, 0x2976

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/6lj;-><init>(Ljava/util/Set;LX/0Ot;)V

    .line 1142903
    move-object v0, v3

    .line 1142904
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1142905
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6lj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1142906
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1142907
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
