.class public final LX/7f2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/7f4;

.field public final b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1220281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1220282
    new-instance v0, LX/7f4;

    const-string v1, "varying vec2 interp_tc;\nattribute vec4 in_pos;\nattribute vec4 in_tc;\n\nuniform mat4 texMatrix;\n\nvoid main() {\n    gl_Position = in_pos;\n    interp_tc = (texMatrix * in_tc).xy;\n}\n"

    invoke-direct {v0, v1, p1}, LX/7f4;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/7f2;->a:LX/7f4;

    .line 1220283
    iget-object v0, p0, LX/7f2;->a:LX/7f4;

    const-string v1, "texMatrix"

    invoke-virtual {v0, v1}, LX/7f4;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/7f2;->b:I

    .line 1220284
    return-void
.end method
