.class public final LX/6oA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Or",
        "<",
        "LX/6oH;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6oI;

.field public final synthetic b:LX/6nz;

.field public final synthetic c:LX/6oC;


# direct methods
.method public constructor <init>(LX/6oC;LX/6oI;LX/6nz;)V
    .locals 0

    .prologue
    .line 1147920
    iput-object p1, p0, LX/6oA;->c:LX/6oC;

    iput-object p2, p0, LX/6oA;->a:LX/6oI;

    iput-object p3, p0, LX/6oA;->b:LX/6nz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 1147921
    iget-object v0, p0, LX/6oA;->a:LX/6oI;

    iget-object v1, p0, LX/6oA;->b:LX/6nz;

    sget-object v2, LX/6oC;->a:LX/0Tn;

    .line 1147922
    new-instance p0, LX/6ny;

    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v2}, LX/6ny;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V

    .line 1147923
    move-object v1, p0

    .line 1147924
    const-string v2, "fingerprint_nonce_keystore_alias"

    .line 1147925
    new-instance v3, LX/6oH;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    .line 1147926
    invoke-static {}, LX/6nd;->a()Ljava/security/KeyStore;

    move-result-object v5

    move-object v5, v5

    .line 1147927
    check-cast v5, Ljava/security/KeyStore;

    .line 1147928
    invoke-static {}, LX/6nd;->b()Ljava/security/KeyPairGenerator;

    move-result-object v6

    move-object v6, v6

    .line 1147929
    check-cast v6, Ljava/security/KeyPairGenerator;

    .line 1147930
    invoke-static {}, LX/6nd;->c()Ljava/security/KeyFactory;

    move-result-object v7

    move-object v7, v7

    .line 1147931
    check-cast v7, Ljava/security/KeyFactory;

    .line 1147932
    new-instance v9, LX/6o4;

    invoke-static {v0}, LX/6oL;->a(LX/0QB;)LX/6oL;

    move-result-object v8

    check-cast v8, LX/6oL;

    invoke-direct {v9, v8}, LX/6o4;-><init>(LX/6oL;)V

    .line 1147933
    move-object v8, v9

    .line 1147934
    check-cast v8, LX/6o4;

    const/16 v9, 0x1652

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    move-object v10, v1

    move-object v11, v2

    invoke-direct/range {v3 .. v11}, LX/6oH;-><init>(Landroid/content/Context;Ljava/security/KeyStore;Ljava/security/KeyPairGenerator;Ljava/security/KeyFactory;LX/6o4;LX/0Or;LX/6ny;Ljava/lang/String;)V

    .line 1147935
    move-object v0, v3

    .line 1147936
    return-object v0
.end method
