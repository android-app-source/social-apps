.class public LX/8Fi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Fh;


# instance fields
.field private final a:LX/7S6;

.field private final b:Lcom/facebook/videocodec/effects/model/ColorFilter;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1318154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318155
    new-instance v0, LX/7S6;

    invoke-direct {v0}, LX/7S6;-><init>()V

    iput-object v0, p0, LX/8Fi;->a:LX/7S6;

    .line 1318156
    new-instance v0, Lcom/facebook/videocodec/effects/model/ColorFilter;

    invoke-direct {v0}, Lcom/facebook/videocodec/effects/model/ColorFilter;-><init>()V

    iput-object v0, p0, LX/8Fi;->b:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1318157
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 1318158
    iget-object v0, p0, LX/8Fi;->a:LX/7S6;

    invoke-virtual {v0, p1, p2}, LX/7S6;->a(II)V

    .line 1318159
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 1

    .prologue
    .line 1318160
    iget-object v0, p0, LX/8Fi;->a:LX/7S6;

    invoke-virtual {v0, p1}, LX/7S6;->a(LX/5Pc;)V

    .line 1318161
    return-void
.end method

.method public final a([F[F[FJ)V
    .locals 6

    .prologue
    .line 1318162
    iget-object v0, p0, LX/8Fi;->a:LX/7S6;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, LX/7S6;->a([F[F[FJ)V

    .line 1318163
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1318164
    iget-object v0, p0, LX/8Fi;->a:LX/7S6;

    invoke-virtual {v0}, LX/7S6;->b()V

    .line 1318165
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1318166
    iget-object v0, p0, LX/8Fi;->a:LX/7S6;

    invoke-virtual {v0}, LX/7S6;->c()Z

    move-result v0

    return v0
.end method
