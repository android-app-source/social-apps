.class public final enum LX/7Jc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Jc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Jc;

.field public static final enum DEFAULT:LX/7Jc;

.field public static final enum EPHEMERAL:LX/7Jc;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1193950
    new-instance v0, LX/7Jc;

    const-string v1, "DEFAULT"

    const-string v2, "default"

    invoke-direct {v0, v1, v3, v2}, LX/7Jc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jc;->DEFAULT:LX/7Jc;

    .line 1193951
    new-instance v0, LX/7Jc;

    const-string v1, "EPHEMERAL"

    const-string v2, "ephemeral"

    invoke-direct {v0, v1, v4, v2}, LX/7Jc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jc;->EPHEMERAL:LX/7Jc;

    .line 1193952
    const/4 v0, 0x2

    new-array v0, v0, [LX/7Jc;

    sget-object v1, LX/7Jc;->DEFAULT:LX/7Jc;

    aput-object v1, v0, v3

    sget-object v1, LX/7Jc;->EPHEMERAL:LX/7Jc;

    aput-object v1, v0, v4

    sput-object v0, LX/7Jc;->$VALUES:[LX/7Jc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1193953
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1193954
    iput-object p3, p0, LX/7Jc;->value:Ljava/lang/String;

    .line 1193955
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Jc;
    .locals 1

    .prologue
    .line 1193956
    const-class v0, LX/7Jc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Jc;

    return-object v0
.end method

.method public static values()[LX/7Jc;
    .locals 1

    .prologue
    .line 1193957
    sget-object v0, LX/7Jc;->$VALUES:[LX/7Jc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Jc;

    return-object v0
.end method
