.class public final LX/8CI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1310680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/8CI;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v0, p0, LX/8CI;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x74a56dd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1310681
    invoke-static {p0, p1}, LX/8CI;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1310682
    iget-object v1, p0, LX/8CI;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1310683
    const/16 v1, 0x27

    const v2, 0x4230f7e6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1310684
    :goto_0
    return-void

    .line 1310685
    :cond_0
    iget-object v1, p0, LX/8CI;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2X7;->b:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1310686
    const v1, 0x1c1143b8    # 4.8064E-22f

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
