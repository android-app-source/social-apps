.class public LX/6mC;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/6lq;


# instance fields
.field public a:LX/6mF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/6ll;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1143396
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1143397
    invoke-direct {p0}, LX/6mC;->a()V

    .line 1143398
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1143399
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1143400
    invoke-direct {p0}, LX/6mC;->a()V

    .line 1143401
    return-void
.end method

.method private a()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1143392
    const-class v0, LX/6mC;

    invoke-static {v0, p0}, LX/6mC;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1143393
    iget-object v0, p0, LX/6mC;->a:LX/6mF;

    new-instance v1, LX/6mB;

    invoke-direct {v1, p0}, LX/6mB;-><init>(LX/6mC;)V

    .line 1143394
    iput-object v1, v0, LX/6mF;->b:LX/6m1;

    .line 1143395
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/6mC;

    invoke-static {p0}, LX/6mF;->b(LX/0QB;)LX/6mF;

    move-result-object p0

    check-cast p0, LX/6mF;

    iput-object p0, p1, LX/6mC;->a:LX/6mF;

    return-void
.end method


# virtual methods
.method public a(LX/6ll;)V
    .locals 0
    .param p1    # LX/6ll;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1143402
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1143386
    iget-object v0, p0, LX/6mC;->a:LX/6mF;

    invoke-virtual {v0, p1}, LX/6mF;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x541754c9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1143387
    iget-object v1, p0, LX/6mC;->a:LX/6mF;

    invoke-virtual {v1, p1}, LX/6mF;->b(Landroid/view/MotionEvent;)V

    .line 1143388
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, -0x24c458b3

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setXMACallback(LX/6ll;)V
    .locals 0
    .param p1    # LX/6ll;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1143389
    iput-object p1, p0, LX/6mC;->b:LX/6ll;

    .line 1143390
    invoke-virtual {p0, p1}, LX/6mC;->a(LX/6ll;)V

    .line 1143391
    return-void
.end method
