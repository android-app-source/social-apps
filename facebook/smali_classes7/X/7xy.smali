.class public LX/7xy;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/62U;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7ws;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/view/View$OnClickListener;

.field public final d:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

.field private final e:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

.field private final f:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

.field private final g:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;


# direct methods
.method public constructor <init>(Landroid/view/View$OnClickListener;Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V
    .locals 7

    .prologue
    .line 1278566
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1278567
    iput-object p1, p0, LX/7xy;->c:Landroid/view/View$OnClickListener;

    .line 1278568
    iput-object p2, p0, LX/7xy;->d:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1278569
    iput-object p3, p0, LX/7xy;->e:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1278570
    iget-object v0, p4, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->y:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, LX/7xy;->f:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1278571
    iget-object v0, p4, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->u:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    iput-object v0, p0, LX/7xy;->g:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    .line 1278572
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1278573
    sget-object v1, LX/7wO;->REGISTRATION_HEADER:LX/7wO;

    invoke-static {v1}, LX/7ws;->a(LX/7wO;)LX/7ws;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1278574
    sget-object v1, LX/7xI;->a:[I

    iget-object v2, p4, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->u:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1278575
    const/4 v1, 0x0

    invoke-static {v1, p4}, LX/7xJ;->a(ILcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1278576
    :goto_0
    sget-object v1, LX/7wO;->REGISTRATION_FOOTER:LX/7wO;

    invoke-static {v1}, LX/7ws;->a(LX/7wO;)LX/7ws;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1278577
    move-object v0, v0

    .line 1278578
    iput-object v0, p0, LX/7xy;->a:Ljava/util/List;

    .line 1278579
    const/4 v2, 0x0

    .line 1278580
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1278581
    iget-object v5, p4, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    move v1, v2

    .line 1278582
    :goto_2
    iget p1, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    if-ge v1, p1, :cond_0

    .line 1278583
    iget-object p1, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->c:Ljava/lang/String;

    invoke-virtual {v4, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1278584
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1278585
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1278586
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1278587
    iput-object v0, p0, LX/7xy;->b:LX/0Px;

    .line 1278588
    return-void

    .line 1278589
    :pswitch_0
    const/4 v3, 0x0

    .line 1278590
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1278591
    iget-object p1, p4, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p2

    move v5, v3

    move v2, v3

    :goto_3
    if-ge v5, p2, :cond_3

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    move v4, v2

    move v2, v3

    .line 1278592
    :goto_4
    iget p3, v1, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    if-ge v2, p3, :cond_2

    .line 1278593
    invoke-static {v4, p4}, LX/7xJ;->a(ILcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)Ljava/util/List;

    move-result-object p3

    invoke-interface {v6, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1278594
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1278595
    :cond_2
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v2, v4

    goto :goto_3

    .line 1278596
    :cond_3
    move-object v1, v6

    .line 1278597
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1278298
    const/4 v2, 0x0

    .line 1278299
    sget-object v0, LX/7xx;->a:[I

    invoke-static {}, LX/7wO;->values()[LX/7wO;

    move-result-object v1

    aget-object v1, v1, p2

    invoke-virtual {v1}, LX/7wO;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1278300
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1278301
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030539

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1278302
    new-instance v0, LX/62U;

    invoke-direct {v0, v1}, LX/62U;-><init>(Landroid/view/View;)V

    .line 1278303
    :goto_0
    return-object v0

    .line 1278304
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030532

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1278305
    new-instance v0, LX/7y1;

    invoke-direct {v0, v1}, LX/7y1;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1278306
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03051d

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1278307
    new-instance v0, LX/7xo;

    invoke-direct {v0, v1}, LX/7xo;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1278308
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03051b

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1278309
    new-instance v0, LX/7xn;

    invoke-direct {v0, v1}, LX/7xn;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1278310
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03053a

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1278311
    new-instance v0, LX/7yH;

    invoke-direct {v0, v1}, LX/7yH;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1278312
    :pswitch_5
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030536

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1278313
    new-instance v0, LX/7y9;

    invoke-direct {v0, v1}, LX/7y9;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1278314
    :pswitch_6
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030538

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1278315
    new-instance v0, LX/7yF;

    invoke-direct {v0, v1}, LX/7yF;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1278316
    :pswitch_7
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030533

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1278317
    new-instance v0, LX/7y3;

    invoke-direct {v0, v1}, LX/7y3;-><init>(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1278318
    :pswitch_8
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030537

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1278319
    new-instance v0, LX/7yB;

    invoke-direct {v0, v1}, LX/7yB;-><init>(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1278320
    :pswitch_9
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030534

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1278321
    new-instance v0, LX/7y6;

    invoke-direct {v0, v1}, LX/7y6;-><init>(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1278322
    :pswitch_a
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030535

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1278323
    new-instance v0, LX/7y8;

    invoke-direct {v0, v1}, LX/7y8;-><init>(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1278324
    :pswitch_b
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03053b

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1278325
    new-instance v0, LX/62U;

    invoke-direct {v0, v1}, LX/62U;-><init>(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1278326
    :pswitch_c
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03052f

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1278327
    new-instance v0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;

    invoke-direct {v0, v1}, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;-><init>(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1278328
    :pswitch_d
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030530

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1278329
    new-instance v0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;

    invoke-direct {v0, v1}, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;-><init>(Landroid/view/View;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 8

    .prologue
    .line 1278330
    check-cast p1, LX/62U;

    .line 1278331
    sget-object v0, LX/7xx;->a:[I

    invoke-static {}, LX/7wO;->values()[LX/7wO;

    move-result-object v1

    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v1}, LX/7wO;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1278332
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Binding illegal view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1278333
    :pswitch_0
    check-cast p1, LX/7y1;

    iget-object v0, p0, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ws;

    .line 1278334
    iget-object v1, v0, LX/7ws;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1278335
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1278336
    iget-object v1, v0, LX/7ws;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1278337
    const-string v2, "empty"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1278338
    :cond_0
    iget-object v1, p1, LX/7y1;->m:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p1, LX/7y1;->n:Landroid/content/res/Resources;

    const v3, 0x7f081fc9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1278339
    :goto_0
    :pswitch_1
    return-void

    .line 1278340
    :pswitch_2
    check-cast p1, LX/7xn;

    iget-object v0, p0, LX/7xy;->c:Landroid/view/View$OnClickListener;

    const/4 v1, 0x1

    iget-object v2, p0, LX/7xy;->f:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1278341
    iget-object v3, p1, LX/7xn;->n:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v3, v0}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1278342
    iget-object v3, p1, LX/7xn;->n:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v3, v1}, Lcom/facebook/widget/text/BetterButton;->setEnabled(Z)V

    .line 1278343
    if-nez v2, :cond_13

    .line 1278344
    iget-object v3, p1, LX/7xn;->o:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 1278345
    :goto_1
    goto :goto_0

    .line 1278346
    :pswitch_3
    iget-object v0, p0, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ws;

    .line 1278347
    iget v1, v0, LX/7ws;->a:I

    move v1, v1

    .line 1278348
    check-cast p1, LX/7yH;

    iget-object v2, p0, LX/7xy;->g:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    iget-object v0, p0, LX/7xy;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1278349
    sget-object v3, LX/7yG;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 1278350
    iget-object v3, p1, LX/7yH;->m:Lcom/facebook/widget/text/BetterTextView;

    iget-object v4, p1, LX/7yH;->n:Landroid/content/res/Resources;

    const v5, 0x7f081fcc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1278351
    :goto_2
    goto :goto_0

    .line 1278352
    :pswitch_4
    check-cast p1, LX/7y9;

    iget-object v0, p0, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ws;

    .line 1278353
    iget-object v1, v0, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v1, v1

    .line 1278354
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1278355
    :cond_1
    iget-object v1, p1, LX/7y9;->m:Lcom/facebook/drawee/span/DraweeSpanTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1278356
    :goto_3
    goto :goto_0

    .line 1278357
    :pswitch_5
    check-cast p1, LX/7yF;

    iget-object v1, p0, LX/7xy;->e:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-object v0, p0, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ws;

    iget-object v2, p0, LX/7xy;->d:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1278358
    iput-object v0, p1, LX/7yF;->o:LX/7ws;

    .line 1278359
    iput-object v2, p1, LX/7yF;->p:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1278360
    iput-object v1, p1, LX/7yF;->q:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1278361
    iget-object v3, p1, LX/7yF;->m:Landroid/support/design/widget/TextInputLayout;

    iget-object v4, p1, LX/7yF;->t:LX/7xH;

    const/4 v6, 0x1

    const/4 p0, 0x0

    .line 1278362
    iget-object v5, v0, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v1, v5

    .line 1278363
    iget-object v5, v0, LX/7ws;->d:Ljava/lang/String;

    move-object v5, v5

    .line 1278364
    if-nez v5, :cond_16

    .line 1278365
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->l()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    .line 1278366
    if-eqz v5, :cond_15

    move v5, v6

    :goto_4
    if-eqz v5, :cond_18

    .line 1278367
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->l()LX/1vs;

    move-result-object v5

    iget-object v2, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 1278368
    invoke-virtual {v2, v5, p0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_17

    move v5, v6

    :goto_5
    if-eqz v5, :cond_19

    .line 1278369
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->l()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    invoke-virtual {v6, v5, p0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 1278370
    :goto_6
    move-object v4, v5

    .line 1278371
    invoke-virtual {v3, v4}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1278372
    iget-object v3, p1, LX/7yF;->n:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v4, p1, LX/7yF;->r:Landroid/text/TextWatcher;

    invoke-virtual {v3, v4}, Lcom/facebook/fig/textinput/FigEditText;->setTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1278373
    iget-object v3, p1, LX/7yF;->n:Lcom/facebook/fig/textinput/FigEditText;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/fig/textinput/FigEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1278374
    iget-object v3, p1, LX/7yF;->n:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v4, p1, LX/7yF;->p:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-object v5, p1, LX/7yF;->o:LX/7ws;

    iget-object v6, p1, LX/7yF;->o:LX/7ws;

    .line 1278375
    iget-object p0, v6, LX/7ws;->d:Ljava/lang/String;

    move-object v6, p0

    .line 1278376
    iget p0, v5, LX/7ws;->a:I

    move p0, p0

    .line 1278377
    iget-object v1, v5, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v1, v1

    .line 1278378
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->fa_()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v1

    .line 1278379
    iget-object v2, v5, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v2, v2

    .line 1278380
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->eZ_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, p0, v1, v2}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a(ILcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;Ljava/lang/String;)Lcom/facebook/events/tickets/modal/model/FieldItem;

    move-result-object p0

    .line 1278381
    if-nez p0, :cond_1b

    .line 1278382
    const/4 p0, 0x0

    .line 1278383
    :goto_7
    move-object v4, p0

    .line 1278384
    invoke-virtual {v3, v4}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1278385
    iget-object v3, p1, LX/7yF;->n:Lcom/facebook/fig/textinput/FigEditText;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/facebook/fig/textinput/FigEditText;->setInputType(I)V

    .line 1278386
    iget-object v3, v0, LX/7ws;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1278387
    if-nez v3, :cond_1d

    .line 1278388
    :cond_2
    :goto_8
    goto/16 :goto_0

    .line 1278389
    :pswitch_6
    check-cast p1, LX/7y3;

    iget-object v0, p0, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ws;

    iget-object v1, p0, LX/7xy;->d:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1278390
    iput-object v0, p1, LX/7y3;->n:LX/7ws;

    .line 1278391
    iput-object v1, p1, LX/7y3;->o:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1278392
    iget-object v2, p1, LX/7y3;->m:Lcom/facebook/resources/ui/FbCheckBox;

    iget-object v3, p1, LX/7y3;->p:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbCheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1278393
    iget-object v2, v0, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v2, v2

    .line 1278394
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1278395
    iget-object v2, p1, LX/7y3;->m:Lcom/facebook/resources/ui/FbCheckBox;

    .line 1278396
    iget-object v3, v0, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v3, v3

    .line 1278397
    invoke-static {v3}, LX/7xH;->a(LX/7om;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbCheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 1278398
    :cond_3
    iget-object v2, p1, LX/7y3;->m:Lcom/facebook/resources/ui/FbCheckBox;

    iget-object v3, p1, LX/7y3;->o:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-object v4, p1, LX/7y3;->n:LX/7ws;

    .line 1278399
    iget p1, v4, LX/7ws;->a:I

    move p1, p1

    .line 1278400
    iget-object v0, v4, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v0, v0

    .line 1278401
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->fa_()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v0

    .line 1278402
    iget-object v1, v4, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v1, v1

    .line 1278403
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->eZ_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, p1, v0, v1}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a(ILcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;Ljava/lang/String;)Lcom/facebook/events/tickets/modal/model/FieldItem;

    move-result-object p1

    .line 1278404
    if-nez p1, :cond_1f

    const/4 p1, 0x0

    :goto_9
    move v3, p1

    .line 1278405
    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    .line 1278406
    goto/16 :goto_0

    .line 1278407
    :pswitch_7
    check-cast p1, LX/7yB;

    iget-object v0, p0, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ws;

    iget-object v1, p0, LX/7xy;->d:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1278408
    iput-object v0, p1, LX/7yB;->o:LX/7ws;

    .line 1278409
    iput-object v1, p1, LX/7yB;->p:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1278410
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p1, LX/7yB;->q:Ljava/util/Set;

    .line 1278411
    iget-object v2, v0, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v2, v2

    .line 1278412
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->j()LX/0Px;

    move-result-object v5

    .line 1278413
    const/4 p0, 0x0

    .line 1278414
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v4

    .line 1278415
    iget-object v2, p1, LX/7yB;->m:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v2}, Lcom/facebook/widget/CustomLinearLayout;->getChildCount()I

    move-result p2

    move v6, p0

    .line 1278416
    :goto_a
    if-ge v6, v4, :cond_6

    .line 1278417
    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7on;

    .line 1278418
    if-ge v6, p2, :cond_5

    iget-object v3, p1, LX/7yB;->m:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v3, v6}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbCheckBox;

    .line 1278419
    :goto_b
    invoke-interface {v2}, LX/7on;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbCheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 1278420
    invoke-virtual {v3, v6}, Lcom/facebook/resources/ui/FbCheckBox;->setId(I)V

    .line 1278421
    invoke-virtual {v3, p0}, Lcom/facebook/resources/ui/FbCheckBox;->setVisibility(I)V

    .line 1278422
    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7on;

    invoke-interface {v2}, LX/7on;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p1, LX/7yB;->o:LX/7ws;

    .line 1278423
    iget-object v1, v0, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v0, v1

    .line 1278424
    invoke-interface {v0}, LX/7ok;->n()I

    move-result v1

    if-nez v1, :cond_21

    .line 1278425
    invoke-interface {v0}, LX/7ok;->j()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_20

    const/4 v1, 0x0

    .line 1278426
    :goto_c
    move v0, v1

    .line 1278427
    new-instance v1, LX/7yA;

    invoke-direct {v1, p1, v2, v0}, LX/7yA;-><init>(LX/7yB;Ljava/lang/String;I)V

    move-object v2, v1

    .line 1278428
    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbCheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1278429
    if-lt v6, p2, :cond_4

    .line 1278430
    iget-object v2, p1, LX/7yB;->m:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    .line 1278431
    :cond_4
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_a

    .line 1278432
    :cond_5
    new-instance v3, Lcom/facebook/resources/ui/FbCheckBox;

    iget-object v0, p1, LX/7yB;->n:Landroid/content/Context;

    invoke-direct {v3, v0}, Lcom/facebook/resources/ui/FbCheckBox;-><init>(Landroid/content/Context;)V

    goto :goto_b

    :cond_6
    move v2, v4

    .line 1278433
    :goto_d
    if-ge v2, p2, :cond_7

    .line 1278434
    iget-object v3, p1, LX/7yB;->m:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v3, v2}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1278435
    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    .line 1278436
    :cond_7
    iget-object v2, p1, LX/7yB;->p:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-object v3, p1, LX/7yB;->o:LX/7ws;

    .line 1278437
    iget v4, v3, LX/7ws;->a:I

    move v4, v4

    .line 1278438
    iget-object v6, v3, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v6, v6

    .line 1278439
    invoke-virtual {v6}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->fa_()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v6

    .line 1278440
    iget-object p0, v3, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object p0, p0

    .line 1278441
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->eZ_()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v4, v6, p0}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a(ILcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;Ljava/lang/String;)Lcom/facebook/events/tickets/modal/model/FieldItem;

    move-result-object v4

    .line 1278442
    if-nez v4, :cond_22

    .line 1278443
    sget-object v4, LX/0Re;->a:LX/0Re;

    move-object v4, v4

    .line 1278444
    :goto_e
    move-object v2, v4

    .line 1278445
    if-eqz v2, :cond_8

    .line 1278446
    iget-object v3, p1, LX/7yB;->q:Ljava/util/Set;

    invoke-virtual {v2}, LX/0Py;->asList()LX/0Px;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1278447
    :cond_8
    iget-object v2, p1, LX/7yB;->m:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v2}, Lcom/facebook/widget/CustomLinearLayout;->getChildCount()I

    move-result v6

    .line 1278448
    const/4 v2, 0x0

    move v4, v2

    :goto_f
    if-ge v4, v6, :cond_9

    .line 1278449
    iget-object v2, p1, LX/7yB;->m:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v2, v4}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbCheckBox;

    .line 1278450
    iget-object p0, p1, LX/7yB;->q:Ljava/util/Set;

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7on;

    invoke-interface {v3}, LX/7on;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    .line 1278451
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_f

    .line 1278452
    :cond_9
    goto/16 :goto_0

    .line 1278453
    :pswitch_8
    check-cast p1, LX/7y6;

    iget-object v0, p0, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ws;

    iget-object v1, p0, LX/7xy;->d:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1278454
    iput-object v0, p1, LX/7y6;->o:LX/7ws;

    .line 1278455
    iput-object v1, p1, LX/7y6;->p:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1278456
    iget-object v2, v0, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v2, v2

    .line 1278457
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->j()LX/0Px;

    move-result-object v4

    .line 1278458
    iget-object v2, p1, LX/7y6;->q:LX/34b;

    iget-object v3, p1, LX/7y6;->o:LX/7ws;

    .line 1278459
    iget-object v5, v3, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v3, v5

    .line 1278460
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/34b;->a(Ljava/lang/String;)V

    .line 1278461
    iget-object v2, p1, LX/7y6;->q:LX/34b;

    invoke-virtual {v2}, LX/34c;->clear()V

    .line 1278462
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_10
    if-ge v3, v5, :cond_a

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7on;

    .line 1278463
    iget-object v6, p1, LX/7y6;->q:LX/34b;

    invoke-interface {v2}, LX/7on;->b()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, p0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v6

    .line 1278464
    new-instance p0, LX/7y5;

    invoke-direct {p0, p1, v2}, LX/7y5;-><init>(LX/7y6;LX/7on;)V

    move-object v2, p0

    .line 1278465
    invoke-virtual {v6, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1278466
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_10

    .line 1278467
    :cond_a
    iget-object v2, p1, LX/7y6;->q:LX/34b;

    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 1278468
    iget-object v2, p1, LX/7y6;->m:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v3, p1, LX/7y6;->p:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-object v4, p1, LX/7y6;->o:LX/7ws;

    invoke-static {v3, v4}, LX/7xD;->b(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;LX/7ws;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1278469
    goto/16 :goto_0

    .line 1278470
    :pswitch_9
    check-cast p1, LX/7y8;

    iget-object v0, p0, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ws;

    iget-object v1, p0, LX/7xy;->d:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1278471
    iput-object v0, p1, LX/7y8;->o:LX/7ws;

    .line 1278472
    iput-object v1, p1, LX/7y8;->p:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1278473
    iget-object v2, v0, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v2, v2

    .line 1278474
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->j()LX/0Px;

    move-result-object v2

    iput-object v2, p1, LX/7y8;->q:LX/0Px;

    .line 1278475
    iget-object v2, p1, LX/7y8;->m:Landroid/widget/RadioGroup;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1278476
    iget-object v2, p1, LX/7y8;->m:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->clearCheck()V

    .line 1278477
    iget-object v2, p1, LX/7y8;->q:LX/0Px;

    const/4 v7, 0x0

    .line 1278478
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    .line 1278479
    iget-object v3, p1, LX/7y8;->m:Landroid/widget/RadioGroup;

    invoke-virtual {v3}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result p0

    .line 1278480
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p1, LX/7y8;->r:Ljava/util/Map;

    move v6, v7

    .line 1278481
    :goto_11
    if-ge v6, v5, :cond_d

    .line 1278482
    if-ge v6, p0, :cond_c

    iget-object v3, p1, LX/7y8;->m:Landroid/widget/RadioGroup;

    invoke-virtual {v3, v6}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbRadioButton;

    move-object v4, v3

    .line 1278483
    :goto_12
    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7on;

    invoke-interface {v3}, LX/7on;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/facebook/resources/ui/FbRadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 1278484
    invoke-virtual {v4, v6}, Lcom/facebook/resources/ui/FbRadioButton;->setId(I)V

    .line 1278485
    invoke-virtual {v4, v7}, Lcom/facebook/resources/ui/FbRadioButton;->setVisibility(I)V

    .line 1278486
    if-lt v6, p0, :cond_b

    .line 1278487
    iget-object v3, p1, LX/7y8;->m:Landroid/widget/RadioGroup;

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 1278488
    :cond_b
    iget-object v4, p1, LX/7y8;->r:Ljava/util/Map;

    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7on;

    invoke-interface {v3}, LX/7on;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v4, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1278489
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_11

    .line 1278490
    :cond_c
    new-instance v3, Lcom/facebook/resources/ui/FbRadioButton;

    iget-object v4, p1, LX/7y8;->n:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/facebook/resources/ui/FbRadioButton;-><init>(Landroid/content/Context;)V

    move-object v4, v3

    goto :goto_12

    :cond_d
    move v3, v5

    .line 1278491
    :goto_13
    if-ge v3, p0, :cond_e

    .line 1278492
    iget-object v4, p1, LX/7y8;->m:Landroid/widget/RadioGroup;

    invoke-virtual {v4, v3}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1278493
    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    .line 1278494
    :cond_e
    iget-object v2, p1, LX/7y8;->m:Landroid/widget/RadioGroup;

    iget-object v3, p1, LX/7y8;->s:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1278495
    invoke-static {v1, v0}, LX/7xD;->b(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;LX/7ws;)Ljava/lang/String;

    move-result-object v2

    .line 1278496
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 1278497
    iget-object v3, p1, LX/7y8;->m:Landroid/widget/RadioGroup;

    iget-object v4, p1, LX/7y8;->r:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 1278498
    :cond_f
    goto/16 :goto_0

    .line 1278499
    :pswitch_a
    check-cast p1, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;

    iget-object v0, p0, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ws;

    .line 1278500
    iget-object v1, v0, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v0, v1

    .line 1278501
    const/4 v5, 0x0

    .line 1278502
    invoke-interface {v0}, LX/7oo;->c()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_23

    const/4 v1, 0x1

    :goto_14
    if-eqz v1, :cond_24

    .line 1278503
    iget-object v1, p1, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1278504
    :goto_15
    goto/16 :goto_0

    .line 1278505
    :pswitch_b
    check-cast p1, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;

    iget-object v0, p0, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ws;

    .line 1278506
    iget-object v1, v0, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v0, v1

    .line 1278507
    invoke-virtual {p1, v0}, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;)V

    goto/16 :goto_0

    .line 1278508
    :cond_10
    iget-object v1, p1, LX/7y1;->m:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p1, LX/7y1;->n:Landroid/content/res/Resources;

    const v3, 0x7f081fca

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p1, LX/7y1;->o:LX/7xH;

    .line 1278509
    iget-object p0, v0, LX/7ws;->d:Ljava/lang/String;

    move-object p0, p0

    .line 1278510
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_11

    .line 1278511
    const-string p0, ""

    .line 1278512
    :goto_16
    move-object v6, p0

    .line 1278513
    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1278514
    :cond_11
    iget-object p0, v0, LX/7ws;->d:Ljava/lang/String;

    move-object p2, p0

    .line 1278515
    const/4 p0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    :cond_12
    :goto_17
    packed-switch p0, :pswitch_data_2

    .line 1278516
    const-string p0, ""

    goto :goto_16

    .line 1278517
    :sswitch_0
    const-string p1, "email_format"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_12

    const/4 p0, 0x0

    goto :goto_17

    :sswitch_1
    const-string p1, "phone_format"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_12

    const/4 p0, 0x1

    goto :goto_17

    .line 1278518
    :pswitch_c
    iget-object p0, v6, LX/7xH;->a:Landroid/content/res/Resources;

    const p2, 0x7f081fd5

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_16

    .line 1278519
    :pswitch_d
    iget-object p0, v6, LX/7xH;->a:Landroid/content/res/Resources;

    const p2, 0x7f081fd4

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_16

    .line 1278520
    :cond_13
    iget-object v3, p1, LX/7xn;->o:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    new-instance v4, LX/7xm;

    invoke-direct {v4, p1}, LX/7xm;-><init>(LX/7xn;)V

    invoke-virtual {v3, v2, v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(LX/3Ab;LX/7wC;)V

    .line 1278521
    iget-object v3, p1, LX/7xn;->o:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto/16 :goto_1

    .line 1278522
    :pswitch_e
    iget-object v3, p1, LX/7yH;->m:Lcom/facebook/widget/text/BetterTextView;

    iget-object v4, p1, LX/7yH;->n:Landroid/content/res/Resources;

    const v5, 0x7f081fcb

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 p0, 0x0

    add-int/lit8 p2, v1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v6, p0

    const/4 p0, 0x1

    aput-object v0, v6, p0

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1278523
    :cond_14
    iget-object v2, p1, LX/7y9;->m:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-static {v1}, LX/7xH;->a(LX/7om;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1278524
    iget-object v1, p1, LX/7y9;->m:Lcom/facebook/drawee/span/DraweeSpanTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_15
    move v5, p0

    .line 1278525
    goto/16 :goto_4

    :cond_16
    move v5, p0

    goto/16 :goto_4

    :cond_17
    move v5, p0

    goto/16 :goto_5

    :cond_18
    move v5, p0

    goto/16 :goto_5

    .line 1278526
    :cond_19
    iget-object v5, v0, LX/7ws;->d:Ljava/lang/String;

    move-object v1, v5

    .line 1278527
    const/4 v5, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_1

    :cond_1a
    move p0, v5

    :goto_18
    packed-switch p0, :pswitch_data_3

    .line 1278528
    const/4 v5, 0x0

    goto/16 :goto_6

    .line 1278529
    :sswitch_2
    const-string v6, "address1"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    goto :goto_18

    :sswitch_3
    const-string p0, "address2"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1a

    move p0, v6

    goto :goto_18

    :sswitch_4
    const-string v6, "city"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    const/4 p0, 0x2

    goto :goto_18

    :sswitch_5
    const-string v6, "state"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    const/4 p0, 0x3

    goto :goto_18

    :sswitch_6
    const-string v6, "zipcode"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    const/4 p0, 0x4

    goto :goto_18

    :sswitch_7
    const-string v6, "first_name"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    const/4 p0, 0x5

    goto :goto_18

    :sswitch_8
    const-string v6, "last_name"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    const/4 p0, 0x6

    goto :goto_18

    :sswitch_9
    const-string v6, "email"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    const/4 p0, 0x7

    goto :goto_18

    :sswitch_a
    const-string v6, "phone"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    const/16 p0, 0x8

    goto :goto_18

    .line 1278530
    :pswitch_f
    iget-object v5, v4, LX/7xH;->a:Landroid/content/res/Resources;

    const v6, 0x7f081fcd

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_6

    .line 1278531
    :pswitch_10
    iget-object v5, v4, LX/7xH;->a:Landroid/content/res/Resources;

    const v6, 0x7f081fce

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_6

    .line 1278532
    :pswitch_11
    iget-object v5, v4, LX/7xH;->a:Landroid/content/res/Resources;

    const v6, 0x7f081fcf

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_6

    .line 1278533
    :pswitch_12
    iget-object v5, v4, LX/7xH;->a:Landroid/content/res/Resources;

    const v6, 0x7f081fd0

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_6

    .line 1278534
    :pswitch_13
    iget-object v5, v4, LX/7xH;->a:Landroid/content/res/Resources;

    const v6, 0x7f081fd1

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_6

    .line 1278535
    :pswitch_14
    iget-object v5, v4, LX/7xH;->a:Landroid/content/res/Resources;

    const v6, 0x7f081fd2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_6

    .line 1278536
    :pswitch_15
    iget-object v5, v4, LX/7xH;->a:Landroid/content/res/Resources;

    const v6, 0x7f081fd3

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_6

    .line 1278537
    :pswitch_16
    iget-object v5, v4, LX/7xH;->a:Landroid/content/res/Resources;

    const v6, 0x7f081fd5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_6

    .line 1278538
    :pswitch_17
    iget-object v5, v4, LX/7xH;->a:Landroid/content/res/Resources;

    const v6, 0x7f081fd4

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_6

    :cond_1b
    if-nez v6, :cond_1c

    .line 1278539
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->b:Ljava/lang/String;

    move-object p0, v1

    .line 1278540
    goto/16 :goto_7

    :cond_1c
    invoke-virtual {p0, v6}, Lcom/facebook/events/tickets/modal/model/FieldItem;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_7

    .line 1278541
    :cond_1d
    sget-object v3, LX/7yE;->a:[I

    .line 1278542
    iget-object v4, v0, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v4, v4

    .line 1278543
    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->fa_()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_4

    goto/16 :goto_8

    .line 1278544
    :pswitch_18
    iget-object v3, v0, LX/7ws;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1278545
    const-string v4, "zipcode"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1278546
    iget-object v3, p1, LX/7yF;->n:Lcom/facebook/fig/textinput/FigEditText;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/facebook/fig/textinput/FigEditText;->setInputType(I)V

    goto/16 :goto_8

    .line 1278547
    :pswitch_19
    iget-object v3, v0, LX/7ws;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1278548
    const-string v4, "email"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 1278549
    iget-object v3, p1, LX/7yF;->n:Lcom/facebook/fig/textinput/FigEditText;

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Lcom/facebook/fig/textinput/FigEditText;->setInputType(I)V

    .line 1278550
    iget-object v3, p1, LX/7yF;->n:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v4, p1, LX/7yF;->s:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v3, v4}, Lcom/facebook/fig/textinput/FigEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto/16 :goto_8

    .line 1278551
    :cond_1e
    iget-object v3, v0, LX/7ws;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1278552
    const-string v4, "phone"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1278553
    iget-object v3, p1, LX/7yF;->n:Lcom/facebook/fig/textinput/FigEditText;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/facebook/fig/textinput/FigEditText;->setInputType(I)V

    .line 1278554
    iget-object v3, p1, LX/7yF;->n:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v4, p1, LX/7yF;->s:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v3, v4}, Lcom/facebook/fig/textinput/FigEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto/16 :goto_8

    .line 1278555
    :cond_1f
    iget-object v0, p1, Lcom/facebook/events/tickets/modal/model/FieldItem;->c:Ljava/lang/Boolean;

    move-object p1, v0

    .line 1278556
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    goto/16 :goto_9

    .line 1278557
    :cond_20
    invoke-interface {v0}, LX/7ok;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    goto/16 :goto_c

    .line 1278558
    :cond_21
    invoke-interface {v0}, LX/7ok;->n()I

    move-result v1

    goto/16 :goto_c

    .line 1278559
    :cond_22
    iget-object v6, v4, Lcom/facebook/events/tickets/modal/model/FieldItem;->d:LX/0Rf;

    move-object v4, v6

    .line 1278560
    goto/16 :goto_e

    .line 1278561
    :cond_23
    invoke-interface {v0}, LX/7oo;->c()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1278562
    invoke-virtual {v2, v1, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    goto/16 :goto_14

    .line 1278563
    :cond_24
    invoke-interface {v0}, LX/7oo;->c()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v3, v1, LX/1vs;->b:I

    .line 1278564
    iget-object v1, p1, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;->n:LX/1Ad;

    sget-object v4, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;->o:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    iget-object v4, p1, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;->m:LX/1Ai;

    invoke-virtual {v1, v4}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v2, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1278565
    iget-object v2, p1, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto/16 :goto_15

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_1
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_e
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        -0x3278a018 -> :sswitch_1
        -0x8d55e06 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_c
        :pswitch_d
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        -0x48a41d43 -> :sswitch_2
        -0x48a41d42 -> :sswitch_3
        -0x10c1f372 -> :sswitch_6
        -0x9987146 -> :sswitch_7
        0x2e996b -> :sswitch_4
        0x5c24b9c -> :sswitch_9
        0x65b3d6e -> :sswitch_a
        0x68ac491 -> :sswitch_5
        0x77fdce94 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1278294
    iget-object v0, p0, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ws;

    .line 1278295
    iget-object p0, v0, LX/7ws;->c:LX/7wO;

    move-object v0, p0

    .line 1278296
    invoke-virtual {v0}, LX/7wO;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1278297
    iget-object v0, p0, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
