.class public LX/70l;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "LX/6vm;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/6qh;

.field public b:LX/6wC;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1162756
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 1162757
    return-void
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1162754
    const/4 v0, 0x0

    return v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1162755
    invoke-virtual {p0, p1}, LX/70l;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6vm;

    invoke-interface {v0}, LX/6vm;->a()LX/71I;

    move-result-object v0

    invoke-virtual {v0}, LX/71I;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1162753
    iget-object v1, p0, LX/70l;->b:LX/6wC;

    iget-object v2, p0, LX/70l;->a:LX/6qh;

    invoke-virtual {p0, p1}, LX/70l;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6vm;

    invoke-interface {v1, v2, v0, p2, p3}, LX/6wC;->a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1162752
    invoke-static {}, LX/71I;->values()[LX/71I;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 1162751
    invoke-virtual {p0, p1}, LX/70l;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6vm;

    invoke-interface {v0}, LX/6vm;->a()LX/71I;

    move-result-object v0

    invoke-virtual {v0}, LX/71I;->isSelectable()Z

    move-result v0

    return v0
.end method
