.class public LX/7WN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/13G;


# instance fields
.field private final a:LX/0yH;


# direct methods
.method public constructor <init>(LX/0yH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1216309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1216310
    iput-object p1, p0, LX/7WN;->a:LX/0yH;

    .line 1216311
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1216320
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(ILandroid/content/Intent;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Intent;",
            ")",
            "LX/0am",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1216321
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 2

    .prologue
    .line 1216316
    iget-object v0, p0, LX/7WN;->a:LX/0yH;

    sget-object v1, LX/0yY;->NATIVE_OPTIN_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1216317
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 1216318
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1216319
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 1216315
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1216314
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1216313
    const-string v0, "1710"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1216312
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->APP_FOREGROUND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SESSION_COLD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
