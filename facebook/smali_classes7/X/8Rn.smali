.class public LX/8Rn;
.super LX/622;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/622",
        "<",
        "LX/8QM;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8QM;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8QM;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/8QM;

.field private e:LX/2c9;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;LX/0Or;LX/2c9;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8QM;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/8QM;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/2c9;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1345517
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/622;-><init>(Ljava/lang/String;)V

    .line 1345518
    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/8Rn;->a:Ljava/util/List;

    .line 1345519
    invoke-static {p2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/8Rn;->b:Ljava/util/List;

    .line 1345520
    iput-object p3, p0, LX/8Rn;->c:LX/0Or;

    .line 1345521
    iput-object p4, p0, LX/8Rn;->e:LX/2c9;

    .line 1345522
    return-void
.end method


# virtual methods
.method public final a(I)LX/8QM;
    .locals 3

    .prologue
    .line 1345513
    iget-object v0, p0, LX/8Rn;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QM;

    .line 1345514
    invoke-virtual {v0}, LX/8QM;->n()Ljava/lang/Integer;

    move-result-object v2

    .line 1345515
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne p1, v2, :cond_0

    .line 1345516
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/8QY;I)V
    .locals 3

    .prologue
    .line 1345504
    iget-object v0, p0, LX/8Rn;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1345505
    :goto_0
    return-void

    .line 1345506
    :cond_0
    iget-object v0, p0, LX/8Rn;->a:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1345507
    if-nez p2, :cond_2

    .line 1345508
    iget-object v0, p0, LX/8Rn;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1345509
    :cond_1
    :goto_1
    iput-object p1, p0, LX/8Rn;->d:LX/8QM;

    goto :goto_0

    .line 1345510
    :cond_2
    iget-object v0, p0, LX/8Rn;->b:Ljava/util/List;

    iget-object v1, p0, LX/8Rn;->a:Ljava/util/List;

    add-int/lit8 v2, p2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1345511
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 1345512
    iget-object v1, p0, LX/8Rn;->b:Ljava/util/List;

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 1345500
    if-eqz p1, :cond_0

    .line 1345501
    iget-object v0, p0, LX/8Rn;->e:LX/2c9;

    const-string v1, "expand_more"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    .line 1345502
    :cond_0
    invoke-super {p0, p1}, LX/622;->a(Z)V

    .line 1345503
    return-void
.end method

.method public final d()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/8QM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1345489
    iget-object v0, p0, LX/8Rn;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1345490
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1345491
    if-eqz v0, :cond_1

    .line 1345492
    const/4 v2, 0x0

    .line 1345493
    iget-object v1, p0, LX/8Rn;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8QM;

    .line 1345494
    invoke-virtual {v1}, LX/8QM;->n()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1345495
    :goto_0
    if-eqz v1, :cond_1

    iget-object v0, p0, LX/8Rn;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1345496
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1345497
    :cond_1
    iget-object v0, p0, LX/8Rn;->d:LX/8QM;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/8Rn;->b:Ljava/util/List;

    iget-object v1, p0, LX/8Rn;->d:LX/8QM;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1345498
    iget-object v0, p0, LX/8Rn;->d:LX/8QM;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1345499
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v1, p0, LX/8Rn;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_3
    move-object v1, v2

    goto :goto_0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/8QM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1345472
    iget-object v0, p0, LX/8Rn;->a:Ljava/util/List;

    return-object v0
.end method

.method public final g()LX/8QN;
    .locals 3

    .prologue
    .line 1345485
    iget-object v0, p0, LX/8Rn;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QM;

    .line 1345486
    instance-of v2, v0, LX/8QN;

    if-eqz v2, :cond_0

    .line 1345487
    check-cast v0, LX/8QN;

    .line 1345488
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()LX/8QO;
    .locals 3

    .prologue
    .line 1345481
    iget-object v0, p0, LX/8Rn;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QM;

    .line 1345482
    instance-of v2, v0, LX/8QO;

    if-eqz v2, :cond_0

    .line 1345483
    check-cast v0, LX/8QO;

    .line 1345484
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()LX/8QX;
    .locals 3

    .prologue
    .line 1345477
    iget-object v0, p0, LX/8Rn;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QM;

    .line 1345478
    instance-of v2, v0, LX/8QX;

    if-eqz v2, :cond_0

    .line 1345479
    check-cast v0, LX/8QX;

    .line 1345480
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 1345473
    iget-object v0, p0, LX/8Rn;->a:Ljava/util/List;

    iget-object v1, p0, LX/8Rn;->d:LX/8QM;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1345474
    iget-object v0, p0, LX/8Rn;->b:Ljava/util/List;

    iget-object v1, p0, LX/8Rn;->d:LX/8QM;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1345475
    const/4 v0, 0x0

    iput-object v0, p0, LX/8Rn;->d:LX/8QM;

    .line 1345476
    return-void
.end method
