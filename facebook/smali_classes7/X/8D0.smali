.class public final enum LX/8D0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8D0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8D0;

.field public static final enum BOTTOM_SHEET:LX/8D0;

.field public static final enum ENTITY_ROW:LX/8D0;

.field public static final enum INLINE_ACTION:LX/8D0;

.field public static final enum MULTI_SELECTOR:LX/8D0;

.field public static final enum SINGLE_SELECTOR:LX/8D0;

.field public static final enum TOGGLE:LX/8D0;

.field public static final enum WASH_TEXT:LX/8D0;

.field public static final enum WEBVIEW:LX/8D0;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1312121
    new-instance v0, LX/8D0;

    const-string v1, "WEBVIEW"

    invoke-direct {v0, v1, v3}, LX/8D0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D0;->WEBVIEW:LX/8D0;

    .line 1312122
    new-instance v0, LX/8D0;

    const-string v1, "ENTITY_ROW"

    invoke-direct {v0, v1, v4}, LX/8D0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D0;->ENTITY_ROW:LX/8D0;

    .line 1312123
    new-instance v0, LX/8D0;

    const-string v1, "MULTI_SELECTOR"

    invoke-direct {v0, v1, v5}, LX/8D0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D0;->MULTI_SELECTOR:LX/8D0;

    .line 1312124
    new-instance v0, LX/8D0;

    const-string v1, "SINGLE_SELECTOR"

    invoke-direct {v0, v1, v6}, LX/8D0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D0;->SINGLE_SELECTOR:LX/8D0;

    .line 1312125
    new-instance v0, LX/8D0;

    const-string v1, "BOTTOM_SHEET"

    invoke-direct {v0, v1, v7}, LX/8D0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D0;->BOTTOM_SHEET:LX/8D0;

    .line 1312126
    new-instance v0, LX/8D0;

    const-string v1, "INLINE_ACTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8D0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D0;->INLINE_ACTION:LX/8D0;

    .line 1312127
    new-instance v0, LX/8D0;

    const-string v1, "TOGGLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8D0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D0;->TOGGLE:LX/8D0;

    .line 1312128
    new-instance v0, LX/8D0;

    const-string v1, "WASH_TEXT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/8D0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8D0;->WASH_TEXT:LX/8D0;

    .line 1312129
    const/16 v0, 0x8

    new-array v0, v0, [LX/8D0;

    sget-object v1, LX/8D0;->WEBVIEW:LX/8D0;

    aput-object v1, v0, v3

    sget-object v1, LX/8D0;->ENTITY_ROW:LX/8D0;

    aput-object v1, v0, v4

    sget-object v1, LX/8D0;->MULTI_SELECTOR:LX/8D0;

    aput-object v1, v0, v5

    sget-object v1, LX/8D0;->SINGLE_SELECTOR:LX/8D0;

    aput-object v1, v0, v6

    sget-object v1, LX/8D0;->BOTTOM_SHEET:LX/8D0;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8D0;->INLINE_ACTION:LX/8D0;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8D0;->TOGGLE:LX/8D0;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8D0;->WASH_TEXT:LX/8D0;

    aput-object v2, v0, v1

    sput-object v0, LX/8D0;->$VALUES:[LX/8D0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1312130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8D0;
    .locals 1

    .prologue
    .line 1312131
    const-class v0, LX/8D0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8D0;

    return-object v0
.end method

.method public static values()[LX/8D0;
    .locals 1

    .prologue
    .line 1312132
    sget-object v0, LX/8D0;->$VALUES:[LX/8D0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8D0;

    return-object v0
.end method
