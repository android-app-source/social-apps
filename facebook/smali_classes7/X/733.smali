.class public LX/733;
.super LX/6tx;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6tx",
        "<",
        "Lcom/facebook/payments/shipping/protocol/GetMailingAddressesResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1165175
    const-class v0, Lcom/facebook/payments/shipping/protocol/GetMailingAddressesResult;

    invoke-direct {p0, p1, v0}, LX/6tx;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 1165176
    return-void
.end method

.method public static b(LX/0QB;)LX/733;
    .locals 2

    .prologue
    .line 1165177
    new-instance v1, LX/733;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/733;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 1165178
    return-object v1
.end method


# virtual methods
.method public final a(LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1165179
    invoke-virtual {p1}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1165180
    const-string v1, "viewer"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 1165181
    const-string v2, "account_user"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 1165182
    const-string v2, "mailing_addresses"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 1165183
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1165184
    const-string v3, "nodes"

    invoke-static {v1, v3}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 1165185
    const-string p0, "address"

    invoke-virtual {v1, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0lF;

    .line 1165186
    invoke-static {}, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->newBuilder()LX/72i;

    move-result-object p1

    const-string v0, "id"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1165187
    iput-object v0, p1, LX/72i;->a:Ljava/lang/String;

    .line 1165188
    move-object p1, p1

    .line 1165189
    const-string v0, "label"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1165190
    iput-object v0, p1, LX/72i;->h:Ljava/lang/String;

    .line 1165191
    move-object p1, p1

    .line 1165192
    const-string v0, "city_name"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1165193
    iput-object v0, p1, LX/72i;->i:Ljava/lang/String;

    .line 1165194
    move-object p1, p1

    .line 1165195
    const-string v0, "region_name"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1165196
    iput-object v0, p1, LX/72i;->j:Ljava/lang/String;

    .line 1165197
    move-object p1, p1

    .line 1165198
    const-string v0, "is_default"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    .line 1165199
    iput-boolean v0, p1, LX/72i;->k:Z

    .line 1165200
    move-object p1, p1

    .line 1165201
    const-string v0, "addressee"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1165202
    iput-object v0, p1, LX/72i;->b:Ljava/lang/String;

    .line 1165203
    move-object p1, p1

    .line 1165204
    const-string v0, "street"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1165205
    iput-object v0, p1, LX/72i;->c:Ljava/lang/String;

    .line 1165206
    move-object p1, p1

    .line 1165207
    const-string v0, "building"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1165208
    iput-object v0, p1, LX/72i;->d:Ljava/lang/String;

    .line 1165209
    move-object p1, p1

    .line 1165210
    const-string v0, "city"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1165211
    iput-object v0, p1, LX/72i;->e:Ljava/lang/String;

    .line 1165212
    move-object p1, p1

    .line 1165213
    const-string v0, "postal_code"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1165214
    iput-object v0, p1, LX/72i;->f:Ljava/lang/String;

    .line 1165215
    move-object p1, p1

    .line 1165216
    const-string v0, "country"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-static {p0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/facebook/common/locale/Country;->a(Ljava/lang/String;)Lcom/facebook/common/locale/Country;

    move-result-object p0

    .line 1165217
    iput-object p0, p1, LX/72i;->g:Lcom/facebook/common/locale/Country;

    .line 1165218
    move-object p0, p1

    .line 1165219
    invoke-virtual {p0}, LX/72i;->l()Lcom/facebook/payments/shipping/model/SimpleMailingAddress;

    move-result-object p0

    move-object v1, p0

    .line 1165220
    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 1165221
    :cond_0
    new-instance v1, Lcom/facebook/payments/shipping/protocol/GetMailingAddressesResult;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/payments/shipping/protocol/GetMailingAddressesResult;-><init>(LX/0Px;)V

    move-object v0, v1

    .line 1165222
    return-object v0
.end method

.method public final b()LX/14N;
    .locals 4

    .prologue
    .line 1165223
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1165224
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "q"

    const-string v3, "viewer() {account_user {mailing_addresses {nodes {id, label, city_name, region_name, is_default, address {addressee, street, building, city, postal_code, country}}}}}"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165225
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "get_mailing_addresses"

    .line 1165226
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1165227
    move-object v1, v1

    .line 1165228
    const-string v2, "GET"

    .line 1165229
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1165230
    move-object v1, v1

    .line 1165231
    const-string v2, "graphql"

    .line 1165232
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1165233
    move-object v1, v1

    .line 1165234
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1165235
    move-object v0, v1

    .line 1165236
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1165237
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1165238
    move-object v0, v0

    .line 1165239
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1165240
    const-string v0, "get_mailing_addresses"

    return-object v0
.end method
