.class public final LX/7A1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "viewerRecommendation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "viewerRecommendation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "eventViewerCapability"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "eventViewerCapability"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:D

.field public h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:I

.field public l:I

.field public m:I

.field public n:Z

.field public o:Z

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:D

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Z

.field public u:D

.field public v:D

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:I

.field public z:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1174733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1174734
    return-void
.end method

.method public static a(Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;)LX/7A1;
    .locals 4

    .prologue
    .line 1174735
    new-instance v0, LX/7A1;

    invoke-direct {v0}, LX/7A1;-><init>()V

    .line 1174736
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/7A1;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1174737
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->k()Z

    move-result v1

    iput-boolean v1, v0, LX/7A1;->b:Z

    .line 1174738
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->l()Z

    move-result v1

    iput-boolean v1, v0, LX/7A1;->c:Z

    .line 1174739
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->m()Z

    move-result v1

    iput-boolean v1, v0, LX/7A1;->d:Z

    .line 1174740
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->n()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/7A1;->e:LX/15i;

    iput v1, v0, LX/7A1;->f:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1174741
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->o()D

    move-result-wide v2

    iput-wide v2, v0, LX/7A1;->g:D

    .line 1174742
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->p()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v1

    iput-object v1, v0, LX/7A1;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    .line 1174743
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->q()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v1

    iput-object v1, v0, LX/7A1;->i:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 1174744
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7A1;->j:Ljava/lang/String;

    .line 1174745
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->s()I

    move-result v1

    iput v1, v0, LX/7A1;->k:I

    .line 1174746
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->t()I

    move-result v1

    iput v1, v0, LX/7A1;->l:I

    .line 1174747
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->u()I

    move-result v1

    iput v1, v0, LX/7A1;->m:I

    .line 1174748
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->v()Z

    move-result v1

    iput-boolean v1, v0, LX/7A1;->n:Z

    .line 1174749
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->w()Z

    move-result v1

    iput-boolean v1, v0, LX/7A1;->o:Z

    .line 1174750
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->x()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7A1;->p:Ljava/lang/String;

    .line 1174751
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->y()D

    move-result-wide v2

    iput-wide v2, v0, LX/7A1;->q:D

    .line 1174752
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->z()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7A1;->r:Ljava/lang/String;

    .line 1174753
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->A()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7A1;->s:Ljava/lang/String;

    .line 1174754
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->B()Z

    move-result v1

    iput-boolean v1, v0, LX/7A1;->t:Z

    .line 1174755
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->C()D

    move-result-wide v2

    iput-wide v2, v0, LX/7A1;->u:D

    .line 1174756
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->D()D

    move-result-wide v2

    iput-wide v2, v0, LX/7A1;->v:D

    .line 1174757
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->E()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7A1;->w:Ljava/lang/String;

    .line 1174758
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->F()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7A1;->x:Ljava/lang/String;

    .line 1174759
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->G()I

    move-result v1

    iput v1, v0, LX/7A1;->y:I

    .line 1174760
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->H()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    move-result-object v1

    iput-object v1, v0, LX/7A1;->z:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    .line 1174761
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->I()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iput-object v2, v0, LX/7A1;->A:LX/15i;

    iput v1, v0, LX/7A1;->B:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1174762
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->J()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    iput-object v1, v0, LX/7A1;->C:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1174763
    return-object v0

    .line 1174764
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1174765
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/15i;I)LX/7A1;
    .locals 2
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "viewerRecommendation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1174766
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, LX/7A1;->A:LX/15i;

    iput p2, p0, LX/7A1;->B:I

    monitor-exit v1

    .line 1174767
    return-object p0

    .line 1174768
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;
    .locals 19

    .prologue
    .line 1174769
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1174770
    move-object/from16 v0, p0

    iget-object v3, v0, LX/7A1;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1174771
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7A1;->e:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/7A1;->f:I

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v4, -0x4ead19c2

    invoke-static {v5, v6, v4}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1174772
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7A1;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1174773
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7A1;->i:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1174774
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7A1;->j:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1174775
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7A1;->p:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1174776
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7A1;->r:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1174777
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7A1;->s:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1174778
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7A1;->w:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1174779
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7A1;->x:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 1174780
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7A1;->z:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1174781
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_1
    move-object/from16 v0, p0

    iget-object v6, v0, LX/7A1;->A:LX/15i;

    move-object/from16 v0, p0

    iget v7, v0, LX/7A1;->B:I

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v5, 0x52c4008e

    invoke-static {v6, v7, v5}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v5

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1174782
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7A1;->C:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v2, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    .line 1174783
    const/16 v5, 0x1b

    invoke-virtual {v2, v5}, LX/186;->c(I)V

    .line 1174784
    const/4 v5, 0x0

    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1174785
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/7A1;->b:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 1174786
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/7A1;->c:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 1174787
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/7A1;->d:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 1174788
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v4}, LX/186;->b(II)V

    .line 1174789
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7A1;->g:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1174790
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1174791
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1174792
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1174793
    const/16 v3, 0x9

    move-object/from16 v0, p0

    iget v4, v0, LX/7A1;->k:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1174794
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget v4, v0, LX/7A1;->l:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1174795
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget v4, v0, LX/7A1;->m:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1174796
    const/16 v3, 0xc

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7A1;->n:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1174797
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7A1;->o:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1174798
    const/16 v3, 0xe

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1174799
    const/16 v3, 0xf

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7A1;->q:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1174800
    const/16 v3, 0x10

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 1174801
    const/16 v3, 0x11

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 1174802
    const/16 v3, 0x12

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7A1;->t:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1174803
    const/16 v3, 0x13

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7A1;->u:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1174804
    const/16 v3, 0x14

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7A1;->v:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1174805
    const/16 v3, 0x15

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 1174806
    const/16 v3, 0x16

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 1174807
    const/16 v3, 0x17

    move-object/from16 v0, p0

    iget v4, v0, LX/7A1;->y:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1174808
    const/16 v3, 0x18

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1174809
    const/16 v3, 0x19

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1174810
    const/16 v3, 0x1a

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1174811
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1174812
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1174813
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1174814
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1174815
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1174816
    new-instance v3, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;-><init>(LX/15i;)V

    .line 1174817
    return-object v3

    .line 1174818
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 1174819
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2
.end method
