.class public LX/71V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wI",
        "<",
        "Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerRunTimeData;",
        "LX/71X;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1163395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163396
    return-void
.end method

.method private static a(LX/0Pz;Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerRunTimeData;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/6vm;",
            ">;",
            "Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerRunTimeData;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1163388
    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;

    .line 1163389
    iget-object v8, v6, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;->c:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v9, :cond_0

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/facebook/payments/picker/option/PaymentsPickerOption;

    .line 1163390
    iget-object v0, v3, Lcom/facebook/payments/picker/option/PaymentsPickerOption;->a:Ljava/lang/String;

    sget-object v1, LX/71X;->PAYMENTS_PICKER_OPTION:LX/71X;

    invoke-virtual {p1, v1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a(LX/6vZ;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 1163391
    new-instance v0, LX/71Q;

    iget-object v1, v6, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;->a:Ljava/lang/String;

    iget-object v2, v3, Lcom/facebook/payments/picker/option/PaymentsPickerOption;->a:Ljava/lang/String;

    iget-object v3, v3, Lcom/facebook/payments/picker/option/PaymentsPickerOption;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->f()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v5

    iget-object v5, v5, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-direct/range {v0 .. v5}, LX/71Q;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/payments/logging/PaymentsLoggingSessionData;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1163392
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 1163393
    :cond_0
    invoke-static {p0}, LX/714;->a(LX/0Pz;)V

    .line 1163394
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;LX/0Px;)LX/0Px;
    .locals 6

    .prologue
    .line 1163380
    check-cast p1, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerRunTimeData;

    .line 1163381
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1163382
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/71X;

    .line 1163383
    sget-object v4, LX/71U;->a:[I

    invoke-virtual {v0}, LX/71X;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1163384
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled section type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1163385
    :pswitch_0
    invoke-static {v2, p1}, LX/71V;->a(LX/0Pz;Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerRunTimeData;)V

    .line 1163386
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1163387
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
