.class public LX/8WF;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8Vc;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1353872
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1353873
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1353874
    packed-switch p2, :pswitch_data_0

    .line 1353875
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1353876
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030292

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1353877
    new-instance v0, LX/8WD;

    invoke-direct {v0, p0, v1}, LX/8WD;-><init>(LX/8WF;Landroid/view/View;)V

    goto :goto_0

    .line 1353878
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030293

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1353879
    new-instance v0, LX/8WE;

    invoke-direct {v0, p0, v1}, LX/8WE;-><init>(LX/8WF;Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 1353880
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1353881
    :cond_0
    :goto_0
    return-void

    .line 1353882
    :pswitch_0
    instance-of v0, p1, LX/8WD;

    if-eqz v0, :cond_0

    .line 1353883
    check-cast p1, LX/8WD;

    .line 1353884
    iget-object v0, p0, LX/8WF;->a:Ljava/util/List;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1353885
    if-ltz p2, :cond_1

    iget-object v0, p0, LX/8WF;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1353886
    iget-object v0, p0, LX/8WF;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Vc;

    move-object v0, v0

    .line 1353887
    iget-object p0, p1, LX/8WD;->m:Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 1353888
    goto :goto_0

    .line 1353889
    :pswitch_1
    instance-of v0, p1, LX/8WE;

    if-eqz v0, :cond_0

    .line 1353890
    check-cast p1, LX/8WE;

    .line 1353891
    iget-object v0, p0, LX/8WF;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x6

    .line 1353892
    iget-object v1, p1, LX/8WE;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p1, LX/8WE;->m:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082370

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 p0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v4, p0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1353893
    goto :goto_0

    .line 1353894
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1353895
    const/4 v0, 0x6

    if-ge p1, v0, :cond_0

    .line 1353896
    const/4 v0, 0x1

    .line 1353897
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 1353898
    iget-object v0, p0, LX/8WF;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1353899
    iget-object v0, p0, LX/8WF;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x7

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1353900
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
