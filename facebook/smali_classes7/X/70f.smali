.class public LX/70f;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1162508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)LX/14O;
    .locals 2

    .prologue
    .line 1162509
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    .line 1162510
    iput-object p0, v0, LX/14O;->b:Ljava/lang/String;

    .line 1162511
    move-object v0, v0

    .line 1162512
    iput-object p1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1162513
    move-object v0, v0

    .line 1162514
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1162515
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1162516
    move-object v0, v0

    .line 1162517
    return-object v0
.end method

.method public static a(Ljava/lang/StringBuilder;Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1162518
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;Ljava/lang/StringBuilder;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1162519
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->e:Lcom/facebook/common/locale/Country;

    move-object v0, v0

    .line 1162520
    if-eqz v0, :cond_0

    .line 1162521
    const-string v0, ".country_code(%s)"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1162522
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->e:Lcom/facebook/common/locale/Country;

    move-object v0, v0

    .line 1162523
    invoke-virtual {v0}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1162524
    :cond_0
    return-void
.end method
