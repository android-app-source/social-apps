.class public LX/87Q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1300075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300076
    return-void
.end method

.method public static a(LX/0Px;Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1300049
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1300050
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1300051
    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1300052
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_2

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1300053
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1300054
    return-object v0

    :cond_0
    move v0, v1

    .line 1300055
    goto :goto_0

    .line 1300056
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1300057
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t find index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in model"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(LX/0is;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0is;",
            ">(TModelData;)",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;"
        }
    .end annotation

    .prologue
    .line 1300058
    invoke-interface {p0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1300059
    const/4 v0, 0x0

    .line 1300060
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/87Q;->a(LX/0is;Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0is;Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0is;",
            ">(TModelData;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;"
        }
    .end annotation

    .prologue
    .line 1300061
    invoke-interface {p0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getInspirationModels()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/87Q;->a(LX/0Px;Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0Px;Ljava/lang/String;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 1300062
    invoke-static {p0, p1}, LX/87Q;->d(LX/0Px;Ljava/lang/String;)I

    move-result v0

    .line 1300063
    if-gez v0, :cond_0

    .line 1300064
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t find id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in model"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1300065
    :cond_0
    return v0
.end method

.method public static c(LX/0Px;Ljava/lang/String;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1300066
    invoke-static {p0, p1}, LX/87Q;->d(LX/0Px;Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(LX/0Px;Ljava/lang/String;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1300067
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1300068
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1300069
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1300070
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1300071
    :goto_1
    return v0

    .line 1300072
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 1300073
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1300074
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
