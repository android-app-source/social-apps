.class public abstract LX/6ld;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6lX;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1142831
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142832
    iput-object p1, p0, LX/6ld;->a:Landroid/content/Context;

    .line 1142833
    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public a(LX/6lg;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1142834
    invoke-virtual {p1}, LX/6lg;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1142835
    iget-object v0, p0, LX/6ld;->a:Landroid/content/Context;

    invoke-virtual {p0}, LX/6ld;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1142836
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6ld;->a:Landroid/content/Context;

    invoke-virtual {p0}, LX/6ld;->b()I

    move-result v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, LX/6lg;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract b()I
.end method
