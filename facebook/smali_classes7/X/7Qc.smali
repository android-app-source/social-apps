.class public final LX/7Qc;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateVideoFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Collection;

.field public final synthetic b:LX/7Qe;


# direct methods
.method public constructor <init>(LX/7Qe;Ljava/util/Collection;)V
    .locals 0

    .prologue
    .line 1204667
    iput-object p1, p0, LX/7Qc;->b:LX/7Qe;

    iput-object p2, p0, LX/7Qc;->a:Ljava/util/Collection;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1204668
    iget-object v0, p0, LX/7Qc;->b:LX/7Qe;

    iget-object v0, v0, LX/7Qe;->e:LX/0Zc;

    const-string v1, ""

    sget-object v2, LX/7IM;->LIVE_SUBSCRIPTION_QUERY_FAIL:LX/7IM;

    const-string v3, "Live subscription query failed"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0Zc;->a(Ljava/lang/String;LX/7IM;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1204669
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1204670
    check-cast p1, Ljava/util/Map;

    .line 1204671
    if-eqz p1, :cond_3

    iget-object v0, p0, LX/7Qc;->b:LX/7Qe;

    iget-object v0, v0, LX/7Qe;->g:LX/7QY;

    if-eqz v0, :cond_3

    .line 1204672
    iget-object v0, p0, LX/7Qc;->b:LX/7Qe;

    iget-object v0, v0, LX/7Qe;->g:LX/7QY;

    iget-object v1, p0, LX/7Qc;->a:Ljava/util/Collection;

    .line 1204673
    iget-object v2, v0, LX/7QY;->a:LX/7Qb;

    iget-object v2, v2, LX/7Qb;->h:LX/7QZ;

    invoke-virtual {v2, v1}, LX/7QZ;->b(Ljava/util/Collection;)V

    .line 1204674
    const/4 v5, 0x0

    .line 1204675
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1204676
    iget-object v3, v0, LX/7QY;->a:LX/7Qb;

    iget-object v3, v3, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7Qa;

    .line 1204677
    if-eqz v3, :cond_0

    .line 1204678
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateVideoFragmentModel;

    .line 1204679
    if-nez v4, :cond_1

    .line 1204680
    iget-object v3, v0, LX/7QY;->a:LX/7Qb;

    invoke-static {v3, v2}, LX/7Qb;->b$redex0(LX/7Qb;Ljava/lang/String;)V

    goto :goto_0

    .line 1204681
    :cond_1
    iget-object v2, v3, LX/7Qa;->f:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1204682
    invoke-virtual {v4}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateVideoFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v4

    .line 1204683
    if-eq v2, v4, :cond_5

    .line 1204684
    if-nez v5, :cond_4

    .line 1204685
    new-instance v2, Ljava/util/HashSet;

    const/4 v4, 0x1

    invoke-direct {v2, v4}, Ljava/util/HashSet;-><init>(I)V

    .line 1204686
    :goto_1
    iget-object v3, v3, LX/7Qa;->c:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_2
    move-object v5, v2

    .line 1204687
    goto :goto_0

    .line 1204688
    :cond_2
    if-eqz v5, :cond_3

    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1204689
    iget-object v2, v0, LX/7QY;->a:LX/7Qb;

    iget-object v2, v2, LX/7Qb;->b:LX/7Qe;

    invoke-virtual {v2, v5}, LX/7Qe;->a(Ljava/util/Set;)V

    .line 1204690
    :cond_3
    return-void

    :cond_4
    move-object v2, v5

    goto :goto_1

    :cond_5
    move-object v2, v5

    goto :goto_2
.end method
