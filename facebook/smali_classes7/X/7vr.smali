.class public final LX/7vr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

.field public final synthetic b:Lcom/facebook/payments/checkout/model/CheckoutData;

.field public final synthetic c:LX/7vs;


# direct methods
.method public constructor <init>(LX/7vs;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 0

    .prologue
    .line 1275169
    iput-object p1, p0, LX/7vr;->c:LX/7vs;

    iput-object p2, p0, LX/7vr;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iput-object p3, p0, LX/7vr;->b:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1275170
    iget-object v0, p0, LX/7vr;->c:LX/7vs;

    iget-object v0, v0, LX/7vs;->d:LX/1nQ;

    iget-object v1, p0, LX/7vr;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->o:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    .line 1275171
    iget-object v2, v0, LX/1nQ;->i:LX/0Zb;

    const-string v3, "event_buy_tickets_purchase_failed"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 1275172
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1275173
    const-string v3, "event_ticketing"

    invoke-virtual {v2, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v2

    iget-object v3, v0, LX/1nQ;->j:LX/0kv;

    iget-object v4, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v3, v4}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "Event"

    invoke-virtual {v2, v3}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v2

    iget-object v3, v1, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "event_id"

    iget-object v4, v1, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "session_id"

    iget-object v4, v1, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    invoke-virtual {v2}, LX/0oG;->d()V

    .line 1275174
    :cond_0
    iget-object v0, p0, LX/7vr;->c:LX/7vs;

    iget-object v1, p0, LX/7vr;->b:Lcom/facebook/payments/checkout/model/CheckoutData;

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2, p1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, p1, v2}, LX/7vs;->a$redex0(LX/7vs;Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1275175
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1275176
    iget-object v0, p0, LX/7vr;->c:LX/7vs;

    iget-object v1, p0, LX/7vr;->b:Lcom/facebook/payments/checkout/model/CheckoutData;

    iget-object v2, p0, LX/7vr;->c:LX/7vs;

    iget-object v2, v2, LX/7vs;->c:Landroid/content/Context;

    const v3, 0x7f08003e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, p1}, LX/7vs;->a$redex0(LX/7vs;Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1275177
    return-void
.end method
