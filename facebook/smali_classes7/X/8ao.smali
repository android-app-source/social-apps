.class public final LX/8ao;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1368741
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1368742
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1368743
    :goto_0
    return v1

    .line 1368744
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1368745
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1368746
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1368747
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1368748
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1368749
    const-string v6, "bottom_spacing"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1368750
    invoke-static {p0, p1}, LX/8an;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1368751
    :cond_2
    const-string v6, "left_spacing"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1368752
    invoke-static {p0, p1}, LX/8an;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1368753
    :cond_3
    const-string v6, "right_spacing"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1368754
    invoke-static {p0, p1}, LX/8an;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1368755
    :cond_4
    const-string v6, "top_spacing"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1368756
    invoke-static {p0, p1}, LX/8an;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1368757
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1368758
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1368759
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1368760
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1368761
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1368762
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1368722
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1368723
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368724
    if-eqz v0, :cond_0

    .line 1368725
    const-string v1, "bottom_spacing"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368726
    invoke-static {p0, v0, p2}, LX/8an;->a(LX/15i;ILX/0nX;)V

    .line 1368727
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368728
    if-eqz v0, :cond_1

    .line 1368729
    const-string v1, "left_spacing"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368730
    invoke-static {p0, v0, p2}, LX/8an;->a(LX/15i;ILX/0nX;)V

    .line 1368731
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368732
    if-eqz v0, :cond_2

    .line 1368733
    const-string v1, "right_spacing"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368734
    invoke-static {p0, v0, p2}, LX/8an;->a(LX/15i;ILX/0nX;)V

    .line 1368735
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368736
    if-eqz v0, :cond_3

    .line 1368737
    const-string v1, "top_spacing"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368738
    invoke-static {p0, v0, p2}, LX/8an;->a(LX/15i;ILX/0nX;)V

    .line 1368739
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1368740
    return-void
.end method
