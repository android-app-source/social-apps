.class public final LX/8Wa;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/View;

.field private final b:Lcom/facebook/widget/tiles/ThreadTileView;

.field public final c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1354249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1354250
    iput-object p1, p0, LX/8Wa;->a:Landroid/view/View;

    .line 1354251
    const v0, 0x7f0d13e7

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, LX/8Wa;->b:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 1354252
    const v0, 0x7f0d13e8

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8Wa;->c:Landroid/widget/TextView;

    .line 1354253
    return-void
.end method


# virtual methods
.method public final a(LX/8Vb;)V
    .locals 2

    .prologue
    .line 1354254
    iget-object v0, p0, LX/8Wa;->b:Lcom/facebook/widget/tiles/ThreadTileView;

    iget-object v1, p1, LX/8Vb;->i:LX/8Vd;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 1354255
    iget-object v0, p0, LX/8Wa;->c:Landroid/widget/TextView;

    iget-object v1, p1, LX/8Vb;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354256
    return-void
.end method
