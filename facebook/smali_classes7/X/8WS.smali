.class public LX/8WS;
.super LX/1a1;
.source ""


# instance fields
.field public final l:Landroid/view/View;

.field public final m:Lcom/facebook/widget/tiles/ThreadTileView;

.field public final n:Lcom/facebook/widget/text/BetterTextView;

.field public final o:Lcom/facebook/widget/text/BetterTextView;

.field public final p:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1354077
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1354078
    iput-object p1, p0, LX/8WS;->l:Landroid/view/View;

    .line 1354079
    const v0, 0x7f0d0946

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, LX/8WS;->m:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 1354080
    const v0, 0x7f0d13f6

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/8WS;->n:Lcom/facebook/widget/text/BetterTextView;

    .line 1354081
    const v0, 0x7f0d13f7

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/8WS;->o:Lcom/facebook/widget/text/BetterTextView;

    .line 1354082
    const v0, 0x7f0d13f8

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/8WS;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 1354083
    return-void
.end method


# virtual methods
.method public final a(ZZZ)V
    .locals 7

    .prologue
    .line 1354084
    iget-object v0, p0, LX/8WS;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f020be8

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 1354085
    if-eqz p1, :cond_3

    .line 1354086
    invoke-virtual {v0}, Landroid/graphics/drawable/GradientDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 1354087
    iget-object v1, p0, LX/8WS;->l:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0711

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1354088
    const/4 v3, 0x0

    .line 1354089
    const/16 v1, 0x8

    new-array v5, v1, [F

    .line 1354090
    iget-object v1, p0, LX/8WS;->l:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1800

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 1354091
    const/4 v1, 0x0

    move v4, v1

    :goto_0
    const/4 v1, 0x4

    if-ge v4, v1, :cond_2

    .line 1354092
    if-eqz p2, :cond_0

    move v1, v2

    :goto_1
    aput v1, v5, v4

    .line 1354093
    add-int/lit8 v6, v4, 0x4

    if-eqz p3, :cond_1

    move v1, v2

    :goto_2
    aput v1, v5, v6

    .line 1354094
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_0
    move v1, v3

    .line 1354095
    goto :goto_1

    :cond_1
    move v1, v3

    .line 1354096
    goto :goto_2

    .line 1354097
    :cond_2
    move-object v1, v5

    .line 1354098
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    .line 1354099
    :cond_3
    iget-object v1, p0, LX/8WS;->l:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1354100
    iget-object v1, p0, LX/8WS;->p:Lcom/facebook/widget/text/BetterTextView;

    iget-object v0, p0, LX/8WS;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_4

    const v0, 0x7f0a0705

    :goto_3
    invoke-static {v2, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1354101
    return-void

    .line 1354102
    :cond_4
    const v0, 0x7f0a0712

    goto :goto_3
.end method
