.class public final enum LX/77m;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/77m;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/77m;

.field public static final enum DISMISS_ACTION:LX/77m;

.field public static final enum PRIMARY_ACTION:LX/77m;

.field public static final enum SECONDARY_ACTION:LX/77m;


# instance fields
.field private final mAnalyticEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1172046
    new-instance v0, LX/77m;

    const-string v1, "PRIMARY_ACTION"

    const-string v2, "primary"

    invoke-direct {v0, v1, v3, v2}, LX/77m;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/77m;->PRIMARY_ACTION:LX/77m;

    .line 1172047
    new-instance v0, LX/77m;

    const-string v1, "SECONDARY_ACTION"

    const-string v2, "secondary"

    invoke-direct {v0, v1, v4, v2}, LX/77m;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/77m;->SECONDARY_ACTION:LX/77m;

    .line 1172048
    new-instance v0, LX/77m;

    const-string v1, "DISMISS_ACTION"

    const-string v2, "dismiss"

    invoke-direct {v0, v1, v5, v2}, LX/77m;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/77m;->DISMISS_ACTION:LX/77m;

    .line 1172049
    const/4 v0, 0x3

    new-array v0, v0, [LX/77m;

    sget-object v1, LX/77m;->PRIMARY_ACTION:LX/77m;

    aput-object v1, v0, v3

    sget-object v1, LX/77m;->SECONDARY_ACTION:LX/77m;

    aput-object v1, v0, v4

    sget-object v1, LX/77m;->DISMISS_ACTION:LX/77m;

    aput-object v1, v0, v5

    sput-object v0, LX/77m;->$VALUES:[LX/77m;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1172050
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1172051
    iput-object p3, p0, LX/77m;->mAnalyticEventName:Ljava/lang/String;

    .line 1172052
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/77m;
    .locals 1

    .prologue
    .line 1172053
    const-class v0, LX/77m;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/77m;

    return-object v0
.end method

.method public static values()[LX/77m;
    .locals 1

    .prologue
    .line 1172054
    sget-object v0, LX/77m;->$VALUES:[LX/77m;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/77m;

    return-object v0
.end method


# virtual methods
.method public final toAnalyticEventName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1172055
    iget-object v0, p0, LX/77m;->mAnalyticEventName:Ljava/lang/String;

    return-object v0
.end method
