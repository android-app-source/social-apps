.class public LX/8cK;
.super LX/8cI;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

.field public final f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final g:Z

.field public final h:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

.field public final i:Z

.field public final j:Ljava/lang/String;

.field public final k:Z


# direct methods
.method public constructor <init>(LX/8cJ;)V
    .locals 10

    .prologue
    .line 1374332
    iget-object v0, p1, LX/8cJ;->c:Ljava/lang/String;

    move-object v2, v0

    .line 1374333
    iget-object v0, p1, LX/8cJ;->d:Ljava/lang/String;

    move-object v3, v0

    .line 1374334
    iget-object v0, p1, LX/8cJ;->g:Ljava/lang/String;

    move-object v4, v0

    .line 1374335
    iget-object v0, p1, LX/8cJ;->i:Ljava/lang/String;

    move-object v5, v0

    .line 1374336
    iget-wide v8, p1, LX/8cJ;->j:D

    move-wide v6, v8

    .line 1374337
    move-object v1, p0

    invoke-direct/range {v1 .. v7}, LX/8cI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V

    .line 1374338
    iget-object v0, p1, LX/8cJ;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1374339
    iput-object v0, p0, LX/8cK;->a:Ljava/lang/String;

    .line 1374340
    iget-object v0, p1, LX/8cJ;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1374341
    iput-object v0, p0, LX/8cK;->b:Ljava/lang/String;

    .line 1374342
    iget-object v0, p1, LX/8cJ;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1374343
    iput-object v0, p0, LX/8cK;->c:Ljava/lang/String;

    .line 1374344
    iget-boolean v0, p1, LX/8cJ;->a:Z

    move v0, v0

    .line 1374345
    iput-boolean v0, p0, LX/8cK;->d:Z

    .line 1374346
    iget-object v0, p1, LX/8cJ;->b:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object v0, v0

    .line 1374347
    iput-object v0, p0, LX/8cK;->e:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1374348
    iget-object v0, p1, LX/8cJ;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v0, v0

    .line 1374349
    iput-object v0, p0, LX/8cK;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1374350
    iget-boolean v0, p1, LX/8cJ;->l:Z

    move v0, v0

    .line 1374351
    iput-boolean v0, p0, LX/8cK;->g:Z

    .line 1374352
    iget-object v0, p1, LX/8cJ;->m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-object v0, v0

    .line 1374353
    iput-object v0, p0, LX/8cK;->h:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1374354
    iget-boolean v0, p1, LX/8cJ;->n:Z

    move v0, v0

    .line 1374355
    iput-boolean v0, p0, LX/8cK;->i:Z

    .line 1374356
    iget-object v0, p1, LX/8cJ;->o:Ljava/lang/String;

    move-object v0, v0

    .line 1374357
    iput-object v0, p0, LX/8cK;->j:Ljava/lang/String;

    .line 1374358
    iget-boolean v0, p1, LX/8cJ;->p:Z

    move v0, v0

    .line 1374359
    iput-boolean v0, p0, LX/8cK;->k:Z

    .line 1374360
    return-void
.end method

.method public static q()LX/8cJ;
    .locals 2

    .prologue
    .line 1374361
    new-instance v0, LX/8cJ;

    invoke-direct {v0}, LX/8cJ;-><init>()V

    return-object v0
.end method
