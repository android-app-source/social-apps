.class public final enum LX/8Oi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Oi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Oi;

.field public static final enum FINISHED:LX/8Oi;

.field public static final enum POST:LX/8Oi;

.field public static final enum RECEIVE:LX/8Oi;

.field public static final enum START:LX/8Oi;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1340278
    new-instance v0, LX/8Oi;

    const-string v1, "START"

    invoke-direct {v0, v1, v2}, LX/8Oi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Oi;->START:LX/8Oi;

    new-instance v0, LX/8Oi;

    const-string v1, "RECEIVE"

    invoke-direct {v0, v1, v3}, LX/8Oi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Oi;->RECEIVE:LX/8Oi;

    new-instance v0, LX/8Oi;

    const-string v1, "POST"

    invoke-direct {v0, v1, v4}, LX/8Oi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Oi;->POST:LX/8Oi;

    new-instance v0, LX/8Oi;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v5}, LX/8Oi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Oi;->FINISHED:LX/8Oi;

    .line 1340279
    const/4 v0, 0x4

    new-array v0, v0, [LX/8Oi;

    sget-object v1, LX/8Oi;->START:LX/8Oi;

    aput-object v1, v0, v2

    sget-object v1, LX/8Oi;->RECEIVE:LX/8Oi;

    aput-object v1, v0, v3

    sget-object v1, LX/8Oi;->POST:LX/8Oi;

    aput-object v1, v0, v4

    sget-object v1, LX/8Oi;->FINISHED:LX/8Oi;

    aput-object v1, v0, v5

    sput-object v0, LX/8Oi;->$VALUES:[LX/8Oi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1340280
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Oi;
    .locals 1

    .prologue
    .line 1340281
    const-class v0, LX/8Oi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Oi;

    return-object v0
.end method

.method public static values()[LX/8Oi;
    .locals 1

    .prologue
    .line 1340282
    sget-object v0, LX/8Oi;->$VALUES:[LX/8Oi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Oi;

    return-object v0
.end method
