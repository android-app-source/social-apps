.class public LX/6gF;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field public a:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1121508
    const-class v0, LX/6gF;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6gF;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1121499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1121500
    return-void
.end method

.method public static a(LX/0QB;)LX/6gF;
    .locals 4

    .prologue
    .line 1121503
    new-instance v3, LX/6gF;

    invoke-direct {v3}, LX/6gF;-><init>()V

    .line 1121504
    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1121505
    iput-object v0, v3, LX/6gF;->a:LX/0SG;

    iput-object v1, v3, LX/6gF;->b:LX/0ad;

    iput-object v2, v3, LX/6gF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1121506
    move-object v0, v3

    .line 1121507
    return-object v0
.end method


# virtual methods
.method public final b(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 1121501
    invoke-static {p1}, LX/6gE;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 1121502
    iget-object v1, p0, LX/6gF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v2, 0x0

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method
