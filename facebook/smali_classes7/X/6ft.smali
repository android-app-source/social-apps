.class public final LX/6ft;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

.field public c:J

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1120667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120668
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/model/threads/ThreadEventReminder;)V
    .locals 4

    .prologue
    .line 1120649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120650
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1120651
    iput-object v0, p0, LX/6ft;->a:Ljava/lang/String;

    .line 1120652
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-object v0, v0

    .line 1120653
    iput-object v0, p0, LX/6ft;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 1120654
    iget-wide v2, p1, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->c:J

    move-wide v0, v2

    .line 1120655
    iput-wide v0, p0, LX/6ft;->c:J

    .line 1120656
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1120657
    iput-object v0, p0, LX/6ft;->d:Ljava/lang/String;

    .line 1120658
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->e:Z

    move v0, v0

    .line 1120659
    iput-boolean v0, p0, LX/6ft;->e:Z

    .line 1120660
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->f:LX/0P1;

    move-object v0, v0

    .line 1120661
    iput-object v0, p0, LX/6ft;->f:LX/0P1;

    .line 1120662
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->g:Z

    move v0, v0

    .line 1120663
    iput-boolean v0, p0, LX/6ft;->g:Z

    .line 1120664
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1120665
    iput-object v0, p0, LX/6ft;->h:Ljava/lang/String;

    .line 1120666
    return-void
.end method


# virtual methods
.method public final a(J)LX/6ft;
    .locals 1

    .prologue
    .line 1120647
    iput-wide p1, p0, LX/6ft;->c:J

    .line 1120648
    return-object p0
.end method

.method public final a(LX/0P1;)LX/6ft;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;",
            ">;)",
            "LX/6ft;"
        }
    .end annotation

    .prologue
    .line 1120645
    iput-object p1, p0, LX/6ft;->f:LX/0P1;

    .line 1120646
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLLightweightEventType;)LX/6ft;
    .locals 0

    .prologue
    .line 1120643
    iput-object p1, p0, LX/6ft;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 1120644
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/6ft;
    .locals 0

    .prologue
    .line 1120632
    iput-object p1, p0, LX/6ft;->a:Ljava/lang/String;

    .line 1120633
    return-object p0
.end method

.method public final a(Z)LX/6ft;
    .locals 0

    .prologue
    .line 1120641
    iput-boolean p1, p0, LX/6ft;->e:Z

    .line 1120642
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/6ft;
    .locals 0

    .prologue
    .line 1120639
    iput-object p1, p0, LX/6ft;->d:Ljava/lang/String;

    .line 1120640
    return-object p0
.end method

.method public final b(Z)LX/6ft;
    .locals 0

    .prologue
    .line 1120637
    iput-boolean p1, p0, LX/6ft;->g:Z

    .line 1120638
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/6ft;
    .locals 0

    .prologue
    .line 1120635
    iput-object p1, p0, LX/6ft;->h:Ljava/lang/String;

    .line 1120636
    return-object p0
.end method

.method public final i()Lcom/facebook/messaging/model/threads/ThreadEventReminder;
    .locals 1

    .prologue
    .line 1120634
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/threads/ThreadEventReminder;-><init>(LX/6ft;)V

    return-object v0
.end method
