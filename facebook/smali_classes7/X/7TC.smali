.class public LX/7TC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/2MV;

.field private final d:LX/61g;

.field private final e:LX/7TK;

.field private final f:LX/61h;

.field private final g:LX/0Sh;

.field private final h:LX/03V;

.field private final i:LX/0Uh;

.field public j:LX/1Er;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1210182
    const-class v0, LX/7TC;

    sput-object v0, LX/7TC;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/2MV;LX/61g;LX/61h;LX/7TK;LX/0Sh;LX/0Uh;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1210172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1210173
    iput-object p1, p0, LX/7TC;->b:Landroid/content/Context;

    .line 1210174
    iput-object p2, p0, LX/7TC;->c:LX/2MV;

    .line 1210175
    iput-object p3, p0, LX/7TC;->d:LX/61g;

    .line 1210176
    iput-object p4, p0, LX/7TC;->f:LX/61h;

    .line 1210177
    iput-object p5, p0, LX/7TC;->e:LX/7TK;

    .line 1210178
    iput-object p6, p0, LX/7TC;->g:LX/0Sh;

    .line 1210179
    iput-object p8, p0, LX/7TC;->h:LX/03V;

    .line 1210180
    iput-object p7, p0, LX/7TC;->i:LX/0Uh;

    .line 1210181
    return-void
.end method

.method private static a(LX/7TC;LX/60y;)LX/60y;
    .locals 1

    .prologue
    .line 1210171
    new-instance v0, LX/7TA;

    invoke-direct {v0, p0, p1}, LX/7TA;-><init>(LX/7TC;LX/60y;)V

    return-object v0
.end method

.method private static a(LX/7TC;Ljava/io/File;)V
    .locals 2

    .prologue
    .line 1210169
    iget-object v0, p0, LX/7TC;->f:LX/61h;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/61h;->a(Ljava/lang/String;)V

    .line 1210170
    return-void
.end method

.method private static a(LX/7TC;Ljava/io/File;Landroid/media/MediaExtractor;LX/7TL;LX/60x;LX/61f;LX/61f;ZJJLX/60y;LX/7TE;ZIZ)V
    .locals 24
    .param p5    # LX/61f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1210057
    const/4 v15, 0x0

    .line 1210058
    :try_start_0
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-ge v4, v5, :cond_2

    .line 1210059
    if-eqz p5, :cond_0

    .line 1210060
    invoke-interface/range {p3 .. p3}, LX/7TL;->d()Landroid/media/MediaFormat;

    move-result-object v4

    .line 1210061
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7TC;->f:LX/61h;

    invoke-interface {v5, v4}, LX/61h;->b(Landroid/media/MediaFormat;)V

    .line 1210062
    const/4 v4, 0x1

    move-object/from16 v0, p13

    iput-boolean v4, v0, LX/7TE;->d:Z

    .line 1210063
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->f:LX/61h;

    invoke-interface/range {p3 .. p3}, LX/7TL;->e()I

    move-result v5

    invoke-interface {v4, v5}, LX/61h;->a(I)V

    .line 1210064
    :cond_0
    if-eqz p6, :cond_1

    .line 1210065
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->f:LX/61h;

    move-object/from16 v0, p6

    iget-object v5, v0, LX/61f;->b:Landroid/media/MediaFormat;

    invoke-interface {v4, v5}, LX/61h;->a(Landroid/media/MediaFormat;)V

    .line 1210066
    const/4 v4, 0x1

    move-object/from16 v0, p13

    iput-boolean v4, v0, LX/7TE;->c:Z

    .line 1210067
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->f:LX/61h;

    invoke-interface {v4}, LX/61h;->a()V

    .line 1210068
    const/4 v15, 0x1

    .line 1210069
    :cond_2
    const/4 v9, 0x0

    .line 1210070
    const/4 v7, 0x0

    .line 1210071
    const/4 v4, 0x0

    .line 1210072
    const-wide/16 v10, 0x0

    cmp-long v5, p8, v10

    if-gez v5, :cond_3

    .line 1210073
    const-wide/16 p8, 0x0

    .line 1210074
    :cond_3
    const-wide/16 v10, 0x0

    cmp-long v5, p10, v10

    if-gez v5, :cond_1d

    .line 1210075
    move-object/from16 v0, p4

    iget-wide v10, v0, LX/60x;->a:J

    const-wide/16 v12, 0x3e8

    mul-long/2addr v12, v10

    .line 1210076
    :goto_0
    move-wide/from16 v0, p8

    move-object/from16 v2, p13

    iput-wide v0, v2, LX/7TE;->h:J

    .line 1210077
    move-object/from16 v0, p13

    iput-wide v12, v0, LX/7TE;->i:J

    .line 1210078
    if-eqz p5, :cond_4

    .line 1210079
    move-object/from16 v0, p5

    iget v5, v0, LX/61f;->c:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/media/MediaExtractor;->selectTrack(I)V

    .line 1210080
    :cond_4
    if-eqz p6, :cond_5

    .line 1210081
    move-object/from16 v0, p6

    iget v5, v0, LX/61f;->c:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/media/MediaExtractor;->selectTrack(I)V

    .line 1210082
    :cond_5
    if-eqz p7, :cond_6

    .line 1210083
    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13, v5}, Landroid/media/MediaExtractor;->seekTo(JI)V

    .line 1210084
    invoke-virtual/range {p2 .. p2}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v10

    .line 1210085
    const-wide/16 v12, 0x1

    sub-long v12, v10, v12

    .line 1210086
    :cond_6
    const/4 v5, 0x0

    move-object/from16 v0, p2

    move-wide/from16 v1, p8

    invoke-virtual {v0, v1, v2, v5}, Landroid/media/MediaExtractor;->seekTo(JI)V

    .line 1210087
    invoke-virtual/range {p2 .. p2}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v16

    .line 1210088
    if-eqz p6, :cond_7

    .line 1210089
    move-object/from16 v0, p6

    iget v5, v0, LX/61f;->c:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/media/MediaExtractor;->unselectTrack(I)V

    .line 1210090
    :cond_7
    move-wide/from16 v0, v16

    move-object/from16 v2, p13

    iput-wide v0, v2, LX/7TE;->k:J

    .line 1210091
    move-object/from16 v0, p13

    iput-wide v12, v0, LX/7TE;->j:J

    .line 1210092
    if-eqz p5, :cond_15

    .line 1210093
    const/4 v8, 0x1

    .line 1210094
    const-wide/16 v10, 0x0

    move-object/from16 v0, p13

    iput-wide v10, v0, LX/7TE;->p:J

    .line 1210095
    sub-long v20, v12, v16

    move v11, v8

    move/from16 v18, v4

    move v14, v9

    move v4, v7

    .line 1210096
    :goto_1
    if-eqz v14, :cond_8

    if-eqz v4, :cond_8

    if-nez v18, :cond_14

    .line 1210097
    :cond_8
    if-nez v14, :cond_1c

    .line 1210098
    const-wide/16 v6, 0x2710

    move-object/from16 v0, p3

    invoke-interface {v0, v6, v7}, LX/7TL;->a(J)LX/614;

    move-result-object v5

    .line 1210099
    if-eqz v5, :cond_1c

    .line 1210100
    invoke-virtual {v5}, LX/614;->a()Ljava/nio/ByteBuffer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, Landroid/media/MediaExtractor;->readSampleData(Ljava/nio/ByteBuffer;I)I

    move-result v7

    .line 1210101
    invoke-virtual/range {p2 .. p2}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v22

    .line 1210102
    if-lez v7, :cond_b

    cmp-long v6, v22, v12

    if-gtz v6, :cond_b

    .line 1210103
    sub-long v8, v22, v16

    .line 1210104
    invoke-virtual/range {p2 .. p2}, Landroid/media/MediaExtractor;->getSampleFlags()I

    move-result v10

    .line 1210105
    const/4 v6, 0x0

    invoke-virtual/range {v5 .. v10}, LX/614;->a(IIJI)V

    .line 1210106
    move-object/from16 v0, p3

    invoke-interface {v0, v5}, LX/7TL;->a(LX/614;)V

    .line 1210107
    invoke-virtual/range {p2 .. p2}, Landroid/media/MediaExtractor;->advance()Z

    .line 1210108
    if-eqz v11, :cond_1b

    .line 1210109
    move-wide/from16 v0, v22

    move-object/from16 v2, p13

    iput-wide v0, v2, LX/7TE;->l:J

    .line 1210110
    const/4 v5, 0x0

    .line 1210111
    :goto_2
    move-wide/from16 v0, v22

    move-object/from16 v2, p13

    iput-wide v0, v2, LX/7TE;->m:J

    move v8, v5

    move v9, v14

    .line 1210112
    :goto_3
    if-nez v4, :cond_1a

    .line 1210113
    const-wide/16 v4, 0x2710

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, LX/7TL;->b(J)V

    .line 1210114
    invoke-interface/range {p3 .. p3}, LX/7TL;->a()Z

    move-result v4

    move v7, v4

    .line 1210115
    :goto_4
    if-nez v18, :cond_19

    .line 1210116
    const-wide/16 v4, 0x2710

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, LX/7TL;->c(J)LX/614;

    move-result-object v4

    move-object v6, v4

    .line 1210117
    :goto_5
    if-eqz v6, :cond_13

    .line 1210118
    invoke-virtual {v6}, LX/614;->d()Z

    move-result v4

    if-nez v4, :cond_c

    invoke-virtual {v6}, LX/614;->e()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1210119
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->f:LX/61h;

    invoke-interface/range {p3 .. p3}, LX/7TL;->d()Landroid/media/MediaFormat;

    move-result-object v5

    invoke-interface {v4, v5}, LX/61h;->b(Landroid/media/MediaFormat;)V

    .line 1210120
    const/4 v4, 0x1

    move-object/from16 v0, p13

    iput-boolean v4, v0, LX/7TE;->d:Z

    .line 1210121
    if-eqz p6, :cond_9

    .line 1210122
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->f:LX/61h;

    move-object/from16 v0, p6

    iget-object v5, v0, LX/61f;->b:Landroid/media/MediaFormat;

    invoke-interface {v4, v5}, LX/61h;->a(Landroid/media/MediaFormat;)V

    .line 1210123
    const/4 v4, 0x1

    move-object/from16 v0, p13

    iput-boolean v4, v0, LX/7TE;->c:Z

    .line 1210124
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->f:LX/61h;

    invoke-interface/range {p3 .. p3}, LX/7TL;->e()I

    move-result v5

    invoke-interface {v4, v5}, LX/61h;->a(I)V

    .line 1210125
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->f:LX/61h;

    invoke-interface {v4}, LX/61h;->a()V

    .line 1210126
    const/4 v15, 0x1

    .line 1210127
    :cond_a
    :goto_6
    move-object/from16 v0, p3

    invoke-interface {v0, v6}, LX/7TL;->b(LX/614;)V

    .line 1210128
    const-wide/16 v4, 0x2710

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, LX/7TL;->c(J)LX/614;

    move-result-object v4

    move-object v6, v4

    goto :goto_5

    .line 1210129
    :cond_b
    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x4

    invoke-virtual/range {v5 .. v10}, LX/614;->a(IIJI)V

    .line 1210130
    move-object/from16 v0, p3

    invoke-interface {v0, v5}, LX/7TL;->a(LX/614;)V

    .line 1210131
    const/4 v5, 0x1

    move v8, v11

    move v9, v5

    goto :goto_3

    .line 1210132
    :cond_c
    invoke-virtual {v6}, LX/614;->d()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1210133
    invoke-virtual {v6}, LX/614;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v4

    iget v4, v4, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_d

    .line 1210134
    const/4 v4, 0x1

    move v11, v8

    move/from16 v18, v4

    move v14, v9

    move v4, v7

    .line 1210135
    goto/16 :goto_1

    .line 1210136
    :cond_d
    const/4 v4, 0x1

    move-object/from16 v0, p13

    iput-boolean v4, v0, LX/7TE;->f:Z

    .line 1210137
    if-nez v15, :cond_f

    if-eqz p16, :cond_f

    .line 1210138
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->f:LX/61h;

    invoke-interface/range {p3 .. p3}, LX/7TL;->d()Landroid/media/MediaFormat;

    move-result-object v5

    invoke-interface {v4, v5}, LX/61h;->b(Landroid/media/MediaFormat;)V

    .line 1210139
    if-eqz p6, :cond_e

    .line 1210140
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->f:LX/61h;

    move-object/from16 v0, p6

    iget-object v5, v0, LX/61f;->b:Landroid/media/MediaFormat;

    invoke-interface {v4, v5}, LX/61h;->a(Landroid/media/MediaFormat;)V

    .line 1210141
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->f:LX/61h;

    invoke-interface/range {p3 .. p3}, LX/7TL;->e()I

    move-result v5

    invoke-interface {v4, v5}, LX/61h;->a(I)V

    .line 1210142
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->f:LX/61h;

    invoke-interface {v4}, LX/61h;->a()V

    .line 1210143
    const/4 v15, 0x1

    .line 1210144
    const/4 v4, 0x1

    move-object/from16 v0, p13

    iput-boolean v4, v0, LX/7TE;->g:Z

    .line 1210145
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->f:LX/61h;

    invoke-interface {v4, v6}, LX/61h;->b(LX/60z;)V

    .line 1210146
    move-object/from16 v0, p13

    iget-wide v4, v0, LX/7TE;->p:J

    const-wide/16 v10, 0x1

    add-long/2addr v4, v10

    move-object/from16 v0, p13

    iput-wide v4, v0, LX/7TE;->p:J

    .line 1210147
    if-eqz p12, :cond_a

    .line 1210148
    invoke-virtual {v6}, LX/614;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v4

    iget-wide v10, v4, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    .line 1210149
    if-eqz p14, :cond_12

    const-wide v4, 0x3feccccccccccccdL    # 0.9

    .line 1210150
    :goto_7
    long-to-double v10, v10

    mul-double/2addr v4, v10

    move-wide/from16 v0, v20

    long-to-double v10, v0

    div-double/2addr v4, v10

    move-object/from16 v0, p12

    invoke-interface {v0, v4, v5}, LX/60y;->a(D)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_6

    .line 1210151
    :catch_0
    move-exception v4

    .line 1210152
    :try_start_1
    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p13

    iput-object v5, v0, LX/7TE;->r:Ljava/lang/String;

    .line 1210153
    invoke-virtual {v4}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    if-eqz v5, :cond_10

    .line 1210154
    invoke-virtual {v4}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p13

    iput-object v5, v0, LX/7TE;->s:Ljava/lang/String;

    .line 1210155
    :cond_10
    invoke-static {v4}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p13

    iput-object v5, v0, LX/7TE;->t:Ljava/lang/String;

    .line 1210156
    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1210157
    :catchall_0
    move-exception v4

    if-eqz v15, :cond_11

    .line 1210158
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7TC;->f:LX/61h;

    invoke-interface {v5}, LX/61h;->b()V

    :cond_11
    throw v4

    .line 1210159
    :cond_12
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    goto :goto_7

    :cond_13
    move v11, v8

    move v4, v7

    move v14, v9

    .line 1210160
    goto/16 :goto_1

    .line 1210161
    :cond_14
    :try_start_2
    invoke-interface/range {p3 .. p3}, LX/7TL;->b()V

    .line 1210162
    :cond_15
    if-eqz p6, :cond_16

    .line 1210163
    if-eqz p14, :cond_18

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move/from16 v7, p15

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-wide/from16 v10, v16

    move-object/from16 v14, p13

    move/from16 v16, p16

    move-object/from16 v17, p12

    .line 1210164
    invoke-direct/range {v4 .. v17}, LX/7TC;->a(Ljava/io/File;Landroid/media/MediaExtractor;ILX/61f;LX/61f;JJLX/7TE;ZZLX/60y;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v15

    .line 1210165
    :cond_16
    :goto_8
    if-eqz v15, :cond_17

    .line 1210166
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->f:LX/61h;

    invoke-interface {v4}, LX/61h;->b()V

    .line 1210167
    :cond_17
    return-void

    :cond_18
    move-object/from16 v8, p0

    move-object/from16 v9, p2

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v14, p13

    move/from16 v18, p16

    .line 1210168
    :try_start_3
    invoke-direct/range {v8 .. v18}, LX/7TC;->a(Landroid/media/MediaExtractor;LX/61f;LX/61f;JLX/7TE;ZJZ)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v15

    goto :goto_8

    :cond_19
    move v11, v8

    move v4, v7

    move v14, v9

    goto/16 :goto_1

    :cond_1a
    move v7, v4

    goto/16 :goto_4

    :cond_1b
    move v5, v11

    goto/16 :goto_2

    :cond_1c
    move v8, v11

    move v9, v14

    goto/16 :goto_3

    :cond_1d
    move-wide/from16 v12, p10

    goto/16 :goto_0
.end method

.method private static a(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1210183
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1210184
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1210185
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 1210186
    invoke-virtual {p1, p0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 1210187
    return-void
.end method

.method private static a()Z
    .locals 2

    .prologue
    .line 1210056
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/7TC;LX/61f;JLX/7TE;ZJLandroid/media/MediaExtractor;Z)Z
    .locals 12

    .prologue
    .line 1210020
    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    iput-wide v2, v0, LX/7TE;->q:J

    .line 1210021
    new-instance v3, LX/7TB;

    invoke-direct {v3}, LX/7TB;-><init>()V

    .line 1210022
    iget-object v2, p1, LX/61f;->b:Landroid/media/MediaFormat;

    const-string v4, "csd-0"

    invoke-virtual {v2, v4}, Landroid/media/MediaFormat;->getByteBuffer(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1210023
    if-nez p5, :cond_0

    .line 1210024
    iget-object v4, p0, LX/7TC;->f:LX/61h;

    iget-object v5, p1, LX/61f;->b:Landroid/media/MediaFormat;

    invoke-interface {v4, v5}, LX/61h;->a(Landroid/media/MediaFormat;)V

    .line 1210025
    const/4 v4, 0x1

    move-object/from16 v0, p4

    iput-boolean v4, v0, LX/7TE;->c:Z

    .line 1210026
    iget-object v4, p0, LX/7TC;->f:LX/61h;

    invoke-interface {v4}, LX/61h;->a()V

    .line 1210027
    const/16 p5, 0x1

    .line 1210028
    :cond_0
    if-eqz v2, :cond_1

    .line 1210029
    const/4 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v5

    const-wide/16 v6, 0x0

    const/4 v8, 0x2

    invoke-interface/range {v3 .. v8}, LX/60z;->a(IIJI)V

    .line 1210030
    invoke-interface {v3}, LX/60z;->a()Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-static {v2, v4}, LX/7TC;->a(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 1210031
    iget-object v2, p0, LX/7TC;->f:LX/61h;

    invoke-interface {v2, v3}, LX/61h;->a(LX/60z;)V

    .line 1210032
    :cond_1
    const/4 v4, 0x0

    .line 1210033
    const/4 v2, 0x1

    move v9, v4

    .line 1210034
    :goto_0
    if-nez v9, :cond_6

    .line 1210035
    invoke-interface {v3}, LX/60z;->a()Ljava/nio/ByteBuffer;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p8

    invoke-virtual {v0, v4, v5}, Landroid/media/MediaExtractor;->readSampleData(Ljava/nio/ByteBuffer;I)I

    move-result v5

    .line 1210036
    invoke-virtual/range {p8 .. p8}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v10

    .line 1210037
    if-lez v5, :cond_5

    cmp-long v4, v10, p2

    if-gtz v4, :cond_5

    .line 1210038
    cmp-long v4, v10, p6

    if-ltz v4, :cond_4

    .line 1210039
    sub-long v6, v10, p6

    .line 1210040
    invoke-virtual/range {p8 .. p8}, Landroid/media/MediaExtractor;->getSampleFlags()I

    move-result v8

    .line 1210041
    const/4 v4, 0x0

    invoke-interface/range {v3 .. v8}, LX/60z;->a(IIJI)V

    .line 1210042
    if-eqz v2, :cond_2

    .line 1210043
    const/4 v2, 0x0

    .line 1210044
    move-object/from16 v0, p4

    iput-wide v10, v0, LX/7TE;->n:J

    .line 1210045
    const/4 v4, 0x1

    move-object/from16 v0, p4

    iput-boolean v4, v0, LX/7TE;->e:Z

    .line 1210046
    :cond_2
    move-object/from16 v0, p4

    iput-wide v10, v0, LX/7TE;->o:J

    .line 1210047
    if-nez p5, :cond_3

    if-eqz p9, :cond_3

    .line 1210048
    iget-object v4, p0, LX/7TC;->f:LX/61h;

    iget-object v5, p1, LX/61f;->b:Landroid/media/MediaFormat;

    invoke-interface {v4, v5}, LX/61h;->a(Landroid/media/MediaFormat;)V

    .line 1210049
    const/4 v4, 0x1

    move-object/from16 v0, p4

    iput-boolean v4, v0, LX/7TE;->g:Z

    .line 1210050
    :cond_3
    iget-object v4, p0, LX/7TC;->f:LX/61h;

    invoke-interface {v4, v3}, LX/61h;->a(LX/60z;)V

    .line 1210051
    move-object/from16 v0, p4

    iget-wide v4, v0, LX/7TE;->q:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    move-object/from16 v0, p4

    iput-wide v4, v0, LX/7TE;->q:J

    .line 1210052
    :cond_4
    invoke-virtual/range {p8 .. p8}, Landroid/media/MediaExtractor;->advance()Z

    goto :goto_0

    .line 1210053
    :cond_5
    const/4 v4, 0x1

    move v9, v4

    .line 1210054
    goto :goto_0

    .line 1210055
    :cond_6
    return p5
.end method

.method private a(Landroid/media/MediaExtractor;LX/61f;LX/61f;JLX/7TE;ZJZ)Z
    .locals 12

    .prologue
    .line 1210014
    iget v2, p3, LX/61f;->c:I

    invoke-virtual {p1, v2}, Landroid/media/MediaExtractor;->selectTrack(I)V

    .line 1210015
    const/4 v2, 0x2

    move-wide/from16 v0, p8

    invoke-virtual {p1, v0, v1, v2}, Landroid/media/MediaExtractor;->seekTo(JI)V

    .line 1210016
    if-eqz p2, :cond_0

    .line 1210017
    iget v2, p2, LX/61f;->c:I

    invoke-virtual {p1, v2}, Landroid/media/MediaExtractor;->unselectTrack(I)V

    :cond_0
    move-object v2, p0

    move-object v3, p3

    move-wide/from16 v4, p4

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-wide/from16 v8, p8

    move-object v10, p1

    move/from16 v11, p10

    .line 1210018
    invoke-static/range {v2 .. v11}, LX/7TC;->a(LX/7TC;LX/61f;JLX/7TE;ZJLandroid/media/MediaExtractor;Z)Z

    move-result v2

    .line 1210019
    return v2
.end method

.method private a(Ljava/io/File;Landroid/media/MediaExtractor;ILX/61f;LX/61f;JJLX/7TE;ZZLX/60y;)Z
    .locals 12

    .prologue
    .line 1209993
    new-instance v10, Landroid/media/MediaExtractor;

    invoke-direct {v10}, Landroid/media/MediaExtractor;-><init>()V

    .line 1209994
    :try_start_0
    iget-object v3, p0, LX/7TC;->j:LX/1Er;

    const-string v4, "audio_stream-"

    const-string v5, ".aac"

    iget-object v2, p0, LX/7TC;->i:LX/0Uh;

    const/16 v6, 0x268

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    :goto_0
    invoke-virtual {v3, v4, v5, v2}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v2

    .line 1209995
    new-instance v3, LX/7T4;

    invoke-direct {v3, p1, v2}, LX/7T4;-><init>(Ljava/io/File;Ljava/io/File;)V

    move-wide/from16 v0, p6

    invoke-virtual {v3, v0, v1}, LX/7T4;->a(J)LX/7T4;

    move-result-object v3

    move-wide/from16 v0, p8

    invoke-virtual {v3, v0, v1}, LX/7T4;->b(J)LX/7T4;

    move-result-object v3

    invoke-virtual {v3, p3}, LX/7T4;->a(I)LX/7T4;

    move-result-object v3

    invoke-virtual {v3}, LX/7T4;->a()LX/7T5;

    move-result-object v3

    .line 1209996
    move-object/from16 v0, p13

    invoke-static {p0, v0}, LX/7TC;->a(LX/7TC;LX/60y;)LX/60y;

    move-result-object v4

    .line 1209997
    invoke-virtual {v3, v4}, LX/7T5;->a(LX/60y;)V

    .line 1209998
    move-object/from16 v0, p5

    iget v4, v0, LX/61f;->c:I

    invoke-virtual {p2, v4}, Landroid/media/MediaExtractor;->selectTrack(I)V

    .line 1209999
    if-eqz p4, :cond_0

    .line 1210000
    move-object/from16 v0, p4

    iget v4, v0, LX/61f;->c:I

    invoke-virtual {p2, v4}, Landroid/media/MediaExtractor;->unselectTrack(I)V

    .line 1210001
    :cond_0
    move-object/from16 v0, p5

    iget v4, v0, LX/61f;->c:I

    invoke-virtual {v3, p2, v4}, LX/7T5;->a(Landroid/media/MediaExtractor;I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1210002
    sget-object v2, LX/7TC;->a:Ljava/lang/Class;

    const-string v3, "Failed transcoding audio stream. File %s, startTime:%sms, EndTime:%sms"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1210003
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Failed to transcode audio stream."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1210004
    :catch_0
    move-exception v2

    .line 1210005
    :try_start_1
    const-string v3, "VideoResizeOperation"

    const-string v4, "Failed transcoding/muxing audio stream."

    invoke-static {v3, v4, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1210006
    invoke-virtual {v10}, Landroid/media/MediaExtractor;->release()V

    .line 1210007
    :goto_1
    return p11

    .line 1210008
    :cond_1
    :try_start_2
    sget-object v2, LX/46h;->PREFER_SDCARD:LX/46h;

    goto :goto_0

    .line 1210009
    :cond_2
    iget-object v3, p0, LX/7TC;->b:Landroid/content/Context;

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v10, v3, v2, v4}, Landroid/media/MediaExtractor;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    .line 1210010
    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Landroid/media/MediaExtractor;->selectTrack(I)V

    .line 1210011
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v10, v2, v3, v4}, Landroid/media/MediaExtractor;->seekTo(JI)V

    .line 1210012
    sub-long v4, p8, p6

    const-wide/16 v8, 0x0

    move-object v2, p0

    move-object/from16 v3, p5

    move-object/from16 v6, p10

    move/from16 v7, p11

    move/from16 v11, p12

    invoke-static/range {v2 .. v11}, LX/7TC;->a(LX/7TC;LX/61f;JLX/7TE;ZJLandroid/media/MediaExtractor;Z)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result p11

    .line 1210013
    invoke-virtual {v10}, Landroid/media/MediaExtractor;->release()V

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-virtual {v10}, Landroid/media/MediaExtractor;->release()V

    throw v2
.end method


# virtual methods
.method public final a(LX/7TH;)LX/7TD;
    .locals 25

    .prologue
    .line 1209931
    const-string v2, "resizeVideoOnCurrentThread"

    const v3, 0xc01924b

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1209932
    new-instance v17, LX/7TE;

    invoke-direct/range {v17 .. v17}, LX/7TE;-><init>()V

    .line 1209933
    const/16 v21, 0x0

    .line 1209934
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/7TC;->g:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->c()Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1209935
    invoke-static {}, LX/7TC;->a()Z

    move-result v2

    const-string v3, "Video Resizing is not supported for this OS version"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1209936
    move-object/from16 v0, p1

    iget-object v2, v0, LX/7TH;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input file does not exist: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v4, v0, LX/7TH;->a:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1209937
    move-object/from16 v0, p1

    iget-object v2, v0, LX/7TH;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v22

    .line 1209938
    move-object/from16 v0, p1

    iget-object v2, v0, LX/7TH;->a:Ljava/io/File;

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1209939
    move-object/from16 v0, p0

    iget-object v3, v0, LX/7TC;->c:LX/2MV;

    invoke-interface {v3, v2}, LX/2MV;->a(Landroid/net/Uri;)LX/60x;

    move-result-object v3

    .line 1209940
    new-instance v24, Landroid/media/MediaExtractor;

    invoke-direct/range {v24 .. v24}, Landroid/media/MediaExtractor;-><init>()V

    .line 1209941
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->b:Landroid/content/Context;

    const/4 v5, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v2, v5}, Landroid/media/MediaExtractor;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    .line 1209942
    const/4 v9, 0x0

    .line 1209943
    move-object/from16 v0, p1

    iget-boolean v4, v0, LX/7TH;->g:Z

    if-nez v4, :cond_0

    .line 1209944
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->d:LX/61g;

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, LX/61g;->a(Landroid/media/MediaExtractor;)LX/61f;

    move-result-object v9

    .line 1209945
    :cond_0
    const/4 v10, 0x0

    .line 1209946
    move-object/from16 v0, p1

    iget-boolean v4, v0, LX/7TH;->f:Z

    if-nez v4, :cond_1

    .line 1209947
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->d:LX/61g;

    move-object/from16 v0, v24

    invoke-virtual {v4, v0, v2}, LX/61g;->a(Landroid/media/MediaExtractor;Landroid/net/Uri;)LX/61f;

    move-result-object v10

    .line 1209948
    :cond_1
    if-eqz v10, :cond_7

    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, v17

    iput-boolean v2, v0, LX/7TE;->a:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1209949
    :try_start_1
    move-object/from16 v0, p1

    iget-object v2, v0, LX/7TH;->c:LX/2Md;

    move-object/from16 v0, p1

    iget v4, v0, LX/7TH;->k:I

    move-object/from16 v0, p1

    iget-object v5, v0, LX/7TH;->d:Landroid/graphics/RectF;

    move-object/from16 v0, p1

    iget-object v6, v0, LX/7TH;->e:LX/7Sv;

    move-object/from16 v0, p1

    iget-object v7, v0, LX/7TH;->m:LX/7Sy;

    move-object/from16 v0, p1

    iget-object v8, v0, LX/7TH;->n:Ljava/util/List;

    invoke-virtual/range {v2 .. v8}, LX/2Md;->a(LX/60x;ILandroid/graphics/RectF;LX/7Sv;LX/7Sy;Ljava/util/List;)LX/7Sx;

    move-result-object v2

    .line 1209950
    const/4 v7, 0x0

    .line 1209951
    move-object/from16 v0, p1

    iget-boolean v4, v0, LX/7TH;->g:Z

    if-nez v4, :cond_2

    .line 1209952
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7TC;->e:LX/7TK;

    invoke-virtual {v4}, LX/7TK;->a()LX/7TL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    .line 1209953
    :cond_2
    if-eqz v7, :cond_3

    .line 1209954
    :try_start_2
    invoke-interface {v7, v2}, LX/7TL;->a(LX/7Sx;)V

    .line 1209955
    iget-object v4, v9, LX/61f;->b:Landroid/media/MediaFormat;

    invoke-interface {v7, v4}, LX/7TL;->a(Landroid/media/MediaFormat;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    .line 1209956
    :cond_3
    :try_start_3
    move-object/from16 v0, p1

    iget-object v4, v0, LX/7TH;->b:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/7TC;->a(LX/7TC;Ljava/io/File;)V

    .line 1209957
    const/4 v4, 0x1

    move-object/from16 v0, v17

    iput-boolean v4, v0, LX/7TE;->b:Z

    .line 1209958
    iget v0, v2, LX/7Sx;->m:I

    move/from16 v19, v0

    .line 1209959
    iget-boolean v4, v2, LX/7Sx;->k:Z

    if-eqz v4, :cond_9

    if-lez v19, :cond_9

    iget v4, v3, LX/60x;->g:I

    move/from16 v0, v19

    if-le v4, v0, :cond_9

    move-object/from16 v0, p1

    iget-object v4, v0, LX/7TH;->a:Ljava/io/File;

    invoke-static {v4}, LX/7T5;->a(Ljava/io/File;)Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v18, 0x1

    .line 1209960
    :goto_2
    iget-boolean v0, v2, LX/7Sx;->l:Z

    move/from16 v20, v0

    .line 1209961
    move-object/from16 v0, p1

    iget-object v5, v0, LX/7TH;->a:Ljava/io/File;

    move-object/from16 v0, p1

    iget-boolean v11, v0, LX/7TH;->h:Z

    move-object/from16 v0, p1

    iget v4, v0, LX/7TH;->i:I

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v12, v4

    move-object/from16 v0, p1

    iget v4, v0, LX/7TH;->j:I

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v14, v4

    move-object/from16 v0, p1

    iget-object v0, v0, LX/7TH;->l:LX/60y;

    move-object/from16 v16, v0

    move-object/from16 v4, p0

    move-object/from16 v6, v24

    move-object v8, v3

    invoke-static/range {v4 .. v20}, LX/7TC;->a(LX/7TC;Ljava/io/File;Landroid/media/MediaExtractor;LX/7TL;LX/60x;LX/61f;LX/61f;ZJJLX/60y;LX/7TE;ZIZ)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_5

    .line 1209962
    if-eqz v7, :cond_4

    .line 1209963
    :try_start_4
    invoke-interface {v7}, LX/7TL;->c()V

    .line 1209964
    :cond_4
    move-object/from16 v0, p1

    iget-object v4, v0, LX/7TH;->b:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_a

    .line 1209965
    new-instance v2, LX/7T9;

    const-string v3, "No output file created"

    move-object/from16 v0, v17

    invoke-direct {v2, v3, v0}, LX/7T9;-><init>(Ljava/lang/String;LX/7TE;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1209966
    :catchall_0
    move-exception v2

    :goto_3
    :try_start_5
    invoke-virtual/range {v24 .. v24}, Landroid/media/MediaExtractor;->release()V

    throw v2
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1209967
    :catch_0
    move-exception v2

    .line 1209968
    :try_start_6
    move-object/from16 v0, p0

    iget-object v3, v0, LX/7TC;->h:LX/03V;

    const-string v4, "VideoResizeOperation_Exception"

    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1209969
    move-object/from16 v0, p1

    iget-object v3, v0, LX/7TH;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 1209970
    const-class v3, LX/7T9;

    invoke-static {v2, v3}, LX/1Bz;->propagateIfInstanceOf(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 1209971
    new-instance v3, LX/7T9;

    const-string v4, "Failed to resize video"

    move/from16 v0, v21

    move-object/from16 v1, v17

    invoke-direct {v3, v4, v2, v0, v1}, LX/7T9;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZLX/7TE;)V

    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1209972
    :catchall_1
    move-exception v2

    if-nez v21, :cond_5

    :try_start_7
    move-object/from16 v0, p1

    iget-object v3, v0, LX/7TH;->l:LX/60y;

    if-eqz v3, :cond_5

    .line 1209973
    move-object/from16 v0, p1

    iget-object v3, v0, LX/7TH;->l:LX/60y;

    invoke-interface {v3}, LX/60y;->a()V

    :cond_5
    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1209974
    :catchall_2
    move-exception v2

    const v3, -0x3f763d97

    invoke-static {v3}, LX/02m;->a(I)V

    .line 1209975
    const-string v3, "VideoResizer Thread"

    invoke-static {v3}, LX/0PR;->c(Ljava/lang/String;)V

    throw v2

    .line 1209976
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1209977
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1209978
    :catch_1
    move-exception v2

    .line 1209979
    const/4 v3, 0x1

    .line 1209980
    :try_start_8
    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 1209981
    :catchall_3
    move-exception v2

    :goto_4
    if-eqz v7, :cond_8

    .line 1209982
    :try_start_9
    invoke-interface {v7}, LX/7TL;->c()V

    :cond_8
    throw v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 1209983
    :catchall_4
    move-exception v2

    move/from16 v21, v3

    goto :goto_3

    .line 1209984
    :cond_9
    const/16 v18, 0x0

    goto/16 :goto_2

    .line 1209985
    :cond_a
    :try_start_a
    move-object/from16 v0, p1

    iget-object v4, v0, LX/7TH;->b:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v8

    .line 1209986
    move-object/from16 v0, p1

    iget-object v5, v0, LX/7TH;->b:Ljava/io/File;

    iget v10, v3, LX/60x;->b:I

    iget v11, v3, LX/60x;->c:I

    iget v12, v3, LX/60x;->e:I

    const/4 v13, -0x1

    iget-wide v14, v3, LX/60x;->a:J

    move-wide/from16 v6, v22

    move-object/from16 v16, v2

    invoke-static/range {v5 .. v17}, LX/7TD;->a(Ljava/io/File;JJIIIIJLX/7Sx;LX/7TE;)LX/7TD;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-object v2

    .line 1209987
    :try_start_b
    invoke-virtual/range {v24 .. v24}, Landroid/media/MediaExtractor;->release()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 1209988
    :try_start_c
    move-object/from16 v0, p1

    iget-object v3, v0, LX/7TH;->l:LX/60y;

    if-eqz v3, :cond_b

    .line 1209989
    move-object/from16 v0, p1

    iget-object v3, v0, LX/7TH;->l:LX/60y;

    invoke-interface {v3}, LX/60y;->a()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 1209990
    :cond_b
    const v3, -0x3e10fb31

    invoke-static {v3}, LX/02m;->a(I)V

    .line 1209991
    const-string v3, "VideoResizer Thread"

    invoke-static {v3}, LX/0PR;->c(Ljava/lang/String;)V

    return-object v2

    .line 1209992
    :catchall_5
    move-exception v2

    move/from16 v3, v21

    goto :goto_4
.end method
