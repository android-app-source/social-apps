.class public LX/80O;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/0sa;

.field private final c:LX/0rq;

.field private final d:LX/0w9;

.field public final e:LX/0SG;

.field private final f:LX/0sU;


# direct methods
.method public constructor <init>(LX/0sa;LX/0rq;LX/0w9;LX/0sO;LX/0SG;LX/0sU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1282828
    invoke-direct {p0, p4}, LX/0ro;-><init>(LX/0sO;)V

    .line 1282829
    iput-object p1, p0, LX/80O;->b:LX/0sa;

    .line 1282830
    iput-object p2, p0, LX/80O;->c:LX/0rq;

    .line 1282831
    iput-object p3, p0, LX/80O;->d:LX/0w9;

    .line 1282832
    iput-object p5, p0, LX/80O;->e:LX/0SG;

    .line 1282833
    iput-object p6, p0, LX/80O;->f:LX/0sU;

    .line 1282834
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;)V
    .locals 2

    .prologue
    .line 1282835
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    if-nez v0, :cond_1

    .line 1282836
    :cond_0
    :goto_0
    return-void

    .line 1282837
    :cond_1
    sget-object v0, LX/80N;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1282838
    const/4 v0, 0x0

    :goto_1
    move-object v0, v0

    .line 1282839
    if-eqz v0, :cond_0

    .line 1282840
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    .line 1282841
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v1

    .line 1282842
    iput-object v0, v1, LX/0x2;->j:Ljava/lang/String;

    .line 1282843
    goto :goto_0

    .line 1282844
    :pswitch_0
    const-string v0, "feed_chaining"

    goto :goto_1

    .line 1282845
    :pswitch_1
    const-string v0, "comment_chaining"

    goto :goto_1

    .line 1282846
    :pswitch_2
    const-string v0, "outbound_click_chaining"

    goto :goto_1

    .line 1282847
    :pswitch_3
    const-string v0, "share_chaining"

    goto :goto_1

    .line 1282848
    :pswitch_4
    const-string v0, "timeline_like_chaining"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1282849
    check-cast p1, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;

    .line 1282850
    const-class v0, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 1282851
    if-eqz v0, :cond_1

    .line 1282852
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->d:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    move-object v1, v1

    .line 1282853
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->do()Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    move-result-object v0

    const/4 v3, 0x0

    .line 1282854
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;->a()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_2

    .line 1282855
    :cond_0
    :goto_0
    move-object v0, v3

    .line 1282856
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1282857
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v4, v2

    :goto_2
    if-ge v4, v6, :cond_0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;

    .line 1282858
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->j()LX/0Px;

    move-result-object v7

    if-eqz v7, :cond_c

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v7

    if-eqz v7, :cond_c

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v7

    invoke-interface {v7}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    if-eqz v7, :cond_c

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->j()LX/0Px;

    move-result-object v7

    invoke-virtual {v7, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 1282859
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 1282860
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    invoke-interface {v4}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    .line 1282861
    const v5, -0x533f585d

    if-ne v4, v5, :cond_4

    .line 1282862
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    .line 1282863
    invoke-static {v2, v1}, LX/80O;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;)V

    move-object v3, v2

    .line 1282864
    :cond_3
    :goto_3
    iget-object v2, p0, LX/80O;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, LX/16t;->a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V

    .line 1282865
    instance-of v2, v3, LX/17w;

    if-eqz v2, :cond_0

    move-object v2, v3

    .line 1282866
    check-cast v2, LX/17w;

    const/4 v4, 0x1

    .line 1282867
    invoke-static {v2}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v5

    .line 1282868
    iput-boolean v4, v5, LX/0x2;->m:Z

    .line 1282869
    goto :goto_0

    .line 1282870
    :cond_4
    const v5, -0x56fb7e27

    if-ne v4, v5, :cond_5

    .line 1282871
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    move-object v3, v2

    goto :goto_3

    .line 1282872
    :cond_5
    const v5, 0x542975e

    if-ne v4, v5, :cond_6

    .line 1282873
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;

    move-object v3, v2

    goto :goto_3

    .line 1282874
    :cond_6
    const v5, 0x6a3d0f4d

    if-ne v4, v5, :cond_7

    .line 1282875
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStorySet;

    move-object v3, v2

    goto :goto_3

    .line 1282876
    :cond_7
    const v5, -0x4ed3e2e6

    if-ne v4, v5, :cond_8

    .line 1282877
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;

    move-object v3, v2

    goto :goto_3

    .line 1282878
    :cond_8
    const v5, -0x4064a59f

    if-ne v4, v5, :cond_9

    .line 1282879
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;

    move-object v3, v2

    goto :goto_3

    .line 1282880
    :cond_9
    const v5, -0x44c718b7

    if-ne v4, v5, :cond_a

    .line 1282881
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;

    move-object v3, v2

    goto :goto_3

    .line 1282882
    :cond_a
    const v5, 0x3bf6f008

    if-ne v4, v5, :cond_b

    .line 1282883
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;

    move-object v3, v2

    goto :goto_3

    .line 1282884
    :cond_b
    const v5, -0x7782fd44

    if-ne v4, v5, :cond_3

    .line 1282885
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsEdge;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;

    move-object v3, v2

    goto :goto_3

    .line 1282886
    :cond_c
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_2
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1282887
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 5

    .prologue
    .line 1282888
    check-cast p1, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;

    .line 1282889
    new-instance v0, LX/80L;

    invoke-direct {v0}, LX/80L;-><init>()V

    move-object v0, v0

    .line 1282890
    const/4 v1, 0x1

    .line 1282891
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 1282892
    invoke-static {v0}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 1282893
    iget-object v1, p0, LX/80O;->d:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 1282894
    const-string v1, "node_id"

    .line 1282895
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1282896
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "followup_action_type"

    .line 1282897
    iget-object v3, p1, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->d:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    move-object v3, v3

    .line 1282898
    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "followup_feed_unit_type"

    .line 1282899
    iget-object v3, p1, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->a:LX/0Px;

    new-instance v4, LX/55I;

    invoke-direct {v4, p1}, LX/55I;-><init>(Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;)V

    invoke-static {v3, v4}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v3

    move-object v3, v3

    .line 1282900
    invoke-virtual {v1, v2, v3}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_pic_swipe_size_param"

    iget-object v3, p0, LX/80O;->b:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->m()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "pymk_size_param"

    iget-object v3, p0, LX/80O;->b:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->f()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "ad_media_type"

    iget-object v3, p0, LX/80O;->c:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->a()LX/0wF;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "num_related_stories"

    .line 1282901
    iget v3, p1, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->e:I

    move v3, v3

    .line 1282902
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "contextual_recommendations_profile_image_size"

    iget-object v3, p0, LX/80O;->b:LX/0sa;

    .line 1282903
    iget-object v4, v3, LX/0sa;->e:Landroid/content/res/Resources;

    const p1, 0x7f0b0967

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1282904
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v3, v4

    .line 1282905
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1282906
    iget-object v1, p0, LX/80O;->f:LX/0sU;

    invoke-virtual {v1, v0}, LX/0sU;->a(LX/0gW;)V

    .line 1282907
    return-object v0
.end method

.method public final i(Ljava/lang/Object;)Lcom/facebook/http/interfaces/RequestPriority;
    .locals 1

    .prologue
    .line 1282908
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    return-object v0
.end method
