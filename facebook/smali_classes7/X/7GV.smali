.class public LX/7GV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/7GV;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1189226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189227
    return-void
.end method

.method public static a([B)LX/7GU;
    .locals 3

    .prologue
    .line 1189228
    new-instance v0, LX/1sp;

    invoke-direct {v0}, LX/1sp;-><init>()V

    .line 1189229
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1189230
    new-instance v2, LX/1sr;

    invoke-direct {v2, v1}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v0, v2}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    .line 1189231
    invoke-static {v0}, LX/3ll;->b(LX/1su;)LX/3ll;

    move-result-object v0

    .line 1189232
    array-length v2, p0

    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v1

    sub-int v1, v2, v1

    .line 1189233
    new-instance v2, LX/7GU;

    invoke-direct {v2, v0, v1}, LX/7GU;-><init>(LX/3ll;I)V

    return-object v2
.end method

.method public static a(LX/0QB;)LX/7GV;
    .locals 3

    .prologue
    .line 1189234
    sget-object v0, LX/7GV;->a:LX/7GV;

    if-nez v0, :cond_1

    .line 1189235
    const-class v1, LX/7GV;

    monitor-enter v1

    .line 1189236
    :try_start_0
    sget-object v0, LX/7GV;->a:LX/7GV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1189237
    if-eqz v2, :cond_0

    .line 1189238
    :try_start_1
    new-instance v0, LX/7GV;

    invoke-direct {v0}, LX/7GV;-><init>()V

    .line 1189239
    move-object v0, v0

    .line 1189240
    sput-object v0, LX/7GV;->a:LX/7GV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1189241
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1189242
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1189243
    :cond_1
    sget-object v0, LX/7GV;->a:LX/7GV;

    return-object v0

    .line 1189244
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1189245
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
