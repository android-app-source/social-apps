.class public final LX/6gQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1125119
    const/4 v10, 0x0

    .line 1125120
    const/4 v9, 0x0

    .line 1125121
    const/4 v8, 0x0

    .line 1125122
    const/4 v7, 0x0

    .line 1125123
    const/4 v6, 0x0

    .line 1125124
    const/4 v5, 0x0

    .line 1125125
    const/4 v4, 0x0

    .line 1125126
    const/4 v3, 0x0

    .line 1125127
    const/4 v2, 0x0

    .line 1125128
    const/4 v1, 0x0

    .line 1125129
    const/4 v0, 0x0

    .line 1125130
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 1125131
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125132
    const/4 v0, 0x0

    .line 1125133
    :goto_0
    return v0

    .line 1125134
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125135
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_d

    .line 1125136
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1125137
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1125138
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1125139
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1125140
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 1125141
    :cond_3
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1125142
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1125143
    :cond_4
    const-string v12, "image_assets"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1125144
    invoke-static {p0, p1}, LX/6gN;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1125145
    :cond_5
    const-string v12, "mask_effect"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1125146
    invoke-static {p0, p1}, LX/6gb;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1125147
    :cond_6
    const-string v12, "particle_effect"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1125148
    invoke-static {p0, p1}, LX/6gz;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1125149
    :cond_7
    const-string v12, "style_transfer_effect"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1125150
    invoke-static {p0, p1}, LX/6h1;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1125151
    :cond_8
    const-string v12, "text_assets"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1125152
    invoke-static {p0, p1}, LX/6gV;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1125153
    :cond_9
    const-string v12, "thumbnail"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1125154
    invoke-static {p0, p1}, LX/6gO;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1125155
    :cond_a
    const-string v12, "thumbnail_image_assets"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 1125156
    invoke-static {p0, p1}, LX/6gN;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1125157
    :cond_b
    const-string v12, "thumbnail_text_assets"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 1125158
    invoke-static {p0, p1}, LX/6gV;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 1125159
    :cond_c
    const-string v12, "unique_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1125160
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 1125161
    :cond_d
    const/16 v11, 0xb

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1125162
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 1125163
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1125164
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1125165
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1125166
    const/4 v7, 0x4

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 1125167
    const/4 v6, 0x5

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 1125168
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 1125169
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 1125170
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1125171
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 1125172
    const/16 v1, 0xa

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1125173
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1125072
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1125073
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1125074
    if-eqz v0, :cond_0

    .line 1125075
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125076
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1125077
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1125078
    if-eqz v0, :cond_1

    .line 1125079
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125080
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1125081
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125082
    if-eqz v0, :cond_2

    .line 1125083
    const-string v1, "image_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125084
    invoke-static {p0, v0, p2, p3}, LX/6gN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1125085
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125086
    if-eqz v0, :cond_3

    .line 1125087
    const-string v1, "mask_effect"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125088
    invoke-static {p0, v0, p2, p3}, LX/6gb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1125089
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125090
    if-eqz v0, :cond_4

    .line 1125091
    const-string v1, "particle_effect"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125092
    invoke-static {p0, v0, p2, p3}, LX/6gz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1125093
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125094
    if-eqz v0, :cond_5

    .line 1125095
    const-string v1, "style_transfer_effect"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125096
    invoke-static {p0, v0, p2, p3}, LX/6h1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1125097
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125098
    if-eqz v0, :cond_6

    .line 1125099
    const-string v1, "text_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125100
    invoke-static {p0, v0, p2, p3}, LX/6gV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1125101
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125102
    if-eqz v0, :cond_7

    .line 1125103
    const-string v1, "thumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125104
    invoke-static {p0, v0, p2}, LX/6gO;->a(LX/15i;ILX/0nX;)V

    .line 1125105
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125106
    if-eqz v0, :cond_8

    .line 1125107
    const-string v1, "thumbnail_image_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125108
    invoke-static {p0, v0, p2, p3}, LX/6gN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1125109
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125110
    if-eqz v0, :cond_9

    .line 1125111
    const-string v1, "thumbnail_text_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125112
    invoke-static {p0, v0, p2, p3}, LX/6gV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1125113
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1125114
    if-eqz v0, :cond_a

    .line 1125115
    const-string v1, "unique_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125116
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1125117
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1125118
    return-void
.end method
