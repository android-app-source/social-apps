.class public final LX/8JU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/tagging/shared/TagTypeahead;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V
    .locals 0

    .prologue
    .line 1328511
    iput-object p1, p0, LX/8JU;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;B)V
    .locals 0

    .prologue
    .line 1328512
    invoke-direct {p0, p1}, LX/8JU;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1328513
    if-gez p3, :cond_0

    .line 1328514
    :goto_0
    return-void

    .line 1328515
    :cond_0
    iget-object v0, p0, LX/8JU;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    invoke-virtual {v0, p3}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1328516
    const/4 v1, -0x1

    .line 1328517
    iget-object v2, p0, LX/8JU;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-boolean v2, v2, Lcom/facebook/photos/tagging/shared/TagTypeahead;->x:Z

    if-eqz v2, :cond_1

    .line 1328518
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "User selected position in tag suggestions="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1328519
    iget-object v1, p0, LX/8JU;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v1, v1, Lcom/facebook/photos/tagging/shared/TagTypeahead;->r:LX/8JT;

    iget-object v2, p0, LX/8JU;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v2, v2, Lcom/facebook/photos/tagging/shared/TagTypeahead;->v:Ljava/lang/String;

    invoke-interface {v1, v0, p3, v2}, LX/8JT;->a(Lcom/facebook/tagging/model/TaggingProfile;ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    move p3, v1

    goto :goto_1
.end method
