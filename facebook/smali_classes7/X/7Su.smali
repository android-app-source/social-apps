.class public LX/7Su;
.super LX/2Md;
.source ""


# instance fields
.field private a:LX/2Mg;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1209374
    invoke-direct {p0}, LX/2Md;-><init>()V

    .line 1209375
    sget-object v0, LX/2Mg;->h:LX/2Mg;

    iput-object v0, p0, LX/7Su;->a:LX/2Mg;

    .line 1209376
    return-void
.end method


# virtual methods
.method public final a()LX/2Mg;
    .locals 1

    .prologue
    .line 1209377
    iget-object v0, p0, LX/7Su;->a:LX/2Mg;

    return-object v0
.end method

.method public final a(II)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v7, 0x0

    .line 1209378
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1209379
    if-lez p2, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1209380
    new-instance v0, LX/2Mg;

    const/high16 v3, 0x41f00000    # 30.0f

    const/16 v4, 0xa

    move v1, p2

    move v2, p1

    move v6, v5

    invoke-direct/range {v0 .. v7}, LX/2Mg;-><init>(IIFIIIZ)V

    iput-object v0, p0, LX/7Su;->a:LX/2Mg;

    .line 1209381
    return-void

    :cond_0
    move v0, v7

    .line 1209382
    goto :goto_0

    :cond_1
    move v1, v7

    .line 1209383
    goto :goto_1
.end method
