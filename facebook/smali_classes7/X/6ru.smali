.class public LX/6ru;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6rs;


# direct methods
.method public constructor <init>(LX/6rs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1152760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152761
    iput-object p1, p0, LX/6ru;->a:LX/6rs;

    .line 1152762
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1152763
    const/4 v2, 0x0

    .line 1152764
    const-string v0, "label"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152765
    const-string v0, "label"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 1152766
    const-string v0, "price_list"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1152767
    iget-object v0, p0, LX/6ru;->a:LX/6rs;

    invoke-virtual {v0, p1}, LX/6rs;->i(Ljava/lang/String;)LX/6rr;

    move-result-object v0

    const-string v1, "price_list"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    move-object v1, v0

    .line 1152768
    :goto_0
    const-string v0, "currency_amount"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1152769
    iget-object v0, p0, LX/6ru;->a:LX/6rs;

    .line 1152770
    iget-object v2, v0, LX/6rs;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6rr;

    move-object v0, v2

    .line 1152771
    const-string v2, "currency_amount"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-interface {v0, p1, v2}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1152772
    :goto_1
    const-string v2, "user_facing_reason"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 1152773
    if-eqz v1, :cond_0

    .line 1152774
    invoke-static {v3, v1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Ljava/lang/String;LX/0Px;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object v0

    .line 1152775
    :goto_2
    return-object v0

    .line 1152776
    :cond_0
    if-eqz v0, :cond_1

    .line 1152777
    invoke-static {v3, v0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object v0

    goto :goto_2

    .line 1152778
    :cond_1
    if-eqz v2, :cond_2

    .line 1152779
    const/4 v1, 0x0

    .line 1152780
    new-instance v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    invoke-direct {v0, v3, v1, v1, v2}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;-><init>(Ljava/lang/String;LX/0Px;Lcom/facebook/payments/currency/CurrencyAmount;Ljava/lang/String;)V

    move-object v0, v0

    .line 1152781
    goto :goto_2

    .line 1152782
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid price price provided: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v2

    goto :goto_1

    :cond_4
    move-object v1, v2

    goto :goto_0
.end method
