.class public LX/7Be;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/lang/String;

.field public static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;

.field private static final h:Ljava/lang/String;


# instance fields
.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Bc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7CU;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7CV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1179291
    const-class v0, LX/7Be;

    sput-object v0, LX/7Be;->a:Ljava/lang/Class;

    .line 1179292
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/7Br;->l:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Be;->b:Ljava/lang/String;

    .line 1179293
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, LX/7Br;->b:LX/0U1;

    .line 1179294
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1179295
    aput-object v1, v0, v3

    sget-object v1, LX/7Br;->c:LX/0U1;

    .line 1179296
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1179297
    aput-object v1, v0, v4

    sget-object v1, LX/7Br;->d:LX/0U1;

    .line 1179298
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1179299
    aput-object v1, v0, v5

    sget-object v1, LX/7Br;->e:LX/0U1;

    .line 1179300
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1179301
    aput-object v1, v0, v6

    sget-object v1, LX/7Br;->f:LX/0U1;

    .line 1179302
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1179303
    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7Br;->g:LX/0U1;

    .line 1179304
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 1179305
    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7Br;->h:LX/0U1;

    .line 1179306
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 1179307
    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7Br;->i:LX/0U1;

    .line 1179308
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 1179309
    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7Br;->j:LX/0U1;

    .line 1179310
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 1179311
    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7Br;->k:LX/0U1;

    .line 1179312
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 1179313
    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7Br;->l:LX/0U1;

    .line 1179314
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 1179315
    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7Br;->m:LX/0U1;

    .line 1179316
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 1179317
    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7Br;->n:LX/0U1;

    .line 1179318
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 1179319
    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/7Br;->o:LX/0U1;

    .line 1179320
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 1179321
    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/7Br;->p:LX/0U1;

    .line 1179322
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 1179323
    aput-object v2, v0, v1

    sput-object v0, LX/7Be;->c:[Ljava/lang/String;

    .line 1179324
    new-array v0, v4, [Ljava/lang/String;

    sget-object v1, LX/7Br;->b:LX/0U1;

    .line 1179325
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1179326
    aput-object v1, v0, v3

    sput-object v0, LX/7Be;->d:[Ljava/lang/String;

    .line 1179327
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/7Bv;->g:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Be;->e:Ljava/lang/String;

    .line 1179328
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, LX/7Bv;->b:LX/0U1;

    .line 1179329
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1179330
    aput-object v1, v0, v3

    sget-object v1, LX/7Bv;->c:LX/0U1;

    .line 1179331
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1179332
    aput-object v1, v0, v4

    sget-object v1, LX/7Bv;->d:LX/0U1;

    .line 1179333
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1179334
    aput-object v1, v0, v5

    sget-object v1, LX/7Bv;->e:LX/0U1;

    .line 1179335
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1179336
    aput-object v1, v0, v6

    sget-object v1, LX/7Bv;->f:LX/0U1;

    .line 1179337
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1179338
    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7Bv;->g:LX/0U1;

    .line 1179339
    iget-object v5, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v5

    .line 1179340
    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7Bv;->h:LX/0U1;

    .line 1179341
    iget-object v5, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v5

    .line 1179342
    aput-object v2, v0, v1

    sput-object v0, LX/7Be;->f:[Ljava/lang/String;

    .line 1179343
    new-array v0, v4, [Ljava/lang/String;

    sget-object v1, LX/7Bv;->b:LX/0U1;

    .line 1179344
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1179345
    aput-object v1, v0, v3

    sput-object v0, LX/7Be;->g:[Ljava/lang/String;

    .line 1179346
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT COUNT(*) FROM entities WHERE "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/7Br;->a:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IN ( SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/7Bn;->a:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM entities_data"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/7Bn;->b:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " GLOB \'*\' )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Be;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1179348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1179349
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1179350
    iput-object v0, p0, LX/7Be;->i:LX/0Ot;

    .line 1179351
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1179352
    iput-object v0, p0, LX/7Be;->j:LX/0Ot;

    .line 1179353
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1179354
    iput-object v0, p0, LX/7Be;->k:LX/0Ot;

    .line 1179355
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1179356
    iput-object v0, p0, LX/7Be;->l:LX/0Ot;

    .line 1179357
    return-void
.end method

.method public static a(LX/7Be;Landroid/database/sqlite/SQLiteQueryBuilder;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 1179347
    iget-object v0, p0, LX/7Be;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Bc;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    move-object v0, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    move-object v6, v4

    move-object v7, p4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Px;LX/0U1;Ljava/lang/String;LX/0U1;LX/0U1;)Ljava/lang/String;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0U1;",
            "Ljava/lang/String;",
            "LX/0U1;",
            "LX/0U1;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1179273
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1179274
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v9, v1, -0x1

    .line 1179275
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v10

    move v7, v6

    move v8, v6

    :goto_0
    if-ge v7, v10, :cond_2

    invoke-virtual {p0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1179276
    if-lez v8, :cond_0

    .line 1179277
    const-string v2, " INTERSECT "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1179278
    :cond_0
    if-ne v8, v9, :cond_1

    const/4 v5, 0x1

    :goto_1
    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 1179279
    invoke-static/range {v0 .. v5}, LX/7Be;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;LX/0U1;LX/0U1;Z)V

    .line 1179280
    add-int/lit8 v2, v8, 0x1

    .line 1179281
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v8, v2

    goto :goto_0

    :cond_1
    move v5, v6

    .line 1179282
    goto :goto_1

    .line 1179283
    :cond_2
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1179284
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;LX/0U1;LX/0U1;Z)V
    .locals 2

    .prologue
    .line 1179285
    const-string v0, "SELECT "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1179286
    if-eqz p5, :cond_0

    .line 1179287
    const-string v0, " BETWEEN X\'"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' AND X\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "FF"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1179288
    :goto_0
    const-string v0, "\'"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1179289
    return-void

    .line 1179290
    :cond_0
    const-string v0, " = X\'"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/7Be;
    .locals 5

    .prologue
    .line 1179269
    new-instance v0, LX/7Be;

    invoke-direct {v0}, LX/7Be;-><init>()V

    .line 1179270
    const/16 v1, 0x32b9

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x3518

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x3519

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x1032

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    .line 1179271
    iput-object v1, v0, LX/7Be;->i:LX/0Ot;

    iput-object v2, v0, LX/7Be;->j:LX/0Ot;

    iput-object v3, v0, LX/7Be;->k:LX/0Ot;

    iput-object v4, v0, LX/7Be;->l:LX/0Ot;

    .line 1179272
    return-object v0
.end method


# virtual methods
.method public final a(LX/0Px;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 1179256
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1179257
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0xc8

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, " entities"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/7Br;->b:LX/0U1;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " IN ( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-static {v2, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1179258
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1179259
    :try_start_0
    iget-object v1, p0, LX/7Be;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Bc;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/7Be;->d:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 1179260
    :try_start_1
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 1179261
    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1179262
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1179263
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 1179264
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 1179265
    :cond_1
    :try_start_2
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 1179266
    if-eqz v1, :cond_2

    .line 1179267
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    return-object v0

    .line 1179268
    :catchall_1
    move-exception v0

    move-object v1, v8

    goto :goto_1
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 1179251
    iget-object v0, p0, LX/7Be;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Bc;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, LX/7Be;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    .line 1179252
    :try_start_0
    iget-object v0, p0, LX/7Be;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Bc;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v2, LX/7Be;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    const v2, 0x1e3e6681

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J

    const v0, 0x18c1b2ce

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1179253
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1179254
    return-void

    .line 1179255
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v0
.end method

.method public final b(LX/0Px;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 1179236
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1179237
    const-string v1, "keywords"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1179238
    :try_start_0
    sget-object v1, LX/7Bv;->b:LX/0U1;

    .line 1179239
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1179240
    invoke-static {v1, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v4

    .line 1179241
    iget-object v1, p0, LX/7Be;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Bc;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/7Be;->g:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 1179242
    :try_start_1
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 1179243
    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1179244
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1179245
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 1179246
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 1179247
    :cond_1
    :try_start_2
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 1179248
    if-eqz v1, :cond_2

    .line 1179249
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    return-object v0

    .line 1179250
    :catchall_1
    move-exception v0

    move-object v1, v8

    goto :goto_1
.end method
