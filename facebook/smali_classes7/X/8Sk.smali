.class public final LX/8Sk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/QuicksilverFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V
    .locals 0

    .prologue
    .line 1347314
    iput-object p1, p0, LX/8Sk;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/8Vb;LX/8TX;I)V
    .locals 3

    .prologue
    .line 1347315
    iget-object v0, p0, LX/8Sk;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    iget-object v1, p2, LX/8TX;->effect:LX/8TJ;

    invoke-virtual {v0, p1, v1}, LX/8TS;->a(LX/8Vb;LX/8TJ;)V

    .line 1347316
    iget-object v0, p0, LX/8Sk;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    sget-object v1, LX/8TV;->IN_GAME:LX/8TV;

    invoke-virtual {v0, v1}, LX/8TS;->a(LX/8TV;)V

    .line 1347317
    iget-object v0, p0, LX/8Sk;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    invoke-virtual {v0, p2, p3}, LX/8TD;->a(LX/8TX;I)V

    .line 1347318
    iget-object v0, p0, LX/8Sk;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-boolean v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->p:Z

    if-eqz v0, :cond_0

    .line 1347319
    iget-object v0, p0, LX/8Sk;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->c:LX/8W7;

    sget-object v1, LX/8W5;->RESTART:LX/8W5;

    iget-object v2, p0, LX/8Sk;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-static {v2}, Lcom/facebook/quicksilver/QuicksilverFragment;->n(Lcom/facebook/quicksilver/QuicksilverFragment;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/8W7;->a(LX/8W5;Ljava/lang/Object;)V

    .line 1347320
    :goto_0
    iget-object v0, p0, LX/8Sk;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->invalidateOptionsMenu()V

    .line 1347321
    iget-object v0, p0, LX/8Sk;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    .line 1347322
    invoke-static {v0}, Lcom/facebook/quicksilver/QuicksilverFragment;->k$redex0(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    .line 1347323
    return-void

    .line 1347324
    :cond_0
    iget-object v0, p0, LX/8Sk;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->c:LX/8W7;

    sget-object v1, LX/8W5;->GAME_START:LX/8W5;

    iget-object v2, p0, LX/8Sk;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-static {v2}, Lcom/facebook/quicksilver/QuicksilverFragment;->n(Lcom/facebook/quicksilver/QuicksilverFragment;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/8W7;->a(LX/8W5;Ljava/lang/Object;)V

    .line 1347325
    iget-object v0, p0, LX/8Sk;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    const/4 v1, 0x1

    .line 1347326
    iput-boolean v1, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->p:Z

    .line 1347327
    goto :goto_0
.end method
