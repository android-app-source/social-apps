.class public final enum LX/7yt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7yt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7yt;

.field public static final enum DEFAULT:LX/7yt;

.field public static final enum FACEBOOK:LX/7yt;

.field public static final enum FLASH:LX/7yt;

.field public static final enum GROUPS:LX/7yt;

.field public static final enum INSTAGRAM:LX/7yt;

.field public static final enum MESSENGER:LX/7yt;

.field public static final enum MESSENGER_ANIMATED_IMAGE:LX/7yt;

.field public static final enum MESSENGER_AUDIO:LX/7yt;

.field public static final enum MESSENGER_FILE:LX/7yt;

.field public static final enum MESSENGER_IMAGE:LX/7yt;

.field public static final enum MESSENGER_VIDEO:LX/7yt;


# instance fields
.field private final mJsonResponseFieldType:LX/7yv;

.field private final mUriPathElement:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1280410
    new-instance v0, LX/7yt;

    const-string v1, "DEFAULT"

    const-string v2, "up"

    sget-object v3, LX/7yv;->MEDIA_ID:LX/7yv;

    invoke-direct {v0, v1, v5, v2, v3}, LX/7yt;-><init>(Ljava/lang/String;ILjava/lang/String;LX/7yv;)V

    sput-object v0, LX/7yt;->DEFAULT:LX/7yt;

    .line 1280411
    new-instance v0, LX/7yt;

    const-string v1, "MESSENGER"

    const-string v2, "up"

    sget-object v3, LX/7yv;->MEDIA_ID:LX/7yv;

    invoke-direct {v0, v1, v6, v2, v3}, LX/7yt;-><init>(Ljava/lang/String;ILjava/lang/String;LX/7yv;)V

    sput-object v0, LX/7yt;->MESSENGER:LX/7yt;

    .line 1280412
    new-instance v0, LX/7yt;

    const-string v1, "MESSENGER_IMAGE"

    const-string v2, "messenger_image"

    sget-object v3, LX/7yv;->MEDIA_ID:LX/7yv;

    invoke-direct {v0, v1, v7, v2, v3}, LX/7yt;-><init>(Ljava/lang/String;ILjava/lang/String;LX/7yv;)V

    sput-object v0, LX/7yt;->MESSENGER_IMAGE:LX/7yt;

    .line 1280413
    new-instance v0, LX/7yt;

    const-string v1, "MESSENGER_ANIMATED_IMAGE"

    const-string v2, "messenger_gif"

    sget-object v3, LX/7yv;->MEDIA_ID:LX/7yv;

    invoke-direct {v0, v1, v8, v2, v3}, LX/7yt;-><init>(Ljava/lang/String;ILjava/lang/String;LX/7yv;)V

    sput-object v0, LX/7yt;->MESSENGER_ANIMATED_IMAGE:LX/7yt;

    .line 1280414
    new-instance v0, LX/7yt;

    const-string v1, "MESSENGER_VIDEO"

    const-string v2, "messenger_video"

    sget-object v3, LX/7yv;->MEDIA_ID:LX/7yv;

    invoke-direct {v0, v1, v9, v2, v3}, LX/7yt;-><init>(Ljava/lang/String;ILjava/lang/String;LX/7yv;)V

    sput-object v0, LX/7yt;->MESSENGER_VIDEO:LX/7yt;

    .line 1280415
    new-instance v0, LX/7yt;

    const-string v1, "MESSENGER_AUDIO"

    const/4 v2, 0x5

    const-string v3, "messenger_audio"

    sget-object v4, LX/7yv;->MEDIA_ID:LX/7yv;

    invoke-direct {v0, v1, v2, v3, v4}, LX/7yt;-><init>(Ljava/lang/String;ILjava/lang/String;LX/7yv;)V

    sput-object v0, LX/7yt;->MESSENGER_AUDIO:LX/7yt;

    .line 1280416
    new-instance v0, LX/7yt;

    const-string v1, "MESSENGER_FILE"

    const/4 v2, 0x6

    const-string v3, "messenger_file"

    sget-object v4, LX/7yv;->MEDIA_ID:LX/7yv;

    invoke-direct {v0, v1, v2, v3, v4}, LX/7yt;-><init>(Ljava/lang/String;ILjava/lang/String;LX/7yv;)V

    sput-object v0, LX/7yt;->MESSENGER_FILE:LX/7yt;

    .line 1280417
    new-instance v0, LX/7yt;

    const-string v1, "FACEBOOK"

    const/4 v2, 0x7

    const-string v3, "fb_video"

    sget-object v4, LX/7yv;->HANDLE:LX/7yv;

    invoke-direct {v0, v1, v2, v3, v4}, LX/7yt;-><init>(Ljava/lang/String;ILjava/lang/String;LX/7yv;)V

    sput-object v0, LX/7yt;->FACEBOOK:LX/7yt;

    .line 1280418
    new-instance v0, LX/7yt;

    const-string v1, "INSTAGRAM"

    const/16 v2, 0x8

    const-string v3, "ig"

    sget-object v4, LX/7yv;->HANDLE:LX/7yv;

    invoke-direct {v0, v1, v2, v3, v4}, LX/7yt;-><init>(Ljava/lang/String;ILjava/lang/String;LX/7yv;)V

    sput-object v0, LX/7yt;->INSTAGRAM:LX/7yt;

    .line 1280419
    new-instance v0, LX/7yt;

    const-string v1, "GROUPS"

    const/16 v2, 0x9

    const-string v3, "groups"

    sget-object v4, LX/7yv;->HANDLE:LX/7yv;

    invoke-direct {v0, v1, v2, v3, v4}, LX/7yt;-><init>(Ljava/lang/String;ILjava/lang/String;LX/7yv;)V

    sput-object v0, LX/7yt;->GROUPS:LX/7yt;

    .line 1280420
    new-instance v0, LX/7yt;

    const-string v1, "FLASH"

    const/16 v2, 0xa

    const-string v3, "flash"

    sget-object v4, LX/7yv;->MEDIA_ID:LX/7yv;

    invoke-direct {v0, v1, v2, v3, v4}, LX/7yt;-><init>(Ljava/lang/String;ILjava/lang/String;LX/7yv;)V

    sput-object v0, LX/7yt;->FLASH:LX/7yt;

    .line 1280421
    const/16 v0, 0xb

    new-array v0, v0, [LX/7yt;

    sget-object v1, LX/7yt;->DEFAULT:LX/7yt;

    aput-object v1, v0, v5

    sget-object v1, LX/7yt;->MESSENGER:LX/7yt;

    aput-object v1, v0, v6

    sget-object v1, LX/7yt;->MESSENGER_IMAGE:LX/7yt;

    aput-object v1, v0, v7

    sget-object v1, LX/7yt;->MESSENGER_ANIMATED_IMAGE:LX/7yt;

    aput-object v1, v0, v8

    sget-object v1, LX/7yt;->MESSENGER_VIDEO:LX/7yt;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/7yt;->MESSENGER_AUDIO:LX/7yt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7yt;->MESSENGER_FILE:LX/7yt;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7yt;->FACEBOOK:LX/7yt;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7yt;->INSTAGRAM:LX/7yt;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7yt;->GROUPS:LX/7yt;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7yt;->FLASH:LX/7yt;

    aput-object v2, v0, v1

    sput-object v0, LX/7yt;->$VALUES:[LX/7yt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;LX/7yv;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/7yv;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1280422
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1280423
    iput-object p3, p0, LX/7yt;->mUriPathElement:Ljava/lang/String;

    .line 1280424
    iput-object p4, p0, LX/7yt;->mJsonResponseFieldType:LX/7yv;

    .line 1280425
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7yt;
    .locals 1

    .prologue
    .line 1280426
    const-class v0, LX/7yt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7yt;

    return-object v0
.end method

.method public static values()[LX/7yt;
    .locals 1

    .prologue
    .line 1280427
    sget-object v0, LX/7yt;->$VALUES:[LX/7yt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7yt;

    return-object v0
.end method


# virtual methods
.method public final getJsonResponseFieldType()LX/7yv;
    .locals 1

    .prologue
    .line 1280428
    iget-object v0, p0, LX/7yt;->mJsonResponseFieldType:LX/7yv;

    return-object v0
.end method

.method public final getUriPathElement()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1280429
    iget-object v0, p0, LX/7yt;->mUriPathElement:Ljava/lang/String;

    return-object v0
.end method
