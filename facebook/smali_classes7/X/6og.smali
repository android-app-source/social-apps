.class public LX/6og;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/6og;


# instance fields
.field private final a:LX/6oi;

.field private final b:LX/03V;


# direct methods
.method public constructor <init>(LX/6oi;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1148369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148370
    iput-object p1, p0, LX/6og;->a:LX/6oi;

    .line 1148371
    iput-object p2, p0, LX/6og;->b:LX/03V;

    .line 1148372
    return-void
.end method

.method public static a(LX/0QB;)LX/6og;
    .locals 5

    .prologue
    .line 1148373
    sget-object v0, LX/6og;->c:LX/6og;

    if-nez v0, :cond_1

    .line 1148374
    const-class v1, LX/6og;

    monitor-enter v1

    .line 1148375
    :try_start_0
    sget-object v0, LX/6og;->c:LX/6og;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1148376
    if-eqz v2, :cond_0

    .line 1148377
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1148378
    new-instance p0, LX/6og;

    invoke-static {v0}, LX/6oi;->a(LX/0QB;)LX/6oi;

    move-result-object v3

    check-cast v3, LX/6oi;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/6og;-><init>(LX/6oi;LX/03V;)V

    .line 1148379
    move-object v0, p0

    .line 1148380
    sput-object v0, LX/6og;->c:LX/6og;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1148381
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1148382
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1148383
    :cond_1
    sget-object v0, LX/6og;->c:LX/6og;

    return-object v0

    .line 1148384
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1148385
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 1148386
    const-string v0, "clearPinTable"

    const v1, -0x49cb2dcb

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1148387
    :try_start_0
    iget-object v0, p0, LX/6og;->a:LX/6oi;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1148388
    const v0, 0x7a676ae9

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1148389
    :try_start_1
    const-string v0, "payment_pin_id"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1148390
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1148391
    const v0, -0x32c568e0

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1148392
    const v0, -0x28c5abf0

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1148393
    return-void

    .line 1148394
    :catchall_0
    move-exception v0

    const v2, 0x668bee77

    :try_start_3
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1148395
    :catchall_1
    move-exception v0

    const v1, -0x77394f12

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/auth/pin/model/PaymentPin;)V
    .locals 5

    .prologue
    .line 1148396
    const-string v0, "insertOrReplacePaymentPin"

    const v1, -0x52c2e312

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1148397
    :try_start_0
    iget-object v0, p0, LX/6og;->a:LX/6oi;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1148398
    const v0, 0x32f01a8d

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1148399
    :try_start_1
    invoke-direct {p0}, LX/6og;->b()V

    .line 1148400
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1148401
    invoke-virtual {p1}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 1148402
    :goto_0
    sget-object v4, LX/6oj;->a:LX/0U1;

    .line 1148403
    iget-object p1, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, p1

    .line 1148404
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1148405
    const-string v0, "payment_pin_id"

    const/4 v1, 0x0

    const v4, -0x74c4ac6f

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v2, v0, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0x38a18e9e

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1148406
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1148407
    const v0, 0x63d820a4

    :try_start_2
    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1148408
    :goto_1
    const v0, 0x5566ba93

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1148409
    return-void

    .line 1148410
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 1148411
    :catch_0
    move-exception v0

    .line 1148412
    :try_start_3
    iget-object v1, p0, LX/6og;->b:LX/03V;

    const-string v3, "DbInsertPaymentPinHandler"

    const-string v4, "A SQLException occurred when trying to insert into the database"

    invoke-virtual {v1, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1148413
    const v0, -0x28b64b29

    :try_start_4
    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 1148414
    :catchall_0
    move-exception v0

    const v1, -0x44e9f419

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 1148415
    :catchall_1
    move-exception v0

    const v1, 0x408c967

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
