.class public final LX/7tF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 37

    .prologue
    .line 1267373
    const/16 v33, 0x0

    .line 1267374
    const/16 v32, 0x0

    .line 1267375
    const/16 v31, 0x0

    .line 1267376
    const/16 v30, 0x0

    .line 1267377
    const/16 v29, 0x0

    .line 1267378
    const/16 v28, 0x0

    .line 1267379
    const/16 v27, 0x0

    .line 1267380
    const/16 v26, 0x0

    .line 1267381
    const/16 v25, 0x0

    .line 1267382
    const/16 v24, 0x0

    .line 1267383
    const/16 v23, 0x0

    .line 1267384
    const/16 v22, 0x0

    .line 1267385
    const/16 v21, 0x0

    .line 1267386
    const/16 v20, 0x0

    .line 1267387
    const/16 v19, 0x0

    .line 1267388
    const/16 v18, 0x0

    .line 1267389
    const/16 v17, 0x0

    .line 1267390
    const/16 v16, 0x0

    .line 1267391
    const/4 v15, 0x0

    .line 1267392
    const/4 v14, 0x0

    .line 1267393
    const/4 v13, 0x0

    .line 1267394
    const/4 v12, 0x0

    .line 1267395
    const/4 v11, 0x0

    .line 1267396
    const/4 v10, 0x0

    .line 1267397
    const/4 v9, 0x0

    .line 1267398
    const/4 v8, 0x0

    .line 1267399
    const/4 v7, 0x0

    .line 1267400
    const/4 v6, 0x0

    .line 1267401
    const/4 v5, 0x0

    .line 1267402
    const/4 v4, 0x0

    .line 1267403
    const/4 v3, 0x0

    .line 1267404
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v34

    sget-object v35, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_1

    .line 1267405
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1267406
    const/4 v3, 0x0

    .line 1267407
    :goto_0
    return v3

    .line 1267408
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1267409
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v34

    sget-object v35, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_11

    .line 1267410
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v34

    .line 1267411
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1267412
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v35

    sget-object v36, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_1

    if-eqz v34, :cond_1

    .line 1267413
    const-string v35, "can_viewer_create_repeat_event"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_2

    .line 1267414
    const/16 v17, 0x1

    .line 1267415
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto :goto_1

    .line 1267416
    :cond_2
    const-string v35, "can_viewer_decline"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_3

    .line 1267417
    const/16 v16, 0x1

    .line 1267418
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v32

    goto :goto_1

    .line 1267419
    :cond_3
    const-string v35, "can_viewer_delete"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_4

    .line 1267420
    const/4 v15, 0x1

    .line 1267421
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto :goto_1

    .line 1267422
    :cond_4
    const-string v35, "can_viewer_edit"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_5

    .line 1267423
    const/4 v14, 0x1

    .line 1267424
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v30

    goto :goto_1

    .line 1267425
    :cond_5
    const-string v35, "can_viewer_edit_host"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_6

    .line 1267426
    const/4 v13, 0x1

    .line 1267427
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto :goto_1

    .line 1267428
    :cond_6
    const-string v35, "can_viewer_invite"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_7

    .line 1267429
    const/4 v12, 0x1

    .line 1267430
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto :goto_1

    .line 1267431
    :cond_7
    const-string v35, "can_viewer_join"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_8

    .line 1267432
    const/4 v11, 0x1

    .line 1267433
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto/16 :goto_1

    .line 1267434
    :cond_8
    const-string v35, "can_viewer_maybe"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_9

    .line 1267435
    const/4 v10, 0x1

    .line 1267436
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto/16 :goto_1

    .line 1267437
    :cond_9
    const-string v35, "can_viewer_promote_as_parent"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_a

    .line 1267438
    const/4 v9, 0x1

    .line 1267439
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v25

    goto/16 :goto_1

    .line 1267440
    :cond_a
    const-string v35, "can_viewer_remove_self"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_b

    .line 1267441
    const/4 v8, 0x1

    .line 1267442
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v24

    goto/16 :goto_1

    .line 1267443
    :cond_b
    const-string v35, "can_viewer_report"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_c

    .line 1267444
    const/4 v7, 0x1

    .line 1267445
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto/16 :goto_1

    .line 1267446
    :cond_c
    const-string v35, "can_viewer_save"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_d

    .line 1267447
    const/4 v6, 0x1

    .line 1267448
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto/16 :goto_1

    .line 1267449
    :cond_d
    const-string v35, "can_viewer_send_message_to_guests"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_e

    .line 1267450
    const/4 v5, 0x1

    .line 1267451
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto/16 :goto_1

    .line 1267452
    :cond_e
    const-string v35, "can_viewer_share"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_f

    .line 1267453
    const/4 v4, 0x1

    .line 1267454
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto/16 :goto_1

    .line 1267455
    :cond_f
    const-string v35, "is_viewer_admin"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_10

    .line 1267456
    const/4 v3, 0x1

    .line 1267457
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v19

    goto/16 :goto_1

    .line 1267458
    :cond_10
    const-string v35, "seen_event"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_0

    .line 1267459
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    goto/16 :goto_1

    .line 1267460
    :cond_11
    const/16 v34, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1267461
    if-eqz v17, :cond_12

    .line 1267462
    const/16 v17, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1267463
    :cond_12
    if-eqz v16, :cond_13

    .line 1267464
    const/16 v16, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1267465
    :cond_13
    if-eqz v15, :cond_14

    .line 1267466
    const/4 v15, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v15, v1}, LX/186;->a(IZ)V

    .line 1267467
    :cond_14
    if-eqz v14, :cond_15

    .line 1267468
    const/4 v14, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v14, v1}, LX/186;->a(IZ)V

    .line 1267469
    :cond_15
    if-eqz v13, :cond_16

    .line 1267470
    const/4 v13, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 1267471
    :cond_16
    if-eqz v12, :cond_17

    .line 1267472
    const/4 v12, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 1267473
    :cond_17
    if-eqz v11, :cond_18

    .line 1267474
    const/4 v11, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 1267475
    :cond_18
    if-eqz v10, :cond_19

    .line 1267476
    const/4 v10, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 1267477
    :cond_19
    if-eqz v9, :cond_1a

    .line 1267478
    const/16 v9, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 1267479
    :cond_1a
    if-eqz v8, :cond_1b

    .line 1267480
    const/16 v8, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 1267481
    :cond_1b
    if-eqz v7, :cond_1c

    .line 1267482
    const/16 v7, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1267483
    :cond_1c
    if-eqz v6, :cond_1d

    .line 1267484
    const/16 v6, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1267485
    :cond_1d
    if-eqz v5, :cond_1e

    .line 1267486
    const/16 v5, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1267487
    :cond_1e
    if-eqz v4, :cond_1f

    .line 1267488
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1267489
    :cond_1f
    if-eqz v3, :cond_20

    .line 1267490
    const/16 v3, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1267491
    :cond_20
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1267492
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/16 v2, 0xf

    .line 1267493
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1267494
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267495
    if-eqz v0, :cond_0

    .line 1267496
    const-string v1, "can_viewer_create_repeat_event"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267497
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267498
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267499
    if-eqz v0, :cond_1

    .line 1267500
    const-string v1, "can_viewer_decline"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267501
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267502
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267503
    if-eqz v0, :cond_2

    .line 1267504
    const-string v1, "can_viewer_delete"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267505
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267506
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267507
    if-eqz v0, :cond_3

    .line 1267508
    const-string v1, "can_viewer_edit"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267509
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267510
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267511
    if-eqz v0, :cond_4

    .line 1267512
    const-string v1, "can_viewer_edit_host"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267513
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267514
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267515
    if-eqz v0, :cond_5

    .line 1267516
    const-string v1, "can_viewer_invite"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267517
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267518
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267519
    if-eqz v0, :cond_6

    .line 1267520
    const-string v1, "can_viewer_join"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267521
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267522
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267523
    if-eqz v0, :cond_7

    .line 1267524
    const-string v1, "can_viewer_maybe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267525
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267526
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267527
    if-eqz v0, :cond_8

    .line 1267528
    const-string v1, "can_viewer_promote_as_parent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267529
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267530
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267531
    if-eqz v0, :cond_9

    .line 1267532
    const-string v1, "can_viewer_remove_self"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267533
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267534
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267535
    if-eqz v0, :cond_a

    .line 1267536
    const-string v1, "can_viewer_report"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267537
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267538
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267539
    if-eqz v0, :cond_b

    .line 1267540
    const-string v1, "can_viewer_save"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267541
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267542
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267543
    if-eqz v0, :cond_c

    .line 1267544
    const-string v1, "can_viewer_send_message_to_guests"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267545
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267546
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267547
    if-eqz v0, :cond_d

    .line 1267548
    const-string v1, "can_viewer_share"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267549
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267550
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1267551
    if-eqz v0, :cond_e

    .line 1267552
    const-string v1, "is_viewer_admin"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267553
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1267554
    :cond_e
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1267555
    if-eqz v0, :cond_f

    .line 1267556
    const-string v0, "seen_event"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1267557
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1267558
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1267559
    return-void
.end method
