.class public LX/6wA;
.super LX/6E7;
.source ""

# interfaces
.implements LX/6vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E7;",
        "LX/6vq",
        "<",
        "LX/6w8;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field public b:Lcom/facebook/fbui/glyph/GlyphView;

.field private c:LX/6w8;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1157444
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1157445
    const v0, 0x7f030363

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1157446
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/6wA;->setOrientation(I)V

    .line 1157447
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/6wA;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a00d5

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1157448
    invoke-virtual {p0}, LX/6wA;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b13ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1157449
    invoke-virtual {p0, v0, v0, v0, v0}, LX/6wA;->setPadding(IIII)V

    .line 1157450
    const v0, 0x7f0d0b1f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, LX/6wA;->a:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 1157451
    const v0, 0x7f0d0b20

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/6wA;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1157452
    return-void
.end method

.method private static a(LX/6vb;)LX/6va;
    .locals 3

    .prologue
    .line 1157462
    sget-object v0, LX/6w9;->b:[I

    invoke-virtual {p0}, LX/6vb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1157463
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1157464
    :pswitch_0
    sget-object v0, LX/6va;->CONTACT_EMAIL:LX/6va;

    .line 1157465
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, LX/6va;->CONTACT_PHONE_NUMBER:LX/6va;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/6w8;)V
    .locals 3

    .prologue
    const/16 v0, 0x8

    .line 1157466
    iput-object p1, p0, LX/6wA;->c:LX/6w8;

    .line 1157467
    sget-object v1, LX/6w9;->a:[I

    iget-object v2, p0, LX/6wA;->c:LX/6w8;

    iget-object v2, v2, LX/6w8;->e:LX/71H;

    invoke-virtual {v2}, LX/71H;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1157468
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6wA;->c:LX/6w8;

    iget-object v2, v2, LX/6w8;->e:LX/71H;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1157469
    :pswitch_0
    iget-object v1, p0, LX/6wA;->b:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v2, p0, LX/6wA;->c:LX/6w8;

    iget-boolean v2, v2, LX/6w8;->c:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1157470
    iget-object v0, p0, LX/6wA;->a:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->b()V

    .line 1157471
    iget-object v0, p0, LX/6wA;->a:Lcom/facebook/payments/ui/FloatingLabelTextView;

    iget-object v1, p0, LX/6wA;->c:LX/6w8;

    iget-object v1, v1, LX/6w8;->d:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v1}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1157472
    :goto_0
    return-void

    .line 1157473
    :pswitch_1
    iget-object v1, p0, LX/6wA;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1157474
    iget-object v0, p0, LX/6wA;->a:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->a()V

    .line 1157475
    iget-object v0, p0, LX/6wA;->a:Lcom/facebook/payments/ui/FloatingLabelTextView;

    iget-object v1, p0, LX/6wA;->c:LX/6w8;

    iget-object v1, v1, LX/6w8;->d:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v1}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1157476
    iget-object v0, p0, LX/6wA;->c:LX/6w8;

    iget-object v0, v0, LX/6w8;->d:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v0}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1157477
    iget-object v0, p0, LX/6wA;->a:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {p0}, LX/6wA;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081e3a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1157478
    :cond_1
    iget-object v0, p0, LX/6wA;->a:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->b()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onClick()V
    .locals 3

    .prologue
    .line 1157453
    sget-object v0, LX/6w9;->a:[I

    iget-object v1, p0, LX/6wA;->c:LX/6w8;

    iget-object v1, v1, LX/6w8;->e:LX/71H;

    invoke-virtual {v1}, LX/71H;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1157454
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6wA;->c:LX/6w8;

    iget-object v2, v2, LX/6w8;->e:LX/71H;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1157455
    :pswitch_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1157456
    const-string v1, "extra_user_action"

    iget-object v2, p0, LX/6wA;->c:LX/6w8;

    iget-object v2, v2, LX/6w8;->d:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v2}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1157457
    const-string v1, "extra_section_type"

    iget-object v2, p0, LX/6wA;->c:LX/6w8;

    iget-object v2, v2, LX/6w8;->d:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v2}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->d()LX/6vb;

    move-result-object v2

    invoke-static {v2}, LX/6wA;->a(LX/6vb;)LX/6va;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1157458
    new-instance v1, LX/73T;

    sget-object v2, LX/73S;->USER_ACTION:LX/73S;

    invoke-direct {v1, v2, v0}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    .line 1157459
    invoke-virtual {p0, v1}, LX/6E7;->a(LX/73T;)V

    .line 1157460
    :goto_0
    return-void

    .line 1157461
    :pswitch_1
    iget-object v0, p0, LX/6wA;->c:LX/6w8;

    iget-object v0, v0, LX/6w8;->a:Landroid/content/Intent;

    iget-object v1, p0, LX/6wA;->c:LX/6w8;

    iget v1, v1, LX/6w8;->b:I

    invoke-virtual {p0, v0, v1}, LX/6E7;->a(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
