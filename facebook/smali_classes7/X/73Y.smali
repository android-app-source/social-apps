.class public final LX/73Y;
.super LX/6E7;
.source ""


# instance fields
.field public a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

.field public b:Lcom/facebook/widget/SwitchCompat;

.field public c:Lcom/facebook/widget/text/BetterTextView;

.field public d:Lcom/facebook/widget/text/BetterTextView;

.field public e:Lcom/facebook/payments/ui/CallToActionSummaryView;

.field public f:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1165905
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1165906
    const p1, 0x7f030f1a

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1165907
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/73Y;->setOrientation(I)V

    .line 1165908
    const p1, 0x7f0d24ae

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    iput-object p1, p0, LX/73Y;->a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    .line 1165909
    const p1, 0x7f0d24af

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/SwitchCompat;

    iput-object p1, p0, LX/73Y;->b:Lcom/facebook/widget/SwitchCompat;

    .line 1165910
    const p1, 0x7f0d24b0

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, LX/73Y;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1165911
    const p1, 0x7f0d24b1

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, LX/73Y;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1165912
    const p1, 0x7f0d24b2

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/payments/ui/CallToActionSummaryView;

    iput-object p1, p0, LX/73Y;->e:Lcom/facebook/payments/ui/CallToActionSummaryView;

    .line 1165913
    const p1, 0x7f0d209a

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, LX/73Y;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 1165914
    return-void
.end method


# virtual methods
.method public final setDeleteButtonText(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1165915
    iget-object v0, p0, LX/73Y;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1165916
    return-void
.end method

.method public final setLeftAndRightPaddingForChildViews(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1165921
    iget-object v0, p0, LX/73Y;->a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    invoke-virtual {v0, p1, v1, p1, v1}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->setPadding(IIII)V

    .line 1165922
    iget-object v0, p0, LX/73Y;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1, v1, p1, v1}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 1165923
    iget-object v0, p0, LX/73Y;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1, v1, p1, v1}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 1165924
    iget-object v0, p0, LX/73Y;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1, v1, p1, v1}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 1165925
    iget-object v0, p0, LX/73Y;->b:Lcom/facebook/widget/SwitchCompat;

    iget-object v1, p0, LX/73Y;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v1}, Lcom/facebook/widget/SwitchCompat;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, LX/73Y;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v2}, Lcom/facebook/widget/SwitchCompat;->getPaddingBottom()I

    move-result v2

    invoke-virtual {v0, p1, v1, p1, v2}, Lcom/facebook/widget/SwitchCompat;->setPadding(IIII)V

    .line 1165926
    iget-object v0, p0, LX/73Y;->e:Lcom/facebook/payments/ui/CallToActionSummaryView;

    iget-object v1, p0, LX/73Y;->e:Lcom/facebook/payments/ui/CallToActionSummaryView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/CallToActionSummaryView;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, LX/73Y;->e:Lcom/facebook/payments/ui/CallToActionSummaryView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/CallToActionSummaryView;->getPaddingBottom()I

    move-result v2

    invoke-virtual {v0, p1, v1, p1, v2}, Lcom/facebook/payments/ui/CallToActionSummaryView;->setPadding(IIII)V

    .line 1165927
    return-void
.end method

.method public final setOnClickListenerForDeleteButton(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1165917
    iget-object v0, p0, LX/73Y;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1165918
    return-void
.end method

.method public final setOnClickListenerForMakeDefaultButton(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1165919
    iget-object v0, p0, LX/73Y;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1165920
    return-void
.end method

.method public final setPaymentsComponentCallback(LX/6qh;)V
    .locals 1

    .prologue
    .line 1165900
    invoke-super {p0, p1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1165901
    iget-object v0, p0, LX/73Y;->a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    invoke-virtual {v0, p1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1165902
    return-void
.end method

.method public final setSecurityInfo(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1165903
    iget-object v0, p0, LX/73Y;->a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->setText(I)V

    .line 1165904
    return-void
.end method

.method public final setSecurityInfo(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1165898
    iget-object v0, p0, LX/73Y;->a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->setText(Ljava/lang/String;)V

    .line 1165899
    return-void
.end method

.method public final setVisibilityOfDefaultActionSummary(I)V
    .locals 1

    .prologue
    .line 1165896
    iget-object v0, p0, LX/73Y;->e:Lcom/facebook/payments/ui/CallToActionSummaryView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/CallToActionSummaryView;->setVisibility(I)V

    .line 1165897
    return-void
.end method

.method public final setVisibilityOfDefaultInfoView(I)V
    .locals 1

    .prologue
    .line 1165894
    iget-object v0, p0, LX/73Y;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1165895
    return-void
.end method

.method public final setVisibilityOfDeleteButton(I)V
    .locals 1

    .prologue
    .line 1165892
    iget-object v0, p0, LX/73Y;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1165893
    return-void
.end method

.method public final setVisibilityOfMakeDefaultButton(I)V
    .locals 1

    .prologue
    .line 1165890
    iget-object v0, p0, LX/73Y;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1165891
    return-void
.end method

.method public final setVisibilityOfMakeDefaultSwitch(I)V
    .locals 1

    .prologue
    .line 1165888
    iget-object v0, p0, LX/73Y;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/SwitchCompat;->setVisibility(I)V

    .line 1165889
    return-void
.end method
