.class public LX/6ji;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final messageMetadata:LX/6kn;

.field public final mode:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1132222
    new-instance v0, LX/1sv;

    const-string v1, "DeltaApprovalMode"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6ji;->b:LX/1sv;

    .line 1132223
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ji;->c:LX/1sw;

    .line 1132224
    new-instance v0, LX/1sw;

    const-string v1, "mode"

    const/16 v2, 0x8

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ji;->d:LX/1sw;

    .line 1132225
    sput-boolean v4, LX/6ji;->a:Z

    return-void
.end method

.method private constructor <init>(LX/6kn;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1132218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1132219
    iput-object p1, p0, LX/6ji;->messageMetadata:LX/6kn;

    .line 1132220
    iput-object p2, p0, LX/6ji;->mode:Ljava/lang/Integer;

    .line 1132221
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1132211
    iget-object v0, p0, LX/6ji;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1132212
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6ji;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1132213
    :cond_0
    iget-object v0, p0, LX/6ji;->mode:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1132214
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'mode\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6ji;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1132215
    :cond_1
    iget-object v0, p0, LX/6ji;->mode:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    sget-object v0, LX/6jW;->a:LX/1sn;

    iget-object v1, p0, LX/6ji;->mode:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1132216
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'mode\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6ji;->mode:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1132217
    :cond_2
    return-void
.end method

.method public static b(LX/1su;)LX/6ji;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1132185
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v1, v0

    .line 1132186
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 1132187
    iget-byte v3, v2, LX/1sw;->b:B

    if-eqz v3, :cond_2

    .line 1132188
    iget-short v3, v2, LX/1sw;->c:S

    packed-switch v3, :pswitch_data_0

    .line 1132189
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1132190
    :pswitch_0
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0xc

    if-ne v3, v4, :cond_0

    .line 1132191
    invoke-static {p0}, LX/6kn;->b(LX/1su;)LX/6kn;

    move-result-object v1

    goto :goto_0

    .line 1132192
    :cond_0
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1132193
    :pswitch_1
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0x8

    if-ne v3, v4, :cond_1

    .line 1132194
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 1132195
    :cond_1
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1132196
    :cond_2
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1132197
    new-instance v2, LX/6ji;

    invoke-direct {v2, v1, v0}, LX/6ji;-><init>(LX/6kn;Ljava/lang/Integer;)V

    .line 1132198
    invoke-direct {v2}, LX/6ji;->a()V

    .line 1132199
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1132226
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1132227
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1132228
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1132229
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaApprovalMode"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1132230
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132231
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132232
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132233
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132234
    const-string v4, "messageMetadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132235
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132236
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132237
    iget-object v4, p0, LX/6ji;->messageMetadata:LX/6kn;

    if-nez v4, :cond_4

    .line 1132238
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132239
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132240
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132241
    const-string v4, "mode"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132242
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132243
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132244
    iget-object v0, p0, LX/6ji;->mode:Ljava/lang/Integer;

    if-nez v0, :cond_5

    .line 1132245
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132246
    :cond_0
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132247
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132248
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1132249
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1132250
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1132251
    :cond_3
    const-string v0, ""

    goto/16 :goto_2

    .line 1132252
    :cond_4
    iget-object v4, p0, LX/6ji;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1132253
    :cond_5
    sget-object v0, LX/6jW;->b:Ljava/util/Map;

    iget-object v4, p0, LX/6ji;->mode:Ljava/lang/Integer;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1132254
    if-eqz v0, :cond_6

    .line 1132255
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132256
    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132257
    :cond_6
    iget-object v4, p0, LX/6ji;->mode:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1132258
    if-eqz v0, :cond_0

    .line 1132259
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1132200
    invoke-direct {p0}, LX/6ji;->a()V

    .line 1132201
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1132202
    iget-object v0, p0, LX/6ji;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1132203
    sget-object v0, LX/6ji;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132204
    iget-object v0, p0, LX/6ji;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1132205
    :cond_0
    iget-object v0, p0, LX/6ji;->mode:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1132206
    sget-object v0, LX/6ji;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132207
    iget-object v0, p0, LX/6ji;->mode:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1132208
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1132209
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1132210
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1132163
    if-nez p1, :cond_1

    .line 1132164
    :cond_0
    :goto_0
    return v0

    .line 1132165
    :cond_1
    instance-of v1, p1, LX/6ji;

    if-eqz v1, :cond_0

    .line 1132166
    check-cast p1, LX/6ji;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1132167
    if-nez p1, :cond_3

    .line 1132168
    :cond_2
    :goto_1
    move v0, v2

    .line 1132169
    goto :goto_0

    .line 1132170
    :cond_3
    iget-object v0, p0, LX/6ji;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1132171
    :goto_2
    iget-object v3, p1, LX/6ji;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1132172
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1132173
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132174
    iget-object v0, p0, LX/6ji;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6ji;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132175
    :cond_5
    iget-object v0, p0, LX/6ji;->mode:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1132176
    :goto_4
    iget-object v3, p1, LX/6ji;->mode:Ljava/lang/Integer;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1132177
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1132178
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132179
    iget-object v0, p0, LX/6ji;->mode:Ljava/lang/Integer;

    iget-object v3, p1, LX/6ji;->mode:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1132180
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1132181
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1132182
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1132183
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1132184
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1132162
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1132159
    sget-boolean v0, LX/6ji;->a:Z

    .line 1132160
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6ji;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1132161
    return-object v0
.end method
