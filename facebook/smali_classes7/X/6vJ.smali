.class public LX/6vJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/util/concurrent/Executor;

.field public c:LX/6qh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;LX/6wT;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1156843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156844
    iput-object p1, p0, LX/6vJ;->a:Landroid/content/Context;

    .line 1156845
    iput-object p2, p0, LX/6vJ;->b:Ljava/util/concurrent/Executor;

    .line 1156846
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;LX/6vY;)Lcom/facebook/payments/contactinfo/model/ContactInfo;
    .locals 3

    .prologue
    .line 1156847
    sget-object v0, LX/6vW;->a:[I

    invoke-virtual {p2}, LX/6vY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1156848
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1156849
    :pswitch_0
    invoke-static {}, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->newBuilder()LX/6vd;

    move-result-object v0

    .line 1156850
    iput-object p0, v0, LX/6vd;->a:Ljava/lang/String;

    .line 1156851
    move-object v0, v0

    .line 1156852
    invoke-interface {p1}, Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;->a()Z

    move-result v1

    .line 1156853
    iput-boolean v1, v0, LX/6vd;->c:Z

    .line 1156854
    move-object v0, v0

    .line 1156855
    check-cast p1, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;

    iget-object v1, p1, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;->a:Ljava/lang/String;

    .line 1156856
    iput-object v1, v0, LX/6vd;->b:Ljava/lang/String;

    .line 1156857
    move-object v0, v0

    .line 1156858
    invoke-virtual {v0}, LX/6vd;->d()Lcom/facebook/payments/contactinfo/model/EmailContactInfo;

    move-result-object v0

    .line 1156859
    :goto_0
    return-object v0

    .line 1156860
    :pswitch_1
    new-instance v0, Lcom/facebook/payments/contactinfo/model/NameContactInfo;

    check-cast p1, Lcom/facebook/payments/contactinfo/model/NameContactInfoFormInput;

    iget-object v1, p1, Lcom/facebook/payments/contactinfo/model/NameContactInfoFormInput;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/payments/contactinfo/model/NameContactInfo;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1156861
    :pswitch_2
    invoke-static {}, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->newBuilder()LX/6vj;

    move-result-object v0

    .line 1156862
    iput-object p0, v0, LX/6vj;->a:Ljava/lang/String;

    .line 1156863
    move-object v0, v0

    .line 1156864
    invoke-interface {p1}, Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;->a()Z

    move-result v1

    .line 1156865
    iput-boolean v1, v0, LX/6vj;->d:Z

    .line 1156866
    move-object v0, v0

    .line 1156867
    check-cast p1, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;

    iget-object v1, p1, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;->a:Ljava/lang/String;

    .line 1156868
    iput-object v1, v0, LX/6vj;->b:Ljava/lang/String;

    .line 1156869
    move-object v0, v0

    .line 1156870
    invoke-virtual {v0}, LX/6vj;->e()Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/6vJ;Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8

    .prologue
    .line 1156871
    move-object v0, p1

    .line 1156872
    iget-object v0, v0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v0}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->a()Ljava/lang/String;

    move-result-object v5

    .line 1156873
    new-instance v0, LX/6wg;

    invoke-direct {v0}, LX/6wg;-><init>()V

    move-object v0, v0

    .line 1156874
    iput-object v5, v0, LX/6wg;->a:Ljava/lang/String;

    .line 1156875
    move-object v0, v0

    .line 1156876
    if-eqz p2, :cond_0

    .line 1156877
    iput-object p2, v0, LX/6wg;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    .line 1156878
    :cond_0
    iput-boolean p3, v0, LX/6wg;->c:Z

    .line 1156879
    iput-boolean p4, v0, LX/6wg;->d:Z

    .line 1156880
    new-instance v1, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;

    invoke-direct {v1, v0}, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;-><init>(LX/6wg;)V

    move-object v0, v1

    .line 1156881
    invoke-virtual {p0, v0}, LX/6vJ;->a(Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 1156882
    new-instance v0, LX/6vV;

    move-object v1, p0

    move v2, p4

    move-object v3, p1

    move-object v4, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, LX/6vV;-><init>(LX/6vJ;ZLcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;Ljava/lang/String;Z)V

    iget-object v1, p0, LX/6vJ;->b:Ljava/util/concurrent/Executor;

    invoke-static {v7, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1156883
    return-object v7
.end method

.method public static a$redex0(LX/6vJ;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1156884
    const-class v0, LX/2Oo;

    invoke-static {p1, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 1156885
    if-nez v0, :cond_0

    .line 1156886
    iget-object v0, p0, LX/6vJ;->a:Landroid/content/Context;

    invoke-static {v0, p1}, LX/6up;->a(Landroid/content/Context;Ljava/lang/Throwable;)V

    .line 1156887
    :goto_0
    return-void

    .line 1156888
    :cond_0
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1156889
    new-instance v1, LX/6dy;

    iget-object v2, p0, LX/6vJ;->a:Landroid/content/Context;

    const v3, 0x7f080016

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p2, v2}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156890
    iput-object v0, v1, LX/6dy;->d:Ljava/lang/String;

    .line 1156891
    move-object v0, v1

    .line 1156892
    const/4 v1, 0x1

    .line 1156893
    iput-boolean v1, v0, LX/6dy;->f:Z

    .line 1156894
    move-object v0, v0

    .line 1156895
    invoke-virtual {v0}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    .line 1156896
    invoke-static {v0}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->b(Lcom/facebook/messaging/dialog/ConfirmActionParams;)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    move-result-object v0

    .line 1156897
    iget-object v1, p0, LX/6vJ;->c:LX/6qh;

    invoke-virtual {v1, v0}, LX/6qh;->a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2

    .prologue
    .line 1156898
    new-instance v0, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;

    const-string v1, "0"

    invoke-direct {v0, v1}, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2

    .prologue
    .line 1156899
    new-instance v0, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;

    const-string v1, "0"

    invoke-direct {v0, v1}, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
