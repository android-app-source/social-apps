.class public final LX/7qj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1260507
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 1260508
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1260509
    :goto_0
    return v1

    .line 1260510
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_5

    .line 1260511
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1260512
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1260513
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 1260514
    const-string v12, "can_user_edit_rsvp_status_of_guest"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1260515
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v7

    move v10, v7

    move v7, v6

    goto :goto_1

    .line 1260516
    :cond_1
    const-string v12, "node"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1260517
    invoke-static {p0, p1}, LX/7u7;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1260518
    :cond_2
    const-string v12, "rsvp_time"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1260519
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 1260520
    :cond_3
    const-string v12, "seen_state"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1260521
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto :goto_1

    .line 1260522
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1260523
    :cond_5
    const/4 v11, 0x4

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1260524
    if-eqz v7, :cond_6

    .line 1260525
    invoke-virtual {p1, v1, v10}, LX/186;->a(IZ)V

    .line 1260526
    :cond_6
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 1260527
    if-eqz v0, :cond_7

    .line 1260528
    const/4 v1, 0x2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1260529
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1260530
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v7, v1

    move v8, v1

    move-wide v2, v4

    move v9, v1

    move v10, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x3

    .line 1260531
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1260532
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1260533
    if-eqz v0, :cond_0

    .line 1260534
    const-string v1, "can_user_edit_rsvp_status_of_guest"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260535
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1260536
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1260537
    if-eqz v0, :cond_1

    .line 1260538
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260539
    invoke-static {p0, v0, p2, p3}, LX/7u7;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1260540
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1260541
    cmp-long v2, v0, v4

    if-eqz v2, :cond_2

    .line 1260542
    const-string v2, "rsvp_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260543
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1260544
    :cond_2
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1260545
    if-eqz v0, :cond_3

    .line 1260546
    const-string v0, "seen_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260547
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1260548
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1260549
    return-void
.end method
