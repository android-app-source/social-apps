.class public LX/7LN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/04D;

.field public b:LX/04G;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1197968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1197969
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    iput-object v0, p0, LX/7LN;->a:LX/04D;

    .line 1197970
    sget-object v0, LX/04G;->OTHERS:LX/04G;

    iput-object v0, p0, LX/7LN;->b:LX/04G;

    .line 1197971
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7LN;->c:Ljava/util/List;

    .line 1197972
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7LN;->d:Z

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)LX/7LN;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2oy;",
            ">;)",
            "LX/7LN;"
        }
    .end annotation

    .prologue
    .line 1197966
    iget-object v0, p0, LX/7LN;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1197967
    return-object p0
.end method

.method public final varargs a([LX/2oy;)LX/7LN;
    .locals 1

    .prologue
    .line 1197981
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/7LN;->a(Ljava/util/List;)LX/7LN;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;)Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 1197982
    new-instance v0, Lcom/facebook/video/player/RichVideoPlayer;

    invoke-direct {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;-><init>(Landroid/content/Context;)V

    .line 1197983
    invoke-virtual {p0, v0}, LX/7LN;->a(Lcom/facebook/video/player/RichVideoPlayer;)Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)Lcom/facebook/video/player/RichVideoPlayer;
    .locals 2

    .prologue
    .line 1197973
    iget-object v0, p0, LX/7LN;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 1197974
    invoke-static {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1197975
    goto :goto_0

    .line 1197976
    :cond_0
    iget-object v0, p0, LX/7LN;->a:LX/04D;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1197977
    iget-object v0, p0, LX/7LN;->b:LX/04G;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1197978
    iget-boolean v0, p0, LX/7LN;->d:Z

    .line 1197979
    iput-boolean v0, p1, Lcom/facebook/video/player/RichVideoPlayer;->C:Z

    .line 1197980
    return-object p1
.end method
