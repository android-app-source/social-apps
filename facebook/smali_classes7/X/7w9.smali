.class public LX/7w9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6F4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6F4",
        "<",
        "Lcom/facebook/payments/confirmation/SimpleConfirmationData;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/6ul;

.field public final d:LX/7vZ;

.field public e:LX/6qh;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1275484
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "faceweb/f?href=%s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7w9;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/6ul;LX/7vZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1275479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275480
    iput-object p1, p0, LX/7w9;->b:Landroid/content/Context;

    .line 1275481
    iput-object p2, p0, LX/7w9;->c:LX/6ul;

    .line 1275482
    iput-object p3, p0, LX/7w9;->d:LX/7vZ;

    .line 1275483
    return-void
.end method


# virtual methods
.method public final a(LX/6qh;)V
    .locals 2

    .prologue
    .line 1275485
    iput-object p1, p0, LX/7w9;->e:LX/6qh;

    .line 1275486
    iget-object v0, p0, LX/7w9;->c:LX/6ul;

    iget-object v1, p0, LX/7w9;->e:LX/6qh;

    invoke-virtual {v0, v1}, LX/6ul;->a(LX/6qh;)V

    .line 1275487
    return-void
.end method

.method public final a(Lcom/facebook/payments/confirmation/ConfirmationData;)V
    .locals 6

    .prologue
    .line 1275471
    check-cast p1, Lcom/facebook/payments/confirmation/SimpleConfirmationData;

    .line 1275472
    invoke-virtual {p1}, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a()Lcom/facebook/payments/confirmation/ConfirmationParams;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;

    .line 1275473
    iget-object v1, p1, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->b:Lcom/facebook/payments/confirmation/ProductConfirmationData;

    move-object v1, v1

    .line 1275474
    check-cast v1, Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;

    .line 1275475
    iget-boolean v2, v1, Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;->a:Z

    if-eqz v2, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1275476
    :goto_0
    iget-object v3, p0, LX/7w9;->d:LX/7vZ;

    iget-object v4, v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v4, v4, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->z:Ljava/lang/String;

    iget-object v5, v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object p1, Lcom/facebook/events/common/ActionMechanism;->BUY_TICKETS_FLOW:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v3, v4, v2, v5, p1}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1275477
    return-void

    .line 1275478
    :cond_0
    iget-object v2, v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v2, v2, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->C:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_0
.end method

.method public bridge synthetic onClick(Lcom/facebook/payments/confirmation/ConfirmationData;LX/6uH;)V
    .locals 2

    .prologue
    .line 1275458
    check-cast p1, Lcom/facebook/payments/confirmation/SimpleConfirmationData;

    .line 1275459
    sget-object v0, LX/7w8;->a:[I

    invoke-interface {p2}, LX/6uG;->d()LX/6uT;

    move-result-object v1

    invoke-virtual {v1}, LX/6uT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1275460
    iget-object v0, p0, LX/7w9;->c:LX/6ul;

    invoke-virtual {v0, p1, p2}, LX/6ul;->onClick(Lcom/facebook/payments/confirmation/SimpleConfirmationData;LX/6uH;)V

    .line 1275461
    :goto_0
    return-void

    .line 1275462
    :pswitch_0
    check-cast p2, LX/7wF;

    .line 1275463
    iget-object v0, p0, LX/7w9;->b:Landroid/content/Context;

    iget-object v1, p2, LX/7wF;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1275464
    iget-object v1, p0, LX/7w9;->e:LX/6qh;

    invoke-virtual {v1, v0}, LX/6qh;->a(Landroid/content/Intent;)V

    .line 1275465
    goto :goto_0

    .line 1275466
    :pswitch_1
    invoke-virtual {p1}, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a()Lcom/facebook/payments/confirmation/ConfirmationParams;

    move-result-object v0

    .line 1275467
    sget-object v1, LX/7w9;->a:Ljava/lang/String;

    invoke-interface {v0}, Lcom/facebook/payments/confirmation/ConfirmationParams;->a()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object p1

    iget-object p1, p1, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->f:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 1275468
    new-instance p1, Landroid/content/Intent;

    const-string p2, "android.intent.action.VIEW"

    invoke-direct {p1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 1275469
    iget-object p1, p0, LX/7w9;->e:LX/6qh;

    invoke-virtual {p1, v1}, LX/6qh;->a(Landroid/content/Intent;)V

    .line 1275470
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
