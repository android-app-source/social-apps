.class public final LX/78Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V
    .locals 0

    .prologue
    .line 1172622
    iput-object p1, p0, LX/78Q;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 4

    .prologue
    .line 1172623
    iget-object v0, p0, LX/78Q;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v0, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->v:Lcom/facebook/structuredsurvey/views/SurveyListView;

    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/views/SurveyListView;->getHeight()I

    move-result v0

    iget-object v1, p0, LX/78Q;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v1, v1, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    invoke-virtual {v1}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/78Q;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v1, v1, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    invoke-virtual {v1}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 1172624
    iget-object v1, p0, LX/78Q;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget v1, v1, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->K:I

    if-eq v0, v1, :cond_0

    .line 1172625
    iget-object v1, p0, LX/78Q;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    .line 1172626
    iput v0, v1, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->K:I

    .line 1172627
    iget-object v0, p0, LX/78Q;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    .line 1172628
    iget-object v1, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->r:LX/78U;

    sget-object v2, LX/78U;->OUTRO_COLLAPSED:LX/78U;

    if-ne v1, v2, :cond_0

    .line 1172629
    const/4 p0, 0x1

    .line 1172630
    iget v1, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->K:I

    invoke-static {v0, v1}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->d(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;I)V

    .line 1172631
    iget-object v1, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    invoke-virtual {v1}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 1172632
    const/4 v2, 0x0

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1172633
    iget v2, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->q:I

    iget v3, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->p:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1172634
    iget-object v2, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    invoke-virtual {v2, v1}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1172635
    iget-object v1, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->t:Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;

    iget v2, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->p:I

    iget v3, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->q:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->a(II)V

    .line 1172636
    iget-object v1, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->t:Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;

    invoke-virtual {v1}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->a()V

    .line 1172637
    invoke-static {v0, p0}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->d(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;Z)V

    .line 1172638
    sget-object v1, LX/31M;->DOWN:LX/31M;

    invoke-virtual {v0, v1, p0}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->a(LX/31M;Z)V

    .line 1172639
    :cond_0
    return-void
.end method
