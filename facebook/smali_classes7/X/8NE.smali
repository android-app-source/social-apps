.class public LX/8NE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/8NF;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1336423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/8NE;
    .locals 1

    .prologue
    .line 1336424
    new-instance v0, LX/8NE;

    invoke-direct {v0}, LX/8NE;-><init>()V

    .line 1336425
    move-object v0, v0

    .line 1336426
    return-object v0
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1336427
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1336428
    :cond_0
    const/4 v0, 0x0

    .line 1336429
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x2c

    invoke-static {v1}, LX/0PO;->on(C)LX/0PO;

    move-result-object v1

    invoke-virtual {v1}, LX/0PO;->skipNulls()LX/0PO;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 1336430
    check-cast p1, LX/8NF;

    .line 1336431
    iget-object v0, p1, LX/8NF;->a:Ljava/lang/String;

    move-object v1, v0

    .line 1336432
    iget-object v0, p1, LX/8NF;->b:LX/0Px;

    move-object v2, v0

    .line 1336433
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1336434
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1336435
    iget-boolean v3, p1, LX/8NF;->c:Z

    move v3, v3

    .line 1336436
    if-eqz v3, :cond_1

    .line 1336437
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "reorder_pids"

    invoke-static {v2}, LX/8NE;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336438
    :goto_1
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "update-photo-order"

    .line 1336439
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1336440
    move-object v2, v2

    .line 1336441
    const-string v3, "POST"

    .line 1336442
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 1336443
    move-object v2, v2

    .line 1336444
    iput-object v1, v2, LX/14O;->d:Ljava/lang/String;

    .line 1336445
    move-object v1, v2

    .line 1336446
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1336447
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1336448
    move-object v1, v1

    .line 1336449
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1336450
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1336451
    move-object v0, v1

    .line 1336452
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1336453
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1336454
    :cond_1
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "photo_order"

    invoke-static {v2}, LX/8NE;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1336455
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1336456
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 1336457
    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 1336458
    iget-object v0, p2, LX/1pN;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1336459
    instance-of v0, v0, LX/1Xb;

    if-eqz v0, :cond_0

    .line 1336460
    iget-object v0, p2, LX/1pN;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1336461
    check-cast v0, LX/1Xb;

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
