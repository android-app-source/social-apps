.class public final LX/8bl;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1372319
    const-class v1, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;

    const v0, -0x6bca263b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchBootstrapEntities"

    const-string v6, "c385c66d0ffbb3d7a6f7d53c0f1a854e"

    const-string v7, "viewer"

    const-string v8, "10155114482961729"

    const-string v9, "10155259087161729"

    .line 1372320
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1372321
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1372322
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1372311
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1372312
    sparse-switch v0, :sswitch_data_0

    .line 1372313
    :goto_0
    return-object p1

    .line 1372314
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1372315
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1372316
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1372317
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1372318
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x29d41394 -> :sswitch_0
        0x196b8 -> :sswitch_3
        0x3492916 -> :sswitch_1
        0x2d4bfb74 -> :sswitch_4
        0x3fcddc5f -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1372307
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1372308
    :goto_1
    return v0

    .line 1372309
    :pswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1372310
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
