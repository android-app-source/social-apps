.class public LX/89F;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1304412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/model/ComposerReshareContext;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1304413
    invoke-static {p0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-static {v0}, LX/2cA;->a(Lcom/facebook/graphql/model/GraphQLPrivacyScope;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1304414
    invoke-static {p0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1304415
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 1304416
    :goto_0
    invoke-static {p0}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    move-object v2, v0

    move-object v0, v4

    .line 1304417
    :goto_1
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1304418
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->setShouldIncludeReshareContext(Z)Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->setOriginalShareActorName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->setReshareMessage(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    move-result-object v1

    .line 1304419
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    .line 1304420
    goto :goto_0

    :cond_2
    move-object v0, v1

    move-object v2, v1

    goto :goto_1
.end method
