.class public final LX/8b6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 32

    .prologue
    .line 1371244
    const/16 v28, 0x0

    .line 1371245
    const/16 v27, 0x0

    .line 1371246
    const/16 v26, 0x0

    .line 1371247
    const/16 v25, 0x0

    .line 1371248
    const/16 v24, 0x0

    .line 1371249
    const/16 v23, 0x0

    .line 1371250
    const/16 v22, 0x0

    .line 1371251
    const/16 v21, 0x0

    .line 1371252
    const/16 v20, 0x0

    .line 1371253
    const/16 v19, 0x0

    .line 1371254
    const/16 v18, 0x0

    .line 1371255
    const/16 v17, 0x0

    .line 1371256
    const/16 v16, 0x0

    .line 1371257
    const/4 v15, 0x0

    .line 1371258
    const/4 v14, 0x0

    .line 1371259
    const/4 v13, 0x0

    .line 1371260
    const/4 v12, 0x0

    .line 1371261
    const/4 v11, 0x0

    .line 1371262
    const/4 v10, 0x0

    .line 1371263
    const/4 v9, 0x0

    .line 1371264
    const/4 v8, 0x0

    .line 1371265
    const/4 v7, 0x0

    .line 1371266
    const/4 v6, 0x0

    .line 1371267
    const/4 v5, 0x0

    .line 1371268
    const/4 v4, 0x0

    .line 1371269
    const/4 v3, 0x0

    .line 1371270
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v29

    sget-object v30, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_1

    .line 1371271
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1371272
    const/4 v3, 0x0

    .line 1371273
    :goto_0
    return v3

    .line 1371274
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1371275
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v29

    sget-object v30, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_1c

    .line 1371276
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v29

    .line 1371277
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1371278
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_1

    if-eqz v29, :cond_1

    .line 1371279
    const-string v30, "__type__"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_2

    const-string v30, "__typename"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_3

    .line 1371280
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v28

    goto :goto_1

    .line 1371281
    :cond_3
    const-string v30, "ad_choices_icon"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_4

    .line 1371282
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v27

    goto :goto_1

    .line 1371283
    :cond_4
    const-string v30, "ad_choices_link_url"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_5

    .line 1371284
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    goto :goto_1

    .line 1371285
    :cond_5
    const-string v30, "ads_encrypted_data"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_6

    .line 1371286
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto :goto_1

    .line 1371287
    :cond_6
    const-string v30, "body"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_7

    .line 1371288
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    goto/16 :goto_1

    .line 1371289
    :cond_7
    const-string v30, "call_to_action"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_8

    .line 1371290
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto/16 :goto_1

    .line 1371291
    :cond_8
    const-string v30, "click_report_url"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_9

    .line 1371292
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto/16 :goto_1

    .line 1371293
    :cond_9
    const-string v30, "client_token"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_a

    .line 1371294
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto/16 :goto_1

    .line 1371295
    :cond_a
    const-string v30, "command_url"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_b

    .line 1371296
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto/16 :goto_1

    .line 1371297
    :cond_b
    const-string v30, "feedback"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_c

    .line 1371298
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1371299
    :cond_c
    const-string v30, "feedback_options"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_d

    .line 1371300
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    goto/16 :goto_1

    .line 1371301
    :cond_d
    const-string v30, "icon"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_e

    .line 1371302
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 1371303
    :cond_e
    const-string v30, "image"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_f

    .line 1371304
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 1371305
    :cond_f
    const-string v30, "impression_report_url"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_10

    .line 1371306
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 1371307
    :cond_10
    const-string v30, "native_ad_type"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_11

    .line 1371308
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v14

    goto/16 :goto_1

    .line 1371309
    :cond_11
    const-string v30, "shareable_id"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_12

    .line 1371310
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 1371311
    :cond_12
    const-string v30, "social_context"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_13

    .line 1371312
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 1371313
    :cond_13
    const-string v30, "subtitle"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_14

    .line 1371314
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 1371315
    :cond_14
    const-string v30, "title"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_15

    .line 1371316
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 1371317
    :cond_15
    const-string v30, "tracking"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_16

    .line 1371318
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 1371319
    :cond_16
    const-string v30, "video"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_17

    .line 1371320
    invoke-static/range {p0 .. p1}, LX/8ZV;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1371321
    :cond_17
    const-string v30, "video_autoplay_style"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_18

    .line 1371322
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto/16 :goto_1

    .line 1371323
    :cond_18
    const-string v30, "video_control_style"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_19

    .line 1371324
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto/16 :goto_1

    .line 1371325
    :cond_19
    const-string v30, "video_looping_style"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_1a

    .line 1371326
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto/16 :goto_1

    .line 1371327
    :cond_1a
    const-string v30, "video_play_report_url"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_1b

    .line 1371328
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 1371329
    :cond_1b
    const-string v30, "video_skip_report_url"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_0

    .line 1371330
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1371331
    :cond_1c
    const/16 v29, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1371332
    const/16 v29, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1371333
    const/16 v28, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1371334
    const/16 v27, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1371335
    const/16 v26, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1371336
    const/16 v25, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1371337
    const/16 v24, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1371338
    const/16 v23, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1371339
    const/16 v22, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1371340
    const/16 v21, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1371341
    const/16 v20, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1371342
    const/16 v19, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1371343
    const/16 v18, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1371344
    const/16 v17, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1371345
    const/16 v16, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1371346
    const/16 v15, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1371347
    const/16 v14, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1371348
    const/16 v13, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1371349
    const/16 v12, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1371350
    const/16 v11, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1371351
    const/16 v10, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1371352
    const/16 v9, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1371353
    const/16 v8, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1371354
    const/16 v7, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1371355
    const/16 v6, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1371356
    const/16 v5, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1371357
    const/16 v4, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1371358
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/16 v5, 0x16

    const/16 v4, 0x15

    const/16 v3, 0xe

    const/16 v2, 0xa

    const/4 v1, 0x0

    .line 1371359
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1371360
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1371361
    if-eqz v0, :cond_0

    .line 1371362
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371363
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1371364
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1371365
    if-eqz v0, :cond_1

    .line 1371366
    const-string v1, "ad_choices_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371367
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1371368
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371369
    if-eqz v0, :cond_2

    .line 1371370
    const-string v1, "ad_choices_link_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371371
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371372
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371373
    if-eqz v0, :cond_3

    .line 1371374
    const-string v1, "ads_encrypted_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371375
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371376
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371377
    if-eqz v0, :cond_4

    .line 1371378
    const-string v1, "body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371379
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371380
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371381
    if-eqz v0, :cond_5

    .line 1371382
    const-string v1, "call_to_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371383
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371384
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371385
    if-eqz v0, :cond_6

    .line 1371386
    const-string v1, "click_report_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371387
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371388
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371389
    if-eqz v0, :cond_7

    .line 1371390
    const-string v1, "client_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371391
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371392
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371393
    if-eqz v0, :cond_8

    .line 1371394
    const-string v1, "command_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371395
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371396
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1371397
    if-eqz v0, :cond_9

    .line 1371398
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371399
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1371400
    :cond_9
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1371401
    if-eqz v0, :cond_a

    .line 1371402
    const-string v0, "feedback_options"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371403
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371404
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1371405
    if-eqz v0, :cond_b

    .line 1371406
    const-string v1, "icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371407
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1371408
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1371409
    if-eqz v0, :cond_c

    .line 1371410
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371411
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1371412
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371413
    if-eqz v0, :cond_d

    .line 1371414
    const-string v1, "impression_report_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371415
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371416
    :cond_d
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1371417
    if-eqz v0, :cond_e

    .line 1371418
    const-string v0, "native_ad_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371419
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371420
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371421
    if-eqz v0, :cond_f

    .line 1371422
    const-string v1, "shareable_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371423
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371424
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371425
    if-eqz v0, :cond_10

    .line 1371426
    const-string v1, "social_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371427
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371428
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371429
    if-eqz v0, :cond_11

    .line 1371430
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371431
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371432
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371433
    if-eqz v0, :cond_12

    .line 1371434
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371435
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371436
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371437
    if-eqz v0, :cond_13

    .line 1371438
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371439
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371440
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1371441
    if-eqz v0, :cond_14

    .line 1371442
    const-string v1, "video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371443
    invoke-static {p0, v0, p2, p3}, LX/8ZV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1371444
    :cond_14
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1371445
    if-eqz v0, :cond_15

    .line 1371446
    const-string v0, "video_autoplay_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371447
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371448
    :cond_15
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 1371449
    if-eqz v0, :cond_16

    .line 1371450
    const-string v0, "video_control_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371451
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371452
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1371453
    if-eqz v0, :cond_17

    .line 1371454
    const-string v0, "video_looping_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371455
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371456
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371457
    if-eqz v0, :cond_18

    .line 1371458
    const-string v1, "video_play_report_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371459
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371460
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1371461
    if-eqz v0, :cond_19

    .line 1371462
    const-string v1, "video_skip_report_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1371463
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1371464
    :cond_19
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1371465
    return-void
.end method
