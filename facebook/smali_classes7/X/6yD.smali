.class public final LX/6yD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V
    .locals 0

    .prologue
    .line 1159592
    iput-object p1, p0, LX/6yD;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    .line 1159593
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 1159594
    iget-object v0, p0, LX/6yD;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->i:LX/6yZ;

    iget-object v1, p0, LX/6yD;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v0, v1}, LX/6yZ;->b(LX/6yO;)LX/6xu;

    move-result-object v0

    .line 1159595
    iget-object v1, p0, LX/6yD;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->j:LX/0Zb;

    iget-object v2, p0, LX/6yD;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    iget-object v3, p0, LX/6yD;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0, v3}, LX/6xu;->f(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1159596
    iget-object v0, p0, LX/6yD;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->b()Z

    move-result v0

    .line 1159597
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
