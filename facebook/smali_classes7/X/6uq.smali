.class public final LX/6uq;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/6uu;


# direct methods
.method public constructor <init>(LX/6uu;Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1156197
    iput-object p1, p0, LX/6uq;->c:LX/6uu;

    iput-object p2, p0, LX/6uq;->a:Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    iput-object p3, p0, LX/6uq;->b:Landroid/view/View;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1156198
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/6uq;->a:Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 1156199
    iget-object v1, p0, LX/6uq;->c:LX/6uu;

    iget-object v1, v1, LX/6uu;->d:LX/6qh;

    invoke-virtual {v1, v0}, LX/6qh;->b(Landroid/content/Intent;)V

    .line 1156200
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1156201
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1156202
    iget-object v0, p0, LX/6uq;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a00aa

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1156203
    return-void
.end method
