.class public final enum LX/7GT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7GT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7GT;

.field public static final enum CONTACTS_QUEUE_TYPE:LX/7GT;

.field public static final enum MESSAGES_QUEUE_TYPE:LX/7GT;

.field public static final enum PAYMENTS_QUEUE_TYPE:LX/7GT;

.field public static final enum VOIP_QUEUE_TYPE:LX/7GT;


# instance fields
.field public final apiString:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1189212
    new-instance v0, LX/7GT;

    const-string v1, "MESSAGES_QUEUE_TYPE"

    const-string v2, "m"

    invoke-direct {v0, v1, v3, v2}, LX/7GT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    .line 1189213
    new-instance v0, LX/7GT;

    const-string v1, "CONTACTS_QUEUE_TYPE"

    const-string v2, "c"

    invoke-direct {v0, v1, v4, v2}, LX/7GT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7GT;->CONTACTS_QUEUE_TYPE:LX/7GT;

    .line 1189214
    new-instance v0, LX/7GT;

    const-string v1, "VOIP_QUEUE_TYPE"

    const-string v2, "v"

    invoke-direct {v0, v1, v5, v2}, LX/7GT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7GT;->VOIP_QUEUE_TYPE:LX/7GT;

    .line 1189215
    new-instance v0, LX/7GT;

    const-string v1, "PAYMENTS_QUEUE_TYPE"

    const-string v2, "p2p"

    invoke-direct {v0, v1, v6, v2}, LX/7GT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7GT;->PAYMENTS_QUEUE_TYPE:LX/7GT;

    .line 1189216
    const/4 v0, 0x4

    new-array v0, v0, [LX/7GT;

    sget-object v1, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    aput-object v1, v0, v3

    sget-object v1, LX/7GT;->CONTACTS_QUEUE_TYPE:LX/7GT;

    aput-object v1, v0, v4

    sget-object v1, LX/7GT;->VOIP_QUEUE_TYPE:LX/7GT;

    aput-object v1, v0, v5

    sget-object v1, LX/7GT;->PAYMENTS_QUEUE_TYPE:LX/7GT;

    aput-object v1, v0, v6

    sput-object v0, LX/7GT;->$VALUES:[LX/7GT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1189217
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1189218
    iput-object p3, p0, LX/7GT;->apiString:Ljava/lang/String;

    .line 1189219
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7GT;
    .locals 1

    .prologue
    .line 1189220
    const-class v0, LX/7GT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7GT;

    return-object v0
.end method

.method public static values()[LX/7GT;
    .locals 1

    .prologue
    .line 1189221
    sget-object v0, LX/7GT;->$VALUES:[LX/7GT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7GT;

    return-object v0
.end method
