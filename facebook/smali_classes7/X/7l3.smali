.class public final LX/7l3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

.field public c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1233214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1233215
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;)V
    .locals 1

    .prologue
    .line 1233201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1233202
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->a:Ljava/lang/String;

    iput-object v0, p0, LX/7l3;->a:Ljava/lang/String;

    .line 1233203
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    iput-object v0, p0, LX/7l3;->b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 1233204
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, LX/7l3;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1233205
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->d:Ljava/lang/String;

    iput-object v0, p0, LX/7l3;->d:Ljava/lang/String;

    .line 1233206
    iget-boolean v0, p1, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->e:Z

    iput-boolean v0, p0, LX/7l3;->e:Z

    .line 1233207
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->f:Ljava/lang/String;

    iput-object v0, p0, LX/7l3;->f:Ljava/lang/String;

    .line 1233208
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->g:Ljava/lang/String;

    iput-object v0, p0, LX/7l3;->g:Ljava/lang/String;

    .line 1233209
    iget-boolean v0, p1, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->h:Z

    iput-boolean v0, p0, LX/7l3;->h:Z

    .line 1233210
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->i:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    iput-object v0, p0, LX/7l3;->i:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    .line 1233211
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->j:Ljava/lang/String;

    iput-object v0, p0, LX/7l3;->j:Ljava/lang/String;

    .line 1233212
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->k:Ljava/lang/String;

    iput-object v0, p0, LX/7l3;->k:Ljava/lang/String;

    .line 1233213
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;)V
    .locals 1

    .prologue
    .line 1233189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1233190
    invoke-virtual {p1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7l3;->a:Ljava/lang/String;

    .line 1233191
    invoke-virtual {p1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->m()Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    move-result-object v0

    iput-object v0, p0, LX/7l3;->b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 1233192
    invoke-virtual {p1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    :goto_0
    iput-object v0, p0, LX/7l3;->i:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    .line 1233193
    invoke-virtual {p1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/7l3;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1233194
    invoke-virtual {p1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7l3;->d:Ljava/lang/String;

    .line 1233195
    return-void

    .line 1233196
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;
    .locals 2

    .prologue
    .line 1233197
    iget-object v0, p0, LX/7l3;->a:Ljava/lang/String;

    const-string v1, "description should not be null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1233198
    iget-object v0, p0, LX/7l3;->b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    const-string v1, "life event type should not be null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1233199
    iget-object v0, p0, LX/7l3;->i:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    const-string v1, "education experience type should not be null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1233200
    new-instance v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    invoke-direct {v0, p0}, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;-><init>(LX/7l3;)V

    return-object v0
.end method
