.class public final LX/87g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$FetchCoverPicPayloadQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Azx;

.field public final synthetic b:LX/87h;


# direct methods
.method public constructor <init>(LX/87h;LX/Azx;)V
    .locals 0

    .prologue
    .line 1300512
    iput-object p1, p0, LX/87g;->b:LX/87h;

    iput-object p2, p0, LX/87g;->a:LX/Azx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1300513
    sget-object v1, LX/Azy;->a:Ljava/lang/String;

    const-string p0, "Failed to retrieve cover pic payload"

    invoke-static {v1, p0, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1300514
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1300515
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1300516
    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 1300517
    :goto_1
    return-void

    .line 1300518
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1300519
    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$FetchCoverPicPayloadQueryModel;

    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$FetchCoverPicPayloadQueryModel;->a()Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$FetchCoverPicPayloadQueryModel$AccountUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$FetchCoverPicPayloadQueryModel$AccountUserModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 1300520
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1300521
    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$FetchCoverPicPayloadQueryModel;

    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$FetchCoverPicPayloadQueryModel;->a()Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$FetchCoverPicPayloadQueryModel$AccountUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$FetchCoverPicPayloadQueryModel$AccountUserModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1300522
    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1300523
    iget-object v1, p0, LX/87g;->a:LX/Azx;

    .line 1300524
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1300525
    :cond_3
    :goto_2
    goto :goto_1

    .line 1300526
    :cond_4
    iget-object v2, v1, LX/Azx;->a:LX/Azy;

    iget-object v2, v2, LX/Azy;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BA0;

    iget-object v3, v1, LX/Azx;->a:LX/Azy;

    iget-object v3, v3, LX/Azy;->l:LX/Azp;

    invoke-virtual {v3}, LX/Azp;->getAspectRatio()F

    move-result v3

    invoke-virtual {v2, v0, v3}, LX/BA0;->a(Ljava/lang/String;F)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1300527
    new-instance v3, LX/Azw;

    invoke-direct {v3, v1}, LX/Azw;-><init>(LX/Azx;)V

    iget-object p0, v1, LX/Azx;->a:LX/Azy;

    iget-object p0, p0, LX/Azy;->g:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_2
.end method
