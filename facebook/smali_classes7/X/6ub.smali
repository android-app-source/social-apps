.class public final LX/6ub;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6uN;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6uN",
        "<",
        "Lcom/facebook/payments/confirmation/ConfirmationParams;",
        "Lcom/facebook/payments/confirmation/SimpleConfirmationData;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/6uO;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1156017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156018
    return-void
.end method

.method public static a(LX/0QB;)LX/6ub;
    .locals 3

    .prologue
    .line 1156019
    const-class v1, LX/6ub;

    monitor-enter v1

    .line 1156020
    :try_start_0
    sget-object v0, LX/6ub;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1156021
    sput-object v2, LX/6ub;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1156022
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1156023
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1156024
    new-instance v0, LX/6ub;

    invoke-direct {v0}, LX/6ub;-><init>()V

    .line 1156025
    move-object v0, v0

    .line 1156026
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1156027
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6ub;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1156028
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1156029
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/confirmation/ConfirmationParams;)Lcom/facebook/payments/confirmation/ConfirmationData;
    .locals 2

    .prologue
    .line 1156030
    new-instance v0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/facebook/payments/confirmation/SimpleConfirmationData;-><init>(Lcom/facebook/payments/confirmation/ConfirmationParams;Lcom/facebook/payments/confirmation/ProductConfirmationData;)V

    return-object v0
.end method

.method public final a(LX/6uO;)V
    .locals 0

    .prologue
    .line 1156031
    iput-object p1, p0, LX/6ub;->a:LX/6uO;

    .line 1156032
    return-void
.end method
