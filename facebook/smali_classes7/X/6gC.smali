.class public LX/6gC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetInterfaces$MomentsAppInvitationActionLinkFragment;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetInterfaces$MomentsAppInvitationActionLinkFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/6gD;)V
    .locals 1

    .prologue
    .line 1121446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1121447
    iget-object v0, p1, LX/6gD;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1121448
    iput-object v0, p0, LX/6gC;->a:Ljava/lang/String;

    .line 1121449
    iget-object v0, p1, LX/6gD;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1121450
    iput-object v0, p0, LX/6gC;->b:Ljava/lang/String;

    .line 1121451
    iget-object v0, p1, LX/6gD;->c:Ljava/util/List;

    move-object v0, v0

    .line 1121452
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/6gC;->c:LX/0Px;

    .line 1121453
    iget-object v0, p1, LX/6gD;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1121454
    iput-object v0, p0, LX/6gC;->d:Ljava/lang/String;

    .line 1121455
    iget-object v0, p1, LX/6gD;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1121456
    iput-object v0, p0, LX/6gC;->f:Ljava/lang/String;

    .line 1121457
    iget-object v0, p1, LX/6gD;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1121458
    iput-object v0, p0, LX/6gC;->e:Ljava/lang/String;

    .line 1121459
    iget-object v0, p1, LX/6gD;->g:LX/0Px;

    move-object v0, v0

    .line 1121460
    iput-object v0, p0, LX/6gC;->g:LX/0Px;

    .line 1121461
    iget-object v0, p1, LX/6gD;->h:LX/0Px;

    move-object v0, v0

    .line 1121462
    iput-object v0, p0, LX/6gC;->h:LX/0Px;

    .line 1121463
    return-void
.end method
