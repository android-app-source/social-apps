.class public final LX/6ul;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6F4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6F4",
        "<",
        "Lcom/facebook/payments/confirmation/SimpleConfirmationData;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/6pC;

.field public c:LX/6qh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6pC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1156139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156140
    iput-object p1, p0, LX/6ul;->a:Landroid/content/Context;

    .line 1156141
    iput-object p2, p0, LX/6ul;->b:LX/6pC;

    .line 1156142
    return-void
.end method

.method public static b(LX/0QB;)LX/6ul;
    .locals 3

    .prologue
    .line 1156137
    new-instance v2, LX/6ul;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/6pC;->a(LX/0QB;)LX/6pC;

    move-result-object v1

    check-cast v1, LX/6pC;

    invoke-direct {v2, v0, v1}, LX/6ul;-><init>(Landroid/content/Context;LX/6pC;)V

    .line 1156138
    return-object v2
.end method


# virtual methods
.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1156135
    iput-object p1, p0, LX/6ul;->c:LX/6qh;

    .line 1156136
    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/confirmation/ConfirmationData;)V
    .locals 0

    .prologue
    .line 1156134
    return-void
.end method

.method public final bridge synthetic onClick(Lcom/facebook/payments/confirmation/ConfirmationData;LX/6uH;)V
    .locals 0

    .prologue
    .line 1156133
    check-cast p1, Lcom/facebook/payments/confirmation/SimpleConfirmationData;

    invoke-virtual {p0, p1, p2}, LX/6ul;->onClick(Lcom/facebook/payments/confirmation/SimpleConfirmationData;LX/6uH;)V

    return-void
.end method

.method public final onClick(Lcom/facebook/payments/confirmation/SimpleConfirmationData;LX/6uH;)V
    .locals 3

    .prologue
    .line 1156124
    sget-object v0, LX/6uk;->a:[I

    invoke-interface {p2}, LX/6uG;->d()LX/6uT;

    move-result-object v1

    invoke-virtual {v1}, LX/6uT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1156125
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, LX/6uG;->d()LX/6uT;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1156126
    :pswitch_0
    check-cast p2, LX/6uZ;

    .line 1156127
    const-string v0, "https://secure.m.facebook.com/settings?tab=payments&id=%s"

    iget-object v1, p2, LX/6uZ;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1156128
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 1156129
    iget-object v1, p0, LX/6ul;->c:LX/6qh;

    invoke-virtual {v1, v0}, LX/6qh;->b(Landroid/content/Intent;)V

    .line 1156130
    :goto_0
    return-void

    .line 1156131
    :pswitch_1
    iget-object v0, p0, LX/6ul;->c:LX/6qh;

    iget-object v1, p0, LX/6ul;->a:Landroid/content/Context;

    sget-object v2, LX/6pF;->CREATE:LX/6pF;

    invoke-static {v2}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->a(LX/6pF;)Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->a(Landroid/content/Context;Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/6qh;->a(Landroid/content/Intent;I)V

    .line 1156132
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
