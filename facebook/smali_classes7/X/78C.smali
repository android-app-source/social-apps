.class public final enum LX/78C;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/78C;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/78C;

.field public static final enum INSTALL_APP:LX/78C;

.field public static final enum UNKNOWN:LX/78C;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1172482
    new-instance v0, LX/78C;

    const-string v1, "INSTALL_APP"

    invoke-direct {v0, v1, v2}, LX/78C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/78C;->INSTALL_APP:LX/78C;

    .line 1172483
    new-instance v0, LX/78C;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/78C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/78C;->UNKNOWN:LX/78C;

    .line 1172484
    const/4 v0, 0x2

    new-array v0, v0, [LX/78C;

    sget-object v1, LX/78C;->INSTALL_APP:LX/78C;

    aput-object v1, v0, v2

    sget-object v1, LX/78C;->UNKNOWN:LX/78C;

    aput-object v1, v0, v3

    sput-object v0, LX/78C;->$VALUES:[LX/78C;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1172485
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/78C;
    .locals 1

    .prologue
    .line 1172486
    if-nez p0, :cond_0

    .line 1172487
    sget-object v0, LX/78C;->UNKNOWN:LX/78C;

    .line 1172488
    :goto_0
    return-object v0

    .line 1172489
    :cond_0
    :try_start_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/78C;->valueOf(Ljava/lang/String;)LX/78C;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1172490
    :catch_0
    sget-object v0, LX/78C;->UNKNOWN:LX/78C;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/78C;
    .locals 1

    .prologue
    .line 1172491
    const-class v0, LX/78C;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/78C;

    return-object v0
.end method

.method public static values()[LX/78C;
    .locals 1

    .prologue
    .line 1172492
    sget-object v0, LX/78C;->$VALUES:[LX/78C;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/78C;

    return-object v0
.end method
