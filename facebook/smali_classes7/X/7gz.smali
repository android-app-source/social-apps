.class public final enum LX/7gz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gz;

.field public static final enum DELIVERED:LX/7gz;

.field public static final enum FAILED:LX/7gz;

.field public static final enum SEEN:LX/7gz;

.field public static final enum SENDING:LX/7gz;

.field public static final enum SENT:LX/7gz;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1225174
    new-instance v0, LX/7gz;

    const-string v1, "SEEN"

    invoke-direct {v0, v1, v2}, LX/7gz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gz;->SEEN:LX/7gz;

    .line 1225175
    new-instance v0, LX/7gz;

    const-string v1, "SENT"

    invoke-direct {v0, v1, v3}, LX/7gz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gz;->SENT:LX/7gz;

    .line 1225176
    new-instance v0, LX/7gz;

    const-string v1, "SENDING"

    invoke-direct {v0, v1, v4}, LX/7gz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gz;->SENDING:LX/7gz;

    .line 1225177
    new-instance v0, LX/7gz;

    const-string v1, "DELIVERED"

    invoke-direct {v0, v1, v5}, LX/7gz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gz;->DELIVERED:LX/7gz;

    .line 1225178
    new-instance v0, LX/7gz;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v6}, LX/7gz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7gz;->FAILED:LX/7gz;

    .line 1225179
    const/4 v0, 0x5

    new-array v0, v0, [LX/7gz;

    sget-object v1, LX/7gz;->SEEN:LX/7gz;

    aput-object v1, v0, v2

    sget-object v1, LX/7gz;->SENT:LX/7gz;

    aput-object v1, v0, v3

    sget-object v1, LX/7gz;->SENDING:LX/7gz;

    aput-object v1, v0, v4

    sget-object v1, LX/7gz;->DELIVERED:LX/7gz;

    aput-object v1, v0, v5

    sget-object v1, LX/7gz;->FAILED:LX/7gz;

    aput-object v1, v0, v6

    sput-object v0, LX/7gz;->$VALUES:[LX/7gz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1225180
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gz;
    .locals 1

    .prologue
    .line 1225181
    const-class v0, LX/7gz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gz;

    return-object v0
.end method

.method public static values()[LX/7gz;
    .locals 1

    .prologue
    .line 1225182
    sget-object v0, LX/7gz;->$VALUES:[LX/7gz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gz;

    return-object v0
.end method
