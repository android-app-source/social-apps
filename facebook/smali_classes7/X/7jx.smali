.class public final LX/7jx;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/commerce/publishing/graphql/CommerceStoreUpdateMutationModels$CommerceStoreUpdateMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1230641
    const-class v1, Lcom/facebook/commerce/publishing/graphql/CommerceStoreUpdateMutationModels$CommerceStoreUpdateMutationModel;

    const v0, 0x3af7d109

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "CommerceStoreUpdateMutation"

    const-string v6, "d11f2c4272b9e090726198dd8da31707"

    const-string v7, "commerce_store_update"

    const-string v8, "4"

    const-string v9, "10155069963316729"

    const/4 v10, 0x0

    .line 1230642
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1230643
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1230644
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1230645
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1230646
    sparse-switch v0, :sswitch_data_0

    .line 1230647
    :goto_0
    return-object p1

    .line 1230648
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1230649
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1230650
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1230651
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1230652
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1230653
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6ef528d5 -> :sswitch_0
        -0x2a0a3d40 -> :sswitch_3
        0x101fb19 -> :sswitch_2
        0x5fb57ca -> :sswitch_4
        0x683094a -> :sswitch_5
        0x1d1017c1 -> :sswitch_1
    .end sparse-switch
.end method
