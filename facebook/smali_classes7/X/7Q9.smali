.class public final LX/7Q9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;)V
    .locals 0

    .prologue
    .line 1203625
    iput-object p1, p0, LX/7Q9;->a:Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1203606
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1203607
    const/4 v0, 0x0

    .line 1203608
    :goto_0
    return v0

    .line 1203609
    :cond_0
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 1203610
    sget-object v1, LX/1mI;->d:LX/0Tn;

    invoke-virtual {v1}, LX/0Tn;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1203611
    iget-object v0, p0, LX/7Q9;->a:Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;

    sget-object v1, LX/1mC;->ON:LX/1mC;

    invoke-virtual {v0, v1}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a(LX/1mC;)V

    .line 1203612
    iget-object v0, p0, LX/7Q9;->a:Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;

    iget-object v1, p0, LX/7Q9;->a:Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;

    iget-object v1, v1, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->f:LX/4oi;

    .line 1203613
    invoke-static {v0, v1}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a$redex0(Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;Landroid/preference/Preference;)V

    .line 1203614
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1203615
    :cond_2
    sget-object v1, LX/1mI;->e:LX/0Tn;

    invoke-virtual {v1}, LX/0Tn;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1203616
    iget-object v0, p0, LX/7Q9;->a:Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;

    sget-object v1, LX/1mC;->WIFI_ONLY:LX/1mC;

    invoke-virtual {v0, v1}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a(LX/1mC;)V

    .line 1203617
    iget-object v0, p0, LX/7Q9;->a:Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;

    iget-object v1, p0, LX/7Q9;->a:Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;

    iget-object v1, v1, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->g:LX/4oi;

    .line 1203618
    invoke-static {v0, v1}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a$redex0(Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;Landroid/preference/Preference;)V

    .line 1203619
    goto :goto_1

    .line 1203620
    :cond_3
    sget-object v1, LX/1mI;->f:LX/0Tn;

    invoke-virtual {v1}, LX/0Tn;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1203621
    iget-object v0, p0, LX/7Q9;->a:Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;

    sget-object v1, LX/1mC;->OFF:LX/1mC;

    invoke-virtual {v0, v1}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a(LX/1mC;)V

    .line 1203622
    iget-object v0, p0, LX/7Q9;->a:Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;

    iget-object v1, p0, LX/7Q9;->a:Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;

    iget-object v1, v1, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->h:LX/4oi;

    .line 1203623
    invoke-static {v0, v1}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a$redex0(Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;Landroid/preference/Preference;)V

    .line 1203624
    goto :goto_1
.end method
