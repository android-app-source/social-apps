.class public LX/7K3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile m:LX/7K3;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/media/MediaPlayer;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0So;

.field public final d:Landroid/content/Context;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field public final f:LX/7K2;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1m0;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0ad;

.field private final i:LX/0Uh;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Q0;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0TD;

.field public final l:LX/0wq;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1194407
    const-class v0, LX/7K3;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7K3;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0So;Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0ad;LX/0Uh;LX/0Ot;LX/0TD;LX/0wq;)V
    .locals 2
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p9    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/VideoPerformanceExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/media/MediaPlayer;",
            ">;",
            "LX/0So;",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/1m0;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/7Q0;",
            ">;",
            "LX/0TD;",
            "LX/0wq;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1194408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194409
    iput-object p1, p0, LX/7K3;->b:LX/0Or;

    .line 1194410
    iput-object p2, p0, LX/7K3;->c:LX/0So;

    .line 1194411
    iput-object p3, p0, LX/7K3;->d:Landroid/content/Context;

    .line 1194412
    iput-object p5, p0, LX/7K3;->g:LX/0Ot;

    .line 1194413
    iput-object p6, p0, LX/7K3;->h:LX/0ad;

    .line 1194414
    iput-object p7, p0, LX/7K3;->i:LX/0Uh;

    .line 1194415
    iput-object p4, p0, LX/7K3;->e:Ljava/util/concurrent/ExecutorService;

    .line 1194416
    iput-object p10, p0, LX/7K3;->l:LX/0wq;

    .line 1194417
    new-instance v0, LX/7K2;

    iget v1, p10, LX/0wq;->t:I

    invoke-direct {v0, p0, v1}, LX/7K2;-><init>(LX/7K3;I)V

    iput-object v0, p0, LX/7K3;->f:LX/7K2;

    .line 1194418
    iput-object p8, p0, LX/7K3;->j:LX/0Ot;

    .line 1194419
    iput-object p9, p0, LX/7K3;->k:LX/0TD;

    .line 1194420
    return-void
.end method

.method public static a(LX/0QB;)LX/7K3;
    .locals 14

    .prologue
    .line 1194421
    sget-object v0, LX/7K3;->m:LX/7K3;

    if-nez v0, :cond_1

    .line 1194422
    const-class v1, LX/7K3;

    monitor-enter v1

    .line 1194423
    :try_start_0
    sget-object v0, LX/7K3;->m:LX/7K3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1194424
    if-eqz v2, :cond_0

    .line 1194425
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1194426
    new-instance v3, LX/7K3;

    const/16 v4, 0x21

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    const/16 v8, 0x1358

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    const/16 v11, 0x3802

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v0}, LX/19U;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, LX/0TD;

    invoke-static {v0}, LX/0wq;->b(LX/0QB;)LX/0wq;

    move-result-object v13

    check-cast v13, LX/0wq;

    invoke-direct/range {v3 .. v13}, LX/7K3;-><init>(LX/0Or;LX/0So;Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0ad;LX/0Uh;LX/0Ot;LX/0TD;LX/0wq;)V

    .line 1194427
    move-object v0, v3

    .line 1194428
    sput-object v0, LX/7K3;->m:LX/7K3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1194429
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1194430
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1194431
    :cond_1
    sget-object v0, LX/7K3;->m:LX/7K3;

    return-object v0

    .line 1194432
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1194433
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;LX/36s;)LX/7Jr;
    .locals 10

    .prologue
    .line 1194434
    new-instance v0, LX/7Jr;

    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    iget-object v1, p0, LX/7K3;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1m0;

    iget-object v4, p0, LX/7K3;->d:Landroid/content/Context;

    iget-object v6, p0, LX/7K3;->c:LX/0So;

    new-instance v7, LX/3FK;

    iget-object v1, p0, LX/7K3;->h:LX/0ad;

    iget-object v5, p0, LX/7K3;->i:LX/0Uh;

    invoke-direct {v7, v1, v5}, LX/3FK;-><init>(LX/0ad;LX/0Uh;)V

    iget-object v1, p0, LX/7K3;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/7Q0;

    iget-object v9, p0, LX/7K3;->k:LX/0TD;

    move-object v1, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v9}, LX/7Jr;-><init>(LX/36s;Landroid/media/MediaPlayer;LX/1m0;Landroid/content/Context;Landroid/net/Uri;LX/0So;LX/3FK;LX/7Q0;LX/0TD;)V

    return-object v0
.end method
