.class public final enum LX/7xB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7xB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7xB;

.field public static final enum DATE:LX/7xB;

.field public static final enum DATE_YEAR:LX/7xB;

.field public static final enum DAY:LX/7xB;

.field public static final enum DAY_ALT:LX/7xB;

.field public static final enum DAY_TWENTY_FOUR:LX/7xB;

.field public static final enum TOMORROW:LX/7xB;

.field public static final enum WEEK_DAY:LX/7xB;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1277061
    new-instance v0, LX/7xB;

    const-string v1, "DATE_YEAR"

    invoke-direct {v0, v1, v3}, LX/7xB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xB;->DATE_YEAR:LX/7xB;

    .line 1277062
    new-instance v0, LX/7xB;

    const-string v1, "DATE"

    invoke-direct {v0, v1, v4}, LX/7xB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xB;->DATE:LX/7xB;

    .line 1277063
    new-instance v0, LX/7xB;

    const-string v1, "WEEK_DAY"

    invoke-direct {v0, v1, v5}, LX/7xB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xB;->WEEK_DAY:LX/7xB;

    .line 1277064
    new-instance v0, LX/7xB;

    const-string v1, "TOMORROW"

    invoke-direct {v0, v1, v6}, LX/7xB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xB;->TOMORROW:LX/7xB;

    .line 1277065
    new-instance v0, LX/7xB;

    const-string v1, "DAY_TWENTY_FOUR"

    invoke-direct {v0, v1, v7}, LX/7xB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xB;->DAY_TWENTY_FOUR:LX/7xB;

    .line 1277066
    new-instance v0, LX/7xB;

    const-string v1, "DAY_ALT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7xB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xB;->DAY_ALT:LX/7xB;

    .line 1277067
    new-instance v0, LX/7xB;

    const-string v1, "DAY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/7xB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7xB;->DAY:LX/7xB;

    .line 1277068
    const/4 v0, 0x7

    new-array v0, v0, [LX/7xB;

    sget-object v1, LX/7xB;->DATE_YEAR:LX/7xB;

    aput-object v1, v0, v3

    sget-object v1, LX/7xB;->DATE:LX/7xB;

    aput-object v1, v0, v4

    sget-object v1, LX/7xB;->WEEK_DAY:LX/7xB;

    aput-object v1, v0, v5

    sget-object v1, LX/7xB;->TOMORROW:LX/7xB;

    aput-object v1, v0, v6

    sget-object v1, LX/7xB;->DAY_TWENTY_FOUR:LX/7xB;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7xB;->DAY_ALT:LX/7xB;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7xB;->DAY:LX/7xB;

    aput-object v2, v0, v1

    sput-object v0, LX/7xB;->$VALUES:[LX/7xB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1277071
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7xB;
    .locals 1

    .prologue
    .line 1277070
    const-class v0, LX/7xB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7xB;

    return-object v0
.end method

.method public static values()[LX/7xB;
    .locals 1

    .prologue
    .line 1277069
    sget-object v0, LX/7xB;->$VALUES:[LX/7xB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7xB;

    return-object v0
.end method
