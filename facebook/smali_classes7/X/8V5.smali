.class public final LX/8V5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8Ss;

.field public final synthetic b:LX/8V7;


# direct methods
.method public constructor <init>(LX/8V7;LX/8Ss;)V
    .locals 0

    .prologue
    .line 1351801
    iput-object p1, p0, LX/8V5;->b:LX/8V7;

    iput-object p2, p0, LX/8V5;->a:LX/8Ss;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1351792
    iget-object v0, p0, LX/8V5;->a:LX/8Ss;

    invoke-virtual {v0, p1}, LX/8Ss;->a(Ljava/lang/Throwable;)V

    .line 1351793
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1351794
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1351795
    if-nez p1, :cond_0

    .line 1351796
    iget-object v0, p0, LX/8V5;->a:LX/8Ss;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Empty result"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/8Ss;->a(Ljava/lang/Throwable;)V

    .line 1351797
    :goto_0
    return-void

    .line 1351798
    :cond_0
    iget-object v1, p0, LX/8V5;->a:LX/8Ss;

    .line 1351799
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1351800
    check-cast v0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;

    invoke-virtual {v1, v0}, LX/8Ss;->a(Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;)V

    goto :goto_0
.end method
