.class public LX/74o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Rl",
        "<",
        "Lcom/facebook/ipc/media/MediaItem;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/74o;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1168559
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1168560
    return-void
.end method

.method public static a(LX/0QB;)LX/74o;
    .locals 3

    .prologue
    .line 1168547
    sget-object v0, LX/74o;->a:LX/74o;

    if-nez v0, :cond_1

    .line 1168548
    const-class v1, LX/74o;

    monitor-enter v1

    .line 1168549
    :try_start_0
    sget-object v0, LX/74o;->a:LX/74o;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1168550
    if-eqz v2, :cond_0

    .line 1168551
    :try_start_1
    new-instance v0, LX/74o;

    invoke-direct {v0}, LX/74o;-><init>()V

    .line 1168552
    move-object v0, v0

    .line 1168553
    sput-object v0, LX/74o;->a:LX/74o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1168554
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1168555
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1168556
    :cond_1
    sget-object v0, LX/74o;->a:LX/74o;

    return-object v0

    .line 1168557
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1168558
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/ipc/media/MediaItem;)Z
    .locals 2

    .prologue
    .line 1168545
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    .line 1168546
    if-eqz v0, :cond_0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic apply(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1168544
    check-cast p1, Lcom/facebook/ipc/media/MediaItem;

    invoke-static {p1}, LX/74o;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v0

    return v0
.end method
