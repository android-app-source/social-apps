.class public LX/8Oc;
.super LX/2Mj;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/8Oc;


# direct methods
.method public constructor <init>(LX/7Ss;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/upload/gatekeeper/VideoUploadResizeEstimatorEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Ss;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1339740
    invoke-direct {p0, p1, p2}, LX/2Mj;-><init>(LX/2Md;LX/0Or;)V

    .line 1339741
    return-void
.end method

.method public static a(LX/0QB;)LX/8Oc;
    .locals 5

    .prologue
    .line 1339742
    sget-object v0, LX/8Oc;->a:LX/8Oc;

    if-nez v0, :cond_1

    .line 1339743
    const-class v1, LX/8Oc;

    monitor-enter v1

    .line 1339744
    :try_start_0
    sget-object v0, LX/8Oc;->a:LX/8Oc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1339745
    if-eqz v2, :cond_0

    .line 1339746
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1339747
    new-instance v4, LX/8Oc;

    invoke-static {v0}, LX/7Ss;->a(LX/0QB;)LX/7Ss;

    move-result-object v3

    check-cast v3, LX/7Ss;

    const/16 p0, 0x1552

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/8Oc;-><init>(LX/7Ss;LX/0Or;)V

    .line 1339748
    move-object v0, v4

    .line 1339749
    sput-object v0, LX/8Oc;->a:LX/8Oc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1339750
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1339751
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1339752
    :cond_1
    sget-object v0, LX/8Oc;->a:LX/8Oc;

    return-object v0

    .line 1339753
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1339754
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
