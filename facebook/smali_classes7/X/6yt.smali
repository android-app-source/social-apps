.class public LX/6yt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/6yt;


# instance fields
.field public final a:LX/6yv;

.field public final b:LX/6yw;

.field public final c:LX/6yx;


# direct methods
.method public constructor <init>(LX/6yv;LX/6yw;LX/6yx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1160547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160548
    iput-object p1, p0, LX/6yt;->a:LX/6yv;

    .line 1160549
    iput-object p2, p0, LX/6yt;->b:LX/6yw;

    .line 1160550
    iput-object p3, p0, LX/6yt;->c:LX/6yx;

    .line 1160551
    return-void
.end method

.method public static a(LX/0QB;)LX/6yt;
    .locals 6

    .prologue
    .line 1160552
    sget-object v0, LX/6yt;->d:LX/6yt;

    if-nez v0, :cond_1

    .line 1160553
    const-class v1, LX/6yt;

    monitor-enter v1

    .line 1160554
    :try_start_0
    sget-object v0, LX/6yt;->d:LX/6yt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1160555
    if-eqz v2, :cond_0

    .line 1160556
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1160557
    new-instance p0, LX/6yt;

    invoke-static {v0}, LX/6yv;->b(LX/0QB;)LX/6yv;

    move-result-object v3

    check-cast v3, LX/6yv;

    invoke-static {v0}, LX/6yw;->b(LX/0QB;)LX/6yw;

    move-result-object v4

    check-cast v4, LX/6yw;

    invoke-static {v0}, LX/6yx;->b(LX/0QB;)LX/6yx;

    move-result-object v5

    check-cast v5, LX/6yx;

    invoke-direct {p0, v3, v4, v5}, LX/6yt;-><init>(LX/6yv;LX/6yw;LX/6yx;)V

    .line 1160558
    move-object v0, p0

    .line 1160559
    sput-object v0, LX/6yt;->d:LX/6yt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1160560
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1160561
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1160562
    :cond_1
    sget-object v0, LX/6yt;->d:LX/6yt;

    return-object v0

    .line 1160563
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1160564
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
