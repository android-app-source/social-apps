.class public LX/88q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LX/88p;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field public final c:LX/0Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ve",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final d:LX/88p;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1302973
    sget-object v0, LX/88p;->INITIAL_FEED_NEWER:LX/88p;

    sget-object v1, LX/88p;->INITIAL_FEED_OLDER:LX/88p;

    sget-object v2, LX/88p;->BASIC_INFO:LX/88p;

    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, LX/88q;->a:Ljava/util/EnumSet;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Callable;LX/0Ve;LX/88p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;>;",
            "LX/0Ve",
            "<TT;>;",
            "LX/88p;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1302974
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1302975
    iput-object p1, p0, LX/88q;->b:Ljava/util/concurrent/Callable;

    .line 1302976
    iput-object p2, p0, LX/88q;->c:LX/0Ve;

    .line 1302977
    iput-object p3, p0, LX/88q;->d:LX/88p;

    .line 1302978
    return-void
.end method
