.class public final LX/8Hd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1321465
    iput-object p1, p0, LX/8Hd;->a:Landroid/view/View;

    iput-object p2, p0, LX/8Hd;->b:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1321466
    iget-object v0, p0, LX/8Hd;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, LX/8Hd;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lez v0, :cond_1

    .line 1321467
    iget-object v0, p0, LX/8Hd;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1321468
    iget-object v0, p0, LX/8Hd;->a:Landroid/view/View;

    invoke-static {v0, p0}, LX/8He;->b(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1321469
    :cond_0
    :goto_0
    return-void

    .line 1321470
    :cond_1
    iget-object v0, p0, LX/8Hd;->a:Landroid/view/View;

    .line 1321471
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 1321472
    :goto_1
    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_2

    .line 1321473
    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_1

    .line 1321474
    :cond_2
    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 1321475
    if-nez v0, :cond_0

    .line 1321476
    iget-object v0, p0, LX/8Hd;->a:Landroid/view/View;

    invoke-static {v0, p0}, LX/8He;->b(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method
