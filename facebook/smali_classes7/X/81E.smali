.class public LX/81E;
.super LX/0rn;
.source ""


# instance fields
.field private final d:LX/0rq;

.field private final e:LX/0w9;


# direct methods
.method public constructor <init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0rq;LX/0w9;LX/0ad;LX/0sZ;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1285500
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-direct/range {v0 .. v7}, LX/0rn;-><init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0ad;LX/0sZ;)V

    .line 1285501
    iput-object p6, p0, LX/81E;->d:LX/0rq;

    .line 1285502
    iput-object p7, p0, LX/81E;->e:LX/0w9;

    .line 1285503
    return-void
.end method

.method private static a(Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 1285453
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1285454
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;->a()Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;

    move-result-object v3

    .line 1285455
    invoke-virtual {v3}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;

    .line 1285456
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    invoke-static {v6}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v6

    .line 1285457
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1285458
    const v7, 0x3b9aca00

    sub-int v0, v7, v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v7, 0xa

    const/16 v8, 0x23

    .line 1285459
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1285460
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v10

    if-lt v10, v7, :cond_1

    .line 1285461
    :goto_1
    move-object v0, v0

    .line 1285462
    new-instance v7, LX/1u8;

    invoke-direct {v7}, LX/1u8;-><init>()V

    invoke-virtual {v6}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    .line 1285463
    iput-object v6, v7, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 1285464
    move-object v6, v7

    .line 1285465
    iput-object v9, v6, LX/1u8;->d:Ljava/lang/String;

    .line 1285466
    move-object v6, v6

    .line 1285467
    iput-object v0, v6, LX/1u8;->i:Ljava/lang/String;

    .line 1285468
    move-object v0, v6

    .line 1285469
    iput-object v9, v0, LX/1u8;->c:Ljava/lang/String;

    .line 1285470
    move-object v0, v0

    .line 1285471
    invoke-virtual {v0}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    .line 1285472
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1285473
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1285474
    :cond_0
    invoke-virtual {v3}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    .line 1285475
    new-instance v1, LX/17L;

    invoke-direct {v1}, LX/17L;-><init>()V

    invoke-interface {v0}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v3

    .line 1285476
    iput-object v3, v1, LX/17L;->f:Ljava/lang/String;

    .line 1285477
    move-object v1, v1

    .line 1285478
    invoke-interface {v0}, LX/0us;->a()Ljava/lang/String;

    move-result-object v3

    .line 1285479
    iput-object v3, v1, LX/17L;->c:Ljava/lang/String;

    .line 1285480
    move-object v1, v1

    .line 1285481
    invoke-interface {v0}, LX/0us;->c()Z

    move-result v3

    .line 1285482
    iput-boolean v3, v1, LX/17L;->e:Z

    .line 1285483
    move-object v1, v1

    .line 1285484
    invoke-interface {v0}, LX/0us;->b()Z

    move-result v3

    .line 1285485
    iput-boolean v3, v1, LX/17L;->d:Z

    .line 1285486
    move-object v1, v1

    .line 1285487
    invoke-virtual {v1}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    move-object v0, v1

    .line 1285488
    new-instance v1, LX/0uq;

    invoke-direct {v1}, LX/0uq;-><init>()V

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1285489
    iput-object v2, v1, LX/0uq;->d:LX/0Px;

    .line 1285490
    move-object v1, v1

    .line 1285491
    iput-object v0, v1, LX/0uq;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1285492
    move-object v0, v1

    .line 1285493
    invoke-virtual {v0}, LX/0uq;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    return-object v0

    .line 1285494
    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1285495
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v10

    :goto_2
    if-ge v10, v7, :cond_2

    .line 1285496
    invoke-virtual {p0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1285497
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 1285498
    :cond_2
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1285499
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private e(Lcom/facebook/api/feed/FetchFeedParams;)LX/0gW;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feed/FetchFeedParams;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1285439
    new-instance v0, LX/829;

    invoke-direct {v0}, LX/829;-><init>()V

    move-object v0, v0

    .line 1285440
    invoke-static {v0}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 1285441
    const/4 v1, 0x0

    const-string v2, "after_cursor"

    invoke-static {v0, p1, v1, v2}, LX/0w9;->a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1285442
    iget-object v1, p0, LX/81E;->e:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 1285443
    iget-object v1, p0, LX/81E;->e:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->c(LX/0gW;)LX/0gW;

    .line 1285444
    invoke-static {v0}, LX/0w9;->d(LX/0gW;)LX/0gW;

    .line 1285445
    const-string v1, "media_type"

    iget-object v2, p0, LX/81E;->d:LX/0rq;

    invoke-virtual {v2}, LX/0rq;->a()LX/0wF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1285446
    if-eqz p1, :cond_0

    .line 1285447
    const-string v1, "reaction_story_id"

    .line 1285448
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 1285449
    invoke-virtual {v2}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "result_count"

    .line 1285450
    iget v3, p1, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v3, v3

    .line 1285451
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1285452
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;LX/0tW;)LX/0v6;
    .locals 3

    .prologue
    .line 1285421
    new-instance v0, LX/0v6;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_single"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 1285422
    invoke-direct {p0, p1}, LX/81E;->e(Lcom/facebook/api/feed/FetchFeedParams;)LX/0gW;

    move-result-object v1

    .line 1285423
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 1285424
    invoke-virtual {v0, v1}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v1

    .line 1285425
    const-string v2, "feed_subscriber"

    invoke-interface {p3, v2}, LX/0tW;->a(Ljava/lang/String;)LX/0rl;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 1285426
    return-object v0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;LX/15w;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    .locals 3

    .prologue
    .line 1285437
    iget-object v0, p0, LX/0ro;->a:LX/0sO;

    const-class v1, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;

    const-string v2, "fetch_reaction_attachments"

    invoke-virtual {v0, p2, v1, v2}, LX/0sO;->a(LX/15w;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;

    .line 1285438
    invoke-static {v0}, LX/81E;->a(Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    .locals 3

    .prologue
    .line 1285432
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1285433
    instance-of v1, v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;

    if-eqz v1, :cond_0

    .line 1285434
    check-cast v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;

    .line 1285435
    invoke-static {v0}, LX/81E;->a(Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    return-object v0

    .line 1285436
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized GraphQLResult:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1285431
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1285430
    const-string v0, "reaction_feed"

    return-object v0
.end method

.method public final c(Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1285429
    const-string v0, "ReactionFeedNetworkTime"

    return-object v0
.end method

.method public final d(Lcom/facebook/api/feed/FetchFeedParams;)I
    .locals 1

    .prologue
    .line 1285428
    const v0, 0xa008b

    return v0
.end method

.method public final synthetic f(Ljava/lang/Object;)LX/0gW;
    .locals 1

    .prologue
    .line 1285427
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-direct {p0, p1}, LX/81E;->e(Lcom/facebook/api/feed/FetchFeedParams;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
