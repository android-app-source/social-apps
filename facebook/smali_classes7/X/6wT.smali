.class public LX/6wT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/6wT;


# instance fields
.field public final a:LX/0TD;

.field public final b:LX/6wW;

.field public final c:LX/6wY;

.field public final d:LX/6wX;

.field public final e:LX/6wZ;

.field public final f:LX/6wa;

.field public final g:LX/6wb;


# direct methods
.method public constructor <init>(LX/0TD;LX/6wW;LX/6wY;LX/6wa;LX/6wX;LX/6wZ;LX/6wb;)V
    .locals 0
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1157661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157662
    iput-object p1, p0, LX/6wT;->a:LX/0TD;

    .line 1157663
    iput-object p2, p0, LX/6wT;->b:LX/6wW;

    .line 1157664
    iput-object p3, p0, LX/6wT;->c:LX/6wY;

    .line 1157665
    iput-object p5, p0, LX/6wT;->d:LX/6wX;

    .line 1157666
    iput-object p6, p0, LX/6wT;->e:LX/6wZ;

    .line 1157667
    iput-object p4, p0, LX/6wT;->f:LX/6wa;

    .line 1157668
    iput-object p7, p0, LX/6wT;->g:LX/6wb;

    .line 1157669
    return-void
.end method

.method public static a(LX/0QB;)LX/6wT;
    .locals 11

    .prologue
    .line 1157670
    sget-object v0, LX/6wT;->h:LX/6wT;

    if-nez v0, :cond_1

    .line 1157671
    const-class v1, LX/6wT;

    monitor-enter v1

    .line 1157672
    :try_start_0
    sget-object v0, LX/6wT;->h:LX/6wT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1157673
    if-eqz v2, :cond_0

    .line 1157674
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1157675
    new-instance v3, LX/6wT;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, LX/6wW;->b(LX/0QB;)LX/6wW;

    move-result-object v5

    check-cast v5, LX/6wW;

    invoke-static {v0}, LX/6wY;->b(LX/0QB;)LX/6wY;

    move-result-object v6

    check-cast v6, LX/6wY;

    invoke-static {v0}, LX/6wa;->b(LX/0QB;)LX/6wa;

    move-result-object v7

    check-cast v7, LX/6wa;

    invoke-static {v0}, LX/6wX;->b(LX/0QB;)LX/6wX;

    move-result-object v8

    check-cast v8, LX/6wX;

    invoke-static {v0}, LX/6wZ;->b(LX/0QB;)LX/6wZ;

    move-result-object v9

    check-cast v9, LX/6wZ;

    invoke-static {v0}, LX/6wb;->b(LX/0QB;)LX/6wb;

    move-result-object v10

    check-cast v10, LX/6wb;

    invoke-direct/range {v3 .. v10}, LX/6wT;-><init>(LX/0TD;LX/6wW;LX/6wY;LX/6wa;LX/6wX;LX/6wZ;LX/6wb;)V

    .line 1157676
    move-object v0, v3

    .line 1157677
    sput-object v0, LX/6wT;->h:LX/6wT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1157678
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1157679
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1157680
    :cond_1
    sget-object v0, LX/6wT;->h:LX/6wT;

    return-object v0

    .line 1157681
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1157682
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/6vb;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6vb;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1157683
    sget-object v0, LX/6wS;->a:[I

    invoke-virtual {p1}, LX/6vb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1157684
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1157685
    :pswitch_0
    iget-object v0, p0, LX/6wT;->f:LX/6wa;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/6sU;->a(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1157686
    new-instance v1, LX/6wQ;

    invoke-direct {v1, p0}, LX/6wQ;-><init>(LX/6wT;)V

    iget-object v2, p0, LX/6wT;->a:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 1157687
    :goto_0
    return-object v0

    .line 1157688
    :pswitch_1
    iget-object v0, p0, LX/6wT;->g:LX/6wb;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/6sU;->a(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1157689
    new-instance v1, LX/6wR;

    invoke-direct {v1, p0}, LX/6wR;-><init>(LX/6wT;)V

    iget-object v2, p0, LX/6wT;->a:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 1157690
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
