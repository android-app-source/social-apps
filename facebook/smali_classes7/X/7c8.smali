.class public final LX/7c8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/7cJ;


# direct methods
.method public constructor <init>(LX/7cJ;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7cJ;

    iput-object v0, p0, LX/7c8;->a:LX/7cJ;

    return-void
.end method


# virtual methods
.method public final a(D)V
    .locals 3

    :try_start_0
    iget-object v0, p0, LX/7c8;->a:LX/7cJ;

    invoke-interface {v0, p1, p2}, LX/7cJ;->a(D)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, LX/7c8;->a:LX/7cJ;

    invoke-interface {v0, p1}, LX/7cJ;->a(Lcom/google/android/gms/maps/model/LatLng;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Z)V
    .locals 2

    :try_start_0
    iget-object v0, p0, LX/7c8;->a:LX/7cJ;

    invoke-interface {v0, p1}, LX/7cJ;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, LX/7c8;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v0, p0, LX/7c8;->a:LX/7cJ;

    check-cast p1, LX/7c8;

    iget-object v1, p1, LX/7c8;->a:LX/7cJ;

    invoke-interface {v0, v1}, LX/7cJ;->a(LX/7cJ;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final hashCode()I
    .locals 2

    :try_start_0
    iget-object v0, p0, LX/7c8;->a:LX/7cJ;

    invoke-interface {v0}, LX/7cJ;->j()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
