.class public final LX/84M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/84N;


# direct methods
.method public constructor <init>(LX/84N;)V
    .locals 0

    .prologue
    .line 1290901
    iput-object p1, p0, LX/84M;->a:LX/84N;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1290902
    iget-object v0, p0, LX/84M;->a:LX/84N;

    iget-object v0, v0, LX/84N;->c:LX/2hX;

    iget-object v1, p0, LX/84M;->a:LX/84N;

    iget-wide v2, v1, LX/84N;->a:J

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, LX/2hX;->b(LX/2hX;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 1290903
    iget-object v0, p0, LX/84M;->a:LX/84N;

    iget-object v0, v0, LX/84N;->c:LX/2hX;

    iget-object v0, v0, LX/2hY;->b:LX/2hZ;

    invoke-virtual {v0, p1}, LX/2hZ;->a(Ljava/lang/Throwable;)V

    .line 1290904
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1290905
    iget-object v0, p0, LX/84M;->a:LX/84N;

    iget-object v0, v0, LX/84N;->c:LX/2hX;

    iget-object v1, p0, LX/84M;->a:LX/84N;

    iget-wide v2, v1, LX/84N;->a:J

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, LX/2hX;->b(LX/2hX;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 1290906
    return-void
.end method
