.class public LX/6z7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wl;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/6yZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1160782
    const-class v0, LX/6z7;

    sput-object v0, LX/6z7;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/6yZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1160783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160784
    iput-object p1, p0, LX/6z7;->b:LX/6yZ;

    .line 1160785
    return-void
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 6
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1160786
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    move v2, v1

    move v0, v1

    .line 1160787
    :goto_0
    if-ge v2, v3, :cond_2

    .line 1160788
    add-int/lit8 v4, v3, -0x1

    sub-int/2addr v4, v2

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 1160789
    add-int/lit8 v4, v4, -0x30

    .line 1160790
    rem-int/lit8 v5, v2, 0x2

    if-nez v5, :cond_0

    .line 1160791
    add-int/2addr v0, v4

    .line 1160792
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1160793
    :cond_0
    mul-int/lit8 v4, v4, 0x2

    .line 1160794
    const/16 v5, 0x9

    if-le v4, v5, :cond_1

    .line 1160795
    add-int/lit8 v4, v4, -0xa

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v0, v4

    goto :goto_1

    .line 1160796
    :cond_1
    add-int/2addr v0, v4

    goto :goto_1

    .line 1160797
    :cond_2
    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_3
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/6z8;)Z
    .locals 8

    .prologue
    .line 1160798
    move-object v0, p1

    check-cast v0, LX/6z9;

    .line 1160799
    invoke-virtual {v0}, LX/6z9;->a()Ljava/lang/String;

    move-result-object v0

    .line 1160800
    const/16 v7, 0x10

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1160801
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1160802
    :cond_0
    :goto_0
    move v0, v1

    .line 1160803
    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, LX/6z7;->c(LX/6z8;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1160804
    :cond_2
    invoke-static {v0}, LX/6yU;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1160805
    invoke-static {v3}, LX/6yU;->a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v4

    .line 1160806
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    .line 1160807
    sget-object v6, LX/6z6;->a:[I

    invoke-virtual {v4}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->ordinal()I

    move-result v4

    aget v4, v6, v4

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 1160808
    :pswitch_0
    const/16 v4, 0xf

    if-ne v5, v4, :cond_0

    invoke-static {v3}, LX/6z7;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    .line 1160809
    :pswitch_1
    if-ne v5, v7, :cond_0

    invoke-static {v3}, LX/6z7;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    .line 1160810
    :pswitch_2
    if-ne v5, v7, :cond_0

    invoke-static {v3}, LX/6z7;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    .line 1160811
    :pswitch_3
    if-ne v5, v7, :cond_0

    invoke-static {v3}, LX/6z7;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    .line 1160812
    :pswitch_4
    if-ne v5, v7, :cond_0

    invoke-static {v3}, LX/6z7;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b(LX/6z8;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1160813
    check-cast p1, LX/6z9;

    .line 1160814
    iget-object v0, p1, LX/6z9;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    move-object v0, v0

    .line 1160815
    iget-object v1, p0, LX/6z7;->b:LX/6yZ;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v1, v2}, LX/6yZ;->c(LX/6yO;)LX/6y0;

    move-result-object v1

    .line 1160816
    invoke-interface {v1, v0}, LX/6y0;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/6z8;)Z
    .locals 4

    .prologue
    .line 1160817
    check-cast p1, LX/6z9;

    .line 1160818
    invoke-virtual {p1}, LX/6z9;->a()Ljava/lang/String;

    move-result-object v0

    .line 1160819
    invoke-static {v0}, LX/6yU;->a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v0

    .line 1160820
    iget-object v1, p1, LX/6z9;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    move-object v1, v1

    .line 1160821
    iget-object v2, p0, LX/6z7;->b:LX/6yZ;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v2, v3}, LX/6yZ;->c(LX/6yO;)LX/6y0;

    move-result-object v2

    .line 1160822
    invoke-interface {v2, v0, v1}, LX/6y0;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    return v0
.end method
