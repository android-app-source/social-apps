.class public LX/7Ql;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Qf;


# instance fields
.field public final a:LX/7Qm;

.field public b:J

.field public c:Lcom/facebook/graphql/model/FeedUnit;

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0lF;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;J)V
    .locals 9
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1204770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204771
    new-instance v2, LX/7Qm;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    move-object v8, p6

    invoke-direct/range {v2 .. v8}, LX/7Qm;-><init>(Ljava/lang/String;LX/0lF;Ljava/lang/String;IILjava/lang/String;)V

    iput-object v2, p0, LX/7Ql;->a:LX/7Qm;

    .line 1204772
    move-object/from16 v0, p7

    iput-object v0, p0, LX/7Ql;->c:Lcom/facebook/graphql/model/FeedUnit;

    .line 1204773
    move-object/from16 v0, p8

    iput-object v0, p0, LX/7Ql;->d:Ljava/lang/String;

    .line 1204774
    move-wide/from16 v0, p9

    iput-wide v0, p0, LX/7Ql;->b:J

    .line 1204775
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 1204776
    iget-object v0, p0, LX/7Ql;->a:LX/7Qm;

    invoke-virtual {v0, p1}, LX/7Qm;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1204777
    iget-object v0, p0, LX/7Ql;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1204778
    sget-object v0, LX/0JS;->BROADCAST_STATUS_FIELD:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget-object v1, p0, LX/7Ql;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1204779
    :cond_0
    return-void
.end method
