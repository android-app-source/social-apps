.class public LX/7C0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public final e:Landroid/net/Uri;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Z

.field public final i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

.field public final j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final k:Z

.field public final l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

.field public final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Z

.field public o:D

.field public p:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/7Bz;)V
    .locals 4

    .prologue
    .line 1179712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1179713
    iget-object v0, p1, LX/7Bz;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1179714
    iput-object v0, p0, LX/7C0;->a:Ljava/lang/String;

    .line 1179715
    iget-object v0, p1, LX/7Bz;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1179716
    iput-object v0, p0, LX/7C0;->b:Ljava/lang/String;

    .line 1179717
    iget-object v0, p1, LX/7Bz;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1179718
    iput-object v0, p0, LX/7C0;->c:Ljava/lang/String;

    .line 1179719
    iget-object v0, p1, LX/7Bz;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 1179720
    iput-object v0, p0, LX/7C0;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1179721
    iget-object v0, p1, LX/7Bz;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 1179722
    iput-object v0, p0, LX/7C0;->e:Landroid/net/Uri;

    .line 1179723
    iget-object v0, p1, LX/7Bz;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1179724
    iput-object v0, p0, LX/7C0;->g:Ljava/lang/String;

    .line 1179725
    iget-object v0, p1, LX/7Bz;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1179726
    iput-object v0, p0, LX/7C0;->f:Ljava/lang/String;

    .line 1179727
    iget-boolean v0, p1, LX/7Bz;->h:Z

    move v0, v0

    .line 1179728
    iput-boolean v0, p0, LX/7C0;->h:Z

    .line 1179729
    iget-object v0, p1, LX/7Bz;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object v0, v0

    .line 1179730
    iput-object v0, p0, LX/7C0;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1179731
    iget-object v0, p1, LX/7Bz;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v0, v0

    .line 1179732
    iput-object v0, p0, LX/7C0;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1179733
    iget-boolean v0, p1, LX/7Bz;->k:Z

    move v0, v0

    .line 1179734
    iput-boolean v0, p0, LX/7C0;->k:Z

    .line 1179735
    iget-object v0, p1, LX/7Bz;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-object v0, v0

    .line 1179736
    iput-object v0, p0, LX/7C0;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1179737
    iget-object v0, p1, LX/7Bz;->m:LX/0Px;

    move-object v0, v0

    .line 1179738
    iput-object v0, p0, LX/7C0;->m:LX/0Px;

    .line 1179739
    iget-wide v2, p1, LX/7Bz;->n:D

    move-wide v0, v2

    .line 1179740
    iput-wide v0, p0, LX/7C0;->o:D

    .line 1179741
    iget-object v0, p1, LX/7Bz;->o:Ljava/lang/String;

    move-object v0, v0

    .line 1179742
    iput-object v0, p0, LX/7C0;->p:Ljava/lang/String;

    .line 1179743
    iget-boolean v0, p1, LX/7Bz;->p:Z

    move v0, v0

    .line 1179744
    iput-boolean v0, p0, LX/7C0;->n:Z

    .line 1179745
    return-void
.end method

.method public static q(LX/7C0;)V
    .locals 7

    .prologue
    .line 1179746
    iget-object v0, p0, LX/7C0;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 1179747
    if-eqz v0, :cond_0

    .line 1179748
    iget-object v0, p0, LX/7C0;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 1179749
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 1179750
    :cond_0
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    const-string v2, "Missing object type"

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1179751
    :cond_1
    iget-object v0, p0, LX/7C0;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 1179752
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    .line 1179753
    iget-object v1, p0, LX/7C0;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1179754
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1179755
    new-instance v1, LX/7C4;

    sget-object v2, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Missing id for entity of type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v1

    .line 1179756
    :cond_2
    iget-object v1, p0, LX/7C0;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1179757
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1179758
    new-instance v1, LX/7C4;

    sget-object v2, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Missing name for id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1179759
    iget-object v4, p0, LX/7C0;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1179760
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v1

    .line 1179761
    :cond_3
    iget-wide v5, p0, LX/7C0;->o:D

    move-wide v0, v5

    .line 1179762
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_4

    .line 1179763
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing costs for id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1179764
    iget-object v3, p0, LX/7C0;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1179765
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1179766
    :cond_4
    iget-object v0, p0, LX/7C0;->m:LX/0Px;

    move-object v0, v0

    .line 1179767
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1179768
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing or empty name search tokens for id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1179769
    iget-object v3, p0, LX/7C0;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1179770
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1179771
    :cond_5
    iget-object v0, p0, LX/7C0;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 1179772
    if-nez v0, :cond_6

    .line 1179773
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing profile pic uri for id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1179774
    iget-object v3, p0, LX/7C0;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1179775
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1179776
    :cond_6
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1179777
    instance-of v0, p1, LX/7C0;

    if-nez v0, :cond_0

    .line 1179778
    const/4 v0, 0x0

    .line 1179779
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/7C0;->a:Ljava/lang/String;

    check-cast p1, LX/7C0;

    iget-object v1, p1, LX/7C0;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1179780
    iget-object v0, p0, LX/7C0;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1179781
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BootstrapEntity["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1179782
    iget-object v1, p0, LX/7C0;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1179783
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
