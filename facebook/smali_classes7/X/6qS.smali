.class public final enum LX/6qS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6qS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6qS;

.field public static final enum JSON_ENCODED_CONFIG:LX/6qS;

.field public static final enum POJO_CONFIG:LX/6qS;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1150596
    new-instance v0, LX/6qS;

    const-string v1, "JSON_ENCODED_CONFIG"

    invoke-direct {v0, v1, v2}, LX/6qS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qS;->JSON_ENCODED_CONFIG:LX/6qS;

    .line 1150597
    new-instance v0, LX/6qS;

    const-string v1, "POJO_CONFIG"

    invoke-direct {v0, v1, v3}, LX/6qS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6qS;->POJO_CONFIG:LX/6qS;

    .line 1150598
    const/4 v0, 0x2

    new-array v0, v0, [LX/6qS;

    sget-object v1, LX/6qS;->JSON_ENCODED_CONFIG:LX/6qS;

    aput-object v1, v0, v2

    sget-object v1, LX/6qS;->POJO_CONFIG:LX/6qS;

    aput-object v1, v0, v3

    sput-object v0, LX/6qS;->$VALUES:[LX/6qS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1150599
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6qS;
    .locals 1

    .prologue
    .line 1150600
    const-class v0, LX/6qS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6qS;

    return-object v0
.end method

.method public static values()[LX/6qS;
    .locals 1

    .prologue
    .line 1150601
    sget-object v0, LX/6qS;->$VALUES:[LX/6qS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6qS;

    return-object v0
.end method
