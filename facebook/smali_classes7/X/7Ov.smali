.class public LX/7Ov;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Oi;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/7Oi;

.field private c:LX/7Ou;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1202338
    const-class v0, LX/7Ov;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Ov;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/7Ou;LX/7Oi;)V
    .locals 1

    .prologue
    .line 1202334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202335
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Oi;

    iput-object v0, p0, LX/7Ov;->b:LX/7Oi;

    .line 1202336
    iput-object p1, p0, LX/7Ov;->c:LX/7Ou;

    .line 1202337
    return-void
.end method


# virtual methods
.method public final a(LX/2WF;Ljava/io/OutputStream;)J
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 1202297
    new-instance v2, LX/1iJ;

    invoke-direct {v2, p2}, LX/1iJ;-><init>(Ljava/io/OutputStream;)V

    .line 1202298
    :goto_0
    :try_start_0
    iget-wide v0, p1, LX/2WF;->a:J

    .line 1202299
    iget-wide v11, v2, LX/1iJ;->a:J

    move-wide v4, v11

    .line 1202300
    add-long/2addr v4, v0

    iget-wide v0, p1, LX/2WF;->b:J

    cmp-long v0, v4, v0

    if-gez v0, :cond_4

    .line 1202301
    iget-object v0, p0, LX/7Ov;->c:LX/7Ou;

    if-nez v0, :cond_1

    .line 1202302
    new-instance v0, LX/2WF;

    iget-wide v6, p1, LX/2WF;->b:J

    invoke-direct {v0, v4, v5, v6, v7}, LX/2WF;-><init>(JJ)V

    .line 1202303
    iget-object v0, p0, LX/7Ov;->b:LX/7Oi;

    new-instance v1, LX/2WF;

    iget-wide v6, p1, LX/2WF;->b:J

    invoke-direct {v1, v4, v5, v6, v7}, LX/2WF;-><init>(JJ)V

    invoke-interface {v0, v1, v2}, LX/7Oi;->a(LX/2WF;Ljava/io/OutputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1202304
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/7Ov;->c:LX/7Ou;

    if-eqz v1, :cond_0

    .line 1202305
    iget-object v1, p0, LX/7Ov;->c:LX/7Ou;

    iget-object v1, v1, LX/7Ou;->b:LX/7Os;

    .line 1202306
    iget-object v2, v1, LX/7Os;->b:Ljava/io/InputStream;

    move-object v1, v2

    .line 1202307
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1202308
    iput-object v10, p0, LX/7Ov;->c:LX/7Ou;

    :cond_0
    throw v0

    .line 1202309
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/7Ov;->c:LX/7Ou;

    iget-object v0, v0, LX/7Ou;->a:LX/2WF;

    iget-wide v0, v0, LX/2WF;->a:J

    cmp-long v0, v4, v0

    if-gez v0, :cond_2

    .line 1202310
    new-instance v0, LX/2WF;

    iget-wide v6, p1, LX/2WF;->b:J

    iget-object v1, p0, LX/7Ov;->c:LX/7Ou;

    iget-object v1, v1, LX/7Ou;->a:LX/2WF;

    iget-wide v8, v1, LX/2WF;->a:J

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    invoke-direct {v0, v4, v5, v6, v7}, LX/2WF;-><init>(JJ)V

    .line 1202311
    iget-object v1, p0, LX/7Ov;->b:LX/7Oi;

    invoke-interface {v1, v0, v2}, LX/7Oi;->a(LX/2WF;Ljava/io/OutputStream;)J

    goto :goto_0

    .line 1202312
    :cond_2
    iget-object v0, p0, LX/7Ov;->c:LX/7Ou;

    iget-object v0, v0, LX/7Ou;->a:LX/2WF;

    invoke-virtual {v0, v4, v5}, LX/2WF;->a(J)Z

    move-result v0

    .line 1202313
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1202314
    if-eqz v0, :cond_3

    .line 1202315
    iget-object v0, p0, LX/7Ov;->c:LX/7Ou;

    iget-object v0, v0, LX/7Ou;->b:LX/7Os;

    .line 1202316
    iget-object v1, v0, LX/7Os;->b:Ljava/io/InputStream;

    move-object v1, v1

    .line 1202317
    iget-object v0, p0, LX/7Ov;->c:LX/7Ou;

    iget-object v0, v0, LX/7Ou;->a:LX/2WF;

    iget-wide v6, v0, LX/2WF;->a:J

    sub-long v6, v4, v6

    invoke-static {v1, v6, v7}, LX/0hW;->b(Ljava/io/InputStream;J)V

    .line 1202318
    iget-wide v6, p1, LX/2WF;->b:J

    iget-object v0, p0, LX/7Ov;->c:LX/7Ou;

    iget-object v0, v0, LX/7Ou;->a:LX/2WF;

    iget-wide v8, v0, LX/2WF;->b:J

    cmp-long v0, v6, v8

    if-gez v0, :cond_6

    .line 1202319
    new-instance v0, LX/45Z;

    iget-wide v6, p1, LX/2WF;->b:J

    sub-long v4, v6, v4

    const/4 v3, 0x0

    invoke-direct {v0, v1, v4, v5, v3}, LX/45Z;-><init>(Ljava/io/InputStream;JZ)V

    .line 1202320
    :goto_1
    invoke-static {v0, v2}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 1202321
    :cond_3
    iget-object v0, p0, LX/7Ov;->c:LX/7Ou;

    iget-object v0, v0, LX/7Ou;->b:LX/7Os;

    .line 1202322
    iget-object v1, v0, LX/7Os;->b:Ljava/io/InputStream;

    move-object v0, v1

    .line 1202323
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 1202324
    const/4 v0, 0x0

    iput-object v0, p0, LX/7Ov;->c:LX/7Ou;

    goto/16 :goto_0

    .line 1202325
    :cond_4
    iget-wide v11, v2, LX/1iJ;->a:J

    move-wide v0, v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1202326
    iget-object v2, p0, LX/7Ov;->c:LX/7Ou;

    if-eqz v2, :cond_5

    .line 1202327
    iget-object v2, p0, LX/7Ov;->c:LX/7Ou;

    iget-object v2, v2, LX/7Ou;->b:LX/7Os;

    .line 1202328
    iget-object v3, v2, LX/7Os;->b:Ljava/io/InputStream;

    move-object v2, v3

    .line 1202329
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 1202330
    iput-object v10, p0, LX/7Ov;->c:LX/7Ou;

    :cond_5
    return-wide v0

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method public final a()LX/3Dd;
    .locals 1

    .prologue
    .line 1202331
    iget-object v0, p0, LX/7Ov;->c:LX/7Ou;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/7Ov;->b:LX/7Oi;

    invoke-interface {v0}, LX/7Oi;->a()LX/3Dd;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/7Ov;->c:LX/7Ou;

    iget-object v0, v0, LX/7Ou;->b:LX/7Os;

    .line 1202332
    iget-object p0, v0, LX/7Os;->a:LX/3Dd;

    move-object v0, p0

    .line 1202333
    goto :goto_0
.end method
