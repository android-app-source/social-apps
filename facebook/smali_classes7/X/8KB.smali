.class public LX/8KB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1329935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1329936
    iput-object p1, p0, LX/8KB;->a:LX/0ad;

    .line 1329937
    return-void
.end method

.method public static t(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1329938
    const v0, 0x7f08201b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1329939
    iget-object v0, p0, LX/8KB;->a:LX/0ad;

    sget-char v1, LX/8Jz;->T:C

    const v2, 0x7f082035

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, LX/8KB;->t(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1329940
    iget-object v0, p0, LX/8KB;->a:LX/0ad;

    sget-char v1, LX/8Jz;->O:C

    const v2, 0x7f082036

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1329941
    iget-object v0, p0, LX/8KB;->a:LX/0ad;

    sget-char v1, LX/8Jz;->P:C

    const v2, 0x7f082037

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1329942
    const v0, 0x7f08204e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1329943
    const v0, 0x7f08204d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
