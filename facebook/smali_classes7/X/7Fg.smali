.class public final LX/7Fg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1187770
    const/4 v12, 0x0

    .line 1187771
    const/4 v11, 0x0

    .line 1187772
    const/4 v10, 0x0

    .line 1187773
    const/4 v9, 0x0

    .line 1187774
    const/4 v8, 0x0

    .line 1187775
    const/4 v7, 0x0

    .line 1187776
    const/4 v6, 0x0

    .line 1187777
    const/4 v5, 0x0

    .line 1187778
    const/4 v4, 0x0

    .line 1187779
    const/4 v3, 0x0

    .line 1187780
    const/4 v2, 0x0

    .line 1187781
    const/4 v1, 0x0

    .line 1187782
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1187783
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1187784
    const/4 v1, 0x0

    .line 1187785
    :goto_0
    return v1

    .line 1187786
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1187787
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_b

    .line 1187788
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1187789
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1187790
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1187791
    const-string v14, "allow_write_in_response"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1187792
    const/4 v2, 0x1

    .line 1187793
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 1187794
    :cond_2
    const-string v14, "body"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1187795
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1187796
    :cond_3
    const-string v14, "custom_question_type"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1187797
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto :goto_1

    .line 1187798
    :cond_4
    const-string v14, "is_required"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1187799
    const/4 v1, 0x1

    .line 1187800
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 1187801
    :cond_5
    const-string v14, "message"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1187802
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1187803
    :cond_6
    const-string v14, "question_class"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1187804
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto :goto_1

    .line 1187805
    :cond_7
    const-string v14, "question_id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1187806
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 1187807
    :cond_8
    const-string v14, "response_options"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1187808
    invoke-static/range {p0 .. p1}, LX/7Fn;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1187809
    :cond_9
    const-string v14, "subquestion_labels"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 1187810
    invoke-static/range {p0 .. p1}, LX/4ap;->b(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1187811
    :cond_a
    const-string v14, "survey_token_params"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1187812
    invoke-static/range {p0 .. p1}, LX/7Fm;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1187813
    :cond_b
    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1187814
    if-eqz v2, :cond_c

    .line 1187815
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->a(IZ)V

    .line 1187816
    :cond_c
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1187817
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1187818
    if-eqz v1, :cond_d

    .line 1187819
    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->a(IZ)V

    .line 1187820
    :cond_d
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1187821
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1187822
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1187823
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1187824
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1187825
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1187826
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x2

    .line 1187827
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1187828
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1187829
    if-eqz v0, :cond_0

    .line 1187830
    const-string v1, "allow_write_in_response"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187831
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1187832
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1187833
    if-eqz v0, :cond_1

    .line 1187834
    const-string v1, "body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187835
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1187836
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1187837
    if-eqz v0, :cond_2

    .line 1187838
    const-string v0, "custom_question_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187839
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1187840
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1187841
    if-eqz v0, :cond_3

    .line 1187842
    const-string v1, "is_required"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187843
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1187844
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1187845
    if-eqz v0, :cond_4

    .line 1187846
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187847
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1187848
    :cond_4
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1187849
    if-eqz v0, :cond_5

    .line 1187850
    const-string v0, "question_class"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187851
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1187852
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1187853
    if-eqz v0, :cond_6

    .line 1187854
    const-string v1, "question_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187855
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1187856
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1187857
    if-eqz v0, :cond_8

    .line 1187858
    const-string v1, "response_options"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187859
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1187860
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_7

    .line 1187861
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/7Fn;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1187862
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1187863
    :cond_7
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1187864
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1187865
    if-eqz v0, :cond_a

    .line 1187866
    const-string v1, "subquestion_labels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187867
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1187868
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_9

    .line 1187869
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1187870
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1187871
    :cond_9
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1187872
    :cond_a
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1187873
    if-eqz v0, :cond_e

    .line 1187874
    const-string v1, "survey_token_params"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187875
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1187876
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_d

    .line 1187877
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 p3, 0x1

    .line 1187878
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1187879
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1187880
    if-eqz v3, :cond_b

    .line 1187881
    const-string p1, "param_name"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187882
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1187883
    :cond_b
    invoke-virtual {p0, v2, p3}, LX/15i;->g(II)I

    move-result v3

    .line 1187884
    if-eqz v3, :cond_c

    .line 1187885
    const-string v3, "survey_param_type"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1187886
    invoke-virtual {p0, v2, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1187887
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1187888
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1187889
    :cond_d
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1187890
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1187891
    return-void
.end method
