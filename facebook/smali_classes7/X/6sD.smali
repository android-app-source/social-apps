.class public LX/6sD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final a:LX/6rs;

.field public final b:LX/6s7;


# direct methods
.method public constructor <init>(LX/6rs;LX/6s7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1152986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152987
    iput-object p1, p0, LX/6sD;->a:LX/6rs;

    .line 1152988
    iput-object p2, p0, LX/6sD;->b:LX/6s7;

    .line 1152989
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1152990
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1152991
    invoke-virtual {p2}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1152992
    const-string v3, "identifier"

    invoke-virtual {v0, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1152993
    const-string v3, "identifier"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/6rc;->forValue(Ljava/lang/String;)LX/6rc;

    move-result-object v3

    .line 1152994
    sget-object v4, LX/6sC;->a:[I

    invoke-virtual {v3}, LX/6rc;->ordinal()I

    move-result p2

    aget v4, v4, p2

    packed-switch v4, :pswitch_data_0

    .line 1152995
    iget-object v4, p0, LX/6sD;->b:LX/6s7;

    invoke-virtual {v4, v3, p1, v0}, LX/6s7;->a(LX/6rc;Ljava/lang/String;LX/0lF;)Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;

    move-result-object v4

    :goto_1
    move-object v0, v4

    .line 1152996
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1152997
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1152998
    :pswitch_0
    iget-object v4, p0, LX/6sD;->a:LX/6rs;

    .line 1152999
    iget-object p2, v4, LX/6rs;->u:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/6rr;

    move-object v4, p2

    .line 1153000
    invoke-interface {v4, p1, v0}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
