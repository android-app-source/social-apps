.class public abstract LX/76J;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:LX/0Xl;

.field private final c:LX/0So;

.field private final d:LX/1fU;

.field private final e:LX/2gV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:LX/03V;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public h:Z

.field private i:LX/0Yb;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Xl;LX/0So;LX/1fU;LX/2gV;LX/03V;)V
    .locals 0

    .prologue
    .line 1170899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1170900
    iput-object p1, p0, LX/76J;->a:Ljava/lang/String;

    .line 1170901
    iput-object p2, p0, LX/76J;->b:LX/0Xl;

    .line 1170902
    iput-object p3, p0, LX/76J;->c:LX/0So;

    .line 1170903
    iput-object p4, p0, LX/76J;->d:LX/1fU;

    .line 1170904
    iput-object p5, p0, LX/76J;->e:LX/2gV;

    .line 1170905
    iput-object p6, p0, LX/76J;->f:LX/03V;

    .line 1170906
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/76J;LX/2EU;)V
    .locals 3

    .prologue
    .line 1170907
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/2EU;->CHANNEL_DISCONNECTED:LX/2EU;

    invoke-virtual {v0, p1}, LX/2EU;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1170908
    iget-object v0, p0, LX/76J;->e:LX/2gV;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/76J;->e:LX/2gV;

    invoke-virtual {v0}, LX/2gV;->c()LX/1Mb;

    move-result-object v0

    sget-object v1, LX/1Mb;->CONNECTED:LX/1Mb;

    if-ne v0, v1, :cond_1

    .line 1170909
    iget-object v0, p0, LX/76J;->f:LX/03V;

    if-eqz v0, :cond_0

    .line 1170910
    iget-object v0, p0, LX/76J;->f:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "skip cancel waiting response"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1170911
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1170912
    :cond_1
    :try_start_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/76J;->h:Z

    .line 1170913
    const v0, -0x35e5ed76    # -2524322.5f

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1170914
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(LX/76J;Ljava/lang/String;[B)V
    .locals 1

    .prologue
    .line 1170915
    monitor-enter p0

    .line 1170916
    :try_start_0
    iget-object v0, p0, LX/76J;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1170917
    if-nez v0, :cond_0

    .line 1170918
    iget-object v0, p0, LX/76J;->a:Ljava/lang/String;

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    move v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1170919
    if-nez v0, :cond_1

    .line 1170920
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1170921
    :cond_1
    :try_start_1
    invoke-virtual {p0, p1, p2}, LX/76J;->a(Ljava/lang/String;[B)V

    .line 1170922
    invoke-virtual {p0}, LX/76J;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1170923
    invoke-virtual {p0}, LX/76J;->b()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/76J;->g:Ljava/lang/Object;

    .line 1170924
    :cond_2
    const v0, 0x62aec729

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1170925
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public abstract a(Ljava/lang/String;[B)V
.end method

.method public abstract a()Z
.end method

.method public final declared-synchronized a(J)Z
    .locals 7

    .prologue
    .line 1170926
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/76J;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    add-long v2, v0, p1

    .line 1170927
    iget-object v0, p0, LX/76J;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sub-long v0, v2, v0

    .line 1170928
    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    .line 1170929
    iget-object v4, p0, LX/76J;->g:Ljava/lang/Object;

    move-object v4, v4

    .line 1170930
    if-nez v4, :cond_0

    .line 1170931
    iget-boolean v4, p0, LX/76J;->h:Z

    move v4, v4

    .line 1170932
    if-nez v4, :cond_0

    .line 1170933
    const v4, 0x1a5ea05e

    invoke-static {p0, v0, v1, v4}, LX/02L;->a(Ljava/lang/Object;JI)V

    .line 1170934
    iget-object v0, p0, LX/76J;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sub-long v0, v2, v0

    goto :goto_0

    .line 1170935
    :cond_0
    iget-object v0, p0, LX/76J;->g:Ljava/lang/Object;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1170936
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1170937
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract b()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1170938
    new-instance v0, LX/76N;

    invoke-direct {v0, p0}, LX/76N;-><init>(LX/76J;)V

    .line 1170939
    iget-object v1, p0, LX/76J;->b:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.push.mqtt.ACTION_MQTT_PUBLISH_ARRIVED"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/76J;->i:LX/0Yb;

    .line 1170940
    iget-object v0, p0, LX/76J;->i:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 1170941
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1170942
    iget-object v0, p0, LX/76J;->i:LX/0Yb;

    if-eqz v0, :cond_0

    .line 1170943
    iget-object v0, p0, LX/76J;->i:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 1170944
    const/4 v0, 0x0

    iput-object v0, p0, LX/76J;->i:LX/0Yb;

    .line 1170945
    :cond_0
    return-void
.end method

.method public final e()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 1170946
    iget-object v0, p0, LX/76J;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public final declared-synchronized f()V
    .locals 1

    .prologue
    .line 1170947
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/76J;->h:Z

    .line 1170948
    const v0, 0xa8e7191

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1170949
    monitor-exit p0

    return-void

    .line 1170950
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
