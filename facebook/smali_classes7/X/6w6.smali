.class public abstract LX/6w6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DATA_FETCHER::",
        "LX/6w1;",
        "SECTION_ORGANIZER::",
        "LX/6wK;",
        "ROW_GENERATOR::",
        "LX/6wI;",
        "ACTIVITY_RESU",
        "LT_HANDLER::Lcom/facebook/payments/picker/PickerScreenOnActivityResultHandler;",
        "RUN_TIME_DATA_MUTATOR::",
        "Lcom/facebook/payments/picker/PickerRunTimeDataMutator;",
        "LIFECYCLE_EVENTS_LISTENER::Lcom/facebook/payments/common/PaymentsLifecycleEventsListener;",
        "VIEW_FACTORY::",
        "LX/6wC;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/71C;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TDATA_FETCHER;>;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TSECTION_ORGANIZER;>;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TROW_GENERATOR;>;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TACTIVITY_RESU",
            "LT_HANDLER;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TRUN_TIME_DATA_MUTATOR;>;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<T",
            "LIFECYCLE_EVENTS_LISTENER;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TVIEW_FACTORY;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/71C;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/71C;",
            "LX/0Ot",
            "<TDATA_FETCHER;>;",
            "LX/0Ot",
            "<TSECTION_ORGANIZER;>;",
            "LX/0Ot",
            "<TROW_GENERATOR;>;",
            "LX/0Ot",
            "<TACTIVITY_RESU",
            "LT_HANDLER;",
            ">;",
            "LX/0Ot",
            "<TRUN_TIME_DATA_MUTATOR;>;",
            "LX/0Ot",
            "<T",
            "LIFECYCLE_EVENTS_LISTENER;",
            ">;",
            "LX/0Ot",
            "<TVIEW_FACTORY;>;)V"
        }
    .end annotation

    .prologue
    .line 1157422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157423
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/71C;

    iput-object v0, p0, LX/6w6;->a:LX/71C;

    .line 1157424
    iput-object p2, p0, LX/6w6;->b:LX/0Ot;

    .line 1157425
    iput-object p3, p0, LX/6w6;->c:LX/0Ot;

    .line 1157426
    iput-object p4, p0, LX/6w6;->d:LX/0Ot;

    .line 1157427
    iput-object p5, p0, LX/6w6;->e:LX/0Ot;

    .line 1157428
    iput-object p6, p0, LX/6w6;->f:LX/0Ot;

    .line 1157429
    iput-object p7, p0, LX/6w6;->g:LX/0Ot;

    .line 1157430
    iput-object p8, p0, LX/6w6;->h:LX/0Ot;

    .line 1157431
    return-void
.end method
