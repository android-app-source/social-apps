.class public final LX/8aw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 35

    .prologue
    .line 1369531
    const/16 v29, 0x0

    .line 1369532
    const/16 v28, 0x0

    .line 1369533
    const/16 v27, 0x0

    .line 1369534
    const/16 v26, 0x0

    .line 1369535
    const-wide/16 v24, 0x0

    .line 1369536
    const-wide/16 v22, 0x0

    .line 1369537
    const-wide/16 v20, 0x0

    .line 1369538
    const/4 v13, 0x0

    .line 1369539
    const-wide/16 v18, 0x0

    .line 1369540
    const-wide/16 v16, 0x0

    .line 1369541
    const-wide/16 v14, 0x0

    .line 1369542
    const/4 v12, 0x0

    .line 1369543
    const/4 v11, 0x0

    .line 1369544
    const/4 v10, 0x0

    .line 1369545
    const/4 v9, 0x0

    .line 1369546
    const/4 v8, 0x0

    .line 1369547
    const/4 v7, 0x0

    .line 1369548
    const/4 v6, 0x0

    .line 1369549
    const/4 v5, 0x0

    .line 1369550
    const/4 v4, 0x0

    .line 1369551
    const/4 v3, 0x0

    .line 1369552
    const/4 v2, 0x0

    .line 1369553
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_18

    .line 1369554
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1369555
    const/4 v2, 0x0

    .line 1369556
    :goto_0
    return v2

    .line 1369557
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_10

    .line 1369558
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1369559
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1369560
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1369561
    const-string v6, "font_family"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1369562
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v32, v2

    goto :goto_1

    .line 1369563
    :cond_1
    const-string v6, "horizontal_constraint"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1369564
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v31, v2

    goto :goto_1

    .line 1369565
    :cond_2
    const-string v6, "horizontal_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1369566
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v30, v2

    goto :goto_1

    .line 1369567
    :cond_3
    const-string v6, "max_color"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1369568
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 1369569
    :cond_4
    const-string v6, "max_font_size"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1369570
    const/4 v2, 0x1

    .line 1369571
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1369572
    :cond_5
    const-string v6, "max_line_height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1369573
    const/4 v2, 0x1

    .line 1369574
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v13, v2

    move-wide/from16 v28, v6

    goto/16 :goto_1

    .line 1369575
    :cond_6
    const-string v6, "max_word_kerning"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1369576
    const/4 v2, 0x1

    .line 1369577
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v12, v2

    move-wide/from16 v26, v6

    goto/16 :goto_1

    .line 1369578
    :cond_7
    const-string v6, "min_color"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1369579
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 1369580
    :cond_8
    const-string v6, "min_font_size"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1369581
    const/4 v2, 0x1

    .line 1369582
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v11, v2

    move-wide/from16 v22, v6

    goto/16 :goto_1

    .line 1369583
    :cond_9
    const-string v6, "min_line_height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1369584
    const/4 v2, 0x1

    .line 1369585
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v20, v6

    goto/16 :goto_1

    .line 1369586
    :cond_a
    const-string v6, "min_word_kerning"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1369587
    const/4 v2, 0x1

    .line 1369588
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide/from16 v18, v6

    goto/16 :goto_1

    .line 1369589
    :cond_b
    const-string v6, "morph_alignment"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1369590
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 1369591
    :cond_c
    const-string v6, "should_shadow"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1369592
    const/4 v2, 0x1

    .line 1369593
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v8, v2

    move/from16 v16, v6

    goto/16 :goto_1

    .line 1369594
    :cond_d
    const-string v6, "vertical_constraint"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 1369595
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 1369596
    :cond_e
    const-string v6, "vertical_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1369597
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 1369598
    :cond_f
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1369599
    :cond_10
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1369600
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1369601
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1369602
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1369603
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1369604
    if-eqz v3, :cond_11

    .line 1369605
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1369606
    :cond_11
    if-eqz v13, :cond_12

    .line 1369607
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v28

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1369608
    :cond_12
    if-eqz v12, :cond_13

    .line 1369609
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v26

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1369610
    :cond_13
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1369611
    if-eqz v11, :cond_14

    .line 1369612
    const/16 v3, 0x8

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v22

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1369613
    :cond_14
    if-eqz v10, :cond_15

    .line 1369614
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1369615
    :cond_15
    if-eqz v9, :cond_16

    .line 1369616
    const/16 v3, 0xa

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v18

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1369617
    :cond_16
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1369618
    if-eqz v8, :cond_17

    .line 1369619
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1369620
    :cond_17
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1369621
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1369622
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_18
    move/from16 v30, v27

    move/from16 v31, v28

    move/from16 v32, v29

    move-wide/from16 v28, v22

    move-wide/from16 v22, v18

    move-wide/from16 v18, v14

    move v14, v9

    move v15, v10

    move v9, v3

    move v10, v4

    move v3, v8

    move v8, v2

    move-wide/from16 v33, v16

    move/from16 v17, v12

    move/from16 v16, v11

    move v11, v5

    move v12, v6

    move-wide/from16 v4, v24

    move/from16 v24, v13

    move/from16 v25, v26

    move v13, v7

    move-wide/from16 v26, v20

    move-wide/from16 v20, v33

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1369623
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1369624
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1369625
    if-eqz v0, :cond_0

    .line 1369626
    const-string v1, "font_family"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369627
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369628
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1369629
    if-eqz v0, :cond_1

    .line 1369630
    const-string v1, "horizontal_constraint"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369631
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369632
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1369633
    if-eqz v0, :cond_2

    .line 1369634
    const-string v1, "horizontal_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369635
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369636
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1369637
    if-eqz v0, :cond_3

    .line 1369638
    const-string v1, "max_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369639
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369640
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1369641
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_4

    .line 1369642
    const-string v2, "max_font_size"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369643
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1369644
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1369645
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_5

    .line 1369646
    const-string v2, "max_line_height"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369647
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1369648
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1369649
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_6

    .line 1369650
    const-string v2, "max_word_kerning"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369651
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1369652
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1369653
    if-eqz v0, :cond_7

    .line 1369654
    const-string v1, "min_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369655
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369656
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1369657
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_8

    .line 1369658
    const-string v2, "min_font_size"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369659
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1369660
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1369661
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_9

    .line 1369662
    const-string v2, "min_line_height"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369663
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1369664
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1369665
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_a

    .line 1369666
    const-string v2, "min_word_kerning"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369667
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1369668
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1369669
    if-eqz v0, :cond_b

    .line 1369670
    const-string v1, "morph_alignment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369671
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369672
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1369673
    if-eqz v0, :cond_c

    .line 1369674
    const-string v1, "should_shadow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369675
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1369676
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1369677
    if-eqz v0, :cond_d

    .line 1369678
    const-string v1, "vertical_constraint"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369679
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369680
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1369681
    if-eqz v0, :cond_e

    .line 1369682
    const-string v1, "vertical_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369683
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369684
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1369685
    return-void
.end method
