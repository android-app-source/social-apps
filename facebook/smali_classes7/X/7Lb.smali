.class public final LX/7Lb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/VideoController;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/VideoController;)V
    .locals 0

    .prologue
    .line 1198151
    iput-object p1, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 8

    .prologue
    .line 1198152
    if-nez p3, :cond_1

    .line 1198153
    :cond_0
    :goto_0
    return-void

    .line 1198154
    :cond_1
    const/4 v0, 0x0

    .line 1198155
    iget-object v1, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    invoke-virtual {v1}, Lcom/facebook/video/player/VideoController;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1198156
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v0, v0, Lcom/facebook/video/player/VideoController;->n:LX/7Kc;

    invoke-interface {v0}, LX/7Kc;->getVideoViewDurationInMillis()I

    move-result v0

    .line 1198157
    :cond_2
    if-gtz v0, :cond_3

    iget-object v1, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-boolean v1, v1, Lcom/facebook/video/player/VideoController;->t:Z

    if-nez v1, :cond_0

    .line 1198158
    :cond_3
    int-to-long v0, v0

    .line 1198159
    int-to-long v2, p2

    mul-long/2addr v2, v0

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1198160
    iget-object v4, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    invoke-virtual {v4}, Lcom/facebook/video/player/VideoController;->a()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1198161
    iget-object v4, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v4, v4, Lcom/facebook/video/player/VideoController;->n:LX/7Kc;

    long-to-int v5, v2

    invoke-interface {v4, v5}, LX/7Kc;->f_(I)V

    .line 1198162
    :cond_4
    iget-object v4, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v4, v4, Lcom/facebook/video/player/VideoController;->i:Landroid/widget/TextView;

    long-to-int v5, v2

    int-to-long v6, v5

    invoke-static {v6, v7}, LX/7LQ;->a(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1198163
    iget-object v4, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v4, v4, Lcom/facebook/video/player/VideoController;->j:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long/2addr v0, v2

    long-to-int v0, v0

    int-to-long v0, v0

    invoke-static {v0, v1}, LX/7LQ;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1198164
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    invoke-virtual {v0}, Lcom/facebook/video/player/VideoController;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1198165
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v0, v0, Lcom/facebook/video/player/VideoController;->p:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2q9;

    .line 1198166
    invoke-virtual {v0}, LX/2q9;->d()V

    goto :goto_0
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1198167
    iget-object v1, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v1, v1, Lcom/facebook/video/player/VideoController;->u:LX/7Kq;

    if-eqz v1, :cond_0

    .line 1198168
    iget-object v1, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v1, v1, Lcom/facebook/video/player/VideoController;->u:LX/7Kq;

    invoke-interface {v1}, LX/7Kq;->a()V

    .line 1198169
    :cond_0
    iget-object v1, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    .line 1198170
    iput-boolean v0, v1, Lcom/facebook/video/player/VideoController;->o:Z

    .line 1198171
    iget-object v1, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v1, v1, Lcom/facebook/video/player/VideoController;->k:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1198172
    iget-object v1, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    invoke-virtual {v1}, Lcom/facebook/video/player/VideoController;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1198173
    iget-object v1, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v2, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v2, v2, Lcom/facebook/video/player/VideoController;->n:LX/7Kc;

    invoke-interface {v2}, LX/7Kc;->getVideoViewCurrentPosition()I

    move-result v2

    .line 1198174
    iput v2, v1, Lcom/facebook/video/player/VideoController;->q:I

    .line 1198175
    :cond_1
    iget-object v1, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    invoke-virtual {v1}, Lcom/facebook/video/player/VideoController;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v1, v1, Lcom/facebook/video/player/VideoController;->n:LX/7Kc;

    invoke-interface {v1}, LX/7Kc;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_0
    iput-boolean v0, p0, LX/7Lb;->b:Z

    .line 1198176
    iget-boolean v0, p0, LX/7Lb;->b:Z

    if-eqz v0, :cond_2

    .line 1198177
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    sget-object v1, LX/04g;->BY_SEEKBAR_CONTROLLER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/VideoController;->b(LX/04g;)V

    .line 1198178
    :cond_2
    return-void

    .line 1198179
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1198180
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v0, v0, Lcom/facebook/video/player/VideoController;->u:LX/7Kq;

    if-eqz v0, :cond_0

    .line 1198181
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v0, v0, Lcom/facebook/video/player/VideoController;->u:LX/7Kq;

    invoke-interface {v0, v1}, LX/7Kq;->a(Z)V

    .line 1198182
    :cond_0
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    .line 1198183
    iput-boolean v1, v0, Lcom/facebook/video/player/VideoController;->o:Z

    .line 1198184
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    invoke-static {v0}, Lcom/facebook/video/player/VideoController;->c(Lcom/facebook/video/player/VideoController;)I

    .line 1198185
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    invoke-static {v0}, Lcom/facebook/video/player/VideoController;->j(Lcom/facebook/video/player/VideoController;)V

    .line 1198186
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    invoke-virtual {v0}, Lcom/facebook/video/player/VideoController;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    invoke-virtual {v0}, Lcom/facebook/video/player/VideoController;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1198187
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v0, v0, Lcom/facebook/video/player/VideoController;->p:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2q9;

    .line 1198188
    invoke-virtual {v0}, LX/2q9;->d()V

    .line 1198189
    iget-object v1, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v1, v1, Lcom/facebook/video/player/VideoController;->n:LX/7Kc;

    invoke-interface {v1}, LX/7Kc;->getVideoViewCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, LX/2q9;->a(I)V

    .line 1198190
    invoke-virtual {v0}, LX/2q9;->b()V

    .line 1198191
    :cond_1
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v0, v0, Lcom/facebook/video/player/VideoController;->m:LX/7L3;

    if-eqz v0, :cond_2

    .line 1198192
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v0, v0, Lcom/facebook/video/player/VideoController;->m:LX/7L3;

    iget-object v1, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget v1, v1, Lcom/facebook/video/player/VideoController;->q:I

    invoke-virtual {v0, v1}, LX/7L3;->a(I)V

    .line 1198193
    :cond_2
    iget-boolean v0, p0, LX/7Lb;->b:Z

    if-eqz v0, :cond_3

    .line 1198194
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    sget-object v1, LX/04g;->BY_SEEKBAR_CONTROLLER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/VideoController;->a(LX/04g;)V

    .line 1198195
    :goto_0
    return-void

    .line 1198196
    :cond_3
    iget-object v0, p0, LX/7Lb;->a:Lcom/facebook/video/player/VideoController;

    iget-object v0, v0, Lcom/facebook/video/player/VideoController;->k:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
