.class public LX/6ta;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/6r9;

.field public final c:LX/3GL;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6r9;LX/3GL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1155090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1155091
    iput-object p1, p0, LX/6ta;->a:Landroid/content/Context;

    .line 1155092
    iput-object p2, p0, LX/6ta;->b:LX/6r9;

    .line 1155093
    iput-object p3, p0, LX/6ta;->c:LX/3GL;

    .line 1155094
    return-void
.end method

.method public static a(Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)I
    .locals 1

    .prologue
    .line 1155095
    iget-boolean v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->g:Z

    if-nez v0, :cond_0

    .line 1155096
    const/16 v0, 0x70

    .line 1155097
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x71

    goto :goto_0
.end method

.method public static c(LX/6ta;Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1155098
    iget-boolean v0, p2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->g:Z

    if-nez v0, :cond_0

    .line 1155099
    iget-object v0, p0, LX/6ta;->b:LX/6r9;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->e(LX/6qw;)LX/6E0;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/6E0;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;

    move-result-object v0

    .line 1155100
    iget-object v1, p0, LX/6ta;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;

    move-result-object v0

    move-object v0, v0

    .line 1155101
    :goto_0
    return-object v0

    .line 1155102
    :cond_0
    iget-object v0, p0, LX/6ta;->b:LX/6r9;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->e(LX/6qw;)LX/6E0;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/6E0;->b(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    move-result-object v0

    .line 1155103
    iget-object v1, p0, LX/6ta;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;)Landroid/content/Intent;

    move-result-object v0

    move-object v0, v0

    .line 1155104
    goto :goto_0
.end method
