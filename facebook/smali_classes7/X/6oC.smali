.class public LX/6oC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6oB;


# static fields
.field public static final a:LX/0Tn;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6oH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1147940
    sget-object v0, LX/0Tm;->b:LX/0Tn;

    const-string v1, "p2p_fingerprint_nonce/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6oC;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/6nz;LX/6oI;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1147937
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1147938
    new-instance v0, LX/6oA;

    invoke-direct {v0, p0, p2, p1}, LX/6oA;-><init>(LX/6oC;LX/6oI;LX/6nz;)V

    invoke-static {v0}, LX/3R5;->a(LX/0Or;)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/6oC;->b:LX/0Ot;

    .line 1147939
    return-void
.end method

.method public static b(LX/0QB;)LX/6oC;
    .locals 3

    .prologue
    .line 1147942
    new-instance v2, LX/6oC;

    const-class v0, LX/6nz;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/6nz;

    const-class v1, LX/6oI;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/6oI;

    invoke-direct {v2, v0, v1}, LX/6oC;-><init>(LX/6nz;LX/6oI;)V

    .line 1147943
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 1147944
    iget-object v0, p0, LX/6oC;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6oH;

    const-string v1, "nonce_key/"

    invoke-virtual {v0, v1, p1}, LX/6oH;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1147945
    return-void
.end method

.method public final c()LX/6oJ;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 1147941
    iget-object v0, p0, LX/6oC;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6oH;

    invoke-virtual {v0}, LX/6oH;->c()LX/6oJ;

    move-result-object v0

    return-object v0
.end method
