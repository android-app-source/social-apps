.class public abstract LX/6sU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;
.implements LX/0rp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PARAM::",
        "Landroid/os/Parcelable;",
        "RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0rp",
        "<TPARAM;>;",
        "Lcom/facebook/payments/common/PaymentsApiMethod",
        "<TPARAM;TRESU",
        "LT;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

.field public final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TRESU",
            "LT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/common/PaymentNetworkOperationHelper;",
            "Ljava/lang/Class",
            "<TRESU",
            "LT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1153437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153438
    iput-object p1, p0, LX/6sU;->a:Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    .line 1153439
    iput-object p2, p0, LX/6sU;->b:Ljava/lang/Class;

    .line 1153440
    return-void
.end method

.method public static a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1153434
    invoke-virtual {p0, p1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v1, "No field %s in %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1153435
    instance-of v1, v0, LX/0mE;

    const-string v2, "%s is not a value node"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1153436
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1pN;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1153433
    invoke-virtual {p0}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-static {v0, p1}, LX/6sU;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/6sU;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1153432
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<-",
            "Lorg/apache/http/message/BasicNameValuePair;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1153430
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1153431
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAM;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1153421
    iget-object v0, p0, LX/6sU;->a:Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-static {p0}, LX/6sU;->a(LX/6sU;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, LX/6sU;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->a(Ljava/lang/String;Landroid/os/Parcelable;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153429
    instance-of v0, p2, LX/2Oo;

    if-eqz v0, :cond_0

    new-instance v0, LX/6u0;

    check-cast p2, LX/2Oo;

    invoke-direct {v0, p0, p2}, LX/6u0;-><init>(LX/6sU;LX/2Oo;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1153428
    return-void
.end method

.method public b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAM;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1153424
    iget-object v0, p0, LX/6sU;->a:Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    iget-object v1, p0, LX/6sU;->a:Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-static {p0}, LX/6sU;->a(LX/6sU;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, LX/6sU;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, p1, v3}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->a(Ljava/lang/String;Landroid/os/Parcelable;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iget-object v2, p0, LX/6sU;->b:Ljava/lang/Class;

    .line 1153425
    const-class v3, Landroid/os/Parcelable;

    invoke-virtual {v3, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->d:LX/0QK;

    :goto_0
    move-object v3, v3

    .line 1153426
    iget-object p0, v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b:LX/0TD;

    invoke-static {v1, v3, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 1153427
    return-object v0

    :cond_0
    new-instance v3, LX/6tz;

    invoke-direct {v3, v0, v2}, LX/6tz;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1153423
    return-void
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1153422
    invoke-virtual {p0}, LX/6sU;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
