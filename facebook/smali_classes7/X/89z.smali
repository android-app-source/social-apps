.class public final enum LX/89z;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/89z;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/89z;

.field public static final enum BOOKMARK:LX/89z;

.field public static final enum DEEPLINK:LX/89z;

.field public static final enum EVENT:LX/89z;

.field public static final enum GROUP:LX/89z;

.field public static final enum LAUNCH_POINT:LX/89z;

.field public static final enum NEWSFEED:LX/89z;

.field public static final enum NOTIFICATION:LX/89z;

.field public static final enum PAGE:LX/89z;

.field public static final enum PAGE_INTERNAL:LX/89z;

.field public static final enum PERMALINK:LX/89z;

.field public static final enum PUSH_NOTIF:LX/89z;

.field public static final enum RECENT_SEARCHS:LX/89z;

.field public static final enum SEARCH_RESULTS:LX/89z;

.field public static final enum SEARCH_TYPEAHEAD:LX/89z;

.field public static final enum UNKNOWN:LX/89z;

.field public static final enum USER_TIMELINE:LX/89z;


# instance fields
.field public final loggingName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1305535
    new-instance v0, LX/89z;

    const-string v1, "NEWSFEED"

    const-string v2, "nf"

    invoke-direct {v0, v1, v4, v2}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->NEWSFEED:LX/89z;

    .line 1305536
    new-instance v0, LX/89z;

    const-string v1, "SEARCH_TYPEAHEAD"

    const-string v2, "ts"

    invoke-direct {v0, v1, v5, v2}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->SEARCH_TYPEAHEAD:LX/89z;

    .line 1305537
    new-instance v0, LX/89z;

    const-string v1, "SEARCH_RESULTS"

    const-string v2, "br_rs"

    invoke-direct {v0, v1, v6, v2}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->SEARCH_RESULTS:LX/89z;

    .line 1305538
    new-instance v0, LX/89z;

    const-string v1, "RECENT_SEARCHS"

    const-string v2, "recent_search"

    invoke-direct {v0, v1, v7, v2}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->RECENT_SEARCHS:LX/89z;

    .line 1305539
    new-instance v0, LX/89z;

    const-string v1, "USER_TIMELINE"

    const-string v2, "profile"

    invoke-direct {v0, v1, v8, v2}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->USER_TIMELINE:LX/89z;

    .line 1305540
    new-instance v0, LX/89z;

    const-string v1, "PAGE"

    const/4 v2, 0x5

    const-string v3, "page_profile"

    invoke-direct {v0, v1, v2, v3}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->PAGE:LX/89z;

    .line 1305541
    new-instance v0, LX/89z;

    const-string v1, "BOOKMARK"

    const/4 v2, 0x6

    const-string v3, "bookmarks"

    invoke-direct {v0, v1, v2, v3}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->BOOKMARK:LX/89z;

    .line 1305542
    new-instance v0, LX/89z;

    const-string v1, "NOTIFICATION"

    const/4 v2, 0x7

    const-string v3, "notif"

    invoke-direct {v0, v1, v2, v3}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->NOTIFICATION:LX/89z;

    .line 1305543
    new-instance v0, LX/89z;

    const-string v1, "PERMALINK"

    const/16 v2, 0x8

    const-string v3, "permalink"

    invoke-direct {v0, v1, v2, v3}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->PERMALINK:LX/89z;

    .line 1305544
    new-instance v0, LX/89z;

    const-string v1, "PUSH_NOTIF"

    const/16 v2, 0x9

    const-string v3, "push"

    invoke-direct {v0, v1, v2, v3}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->PUSH_NOTIF:LX/89z;

    .line 1305545
    new-instance v0, LX/89z;

    const-string v1, "EVENT"

    const/16 v2, 0xa

    const-string v3, "event"

    invoke-direct {v0, v1, v2, v3}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->EVENT:LX/89z;

    .line 1305546
    new-instance v0, LX/89z;

    const-string v1, "GROUP"

    const/16 v2, 0xb

    const-string v3, "group"

    invoke-direct {v0, v1, v2, v3}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->GROUP:LX/89z;

    .line 1305547
    new-instance v0, LX/89z;

    const-string v1, "LAUNCH_POINT"

    const/16 v2, 0xc

    const-string v3, "launch_point"

    invoke-direct {v0, v1, v2, v3}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->LAUNCH_POINT:LX/89z;

    .line 1305548
    new-instance v0, LX/89z;

    const-string v1, "PAGE_INTERNAL"

    const/16 v2, 0xd

    const-string v3, "page_internal"

    invoke-direct {v0, v1, v2, v3}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->PAGE_INTERNAL:LX/89z;

    .line 1305549
    new-instance v0, LX/89z;

    const-string v1, "DEEPLINK"

    const/16 v2, 0xe

    const-string v3, "deeplink"

    invoke-direct {v0, v1, v2, v3}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->DEEPLINK:LX/89z;

    .line 1305550
    new-instance v0, LX/89z;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xf

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/89z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89z;->UNKNOWN:LX/89z;

    .line 1305551
    const/16 v0, 0x10

    new-array v0, v0, [LX/89z;

    sget-object v1, LX/89z;->NEWSFEED:LX/89z;

    aput-object v1, v0, v4

    sget-object v1, LX/89z;->SEARCH_TYPEAHEAD:LX/89z;

    aput-object v1, v0, v5

    sget-object v1, LX/89z;->SEARCH_RESULTS:LX/89z;

    aput-object v1, v0, v6

    sget-object v1, LX/89z;->RECENT_SEARCHS:LX/89z;

    aput-object v1, v0, v7

    sget-object v1, LX/89z;->USER_TIMELINE:LX/89z;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/89z;->PAGE:LX/89z;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/89z;->BOOKMARK:LX/89z;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/89z;->NOTIFICATION:LX/89z;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/89z;->PERMALINK:LX/89z;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/89z;->PUSH_NOTIF:LX/89z;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/89z;->EVENT:LX/89z;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/89z;->GROUP:LX/89z;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/89z;->LAUNCH_POINT:LX/89z;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/89z;->PAGE_INTERNAL:LX/89z;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/89z;->DEEPLINK:LX/89z;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/89z;->UNKNOWN:LX/89z;

    aput-object v2, v0, v1

    sput-object v0, LX/89z;->$VALUES:[LX/89z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1305552
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1305553
    iput-object p3, p0, LX/89z;->loggingName:Ljava/lang/String;

    .line 1305554
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/89z;
    .locals 1

    .prologue
    .line 1305555
    const-class v0, LX/89z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/89z;

    return-object v0
.end method

.method public static values()[LX/89z;
    .locals 1

    .prologue
    .line 1305556
    sget-object v0, LX/89z;->$VALUES:[LX/89z;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/89z;

    return-object v0
.end method
