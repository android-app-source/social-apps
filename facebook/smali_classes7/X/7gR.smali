.class public final enum LX/7gR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gR;

.field public static final enum BACK_TO_CAMERA:LX/7gR;

.field public static final enum DESELECTED_FRIENDS_EVER:LX/7gR;

.field public static final enum DESELECT_FIRST_FRIEND:LX/7gR;

.field public static final enum DIRECT_DIVEBAR_BADGE:LX/7gR;

.field public static final enum ENTER_SEARCH_FRIENDS:LX/7gR;

.field public static final enum ENTER_SHARE_SHEET:LX/7gR;

.field public static final enum EXCLUDE_NEWS_FEED:LX/7gR;

.field public static final enum EXCLUDE_STORY:LX/7gR;

.field public static final enum INCLUDE_NEWS_FEED:LX/7gR;

.field public static final enum INCLUDE_STORY:LX/7gR;

.field public static final enum MODIFY_NEWS_FEED_PRIVACY:LX/7gR;

.field public static final enum OPEN_DIRECT:LX/7gR;

.field public static final enum OPEN_ROW:LX/7gR;

.field public static final enum OPEN_THREAD:LX/7gR;

.field public static final enum OPEN_UNTAPPABLE_ROW:LX/7gR;

.field public static final enum PRIVATE_LISTS_SHOWN:LX/7gR;

.field public static final enum REFRESH_DIRECT:LX/7gR;

.field public static final enum ROW_IMPRESSION:LX/7gR;

.field public static final enum SELECTED_FRIENDS_EVER:LX/7gR;

.field public static final enum SELECT_FIRST_FRIEND:LX/7gR;

.field public static final enum SEND_DIRECT:LX/7gR;

.field public static final enum SEND_NEWS_FEED:LX/7gR;

.field public static final enum SEND_REPLY:LX/7gR;

.field public static final enum SEND_STORY:LX/7gR;

.field public static final enum TYPE_SEARCH_FRIENDS:LX/7gR;


# instance fields
.field private final mModuleName:Ljava/lang/String;

.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1224287
    new-instance v0, LX/7gR;

    const-string v1, "DIRECT_DIVEBAR_BADGE"

    const-string v2, "direct_divebar_impression"

    const-string v3, "direct_actions"

    invoke-direct {v0, v1, v5, v2, v3}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->DIRECT_DIVEBAR_BADGE:LX/7gR;

    .line 1224288
    new-instance v0, LX/7gR;

    const-string v1, "OPEN_DIRECT"

    const-string v2, "open_direct"

    const-string v3, "direct_actions"

    invoke-direct {v0, v1, v6, v2, v3}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->OPEN_DIRECT:LX/7gR;

    .line 1224289
    new-instance v0, LX/7gR;

    const-string v1, "REFRESH_DIRECT"

    const-string v2, "refresh_direct"

    const-string v3, "direct_actions"

    invoke-direct {v0, v1, v7, v2, v3}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->REFRESH_DIRECT:LX/7gR;

    .line 1224290
    new-instance v0, LX/7gR;

    const-string v1, "OPEN_ROW"

    const-string v2, "open_row"

    const-string v3, "direct_actions"

    invoke-direct {v0, v1, v8, v2, v3}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->OPEN_ROW:LX/7gR;

    .line 1224291
    new-instance v0, LX/7gR;

    const-string v1, "OPEN_UNTAPPABLE_ROW"

    const-string v2, "open_untappable_row"

    const-string v3, "direct_actions"

    invoke-direct {v0, v1, v9, v2, v3}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->OPEN_UNTAPPABLE_ROW:LX/7gR;

    .line 1224292
    new-instance v0, LX/7gR;

    const-string v1, "OPEN_THREAD"

    const/4 v2, 0x5

    const-string v3, "open_thread"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->OPEN_THREAD:LX/7gR;

    .line 1224293
    new-instance v0, LX/7gR;

    const-string v1, "SEND_REPLY"

    const/4 v2, 0x6

    const-string v3, "send_reply_attempted"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->SEND_REPLY:LX/7gR;

    .line 1224294
    new-instance v0, LX/7gR;

    const-string v1, "ROW_IMPRESSION"

    const/4 v2, 0x7

    const-string v3, "row_impression"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->ROW_IMPRESSION:LX/7gR;

    .line 1224295
    new-instance v0, LX/7gR;

    const-string v1, "ENTER_SHARE_SHEET"

    const/16 v2, 0x8

    const-string v3, "enter_share_sheet"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->ENTER_SHARE_SHEET:LX/7gR;

    .line 1224296
    new-instance v0, LX/7gR;

    const-string v1, "SEND_DIRECT"

    const/16 v2, 0x9

    const-string v3, "send_direct"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->SEND_DIRECT:LX/7gR;

    .line 1224297
    new-instance v0, LX/7gR;

    const-string v1, "SEND_NEWS_FEED"

    const/16 v2, 0xa

    const-string v3, "send_news_feed"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->SEND_NEWS_FEED:LX/7gR;

    .line 1224298
    new-instance v0, LX/7gR;

    const-string v1, "SEND_STORY"

    const/16 v2, 0xb

    const-string v3, "send_story"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->SEND_STORY:LX/7gR;

    .line 1224299
    new-instance v0, LX/7gR;

    const-string v1, "BACK_TO_CAMERA"

    const/16 v2, 0xc

    const-string v3, "back_to_camera"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->BACK_TO_CAMERA:LX/7gR;

    .line 1224300
    new-instance v0, LX/7gR;

    const-string v1, "INCLUDE_NEWS_FEED"

    const/16 v2, 0xd

    const-string v3, "include_news_feed"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->INCLUDE_NEWS_FEED:LX/7gR;

    .line 1224301
    new-instance v0, LX/7gR;

    const-string v1, "MODIFY_NEWS_FEED_PRIVACY"

    const/16 v2, 0xe

    const-string v3, "modify_news_feed_privacy"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->MODIFY_NEWS_FEED_PRIVACY:LX/7gR;

    .line 1224302
    new-instance v0, LX/7gR;

    const-string v1, "EXCLUDE_NEWS_FEED"

    const/16 v2, 0xf

    const-string v3, "exclude_news_feed"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->EXCLUDE_NEWS_FEED:LX/7gR;

    .line 1224303
    new-instance v0, LX/7gR;

    const-string v1, "INCLUDE_STORY"

    const/16 v2, 0x10

    const-string v3, "include_story"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->INCLUDE_STORY:LX/7gR;

    .line 1224304
    new-instance v0, LX/7gR;

    const-string v1, "EXCLUDE_STORY"

    const/16 v2, 0x11

    const-string v3, "exclude_story"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->EXCLUDE_STORY:LX/7gR;

    .line 1224305
    new-instance v0, LX/7gR;

    const-string v1, "PRIVATE_LISTS_SHOWN"

    const/16 v2, 0x12

    const-string v3, "private_lists_shown"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->PRIVATE_LISTS_SHOWN:LX/7gR;

    .line 1224306
    new-instance v0, LX/7gR;

    const-string v1, "SELECT_FIRST_FRIEND"

    const/16 v2, 0x13

    const-string v3, "select_first_friend"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->SELECT_FIRST_FRIEND:LX/7gR;

    .line 1224307
    new-instance v0, LX/7gR;

    const-string v1, "DESELECT_FIRST_FRIEND"

    const/16 v2, 0x14

    const-string v3, "deselect_first_friend"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->DESELECT_FIRST_FRIEND:LX/7gR;

    .line 1224308
    new-instance v0, LX/7gR;

    const-string v1, "SELECTED_FRIENDS_EVER"

    const/16 v2, 0x15

    const-string v3, "selected_friends_ever"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->SELECTED_FRIENDS_EVER:LX/7gR;

    .line 1224309
    new-instance v0, LX/7gR;

    const-string v1, "DESELECTED_FRIENDS_EVER"

    const/16 v2, 0x16

    const-string v3, "deselected_friends_ever"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->DESELECTED_FRIENDS_EVER:LX/7gR;

    .line 1224310
    new-instance v0, LX/7gR;

    const-string v1, "ENTER_SEARCH_FRIENDS"

    const/16 v2, 0x17

    const-string v3, "enter_search_friends"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->ENTER_SEARCH_FRIENDS:LX/7gR;

    .line 1224311
    new-instance v0, LX/7gR;

    const-string v1, "TYPE_SEARCH_FRIENDS"

    const/16 v2, 0x18

    const-string v3, "type_search_friends"

    const-string v4, "direct_actions"

    invoke-direct {v0, v1, v2, v3, v4}, LX/7gR;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/7gR;->TYPE_SEARCH_FRIENDS:LX/7gR;

    .line 1224312
    const/16 v0, 0x19

    new-array v0, v0, [LX/7gR;

    sget-object v1, LX/7gR;->DIRECT_DIVEBAR_BADGE:LX/7gR;

    aput-object v1, v0, v5

    sget-object v1, LX/7gR;->OPEN_DIRECT:LX/7gR;

    aput-object v1, v0, v6

    sget-object v1, LX/7gR;->REFRESH_DIRECT:LX/7gR;

    aput-object v1, v0, v7

    sget-object v1, LX/7gR;->OPEN_ROW:LX/7gR;

    aput-object v1, v0, v8

    sget-object v1, LX/7gR;->OPEN_UNTAPPABLE_ROW:LX/7gR;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/7gR;->OPEN_THREAD:LX/7gR;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7gR;->SEND_REPLY:LX/7gR;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7gR;->ROW_IMPRESSION:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7gR;->ENTER_SHARE_SHEET:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7gR;->SEND_DIRECT:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7gR;->SEND_NEWS_FEED:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7gR;->SEND_STORY:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7gR;->BACK_TO_CAMERA:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/7gR;->INCLUDE_NEWS_FEED:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/7gR;->MODIFY_NEWS_FEED_PRIVACY:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/7gR;->EXCLUDE_NEWS_FEED:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/7gR;->INCLUDE_STORY:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/7gR;->EXCLUDE_STORY:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/7gR;->PRIVATE_LISTS_SHOWN:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/7gR;->SELECT_FIRST_FRIEND:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/7gR;->DESELECT_FIRST_FRIEND:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/7gR;->SELECTED_FRIENDS_EVER:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/7gR;->DESELECTED_FRIENDS_EVER:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/7gR;->ENTER_SEARCH_FRIENDS:LX/7gR;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/7gR;->TYPE_SEARCH_FRIENDS:LX/7gR;

    aput-object v2, v0, v1

    sput-object v0, LX/7gR;->$VALUES:[LX/7gR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1224313
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1224314
    iput-object p3, p0, LX/7gR;->mName:Ljava/lang/String;

    .line 1224315
    iput-object p4, p0, LX/7gR;->mModuleName:Ljava/lang/String;

    .line 1224316
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gR;
    .locals 1

    .prologue
    .line 1224317
    const-class v0, LX/7gR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gR;

    return-object v0
.end method

.method public static values()[LX/7gR;
    .locals 1

    .prologue
    .line 1224318
    sget-object v0, LX/7gR;->$VALUES:[LX/7gR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gR;

    return-object v0
.end method


# virtual methods
.method public final getModuleName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224319
    iget-object v0, p0, LX/7gR;->mModuleName:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224320
    iget-object v0, p0, LX/7gR;->mName:Ljava/lang/String;

    return-object v0
.end method
