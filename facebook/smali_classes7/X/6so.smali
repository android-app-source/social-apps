.class public final enum LX/6so;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6so;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6so;

.field public static final enum ACTIONABLE_CHECKOUT_OPTION:LX/6so;

.field public static final enum CHECKOUT_OPTION:LX/6so;

.field public static final enum CONTACT_INFORMATION:LX/6so;

.field public static final enum CONTACT_NAME:LX/6so;

.field public static final enum DIVIDER:LX/6so;

.field public static final enum ENTITY:LX/6so;

.field public static final enum EXPANDING_ELLIPSIZING_TEXT:LX/6so;

.field public static final enum MAILING_ADDRESS:LX/6so;

.field public static final enum NOTE:LX/6so;

.field public static final enum PAYMENT_METHOD:LX/6so;

.field public static final enum PAY_BUTTON:LX/6so;

.field public static final enum PRICE_SELECTOR:LX/6so;

.field public static final enum PRICE_TABLE:LX/6so;

.field public static final enum PRIVACY_SELECTOR:LX/6so;

.field public static final enum PURCHASE_REVIEW_CELL:LX/6so;

.field public static final enum SHIPPING_OPTION:LX/6so;

.field public static final enum TERMS_AND_POLICIES:LX/6so;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1153785
    new-instance v0, LX/6so;

    const-string v1, "ACTIONABLE_CHECKOUT_OPTION"

    invoke-direct {v0, v1, v3}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->ACTIONABLE_CHECKOUT_OPTION:LX/6so;

    .line 1153786
    new-instance v0, LX/6so;

    const-string v1, "CHECKOUT_OPTION"

    invoke-direct {v0, v1, v4}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->CHECKOUT_OPTION:LX/6so;

    .line 1153787
    new-instance v0, LX/6so;

    const-string v1, "CONTACT_INFORMATION"

    invoke-direct {v0, v1, v5}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->CONTACT_INFORMATION:LX/6so;

    .line 1153788
    new-instance v0, LX/6so;

    const-string v1, "CONTACT_NAME"

    invoke-direct {v0, v1, v6}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->CONTACT_NAME:LX/6so;

    .line 1153789
    new-instance v0, LX/6so;

    const-string v1, "DIVIDER"

    invoke-direct {v0, v1, v7}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->DIVIDER:LX/6so;

    .line 1153790
    new-instance v0, LX/6so;

    const-string v1, "ENTITY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->ENTITY:LX/6so;

    .line 1153791
    new-instance v0, LX/6so;

    const-string v1, "EXPANDING_ELLIPSIZING_TEXT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->EXPANDING_ELLIPSIZING_TEXT:LX/6so;

    .line 1153792
    new-instance v0, LX/6so;

    const-string v1, "MAILING_ADDRESS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->MAILING_ADDRESS:LX/6so;

    .line 1153793
    new-instance v0, LX/6so;

    const-string v1, "NOTE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->NOTE:LX/6so;

    .line 1153794
    new-instance v0, LX/6so;

    const-string v1, "PAY_BUTTON"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->PAY_BUTTON:LX/6so;

    .line 1153795
    new-instance v0, LX/6so;

    const-string v1, "PAYMENT_METHOD"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->PAYMENT_METHOD:LX/6so;

    .line 1153796
    new-instance v0, LX/6so;

    const-string v1, "PRICE_TABLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->PRICE_TABLE:LX/6so;

    .line 1153797
    new-instance v0, LX/6so;

    const-string v1, "PRICE_SELECTOR"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->PRICE_SELECTOR:LX/6so;

    .line 1153798
    new-instance v0, LX/6so;

    const-string v1, "PRIVACY_SELECTOR"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->PRIVACY_SELECTOR:LX/6so;

    .line 1153799
    new-instance v0, LX/6so;

    const-string v1, "SHIPPING_OPTION"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->SHIPPING_OPTION:LX/6so;

    .line 1153800
    new-instance v0, LX/6so;

    const-string v1, "PURCHASE_REVIEW_CELL"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->PURCHASE_REVIEW_CELL:LX/6so;

    .line 1153801
    new-instance v0, LX/6so;

    const-string v1, "TERMS_AND_POLICIES"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/6so;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6so;->TERMS_AND_POLICIES:LX/6so;

    .line 1153802
    const/16 v0, 0x11

    new-array v0, v0, [LX/6so;

    sget-object v1, LX/6so;->ACTIONABLE_CHECKOUT_OPTION:LX/6so;

    aput-object v1, v0, v3

    sget-object v1, LX/6so;->CHECKOUT_OPTION:LX/6so;

    aput-object v1, v0, v4

    sget-object v1, LX/6so;->CONTACT_INFORMATION:LX/6so;

    aput-object v1, v0, v5

    sget-object v1, LX/6so;->CONTACT_NAME:LX/6so;

    aput-object v1, v0, v6

    sget-object v1, LX/6so;->DIVIDER:LX/6so;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6so;->ENTITY:LX/6so;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6so;->EXPANDING_ELLIPSIZING_TEXT:LX/6so;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6so;->MAILING_ADDRESS:LX/6so;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6so;->NOTE:LX/6so;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6so;->PAY_BUTTON:LX/6so;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6so;->PAYMENT_METHOD:LX/6so;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/6so;->PRICE_TABLE:LX/6so;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/6so;->PRICE_SELECTOR:LX/6so;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/6so;->PRIVACY_SELECTOR:LX/6so;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/6so;->SHIPPING_OPTION:LX/6so;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/6so;->PURCHASE_REVIEW_CELL:LX/6so;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/6so;->TERMS_AND_POLICIES:LX/6so;

    aput-object v2, v0, v1

    sput-object v0, LX/6so;->$VALUES:[LX/6so;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1153803
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6so;
    .locals 1

    .prologue
    .line 1153804
    const-class v0, LX/6so;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6so;

    return-object v0
.end method

.method public static values()[LX/6so;
    .locals 1

    .prologue
    .line 1153805
    sget-object v0, LX/6so;->$VALUES:[LX/6so;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6so;

    return-object v0
.end method
