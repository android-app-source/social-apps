.class public final LX/8Xl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/views/loading/CircularProgressView;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/loading/CircularProgressView;)V
    .locals 0

    .prologue
    .line 1355330
    iput-object p1, p0, LX/8Xl;->a:Lcom/facebook/quicksilver/views/loading/CircularProgressView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 1355331
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1355332
    iget-object v1, p0, LX/8Xl;->a:Lcom/facebook/quicksilver/views/loading/CircularProgressView;

    const/high16 v2, 0x43340000    # 180.0f

    mul-float/2addr v0, v2

    .line 1355333
    iput v0, v1, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->m:F

    .line 1355334
    iget-object v0, p0, LX/8Xl;->a:Lcom/facebook/quicksilver/views/loading/CircularProgressView;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->postInvalidate()V

    .line 1355335
    return-void
.end method
