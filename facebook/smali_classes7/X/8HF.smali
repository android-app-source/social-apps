.class public final LX/8HF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/8HG;",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/tagging/model/TaggingProfile;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8HH;


# direct methods
.method public constructor <init>(LX/8HH;)V
    .locals 0

    .prologue
    .line 1320674
    iput-object p1, p0, LX/8HF;->a:LX/8HH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/0lF;)Z
    .locals 1

    .prologue
    .line 1320675
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1320676
    check-cast p1, LX/8HG;

    .line 1320677
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1320678
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "JSON"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1320679
    iget-boolean v1, p1, LX/8HG;->a:Z

    if-eqz v1, :cond_0

    .line 1320680
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "needs_userinfo"

    const-string v3, "true"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1320681
    :cond_0
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "facerec"

    .line 1320682
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1320683
    move-object v1, v1

    .line 1320684
    const-string v2, "GET"

    .line 1320685
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1320686
    move-object v1, v1

    .line 1320687
    const-string v2, "method/photos.getSuggestedTags"

    .line 1320688
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1320689
    move-object v1, v1

    .line 1320690
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1320691
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1320692
    move-object v0, v1

    .line 1320693
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1320694
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1320695
    move-object v0, v0

    .line 1320696
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1320697
    check-cast p1, LX/8HG;

    const/4 v2, 0x0

    .line 1320698
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 1320699
    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 1320700
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1320701
    if-nez v0, :cond_1

    .line 1320702
    :cond_0
    :goto_0
    return-object v2

    .line 1320703
    :cond_1
    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v1

    .line 1320704
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1320705
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1320706
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1320707
    const-string v3, "error"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 1320708
    if-nez v3, :cond_2

    .line 1320709
    const-string v3, "tags"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1320710
    if-eqz v0, :cond_2

    .line 1320711
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    const-string v3, "suggestions"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1320712
    if-eqz v0, :cond_2

    .line 1320713
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1320714
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1320715
    invoke-virtual {v0}, LX/0lF;->G()Ljava/util/Iterator;

    move-result-object v4

    .line 1320716
    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1320717
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1320718
    const-string v5, "id"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 1320719
    invoke-static {v5}, LX/8HF;->a(LX/0lF;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1320720
    iget-boolean v6, p1, LX/8HG;->a:Z

    if-eqz v6, :cond_5

    .line 1320721
    const-string v6, "name"

    invoke-virtual {v0, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    .line 1320722
    const-string v7, "pic"

    invoke-virtual {v0, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1320723
    invoke-static {v6}, LX/8HF;->a(LX/0lF;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1320724
    new-instance v7, LX/7Gq;

    invoke-direct {v7}, LX/7Gq;-><init>()V

    invoke-virtual {v5}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 1320725
    iput-wide v8, v7, LX/7Gq;->b:J

    .line 1320726
    move-object v5, v7

    .line 1320727
    new-instance v7, Lcom/facebook/user/model/Name;

    invoke-virtual {v6}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 1320728
    iput-object v7, v5, LX/7Gq;->a:Lcom/facebook/user/model/Name;

    .line 1320729
    move-object v5, v5

    .line 1320730
    if-nez v0, :cond_4

    move-object v0, v2

    .line 1320731
    :goto_2
    iput-object v0, v5, LX/7Gq;->c:Ljava/lang/String;

    .line 1320732
    move-object v0, v5

    .line 1320733
    sget-object v5, LX/7Gr;->USER:LX/7Gr;

    .line 1320734
    iput-object v5, v0, LX/7Gq;->e:LX/7Gr;

    .line 1320735
    move-object v0, v0

    .line 1320736
    invoke-virtual {v0}, LX/7Gq;->l()Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1320737
    :cond_5
    invoke-virtual {v5}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1320738
    :cond_6
    iget-boolean v0, p1, LX/8HG;->a:Z

    if-eqz v0, :cond_7

    move-object v2, v1

    .line 1320739
    goto/16 :goto_0

    .line 1320740
    :cond_7
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1320741
    const-string v1, "-1"

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320742
    iget-object v1, p0, LX/8HF;->a:LX/8HH;

    iget-object v1, v1, LX/8HH;->d:LX/8Gm;

    .line 1320743
    iget-object v3, v1, LX/8Gm;->a:LX/0TD;

    new-instance v4, LX/8Gk;

    invoke-direct {v4, v1, v0}, LX/8Gk;-><init>(LX/8Gm;Ljava/util/Map;)V

    invoke-interface {v3, v4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 1320744
    move-object v0, v3

    .line 1320745
    const v1, -0x130371f1

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string v1, "-1"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1320746
    if-eqz v0, :cond_a

    .line 1320747
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1320748
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1320749
    if-nez v1, :cond_8

    .line 1320750
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1320751
    :cond_9
    invoke-interface {v0, v3}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 1320752
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_a
    move-object v2, v0

    .line 1320753
    goto/16 :goto_0
.end method
