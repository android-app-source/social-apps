.class public final LX/8RY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Landroid/widget/TextView;

.field public final synthetic b:Landroid/view/ViewTreeObserver;

.field public final synthetic c:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Landroid/widget/TextView;Landroid/view/ViewTreeObserver;)V
    .locals 0

    .prologue
    .line 1344718
    iput-object p1, p0, LX/8RY;->c:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iput-object p2, p0, LX/8RY;->a:Landroid/widget/TextView;

    iput-object p3, p0, LX/8RY;->b:Landroid/view/ViewTreeObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 3

    .prologue
    .line 1344719
    iget-object v0, p0, LX/8RY;->c:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v1, p0, LX/8RY;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    iget-object v2, p0, LX/8RY;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 1344720
    invoke-static {v0, v1, v2}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a$redex0(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Landroid/text/Layout;Ljava/lang/CharSequence;)V

    .line 1344721
    iget-object v0, p0, LX/8RY;->b:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1344722
    return-void
.end method
