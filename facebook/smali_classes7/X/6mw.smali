.class public final LX/6mw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/proxygen/MQTTClientCallback;


# instance fields
.field public a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic b:LX/6my;


# direct methods
.method public constructor <init>(LX/6my;)V
    .locals 2

    .prologue
    .line 1146930
    iput-object p1, p0, LX/6mw;->b:LX/6my;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146931
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/6mw;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method


# virtual methods
.method public final onConnectFailure(Lcom/facebook/proxygen/MQTTClientError;)V
    .locals 4

    .prologue
    .line 1146917
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "connectFailed err=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146918
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    invoke-virtual {v0}, LX/6my;->a()V

    .line 1146919
    iget v0, p1, Lcom/facebook/proxygen/MQTTClientError;->mConnAckCode:I

    move v0, v0

    .line 1146920
    int-to-byte v1, v0

    .line 1146921
    sparse-switch v1, :sswitch_data_0

    .line 1146922
    sget-object v0, LX/0Hs;->FAILED_CONNECTION_REFUSED:LX/0Hs;

    .line 1146923
    :goto_0
    new-instance v2, LX/07k;

    invoke-direct {v2, v0, v1}, LX/07k;-><init>(LX/0Hs;B)V

    .line 1146924
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    invoke-virtual {v0, v2}, LX/076;->a(LX/07k;)V

    .line 1146925
    return-void

    .line 1146926
    :sswitch_0
    sget-object v0, LX/0Hs;->FAILED_CONNECTION_REFUSED_SERVER_SHEDDING_LOAD:LX/0Hs;

    goto :goto_0

    .line 1146927
    :sswitch_1
    sget-object v0, LX/0Hs;->FAILED_CONNECTION_REFUSED_NOT_AUTHORIZED:LX/0Hs;

    goto :goto_0

    .line 1146928
    :sswitch_2
    sget-object v0, LX/0Hs;->FAILED_CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD:LX/0Hs;

    goto :goto_0

    .line 1146929
    :sswitch_3
    sget-object v0, LX/0Hs;->FAILED_CONNECTION_UNKNOWN_CONNECT_HASH:LX/0Hs;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x5 -> :sswitch_1
        0x11 -> :sswitch_0
        0x13 -> :sswitch_3
    .end sparse-switch
.end method

.method public final onConnectSent()V
    .locals 4

    .prologue
    .line 1146907
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    .line 1146908
    iget-object v1, v0, LX/076;->a:LX/072;

    iget-object v1, v1, LX/072;->E:LX/077;

    .line 1146909
    if-eqz v1, :cond_0

    iget-object v2, v0, LX/076;->a:LX/072;

    iget-boolean v2, v2, LX/072;->s:Z

    if-eqz v2, :cond_0

    .line 1146910
    const-string v2, "FbnsConnectionManager"

    const-string v3, "connection/ConnectSent/runnable"

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {v2, v3, p0}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146911
    iget-object v2, v1, LX/077;->a:LX/056;

    iget-object v2, v2, LX/056;->y:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$1;

    invoke-direct {v3, v1}, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$1;-><init>(LX/077;)V

    const p0, -0x30b608c

    invoke-static {v2, v3, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1146912
    :cond_0
    iget-object v1, v0, LX/076;->a:LX/072;

    iget-object v1, v1, LX/072;->e:LX/06z;

    .line 1146913
    iget-boolean v2, v1, LX/06z;->B:Z

    move v1, v2

    .line 1146914
    if-eqz v1, :cond_1

    .line 1146915
    sget-object v1, LX/074;->CONNECTED:LX/074;

    invoke-virtual {v0, v1}, LX/076;->b(LX/074;)V

    .line 1146916
    :cond_1
    return-void
.end method

.method public final onConnectSuccess([B)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1146890
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "succeed conAck received"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146891
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p1, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-static {v0}, LX/07h;->a(Ljava/lang/String;)LX/07h;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1146892
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    iget-object v2, v1, LX/07h;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/076;->a(Ljava/lang/String;)V

    .line 1146893
    iget-object v0, v1, LX/07h;->c:Ljava/lang/String;

    invoke-static {v0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v1, LX/07h;->d:Ljava/lang/String;

    invoke-static {v0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1146894
    :cond_0
    sget-object v0, LX/06y;->a:LX/06y;

    .line 1146895
    :goto_0
    iget-object v2, p0, LX/6mw;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v6, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1146896
    iget-object v2, p0, LX/6mw;->b:LX/6my;

    iget-object v2, v2, LX/6my;->g:LX/076;

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    const-string v4, "Whisle Client Error"

    const-string v5, "onConnectSuccess being called twice"

    invoke-virtual {v2, v3, v4, v5}, LX/076;->a(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V

    .line 1146897
    :cond_1
    new-instance v2, LX/07k;

    iget-object v3, v1, LX/07h;->a:Ljava/lang/String;

    invoke-static {v3}, LX/05V;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v1, v1, LX/07h;->b:Ljava/lang/String;

    invoke-static {v1}, LX/05V;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, LX/06k;->a(Ljava/lang/String;Ljava/lang/String;)LX/06k;

    move-result-object v1

    invoke-direct {v2, v1, v0}, LX/07k;-><init>(LX/06k;LX/06y;)V

    .line 1146898
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    sget-object v1, LX/074;->CONNECTED:LX/074;

    invoke-virtual {v0, v1}, LX/076;->b(LX/074;)V

    .line 1146899
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    invoke-virtual {v0}, LX/076;->c()V

    .line 1146900
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    invoke-virtual {v0, v2}, LX/076;->a(LX/07k;)V

    .line 1146901
    :goto_1
    return-void

    .line 1146902
    :catch_0
    move-exception v0

    .line 1146903
    sget-object v1, LX/6my;->a:Ljava/lang/String;

    const-string v2, "conAck payload deserialization failure=%s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v0, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146904
    new-instance v1, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_CONNACK_READ:LX/0Hs;

    invoke-direct {v1, v2, v0}, LX/07k;-><init>(LX/0Hs;Ljava/lang/Exception;)V

    .line 1146905
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    invoke-virtual {v0, v1}, LX/076;->a(LX/07k;)V

    goto :goto_1

    .line 1146906
    :cond_2
    new-instance v0, LX/06y;

    iget-object v2, v1, LX/07h;->c:Ljava/lang/String;

    iget-object v3, v1, LX/07h;->d:Ljava/lang/String;

    iget-object v4, p0, LX/6mw;->b:LX/6my;

    iget-object v4, v4, LX/6my;->d:LX/01o;

    invoke-virtual {v4}, LX/01o;->a()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, LX/06y;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public final onError(Lcom/facebook/proxygen/MQTTClientError;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1146880
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "onError=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146881
    iget-object v0, p1, Lcom/facebook/proxygen/MQTTClientError;->mErrType:Lcom/facebook/proxygen/MQTTClientError$MQTTErrorType;

    move-object v0, v0

    .line 1146882
    sget-object v1, Lcom/facebook/proxygen/MQTTClientError$MQTTErrorType;->DISCONNECT:Lcom/facebook/proxygen/MQTTClientError$MQTTErrorType;

    invoke-virtual {v0, v1}, Lcom/facebook/proxygen/MQTTClientError$MQTTErrorType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1146883
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    sget-object v1, LX/0AX;->DISCONNECT_FROM_SERVER:LX/0AX;

    sget-object v2, LX/0BJ;->NETWORK_THREAD_LOOP:LX/0BJ;

    invoke-virtual {v0, v1, v2, v4}, LX/076;->a(LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    .line 1146884
    :goto_0
    return-void

    .line 1146885
    :cond_0
    iget-object v0, p1, Lcom/facebook/proxygen/MQTTClientError;->mErrType:Lcom/facebook/proxygen/MQTTClientError$MQTTErrorType;

    move-object v0, v0

    .line 1146886
    sget-object v1, Lcom/facebook/proxygen/MQTTClientError$MQTTErrorType;->STOPPED_BEFORE_MQTT_CONNECT:Lcom/facebook/proxygen/MQTTClientError$MQTTErrorType;

    invoke-virtual {v0, v1}, Lcom/facebook/proxygen/MQTTClientError$MQTTErrorType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1146887
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    sget-object v1, LX/0AX;->ABORTED_PREEMPTIVE_RECONNECT:LX/0AX;

    sget-object v2, LX/0BJ;->NETWORK_THREAD_LOOP:LX/0BJ;

    invoke-virtual {v0, v1, v2, v4}, LX/076;->a(LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1146888
    :cond_1
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    invoke-static {v0, p1}, LX/6my;->a(LX/6my;Lcom/facebook/proxygen/MQTTClientError;)Ljava/lang/Throwable;

    move-result-object v0

    .line 1146889
    iget-object v1, p0, LX/6mw;->b:LX/6my;

    iget-object v1, v1, LX/6my;->g:LX/076;

    invoke-static {v0}, LX/0AX;->getFromReadException(Ljava/lang/Throwable;)LX/0AX;

    move-result-object v2

    sget-object v3, LX/0BJ;->NETWORK_THREAD_LOOP:LX/0BJ;

    invoke-virtual {v1, v2, v3, v0}, LX/076;->a(LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onPingRequest()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1146877
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "pingReqReceived"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146878
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    iget-object v1, p0, LX/6mw;->b:LX/6my;

    sget-object v2, LX/07S;->PINGREQ:LX/07S;

    invoke-static {v1, v2, v3}, LX/6my;->a(LX/6my;LX/07S;I)LX/07W;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/076;->a(LX/07W;)V

    .line 1146879
    return-void
.end method

.method public final onPingRequestFailure(Lcom/facebook/proxygen/MQTTClientError;)V
    .locals 4

    .prologue
    .line 1146873
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "pingReq failed err=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146874
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    invoke-static {v0, p1}, LX/6my;->a(LX/6my;Lcom/facebook/proxygen/MQTTClientError;)Ljava/lang/Throwable;

    move-result-object v0

    .line 1146875
    iget-object v1, p0, LX/6mw;->b:LX/6my;

    iget-object v1, v1, LX/6my;->g:LX/076;

    invoke-static {v0}, LX/0AX;->getFromWriteException(Ljava/lang/Throwable;)LX/0AX;

    move-result-object v2

    sget-object v3, LX/0BJ;->PING:LX/0BJ;

    invoke-virtual {v1, v2, v3, v0}, LX/076;->a(LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    .line 1146876
    return-void
.end method

.method public final onPingRequestSent()V
    .locals 3

    .prologue
    .line 1146870
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "pingReq Sent"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146871
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    sget-object v1, LX/07S;->PINGREQ:LX/07S;

    invoke-virtual {v1}, LX/07S;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, LX/076;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1146872
    return-void
.end method

.method public final onPingResponse()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1146867
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "pingRespReceived"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146868
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    iget-object v1, p0, LX/6mw;->b:LX/6my;

    sget-object v2, LX/07S;->PINGRESP:LX/07S;

    invoke-static {v1, v2, v3}, LX/6my;->a(LX/6my;LX/07S;I)LX/07W;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/076;->a(LX/07W;)V

    .line 1146869
    return-void
.end method

.method public final onPingResponseFailure(Lcom/facebook/proxygen/MQTTClientError;)V
    .locals 4

    .prologue
    .line 1146932
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "pingResp failed err=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146933
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    invoke-static {v0, p1}, LX/6my;->a(LX/6my;Lcom/facebook/proxygen/MQTTClientError;)Ljava/lang/Throwable;

    move-result-object v0

    .line 1146934
    iget-object v1, p0, LX/6mw;->b:LX/6my;

    iget-object v1, v1, LX/6my;->g:LX/076;

    invoke-static {v0}, LX/0AX;->getFromWriteException(Ljava/lang/Throwable;)LX/0AX;

    move-result-object v2

    sget-object v3, LX/0BJ;->PINGRESP:LX/0BJ;

    invoke-virtual {v1, v2, v3, v0}, LX/076;->a(LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    .line 1146935
    return-void
.end method

.method public final onPublish(Ljava/lang/String;[BII)V
    .locals 6

    .prologue
    .line 1146861
    new-instance v0, LX/07R;

    sget-object v1, LX/07S;->PUBLISH:LX/07S;

    invoke-direct {v0, v1, p3}, LX/07R;-><init>(LX/07S;I)V

    .line 1146862
    new-instance v1, LX/0B5;

    invoke-direct {v1, p1, p4}, LX/0B5;-><init>(Ljava/lang/String;I)V

    .line 1146863
    new-instance v2, LX/07X;

    invoke-direct {v2, v0, v1, p2}, LX/07X;-><init>(LX/07R;LX/0B5;[B)V

    .line 1146864
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "publishReceived topic=%s, qos=%d, msgId=%d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v1, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146865
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    invoke-virtual {v0, v2}, LX/076;->a(LX/07W;)V

    .line 1146866
    return-void
.end method

.method public final onPublishAck(I)V
    .locals 5

    .prologue
    .line 1146858
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "pubAckReceived msgId=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146859
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    iget-object v1, p0, LX/6mw;->b:LX/6my;

    sget-object v2, LX/07S;->PUBACK:LX/07S;

    invoke-static {v1, v2, p1}, LX/6my;->a(LX/6my;LX/07S;I)LX/07W;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/076;->a(LX/07W;)V

    .line 1146860
    return-void
.end method

.method public final onPublishAckFailure(Lcom/facebook/proxygen/MQTTClientError;)V
    .locals 4

    .prologue
    .line 1146854
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "pubAck failed err=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146855
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    invoke-static {v0, p1}, LX/6my;->a(LX/6my;Lcom/facebook/proxygen/MQTTClientError;)Ljava/lang/Throwable;

    move-result-object v0

    .line 1146856
    iget-object v1, p0, LX/6mw;->b:LX/6my;

    iget-object v1, v1, LX/6my;->g:LX/076;

    invoke-static {v0}, LX/0AX;->getFromWriteException(Ljava/lang/Throwable;)LX/0AX;

    move-result-object v2

    sget-object v3, LX/0BJ;->PUBACK:LX/0BJ;

    invoke-virtual {v1, v2, v3, v0}, LX/076;->a(LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    .line 1146857
    return-void
.end method

.method public final onPublishFailure(ILcom/facebook/proxygen/MQTTClientError;)V
    .locals 5

    .prologue
    .line 1146850
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "publishFailed msgId=%d, err=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146851
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    invoke-static {v0, p2}, LX/6my;->a(LX/6my;Lcom/facebook/proxygen/MQTTClientError;)Ljava/lang/Throwable;

    move-result-object v0

    .line 1146852
    iget-object v1, p0, LX/6mw;->b:LX/6my;

    iget-object v1, v1, LX/6my;->g:LX/076;

    invoke-static {v0}, LX/0AX;->getFromWriteException(Ljava/lang/Throwable;)LX/0AX;

    move-result-object v2

    sget-object v3, LX/0BJ;->PUBLISH:LX/0BJ;

    invoke-virtual {v1, v2, v3, v0}, LX/076;->a(LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    .line 1146853
    return-void
.end method

.method public final onPublishSent(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 1146845
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "publishSucceed topic=%s, msgId=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146846
    invoke-static {p1}, LX/0AJ;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1146847
    if-nez v0, :cond_0

    .line 1146848
    :goto_0
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    sget-object v1, LX/07S;->PUBLISH:LX/07S;

    invoke-virtual {v1}, LX/07S;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/076;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1146849
    return-void

    :cond_0
    move-object p1, v0

    goto :goto_0
.end method

.method public final onSubscribeAck(I)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1146842
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "SubAck msgId=%d, messageId"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146843
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    iget-object v1, p0, LX/6mw;->b:LX/6my;

    sget-object v2, LX/07S;->SUBACK:LX/07S;

    invoke-static {v1, v2, p1}, LX/6my;->a(LX/6my;LX/07S;I)LX/07W;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/076;->a(LX/07W;)V

    .line 1146844
    return-void
.end method

.method public final onSubscribeFailure(ILcom/facebook/proxygen/MQTTClientError;)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1146838
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "Subscribe should not be used"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146839
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    invoke-static {v0, p2}, LX/6my;->a(LX/6my;Lcom/facebook/proxygen/MQTTClientError;)Ljava/lang/Throwable;

    move-result-object v0

    .line 1146840
    iget-object v1, p0, LX/6mw;->b:LX/6my;

    iget-object v1, v1, LX/6my;->g:LX/076;

    invoke-static {v0}, LX/0AX;->getFromWriteException(Ljava/lang/Throwable;)LX/0AX;

    move-result-object v2

    sget-object v3, LX/0BJ;->SUBSCRIBE:LX/0BJ;

    invoke-virtual {v1, v2, v3, v0}, LX/076;->a(LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    .line 1146841
    return-void
.end method

.method public final onUnsubscribeAck(I)V
    .locals 5

    .prologue
    .line 1146835
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "unsubAckReceived msgId=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146836
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    iget-object v0, v0, LX/6my;->g:LX/076;

    iget-object v1, p0, LX/6mw;->b:LX/6my;

    sget-object v2, LX/07S;->UNSUBACK:LX/07S;

    invoke-static {v1, v2, p1}, LX/6my;->a(LX/6my;LX/07S;I)LX/07W;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/076;->a(LX/07W;)V

    .line 1146837
    return-void
.end method

.method public final onUnsubscribeFailure(ILcom/facebook/proxygen/MQTTClientError;)V
    .locals 5

    .prologue
    .line 1146831
    sget-object v0, LX/6my;->a:Ljava/lang/String;

    const-string v1, "unSub failed msgId=%d, err=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146832
    iget-object v0, p0, LX/6mw;->b:LX/6my;

    invoke-static {v0, p2}, LX/6my;->a(LX/6my;Lcom/facebook/proxygen/MQTTClientError;)Ljava/lang/Throwable;

    move-result-object v0

    .line 1146833
    iget-object v1, p0, LX/6mw;->b:LX/6my;

    iget-object v1, v1, LX/6my;->g:LX/076;

    invoke-static {v0}, LX/0AX;->getFromWriteException(Ljava/lang/Throwable;)LX/0AX;

    move-result-object v2

    sget-object v3, LX/0BJ;->UNSUBSCRIBE:LX/0BJ;

    invoke-virtual {v1, v2, v3, v0}, LX/076;->a(LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    .line 1146834
    return-void
.end method
