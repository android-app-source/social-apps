.class public final LX/8KS;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V
    .locals 0

    .prologue
    .line 1330042
    iput-object p1, p0, LX/8KS;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;B)V
    .locals 0

    .prologue
    .line 1330041
    invoke-direct {p0, p1}, LX/8KS;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    return-void
.end method

.method private a(Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;)V
    .locals 3

    .prologue
    .line 1330023
    iget-object v0, p0, LX/8KS;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v0, v0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8KS;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v0, v0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    if-eqz v0, :cond_0

    .line 1330024
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 1330025
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v1

    .line 1330026
    iget-object v1, p0, LX/8KS;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v1, v1, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330027
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1330028
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1330029
    iget-object v0, p0, LX/8KS;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v0, v0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 1330030
    iget-boolean v0, p1, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->b:Z

    move v0, v0

    .line 1330031
    if-nez v0, :cond_0

    .line 1330032
    iget-object v0, p0, LX/8KS;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    .line 1330033
    iget-object v1, p1, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->a:Landroid/content/Intent;

    move-object v1, v1

    .line 1330034
    iput-object v1, v0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->E:Landroid/content/Intent;

    .line 1330035
    iget-object v1, p0, LX/8KS;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v0, p0, LX/8KS;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v0, v0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->E:Landroid/content/Intent;

    const-string v2, "uploadOp"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330036
    iput-object v0, v1, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330037
    iget-object v0, p0, LX/8KS;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    invoke-static {v0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->b(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    .line 1330038
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1330040
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;

    return-object v0
.end method

.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 1330039
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;

    invoke-direct {p0, p1}, LX/8KS;->a(Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;)V

    return-void
.end method
