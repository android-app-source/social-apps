.class public LX/7yh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/7yi;",
        "LX/7yj;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/7yd;

.field private final b:Lcom/facebook/performancelogger/PerformanceLogger;


# direct methods
.method public constructor <init>(LX/7yd;Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1279931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279932
    iput-object p1, p0, LX/7yh;->a:LX/7yd;

    .line 1279933
    iput-object p2, p0, LX/7yh;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 1279934
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 1279935
    check-cast p1, LX/7yi;

    .line 1279936
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1279937
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "JSON"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1279938
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "facerec"

    .line 1279939
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1279940
    move-object v1, v1

    .line 1279941
    const-string v2, "POST"

    .line 1279942
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1279943
    move-object v1, v1

    .line 1279944
    const-string v2, "method/photos.getSuggestedTags"

    .line 1279945
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1279946
    move-object v1, v1

    .line 1279947
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1279948
    iget-object v2, p1, LX/7yi;->b:Ljava/util/List;

    move-object v2, v2

    .line 1279949
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1279950
    if-eqz v2, :cond_0

    .line 1279951
    iget-object v5, v2, Lcom/facebook/photos/base/tagging/FaceBox;->i:[B

    move-object v5, v5

    .line 1279952
    if-eqz v5, :cond_0

    .line 1279953
    new-instance v5, LX/4cq;

    .line 1279954
    iget-object v6, v2, Lcom/facebook/photos/base/tagging/FaceBox;->i:[B

    move-object v6, v6

    .line 1279955
    const-string v7, "image/jpeg"

    .line 1279956
    iget-object p0, v2, Lcom/facebook/photos/base/tagging/FaceBox;->a:Ljava/lang/String;

    move-object p0, p0

    .line 1279957
    invoke-direct {v5, v6, v7, p0}, LX/4cq;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    .line 1279958
    new-instance v6, LX/4cQ;

    .line 1279959
    iget-object v7, v2, Lcom/facebook/photos/base/tagging/FaceBox;->a:Ljava/lang/String;

    move-object v2, v7

    .line 1279960
    invoke-direct {v6, v2, v5}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 1279961
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1279962
    :cond_1
    move-object v2, v3

    .line 1279963
    iput-object v2, v1, LX/14O;->l:Ljava/util/List;

    .line 1279964
    move-object v1, v1

    .line 1279965
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1279966
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1279967
    move-object v0, v1

    .line 1279968
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1279969
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1279970
    move-object v0, v0

    .line 1279971
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1279972
    check-cast p1, LX/7yi;

    .line 1279973
    new-instance v0, LX/7yj;

    iget-object v1, p0, LX/7yh;->a:LX/7yd;

    invoke-direct {v0, v1}, LX/7yj;-><init>(LX/7yd;)V

    .line 1279974
    iget v1, p2, LX/1pN;->b:I

    move v1, v1

    .line 1279975
    const/16 v2, 0xc8

    if-ne v1, v2, :cond_0

    .line 1279976
    iget-object v1, p1, LX/7yi;->a:LX/03V;

    move-object v1, v1

    .line 1279977
    invoke-virtual {v0, p2, v1}, LX/7yj;->a(LX/1pN;LX/03V;)LX/7yj;

    move-result-object v0

    .line 1279978
    :goto_0
    return-object v0

    .line 1279979
    :cond_0
    const-string v1, "FaceRecMethod"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HTTP Error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1279980
    iget v3, p2, LX/1pN;->b:I

    move v3, v3

    .line 1279981
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1279982
    iget-object v1, p0, LX/7yh;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x3b0001

    const-string v3, "FaceRecServerCommunication"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 1279983
    iget-object v1, p1, LX/7yi;->a:LX/03V;

    move-object v1, v1

    .line 1279984
    const-string v2, "FaceRecMethod"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "HTTP Error: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1279985
    iget-object v4, p2, LX/1pN;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 1279986
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
