.class public LX/8Op;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1341112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/0b3;JF)V
    .locals 6

    .prologue
    .line 1341113
    const/4 v0, 0x0

    cmpl-float v0, p4, v0

    if-eqz v0, :cond_0

    .line 1341114
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    long-to-float v1, p2

    div-float/2addr v1, p4

    float-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v4

    .line 1341115
    const-wide/16 v0, 0x30

    cmp-long v0, v4, v0

    if-ltz v0, :cond_0

    .line 1341116
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->V:Z

    move v0, v0

    .line 1341117
    if-nez v0, :cond_0

    .line 1341118
    const/4 v0, 0x1

    .line 1341119
    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->V:Z

    .line 1341120
    new-instance v0, LX/8Kl;

    sget-object v2, LX/8KZ;->UPLOADING:LX/8KZ;

    const/4 v3, -0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/8Kl;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;IJ)V

    invoke-virtual {p1, v0}, LX/0b4;->a(LX/0b7;)V

    .line 1341121
    :cond_0
    return-void
.end method
