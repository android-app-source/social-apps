.class public LX/7Y7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/7Y7;


# instance fields
.field public final a:LX/0Sh;


# direct methods
.method public constructor <init>(LX/0Sh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1218964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1218965
    iput-object p1, p0, LX/7Y7;->a:LX/0Sh;

    .line 1218966
    return-void
.end method

.method public static a(LX/0QB;)LX/7Y7;
    .locals 4

    .prologue
    .line 1218949
    sget-object v0, LX/7Y7;->b:LX/7Y7;

    if-nez v0, :cond_1

    .line 1218950
    const-class v1, LX/7Y7;

    monitor-enter v1

    .line 1218951
    :try_start_0
    sget-object v0, LX/7Y7;->b:LX/7Y7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1218952
    if-eqz v2, :cond_0

    .line 1218953
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1218954
    new-instance p0, LX/7Y7;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-direct {p0, v3}, LX/7Y7;-><init>(LX/0Sh;)V

    .line 1218955
    move-object v0, p0

    .line 1218956
    sput-object v0, LX/7Y7;->b:LX/7Y7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1218957
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1218958
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1218959
    :cond_1
    sget-object v0, LX/7Y7;->b:LX/7Y7;

    return-object v0

    .line 1218960
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1218961
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;",
            "LX/0TF",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1218962
    iget-object v0, p0, LX/7Y7;->a:LX/0Sh;

    invoke-virtual {v0, p1, p2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1218963
    return-void
.end method
