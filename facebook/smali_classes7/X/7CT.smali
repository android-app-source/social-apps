.class public LX/7CT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/common/locale/ForPrimaryCanonicalDecomposition;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/text/Collator;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1180934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1180935
    return-void
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1180918
    if-nez p0, :cond_1

    .line 1180919
    const-string p0, ""

    .line 1180920
    :cond_0
    :goto_0
    return-object p0

    .line 1180921
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    new-array v3, v0, [I

    .line 1180922
    invoke-static {p0}, LX/3hP;->a(Ljava/lang/String;)LX/3hP;

    move-result-object v4

    move v0, v1

    move v2, v1

    .line 1180923
    :cond_2
    :goto_1
    invoke-virtual {v4}, LX/3hP;->a()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1180924
    invoke-virtual {v4}, LX/3hP;->b()I

    move-result v5

    .line 1180925
    invoke-static {v5}, Ljava/lang/Character;->isLetterOrDigit(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1180926
    add-int/lit8 v0, v2, 0x1

    aput v5, v3, v2

    move v2, v0

    move v0, v1

    .line 1180927
    goto :goto_1

    .line 1180928
    :cond_3
    invoke-static {v5}, Ljava/lang/Character;->isWhitespace(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1180929
    if-nez v0, :cond_4

    .line 1180930
    add-int/lit8 v0, v2, 0x1

    const/16 v5, 0x20

    aput v5, v3, v2

    move v2, v0

    .line 1180931
    :cond_4
    const/4 v0, 0x1

    goto :goto_1

    .line 1180932
    :cond_5
    array-length v0, v3

    if-eq v2, v0, :cond_0

    .line 1180933
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v3, v1, v2}, Ljava/lang/String;-><init>([III)V

    goto :goto_0
.end method
