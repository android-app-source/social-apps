.class public final enum LX/71C;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/71C;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/71C;

.field public static final enum CONTACT_INFORMATION:LX/71C;

.field public static final enum FB_PAYMENT_SETTINGS:LX/71C;

.field public static final enum MESSENGER_COMMERCE:LX/71C;

.field public static final enum MESSENGER_COMMERCE_SHIPPING_OPTION_PICKER:LX/71C;

.field public static final enum MESSENGER_PAYMENT_SETTINGS:LX/71C;

.field public static final enum PAYMENTS_OPTIONS_PICKER:LX/71C;

.field public static final enum PAYMENT_HISTORY:LX/71C;

.field public static final enum PAYMENT_METHODS:LX/71C;

.field public static final enum SIMPLE:LX/71C;

.field public static final enum SIMPLE_SHIPPING_ADDRESS:LX/71C;

.field public static final enum SIMPLE_SHIPPING_OPTION_PICKER:LX/71C;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1163146
    new-instance v0, LX/71C;

    const-string v1, "CONTACT_INFORMATION"

    invoke-direct {v0, v1, v3}, LX/71C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71C;->CONTACT_INFORMATION:LX/71C;

    .line 1163147
    new-instance v0, LX/71C;

    const-string v1, "FB_PAYMENT_SETTINGS"

    invoke-direct {v0, v1, v4}, LX/71C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71C;->FB_PAYMENT_SETTINGS:LX/71C;

    .line 1163148
    new-instance v0, LX/71C;

    const-string v1, "MESSENGER_COMMERCE"

    invoke-direct {v0, v1, v5}, LX/71C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71C;->MESSENGER_COMMERCE:LX/71C;

    .line 1163149
    new-instance v0, LX/71C;

    const-string v1, "MESSENGER_COMMERCE_SHIPPING_OPTION_PICKER"

    invoke-direct {v0, v1, v6}, LX/71C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71C;->MESSENGER_COMMERCE_SHIPPING_OPTION_PICKER:LX/71C;

    .line 1163150
    new-instance v0, LX/71C;

    const-string v1, "MESSENGER_PAYMENT_SETTINGS"

    invoke-direct {v0, v1, v7}, LX/71C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71C;->MESSENGER_PAYMENT_SETTINGS:LX/71C;

    .line 1163151
    new-instance v0, LX/71C;

    const-string v1, "PAYMENT_HISTORY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/71C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71C;->PAYMENT_HISTORY:LX/71C;

    .line 1163152
    new-instance v0, LX/71C;

    const-string v1, "PAYMENT_METHODS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/71C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71C;->PAYMENT_METHODS:LX/71C;

    .line 1163153
    new-instance v0, LX/71C;

    const-string v1, "PAYMENTS_OPTIONS_PICKER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/71C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71C;->PAYMENTS_OPTIONS_PICKER:LX/71C;

    .line 1163154
    new-instance v0, LX/71C;

    const-string v1, "SIMPLE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/71C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71C;->SIMPLE:LX/71C;

    .line 1163155
    new-instance v0, LX/71C;

    const-string v1, "SIMPLE_SHIPPING_OPTION_PICKER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/71C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71C;->SIMPLE_SHIPPING_OPTION_PICKER:LX/71C;

    .line 1163156
    new-instance v0, LX/71C;

    const-string v1, "SIMPLE_SHIPPING_ADDRESS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/71C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71C;->SIMPLE_SHIPPING_ADDRESS:LX/71C;

    .line 1163157
    const/16 v0, 0xb

    new-array v0, v0, [LX/71C;

    sget-object v1, LX/71C;->CONTACT_INFORMATION:LX/71C;

    aput-object v1, v0, v3

    sget-object v1, LX/71C;->FB_PAYMENT_SETTINGS:LX/71C;

    aput-object v1, v0, v4

    sget-object v1, LX/71C;->MESSENGER_COMMERCE:LX/71C;

    aput-object v1, v0, v5

    sget-object v1, LX/71C;->MESSENGER_COMMERCE_SHIPPING_OPTION_PICKER:LX/71C;

    aput-object v1, v0, v6

    sget-object v1, LX/71C;->MESSENGER_PAYMENT_SETTINGS:LX/71C;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/71C;->PAYMENT_HISTORY:LX/71C;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/71C;->PAYMENT_METHODS:LX/71C;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/71C;->PAYMENTS_OPTIONS_PICKER:LX/71C;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/71C;->SIMPLE:LX/71C;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/71C;->SIMPLE_SHIPPING_OPTION_PICKER:LX/71C;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/71C;->SIMPLE_SHIPPING_ADDRESS:LX/71C;

    aput-object v2, v0, v1

    sput-object v0, LX/71C;->$VALUES:[LX/71C;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1163143
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/71C;
    .locals 1

    .prologue
    .line 1163145
    const-class v0, LX/71C;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/71C;

    return-object v0
.end method

.method public static values()[LX/71C;
    .locals 1

    .prologue
    .line 1163144
    sget-object v0, LX/71C;->$VALUES:[LX/71C;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/71C;

    return-object v0
.end method
