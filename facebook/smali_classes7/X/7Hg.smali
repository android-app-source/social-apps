.class public LX/7Hg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/ui/typeahead/TypeaheadFetchStrategy",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/7He;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public b:LX/7He;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Hg",
            "<TT;>.TypeaheadFetchHandler<TT;>;"
        }
    .end annotation
.end field

.field public final d:LX/0Sh;

.field private final e:LX/7Ho;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Ho",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final f:LX/7Hd;

.field public g:Z

.field private h:LX/7HS;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7HS",
            "<TT;>;"
        }
    .end annotation
.end field

.field public i:LX/7HS;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7HS",
            "<TT;>;"
        }
    .end annotation
.end field

.field public j:LX/2Sq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2Sq",
            "<TT;>;"
        }
    .end annotation
.end field

.field public k:LX/2Sp;

.field public l:LX/7B6;

.field public m:LX/7HZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1191041
    const-class v0, LX/7He;

    sput-object v0, LX/7Hg;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/7Ho;LX/7Hd;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1191042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1191043
    sget-object v0, LX/7B6;->a:LX/7B6;

    iput-object v0, p0, LX/7Hg;->l:LX/7B6;

    .line 1191044
    sget-object v0, LX/7HZ;->IDLE:LX/7HZ;

    iput-object v0, p0, LX/7Hg;->m:LX/7HZ;

    .line 1191045
    iput-object p1, p0, LX/7Hg;->d:LX/0Sh;

    .line 1191046
    iput-object p2, p0, LX/7Hg;->e:LX/7Ho;

    .line 1191047
    iput-object p3, p0, LX/7Hg;->f:LX/7Hd;

    .line 1191048
    return-void
.end method

.method public static a(LX/7Hg;LX/7HZ;)Z
    .locals 2

    .prologue
    .line 1191051
    iget-object v0, p0, LX/7Hg;->b:LX/7He;

    .line 1191052
    iget-object v1, v0, LX/7He;->e:LX/7HZ;

    move-object v0, v1

    .line 1191053
    if-eq v0, p1, :cond_0

    invoke-static {p0}, LX/7Hg;->g(LX/7Hg;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/7Hg;->a:LX/7He;

    .line 1191054
    iget-object v1, v0, LX/7He;->e:LX/7HZ;

    move-object v0, v1

    .line 1191055
    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/7Hg;
    .locals 4

    .prologue
    .line 1191049
    new-instance v3, LX/7Hg;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-static {p0}, LX/7Ho;->b(LX/0QB;)LX/7Ho;

    move-result-object v1

    check-cast v1, LX/7Ho;

    invoke-static {p0}, LX/7Hd;->b(LX/0QB;)LX/7Hd;

    move-result-object v2

    check-cast v2, LX/7Hd;

    invoke-direct {v3, v0, v1, v2}, LX/7Hg;-><init>(LX/0Sh;LX/7Ho;LX/7Hd;)V

    .line 1191050
    return-object v3
.end method

.method public static g(LX/7Hg;)Z
    .locals 1

    .prologue
    .line 1191038
    iget-object v0, p0, LX/7Hg;->h:LX/7HS;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7Hg;->a:LX/7He;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7HS;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7HS",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1191039
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/7Hg;->a(LX/7HS;I)V

    .line 1191040
    return-void
.end method

.method public final a(LX/7HS;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7HS",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 1191033
    iput-object p1, p0, LX/7Hg;->i:LX/7HS;

    .line 1191034
    new-instance v0, LX/7He;

    iget-object v2, p0, LX/7Hg;->i:LX/7HS;

    sget-object v4, LX/7Hf;->REMOTE:LX/7Hf;

    move-object v1, p0

    move-object v3, p0

    move v5, p2

    invoke-direct/range {v0 .. v5}, LX/7He;-><init>(LX/7Hg;LX/7HS;LX/7Hg;LX/7Hf;I)V

    iput-object v0, p0, LX/7Hg;->b:LX/7He;

    .line 1191035
    iget-object v0, p0, LX/7Hg;->i:LX/7HS;

    iget-object v1, p0, LX/7Hg;->b:LX/7He;

    invoke-interface {v0, v1}, LX/7HS;->a(LX/2Sq;)V

    .line 1191036
    iget-object v0, p0, LX/7Hg;->i:LX/7HS;

    iget-object v1, p0, LX/7Hg;->b:LX/7He;

    invoke-interface {v0, v1}, LX/7HS;->a(LX/2Sp;)V

    .line 1191037
    return-void
.end method

.method public a(LX/7Hi;LX/7Hf;)V
    .locals 1
    .param p2    # LX/7Hf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<TT;>;",
            "LX/7Hf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1191030
    iget-object v0, p0, LX/7Hg;->j:LX/2Sq;

    if-eqz v0, :cond_0

    .line 1191031
    iget-object v0, p0, LX/7Hg;->j:LX/2Sq;

    invoke-interface {v0, p1}, LX/2Sq;->a(LX/7Hi;)V

    .line 1191032
    :cond_0
    return-void
.end method

.method public a(LX/7B6;)Z
    .locals 9

    .prologue
    .line 1191001
    iput-object p1, p0, LX/7Hg;->l:LX/7B6;

    .line 1191002
    iget-object v0, p0, LX/7Hg;->e:LX/7Ho;

    iget-object v1, p0, LX/7Hg;->l:LX/7B6;

    .line 1191003
    iget-object v2, v0, LX/7Ho;->a:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1191004
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7HY;

    invoke-virtual {v3}, LX/7HY;->isRemote()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1191005
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7Hn;

    .line 1191006
    iget-object v3, v1, LX/7B6;->c:Ljava/lang/String;

    iget-object p1, v1, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, LX/7Hn;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1191007
    iget-object v3, v1, LX/7B6;->c:Ljava/lang/String;

    iget-object v4, v1, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/7Hn;->a(Ljava/lang/String;Ljava/lang/String;)LX/7Hi;

    move-result-object v2

    .line 1191008
    :goto_0
    move-object v0, v2

    .line 1191009
    if-eqz v0, :cond_1

    .line 1191010
    sget-object v1, LX/7Hf;->MEMORY:LX/7Hf;

    invoke-virtual {p0, v0, v1}, LX/7Hg;->a(LX/7Hi;LX/7Hf;)V

    .line 1191011
    const/4 v0, 0x0

    .line 1191012
    :goto_1
    return v0

    .line 1191013
    :cond_1
    invoke-virtual {p0}, LX/7Hg;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1191014
    invoke-virtual {p0}, LX/7Hg;->d()V

    .line 1191015
    :cond_2
    invoke-static {p0}, LX/7Hg;->g(LX/7Hg;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/7Hg;->l:LX/7B6;

    invoke-virtual {v2}, LX/7B6;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1191016
    iget-object v2, p0, LX/7Hg;->a:LX/7He;

    invoke-static {v2}, LX/7He;->b(LX/7He;)V

    .line 1191017
    :cond_3
    iget-object v2, p0, LX/7Hg;->i:LX/7HS;

    const-string v3, "Remote Typeahead fetcher hasn\'t been set yet!"

    invoke-static {v2, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1191018
    iget-object v2, p0, LX/7Hg;->l:LX/7B6;

    iget-object v2, v2, LX/7B6;->b:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, LX/7Hg;->l:LX/7B6;

    iget-object v4, v4, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->codePointCount(II)I

    move-result v2

    iget-object v3, p0, LX/7Hg;->f:LX/7Hd;

    .line 1191019
    iget-object v5, v3, LX/7Hd;->b:LX/0W3;

    sget-wide v7, LX/0X5;->hn:J

    const/4 v6, 0x0

    invoke-interface {v5, v7, v8, v6}, LX/0W4;->a(JZ)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1191020
    iget-object v5, v3, LX/7Hd;->a:LX/0uf;

    sget-wide v7, LX/0X5;->hm:J

    invoke-virtual {v5, v7, v8}, LX/0uf;->a(J)LX/1jr;

    move-result-object v5

    const-string v6, "trigger_count"

    const-wide/16 v7, 0x3

    invoke-virtual {v5, v6, v7, v8}, LX/1jr;->a(Ljava/lang/String;J)J

    move-result-wide v5

    long-to-int v5, v5

    .line 1191021
    :goto_2
    move v3, v5

    .line 1191022
    if-lt v2, v3, :cond_5

    .line 1191023
    iget-object v2, p0, LX/7Hg;->b:LX/7He;

    invoke-static {v2}, LX/7He;->b(LX/7He;)V

    .line 1191024
    :goto_3
    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 1191025
    :cond_5
    iget-object v2, p0, LX/7Hg;->b:LX/7He;

    iget-object v3, p0, LX/7Hg;->f:LX/7Hd;

    .line 1191026
    iget-object v5, v3, LX/7Hd;->b:LX/0W3;

    sget-wide v7, LX/0X5;->hn:J

    const/4 v6, 0x0

    invoke-interface {v5, v7, v8, v6}, LX/0W4;->a(JZ)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1191027
    iget-object v5, v3, LX/7Hd;->a:LX/0uf;

    sget-wide v7, LX/0X5;->hm:J

    invoke-virtual {v5, v7, v8}, LX/0uf;->a(J)LX/1jr;

    move-result-object v5

    const-string v6, "initial_delay"

    const-wide/16 v7, 0x12c

    invoke-virtual {v5, v6, v7, v8}, LX/1jr;->a(Ljava/lang/String;J)J

    move-result-wide v5

    long-to-int v5, v5

    .line 1191028
    :goto_4
    move v3, v5

    .line 1191029
    invoke-static {v2, v3}, LX/7He;->a$redex0(LX/7He;I)V

    goto :goto_3

    :cond_6
    const/4 v5, 0x3

    goto :goto_2

    :cond_7
    const/16 v5, 0x12c

    goto :goto_4
.end method

.method public final b(LX/7HS;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7HS",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1190996
    iput-object p1, p0, LX/7Hg;->h:LX/7HS;

    .line 1190997
    new-instance v0, LX/7He;

    iget-object v2, p0, LX/7Hg;->h:LX/7HS;

    sget-object v4, LX/7Hf;->LOCAL:LX/7Hf;

    const/4 v5, 0x1

    move-object v1, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, LX/7He;-><init>(LX/7Hg;LX/7HS;LX/7Hg;LX/7Hf;I)V

    iput-object v0, p0, LX/7Hg;->a:LX/7He;

    .line 1190998
    iget-object v0, p0, LX/7Hg;->h:LX/7HS;

    iget-object v1, p0, LX/7Hg;->a:LX/7He;

    invoke-interface {v0, v1}, LX/7HS;->a(LX/2Sq;)V

    .line 1190999
    iget-object v0, p0, LX/7Hg;->h:LX/7HS;

    iget-object v1, p0, LX/7Hg;->a:LX/7He;

    invoke-interface {v0, v1}, LX/7HS;->a(LX/2Sp;)V

    .line 1191000
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 1190995
    const/4 v0, 0x0

    return v0
.end method

.method public final d()V
    .locals 7

    .prologue
    .line 1190978
    iget-object v0, p0, LX/7Hg;->e:LX/7Ho;

    iget-object v1, p0, LX/7Hg;->l:LX/7B6;

    .line 1190979
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1190980
    iget-object v2, v0, LX/7Ho;->c:LX/0Uh;

    sget v4, LX/7Hh;->a:I

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v4

    .line 1190981
    if-nez v4, :cond_0

    .line 1190982
    invoke-static {v0, v1, v3}, LX/7Ho;->a(LX/7Ho;LX/7B6;Ljava/util/List;)V

    .line 1190983
    :cond_0
    iget-object v2, v0, LX/7Ho;->a:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7HY;

    .line 1190984
    invoke-virtual {v2}, LX/7HY;->isRemote()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1190985
    invoke-static {v0, v1, v2}, LX/7Ho;->a(LX/7Ho;LX/7B6;LX/7HY;)LX/7Hi;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1190986
    :cond_2
    if-eqz v4, :cond_3

    .line 1190987
    invoke-static {v0, v1, v3}, LX/7Ho;->a(LX/7Ho;LX/7B6;Ljava/util/List;)V

    .line 1190988
    :cond_3
    move-object v0, v3

    .line 1190989
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Hi;

    .line 1190990
    new-instance v2, LX/7Hi;

    .line 1190991
    iget-object v3, v0, LX/7Hi;->a:LX/7B6;

    move-object v3, v3

    .line 1190992
    iget-object v4, v0, LX/7Hi;->b:LX/7Hc;

    move-object v0, v4

    .line 1190993
    sget-object v4, LX/7HY;->MEMORY_CACHE:LX/7HY;

    sget-object v5, LX/7Ha;->PREFIX:LX/7Ha;

    invoke-direct {v2, v3, v0, v4, v5}, LX/7Hi;-><init>(LX/7B6;LX/7Hc;LX/7HY;LX/7Ha;)V

    sget-object v0, LX/7Hf;->MEMORY:LX/7Hf;

    invoke-virtual {p0, v2, v0}, LX/7Hg;->a(LX/7Hi;LX/7Hf;)V

    goto :goto_1

    .line 1190994
    :cond_4
    return-void
.end method
