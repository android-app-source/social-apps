.class public LX/6mV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1144892
    new-instance v0, LX/1sv;

    const-string v1, "PresenceChangeSubscribe"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mV;->b:LX/1sv;

    .line 1144893
    new-instance v0, LX/1sw;

    const-string v1, "ids"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mV;->c:LX/1sw;

    .line 1144894
    sput-boolean v3, LX/6mV;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1144895
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1144896
    iput-object p1, p0, LX/6mV;->ids:Ljava/util/List;

    .line 1144897
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1144898
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1144899
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1144900
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1144901
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PresenceChangeSubscribe"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1144902
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144903
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144904
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144905
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144906
    const-string v4, "ids"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144907
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144908
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144909
    iget-object v0, p0, LX/6mV;->ids:Ljava/util/List;

    if-nez v0, :cond_3

    .line 1144910
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144911
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144912
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144913
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1144914
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 1144915
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 1144916
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 1144917
    :cond_3
    iget-object v0, p0, LX/6mV;->ids:Ljava/util/List;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 4

    .prologue
    .line 1144918
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1144919
    iget-object v0, p0, LX/6mV;->ids:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1144920
    sget-object v0, LX/6mV;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1144921
    new-instance v0, LX/1u3;

    const/16 v1, 0xa

    iget-object v2, p0, LX/6mV;->ids:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1144922
    iget-object v0, p0, LX/6mV;->ids:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1144923
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, LX/1su;->a(J)V

    goto :goto_0

    .line 1144924
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1144925
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1144926
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1144927
    if-nez p1, :cond_1

    .line 1144928
    :cond_0
    :goto_0
    return v0

    .line 1144929
    :cond_1
    instance-of v1, p1, LX/6mV;

    if-eqz v1, :cond_0

    .line 1144930
    check-cast p1, LX/6mV;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1144931
    if-nez p1, :cond_3

    .line 1144932
    :cond_2
    :goto_1
    move v0, v2

    .line 1144933
    goto :goto_0

    .line 1144934
    :cond_3
    iget-object v0, p0, LX/6mV;->ids:Ljava/util/List;

    if-eqz v0, :cond_6

    move v0, v1

    .line 1144935
    :goto_2
    iget-object v3, p1, LX/6mV;->ids:Ljava/util/List;

    if-eqz v3, :cond_7

    move v3, v1

    .line 1144936
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1144937
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1144938
    iget-object v0, p0, LX/6mV;->ids:Ljava/util/List;

    iget-object v3, p1, LX/6mV;->ids:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 1144939
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1144940
    goto :goto_2

    :cond_7
    move v3, v2

    .line 1144941
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1144942
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1144943
    sget-boolean v0, LX/6mV;->a:Z

    .line 1144944
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mV;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1144945
    return-object v0
.end method
