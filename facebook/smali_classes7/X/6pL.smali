.class public final enum LX/6pL;
.super LX/6pJ;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1149259
    invoke-direct {p0, p1, p2}, LX/6pJ;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final getFragment(LX/6pM;Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;Landroid/content/res/Resources;I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 1149260
    iget-object v0, p2, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1149261
    invoke-static {v0, p3, p1}, LX/6pJ;->getHeaderText(Ljava/lang/String;Landroid/content/res/Resources;LX/6pM;)Ljava/lang/String;

    move-result-object v0

    .line 1149262
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1149263
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 1149264
    const-string p1, "savedHeaderText"

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1149265
    const-string p1, "savedTag"

    invoke-virtual {p0, p1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1149266
    new-instance p1, Lcom/facebook/payments/auth/pin/ResetPinFragment;

    invoke-direct {p1}, Lcom/facebook/payments/auth/pin/ResetPinFragment;-><init>()V

    .line 1149267
    invoke-virtual {p1, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1149268
    move-object v0, p1

    .line 1149269
    return-object v0
.end method
