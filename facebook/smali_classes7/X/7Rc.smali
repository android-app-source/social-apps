.class public final enum LX/7Rc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Rc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Rc;

.field public static final enum STREAMING_FINISHED:LX/7Rc;

.field public static final enum STREAMING_INIT_COMPLETE:LX/7Rc;

.field public static final enum STREAMING_OFF:LX/7Rc;

.field public static final enum STREAMING_STARTED:LX/7Rc;

.field public static final enum STREAMING_STOPPED:LX/7Rc;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1207145
    new-instance v0, LX/7Rc;

    const-string v1, "STREAMING_OFF"

    invoke-direct {v0, v1, v2}, LX/7Rc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Rc;->STREAMING_OFF:LX/7Rc;

    .line 1207146
    new-instance v0, LX/7Rc;

    const-string v1, "STREAMING_INIT_COMPLETE"

    invoke-direct {v0, v1, v3}, LX/7Rc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Rc;->STREAMING_INIT_COMPLETE:LX/7Rc;

    .line 1207147
    new-instance v0, LX/7Rc;

    const-string v1, "STREAMING_STARTED"

    invoke-direct {v0, v1, v4}, LX/7Rc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Rc;->STREAMING_STARTED:LX/7Rc;

    .line 1207148
    new-instance v0, LX/7Rc;

    const-string v1, "STREAMING_STOPPED"

    invoke-direct {v0, v1, v5}, LX/7Rc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Rc;->STREAMING_STOPPED:LX/7Rc;

    .line 1207149
    new-instance v0, LX/7Rc;

    const-string v1, "STREAMING_FINISHED"

    invoke-direct {v0, v1, v6}, LX/7Rc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Rc;->STREAMING_FINISHED:LX/7Rc;

    .line 1207150
    const/4 v0, 0x5

    new-array v0, v0, [LX/7Rc;

    sget-object v1, LX/7Rc;->STREAMING_OFF:LX/7Rc;

    aput-object v1, v0, v2

    sget-object v1, LX/7Rc;->STREAMING_INIT_COMPLETE:LX/7Rc;

    aput-object v1, v0, v3

    sget-object v1, LX/7Rc;->STREAMING_STARTED:LX/7Rc;

    aput-object v1, v0, v4

    sget-object v1, LX/7Rc;->STREAMING_STOPPED:LX/7Rc;

    aput-object v1, v0, v5

    sget-object v1, LX/7Rc;->STREAMING_FINISHED:LX/7Rc;

    aput-object v1, v0, v6

    sput-object v0, LX/7Rc;->$VALUES:[LX/7Rc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1207151
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Rc;
    .locals 1

    .prologue
    .line 1207152
    const-class v0, LX/7Rc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Rc;

    return-object v0
.end method

.method public static values()[LX/7Rc;
    .locals 1

    .prologue
    .line 1207153
    sget-object v0, LX/7Rc;->$VALUES:[LX/7Rc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Rc;

    return-object v0
.end method
