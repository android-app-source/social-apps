.class public LX/8NU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/8NV;",
        "LX/8NW;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1337314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/8NU;
    .locals 1

    .prologue
    .line 1337315
    new-instance v0, LX/8NU;

    invoke-direct {v0}, LX/8NU;-><init>()V

    .line 1337316
    move-object v0, v0

    .line 1337317
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 1337318
    check-cast p1, LX/8NV;

    const/4 v5, 0x1

    .line 1337319
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1337320
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1337321
    const-string v2, "upload_settings_version"

    const-string v3, "v0.1"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337322
    const-string v2, "video"

    .line 1337323
    iget-object v3, p1, LX/8NV;->c:Ljava/util/Map;

    move-object v3, v3

    .line 1337324
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337325
    const-string v2, "context"

    .line 1337326
    iget-object v3, p1, LX/8NV;->d:Ljava/util/Map;

    move-object v3, v3

    .line 1337327
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337328
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "upload_setting_properties"

    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1337329
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v2.6/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1337330
    iget-wide v6, p1, LX/8NV;->b:J

    move-wide v2, v6

    .line 1337331
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/videos"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1337332
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "upload-video-chunk-settings"

    .line 1337333
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1337334
    move-object v2, v2

    .line 1337335
    const-string v3, "POST"

    .line 1337336
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 1337337
    move-object v2, v2

    .line 1337338
    iput-object v1, v2, LX/14O;->d:Ljava/lang/String;

    .line 1337339
    move-object v1, v2

    .line 1337340
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1337341
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1337342
    move-object v1, v1

    .line 1337343
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1337344
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1337345
    move-object v0, v1

    .line 1337346
    iput-boolean v5, v0, LX/14O;->n:Z

    .line 1337347
    move-object v0, v0

    .line 1337348
    iput-boolean v5, v0, LX/14O;->p:Z

    .line 1337349
    move-object v0, v0

    .line 1337350
    iget-object v1, p1, LX/8NV;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1337351
    iput-object v1, v0, LX/14O;->A:Ljava/lang/String;

    .line 1337352
    move-object v0, v0

    .line 1337353
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1337354
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1337355
    new-instance v1, LX/8NW;

    const-string v2, "transcode_dimension"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->D()J

    move-result-wide v2

    const-string v4, "transcode_bit_rate_bps"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->D()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, LX/8NW;-><init>(JJ)V

    return-object v1
.end method
