.class public LX/7ej;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7e0;


# static fields
.field public static final a:LX/2vn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vn",
            "<",
            "LX/7ef;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/2vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vq",
            "<",
            "LX/7ef;",
            "LX/7e2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/2vn;

    invoke-direct {v0}, LX/2vn;-><init>()V

    sput-object v0, LX/7ej;->a:LX/2vn;

    new-instance v0, LX/7eg;

    invoke-direct {v0}, LX/7eg;-><init>()V

    sput-object v0, LX/7ej;->b:LX/2vq;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2wX;Landroid/app/PendingIntent;LX/7e6;)LX/2wg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Landroid/app/PendingIntent;",
            "LX/7e6;",
            ")",
            "LX/2wg",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    invoke-static {p2}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, LX/7ei;

    invoke-direct {v0, p0, p1, p2, p3}, LX/7ei;-><init>(LX/7ej;LX/2wX;Landroid/app/PendingIntent;LX/7e6;)V

    invoke-virtual {p1, v0}, LX/2wX;->b(LX/2we;)LX/2we;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/6By;)V
    .locals 2

    const-string v0, "com.google.android.gms.nearby.messages.UPDATES"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p0

    if-nez p0, :cond_5

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    :goto_0
    move-object v0, p0

    move-object v0, v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/nearby/messages/internal/Update;

    const/4 p1, 0x1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/nearby/messages/internal/Update;->a(I)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, v1, Lcom/google/android/gms/nearby/messages/internal/Update;->c:Lcom/google/android/gms/nearby/messages/Message;

    invoke-virtual {p2, p1}, LX/6By;->a(Lcom/google/android/gms/nearby/messages/Message;)V

    :cond_1
    const/4 p1, 0x2

    invoke-virtual {v1, p1}, Lcom/google/android/gms/nearby/messages/internal/Update;->a(I)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, v1, Lcom/google/android/gms/nearby/messages/internal/Update;->c:Lcom/google/android/gms/nearby/messages/Message;

    invoke-virtual {p2, p1}, LX/6By;->b(Lcom/google/android/gms/nearby/messages/Message;)V

    :cond_2
    const/4 p1, 0x4

    invoke-virtual {v1, p1}, Lcom/google/android/gms/nearby/messages/internal/Update;->a(I)Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_3
    const/16 p1, 0x8

    invoke-virtual {v1, p1}, Lcom/google/android/gms/nearby/messages/internal/Update;->a(I)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_1

    :cond_4
    return-void

    :cond_5
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p0

    goto :goto_0
.end method
