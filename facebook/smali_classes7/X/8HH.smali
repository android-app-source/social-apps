.class public LX/8HH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/8HH;


# instance fields
.field private final a:LX/18V;

.field private final b:LX/03V;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/8Gm;

.field private final e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7yX;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Z

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1E2;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/1EI;

.field private final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1E2;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/18V;LX/03V;Ljava/util/concurrent/ExecutorService;LX/8Gm;Ljava/lang/Boolean;LX/0Or;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/8Gm;",
            "Ljava/lang/Boolean;",
            "LX/0Or",
            "<",
            "LX/1E2;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320784
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/8HH;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1320785
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/8HH;->f:Ljava/util/List;

    .line 1320786
    new-instance v0, LX/8HE;

    invoke-direct {v0, p0}, LX/8HE;-><init>(LX/8HH;)V

    iput-object v0, p0, LX/8HH;->i:LX/1EI;

    .line 1320787
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/8HH;->j:Ljava/util/Set;

    .line 1320788
    iput-object p1, p0, LX/8HH;->a:LX/18V;

    .line 1320789
    iput-object p2, p0, LX/8HH;->b:LX/03V;

    .line 1320790
    iput-object p3, p0, LX/8HH;->c:Ljava/util/concurrent/ExecutorService;

    .line 1320791
    iput-object p4, p0, LX/8HH;->d:LX/8Gm;

    .line 1320792
    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/8HH;->g:Z

    .line 1320793
    iput-object p6, p0, LX/8HH;->h:LX/0Or;

    .line 1320794
    return-void
.end method

.method public static a(LX/0QB;)LX/8HH;
    .locals 10

    .prologue
    .line 1320770
    sget-object v0, LX/8HH;->l:LX/8HH;

    if-nez v0, :cond_1

    .line 1320771
    const-class v1, LX/8HH;

    monitor-enter v1

    .line 1320772
    :try_start_0
    sget-object v0, LX/8HH;->l:LX/8HH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1320773
    if-eqz v2, :cond_0

    .line 1320774
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1320775
    new-instance v3, LX/8HH;

    invoke-static {v0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v4

    check-cast v4, LX/18V;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/8Gm;->b(LX/0QB;)LX/8Gm;

    move-result-object v7

    check-cast v7, LX/8Gm;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    const/16 v9, 0x1265

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/8HH;-><init>(LX/18V;LX/03V;Ljava/util/concurrent/ExecutorService;LX/8Gm;Ljava/lang/Boolean;LX/0Or;)V

    .line 1320776
    move-object v0, v3

    .line 1320777
    sput-object v0, LX/8HH;->l:LX/8HH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1320778
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1320779
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1320780
    :cond_1
    sget-object v0, LX/8HH;->l:LX/8HH;

    return-object v0

    .line 1320781
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1320782
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/8HH;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1320795
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    .line 1320796
    :try_start_0
    new-instance v1, LX/8HG;

    iget-boolean v2, p0, LX/8HH;->g:Z

    invoke-direct {v1, v2}, LX/8HG;-><init>(Z)V

    .line 1320797
    iget-object v2, p0, LX/8HH;->a:LX/18V;

    new-instance v3, LX/8HF;

    invoke-direct {v3, p0}, LX/8HF;-><init>(LX/8HH;)V

    invoke-virtual {v2, v3, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LX/8HH;->k:Ljava/util/List;

    .line 1320798
    iget-object v0, p0, LX/8HH;->k:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1320799
    iget-object v0, p0, LX/8HH;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yX;

    .line 1320800
    if-eqz v0, :cond_0

    .line 1320801
    iget-object v2, p0, LX/8HH;->k:Ljava/util/List;

    invoke-interface {v0, v2}, LX/7yX;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1320802
    :catch_0
    move-exception v0

    .line 1320803
    :try_start_1
    iget-object v1, p0, LX/8HH;->b:LX/03V;

    const-string v2, "FetchDefaultTagSuggestions"

    const-string v3, "FaceRecMethod threw an Error"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1320804
    iget-object v0, p0, LX/8HH;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    .line 1320805
    :goto_1
    iget-object v0, p0, LX/8HH;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1320806
    return-void

    .line 1320807
    :cond_1
    iget-object v0, p0, LX/8HH;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/8HH;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    throw v0
.end method


# virtual methods
.method public final a(LX/7yX;)V
    .locals 4

    .prologue
    .line 1320760
    iget-object v0, p0, LX/8HH;->k:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 1320761
    iget-object v0, p0, LX/8HH;->k:Ljava/util/List;

    invoke-interface {p1, v0}, LX/7yX;->a(Ljava/util/List;)V

    .line 1320762
    :cond_0
    :goto_0
    iget-object v0, p0, LX/8HH;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1E2;

    .line 1320763
    iget-object v1, p0, LX/8HH;->j:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1320764
    iget-object v1, p0, LX/8HH;->i:LX/1EI;

    invoke-virtual {v0, v1}, LX/1E2;->a(LX/1EI;)V

    .line 1320765
    iget-object v1, p0, LX/8HH;->j:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1320766
    :cond_1
    return-void

    .line 1320767
    :cond_2
    iget-object v0, p0, LX/8HH;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320768
    iget-object v0, p0, LX/8HH;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1320769
    iget-object v0, p0, LX/8HH;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/photos/data/method/FetchDefaultTagSuggestions$2;

    const-string v2, "FetchDefaultTagSuggestions"

    const-string v3, "GetDefaultTagSuggestions"

    invoke-direct {v1, p0, v2, v3}, Lcom/facebook/photos/data/method/FetchDefaultTagSuggestions$2;-><init>(LX/8HH;Ljava/lang/String;Ljava/lang/String;)V

    const v2, 0x5cc4901e

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 1320757
    const/4 v0, 0x0

    iput-object v0, p0, LX/8HH;->k:Ljava/util/List;

    .line 1320758
    iget-object v0, p0, LX/8HH;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1320759
    return-void
.end method
