.class public final LX/7Un;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;)V
    .locals 0

    .prologue
    .line 1213627
    iput-object p1, p0, LX/7Un;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 1213628
    iget-object v0, p0, LX/7Un;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    iget-object v1, v0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->h:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213629
    iget-object v0, p0, LX/7Un;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    invoke-virtual {v0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213630
    return-void
.end method
