.class public LX/7gJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3FQ;


# instance fields
.field private final a:LX/2oM;


# direct methods
.method public constructor <init>(LX/2oM;)V
    .locals 0

    .prologue
    .line 1224125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224126
    iput-object p1, p0, LX/7gJ;->a:LX/2oM;

    .line 1224127
    return-void
.end method


# virtual methods
.method public final getAudioChannelLayout()LX/03z;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1224124
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getLastStartPosition()I
    .locals 1

    .prologue
    .line 1224123
    const/4 v0, 0x0

    return v0
.end method

.method public final getProjectionType()LX/19o;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1224122
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getSeekPosition()I
    .locals 1

    .prologue
    .line 1224119
    iget-object v0, p0, LX/7gJ;->a:LX/2oM;

    if-nez v0, :cond_0

    .line 1224120
    const/4 v0, 0x0

    .line 1224121
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/7gJ;->a:LX/2oM;

    invoke-interface {v0}, LX/2oM;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final getTransitionNode()LX/3FT;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1224117
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getVideoStoryPersistentState()LX/2oM;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1224118
    iget-object v0, p0, LX/7gJ;->a:LX/2oM;

    return-object v0
.end method
