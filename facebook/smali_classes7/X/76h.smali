.class public final LX/76h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;)V
    .locals 0

    .prologue
    .line 1171227
    iput-object p1, p0, LX/76h;->a:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    .line 1171228
    new-instance v0, Lcom/facebook/interstitial/api/FetchInterstitialsParams;

    iget-object v1, p0, LX/76h;->a:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    iget-object v1, v1, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->l:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/api/FetchInterstitialsParams;-><init>(LX/0Px;)V

    .line 1171229
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1171230
    const-string v2, "fetchAndUpdateInterstitialsParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1171231
    iget-object v0, p0, LX/76h;->a:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    iget-object v0, v0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->e:LX/0aG;

    const-string v2, "interstitials_fetch_and_update"

    const v3, 0x32092ba4

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    .line 1171232
    new-instance v1, LX/4At;

    iget-object v2, p0, LX/76h;->a:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    const-string v3, "Fetching Promotions..."

    invoke-direct {v1, v2, v3}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/1MF;->setOperationProgressIndicator(LX/4At;)LX/1MF;

    .line 1171233
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    new-instance v1, LX/76g;

    invoke-direct {v1, p0}, LX/76g;-><init>(LX/76h;)V

    iget-object v2, p0, LX/76h;->a:Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    iget-object v2, v2, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->h:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1171234
    const/4 v0, 0x1

    return v0
.end method
